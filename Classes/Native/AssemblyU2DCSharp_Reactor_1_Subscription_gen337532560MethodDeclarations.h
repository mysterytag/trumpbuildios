﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<UniRx.Unit>
struct Subscription_t337532562;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<UniRx.Unit>::.ctor()
extern "C"  void Subscription__ctor_m47790428_gshared (Subscription_t337532562 * __this, const MethodInfo* method);
#define Subscription__ctor_m47790428(__this, method) ((  void (*) (Subscription_t337532562 *, const MethodInfo*))Subscription__ctor_m47790428_gshared)(__this, method)
// System.Void Reactor`1/Subscription<UniRx.Unit>::Dispose()
extern "C"  void Subscription_Dispose_m2875211417_gshared (Subscription_t337532562 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2875211417(__this, method) ((  void (*) (Subscription_t337532562 *, const MethodInfo*))Subscription_Dispose_m2875211417_gshared)(__this, method)
