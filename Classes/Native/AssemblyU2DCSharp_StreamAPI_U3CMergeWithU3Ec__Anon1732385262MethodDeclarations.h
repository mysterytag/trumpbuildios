﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<MergeWith>c__AnonStorey139`1<System.Object>
struct U3CMergeWithU3Ec__AnonStorey139_1_t1732385262;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void StreamAPI/<MergeWith>c__AnonStorey139`1<System.Object>::.ctor()
extern "C"  void U3CMergeWithU3Ec__AnonStorey139_1__ctor_m3507914823_gshared (U3CMergeWithU3Ec__AnonStorey139_1_t1732385262 * __this, const MethodInfo* method);
#define U3CMergeWithU3Ec__AnonStorey139_1__ctor_m3507914823(__this, method) ((  void (*) (U3CMergeWithU3Ec__AnonStorey139_1_t1732385262 *, const MethodInfo*))U3CMergeWithU3Ec__AnonStorey139_1__ctor_m3507914823_gshared)(__this, method)
// System.IDisposable StreamAPI/<MergeWith>c__AnonStorey139`1<System.Object>::<>m__1C8(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CMergeWithU3Ec__AnonStorey139_1_U3CU3Em__1C8_m301960375_gshared (U3CMergeWithU3Ec__AnonStorey139_1_t1732385262 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CMergeWithU3Ec__AnonStorey139_1_U3CU3Em__1C8_m301960375(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CMergeWithU3Ec__AnonStorey139_1_t1732385262 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CMergeWithU3Ec__AnonStorey139_1_U3CU3Em__1C8_m301960375_gshared)(__this, ___reaction0, ___p1, method)
