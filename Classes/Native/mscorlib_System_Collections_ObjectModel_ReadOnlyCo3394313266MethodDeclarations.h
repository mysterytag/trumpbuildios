﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4000251768MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m3227828347(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2091793637(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, Tuple_2_t231167918 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2427784325(__this, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1730389388(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, Tuple_2_t231167918 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2306187314(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3394313266 *, Tuple_2_t231167918 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3899209554(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m741491192(__this, ___index0, method) ((  Tuple_2_t231167918 * (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2561454947(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, Tuple_2_t231167918 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1273553181(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2286192554(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2637181241(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2775162020(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3394313266 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3924475576(__this, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1961255964(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3394313266 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m394453244(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3394313266 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3912424047(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m75728473(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3932938367(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3331272128(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3303141554(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2820823179(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1237739790(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1403813305(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2357077638(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::Contains(T)
#define ReadOnlyCollection_1_Contains_m3828270967(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3394313266 *, Tuple_2_t231167918 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1628548885(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3394313266 *, Tuple_2U5BU5D_t2532318683*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m1769126222(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m2971898273(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3394313266 *, Tuple_2_t231167918 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1751840762(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3394313266 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3044179896(__this, ___index0, method) ((  Tuple_2_t231167918 * (*) (ReadOnlyCollection_1_t3394313266 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
