﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/<PostInject>c__AnonStoreyE2
struct U3CPostInjectU3Ec__AnonStoreyE2_t980490755;
// ConnectionCollector
struct ConnectionCollector_t444796719;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharp_ConnectionCollector444796719.h"

// System.Void GlobalStat/<PostInject>c__AnonStoreyE2::.ctor()
extern "C"  void U3CPostInjectU3Ec__AnonStoreyE2__ctor_m3208083536 (U3CPostInjectU3Ec__AnonStoreyE2_t980490755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<PostInject>c__AnonStoreyE2::<>m__10E(System.DateTime,ConnectionCollector)
extern "C"  void U3CPostInjectU3Ec__AnonStoreyE2_U3CU3Em__10E_m401424582 (U3CPostInjectU3Ec__AnonStoreyE2_t980490755 * __this, DateTime_t339033936  ___date0, ConnectionCollector_t444796719 * ___connections1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<PostInject>c__AnonStoreyE2::<>m__10F(System.DateTime,ConnectionCollector)
extern "C"  void U3CPostInjectU3Ec__AnonStoreyE2_U3CU3Em__10F_m2034228581 (U3CPostInjectU3Ec__AnonStoreyE2_t980490755 * __this, DateTime_t339033936  ___date0, ConnectionCollector_t444796719 * ___connections1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
