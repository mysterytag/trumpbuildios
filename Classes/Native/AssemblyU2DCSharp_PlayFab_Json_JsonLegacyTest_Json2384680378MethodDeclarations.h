﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Json.JsonLegacyTest/JsonTest
struct JsonTest_t2384680378;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.Json.JsonLegacyTest/JsonTest::.ctor()
extern "C"  void JsonTest__ctor_m1393673152 (JsonTest_t2384680378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
