﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Single,System.Single>
struct OperatorObserverBase_2_t2516778257;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Single,System.Single>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1873532218_gshared (OperatorObserverBase_2_t2516778257 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m1873532218(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t2516778257 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m1873532218_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Single,System.Single>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m1488289008_gshared (OperatorObserverBase_2_t2516778257 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m1488289008(__this, method) ((  void (*) (OperatorObserverBase_2_t2516778257 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m1488289008_gshared)(__this, method)
