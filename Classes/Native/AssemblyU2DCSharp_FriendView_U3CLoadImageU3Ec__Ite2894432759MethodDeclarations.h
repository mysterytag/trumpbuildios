﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendView/<LoadImage>c__IteratorC
struct U3CLoadImageU3Ec__IteratorC_t2894432759;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendView/<LoadImage>c__IteratorC::.ctor()
extern "C"  void U3CLoadImageU3Ec__IteratorC__ctor_m2516391568 (U3CLoadImageU3Ec__IteratorC_t2894432759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FriendView/<LoadImage>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadImageU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3710604300 (U3CLoadImageU3Ec__IteratorC_t2894432759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FriendView/<LoadImage>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadImageU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1908338592 (U3CLoadImageU3Ec__IteratorC_t2894432759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendView/<LoadImage>c__IteratorC::MoveNext()
extern "C"  bool U3CLoadImageU3Ec__IteratorC_MoveNext_m1266264140 (U3CLoadImageU3Ec__IteratorC_t2894432759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendView/<LoadImage>c__IteratorC::Dispose()
extern "C"  void U3CLoadImageU3Ec__IteratorC_Dispose_m83992269 (U3CLoadImageU3Ec__IteratorC_t2894432759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendView/<LoadImage>c__IteratorC::Reset()
extern "C"  void U3CLoadImageU3Ec__IteratorC_Reset_m162824509 (U3CLoadImageU3Ec__IteratorC_t2894432759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
