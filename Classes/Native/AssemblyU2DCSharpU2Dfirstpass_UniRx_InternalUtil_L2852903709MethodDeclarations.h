﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.Unit>
struct ListObserver_1_t2852903709;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Unit>>
struct ImmutableList_1_t1535522144;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2218457479_gshared (ListObserver_1_t2852903709 * __this, ImmutableList_1_t1535522144 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2218457479(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2852903709 *, ImmutableList_1_t1535522144 *, const MethodInfo*))ListObserver_1__ctor_m2218457479_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m776971135_gshared (ListObserver_1_t2852903709 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m776971135(__this, method) ((  void (*) (ListObserver_1_t2852903709 *, const MethodInfo*))ListObserver_1_OnCompleted_m776971135_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m211684908_gshared (ListObserver_1_t2852903709 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m211684908(__this, ___error0, method) ((  void (*) (ListObserver_1_t2852903709 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m211684908_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1096142109_gshared (ListObserver_1_t2852903709 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1096142109(__this, ___value0, method) ((  void (*) (ListObserver_1_t2852903709 *, Unit_t2558286038 , const MethodInfo*))ListObserver_1_OnNext_m1096142109_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1792376253_gshared (ListObserver_1_t2852903709 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1792376253(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2852903709 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1792376253_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Unit>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2080974958_gshared (ListObserver_1_t2852903709 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2080974958(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2852903709 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2080974958_gshared)(__this, ___observer0, method)
