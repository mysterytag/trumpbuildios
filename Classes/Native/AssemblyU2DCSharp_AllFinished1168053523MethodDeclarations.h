﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllFinished
struct AllFinished_t1168053523;

#include "codegen/il2cpp-codegen.h"

// System.Void AllFinished::.ctor()
extern "C"  void AllFinished__ctor_m405236664 (AllFinished_t1168053523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
