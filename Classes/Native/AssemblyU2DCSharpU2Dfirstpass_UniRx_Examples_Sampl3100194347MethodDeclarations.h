﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample09_EventHandling/MyEventArgs
struct MyEventArgs_t3100194347;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample09_EventHandling/MyEventArgs::.ctor()
extern "C"  void MyEventArgs__ctor_m2051191323 (MyEventArgs_t3100194347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.Examples.Sample09_EventHandling/MyEventArgs::get_MyProperty()
extern "C"  int32_t MyEventArgs_get_MyProperty_m1029180861 (MyEventArgs_t3100194347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/MyEventArgs::set_MyProperty(System.Int32)
extern "C"  void MyEventArgs_set_MyProperty_m1957490872 (MyEventArgs_t3100194347 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
