﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<Zenject.InjectContext,UnityEngine.GameObject>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2094553033(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1501866983 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Zenject.InjectContext,UnityEngine.GameObject>::Invoke(T)
#define Func_2_Invoke_m3736184957(__this, ___arg10, method) ((  GameObject_t4012695102 * (*) (Func_2_t1501866983 *, InjectContext_t3456483891 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Zenject.InjectContext,UnityEngine.GameObject>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2886915248(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1501866983 *, InjectContext_t3456483891 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Zenject.InjectContext,UnityEngine.GameObject>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2592586615(__this, ___result0, method) ((  GameObject_t4012695102 * (*) (Func_2_t1501866983 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
