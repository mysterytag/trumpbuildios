﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetSharedGroupData>c__AnonStoreyBE
struct U3CGetSharedGroupDataU3Ec__AnonStoreyBE_t2214909718;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetSharedGroupData>c__AnonStoreyBE::.ctor()
extern "C"  void U3CGetSharedGroupDataU3Ec__AnonStoreyBE__ctor_m754909533 (U3CGetSharedGroupDataU3Ec__AnonStoreyBE_t2214909718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetSharedGroupData>c__AnonStoreyBE::<>m__AB(PlayFab.CallRequestContainer)
extern "C"  void U3CGetSharedGroupDataU3Ec__AnonStoreyBE_U3CU3Em__AB_m865527772 (U3CGetSharedGroupDataU3Ec__AnonStoreyBE_t2214909718 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
