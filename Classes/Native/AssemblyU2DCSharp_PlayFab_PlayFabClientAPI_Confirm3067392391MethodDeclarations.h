﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ConfirmPurchaseResponseCallback
struct ConfirmPurchaseResponseCallback_t3067392391;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ConfirmPurchaseRequest
struct ConfirmPurchaseRequest_t1360937886;
// PlayFab.ClientModels.ConfirmPurchaseResult
struct ConfirmPurchaseResult_t633238350;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConfirmPurc1360937886.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConfirmPurch633238350.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ConfirmPurchaseResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ConfirmPurchaseResponseCallback__ctor_m1877886614 (ConfirmPurchaseResponseCallback_t3067392391 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConfirmPurchaseResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ConfirmPurchaseRequest,PlayFab.ClientModels.ConfirmPurchaseResult,PlayFab.PlayFabError,System.Object)
extern "C"  void ConfirmPurchaseResponseCallback_Invoke_m1115734531 (ConfirmPurchaseResponseCallback_t3067392391 * __this, String_t* ___urlPath0, int32_t ___callId1, ConfirmPurchaseRequest_t1360937886 * ___request2, ConfirmPurchaseResult_t633238350 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ConfirmPurchaseResponseCallback_t3067392391(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ConfirmPurchaseRequest_t1360937886 * ___request2, ConfirmPurchaseResult_t633238350 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ConfirmPurchaseResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ConfirmPurchaseRequest,PlayFab.ClientModels.ConfirmPurchaseResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConfirmPurchaseResponseCallback_BeginInvoke_m1667482552 (ConfirmPurchaseResponseCallback_t3067392391 * __this, String_t* ___urlPath0, int32_t ___callId1, ConfirmPurchaseRequest_t1360937886 * ___request2, ConfirmPurchaseResult_t633238350 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConfirmPurchaseResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ConfirmPurchaseResponseCallback_EndInvoke_m1416131494 (ConfirmPurchaseResponseCallback_t3067392391 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
