﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadValuesCompletedEventHandler
struct UploadValuesCompletedEventHandler_t1802287554;
// System.Object
struct Il2CppObject;
// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t1359222777;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_UploadValuesCompletedEventArgs1359222777.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.UploadValuesCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadValuesCompletedEventHandler__ctor_m1088471886 (UploadValuesCompletedEventHandler_t1802287554 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadValuesCompletedEventHandler::Invoke(System.Object,System.Net.UploadValuesCompletedEventArgs)
extern "C"  void UploadValuesCompletedEventHandler_Invoke_m2739632025 (UploadValuesCompletedEventHandler_t1802287554 * __this, Il2CppObject * ___sender0, UploadValuesCompletedEventArgs_t1359222777 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UploadValuesCompletedEventHandler_t1802287554(Il2CppObject* delegate, Il2CppObject * ___sender0, UploadValuesCompletedEventArgs_t1359222777 * ___e1);
// System.IAsyncResult System.Net.UploadValuesCompletedEventHandler::BeginInvoke(System.Object,System.Net.UploadValuesCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UploadValuesCompletedEventHandler_BeginInvoke_m4064605312 (UploadValuesCompletedEventHandler_t1802287554 * __this, Il2CppObject * ___sender0, UploadValuesCompletedEventArgs_t1359222777 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadValuesCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UploadValuesCompletedEventHandler_EndInvoke_m3016100958 (UploadValuesCompletedEventHandler_t1802287554 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
