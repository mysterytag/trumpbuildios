﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>
struct U3COnNextDelayU3Ec__Iterator26_t3660093125;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::.ctor()
extern "C"  void U3COnNextDelayU3Ec__Iterator26__ctor_m138004466_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method);
#define U3COnNextDelayU3Ec__Iterator26__ctor_m138004466(__this, method) ((  void (*) (U3COnNextDelayU3Ec__Iterator26_t3660093125 *, const MethodInfo*))U3COnNextDelayU3Ec__Iterator26__ctor_m138004466_gshared)(__this, method)
// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3COnNextDelayU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2756900640_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method);
#define U3COnNextDelayU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2756900640(__this, method) ((  Il2CppObject * (*) (U3COnNextDelayU3Ec__Iterator26_t3660093125 *, const MethodInfo*))U3COnNextDelayU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2756900640_gshared)(__this, method)
// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3COnNextDelayU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m3617012404_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method);
#define U3COnNextDelayU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m3617012404(__this, method) ((  Il2CppObject * (*) (U3COnNextDelayU3Ec__Iterator26_t3660093125 *, const MethodInfo*))U3COnNextDelayU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m3617012404_gshared)(__this, method)
// System.Boolean UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::MoveNext()
extern "C"  bool U3COnNextDelayU3Ec__Iterator26_MoveNext_m2925189850_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method);
#define U3COnNextDelayU3Ec__Iterator26_MoveNext_m2925189850(__this, method) ((  bool (*) (U3COnNextDelayU3Ec__Iterator26_t3660093125 *, const MethodInfo*))U3COnNextDelayU3Ec__Iterator26_MoveNext_m2925189850_gshared)(__this, method)
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::Dispose()
extern "C"  void U3COnNextDelayU3Ec__Iterator26_Dispose_m3671556015_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method);
#define U3COnNextDelayU3Ec__Iterator26_Dispose_m3671556015(__this, method) ((  void (*) (U3COnNextDelayU3Ec__Iterator26_t3660093125 *, const MethodInfo*))U3COnNextDelayU3Ec__Iterator26_Dispose_m3671556015_gshared)(__this, method)
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>::Reset()
extern "C"  void U3COnNextDelayU3Ec__Iterator26_Reset_m2079404703_gshared (U3COnNextDelayU3Ec__Iterator26_t3660093125 * __this, const MethodInfo* method);
#define U3COnNextDelayU3Ec__Iterator26_Reset_m2079404703(__this, method) ((  void (*) (U3COnNextDelayU3Ec__Iterator26_t3660093125 *, const MethodInfo*))U3COnNextDelayU3Ec__Iterator26_Reset_m2079404703_gshared)(__this, method)
