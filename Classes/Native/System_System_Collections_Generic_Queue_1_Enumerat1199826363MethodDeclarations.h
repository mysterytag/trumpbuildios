﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.IObservable`1<UniRx.Unit>>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m2570997970(__this, ___q0, method) ((  void (*) (Enumerator_t1199826363 *, Queue_1_t4025171942 *, const MethodInfo*))Enumerator__ctor_m3846579967_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.IObservable`1<UniRx.Unit>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2203787277(__this, method) ((  void (*) (Enumerator_t1199826363 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2396412922_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<UniRx.IObservable`1<UniRx.Unit>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4147733955(__this, method) ((  Il2CppObject * (*) (Enumerator_t1199826363 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m280731440_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.IObservable`1<UniRx.Unit>>::Dispose()
#define Enumerator_Dispose_m959477834(__this, method) ((  void (*) (Enumerator_t1199826363 *, const MethodInfo*))Enumerator_Dispose_m3069945149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UniRx.IObservable`1<UniRx.Unit>>::MoveNext()
#define Enumerator_MoveNext_m3121525713(__this, method) ((  bool (*) (Enumerator_t1199826363 *, const MethodInfo*))Enumerator_MoveNext_m262168254_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<UniRx.IObservable`1<UniRx.Unit>>::get_Current()
#define Enumerator_get_Current_m896725148(__this, method) ((  Il2CppObject* (*) (Enumerator_t1199826363 *, const MethodInfo*))Enumerator_get_Current_m3381813839_gshared)(__this, method)
