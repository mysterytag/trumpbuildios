﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>
struct GroupByObservable_3_t657897610;
// System.Collections.Generic.Dictionary`2<System.Object,UniRx.ISubject`1<System.Object>>
struct Dictionary_2_t3341028665;
// UniRx.ISubject`1<System.Object>
struct ISubject_1_t353709935;
// UniRx.CompositeDisposable
struct CompositeDisposable_t1894629977;
// UniRx.RefCountDisposable
struct RefCountDisposable_t3288429070;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1657047628.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.GroupByObservable`3/GroupBy<System.Object,System.Object,System.Object>
struct  GroupBy_t3470189943  : public OperatorObserverBase_2_t1657047628
{
public:
	// UniRx.Operators.GroupByObservable`3<TSource,TKey,TElement> UniRx.Operators.GroupByObservable`3/GroupBy::parent
	GroupByObservable_3_t657897610 * ___parent_2;
	// System.Collections.Generic.Dictionary`2<TKey,UniRx.ISubject`1<TElement>> UniRx.Operators.GroupByObservable`3/GroupBy::map
	Dictionary_2_t3341028665 * ___map_3;
	// UniRx.ISubject`1<TElement> UniRx.Operators.GroupByObservable`3/GroupBy::nullKeySubject
	Il2CppObject* ___nullKeySubject_4;
	// UniRx.CompositeDisposable UniRx.Operators.GroupByObservable`3/GroupBy::groupDisposable
	CompositeDisposable_t1894629977 * ___groupDisposable_5;
	// UniRx.RefCountDisposable UniRx.Operators.GroupByObservable`3/GroupBy::refCountDisposable
	RefCountDisposable_t3288429070 * ___refCountDisposable_6;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(GroupBy_t3470189943, ___parent_2)); }
	inline GroupByObservable_3_t657897610 * get_parent_2() const { return ___parent_2; }
	inline GroupByObservable_3_t657897610 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(GroupByObservable_3_t657897610 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_map_3() { return static_cast<int32_t>(offsetof(GroupBy_t3470189943, ___map_3)); }
	inline Dictionary_2_t3341028665 * get_map_3() const { return ___map_3; }
	inline Dictionary_2_t3341028665 ** get_address_of_map_3() { return &___map_3; }
	inline void set_map_3(Dictionary_2_t3341028665 * value)
	{
		___map_3 = value;
		Il2CppCodeGenWriteBarrier(&___map_3, value);
	}

	inline static int32_t get_offset_of_nullKeySubject_4() { return static_cast<int32_t>(offsetof(GroupBy_t3470189943, ___nullKeySubject_4)); }
	inline Il2CppObject* get_nullKeySubject_4() const { return ___nullKeySubject_4; }
	inline Il2CppObject** get_address_of_nullKeySubject_4() { return &___nullKeySubject_4; }
	inline void set_nullKeySubject_4(Il2CppObject* value)
	{
		___nullKeySubject_4 = value;
		Il2CppCodeGenWriteBarrier(&___nullKeySubject_4, value);
	}

	inline static int32_t get_offset_of_groupDisposable_5() { return static_cast<int32_t>(offsetof(GroupBy_t3470189943, ___groupDisposable_5)); }
	inline CompositeDisposable_t1894629977 * get_groupDisposable_5() const { return ___groupDisposable_5; }
	inline CompositeDisposable_t1894629977 ** get_address_of_groupDisposable_5() { return &___groupDisposable_5; }
	inline void set_groupDisposable_5(CompositeDisposable_t1894629977 * value)
	{
		___groupDisposable_5 = value;
		Il2CppCodeGenWriteBarrier(&___groupDisposable_5, value);
	}

	inline static int32_t get_offset_of_refCountDisposable_6() { return static_cast<int32_t>(offsetof(GroupBy_t3470189943, ___refCountDisposable_6)); }
	inline RefCountDisposable_t3288429070 * get_refCountDisposable_6() const { return ___refCountDisposable_6; }
	inline RefCountDisposable_t3288429070 ** get_address_of_refCountDisposable_6() { return &___refCountDisposable_6; }
	inline void set_refCountDisposable_6(RefCountDisposable_t3288429070 * value)
	{
		___refCountDisposable_6 = value;
		Il2CppCodeGenWriteBarrier(&___refCountDisposable_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
