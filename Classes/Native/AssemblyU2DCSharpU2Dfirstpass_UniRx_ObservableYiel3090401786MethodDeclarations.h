﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel2959019304MethodDeclarations.h"

// System.Void UniRx.ObservableYieldInstruction`1<System.String>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
#define ObservableYieldInstruction_1__ctor_m3556264580(__this, ___source0, ___reThrowOnError1, method) ((  void (*) (ObservableYieldInstruction_1_t3090401786 *, Il2CppObject*, bool, const MethodInfo*))ObservableYieldInstruction_1__ctor_m1999879574_gshared)(__this, ___source0, ___reThrowOnError1, method)
// T UniRx.ObservableYieldInstruction`1<System.String>::System.Collections.Generic.IEnumerator<T>.get_Current()
#define ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1490586561(__this, method) ((  String_t* (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1250220399_gshared)(__this, method)
// System.Object UniRx.ObservableYieldInstruction`1<System.String>::System.Collections.IEnumerator.get_Current()
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m3053031404(__this, method) ((  Il2CppObject * (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m1496646398_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.String>::System.Collections.IEnumerator.MoveNext()
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m2695870033(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m539323519_gshared)(__this, method)
// System.Void UniRx.ObservableYieldInstruction`1<System.String>::System.Collections.IEnumerator.Reset()
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m4070488128(__this, method) ((  void (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m3085590738_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.String>::get_HasError()
#define ObservableYieldInstruction_1_get_HasError_m2772237829(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_get_HasError_m685161623_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.String>::get_HasResult()
#define ObservableYieldInstruction_1_get_HasResult_m2603143266(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_get_HasResult_m2328290320_gshared)(__this, method)
// T UniRx.ObservableYieldInstruction`1<System.String>::get_Result()
#define ObservableYieldInstruction_1_get_Result_m722695953(__this, method) ((  String_t* (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_get_Result_m751809059_gshared)(__this, method)
// System.Exception UniRx.ObservableYieldInstruction`1<System.String>::get_Error()
#define ObservableYieldInstruction_1_get_Error_m2578890956(__this, method) ((  Exception_t1967233988 * (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_get_Error_m4242398074_gshared)(__this, method)
// System.Void UniRx.ObservableYieldInstruction`1<System.String>::Dispose()
#define ObservableYieldInstruction_1_Dispose_m69993271(__this, method) ((  void (*) (ObservableYieldInstruction_1_t3090401786 *, const MethodInfo*))ObservableYieldInstruction_1_Dispose_m974516581_gshared)(__this, method)
