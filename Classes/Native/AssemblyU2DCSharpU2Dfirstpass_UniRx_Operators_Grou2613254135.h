﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.RefCountDisposable
struct RefCountDisposable_t3288429070;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.GroupedObservable`2<System.Object,System.Object>
struct  GroupedObservable_2_t2613254135  : public Il2CppObject
{
public:
	// TKey UniRx.Operators.GroupedObservable`2::key
	Il2CppObject * ___key_0;
	// UniRx.IObservable`1<TElement> UniRx.Operators.GroupedObservable`2::subject
	Il2CppObject* ___subject_1;
	// UniRx.RefCountDisposable UniRx.Operators.GroupedObservable`2::refCount
	RefCountDisposable_t3288429070 * ___refCount_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(GroupedObservable_2_t2613254135, ___key_0)); }
	inline Il2CppObject * get_key_0() const { return ___key_0; }
	inline Il2CppObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Il2CppObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_subject_1() { return static_cast<int32_t>(offsetof(GroupedObservable_2_t2613254135, ___subject_1)); }
	inline Il2CppObject* get_subject_1() const { return ___subject_1; }
	inline Il2CppObject** get_address_of_subject_1() { return &___subject_1; }
	inline void set_subject_1(Il2CppObject* value)
	{
		___subject_1 = value;
		Il2CppCodeGenWriteBarrier(&___subject_1, value);
	}

	inline static int32_t get_offset_of_refCount_2() { return static_cast<int32_t>(offsetof(GroupedObservable_2_t2613254135, ___refCount_2)); }
	inline RefCountDisposable_t3288429070 * get_refCount_2() const { return ___refCount_2; }
	inline RefCountDisposable_t3288429070 ** get_address_of_refCount_2() { return &___refCount_2; }
	inline void set_refCount_2(RefCountDisposable_t3288429070 * value)
	{
		___refCount_2 = value;
		Il2CppCodeGenWriteBarrier(&___refCount_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
