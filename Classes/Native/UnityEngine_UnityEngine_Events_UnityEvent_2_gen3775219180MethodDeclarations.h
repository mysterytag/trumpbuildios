﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t3775219180;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t2076416286;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1733537956;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"

// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_2__ctor_m1950601551_gshared (UnityEvent_2_t3775219180 * __this, const MethodInfo* method);
#define UnityEvent_2__ctor_m1950601551(__this, method) ((  void (*) (UnityEvent_2_t3775219180 *, const MethodInfo*))UnityEvent_2__ctor_m1950601551_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::AddListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_AddListener_m3895966050_gshared (UnityEvent_2_t3775219180 * __this, UnityAction_2_t2076416286 * ___call0, const MethodInfo* method);
#define UnityEvent_2_AddListener_m3895966050(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3775219180 *, UnityAction_2_t2076416286 *, const MethodInfo*))UnityEvent_2_AddListener_m3895966050_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::RemoveListener(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void UnityEvent_2_RemoveListener_m3484443447_gshared (UnityEvent_2_t3775219180 * __this, UnityAction_2_t2076416286 * ___call0, const MethodInfo* method);
#define UnityEvent_2_RemoveListener_m3484443447(__this, ___call0, method) ((  void (*) (UnityEvent_2_t3775219180 *, UnityAction_2_t2076416286 *, const MethodInfo*))UnityEvent_2_RemoveListener_m3484443447_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3911325978_gshared (UnityEvent_2_t3775219180 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_2_FindMethod_Impl_m3911325978(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_2_t3775219180 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_2_FindMethod_Impl_m3911325978_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1733537956 * UnityEvent_2_GetDelegate_m3055054414_gshared (UnityEvent_2_t3775219180 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m3055054414(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t1733537956 * (*) (UnityEvent_2_t3775219180 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_2_GetDelegate_m3055054414_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  BaseInvokableCall_t1733537956 * UnityEvent_2_GetDelegate_m4028737589_gshared (Il2CppObject * __this /* static, unused */, UnityAction_2_t2076416286 * ___action0, const MethodInfo* method);
#define UnityEvent_2_GetDelegate_m4028737589(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t1733537956 * (*) (Il2CppObject * /* static, unused */, UnityAction_2_t2076416286 *, const MethodInfo*))UnityEvent_2_GetDelegate_m4028737589_gshared)(__this /* static, unused */, ___action0, method)
