﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardForUserCharactersRequestCallback
struct GetLeaderboardForUserCharactersRequestCallback_t2495424733;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest
struct GetLeaderboardForUsersCharactersRequest_t3978815279;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo3978815279.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardForUserCharactersRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardForUserCharactersRequestCallback__ctor_m1530391372 (GetLeaderboardForUserCharactersRequestCallback_t2495424733 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardForUserCharactersRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest,System.Object)
extern "C"  void GetLeaderboardForUserCharactersRequestCallback_Invoke_m196199864 (GetLeaderboardForUserCharactersRequestCallback_t2495424733 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardForUsersCharactersRequest_t3978815279 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardForUserCharactersRequestCallback_t2495424733(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardForUsersCharactersRequest_t3978815279 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardForUserCharactersRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardForUserCharactersRequestCallback_BeginInvoke_m3603365133 (GetLeaderboardForUserCharactersRequestCallback_t2495424733 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardForUsersCharactersRequest_t3978815279 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardForUserCharactersRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardForUserCharactersRequestCallback_EndInvoke_m3279741788 (GetLeaderboardForUserCharactersRequestCallback_t2495424733 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
