﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PlayFabNotificationPackage_t2037342920;
struct PlayFabNotificationPackage_t2037342920_marshaled_pinvoke;

extern "C" void PlayFabNotificationPackage_t2037342920_marshal_pinvoke(const PlayFabNotificationPackage_t2037342920& unmarshaled, PlayFabNotificationPackage_t2037342920_marshaled_pinvoke& marshaled);
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_pinvoke_back(const PlayFabNotificationPackage_t2037342920_marshaled_pinvoke& marshaled, PlayFabNotificationPackage_t2037342920& unmarshaled);
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_pinvoke_cleanup(PlayFabNotificationPackage_t2037342920_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PlayFabNotificationPackage_t2037342920;
struct PlayFabNotificationPackage_t2037342920_marshaled_com;

extern "C" void PlayFabNotificationPackage_t2037342920_marshal_com(const PlayFabNotificationPackage_t2037342920& unmarshaled, PlayFabNotificationPackage_t2037342920_marshaled_com& marshaled);
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_com_back(const PlayFabNotificationPackage_t2037342920_marshaled_com& marshaled, PlayFabNotificationPackage_t2037342920& unmarshaled);
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_com_cleanup(PlayFabNotificationPackage_t2037342920_marshaled_com& marshaled);
