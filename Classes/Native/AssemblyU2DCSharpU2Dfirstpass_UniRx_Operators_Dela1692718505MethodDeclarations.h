﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey60__ctor_m1480217085_gshared (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey60__ctor_m1480217085(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey60__ctor_m1480217085_gshared)(__this, method)
// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey60<System.Object>::<>m__7A()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey60_U3CU3Em__7A_m3162678768_gshared (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey60_U3CU3Em__7A_m3162678768(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey60_t1692718505 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey60_U3CU3Em__7A_m3162678768_gshared)(__this, method)
