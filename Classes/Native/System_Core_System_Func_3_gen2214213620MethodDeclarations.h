﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen3689651873MethodDeclarations.h"

// System.Void System.Func`3<System.Boolean,System.Boolean,<>__AnonType2`2<System.Boolean,System.Boolean>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m1844410525(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t2214213620 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m4282269245_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Boolean,System.Boolean,<>__AnonType2`2<System.Boolean,System.Boolean>>::Invoke(T1,T2)
#define Func_3_Invoke_m1840436128(__this, ___arg10, ___arg21, method) ((  U3CU3E__AnonType2_2_t3656635463 * (*) (Func_3_t2214213620 *, bool, bool, const MethodInfo*))Func_3_Invoke_m3636733548_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Boolean,System.Boolean,<>__AnonType2`2<System.Boolean,System.Boolean>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m2924389925(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t2214213620 *, bool, bool, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3842807277_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Boolean,System.Boolean,<>__AnonType2`2<System.Boolean,System.Boolean>>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m3182982971(__this, ___result0, method) ((  U3CU3E__AnonType2_2_t3656635463 * (*) (Func_3_t2214213620 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m360027631_gshared)(__this, ___result0, method)
