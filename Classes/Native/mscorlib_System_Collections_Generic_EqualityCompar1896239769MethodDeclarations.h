﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Unit>
struct DefaultComparer_t1896239769;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Unit>::.ctor()
extern "C"  void DefaultComparer__ctor_m3046151048_gshared (DefaultComparer_t1896239769 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3046151048(__this, method) ((  void (*) (DefaultComparer_t1896239769 *, const MethodInfo*))DefaultComparer__ctor_m3046151048_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Unit>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1413444971_gshared (DefaultComparer_t1896239769 * __this, Unit_t2558286038  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1413444971(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1896239769 *, Unit_t2558286038 , const MethodInfo*))DefaultComparer_GetHashCode_m1413444971_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Unit>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3579307293_gshared (DefaultComparer_t1896239769 * __this, Unit_t2558286038  ___x0, Unit_t2558286038  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3579307293(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1896239769 *, Unit_t2558286038 , Unit_t2558286038 , const MethodInfo*))DefaultComparer_Equals_m3579307293_gshared)(__this, ___x0, ___y1, method)
