﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.Studio
struct Studio_t2092322584;
// System.String
struct String_t;
// System.Collections.Generic.List`1<GameAnalyticsSDK.Game>
struct List_1_t404560429;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Collections.Generic.List`1<GameAnalyticsSDK.Studio>
struct List_1_t2889281553;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void GameAnalyticsSDK.Studio::.ctor(System.String,System.String,System.Collections.Generic.List`1<GameAnalyticsSDK.Game>)
extern "C"  void Studio__ctor_m2035425619 (Studio_t2092322584 * __this, String_t* ___name0, String_t* ___id1, List_1_t404560429 * ___games2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.Studio::get_Name()
extern "C"  String_t* Studio_get_Name_m373546024 (Studio_t2092322584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Studio::set_Name(System.String)
extern "C"  void Studio_set_Name_m1706306243 (Studio_t2092322584 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.Studio::get_ID()
extern "C"  String_t* Studio_get_ID_m3553278360 (Studio_t2092322584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Studio::set_ID(System.String)
extern "C"  void Studio_set_ID_m3755166099 (Studio_t2092322584 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GameAnalyticsSDK.Game> GameAnalyticsSDK.Studio::get_Games()
extern "C"  List_1_t404560429 * Studio_get_Games_m651035198 (Studio_t2092322584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Studio::set_Games(System.Collections.Generic.List`1<GameAnalyticsSDK.Game>)
extern "C"  void Studio_set_Games_m3237760013 (Studio_t2092322584 * __this, List_1_t404560429 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] GameAnalyticsSDK.Studio::GetStudioNames(System.Collections.Generic.List`1<GameAnalyticsSDK.Studio>,System.Boolean)
extern "C"  StringU5BU5D_t2956870243* Studio_GetStudioNames_m1072296521 (Il2CppObject * __this /* static, unused */, List_1_t2889281553 * ___studios0, bool ___addFirstEmpty1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] GameAnalyticsSDK.Studio::GetGameNames(System.Int32,System.Collections.Generic.List`1<GameAnalyticsSDK.Studio>)
extern "C"  StringU5BU5D_t2956870243* Studio_GetGameNames_m3834371137 (Il2CppObject * __this /* static, unused */, int32_t ___index0, List_1_t2889281553 * ___studios1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
