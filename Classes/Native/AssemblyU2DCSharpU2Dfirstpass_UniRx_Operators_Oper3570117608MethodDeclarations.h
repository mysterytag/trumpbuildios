﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<System.Boolean>
struct OperatorObservableBase_1_t3570117608;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<System.Boolean>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m286364973_gshared (OperatorObservableBase_1_t3570117608 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m286364973(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t3570117608 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m286364973_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Boolean>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m219767309_gshared (OperatorObservableBase_1_t3570117608 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m219767309(__this, method) ((  bool (*) (OperatorObservableBase_1_t3570117608 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m219767309_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m19433973_gshared (OperatorObservableBase_1_t3570117608 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m19433973(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t3570117608 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m19433973_gshared)(__this, ___observer0, method)
