﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Single>
struct U3COnChangedU3Ec__AnonStorey142_t3792630170;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Single>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m66222260_gshared (U3COnChangedU3Ec__AnonStorey142_t3792630170 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142__ctor_m66222260(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3792630170 *, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142__ctor_m66222260_gshared)(__this, method)
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Single>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m3450690676_gshared (U3COnChangedU3Ec__AnonStorey142_t3792630170 * __this, float ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m3450690676(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3792630170 *, float, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m3450690676_gshared)(__this, ____0, method)
