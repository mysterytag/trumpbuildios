﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetUserData>c__AnonStorey92
struct U3CGetUserDataU3Ec__AnonStorey92_t1824061073;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetUserData>c__AnonStorey92::.ctor()
extern "C"  void U3CGetUserDataU3Ec__AnonStorey92__ctor_m1114792930 (U3CGetUserDataU3Ec__AnonStorey92_t1824061073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetUserData>c__AnonStorey92::<>m__7F(PlayFab.CallRequestContainer)
extern "C"  void U3CGetUserDataU3Ec__AnonStorey92_U3CU3Em__7F_m2032367087 (U3CGetUserDataU3Ec__AnonStorey92_t1824061073 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
