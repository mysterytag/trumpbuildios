﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType3`2<System.Object,System.Object>
struct U3CU3E__AnonType3_2_t1586863982;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType3`2<System.Object,System.Object>::.ctor(<b>__T,<i>__T)
extern "C"  void U3CU3E__AnonType3_2__ctor_m3532991313_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, Il2CppObject * ___b0, Il2CppObject * ___i1, const MethodInfo* method);
#define U3CU3E__AnonType3_2__ctor_m3532991313(__this, ___b0, ___i1, method) ((  void (*) (U3CU3E__AnonType3_2_t1586863982 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType3_2__ctor_m3532991313_gshared)(__this, ___b0, ___i1, method)
// <b>__T <>__AnonType3`2<System.Object,System.Object>::get_b()
extern "C"  Il2CppObject * U3CU3E__AnonType3_2_get_b_m2717857092_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, const MethodInfo* method);
#define U3CU3E__AnonType3_2_get_b_m2717857092(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType3_2_t1586863982 *, const MethodInfo*))U3CU3E__AnonType3_2_get_b_m2717857092_gshared)(__this, method)
// <i>__T <>__AnonType3`2<System.Object,System.Object>::get_i()
extern "C"  Il2CppObject * U3CU3E__AnonType3_2_get_i_m385345746_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, const MethodInfo* method);
#define U3CU3E__AnonType3_2_get_i_m385345746(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType3_2_t1586863982 *, const MethodInfo*))U3CU3E__AnonType3_2_get_i_m385345746_gshared)(__this, method)
// System.Boolean <>__AnonType3`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType3_2_Equals_m1930117515_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType3_2_Equals_m1930117515(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType3_2_t1586863982 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType3_2_Equals_m1930117515_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType3`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType3_2_GetHashCode_m2583466671_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, const MethodInfo* method);
#define U3CU3E__AnonType3_2_GetHashCode_m2583466671(__this, method) ((  int32_t (*) (U3CU3E__AnonType3_2_t1586863982 *, const MethodInfo*))U3CU3E__AnonType3_2_GetHashCode_m2583466671_gshared)(__this, method)
// System.String <>__AnonType3`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType3_2_ToString_m398369349_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, const MethodInfo* method);
#define U3CU3E__AnonType3_2_ToString_m398369349(__this, method) ((  String_t* (*) (U3CU3E__AnonType3_2_t1586863982 *, const MethodInfo*))U3CU3E__AnonType3_2_ToString_m398369349_gshared)(__this, method)
