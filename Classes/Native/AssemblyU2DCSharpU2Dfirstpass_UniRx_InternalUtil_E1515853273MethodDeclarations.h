﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct EmptyObserver_1_t1515853273;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2926995122_gshared (EmptyObserver_1_t1515853273 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2926995122(__this, method) ((  void (*) (EmptyObserver_1_t1515853273 *, const MethodInfo*))EmptyObserver_1__ctor_m2926995122_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m60439355_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m60439355(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m60439355_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1922190524_gshared (EmptyObserver_1_t1515853273 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1922190524(__this, method) ((  void (*) (EmptyObserver_1_t1515853273 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1922190524_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1382269929_gshared (EmptyObserver_1_t1515853273 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1382269929(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1515853273 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1382269929_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2469436634_gshared (EmptyObserver_1_t1515853273 * __this, Tuple_2_t2106500283  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2469436634(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1515853273 *, Tuple_2_t2106500283 , const MethodInfo*))EmptyObserver_1_OnNext_m2469436634_gshared)(__this, ___value0, method)
