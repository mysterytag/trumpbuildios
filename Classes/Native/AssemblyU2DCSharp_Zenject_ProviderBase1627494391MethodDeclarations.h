﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// Zenject.BindingCondition
struct BindingCondition_t1528286123;
// System.String
struct String_t;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_BindingCondition1528286123.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.ProviderBase::.ctor()
extern "C"  void ProviderBase__ctor_m796348314 (ProviderBase_t1627494391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingCondition Zenject.ProviderBase::get_Condition()
extern "C"  BindingCondition_t1528286123 * ProviderBase_get_Condition_m313987234 (ProviderBase_t1627494391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ProviderBase::set_Condition(Zenject.BindingCondition)
extern "C"  void ProviderBase_set_Condition_m3652857601 (ProviderBase_t1627494391 * __this, BindingCondition_t1528286123 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Zenject.ProviderBase::get_Identifier()
extern "C"  String_t* ProviderBase_get_Identifier_m1537034175 (ProviderBase_t1627494391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ProviderBase::set_Identifier(System.String)
extern "C"  void ProviderBase_set_Identifier_m3023485618 (ProviderBase_t1627494391 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.ProviderBase::Matches(Zenject.InjectContext)
extern "C"  bool ProviderBase_Matches_m1809778620 (ProviderBase_t1627494391 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ProviderBase::Dispose()
extern "C"  void ProviderBase_Dispose_m684834135 (ProviderBase_t1627494391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
