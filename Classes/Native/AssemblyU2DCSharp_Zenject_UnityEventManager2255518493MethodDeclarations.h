﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.UnityEventManager
struct UnityEventManager_t2255518493;
// System.Action
struct Action_t437523947;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Action`1<ModestTree.Util.MouseWheelScrollDirections>
struct Action_1_t3075561458;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_ModestTree_Util_MouseWheelScroll2927108753.h"

// System.Void Zenject.UnityEventManager::.ctor()
extern "C"  void UnityEventManager__ctor_m1400173602 (UnityEventManager_t2255518493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_ApplicationGainedFocus(System.Action)
extern "C"  void UnityEventManager_add_ApplicationGainedFocus_m4054089875 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_ApplicationGainedFocus(System.Action)
extern "C"  void UnityEventManager_remove_ApplicationGainedFocus_m1059060888 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_ApplicationLostFocus(System.Action)
extern "C"  void UnityEventManager_add_ApplicationLostFocus_m1839379609 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_ApplicationLostFocus(System.Action)
extern "C"  void UnityEventManager_remove_ApplicationLostFocus_m2573692382 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_ApplicationFocusChanged(System.Action`1<System.Boolean>)
extern "C"  void UnityEventManager_add_ApplicationFocusChanged_m3839316197 (UnityEventManager_t2255518493 * __this, Action_1_t359458046 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_ApplicationFocusChanged(System.Action`1<System.Boolean>)
extern "C"  void UnityEventManager_remove_ApplicationFocusChanged_m1635484864 (UnityEventManager_t2255518493 * __this, Action_1_t359458046 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_ApplicationQuit(System.Action)
extern "C"  void UnityEventManager_add_ApplicationQuit_m1041428540 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_ApplicationQuit(System.Action)
extern "C"  void UnityEventManager_remove_ApplicationQuit_m4158046167 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_ChangingScenes(System.Action)
extern "C"  void UnityEventManager_add_ChangingScenes_m1889670089 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_ChangingScenes(System.Action)
extern "C"  void UnityEventManager_remove_ChangingScenes_m2544395470 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_DrawGizmos(System.Action)
extern "C"  void UnityEventManager_add_DrawGizmos_m3026080448 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_DrawGizmos(System.Action)
extern "C"  void UnityEventManager_remove_DrawGizmos_m4032294469 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_MouseButtonDown(System.Action`1<System.Int32>)
extern "C"  void UnityEventManager_add_MouseButtonDown_m1820189278 (UnityEventManager_t2255518493 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_MouseButtonDown(System.Action`1<System.Int32>)
extern "C"  void UnityEventManager_remove_MouseButtonDown_m1906121721 (UnityEventManager_t2255518493 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_MouseButtonUp(System.Action`1<System.Int32>)
extern "C"  void UnityEventManager_add_MouseButtonUp_m2565593925 (UnityEventManager_t2255518493 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_MouseButtonUp(System.Action`1<System.Int32>)
extern "C"  void UnityEventManager_remove_MouseButtonUp_m1412612000 (UnityEventManager_t2255518493 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_LeftMouseButtonDown(System.Action)
extern "C"  void UnityEventManager_add_LeftMouseButtonDown_m3879338409 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_LeftMouseButtonDown(System.Action)
extern "C"  void UnityEventManager_remove_LeftMouseButtonDown_m1963363268 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_LeftMouseButtonUp(System.Action)
extern "C"  void UnityEventManager_add_LeftMouseButtonUp_m188946000 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_LeftMouseButtonUp(System.Action)
extern "C"  void UnityEventManager_remove_LeftMouseButtonUp_m1666280235 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_MiddleMouseButtonDown(System.Action)
extern "C"  void UnityEventManager_add_MiddleMouseButtonDown_m1624144631 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_MiddleMouseButtonDown(System.Action)
extern "C"  void UnityEventManager_remove_MiddleMouseButtonDown_m2913004114 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_MiddleMouseButtonUp(System.Action)
extern "C"  void UnityEventManager_add_MiddleMouseButtonUp_m3802237726 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_MiddleMouseButtonUp(System.Action)
extern "C"  void UnityEventManager_remove_MiddleMouseButtonUp_m1886262585 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_RightMouseButtonDown(System.Action)
extern "C"  void UnityEventManager_add_RightMouseButtonDown_m2991122528 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_RightMouseButtonDown(System.Action)
extern "C"  void UnityEventManager_remove_RightMouseButtonDown_m3725435301 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_RightMouseButtonUp(System.Action)
extern "C"  void UnityEventManager_add_RightMouseButtonUp_m125451975 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_RightMouseButtonUp(System.Action)
extern "C"  void UnityEventManager_remove_RightMouseButtonUp_m2973140300 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_MouseWheelMoved(System.Action`1<ModestTree.Util.MouseWheelScrollDirections>)
extern "C"  void UnityEventManager_add_MouseWheelMoved_m3459489442 (UnityEventManager_t2255518493 * __this, Action_1_t3075561458 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_MouseWheelMoved(System.Action`1<ModestTree.Util.MouseWheelScrollDirections>)
extern "C"  void UnityEventManager_remove_MouseWheelMoved_m3499775741 (UnityEventManager_t2255518493 * __this, Action_1_t3075561458 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_MouseMove(System.Action)
extern "C"  void UnityEventManager_add_MouseMove_m3796333637 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_MouseMove(System.Action)
extern "C"  void UnityEventManager_remove_MouseMove_m1889129504 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_ScreenSizeChanged(System.Action)
extern "C"  void UnityEventManager_add_ScreenSizeChanged_m896546772 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_ScreenSizeChanged(System.Action)
extern "C"  void UnityEventManager_remove_ScreenSizeChanged_m2373881007 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::add_Started(System.Action)
extern "C"  void UnityEventManager_add_Started_m1641477690 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::remove_Started(System.Action)
extern "C"  void UnityEventManager_remove_Started_m2573570261 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.UnityEventManager::get_IsFocused()
extern "C"  bool UnityEventManager_get_IsFocused_m213405752 (UnityEventManager_t2255518493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::set_IsFocused(System.Boolean)
extern "C"  void UnityEventManager_set_IsFocused_m3303716615 (UnityEventManager_t2255518493 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::Start()
extern "C"  void UnityEventManager_Start_m347311394 (UnityEventManager_t2255518493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::Tick()
extern "C"  void UnityEventManager_Tick_m3216313151 (UnityEventManager_t2255518493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::OnDestroy()
extern "C"  void UnityEventManager_OnDestroy_m2039212059 (UnityEventManager_t2255518493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::OnApplicationQuit()
extern "C"  void UnityEventManager_OnApplicationQuit_m1108754080 (UnityEventManager_t2255518493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::OnDrawGizmos()
extern "C"  void UnityEventManager_OnDrawGizmos_m4106392254 (UnityEventManager_t2255518493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::OnApplicationFocus(System.Boolean)
extern "C"  void UnityEventManager_OnApplicationFocus_m4278425728 (UnityEventManager_t2255518493 * __this, bool ___newIsFocused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<ApplicationGainedFocus>m__2F1()
extern "C"  void UnityEventManager_U3CApplicationGainedFocusU3Em__2F1_m2575721386 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<ApplicationLostFocus>m__2F2()
extern "C"  void UnityEventManager_U3CApplicationLostFocusU3Em__2F2_m1691150577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<ApplicationFocusChanged>m__2F3(System.Boolean)
extern "C"  void UnityEventManager_U3CApplicationFocusChangedU3Em__2F3_m3170362807 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<ApplicationQuit>m__2F4()
extern "C"  void UnityEventManager_U3CApplicationQuitU3Em__2F4_m2326336718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<ChangingScenes>m__2F5()
extern "C"  void UnityEventManager_U3CChangingScenesU3Em__2F5_m2194074212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<DrawGizmos>m__2F6()
extern "C"  void UnityEventManager_U3CDrawGizmosU3Em__2F6_m2238731356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<MouseButtonDown>m__2F7(System.Int32)
extern "C"  void UnityEventManager_U3CMouseButtonDownU3Em__2F7_m2930362984 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<MouseButtonUp>m__2F8(System.Int32)
extern "C"  void UnityEventManager_U3CMouseButtonUpU3Em__2F8_m4087750992 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<LeftMouseButtonDown>m__2F9()
extern "C"  void UnityEventManager_U3CLeftMouseButtonDownU3Em__2F9_m148156608 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<LeftMouseButtonUp>m__2FA()
extern "C"  void UnityEventManager_U3CLeftMouseButtonUpU3Em__2FA_m2430237615 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<MiddleMouseButtonDown>m__2FB()
extern "C"  void UnityEventManager_U3CMiddleMouseButtonDownU3Em__2FB_m2259636055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<MiddleMouseButtonUp>m__2FC()
extern "C"  void UnityEventManager_U3CMiddleMouseButtonUpU3Em__2FC_m3844725631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<RightMouseButtonDown>m__2FD()
extern "C"  void UnityEventManager_U3CRightMouseButtonDownU3Em__2FD_m1610318218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<RightMouseButtonUp>m__2FE()
extern "C"  void UnityEventManager_U3CRightMouseButtonUpU3Em__2FE_m1546847730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<MouseWheelMoved>m__2FF(ModestTree.Util.MouseWheelScrollDirections)
extern "C"  void UnityEventManager_U3CMouseWheelMovedU3Em__2FF_m1664420075 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<MouseMove>m__300()
extern "C"  void UnityEventManager_U3CMouseMoveU3Em__300_m3730852074 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<ScreenSizeChanged>m__301()
extern "C"  void UnityEventManager_U3CScreenSizeChangedU3Em__301_m2782129018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityEventManager::<Started>m__302()
extern "C"  void UnityEventManager_U3CStartedU3Em__302_m1311423777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
