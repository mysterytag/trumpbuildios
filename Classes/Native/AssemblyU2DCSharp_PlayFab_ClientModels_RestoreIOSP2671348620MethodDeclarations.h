﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RestoreIOSPurchasesRequest
struct RestoreIOSPurchasesRequest_t2671348620;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.RestoreIOSPurchasesRequest::.ctor()
extern "C"  void RestoreIOSPurchasesRequest__ctor_m807802289 (RestoreIOSPurchasesRequest_t2671348620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RestoreIOSPurchasesRequest::get_ReceiptData()
extern "C"  String_t* RestoreIOSPurchasesRequest_get_ReceiptData_m1723366155 (RestoreIOSPurchasesRequest_t2671348620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RestoreIOSPurchasesRequest::set_ReceiptData(System.String)
extern "C"  void RestoreIOSPurchasesRequest_set_ReceiptData_m3310536142 (RestoreIOSPurchasesRequest_t2671348620 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
