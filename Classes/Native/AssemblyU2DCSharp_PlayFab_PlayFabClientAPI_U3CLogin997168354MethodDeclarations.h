﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithCustomID>c__AnonStorey60
struct U3CLoginWithCustomIDU3Ec__AnonStorey60_t997168354;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithCustomID>c__AnonStorey60::.ctor()
extern "C"  void U3CLoginWithCustomIDU3Ec__AnonStorey60__ctor_m2346972337 (U3CLoginWithCustomIDU3Ec__AnonStorey60_t997168354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithCustomID>c__AnonStorey60::<>m__4D(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithCustomIDU3Ec__AnonStorey60_U3CU3Em__4D_m2102436447 (U3CLoginWithCustomIDU3Ec__AnonStorey60_t997168354 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
