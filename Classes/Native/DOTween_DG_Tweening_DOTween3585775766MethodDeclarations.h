﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.IDOTweenInit
struct IDOTweenInit_t3749595613;
// DG.Tweening.Core.DOTweenSettings
struct DOTweenSettings_t2040152940;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2841494226;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t3947337298;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t689009830;
// DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1963399521;
// DG.Tweening.Core.DOGetter`1<System.Double>
struct DOGetter_1_t3523644891;
// DG.Tweening.Core.DOSetter`1<System.Double>
struct DOSetter_1_t265317423;
// DG.Tweening.Tweener
struct Tweener_t1766303790;
// DG.Tweening.Core.DOGetter`1<System.Int32>
struct DOGetter_1_t1541575768;
// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t2578215596;
// DG.Tweening.Core.DOGetter`1<System.UInt32>
struct DOGetter_1_t3975053603;
// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct DOSetter_1_t716726135;
// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t1541575863;
// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t2578215691;
// DG.Tweening.Core.DOGetter`1<System.UInt64>
struct DOGetter_1_t3975053698;
// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct DOSetter_1_t716726230;
// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t1100577297;
// DG.Tweening.Core.DOGetter`1<System.String>
struct DOGetter_1_t3957617179;
// DG.Tweening.Core.DOSetter`1<System.String>
struct DOSetter_1_t699289711;
// System.String
struct String_t;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t753146263;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t2219490769;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t3256130597;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t2905908171;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t2219490770;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t3256130598;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t763702783;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct DOGetter_1_t2219490771;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t3256130599;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct TweenerCore_3_t3741400174;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t585876960;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1622516788;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t4289140679;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t282336741;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t1318976569;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t2274666266;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t219589798;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1256229626;
// DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>
struct DOGetter_1_t2088331865;
// DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>
struct DOSetter_1_t3124971693;
// UnityEngine.RectOffset
struct RectOffset_t3394170884;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t1715509190;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t3227571696;
// System.Single[]
struct SingleU5BU5D_t1219431280;
// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t182370955;
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct DOGetter_1_t1740296090;
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct DOSetter_1_t2776935918;
// DG.Tweening.Sequence
struct Sequence_t2436335575;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t1900323642;
// DG.Tweening.DOTween
struct DOTween_t3585775766;

#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_LogBehaviour3180954095.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"
#include "mscorlib_System_Nullable_1_gen1772024707.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings2040152940.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "UnityEngine_UnityEngine_RectOffset3394170884.h"
#include "DOTween_DG_Tweening_AxisConstraint3652844660.h"
#include "DOTween_DG_Tweening_Color23046135109.h"
#include "mscorlib_System_Object837106420.h"

// DG.Tweening.LogBehaviour DG.Tweening.DOTween::get_logBehaviour()
extern "C"  int32_t DOTween_get_logBehaviour_m1425304677 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::set_logBehaviour(DG.Tweening.LogBehaviour)
extern "C"  void DOTween_set_logBehaviour_m2740089712 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::.cctor()
extern "C"  void DOTween__cctor_m2298985512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern "C"  Il2CppObject * DOTween_Init_m3949764519 (Il2CppObject * __this /* static, unused */, Nullable_1_t3097043249  ___recycleAllByDefault0, Nullable_1_t3097043249  ___useSafeMode1, Nullable_1_t1772024707  ___logBehaviour2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::AutoInit()
extern "C"  void DOTween_AutoInit_m2429517086 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(DG.Tweening.Core.DOTweenSettings,System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern "C"  Il2CppObject * DOTween_Init_m2066611683 (Il2CppObject * __this /* static, unused */, DOTweenSettings_t2040152940 * ___settings0, Nullable_1_t3097043249  ___recycleAllByDefault1, Nullable_1_t3097043249  ___useSafeMode2, Nullable_1_t1772024707  ___logBehaviour3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::SetTweensCapacity(System.Int32,System.Int32)
extern "C"  void DOTween_SetTweensCapacity_m232240767 (Il2CppObject * __this /* static, unused */, int32_t ___tweenersCapacity0, int32_t ___sequencesCapacity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::Clear(System.Boolean)
extern "C"  void DOTween_Clear_m3831445959 (Il2CppObject * __this /* static, unused */, bool ___destroy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::ClearCachedTweens()
extern "C"  void DOTween_ClearCachedTweens_m1213222554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Validate()
extern "C"  int32_t DOTween_Validate_m128047203 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single)
extern "C"  TweenerCore_3_t2841494226 * DOTween_To_m123324536 (Il2CppObject * __this /* static, unused */, DOGetter_1_t3947337298 * ___getter0, DOSetter_1_t689009830 * ___setter1, float ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Double>,DG.Tweening.Core.DOSetter`1<System.Double>,System.Double,System.Single)
extern "C"  TweenerCore_3_t1963399521 * DOTween_To_m2007772988 (Il2CppObject * __this /* static, unused */, DOGetter_1_t3523644891 * ___getter0, DOSetter_1_t265317423 * ___setter1, double ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Int32>,DG.Tweening.Core.DOSetter`1<System.Int32>,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * DOTween_To_m1551042591 (Il2CppObject * __this /* static, unused */, DOGetter_1_t1541575768 * ___getter0, DOSetter_1_t2578215596 * ___setter1, int32_t ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.UInt32>,DG.Tweening.Core.DOSetter`1<System.UInt32>,System.UInt32,System.Single)
extern "C"  Tweener_t1766303790 * DOTween_To_m3201617760 (Il2CppObject * __this /* static, unused */, DOGetter_1_t3975053603 * ___getter0, DOSetter_1_t716726135 * ___setter1, uint32_t ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Int64>,DG.Tweening.Core.DOSetter`1<System.Int64>,System.Int64,System.Single)
extern "C"  Tweener_t1766303790 * DOTween_To_m2875272354 (Il2CppObject * __this /* static, unused */, DOGetter_1_t1541575863 * ___getter0, DOSetter_1_t2578215691 * ___setter1, int64_t ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.UInt64>,DG.Tweening.Core.DOSetter`1<System.UInt64>,System.UInt64,System.Single)
extern "C"  Tweener_t1766303790 * DOTween_To_m105179073 (Il2CppObject * __this /* static, unused */, DOGetter_1_t3975053698 * ___getter0, DOSetter_1_t716726230 * ___setter1, uint64_t ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.String>,DG.Tweening.Core.DOSetter`1<System.String>,System.String,System.Single)
extern "C"  TweenerCore_3_t1100577297 * DOTween_To_m3973076524 (Il2CppObject * __this /* static, unused */, DOGetter_1_t3957617179 * ___getter0, DOSetter_1_t699289711 * ___setter1, String_t* ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,UnityEngine.Vector2,System.Single)
extern "C"  TweenerCore_3_t753146263 * DOTween_To_m1242219842 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490769 * ___getter0, DOSetter_1_t3256130597 * ___setter1, Vector2_t3525329788  ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern "C"  TweenerCore_3_t2905908171 * DOTween_To_m1633868995 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490770 * ___getter0, DOSetter_1_t3256130598 * ___setter1, Vector3_t3525329789  ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>,UnityEngine.Vector4,System.Single)
extern "C"  TweenerCore_3_t763702783 * DOTween_To_m2025518148 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490771 * ___getter0, DOSetter_1_t3256130599 * ___setter1, Vector4_t3525329790  ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,UnityEngine.Vector3,System.Single)
extern "C"  TweenerCore_3_t3741400174 * DOTween_To_m1607864926 (Il2CppObject * __this /* static, unused */, DOGetter_1_t585876960 * ___getter0, DOSetter_1_t1622516788 * ___setter1, Vector3_t3525329789  ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,UnityEngine.Color,System.Single)
extern "C"  TweenerCore_3_t4289140679 * DOTween_To_m3709064802 (Il2CppObject * __this /* static, unused */, DOGetter_1_t282336741 * ___getter0, DOSetter_1_t1318976569 * ___setter1, Color_t1588175760  ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>,DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>,UnityEngine.Rect,System.Single)
extern "C"  TweenerCore_3_t2274666266 * DOTween_To_m12376626 (Il2CppObject * __this /* static, unused */, DOGetter_1_t219589798 * ___getter0, DOSetter_1_t1256229626 * ___setter1, Rect_t1525428817  ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>,DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>,UnityEngine.RectOffset,System.Single)
extern "C"  Tweener_t1766303790 * DOTween_To_m2905174466 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2088331865 * ___getter0, DOSetter_1_t3124971693 * ___setter1, RectOffset_t3394170884 * ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::ToAxis(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,System.Single,DG.Tweening.AxisConstraint)
extern "C"  TweenerCore_3_t2905908171 * DOTween_ToAxis_m2989248488 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490770 * ___getter0, DOSetter_1_t3256130598 * ___setter1, float ___endValue2, float ___duration3, int32_t ___axisConstraint4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::ToAlpha(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * DOTween_ToAlpha_m3396472119 (Il2CppObject * __this /* static, unused */, DOGetter_1_t282336741 * ___getter0, DOSetter_1_t1318976569 * ___setter1, float ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * DOTween_To_m4062365325 (Il2CppObject * __this /* static, unused */, DOSetter_1_t689009830 * ___setter0, float ___startValue1, float ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Punch(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  TweenerCore_3_t1715509190 * DOTween_Punch_m4193422018 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490770 * ___getter0, DOSetter_1_t3256130598 * ___setter1, Vector3_t3525329789  ___direction2, float ___duration3, int32_t ___vibrato4, float ___elasticity5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Shake(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  TweenerCore_3_t1715509190 * DOTween_Shake_m2713530997 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490770 * ___getter0, DOSetter_1_t3256130598 * ___setter1, float ___duration2, float ___strength3, int32_t ___vibrato4, float ___randomness5, bool ___ignoreZAxis6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Shake(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,System.Int32,System.Single)
extern "C"  TweenerCore_3_t1715509190 * DOTween_Shake_m1763772378 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490770 * ___getter0, DOSetter_1_t3256130598 * ___setter1, float ___duration2, Vector3_t3525329789  ___strength3, int32_t ___vibrato4, float ___randomness5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Shake(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean,System.Boolean)
extern "C"  TweenerCore_3_t1715509190 * DOTween_Shake_m742278330 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490770 * ___getter0, DOSetter_1_t3256130598 * ___setter1, float ___duration2, Vector3_t3525329789  ___strength3, int32_t ___vibrato4, float ___randomness5, bool ___ignoreZAxis6, bool ___vectorBased7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::ToArray(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3[],System.Single[])
extern "C"  TweenerCore_3_t1715509190 * DOTween_ToArray_m319454150 (Il2CppObject * __this /* static, unused */, DOGetter_1_t2219490770 * ___getter0, DOSetter_1_t3256130598 * ___setter1, Vector3U5BU5D_t3227571696* ___endValues2, SingleU5BU5D_t1219431280* ___durations3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>,DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>,DG.Tweening.Color2,System.Single)
extern "C"  TweenerCore_3_t182370955 * DOTween_To_m1205627237 (Il2CppObject * __this /* static, unused */, DOGetter_1_t1740296090 * ___getter0, DOSetter_1_t2776935918 * ___setter1, Color2_t3046135109  ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.DOTween::Sequence()
extern "C"  Sequence_t2436335575 * DOTween_Sequence_m2991851212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::CompleteAll(System.Boolean)
extern "C"  int32_t DOTween_CompleteAll_m188848148 (Il2CppObject * __this /* static, unused */, bool ___withCallbacks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Complete(System.Object,System.Boolean)
extern "C"  int32_t DOTween_Complete_m3051644463 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, bool ___withCallbacks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::CompleteAndReturnKilledTot()
extern "C"  int32_t DOTween_CompleteAndReturnKilledTot_m3642243291 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::CompleteAndReturnKilledTot(System.Object)
extern "C"  int32_t DOTween_CompleteAndReturnKilledTot_m1314718873 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::CompleteAndReturnKilledTotExceptFor(System.Object[])
extern "C"  int32_t DOTween_CompleteAndReturnKilledTotExceptFor_m1069105051 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___excludeTargetsOrIds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::FlipAll()
extern "C"  int32_t DOTween_FlipAll_m3066172585 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Flip(System.Object)
extern "C"  int32_t DOTween_Flip_m699366362 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::GotoAll(System.Single,System.Boolean)
extern "C"  int32_t DOTween_GotoAll_m4122626405 (Il2CppObject * __this /* static, unused */, float ___to0, bool ___andPlay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Goto(System.Object,System.Single,System.Boolean)
extern "C"  int32_t DOTween_Goto_m3716674644 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, float ___to1, bool ___andPlay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::KillAll(System.Boolean)
extern "C"  int32_t DOTween_KillAll_m2591638479 (Il2CppObject * __this /* static, unused */, bool ___complete0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::KillAll(System.Boolean,System.Object[])
extern "C"  int32_t DOTween_KillAll_m3225246779 (Il2CppObject * __this /* static, unused */, bool ___complete0, ObjectU5BU5D_t11523773* ___idsOrTargetsToExclude1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Kill(System.Object,System.Boolean)
extern "C"  int32_t DOTween_Kill_m2525472660 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, bool ___complete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PauseAll()
extern "C"  int32_t DOTween_PauseAll_m96499160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Pause(System.Object)
extern "C"  int32_t DOTween_Pause_m1357213257 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayAll()
extern "C"  int32_t DOTween_PlayAll_m3919064642 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Play(System.Object)
extern "C"  int32_t DOTween_Play_m2603166259 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Play(System.Object,System.Object)
extern "C"  int32_t DOTween_Play_m1665408961 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___target0, Il2CppObject * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayBackwardsAll()
extern "C"  int32_t DOTween_PlayBackwardsAll_m873125746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayBackwards(System.Object)
extern "C"  int32_t DOTween_PlayBackwards_m3825080163 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayForwardAll()
extern "C"  int32_t DOTween_PlayForwardAll_m800479933 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayForward(System.Object)
extern "C"  int32_t DOTween_PlayForward_m744634606 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::RestartAll(System.Boolean)
extern "C"  int32_t DOTween_RestartAll_m2539062678 (Il2CppObject * __this /* static, unused */, bool ___includeDelay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Restart(System.Object,System.Boolean)
extern "C"  int32_t DOTween_Restart_m3327054701 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, bool ___includeDelay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Restart(System.Object,System.Object,System.Boolean)
extern "C"  int32_t DOTween_Restart_m440741599 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___target0, Il2CppObject * ___id1, bool ___includeDelay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::RewindAll(System.Boolean)
extern "C"  int32_t DOTween_RewindAll_m2386900562 (Il2CppObject * __this /* static, unused */, bool ___includeDelay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Rewind(System.Object,System.Boolean)
extern "C"  int32_t DOTween_Rewind_m3289912625 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, bool ___includeDelay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::SmoothRewindAll()
extern "C"  int32_t DOTween_SmoothRewindAll_m4013665933 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::SmoothRewind(System.Object)
extern "C"  int32_t DOTween_SmoothRewind_m695011518 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::TogglePauseAll()
extern "C"  int32_t DOTween_TogglePauseAll_m3247172428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::TogglePause(System.Object)
extern "C"  int32_t DOTween_TogglePause_m138650301 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.DOTween::IsTweening(System.Object)
extern "C"  bool DOTween_IsTweening_m2740064140 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___targetOrId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::TotalPlayingTweens()
extern "C"  int32_t DOTween_TotalPlayingTweens_m1064517023 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTween::PlayingTweens()
extern "C"  List_1_t1900323642 * DOTween_PlayingTweens_m421034365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTween::PausedTweens()
extern "C"  List_1_t1900323642 * DOTween_PausedTweens_m2418741297 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTween::TweensById(System.Object,System.Boolean)
extern "C"  List_1_t1900323642 * DOTween_TweensById_m3756586174 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___id0, bool ___playingOnly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTween::TweensByTarget(System.Object,System.Boolean)
extern "C"  List_1_t1900323642 * DOTween_TweensByTarget_m2660631092 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___target0, bool ___playingOnly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::InitCheck()
extern "C"  void DOTween_InitCheck_m3805752347 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::.ctor()
extern "C"  void DOTween__ctor_m3691942949 (DOTween_t3585775766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
