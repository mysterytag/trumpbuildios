﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>
struct Func_3_t631773672;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m457791298_gshared (Func_3_t631773672 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_3__ctor_m457791298(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t631773672 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m457791298_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>::Invoke(T1,T2)
extern "C"  Vector2_t3525329788  Func_3_Invoke_m1150035143_gshared (Func_3_t631773672 * __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
#define Func_3_Invoke_m1150035143(__this, ___arg10, ___arg21, method) ((  Vector2_t3525329788  (*) (Func_3_t631773672 *, int64_t, int32_t, const MethodInfo*))Func_3_Invoke_m1150035143_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m1889779144_gshared (Func_3_t631773672 * __this, int64_t ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Func_3_BeginInvoke_m1889779144(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t631773672 *, int64_t, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m1889779144_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t3525329788  Func_3_EndInvoke_m1858386164_gshared (Func_3_t631773672 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_3_EndInvoke_m1858386164(__this, ___result0, method) ((  Vector2_t3525329788  (*) (Func_3_t631773672 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m1858386164_gshared)(__this, ___result0, method)
