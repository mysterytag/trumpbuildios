﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SingletonId
struct SingletonId_t1838183899;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_SingletonId1838183899.h"

// System.Void Zenject.SingletonId::.ctor(System.String,System.Type)
extern "C"  void SingletonId__ctor_m351994705 (SingletonId_t1838183899 * __this, String_t* ___identifier0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.SingletonId::GetHashCode()
extern "C"  int32_t SingletonId_GetHashCode_m3795081377 (SingletonId_t1838183899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SingletonId::Equals(System.Object)
extern "C"  bool SingletonId_Equals_m1396521225 (SingletonId_t1838183899 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SingletonId::Equals(Zenject.SingletonId)
extern "C"  bool SingletonId_Equals_m3944619168 (SingletonId_t1838183899 * __this, SingletonId_t1838183899 * ___that0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SingletonId::op_Equality(Zenject.SingletonId,Zenject.SingletonId)
extern "C"  bool SingletonId_op_Equality_m2083314178 (Il2CppObject * __this /* static, unused */, SingletonId_t1838183899 * ___left0, SingletonId_t1838183899 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SingletonId::op_Inequality(Zenject.SingletonId,Zenject.SingletonId)
extern "C"  bool SingletonId_op_Inequality_m3325898813 (Il2CppObject * __this /* static, unused */, SingletonId_t1838183899 * ___left0, SingletonId_t1838183899 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
