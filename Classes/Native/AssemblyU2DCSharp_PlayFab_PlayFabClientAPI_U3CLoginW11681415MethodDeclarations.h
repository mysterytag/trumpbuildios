﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithAndroidDeviceID>c__AnonStorey5F
struct U3CLoginWithAndroidDeviceIDU3Ec__AnonStorey5F_t11681415;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithAndroidDeviceID>c__AnonStorey5F::.ctor()
extern "C"  void U3CLoginWithAndroidDeviceIDU3Ec__AnonStorey5F__ctor_m304497932 (U3CLoginWithAndroidDeviceIDU3Ec__AnonStorey5F_t11681415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithAndroidDeviceID>c__AnonStorey5F::<>m__4C(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithAndroidDeviceIDU3Ec__AnonStorey5F_U3CU3Em__4C_m1014847801 (U3CLoginWithAndroidDeviceIDU3Ec__AnonStorey5F_t11681415 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
