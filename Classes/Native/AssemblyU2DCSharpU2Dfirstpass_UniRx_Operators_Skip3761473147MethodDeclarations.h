﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>
struct SkipWhile_t3761473147;
// UniRx.Operators.SkipWhileObservable`1<System.Object>
struct SkipWhileObservable_1_t2542074388;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::.ctor(UniRx.Operators.SkipWhileObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SkipWhile__ctor_m2188486226_gshared (SkipWhile_t3761473147 * __this, SkipWhileObservable_1_t2542074388 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SkipWhile__ctor_m2188486226(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SkipWhile_t3761473147 *, SkipWhileObservable_1_t2542074388 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SkipWhile__ctor_m2188486226_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::Run()
extern "C"  Il2CppObject * SkipWhile_Run_m480590675_gshared (SkipWhile_t3761473147 * __this, const MethodInfo* method);
#define SkipWhile_Run_m480590675(__this, method) ((  Il2CppObject * (*) (SkipWhile_t3761473147 *, const MethodInfo*))SkipWhile_Run_m480590675_gshared)(__this, method)
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::OnNext(T)
extern "C"  void SkipWhile_OnNext_m3360808365_gshared (SkipWhile_t3761473147 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SkipWhile_OnNext_m3360808365(__this, ___value0, method) ((  void (*) (SkipWhile_t3761473147 *, Il2CppObject *, const MethodInfo*))SkipWhile_OnNext_m3360808365_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::OnError(System.Exception)
extern "C"  void SkipWhile_OnError_m419888828_gshared (SkipWhile_t3761473147 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SkipWhile_OnError_m419888828(__this, ___error0, method) ((  void (*) (SkipWhile_t3761473147 *, Exception_t1967233988 *, const MethodInfo*))SkipWhile_OnError_m419888828_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>::OnCompleted()
extern "C"  void SkipWhile_OnCompleted_m3232820239_gshared (SkipWhile_t3761473147 * __this, const MethodInfo* method);
#define SkipWhile_OnCompleted_m3232820239(__this, method) ((  void (*) (SkipWhile_t3761473147 *, const MethodInfo*))SkipWhile_OnCompleted_m3232820239_gshared)(__this, method)
