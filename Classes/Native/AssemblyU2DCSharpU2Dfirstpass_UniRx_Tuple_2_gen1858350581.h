﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZergRush.IObservableCollection`1<System.Object>
struct IObservableCollection_1_t200574308;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`2<ZergRush.IObservableCollection`1<System.Object>,System.IDisposable>
struct  Tuple_2_t1858350581 
{
public:
	// T1 UniRx.Tuple`2::item1
	Il2CppObject* ___item1_0;
	// T2 UniRx.Tuple`2::item2
	Il2CppObject * ___item2_1;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_2_t1858350581, ___item1_0)); }
	inline Il2CppObject* get_item1_0() const { return ___item1_0; }
	inline Il2CppObject** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(Il2CppObject* value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___item1_0, value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_2_t1858350581, ___item2_1)); }
	inline Il2CppObject * get_item2_1() const { return ___item2_1; }
	inline Il2CppObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(Il2CppObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier(&___item2_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
