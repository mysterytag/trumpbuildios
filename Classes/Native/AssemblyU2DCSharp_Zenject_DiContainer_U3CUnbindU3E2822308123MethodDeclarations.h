﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<Unbind>c__AnonStorey18C`1<System.Object>
struct U3CUnbindU3Ec__AnonStorey18C_1_t2822308123;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22091358871.h"

// System.Void Zenject.DiContainer/<Unbind>c__AnonStorey18C`1<System.Object>::.ctor()
extern "C"  void U3CUnbindU3Ec__AnonStorey18C_1__ctor_m1667170675_gshared (U3CUnbindU3Ec__AnonStorey18C_1_t2822308123 * __this, const MethodInfo* method);
#define U3CUnbindU3Ec__AnonStorey18C_1__ctor_m1667170675(__this, method) ((  void (*) (U3CUnbindU3Ec__AnonStorey18C_1_t2822308123 *, const MethodInfo*))U3CUnbindU3Ec__AnonStorey18C_1__ctor_m1667170675_gshared)(__this, method)
// System.Boolean Zenject.DiContainer/<Unbind>c__AnonStorey18C`1<System.Object>::<>m__2CA(System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>)
extern "C"  bool U3CUnbindU3Ec__AnonStorey18C_1_U3CU3Em__2CA_m1903931049_gshared (U3CUnbindU3Ec__AnonStorey18C_1_t2822308123 * __this, KeyValuePair_2_t2091358871  ___x0, const MethodInfo* method);
#define U3CUnbindU3Ec__AnonStorey18C_1_U3CU3Em__2CA_m1903931049(__this, ___x0, method) ((  bool (*) (U3CUnbindU3Ec__AnonStorey18C_1_t2822308123 *, KeyValuePair_2_t2091358871 , const MethodInfo*))U3CUnbindU3Ec__AnonStorey18C_1_U3CU3Em__2CA_m1903931049_gshared)(__this, ___x0, method)
