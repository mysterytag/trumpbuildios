﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>
struct GameObjectFactory_4_t486081797;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void GameObjectFactory_4__ctor_m362319683_gshared (GameObjectFactory_4_t486081797 * __this, const MethodInfo* method);
#define GameObjectFactory_4__ctor_m362319683(__this, method) ((  void (*) (GameObjectFactory_4_t486081797 *, const MethodInfo*))GameObjectFactory_4__ctor_m362319683_gshared)(__this, method)
// TValue Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3)
extern "C"  Il2CppObject * GameObjectFactory_4_Create_m1208381602_gshared (GameObjectFactory_4_t486081797 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, const MethodInfo* method);
#define GameObjectFactory_4_Create_m1208381602(__this, ___param10, ___param21, ___param32, method) ((  Il2CppObject * (*) (GameObjectFactory_4_t486081797 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))GameObjectFactory_4_Create_m1208381602_gshared)(__this, ___param10, ___param21, ___param32, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* GameObjectFactory_4_Validate_m1198162582_gshared (GameObjectFactory_4_t486081797 * __this, const MethodInfo* method);
#define GameObjectFactory_4_Validate_m1198162582(__this, method) ((  Il2CppObject* (*) (GameObjectFactory_4_t486081797 *, const MethodInfo*))GameObjectFactory_4_Validate_m1198162582_gshared)(__this, method)
