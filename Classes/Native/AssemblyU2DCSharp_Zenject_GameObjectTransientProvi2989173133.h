﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectTransientProviderFromPrefab
struct  GameObjectTransientProviderFromPrefab_t2989173133  : public ProviderBase_t1627494391
{
public:
	// System.Type Zenject.GameObjectTransientProviderFromPrefab::_concreteType
	Type_t * ____concreteType_2;
	// Zenject.DiContainer Zenject.GameObjectTransientProviderFromPrefab::_container
	DiContainer_t2383114449 * ____container_3;
	// UnityEngine.GameObject Zenject.GameObjectTransientProviderFromPrefab::_template
	GameObject_t4012695102 * ____template_4;

public:
	inline static int32_t get_offset_of__concreteType_2() { return static_cast<int32_t>(offsetof(GameObjectTransientProviderFromPrefab_t2989173133, ____concreteType_2)); }
	inline Type_t * get__concreteType_2() const { return ____concreteType_2; }
	inline Type_t ** get_address_of__concreteType_2() { return &____concreteType_2; }
	inline void set__concreteType_2(Type_t * value)
	{
		____concreteType_2 = value;
		Il2CppCodeGenWriteBarrier(&____concreteType_2, value);
	}

	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(GameObjectTransientProviderFromPrefab_t2989173133, ____container_3)); }
	inline DiContainer_t2383114449 * get__container_3() const { return ____container_3; }
	inline DiContainer_t2383114449 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t2383114449 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier(&____container_3, value);
	}

	inline static int32_t get_offset_of__template_4() { return static_cast<int32_t>(offsetof(GameObjectTransientProviderFromPrefab_t2989173133, ____template_4)); }
	inline GameObject_t4012695102 * get__template_4() const { return ____template_4; }
	inline GameObject_t4012695102 ** get_address_of__template_4() { return &____template_4; }
	inline void set__template_4(GameObject_t4012695102 * value)
	{
		____template_4 = value;
		Il2CppCodeGenWriteBarrier(&____template_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
