﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/OpenTradeResponseCallback
struct OpenTradeResponseCallback_t4203606240;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.OpenTradeRequest
struct OpenTradeRequest_t1121233957;
// PlayFab.ClientModels.OpenTradeResponse
struct OpenTradeResponse_t3658687371;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_OpenTradeRe1121233957.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_OpenTradeRe3658687371.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/OpenTradeResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OpenTradeResponseCallback__ctor_m719251823 (OpenTradeResponseCallback_t4203606240 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/OpenTradeResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.OpenTradeRequest,PlayFab.ClientModels.OpenTradeResponse,PlayFab.PlayFabError,System.Object)
extern "C"  void OpenTradeResponseCallback_Invoke_m2315798066 (OpenTradeResponseCallback_t4203606240 * __this, String_t* ___urlPath0, int32_t ___callId1, OpenTradeRequest_t1121233957 * ___request2, OpenTradeResponse_t3658687371 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OpenTradeResponseCallback_t4203606240(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, OpenTradeRequest_t1121233957 * ___request2, OpenTradeResponse_t3658687371 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/OpenTradeResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.OpenTradeRequest,PlayFab.ClientModels.OpenTradeResponse,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OpenTradeResponseCallback_BeginInvoke_m3315539643 (OpenTradeResponseCallback_t4203606240 * __this, String_t* ___urlPath0, int32_t ___callId1, OpenTradeRequest_t1121233957 * ___request2, OpenTradeResponse_t3658687371 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/OpenTradeResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OpenTradeResponseCallback_EndInvoke_m2823187199 (OpenTradeResponseCallback_t4203606240 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
