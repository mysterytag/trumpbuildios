﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"

// System.Void System.Action`1<CollectionReplaceEvent`1<DonateButton>>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m516688672(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2479471796 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1498188969_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<CollectionReplaceEvent`1<DonateButton>>::Invoke(T)
#define Action_1_Invoke_m2155844900(__this, ___obj0, method) ((  void (*) (Action_1_t2479471796 *, CollectionReplaceEvent_1_t2331019091 *, const MethodInfo*))Action_1_Invoke_m2789055860_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<CollectionReplaceEvent`1<DonateButton>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m1468776825(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2479471796 *, CollectionReplaceEvent_1_t2331019091 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m917692971_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<CollectionReplaceEvent`1<DonateButton>>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m337775920(__this, ___result0, method) ((  void (*) (Action_1_t2479471796 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3562128182_gshared)(__this, ___result0, method)
