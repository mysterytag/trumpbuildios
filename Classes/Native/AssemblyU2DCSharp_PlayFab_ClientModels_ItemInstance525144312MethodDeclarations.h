﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ItemInstance
struct ItemInstance_t525144312;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.ItemInstance::.ctor()
extern "C"  void ItemInstance__ctor_m745986885 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemInstance::get_ItemId()
extern "C"  String_t* ItemInstance_get_ItemId_m4027756851 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_ItemId(System.String)
extern "C"  void ItemInstance_set_ItemId_m1818339608 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemInstance::get_ItemInstanceId()
extern "C"  String_t* ItemInstance_get_ItemInstanceId_m819241064 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_ItemInstanceId(System.String)
extern "C"  void ItemInstance_set_ItemInstanceId_m2357747459 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemInstance::get_ItemClass()
extern "C"  String_t* ItemInstance_get_ItemClass_m1656935234 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_ItemClass(System.String)
extern "C"  void ItemInstance_set_ItemClass_m627667447 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.ItemInstance::get_PurchaseDate()
extern "C"  Nullable_1_t3225071844  ItemInstance_get_PurchaseDate_m3779668649 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_PurchaseDate(System.Nullable`1<System.DateTime>)
extern "C"  void ItemInstance_set_PurchaseDate_m4169104508 (ItemInstance_t525144312 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.ItemInstance::get_Expiration()
extern "C"  Nullable_1_t3225071844  ItemInstance_get_Expiration_m2634469193 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_Expiration(System.Nullable`1<System.DateTime>)
extern "C"  void ItemInstance_set_Expiration_m1065673116 (ItemInstance_t525144312 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.ItemInstance::get_RemainingUses()
extern "C"  Nullable_1_t1438485399  ItemInstance_get_RemainingUses_m1309735051 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_RemainingUses(System.Nullable`1<System.Int32>)
extern "C"  void ItemInstance_set_RemainingUses_m1693934438 (ItemInstance_t525144312 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.ItemInstance::get_UsesIncrementedBy()
extern "C"  Nullable_1_t1438485399  ItemInstance_get_UsesIncrementedBy_m693228738 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_UsesIncrementedBy(System.Nullable`1<System.Int32>)
extern "C"  void ItemInstance_set_UsesIncrementedBy_m3067840207 (ItemInstance_t525144312 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemInstance::get_Annotation()
extern "C"  String_t* ItemInstance_get_Annotation_m3051322004 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_Annotation(System.String)
extern "C"  void ItemInstance_set_Annotation_m1354533079 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemInstance::get_CatalogVersion()
extern "C"  String_t* ItemInstance_get_CatalogVersion_m2808610788 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_CatalogVersion(System.String)
extern "C"  void ItemInstance_set_CatalogVersion_m1377110279 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemInstance::get_BundleParent()
extern "C"  String_t* ItemInstance_get_BundleParent_m775233265 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_BundleParent(System.String)
extern "C"  void ItemInstance_set_BundleParent_m950947162 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemInstance::get_DisplayName()
extern "C"  String_t* ItemInstance_get_DisplayName_m3211107978 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_DisplayName(System.String)
extern "C"  void ItemInstance_set_DisplayName_m1895979503 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemInstance::get_UnitCurrency()
extern "C"  String_t* ItemInstance_get_UnitCurrency_m4058609306 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_UnitCurrency(System.String)
extern "C"  void ItemInstance_set_UnitCurrency_m2489938001 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.ItemInstance::get_UnitPrice()
extern "C"  uint32_t ItemInstance_get_UnitPrice_m215629626 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_UnitPrice(System.UInt32)
extern "C"  void ItemInstance_set_UnitPrice_m603798511 (ItemInstance_t525144312 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.ItemInstance::get_BundleContents()
extern "C"  List_1_t1765447871 * ItemInstance_get_BundleContents_m3858466899 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_BundleContents(System.Collections.Generic.List`1<System.String>)
extern "C"  void ItemInstance_set_BundleContents_m271379980 (ItemInstance_t525144312 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.ItemInstance::get_CustomData()
extern "C"  Dictionary_2_t2606186806 * ItemInstance_get_CustomData_m1704108405 (ItemInstance_t525144312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemInstance::set_CustomData(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void ItemInstance_set_CustomData_m1351768136 (ItemInstance_t525144312 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
