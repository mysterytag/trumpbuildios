﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SafeScreenController
struct SafeScreenController_t1559357173;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void SafeScreenController::.ctor()
extern "C"  void SafeScreenController__ctor_m2135062342 (SafeScreenController_t1559357173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeScreenController::OnEnable()
extern "C"  void SafeScreenController_OnEnable_m3543122560 (SafeScreenController_t1559357173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeScreenController::OnDisable()
extern "C"  void SafeScreenController_OnDisable_m2903554221 (SafeScreenController_t1559357173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeScreenController::Update()
extern "C"  void SafeScreenController_Update_m3489285255 (SafeScreenController_t1559357173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SafeScreenController::AppearingCoroutine()
extern "C"  Il2CppObject * SafeScreenController_AppearingCoroutine_m3355224881 (SafeScreenController_t1559357173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SafeScreenController::DisappearingCoroutine()
extern "C"  Il2CppObject * SafeScreenController_DisappearingCoroutine_m3738337045 (SafeScreenController_t1559357173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
