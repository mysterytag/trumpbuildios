﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1<System.DateTime>
struct Cell_1_t521131568;
// ICell`1<System.DateTime>
struct ICell_1_t1890664913;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// IStream`1<System.DateTime>
struct IStream_1_t891724927;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"

// System.Void Cell`1<System.DateTime>::.ctor()
extern "C"  void Cell_1__ctor_m1694482238_gshared (Cell_1_t521131568 * __this, const MethodInfo* method);
#define Cell_1__ctor_m1694482238(__this, method) ((  void (*) (Cell_1_t521131568 *, const MethodInfo*))Cell_1__ctor_m1694482238_gshared)(__this, method)
// System.Void Cell`1<System.DateTime>::.ctor(T)
extern "C"  void Cell_1__ctor_m989343200_gshared (Cell_1_t521131568 * __this, DateTime_t339033936  ___initial0, const MethodInfo* method);
#define Cell_1__ctor_m989343200(__this, ___initial0, method) ((  void (*) (Cell_1_t521131568 *, DateTime_t339033936 , const MethodInfo*))Cell_1__ctor_m989343200_gshared)(__this, ___initial0, method)
// System.Void Cell`1<System.DateTime>::.ctor(ICell`1<T>)
extern "C"  void Cell_1__ctor_m4170136180_gshared (Cell_1_t521131568 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define Cell_1__ctor_m4170136180(__this, ___other0, method) ((  void (*) (Cell_1_t521131568 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m4170136180_gshared)(__this, ___other0, method)
// System.Void Cell`1<System.DateTime>::SetInputCell(ICell`1<T>)
extern "C"  void Cell_1_SetInputCell_m1460034950_gshared (Cell_1_t521131568 * __this, Il2CppObject* ___cell0, const MethodInfo* method);
#define Cell_1_SetInputCell_m1460034950(__this, ___cell0, method) ((  void (*) (Cell_1_t521131568 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m1460034950_gshared)(__this, ___cell0, method)
// T Cell`1<System.DateTime>::get_value()
extern "C"  DateTime_t339033936  Cell_1_get_value_m1160472643_gshared (Cell_1_t521131568 * __this, const MethodInfo* method);
#define Cell_1_get_value_m1160472643(__this, method) ((  DateTime_t339033936  (*) (Cell_1_t521131568 *, const MethodInfo*))Cell_1_get_value_m1160472643_gshared)(__this, method)
// System.Void Cell`1<System.DateTime>::set_value(T)
extern "C"  void Cell_1_set_value_m300678670_gshared (Cell_1_t521131568 * __this, DateTime_t339033936  ___value0, const MethodInfo* method);
#define Cell_1_set_value_m300678670(__this, ___value0, method) ((  void (*) (Cell_1_t521131568 *, DateTime_t339033936 , const MethodInfo*))Cell_1_set_value_m300678670_gshared)(__this, ___value0, method)
// System.Void Cell`1<System.DateTime>::Set(T)
extern "C"  void Cell_1_Set_m7338528_gshared (Cell_1_t521131568 * __this, DateTime_t339033936  ___val0, const MethodInfo* method);
#define Cell_1_Set_m7338528(__this, ___val0, method) ((  void (*) (Cell_1_t521131568 *, DateTime_t339033936 , const MethodInfo*))Cell_1_Set_m7338528_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<System.DateTime>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m3463937193_gshared (Cell_1_t521131568 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_OnChanged_m3463937193(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t521131568 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m3463937193_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<System.DateTime>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m4016140312_gshared (Cell_1_t521131568 * __this, const MethodInfo* method);
#define Cell_1_get_valueObject_m4016140312(__this, method) ((  Il2CppObject * (*) (Cell_1_t521131568 *, const MethodInfo*))Cell_1_get_valueObject_m4016140312_gshared)(__this, method)
// System.IDisposable Cell`1<System.DateTime>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m2767911504_gshared (Cell_1_t521131568 * __this, Action_1_t487486641 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_ListenUpdates_m2767911504(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t521131568 *, Action_1_t487486641 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m2767911504_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<System.DateTime>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m4219183406_gshared (Cell_1_t521131568 * __this, const MethodInfo* method);
#define Cell_1_get_updates_m4219183406(__this, method) ((  Il2CppObject* (*) (Cell_1_t521131568 *, const MethodInfo*))Cell_1_get_updates_m4219183406_gshared)(__this, method)
// System.IDisposable Cell`1<System.DateTime>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m2895034866_gshared (Cell_1_t521131568 * __this, Action_1_t487486641 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_Bind_m2895034866(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t521131568 *, Action_1_t487486641 *, int32_t, const MethodInfo*))Cell_1_Bind_m2895034866_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<System.DateTime>::SetInputStream(IStream`1<T>)
extern "C"  void Cell_1_SetInputStream_m98706306_gshared (Cell_1_t521131568 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Cell_1_SetInputStream_m98706306(__this, ___stream0, method) ((  void (*) (Cell_1_t521131568 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m98706306_gshared)(__this, ___stream0, method)
// System.Void Cell`1<System.DateTime>::ResetInputStream()
extern "C"  void Cell_1_ResetInputStream_m460880673_gshared (Cell_1_t521131568 * __this, const MethodInfo* method);
#define Cell_1_ResetInputStream_m460880673(__this, method) ((  void (*) (Cell_1_t521131568 *, const MethodInfo*))Cell_1_ResetInputStream_m460880673_gshared)(__this, method)
// System.Void Cell`1<System.DateTime>::TransactionIterationFinished()
extern "C"  void Cell_1_TransactionIterationFinished_m2659938871_gshared (Cell_1_t521131568 * __this, const MethodInfo* method);
#define Cell_1_TransactionIterationFinished_m2659938871(__this, method) ((  void (*) (Cell_1_t521131568 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m2659938871_gshared)(__this, method)
// System.Void Cell`1<System.DateTime>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m2004511241_gshared (Cell_1_t521131568 * __this, int32_t ___p0, const MethodInfo* method);
#define Cell_1_Unpack_m2004511241(__this, ___p0, method) ((  void (*) (Cell_1_t521131568 *, int32_t, const MethodInfo*))Cell_1_Unpack_m2004511241_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<System.DateTime>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m2421816048_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t521131568 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_LessThanOrEqual_m2421816048(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t521131568 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m2421816048_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<System.DateTime>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m1543445911_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t521131568 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_GreaterThanOrEqual_m1543445911(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t521131568 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m1543445911_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
