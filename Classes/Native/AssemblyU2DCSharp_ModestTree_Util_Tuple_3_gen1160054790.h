﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>
struct  Tuple_3_t1160054790  : public Il2CppObject
{
public:
	// T1 ModestTree.Util.Tuple`3::First
	Il2CppObject * ___First_0;
	// T2 ModestTree.Util.Tuple`3::Second
	Il2CppObject * ___Second_1;
	// T3 ModestTree.Util.Tuple`3::Third
	Il2CppObject * ___Third_2;

public:
	inline static int32_t get_offset_of_First_0() { return static_cast<int32_t>(offsetof(Tuple_3_t1160054790, ___First_0)); }
	inline Il2CppObject * get_First_0() const { return ___First_0; }
	inline Il2CppObject ** get_address_of_First_0() { return &___First_0; }
	inline void set_First_0(Il2CppObject * value)
	{
		___First_0 = value;
		Il2CppCodeGenWriteBarrier(&___First_0, value);
	}

	inline static int32_t get_offset_of_Second_1() { return static_cast<int32_t>(offsetof(Tuple_3_t1160054790, ___Second_1)); }
	inline Il2CppObject * get_Second_1() const { return ___Second_1; }
	inline Il2CppObject ** get_address_of_Second_1() { return &___Second_1; }
	inline void set_Second_1(Il2CppObject * value)
	{
		___Second_1 = value;
		Il2CppCodeGenWriteBarrier(&___Second_1, value);
	}

	inline static int32_t get_offset_of_Third_2() { return static_cast<int32_t>(offsetof(Tuple_3_t1160054790, ___Third_2)); }
	inline Il2CppObject * get_Third_2() const { return ___Third_2; }
	inline Il2CppObject ** get_address_of_Third_2() { return &___Third_2; }
	inline void set_Third_2(Il2CppObject * value)
	{
		___Third_2 = value;
		Il2CppCodeGenWriteBarrier(&___Third_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
