﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AbstractLayoutCell
struct  AbstractLayoutCell_t3337132430  : public Il2CppObject
{
public:
	// UnityEngine.Vector2 AbstractLayoutCell::pos
	Vector2_t3525329788  ___pos_0;
	// System.Single AbstractLayoutCell::height
	float ___height_1;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(AbstractLayoutCell_t3337132430, ___pos_0)); }
	inline Vector2_t3525329788  get_pos_0() const { return ___pos_0; }
	inline Vector2_t3525329788 * get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(Vector2_t3525329788  value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(AbstractLayoutCell_t3337132430, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
