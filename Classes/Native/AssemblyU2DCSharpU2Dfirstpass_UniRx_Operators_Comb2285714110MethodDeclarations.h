﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_6_t2285714110;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_6__ctor_m4168547820_gshared (CombineLatestFunc_6_t2285714110 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define CombineLatestFunc_6__ctor_m4168547820(__this, ___object0, ___method1, method) ((  void (*) (CombineLatestFunc_6_t2285714110 *, Il2CppObject *, IntPtr_t, const MethodInfo*))CombineLatestFunc_6__ctor_m4168547820_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5)
extern "C"  Il2CppObject * CombineLatestFunc_6_Invoke_m1574352046_gshared (CombineLatestFunc_6_t2285714110 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
#define CombineLatestFunc_6_Invoke_m1574352046(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method) ((  Il2CppObject * (*) (CombineLatestFunc_6_t2285714110 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_6_Invoke_m1574352046_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method)
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_6_BeginInvoke_m49947744_gshared (CombineLatestFunc_6_t2285714110 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, AsyncCallback_t1363551830 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method);
#define CombineLatestFunc_6_BeginInvoke_m49947744(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___callback5, ___object6, method) ((  Il2CppObject * (*) (CombineLatestFunc_6_t2285714110 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_6_BeginInvoke_m49947744_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___callback5, ___object6, method)
// TR UniRx.Operators.CombineLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_6_EndInvoke_m246089635_gshared (CombineLatestFunc_6_t2285714110 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define CombineLatestFunc_6_EndInvoke_m246089635(__this, ___result0, method) ((  Il2CppObject * (*) (CombineLatestFunc_6_t2285714110 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_6_EndInvoke_m246089635_gshared)(__this, ___result0, method)
