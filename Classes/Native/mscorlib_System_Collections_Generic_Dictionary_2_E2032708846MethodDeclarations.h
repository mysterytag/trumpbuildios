﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1455559680(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2032708846 *, Dictionary_2_t2265680905 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m653397035(__this, method) ((  Il2CppObject * (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2800417973(__this, method) ((  void (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1294558060(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m611597511(__this, method) ((  Il2CppObject * (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m383124889(__this, method) ((  Il2CppObject * (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::MoveNext()
#define Enumerator_MoveNext_m1377048165(__this, method) ((  bool (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::get_Current()
#define Enumerator_get_Current_m2772265911(__this, method) ((  KeyValuePair_2_t1754212203  (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2254617262(__this, method) ((  SingletonId_t1838183899 * (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3580575470(__this, method) ((  SingletonLazyCreator_t2762284194 * (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::Reset()
#define Enumerator_Reset_m3766005330(__this, method) ((  void (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::VerifyState()
#define Enumerator_VerifyState_m1183873243(__this, method) ((  void (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2280724867(__this, method) ((  void (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.SingletonId,Zenject.SingletonLazyCreator>::Dispose()
#define Enumerator_Dispose_m997120674(__this, method) ((  void (*) (Enumerator_t2032708846 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
