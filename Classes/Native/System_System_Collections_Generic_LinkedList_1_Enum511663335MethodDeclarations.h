﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.IDisposable>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m3543097305(__this, ___parent0, method) ((  void (*) (Enumerator_t511663336 *, LinkedList_1_t3369050920 *, const MethodInfo*))Enumerator__ctor_m857368315_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.IDisposable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2683854824(__this, method) ((  Il2CppObject * (*) (Enumerator_t511663336 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.IDisposable>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2074924274(__this, method) ((  void (*) (Enumerator_t511663336 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.IDisposable>::get_Current()
#define Enumerator_get_Current_m1256013079(__this, method) ((  Il2CppObject * (*) (Enumerator_t511663336 *, const MethodInfo*))Enumerator_get_Current_m1124073047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.IDisposable>::MoveNext()
#define Enumerator_MoveNext_m2208938038(__this, method) ((  bool (*) (Enumerator_t511663336 *, const MethodInfo*))Enumerator_MoveNext_m2358966120_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.IDisposable>::Dispose()
#define Enumerator_Dispose_m3107260741(__this, method) ((  void (*) (Enumerator_t511663336 *, const MethodInfo*))Enumerator_Dispose_m272587367_gshared)(__this, method)
