﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RateScreen
struct RateScreen_t4053577100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void RateScreen::.ctor()
extern "C"  void RateScreen__ctor_m4081805455 (RateScreen_t4053577100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RateScreen::PostInject()
extern "C"  void RateScreen_PostInject_m159690214 (RateScreen_t4053577100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RateScreen::<PostInject>m__C4(UniRx.Unit)
extern "C"  void RateScreen_U3CPostInjectU3Em__C4_m3535702014 (RateScreen_t4053577100 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RateScreen::<PostInject>m__C5(UniRx.Unit)
extern "C"  void RateScreen_U3CPostInjectU3Em__C5_m3242299007 (RateScreen_t4053577100 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
