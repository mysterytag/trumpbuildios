﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithEmailAddressResponseCallback
struct LoginWithEmailAddressResponseCallback_t1849531341;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithEmailAddressRequest
struct LoginWithEmailAddressRequest_t3586495128;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithEm3586495128.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithEmailAddressResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithEmailAddressResponseCallback__ctor_m1701882332 (LoginWithEmailAddressResponseCallback_t1849531341 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithEmailAddressResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithEmailAddressRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LoginWithEmailAddressResponseCallback_Invoke_m368155799 (LoginWithEmailAddressResponseCallback_t1849531341 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithEmailAddressRequest_t3586495128 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithEmailAddressResponseCallback_t1849531341(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithEmailAddressRequest_t3586495128 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithEmailAddressResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithEmailAddressRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithEmailAddressResponseCallback_BeginInvoke_m739710832 (LoginWithEmailAddressResponseCallback_t1849531341 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithEmailAddressRequest_t3586495128 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithEmailAddressResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithEmailAddressResponseCallback_EndInvoke_m343632364 (LoginWithEmailAddressResponseCallback_t1849531341 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
