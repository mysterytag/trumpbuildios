﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<PlayFab.Internal.GMFB_327/testRegion>
struct Action_1_t1833392007;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m765264978_gshared (Action_1_t1833392007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m765264978(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t1833392007 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m765264978_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<PlayFab.Internal.GMFB_327/testRegion>::Invoke(T)
extern "C"  void Action_1_Invoke_m111998898_gshared (Action_1_t1833392007 * __this, int32_t ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m111998898(__this, ___obj0, method) ((  void (*) (Action_1_t1833392007 *, int32_t, const MethodInfo*))Action_1_Invoke_m111998898_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<PlayFab.Internal.GMFB_327/testRegion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1397591815_gshared (Action_1_t1833392007 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m1397591815(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t1833392007 *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1397591815_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<PlayFab.Internal.GMFB_327/testRegion>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3954405218_gshared (Action_1_t1833392007 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m3954405218(__this, ___result0, method) ((  void (*) (Action_1_t1833392007 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3954405218_gshared)(__this, ___result0, method)
