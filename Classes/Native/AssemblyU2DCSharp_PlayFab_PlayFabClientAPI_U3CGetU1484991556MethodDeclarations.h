﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetUserPublisherData>c__AnonStorey93
struct U3CGetUserPublisherDataU3Ec__AnonStorey93_t1484991556;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetUserPublisherData>c__AnonStorey93::.ctor()
extern "C"  void U3CGetUserPublisherDataU3Ec__AnonStorey93__ctor_m988910575 (U3CGetUserPublisherDataU3Ec__AnonStorey93_t1484991556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetUserPublisherData>c__AnonStorey93::<>m__80(PlayFab.CallRequestContainer)
extern "C"  void U3CGetUserPublisherDataU3Ec__AnonStorey93_U3CU3Em__80_m988797829 (U3CGetUserPublisherDataU3Ec__AnonStorey93_t1484991556 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
