﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Message/<PostInject>c__AnonStorey58
struct U3CPostInjectU3Ec__AnonStorey58_t980490265;
// <>__AnonType3`2<System.Boolean,System.Int32>
struct U3CU3E__AnonType3_2_t3095608592;

#include "codegen/il2cpp-codegen.h"

// System.Void Message/<PostInject>c__AnonStorey58::.ctor()
extern "C"  void U3CPostInjectU3Ec__AnonStorey58__ctor_m2947229850 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message/<PostInject>c__AnonStorey58::<>m__3D(System.Boolean)
extern "C"  void U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3D_m729826699 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message/<PostInject>c__AnonStorey58::<>m__3F(<>__AnonType3`2<System.Boolean,System.Int32>)
extern "C"  void U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__3F_m1207743275 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, U3CU3E__AnonType3_2_t3095608592 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message/<PostInject>c__AnonStorey58::<>m__40()
extern "C"  void U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__40_m4212567135 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message/<PostInject>c__AnonStorey58::<>m__41()
extern "C"  void U3CPostInjectU3Ec__AnonStorey58_U3CU3Em__41_m4212568096 (U3CPostInjectU3Ec__AnonStorey58_t980490265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
