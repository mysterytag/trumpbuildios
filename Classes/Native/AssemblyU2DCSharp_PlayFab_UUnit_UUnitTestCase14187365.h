﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Diagnostics.Stopwatch
struct Stopwatch_t2509581612;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.UUnitTestCase
struct  UUnitTestCase_t14187365  : public Il2CppObject
{
public:
	// System.Diagnostics.Stopwatch PlayFab.UUnit.UUnitTestCase::setUpStopwatch
	Stopwatch_t2509581612 * ___setUpStopwatch_0;
	// System.Diagnostics.Stopwatch PlayFab.UUnit.UUnitTestCase::tearDownStopwatch
	Stopwatch_t2509581612 * ___tearDownStopwatch_1;
	// System.Diagnostics.Stopwatch PlayFab.UUnit.UUnitTestCase::eachTestStopwatch
	Stopwatch_t2509581612 * ___eachTestStopwatch_2;
	// System.String PlayFab.UUnit.UUnitTestCase::testMethodName
	String_t* ___testMethodName_3;

public:
	inline static int32_t get_offset_of_setUpStopwatch_0() { return static_cast<int32_t>(offsetof(UUnitTestCase_t14187365, ___setUpStopwatch_0)); }
	inline Stopwatch_t2509581612 * get_setUpStopwatch_0() const { return ___setUpStopwatch_0; }
	inline Stopwatch_t2509581612 ** get_address_of_setUpStopwatch_0() { return &___setUpStopwatch_0; }
	inline void set_setUpStopwatch_0(Stopwatch_t2509581612 * value)
	{
		___setUpStopwatch_0 = value;
		Il2CppCodeGenWriteBarrier(&___setUpStopwatch_0, value);
	}

	inline static int32_t get_offset_of_tearDownStopwatch_1() { return static_cast<int32_t>(offsetof(UUnitTestCase_t14187365, ___tearDownStopwatch_1)); }
	inline Stopwatch_t2509581612 * get_tearDownStopwatch_1() const { return ___tearDownStopwatch_1; }
	inline Stopwatch_t2509581612 ** get_address_of_tearDownStopwatch_1() { return &___tearDownStopwatch_1; }
	inline void set_tearDownStopwatch_1(Stopwatch_t2509581612 * value)
	{
		___tearDownStopwatch_1 = value;
		Il2CppCodeGenWriteBarrier(&___tearDownStopwatch_1, value);
	}

	inline static int32_t get_offset_of_eachTestStopwatch_2() { return static_cast<int32_t>(offsetof(UUnitTestCase_t14187365, ___eachTestStopwatch_2)); }
	inline Stopwatch_t2509581612 * get_eachTestStopwatch_2() const { return ___eachTestStopwatch_2; }
	inline Stopwatch_t2509581612 ** get_address_of_eachTestStopwatch_2() { return &___eachTestStopwatch_2; }
	inline void set_eachTestStopwatch_2(Stopwatch_t2509581612 * value)
	{
		___eachTestStopwatch_2 = value;
		Il2CppCodeGenWriteBarrier(&___eachTestStopwatch_2, value);
	}

	inline static int32_t get_offset_of_testMethodName_3() { return static_cast<int32_t>(offsetof(UUnitTestCase_t14187365, ___testMethodName_3)); }
	inline String_t* get_testMethodName_3() const { return ___testMethodName_3; }
	inline String_t** get_address_of_testMethodName_3() { return &___testMethodName_3; }
	inline void set_testMethodName_3(String_t* value)
	{
		___testMethodName_3 = value;
		Il2CppCodeGenWriteBarrier(&___testMethodName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
