﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.DeflateManager/Config
struct Config_t2024042338;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_DeflateFlavor4208336886.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"

// System.Void Ionic.Zlib.DeflateManager/Config::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,Ionic.Zlib.DeflateFlavor)
extern "C"  void Config__ctor_m4062872785 (Config_t2024042338 * __this, int32_t ___goodLength0, int32_t ___maxLazy1, int32_t ___niceLength2, int32_t ___maxChainLength3, int32_t ___flavor4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager/Config::.cctor()
extern "C"  void Config__cctor_m259104778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.DeflateManager/Config Ionic.Zlib.DeflateManager/Config::Lookup(Ionic.Zlib.CompressionLevel)
extern "C"  Config_t2024042338 * Config_Lookup_m1544319037 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
