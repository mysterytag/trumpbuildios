﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2<System.Object,System.Object>
struct SelectObservable_2_t575849553;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Func`3<System.Object,System.Int32,System.Object>
struct Func_3_t3690668578;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m1770391551_gshared (SelectObservable_2_t575849553 * __this, Il2CppObject* ___source0, Func_2_t2135783352 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m1770391551(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t575849553 *, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))SelectObservable_2__ctor_m1770391551_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m533338129_gshared (SelectObservable_2_t575849553 * __this, Il2CppObject* ___source0, Func_3_t3690668578 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m533338129(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t575849553 *, Il2CppObject*, Func_3_t3690668578 *, const MethodInfo*))SelectObservable_2__ctor_m533338129_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m2975040157_gshared (SelectObservable_2_t575849553 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectObservable_2_SubscribeCore_m2975040157(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectObservable_2_t575849553 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectObservable_2_SubscribeCore_m2975040157_gshared)(__this, ___observer0, ___cancel1, method)
