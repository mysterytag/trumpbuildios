﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::.ctor()
#define Subject_1__ctor_m3370084197(__this, method) ((  void (*) (Subject_1_t1488904540 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::get_HasObservers()
#define Subject_1_get_HasObservers_m756612687(__this, method) ((  bool (*) (Subject_1_t1488904540 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::OnCompleted()
#define Subject_1_OnCompleted_m1463204655(__this, method) ((  void (*) (Subject_1_t1488904540 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::OnError(System.Exception)
#define Subject_1_OnError_m3534691804(__this, ___error0, method) ((  void (*) (Subject_1_t1488904540 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::OnNext(T)
#define Subject_1_OnNext_m3076275405(__this, ___value0, method) ((  void (*) (Subject_1_t1488904540 *, OnStateInfo_t3845837216 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m2232791674(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1488904540 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::Dispose()
#define Subject_1_Dispose_m143855202(__this, method) ((  void (*) (Subject_1_t1488904540 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m1425356267(__this, method) ((  void (*) (Subject_1_t1488904540 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1888884102(__this, method) ((  bool (*) (Subject_1_t1488904540 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
