﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.ReportProgress
struct ReportProgress_t1158862141;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.PlayGamesAchievement
struct  PlayGamesAchievement_t145591998  : public Il2CppObject
{
public:
	// GooglePlayGames.ReportProgress GooglePlayGames.PlayGamesAchievement::mProgressCallback
	ReportProgress_t1158862141 * ___mProgressCallback_0;
	// System.String GooglePlayGames.PlayGamesAchievement::mId
	String_t* ___mId_1;
	// System.Double GooglePlayGames.PlayGamesAchievement::mPercentComplete
	double ___mPercentComplete_2;

public:
	inline static int32_t get_offset_of_mProgressCallback_0() { return static_cast<int32_t>(offsetof(PlayGamesAchievement_t145591998, ___mProgressCallback_0)); }
	inline ReportProgress_t1158862141 * get_mProgressCallback_0() const { return ___mProgressCallback_0; }
	inline ReportProgress_t1158862141 ** get_address_of_mProgressCallback_0() { return &___mProgressCallback_0; }
	inline void set_mProgressCallback_0(ReportProgress_t1158862141 * value)
	{
		___mProgressCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___mProgressCallback_0, value);
	}

	inline static int32_t get_offset_of_mId_1() { return static_cast<int32_t>(offsetof(PlayGamesAchievement_t145591998, ___mId_1)); }
	inline String_t* get_mId_1() const { return ___mId_1; }
	inline String_t** get_address_of_mId_1() { return &___mId_1; }
	inline void set_mId_1(String_t* value)
	{
		___mId_1 = value;
		Il2CppCodeGenWriteBarrier(&___mId_1, value);
	}

	inline static int32_t get_offset_of_mPercentComplete_2() { return static_cast<int32_t>(offsetof(PlayGamesAchievement_t145591998, ___mPercentComplete_2)); }
	inline double get_mPercentComplete_2() const { return ___mPercentComplete_2; }
	inline double* get_address_of_mPercentComplete_2() { return &___mPercentComplete_2; }
	inline void set_mPercentComplete_2(double value)
	{
		___mPercentComplete_2 = value;
	}
};

struct PlayGamesAchievement_t145591998_StaticFields
{
public:
	// System.DateTime GooglePlayGames.PlayGamesAchievement::Thesentinel
	DateTime_t339033936  ___Thesentinel_3;

public:
	inline static int32_t get_offset_of_Thesentinel_3() { return static_cast<int32_t>(offsetof(PlayGamesAchievement_t145591998_StaticFields, ___Thesentinel_3)); }
	inline DateTime_t339033936  get_Thesentinel_3() const { return ___Thesentinel_3; }
	inline DateTime_t339033936 * get_address_of_Thesentinel_3() { return &___Thesentinel_3; }
	inline void set_Thesentinel_3(DateTime_t339033936  value)
	{
		___Thesentinel_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
