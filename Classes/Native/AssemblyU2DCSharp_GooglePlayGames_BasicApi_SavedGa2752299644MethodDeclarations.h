﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa2752299644.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa1895597947.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"

// System.Void GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::.ctor(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder)
extern "C"  void SavedGameMetadataUpdate__ctor_m2933627399 (SavedGameMetadataUpdate_t2752299644 * __this, Builder_t1895597947  ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsDescriptionUpdated()
extern "C"  bool SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1911575107 (SavedGameMetadataUpdate_t2752299644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedDescription()
extern "C"  String_t* SavedGameMetadataUpdate_get_UpdatedDescription_m2478192842 (SavedGameMetadataUpdate_t2752299644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsCoverImageUpdated()
extern "C"  bool SavedGameMetadataUpdate_get_IsCoverImageUpdated_m3528254261 (SavedGameMetadataUpdate_t2752299644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPngCoverImage()
extern "C"  ByteU5BU5D_t58506160* SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m2686768578 (SavedGameMetadataUpdate_t2752299644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsPlayedTimeUpdated()
extern "C"  bool SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_m2853300825 (SavedGameMetadataUpdate_t2752299644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPlayedTime()
extern "C"  Nullable_1_t3649900800  SavedGameMetadataUpdate_get_UpdatedPlayedTime_m2719683437 (SavedGameMetadataUpdate_t2752299644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
