﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>
struct TakeWhile__t2842089094;
// UniRx.Operators.TakeWhileObservable`1<System.Object>
struct TakeWhileObservable_1_t3525448012;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::.ctor(UniRx.Operators.TakeWhileObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeWhile___ctor_m2948940349_gshared (TakeWhile__t2842089094 * __this, TakeWhileObservable_1_t3525448012 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define TakeWhile___ctor_m2948940349(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (TakeWhile__t2842089094 *, TakeWhileObservable_1_t3525448012 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeWhile___ctor_m2948940349_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::Run()
extern "C"  Il2CppObject * TakeWhile__Run_m3431004508_gshared (TakeWhile__t2842089094 * __this, const MethodInfo* method);
#define TakeWhile__Run_m3431004508(__this, method) ((  Il2CppObject * (*) (TakeWhile__t2842089094 *, const MethodInfo*))TakeWhile__Run_m3431004508_gshared)(__this, method)
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::OnNext(T)
extern "C"  void TakeWhile__OnNext_m1156978912_gshared (TakeWhile__t2842089094 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define TakeWhile__OnNext_m1156978912(__this, ___value0, method) ((  void (*) (TakeWhile__t2842089094 *, Il2CppObject *, const MethodInfo*))TakeWhile__OnNext_m1156978912_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::OnError(System.Exception)
extern "C"  void TakeWhile__OnError_m95187951_gshared (TakeWhile__t2842089094 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeWhile__OnError_m95187951(__this, ___error0, method) ((  void (*) (TakeWhile__t2842089094 *, Exception_t1967233988 *, const MethodInfo*))TakeWhile__OnError_m95187951_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeWhileObservable`1/TakeWhile_<System.Object>::OnCompleted()
extern "C"  void TakeWhile__OnCompleted_m2374915522_gshared (TakeWhile__t2842089094 * __this, const MethodInfo* method);
#define TakeWhile__OnCompleted_m2374915522(__this, method) ((  void (*) (TakeWhile__t2842089094 *, const MethodInfo*))TakeWhile__OnCompleted_m2374915522_gshared)(__this, method)
