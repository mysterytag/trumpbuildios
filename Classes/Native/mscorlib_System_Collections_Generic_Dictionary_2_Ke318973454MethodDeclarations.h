﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>
struct KeyCollection_t318973454;
// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct Dictionary_2_t2290665470;
// System.Collections.Generic.IEnumerator`1<SponsorPay.SPLogLevel>
struct IEnumerator_1_t1703828583;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// SponsorPay.SPLogLevel[]
struct SPLogLevelU5BU5D_t762423022;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2057693411.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1771552870_gshared (KeyCollection_t318973454 * __this, Dictionary_2_t2290665470 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1771552870(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t318973454 *, Dictionary_2_t2290665470 *, const MethodInfo*))KeyCollection__ctor_m1771552870_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4196692272_gshared (KeyCollection_t318973454 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4196692272(__this, ___item0, method) ((  void (*) (KeyCollection_t318973454 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4196692272_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2989826727_gshared (KeyCollection_t318973454 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2989826727(__this, method) ((  void (*) (KeyCollection_t318973454 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2989826727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3467956382_gshared (KeyCollection_t318973454 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3467956382(__this, ___item0, method) ((  bool (*) (KeyCollection_t318973454 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3467956382_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m848875907_gshared (KeyCollection_t318973454 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m848875907(__this, ___item0, method) ((  bool (*) (KeyCollection_t318973454 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m848875907_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3935080953_gshared (KeyCollection_t318973454 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3935080953(__this, method) ((  Il2CppObject* (*) (KeyCollection_t318973454 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3935080953_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3379381401_gshared (KeyCollection_t318973454 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3379381401(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t318973454 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3379381401_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2965712232_gshared (KeyCollection_t318973454 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2965712232(__this, method) ((  Il2CppObject * (*) (KeyCollection_t318973454 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2965712232_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2692520063_gshared (KeyCollection_t318973454 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2692520063(__this, method) ((  bool (*) (KeyCollection_t318973454 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2692520063_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m745300977_gshared (KeyCollection_t318973454 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m745300977(__this, method) ((  bool (*) (KeyCollection_t318973454 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m745300977_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3200787683_gshared (KeyCollection_t318973454 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3200787683(__this, method) ((  Il2CppObject * (*) (KeyCollection_t318973454 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3200787683_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m753942299_gshared (KeyCollection_t318973454 * __this, SPLogLevelU5BU5D_t762423022* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m753942299(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t318973454 *, SPLogLevelU5BU5D_t762423022*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m753942299_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2057693411  KeyCollection_GetEnumerator_m1895741672_gshared (KeyCollection_t318973454 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1895741672(__this, method) ((  Enumerator_t2057693411  (*) (KeyCollection_t318973454 *, const MethodInfo*))KeyCollection_GetEnumerator_m1895741672_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m567068843_gshared (KeyCollection_t318973454 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m567068843(__this, method) ((  int32_t (*) (KeyCollection_t318973454 *, const MethodInfo*))KeyCollection_get_Count_m567068843_gshared)(__this, method)
