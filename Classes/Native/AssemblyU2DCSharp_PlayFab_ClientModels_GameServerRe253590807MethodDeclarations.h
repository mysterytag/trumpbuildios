﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GameServerRegionsResult
struct GameServerRegionsResult_t253590807;
// System.Collections.Generic.List`1<PlayFab.ClientModels.RegionInfo>
struct List_1_t4271828715;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GameServerRegionsResult::.ctor()
extern "C"  void GameServerRegionsResult__ctor_m2580844146 (GameServerRegionsResult_t253590807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.RegionInfo> PlayFab.ClientModels.GameServerRegionsResult::get_Regions()
extern "C"  List_1_t4271828715 * GameServerRegionsResult_get_Regions_m3612773221 (GameServerRegionsResult_t253590807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameServerRegionsResult::set_Regions(System.Collections.Generic.List`1<PlayFab.ClientModels.RegionInfo>)
extern "C"  void GameServerRegionsResult_set_Regions_m360066326 (GameServerRegionsResult_t253590807 * __this, List_1_t4271828715 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
