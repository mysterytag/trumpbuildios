﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Func`3<System.Int64,UniRx.Unit,System.Int64>
struct Func_3_t3706795343;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559853.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ScanObservable`2<UniRx.Unit,System.Int64>
struct  ScanObservable_2_t850793272  : public OperatorObservableBase_1_t1911559853
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.ScanObservable`2::source
	Il2CppObject* ___source_1;
	// TAccumulate UniRx.Operators.ScanObservable`2::seed
	int64_t ___seed_2;
	// System.Func`3<TAccumulate,TSource,TAccumulate> UniRx.Operators.ScanObservable`2::accumulator
	Func_3_t3706795343 * ___accumulator_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ScanObservable_2_t850793272, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_seed_2() { return static_cast<int32_t>(offsetof(ScanObservable_2_t850793272, ___seed_2)); }
	inline int64_t get_seed_2() const { return ___seed_2; }
	inline int64_t* get_address_of_seed_2() { return &___seed_2; }
	inline void set_seed_2(int64_t value)
	{
		___seed_2 = value;
	}

	inline static int32_t get_offset_of_accumulator_3() { return static_cast<int32_t>(offsetof(ScanObservable_2_t850793272, ___accumulator_3)); }
	inline Func_3_t3706795343 * get_accumulator_3() const { return ___accumulator_3; }
	inline Func_3_t3706795343 ** get_address_of_accumulator_3() { return &___accumulator_3; }
	inline void set_accumulator_3(Func_3_t3706795343 * value)
	{
		___accumulator_3 = value;
		Il2CppCodeGenWriteBarrier(&___accumulator_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
