﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkSteamAccount>c__AnonStorey88
struct U3CUnlinkSteamAccountU3Ec__AnonStorey88_t365197461;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkSteamAccount>c__AnonStorey88::.ctor()
extern "C"  void U3CUnlinkSteamAccountU3Ec__AnonStorey88__ctor_m2192809662 (U3CUnlinkSteamAccountU3Ec__AnonStorey88_t365197461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkSteamAccount>c__AnonStorey88::<>m__75(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkSteamAccountU3Ec__AnonStorey88_U3CU3Em__75_m3818519418 (U3CUnlinkSteamAccountU3Ec__AnonStorey88_t365197461 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
