﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<System.Action`1<UnityEngine.EventSystems.BaseEventData>,Priority,System.IDisposable>
struct Func_3_t3600102922;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnonymousStream`1<UnityEngine.EventSystems.BaseEventData>
struct  AnonymousStream_1_t2804241537  : public Il2CppObject
{
public:
	// System.Func`3<System.Action`1<T>,Priority,System.IDisposable> AnonymousStream`1::listen
	Func_3_t3600102922 * ___listen_0;

public:
	inline static int32_t get_offset_of_listen_0() { return static_cast<int32_t>(offsetof(AnonymousStream_1_t2804241537, ___listen_0)); }
	inline Func_3_t3600102922 * get_listen_0() const { return ___listen_0; }
	inline Func_3_t3600102922 ** get_address_of_listen_0() { return &___listen_0; }
	inline void set_listen_0(Func_3_t3600102922 * value)
	{
		___listen_0 = value;
		Il2CppCodeGenWriteBarrier(&___listen_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
