﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,System.Object>
struct SelectManyObserverWithIndex_t2112772392;
// UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>
struct SelectManyObservable_2_t562993966;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyObserverWithIndex__ctor_m2658305626_gshared (SelectManyObserverWithIndex_t2112772392 * __this, SelectManyObservable_2_t562993966 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyObserverWithIndex__ctor_m2658305626(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyObserverWithIndex_t2112772392 *, SelectManyObservable_2_t562993966 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex__ctor_m2658305626_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyObserverWithIndex_Run_m1837685092_gshared (SelectManyObserverWithIndex_t2112772392 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_Run_m1837685092(__this, method) ((  Il2CppObject * (*) (SelectManyObserverWithIndex_t2112772392 *, const MethodInfo*))SelectManyObserverWithIndex_Run_m1837685092_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,System.Object>::OnNext(TSource)
extern "C"  void SelectManyObserverWithIndex_OnNext_m3707685069_gshared (SelectManyObserverWithIndex_t2112772392 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnNext_m3707685069(__this, ___value0, method) ((  void (*) (SelectManyObserverWithIndex_t2112772392 *, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex_OnNext_m3707685069_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyObserverWithIndex_OnError_m989899767_gshared (SelectManyObserverWithIndex_t2112772392 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnError_m989899767(__this, ___error0, method) ((  void (*) (SelectManyObserverWithIndex_t2112772392 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserverWithIndex_OnError_m989899767_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,System.Object>::OnCompleted()
extern "C"  void SelectManyObserverWithIndex_OnCompleted_m1706056138_gshared (SelectManyObserverWithIndex_t2112772392 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnCompleted_m1706056138(__this, method) ((  void (*) (SelectManyObserverWithIndex_t2112772392 *, const MethodInfo*))SelectManyObserverWithIndex_OnCompleted_m1706056138_gshared)(__this, method)
