﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserRequestCallback
struct GetLeaderboardAroundCurrentUserRequestCallback_t3017392292;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest
struct GetLeaderboardAroundCurrentUserRequest_t545060687;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa545060687.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardAroundCurrentUserRequestCallback__ctor_m1636461459 (GetLeaderboardAroundCurrentUserRequestCallback_t3017392292 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest,System.Object)
extern "C"  void GetLeaderboardAroundCurrentUserRequestCallback_Invoke_m768390687 (GetLeaderboardAroundCurrentUserRequestCallback_t3017392292 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCurrentUserRequest_t545060687 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundCurrentUserRequestCallback_t3017392292(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCurrentUserRequest_t545060687 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardAroundCurrentUserRequestCallback_BeginInvoke_m1846554670 (GetLeaderboardAroundCurrentUserRequestCallback_t3017392292 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCurrentUserRequest_t545060687 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCurrentUserRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardAroundCurrentUserRequestCallback_EndInvoke_m2234556195 (GetLeaderboardAroundCurrentUserRequestCallback_t3017392292 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
