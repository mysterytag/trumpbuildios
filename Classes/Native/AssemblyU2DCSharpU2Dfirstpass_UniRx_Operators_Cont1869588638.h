﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>
struct ContinueWithObservable_2_t4007622692;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ContinueWithObservable`2/ContinueWith<System.Object,System.Object>
struct  ContinueWith_t1869588638  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.ContinueWithObservable`2<TSource,TResult> UniRx.Operators.ContinueWithObservable`2/ContinueWith::parent
	ContinueWithObservable_2_t4007622692 * ___parent_2;
	// UniRx.SerialDisposable UniRx.Operators.ContinueWithObservable`2/ContinueWith::serialDisposable
	SerialDisposable_t2547852742 * ___serialDisposable_3;
	// System.Boolean UniRx.Operators.ContinueWithObservable`2/ContinueWith::seenValue
	bool ___seenValue_4;
	// TSource UniRx.Operators.ContinueWithObservable`2/ContinueWith::lastValue
	Il2CppObject * ___lastValue_5;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ContinueWith_t1869588638, ___parent_2)); }
	inline ContinueWithObservable_2_t4007622692 * get_parent_2() const { return ___parent_2; }
	inline ContinueWithObservable_2_t4007622692 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ContinueWithObservable_2_t4007622692 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_serialDisposable_3() { return static_cast<int32_t>(offsetof(ContinueWith_t1869588638, ___serialDisposable_3)); }
	inline SerialDisposable_t2547852742 * get_serialDisposable_3() const { return ___serialDisposable_3; }
	inline SerialDisposable_t2547852742 ** get_address_of_serialDisposable_3() { return &___serialDisposable_3; }
	inline void set_serialDisposable_3(SerialDisposable_t2547852742 * value)
	{
		___serialDisposable_3 = value;
		Il2CppCodeGenWriteBarrier(&___serialDisposable_3, value);
	}

	inline static int32_t get_offset_of_seenValue_4() { return static_cast<int32_t>(offsetof(ContinueWith_t1869588638, ___seenValue_4)); }
	inline bool get_seenValue_4() const { return ___seenValue_4; }
	inline bool* get_address_of_seenValue_4() { return &___seenValue_4; }
	inline void set_seenValue_4(bool value)
	{
		___seenValue_4 = value;
	}

	inline static int32_t get_offset_of_lastValue_5() { return static_cast<int32_t>(offsetof(ContinueWith_t1869588638, ___lastValue_5)); }
	inline Il2CppObject * get_lastValue_5() const { return ___lastValue_5; }
	inline Il2CppObject ** get_address_of_lastValue_5() { return &___lastValue_5; }
	inline void set_lastValue_5(Il2CppObject * value)
	{
		___lastValue_5 = value;
		Il2CppCodeGenWriteBarrier(&___lastValue_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
