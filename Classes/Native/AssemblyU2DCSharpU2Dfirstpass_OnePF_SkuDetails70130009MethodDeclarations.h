﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.SkuDetails
struct SkuDetails_t70130009;
// System.String
struct String_t;
// OnePF.JSON
struct JSON_t2649481148;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON2649481148.h"

// System.Void OnePF.SkuDetails::.ctor(System.String)
extern "C"  void SkuDetails__ctor_m3647762378 (SkuDetails_t70130009 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::.ctor(OnePF.JSON)
extern "C"  void SkuDetails__ctor_m108523998 (SkuDetails_t70130009 * __this, JSON_t2649481148 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_ItemType()
extern "C"  String_t* SkuDetails_get_ItemType_m1055123045 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_ItemType(System.String)
extern "C"  void SkuDetails_set_ItemType_m2364950540 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_Sku()
extern "C"  String_t* SkuDetails_get_Sku_m1697287367 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_Sku(System.String)
extern "C"  void SkuDetails_set_Sku_m1064891628 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_Type()
extern "C"  String_t* SkuDetails_get_Type_m1117768946 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_Type(System.String)
extern "C"  void SkuDetails_set_Type_m485764639 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_Price()
extern "C"  String_t* SkuDetails_get_Price_m829181875 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_Price(System.String)
extern "C"  void SkuDetails_set_Price_m3037446464 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_Title()
extern "C"  String_t* SkuDetails_get_Title_m4131961090 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_Title(System.String)
extern "C"  void SkuDetails_set_Title_m1624453201 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_Description()
extern "C"  String_t* SkuDetails_get_Description_m400826086 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_Description(System.String)
extern "C"  void SkuDetails_set_Description_m810036269 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_Json()
extern "C"  String_t* SkuDetails_get_Json_m825915168 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_Json(System.String)
extern "C"  void SkuDetails_set_Json_m374865585 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_CurrencyCode()
extern "C"  String_t* SkuDetails_get_CurrencyCode_m2608246614 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_CurrencyCode(System.String)
extern "C"  void SkuDetails_set_CurrencyCode_m2155024187 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::get_PriceValue()
extern "C"  String_t* SkuDetails_get_PriceValue_m3111594656 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::set_PriceValue(System.String)
extern "C"  void SkuDetails_set_PriceValue_m2641060849 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.SkuDetails::ParseFromJson()
extern "C"  void SkuDetails_ParseFromJson_m3619400859 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.SkuDetails::ToString()
extern "C"  String_t* SkuDetails_ToString_m285465243 (SkuDetails_t70130009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
