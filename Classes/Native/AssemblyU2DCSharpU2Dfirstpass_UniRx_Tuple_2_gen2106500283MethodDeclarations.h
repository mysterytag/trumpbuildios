﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"

// System.Void UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m771351398_gshared (Tuple_2_t2106500283 * __this, Il2CppObject * ___item10, NetworkMessageInfo_t2574344884  ___item21, const MethodInfo* method);
#define Tuple_2__ctor_m771351398(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t2106500283 *, Il2CppObject *, NetworkMessageInfo_t2574344884 , const MethodInfo*))Tuple_2__ctor_m771351398_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_2_System_IComparable_CompareTo_m3405317199_gshared (Tuple_2_t2106500283 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_System_IComparable_CompareTo_m3405317199(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t2106500283 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m3405317199_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_2_UniRx_IStructuralComparable_CompareTo_m1856326113_gshared (Tuple_2_t2106500283 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m1856326113(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t2106500283 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m1856326113_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_2_UniRx_IStructuralEquatable_Equals_m1941656536_gshared (Tuple_2_t2106500283 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m1941656536(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t2106500283 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m1941656536_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m4285953250_gshared (Tuple_2_t2106500283 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m4285953250(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t2106500283 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m4285953250_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_2_UniRx_ITuple_ToString_m625326147_gshared (Tuple_2_t2106500283 * __this, const MethodInfo* method);
#define Tuple_2_UniRx_ITuple_ToString_m625326147(__this, method) ((  String_t* (*) (Tuple_2_t2106500283 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m625326147_gshared)(__this, method)
// T1 UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::get_Item1()
extern "C"  Il2CppObject * Tuple_2_get_Item1_m1367433754_gshared (Tuple_2_t2106500283 * __this, const MethodInfo* method);
#define Tuple_2_get_Item1_m1367433754(__this, method) ((  Il2CppObject * (*) (Tuple_2_t2106500283 *, const MethodInfo*))Tuple_2_get_Item1_m1367433754_gshared)(__this, method)
// T2 UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::get_Item2()
extern "C"  NetworkMessageInfo_t2574344884  Tuple_2_get_Item2_m1811046138_gshared (Tuple_2_t2106500283 * __this, const MethodInfo* method);
#define Tuple_2_get_Item2_m1811046138(__this, method) ((  NetworkMessageInfo_t2574344884  (*) (Tuple_2_t2106500283 *, const MethodInfo*))Tuple_2_get_Item2_m1811046138_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m444931724_gshared (Tuple_2_t2106500283 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_Equals_m444931724(__this, ___obj0, method) ((  bool (*) (Tuple_2_t2106500283 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m444931724_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m2471865380_gshared (Tuple_2_t2106500283 * __this, const MethodInfo* method);
#define Tuple_2_GetHashCode_m2471865380(__this, method) ((  int32_t (*) (Tuple_2_t2106500283 *, const MethodInfo*))Tuple_2_GetHashCode_m2471865380_gshared)(__this, method)
// System.String UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::ToString()
extern "C"  String_t* Tuple_2_ToString_m1660547598_gshared (Tuple_2_t2106500283 * __this, const MethodInfo* method);
#define Tuple_2_ToString_m1660547598(__this, method) ((  String_t* (*) (Tuple_2_t2106500283 *, const MethodInfo*))Tuple_2_ToString_m1660547598_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>::Equals(UniRx.Tuple`2<T1,T2>)
extern "C"  bool Tuple_2_Equals_m3962177501_gshared (Tuple_2_t2106500283 * __this, Tuple_2_t2106500283  ___other0, const MethodInfo* method);
#define Tuple_2_Equals_m3962177501(__this, ___other0, method) ((  bool (*) (Tuple_2_t2106500283 *, Tuple_2_t2106500283 , const MethodInfo*))Tuple_2_Equals_m3962177501_gshared)(__this, ___other0, method)
