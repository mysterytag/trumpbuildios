﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DoubleCell
struct DoubleCell_t2771715315;
// IStream`1<System.Double>
struct IStream_1_t1087207605;

#include "codegen/il2cpp-codegen.h"

// System.Void DoubleCell::.ctor()
extern "C"  void DoubleCell__ctor_m162624776 (DoubleCell_t2771715315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoubleCell::.ctor(System.Double)
extern "C"  void DoubleCell__ctor_m512095034 (DoubleCell_t2771715315 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IStream`1<System.Double> DoubleCell::get_diff()
extern "C"  Il2CppObject* DoubleCell_get_diff_m3202277207 (DoubleCell_t2771715315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double DoubleCell::get_value()
extern "C"  double DoubleCell_get_value_m2241461067 (DoubleCell_t2771715315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoubleCell::set_value(System.Double)
extern "C"  void DoubleCell_set_value_m2579153768 (DoubleCell_t2771715315 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
