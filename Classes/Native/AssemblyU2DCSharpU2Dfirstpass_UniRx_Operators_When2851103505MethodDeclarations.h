﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1/WhenAll_<System.Object>
struct WhenAll__t2851103505;
// System.Collections.Generic.IList`1<UniRx.IObservable`1<System.Object>>
struct IList_1_t2762397098;
// UniRx.IObserver`1<System.Object[]>
struct IObserver_1_t2223522676;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_<System.Object>::.ctor(System.Collections.Generic.IList`1<UniRx.IObservable`1<T>>,UniRx.IObserver`1<T[]>,System.IDisposable)
extern "C"  void WhenAll___ctor_m3798155158_gshared (WhenAll__t2851103505 * __this, Il2CppObject* ___sources0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define WhenAll___ctor_m3798155158(__this, ___sources0, ___observer1, ___cancel2, method) ((  void (*) (WhenAll__t2851103505 *, Il2CppObject*, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhenAll___ctor_m3798155158_gshared)(__this, ___sources0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.WhenAllObservable`1/WhenAll_<System.Object>::Run()
extern "C"  Il2CppObject * WhenAll__Run_m570864034_gshared (WhenAll__t2851103505 * __this, const MethodInfo* method);
#define WhenAll__Run_m570864034(__this, method) ((  Il2CppObject * (*) (WhenAll__t2851103505 *, const MethodInfo*))WhenAll__Run_m570864034_gshared)(__this, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_<System.Object>::OnNext(T[])
extern "C"  void WhenAll__OnNext_m147029572_gshared (WhenAll__t2851103505 * __this, ObjectU5BU5D_t11523773* ___value0, const MethodInfo* method);
#define WhenAll__OnNext_m147029572(__this, ___value0, method) ((  void (*) (WhenAll__t2851103505 *, ObjectU5BU5D_t11523773*, const MethodInfo*))WhenAll__OnNext_m147029572_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_<System.Object>::OnError(System.Exception)
extern "C"  void WhenAll__OnError_m2819381173_gshared (WhenAll__t2851103505 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WhenAll__OnError_m2819381173(__this, ___error0, method) ((  void (*) (WhenAll__t2851103505 *, Exception_t1967233988 *, const MethodInfo*))WhenAll__OnError_m2819381173_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_<System.Object>::OnCompleted()
extern "C"  void WhenAll__OnCompleted_m939602568_gshared (WhenAll__t2851103505 * __this, const MethodInfo* method);
#define WhenAll__OnCompleted_m939602568(__this, method) ((  void (*) (WhenAll__t2851103505 *, const MethodInfo*))WhenAll__OnCompleted_m939602568_gshared)(__this, method)
