﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithPlayFabRequestCallback
struct LoginWithPlayFabRequestCallback_t102383312;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithPlayFabRequest
struct LoginWithPlayFabRequest_t402940283;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithPla402940283.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithPlayFabRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithPlayFabRequestCallback__ctor_m1259119455 (LoginWithPlayFabRequestCallback_t102383312 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithPlayFabRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithPlayFabRequest,System.Object)
extern "C"  void LoginWithPlayFabRequestCallback_Invoke_m664226297 (LoginWithPlayFabRequestCallback_t102383312 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithPlayFabRequest_t402940283 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithPlayFabRequestCallback_t102383312(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithPlayFabRequest_t402940283 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithPlayFabRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithPlayFabRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithPlayFabRequestCallback_BeginInvoke_m279738590 (LoginWithPlayFabRequestCallback_t102383312 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithPlayFabRequest_t402940283 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithPlayFabRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithPlayFabRequestCallback_EndInvoke_m79600879 (LoginWithPlayFabRequestCallback_t102383312 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
