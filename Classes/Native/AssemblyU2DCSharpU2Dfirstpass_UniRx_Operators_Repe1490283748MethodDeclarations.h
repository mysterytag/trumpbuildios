﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>
struct RepeatUntil_t1490283748;
// UniRx.Operators.RepeatUntilObservable`1<System.Object>
struct RepeatUntilObservable_1_t679086845;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::.ctor(UniRx.Operators.RepeatUntilObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void RepeatUntil__ctor_m2650949893_gshared (RepeatUntil_t1490283748 * __this, RepeatUntilObservable_1_t679086845 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define RepeatUntil__ctor_m2650949893(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (RepeatUntil_t1490283748 *, RepeatUntilObservable_1_t679086845 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))RepeatUntil__ctor_m2650949893_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::Run()
extern "C"  Il2CppObject * RepeatUntil_Run_m2269257525_gshared (RepeatUntil_t1490283748 * __this, const MethodInfo* method);
#define RepeatUntil_Run_m2269257525(__this, method) ((  Il2CppObject * (*) (RepeatUntil_t1490283748 *, const MethodInfo*))RepeatUntil_Run_m2269257525_gshared)(__this, method)
// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::RecursiveRun(System.Action)
extern "C"  void RepeatUntil_RecursiveRun_m608789575_gshared (RepeatUntil_t1490283748 * __this, Action_t437523947 * ___self0, const MethodInfo* method);
#define RepeatUntil_RecursiveRun_m608789575(__this, ___self0, method) ((  void (*) (RepeatUntil_t1490283748 *, Action_t437523947 *, const MethodInfo*))RepeatUntil_RecursiveRun_m608789575_gshared)(__this, ___self0, method)
// System.Collections.IEnumerator UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::SubscribeAfterEndOfFrame(UniRx.SingleAssignmentDisposable,UniRx.IObservable`1<T>,UniRx.IObserver`1<T>,UnityEngine.GameObject)
extern "C"  Il2CppObject * RepeatUntil_SubscribeAfterEndOfFrame_m675678398_gshared (Il2CppObject * __this /* static, unused */, SingleAssignmentDisposable_t2336378823 * ___d0, Il2CppObject* ___source1, Il2CppObject* ___observer2, GameObject_t4012695102 * ___lifeTimeChecker3, const MethodInfo* method);
#define RepeatUntil_SubscribeAfterEndOfFrame_m675678398(__this /* static, unused */, ___d0, ___source1, ___observer2, ___lifeTimeChecker3, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, SingleAssignmentDisposable_t2336378823 *, Il2CppObject*, Il2CppObject*, GameObject_t4012695102 *, const MethodInfo*))RepeatUntil_SubscribeAfterEndOfFrame_m675678398_gshared)(__this /* static, unused */, ___d0, ___source1, ___observer2, ___lifeTimeChecker3, method)
// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::OnNext(T)
extern "C"  void RepeatUntil_OnNext_m3051887375_gshared (RepeatUntil_t1490283748 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define RepeatUntil_OnNext_m3051887375(__this, ___value0, method) ((  void (*) (RepeatUntil_t1490283748 *, Il2CppObject *, const MethodInfo*))RepeatUntil_OnNext_m3051887375_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::OnError(System.Exception)
extern "C"  void RepeatUntil_OnError_m1971992606_gshared (RepeatUntil_t1490283748 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RepeatUntil_OnError_m1971992606(__this, ___error0, method) ((  void (*) (RepeatUntil_t1490283748 *, Exception_t1967233988 *, const MethodInfo*))RepeatUntil_OnError_m1971992606_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::OnCompleted()
extern "C"  void RepeatUntil_OnCompleted_m1413851249_gshared (RepeatUntil_t1490283748 * __this, const MethodInfo* method);
#define RepeatUntil_OnCompleted_m1413851249(__this, method) ((  void (*) (RepeatUntil_t1490283748 *, const MethodInfo*))RepeatUntil_OnCompleted_m1413851249_gshared)(__this, method)
// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::<Run>m__C0(UniRx.Unit)
extern "C"  void RepeatUntil_U3CRunU3Em__C0_m4198459340_gshared (RepeatUntil_t1490283748 * __this, Unit_t2558286038  ____0, const MethodInfo* method);
#define RepeatUntil_U3CRunU3Em__C0_m4198459340(__this, ____0, method) ((  void (*) (RepeatUntil_t1490283748 *, Unit_t2558286038 , const MethodInfo*))RepeatUntil_U3CRunU3Em__C0_m4198459340_gshared)(__this, ____0, method)
// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>::<Run>m__C1()
extern "C"  void RepeatUntil_U3CRunU3Em__C1_m2734454825_gshared (RepeatUntil_t1490283748 * __this, const MethodInfo* method);
#define RepeatUntil_U3CRunU3Em__C1_m2734454825(__this, method) ((  void (*) (RepeatUntil_t1490283748 *, const MethodInfo*))RepeatUntil_U3CRunU3Em__C1_m2734454825_gshared)(__this, method)
