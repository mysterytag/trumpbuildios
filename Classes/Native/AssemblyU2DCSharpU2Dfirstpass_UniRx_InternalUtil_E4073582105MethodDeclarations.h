﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct EmptyObserver_1_t4073582105;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2923199000_gshared (EmptyObserver_1_t4073582105 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2923199000(__this, method) ((  void (*) (EmptyObserver_1_t4073582105 *, const MethodInfo*))EmptyObserver_1__ctor_m2923199000_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m4237726869_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m4237726869(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m4237726869_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m804895650_gshared (EmptyObserver_1_t4073582105 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m804895650(__this, method) ((  void (*) (EmptyObserver_1_t4073582105 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m804895650_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3285060047_gshared (EmptyObserver_1_t4073582105 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3285060047(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t4073582105 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3285060047_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3116330688_gshared (EmptyObserver_1_t4073582105 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3116330688(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t4073582105 *, Tuple_2_t369261819 , const MethodInfo*))EmptyObserver_1_OnNext_m3116330688_gshared)(__this, ___value0, method)
