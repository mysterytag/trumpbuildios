﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<WrapEnumerator>c__IteratorF
struct  U3CWrapEnumeratorU3Ec__IteratorF_t3900889545  : public Il2CppObject
{
public:
	// System.Boolean UniRx.Observable/<WrapEnumerator>c__IteratorF::<hasNext>__0
	bool ___U3ChasNextU3E__0_0;
	// System.Boolean UniRx.Observable/<WrapEnumerator>c__IteratorF::<raisedError>__1
	bool ___U3CraisedErrorU3E__1_1;
	// System.Collections.IEnumerator UniRx.Observable/<WrapEnumerator>c__IteratorF::enumerator
	Il2CppObject * ___enumerator_2;
	// UniRx.IObserver`1<UniRx.Unit> UniRx.Observable/<WrapEnumerator>c__IteratorF::observer
	Il2CppObject* ___observer_3;
	// System.Exception UniRx.Observable/<WrapEnumerator>c__IteratorF::<ex>__2
	Exception_t1967233988 * ___U3CexU3E__2_4;
	// System.IDisposable UniRx.Observable/<WrapEnumerator>c__IteratorF::<d>__3
	Il2CppObject * ___U3CdU3E__3_5;
	// System.Boolean UniRx.Observable/<WrapEnumerator>c__IteratorF::publishEveryYield
	bool ___publishEveryYield_6;
	// System.IDisposable UniRx.Observable/<WrapEnumerator>c__IteratorF::<d>__4
	Il2CppObject * ___U3CdU3E__4_7;
	// UniRx.CancellationToken UniRx.Observable/<WrapEnumerator>c__IteratorF::cancellationToken
	CancellationToken_t1439151560  ___cancellationToken_8;
	// System.IDisposable UniRx.Observable/<WrapEnumerator>c__IteratorF::<d>__5
	Il2CppObject * ___U3CdU3E__5_9;
	// System.Int32 UniRx.Observable/<WrapEnumerator>c__IteratorF::$PC
	int32_t ___U24PC_10;
	// System.Object UniRx.Observable/<WrapEnumerator>c__IteratorF::$current
	Il2CppObject * ___U24current_11;
	// System.Collections.IEnumerator UniRx.Observable/<WrapEnumerator>c__IteratorF::<$>enumerator
	Il2CppObject * ___U3CU24U3Eenumerator_12;
	// UniRx.IObserver`1<UniRx.Unit> UniRx.Observable/<WrapEnumerator>c__IteratorF::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_13;
	// System.Boolean UniRx.Observable/<WrapEnumerator>c__IteratorF::<$>publishEveryYield
	bool ___U3CU24U3EpublishEveryYield_14;
	// UniRx.CancellationToken UniRx.Observable/<WrapEnumerator>c__IteratorF::<$>cancellationToken
	CancellationToken_t1439151560  ___U3CU24U3EcancellationToken_15;

public:
	inline static int32_t get_offset_of_U3ChasNextU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3ChasNextU3E__0_0)); }
	inline bool get_U3ChasNextU3E__0_0() const { return ___U3ChasNextU3E__0_0; }
	inline bool* get_address_of_U3ChasNextU3E__0_0() { return &___U3ChasNextU3E__0_0; }
	inline void set_U3ChasNextU3E__0_0(bool value)
	{
		___U3ChasNextU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CraisedErrorU3E__1_1() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CraisedErrorU3E__1_1)); }
	inline bool get_U3CraisedErrorU3E__1_1() const { return ___U3CraisedErrorU3E__1_1; }
	inline bool* get_address_of_U3CraisedErrorU3E__1_1() { return &___U3CraisedErrorU3E__1_1; }
	inline void set_U3CraisedErrorU3E__1_1(bool value)
	{
		___U3CraisedErrorU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_enumerator_2() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___enumerator_2)); }
	inline Il2CppObject * get_enumerator_2() const { return ___enumerator_2; }
	inline Il2CppObject ** get_address_of_enumerator_2() { return &___enumerator_2; }
	inline void set_enumerator_2(Il2CppObject * value)
	{
		___enumerator_2 = value;
		Il2CppCodeGenWriteBarrier(&___enumerator_2, value);
	}

	inline static int32_t get_offset_of_observer_3() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___observer_3)); }
	inline Il2CppObject* get_observer_3() const { return ___observer_3; }
	inline Il2CppObject** get_address_of_observer_3() { return &___observer_3; }
	inline void set_observer_3(Il2CppObject* value)
	{
		___observer_3 = value;
		Il2CppCodeGenWriteBarrier(&___observer_3, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__2_4() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CexU3E__2_4)); }
	inline Exception_t1967233988 * get_U3CexU3E__2_4() const { return ___U3CexU3E__2_4; }
	inline Exception_t1967233988 ** get_address_of_U3CexU3E__2_4() { return &___U3CexU3E__2_4; }
	inline void set_U3CexU3E__2_4(Exception_t1967233988 * value)
	{
		___U3CexU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CdU3E__3_5() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CdU3E__3_5)); }
	inline Il2CppObject * get_U3CdU3E__3_5() const { return ___U3CdU3E__3_5; }
	inline Il2CppObject ** get_address_of_U3CdU3E__3_5() { return &___U3CdU3E__3_5; }
	inline void set_U3CdU3E__3_5(Il2CppObject * value)
	{
		___U3CdU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__3_5, value);
	}

	inline static int32_t get_offset_of_publishEveryYield_6() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___publishEveryYield_6)); }
	inline bool get_publishEveryYield_6() const { return ___publishEveryYield_6; }
	inline bool* get_address_of_publishEveryYield_6() { return &___publishEveryYield_6; }
	inline void set_publishEveryYield_6(bool value)
	{
		___publishEveryYield_6 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E__4_7() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CdU3E__4_7)); }
	inline Il2CppObject * get_U3CdU3E__4_7() const { return ___U3CdU3E__4_7; }
	inline Il2CppObject ** get_address_of_U3CdU3E__4_7() { return &___U3CdU3E__4_7; }
	inline void set_U3CdU3E__4_7(Il2CppObject * value)
	{
		___U3CdU3E__4_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__4_7, value);
	}

	inline static int32_t get_offset_of_cancellationToken_8() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___cancellationToken_8)); }
	inline CancellationToken_t1439151560  get_cancellationToken_8() const { return ___cancellationToken_8; }
	inline CancellationToken_t1439151560 * get_address_of_cancellationToken_8() { return &___cancellationToken_8; }
	inline void set_cancellationToken_8(CancellationToken_t1439151560  value)
	{
		___cancellationToken_8 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E__5_9() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CdU3E__5_9)); }
	inline Il2CppObject * get_U3CdU3E__5_9() const { return ___U3CdU3E__5_9; }
	inline Il2CppObject ** get_address_of_U3CdU3E__5_9() { return &___U3CdU3E__5_9; }
	inline void set_U3CdU3E__5_9(Il2CppObject * value)
	{
		___U3CdU3E__5_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__5_9, value);
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eenumerator_12() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CU24U3Eenumerator_12)); }
	inline Il2CppObject * get_U3CU24U3Eenumerator_12() const { return ___U3CU24U3Eenumerator_12; }
	inline Il2CppObject ** get_address_of_U3CU24U3Eenumerator_12() { return &___U3CU24U3Eenumerator_12; }
	inline void set_U3CU24U3Eenumerator_12(Il2CppObject * value)
	{
		___U3CU24U3Eenumerator_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eenumerator_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_13() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CU24U3Eobserver_13)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_13() const { return ___U3CU24U3Eobserver_13; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_13() { return &___U3CU24U3Eobserver_13; }
	inline void set_U3CU24U3Eobserver_13(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EpublishEveryYield_14() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CU24U3EpublishEveryYield_14)); }
	inline bool get_U3CU24U3EpublishEveryYield_14() const { return ___U3CU24U3EpublishEveryYield_14; }
	inline bool* get_address_of_U3CU24U3EpublishEveryYield_14() { return &___U3CU24U3EpublishEveryYield_14; }
	inline void set_U3CU24U3EpublishEveryYield_14(bool value)
	{
		___U3CU24U3EpublishEveryYield_14 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EcancellationToken_15() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545, ___U3CU24U3EcancellationToken_15)); }
	inline CancellationToken_t1439151560  get_U3CU24U3EcancellationToken_15() const { return ___U3CU24U3EcancellationToken_15; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3EcancellationToken_15() { return &___U3CU24U3EcancellationToken_15; }
	inline void set_U3CU24U3EcancellationToken_15(CancellationToken_t1439151560  value)
	{
		___U3CU24U3EcancellationToken_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
