﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// System.Func`1<UniRx.Unit>
struct Func_1_t3701067285;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample10_MainThreadDispatcher
struct  Sample10_MainThreadDispatcher_t1609111451  : public Il2CppObject
{
public:

public:
};

struct Sample10_MainThreadDispatcher_t1609111451_StaticFields
{
public:
	// System.Action`1<System.Object> UniRx.Examples.Sample10_MainThreadDispatcher::<>f__am$cache0
	Action_1_t985559125 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`1<System.Int64> UniRx.Examples.Sample10_MainThreadDispatcher::<>f__am$cache1
	Action_1_t2995867587 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`1<UniRx.Unit> UniRx.Examples.Sample10_MainThreadDispatcher::<>f__am$cache2
	Func_1_t3701067285 * ___U3CU3Ef__amU24cache2_2;
	// System.Action`1<UniRx.Unit> UniRx.Examples.Sample10_MainThreadDispatcher::<>f__am$cache3
	Action_1_t2706738743 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(Sample10_MainThreadDispatcher_t1609111451_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_1_t985559125 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_1_t985559125 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_1_t985559125 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(Sample10_MainThreadDispatcher_t1609111451_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_1_t2995867587 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_1_t2995867587 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_1_t2995867587 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Sample10_MainThreadDispatcher_t1609111451_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_1_t3701067285 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_1_t3701067285 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_1_t3701067285 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Sample10_MainThreadDispatcher_t1609111451_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Action_1_t2706738743 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Action_1_t2706738743 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Action_1_t2706738743 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
