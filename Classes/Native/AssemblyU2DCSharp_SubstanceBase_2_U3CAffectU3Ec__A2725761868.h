﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SubstanceBase`2/Intrusion<System.Single,System.Object>
struct Intrusion_t4252761119;
// SubstanceBase`2<System.Single,System.Object>
struct SubstanceBase_2_t1748143363;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Object>
struct  U3CAffectU3Ec__AnonStorey143_t2725761868  : public Il2CppObject
{
public:
	// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2/<Affect>c__AnonStorey143::intrusion
	Intrusion_t4252761119 * ___intrusion_0;
	// SubstanceBase`2<T,InfT> SubstanceBase`2/<Affect>c__AnonStorey143::<>f__this
	SubstanceBase_2_t1748143363 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_intrusion_0() { return static_cast<int32_t>(offsetof(U3CAffectU3Ec__AnonStorey143_t2725761868, ___intrusion_0)); }
	inline Intrusion_t4252761119 * get_intrusion_0() const { return ___intrusion_0; }
	inline Intrusion_t4252761119 ** get_address_of_intrusion_0() { return &___intrusion_0; }
	inline void set_intrusion_0(Intrusion_t4252761119 * value)
	{
		___intrusion_0 = value;
		Il2CppCodeGenWriteBarrier(&___intrusion_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CAffectU3Ec__AnonStorey143_t2725761868, ___U3CU3Ef__this_1)); }
	inline SubstanceBase_2_t1748143363 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline SubstanceBase_2_t1748143363 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(SubstanceBase_2_t1748143363 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
