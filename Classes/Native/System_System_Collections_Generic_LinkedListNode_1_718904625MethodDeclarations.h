﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_1_539097040MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m4004983729(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t718904625 *, LinkedList_1_t2757043551 *, TaskInfo_t1016914005 *, const MethodInfo*))LinkedListNode_1__ctor_m648136130_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m2868941521(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t718904625 *, LinkedList_1_t2757043551 *, TaskInfo_t1016914005 *, LinkedListNode_1_t718904625 *, LinkedListNode_1_t718904625 *, const MethodInfo*))LinkedListNode_1__ctor_m448391458_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Detach()
#define LinkedListNode_1_Detach_m2986690767(__this, method) ((  void (*) (LinkedListNode_1_t718904625 *, const MethodInfo*))LinkedListNode_1_Detach_m3406254942_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_List()
#define LinkedListNode_1_get_List_m1851805395(__this, method) ((  LinkedList_1_t2757043551 * (*) (LinkedListNode_1_t718904625 *, const MethodInfo*))LinkedListNode_1_get_List_m3467110818_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_Next()
#define LinkedListNode_1_get_Next_m20035594(__this, method) ((  LinkedListNode_1_t718904625 * (*) (LinkedListNode_1_t718904625 *, const MethodInfo*))LinkedListNode_1_get_Next_m1427618777_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_Previous()
#define LinkedListNode_1_get_Previous_m3108084750(__this, method) ((  LinkedListNode_1_t718904625 * (*) (LinkedListNode_1_t718904625 *, const MethodInfo*))LinkedListNode_1_get_Previous_m3755155549_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_Value()
#define LinkedListNode_1_get_Value_m2405008335(__this, method) ((  TaskInfo_t1016914005 * (*) (LinkedListNode_1_t718904625 *, const MethodInfo*))LinkedListNode_1_get_Value_m702633824_gshared)(__this, method)
