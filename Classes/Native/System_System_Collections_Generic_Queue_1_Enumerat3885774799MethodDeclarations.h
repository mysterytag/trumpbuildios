﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>
struct Queue_1_t2416153082;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3885774799.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.TimeInterval`1<System.Object>>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m1328452124_gshared (Enumerator_t3885774799 * __this, Queue_1_t2416153082 * ___q0, const MethodInfo* method);
#define Enumerator__ctor_m1328452124(__this, ___q0, method) ((  void (*) (Enumerator_t3885774799 *, Queue_1_t2416153082 *, const MethodInfo*))Enumerator__ctor_m1328452124_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.TimeInterval`1<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2109776087_gshared (Enumerator_t3885774799 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2109776087(__this, method) ((  void (*) (Enumerator_t3885774799 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2109776087_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<UniRx.TimeInterval`1<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2354635917_gshared (Enumerator_t3885774799 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2354635917(__this, method) ((  Il2CppObject * (*) (Enumerator_t3885774799 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2354635917_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.TimeInterval`1<System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m789586752_gshared (Enumerator_t3885774799 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m789586752(__this, method) ((  void (*) (Enumerator_t3885774799 *, const MethodInfo*))Enumerator_Dispose_m789586752_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UniRx.TimeInterval`1<System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m296610971_gshared (Enumerator_t3885774799 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m296610971(__this, method) ((  bool (*) (Enumerator_t3885774799 *, const MethodInfo*))Enumerator_MoveNext_m296610971_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<UniRx.TimeInterval`1<System.Object>>::get_Current()
extern "C"  TimeInterval_1_t708065542  Enumerator_get_Current_m772945938_gshared (Enumerator_t3885774799 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m772945938(__this, method) ((  TimeInterval_1_t708065542  (*) (Enumerator_t3885774799 *, const MethodInfo*))Enumerator_get_Current_m772945938_gshared)(__this, method)
