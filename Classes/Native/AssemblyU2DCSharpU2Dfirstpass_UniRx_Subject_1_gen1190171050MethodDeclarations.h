﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::.ctor()
#define Subject_1__ctor_m2793565974(__this, method) ((  void (*) (Subject_1_t1190171050 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::get_HasObservers()
#define Subject_1_get_HasObservers_m1737613054(__this, method) ((  bool (*) (Subject_1_t1190171050 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::OnCompleted()
#define Subject_1_OnCompleted_m224885280(__this, method) ((  void (*) (Subject_1_t1190171050 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::OnError(System.Exception)
#define Subject_1_OnError_m2299263821(__this, ___error0, method) ((  void (*) (Subject_1_t1190171050 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::OnNext(T)
#define Subject_1_OnNext_m3093044286(__this, ___value0, method) ((  void (*) (Subject_1_t1190171050 *, BaseEventData_t3547103726 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m3652906987(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1190171050 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::Dispose()
#define Subject_1_Dispose_m160624083(__this, method) ((  void (*) (Subject_1_t1190171050 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m4124775516(__this, method) ((  void (*) (Subject_1_t1190171050 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2853161077(__this, method) ((  bool (*) (Subject_1_t1190171050 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
