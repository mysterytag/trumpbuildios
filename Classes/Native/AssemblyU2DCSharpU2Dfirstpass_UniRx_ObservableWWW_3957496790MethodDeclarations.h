﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey85
struct U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Byte[]>
struct IObserver_1_t2270505063;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey85::.ctor()
extern "C"  void U3CPostAndGetBytesU3Ec__AnonStorey85__ctor_m78346854 (U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey85::<>m__B1(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CPostAndGetBytesU3Ec__AnonStorey85_U3CU3Em__B1_m247104291 (U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
