﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<ResolveAll>c__AnonStorey189
struct  U3CResolveAllU3Ec__AnonStorey189_t628370168  : public Il2CppObject
{
public:
	// Zenject.InjectContext Zenject.DiContainer/<ResolveAll>c__AnonStorey189::context
	InjectContext_t3456483891 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(U3CResolveAllU3Ec__AnonStorey189_t628370168, ___context_0)); }
	inline InjectContext_t3456483891 * get_context_0() const { return ___context_0; }
	inline InjectContext_t3456483891 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(InjectContext_t3456483891 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier(&___context_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
