﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.NetworkConnectionError>
struct Subject_1_t2961840613;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.NetworkConnectionError>
struct IObserver_1_t3235804896;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"

// System.Void UniRx.Subject`1<UnityEngine.NetworkConnectionError>::.ctor()
extern "C"  void Subject_1__ctor_m1506795235_gshared (Subject_1_t2961840613 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1506795235(__this, method) ((  void (*) (Subject_1_t2961840613 *, const MethodInfo*))Subject_1__ctor_m1506795235_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.NetworkConnectionError>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m4278882065_gshared (Subject_1_t2961840613 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m4278882065(__this, method) ((  bool (*) (Subject_1_t2961840613 *, const MethodInfo*))Subject_1_get_HasObservers_m4278882065_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkConnectionError>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m2198019117_gshared (Subject_1_t2961840613 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m2198019117(__this, method) ((  void (*) (Subject_1_t2961840613 *, const MethodInfo*))Subject_1_OnCompleted_m2198019117_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkConnectionError>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m1961619930_gshared (Subject_1_t2961840613 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m1961619930(__this, ___error0, method) ((  void (*) (Subject_1_t2961840613 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1961619930_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkConnectionError>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3456945355_gshared (Subject_1_t2961840613 * __this, int32_t ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m3456945355(__this, ___value0, method) ((  void (*) (Subject_1_t2961840613 *, int32_t, const MethodInfo*))Subject_1_OnNext_m3456945355_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.NetworkConnectionError>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2630180408_gshared (Subject_1_t2961840613 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2630180408(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2961840613 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2630180408_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkConnectionError>::Dispose()
extern "C"  void Subject_1_Dispose_m524525152_gshared (Subject_1_t2961840613 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m524525152(__this, method) ((  void (*) (Subject_1_t2961840613 *, const MethodInfo*))Subject_1_Dispose_m524525152_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkConnectionError>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m294447081_gshared (Subject_1_t2961840613 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m294447081(__this, method) ((  void (*) (Subject_1_t2961840613 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m294447081_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.NetworkConnectionError>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3126966728_gshared (Subject_1_t2961840613 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3126966728(__this, method) ((  bool (*) (Subject_1_t2961840613 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3126966728_gshared)(__this, method)
