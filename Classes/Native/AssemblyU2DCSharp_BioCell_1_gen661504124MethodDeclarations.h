﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioCell`1<System.Object>
struct BioCell_1_t661504124;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// IOrganism
struct IOrganism_t2580843579;
// IRoot
struct IRoot_t69970123;
// IOnceEmptyStream
struct IOnceEmptyStream_t172682851;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void BioCell`1<System.Object>::.ctor()
extern "C"  void BioCell_1__ctor_m27543930_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1__ctor_m27543930(__this, method) ((  void (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1__ctor_m27543930_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::.ctor(T)
extern "C"  void BioCell_1__ctor_m853863204_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define BioCell_1__ctor_m853863204(__this, ___val0, method) ((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))BioCell_1__ctor_m853863204_gshared)(__this, ___val0, method)
// System.Void BioCell`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void BioCell_1__ctor_m4206272443_gshared (BioCell_1_t661504124 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define BioCell_1__ctor_m4206272443(__this, ___info0, ___context1, method) ((  void (*) (BioCell_1_t661504124 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))BioCell_1__ctor_m4206272443_gshared)(__this, ___info0, ___context1, method)
// System.Void BioCell`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void BioCell_1_GetObjectData_m870710296_gshared (BioCell_1_t661504124 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define BioCell_1_GetObjectData_m870710296(__this, ___info0, ___context1, method) ((  void (*) (BioCell_1_t661504124 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))BioCell_1_GetObjectData_m870710296_gshared)(__this, ___info0, ___context1, method)
// IOrganism BioCell`1<System.Object>::get_carrier()
extern "C"  Il2CppObject * BioCell_1_get_carrier_m192118863_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_get_carrier_m192118863(__this, method) ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_get_carrier_m192118863_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::set_carrier(IOrganism)
extern "C"  void BioCell_1_set_carrier_m1896678372_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BioCell_1_set_carrier_m1896678372(__this, ___value0, method) ((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))BioCell_1_set_carrier_m1896678372_gshared)(__this, ___value0, method)
// IRoot BioCell`1<System.Object>::get_root()
extern "C"  Il2CppObject * BioCell_1_get_root_m2120150461_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_get_root_m2120150461(__this, method) ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_get_root_m2120150461_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::set_root(IRoot)
extern "C"  void BioCell_1_set_root_m3977040414_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BioCell_1_set_root_m3977040414(__this, ___value0, method) ((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))BioCell_1_set_root_m3977040414_gshared)(__this, ___value0, method)
// IOnceEmptyStream BioCell`1<System.Object>::get_fellAsleep()
extern "C"  Il2CppObject * BioCell_1_get_fellAsleep_m3267003874_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_get_fellAsleep_m3267003874(__this, method) ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_get_fellAsleep_m3267003874_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::DisposeWhenAsleep(System.IDisposable)
extern "C"  void BioCell_1_DisposeWhenAsleep_m2399898397_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___disposable0, const MethodInfo* method);
#define BioCell_1_DisposeWhenAsleep_m2399898397(__this, ___disposable0, method) ((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))BioCell_1_DisposeWhenAsleep_m2399898397_gshared)(__this, ___disposable0, method)
// System.Boolean BioCell`1<System.Object>::get_isAlive()
extern "C"  bool BioCell_1_get_isAlive_m2614499230_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_get_isAlive_m2614499230(__this, method) ((  bool (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_get_isAlive_m2614499230_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::Setup(IOrganism,IRoot)
extern "C"  void BioCell_1_Setup_m3936296995_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___c0, Il2CppObject * ___r1, const MethodInfo* method);
#define BioCell_1_Setup_m3936296995(__this, ___c0, ___r1, method) ((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))BioCell_1_Setup_m3936296995_gshared)(__this, ___c0, ___r1, method)
// System.Void BioCell`1<System.Object>::WakeUp()
extern "C"  void BioCell_1_WakeUp_m1546909161_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_WakeUp_m1546909161(__this, method) ((  void (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_WakeUp_m1546909161_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::Spread()
extern "C"  void BioCell_1_Spread_m3794005757_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_Spread_m3794005757(__this, method) ((  void (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_Spread_m3794005757_gshared)(__this, method)
// System.IDisposable BioCell`1<System.Object>::get_prevToSleepConnection()
extern "C"  Il2CppObject * BioCell_1_get_prevToSleepConnection_m477401155_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_get_prevToSleepConnection_m477401155(__this, method) ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_get_prevToSleepConnection_m477401155_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::set_prevToSleepConnection(System.IDisposable)
extern "C"  void BioCell_1_set_prevToSleepConnection_m3653585240_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BioCell_1_set_prevToSleepConnection_m3653585240(__this, ___value0, method) ((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))BioCell_1_set_prevToSleepConnection_m3653585240_gshared)(__this, ___value0, method)
// T BioCell`1<System.Object>::get_value()
extern "C"  Il2CppObject * BioCell_1_get_value_m266880097_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_get_value_m266880097(__this, method) ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_get_value_m266880097_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::set_value(T)
extern "C"  void BioCell_1_set_value_m2666559826_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BioCell_1_set_value_m2666559826(__this, ___value0, method) ((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))BioCell_1_set_value_m2666559826_gshared)(__this, ___value0, method)
// T BioCell`1<System.Object>::get_innerVal()
extern "C"  Il2CppObject * BioCell_1_get_innerVal_m3760006813_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_get_innerVal_m3760006813(__this, method) ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_get_innerVal_m3760006813_gshared)(__this, method)
// System.Void BioCell`1<System.Object>::Collect(System.IDisposable)
extern "C"  void BioCell_1_Collect_m1958937368_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___tail0, const MethodInfo* method);
#define BioCell_1_Collect_m1958937368(__this, ___tail0, method) ((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))BioCell_1_Collect_m1958937368_gshared)(__this, ___tail0, method)
// System.Void BioCell`1<System.Object>::ToSleep()
extern "C"  void BioCell_1_ToSleep_m856150772_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method);
#define BioCell_1_ToSleep_m856150772(__this, method) ((  void (*) (BioCell_1_t661504124 *, const MethodInfo*))BioCell_1_ToSleep_m856150772_gshared)(__this, method)
