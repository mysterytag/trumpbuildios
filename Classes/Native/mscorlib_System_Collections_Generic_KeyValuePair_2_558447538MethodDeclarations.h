﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_427065056MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m775244839(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t558447538 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m3133764025_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m1891477473(__this, method) ((  int32_t (*) (KeyValuePair_2_t558447538 *, const MethodInfo*))KeyValuePair_2_get_Key_m2796000783_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1327944098(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t558447538 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2230450384_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m879194721(__this, method) ((  String_t* (*) (KeyValuePair_2_t558447538 *, const MethodInfo*))KeyValuePair_2_get_Value_m2542701839_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3399255202(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t558447538 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m1258579152_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::ToString()
#define KeyValuePair_2_ToString_m516635328(__this, method) ((  String_t* (*) (KeyValuePair_2_t558447538 *, const MethodInfo*))KeyValuePair_2_ToString_m2787054162_gshared)(__this, method)
