﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StaticCell`1<System.Object>
struct StaticCell_1_t175558834;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void StaticCell`1<System.Object>::.ctor()
extern "C"  void StaticCell_1__ctor_m240399728_gshared (StaticCell_1_t175558834 * __this, const MethodInfo* method);
#define StaticCell_1__ctor_m240399728(__this, method) ((  void (*) (StaticCell_1_t175558834 *, const MethodInfo*))StaticCell_1__ctor_m240399728_gshared)(__this, method)
// System.Void StaticCell`1<System.Object>::.ctor(T)
extern "C"  void StaticCell_1__ctor_m3157425646_gshared (StaticCell_1_t175558834 * __this, Il2CppObject * ___initial0, const MethodInfo* method);
#define StaticCell_1__ctor_m3157425646(__this, ___initial0, method) ((  void (*) (StaticCell_1_t175558834 *, Il2CppObject *, const MethodInfo*))StaticCell_1__ctor_m3157425646_gshared)(__this, ___initial0, method)
// T StaticCell`1<System.Object>::get_value()
extern "C"  Il2CppObject * StaticCell_1_get_value_m2541444597_gshared (StaticCell_1_t175558834 * __this, const MethodInfo* method);
#define StaticCell_1_get_value_m2541444597(__this, method) ((  Il2CppObject * (*) (StaticCell_1_t175558834 *, const MethodInfo*))StaticCell_1_get_value_m2541444597_gshared)(__this, method)
// System.IDisposable StaticCell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * StaticCell_1_ListenUpdates_m3711080542_gshared (StaticCell_1_t175558834 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define StaticCell_1_ListenUpdates_m3711080542(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (StaticCell_1_t175558834 *, Action_1_t985559125 *, int32_t, const MethodInfo*))StaticCell_1_ListenUpdates_m3711080542_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable StaticCell`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * StaticCell_1_OnChanged_m3085514459_gshared (StaticCell_1_t175558834 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define StaticCell_1_OnChanged_m3085514459(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (StaticCell_1_t175558834 *, Action_t437523947 *, int32_t, const MethodInfo*))StaticCell_1_OnChanged_m3085514459_gshared)(__this, ___action0, ___p1, method)
// System.Object StaticCell`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * StaticCell_1_get_valueObject_m2609974602_gshared (StaticCell_1_t175558834 * __this, const MethodInfo* method);
#define StaticCell_1_get_valueObject_m2609974602(__this, method) ((  Il2CppObject * (*) (StaticCell_1_t175558834 *, const MethodInfo*))StaticCell_1_get_valueObject_m2609974602_gshared)(__this, method)
// System.IDisposable StaticCell`1<System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * StaticCell_1_Bind_m2516612132_gshared (StaticCell_1_t175558834 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method);
#define StaticCell_1_Bind_m2516612132(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (StaticCell_1_t175558834 *, Action_1_t985559125 *, int32_t, const MethodInfo*))StaticCell_1_Bind_m2516612132_gshared)(__this, ___action0, ___p1, method)
