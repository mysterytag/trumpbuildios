﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>
struct EmptyObserver_1_t1928537263;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3462131422_gshared (EmptyObserver_1_t1928537263 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m3462131422(__this, method) ((  void (*) (EmptyObserver_1_t1928537263 *, const MethodInfo*))EmptyObserver_1__ctor_m3462131422_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3764762767_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3764762767(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3764762767_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3116631016_gshared (EmptyObserver_1_t1928537263 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m3116631016(__this, method) ((  void (*) (EmptyObserver_1_t1928537263 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m3116631016_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4200725_gshared (EmptyObserver_1_t1928537263 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m4200725(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1928537263 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m4200725_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Timestamped`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1339345414_gshared (EmptyObserver_1_t1928537263 * __this, Timestamped_1_t2519184273  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m1339345414(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1928537263 *, Timestamped_1_t2519184273 , const MethodInfo*))EmptyObserver_1_OnNext_m1339345414_gshared)(__this, ___value0, method)
