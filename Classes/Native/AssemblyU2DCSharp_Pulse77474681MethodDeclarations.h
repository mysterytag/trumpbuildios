﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pulse
struct Pulse_t77474681;
// System.Action`1<System.Single>
struct Action_1_t1106661726;

#include "codegen/il2cpp-codegen.h"

// System.Void Pulse::.ctor(System.Single,System.Single,System.Single,System.Single,System.Action`1<System.Single>)
extern "C"  void Pulse__ctor_m2360243985 (Pulse_t77474681 * __this, float ___duration0, float ___inFrom1, float ___inTo2, float ___waveLength3, Action_1_t1106661726 * ___inAction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pulse::Update(System.Single)
extern "C"  void Pulse_Update_m2159852112 (Pulse_t77474681 * __this, float ___relativeTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pulse::Dispose()
extern "C"  void Pulse_Dispose_m33105359 (Pulse_t77474681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
