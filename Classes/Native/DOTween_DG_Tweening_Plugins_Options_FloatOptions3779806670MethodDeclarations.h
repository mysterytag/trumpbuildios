﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct FloatOptions_t3779806670;
struct FloatOptions_t3779806670_marshaled_pinvoke;

extern "C" void FloatOptions_t3779806670_marshal_pinvoke(const FloatOptions_t3779806670& unmarshaled, FloatOptions_t3779806670_marshaled_pinvoke& marshaled);
extern "C" void FloatOptions_t3779806670_marshal_pinvoke_back(const FloatOptions_t3779806670_marshaled_pinvoke& marshaled, FloatOptions_t3779806670& unmarshaled);
extern "C" void FloatOptions_t3779806670_marshal_pinvoke_cleanup(FloatOptions_t3779806670_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct FloatOptions_t3779806670;
struct FloatOptions_t3779806670_marshaled_com;

extern "C" void FloatOptions_t3779806670_marshal_com(const FloatOptions_t3779806670& unmarshaled, FloatOptions_t3779806670_marshaled_com& marshaled);
extern "C" void FloatOptions_t3779806670_marshal_com_back(const FloatOptions_t3779806670_marshaled_com& marshaled, FloatOptions_t3779806670& unmarshaled);
extern "C" void FloatOptions_t3779806670_marshal_com_cleanup(FloatOptions_t3779806670_marshaled_com& marshaled);
