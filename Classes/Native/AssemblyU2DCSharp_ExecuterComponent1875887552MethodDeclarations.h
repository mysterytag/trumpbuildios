﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExecuterComponent
struct ExecuterComponent_t1875887552;

#include "codegen/il2cpp-codegen.h"

// System.Void ExecuterComponent::.ctor()
extern "C"  void ExecuterComponent__ctor_m1007050667 (ExecuterComponent_t1875887552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExecuterComponent::Update()
extern "C"  void ExecuterComponent_Update_m2880661698 (ExecuterComponent_t1875887552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExecuterComponent::get_isAlive()
extern "C"  bool ExecuterComponent_get_isAlive_m3420419319 (ExecuterComponent_t1875887552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
