﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.Action
struct Action_t437523947;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1291133240;
struct WaitForSeconds_t1291133240_marshaled_pinvoke;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A
struct  U3CPeriodicActionU3Ec__Iterator1A_t3918433488  : public Il2CppObject
{
public:
	// System.TimeSpan UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::period
	TimeSpan_t763862892  ___period_0;
	// UniRx.ICancelable UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::cancellation
	Il2CppObject * ___cancellation_1;
	// System.Action UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::action
	Action_t437523947 * ___action_2;
	// System.Single UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::<seconds>__0
	float ___U3CsecondsU3E__0_3;
	// UnityEngine.WaitForSeconds UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::<yieldInstruction>__1
	WaitForSeconds_t1291133240 * ___U3CyieldInstructionU3E__1_4;
	// System.Int32 UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::$PC
	int32_t ___U24PC_5;
	// System.Object UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::$current
	Il2CppObject * ___U24current_6;
	// System.TimeSpan UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::<$>period
	TimeSpan_t763862892  ___U3CU24U3Eperiod_7;
	// UniRx.ICancelable UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::<$>cancellation
	Il2CppObject * ___U3CU24U3Ecancellation_8;
	// System.Action UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::<$>action
	Action_t437523947 * ___U3CU24U3Eaction_9;

public:
	inline static int32_t get_offset_of_period_0() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___period_0)); }
	inline TimeSpan_t763862892  get_period_0() const { return ___period_0; }
	inline TimeSpan_t763862892 * get_address_of_period_0() { return &___period_0; }
	inline void set_period_0(TimeSpan_t763862892  value)
	{
		___period_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___cancellation_1)); }
	inline Il2CppObject * get_cancellation_1() const { return ___cancellation_1; }
	inline Il2CppObject ** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(Il2CppObject * value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier(&___cancellation_1, value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___action_2)); }
	inline Action_t437523947 * get_action_2() const { return ___action_2; }
	inline Action_t437523947 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t437523947 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier(&___action_2, value);
	}

	inline static int32_t get_offset_of_U3CsecondsU3E__0_3() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___U3CsecondsU3E__0_3)); }
	inline float get_U3CsecondsU3E__0_3() const { return ___U3CsecondsU3E__0_3; }
	inline float* get_address_of_U3CsecondsU3E__0_3() { return &___U3CsecondsU3E__0_3; }
	inline void set_U3CsecondsU3E__0_3(float value)
	{
		___U3CsecondsU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CyieldInstructionU3E__1_4() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___U3CyieldInstructionU3E__1_4)); }
	inline WaitForSeconds_t1291133240 * get_U3CyieldInstructionU3E__1_4() const { return ___U3CyieldInstructionU3E__1_4; }
	inline WaitForSeconds_t1291133240 ** get_address_of_U3CyieldInstructionU3E__1_4() { return &___U3CyieldInstructionU3E__1_4; }
	inline void set_U3CyieldInstructionU3E__1_4(WaitForSeconds_t1291133240 * value)
	{
		___U3CyieldInstructionU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CyieldInstructionU3E__1_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eperiod_7() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___U3CU24U3Eperiod_7)); }
	inline TimeSpan_t763862892  get_U3CU24U3Eperiod_7() const { return ___U3CU24U3Eperiod_7; }
	inline TimeSpan_t763862892 * get_address_of_U3CU24U3Eperiod_7() { return &___U3CU24U3Eperiod_7; }
	inline void set_U3CU24U3Eperiod_7(TimeSpan_t763862892  value)
	{
		___U3CU24U3Eperiod_7 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ecancellation_8() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___U3CU24U3Ecancellation_8)); }
	inline Il2CppObject * get_U3CU24U3Ecancellation_8() const { return ___U3CU24U3Ecancellation_8; }
	inline Il2CppObject ** get_address_of_U3CU24U3Ecancellation_8() { return &___U3CU24U3Ecancellation_8; }
	inline void set_U3CU24U3Ecancellation_8(Il2CppObject * value)
	{
		___U3CU24U3Ecancellation_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecancellation_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eaction_9() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1A_t3918433488, ___U3CU24U3Eaction_9)); }
	inline Action_t437523947 * get_U3CU24U3Eaction_9() const { return ___U3CU24U3Eaction_9; }
	inline Action_t437523947 ** get_address_of_U3CU24U3Eaction_9() { return &___U3CU24U3Eaction_9; }
	inline void set_U3CU24U3Eaction_9(Action_t437523947 * value)
	{
		___U3CU24U3Eaction_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eaction_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
