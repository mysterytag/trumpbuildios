﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.SubjectExtensions/AnonymousSubject`1<System.Object>
struct AnonymousSubject_1_t3796855849;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.SubjectExtensions/AnonymousSubject`1<System.Object>::.ctor(UniRx.IObserver`1<T>,UniRx.IObservable`1<T>)
extern "C"  void AnonymousSubject_1__ctor_m3265620673_gshared (AnonymousSubject_1_t3796855849 * __this, Il2CppObject* ___observer0, Il2CppObject* ___observable1, const MethodInfo* method);
#define AnonymousSubject_1__ctor_m3265620673(__this, ___observer0, ___observable1, method) ((  void (*) (AnonymousSubject_1_t3796855849 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))AnonymousSubject_1__ctor_m3265620673_gshared)(__this, ___observer0, ___observable1, method)
