﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t4074528527;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t817568325;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergEngineTools/UnityActionDisposable`1<System.Object>
struct  UnityActionDisposable_1_t3710771925  : public Il2CppObject
{
public:
	// UnityEngine.Events.UnityEvent`1<T> ZergEngineTools/UnityActionDisposable`1::e
	UnityEvent_1_t4074528527 * ___e_0;
	// UnityEngine.Events.UnityAction`1<T> ZergEngineTools/UnityActionDisposable`1::action
	UnityAction_1_t817568325 * ___action_1;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(UnityActionDisposable_1_t3710771925, ___e_0)); }
	inline UnityEvent_1_t4074528527 * get_e_0() const { return ___e_0; }
	inline UnityEvent_1_t4074528527 ** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(UnityEvent_1_t4074528527 * value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier(&___e_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(UnityActionDisposable_1_t3710771925, ___action_1)); }
	inline UnityAction_1_t817568325 * get_action_1() const { return ___action_1; }
	inline UnityAction_1_t817568325 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(UnityAction_1_t817568325 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
