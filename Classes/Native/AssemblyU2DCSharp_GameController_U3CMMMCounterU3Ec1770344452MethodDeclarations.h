﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<MMMCounter>c__Iterator17
struct U3CMMMCounterU3Ec__Iterator17_t1770344452;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<MMMCounter>c__Iterator17::.ctor()
extern "C"  void U3CMMMCounterU3Ec__Iterator17__ctor_m895681400 (U3CMMMCounterU3Ec__Iterator17_t1770344452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<MMMCounter>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMMMCounterU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m14388324 (U3CMMMCounterU3Ec__Iterator17_t1770344452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<MMMCounter>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMMMCounterU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m2145048568 (U3CMMMCounterU3Ec__Iterator17_t1770344452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<MMMCounter>c__Iterator17::MoveNext()
extern "C"  bool U3CMMMCounterU3Ec__Iterator17_MoveNext_m1572674404 (U3CMMMCounterU3Ec__Iterator17_t1770344452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<MMMCounter>c__Iterator17::Dispose()
extern "C"  void U3CMMMCounterU3Ec__Iterator17_Dispose_m1654649269 (U3CMMMCounterU3Ec__Iterator17_t1770344452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<MMMCounter>c__Iterator17::Reset()
extern "C"  void U3CMMMCounterU3Ec__Iterator17_Reset_m2837081637 (U3CMMMCounterU3Ec__Iterator17_t1770344452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
