﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Zenject.ZenjectTypeInfo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2740560362(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1884292721 *, Type_t *, ZenjectTypeInfo_t283213708 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,Zenject.ZenjectTypeInfo>::get_Key()
#define KeyValuePair_2_get_Key_m3821171262(__this, method) ((  Type_t * (*) (KeyValuePair_2_t1884292721 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Zenject.ZenjectTypeInfo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2571021823(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1884292721 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,Zenject.ZenjectTypeInfo>::get_Value()
#define KeyValuePair_2_get_Value_m1052062050(__this, method) ((  ZenjectTypeInfo_t283213708 * (*) (KeyValuePair_2_t1884292721 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Zenject.ZenjectTypeInfo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1384442495(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1884292721 *, ZenjectTypeInfo_t283213708 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,Zenject.ZenjectTypeInfo>::ToString()
#define KeyValuePair_2_ToString_m4128760425(__this, method) ((  String_t* (*) (KeyValuePair_2_t1884292721 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
