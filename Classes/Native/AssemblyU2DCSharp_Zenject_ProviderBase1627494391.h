﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Zenject.BindingCondition
struct BindingCondition_t1528286123;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBase
struct  ProviderBase_t1627494391  : public Il2CppObject
{
public:
	// System.String Zenject.ProviderBase::_identifier
	String_t* ____identifier_0;
	// Zenject.BindingCondition Zenject.ProviderBase::_condition
	BindingCondition_t1528286123 * ____condition_1;

public:
	inline static int32_t get_offset_of__identifier_0() { return static_cast<int32_t>(offsetof(ProviderBase_t1627494391, ____identifier_0)); }
	inline String_t* get__identifier_0() const { return ____identifier_0; }
	inline String_t** get_address_of__identifier_0() { return &____identifier_0; }
	inline void set__identifier_0(String_t* value)
	{
		____identifier_0 = value;
		Il2CppCodeGenWriteBarrier(&____identifier_0, value);
	}

	inline static int32_t get_offset_of__condition_1() { return static_cast<int32_t>(offsetof(ProviderBase_t1627494391, ____condition_1)); }
	inline BindingCondition_t1528286123 * get__condition_1() const { return ____condition_1; }
	inline BindingCondition_t1528286123 ** get_address_of__condition_1() { return &____condition_1; }
	inline void set__condition_1(BindingCondition_t1528286123 * value)
	{
		____condition_1 = value;
		Il2CppCodeGenWriteBarrier(&____condition_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
