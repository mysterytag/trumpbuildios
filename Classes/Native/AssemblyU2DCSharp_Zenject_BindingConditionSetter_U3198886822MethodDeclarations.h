﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindingConditionSetter/<WhenInjectedIntoInstance>c__AnonStorey170
struct U3CWhenInjectedIntoInstanceU3Ec__AnonStorey170_t198886822;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.BindingConditionSetter/<WhenInjectedIntoInstance>c__AnonStorey170::.ctor()
extern "C"  void U3CWhenInjectedIntoInstanceU3Ec__AnonStorey170__ctor_m2872083754 (U3CWhenInjectedIntoInstanceU3Ec__AnonStorey170_t198886822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingConditionSetter/<WhenInjectedIntoInstance>c__AnonStorey170::<>m__28E(Zenject.InjectContext)
extern "C"  bool U3CWhenInjectedIntoInstanceU3Ec__AnonStorey170_U3CU3Em__28E_m2520725049 (U3CWhenInjectedIntoInstanceU3Ec__AnonStorey170_t198886822 * __this, InjectContext_t3456483891 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
