﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.AncestorOrSelfIterator
struct AncestorOrSelfIterator_t2024991533;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_AncestorOrSelfIterator2024991533.h"

// System.Void System.Xml.XPath.AncestorOrSelfIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void AncestorOrSelfIterator__ctor_m3111916002 (AncestorOrSelfIterator_t2024991533 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.AncestorOrSelfIterator::.ctor(System.Xml.XPath.AncestorOrSelfIterator)
extern "C"  void AncestorOrSelfIterator__ctor_m334673489 (AncestorOrSelfIterator_t2024991533 * __this, AncestorOrSelfIterator_t2024991533 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.AncestorOrSelfIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * AncestorOrSelfIterator_Clone_m1287523056 (AncestorOrSelfIterator_t2024991533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.AncestorOrSelfIterator::CollectResults()
extern "C"  void AncestorOrSelfIterator_CollectResults_m2689768550 (AncestorOrSelfIterator_t2024991533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.AncestorOrSelfIterator::MoveNextCore()
extern "C"  bool AncestorOrSelfIterator_MoveNextCore_m2628316821 (AncestorOrSelfIterator_t2024991533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.AncestorOrSelfIterator::get_ReverseAxis()
extern "C"  bool AncestorOrSelfIterator_get_ReverseAxis_m1088500970 (AncestorOrSelfIterator_t2024991533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XPath.AncestorOrSelfIterator::get_Count()
extern "C"  int32_t AncestorOrSelfIterator_get_Count_m312110768 (AncestorOrSelfIterator_t2024991533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
