﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile)
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/Binary
struct  Binary_t1989867553  : public StableCompositeDisposable_t3433635614
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/Binary::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Binary::_disposable1
	Il2CppObject * ____disposable1_1;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Binary::_disposable2
	Il2CppObject * ____disposable2_2;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(Binary_t1989867553, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposable1_1() { return static_cast<int32_t>(offsetof(Binary_t1989867553, ____disposable1_1)); }
	inline Il2CppObject * get__disposable1_1() const { return ____disposable1_1; }
	inline Il2CppObject ** get_address_of__disposable1_1() { return &____disposable1_1; }
	inline void set__disposable1_1(Il2CppObject * value)
	{
		____disposable1_1 = value;
		Il2CppCodeGenWriteBarrier(&____disposable1_1, value);
	}

	inline static int32_t get_offset_of__disposable2_2() { return static_cast<int32_t>(offsetof(Binary_t1989867553, ____disposable2_2)); }
	inline Il2CppObject * get__disposable2_2() const { return ____disposable2_2; }
	inline Il2CppObject ** get_address_of__disposable2_2() { return &____disposable2_2; }
	inline void set__disposable2_2(Il2CppObject * value)
	{
		____disposable2_2 = value;
		Il2CppCodeGenWriteBarrier(&____disposable2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
