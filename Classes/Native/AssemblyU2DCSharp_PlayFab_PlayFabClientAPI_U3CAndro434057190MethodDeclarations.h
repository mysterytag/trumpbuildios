﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<AndroidDevicePushNotificationRegistration>c__AnonStoreyB8
struct U3CAndroidDevicePushNotificationRegistrationU3Ec__AnonStoreyB8_t434057190;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<AndroidDevicePushNotificationRegistration>c__AnonStoreyB8::.ctor()
extern "C"  void U3CAndroidDevicePushNotificationRegistrationU3Ec__AnonStoreyB8__ctor_m384896557 (U3CAndroidDevicePushNotificationRegistrationU3Ec__AnonStoreyB8_t434057190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<AndroidDevicePushNotificationRegistration>c__AnonStoreyB8::<>m__A5(PlayFab.CallRequestContainer)
extern "C"  void U3CAndroidDevicePushNotificationRegistrationU3Ec__AnonStoreyB8_U3CU3Em__A5_m2159836511 (U3CAndroidDevicePushNotificationRegistrationU3Ec__AnonStoreyB8_t434057190 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
