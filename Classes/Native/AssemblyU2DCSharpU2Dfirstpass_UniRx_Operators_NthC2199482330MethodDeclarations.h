﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>
struct NthCombineLatestObserverBase_1_t2199482330;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::.ctor(System.Int32,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void NthCombineLatestObserverBase_1__ctor_m1135319011_gshared (NthCombineLatestObserverBase_1_t2199482330 * __this, int32_t ___length0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define NthCombineLatestObserverBase_1__ctor_m1135319011(__this, ___length0, ___observer1, ___cancel2, method) ((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, Il2CppObject*, Il2CppObject *, const MethodInfo*))NthCombineLatestObserverBase_1__ctor_m1135319011_gshared)(__this, ___length0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::Publish(System.Int32)
extern "C"  void NthCombineLatestObserverBase_1_Publish_m3159882668_gshared (NthCombineLatestObserverBase_1_t2199482330 * __this, int32_t ___index0, const MethodInfo* method);
#define NthCombineLatestObserverBase_1_Publish_m3159882668(__this, ___index0, method) ((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, const MethodInfo*))NthCombineLatestObserverBase_1_Publish_m3159882668_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::Done(System.Int32)
extern "C"  void NthCombineLatestObserverBase_1_Done_m1063512233_gshared (NthCombineLatestObserverBase_1_t2199482330 * __this, int32_t ___index0, const MethodInfo* method);
#define NthCombineLatestObserverBase_1_Done_m1063512233(__this, ___index0, method) ((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, int32_t, const MethodInfo*))NthCombineLatestObserverBase_1_Done_m1063512233_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::Fail(System.Exception)
extern "C"  void NthCombineLatestObserverBase_1_Fail_m4048805028_gshared (NthCombineLatestObserverBase_1_t2199482330 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define NthCombineLatestObserverBase_1_Fail_m4048805028(__this, ___error0, method) ((  void (*) (NthCombineLatestObserverBase_1_t2199482330 *, Exception_t1967233988 *, const MethodInfo*))NthCombineLatestObserverBase_1_Fail_m4048805028_gshared)(__this, ___error0, method)
