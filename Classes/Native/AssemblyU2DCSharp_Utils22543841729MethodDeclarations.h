﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IDisposable
struct IDisposable_t1628921374;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t2937500249;
// System.Action
struct Action_t437523947;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1755167990;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t3830363377;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Component
struct Component_t2126946602;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2937500249.h"
#include "System_Core_System_Action437523947.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "mscorlib_System_String968488902.h"

// System.Void Utils2::.cctor()
extern "C"  void Utils2__cctor_m2306502387 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Utils2::Loop(System.Int32,System.Int32)
extern "C"  int32_t Utils2_Loop_m2725012842 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___cycle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable Utils2::OnPointerDown(UnityEngine.EventSystems.EventTrigger,System.Action)
extern "C"  Il2CppObject * Utils2_OnPointerDown_m1435445017 (Il2CppObject * __this /* static, unused */, EventTrigger_t2937500249 * ___self0, Action_t437523947 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Single> Utils2::NormalizeFloatRange(System.Collections.Generic.IEnumerable`1<System.Single>)
extern "C"  List_1_t1755167990 * Utils2_NormalizeFloatRange_m725231988 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___range0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Utils2::GetRandomIndexFromWeghts(System.Collections.Generic.IEnumerable`1<System.Single>)
extern "C"  int32_t Utils2_GetRandomIndexFromWeghts_m1814573993 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___probabilities0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils2::EaseOut(System.Single)
extern "C"  float Utils2_EaseOut_m1106580639 (Il2CppObject * __this /* static, unused */, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils2::EaseIn(System.Single)
extern "C"  float Utils2_EaseIn_m4233014978 (Il2CppObject * __this /* static, unused */, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils2::EaseElasticOut(System.Single)
extern "C"  float Utils2_EaseElasticOut_m76024878 (Il2CppObject * __this /* static, unused */, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils2::EaseOut(System.Single,System.Single,System.Single)
extern "C"  float Utils2_EaseOut_m3706691881 (Il2CppObject * __this /* static, unused */, float ___from0, float ___to1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils2::EaseIn(System.Single,System.Single,System.Single)
extern "C"  float Utils2_EaseIn_m3173677260 (Il2CppObject * __this /* static, unused */, float ___from0, float ___to1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils2::GetNumberRepresentation(System.Int64,System.Int32,System.Int32)
extern "C"  String_t* Utils2_GetNumberRepresentation_m1892862067 (Il2CppObject * __this /* static, unused */, int64_t ___number0, int32_t ___maxDigits1, int32_t ___maxDigitsAfterDot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils2::GetNumberRepresentation(System.Double,System.Int32,System.Int32)
extern "C"  String_t* Utils2_GetNumberRepresentation_m3652355457 (Il2CppObject * __this /* static, unused */, double ___number0, int32_t ___maxDigits1, int32_t ___maxDigitsAfterDot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils2::TenPower(System.Int32)
extern "C"  float Utils2_TenPower_m2328187023 (Il2CppObject * __this /* static, unused */, int32_t ___power0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Utils2::RandomNonoverlappedIndices(System.Int32,System.Int32)
extern "C"  Int32U5BU5D_t1809983122* Utils2_RandomNonoverlappedIndices_m2649490619 (Il2CppObject * __this /* static, unused */, int32_t ___max0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Utils2::MakeInstance(UnityEngine.GameObject)
extern "C"  GameObject_t4012695102 * Utils2_MakeInstance_m645026778 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2::ResetTransform(UnityEngine.GameObject)
extern "C"  void Utils2_ResetTransform_m946978463 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2::ResetTransform(UnityEngine.Component)
extern "C"  void Utils2_ResetTransform_m2273345477 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2::SetPositionX(UnityEngine.Transform,System.Single)
extern "C"  void Utils2_SetPositionX_m1599284267 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___t0, float ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2::SetLocalPositionX(UnityEngine.Transform,System.Single)
extern "C"  void Utils2_SetLocalPositionX_m2954896228 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___t0, float ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2::SetPositionY(UnityEngine.Transform,System.Single)
extern "C"  void Utils2_SetPositionY_m3232088266 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___t0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2::SetLocalPositionY(UnityEngine.Transform,System.Single)
extern "C"  void Utils2_SetLocalPositionY_m292732931 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___t0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2::SetPositionZ(UnityEngine.Transform,System.Single)
extern "C"  void Utils2_SetPositionZ_m569924969 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___t0, float ___z1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Utils2::SetLocalPositionZ(UnityEngine.Transform,System.Single)
extern "C"  void Utils2_SetLocalPositionZ_m1925536930 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___t0, float ___z1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite Utils2::LoadSprite(System.String)
extern "C"  Sprite_t4006040370 * Utils2_LoadSprite_m1335642668 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
