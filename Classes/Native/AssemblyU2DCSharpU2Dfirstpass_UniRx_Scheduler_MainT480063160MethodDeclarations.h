﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/MainThreadScheduler
struct MainThreadScheduler_t480063160;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Action
struct Action_t437523947;
// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Scheduler/MainThreadScheduler::.ctor()
extern "C"  void MainThreadScheduler__ctor_m1539190571 (MainThreadScheduler_t480063160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Scheduler/MainThreadScheduler::DelayAction(System.TimeSpan,System.Action,UniRx.ICancelable)
extern "C"  Il2CppObject * MainThreadScheduler_DelayAction_m1125531720 (MainThreadScheduler_t480063160 * __this, TimeSpan_t763862892  ___dueTime0, Action_t437523947 * ___action1, Il2CppObject * ___cancellation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Scheduler/MainThreadScheduler::PeriodicAction(System.TimeSpan,System.Action,UniRx.ICancelable)
extern "C"  Il2CppObject * MainThreadScheduler_PeriodicAction_m1001143840 (MainThreadScheduler_t480063160 * __this, TimeSpan_t763862892  ___period0, Action_t437523947 * ___action1, Il2CppObject * ___cancellation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset UniRx.Scheduler/MainThreadScheduler::get_Now()
extern "C"  DateTimeOffset_t3712260035  MainThreadScheduler_get_Now_m2743672252 (MainThreadScheduler_t480063160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/MainThreadScheduler::Schedule(System.Object)
extern "C"  void MainThreadScheduler_Schedule_m129217028 (MainThreadScheduler_t480063160 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/MainThreadScheduler::Schedule(System.Action)
extern "C"  Il2CppObject * MainThreadScheduler_Schedule_m1108981416 (MainThreadScheduler_t480063160 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/MainThreadScheduler::Schedule(System.DateTimeOffset,System.Action)
extern "C"  Il2CppObject * MainThreadScheduler_Schedule_m1078996359 (MainThreadScheduler_t480063160 * __this, DateTimeOffset_t3712260035  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/MainThreadScheduler::Schedule(System.TimeSpan,System.Action)
extern "C"  Il2CppObject * MainThreadScheduler_Schedule_m2438980222 (MainThreadScheduler_t480063160 * __this, TimeSpan_t763862892  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/MainThreadScheduler::SchedulePeriodic(System.TimeSpan,System.Action)
extern "C"  Il2CppObject * MainThreadScheduler_SchedulePeriodic_m2830140515 (MainThreadScheduler_t480063160 * __this, TimeSpan_t763862892  ___period0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
