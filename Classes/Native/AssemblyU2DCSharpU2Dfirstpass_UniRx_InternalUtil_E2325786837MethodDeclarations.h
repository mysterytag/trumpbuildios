﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct EmptyObserver_1_t2325786837;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m380576264_gshared (EmptyObserver_1_t2325786837 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m380576264(__this, method) ((  void (*) (EmptyObserver_1_t2325786837 *, const MethodInfo*))EmptyObserver_1__ctor_m380576264_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2725833381_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m2725833381(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m2725833381_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3278548882_gshared (EmptyObserver_1_t2325786837 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m3278548882(__this, method) ((  void (*) (EmptyObserver_1_t2325786837 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m3278548882_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2839582143_gshared (EmptyObserver_1_t2325786837 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m2839582143(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2325786837 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m2839582143_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3492272816_gshared (EmptyObserver_1_t2325786837 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3492272816(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2325786837 *, CollectionMoveEvent_1_t2916433847 , const MethodInfo*))EmptyObserver_1_OnNext_m3492272816_gshared)(__this, ___value0, method)
