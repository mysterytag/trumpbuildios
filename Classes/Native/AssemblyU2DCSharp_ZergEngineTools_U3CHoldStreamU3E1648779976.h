﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t990034267;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergEngineTools/<HoldStream>c__AnonStorey150
struct  U3CHoldStreamU3Ec__AnonStorey150_t1648779976  : public Il2CppObject
{
public:
	// UnityEngine.UI.Button ZergEngineTools/<HoldStream>c__AnonStorey150::button
	Button_t990034267 * ___button_0;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(U3CHoldStreamU3Ec__AnonStorey150_t1648779976, ___button_0)); }
	inline Button_t990034267 * get_button_0() const { return ___button_0; }
	inline Button_t990034267 ** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(Button_t990034267 * value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier(&___button_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
