﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct DisposedObserver_1_t548363397;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3694154233_gshared (DisposedObserver_1_t548363397 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m3694154233(__this, method) ((  void (*) (DisposedObserver_1_t548363397 *, const MethodInfo*))DisposedObserver_1__ctor_m3694154233_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2367535316_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m2367535316(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m2367535316_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3049940675_gshared (DisposedObserver_1_t548363397 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3049940675(__this, method) ((  void (*) (DisposedObserver_1_t548363397 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3049940675_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1440353136_gshared (DisposedObserver_1_t548363397 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1440353136(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t548363397 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1440353136_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m974967393_gshared (DisposedObserver_1_t548363397 * __this, CollectionReplaceEvent_1_t1373905935  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m974967393(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t548363397 *, CollectionReplaceEvent_1_t1373905935 , const MethodInfo*))DisposedObserver_1_OnNext_m974967393_gshared)(__this, ___value0, method)
