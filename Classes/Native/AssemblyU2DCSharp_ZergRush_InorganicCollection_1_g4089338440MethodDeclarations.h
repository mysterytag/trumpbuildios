﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ZergRush_InorganicCollection_1_g3450977518MethodDeclarations.h"

// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::.ctor()
#define InorganicCollection_1__ctor_m585377374(__this, method) ((  void (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1__ctor_m2562722286_gshared)(__this, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define InorganicCollection_1__ctor_m3948197729(__this, ___collection0, method) ((  void (*) (InorganicCollection_1_t4089338440 *, Il2CppObject*, const MethodInfo*))InorganicCollection_1__ctor_m3238279633_gshared)(__this, ___collection0, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::.ctor(System.Collections.Generic.List`1<T>)
#define InorganicCollection_1__ctor_m2806421430(__this, ___list0, method) ((  void (*) (InorganicCollection_1_t4089338440 *, List_1_t2272426311 *, const MethodInfo*))InorganicCollection_1__ctor_m1788080966_gshared)(__this, ___list0, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::ClearItems()
#define InorganicCollection_1_ClearItems_m1294461977(__this, method) ((  void (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_ClearItems_m2583413385_gshared)(__this, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::InsertItem(System.Int32,T)
#define InorganicCollection_1_InsertItem_m2978380027(__this, ___index0, ___item1, method) ((  void (*) (InorganicCollection_1_t4089338440 *, int32_t, UpgradeButton_t1475467342 *, const MethodInfo*))InorganicCollection_1_InsertItem_m1273645931_gshared)(__this, ___index0, ___item1, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::Move(System.Int32,System.Int32)
#define InorganicCollection_1_Move_m401256527(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (InorganicCollection_1_t4089338440 *, int32_t, int32_t, const MethodInfo*))InorganicCollection_1_Move_m2620974047_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::MoveItem(System.Int32,System.Int32)
#define InorganicCollection_1_MoveItem_m3863757884(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (InorganicCollection_1_t4089338440 *, int32_t, int32_t, const MethodInfo*))InorganicCollection_1_MoveItem_m1781936076_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::RemoveItem(System.Int32)
#define InorganicCollection_1_RemoveItem_m1110464814(__this, ___index0, method) ((  void (*) (InorganicCollection_1_t4089338440 *, int32_t, const MethodInfo*))InorganicCollection_1_RemoveItem_m3410364318_gshared)(__this, ___index0, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::RemoveItem(System.Func`2<T,System.Boolean>)
#define InorganicCollection_1_RemoveItem_m3385287263(__this, ___filter0, method) ((  void (*) (InorganicCollection_1_t4089338440 *, Func_2_t1681751135 *, const MethodInfo*))InorganicCollection_1_RemoveItem_m2366946799_gshared)(__this, ___filter0, method)
// System.Void ZergRush.InorganicCollection`1<UpgradeButton>::SetItem(System.Int32,T)
#define InorganicCollection_1_SetItem_m1082358522(__this, ___index0, ___item1, method) ((  void (*) (InorganicCollection_1_t4089338440 *, int32_t, UpgradeButton_t1475467342 *, const MethodInfo*))InorganicCollection_1_SetItem_m2680569482_gshared)(__this, ___index0, ___item1, method)
// IStream`1<System.Int32> ZergRush.InorganicCollection`1<UpgradeButton>::ObserveCountChanged()
#define InorganicCollection_1_ObserveCountChanged_m320493827(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_ObserveCountChanged_m3361259667_gshared)(__this, method)
// IStream`1<UniRx.Unit> ZergRush.InorganicCollection`1<UpgradeButton>::ObserveReset()
#define InorganicCollection_1_ObserveReset_m3448147558(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_ObserveReset_m884902102_gshared)(__this, method)
// IStream`1<UniRx.CollectionAddEvent`1<T>> ZergRush.InorganicCollection`1<UpgradeButton>::ObserveAdd()
#define InorganicCollection_1_ObserveAdd_m3506490856(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_ObserveAdd_m500474968_gshared)(__this, method)
// IStream`1<CollectionMoveEvent`1<T>> ZergRush.InorganicCollection`1<UpgradeButton>::ObserveMove()
#define InorganicCollection_1_ObserveMove_m1495167562(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_ObserveMove_m2797955546_gshared)(__this, method)
// IStream`1<CollectionRemoveEvent`1<T>> ZergRush.InorganicCollection`1<UpgradeButton>::ObserveRemove()
#define InorganicCollection_1_ObserveRemove_m1651867248(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_ObserveRemove_m3795636736_gshared)(__this, method)
// IStream`1<CollectionReplaceEvent`1<T>> ZergRush.InorganicCollection`1<UpgradeButton>::ObserveReplace()
#define InorganicCollection_1_ObserveReplace_m1093249232(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_ObserveReplace_m3125593920_gshared)(__this, method)
// System.IDisposable ZergRush.InorganicCollection`1<UpgradeButton>::OnChanged(System.Action,Priority)
#define InorganicCollection_1_OnChanged_m2672442249(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t4089338440 *, Action_t437523947 *, int32_t, const MethodInfo*))InorganicCollection_1_OnChanged_m1222232857_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable ZergRush.InorganicCollection`1<UpgradeButton>::Bind(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
#define InorganicCollection_1_Bind_m2434292233(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t4089338440 *, Action_1_t2089751433 *, int32_t, const MethodInfo*))InorganicCollection_1_Bind_m1502836121_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable ZergRush.InorganicCollection`1<UpgradeButton>::ListenUpdates(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
#define InorganicCollection_1_ListenUpdates_m2860299047(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t4089338440 *, Action_1_t2089751433 *, int32_t, const MethodInfo*))InorganicCollection_1_ListenUpdates_m1688181143_gshared)(__this, ___reaction0, ___p1, method)
// System.Collections.Generic.ICollection`1<T> ZergRush.InorganicCollection`1<UpgradeButton>::get_value()
#define InorganicCollection_1_get_value_m589121114(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_get_value_m3124552170_gshared)(__this, method)
// System.Object ZergRush.InorganicCollection`1<UpgradeButton>::get_valueObject()
#define InorganicCollection_1_get_valueObject_m3629578616(__this, method) ((  Il2CppObject * (*) (InorganicCollection_1_t4089338440 *, const MethodInfo*))InorganicCollection_1_get_valueObject_m2207754504_gshared)(__this, method)
