﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AcceptTradeResponseCallback
struct AcceptTradeResponseCallback_t371666146;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AcceptTradeRequest
struct AcceptTradeRequest_t1814136291;
// PlayFab.ClientModels.AcceptTradeResponse
struct AcceptTradeResponse_t3663823245;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AcceptTrade1814136291.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AcceptTrade3663823245.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AcceptTradeResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AcceptTradeResponseCallback__ctor_m53565553 (AcceptTradeResponseCallback_t371666146 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AcceptTradeResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AcceptTradeRequest,PlayFab.ClientModels.AcceptTradeResponse,PlayFab.PlayFabError,System.Object)
extern "C"  void AcceptTradeResponseCallback_Invoke_m2098254968 (AcceptTradeResponseCallback_t371666146 * __this, String_t* ___urlPath0, int32_t ___callId1, AcceptTradeRequest_t1814136291 * ___request2, AcceptTradeResponse_t3663823245 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AcceptTradeResponseCallback_t371666146(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AcceptTradeRequest_t1814136291 * ___request2, AcceptTradeResponse_t3663823245 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AcceptTradeResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AcceptTradeRequest,PlayFab.ClientModels.AcceptTradeResponse,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AcceptTradeResponseCallback_BeginInvoke_m2404982137 (AcceptTradeResponseCallback_t371666146 * __this, String_t* ___urlPath0, int32_t ___callId1, AcceptTradeRequest_t1814136291 * ___request2, AcceptTradeResponse_t3663823245 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AcceptTradeResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AcceptTradeResponseCallback_EndInvoke_m2510072577 (AcceptTradeResponseCallback_t371666146 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
