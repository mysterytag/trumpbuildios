﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCloudScriptUrlRequestCallback
struct GetCloudScriptUrlRequestCallback_t3206540911;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCloudScriptUrlRequest
struct GetCloudScriptUrlRequest_t841424922;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCloudScri841424922.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCloudScriptUrlRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCloudScriptUrlRequestCallback__ctor_m4018086878 (GetCloudScriptUrlRequestCallback_t3206540911 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCloudScriptUrlRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCloudScriptUrlRequest,System.Object)
extern "C"  void GetCloudScriptUrlRequestCallback_Invoke_m1622983679 (GetCloudScriptUrlRequestCallback_t3206540911 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCloudScriptUrlRequest_t841424922 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCloudScriptUrlRequestCallback_t3206540911(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCloudScriptUrlRequest_t841424922 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCloudScriptUrlRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCloudScriptUrlRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCloudScriptUrlRequestCallback_BeginInvoke_m3182789380 (GetCloudScriptUrlRequestCallback_t3206540911 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCloudScriptUrlRequest_t841424922 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCloudScriptUrlRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCloudScriptUrlRequestCallback_EndInvoke_m1900754158 (GetCloudScriptUrlRequestCallback_t3206540911 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
