﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkAndroidDeviceIDRequest
struct LinkAndroidDeviceIDRequest_t2075009465;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDRequest::.ctor()
extern "C"  void LinkAndroidDeviceIDRequest__ctor_m258155044 (LinkAndroidDeviceIDRequest_t2075009465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkAndroidDeviceIDRequest::get_AndroidDeviceId()
extern "C"  String_t* LinkAndroidDeviceIDRequest_get_AndroidDeviceId_m1336801884 (LinkAndroidDeviceIDRequest_t2075009465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDRequest::set_AndroidDeviceId(System.String)
extern "C"  void LinkAndroidDeviceIDRequest_set_AndroidDeviceId_m2976226973 (LinkAndroidDeviceIDRequest_t2075009465 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkAndroidDeviceIDRequest::get_OS()
extern "C"  String_t* LinkAndroidDeviceIDRequest_get_OS_m436307498 (LinkAndroidDeviceIDRequest_t2075009465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDRequest::set_OS(System.String)
extern "C"  void LinkAndroidDeviceIDRequest_set_OS_m499954497 (LinkAndroidDeviceIDRequest_t2075009465 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkAndroidDeviceIDRequest::get_AndroidDevice()
extern "C"  String_t* LinkAndroidDeviceIDRequest_get_AndroidDevice_m2110884833 (LinkAndroidDeviceIDRequest_t2075009465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDRequest::set_AndroidDevice(System.String)
extern "C"  void LinkAndroidDeviceIDRequest_set_AndroidDevice_m3803455224 (LinkAndroidDeviceIDRequest_t2075009465 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
