﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "AssemblyU2DCSharp_Zenject_Installer3915807453.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AutoBindInstaller
struct  AutoBindInstaller_t794972593  : public Installer_t3915807453
{
public:
	// UnityEngine.GameObject Zenject.AutoBindInstaller::_root
	GameObject_t4012695102 * ____root_1;

public:
	inline static int32_t get_offset_of__root_1() { return static_cast<int32_t>(offsetof(AutoBindInstaller_t794972593, ____root_1)); }
	inline GameObject_t4012695102 * get__root_1() const { return ____root_1; }
	inline GameObject_t4012695102 ** get_address_of__root_1() { return &____root_1; }
	inline void set__root_1(GameObject_t4012695102 * value)
	{
		____root_1 = value;
		Il2CppCodeGenWriteBarrier(&____root_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
