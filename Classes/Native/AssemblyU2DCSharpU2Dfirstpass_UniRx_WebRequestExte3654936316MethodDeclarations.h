﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1/<AbortableDeferredAsyncRequest>c__AnonStorey34`1<System.Object>
struct U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1/<AbortableDeferredAsyncRequest>c__AnonStorey34`1<System.Object>::.ctor()
extern "C"  void U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1__ctor_m1775481955_gshared (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316 * __this, const MethodInfo* method);
#define U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1__ctor_m1775481955(__this, method) ((  void (*) (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316 *, const MethodInfo*))U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1__ctor_m1775481955_gshared)(__this, method)
// TResult UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1/<AbortableDeferredAsyncRequest>c__AnonStorey34`1<System.Object>::<>m__38(System.IAsyncResult)
extern "C"  Il2CppObject * U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_U3CU3Em__38_m226782544_gshared (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316 * __this, Il2CppObject * ___ar0, const MethodInfo* method);
#define U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_U3CU3Em__38_m226782544(__this, ___ar0, method) ((  Il2CppObject * (*) (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316 *, Il2CppObject *, const MethodInfo*))U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_U3CU3Em__38_m226782544_gshared)(__this, ___ar0, method)
// System.Void UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1/<AbortableDeferredAsyncRequest>c__AnonStorey34`1<System.Object>::<>m__39()
extern "C"  void U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_U3CU3Em__39_m3444250450_gshared (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316 * __this, const MethodInfo* method);
#define U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_U3CU3Em__39_m3444250450(__this, method) ((  void (*) (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316 *, const MethodInfo*))U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_U3CU3Em__39_m3444250450_gshared)(__this, method)
