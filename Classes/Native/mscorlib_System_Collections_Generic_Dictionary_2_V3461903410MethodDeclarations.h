﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>
struct ValueCollection_t3461903410;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t1539766316;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t35554034;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Int64[]
struct Int64U5BU5D_t753178071;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1306794257.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m4180747826_gshared (ValueCollection_t3461903410 * __this, Dictionary_2_t1539766316 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m4180747826(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3461903410 *, Dictionary_2_t1539766316 *, const MethodInfo*))ValueCollection__ctor_m4180747826_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1389791168_gshared (ValueCollection_t3461903410 * __this, int64_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1389791168(__this, ___item0, method) ((  void (*) (ValueCollection_t3461903410 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1389791168_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m558257289_gshared (ValueCollection_t3461903410 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m558257289(__this, method) ((  void (*) (ValueCollection_t3461903410 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m558257289_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2921328682_gshared (ValueCollection_t3461903410 * __this, int64_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2921328682(__this, ___item0, method) ((  bool (*) (ValueCollection_t3461903410 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2921328682_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2779450191_gshared (ValueCollection_t3461903410 * __this, int64_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2779450191(__this, ___item0, method) ((  bool (*) (ValueCollection_t3461903410 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2779450191_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m121887305_gshared (ValueCollection_t3461903410 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m121887305(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3461903410 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m121887305_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3339165261_gshared (ValueCollection_t3461903410 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3339165261(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3461903410 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3339165261_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3126473628_gshared (ValueCollection_t3461903410 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3126473628(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3461903410 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3126473628_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1196504541_gshared (ValueCollection_t3461903410 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1196504541(__this, method) ((  bool (*) (ValueCollection_t3461903410 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1196504541_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3833306941_gshared (ValueCollection_t3461903410 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3833306941(__this, method) ((  bool (*) (ValueCollection_t3461903410 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3833306941_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m874765167_gshared (ValueCollection_t3461903410 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m874765167(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3461903410 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m874765167_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2233626105_gshared (ValueCollection_t3461903410 * __this, Int64U5BU5D_t753178071* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2233626105(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3461903410 *, Int64U5BU5D_t753178071*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2233626105_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::GetEnumerator()
extern "C"  Enumerator_t1306794259  ValueCollection_GetEnumerator_m300312098_gshared (ValueCollection_t3461903410 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m300312098(__this, method) ((  Enumerator_t1306794259  (*) (ValueCollection_t3461903410 *, const MethodInfo*))ValueCollection_GetEnumerator_m300312098_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m543685495_gshared (ValueCollection_t3461903410 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m543685495(__this, method) ((  int32_t (*) (ValueCollection_t3461903410 *, const MethodInfo*))ValueCollection_get_Count_m543685495_gshared)(__this, method)
