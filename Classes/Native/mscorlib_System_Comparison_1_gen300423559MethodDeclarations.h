﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UnityEngine.Quaternion>
struct Comparison_1_t300423559;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m4136345524_gshared (Comparison_1_t300423559 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m4136345524(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t300423559 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m4136345524_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<UnityEngine.Quaternion>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m750226372_gshared (Comparison_1_t300423559 * __this, Quaternion_t1891715979  ___x0, Quaternion_t1891715979  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m750226372(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t300423559 *, Quaternion_t1891715979 , Quaternion_t1891715979 , const MethodInfo*))Comparison_1_Invoke_m750226372_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Quaternion>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m3373644557_gshared (Comparison_1_t300423559 * __this, Quaternion_t1891715979  ___x0, Quaternion_t1891715979  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m3373644557(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t300423559 *, Quaternion_t1891715979 , Quaternion_t1891715979 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3373644557_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m962455080_gshared (Comparison_1_t300423559 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m962455080(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t300423559 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m962455080_gshared)(__this, ___result0, method)
