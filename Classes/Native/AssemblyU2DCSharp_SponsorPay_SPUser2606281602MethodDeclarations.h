﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser
struct SPUser_t2606281602;
// SponsorPay.SPLocation
struct SPLocation_t126034156;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Nullable_1_gen2074414583.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserGender3483343971.h"
#include "mscorlib_System_Nullable_1_gen3787560604.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserSexualOrientatio901522696.h"
#include "mscorlib_System_Nullable_1_gen3783893733.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserEthnicity897855825.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLocation126034156.h"
#include "mscorlib_System_Nullable_1_gen2392475756.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserMaritalStatus3801405144.h"
#include "mscorlib_System_Nullable_1_gen4294542862.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserEducation1408504954.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"
#include "mscorlib_System_Nullable_1_gen3844246929.h"
#include "mscorlib_System_Nullable_1_gen1438485494.h"
#include "mscorlib_System_Nullable_1_gen2936825364.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserConnection50787456.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SponsorPay.SPUser::.ctor()
extern "C"  void SPUser__ctor_m2224271363 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::Reset()
extern "C"  void SPUser_Reset_m4165671600 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SponsorPay.SPUser::GetAge()
extern "C"  Nullable_1_t1438485399  SPUser_GetAge_m3859017243 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetAge(System.Int32)
extern "C"  void SPUser_SetAge_m2573616943 (SPUser_t2606281602 * __this, int32_t ___age0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> SponsorPay.SPUser::GetBirthdate()
extern "C"  Nullable_1_t3225071844  SPUser_GetBirthdate_m3596794890 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetBirthdate(System.DateTime)
extern "C"  void SPUser_SetBirthdate_m2195772396 (SPUser_t2606281602 * __this, DateTime_t339033936  ___birthdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserGender> SponsorPay.SPUser::GetGender()
extern "C"  Nullable_1_t2074414583  SPUser_GetGender_m3603379317 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetGender(SponsorPay.SPUserGender)
extern "C"  void SPUser_SetGender_m2525548965 (SPUser_t2606281602 * __this, int32_t ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserSexualOrientation> SponsorPay.SPUser::GetSexualOrientation()
extern "C"  Nullable_1_t3787560604  SPUser_GetSexualOrientation_m1711043283 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetSexualOrientation(SponsorPay.SPUserSexualOrientation)
extern "C"  void SPUser_SetSexualOrientation_m3894302695 (SPUser_t2606281602 * __this, int32_t ___sexualOrientation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserEthnicity> SponsorPay.SPUser::GetEthnicity()
extern "C"  Nullable_1_t3783893733  SPUser_GetEthnicity_m719779315 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetEthnicity(SponsorPay.SPUserEthnicity)
extern "C"  void SPUser_SetEthnicity_m3667721095 (SPUser_t2606281602 * __this, int32_t ___ethnicity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SponsorPay.SPLocation SponsorPay.SPUser::GetLocation()
extern "C"  SPLocation_t126034156 * SPUser_GetLocation_m568928205 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetLocation(SponsorPay.SPLocation)
extern "C"  void SPUser_SetLocation_m256846984 (SPUser_t2606281602 * __this, SPLocation_t126034156 * ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserMaritalStatus> SponsorPay.SPUser::GetMaritalStatus()
extern "C"  Nullable_1_t2392475756  SPUser_GetMaritalStatus_m3797418963 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetMaritalStatus(SponsorPay.SPUserMaritalStatus)
extern "C"  void SPUser_SetMaritalStatus_m1562708711 (SPUser_t2606281602 * __this, int32_t ___maritalStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SponsorPay.SPUser::GetNumberOfChildrens()
extern "C"  Nullable_1_t1438485399  SPUser_GetNumberOfChildrens_m2282720208 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetNumberOfChildrens(System.Int32)
extern "C"  void SPUser_SetNumberOfChildrens_m3389246500 (SPUser_t2606281602 * __this, int32_t ___numberOfChildrens0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SponsorPay.SPUser::GetAnnualHouseholdIncome()
extern "C"  Nullable_1_t1438485399  SPUser_GetAnnualHouseholdIncome_m716315749 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetAnnualHouseholdIncome(System.Int32)
extern "C"  void SPUser_SetAnnualHouseholdIncome_m790752185 (SPUser_t2606281602 * __this, int32_t ___annualHouseholdIncome0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserEducation> SponsorPay.SPUser::GetEducation()
extern "C"  Nullable_1_t4294542862  SPUser_GetEducation_m2906126099 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetEducation(SponsorPay.SPUserEducation)
extern "C"  void SPUser_SetEducation_m830634279 (SPUser_t2606281602 * __this, int32_t ___education0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SPUser::GetZipcode()
extern "C"  String_t* SPUser_GetZipcode_m2642697398 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetZipcode(System.String)
extern "C"  void SPUser_SetZipcode_m1377985653 (SPUser_t2606281602 * __this, String_t* ___zipcode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] SponsorPay.SPUser::GetInterests()
extern "C"  StringU5BU5D_t2956870243* SPUser_GetInterests_m2162579411 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetInterests(System.String[])
extern "C"  void SPUser_SetInterests_m523855160 (SPUser_t2606281602 * __this, StringU5BU5D_t2956870243* ___interests0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> SponsorPay.SPUser::GetIap()
extern "C"  Nullable_1_t3097043249  SPUser_GetIap_m384020378 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetIap(System.Boolean)
extern "C"  void SPUser_SetIap_m2056688430 (SPUser_t2606281602 * __this, bool ___iap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> SponsorPay.SPUser::GetIapAmount()
extern "C"  Nullable_1_t3844246929  SPUser_GetIapAmount_m2234225952 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetIapAmount(System.Single)
extern "C"  void SPUser_SetIapAmount_m1127847452 (SPUser_t2606281602 * __this, float ___iap_amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SponsorPay.SPUser::GetNumberOfSessions()
extern "C"  Nullable_1_t1438485399  SPUser_GetNumberOfSessions_m3134201219 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetNumberOfSessions(System.Int32)
extern "C"  void SPUser_SetNumberOfSessions_m1195833073 (SPUser_t2606281602 * __this, int32_t ___numberOfSessions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int64> SponsorPay.SPUser::GetPsTime()
extern "C"  Nullable_1_t1438485494  SPUser_GetPsTime_m1310114581 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetPsTime(System.Int64)
extern "C"  void SPUser_SetPsTime_m1189175141 (SPUser_t2606281602 * __this, int64_t ___ps_time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int64> SponsorPay.SPUser::GetLastSession()
extern "C"  Nullable_1_t1438485494  SPUser_GetLastSession_m731060221 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetLastSession(System.Int64)
extern "C"  void SPUser_SetLastSession_m2129404177 (SPUser_t2606281602 * __this, int64_t ___lastSession0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserConnection> SponsorPay.SPUser::GetConnection()
extern "C"  Nullable_1_t2936825364  SPUser_GetConnection_m1316617327 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetConnection(SponsorPay.SPUserConnection)
extern "C"  void SPUser_SetConnection_m3433797803 (SPUser_t2606281602 * __this, int32_t ___connection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SPUser::GetDevice()
extern "C"  String_t* SPUser_GetDevice_m2566962768 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetDevice(System.String)
extern "C"  void SPUser_SetDevice_m2558191401 (SPUser_t2606281602 * __this, String_t* ___device0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SPUser::GetAppVersion()
extern "C"  String_t* SPUser_GetAppVersion_m703838897 (SPUser_t2606281602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::SetAppVersion(System.String)
extern "C"  void SPUser_SetAppVersion_m3390422248 (SPUser_t2606281602 * __this, String_t* ___appVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::PutCustomValue(System.String,System.String)
extern "C"  void SPUser_PutCustomValue_m2352911852 (SPUser_t2606281602 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SPUser::GetCustomValue(System.String)
extern "C"  String_t* SPUser_GetCustomValue_m3060034394 (SPUser_t2606281602 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPUser::Put(System.String,System.Object)
extern "C"  void SPUser_Put_m68480992 (SPUser_t2606281602 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SPUser::GeneratePutJsonString(System.String,System.Object)
extern "C"  String_t* SPUser_GeneratePutJsonString_m3568165145 (SPUser_t2606281602 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SPUser::GenerateGetJsonString(System.String)
extern "C"  String_t* SPUser_GenerateGetJsonString_m3568260900 (SPUser_t2606281602 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
