﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.GA_SpecialEvents/<CheckCriticalFPSRoutine>c__Iterator3
struct U3CCheckCriticalFPSRoutineU3Ec__Iterator3_t160194138;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameAnalyticsSDK.GA_SpecialEvents/<CheckCriticalFPSRoutine>c__Iterator3::.ctor()
extern "C"  void U3CCheckCriticalFPSRoutineU3Ec__Iterator3__ctor_m3108691531 (U3CCheckCriticalFPSRoutineU3Ec__Iterator3_t160194138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameAnalyticsSDK.GA_SpecialEvents/<CheckCriticalFPSRoutine>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckCriticalFPSRoutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m813229799 (U3CCheckCriticalFPSRoutineU3Ec__Iterator3_t160194138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameAnalyticsSDK.GA_SpecialEvents/<CheckCriticalFPSRoutine>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckCriticalFPSRoutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1644927099 (U3CCheckCriticalFPSRoutineU3Ec__Iterator3_t160194138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.GA_SpecialEvents/<CheckCriticalFPSRoutine>c__Iterator3::MoveNext()
extern "C"  bool U3CCheckCriticalFPSRoutineU3Ec__Iterator3_MoveNext_m2590825417 (U3CCheckCriticalFPSRoutineU3Ec__Iterator3_t160194138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents/<CheckCriticalFPSRoutine>c__Iterator3::Dispose()
extern "C"  void U3CCheckCriticalFPSRoutineU3Ec__Iterator3_Dispose_m2348573640 (U3CCheckCriticalFPSRoutineU3Ec__Iterator3_t160194138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents/<CheckCriticalFPSRoutine>c__Iterator3::Reset()
extern "C"  void U3CCheckCriticalFPSRoutineU3Ec__Iterator3_Reset_m755124472 (U3CCheckCriticalFPSRoutineU3Ec__Iterator3_t160194138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
