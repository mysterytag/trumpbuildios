﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// UniRx.ReactiveProperty`1<VKProfileInfo>
struct ReactiveProperty_1_t4081982704;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditorVkConnector
struct  EditorVkConnector_t1520390475  : public Il2CppObject
{
public:
	// UniRx.ReactiveProperty`1<System.Boolean> EditorVkConnector::<Connected>k__BackingField
	ReactiveProperty_1_t819338379 * ___U3CConnectedU3Ek__BackingField_0;
	// UniRx.ReactiveProperty`1<VKProfileInfo> EditorVkConnector::<VkProfile>k__BackingField
	ReactiveProperty_1_t4081982704 * ___U3CVkProfileU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CConnectedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EditorVkConnector_t1520390475, ___U3CConnectedU3Ek__BackingField_0)); }
	inline ReactiveProperty_1_t819338379 * get_U3CConnectedU3Ek__BackingField_0() const { return ___U3CConnectedU3Ek__BackingField_0; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_U3CConnectedU3Ek__BackingField_0() { return &___U3CConnectedU3Ek__BackingField_0; }
	inline void set_U3CConnectedU3Ek__BackingField_0(ReactiveProperty_1_t819338379 * value)
	{
		___U3CConnectedU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConnectedU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CVkProfileU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EditorVkConnector_t1520390475, ___U3CVkProfileU3Ek__BackingField_1)); }
	inline ReactiveProperty_1_t4081982704 * get_U3CVkProfileU3Ek__BackingField_1() const { return ___U3CVkProfileU3Ek__BackingField_1; }
	inline ReactiveProperty_1_t4081982704 ** get_address_of_U3CVkProfileU3Ek__BackingField_1() { return &___U3CVkProfileU3Ek__BackingField_1; }
	inline void set_U3CVkProfileU3Ek__BackingField_1(ReactiveProperty_1_t4081982704 * value)
	{
		___U3CVkProfileU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVkProfileU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
