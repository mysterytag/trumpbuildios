﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.Collections.ArrayList
struct ArrayList_t2121638921;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"

// System.Void System.Xml.XPath.FunctionArguments::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.FunctionArguments)
extern "C"  void FunctionArguments__ctor_m3611561738 (FunctionArguments_t3402201403 * __this, Expression_t4217024437 * ___arg0, FunctionArguments_t3402201403 * ___tail1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.Expression System.Xml.XPath.FunctionArguments::get_Arg()
extern "C"  Expression_t4217024437 * FunctionArguments_get_Arg_m969707333 (FunctionArguments_t3402201403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.FunctionArguments System.Xml.XPath.FunctionArguments::get_Tail()
extern "C"  FunctionArguments_t3402201403 * FunctionArguments_get_Tail_m52932169 (FunctionArguments_t3402201403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.FunctionArguments::ToArrayList(System.Collections.ArrayList)
extern "C"  void FunctionArguments_ToArrayList_m227079145 (FunctionArguments_t3402201403 * __this, ArrayList_t2121638921 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
