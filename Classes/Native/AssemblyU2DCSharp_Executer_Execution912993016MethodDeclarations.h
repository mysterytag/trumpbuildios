﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Executer/Execution
struct Execution_t912993016;

#include "codegen/il2cpp-codegen.h"

// System.Void Executer/Execution::.ctor()
extern "C"  void Execution__ctor_m136751157 (Execution_t912993016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Executer/Execution::Stop()
extern "C"  void Execution_Stop_m2880354801 (Execution_t912993016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Executer/Execution::get_finished()
extern "C"  bool Execution_get_finished_m2711171710 (Execution_t912993016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
