﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementDescription
struct AchievementDescription_t2323334509;

#include "codegen/il2cpp-codegen.h"

// System.Void AchievementDescription::.ctor()
extern "C"  void AchievementDescription__ctor_m2107881934 (AchievementDescription_t2323334509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
