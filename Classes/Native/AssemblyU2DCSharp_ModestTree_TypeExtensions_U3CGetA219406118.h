﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1144794227;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
struct IEnumerator_1_t2648036230;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F
struct  U3CGetAllFieldsU3Ec__Iterator3F_t219406118  : public Il2CppObject
{
public:
	// System.Reflection.BindingFlags ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::flags
	int32_t ___flags_0;
	// System.Type ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::type
	Type_t * ___type_1;
	// System.Reflection.FieldInfo[] ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<$s_244>__0
	FieldInfoU5BU5D_t1144794227* ___U3CU24s_244U3E__0_2;
	// System.Int32 ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<$s_245>__1
	int32_t ___U3CU24s_245U3E__1_3;
	// System.Reflection.FieldInfo ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<fieldInfo>__2
	FieldInfo_t * ___U3CfieldInfoU3E__2_4;
	// System.Reflection.FieldInfo[] ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<$s_246>__3
	FieldInfoU5BU5D_t1144794227* ___U3CU24s_246U3E__3_5;
	// System.Int32 ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<$s_247>__4
	int32_t ___U3CU24s_247U3E__4_6;
	// System.Reflection.FieldInfo ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<fieldInfo>__5
	FieldInfo_t * ___U3CfieldInfoU3E__5_7;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo> ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<$s_248>__6
	Il2CppObject* ___U3CU24s_248U3E__6_8;
	// System.Reflection.FieldInfo ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<fieldInfo>__7
	FieldInfo_t * ___U3CfieldInfoU3E__7_9;
	// System.Int32 ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::$PC
	int32_t ___U24PC_10;
	// System.Reflection.FieldInfo ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::$current
	FieldInfo_t * ___U24current_11;
	// System.Reflection.BindingFlags ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<$>flags
	int32_t ___U3CU24U3Eflags_12;
	// System.Type ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::<$>type
	Type_t * ___U3CU24U3Etype_13;

public:
	inline static int32_t get_offset_of_flags_0() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___flags_0)); }
	inline int32_t get_flags_0() const { return ___flags_0; }
	inline int32_t* get_address_of_flags_0() { return &___flags_0; }
	inline void set_flags_0(int32_t value)
	{
		___flags_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_244U3E__0_2() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CU24s_244U3E__0_2)); }
	inline FieldInfoU5BU5D_t1144794227* get_U3CU24s_244U3E__0_2() const { return ___U3CU24s_244U3E__0_2; }
	inline FieldInfoU5BU5D_t1144794227** get_address_of_U3CU24s_244U3E__0_2() { return &___U3CU24s_244U3E__0_2; }
	inline void set_U3CU24s_244U3E__0_2(FieldInfoU5BU5D_t1144794227* value)
	{
		___U3CU24s_244U3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_244U3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_245U3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CU24s_245U3E__1_3)); }
	inline int32_t get_U3CU24s_245U3E__1_3() const { return ___U3CU24s_245U3E__1_3; }
	inline int32_t* get_address_of_U3CU24s_245U3E__1_3() { return &___U3CU24s_245U3E__1_3; }
	inline void set_U3CU24s_245U3E__1_3(int32_t value)
	{
		___U3CU24s_245U3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CfieldInfoU3E__2_4() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CfieldInfoU3E__2_4)); }
	inline FieldInfo_t * get_U3CfieldInfoU3E__2_4() const { return ___U3CfieldInfoU3E__2_4; }
	inline FieldInfo_t ** get_address_of_U3CfieldInfoU3E__2_4() { return &___U3CfieldInfoU3E__2_4; }
	inline void set_U3CfieldInfoU3E__2_4(FieldInfo_t * value)
	{
		___U3CfieldInfoU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfieldInfoU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_246U3E__3_5() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CU24s_246U3E__3_5)); }
	inline FieldInfoU5BU5D_t1144794227* get_U3CU24s_246U3E__3_5() const { return ___U3CU24s_246U3E__3_5; }
	inline FieldInfoU5BU5D_t1144794227** get_address_of_U3CU24s_246U3E__3_5() { return &___U3CU24s_246U3E__3_5; }
	inline void set_U3CU24s_246U3E__3_5(FieldInfoU5BU5D_t1144794227* value)
	{
		___U3CU24s_246U3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_246U3E__3_5, value);
	}

	inline static int32_t get_offset_of_U3CU24s_247U3E__4_6() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CU24s_247U3E__4_6)); }
	inline int32_t get_U3CU24s_247U3E__4_6() const { return ___U3CU24s_247U3E__4_6; }
	inline int32_t* get_address_of_U3CU24s_247U3E__4_6() { return &___U3CU24s_247U3E__4_6; }
	inline void set_U3CU24s_247U3E__4_6(int32_t value)
	{
		___U3CU24s_247U3E__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CfieldInfoU3E__5_7() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CfieldInfoU3E__5_7)); }
	inline FieldInfo_t * get_U3CfieldInfoU3E__5_7() const { return ___U3CfieldInfoU3E__5_7; }
	inline FieldInfo_t ** get_address_of_U3CfieldInfoU3E__5_7() { return &___U3CfieldInfoU3E__5_7; }
	inline void set_U3CfieldInfoU3E__5_7(FieldInfo_t * value)
	{
		___U3CfieldInfoU3E__5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfieldInfoU3E__5_7, value);
	}

	inline static int32_t get_offset_of_U3CU24s_248U3E__6_8() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CU24s_248U3E__6_8)); }
	inline Il2CppObject* get_U3CU24s_248U3E__6_8() const { return ___U3CU24s_248U3E__6_8; }
	inline Il2CppObject** get_address_of_U3CU24s_248U3E__6_8() { return &___U3CU24s_248U3E__6_8; }
	inline void set_U3CU24s_248U3E__6_8(Il2CppObject* value)
	{
		___U3CU24s_248U3E__6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_248U3E__6_8, value);
	}

	inline static int32_t get_offset_of_U3CfieldInfoU3E__7_9() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CfieldInfoU3E__7_9)); }
	inline FieldInfo_t * get_U3CfieldInfoU3E__7_9() const { return ___U3CfieldInfoU3E__7_9; }
	inline FieldInfo_t ** get_address_of_U3CfieldInfoU3E__7_9() { return &___U3CfieldInfoU3E__7_9; }
	inline void set_U3CfieldInfoU3E__7_9(FieldInfo_t * value)
	{
		___U3CfieldInfoU3E__7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfieldInfoU3E__7_9, value);
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U24current_11)); }
	inline FieldInfo_t * get_U24current_11() const { return ___U24current_11; }
	inline FieldInfo_t ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(FieldInfo_t * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eflags_12() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CU24U3Eflags_12)); }
	inline int32_t get_U3CU24U3Eflags_12() const { return ___U3CU24U3Eflags_12; }
	inline int32_t* get_address_of_U3CU24U3Eflags_12() { return &___U3CU24U3Eflags_12; }
	inline void set_U3CU24U3Eflags_12(int32_t value)
	{
		___U3CU24U3Eflags_12 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_13() { return static_cast<int32_t>(offsetof(U3CGetAllFieldsU3Ec__Iterator3F_t219406118, ___U3CU24U3Etype_13)); }
	inline Type_t * get_U3CU24U3Etype_13() const { return ___U3CU24U3Etype_13; }
	inline Type_t ** get_address_of_U3CU24U3Etype_13() { return &___U3CU24U3Etype_13; }
	inline void set_U3CU24U3Etype_13(Type_t * value)
	{
		___U3CU24U3Etype_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etype_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
