﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`4/<ToMethod>c__AnonStorey17A<System.Object,System.Object,System.Object,System.Object>
struct U3CToMethodU3Ec__AnonStorey17A_t2106438637;
// Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>
struct IFactory_4_t1894544701;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`4/<ToMethod>c__AnonStorey17A<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey17A__ctor_m1059049120_gshared (U3CToMethodU3Ec__AnonStorey17A_t2106438637 * __this, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey17A__ctor_m1059049120(__this, method) ((  void (*) (U3CToMethodU3Ec__AnonStorey17A_t2106438637 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey17A__ctor_m1059049120_gshared)(__this, method)
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4/<ToMethod>c__AnonStorey17A<System.Object,System.Object,System.Object,System.Object>::<>m__2A0(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey17A_U3CU3Em__2A0_m2903502399_gshared (U3CToMethodU3Ec__AnonStorey17A_t2106438637 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey17A_U3CU3Em__2A0_m2903502399(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToMethodU3Ec__AnonStorey17A_t2106438637 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey17A_U3CU3Em__2A0_m2903502399_gshared)(__this, ___ctx0, method)
