﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>
struct U3CHoldU3Ec__AnonStoreyFF_1_t2863379238;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>::.ctor()
extern "C"  void U3CHoldU3Ec__AnonStoreyFF_1__ctor_m2711189924_gshared (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * __this, const MethodInfo* method);
#define U3CHoldU3Ec__AnonStoreyFF_1__ctor_m2711189924(__this, method) ((  void (*) (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 *, const MethodInfo*))U3CHoldU3Ec__AnonStoreyFF_1__ctor_m2711189924_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>::<>m__175(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CHoldU3Ec__AnonStoreyFF_1_U3CU3Em__175_m1395136363_gshared (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CHoldU3Ec__AnonStoreyFF_1_U3CU3Em__175_m1395136363(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CHoldU3Ec__AnonStoreyFF_1_U3CU3Em__175_m1395136363_gshared)(__this, ___reaction0, ___p1, method)
// T CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>::<>m__176()
extern "C"  Il2CppObject * U3CHoldU3Ec__AnonStoreyFF_1_U3CU3Em__176_m512190116_gshared (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * __this, const MethodInfo* method);
#define U3CHoldU3Ec__AnonStoreyFF_1_U3CU3Em__176_m512190116(__this, method) ((  Il2CppObject * (*) (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 *, const MethodInfo*))U3CHoldU3Ec__AnonStoreyFF_1_U3CU3Em__176_m512190116_gshared)(__this, method)
