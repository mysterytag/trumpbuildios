﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsRequestCallback
struct GetPlayFabIDsFromPSNAccountIDsRequestCallback_t2015930851;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest
struct GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3305934734.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequestCallback__ctor_m154998002 (GetPlayFabIDsFromPSNAccountIDsRequestCallback_t2015930851 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest,System.Object)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequestCallback_Invoke_m1072354451 (GetPlayFabIDsFromPSNAccountIDsRequestCallback_t2015930851 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromPSNAccountIDsRequestCallback_t2015930851(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromPSNAccountIDsRequestCallback_BeginInvoke_m3463746558 (GetPlayFabIDsFromPSNAccountIDsRequestCallback_t2015930851 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromPSNAccountIDsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequestCallback_EndInvoke_m2654261250 (GetPlayFabIDsFromPSNAccountIDsRequestCallback_t2015930851 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
