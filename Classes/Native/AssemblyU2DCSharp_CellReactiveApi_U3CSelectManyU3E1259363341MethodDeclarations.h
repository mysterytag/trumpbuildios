﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey119_3_t1259363341;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey119_3__ctor_m67190799_gshared (U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey119_3__ctor_m67190799(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey119_3__ctor_m67190799_gshared)(__this, method)
// ICell`1<TR> CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Object,System.Object,System.Object>::<>m__18E(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey119_3_U3CU3Em__18E_m3276304969_gshared (U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey119_3_U3CU3Em__18E_m3276304969(__this, ___x0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey119_3_U3CU3Em__18E_m3276304969_gshared)(__this, ___x0, method)
