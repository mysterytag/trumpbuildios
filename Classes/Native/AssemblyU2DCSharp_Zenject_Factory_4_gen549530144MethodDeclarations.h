﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>
struct Factory_4_t549530144;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_4__ctor_m582229268_gshared (Factory_4_t549530144 * __this, const MethodInfo* method);
#define Factory_4__ctor_m582229268(__this, method) ((  void (*) (Factory_4_t549530144 *, const MethodInfo*))Factory_4__ctor_m582229268_gshared)(__this, method)
// System.Type Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_4_get_ConstructedType_m2144061183_gshared (Factory_4_t549530144 * __this, const MethodInfo* method);
#define Factory_4_get_ConstructedType_m2144061183(__this, method) ((  Type_t * (*) (Factory_4_t549530144 *, const MethodInfo*))Factory_4_get_ConstructedType_m2144061183_gshared)(__this, method)
// System.Type[] Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_4_get_ProvidedTypes_m915303975_gshared (Factory_4_t549530144 * __this, const MethodInfo* method);
#define Factory_4_get_ProvidedTypes_m915303975(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_4_t549530144 *, const MethodInfo*))Factory_4_get_ProvidedTypes_m915303975_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_4_get_Container_m1124560372_gshared (Factory_4_t549530144 * __this, const MethodInfo* method);
#define Factory_4_get_Container_m1124560372(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_4_t549530144 *, const MethodInfo*))Factory_4_get_Container_m1124560372_gshared)(__this, method)
// TValue Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3)
extern "C"  Il2CppObject * Factory_4_Create_m626144115_gshared (Factory_4_t549530144 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, const MethodInfo* method);
#define Factory_4_Create_m626144115(__this, ___param10, ___param21, ___param32, method) ((  Il2CppObject * (*) (Factory_4_t549530144 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_4_Create_m626144115_gshared)(__this, ___param10, ___param21, ___param32, method)
