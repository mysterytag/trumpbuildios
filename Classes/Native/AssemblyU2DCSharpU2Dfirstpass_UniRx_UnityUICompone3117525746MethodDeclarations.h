﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA7
struct U3COnValueChangedAsObservableU3Ec__AnonStoreyA7_t3117525746;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA7::.ctor()
extern "C"  void U3COnValueChangedAsObservableU3Ec__AnonStoreyA7__ctor_m2381832204 (U3COnValueChangedAsObservableU3Ec__AnonStoreyA7_t3117525746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA7::<>m__E5(UniRx.IObserver`1<System.Boolean>)
extern "C"  Il2CppObject * U3COnValueChangedAsObservableU3Ec__AnonStoreyA7_U3CU3Em__E5_m103283191 (U3COnValueChangedAsObservableU3Ec__AnonStoreyA7_t3117525746 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
