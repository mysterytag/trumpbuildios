﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RangeObservable/Range
struct Range_t78727453;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.RangeObservable/Range::.ctor(UniRx.IObserver`1<System.Int32>,System.IDisposable)
extern "C"  void Range__ctor_m976364437 (Range_t78727453 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.RangeObservable/Range::OnNext(System.Int32)
extern "C"  void Range_OnNext_m3042627079 (Range_t78727453 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.RangeObservable/Range::OnError(System.Exception)
extern "C"  void Range_OnError_m2670562679 (Range_t78727453 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.RangeObservable/Range::OnCompleted()
extern "C"  void Range_OnCompleted_m151223114 (Range_t78727453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
