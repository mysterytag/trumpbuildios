﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.AggregateObservable`2<System.Object,System.Object>
struct AggregateObservable_2_t1747477946;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>
struct  Aggregate_t1831847732  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.AggregateObservable`2<TSource,TAccumulate> UniRx.Operators.AggregateObservable`2/Aggregate::parent
	AggregateObservable_2_t1747477946 * ___parent_2;
	// TAccumulate UniRx.Operators.AggregateObservable`2/Aggregate::accumulation
	Il2CppObject * ___accumulation_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Aggregate_t1831847732, ___parent_2)); }
	inline AggregateObservable_2_t1747477946 * get_parent_2() const { return ___parent_2; }
	inline AggregateObservable_2_t1747477946 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(AggregateObservable_2_t1747477946 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_accumulation_3() { return static_cast<int32_t>(offsetof(Aggregate_t1831847732, ___accumulation_3)); }
	inline Il2CppObject * get_accumulation_3() const { return ___accumulation_3; }
	inline Il2CppObject ** get_address_of_accumulation_3() { return &___accumulation_3; }
	inline void set_accumulation_3(Il2CppObject * value)
	{
		___accumulation_3 = value;
		Il2CppCodeGenWriteBarrier(&___accumulation_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
