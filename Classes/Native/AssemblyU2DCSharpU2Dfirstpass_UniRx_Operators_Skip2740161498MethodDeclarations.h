﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipUntilObservable`2<System.Object,System.Object>
struct SkipUntilObservable_2_t2740161498;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SkipUntilObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<TOther>)
extern "C"  void SkipUntilObservable_2__ctor_m2822742818_gshared (SkipUntilObservable_2_t2740161498 * __this, Il2CppObject* ___source0, Il2CppObject* ___other1, const MethodInfo* method);
#define SkipUntilObservable_2__ctor_m2822742818(__this, ___source0, ___other1, method) ((  void (*) (SkipUntilObservable_2_t2740161498 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))SkipUntilObservable_2__ctor_m2822742818_gshared)(__this, ___source0, ___other1, method)
// System.IDisposable UniRx.Operators.SkipUntilObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SkipUntilObservable_2_SubscribeCore_m706503804_gshared (SkipUntilObservable_2_t2740161498 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SkipUntilObservable_2_SubscribeCore_m706503804(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SkipUntilObservable_2_t2740161498 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SkipUntilObservable_2_SubscribeCore_m706503804_gshared)(__this, ___observer0, ___cancel1, method)
