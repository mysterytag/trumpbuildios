﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// InstantiationPool`1<System.Object>
struct InstantiationPool_1_t1380750307;
// InstantiatableInPool
struct InstantiatableInPool_t1882737686;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Object>>
struct Dictionary_2_t3271763293;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3824425150;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// LitJson.ExporterFunc`1<System.Object>
struct ExporterFunc_1_t4015841116;
// LitJson.JsonWriter
struct JsonWriter_t3021344960;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// LitJson.ImporterFunc`2<System.Object,System.Object>
struct ImporterFunc_2_t2649868222;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey153`1<System.Object>
struct U3CRegisterExporterU3Ec__AnonStorey153_1_t909718040;
// LitJson.JsonMapper/<RegisterImporter>c__AnonStorey154`2<System.Object,System.Object>
struct U3CRegisterImporterU3Ec__AnonStorey154_2_t3095985135;
// ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>
struct U3CAppendU3Ec__Iterator36_1_t3337881070;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>
struct U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091;
// ModestTree.LinqExtensions/<OfType>c__AnonStorey16C`1<System.Object>
struct U3COfTypeU3Ec__AnonStorey16C_1_t3821260938;
// ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>
struct U3CPrependU3Ec__Iterator37_1_t1884925155;
// ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>
struct U3CPrependU3Ec__Iterator38_1_t1439900156;
// ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>
struct U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384;
// ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator3A_1_t3575002786;
// ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator3B_1_t3129977787;
// ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>
struct U3CYieldU3Ec__Iterator3D_1_t3174792911;
// ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_10_t941656758;
// ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_6_t698299427;
// ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_7_t3735803334;
// ModestTree.Util.Func`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_8_t2205681617;
// ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_9_t38423544;
// ModestTree.Util.Tuple`2<System.Object,System.Int32>
struct Tuple_2_t299352770;
// ModestTree.Util.Tuple`2<System.Object,System.Object>
struct Tuple_2_t2584011699;
// ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>
struct Tuple_3_t1160054790;
// OnceStream`1<System.Object>
struct OnceStream_1_t151376059;
// Organism`2/<Setup>c__AnonStorey11F<System.Object,System.Object>
struct U3CSetupU3Ec__AnonStorey11F_t4199016171;
// IOrganism
struct IOrganism_t2580843579;
// Organism`2<System.Object,System.Object>
struct Organism_2_t948289219;
// IOnceEmptyStream
struct IOnceEmptyStream_t172682851;
// System.IDisposable
struct IDisposable_t1628921374;
// IRoot
struct IRoot_t69970123;
// PlayFab.Internal.ResultContainer`1<System.Object>
struct ResultContainer_1_t1789381198;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;
// System.Delegate
struct Delegate_t3660574010;
// PlayFab.ErrorCallback
struct ErrorCallback_t582108558;
// System.Action`2<System.Object,PlayFab.CallRequestContainer>
struct Action_2_t3700672107;
// PlayFab.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t3082203415;
// PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>
struct SingletonMonoBehaviour_1_t312324993;
// PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>
struct ProcessApiCallback_1_t3425006930;
// PlayFab.PlayFabSettings/RequestCallback`1<System.Object>
struct RequestCallback_1_t923153878;
// PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>
struct ResponseCallback_2_t3240762047;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t1163962996;
// PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t1771086527;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t346249057;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;
// ReactiveUI/<SetContent>c__AnonStorey120`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey120_1_t1908708797;
// ReactiveUI/<SetContent>c__AnonStorey121`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey121_1_t1463683798;
// ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey123_1_t573633800;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// ReactiveUI/<SetContent>c__AnonStorey122`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey122_1_t1018658799;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerable`1<System.Func`2<System.Object,System.Object>>
struct IEnumerable_1_t712970413;
// System.Func`2<System.Func`2<System.Object,System.Object>,System.Object>
struct Func_2_t1968536420;
// ReactiveUI/<SetContent>c__AnonStorey124`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey124_1_t128608801;
// System.Collections.IEnumerable
struct IEnumerable_t287189635;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Boolean>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.DateTime>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Double>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int32>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int64>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Object>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Single>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.CollectionAddEvent`1<System.Object>>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002;
// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.Unit>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053;
// Reactor`1/Subscription<System.Boolean>
struct Subscription_t2285219160;
// Reactor`1/Subscription<System.DateTime>
struct Subscription_t2413247754;
// Reactor`1/Subscription<System.Double>
struct Subscription_t2608730433;
// Reactor`1/Subscription<System.Int32>
struct Subscription_t626661310;
// Reactor`1/Subscription<System.Int64>
struct Subscription_t626661405;
// Reactor`1/Subscription<System.Object>
struct Subscription_t2911320242;
// Reactor`1/Subscription<System.Single>
struct Subscription_t3032422840;
// Reactor`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>
struct Subscription_t195768510;
// Reactor`1/Subscription<UniRx.Unit>
struct Subscription_t337532562;
// Reactor`1<System.Boolean>
struct Reactor_1_t3765777533;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// Reactor`1<System.DateTime>
struct Reactor_1_t3893806128;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// Reactor`1<System.Double>
struct Reactor_1_t4089288806;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// Reactor`1<System.Int32>
struct Reactor_1_t2107219683;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// Reactor`1<System.Int64>
struct Reactor_1_t2107219778;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// Reactor`1<System.Object>
struct Reactor_1_t96911316;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// Reactor`1<System.Single>
struct Reactor_1_t218013917;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Reactor_1_t1676326883;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t2564974692;
// Reactor`1<UniRx.Unit>
struct Reactor_1_t1818090934;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// Root`2<System.Object,System.Object>
struct Root_2_t2363348211;
// SerializableDictionary`2<System.Object,System.Object>
struct SerializableDictionary_2_t2269426764;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t1932230565;
// System.Xml.XmlReader
struct XmlReader_t4229084514;
// System.Xml.XmlWriter
struct XmlWriter_t89522450;
// Singleton`1<System.Object>
struct Singleton_1_t1089921813;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>
struct JsonResponse_1_t4259692931;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>
struct JsonResponse_1_t1322443133;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>
struct JsonResponse_1_t811794004;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>
struct JsonResponse_1_t3397282150;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>
struct JsonResponse_1_t3715343323;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>
struct JsonResponse_1_t815460875;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>
struct JsonResponse_1_t124943520;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>
struct JsonResponse_1_t448454793;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>
struct JsonResponse_1_t2761352966;
// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>
struct JsonResponse_1_t2761353061;
// SponsorPay.SPUser/JsonResponse`1<System.Object>
struct JsonResponse_1_t2159973987;
// StaticCell`1<System.Object>
struct StaticCell_1_t175558834;
// System.Action
struct Action_t437523947;
// Stream`1/<Listen>c__AnonStorey128<System.Boolean>
struct U3CListenU3Ec__AnonStorey128_t3737332661;
// Stream`1/<Listen>c__AnonStorey128<System.DateTime>
struct U3CListenU3Ec__AnonStorey128_t3865361256;
// Stream`1/<Listen>c__AnonStorey128<System.Double>
struct U3CListenU3Ec__AnonStorey128_t4060843934;
// Stream`1/<Listen>c__AnonStorey128<System.Int32>
struct U3CListenU3Ec__AnonStorey128_t2078774811;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_InstantiationPool_1_gen1380750307.h"
#include "AssemblyU2DCSharp_InstantiationPool_1_gen1380750307MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1355425710MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3271763293.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3271763293MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1355425710.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22760294591.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3038791234.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3038791234MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22760294591MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharp_InstantiatableInPool1882737686.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "AssemblyU2DCSharp_ConnectionCollector444796719MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Utils22543841729MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils22543841729.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_ConnectionCollector444796719.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "UnityEngine_UnityEngine_Resources1543782994.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen4015841116.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen4015841116MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter3021344960.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc_2_gen2649868222.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc_2_gen2649868222MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterExp909718040.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterExp909718040MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterIm3095985135.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterIm3095985135MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CApp3337881070.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CApp3337881070MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CDis2613741091.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CDis2613741091MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g3535795091.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g3535795091MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3COfT3821260938.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3COfT3821260938MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CPre1884925155.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CPre1884925155MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CPre1439900156.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CPre1439900156MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CRep3587000384.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CRep3587000384MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1408070318.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CToE3575002786.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CToE3575002786MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CToE3129977787.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions_U3CToE3129977787MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_MiscExtensions_U3CYie3174792911.h"
#include "AssemblyU2DCSharp_ModestTree_MiscExtensions_U3CYie3174792911MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_10_gen941656758.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_10_gen941656758MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_6_gen698299427.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_6_gen698299427MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_7_gen3735803334.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_7_gen3735803334MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_8_gen2205681617.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_8_gen2205681617MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_9_gen38423544.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_9_gen38423544MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen299352770.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen299352770MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen2584011699.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen2584011699MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_3_gen1160054790.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_3_gen1160054790MethodDeclarations.h"
#include "AssemblyU2DCSharp_OnceStream_1_gen151376059.h"
#include "AssemblyU2DCSharp_OnceStream_1_gen151376059MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738.h"
#include "AssemblyU2DCSharp_Organism_2_U3CSetupU3Ec__AnonSto4199016171.h"
#include "AssemblyU2DCSharp_Organism_2_U3CSetupU3Ec__AnonSto4199016171MethodDeclarations.h"
#include "AssemblyU2DCSharp_Organism_2_gen948289219.h"
#include "AssemblyU2DCSharp_Organism_2_gen948289219MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3377802548MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3377802548.h"
#include "AssemblyU2DCSharp_OnceEmptyStream3081939404MethodDeclarations.h"
#include "AssemblyU2DCSharp_OnceEmptyStream3081939404.h"
#include "mscorlib_System_Action_1_gen2729296284MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2729296284.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425880343MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425880343.h"
#include "mscorlib_System_Action_1_gen1777374079MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmptyStream2850978573MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1777374079.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_ResultContainer1789381198.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_ResultContainer1789381198MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1438485399MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3403145775.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_ErrorCallback582108558.h"
#include "System_Core_System_Action_2_gen3700672107.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabSettings4113663895MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_ErrorCallback582108558MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_Util1193378922.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_Util1193378922MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_Util_MyJsonSeria989142809.h"
#include "AssemblyU2DCSharp_PlayFab_SimpleJson1409256731MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_SimpleJson1409256731.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"
#include "System_Core_System_Action_2_gen3700672107MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError2114648451.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabSettings4113663895.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_SingletonMonoBeh312324993.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_SingletonMonoBeh312324993MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabClientAPI_Process3425006930.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabClientAPI_Process3425006930MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabSettings_RequestCa923153878.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabSettings_RequestCa923153878MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabSettings_Response3240762047.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabSettings_Response3240762047MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1163962996.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1163962996MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1771086527.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3824425150.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3824425150MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1771086527MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1091014741MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1091014741.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1852733134.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1451594948.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__An1908708797.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__An1908708797MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__An1463683798.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__An1463683798MethodDeclarations.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__Ano573633800.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__Ano573633800MethodDeclarations.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__An1018658799.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__An1018658799MethodDeclarations.h"
#include "System.Core_ArrayTypes.h"
#include "System_Core_System_Func_2_gen1968536420.h"
#include "System_Core_System_Func_2_gen1968536420MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__Ano128608801.h"
#include "AssemblyU2DCSharp_ReactiveUI_U3CSetContentU3Ec__Ano128608801MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe2538948652.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe2538948652MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen4157809524.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen4157809524MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe2666977247.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe2666977247MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen487486641.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen4285838119.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen4285838119MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Action_1_gen487486641MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe2862459925.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe2862459925MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen682969319.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen186353501.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen186353501MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Action_1_gen682969319MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRea880390802.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRea880390802MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2499251674.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2499251674MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRea880390897.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRea880390897MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2499251769.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2499251769MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Action_1_gen2995867587MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe3165049731.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe3165049731MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen488943307.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen488943307MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe3286152332.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRe3286152332MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1106661726.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen610045908.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen610045908MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1106661726MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRea449498002.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRea449498002MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2564974692.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2068358874.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2068358874MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "mscorlib_System_Action_1_gen2564974692MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRea591262053.h"
#include "AssemblyU2DCSharp_Reactor_1_U3CTransactionUnpackRea591262053MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2210122925.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2210122925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Action_1_gen2706738743MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen2285219159.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen2285219159MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen3765777533.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "AssemblyU2DCSharp_Reactor_1_gen3765777533MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen2413247754.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen2413247754MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen3893806128.h"
#include "AssemblyU2DCSharp_Reactor_1_gen3893806128MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen2608730432.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen2608730432MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen4089288806.h"
#include "AssemblyU2DCSharp_Reactor_1_gen4089288806MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen626661309.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen626661309MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen2107219683.h"
#include "AssemblyU2DCSharp_Reactor_1_gen2107219683MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen626661404.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen626661404MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen2107219778.h"
#include "AssemblyU2DCSharp_Reactor_1_gen2107219778MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen2911320238.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen2911320238MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen3032422839.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen3032422839MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen218013917.h"
#include "AssemblyU2DCSharp_Reactor_1_gen218013917MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen195768509.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen195768509MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1676326883.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1676326883MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen337532560.h"
#include "AssemblyU2DCSharp_Reactor_1_Subscription_gen337532560MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1818090934.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1818090934MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "AssemblyU2DCSharp_Transaction3809114814MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Root_2_gen2363348211.h"
#include "AssemblyU2DCSharp_Root_2_gen2363348211MethodDeclarations.h"
#include "AssemblyU2DCSharp_SerializableDictionary_2_gen2269426764.h"
#include "AssemblyU2DCSharp_SerializableDictionary_2_gen2269426764MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "System_Xml_System_Xml_Schema_XmlSchema1932230565.h"
#include "System_Xml_System_Xml_XmlReader4229084514.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializer1888860807MethodDeclarations.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializer1888860807.h"
#include "System_Xml_System_Xml_XmlReader4229084514MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNodeType3966624571.h"
#include "System_Xml_System_Xml_XmlWriter89522450.h"
#include "System_Xml_System_Xml_XmlWriter89522450MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3591453091.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1852733134MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3591453091MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton_1_gen1089921813.h"
#include "AssemblyU2DCSharp_Singleton_1_gen1089921813MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_14259692931.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_14259692931MethodDeclarations.h"
#include "AssemblyU2DCSharp_SponsorPay_AbstractResponse2918001245MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2936825364.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_11322443133.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_11322443133MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen4294542862.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_1_811794004.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_1_811794004MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3783893733.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_13397282150.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_13397282150MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2074414583.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_13715343323.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_13715343323MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2392475756.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_1_815460875.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_1_815460875MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3787560604.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_1_124943520.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_1_124943520MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_1_448454793.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_1_448454793MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3420554522.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_12761352966.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_12761352966MethodDeclarations.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_12761353061.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_12761353061MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1438485494.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_12159973987.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUser_JsonResponse_12159973987MethodDeclarations.h"
#include "AssemblyU2DCSharp_StaticCell_1_gen175558834.h"
#include "AssemblyU2DCSharp_StaticCell_1_gen175558834MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_EmptyDisposable1512564738MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_EmptyDisposable1512564738.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor3737332661.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor3737332661MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor3865361256.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor3865361256MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor4060843934.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor4060843934MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor2078774811.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor2078774811MethodDeclarations.h"

// !!1 Utils2::TryGetOrNew<System.Object,System.Object>(System.Collections.Generic.Dictionary`2<!!0,!!1>,!!0)
extern "C"  Il2CppObject * Utils2_TryGetOrNew_TisIl2CppObject_TisIl2CppObject_m2106141314_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t3824425150 * p0, Il2CppObject * p1, const MethodInfo* method);
#define Utils2_TryGetOrNew_TisIl2CppObject_TisIl2CppObject_m2106141314(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t3824425150 *, Il2CppObject *, const MethodInfo*))Utils2_TryGetOrNew_TisIl2CppObject_TisIl2CppObject_m2106141314_gshared)(__this /* static, unused */, p0, p1, method)
// !!1 Utils2::TryGetOrNew<System.String,System.Collections.Generic.List`1<System.Object>>(System.Collections.Generic.Dictionary`2<!!0,!!1>,!!0)
#define Utils2_TryGetOrNew_TisString_t_TisList_1_t1634065389_m3398629262(__this /* static, unused */, p0, p1, method) ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t3271763293 *, String_t*, const MethodInfo*))Utils2_TryGetOrNew_TisIl2CppObject_TisIl2CppObject_m2106141314_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 Utils2::Take<System.Object>(System.Collections.Generic.List`1<!!0>,System.Int32)
extern "C"  Il2CppObject * Utils2_Take_TisIl2CppObject_m3992767577_gshared (Il2CppObject * __this /* static, unused */, List_1_t1634065389 * p0, int32_t p1, const MethodInfo* method);
#define Utils2_Take_TisIl2CppObject_m3992767577(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, List_1_t1634065389 *, int32_t, const MethodInfo*))Utils2_Take_TisIl2CppObject_m3992767577_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2208345422_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2208345422(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Resources::Load<UnityEngine.GameObject>(System.String)
#define Resources_Load_TisGameObject_t4012695102_m1356085950(__this /* static, unused */, p0, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t4012695102_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 PlayFab.SimpleJson::DeserializeObject<System.Object>(System.String,PlayFab.IJsonSerializerStrategy)
extern "C"  Il2CppObject * SimpleJson_DeserializeObject_TisIl2CppObject_m2571737556_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method);
#define SimpleJson_DeserializeObject_TisIl2CppObject_m2571737556(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, Il2CppObject *, const MethodInfo*))SimpleJson_DeserializeObject_TisIl2CppObject_m2571737556_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 PlayFab.SimpleJson::DeserializeObject<PlayFab.Internal.ResultContainer`1<System.Object>>(System.String,PlayFab.IJsonSerializerStrategy)
#define SimpleJson_DeserializeObject_TisResultContainer_1_t1789381198_m3235695091(__this /* static, unused */, p0, p1, method) ((  ResultContainer_1_t1789381198 * (*) (Il2CppObject * /* static, unused */, String_t*, Il2CppObject *, const MethodInfo*))SimpleJson_DeserializeObject_TisIl2CppObject_m2571737556_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m1765599783_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m1765599783(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m1765599783_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3048268896(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Func`2<System.Object,System.Object>,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisFunc_2_t2135783352_TisIl2CppObject_m2439666671(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1968536420 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_Cast_TisIl2CppObject_m3904275306_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_Cast_TisIl2CppObject_m3904275306(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisIl2CppObject_m3904275306_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m1058801104(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InstantiationPool`1<System.Object>::.ctor()
extern Il2CppClass* Dictionary_2_t1355425710_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2338362006_MethodInfo_var;
extern const uint32_t InstantiationPool_1__ctor_m3774412601_MetadataUsageId;
extern "C"  void InstantiationPool_1__ctor_m3774412601_gshared (InstantiationPool_1_t1380750307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InstantiationPool_1__ctor_m3774412601_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3271763293 * L_0 = (Dictionary_2_t3271763293 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Dictionary_2_t3271763293 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_usedObjects_4(L_0);
		Dictionary_2_t3271763293 * L_1 = (Dictionary_2_t3271763293 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Dictionary_2_t3271763293 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_recycledObjects_5(L_1);
		Dictionary_2_t1355425710 * L_2 = (Dictionary_2_t1355425710 *)il2cpp_codegen_object_new(Dictionary_2_t1355425710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2338362006(L_2, /*hidden argument*/Dictionary_2__ctor_m2338362006_MethodInfo_var);
		__this->set_loadedPrefabs_6(L_2);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstantiationPool`1<System.Object>::KillAll()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t InstantiationPool_1_KillAll_m314356602_MetadataUsageId;
extern "C"  void InstantiationPool_1_KillAll_m314356602_gshared (InstantiationPool_1_t1380750307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InstantiationPool_1_KillAll_m314356602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t985559125 * V_0 = NULL;
	KeyValuePair_2_t2760294591  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t3038791234  V_2;
	memset(&V_2, 0, sizeof(V_2));
	KeyValuePair_2_t2760294591  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Enumerator_t3038791234  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Action_1_t985559125 * L_0 = ((InstantiationPool_1_t1380750307_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_U3CU3Ef__amU24cache7_7();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((InstantiationPool_1_t1380750307_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_U3CU3Ef__amU24cache7_7(L_2);
	}

IL_0018:
	{
		Action_1_t985559125 * L_3 = ((InstantiationPool_1_t1380750307_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_U3CU3Ef__amU24cache7_7();
		V_0 = (Action_1_t985559125 *)L_3;
		Dictionary_2_t3271763293 * L_4 = (Dictionary_2_t3271763293 *)__this->get_usedObjects_4();
		NullCheck((Dictionary_2_t3271763293 *)L_4);
		Enumerator_t3038791234  L_5 = ((  Enumerator_t3038791234  (*) (Dictionary_2_t3271763293 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Dictionary_2_t3271763293 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_2 = (Enumerator_t3038791234 )L_5;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0044;
		}

IL_002f:
		{
			KeyValuePair_2_t2760294591  L_6 = ((  KeyValuePair_2_t2760294591  (*) (Enumerator_t3038791234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t3038791234 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_1 = (KeyValuePair_2_t2760294591 )L_6;
			List_1_t1634065389 * L_7 = ((  List_1_t1634065389 * (*) (KeyValuePair_2_t2760294591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((KeyValuePair_2_t2760294591 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			Action_1_t985559125 * L_8 = V_0;
			NullCheck((List_1_t1634065389 *)L_7);
			((  void (*) (List_1_t1634065389 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((List_1_t1634065389 *)L_7, (Action_1_t985559125 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		}

IL_0044:
		{
			bool L_9 = ((  bool (*) (Enumerator_t3038791234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Enumerator_t3038791234 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			if (L_9)
			{
				goto IL_002f;
			}
		}

IL_0050:
		{
			IL2CPP_LEAVE(0x61, FINALLY_0055);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		Enumerator_t3038791234  L_10 = V_2;
		Enumerator_t3038791234  L_11 = L_10;
		Il2CppObject * L_12 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(85)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0061:
	{
		Dictionary_2_t3271763293 * L_13 = (Dictionary_2_t3271763293 *)__this->get_recycledObjects_5();
		NullCheck((Dictionary_2_t3271763293 *)L_13);
		Enumerator_t3038791234  L_14 = ((  Enumerator_t3038791234  (*) (Dictionary_2_t3271763293 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Dictionary_2_t3271763293 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_4 = (Enumerator_t3038791234 )L_14;
	}

IL_006e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0088;
		}

IL_0073:
		{
			KeyValuePair_2_t2760294591  L_15 = ((  KeyValuePair_2_t2760294591  (*) (Enumerator_t3038791234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t3038791234 *)(&V_4), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_3 = (KeyValuePair_2_t2760294591 )L_15;
			List_1_t1634065389 * L_16 = ((  List_1_t1634065389 * (*) (KeyValuePair_2_t2760294591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((KeyValuePair_2_t2760294591 *)(&V_3), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			Action_1_t985559125 * L_17 = V_0;
			NullCheck((List_1_t1634065389 *)L_16);
			((  void (*) (List_1_t1634065389 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((List_1_t1634065389 *)L_16, (Action_1_t985559125 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		}

IL_0088:
		{
			bool L_18 = ((  bool (*) (Enumerator_t3038791234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Enumerator_t3038791234 *)(&V_4), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			if (L_18)
			{
				goto IL_0073;
			}
		}

IL_0094:
		{
			IL2CPP_LEAVE(0xA6, FINALLY_0099);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0099;
	}

FINALLY_0099:
	{ // begin finally (depth: 1)
		Enumerator_t3038791234  L_19 = V_4;
		Enumerator_t3038791234  L_20 = L_19;
		Il2CppObject * L_21 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), &L_20);
		NullCheck((Il2CppObject *)L_21);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_21);
		IL2CPP_END_FINALLY(153)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(153)
	{
		IL2CPP_JUMP_TBL(0xA6, IL_00a6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a6:
	{
		Dictionary_2_t3271763293 * L_22 = (Dictionary_2_t3271763293 *)__this->get_usedObjects_4();
		NullCheck((Dictionary_2_t3271763293 *)L_22);
		VirtActionInvoker0::Invoke(33 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Object>>::Clear() */, (Dictionary_2_t3271763293 *)L_22);
		Dictionary_2_t3271763293 * L_23 = (Dictionary_2_t3271763293 *)__this->get_recycledObjects_5();
		NullCheck((Dictionary_2_t3271763293 *)L_23);
		VirtActionInvoker0::Invoke(33 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Object>>::Clear() */, (Dictionary_2_t3271763293 *)L_23);
		return;
	}
}
// System.Void InstantiationPool`1<System.Object>::RecycleAll()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t InstantiationPool_1_RecycleAll_m167619065_MetadataUsageId;
extern "C"  void InstantiationPool_1_RecycleAll_m167619065_gshared (InstantiationPool_1_t1380750307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InstantiationPool_1_RecycleAll_m167619065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2760294591  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3038791234  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3271763293 * L_0 = (Dictionary_2_t3271763293 *)__this->get_usedObjects_4();
		NullCheck((Dictionary_2_t3271763293 *)L_0);
		Enumerator_t3038791234  L_1 = ((  Enumerator_t3038791234  (*) (Dictionary_2_t3271763293 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Dictionary_2_t3271763293 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_1 = (Enumerator_t3038791234 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003d;
		}

IL_0011:
		{
			KeyValuePair_2_t2760294591  L_2 = ((  KeyValuePair_2_t2760294591  (*) (Enumerator_t3038791234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t3038791234 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_0 = (KeyValuePair_2_t2760294591 )L_2;
			List_1_t1634065389 * L_3 = ((  List_1_t1634065389 * (*) (KeyValuePair_2_t2760294591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((KeyValuePair_2_t2760294591 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			IntPtr_t L_4;
			L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			Action_1_t985559125 * L_5 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_5, (Il2CppObject *)__this, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			NullCheck((List_1_t1634065389 *)L_3);
			((  void (*) (List_1_t1634065389 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((List_1_t1634065389 *)L_3, (Action_1_t985559125 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			List_1_t1634065389 * L_6 = ((  List_1_t1634065389 * (*) (KeyValuePair_2_t2760294591 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((KeyValuePair_2_t2760294591 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			NullCheck((List_1_t1634065389 *)L_6);
			VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Object>::Clear() */, (List_1_t1634065389 *)L_6);
		}

IL_003d:
		{
			bool L_7 = ((  bool (*) (Enumerator_t3038791234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Enumerator_t3038791234 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0049:
		{
			IL2CPP_LEAVE(0x5A, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		Enumerator_t3038791234  L_8 = V_1;
		Enumerator_t3038791234  L_9 = L_8;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), &L_9);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005a:
	{
		return;
	}
}
// System.Void InstantiationPool`1<System.Object>::Recycle(InstantiatableInPool)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* Utils2_t2543841729_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4099123800;
extern const uint32_t InstantiationPool_1_Recycle_m3703956692_MetadataUsageId;
extern "C"  void InstantiationPool_1_Recycle_m3703956692_gshared (InstantiationPool_1_t1380750307 * __this, InstantiatableInPool_t1882737686 * ___inst0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InstantiationPool_1_Recycle_m3703956692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		InstantiatableInPool_t1882737686 * L_0 = ___inst0;
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
		Il2CppObject * L_1 = V_0;
		bool L_2 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4099123800, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		Dictionary_2_t3271763293 * L_3 = (Dictionary_2_t3271763293 *)__this->get_usedObjects_4();
		InstantiatableInPool_t1882737686 * L_4 = ___inst0;
		NullCheck(L_4);
		String_t* L_5 = (String_t*)L_4->get___prefabName_3();
		IL2CPP_RUNTIME_CLASS_INIT(Utils2_t2543841729_il2cpp_TypeInfo_var);
		List_1_t1634065389 * L_6 = ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t3271763293 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(NULL /*static, unused*/, (Dictionary_2_t3271763293 *)L_3, (String_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Il2CppObject * L_7 = V_0;
		NullCheck((List_1_t1634065389 *)L_6);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0) */, (List_1_t1634065389 *)L_6, (Il2CppObject *)L_7);
		NullCheck((Component_t2126946602 *)(*(&V_0)));
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m2112202034((Component_t2126946602 *)(*(&V_0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_8);
		GameObject_SetActive_m3538205401((GameObject_t4012695102 *)L_8, (bool)0, /*hidden argument*/NULL);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_onRecycle_0();
		if (!L_9)
		{
			goto IL_006a;
		}
	}
	{
		Action_1_t985559125 * L_10 = (Action_1_t985559125 *)__this->get_onRecycle_0();
		Il2CppObject * L_11 = V_0;
		NullCheck((Action_1_t985559125 *)L_10);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Action_1_t985559125 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
	}

IL_006a:
	{
		InstantiatableInPool_t1882737686 * L_12 = ___inst0;
		NullCheck(L_12);
		ConnectionCollector_t444796719 * L_13 = (ConnectionCollector_t444796719 *)L_12->get_connections_4();
		NullCheck((ConnectionCollector_t444796719 *)L_13);
		ConnectionCollector_DisconnectAll_m662901599((ConnectionCollector_t444796719 *)L_13, /*hidden argument*/NULL);
		Dictionary_2_t3271763293 * L_14 = (Dictionary_2_t3271763293 *)__this->get_recycledObjects_5();
		InstantiatableInPool_t1882737686 * L_15 = ___inst0;
		NullCheck(L_15);
		String_t* L_16 = (String_t*)L_15->get___prefabName_3();
		IL2CPP_RUNTIME_CLASS_INIT(Utils2_t2543841729_il2cpp_TypeInfo_var);
		List_1_t1634065389 * L_17 = ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t3271763293 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(NULL /*static, unused*/, (Dictionary_2_t3271763293 *)L_14, (String_t*)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Il2CppObject * L_18 = V_0;
		NullCheck((List_1_t1634065389 *)L_17);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_17, (Il2CppObject *)L_18);
		return;
	}
}
// T InstantiationPool`1<System.Object>::Create(System.String,UnityEngine.Transform)
extern Il2CppClass* Utils2_t2543841729_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_Load_TisGameObject_t4012695102_m1356085950_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3289427917;
extern Il2CppCodeGenString* _stringLiteral58;
extern const uint32_t InstantiationPool_1_Create_m1507201747_MetadataUsageId;
extern "C"  Il2CppObject * InstantiationPool_1_Create_m1507201747_gshared (InstantiationPool_1_t1380750307 * __this, String_t* ___prefabName0, Transform_t284553113 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InstantiationPool_1_Create_m1507201747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	List_1_t1634065389 * V_1 = NULL;
	GameObject_t4012695102 * V_2 = NULL;
	{
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
		Dictionary_2_t3271763293 * L_0 = (Dictionary_2_t3271763293 *)__this->get_recycledObjects_5();
		String_t* L_1 = ___prefabName0;
		IL2CPP_RUNTIME_CLASS_INIT(Utils2_t2543841729_il2cpp_TypeInfo_var);
		List_1_t1634065389 * L_2 = ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t3271763293 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(NULL /*static, unused*/, (Dictionary_2_t3271763293 *)L_0, (String_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_1 = (List_1_t1634065389 *)L_2;
		List_1_t1634065389 * L_3 = V_1;
		NullCheck((List_1_t1634065389 *)L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_3);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		List_1_t1634065389 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Utils2_t2543841729_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)(NULL /*static, unused*/, (List_1_t1634065389 *)L_5, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		V_0 = (Il2CppObject *)L_6;
		goto IL_00f0;
	}

IL_002d:
	{
		V_2 = (GameObject_t4012695102 *)NULL;
		Dictionary_2_t1355425710 * L_7 = (Dictionary_2_t1355425710 *)__this->get_loadedPrefabs_6();
		String_t* L_8 = ___prefabName0;
		NullCheck((Dictionary_2_t1355425710 *)L_7);
		bool L_9 = VirtFuncInvoker2< bool, String_t*, GameObject_t4012695102 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::TryGetValue(!0,!1&) */, (Dictionary_2_t1355425710 *)L_7, (String_t*)L_8, (GameObject_t4012695102 **)(&V_2));
		if (L_9)
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_10 = (String_t*)__this->get_searchFolder_3();
		String_t* L_11 = ___prefabName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)L_10, (String_t*)L_11, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_13 = Resources_Load_TisGameObject_t4012695102_m1356085950(NULL /*static, unused*/, (String_t*)L_12, /*hidden argument*/Resources_Load_TisGameObject_t4012695102_m1356085950_MethodInfo_var);
		V_2 = (GameObject_t4012695102 *)L_13;
		GameObject_t4012695102 * L_14 = V_2;
		bool L_15 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_14, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_16 = (String_t*)__this->get_searchFolder_3();
		String_t* L_17 = ___prefabName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2933632197(NULL /*static, unused*/, (String_t*)_stringLiteral3289427917, (String_t*)L_16, (String_t*)_stringLiteral58, (String_t*)L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)L_18, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
	}

IL_0082:
	{
		Dictionary_2_t1355425710 * L_19 = (Dictionary_2_t1355425710 *)__this->get_loadedPrefabs_6();
		String_t* L_20 = ___prefabName0;
		GameObject_t4012695102 * L_21 = V_2;
		NullCheck((Dictionary_2_t1355425710 *)L_19);
		VirtActionInvoker2< String_t*, GameObject_t4012695102 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::set_Item(!0,!1) */, (Dictionary_2_t1355425710 *)L_19, (String_t*)L_20, (GameObject_t4012695102 *)L_21);
	}

IL_008f:
	{
		GameObject_t4012695102 * L_22 = V_2;
		GameObject_t4012695102 * L_23 = Object_Instantiate_TisGameObject_t4012695102_m3917608929(NULL /*static, unused*/, (GameObject_t4012695102 *)L_22, /*hidden argument*/Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var);
		NullCheck((GameObject_t4012695102 *)L_23);
		Il2CppObject * L_24 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((GameObject_t4012695102 *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		V_0 = (Il2CppObject *)L_24;
		Il2CppObject * L_25 = V_0;
		String_t* L_26 = ___prefabName0;
		NullCheck(L_25);
		((InstantiatableInPool_t1882737686 *)L_25)->set___prefabName_3(L_26);
		NullCheck((Component_t2126946602 *)(*(&V_0)));
		Transform_t284553113 * L_27 = Component_get_transform_m2452535634((Component_t2126946602 *)(*(&V_0)), /*hidden argument*/NULL);
		Vector3_t3525329789  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m1243251509(&L_28, (float)(1.0f), (float)(1.0f), (float)(1.0f), /*hidden argument*/NULL);
		NullCheck((Transform_t284553113 *)L_27);
		Transform_set_localScale_m1776830461((Transform_t284553113 *)L_27, (Vector3_t3525329789 )L_28, /*hidden argument*/NULL);
		Il2CppObject * L_29 = V_0;
		NullCheck(L_29);
		((InstantiatableInPool_t1882737686 *)L_29)->set_pool_2(__this);
		Action_1_t985559125 * L_30 = (Action_1_t985559125 *)__this->get_onCreated_2();
		if (!L_30)
		{
			goto IL_00f0;
		}
	}
	{
		Action_1_t985559125 * L_31 = (Action_1_t985559125 *)__this->get_onCreated_2();
		Il2CppObject * L_32 = V_0;
		NullCheck((Action_1_t985559125 *)L_31);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Action_1_t985559125 *)L_31, (Il2CppObject *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
	}

IL_00f0:
	{
		NullCheck((Component_t2126946602 *)(*(&V_0)));
		GameObject_t4012695102 * L_33 = Component_get_gameObject_m2112202034((Component_t2126946602 *)(*(&V_0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_33);
		GameObject_SetActive_m3538205401((GameObject_t4012695102 *)L_33, (bool)1, /*hidden argument*/NULL);
		Dictionary_2_t3271763293 * L_34 = (Dictionary_2_t3271763293 *)__this->get_usedObjects_4();
		String_t* L_35 = ___prefabName0;
		IL2CPP_RUNTIME_CLASS_INIT(Utils2_t2543841729_il2cpp_TypeInfo_var);
		List_1_t1634065389 * L_36 = ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t3271763293 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(NULL /*static, unused*/, (Dictionary_2_t3271763293 *)L_34, (String_t*)L_35, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Il2CppObject * L_37 = V_0;
		NullCheck((List_1_t1634065389 *)L_36);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_36, (Il2CppObject *)L_37);
		Action_1_t985559125 * L_38 = (Action_1_t985559125 *)__this->get_onReuse_1();
		if (!L_38)
		{
			goto IL_012c;
		}
	}
	{
		Action_1_t985559125 * L_39 = (Action_1_t985559125 *)__this->get_onReuse_1();
		Il2CppObject * L_40 = V_0;
		NullCheck((Action_1_t985559125 *)L_39);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Action_1_t985559125 *)L_39, (Il2CppObject *)L_40, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
	}

IL_012c:
	{
		Il2CppObject * L_41 = V_0;
		return L_41;
	}
}
// System.Void InstantiationPool`1<System.Object>::<KillAll>m__12F(T)
extern "C"  void InstantiationPool_1_U3CKillAllU3Em__12F_m2237180024_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___t0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___t0;
		Object_Destroy_m3720418393(NULL /*static, unused*/, (Object_t3878351788 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstantiationPool`1<System.Object>::<RecycleAll>m__130(T)
extern Il2CppClass* Utils2_t2543841729_il2cpp_TypeInfo_var;
extern const uint32_t InstantiationPool_1_U3CRecycleAllU3Em__130_m3480519710_MetadataUsageId;
extern "C"  void InstantiationPool_1_U3CRecycleAllU3Em__130_m3480519710_gshared (InstantiationPool_1_t1380750307 * __this, Il2CppObject * ___inst0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InstantiationPool_1_U3CRecycleAllU3Em__130_m3480519710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Component_t2126946602 *)(*(&___inst0)));
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m2112202034((Component_t2126946602 *)(*(&___inst0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_0);
		GameObject_SetActive_m3538205401((GameObject_t4012695102 *)L_0, (bool)0, /*hidden argument*/NULL);
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)__this->get_onRecycle_0();
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)__this->get_onRecycle_0();
		Il2CppObject * L_3 = ___inst0;
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
	}

IL_002a:
	{
		Il2CppObject * L_4 = ___inst0;
		NullCheck(L_4);
		ConnectionCollector_t444796719 * L_5 = (ConnectionCollector_t444796719 *)((InstantiatableInPool_t1882737686 *)L_4)->get_connections_4();
		NullCheck((ConnectionCollector_t444796719 *)L_5);
		ConnectionCollector_DisconnectAll_m662901599((ConnectionCollector_t444796719 *)L_5, /*hidden argument*/NULL);
		Dictionary_2_t3271763293 * L_6 = (Dictionary_2_t3271763293 *)__this->get_recycledObjects_5();
		Il2CppObject * L_7 = ___inst0;
		NullCheck(L_7);
		String_t* L_8 = (String_t*)((InstantiatableInPool_t1882737686 *)L_7)->get___prefabName_3();
		IL2CPP_RUNTIME_CLASS_INIT(Utils2_t2543841729_il2cpp_TypeInfo_var);
		List_1_t1634065389 * L_9 = ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t3271763293 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(NULL /*static, unused*/, (Dictionary_2_t3271763293 *)L_6, (String_t*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Il2CppObject * L_10 = ___inst0;
		NullCheck((List_1_t1634065389 *)L_9);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_9, (Il2CppObject *)L_10);
		return;
	}
}
// System.Void LitJson.ExporterFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m1424915150_gshared (ExporterFunc_1_t4015841116 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<System.Object>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m3275607590_gshared (ExporterFunc_1_t4015841116 * __this, Il2CppObject * ___obj0, JsonWriter_t3021344960 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m3275607590((ExporterFunc_1_t4015841116 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, JsonWriter_t3021344960 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, JsonWriter_t3021344960 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t3021344960 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<System.Object>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m3759174045_gshared (ExporterFunc_1_t4015841116 * __this, Il2CppObject * ___obj0, JsonWriter_t3021344960 * ___writer1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m2254144478_gshared (ExporterFunc_1_t4015841116 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ImporterFunc`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ImporterFunc_2__ctor_m671148138_gshared (ImporterFunc_2_t2649868222 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::Invoke(TJson)
extern "C"  Il2CppObject * ImporterFunc_2_Invoke_m2198455346_gshared (ImporterFunc_2_t2649868222 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ImporterFunc_2_Invoke_m2198455346((ImporterFunc_2_t2649868222 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ImporterFunc`2<System.Object,System.Object>::BeginInvoke(TJson,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImporterFunc_2_BeginInvoke_m2220076023_gshared (ImporterFunc_2_t2649868222 * __this, Il2CppObject * ___input0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ImporterFunc_2_EndInvoke_m3671500634_gshared (ImporterFunc_2_t2649868222 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey153`1<System.Object>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey153_1__ctor_m3831635565_gshared (U3CRegisterExporterU3Ec__AnonStorey153_1_t909718040 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey153`1<System.Object>::<>m__221(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey153_1_U3CU3Em__221_m2139979237_gshared (U3CRegisterExporterU3Ec__AnonStorey153_1_t909718040 * __this, Il2CppObject * ___obj0, JsonWriter_t3021344960 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t4015841116 * L_0 = (ExporterFunc_1_t4015841116 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t3021344960 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t4015841116 *)L_0);
		((  void (*) (ExporterFunc_1_t4015841116 *, Il2CppObject *, JsonWriter_t3021344960 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExporterFunc_1_t4015841116 *)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))), (JsonWriter_t3021344960 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterImporter>c__AnonStorey154`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CRegisterImporterU3Ec__AnonStorey154_2__ctor_m4172800208_gshared (U3CRegisterImporterU3Ec__AnonStorey154_2_t3095985135 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonMapper/<RegisterImporter>c__AnonStorey154`2<System.Object,System.Object>::<>m__222(System.Object)
extern "C"  Il2CppObject * U3CRegisterImporterU3Ec__AnonStorey154_2_U3CU3Em__222_m248240132_gshared (U3CRegisterImporterU3Ec__AnonStorey154_2_t3095985135 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	{
		ImporterFunc_2_t2649868222 * L_0 = (ImporterFunc_2_t2649868222 *)__this->get_importer_0();
		Il2CppObject * L_1 = ___input0;
		NullCheck((ImporterFunc_2_t2649868222 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ImporterFunc_2_t2649868222 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ImporterFunc_2_t2649868222 *)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}
}
// System.Void ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::.ctor()
extern "C"  void U3CAppendU3Ec__Iterator36_1__ctor_m4053237453_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CAppendU3Ec__Iterator36_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2302975158_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAppendU3Ec__Iterator36_1_System_Collections_IEnumerator_get_Current_m491056835_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAppendU3Ec__Iterator36_1_System_Collections_IEnumerable_GetEnumerator_m873747844_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CAppendU3Ec__Iterator36_1_t3337881070 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CAppendU3Ec__Iterator36_1_t3337881070 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CAppendU3Ec__Iterator36_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3911073503_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method)
{
	U3CAppendU3Ec__Iterator36_1_t3337881070 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CAppendU3Ec__Iterator36_1_t3337881070 * L_2 = (U3CAppendU3Ec__Iterator36_1_t3337881070 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CAppendU3Ec__Iterator36_1_t3337881070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CAppendU3Ec__Iterator36_1_t3337881070 *)L_2;
		U3CAppendU3Ec__Iterator36_1_t3337881070 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Efirst_6();
		NullCheck(L_3);
		L_3->set_first_0(L_4);
		U3CAppendU3Ec__Iterator36_1_t3337881070 * L_5 = V_0;
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_U3CU24U3Eitem_7();
		NullCheck(L_5);
		L_5->set_item_3(L_6);
		U3CAppendU3Ec__Iterator36_1_t3337881070 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CAppendU3Ec__Iterator36_1_MoveNext_m1721468911_MetadataUsageId;
extern "C"  bool U3CAppendU3Ec__Iterator36_1_MoveNext_m1721468911_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAppendU3Ec__Iterator36_1_MoveNext_m1721468911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_003b;
		}
		if (L_1 == 2)
		{
			goto IL_00bd;
		}
	}
	{
		goto IL_00c4;
	}

IL_0027:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_first_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_234U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0077;
			}
		}

IL_0047:
		{
			goto IL_0077;
		}

IL_004c:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_234U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CtU3E__1_2(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CtU3E__1_2();
			__this->set_U24current_5(L_7);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC6, FINALLY_008c);
		}

IL_0077:
		{
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_U3CU24s_234U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_004c;
			}
		}

IL_0087:
		{
			IL2CPP_LEAVE(0xA5, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0090;
			}
		}

IL_008f:
		{
			IL2CPP_END_FINALLY(140)
		}

IL_0090:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_234U3E__0_1();
			if (L_11)
			{
				goto IL_0099;
			}
		}

IL_0098:
		{
			IL2CPP_END_FINALLY(140)
		}

IL_0099:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_234U3E__0_1();
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(140)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0xC6, IL_00c6)
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a5:
	{
		Il2CppObject * L_13 = (Il2CppObject *)__this->get_item_3();
		__this->set_U24current_5(L_13);
		__this->set_U24PC_4(2);
		goto IL_00c6;
	}

IL_00bd:
	{
		__this->set_U24PC_4((-1));
	}

IL_00c4:
	{
		return (bool)0;
	}

IL_00c6:
	{
		return (bool)1;
	}
	// Dead block : IL_00c8: ldloc.2
}
// System.Void ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CAppendU3Ec__Iterator36_1_Dispose_m3819105226_MetadataUsageId;
extern "C"  void U3CAppendU3Ec__Iterator36_1_Dispose_m3819105226_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAppendU3Ec__Iterator36_1_Dispose_m3819105226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003f;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_003f;
		}
	}
	{
		goto IL_003f;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_234U3E__0_1();
			if (L_2)
			{
				goto IL_0033;
			}
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_0033:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_234U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void ModestTree.LinqExtensions/<Append>c__Iterator36`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CAppendU3Ec__Iterator36_1_Reset_m1699670394_MetadataUsageId;
extern "C"  void U3CAppendU3Ec__Iterator36_1_Reset_m1699670394_gshared (U3CAppendU3Ec__Iterator36_1_t3337881070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAppendU3Ec__Iterator36_1_Reset_m1699670394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CDistinctByImplU3Ec__Iterator3C_2__ctor_m3111199782_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1603733615_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_IEnumerator_get_Current_m428164938_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_IEnumerable_GetEnumerator_m3673712651_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2685995522_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method)
{
	U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_6();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * L_2 = (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *)L_2;
		U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Ecomparer_8();
		NullCheck(L_3);
		L_3->set_comparer_0(L_4);
		U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Esource_9();
		NullCheck(L_5);
		L_5->set_source_2(L_6);
		U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * L_7 = V_0;
		Func_2_t2135783352 * L_8 = (Func_2_t2135783352 *)__this->get_U3CU24U3EkeySelector_10();
		NullCheck(L_7);
		L_7->set_keySelector_5(L_8);
		U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * L_9 = V_0;
		return L_9;
	}
}
// System.Boolean ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CDistinctByImplU3Ec__Iterator3C_2_MoveNext_m881064182_MetadataUsageId;
extern "C"  bool U3CDistinctByImplU3Ec__Iterator3C_2_MoveNext_m881064182_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDistinctByImplU3Ec__Iterator3C_2_MoveNext_m881064182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_00da;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_comparer_0();
		HashSet_1_t3535795091 * L_3 = (HashSet_1_t3535795091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (HashSet_1_t3535795091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		__this->set_U3CknownKeysU3E__0_1(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_source_2();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_4);
		__this->set_U3CU24s_239U3E__1_3(L_5);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0048:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_00a5;
			}
		}

IL_0054:
		{
			goto IL_00a5;
		}

IL_0059:
		{
			Il2CppObject* L_7 = (Il2CppObject*)__this->get_U3CU24s_239U3E__1_3();
			NullCheck((Il2CppObject*)L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_7);
			__this->set_U3CelementU3E__2_4(L_8);
			HashSet_1_t3535795091 * L_9 = (HashSet_1_t3535795091 *)__this->get_U3CknownKeysU3E__0_1();
			Func_2_t2135783352 * L_10 = (Func_2_t2135783352 *)__this->get_keySelector_5();
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			NullCheck((Func_2_t2135783352 *)L_10);
			Il2CppObject * L_12 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Func_2_t2135783352 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			NullCheck((HashSet_1_t3535795091 *)L_9);
			bool L_13 = ((  bool (*) (HashSet_1_t3535795091 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((HashSet_1_t3535795091 *)L_9, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			if (!L_13)
			{
				goto IL_00a5;
			}
		}

IL_008b:
		{
			Il2CppObject * L_14 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			__this->set_U24current_7(L_14);
			__this->set_U24PC_6(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xDC, FINALLY_00ba);
		}

IL_00a5:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_239U3E__1_3();
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0059;
			}
		}

IL_00b5:
		{
			IL2CPP_LEAVE(0xD3, FINALLY_00ba);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ba;
	}

FINALLY_00ba:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_00be;
			}
		}

IL_00bd:
		{
			IL2CPP_END_FINALLY(186)
		}

IL_00be:
		{
			Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CU24s_239U3E__1_3();
			if (L_18)
			{
				goto IL_00c7;
			}
		}

IL_00c6:
		{
			IL2CPP_END_FINALLY(186)
		}

IL_00c7:
		{
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_239U3E__1_3();
			NullCheck((Il2CppObject *)L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
			IL2CPP_END_FINALLY(186)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(186)
	{
		IL2CPP_JUMP_TBL(0xDC, IL_00dc)
		IL2CPP_JUMP_TBL(0xD3, IL_00d3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d3:
	{
		__this->set_U24PC_6((-1));
	}

IL_00da:
	{
		return (bool)0;
	}

IL_00dc:
	{
		return (bool)1;
	}
	// Dead block : IL_00de: ldloc.2
}
// System.Void ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CDistinctByImplU3Ec__Iterator3C_2_Dispose_m464035555_MetadataUsageId;
extern "C"  void U3CDistinctByImplU3Ec__Iterator3C_2_Dispose_m464035555_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDistinctByImplU3Ec__Iterator3C_2_Dispose_m464035555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_239U3E__1_3();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_239U3E__1_3();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CDistinctByImplU3Ec__Iterator3C_2_Reset_m757632723_MetadataUsageId;
extern "C"  void U3CDistinctByImplU3Ec__Iterator3C_2_Reset_m757632723_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDistinctByImplU3Ec__Iterator3C_2_Reset_m757632723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.LinqExtensions/<OfType>c__AnonStorey16C`1<System.Object>::.ctor()
extern "C"  void U3COfTypeU3Ec__AnonStorey16C_1__ctor_m513912023_gshared (U3COfTypeU3Ec__AnonStorey16C_1_t3821260938 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ModestTree.LinqExtensions/<OfType>c__AnonStorey16C`1<System.Object>::<>m__288(T)
extern "C"  bool U3COfTypeU3Ec__AnonStorey16C_1_U3CU3Em__288_m2106715230_gshared (U3COfTypeU3Ec__AnonStorey16C_1_t3821260938 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)(*(&___x0)));
		Type_t * L_0 = Object_GetType_m2022236990((Il2CppObject *)(*(&___x0)), /*hidden argument*/NULL);
		Type_t * L_1 = (Type_t *)__this->get_type_0();
		return (bool)((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))? 1 : 0);
	}
}
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::.ctor()
extern "C"  void U3CPrependU3Ec__Iterator37_1__ctor_m1486960792_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator37_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1462883807_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator37_1_System_Collections_IEnumerator_get_Current_m930842254_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator37_1_System_Collections_IEnumerable_GetEnumerator_m2374052297_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CPrependU3Ec__Iterator37_1_t1884925155 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CPrependU3Ec__Iterator37_1_t1884925155 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CPrependU3Ec__Iterator37_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3578865058_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method)
{
	U3CPrependU3Ec__Iterator37_1_t1884925155 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CPrependU3Ec__Iterator37_1_t1884925155 * L_2 = (U3CPrependU3Ec__Iterator37_1_t1884925155 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CPrependU3Ec__Iterator37_1_t1884925155 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CPrependU3Ec__Iterator37_1_t1884925155 *)L_2;
		U3CPrependU3Ec__Iterator37_1_t1884925155 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Eitem_6();
		NullCheck(L_3);
		L_3->set_item_0(L_4);
		U3CPrependU3Ec__Iterator37_1_t1884925155 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Efirst_7();
		NullCheck(L_5);
		L_5->set_first_1(L_6);
		U3CPrependU3Ec__Iterator37_1_t1884925155 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrependU3Ec__Iterator37_1_MoveNext_m2435304284_MetadataUsageId;
extern "C"  bool U3CPrependU3Ec__Iterator37_1_MoveNext_m2435304284_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrependU3Ec__Iterator37_1_MoveNext_m2435304284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_003f;
		}
		if (L_1 == 2)
		{
			goto IL_0053;
		}
	}
	{
		goto IL_00c4;
	}

IL_0027:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_item_0();
		__this->set_U24current_5(L_2);
		__this->set_U24PC_4(1);
		goto IL_00c6;
	}

IL_003f:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_first_1();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3);
		__this->set_U3CU24s_235U3E__0_2(L_4);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)2)) == 0)
			{
				goto IL_008f;
			}
		}

IL_005f:
		{
			goto IL_008f;
		}

IL_0064:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_235U3E__0_2();
			NullCheck((Il2CppObject*)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6);
			__this->set_U3CtU3E__1_3(L_7);
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CtU3E__1_3();
			__this->set_U24current_5(L_8);
			__this->set_U24PC_4(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC6, FINALLY_00a4);
		}

IL_008f:
		{
			Il2CppObject* L_9 = (Il2CppObject*)__this->get_U3CU24s_235U3E__0_2();
			NullCheck((Il2CppObject *)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			if (L_10)
			{
				goto IL_0064;
			}
		}

IL_009f:
		{
			IL2CPP_LEAVE(0xBD, FINALLY_00a4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00a4;
	}

FINALLY_00a4:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_00a8;
			}
		}

IL_00a7:
		{
			IL2CPP_END_FINALLY(164)
		}

IL_00a8:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_235U3E__0_2();
			if (L_12)
			{
				goto IL_00b1;
			}
		}

IL_00b0:
		{
			IL2CPP_END_FINALLY(164)
		}

IL_00b1:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_235U3E__0_2();
			NullCheck((Il2CppObject *)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			IL2CPP_END_FINALLY(164)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(164)
	{
		IL2CPP_JUMP_TBL(0xC6, IL_00c6)
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00bd:
	{
		__this->set_U24PC_4((-1));
	}

IL_00c4:
	{
		return (bool)0;
	}

IL_00c6:
	{
		return (bool)1;
	}
	// Dead block : IL_00c8: ldloc.2
}
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrependU3Ec__Iterator37_1_Dispose_m2938461909_MetadataUsageId;
extern "C"  void U3CPrependU3Ec__Iterator37_1_Dispose_m2938461909_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrependU3Ec__Iterator37_1_Dispose_m2938461909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003f;
		}
		if (L_1 == 1)
		{
			goto IL_003f;
		}
		if (L_1 == 2)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_003f;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_235U3E__0_2();
			if (L_2)
			{
				goto IL_0033;
			}
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_0033:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_235U3E__0_2();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrependU3Ec__Iterator37_1_Reset_m3428361029_MetadataUsageId;
extern "C"  void U3CPrependU3Ec__Iterator37_1_Reset_m3428361029_gshared (U3CPrependU3Ec__Iterator37_1_t1884925155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrependU3Ec__Iterator37_1_Reset_m3428361029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::.ctor()
extern "C"  void U3CPrependU3Ec__Iterator38_1__ctor_m2448574809_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator38_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2329936032_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator38_1_System_Collections_IEnumerator_get_Current_m3051129453_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator38_1_System_Collections_IEnumerable_GetEnumerator_m4155552232_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CPrependU3Ec__Iterator38_1_t1439900156 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CPrependU3Ec__Iterator38_1_t1439900156 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CPrependU3Ec__Iterator38_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3592397859_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method)
{
	U3CPrependU3Ec__Iterator38_1_t1439900156 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_6();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CPrependU3Ec__Iterator38_1_t1439900156 * L_2 = (U3CPrependU3Ec__Iterator38_1_t1439900156 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CPrependU3Ec__Iterator38_1_t1439900156 *)L_2;
		U3CPrependU3Ec__Iterator38_1_t1439900156 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esecond_8();
		NullCheck(L_3);
		L_3->set_second_0(L_4);
		U3CPrependU3Ec__Iterator38_1_t1439900156 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Efirst_9();
		NullCheck(L_5);
		L_5->set_first_3(L_6);
		U3CPrependU3Ec__Iterator38_1_t1439900156 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrependU3Ec__Iterator38_1_MoveNext_m2446620411_MetadataUsageId;
extern "C"  bool U3CPrependU3Ec__Iterator38_1_MoveNext_m2446620411_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrependU3Ec__Iterator38_1_MoveNext_m2446620411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_003b;
		}
		if (L_1 == 2)
		{
			goto IL_00b9;
		}
	}
	{
		goto IL_012a;
	}

IL_0027:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_second_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_236U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0077;
			}
		}

IL_0047:
		{
			goto IL_0077;
		}

IL_004c:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_236U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CtU3E__1_2(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CtU3E__1_2();
			__this->set_U24current_7(L_7);
			__this->set_U24PC_6(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x12C, FINALLY_008c);
		}

IL_0077:
		{
			Il2CppObject* L_8 = (Il2CppObject*)__this->get_U3CU24s_236U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_004c;
			}
		}

IL_0087:
		{
			IL2CPP_LEAVE(0xA5, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0090;
			}
		}

IL_008f:
		{
			IL2CPP_END_FINALLY(140)
		}

IL_0090:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_236U3E__0_1();
			if (L_11)
			{
				goto IL_0099;
			}
		}

IL_0098:
		{
			IL2CPP_END_FINALLY(140)
		}

IL_0099:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_236U3E__0_1();
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(140)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x12C, IL_012c)
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a5:
	{
		Il2CppObject* L_13 = (Il2CppObject*)__this->get_first_3();
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_13);
		__this->set_U3CU24s_237U3E__2_4(L_14);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_00b9:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_15 = V_0;
			if (((int32_t)((int32_t)L_15-(int32_t)2)) == 0)
			{
				goto IL_00f5;
			}
		}

IL_00c5:
		{
			goto IL_00f5;
		}

IL_00ca:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_237U3E__2_4();
			NullCheck((Il2CppObject*)L_16);
			Il2CppObject * L_17 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_16);
			__this->set_U3CtU3E__3_5(L_17);
			Il2CppObject * L_18 = (Il2CppObject *)__this->get_U3CtU3E__3_5();
			__this->set_U24current_7(L_18);
			__this->set_U24PC_6(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x12C, FINALLY_010a);
		}

IL_00f5:
		{
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_237U3E__2_4();
			NullCheck((Il2CppObject *)L_19);
			bool L_20 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
			if (L_20)
			{
				goto IL_00ca;
			}
		}

IL_0105:
		{
			IL2CPP_LEAVE(0x123, FINALLY_010a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_010a;
	}

FINALLY_010a:
	{ // begin finally (depth: 1)
		{
			bool L_21 = V_1;
			if (!L_21)
			{
				goto IL_010e;
			}
		}

IL_010d:
		{
			IL2CPP_END_FINALLY(266)
		}

IL_010e:
		{
			Il2CppObject* L_22 = (Il2CppObject*)__this->get_U3CU24s_237U3E__2_4();
			if (L_22)
			{
				goto IL_0117;
			}
		}

IL_0116:
		{
			IL2CPP_END_FINALLY(266)
		}

IL_0117:
		{
			Il2CppObject* L_23 = (Il2CppObject*)__this->get_U3CU24s_237U3E__2_4();
			NullCheck((Il2CppObject *)L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_23);
			IL2CPP_END_FINALLY(266)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(266)
	{
		IL2CPP_JUMP_TBL(0x12C, IL_012c)
		IL2CPP_JUMP_TBL(0x123, IL_0123)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0123:
	{
		__this->set_U24PC_6((-1));
	}

IL_012a:
	{
		return (bool)0;
	}

IL_012c:
	{
		return (bool)1;
	}
	// Dead block : IL_012e: ldloc.2
}
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrependU3Ec__Iterator38_1_Dispose_m3631563606_MetadataUsageId;
extern "C"  void U3CPrependU3Ec__Iterator38_1_Dispose_m3631563606_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrependU3Ec__Iterator38_1_Dispose_m3631563606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_005e;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_005e;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_236U3E__0_1();
			if (L_2)
			{
				goto IL_0033;
			}
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_0033:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_236U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		goto IL_005e;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x5E, FINALLY_0049);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24s_237U3E__2_4();
			if (L_4)
			{
				goto IL_0052;
			}
		}

IL_0051:
		{
			IL2CPP_END_FINALLY(73)
		}

IL_0052:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_237U3E__2_4();
			NullCheck((Il2CppObject *)L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			IL2CPP_END_FINALLY(73)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrependU3Ec__Iterator38_1_Reset_m95007750_MetadataUsageId;
extern "C"  void U3CPrependU3Ec__Iterator38_1_Reset_m95007750_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrependU3Ec__Iterator38_1_Reset_m95007750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::.ctor()
extern "C"  void U3CReplaceOrAppendU3Ec__Iterator39_1__ctor_m559286845_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1668921988_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_IEnumerator_get_Current_m1694264585_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_IEnumerable_GetEnumerator_m1351958660_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CReplaceOrAppendU3Ec__Iterator39_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3452241927_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method)
{
	U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_6();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * L_2 = (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 *)L_2;
		U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Eenumerable_8();
		NullCheck(L_3);
		L_3->set_enumerable_1(L_4);
		U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * L_5 = V_0;
		Predicate_1_t1408070318 * L_6 = (Predicate_1_t1408070318 *)__this->get_U3CU24U3Ematch_9();
		NullCheck(L_5);
		L_5->set_match_4(L_6);
		U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * L_7 = V_0;
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CU24U3Ereplacement_10();
		NullCheck(L_7);
		L_7->set_replacement_5(L_8);
		U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * L_9 = V_0;
		return L_9;
	}
}
// System.Boolean ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CReplaceOrAppendU3Ec__Iterator39_1_MoveNext_m2932068247_MetadataUsageId;
extern "C"  bool U3CReplaceOrAppendU3Ec__Iterator39_1_MoveNext_m2932068247_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CReplaceOrAppendU3Ec__Iterator39_1_MoveNext_m2932068247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
		if (L_1 == 2)
		{
			goto IL_0046;
		}
		if (L_1 == 3)
		{
			goto IL_0113;
		}
	}
	{
		goto IL_011a;
	}

IL_002b:
	{
		__this->set_U3CreplacedU3E__0_0((bool)0);
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_enumerable_1();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_238U3E__1_2(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_00a3;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 1)
			{
				goto IL_00c2;
			}
		}

IL_0056:
		{
			goto IL_00c2;
		}

IL_005b:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_238U3E__1_2();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CtU3E__2_3(L_6);
			Predicate_1_t1408070318 * L_7 = (Predicate_1_t1408070318 *)__this->get_match_4();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CtU3E__2_3();
			NullCheck((Predicate_1_t1408070318 *)L_7);
			bool L_9 = ((  bool (*) (Predicate_1_t1408070318 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Predicate_1_t1408070318 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_00a8;
			}
		}

IL_0082:
		{
			__this->set_U3CreplacedU3E__0_0((bool)1);
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_replacement_5();
			__this->set_U24current_7(L_10);
			__this->set_U24PC_6(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x11C, FINALLY_00d7);
		}

IL_00a3:
		{
			goto IL_00c2;
		}

IL_00a8:
		{
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CtU3E__2_3();
			__this->set_U24current_7(L_11);
			__this->set_U24PC_6(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x11C, FINALLY_00d7);
		}

IL_00c2:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_238U3E__1_2();
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_005b;
			}
		}

IL_00d2:
		{
			IL2CPP_LEAVE(0xF0, FINALLY_00d7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00d7;
	}

FINALLY_00d7:
	{ // begin finally (depth: 1)
		{
			bool L_14 = V_1;
			if (!L_14)
			{
				goto IL_00db;
			}
		}

IL_00da:
		{
			IL2CPP_END_FINALLY(215)
		}

IL_00db:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_238U3E__1_2();
			if (L_15)
			{
				goto IL_00e4;
			}
		}

IL_00e3:
		{
			IL2CPP_END_FINALLY(215)
		}

IL_00e4:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_238U3E__1_2();
			NullCheck((Il2CppObject *)L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
			IL2CPP_END_FINALLY(215)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(215)
	{
		IL2CPP_JUMP_TBL(0x11C, IL_011c)
		IL2CPP_JUMP_TBL(0xF0, IL_00f0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00f0:
	{
		bool L_17 = (bool)__this->get_U3CreplacedU3E__0_0();
		if (L_17)
		{
			goto IL_0113;
		}
	}
	{
		Il2CppObject * L_18 = (Il2CppObject *)__this->get_replacement_5();
		__this->set_U24current_7(L_18);
		__this->set_U24PC_6(3);
		goto IL_011c;
	}

IL_0113:
	{
		__this->set_U24PC_6((-1));
	}

IL_011a:
	{
		return (bool)0;
	}

IL_011c:
	{
		return (bool)1;
	}
	// Dead block : IL_011e: ldloc.2
}
// System.Void ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CReplaceOrAppendU3Ec__Iterator39_1_Dispose_m502029114_MetadataUsageId;
extern "C"  void U3CReplaceOrAppendU3Ec__Iterator39_1_Dispose_m502029114_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CReplaceOrAppendU3Ec__Iterator39_1_Dispose_m502029114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0043;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0043;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x43, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_238U3E__1_2();
			if (L_2)
			{
				goto IL_0037;
			}
		}

IL_0036:
		{
			IL2CPP_END_FINALLY(46)
		}

IL_0037:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_238U3E__1_2();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CReplaceOrAppendU3Ec__Iterator39_1_Reset_m2500687082_MetadataUsageId;
extern "C"  void U3CReplaceOrAppendU3Ec__Iterator39_1_Reset_m2500687082_gshared (U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CReplaceOrAppendU3Ec__Iterator39_1_Reset_m2500687082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::.ctor()
extern "C"  void U3CToEnumerableU3Ec__Iterator3A_1__ctor_m2261611489_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2791918858_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_IEnumerator_get_Current_m3608398447_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_IEnumerable_GetEnumerator_m486128624_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m79047667_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method)
{
	U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_1();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * L_2 = (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *)L_2;
		U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Eenumerator_3();
		NullCheck(L_3);
		L_3->set_enumerator_0(L_4);
		U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const uint32_t U3CToEnumerableU3Ec__Iterator3A_1_MoveNext_m1822962523_MetadataUsageId;
extern "C"  bool U3CToEnumerableU3Ec__Iterator3A_1_MoveNext_m1822962523_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToEnumerableU3Ec__Iterator3A_1_MoveNext_m1822962523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_005f;
	}

IL_0021:
	{
		goto IL_0048;
	}

IL_0026:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_enumerator_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U24current_2(((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))));
		__this->set_U24PC_1(1);
		goto IL_0061;
	}

IL_0048:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_enumerator_0();
		NullCheck((Il2CppObject *)L_4);
		bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_005f:
	{
		return (bool)0;
	}

IL_0061:
	{
		return (bool)1;
	}
	// Dead block : IL_0063: ldloc.1
}
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::Dispose()
extern "C"  void U3CToEnumerableU3Ec__Iterator3A_1_Dispose_m53472222_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CToEnumerableU3Ec__Iterator3A_1_Reset_m4203011726_MetadataUsageId;
extern "C"  void U3CToEnumerableU3Ec__Iterator3A_1_Reset_m4203011726_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToEnumerableU3Ec__Iterator3A_1_Reset_m4203011726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::.ctor()
extern "C"  void U3CToEnumerableU3Ec__Iterator3B_1__ctor_m3223225506_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3658971083_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_IEnumerator_get_Current_m1433718350_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_IEnumerable_GetEnumerator_m2267628559_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m92580468_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method)
{
	U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_1();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * L_2 = (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *)L_2;
		U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Eenumerator_3();
		NullCheck(L_3);
		L_3->set_enumerator_0(L_4);
		U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const uint32_t U3CToEnumerableU3Ec__Iterator3B_1_MoveNext_m1834278650_MetadataUsageId;
extern "C"  bool U3CToEnumerableU3Ec__Iterator3B_1_MoveNext_m1834278650_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToEnumerableU3Ec__Iterator3B_1_MoveNext_m1834278650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_005a;
	}

IL_0021:
	{
		goto IL_0043;
	}

IL_0026:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_enumerator_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U24current_2(L_3);
		__this->set_U24PC_1(1);
		goto IL_005c;
	}

IL_0043:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_enumerator_0();
		NullCheck((Il2CppObject *)L_4);
		bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_005a:
	{
		return (bool)0;
	}

IL_005c:
	{
		return (bool)1;
	}
	// Dead block : IL_005e: ldloc.1
}
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::Dispose()
extern "C"  void U3CToEnumerableU3Ec__Iterator3B_1_Dispose_m746573919_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CToEnumerableU3Ec__Iterator3B_1_Reset_m869658447_MetadataUsageId;
extern "C"  void U3CToEnumerableU3Ec__Iterator3B_1_Reset_m869658447_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToEnumerableU3Ec__Iterator3B_1_Reset_m869658447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::.ctor()
extern "C"  void U3CYieldU3Ec__Iterator3D_1__ctor_m2090070712_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CYieldU3Ec__Iterator3D_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3185341375_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CYieldU3Ec__Iterator3D_1_System_Collections_IEnumerator_get_Current_m24006446_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Collections.IEnumerator ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CYieldU3Ec__Iterator3D_1_System_Collections_IEnumerable_GetEnumerator_m2463013289_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CYieldU3Ec__Iterator3D_1_t3174792911 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CYieldU3Ec__Iterator3D_1_t3174792911 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CYieldU3Ec__Iterator3D_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2861739714_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method)
{
	U3CYieldU3Ec__Iterator3D_1_t3174792911 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_1();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CYieldU3Ec__Iterator3D_1_t3174792911 * L_2 = (U3CYieldU3Ec__Iterator3D_1_t3174792911 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CYieldU3Ec__Iterator3D_1_t3174792911 *)L_2;
		U3CYieldU3Ec__Iterator3D_1_t3174792911 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Eitem_3();
		NullCheck(L_3);
		L_3->set_item_0(L_4);
		U3CYieldU3Ec__Iterator3D_1_t3174792911 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::MoveNext()
extern "C"  bool U3CYieldU3Ec__Iterator3D_1_MoveNext_m1241276476_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0040;
	}

IL_0021:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_item_0();
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_0042;
	}

IL_0039:
	{
		__this->set_U24PC_1((-1));
	}

IL_0040:
	{
		return (bool)0;
	}

IL_0042:
	{
		return (bool)1;
	}
	// Dead block : IL_0044: ldloc.1
}
// System.Void ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::Dispose()
extern "C"  void U3CYieldU3Ec__Iterator3D_1_Dispose_m2706510069_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CYieldU3Ec__Iterator3D_1_Reset_m4031470949_MetadataUsageId;
extern "C"  void U3CYieldU3Ec__Iterator3D_1_Reset_m4031470949_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CYieldU3Ec__Iterator3D_1_Reset_m4031470949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_10__ctor_m1413835974_gshared (Func_10_t941656758 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7,T8,T9)
extern "C"  Il2CppObject * Func_10_Invoke_m2701011777_gshared (Func_10_t941656758 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, Il2CppObject * ___arg98, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_10_Invoke_m2701011777((Func_10_t941656758 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___arg98, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, Il2CppObject * ___arg98, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___arg98,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, Il2CppObject * ___arg98, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___arg98,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, Il2CppObject * ___arg98, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___arg98,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,T8,T9,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_10_BeginInvoke_m2118615612_gshared (Func_10_t941656758 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, Il2CppObject * ___arg98, AsyncCallback_t1363551830 * ___callback9, Il2CppObject * ___object10, const MethodInfo* method)
{
	void *__d_args[10] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	__d_args[6] = ___arg76;
	__d_args[7] = ___arg87;
	__d_args[8] = ___arg98;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback9, (Il2CppObject*)___object10);
}
// TResult ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_10_EndInvoke_m3790753400_gshared (Func_10_t941656758 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_6__ctor_m1024459519_gshared (Func_6_t698299427 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5)
extern "C"  Il2CppObject * Func_6_Invoke_m272686264_gshared (Func_6_t698299427 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_6_Invoke_m272686264((Func_6_t698299427 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43, ___arg54,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_6_BeginInvoke_m2607504747_gshared (Func_6_t698299427 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, AsyncCallback_t1363551830 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	void *__d_args[6] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// TResult ModestTree.Util.Func`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_6_EndInvoke_m857310637_gshared (Func_6_t698299427 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_7__ctor_m2659415148_gshared (Func_7_t3735803334 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6)
extern "C"  Il2CppObject * Func_7_Invoke_m632476639_gshared (Func_7_t3735803334 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_7_Invoke_m632476639((Func_7_t3735803334 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_7_BeginInvoke_m2277157288_gshared (Func_7_t3735803334 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method)
{
	void *__d_args[7] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback6, (Il2CppObject*)___object7);
}
// TResult ModestTree.Util.Func`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_7_EndInvoke_m3853478170_gshared (Func_7_t3735803334 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void ModestTree.Util.Func`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_8__ctor_m4010930393_gshared (Func_8_t2205681617 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult ModestTree.Util.Func`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7)
extern "C"  Il2CppObject * Func_8_Invoke_m3002683569_gshared (Func_8_t2205681617 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_8_Invoke_m3002683569((Func_8_t2205681617 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ModestTree.Util.Func`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_8_BeginInvoke_m2529982982_gshared (Func_8_t2205681617 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, AsyncCallback_t1363551830 * ___callback7, Il2CppObject * ___object8, const MethodInfo* method)
{
	void *__d_args[8] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	__d_args[6] = ___arg76;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback7, (Il2CppObject*)___object8);
}
// TResult ModestTree.Util.Func`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_8_EndInvoke_m2434643847_gshared (Func_8_t2205681617 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_9__ctor_m555821126_gshared (Func_9_t38423544 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7,T8)
extern "C"  Il2CppObject * Func_9_Invoke_m451447108_gshared (Func_9_t38423544 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_9_Invoke_m451447108((Func_9_t38423544 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,T8,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_9_BeginInvoke_m4241629679_gshared (Func_9_t38423544 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, AsyncCallback_t1363551830 * ___callback8, Il2CppObject * ___object9, const MethodInfo* method)
{
	void *__d_args[9] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	__d_args[6] = ___arg76;
	__d_args[7] = ___arg87;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback8, (Il2CppObject*)___object9);
}
// TResult ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_9_EndInvoke_m3503956212_gshared (Func_9_t38423544 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void ModestTree.Util.Tuple`2<System.Object,System.Int32>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Tuple_2__ctor_m3946485580_MetadataUsageId;
extern "C"  void Tuple_2__ctor_m3946485580_gshared (Tuple_2_t299352770 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2__ctor_m3946485580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_First_0(L_0);
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_1 = V_1;
		__this->set_Second_1(L_1);
		return;
	}
}
// System.Void ModestTree.Util.Tuple`2<System.Object,System.Int32>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m2191801465_gshared (Tuple_2_t299352770 * __this, Il2CppObject * ___first0, int32_t ___second1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___first0;
		__this->set_First_0(L_0);
		int32_t L_1 = ___second1;
		__this->set_Second_1(L_1);
		return;
	}
}
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Int32>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m897686001_gshared (Tuple_2_t299352770 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	Tuple_2_t299352770 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (Tuple_2_t299352770 *)((Tuple_2_t299352770 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		Tuple_2_t299352770 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		Tuple_2_t299352770 * L_2 = V_0;
		NullCheck((Tuple_2_t299352770 *)__this);
		bool L_3 = ((  bool (*) (Tuple_2_t299352770 *, Tuple_2_t299352770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Tuple_2_t299352770 *)__this, (Tuple_2_t299352770 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_3;
	}
}
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Int32>::Equals(ModestTree.Util.Tuple`2<T1,T2>)
extern "C"  bool Tuple_2_Equals_m194624144_gshared (Tuple_2_t299352770 * __this, Tuple_2_t299352770 * ___that0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		Tuple_2_t299352770 * L_0 = ___that0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_First_0();
		Tuple_2_t299352770 * L_2 = ___that0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_First_0();
		bool L_4 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_5 = (int32_t)__this->get_Second_1();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_6);
		Tuple_2_t299352770 * L_8 = ___that0;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)L_8->get_Second_1();
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_10);
		bool L_12 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_7, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_12));
		goto IL_0046;
	}

IL_0045:
	{
		G_B5_0 = 0;
	}

IL_0046:
	{
		return (bool)G_B5_0;
	}
}
// System.Int32 ModestTree.Util.Tuple`2<System.Object,System.Int32>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m1510095241_gshared (Tuple_2_t299352770 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	{
		V_0 = (int32_t)((int32_t)17);
		int32_t L_0 = V_0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_First_0();
		G_B1_0 = ((int32_t)((int32_t)L_0*(int32_t)((int32_t)29)));
		if (L_1)
		{
			G_B2_0 = ((int32_t)((int32_t)L_0*(int32_t)((int32_t)29)));
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0031;
	}

IL_001d:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_First_0();
		V_1 = (Il2CppObject *)L_2;
		NullCheck((Il2CppObject *)(*(&V_1)));
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_1)));
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0031:
	{
		V_0 = (int32_t)((int32_t)((int32_t)G_B3_1+(int32_t)G_B3_0));
		int32_t L_4 = V_0;
		int32_t L_5 = (int32_t)__this->get_Second_1();
		G_B5_0 = ((int32_t)((int32_t)L_4*(int32_t)((int32_t)29)));
		goto IL_004d;
		G_B4_0 = ((int32_t)((int32_t)L_4*(int32_t)((int32_t)29)));
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		goto IL_0061;
	}

IL_004d:
	{
		int32_t L_6 = (int32_t)__this->get_Second_1();
		V_2 = (int32_t)L_6;
		int32_t L_7 = Int32_GetHashCode_m3396943446((int32_t*)(&V_2), /*hidden argument*/NULL);
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_0061:
	{
		V_0 = (int32_t)((int32_t)((int32_t)G_B6_1+(int32_t)G_B6_0));
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Void ModestTree.Util.Tuple`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Tuple_2__ctor_m3534991723_MetadataUsageId;
extern "C"  void Tuple_2__ctor_m3534991723_gshared (Tuple_2_t2584011699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tuple_2__ctor_m3534991723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_First_0(L_0);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_1 = V_1;
		__this->set_Second_1(L_1);
		return;
	}
}
// System.Void ModestTree.Util.Tuple`2<System.Object,System.Object>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m4054949306_gshared (Tuple_2_t2584011699 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___first0;
		__this->set_First_0(L_0);
		Il2CppObject * L_1 = ___second1;
		__this->set_Second_1(L_1);
		return;
	}
}
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m2602159336_gshared (Tuple_2_t2584011699 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	Tuple_2_t2584011699 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (Tuple_2_t2584011699 *)((Tuple_2_t2584011699 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		Tuple_2_t2584011699 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		Tuple_2_t2584011699 * L_2 = V_0;
		NullCheck((Tuple_2_t2584011699 *)__this);
		bool L_3 = ((  bool (*) (Tuple_2_t2584011699 *, Tuple_2_t2584011699 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Tuple_2_t2584011699 *)__this, (Tuple_2_t2584011699 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_3;
	}
}
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Object>::Equals(ModestTree.Util.Tuple`2<T1,T2>)
extern "C"  bool Tuple_2_Equals_m2951541881_gshared (Tuple_2_t2584011699 * __this, Tuple_2_t2584011699 * ___that0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		Tuple_2_t2584011699 * L_0 = ___that0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_First_0();
		Tuple_2_t2584011699 * L_2 = ___that0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_First_0();
		bool L_4 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_Second_1();
		Tuple_2_t2584011699 * L_6 = ___that0;
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_Second_1();
		bool L_8 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_8));
		goto IL_0046;
	}

IL_0045:
	{
		G_B5_0 = 0;
	}

IL_0046:
	{
		return (bool)G_B5_0;
	}
}
// System.Int32 ModestTree.Util.Tuple`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m16712972_gshared (Tuple_2_t2584011699 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	{
		V_0 = (int32_t)((int32_t)17);
		int32_t L_0 = V_0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_First_0();
		G_B1_0 = ((int32_t)((int32_t)L_0*(int32_t)((int32_t)29)));
		if (L_1)
		{
			G_B2_0 = ((int32_t)((int32_t)L_0*(int32_t)((int32_t)29)));
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0031;
	}

IL_001d:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_First_0();
		V_1 = (Il2CppObject *)L_2;
		NullCheck((Il2CppObject *)(*(&V_1)));
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_1)));
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0031:
	{
		V_0 = (int32_t)((int32_t)((int32_t)G_B3_1+(int32_t)G_B3_0));
		int32_t L_4 = V_0;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_Second_1();
		G_B4_0 = ((int32_t)((int32_t)L_4*(int32_t)((int32_t)29)));
		if (L_5)
		{
			G_B5_0 = ((int32_t)((int32_t)L_4*(int32_t)((int32_t)29)));
			goto IL_004d;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		goto IL_0061;
	}

IL_004d:
	{
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_Second_1();
		V_2 = (Il2CppObject *)L_6;
		NullCheck((Il2CppObject *)(*(&V_2)));
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_2)));
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_0061:
	{
		V_0 = (int32_t)((int32_t)((int32_t)G_B6_1+(int32_t)G_B6_0));
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Void ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Tuple_3__ctor_m3193714974_MetadataUsageId;
extern "C"  void Tuple_3__ctor_m3193714974_gshared (Tuple_3_t1160054790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tuple_3__ctor_m3193714974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_First_0(L_0);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_1 = V_1;
		__this->set_Second_1(L_1);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_2 = V_2;
		__this->set_Third_2(L_2);
		return;
	}
}
// System.Void ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::.ctor(T1,T2,T3)
extern "C"  void Tuple_3__ctor_m2993656704_gshared (Tuple_3_t1160054790 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, Il2CppObject * ___third2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___first0;
		__this->set_First_0(L_0);
		Il2CppObject * L_1 = ___second1;
		__this->set_Second_1(L_1);
		Il2CppObject * L_2 = ___third2;
		__this->set_Third_2(L_2);
		return;
	}
}
// System.Boolean ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_3_Equals_m680266331_gshared (Tuple_3_t1160054790 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	Tuple_3_t1160054790 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (Tuple_3_t1160054790 *)((Tuple_3_t1160054790 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		Tuple_3_t1160054790 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		Tuple_3_t1160054790 * L_2 = V_0;
		NullCheck((Tuple_3_t1160054790 *)__this);
		bool L_3 = ((  bool (*) (Tuple_3_t1160054790 *, Tuple_3_t1160054790 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Tuple_3_t1160054790 *)__this, (Tuple_3_t1160054790 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_3;
	}
}
// System.Boolean ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::Equals(ModestTree.Util.Tuple`3<T1,T2,T3>)
extern "C"  bool Tuple_3_Equals_m707126330_gshared (Tuple_3_t1160054790 * __this, Tuple_3_t1160054790 * ___that0, const MethodInfo* method)
{
	int32_t G_B6_0 = 0;
	{
		Tuple_3_t1160054790 * L_0 = ___that0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_First_0();
		Tuple_3_t1160054790 * L_2 = ___that0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_First_0();
		bool L_4 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0065;
		}
	}
	{
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_Second_1();
		Tuple_3_t1160054790 * L_6 = ___that0;
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_Second_1();
		bool L_8 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0065;
		}
	}
	{
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_Third_2();
		Tuple_3_t1160054790 * L_10 = ___that0;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_Third_2();
		bool L_12 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_9, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_12));
		goto IL_0066;
	}

IL_0065:
	{
		G_B6_0 = 0;
	}

IL_0066:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_3_GetHashCode_m845495039_gshared (Tuple_3_t1160054790 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_First_0();
		V_0 = (Il2CppObject *)L_0;
		NullCheck((Il2CppObject *)(*(&V_0)));
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_0)));
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_Second_1();
		V_1 = (Il2CppObject *)L_2;
		NullCheck((Il2CppObject *)(*(&V_1)));
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_1)));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_Third_2();
		V_2 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_2)));
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_2)));
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)17)*(int32_t)L_1))+(int32_t)((int32_t)((int32_t)((int32_t)31)*(int32_t)L_3))))+(int32_t)((int32_t)((int32_t)((int32_t)47)*(int32_t)L_5))));
	}
}
// System.Void OnceStream`1<System.Object>::.ctor()
extern "C"  void OnceStream_1__ctor_m2654431905_gshared (OnceStream_1_t151376059 * __this, const MethodInfo* method)
{
	{
		NullCheck((Stream_1_t3823212738 *)__this);
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Stream_1_t3823212738 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((Stream_1_t3823212738 *)__this)->set_autoDisconnectAfterEvent_4((bool)1);
		return;
	}
}
// System.Void Organism`2/<Setup>c__AnonStorey11F<System.Object,System.Object>::.ctor()
extern "C"  void U3CSetupU3Ec__AnonStorey11F__ctor_m1680593202_gshared (U3CSetupU3Ec__AnonStorey11F_t4199016171 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Organism`2/<Setup>c__AnonStorey11F<System.Object,System.Object>::<>m__1AC(IOrganism)
extern Il2CppClass* IRoot_t69970123_il2cpp_TypeInfo_var;
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t U3CSetupU3Ec__AnonStorey11F_U3CU3Em__1AC_m466440573_MetadataUsageId;
extern "C"  void U3CSetupU3Ec__AnonStorey11F_U3CU3Em__1AC_m466440573_gshared (U3CSetupU3Ec__AnonStorey11F_t4199016171 * __this, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSetupU3Ec__AnonStorey11F_U3CU3Em__1AC_m466440573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B3_1 = NULL;
	Il2CppObject * G_B5_0 = NULL;
	Il2CppObject * G_B5_1 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	Il2CppObject * G_B4_1 = NULL;
	Il2CppObject * G_B6_0 = NULL;
	Il2CppObject * G_B6_1 = NULL;
	Il2CppObject * G_B6_2 = NULL;
	{
		Organism_2_t948289219 * L_0 = (Organism_2_t948289219 *)__this->get_U3CU3Ef__this_1();
		V_0 = (Il2CppObject *)((Il2CppObject *)IsInst(L_0, IRoot_t69970123_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___child0;
		Organism_2_t948289219 * L_2 = (Organism_2_t948289219 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_2);
		bool L_3 = (bool)L_2->get_notACarrier_3();
		G_B1_0 = L_1;
		if (!L_3)
		{
			G_B2_0 = L_1;
			goto IL_002a;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_c_0();
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_002a:
	{
		Organism_2_t948289219 * L_6 = (Organism_2_t948289219 *)__this->get_U3CU3Ef__this_1();
		G_B3_0 = ((Il2CppObject *)(L_6));
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		Il2CppObject * L_7 = V_0;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		if (!L_7)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			goto IL_003e;
		}
	}
	{
		Il2CppObject * L_8 = V_0;
		V_2 = (Il2CppObject *)L_8;
		Il2CppObject * L_9 = V_2;
		G_B6_0 = L_9;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_004e;
	}

IL_003e:
	{
		Organism_2_t948289219 * L_10 = (Organism_2_t948289219 *)__this->get_U3CU3Ef__this_1();
		NullCheck((Organism_2_t948289219 *)L_10);
		Il2CppObject * L_11 = VirtFuncInvoker0< Il2CppObject * >::Invoke(13 /* TRoot Organism`2<System.Object,System.Object>::get_root() */, (Organism_2_t948289219 *)L_10);
		G_B6_0 = ((Il2CppObject *)(L_11));
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_004e:
	{
		NullCheck((Il2CppObject *)G_B6_2);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void IOrganism::Setup(IOrganism,IRoot) */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)G_B6_2, (Il2CppObject *)G_B6_1, (Il2CppObject *)G_B6_0);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* List_1_t3377802548_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3755966385_MethodInfo_var;
extern const uint32_t Organism_2__ctor_m4082911749_MetadataUsageId;
extern "C"  void Organism_2__ctor_m4082911749_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2__ctor_m4082911749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3377802548 * L_0 = (List_1_t3377802548 *)il2cpp_codegen_object_new(List_1_t3377802548_il2cpp_TypeInfo_var);
		List_1__ctor_m3755966385(L_0, /*hidden argument*/List_1__ctor_m3755966385_MethodInfo_var);
		__this->set_children_6(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TCarrier Organism`2<System.Object,System.Object>::get_carrier()
extern "C"  Il2CppObject * Organism_2_get_carrier_m4029902219_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_carrier__1();
		return L_0;
	}
}
// System.Void Organism`2<System.Object,System.Object>::set_carrier(TCarrier)
extern "C"  void Organism_2_set_carrier_m2958823066_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_carrier__1(L_0);
		return;
	}
}
// TRoot Organism`2<System.Object,System.Object>::get_root()
extern "C"  Il2CppObject * Organism_2_get_root_m2383393421_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_root__2();
		return L_0;
	}
}
// System.Void Organism`2<System.Object,System.Object>::set_root(TRoot)
extern "C"  void Organism_2_set_root_m984770174_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_root__2(L_0);
		return;
	}
}
// IOnceEmptyStream Organism`2<System.Object,System.Object>::get_fellAsleep()
extern Il2CppClass* OnceEmptyStream_t3081939404_il2cpp_TypeInfo_var;
extern const uint32_t Organism_2_get_fellAsleep_m2050669571_MetadataUsageId;
extern "C"  Il2CppObject * Organism_2_get_fellAsleep_m2050669571_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_get_fellAsleep_m2050669571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OnceEmptyStream_t3081939404 * V_0 = NULL;
	OnceEmptyStream_t3081939404 * G_B2_0 = NULL;
	OnceEmptyStream_t3081939404 * G_B1_0 = NULL;
	{
		OnceEmptyStream_t3081939404 * L_0 = (OnceEmptyStream_t3081939404 *)__this->get_fellAsleep__5();
		OnceEmptyStream_t3081939404 * L_1 = (OnceEmptyStream_t3081939404 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		OnceEmptyStream_t3081939404 * L_2 = (OnceEmptyStream_t3081939404 *)il2cpp_codegen_object_new(OnceEmptyStream_t3081939404_il2cpp_TypeInfo_var);
		OnceEmptyStream__ctor_m3370738463(L_2, /*hidden argument*/NULL);
		OnceEmptyStream_t3081939404 * L_3 = (OnceEmptyStream_t3081939404 *)L_2;
		V_0 = (OnceEmptyStream_t3081939404 *)L_3;
		__this->set_fellAsleep__5(L_3);
		OnceEmptyStream_t3081939404 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void Organism`2<System.Object,System.Object>::DisposeWhenAsleep(System.IDisposable)
extern "C"  void Organism_2_DisposeWhenAsleep_m153634408_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___disposable0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___disposable0;
		NullCheck((Organism_2_t948289219 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(4 /* System.Void Organism`2<System.Object,System.Object>::Collect(System.IDisposable) */, (Organism_2_t948289219 *)__this, (Il2CppObject *)L_0);
		return;
	}
}
// System.Boolean Organism`2<System.Object,System.Object>::get_isAlive()
extern "C"  bool Organism_2_get_isAlive_m434369745_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_alive_4();
		return L_0;
	}
}
// System.Boolean Organism`2<System.Object,System.Object>::get_primal()
extern "C"  bool Organism_2_get_primal_m1803875749_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void Organism`2<System.Object,System.Object>::Setup(IOrganism,IRoot)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2729296284_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m2946257769_MethodInfo_var;
extern const MethodInfo* List_1_ForEach_m1069597881_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1793404592;
extern Il2CppCodeGenString* _stringLiteral456821652;
extern const uint32_t Organism_2_Setup_m3424294968_MetadataUsageId;
extern "C"  void Organism_2_Setup_m3424294968_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___c0, Il2CppObject * ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_Setup_m3424294968_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSetupU3Ec__AnonStorey11F_t4199016171 * V_0 = NULL;
	{
		U3CSetupU3Ec__AnonStorey11F_t4199016171 * L_0 = (U3CSetupU3Ec__AnonStorey11F_t4199016171 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CSetupU3Ec__AnonStorey11F_t4199016171 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (U3CSetupU3Ec__AnonStorey11F_t4199016171 *)L_0;
		U3CSetupU3Ec__AnonStorey11F_t4199016171 * L_1 = V_0;
		Il2CppObject * L_2 = ___c0;
		NullCheck(L_1);
		L_1->set_c_0(L_2);
		U3CSetupU3Ec__AnonStorey11F_t4199016171 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		U3CSetupU3Ec__AnonStorey11F_t4199016171 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_c_0();
		NullCheck((Organism_2_t948289219 *)__this);
		((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Organism_2_t948289219 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_6 = ___r1;
		NullCheck((Organism_2_t948289219 *)__this);
		((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Organism_2_t948289219 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Organism_2_t948289219 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* TCarrier Organism`2<System.Object,System.Object>::get_carrier() */, (Organism_2_t948289219 *)__this);
		if (L_7)
		{
			goto IL_0060;
		}
	}
	{
		U3CSetupU3Ec__AnonStorey11F_t4199016171 * L_8 = V_0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_c_0();
		if (!L_9)
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1793404592, /*hidden argument*/NULL);
	}

IL_0060:
	{
		NullCheck((Organism_2_t948289219 *)__this);
		Il2CppObject * L_10 = VirtFuncInvoker0< Il2CppObject * >::Invoke(13 /* TRoot Organism`2<System.Object,System.Object>::get_root() */, (Organism_2_t948289219 *)__this);
		if (L_10)
		{
			goto IL_0080;
		}
	}
	{
		Il2CppObject * L_11 = ___r1;
		if (!L_11)
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral456821652, /*hidden argument*/NULL);
	}

IL_0080:
	{
		NullCheck((Organism_2_t948289219 *)__this);
		VirtActionInvoker0::Invoke(20 /* System.Void Organism`2<System.Object,System.Object>::RegisterChildren() */, (Organism_2_t948289219 *)__this);
		List_1_t3377802548 * L_12 = (List_1_t3377802548 *)__this->get_children_6();
		U3CSetupU3Ec__AnonStorey11F_t4199016171 * L_13 = V_0;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_1_t2729296284 * L_15 = (Action_1_t2729296284 *)il2cpp_codegen_object_new(Action_1_t2729296284_il2cpp_TypeInfo_var);
		Action_1__ctor_m2946257769(L_15, (Il2CppObject *)L_13, (IntPtr_t)L_14, /*hidden argument*/Action_1__ctor_m2946257769_MethodInfo_var);
		NullCheck((List_1_t3377802548 *)L_12);
		List_1_ForEach_m1069597881((List_1_t3377802548 *)L_12, (Action_1_t2729296284 *)L_15, /*hidden argument*/List_1_ForEach_m1069597881_MethodInfo_var);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::WakeUp()
extern Il2CppClass* Action_1_t2729296284_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m2946257769_MethodInfo_var;
extern const MethodInfo* List_1_ForEach_m1069597881_MethodInfo_var;
extern const uint32_t Organism_2_WakeUp_m2709259966_MetadataUsageId;
extern "C"  void Organism_2_WakeUp_m2709259966_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_WakeUp_m2709259966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3377802548 * G_B2_0 = NULL;
	List_1_t3377802548 * G_B1_0 = NULL;
	{
		List_1_t3377802548 * L_0 = (List_1_t3377802548 *)__this->get_children_6();
		Action_1_t2729296284 * L_1 = ((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cache7_7();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t2729296284 * L_3 = (Action_1_t2729296284 *)il2cpp_codegen_object_new(Action_1_t2729296284_il2cpp_TypeInfo_var);
		Action_1__ctor_m2946257769(L_3, (Il2CppObject *)NULL, (IntPtr_t)L_2, /*hidden argument*/Action_1__ctor_m2946257769_MethodInfo_var);
		((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->set_U3CU3Ef__amU24cache7_7(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Action_1_t2729296284 * L_4 = ((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cache7_7();
		NullCheck((List_1_t3377802548 *)G_B2_0);
		List_1_ForEach_m1069597881((List_1_t3377802548 *)G_B2_0, (Action_1_t2729296284 *)L_4, /*hidden argument*/List_1_ForEach_m1069597881_MethodInfo_var);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::Spread()
extern Il2CppClass* Action_1_t2729296284_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m2946257769_MethodInfo_var;
extern const MethodInfo* List_1_ForEach_m1069597881_MethodInfo_var;
extern const uint32_t Organism_2_Spread_m661389266_MetadataUsageId;
extern "C"  void Organism_2_Spread_m661389266_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_Spread_m661389266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3377802548 * G_B2_0 = NULL;
	List_1_t3377802548 * G_B1_0 = NULL;
	{
		List_1_t3377802548 * L_0 = (List_1_t3377802548 *)__this->get_children_6();
		Action_1_t2729296284 * L_1 = ((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cache8_8();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		Action_1_t2729296284 * L_3 = (Action_1_t2729296284 *)il2cpp_codegen_object_new(Action_1_t2729296284_il2cpp_TypeInfo_var);
		Action_1__ctor_m2946257769(L_3, (Il2CppObject *)NULL, (IntPtr_t)L_2, /*hidden argument*/Action_1__ctor_m2946257769_MethodInfo_var);
		((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->set_U3CU3Ef__amU24cache8_8(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Action_1_t2729296284 * L_4 = ((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cache8_8();
		NullCheck((List_1_t3377802548 *)G_B2_0);
		List_1_ForEach_m1069597881((List_1_t3377802548 *)G_B2_0, (Action_1_t2729296284 *)L_4, /*hidden argument*/List_1_ForEach_m1069597881_MethodInfo_var);
		__this->set_alive_4((bool)1);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::Collect(System.IDisposable)
extern Il2CppClass* ConnectionCollector_t444796719_il2cpp_TypeInfo_var;
extern const uint32_t Organism_2_Collect_m831016995_MetadataUsageId;
extern "C"  void Organism_2_Collect_m831016995_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___tail0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_Collect_m831016995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ConnectionCollector_t444796719 * L_0 = (ConnectionCollector_t444796719 *)__this->get_tails_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ConnectionCollector_t444796719 * L_1 = (ConnectionCollector_t444796719 *)il2cpp_codegen_object_new(ConnectionCollector_t444796719_il2cpp_TypeInfo_var);
		ConnectionCollector__ctor_m3524765980(L_1, /*hidden argument*/NULL);
		__this->set_tails_0(L_1);
	}

IL_0016:
	{
		ConnectionCollector_t444796719 * L_2 = (ConnectionCollector_t444796719 *)__this->get_tails_0();
		Il2CppObject * L_3 = ___tail0;
		NullCheck((List_1_t2425880343 *)L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Add(!0) */, (List_1_t2425880343 *)L_2, (Il2CppObject *)L_3);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::set_collect(System.IDisposable)
extern Il2CppClass* ConnectionCollector_t444796719_il2cpp_TypeInfo_var;
extern const uint32_t Organism_2_set_collect_m1070393574_MetadataUsageId;
extern "C"  void Organism_2_set_collect_m1070393574_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_set_collect_m1070393574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ConnectionCollector_t444796719 * L_0 = (ConnectionCollector_t444796719 *)__this->get_tails_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ConnectionCollector_t444796719 * L_1 = (ConnectionCollector_t444796719 *)il2cpp_codegen_object_new(ConnectionCollector_t444796719_il2cpp_TypeInfo_var);
		ConnectionCollector__ctor_m3524765980(L_1, /*hidden argument*/NULL);
		__this->set_tails_0(L_1);
	}

IL_0016:
	{
		ConnectionCollector_t444796719 * L_2 = (ConnectionCollector_t444796719 *)__this->get_tails_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((List_1_t2425880343 *)L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Add(!0) */, (List_1_t2425880343 *)L_2, (Il2CppObject *)L_3);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::ToSleep()
extern Il2CppClass* Action_1_t2729296284_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1777374079_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m2946257769_MethodInfo_var;
extern const MethodInfo* List_1_ForEach_m1069597881_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1327751148_MethodInfo_var;
extern const MethodInfo* List_1_ForEach_m3025640294_MethodInfo_var;
extern const uint32_t Organism_2_ToSleep_m2529287359_MetadataUsageId;
extern "C"  void Organism_2_ToSleep_m2529287359_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_ToSleep_m2529287359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3377802548 * G_B5_0 = NULL;
	List_1_t3377802548 * G_B4_0 = NULL;
	ConnectionCollector_t444796719 * G_B9_0 = NULL;
	ConnectionCollector_t444796719 * G_B8_0 = NULL;
	{
		bool L_0 = (bool)__this->get_alive_4();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_alive_4((bool)0);
		List_1_t3377802548 * L_1 = (List_1_t3377802548 *)__this->get_children_6();
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		List_1_t3377802548 * L_2 = (List_1_t3377802548 *)__this->get_children_6();
		Action_1_t2729296284 * L_3 = ((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cache9_9();
		G_B4_0 = L_2;
		if (L_3)
		{
			G_B5_0 = L_2;
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		Action_1_t2729296284 * L_5 = (Action_1_t2729296284 *)il2cpp_codegen_object_new(Action_1_t2729296284_il2cpp_TypeInfo_var);
		Action_1__ctor_m2946257769(L_5, (Il2CppObject *)NULL, (IntPtr_t)L_4, /*hidden argument*/Action_1__ctor_m2946257769_MethodInfo_var);
		((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->set_U3CU3Ef__amU24cache9_9(L_5);
		G_B5_0 = G_B4_0;
	}

IL_003c:
	{
		Action_1_t2729296284 * L_6 = ((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cache9_9();
		NullCheck((List_1_t3377802548 *)G_B5_0);
		List_1_ForEach_m1069597881((List_1_t3377802548 *)G_B5_0, (Action_1_t2729296284 *)L_6, /*hidden argument*/List_1_ForEach_m1069597881_MethodInfo_var);
		List_1_t3377802548 * L_7 = (List_1_t3377802548 *)__this->get_children_6();
		NullCheck((List_1_t3377802548 *)L_7);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<IOrganism>::Clear() */, (List_1_t3377802548 *)L_7);
	}

IL_0051:
	{
		ConnectionCollector_t444796719 * L_8 = (ConnectionCollector_t444796719 *)__this->get_tails_0();
		if (!L_8)
		{
			goto IL_008f;
		}
	}
	{
		ConnectionCollector_t444796719 * L_9 = (ConnectionCollector_t444796719 *)__this->get_tails_0();
		Action_1_t1777374079 * L_10 = ((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cacheA_10();
		G_B8_0 = L_9;
		if (L_10)
		{
			G_B9_0 = L_9;
			goto IL_007a;
		}
	}
	{
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t1777374079 * L_12 = (Action_1_t1777374079 *)il2cpp_codegen_object_new(Action_1_t1777374079_il2cpp_TypeInfo_var);
		Action_1__ctor_m1327751148(L_12, (Il2CppObject *)NULL, (IntPtr_t)L_11, /*hidden argument*/Action_1__ctor_m1327751148_MethodInfo_var);
		((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->set_U3CU3Ef__amU24cacheA_10(L_12);
		G_B9_0 = G_B8_0;
	}

IL_007a:
	{
		Action_1_t1777374079 * L_13 = ((Organism_2_t948289219_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->static_fields)->get_U3CU3Ef__amU24cacheA_10();
		NullCheck((List_1_t2425880343 *)G_B9_0);
		List_1_ForEach_m3025640294((List_1_t2425880343 *)G_B9_0, (Action_1_t1777374079 *)L_13, /*hidden argument*/List_1_ForEach_m3025640294_MethodInfo_var);
		ConnectionCollector_t444796719 * L_14 = (ConnectionCollector_t444796719 *)__this->get_tails_0();
		NullCheck((List_1_t2425880343 *)L_14);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Clear() */, (List_1_t2425880343 *)L_14);
	}

IL_008f:
	{
		OnceEmptyStream_t3081939404 * L_15 = (OnceEmptyStream_t3081939404 *)__this->get_fellAsleep__5();
		if (!L_15)
		{
			goto IL_00a5;
		}
	}
	{
		OnceEmptyStream_t3081939404 * L_16 = (OnceEmptyStream_t3081939404 *)__this->get_fellAsleep__5();
		NullCheck((EmptyStream_t2850978573 *)L_16);
		EmptyStream_Send_m2813379598((EmptyStream_t2850978573 *)L_16, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::Put(IOrganism)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3377802548_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3755966385_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1055728564;
extern const uint32_t Organism_2_Put_m1862734501_MetadataUsageId;
extern "C"  void Organism_2_Put_m1862734501_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___organism0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_Put_m1862734501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___organism0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1055728564, /*hidden argument*/NULL);
		return;
	}

IL_0011:
	{
		List_1_t3377802548 * L_1 = (List_1_t3377802548 *)__this->get_children_6();
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		List_1_t3377802548 * L_2 = (List_1_t3377802548 *)il2cpp_codegen_object_new(List_1_t3377802548_il2cpp_TypeInfo_var);
		List_1__ctor_m3755966385(L_2, /*hidden argument*/List_1__ctor_m3755966385_MethodInfo_var);
		__this->set_children_6(L_2);
	}

IL_0027:
	{
		List_1_t3377802548 * L_3 = (List_1_t3377802548 *)__this->get_children_6();
		Il2CppObject * L_4 = ___organism0;
		NullCheck((List_1_t3377802548 *)L_3);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<IOrganism>::Add(!0) */, (List_1_t3377802548 *)L_3, (Il2CppObject *)L_4);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::Take(IOrganism)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2206628300;
extern const uint32_t Organism_2_Take_m340054705_MetadataUsageId;
extern "C"  void Organism_2_Take_m340054705_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___organism0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_Take_m340054705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		List_1_t3377802548 * L_0 = (List_1_t3377802548 *)__this->get_children_6();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2206628300, /*hidden argument*/NULL);
	}

IL_0015:
	{
		List_1_t3377802548 * L_1 = (List_1_t3377802548 *)__this->get_children_6();
		Il2CppObject * L_2 = ___organism0;
		NullCheck((List_1_t3377802548 *)L_1);
		bool L_3 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<IOrganism>::Remove(!0) */, (List_1_t3377802548 *)L_1, (Il2CppObject *)L_2);
		V_0 = (bool)L_3;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_5 = ___organism0;
		NullCheck((Il2CppObject *)L_5);
		InterfaceActionInvoker0::Invoke(3 /* System.Void IOrganism::ToSleep() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
	}

IL_002e:
	{
		return;
	}
}
// IOrganism Organism`2<System.Object,System.Object>::ChangeRoot(TRoot)
extern "C"  Il2CppObject * Organism_2_ChangeRoot_m2085091299_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___r0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___r0;
		return (Il2CppObject *)L_0;
	}
}
// System.Void Organism`2<System.Object,System.Object>::RegisterChildren()
extern const Il2CppType* IOrganism_t2580843579_0_0_0_var;
extern Il2CppClass* List_1_t3377802548_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3755966385_MethodInfo_var;
extern const uint32_t Organism_2_RegisterChildren_m1720665825_MetadataUsageId;
extern "C"  void Organism_2_RegisterChildren_m1720665825_gshared (Organism_2_t948289219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_RegisterChildren_m1720665825_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FieldInfo_t * V_0 = NULL;
	FieldInfoU5BU5D_t1144794227* V_1 = NULL;
	int32_t V_2 = 0;
	{
		List_1_t3377802548 * L_0 = (List_1_t3377802548 *)__this->get_children_6();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t3377802548 * L_1 = (List_1_t3377802548 *)il2cpp_codegen_object_new(List_1_t3377802548_il2cpp_TypeInfo_var);
		List_1__ctor_m3755966385(L_1, /*hidden argument*/List_1__ctor_m3755966385_MethodInfo_var);
		__this->set_children_6(L_1);
	}

IL_0016:
	{
		List_1_t3377802548 * L_2 = (List_1_t3377802548 *)__this->get_children_6();
		NullCheck((List_1_t3377802548 *)L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<IOrganism>::Clear() */, (List_1_t3377802548 *)L_2);
		NullCheck((Il2CppObject *)__this);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		FieldInfoU5BU5D_t1144794227* L_4 = VirtFuncInvoker1< FieldInfoU5BU5D_t1144794227*, int32_t >::Invoke(57 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, (Type_t *)L_3, (int32_t)((int32_t)52));
		V_1 = (FieldInfoU5BU5D_t1144794227*)L_4;
		V_2 = (int32_t)0;
		goto IL_006a;
	}

IL_0036:
	{
		FieldInfoU5BU5D_t1144794227* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (FieldInfo_t *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IOrganism_t2580843579_0_0_0_var), /*hidden argument*/NULL);
		FieldInfo_t * L_9 = V_0;
		NullCheck((FieldInfo_t *)L_9);
		Type_t * L_10 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, (FieldInfo_t *)L_9);
		NullCheck((Type_t *)L_8);
		bool L_11 = VirtFuncInvoker1< bool, Type_t * >::Invoke(46 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_8, (Type_t *)L_10);
		if (!L_11)
		{
			goto IL_0066;
		}
	}
	{
		FieldInfo_t * L_12 = V_0;
		NullCheck((FieldInfo_t *)L_12);
		Il2CppObject * L_13 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, (FieldInfo_t *)L_12, (Il2CppObject *)__this);
		NullCheck((Organism_2_t948289219 *)__this);
		((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Organism_2_t948289219 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IOrganism_t2580843579_il2cpp_TypeInfo_var)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
	}

IL_0066:
	{
		int32_t L_14 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_006a:
	{
		int32_t L_15 = V_2;
		FieldInfoU5BU5D_t1144794227* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::<WakeUp>m__1AD(IOrganism)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t Organism_2_U3CWakeUpU3Em__1AD_m1095000686_MetadataUsageId;
extern "C"  void Organism_2_U3CWakeUpU3Em__1AD_m1095000686_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_U3CWakeUpU3Em__1AD_m1095000686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___child0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(1 /* System.Void IOrganism::WakeUp() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::<Spread>m__1AE(IOrganism)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t Organism_2_U3CSpreadU3Em__1AE_m2535020769_MetadataUsageId;
extern "C"  void Organism_2_U3CSpreadU3Em__1AE_m2535020769_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_U3CSpreadU3Em__1AE_m2535020769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___child0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void IOrganism::Spread() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::<ToSleep>m__1AF(IOrganism)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t Organism_2_U3CToSleepU3Em__1AF_m3215044909_MetadataUsageId;
extern "C"  void Organism_2_U3CToSleepU3Em__1AF_m3215044909_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_U3CToSleepU3Em__1AF_m3215044909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___child0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void IOrganism::ToSleep() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void Organism`2<System.Object,System.Object>::<ToSleep>m__1B0(System.IDisposable)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Organism_2_U3CToSleepU3Em__1B0_m3346233993_MetadataUsageId;
extern "C"  void Organism_2_U3CToSleepU3Em__1B0_m3346233993_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___d0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Organism_2_U3CToSleepU3Em__1B0_m3346233993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___d0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void PlayFab.Internal.ResultContainer`1<System.Object>::.ctor()
extern "C"  void ResultContainer_1__ctor_m944826126_gshared (ResultContainer_1_t1789381198 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.Internal.ResultContainer`1<System.Object>::.cctor()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t ResultContainer_1__cctor_m3037709919_MetadataUsageId;
extern "C"  void ResultContainer_1__cctor_m3037709919_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResultContainer_1__cctor_m3037709919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ResultContainer_1_t1789381198_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__invokeParams_6(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// PlayFab.Internal.ResultContainer`1<TResultType> PlayFab.Internal.ResultContainer`1<System.Object>::KillWarnings()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m944559736_MethodInfo_var;
extern const uint32_t ResultContainer_1_KillWarnings_m2638698408_MetadataUsageId;
extern "C"  ResultContainer_1_t1789381198 * ResultContainer_1_KillWarnings_m2638698408_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResultContainer_1_KillWarnings_m2638698408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ResultContainer_1_t1789381198 * V_0 = NULL;
	{
		ResultContainer_1_t1789381198 * L_0 = (ResultContainer_1_t1789381198 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ResultContainer_1_t1789381198 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (ResultContainer_1_t1789381198 *)L_0;
		ResultContainer_1_t1789381198 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_code_0(((int32_t)200));
		ResultContainer_1_t1789381198 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_2);
		L_2->set_status_1(L_3);
		ResultContainer_1_t1789381198 * L_4 = V_0;
		Nullable_1_t1438485399  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Nullable_1__ctor_m944559736(&L_5, (int32_t)0, /*hidden argument*/Nullable_1__ctor_m944559736_MethodInfo_var);
		NullCheck(L_4);
		L_4->set_errorCode_2(L_5);
		ResultContainer_1_t1789381198 * L_6 = V_0;
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_6);
		L_6->set_errorMessage_3(L_7);
		ResultContainer_1_t1789381198 * L_8 = V_0;
		NullCheck(L_8);
		L_8->set_errorDetails_4((Dictionary_2_t3403145775 *)NULL);
		ResultContainer_1_t1789381198 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_data_5(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
		ResultContainer_1_t1789381198 * L_10 = V_0;
		return L_10;
	}
}
// TResultType PlayFab.Internal.ResultContainer`1<System.Object>::HandleResults(PlayFab.CallRequestContainer,System.Delegate,PlayFab.ErrorCallback,System.Action`2<TResultType,PlayFab.CallRequestContainer>)
extern Il2CppClass* Util_t1193378922_il2cpp_TypeInfo_var;
extern Il2CppClass* SimpleJson_t1409256731_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayFabSettings_t4113663895_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayFabError_t750598646_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m1686547625_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m844974555_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3315626398;
extern const uint32_t ResultContainer_1_HandleResults_m887745544_MetadataUsageId;
extern "C"  Il2CppObject * ResultContainer_1_HandleResults_m887745544_gshared (Il2CppObject * __this /* static, unused */, CallRequestContainer_t432318609 * ___callRequest0, Delegate_t3660574010 * ___resultCallback1, ErrorCallback_t582108558 * ___errorCallback2, Action_2_t3700672107 * ___resultAction3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResultContainer_1_HandleResults_m887745544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ResultContainer_1_t1789381198 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	PlayFabError_t750598646 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CallRequestContainer_t432318609 * L_0 = ___callRequest0;
		NullCheck(L_0);
		PlayFabError_t750598646 * L_1 = (PlayFabError_t750598646 *)L_0->get_Error_11();
		if (L_1)
		{
			goto IL_016f;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			CallRequestContainer_t432318609 * L_2 = ___callRequest0;
			NullCheck(L_2);
			String_t* L_3 = (String_t*)L_2->get_ResultStr_8();
			IL2CPP_RUNTIME_CLASS_INIT(Util_t1193378922_il2cpp_TypeInfo_var);
			MyJsonSerializerStrategy_t989142809 * L_4 = ((Util_t1193378922_StaticFields*)Util_t1193378922_il2cpp_TypeInfo_var->static_fields)->get_ApiSerializerStrategy_4();
			IL2CPP_RUNTIME_CLASS_INIT(SimpleJson_t1409256731_il2cpp_TypeInfo_var);
			ResultContainer_1_t1789381198 * L_5 = ((  ResultContainer_1_t1789381198 * (*) (Il2CppObject * /* static, unused */, String_t*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (String_t*)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_0 = (ResultContainer_1_t1789381198 *)L_5;
			ResultContainer_1_t1789381198 * L_6 = V_0;
			NullCheck(L_6);
			Nullable_1_t1438485399 * L_7 = (Nullable_1_t1438485399 *)L_6->get_address_of_errorCode_2();
			bool L_8 = Nullable_1_get_HasValue_m1686547625((Nullable_1_t1438485399 *)L_7, /*hidden argument*/Nullable_1_get_HasValue_m1686547625_MethodInfo_var);
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_002c:
		{
			ResultContainer_1_t1789381198 * L_9 = V_0;
			NullCheck(L_9);
			Nullable_1_t1438485399 * L_10 = (Nullable_1_t1438485399 *)L_9->get_address_of_errorCode_2();
			int32_t L_11 = Nullable_1_get_Value_m844974555((Nullable_1_t1438485399 *)L_10, /*hidden argument*/Nullable_1_get_Value_m844974555_MethodInfo_var);
			if (L_11)
			{
				goto IL_00d9;
			}
		}

IL_003c:
		{
			ResultContainer_1_t1789381198 * L_12 = V_0;
			NullCheck(L_12);
			Il2CppObject * L_13 = (Il2CppObject *)L_12->get_data_5();
			CallRequestContainer_t432318609 * L_14 = ___callRequest0;
			NullCheck(L_14);
			Il2CppObject * L_15 = (Il2CppObject *)L_14->get_Request_7();
			NullCheck(L_13);
			((PlayFabResultCommon_t379512675 *)L_13)->set_Request_0(L_15);
			ResultContainer_1_t1789381198 * L_16 = V_0;
			NullCheck(L_16);
			Il2CppObject * L_17 = (Il2CppObject *)L_16->get_data_5();
			CallRequestContainer_t432318609 * L_18 = ___callRequest0;
			NullCheck(L_18);
			Il2CppObject * L_19 = (Il2CppObject *)L_18->get_CustomData_9();
			NullCheck(L_17);
			((PlayFabResultCommon_t379512675 *)L_17)->set_CustomData_1(L_19);
			Action_2_t3700672107 * L_20 = ___resultAction3;
			if (!L_20)
			{
				goto IL_007b;
			}
		}

IL_006e:
		{
			Action_2_t3700672107 * L_21 = ___resultAction3;
			ResultContainer_1_t1789381198 * L_22 = V_0;
			NullCheck(L_22);
			Il2CppObject * L_23 = (Il2CppObject *)L_22->get_data_5();
			CallRequestContainer_t432318609 * L_24 = ___callRequest0;
			NullCheck((Action_2_t3700672107 *)L_21);
			((  void (*) (Action_2_t3700672107 *, Il2CppObject *, CallRequestContainer_t432318609 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Action_2_t3700672107 *)L_21, (Il2CppObject *)L_23, (CallRequestContainer_t432318609 *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		}

IL_007b:
		{
			Delegate_t3660574010 * L_25 = ___resultCallback1;
			if (!L_25)
			{
				goto IL_009f;
			}
		}

IL_0081:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			ObjectU5BU5D_t11523773* L_26 = ((ResultContainer_1_t1789381198_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__invokeParams_6();
			ResultContainer_1_t1789381198 * L_27 = V_0;
			NullCheck(L_27);
			Il2CppObject * L_28 = (Il2CppObject *)L_27->get_data_5();
			NullCheck(L_26);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
			ArrayElementTypeCheck (L_26, L_28);
			(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_28);
			Delegate_t3660574010 * L_29 = ___resultCallback1;
			ObjectU5BU5D_t11523773* L_30 = ((ResultContainer_1_t1789381198_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__invokeParams_6();
			NullCheck((Delegate_t3660574010 *)L_29);
			Delegate_DynamicInvoke_m3298408440((Delegate_t3660574010 *)L_29, (ObjectU5BU5D_t11523773*)L_30, /*hidden argument*/NULL);
		}

IL_009f:
		{
			CallRequestContainer_t432318609 * L_31 = ___callRequest0;
			NullCheck(L_31);
			String_t* L_32 = (String_t*)L_31->get_Url_2();
			CallRequestContainer_t432318609 * L_33 = ___callRequest0;
			NullCheck(L_33);
			int32_t L_34 = (int32_t)L_33->get_CallId_3();
			CallRequestContainer_t432318609 * L_35 = ___callRequest0;
			NullCheck(L_35);
			Il2CppObject * L_36 = (Il2CppObject *)L_35->get_Request_7();
			ResultContainer_1_t1789381198 * L_37 = V_0;
			NullCheck(L_37);
			Il2CppObject * L_38 = (Il2CppObject *)L_37->get_data_5();
			CallRequestContainer_t432318609 * L_39 = ___callRequest0;
			NullCheck(L_39);
			PlayFabError_t750598646 * L_40 = (PlayFabError_t750598646 *)L_39->get_Error_11();
			CallRequestContainer_t432318609 * L_41 = ___callRequest0;
			NullCheck(L_41);
			Il2CppObject * L_42 = (Il2CppObject *)L_41->get_CustomData_9();
			IL2CPP_RUNTIME_CLASS_INIT(PlayFabSettings_t4113663895_il2cpp_TypeInfo_var);
			PlayFabSettings_InvokeResponse_m3170466598(NULL /*static, unused*/, (String_t*)L_32, (int32_t)L_34, (Il2CppObject *)L_36, (Il2CppObject *)L_38, (PlayFabError_t750598646 *)L_40, (Il2CppObject *)L_42, /*hidden argument*/NULL);
			ResultContainer_1_t1789381198 * L_43 = V_0;
			NullCheck(L_43);
			Il2CppObject * L_44 = (Il2CppObject *)L_43->get_data_5();
			V_2 = (Il2CppObject *)L_44;
			goto IL_01a2;
		}

IL_00d9:
		{
			CallRequestContainer_t432318609 * L_45 = ___callRequest0;
			PlayFabError_t750598646 * L_46 = (PlayFabError_t750598646 *)il2cpp_codegen_object_new(PlayFabError_t750598646_il2cpp_TypeInfo_var);
			PlayFabError__ctor_m1421089695(L_46, /*hidden argument*/NULL);
			V_3 = (PlayFabError_t750598646 *)L_46;
			PlayFabError_t750598646 * L_47 = V_3;
			ResultContainer_1_t1789381198 * L_48 = V_0;
			NullCheck(L_48);
			int32_t L_49 = (int32_t)L_48->get_code_0();
			NullCheck(L_47);
			L_47->set_HttpCode_0(L_49);
			PlayFabError_t750598646 * L_50 = V_3;
			ResultContainer_1_t1789381198 * L_51 = V_0;
			NullCheck(L_51);
			String_t* L_52 = (String_t*)L_51->get_status_1();
			NullCheck(L_50);
			L_50->set_HttpStatus_1(L_52);
			PlayFabError_t750598646 * L_53 = V_3;
			ResultContainer_1_t1789381198 * L_54 = V_0;
			NullCheck(L_54);
			Nullable_1_t1438485399 * L_55 = (Nullable_1_t1438485399 *)L_54->get_address_of_errorCode_2();
			int32_t L_56 = Nullable_1_get_Value_m844974555((Nullable_1_t1438485399 *)L_55, /*hidden argument*/Nullable_1_get_Value_m844974555_MethodInfo_var);
			NullCheck(L_53);
			L_53->set_Error_2(L_56);
			PlayFabError_t750598646 * L_57 = V_3;
			ResultContainer_1_t1789381198 * L_58 = V_0;
			NullCheck(L_58);
			String_t* L_59 = (String_t*)L_58->get_errorMessage_3();
			NullCheck(L_57);
			L_57->set_ErrorMessage_3(L_59);
			PlayFabError_t750598646 * L_60 = V_3;
			ResultContainer_1_t1789381198 * L_61 = V_0;
			NullCheck(L_61);
			Dictionary_2_t3403145775 * L_62 = (Dictionary_2_t3403145775 *)L_61->get_errorDetails_4();
			NullCheck(L_60);
			L_60->set_ErrorDetails_4(L_62);
			PlayFabError_t750598646 * L_63 = V_3;
			NullCheck(L_45);
			L_45->set_Error_11(L_63);
			goto IL_016f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_012c;
		throw e;
	}

CATCH_012c:
	{ // begin catch(System.Exception)
		V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		CallRequestContainer_t432318609 * L_64 = ___callRequest0;
		PlayFabError_t750598646 * L_65 = (PlayFabError_t750598646 *)il2cpp_codegen_object_new(PlayFabError_t750598646_il2cpp_TypeInfo_var);
		PlayFabError__ctor_m1421089695(L_65, /*hidden argument*/NULL);
		V_3 = (PlayFabError_t750598646 *)L_65;
		PlayFabError_t750598646 * L_66 = V_3;
		NullCheck(L_66);
		L_66->set_HttpCode_0(((int32_t)200));
		PlayFabError_t750598646 * L_67 = V_3;
		NullCheck(L_67);
		L_67->set_HttpStatus_1(_stringLiteral3315626398);
		PlayFabError_t750598646 * L_68 = V_3;
		NullCheck(L_68);
		L_68->set_Error_2(1);
		PlayFabError_t750598646 * L_69 = V_3;
		Exception_t1967233988 * L_70 = V_1;
		NullCheck((Exception_t1967233988 *)L_70);
		String_t* L_71 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, (Exception_t1967233988 *)L_70);
		NullCheck(L_69);
		L_69->set_ErrorMessage_3(L_71);
		PlayFabError_t750598646 * L_72 = V_3;
		NullCheck(L_72);
		L_72->set_ErrorDetails_4((Dictionary_2_t3403145775 *)NULL);
		PlayFabError_t750598646 * L_73 = V_3;
		NullCheck(L_64);
		L_64->set_Error_11(L_73);
		goto IL_016f;
	} // end catch (depth: 1)

IL_016f:
	{
		ErrorCallback_t582108558 * L_74 = ___errorCallback2;
		if (!L_74)
		{
			goto IL_0181;
		}
	}
	{
		ErrorCallback_t582108558 * L_75 = ___errorCallback2;
		CallRequestContainer_t432318609 * L_76 = ___callRequest0;
		NullCheck(L_76);
		PlayFabError_t750598646 * L_77 = (PlayFabError_t750598646 *)L_76->get_Error_11();
		NullCheck((ErrorCallback_t582108558 *)L_75);
		ErrorCallback_Invoke_m2764045351((ErrorCallback_t582108558 *)L_75, (PlayFabError_t750598646 *)L_77, /*hidden argument*/NULL);
	}

IL_0181:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabSettings_t4113663895_il2cpp_TypeInfo_var);
		ErrorCallback_t582108558 * L_78 = ((PlayFabSettings_t4113663895_StaticFields*)PlayFabSettings_t4113663895_il2cpp_TypeInfo_var->static_fields)->get_GlobalErrorHandler_3();
		if (!L_78)
		{
			goto IL_019b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabSettings_t4113663895_il2cpp_TypeInfo_var);
		ErrorCallback_t582108558 * L_79 = ((PlayFabSettings_t4113663895_StaticFields*)PlayFabSettings_t4113663895_il2cpp_TypeInfo_var->static_fields)->get_GlobalErrorHandler_3();
		CallRequestContainer_t432318609 * L_80 = ___callRequest0;
		NullCheck(L_80);
		PlayFabError_t750598646 * L_81 = (PlayFabError_t750598646 *)L_80->get_Error_11();
		NullCheck((ErrorCallback_t582108558 *)L_79);
		ErrorCallback_Invoke_m2764045351((ErrorCallback_t582108558 *)L_79, (PlayFabError_t750598646 *)L_81, /*hidden argument*/NULL);
	}

IL_019b:
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}

IL_01a2:
	{
		Il2CppObject * L_82 = V_2;
		return L_82;
	}
}
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::.ctor()
extern "C"  void SingletonMonoBehaviour_1__ctor_m4044296597_gshared (SingletonMonoBehaviour_1_t312324993 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t3012272455 *)__this);
		MonoBehaviour__ctor_m350695606((MonoBehaviour_t3012272455 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::get_instance()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const uint32_t SingletonMonoBehaviour_1_get_instance_m1518032716_MetadataUsageId;
extern "C"  Il2CppObject * SingletonMonoBehaviour_1_get_instance_m1518032716_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonMonoBehaviour_1_get_instance_m1518032716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_instance_2();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_m_instance_2(L_2);
		Il2CppObject * L_3 = ((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_instance_2();
		bool L_4 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_5);
		GameObject_t4012695102 * L_7 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
		GameObject__ctor_m1336994365(L_7, (String_t*)L_6, /*hidden argument*/NULL);
		V_0 = (GameObject_t4012695102 *)L_7;
		GameObject_t4012695102 * L_8 = V_0;
		NullCheck((GameObject_t4012695102 *)L_8);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((GameObject_t4012695102 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_m_instance_2(L_9);
		GameObject_t4012695102 * L_10 = V_0;
		Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, (Object_t3878351788 *)L_10, /*hidden argument*/NULL);
	}

IL_005a:
	{
		NullCheck((SingletonMonoBehaviour_1_t312324993 *)(*(((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of_m_instance_2())));
		bool L_11 = ((  bool (*) (SingletonMonoBehaviour_1_t312324993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((SingletonMonoBehaviour_1_t312324993 *)(*(((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of_m_instance_2())), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		if (L_11)
		{
			goto IL_0090;
		}
	}
	{
		NullCheck((SingletonMonoBehaviour_1_t312324993 *)(*(((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of_m_instance_2())));
		VirtActionInvoker0::Invoke(4 /* System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::Initialize() */, (SingletonMonoBehaviour_1_t312324993 *)(*(((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of_m_instance_2())));
		NullCheck((SingletonMonoBehaviour_1_t312324993 *)(*(((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of_m_instance_2())));
		((  void (*) (SingletonMonoBehaviour_1_t312324993 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((SingletonMonoBehaviour_1_t312324993 *)(*(((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of_m_instance_2())), (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_0090:
	{
		Il2CppObject * L_12 = ((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_instance_2();
		return L_12;
	}
}
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::Awake()
extern "C"  void SingletonMonoBehaviour_1_Awake_m4281901816_gshared (SingletonMonoBehaviour_1_t312324993 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((SingletonMonoBehaviour_1_t312324993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_m_instance_2();
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_2 = Component_get_gameObject_m2112202034((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, (Object_t3878351788 *)L_2, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Boolean PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::get_initialized()
extern "C"  bool SingletonMonoBehaviour_1_get_initialized_m1888521674_gshared (SingletonMonoBehaviour_1_t312324993 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CinitializedU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::set_initialized(System.Boolean)
extern "C"  void SingletonMonoBehaviour_1_set_initialized_m1544018177_gshared (SingletonMonoBehaviour_1_t312324993 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CinitializedU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::Initialize()
extern "C"  void SingletonMonoBehaviour_1_Initialize_m2506226207_gshared (SingletonMonoBehaviour_1_t312324993 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ProcessApiCallback_1__ctor_m1266910666_gshared (ProcessApiCallback_1_t3425006930 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>::Invoke(TResult)
extern "C"  void ProcessApiCallback_1_Invoke_m1002635741_gshared (ProcessApiCallback_1_t3425006930 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ProcessApiCallback_1_Invoke_m1002635741((ProcessApiCallback_1_t3425006930 *)__this->get_prev_9(),___result0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___result0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___result0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>::BeginInvoke(TResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ProcessApiCallback_1_BeginInvoke_m1317674820_gshared (ProcessApiCallback_1_t3425006930 * __this, Il2CppObject * ___result0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___result0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void PlayFab.PlayFabClientAPI/ProcessApiCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ProcessApiCallback_1_EndInvoke_m3326438618_gshared (ProcessApiCallback_1_t3425006930 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PlayFab.PlayFabSettings/RequestCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestCallback_1__ctor_m804351314_gshared (RequestCallback_1_t923153878 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayFab.PlayFabSettings/RequestCallback`1<System.Object>::Invoke(System.String,System.Int32,TRequest,System.Object)
extern "C"  void RequestCallback_1_Invoke_m2965292920_gshared (RequestCallback_1_t923153878 * __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RequestCallback_1_Invoke_m2965292920((RequestCallback_1_t923153878 *)__this->get_prev_9(),___urlPath0, ___callId1, ___request2, ___customData3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___urlPath0, ___callId1, ___request2, ___customData3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___urlPath0, ___callId1, ___request2, ___customData3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___urlPath0, ___callId1, ___request2, ___customData3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PlayFab.PlayFabSettings/RequestCallback`1<System.Object>::BeginInvoke(System.String,System.Int32,TRequest,System.Object,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t RequestCallback_1_BeginInvoke_m2391617785_MetadataUsageId;
extern "C"  Il2CppObject * RequestCallback_1_BeginInvoke_m2391617785_gshared (RequestCallback_1_t923153878 * __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RequestCallback_1_BeginInvoke_m2391617785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___urlPath0;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___callId1);
	__d_args[2] = ___request2;
	__d_args[3] = ___customData3;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void PlayFab.PlayFabSettings/RequestCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void RequestCallback_1_EndInvoke_m312760418_gshared (RequestCallback_1_t923153878 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ResponseCallback_2__ctor_m3836631829_gshared (ResponseCallback_2_t3240762047 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>::Invoke(System.String,System.Int32,TRequest,TResult,PlayFab.PlayFabError,System.Object)
extern "C"  void ResponseCallback_2_Invoke_m2226430690_gshared (ResponseCallback_2_t3240762047 * __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ResponseCallback_2_Invoke_m2226430690((ResponseCallback_2_t3240762047 *)__this->get_prev_9(),___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>::BeginInvoke(System.String,System.Int32,TRequest,TResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t ResponseCallback_2_BeginInvoke_m1492811023_MetadataUsageId;
extern "C"  Il2CppObject * ResponseCallback_2_BeginInvoke_m1492811023_gshared (ResponseCallback_2_t3240762047 * __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResponseCallback_2_BeginInvoke_m1492811023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[7] = {0};
	__d_args[0] = ___urlPath0;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___callId1);
	__d_args[2] = ___request2;
	__d_args[3] = ___result3;
	__d_args[4] = ___error4;
	__d_args[5] = ___customData5;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback6, (Il2CppObject*)___object7);
}
// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ResponseCallback_2_EndInvoke_m1596759973_gshared (ResponseCallback_2_t3240762047 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2__ctor_m145995086_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2__ctor_m145995086_gshared (ThreadSafeDictionary_2_t1163962996 * __this, ThreadSafeDictionaryValueFactory_2_t1771086527 * ___valueFactory0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2__ctor_m145995086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ThreadSafeDictionaryValueFactory_2_t1771086527 * L_1 = ___valueFactory0;
		__this->set__valueFactory_1(L_1);
		return;
	}
}
// System.Collections.IEnumerator PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m647792317_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t3824425150 *)L_0);
		Enumerator_t3591453092  L_1 = ((  Enumerator_t3591453092  (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t3824425150 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Enumerator_t3591453092  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_2);
		return (Il2CppObject *)L_3;
	}
}
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_Get_m3585103853_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1163962996 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ThreadSafeDictionary_2_t1163962996 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_2;
	}

IL_0013:
	{
		Dictionary_2_t3824425150 * L_3 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
		Il2CppObject * L_4 = ___key0;
		NullCheck((Dictionary_2_t3824425150 *)L_3);
		bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, (Dictionary_2_t3824425150 *)L_3, (Il2CppObject *)L_4, (Il2CppObject **)(&V_0));
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_6 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1163962996 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ThreadSafeDictionary_2_t1163962996 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_7;
	}

IL_002e:
	{
		Il2CppObject * L_8 = V_0;
		return L_8;
	}
}
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_AddValue_m844388037_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t3824425150 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ThreadSafeDictionaryValueFactory_2_t1771086527 * L_0 = (ThreadSafeDictionaryValueFactory_2_t1771086527 *)__this->get__valueFactory_1();
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionaryValueFactory_2_t1771086527 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t1771086527 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ThreadSafeDictionaryValueFactory_2_t1771086527 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3824425150 * L_5 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t3824425150 * L_6 = (Dictionary_2_t3824425150 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			((  void (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			__this->set__dictionary_2(L_6);
			Dictionary_2_t3824425150 * L_7 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
			Il2CppObject * L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t3824425150 *)L_7);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, (Dictionary_2_t3824425150 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t3824425150 * L_10 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
			Il2CppObject * L_11 = ___key0;
			NullCheck((Dictionary_2_t3824425150 *)L_10);
			bool L_12 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, (Dictionary_2_t3824425150 *)L_10, (Il2CppObject *)L_11, (Il2CppObject **)(&V_2));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x86, FINALLY_007d);
		}

IL_005d:
		{
			Dictionary_2_t3824425150 * L_14 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
			Dictionary_2_t3824425150 * L_15 = (Dictionary_2_t3824425150 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			((  void (*) (Dictionary_2_t3824425150 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			V_3 = (Dictionary_2_t3824425150 *)L_15;
			Dictionary_2_t3824425150 * L_16 = V_3;
			Il2CppObject * L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t3824425150 *)L_16);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, (Dictionary_2_t3824425150 *)L_16, (Il2CppObject *)L_17, (Il2CppObject *)L_18);
			Dictionary_2_t3824425150 * L_19 = V_3;
			__this->set__dictionary_2(L_19);
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x84, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_20 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(125)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0084:
	{
		Il2CppObject * L_21 = V_0;
		return L_21;
	}

IL_0086:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Add_m3884150033_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Add_m3884150033_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Add_m3884150033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ThreadSafeDictionary_2_ContainsKey_m2111272811_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t3824425150 *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0) */, (Dictionary_2_t3824425150 *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Keys_m79078016_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t3824425150 *)L_0);
		KeyCollection_t1852733134 * L_1 = ((  KeyCollection_t1852733134 * (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t3824425150 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return L_1;
	}
}
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Remove_m3287321253_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Remove_m3287321253_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Remove_m3287321253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ThreadSafeDictionary_2_TryGetValue_m2392274116_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		Il2CppObject ** L_0 = ___value1;
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1163962996 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((ThreadSafeDictionary_2_t1163962996 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		(*(Il2CppObject **)L_0) = L_2;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_0, L_2);
		return (bool)1;
	}
}
// System.Collections.Generic.ICollection`1<TValue> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Values_m2528035356_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t3824425150 *)L_0);
		ValueCollection_t1451594948 * L_1 = ((  ValueCollection_t1451594948 * (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Dictionary_2_t3824425150 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_1;
	}
}
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_get_Item_m1128780497_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1163962996 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((ThreadSafeDictionary_2_t1163962996 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_1;
	}
}
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_set_Item_m1582880484_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_set_Item_m1582880484_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_set_Item_m1582880484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Add_m1635310772_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Add_m1635310772_gshared (ThreadSafeDictionary_2_t1163962996 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Add_m1635310772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Clear_m3600968783_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Clear_m3600968783_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Clear_m3600968783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Contains_m78891804_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Contains_m78891804_gshared (ThreadSafeDictionary_2_t1163962996 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Contains_m78891804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_CopyTo_m3361965976_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_CopyTo_m3361965976_gshared (ThreadSafeDictionary_2_t1163962996 * __this, KeyValuePair_2U5BU5D_t346249057* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_CopyTo_m3361965976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ThreadSafeDictionary_2_get_Count_m187477558_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t3824425150 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count() */, (Dictionary_2_t3824425150 *)L_0);
		return L_1;
	}
}
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_get_IsReadOnly_m3744395425_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_get_IsReadOnly_m3744395425_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_get_IsReadOnly_m3744395425_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Remove_m4054976833_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Remove_m4054976833_gshared (ThreadSafeDictionary_2_t1163962996 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Remove_m4054976833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_GetEnumerator_m2011494711_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3824425150 * L_0 = (Dictionary_2_t3824425150 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t3824425150 *)L_0);
		Enumerator_t3591453092  L_1 = ((  Enumerator_t3591453092  (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t3824425150 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Enumerator_t3591453092  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_2);
		return (Il2CppObject*)L_3;
	}
}
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ThreadSafeDictionaryValueFactory_2__ctor_m191619557_gshared (ThreadSafeDictionaryValueFactory_2_t1771086527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::Invoke(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_Invoke_m808333140_gshared (ThreadSafeDictionaryValueFactory_2_t1771086527 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ThreadSafeDictionaryValueFactory_2_Invoke_m808333140((ThreadSafeDictionaryValueFactory_2_t1771086527 *)__this->get_prev_9(),___key0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::BeginInvoke(TKey,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3493335453_gshared (ThreadSafeDictionaryValueFactory_2_t1771086527 * __this, Il2CppObject * ___key0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___key0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_EndInvoke_m2172200533_gshared (ThreadSafeDictionaryValueFactory_2_t1771086527 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void ReactiveUI/<SetContent>c__AnonStorey120`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey120_1__ctor_m2428989317_gshared (U3CSetContentU3Ec__AnonStorey120_1_t1908708797 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReactiveUI/<SetContent>c__AnonStorey120`1<System.Object>::<>m__1B1(T)
extern "C"  void U3CSetContentU3Ec__AnonStorey120_1_U3CU3Em__1B1_m1719215402_gshared (U3CSetContentU3Ec__AnonStorey120_1_t1908708797 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Text_t3286458198 * L_0 = (Text_t3286458198 *)__this->get_text_0();
		NullCheck((Il2CppObject *)(*(&___obj0)));
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&___obj0)));
		NullCheck((Text_t3286458198 *)L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, (Text_t3286458198 *)L_0, (String_t*)L_1);
		return;
	}
}
// System.Void ReactiveUI/<SetContent>c__AnonStorey121`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey121_1__ctor_m3390603334_gshared (U3CSetContentU3Ec__AnonStorey121_1_t1463683798 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReactiveUI/<SetContent>c__AnonStorey121`1<System.Object>::<>m__1B2(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CSetContentU3Ec__AnonStorey121_1_U3CU3Em__1B2_m2070045130_MetadataUsageId;
extern "C"  void U3CSetContentU3Ec__AnonStorey121_1_U3CU3Em__1B2_m2070045130_gshared (U3CSetContentU3Ec__AnonStorey121_1_t1463683798 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSetContentU3Ec__AnonStorey121_1_U3CU3Em__1B2_m2070045130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t3286458198 * L_0 = (Text_t3286458198 *)__this->get_text_1();
		String_t* L_1 = (String_t*)__this->get_format_0();
		Il2CppObject * L_2 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2471250780(NULL /*static, unused*/, (String_t*)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Text_t3286458198 *)L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, (Text_t3286458198 *)L_0, (String_t*)L_3);
		return;
	}
}
// System.Void ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey123_1__ctor_m2136602973_gshared (U3CSetContentU3Ec__AnonStorey123_1_t573633800 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1<System.Object>::<>m__1B8(System.Func`2<T,System.Object>)
extern "C"  Il2CppObject * U3CSetContentU3Ec__AnonStorey123_1_U3CU3Em__1B8_m646068559_gshared (U3CSetContentU3Ec__AnonStorey123_1_t573633800 * __this, Func_2_t2135783352 * ___func0, const MethodInfo* method)
{
	{
		Func_2_t2135783352 * L_0 = ___func0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_obj_0();
		NullCheck((Func_2_t2135783352 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t2135783352 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Void ReactiveUI/<SetContent>c__AnonStorey122`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey122_1__ctor_m57250055_gshared (U3CSetContentU3Ec__AnonStorey122_1_t1018658799 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReactiveUI/<SetContent>c__AnonStorey122`1<System.Object>::<>m__1B3(T)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CSetContentU3Ec__AnonStorey122_1_U3CU3Em__1B3_m2420874858_MetadataUsageId;
extern "C"  void U3CSetContentU3Ec__AnonStorey122_1_U3CU3Em__1B3_m2420874858_gshared (U3CSetContentU3Ec__AnonStorey122_1_t1018658799 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSetContentU3Ec__AnonStorey122_1_U3CU3Em__1B3_m2420874858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSetContentU3Ec__AnonStorey123_1_t573633800 * V_0 = NULL;
	{
		U3CSetContentU3Ec__AnonStorey123_1_t573633800 * L_0 = (U3CSetContentU3Ec__AnonStorey123_1_t573633800 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSetContentU3Ec__AnonStorey123_1_t573633800 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSetContentU3Ec__AnonStorey123_1_t573633800 *)L_0;
		U3CSetContentU3Ec__AnonStorey123_1_t573633800 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24290_1(__this);
		U3CSetContentU3Ec__AnonStorey123_1_t573633800 * L_2 = V_0;
		Il2CppObject * L_3 = ___obj0;
		NullCheck(L_2);
		L_2->set_obj_0(L_3);
		Text_t3286458198 * L_4 = (Text_t3286458198 *)__this->get_text_2();
		String_t* L_5 = (String_t*)__this->get_format_0();
		Func_2U5BU5D_t4139244137* L_6 = (Func_2U5BU5D_t4139244137*)__this->get_parameters_1();
		U3CSetContentU3Ec__AnonStorey123_1_t573633800 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Func_2_t1968536420 * L_9 = (Func_2_t1968536420 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Func_2_t1968536420 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject* L_10 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1968536420 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_6, (Func_2_t1968536420 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2471250780(NULL /*static, unused*/, (String_t*)L_5, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		NullCheck((Text_t3286458198 *)L_4);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, (Text_t3286458198 *)L_4, (String_t*)L_11);
		return;
	}
}
// System.Void ReactiveUI/<SetContent>c__AnonStorey124`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey124_1__ctor_m1980478089_gshared (U3CSetContentU3Ec__AnonStorey124_1_t128608801 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String ReactiveUI/<SetContent>c__AnonStorey124`1<System.Object>::<>m__1B4(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Cast_TisIl2CppObject_m3904275306_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisIl2CppObject_m1058801104_MethodInfo_var;
extern const uint32_t U3CSetContentU3Ec__AnonStorey124_1_U3CU3Em__1B4_m910397865_MetadataUsageId;
extern "C"  String_t* U3CSetContentU3Ec__AnonStorey124_1_U3CU3Em__1B4_m910397865_gshared (U3CSetContentU3Ec__AnonStorey124_1_t128608801 * __this, Il2CppObject* ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSetContentU3Ec__AnonStorey124_1_U3CU3Em__1B4_m910397865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (String_t*)__this->get_format_0();
		Il2CppObject* L_1 = ___val0;
		Il2CppObject* L_2 = Enumerable_Cast_TisIl2CppObject_m3904275306(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/Enumerable_Cast_TisIl2CppObject_m3904275306_MethodInfo_var);
		ObjectU5BU5D_t11523773* L_3 = Enumerable_ToArray_TisIl2CppObject_m1058801104(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/Enumerable_ToArray_TisIl2CppObject_m1058801104_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m4050103162(NULL /*static, unused*/, (String_t*)L_0, (ObjectU5BU5D_t11523773*)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Boolean>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m4065999468_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Boolean>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2057873904_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2057873904_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2057873904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t359458046 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t4157809524 * L_0 = (ImmutableList_1_t4157809524 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t4157809524 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Boolean>>::GetEnumerator() */, (ImmutableList_1_t4157809524 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t359458046 * L_3 = InterfaceFuncInvoker0< Action_1_t359458046 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Boolean>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t359458046 *)L_3;
			Action_1_t359458046 * L_4 = V_0;
			bool L_5 = (bool)__this->get_t_1();
			NullCheck((Action_1_t359458046 *)L_4);
			((  void (*) (Action_1_t359458046 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t359458046 *)L_4, (bool)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.DateTime>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m644935041_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.DateTime>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m406495931_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m406495931_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m406495931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t487486641 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t4285838119 * L_0 = (ImmutableList_1_t4285838119 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t4285838119 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>::GetEnumerator() */, (ImmutableList_1_t4285838119 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t487486641 * L_3 = InterfaceFuncInvoker0< Action_1_t487486641 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.DateTime>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t487486641 *)L_3;
			Action_1_t487486641 * L_4 = V_0;
			DateTime_t339033936  L_5 = (DateTime_t339033936 )__this->get_t_1();
			NullCheck((Action_1_t487486641 *)L_4);
			((  void (*) (Action_1_t487486641 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t487486641 *)L_4, (DateTime_t339033936 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Double>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1518789239_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Double>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m1600127493_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m1600127493_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m1600127493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t682969319 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t186353501 * L_0 = (ImmutableList_1_t186353501 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t186353501 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Double>>::GetEnumerator() */, (ImmutableList_1_t186353501 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t682969319 * L_3 = InterfaceFuncInvoker0< Action_1_t682969319 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Double>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t682969319 *)L_3;
			Action_1_t682969319 * L_4 = V_0;
			double L_5 = (double)__this->get_t_1();
			NullCheck((Action_1_t682969319 *)L_4);
			((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t682969319 *)L_4, (double)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int32>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m2866309394_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int32>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m513749386_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m513749386_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m513749386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t2995867492 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t2499251674 * L_0 = (ImmutableList_1_t2499251674 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t2499251674 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Int32>>::GetEnumerator() */, (ImmutableList_1_t2499251674 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t2995867492 * L_3 = InterfaceFuncInvoker0< Action_1_t2995867492 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Int32>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t2995867492 *)L_3;
			Action_1_t2995867492 * L_4 = V_0;
			int32_t L_5 = (int32_t)__this->get_t_1();
			NullCheck((Action_1_t2995867492 *)L_4);
			((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t2995867492 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int64>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m3954622129_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int64>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m4025287563_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m4025287563_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m4025287563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t2995867587 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t2499251769 * L_0 = (ImmutableList_1_t2499251769 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t2499251769 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Int64>>::GetEnumerator() */, (ImmutableList_1_t2499251769 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t2995867587 * L_3 = InterfaceFuncInvoker0< Action_1_t2995867587 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Int64>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t2995867587 *)L_3;
			Action_1_t2995867587 * L_4 = V_0;
			int64_t L_5 = (int64_t)__this->get_t_1();
			NullCheck((Action_1_t2995867587 *)L_4);
			((  void (*) (Action_1_t2995867587 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t2995867587 *)L_4, (int64_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Object>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m114301029_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Object>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2063261015_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2063261015_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m2063261015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t985559125 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t488943307 * L_0 = (ImmutableList_1_t488943307 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t488943307 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Object>>::GetEnumerator() */, (ImmutableList_1_t488943307 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t985559125 * L_3 = InterfaceFuncInvoker0< Action_1_t985559125 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t985559125 *)L_3;
			Action_1_t985559125 * L_4 = V_0;
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_t_1();
			NullCheck((Action_1_t985559125 *)L_4);
			((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Single>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m4067230254_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Single>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m69513966_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m69513966_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m69513966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1106661726 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t610045908 * L_0 = (ImmutableList_1_t610045908 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t610045908 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Single>>::GetEnumerator() */, (ImmutableList_1_t610045908 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t1106661726 * L_3 = InterfaceFuncInvoker0< Action_1_t1106661726 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Single>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t1106661726 *)L_3;
			Action_1_t1106661726 * L_4 = V_0;
			float L_5 = (float)__this->get_t_1();
			NullCheck((Action_1_t1106661726 *)L_4);
			((  void (*) (Action_1_t1106661726 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t1106661726 *)L_4, (float)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1609400899_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.CollectionAddEvent`1<System.Object>>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m3772628665_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m3772628665_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m3772628665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t2564974692 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t2068358874 * L_0 = (ImmutableList_1_t2068358874 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t2068358874 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>>::GetEnumerator() */, (ImmutableList_1_t2068358874 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t2564974692 * L_3 = InterfaceFuncInvoker0< Action_1_t2564974692 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t2564974692 *)L_3;
			Action_1_t2564974692 * L_4 = V_0;
			CollectionAddEvent_1_t2416521987  L_5 = (CollectionAddEvent_1_t2416521987 )__this->get_t_1();
			NullCheck((Action_1_t2564974692 *)L_4);
			((  void (*) (Action_1_t2564974692 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t2564974692 *)L_4, (CollectionAddEvent_1_t2416521987 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.Unit>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1621850015_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.Unit>::<>m__1D6()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m982088669_MetadataUsageId;
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m982088669_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m982088669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t2706738743 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ImmutableList_1_t2210122925 * L_0 = (ImmutableList_1_t2210122925 *)__this->get_list_0();
		NullCheck((ImmutableList_1_t2210122925 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<UniRx.Unit>>::GetEnumerator() */, (ImmutableList_1_t2210122925 *)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Action_1_t2706738743 * L_3 = InterfaceFuncInvoker0< Action_1_t2706738743 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<UniRx.Unit>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Action_1_t2706738743 *)L_3;
			Action_1_t2706738743 * L_4 = V_0;
			Unit_t2558286038  L_5 = (Unit_t2558286038 )__this->get_t_1();
			NullCheck((Action_1_t2706738743 *)L_4);
			((  void (*) (Action_1_t2706738743 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t2706738743 *)L_4, (Unit_t2558286038 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_0024:
		{
			Il2CppObject* L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_8 = V_1;
			if (L_8)
			{
				goto IL_0038;
			}
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(52)
		}

IL_0038:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Boolean>::.ctor()
extern "C"  void Subscription__ctor_m1613157801_gshared (Subscription_t2285219160 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Boolean>::Dispose()
extern "C"  void Subscription_Dispose_m3954703270_gshared (Subscription_t2285219160 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t3765777533 * L_0 = (Reactor_1_t3765777533 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t3765777533 * L_1 = (Reactor_1_t3765777533 *)__this->get_parent_0();
		Action_1_t359458046 * L_2 = (Action_1_t359458046 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t3765777533 *)L_1);
		((  void (*) (Reactor_1_t3765777533 *, Action_1_t359458046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t3765777533 *)L_1, (Action_1_t359458046 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t359458046 *)NULL);
		__this->set_parent_0((Reactor_1_t3765777533 *)NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.DateTime>::.ctor()
extern "C"  void Subscription__ctor_m1916254692_gshared (Subscription_t2413247754 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.DateTime>::Dispose()
extern "C"  void Subscription_Dispose_m3173039393_gshared (Subscription_t2413247754 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t3893806128 * L_0 = (Reactor_1_t3893806128 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t3893806128 * L_1 = (Reactor_1_t3893806128 *)__this->get_parent_0();
		Action_1_t487486641 * L_2 = (Action_1_t487486641 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t3893806128 *)L_1);
		((  void (*) (Reactor_1_t3893806128 *, Action_1_t487486641 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t3893806128 *)L_1, (Action_1_t487486641 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t487486641 *)NULL);
		__this->set_parent_0((Reactor_1_t3893806128 *)NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Double>::.ctor()
extern "C"  void Subscription__ctor_m1162570650_gshared (Subscription_t2608730433 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Double>::Dispose()
extern "C"  void Subscription_Dispose_m437180759_gshared (Subscription_t2608730433 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t4089288806 * L_0 = (Reactor_1_t4089288806 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t4089288806 * L_1 = (Reactor_1_t4089288806 *)__this->get_parent_0();
		Action_1_t682969319 * L_2 = (Action_1_t682969319 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t4089288806 *)L_1);
		((  void (*) (Reactor_1_t4089288806 *, Action_1_t682969319 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t4089288806 *)L_1, (Action_1_t682969319 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t682969319 *)NULL);
		__this->set_parent_0((Reactor_1_t4089288806 *)NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Int32>::.ctor()
extern "C"  void Subscription__ctor_m2023534479_gshared (Subscription_t626661310 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Int32>::Dispose()
extern "C"  void Subscription_Dispose_m3189699596_gshared (Subscription_t626661310 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t2107219683 * L_0 = (Reactor_1_t2107219683 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t2107219683 * L_1 = (Reactor_1_t2107219683 *)__this->get_parent_0();
		Action_1_t2995867492 * L_2 = (Action_1_t2995867492 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t2107219683 *)L_1);
		((  void (*) (Reactor_1_t2107219683 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t2107219683 *)L_1, (Action_1_t2995867492 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t2995867492 *)NULL);
		__this->set_parent_0((Reactor_1_t2107219683 *)NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Int64>::.ctor()
extern "C"  void Subscription__ctor_m3111847214_gshared (Subscription_t626661405 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Int64>::Dispose()
extern "C"  void Subscription_Dispose_m1086217707_gshared (Subscription_t626661405 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t2107219778 * L_0 = (Reactor_1_t2107219778 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t2107219778 * L_1 = (Reactor_1_t2107219778 *)__this->get_parent_0();
		Action_1_t2995867587 * L_2 = (Action_1_t2995867587 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t2107219778 *)L_1);
		((  void (*) (Reactor_1_t2107219778 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t2107219778 *)L_1, (Action_1_t2995867587 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t2995867587 *)NULL);
		__this->set_parent_0((Reactor_1_t2107219778 *)NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Object>::.ctor()
extern "C"  void Subscription__ctor_m4053049736_gshared (Subscription_t2911320242 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m3638709189_gshared (Subscription_t2911320242 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t96911316 * L_0 = (Reactor_1_t96911316 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t96911316 * L_1 = (Reactor_1_t96911316 *)__this->get_parent_0();
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t96911316 *)L_1);
		((  void (*) (Reactor_1_t96911316 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t96911316 *)L_1, (Action_1_t985559125 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t985559125 *)NULL);
		__this->set_parent_0((Reactor_1_t96911316 *)NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Single>::.ctor()
extern "C"  void Subscription__ctor_m3711011665_gshared (Subscription_t3032422840 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<System.Single>::Dispose()
extern "C"  void Subscription_Dispose_m1357637454_gshared (Subscription_t3032422840 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t218013917 * L_0 = (Reactor_1_t218013917 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t218013917 * L_1 = (Reactor_1_t218013917 *)__this->get_parent_0();
		Action_1_t1106661726 * L_2 = (Action_1_t1106661726 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t218013917 *)L_1);
		((  void (*) (Reactor_1_t218013917 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t218013917 *)L_1, (Action_1_t1106661726 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t1106661726 *)NULL);
		__this->set_parent_0((Reactor_1_t218013917 *)NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void Subscription__ctor_m2749810918_gshared (Subscription_t195768510 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m1061688227_gshared (Subscription_t195768510 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t1676326883 * L_0 = (Reactor_1_t1676326883 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t1676326883 * L_1 = (Reactor_1_t1676326883 *)__this->get_parent_0();
		Action_1_t2564974692 * L_2 = (Action_1_t2564974692 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t1676326883 *)L_1);
		((  void (*) (Reactor_1_t1676326883 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t1676326883 *)L_1, (Action_1_t2564974692 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t2564974692 *)NULL);
		__this->set_parent_0((Reactor_1_t1676326883 *)NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<UniRx.Unit>::.ctor()
extern "C"  void Subscription__ctor_m47790428_gshared (Subscription_t337532562 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1/Subscription<UniRx.Unit>::Dispose()
extern "C"  void Subscription_Dispose_m2875211417_gshared (Subscription_t337532562 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t1818090934 * L_0 = (Reactor_1_t1818090934 *)__this->get_parent_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Reactor_1_t1818090934 * L_1 = (Reactor_1_t1818090934 *)__this->get_parent_0();
		Action_1_t2706738743 * L_2 = (Action_1_t2706738743 *)__this->get_unsubscribeTarget_1();
		int32_t L_3 = (int32_t)__this->get_priority_2();
		NullCheck((Reactor_1_t1818090934 *)L_1);
		((  void (*) (Reactor_1_t1818090934 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t1818090934 *)L_1, (Action_1_t2706738743 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_unsubscribeTarget_1((Action_1_t2706738743 *)NULL);
		__this->set_parent_0((Reactor_1_t1818090934 *)NULL);
		return;
	}
}
// System.Void Reactor`1<System.Boolean>::.ctor()
extern "C"  void Reactor_1__ctor_m2182279135_gshared (Reactor_1_t3765777533 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<System.Boolean>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m2762838562_MetadataUsageId;
extern "C"  void Reactor_1_React_m2762838562_gshared (Reactor_1_t3765777533 * __this, bool ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m2762838562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t4157809524 * V_0 = NULL;
	ImmutableList_1U5BU5D_t3981884477* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t359458046 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t3765777533 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t3765777533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t3765777533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t359458046 * L_1 = (Action_1_t359458046 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t359458046 * L_2 = (Action_1_t359458046 *)__this->get_single_0();
		bool L_3 = ___t0;
		NullCheck((Action_1_t359458046 *)L_2);
		((  void (*) (Action_1_t359458046 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t359458046 *)L_2, (bool)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t3981884477* L_4 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t3981884477*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t3981884477* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t4157809524 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t4157809524 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t4157809524 * L_9 = V_0;
		NullCheck((ImmutableList_1_t4157809524 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Boolean>>::GetEnumerator() */, (ImmutableList_1_t4157809524 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t359458046 * L_12 = InterfaceFuncInvoker0< Action_1_t359458046 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Boolean>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t359458046 *)L_12;
			Action_1_t359458046 * L_13 = V_3;
			bool L_14 = ___t0;
			NullCheck((Action_1_t359458046 *)L_13);
			((  void (*) (Action_1_t359458046 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t359458046 *)L_13, (bool)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t3981884477* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<System.Boolean>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m219389101_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m219389101_gshared (Reactor_1_t3765777533 * __this, bool ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m219389101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * L_1 = V_1;
		bool L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t3765777533 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t3765777533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t3765777533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t359458046 * L_4 = (Action_1_t359458046 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t3765777533 *)__this);
		((  void (*) (Reactor_1_t3765777533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t3765777533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * L_5 = V_1;
		ImmutableList_1U5BU5D_t3981884477* L_6 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t4157809524 * L_10 = (ImmutableList_1_t4157809524 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t4157809524 * L_12 = (ImmutableList_1_t4157809524 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t4157809524 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t4157809524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t4157809524 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2538948652 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<System.Boolean>::Empty()
extern "C"  bool Reactor_1_Empty_m2243558590_gshared (Reactor_1_t3765777533 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t359458046 * L_0 = (Action_1_t359458046 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t3981884477* L_1 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t3981884477*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<System.Boolean>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m2324894600_gshared (Reactor_1_t3765777533 * __this, Action_1_t359458046 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t2285219160 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t3765777533 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t3765777533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t3765777533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t359458046 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t359458046 * L_3 = (Action_1_t359458046 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t3765777533 *)__this);
		((  void (*) (Reactor_1_t3765777533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t3765777533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t359458046 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t3765777533 *)__this);
		((  void (*) (Reactor_1_t3765777533 *, Action_1_t359458046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t3765777533 *)__this, (Action_1_t359458046 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t2285219160 * L_6 = (Subscription_t2285219160 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t2285219160 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t2285219160 *)L_6;
		Subscription_t2285219160 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t2285219160 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t2285219160 * L_10 = V_0;
		Action_1_t359458046 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t2285219160 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<System.Boolean>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m601382043_gshared (Reactor_1_t3765777533 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t3981884477* L_0 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t3981884477*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t359458046 * L_1 = (Action_1_t359458046 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t3765777533 *)__this);
		((  void (*) (Reactor_1_t3765777533 *, Action_1_t359458046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t3765777533 *)__this, (Action_1_t359458046 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t359458046 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<System.Boolean>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3828726844_gshared (Reactor_1_t3765777533 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t4157809524 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t3981884477* L_0 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t4157809524 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t4157809524 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t359458046 * L_4 = ___reaction0;
		ImmutableList_1_t4157809524 * L_5 = (ImmutableList_1_t4157809524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t4157809524 *, Action_1_t359458046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t359458046 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t4157809524 *)L_5;
		ImmutableList_1U5BU5D_t3981884477* L_6 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t4157809524 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t4157809524 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t3981884477* L_9 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t4157809524 * L_11 = V_0;
		Action_1_t359458046 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t4157809524 *)L_11);
		ImmutableList_1_t4157809524 * L_13 = ((  ImmutableList_1_t4157809524 * (*) (ImmutableList_1_t4157809524 *, Action_1_t359458046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t4157809524 *)L_11, (Action_1_t359458046 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t4157809524 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<System.Boolean>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m1313493808_gshared (Reactor_1_t3765777533 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t359458046 * L_0 = (Action_1_t359458046 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t359458046 * L_1 = (Action_1_t359458046 *)__this->get_single_0();
		Action_1_t359458046 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t359458046 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t3981884477* L_6 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t3981884477* L_7 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t3981884477* L_9 = (ImmutableList_1U5BU5D_t3981884477*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t359458046 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t4157809524 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t4157809524 * L_13 = ((  ImmutableList_1_t4157809524 * (*) (ImmutableList_1_t4157809524 *, Action_1_t359458046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t4157809524 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t359458046 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t4157809524 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<System.Boolean>::Clear()
extern "C"  void Reactor_1_Clear_m3883379722_gshared (Reactor_1_t3765777533 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t359458046 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t3981884477*)NULL);
		return;
	}
}
// System.Void Reactor`1<System.DateTime>::.ctor()
extern "C"  void Reactor_1__ctor_m2379146862_gshared (Reactor_1_t3893806128 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<System.DateTime>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m275803507_MetadataUsageId;
extern "C"  void Reactor_1_React_m275803507_gshared (Reactor_1_t3893806128 * __this, DateTime_t339033936  ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m275803507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t4285838119 * V_0 = NULL;
	ImmutableList_1U5BU5D_t2588536926* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t487486641 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t3893806128 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t3893806128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t3893806128 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t487486641 * L_1 = (Action_1_t487486641 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t487486641 * L_2 = (Action_1_t487486641 *)__this->get_single_0();
		DateTime_t339033936  L_3 = ___t0;
		NullCheck((Action_1_t487486641 *)L_2);
		((  void (*) (Action_1_t487486641 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t487486641 *)L_2, (DateTime_t339033936 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t2588536926* L_4 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t2588536926*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t2588536926* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t4285838119 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t4285838119 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t4285838119 * L_9 = V_0;
		NullCheck((ImmutableList_1_t4285838119 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>::GetEnumerator() */, (ImmutableList_1_t4285838119 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t487486641 * L_12 = InterfaceFuncInvoker0< Action_1_t487486641 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.DateTime>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t487486641 *)L_12;
			Action_1_t487486641 * L_13 = V_3;
			DateTime_t339033936  L_14 = ___t0;
			NullCheck((Action_1_t487486641 *)L_13);
			((  void (*) (Action_1_t487486641 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t487486641 *)L_13, (DateTime_t339033936 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t2588536926* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<System.DateTime>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m3125580862_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m3125580862_gshared (Reactor_1_t3893806128 * __this, DateTime_t339033936  ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m3125580862_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * L_1 = V_1;
		DateTime_t339033936  L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t3893806128 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t3893806128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t3893806128 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t487486641 * L_4 = (Action_1_t487486641 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t3893806128 *)__this);
		((  void (*) (Reactor_1_t3893806128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t3893806128 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * L_5 = V_1;
		ImmutableList_1U5BU5D_t2588536926* L_6 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t4285838119 * L_10 = (ImmutableList_1_t4285838119 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t4285838119 * L_12 = (ImmutableList_1_t4285838119 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t4285838119 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t4285838119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t4285838119 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2666977247 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<System.DateTime>::Empty()
extern "C"  bool Reactor_1_Empty_m1893967461_gshared (Reactor_1_t3893806128 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t487486641 * L_0 = (Action_1_t487486641 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t2588536926* L_1 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t2588536926*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<System.DateTime>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m3297589983_gshared (Reactor_1_t3893806128 * __this, Action_1_t487486641 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t2413247754 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t3893806128 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t3893806128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t3893806128 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t487486641 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t487486641 * L_3 = (Action_1_t487486641 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t3893806128 *)__this);
		((  void (*) (Reactor_1_t3893806128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t3893806128 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t487486641 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t3893806128 *)__this);
		((  void (*) (Reactor_1_t3893806128 *, Action_1_t487486641 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t3893806128 *)__this, (Action_1_t487486641 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t2413247754 * L_6 = (Subscription_t2413247754 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t2413247754 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t2413247754 *)L_6;
		Subscription_t2413247754 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t2413247754 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t2413247754 * L_10 = V_0;
		Action_1_t487486641 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t2413247754 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<System.DateTime>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m2722395628_gshared (Reactor_1_t3893806128 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t2588536926* L_0 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t2588536926*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t487486641 * L_1 = (Action_1_t487486641 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t3893806128 *)__this);
		((  void (*) (Reactor_1_t3893806128 *, Action_1_t487486641 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t3893806128 *)__this, (Action_1_t487486641 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t487486641 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<System.DateTime>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3485185995_gshared (Reactor_1_t3893806128 * __this, Action_1_t487486641 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t4285838119 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t2588536926* L_0 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t4285838119 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t4285838119 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t487486641 * L_4 = ___reaction0;
		ImmutableList_1_t4285838119 * L_5 = (ImmutableList_1_t4285838119 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t4285838119 *, Action_1_t487486641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t487486641 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t4285838119 *)L_5;
		ImmutableList_1U5BU5D_t2588536926* L_6 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t4285838119 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t4285838119 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t2588536926* L_9 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t4285838119 * L_11 = V_0;
		Action_1_t487486641 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t4285838119 *)L_11);
		ImmutableList_1_t4285838119 * L_13 = ((  ImmutableList_1_t4285838119 * (*) (ImmutableList_1_t4285838119 *, Action_1_t487486641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t4285838119 *)L_11, (Action_1_t487486641 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t4285838119 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<System.DateTime>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m2558684735_gshared (Reactor_1_t3893806128 * __this, Action_1_t487486641 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t487486641 * L_0 = (Action_1_t487486641 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t487486641 * L_1 = (Action_1_t487486641 *)__this->get_single_0();
		Action_1_t487486641 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t487486641 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t2588536926* L_6 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t2588536926* L_7 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t2588536926* L_9 = (ImmutableList_1U5BU5D_t2588536926*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t487486641 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t4285838119 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t4285838119 * L_13 = ((  ImmutableList_1_t4285838119 * (*) (ImmutableList_1_t4285838119 *, Action_1_t487486641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t4285838119 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t487486641 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t4285838119 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<System.DateTime>::Clear()
extern "C"  void Reactor_1_Clear_m4080247449_gshared (Reactor_1_t3893806128 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t487486641 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t2588536926*)NULL);
		return;
	}
}
// System.Void Reactor`1<System.Double>::.ctor()
extern "C"  void Reactor_1__ctor_m2566402724_gshared (Reactor_1_t4089288806 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<System.Double>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m1785767933_MetadataUsageId;
extern "C"  void Reactor_1_React_m1785767933_gshared (Reactor_1_t4089288806 * __this, double ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m1785767933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t186353501 * V_0 = NULL;
	ImmutableList_1U5BU5D_t1225238160* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t682969319 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t4089288806 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t4089288806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t4089288806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t682969319 * L_1 = (Action_1_t682969319 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t682969319 * L_2 = (Action_1_t682969319 *)__this->get_single_0();
		double L_3 = ___t0;
		NullCheck((Action_1_t682969319 *)L_2);
		((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t682969319 *)L_2, (double)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t1225238160* L_4 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t1225238160*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t1225238160* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t186353501 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t186353501 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t186353501 * L_9 = V_0;
		NullCheck((ImmutableList_1_t186353501 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Double>>::GetEnumerator() */, (ImmutableList_1_t186353501 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t682969319 * L_12 = InterfaceFuncInvoker0< Action_1_t682969319 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Double>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t682969319 *)L_12;
			Action_1_t682969319 * L_13 = V_3;
			double L_14 = ___t0;
			NullCheck((Action_1_t682969319 *)L_13);
			((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t682969319 *)L_13, (double)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t1225238160* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<System.Double>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m2535932232_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m2535932232_gshared (Reactor_1_t4089288806 * __this, double ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m2535932232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * L_1 = V_1;
		double L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t4089288806 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t4089288806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t4089288806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t682969319 * L_4 = (Action_1_t682969319 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t4089288806 *)__this);
		((  void (*) (Reactor_1_t4089288806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t4089288806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * L_5 = V_1;
		ImmutableList_1U5BU5D_t1225238160* L_6 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t186353501 * L_10 = (ImmutableList_1_t186353501 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t186353501 * L_12 = (ImmutableList_1_t186353501 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t186353501 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t186353501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t186353501 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<System.Double>::Empty()
extern "C"  bool Reactor_1_Empty_m1536931227_gshared (Reactor_1_t4089288806 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t682969319 * L_0 = (Action_1_t682969319 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t1225238160* L_1 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t1225238160*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<System.Double>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m825091625_gshared (Reactor_1_t4089288806 * __this, Action_1_t682969319 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t2608730433 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t4089288806 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t4089288806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t4089288806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t682969319 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t682969319 * L_3 = (Action_1_t682969319 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t4089288806 *)__this);
		((  void (*) (Reactor_1_t4089288806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t4089288806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t682969319 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t4089288806 *)__this);
		((  void (*) (Reactor_1_t4089288806 *, Action_1_t682969319 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t4089288806 *)__this, (Action_1_t682969319 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t2608730433 * L_6 = (Subscription_t2608730433 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t2608730433 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t2608730433 *)L_6;
		Subscription_t2608730433 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t2608730433 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t2608730433 * L_10 = V_0;
		Action_1_t682969319 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t2608730433 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<System.Double>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m4095892598_gshared (Reactor_1_t4089288806 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t1225238160* L_0 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t1225238160*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t682969319 * L_1 = (Action_1_t682969319 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t4089288806 *)__this);
		((  void (*) (Reactor_1_t4089288806 *, Action_1_t682969319 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t4089288806 *)__this, (Action_1_t682969319 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t682969319 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<System.Double>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m4101117441_gshared (Reactor_1_t4089288806 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t186353501 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t1225238160* L_0 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t186353501 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t186353501 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t682969319 * L_4 = ___reaction0;
		ImmutableList_1_t186353501 * L_5 = (ImmutableList_1_t186353501 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t186353501 *, Action_1_t682969319 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t682969319 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t186353501 *)L_5;
		ImmutableList_1U5BU5D_t1225238160* L_6 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t186353501 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t186353501 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t1225238160* L_9 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t186353501 * L_11 = V_0;
		Action_1_t682969319 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t186353501 *)L_11);
		ImmutableList_1_t186353501 * L_13 = ((  ImmutableList_1_t186353501 * (*) (ImmutableList_1_t186353501 *, Action_1_t682969319 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t186353501 *)L_11, (Action_1_t682969319 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t186353501 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<System.Double>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m4217132917_gshared (Reactor_1_t4089288806 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t682969319 * L_0 = (Action_1_t682969319 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t682969319 * L_1 = (Action_1_t682969319 *)__this->get_single_0();
		Action_1_t682969319 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t682969319 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t1225238160* L_6 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t1225238160* L_7 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t1225238160* L_9 = (ImmutableList_1U5BU5D_t1225238160*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t682969319 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t186353501 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t186353501 * L_13 = ((  ImmutableList_1_t186353501 * (*) (ImmutableList_1_t186353501 *, Action_1_t682969319 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t186353501 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t682969319 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t186353501 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<System.Double>::Clear()
extern "C"  void Reactor_1_Clear_m4267503311_gshared (Reactor_1_t4089288806 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t682969319 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t1225238160*)NULL);
		return;
	}
}
// System.Void Reactor`1<System.Int32>::.ctor()
extern "C"  void Reactor_1__ctor_m2484461381_gshared (Reactor_1_t2107219683 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<System.Int32>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m3540553596_MetadataUsageId;
extern "C"  void Reactor_1_React_m3540553596_gshared (Reactor_1_t2107219683 * __this, int32_t ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m3540553596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t2499251674 * V_0 = NULL;
	ImmutableList_1U5BU5D_t1986940287* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t2995867492 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t2107219683 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t2107219683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t2107219683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t2995867492 * L_1 = (Action_1_t2995867492 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t2995867492 * L_2 = (Action_1_t2995867492 *)__this->get_single_0();
		int32_t L_3 = ___t0;
		NullCheck((Action_1_t2995867492 *)L_2);
		((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2995867492 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t1986940287* L_4 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t1986940287*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t1986940287* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t2499251674 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t2499251674 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t2499251674 * L_9 = V_0;
		NullCheck((ImmutableList_1_t2499251674 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Int32>>::GetEnumerator() */, (ImmutableList_1_t2499251674 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t2995867492 * L_12 = InterfaceFuncInvoker0< Action_1_t2995867492 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Int32>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t2995867492 *)L_12;
			Action_1_t2995867492 * L_13 = V_3;
			int32_t L_14 = ___t0;
			NullCheck((Action_1_t2995867492 *)L_13);
			((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2995867492 *)L_13, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t1986940287* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<System.Int32>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m2309767303_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m2309767303_gshared (Reactor_1_t2107219683 * __this, int32_t ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m2309767303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * L_1 = V_1;
		int32_t L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t2107219683 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t2107219683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t2107219683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t2995867492 * L_4 = (Action_1_t2995867492 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t2107219683 *)__this);
		((  void (*) (Reactor_1_t2107219683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t2107219683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * L_5 = V_1;
		ImmutableList_1U5BU5D_t1986940287* L_6 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t2499251674 * L_10 = (ImmutableList_1_t2499251674 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t2499251674 * L_12 = (ImmutableList_1_t2499251674 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t2499251674 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t2499251674 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t2499251674 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390802 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<System.Int32>::Empty()
extern "C"  bool Reactor_1_Empty_m4190751012_gshared (Reactor_1_t2107219683 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t2995867492 * L_0 = (Action_1_t2995867492 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t1986940287* L_1 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t1986940287*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<System.Int32>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m3815700514_gshared (Reactor_1_t2107219683 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t626661310 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t2107219683 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t2107219683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t2107219683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t2995867492 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t2995867492 * L_3 = (Action_1_t2995867492 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t2107219683 *)__this);
		((  void (*) (Reactor_1_t2107219683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t2107219683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t2995867492 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t2107219683 *)__this);
		((  void (*) (Reactor_1_t2107219683 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t2107219683 *)__this, (Action_1_t2995867492 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t626661310 * L_6 = (Subscription_t626661310 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t626661310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t626661310 *)L_6;
		Subscription_t626661310 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t626661310 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t626661310 * L_10 = V_0;
		Action_1_t2995867492 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t626661310 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<System.Int32>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m2589025781_gshared (Reactor_1_t2107219683 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t1986940287* L_0 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t1986940287*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t2995867492 * L_1 = (Action_1_t2995867492 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t2107219683 *)__this);
		((  void (*) (Reactor_1_t2107219683 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t2107219683 *)__this, (Action_1_t2995867492 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t2995867492 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<System.Int32>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3694054818_gshared (Reactor_1_t2107219683 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t2499251674 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t1986940287* L_0 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t2499251674 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t2499251674 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t2995867492 * L_4 = ___reaction0;
		ImmutableList_1_t2499251674 * L_5 = (ImmutableList_1_t2499251674 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t2499251674 *, Action_1_t2995867492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t2995867492 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t2499251674 *)L_5;
		ImmutableList_1U5BU5D_t1986940287* L_6 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t2499251674 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t2499251674 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t1986940287* L_9 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t2499251674 * L_11 = V_0;
		Action_1_t2995867492 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t2499251674 *)L_11);
		ImmutableList_1_t2499251674 * L_13 = ((  ImmutableList_1_t2499251674 * (*) (ImmutableList_1_t2499251674 *, Action_1_t2995867492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t2499251674 *)L_11, (Action_1_t2995867492 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t2499251674 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<System.Int32>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m2325743510_gshared (Reactor_1_t2107219683 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t2995867492 * L_0 = (Action_1_t2995867492 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t2995867492 * L_1 = (Action_1_t2995867492 *)__this->get_single_0();
		Action_1_t2995867492 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t2995867492 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t1986940287* L_6 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t1986940287* L_7 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t1986940287* L_9 = (ImmutableList_1U5BU5D_t1986940287*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t2995867492 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t2499251674 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t2499251674 * L_13 = ((  ImmutableList_1_t2499251674 * (*) (ImmutableList_1_t2499251674 *, Action_1_t2995867492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t2499251674 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t2995867492 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t2499251674 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<System.Int32>::Clear()
extern "C"  void Reactor_1_Clear_m4185561968_gshared (Reactor_1_t2107219683 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t2995867492 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t1986940287*)NULL);
		return;
	}
}
// System.Void Reactor`1<System.Int64>::.ctor()
extern "C"  void Reactor_1__ctor_m3572774116_gshared (Reactor_1_t2107219778 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<System.Int64>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m2918510013_MetadataUsageId;
extern "C"  void Reactor_1_React_m2918510013_gshared (Reactor_1_t2107219778 * __this, int64_t ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m2918510013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t2499251769 * V_0 = NULL;
	ImmutableList_1U5BU5D_t930135236* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t2995867587 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t2107219778 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t2107219778 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t2107219778 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t2995867587 * L_1 = (Action_1_t2995867587 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t2995867587 * L_2 = (Action_1_t2995867587 *)__this->get_single_0();
		int64_t L_3 = ___t0;
		NullCheck((Action_1_t2995867587 *)L_2);
		((  void (*) (Action_1_t2995867587 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2995867587 *)L_2, (int64_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t930135236* L_4 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t930135236*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t930135236* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t2499251769 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t2499251769 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t2499251769 * L_9 = V_0;
		NullCheck((ImmutableList_1_t2499251769 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Int64>>::GetEnumerator() */, (ImmutableList_1_t2499251769 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t2995867587 * L_12 = InterfaceFuncInvoker0< Action_1_t2995867587 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Int64>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t2995867587 *)L_12;
			Action_1_t2995867587 * L_13 = V_3;
			int64_t L_14 = ___t0;
			NullCheck((Action_1_t2995867587 *)L_13);
			((  void (*) (Action_1_t2995867587 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2995867587 *)L_13, (int64_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t930135236* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<System.Int64>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m1211823880_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m1211823880_gshared (Reactor_1_t2107219778 * __this, int64_t ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m1211823880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * L_1 = V_1;
		int64_t L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t2107219778 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t2107219778 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t2107219778 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t2995867587 * L_4 = (Action_1_t2995867587 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t2107219778 *)__this);
		((  void (*) (Reactor_1_t2107219778 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t2107219778 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * L_5 = V_1;
		ImmutableList_1U5BU5D_t930135236* L_6 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t2499251769 * L_10 = (ImmutableList_1_t2499251769 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t2499251769 * L_12 = (ImmutableList_1_t2499251769 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t2499251769 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t2499251769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t2499251769 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<System.Int64>::Empty()
extern "C"  bool Reactor_1_Empty_m984096451_gshared (Reactor_1_t2107219778 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t2995867587 * L_0 = (Action_1_t2995867587 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t930135236* L_1 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t930135236*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<System.Int64>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m959058531_gshared (Reactor_1_t2107219778 * __this, Action_1_t2995867587 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t626661405 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t2107219778 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t2107219778 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t2107219778 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t2995867587 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t2995867587 * L_3 = (Action_1_t2995867587 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t2107219778 *)__this);
		((  void (*) (Reactor_1_t2107219778 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t2107219778 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t2995867587 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t2107219778 *)__this);
		((  void (*) (Reactor_1_t2107219778 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t2107219778 *)__this, (Action_1_t2995867587 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t626661405 * L_6 = (Subscription_t626661405 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t626661405 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t626661405 *)L_6;
		Subscription_t626661405 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t626661405 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t626661405 * L_10 = V_0;
		Action_1_t2995867587 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t626661405 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<System.Int64>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m4158857782_gshared (Reactor_1_t2107219778 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t930135236* L_0 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t930135236*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t2995867587 * L_1 = (Action_1_t2995867587 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t2107219778 *)__this);
		((  void (*) (Reactor_1_t2107219778 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t2107219778 *)__this, (Action_1_t2995867587 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t2995867587 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<System.Int64>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3972978753_gshared (Reactor_1_t2107219778 * __this, Action_1_t2995867587 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t2499251769 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t930135236* L_0 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t2499251769 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t2499251769 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t2995867587 * L_4 = ___reaction0;
		ImmutableList_1_t2499251769 * L_5 = (ImmutableList_1_t2499251769 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t2499251769 *, Action_1_t2995867587 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t2995867587 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t2499251769 *)L_5;
		ImmutableList_1U5BU5D_t930135236* L_6 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t2499251769 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t2499251769 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t930135236* L_9 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t2499251769 * L_11 = V_0;
		Action_1_t2995867587 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t2499251769 *)L_11);
		ImmutableList_1_t2499251769 * L_13 = ((  ImmutableList_1_t2499251769 * (*) (ImmutableList_1_t2499251769 *, Action_1_t2995867587 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t2499251769 *)L_11, (Action_1_t2995867587 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t2499251769 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<System.Int64>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m586430901_gshared (Reactor_1_t2107219778 * __this, Action_1_t2995867587 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t2995867587 * L_0 = (Action_1_t2995867587 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t2995867587 * L_1 = (Action_1_t2995867587 *)__this->get_single_0();
		Action_1_t2995867587 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t2995867587 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t930135236* L_6 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t930135236* L_7 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t930135236* L_9 = (ImmutableList_1U5BU5D_t930135236*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t2995867587 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t2499251769 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t2499251769 * L_13 = ((  ImmutableList_1_t2499251769 * (*) (ImmutableList_1_t2499251769 *, Action_1_t2995867587 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t2499251769 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t2995867587 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t2499251769 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<System.Int64>::Clear()
extern "C"  void Reactor_1_Clear_m978907407_gshared (Reactor_1_t2107219778 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t2995867587 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t930135236*)NULL);
		return;
	}
}
// System.Void Reactor`1<System.Object>::.ctor()
extern "C"  void Reactor_1__ctor_m1161914514_gshared (Reactor_1_t96911316 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<System.Object>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m1196306383_MetadataUsageId;
extern "C"  void Reactor_1_React_m1196306383_gshared (Reactor_1_t96911316 * __this, Il2CppObject * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m1196306383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t488943307 * V_0 = NULL;
	ImmutableList_1U5BU5D_t188480938* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t985559125 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t96911316 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t96911316 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t96911316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)__this->get_single_0();
		Il2CppObject * L_3 = ___t0;
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t188480938* L_4 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t188480938*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t188480938* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t488943307 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t488943307 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t488943307 * L_9 = V_0;
		NullCheck((ImmutableList_1_t488943307 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Object>>::GetEnumerator() */, (ImmutableList_1_t488943307 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t985559125 * L_12 = InterfaceFuncInvoker0< Action_1_t985559125 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t985559125 *)L_12;
			Action_1_t985559125 * L_13 = V_3;
			Il2CppObject * L_14 = ___t0;
			NullCheck((Action_1_t985559125 *)L_13);
			((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t985559125 *)L_13, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t188480938* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<System.Object>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m1003670938_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m1003670938_gshared (Reactor_1_t96911316 * __this, Il2CppObject * ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m1003670938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * L_1 = V_1;
		Il2CppObject * L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t96911316 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t96911316 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t96911316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t985559125 * L_4 = (Action_1_t985559125 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t96911316 *)__this);
		((  void (*) (Reactor_1_t96911316 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t96911316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * L_5 = V_1;
		ImmutableList_1U5BU5D_t188480938* L_6 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t488943307 * L_10 = (ImmutableList_1_t488943307 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t488943307 * L_12 = (ImmutableList_1_t488943307 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t488943307 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t488943307 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t488943307 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3165049731 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<System.Object>::Empty()
extern "C"  bool Reactor_1_Empty_m132443017_gshared (Reactor_1_t96911316 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t188480938* L_1 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t188480938*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<System.Object>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m1495770619_gshared (Reactor_1_t96911316 * __this, Action_1_t985559125 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t2911320242 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t96911316 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t96911316 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t96911316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t985559125 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t985559125 * L_3 = (Action_1_t985559125 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t96911316 *)__this);
		((  void (*) (Reactor_1_t96911316 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t96911316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t985559125 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t96911316 *)__this);
		((  void (*) (Reactor_1_t96911316 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t96911316 *)__this, (Action_1_t985559125 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t2911320242 * L_6 = (Subscription_t2911320242 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t2911320242 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t2911320242 *)L_6;
		Subscription_t2911320242 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t2911320242 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t2911320242 * L_10 = V_0;
		Action_1_t985559125 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t2911320242 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<System.Object>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m912916552_gshared (Reactor_1_t96911316 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t188480938* L_0 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t188480938*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t96911316 *)__this);
		((  void (*) (Reactor_1_t96911316 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t96911316 *)__this, (Action_1_t985559125 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t985559125 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<System.Object>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3381282287_gshared (Reactor_1_t96911316 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t488943307 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t188480938* L_0 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t488943307 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t488943307 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t985559125 * L_4 = ___reaction0;
		ImmutableList_1_t488943307 * L_5 = (ImmutableList_1_t488943307 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t488943307 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t985559125 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t488943307 *)L_5;
		ImmutableList_1U5BU5D_t188480938* L_6 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t488943307 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t488943307 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t188480938* L_9 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t488943307 * L_11 = V_0;
		Action_1_t985559125 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t488943307 *)L_11);
		ImmutableList_1_t488943307 * L_13 = ((  ImmutableList_1_t488943307 * (*) (ImmutableList_1_t488943307 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t488943307 *)L_11, (Action_1_t985559125 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t488943307 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<System.Object>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m4227182179_gshared (Reactor_1_t96911316 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)__this->get_single_0();
		Action_1_t985559125 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t985559125 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t188480938* L_6 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t188480938* L_7 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t188480938* L_9 = (ImmutableList_1U5BU5D_t188480938*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t985559125 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t488943307 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t488943307 * L_13 = ((  ImmutableList_1_t488943307 * (*) (ImmutableList_1_t488943307 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t488943307 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t985559125 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t488943307 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<System.Object>::Clear()
extern "C"  void Reactor_1_Clear_m2863015101_gshared (Reactor_1_t96911316 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t985559125 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t188480938*)NULL);
		return;
	}
}
// System.Void Reactor`1<System.Single>::.ctor()
extern "C"  void Reactor_1__ctor_m819876443_gshared (Reactor_1_t218013917 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<System.Single>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m3478028070_MetadataUsageId;
extern "C"  void Reactor_1_React_m3478028070_gshared (Reactor_1_t218013917 * __this, float ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m3478028070_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t610045908 * V_0 = NULL;
	ImmutableList_1U5BU5D_t1396388445* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t1106661726 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t218013917 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t218013917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t218013917 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t1106661726 * L_1 = (Action_1_t1106661726 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t1106661726 * L_2 = (Action_1_t1106661726 *)__this->get_single_0();
		float L_3 = ___t0;
		NullCheck((Action_1_t1106661726 *)L_2);
		((  void (*) (Action_1_t1106661726 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t1106661726 *)L_2, (float)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t1396388445* L_4 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t1396388445*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t1396388445* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t610045908 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t610045908 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t610045908 * L_9 = V_0;
		NullCheck((ImmutableList_1_t610045908 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Single>>::GetEnumerator() */, (ImmutableList_1_t610045908 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t1106661726 * L_12 = InterfaceFuncInvoker0< Action_1_t1106661726 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<System.Single>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t1106661726 *)L_12;
			Action_1_t1106661726 * L_13 = V_3;
			float L_14 = ___t0;
			NullCheck((Action_1_t1106661726 *)L_13);
			((  void (*) (Action_1_t1106661726 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t1106661726 *)L_13, (float)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t1396388445* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<System.Single>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m3032148657_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m3032148657_gshared (Reactor_1_t218013917 * __this, float ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m3032148657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * L_1 = V_1;
		float L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t218013917 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t218013917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t218013917 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t1106661726 * L_4 = (Action_1_t1106661726 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t218013917 *)__this);
		((  void (*) (Reactor_1_t218013917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t218013917 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * L_5 = V_1;
		ImmutableList_1U5BU5D_t1396388445* L_6 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t610045908 * L_10 = (ImmutableList_1_t610045908 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t610045908 * L_12 = (ImmutableList_1_t610045908 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t610045908 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t610045908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t610045908 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t3286152332 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<System.Single>::Empty()
extern "C"  bool Reactor_1_Empty_m4085372242_gshared (Reactor_1_t218013917 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t1106661726 * L_0 = (Action_1_t1106661726 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t1396388445* L_1 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t1396388445*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<System.Single>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m947706194_gshared (Reactor_1_t218013917 * __this, Action_1_t1106661726 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t3032422840 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t218013917 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t218013917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t218013917 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t1106661726 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t1106661726 * L_3 = (Action_1_t1106661726 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t218013917 *)__this);
		((  void (*) (Reactor_1_t218013917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t218013917 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t1106661726 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t218013917 *)__this);
		((  void (*) (Reactor_1_t218013917 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t218013917 *)__this, (Action_1_t1106661726 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t3032422840 * L_6 = (Subscription_t3032422840 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t3032422840 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t3032422840 *)L_6;
		Subscription_t3032422840 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t3032422840 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t3032422840 * L_10 = V_0;
		Action_1_t1106661726 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t3032422840 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<System.Single>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m2477179295_gshared (Reactor_1_t218013917 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t1396388445* L_0 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t1396388445*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t1106661726 * L_1 = (Action_1_t1106661726 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t218013917 *)__this);
		((  void (*) (Reactor_1_t218013917 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t218013917 *)__this, (Action_1_t1106661726 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t1106661726 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<System.Single>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m436990648_gshared (Reactor_1_t218013917 * __this, Action_1_t1106661726 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t610045908 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t1396388445* L_0 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t610045908 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t610045908 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t1106661726 * L_4 = ___reaction0;
		ImmutableList_1_t610045908 * L_5 = (ImmutableList_1_t610045908 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t610045908 *, Action_1_t1106661726 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t1106661726 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t610045908 *)L_5;
		ImmutableList_1U5BU5D_t1396388445* L_6 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t610045908 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t610045908 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t1396388445* L_9 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t610045908 * L_11 = V_0;
		Action_1_t1106661726 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t610045908 *)L_11);
		ImmutableList_1_t610045908 * L_13 = ((  ImmutableList_1_t610045908 * (*) (ImmutableList_1_t610045908 *, Action_1_t1106661726 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t610045908 *)L_11, (Action_1_t1106661726 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t610045908 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<System.Single>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m2010589100_gshared (Reactor_1_t218013917 * __this, Action_1_t1106661726 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t1106661726 * L_0 = (Action_1_t1106661726 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t1106661726 * L_1 = (Action_1_t1106661726 *)__this->get_single_0();
		Action_1_t1106661726 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t1106661726 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t1396388445* L_6 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t1396388445* L_7 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t1396388445* L_9 = (ImmutableList_1U5BU5D_t1396388445*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t1106661726 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t610045908 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t610045908 * L_13 = ((  ImmutableList_1_t610045908 * (*) (ImmutableList_1_t610045908 *, Action_1_t1106661726 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t610045908 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t1106661726 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t610045908 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<System.Single>::Clear()
extern "C"  void Reactor_1_Clear_m2520977030_gshared (Reactor_1_t218013917 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t1106661726 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t1396388445*)NULL);
		return;
	}
}
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void Reactor_1__ctor_m3042049264_gshared (Reactor_1_t1676326883 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m3645908785_MetadataUsageId;
extern "C"  void Reactor_1_React_m3645908785_gshared (Reactor_1_t1676326883 * __this, CollectionAddEvent_1_t2416521987  ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m3645908785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t2068358874 * V_0 = NULL;
	ImmutableList_1U5BU5D_t966832255* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t2564974692 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t1676326883 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t1676326883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t1676326883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t2564974692 * L_1 = (Action_1_t2564974692 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t2564974692 * L_2 = (Action_1_t2564974692 *)__this->get_single_0();
		CollectionAddEvent_1_t2416521987  L_3 = ___t0;
		NullCheck((Action_1_t2564974692 *)L_2);
		((  void (*) (Action_1_t2564974692 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2564974692 *)L_2, (CollectionAddEvent_1_t2416521987 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t966832255* L_4 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t966832255*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t966832255* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t2068358874 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t2068358874 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t2068358874 * L_9 = V_0;
		NullCheck((ImmutableList_1_t2068358874 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>>::GetEnumerator() */, (ImmutableList_1_t2068358874 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t2564974692 * L_12 = InterfaceFuncInvoker0< Action_1_t2564974692 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t2564974692 *)L_12;
			Action_1_t2564974692 * L_13 = V_3;
			CollectionAddEvent_1_t2416521987  L_14 = ___t0;
			NullCheck((Action_1_t2564974692 *)L_13);
			((  void (*) (Action_1_t2564974692 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2564974692 *)L_13, (CollectionAddEvent_1_t2416521987 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t966832255* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m2959551868_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m2959551868_gshared (Reactor_1_t1676326883 * __this, CollectionAddEvent_1_t2416521987  ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m2959551868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * L_1 = V_1;
		CollectionAddEvent_1_t2416521987  L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t1676326883 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t1676326883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t1676326883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t2564974692 * L_4 = (Action_1_t2564974692 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t1676326883 *)__this);
		((  void (*) (Reactor_1_t1676326883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t1676326883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * L_5 = V_1;
		ImmutableList_1U5BU5D_t966832255* L_6 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t2068358874 * L_10 = (ImmutableList_1_t2068358874 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t2068358874 * L_12 = (ImmutableList_1_t2068358874 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t2068358874 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t2068358874 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t2068358874 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::Empty()
extern "C"  bool Reactor_1_Empty_m1349981671_gshared (Reactor_1_t1676326883 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t2564974692 * L_0 = (Action_1_t2564974692 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t966832255* L_1 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t966832255*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m243157725_gshared (Reactor_1_t1676326883 * __this, Action_1_t2564974692 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t195768510 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t1676326883 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t1676326883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t1676326883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t2564974692 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t2564974692 * L_3 = (Action_1_t2564974692 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t1676326883 *)__this);
		((  void (*) (Reactor_1_t1676326883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t1676326883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t2564974692 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t1676326883 *)__this);
		((  void (*) (Reactor_1_t1676326883 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t1676326883 *)__this, (Action_1_t2564974692 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t195768510 * L_6 = (Subscription_t195768510 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t195768510 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t195768510 *)L_6;
		Subscription_t195768510 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t195768510 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t195768510 * L_10 = V_0;
		Action_1_t2564974692 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t195768510 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m1822749610_gshared (Reactor_1_t1676326883 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t966832255* L_0 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t966832255*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t2564974692 * L_1 = (Action_1_t2564974692 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t1676326883 *)__this);
		((  void (*) (Reactor_1_t1676326883 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t1676326883 *)__this, (Action_1_t2564974692 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t2564974692 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m625386573_gshared (Reactor_1_t1676326883 * __this, Action_1_t2564974692 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t2068358874 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t966832255* L_0 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t2068358874 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t2068358874 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t2564974692 * L_4 = ___reaction0;
		ImmutableList_1_t2068358874 * L_5 = (ImmutableList_1_t2068358874 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t2068358874 *, Action_1_t2564974692 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t2564974692 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t2068358874 *)L_5;
		ImmutableList_1U5BU5D_t966832255* L_6 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t2068358874 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t2068358874 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t966832255* L_9 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t2068358874 * L_11 = V_0;
		Action_1_t2564974692 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t2068358874 *)L_11);
		ImmutableList_1_t2068358874 * L_13 = ((  ImmutableList_1_t2068358874 * (*) (ImmutableList_1_t2068358874 *, Action_1_t2564974692 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t2068358874 *)L_11, (Action_1_t2564974692 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t2068358874 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m3790334913_gshared (Reactor_1_t1676326883 * __this, Action_1_t2564974692 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t2564974692 * L_0 = (Action_1_t2564974692 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t2564974692 * L_1 = (Action_1_t2564974692 *)__this->get_single_0();
		Action_1_t2564974692 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t2564974692 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t966832255* L_6 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t966832255* L_7 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t966832255* L_9 = (ImmutableList_1U5BU5D_t966832255*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t2564974692 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t2068358874 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t2068358874 * L_13 = ((  ImmutableList_1_t2068358874 * (*) (ImmutableList_1_t2068358874 *, Action_1_t2564974692 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t2068358874 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t2564974692 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t2068358874 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<System.Object>>::Clear()
extern "C"  void Reactor_1_Clear_m448182555_gshared (Reactor_1_t1676326883 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t2564974692 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t966832255*)NULL);
		return;
	}
}
// System.Void Reactor`1<UniRx.Unit>::.ctor()
extern "C"  void Reactor_1__ctor_m4213628562_gshared (Reactor_1_t1818090934 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reactor`1<UniRx.Unit>::React(T)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_React_m1310161359_MetadataUsageId;
extern "C"  void Reactor_1_React_m1310161359_gshared (Reactor_1_t1818090934 * __this, Unit_t2558286038  ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_React_m1310161359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImmutableList_1_t2210122925 * V_0 = NULL;
	ImmutableList_1U5BU5D_t1444909568* V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t2706738743 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Reactor_1_t1818090934 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t1818090934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t1818090934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_1_t2706738743 * L_1 = (Action_1_t2706738743 *)__this->get_single_0();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_1_t2706738743 * L_2 = (Action_1_t2706738743 *)__this->get_single_0();
		Unit_t2558286038  L_3 = ___t0;
		NullCheck((Action_1_t2706738743 *)L_2);
		((  void (*) (Action_1_t2706738743 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2706738743 *)L_2, (Unit_t2558286038 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		goto IL_008c;
	}

IL_0028:
	{
		ImmutableList_1U5BU5D_t1444909568* L_4 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		V_1 = (ImmutableList_1U5BU5D_t1444909568*)L_4;
		V_2 = (int32_t)0;
		goto IL_0083;
	}

IL_0036:
	{
		ImmutableList_1U5BU5D_t1444909568* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (ImmutableList_1_t2210122925 *)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ImmutableList_1_t2210122925 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_007f;
	}

IL_0045:
	{
		ImmutableList_1_t2210122925 * L_9 = V_0;
		NullCheck((ImmutableList_1_t2210122925 *)L_9);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<UniRx.Unit>>::GetEnumerator() */, (ImmutableList_1_t2210122925 *)L_9);
		V_4 = (Il2CppObject*)L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Il2CppObject* L_11 = V_4;
			NullCheck((Il2CppObject*)L_11);
			Action_1_t2706738743 * L_12 = InterfaceFuncInvoker0< Action_1_t2706738743 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Action`1<UniRx.Unit>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
			V_3 = (Action_1_t2706738743 *)L_12;
			Action_1_t2706738743 * L_13 = V_3;
			Unit_t2558286038  L_14 = ___t0;
			NullCheck((Action_1_t2706738743 *)L_13);
			((  void (*) (Action_1_t2706738743 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2706738743 *)L_13, (Unit_t2558286038 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_4;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_4;
			if (L_17)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_0077:
		{
			Il2CppObject* L_18 = V_4;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007f:
	{
		int32_t L_19 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_20 = V_2;
		ImmutableList_1U5BU5D_t1444909568* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_008c:
	{
		return;
	}
}
// System.Void Reactor`1<UniRx.Unit>::TransactionUnpackReact(T,System.Int32)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Reactor_1_TransactionUnpackReact_m1186895770_MetadataUsageId;
extern "C"  void Reactor_1_TransactionUnpackReact_m1186895770_gshared (Reactor_1_t1818090934 * __this, Unit_t2558286038  ___t0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Reactor_1_TransactionUnpackReact_m1186895770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t437523947 * V_0 = NULL;
	U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * V_1 = NULL;
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * L_0 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 *)L_0;
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * L_1 = V_1;
		Unit_t2558286038  L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		NullCheck((Reactor_1_t1818090934 *)__this);
		bool L_3 = ((  bool (*) (Reactor_1_t1818090934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t1818090934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		Action_1_t2706738743 * L_4 = (Action_1_t2706738743 *)__this->get_single_0();
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Reactor_1_t1818090934 *)__this);
		((  void (*) (Reactor_1_t1818090934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t1818090934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002a:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * L_5 = V_1;
		ImmutableList_1U5BU5D_t1444909568* L_6 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___i1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_5);
		L_5->set_list_0(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * L_9 = V_1;
		NullCheck(L_9);
		ImmutableList_1_t2210122925 * L_10 = (ImmutableList_1_t2210122925 *)L_9->get_list_0();
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * L_11 = V_1;
		NullCheck(L_11);
		ImmutableList_1_t2210122925 * L_12 = (ImmutableList_1_t2210122925 *)L_11->get_list_0();
		NullCheck((ImmutableList_1_t2210122925 *)L_12);
		int32_t L_13 = ((  int32_t (*) (ImmutableList_1_t2210122925 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ImmutableList_1_t2210122925 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (L_13)
		{
			goto IL_0054;
		}
	}

IL_0053:
	{
		return;
	}

IL_0054:
	{
		U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_t437523947 * L_16 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_16, (Il2CppObject *)L_14, (IntPtr_t)L_15, /*hidden argument*/NULL);
		V_0 = (Action_t437523947 *)L_16;
		int32_t L_17 = ___i1;
		Action_t437523947 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddDataAction_m1712112276(NULL /*static, unused*/, (int32_t)L_17, (Action_t437523947 *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Reactor`1<UniRx.Unit>::Empty()
extern "C"  bool Reactor_1_Empty_m2860180849_gshared (Reactor_1_t1818090934 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Action_1_t2706738743 * L_0 = (Action_1_t2706738743 *)__this->get_single_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ImmutableList_1U5BU5D_t1444909568* L_1 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		G_B3_0 = ((((Il2CppObject*)(ImmutableList_1U5BU5D_t1444909568*)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.IDisposable Reactor`1<UniRx.Unit>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m4170151349_gshared (Reactor_1_t1818090934 * __this, Action_1_t2706738743 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	Subscription_t337532562 * V_0 = NULL;
	{
		NullCheck((Reactor_1_t1818090934 *)__this);
		bool L_0 = ((  bool (*) (Reactor_1_t1818090934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Reactor_1_t1818090934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Action_1_t2706738743 * L_1 = ___reaction0;
		__this->set_single_0(L_1);
		int32_t L_2 = ___priority1;
		__this->set_singlePriority_1(L_2);
		goto IL_0037;
	}

IL_001e:
	{
		Action_1_t2706738743 * L_3 = (Action_1_t2706738743 *)__this->get_single_0();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		NullCheck((Reactor_1_t1818090934 *)__this);
		((  void (*) (Reactor_1_t1818090934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t1818090934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002f:
	{
		Action_1_t2706738743 * L_4 = ___reaction0;
		int32_t L_5 = ___priority1;
		NullCheck((Reactor_1_t1818090934 *)__this);
		((  void (*) (Reactor_1_t1818090934 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t1818090934 *)__this, (Action_1_t2706738743 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0037:
	{
		Subscription_t337532562 * L_6 = (Subscription_t337532562 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Subscription_t337532562 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (Subscription_t337532562 *)L_6;
		Subscription_t337532562 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_parent_0(__this);
		Subscription_t337532562 * L_8 = V_0;
		int32_t L_9 = ___priority1;
		NullCheck(L_8);
		L_8->set_priority_2(L_9);
		Subscription_t337532562 * L_10 = V_0;
		Action_1_t2706738743 * L_11 = ___reaction0;
		NullCheck(L_10);
		L_10->set_unsubscribeTarget_1(L_11);
		Subscription_t337532562 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Reactor`1<UniRx.Unit>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m2774485576_gshared (Reactor_1_t1818090934 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1U5BU5D_t1444909568* L_0 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		__this->set_multipriorityReactions_2(((ImmutableList_1U5BU5D_t1444909568*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (uint32_t)3)));
		Action_1_t2706738743 * L_1 = (Action_1_t2706738743 *)__this->get_single_0();
		int32_t L_2 = (int32_t)__this->get_singlePriority_1();
		NullCheck((Reactor_1_t1818090934 *)__this);
		((  void (*) (Reactor_1_t1818090934 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t1818090934 *)__this, (Action_1_t2706738743 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_single_0((Action_1_t2706738743 *)NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Reactor`1<UniRx.Unit>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3393305583_gshared (Reactor_1_t1818090934 * __this, Action_1_t2706738743 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	ImmutableList_1_t2210122925 * V_0 = NULL;
	{
		ImmutableList_1U5BU5D_t1444909568* L_0 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		int32_t L_1 = ___p1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (ImmutableList_1_t2210122925 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		ImmutableList_1_t2210122925 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t2706738743 * L_4 = ___reaction0;
		ImmutableList_1_t2210122925 * L_5 = (ImmutableList_1_t2210122925 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (ImmutableList_1_t2210122925 *, Action_1_t2706738743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_5, (Action_1_t2706738743 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (ImmutableList_1_t2210122925 *)L_5;
		ImmutableList_1U5BU5D_t1444909568* L_6 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		int32_t L_7 = ___p1;
		ImmutableList_1_t2210122925 * L_8 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (ImmutableList_1_t2210122925 *)L_8);
		goto IL_0033;
	}

IL_0024:
	{
		ImmutableList_1U5BU5D_t1444909568* L_9 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		ImmutableList_1_t2210122925 * L_11 = V_0;
		Action_1_t2706738743 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t2210122925 *)L_11);
		ImmutableList_1_t2210122925 * L_13 = ((  ImmutableList_1_t2210122925 * (*) (ImmutableList_1_t2210122925 *, Action_1_t2706738743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ImmutableList_1_t2210122925 *)L_11, (Action_1_t2706738743 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (ImmutableList_1_t2210122925 *)L_13);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Reactor`1<UniRx.Unit>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m3201836131_gshared (Reactor_1_t1818090934 * __this, Action_1_t2706738743 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t2706738743 * L_0 = (Action_1_t2706738743 *)__this->get_single_0();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t2706738743 * L_1 = (Action_1_t2706738743 *)__this->get_single_0();
		Action_1_t2706738743 * L_2 = ___reaction0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = ___p1;
		int32_t L_5 = (int32_t)__this->get_singlePriority_1();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		__this->set_single_0((Action_1_t2706738743 *)NULL);
		goto IL_0056;
	}

IL_0035:
	{
		ImmutableList_1U5BU5D_t1444909568* L_6 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		ImmutableList_1U5BU5D_t1444909568* L_7 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		int32_t L_8 = ___p1;
		ImmutableList_1U5BU5D_t1444909568* L_9 = (ImmutableList_1U5BU5D_t1444909568*)__this->get_multipriorityReactions_2();
		int32_t L_10 = ___p1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Action_1_t2706738743 * L_12 = ___reaction0;
		NullCheck((ImmutableList_1_t2210122925 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		ImmutableList_1_t2210122925 * L_13 = ((  ImmutableList_1_t2210122925 * (*) (ImmutableList_1_t2210122925 *, Action_1_t2706738743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((ImmutableList_1_t2210122925 *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Action_1_t2706738743 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_13);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (ImmutableList_1_t2210122925 *)L_13);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Reactor`1<UniRx.Unit>::Clear()
extern "C"  void Reactor_1_Clear_m1619761853_gshared (Reactor_1_t1818090934 * __this, const MethodInfo* method)
{
	{
		__this->set_single_0((Action_1_t2706738743 *)NULL);
		__this->set_multipriorityReactions_2((ImmutableList_1U5BU5D_t1444909568*)NULL);
		return;
	}
}
// System.Void Root`2<System.Object,System.Object>::.ctor()
extern "C"  void Root_2__ctor_m3188506133_gshared (Root_2_t2363348211 * __this, const MethodInfo* method)
{
	{
		NullCheck((Organism_2_t948289219 *)__this);
		((  void (*) (Organism_2_t948289219 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Organism_2_t948289219 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void SerializableDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void SerializableDictionary_2__ctor_m3570888488_gshared (SerializableDictionary_2_t2269426764 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t3824425150 *)__this);
		((  void (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t3824425150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void SerializableDictionary`2<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SerializableDictionary_2__ctor_m2587960425_gshared (SerializableDictionary_2_t2269426764 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		StreamingContext_t986364934  L_1 = ___context1;
		NullCheck((Dictionary_2_t3824425150 *)__this);
		((  void (*) (Dictionary_2_t3824425150 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Dictionary_2_t3824425150 *)__this, (SerializationInfo_t2995724695 *)L_0, (StreamingContext_t986364934 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Xml.Schema.XmlSchema SerializableDictionary`2<System.Object,System.Object>::GetSchema()
extern "C"  XmlSchema_t1932230565 * SerializableDictionary_2_GetSchema_m992727535_gshared (SerializableDictionary_2_t2269426764 * __this, const MethodInfo* method)
{
	{
		return (XmlSchema_t1932230565 *)NULL;
	}
}
// System.Void SerializableDictionary`2<System.Object,System.Object>::ReadXml(System.Xml.XmlReader)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSerializer_t1888860807_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t SerializableDictionary_2_ReadXml_m3137948131_MetadataUsageId;
extern "C"  void SerializableDictionary_2_ReadXml_m3137948131_gshared (SerializableDictionary_2_t2269426764 * __this, XmlReader_t4229084514 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SerializableDictionary_2_ReadXml_m3137948131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSerializer_t1888860807 * V_0 = NULL;
	XmlSerializer_t1888860807 * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		XmlSerializer_t1888860807 * L_1 = (XmlSerializer_t1888860807 *)il2cpp_codegen_object_new(XmlSerializer_t1888860807_il2cpp_TypeInfo_var);
		XmlSerializer__ctor_m1894928041(L_1, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (XmlSerializer_t1888860807 *)L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		XmlSerializer_t1888860807 * L_3 = (XmlSerializer_t1888860807 *)il2cpp_codegen_object_new(XmlSerializer_t1888860807_il2cpp_TypeInfo_var);
		XmlSerializer__ctor_m1894928041(L_3, (Type_t *)L_2, /*hidden argument*/NULL);
		V_1 = (XmlSerializer_t1888860807 *)L_3;
		XmlReader_t4229084514 * L_4 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, (XmlReader_t4229084514 *)L_4);
		V_2 = (bool)L_5;
		XmlReader_t4229084514 * L_6 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_6);
		VirtFuncInvoker0< bool >::Invoke(38 /* System.Boolean System.Xml.XmlReader::Read() */, (XmlReader_t4229084514 *)L_6);
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		return;
	}

IL_0035:
	{
		goto IL_0098;
	}

IL_003a:
	{
		XmlReader_t4229084514 * L_8 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_8);
		VirtActionInvoker1< String_t* >::Invoke(44 /* System.Void System.Xml.XmlReader::ReadStartElement(System.String) */, (XmlReader_t4229084514 *)L_8, (String_t*)_stringLiteral3242771);
		XmlReader_t4229084514 * L_9 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_9);
		VirtActionInvoker1< String_t* >::Invoke(44 /* System.Void System.Xml.XmlReader::ReadStartElement(System.String) */, (XmlReader_t4229084514 *)L_9, (String_t*)_stringLiteral106079);
		XmlSerializer_t1888860807 * L_10 = V_0;
		XmlReader_t4229084514 * L_11 = ___reader0;
		NullCheck((XmlSerializer_t1888860807 *)L_10);
		Il2CppObject * L_12 = XmlSerializer_Deserialize_m1814899668((XmlSerializer_t1888860807 *)L_10, (XmlReader_t4229084514 *)L_11, /*hidden argument*/NULL);
		V_3 = (Il2CppObject *)((Il2CppObject *)Castclass(L_12, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)));
		XmlReader_t4229084514 * L_13 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_13);
		VirtActionInvoker0::Invoke(41 /* System.Void System.Xml.XmlReader::ReadEndElement() */, (XmlReader_t4229084514 *)L_13);
		XmlReader_t4229084514 * L_14 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_14);
		VirtActionInvoker1< String_t* >::Invoke(44 /* System.Void System.Xml.XmlReader::ReadStartElement(System.String) */, (XmlReader_t4229084514 *)L_14, (String_t*)_stringLiteral111972721);
		XmlSerializer_t1888860807 * L_15 = V_1;
		XmlReader_t4229084514 * L_16 = ___reader0;
		NullCheck((XmlSerializer_t1888860807 *)L_15);
		Il2CppObject * L_17 = XmlSerializer_Deserialize_m1814899668((XmlSerializer_t1888860807 *)L_15, (XmlReader_t4229084514 *)L_16, /*hidden argument*/NULL);
		V_4 = (Il2CppObject *)((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)));
		XmlReader_t4229084514 * L_18 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_18);
		VirtActionInvoker0::Invoke(41 /* System.Void System.Xml.XmlReader::ReadEndElement() */, (XmlReader_t4229084514 *)L_18);
		Il2CppObject * L_19 = V_3;
		Il2CppObject * L_20 = V_4;
		NullCheck((Dictionary_2_t3824425150 *)__this);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1) */, (Dictionary_2_t3824425150 *)__this, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		XmlReader_t4229084514 * L_21 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_21);
		VirtActionInvoker0::Invoke(41 /* System.Void System.Xml.XmlReader::ReadEndElement() */, (XmlReader_t4229084514 *)L_21);
		XmlReader_t4229084514 * L_22 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_22);
		VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, (XmlReader_t4229084514 *)L_22);
	}

IL_0098:
	{
		XmlReader_t4229084514 * L_23 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_23);
		int32_t L_24 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, (XmlReader_t4229084514 *)L_23);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_003a;
		}
	}
	{
		XmlReader_t4229084514 * L_25 = ___reader0;
		NullCheck((XmlReader_t4229084514 *)L_25);
		VirtActionInvoker0::Invoke(41 /* System.Void System.Xml.XmlReader::ReadEndElement() */, (XmlReader_t4229084514 *)L_25);
		return;
	}
}
// System.Void SerializableDictionary`2<System.Object,System.Object>::WriteXml(System.Xml.XmlWriter)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSerializer_t1888860807_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t SerializableDictionary_2_WriteXml_m3811106720_MetadataUsageId;
extern "C"  void SerializableDictionary_2_WriteXml_m3811106720_gshared (SerializableDictionary_2_t2269426764 * __this, XmlWriter_t89522450 * ___writer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SerializableDictionary_2_WriteXml_m3811106720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSerializer_t1888860807 * V_0 = NULL;
	XmlSerializer_t1888860807 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Enumerator_t3591453091  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		XmlSerializer_t1888860807 * L_1 = (XmlSerializer_t1888860807 *)il2cpp_codegen_object_new(XmlSerializer_t1888860807_il2cpp_TypeInfo_var);
		XmlSerializer__ctor_m1894928041(L_1, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (XmlSerializer_t1888860807 *)L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		XmlSerializer_t1888860807 * L_3 = (XmlSerializer_t1888860807 *)il2cpp_codegen_object_new(XmlSerializer_t1888860807_il2cpp_TypeInfo_var);
		XmlSerializer__ctor_m1894928041(L_3, (Type_t *)L_2, /*hidden argument*/NULL);
		V_1 = (XmlSerializer_t1888860807 *)L_3;
		NullCheck((Dictionary_2_t3824425150 *)__this);
		KeyCollection_t1852733134 * L_4 = ((  KeyCollection_t1852733134 * (*) (Dictionary_2_t3824425150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Dictionary_2_t3824425150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((KeyCollection_t1852733134 *)L_4);
		Enumerator_t3591453091  L_5 = ((  Enumerator_t3591453091  (*) (KeyCollection_t1852733134 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((KeyCollection_t1852733134 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_3 = (Enumerator_t3591453091 )L_5;
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0090;
		}

IL_0031:
		{
			Il2CppObject * L_6 = ((  Il2CppObject * (*) (Enumerator_t3591453091 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Enumerator_t3591453091 *)(&V_3), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			V_2 = (Il2CppObject *)L_6;
			XmlWriter_t89522450 * L_7 = ___writer0;
			NullCheck((XmlWriter_t89522450 *)L_7);
			XmlWriter_WriteStartElement_m4138955962((XmlWriter_t89522450 *)L_7, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
			XmlWriter_t89522450 * L_8 = ___writer0;
			NullCheck((XmlWriter_t89522450 *)L_8);
			XmlWriter_WriteStartElement_m4138955962((XmlWriter_t89522450 *)L_8, (String_t*)_stringLiteral106079, /*hidden argument*/NULL);
			XmlSerializer_t1888860807 * L_9 = V_0;
			XmlWriter_t89522450 * L_10 = ___writer0;
			Il2CppObject * L_11 = V_2;
			NullCheck((XmlSerializer_t1888860807 *)L_9);
			XmlSerializer_Serialize_m3376583400((XmlSerializer_t1888860807 *)L_9, (XmlWriter_t89522450 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
			XmlWriter_t89522450 * L_12 = ___writer0;
			NullCheck((XmlWriter_t89522450 *)L_12);
			VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, (XmlWriter_t89522450 *)L_12);
			XmlWriter_t89522450 * L_13 = ___writer0;
			NullCheck((XmlWriter_t89522450 *)L_13);
			XmlWriter_WriteStartElement_m4138955962((XmlWriter_t89522450 *)L_13, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
			Il2CppObject * L_14 = V_2;
			NullCheck((Dictionary_2_t3824425150 *)__this);
			Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0) */, (Dictionary_2_t3824425150 *)__this, (Il2CppObject *)L_14);
			V_4 = (Il2CppObject *)L_15;
			XmlSerializer_t1888860807 * L_16 = V_1;
			XmlWriter_t89522450 * L_17 = ___writer0;
			Il2CppObject * L_18 = V_4;
			NullCheck((XmlSerializer_t1888860807 *)L_16);
			XmlSerializer_Serialize_m3376583400((XmlSerializer_t1888860807 *)L_16, (XmlWriter_t89522450 *)L_17, (Il2CppObject *)L_18, /*hidden argument*/NULL);
			XmlWriter_t89522450 * L_19 = ___writer0;
			NullCheck((XmlWriter_t89522450 *)L_19);
			VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, (XmlWriter_t89522450 *)L_19);
			XmlWriter_t89522450 * L_20 = ___writer0;
			NullCheck((XmlWriter_t89522450 *)L_20);
			VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, (XmlWriter_t89522450 *)L_20);
		}

IL_0090:
		{
			bool L_21 = ((  bool (*) (Enumerator_t3591453091 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Enumerator_t3591453091 *)(&V_3), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			if (L_21)
			{
				goto IL_0031;
			}
		}

IL_009c:
		{
			IL2CPP_LEAVE(0xAD, FINALLY_00a1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00a1;
	}

FINALLY_00a1:
	{ // begin finally (depth: 1)
		Enumerator_t3591453091  L_22 = V_3;
		Enumerator_t3591453091  L_23 = L_22;
		Il2CppObject * L_24 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), &L_23);
		NullCheck((Il2CppObject *)L_24);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_24);
		IL2CPP_END_FINALLY(161)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(161)
	{
		IL2CPP_JUMP_TBL(0xAD, IL_00ad)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00ad:
	{
		return;
	}
}
// System.Void Singleton`1<System.Object>::.ctor()
extern "C"  void Singleton_1__ctor_m3958676923_gshared (Singleton_1_t1089921813 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t3012272455 *)__this);
		MonoBehaviour__ctor_m350695606((MonoBehaviour_t3012272455 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton`1<System.Object>::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Singleton_1__cctor_m1977804114_MetadataUsageId;
extern "C"  void Singleton_1__cctor_m1977804114_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Singleton_1__cctor_m1977804114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__lock_3(L_0);
		((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_applicationIsQuitting_4((bool)0);
		return;
	}
}
// T Singleton`1<System.Object>::get_Instance()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2620945679;
extern Il2CppCodeGenString* _stringLiteral2213913289;
extern Il2CppCodeGenString* _stringLiteral4147623136;
extern Il2CppCodeGenString* _stringLiteral3450830138;
extern Il2CppCodeGenString* _stringLiteral2812549726;
extern Il2CppCodeGenString* _stringLiteral3415860014;
extern Il2CppCodeGenString* _stringLiteral404738514;
extern Il2CppCodeGenString* _stringLiteral3410841098;
extern const uint32_t Singleton_1_get_Instance_m1020946630_MetadataUsageId;
extern "C"  Il2CppObject * Singleton_1_get_Instance_m1020946630_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Singleton_1_get_Instance_m1020946630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	GameObject_t4012695102 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_0 = ((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_applicationIsQuitting_4();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2809334143(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2620945679, (Il2CppObject *)L_1, (Il2CppObject *)_stringLiteral2213913289, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_3 = ((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__lock_3();
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			Il2CppObject * L_5 = ((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_2();
			bool L_6 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0142;
			}
		}

IL_0050:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
			Object_t3878351788 * L_8 = Object_FindObjectOfType_m709000164(NULL /*static, unused*/, (Type_t *)L_7, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
			Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
			ObjectU5BU5D_t3051965477* L_10 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
			NullCheck(L_10);
			if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))) <= ((int32_t)1)))
			{
				goto IL_0095;
			}
		}

IL_0080:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4147623136, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			Il2CppObject * L_11 = ((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_2();
			V_2 = (Il2CppObject *)L_11;
			IL2CPP_LEAVE(0x159, FINALLY_0152);
		}

IL_0095:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			Il2CppObject * L_12 = ((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_2();
			bool L_13 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_12, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
			if (!L_13)
			{
				goto IL_011e;
			}
		}

IL_00aa:
		{
			GameObject_t4012695102 * L_14 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
			GameObject__ctor_m845034556(L_14, /*hidden argument*/NULL);
			V_1 = (GameObject_t4012695102 *)L_14;
			GameObject_t4012695102 * L_15 = V_1;
			NullCheck((GameObject_t4012695102 *)L_15);
			Il2CppObject * L_16 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((GameObject_t4012695102 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__instance_2(L_16);
			GameObject_t4012695102 * L_17 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
			NullCheck((Type_t *)L_18);
			String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_18);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_20 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3450830138, (String_t*)L_19, /*hidden argument*/NULL);
			NullCheck((Object_t3878351788 *)L_17);
			Object_set_name_m2334706817((Object_t3878351788 *)L_17, (String_t*)L_20, /*hidden argument*/NULL);
			GameObject_t4012695102 * L_21 = V_1;
			Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, (Object_t3878351788 *)L_21, /*hidden argument*/NULL);
			ObjectU5BU5D_t11523773* L_22 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
			NullCheck(L_22);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
			ArrayElementTypeCheck (L_22, _stringLiteral2812549726);
			(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2812549726);
			ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_22;
			Type_t * L_24 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
			NullCheck(L_23);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
			ArrayElementTypeCheck (L_23, L_24);
			(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_24);
			ObjectU5BU5D_t11523773* L_25 = (ObjectU5BU5D_t11523773*)L_23;
			NullCheck(L_25);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 2);
			ArrayElementTypeCheck (L_25, _stringLiteral3415860014);
			(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3415860014);
			ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_25;
			GameObject_t4012695102 * L_27 = V_1;
			NullCheck(L_26);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 3);
			ArrayElementTypeCheck (L_26, L_27);
			(L_26)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_27);
			ObjectU5BU5D_t11523773* L_28 = (ObjectU5BU5D_t11523773*)L_26;
			NullCheck(L_28);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 4);
			ArrayElementTypeCheck (L_28, _stringLiteral404738514);
			(L_28)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral404738514);
			String_t* L_29 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_28, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_Log_m3349710197(NULL /*static, unused*/, (Il2CppObject *)L_29, /*hidden argument*/NULL);
			goto IL_0142;
		}

IL_011e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			NullCheck((Component_t2126946602 *)(*(((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of__instance_2())));
			GameObject_t4012695102 * L_30 = Component_get_gameObject_m2112202034((Component_t2126946602 *)(*(((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of__instance_2())), /*hidden argument*/NULL);
			NullCheck((Object_t3878351788 *)L_30);
			String_t* L_31 = Object_get_name_m3709440845((Object_t3878351788 *)L_30, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_32 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3410841098, (String_t*)L_31, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_Log_m3349710197(NULL /*static, unused*/, (Il2CppObject *)L_32, /*hidden argument*/NULL);
		}

IL_0142:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			Il2CppObject * L_33 = ((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__instance_2();
			V_2 = (Il2CppObject *)L_33;
			IL2CPP_LEAVE(0x159, FINALLY_0152);
		}

IL_014d:
		{
			; // IL_014d: leave IL_0159
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0152;
	}

FINALLY_0152:
	{ // begin finally (depth: 1)
		Il2CppObject * L_34 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_34, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(338)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(338)
	{
		IL2CPP_JUMP_TBL(0x159, IL_0159)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0159:
	{
		Il2CppObject * L_35 = V_2;
		return L_35;
	}
}
// System.Void Singleton`1<System.Object>::OnDestroy()
extern "C"  void Singleton_1_OnDestroy_m276503860_gshared (Singleton_1_t1089921813 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((Singleton_1_t1089921813_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_applicationIsQuitting_4((bool)1);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m146280347_gshared (JsonResponse_1_t4259692931 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m3520725836_gshared (JsonResponse_1_t4259692931 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m1936855943_gshared (JsonResponse_1_t4259692931 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::get_value()
extern "C"  Nullable_1_t2936825364  JsonResponse_1_get_value_m3308583682_gshared (JsonResponse_1_t4259692931 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t2936825364  L_0 = (Nullable_1_t2936825364 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m597188561_gshared (JsonResponse_1_t4259692931 * __this, Nullable_1_t2936825364  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t2936825364  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m2616112565_gshared (JsonResponse_1_t4259692931 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m4253582206_gshared (JsonResponse_1_t4259692931 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m1872028901_gshared (JsonResponse_1_t1322443133 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m1310521980_gshared (JsonResponse_1_t1322443133 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m2329592061_gshared (JsonResponse_1_t1322443133 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::get_value()
extern "C"  Nullable_1_t4294542862  JsonResponse_1_get_value_m2694535530_gshared (JsonResponse_1_t1322443133 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t4294542862  L_0 = (Nullable_1_t4294542862 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m2629781703_gshared (JsonResponse_1_t1322443133 * __this, Nullable_1_t4294542862  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t4294542862  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m324051173_gshared (JsonResponse_1_t1322443133 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEducation>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m3715869556_gshared (JsonResponse_1_t1322443133 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m3628936110_gshared (JsonResponse_1_t811794004 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m1776202501_gshared (JsonResponse_1_t811794004 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m2572480276_gshared (JsonResponse_1_t811794004 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::get_value()
extern "C"  Nullable_1_t3783893733  JsonResponse_1_get_value_m3536917427_gshared (JsonResponse_1_t811794004 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3783893733  L_0 = (Nullable_1_t3783893733 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m2973816734_gshared (JsonResponse_1_t811794004 * __this, Nullable_1_t3783893733  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3783893733  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m1166433070_gshared (JsonResponse_1_t811794004 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m908242891_gshared (JsonResponse_1_t811794004 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m2502626008_gshared (JsonResponse_1_t3397282150 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m94153161_gshared (JsonResponse_1_t3397282150 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m4178286250_gshared (JsonResponse_1_t3397282150 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::get_value()
extern "C"  Nullable_1_t2074414583  JsonResponse_1_get_value_m13871167_gshared (JsonResponse_1_t3397282150 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t2074414583  L_0 = (Nullable_1_t2074414583 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m1189208756_gshared (JsonResponse_1_t3397282150 * __this, Nullable_1_t2074414583  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t2074414583  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m3919687922_gshared (JsonResponse_1_t3397282150 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m2194524641_gshared (JsonResponse_1_t3397282150 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m1769364167_gshared (JsonResponse_1_t3715343323 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m269850206_gshared (JsonResponse_1_t3715343323 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m1074170587_gshared (JsonResponse_1_t3715343323 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::get_value()
extern "C"  Nullable_1_t2392475756  JsonResponse_1_get_value_m2948429516_gshared (JsonResponse_1_t3715343323 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t2392475756  L_0 = (Nullable_1_t2392475756 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m3492163621_gshared (JsonResponse_1_t3715343323 * __this, Nullable_1_t2392475756  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t2392475756  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m965856327_gshared (JsonResponse_1_t3715343323 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m4141643218_gshared (JsonResponse_1_t3715343323 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m3709767063_gshared (JsonResponse_1_t815460875 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m2611620782_gshared (JsonResponse_1_t815460875 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m3409468939_gshared (JsonResponse_1_t815460875 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::get_value()
extern "C"  Nullable_1_t3787560604  JsonResponse_1_get_value_m1939466780_gshared (JsonResponse_1_t815460875 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3787560604  L_0 = (Nullable_1_t3787560604 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m448610133_gshared (JsonResponse_1_t815460875 * __this, Nullable_1_t3787560604  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3787560604  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m844516759_gshared (JsonResponse_1_t815460875 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m2095463682_gshared (JsonResponse_1_t815460875 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m2368149244_gshared (JsonResponse_1_t124943520 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m4147658579_gshared (JsonResponse_1_t124943520 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m410598918_gshared (JsonResponse_1_t124943520 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::get_value()
extern "C"  Nullable_1_t3097043249  JsonResponse_1_get_value_m3315592833_gshared (JsonResponse_1_t124943520 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = (Nullable_1_t3097043249 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m2751188240_gshared (JsonResponse_1_t124943520 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m3803057148_gshared (JsonResponse_1_t124943520 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m2104429117_gshared (JsonResponse_1_t124943520 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m97221841_gshared (JsonResponse_1_t448454793 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m823265730_gshared (JsonResponse_1_t448454793 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m4265333649_gshared (JsonResponse_1_t448454793 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::get_value()
extern "C"  Nullable_1_t3420554522  JsonResponse_1_get_value_m3940125432_gshared (JsonResponse_1_t448454793 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3420554522  L_0 = (Nullable_1_t3420554522 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m3066479707_gshared (JsonResponse_1_t448454793 * __this, Nullable_1_t3420554522  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3420554522  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m222230187_gshared (JsonResponse_1_t448454793 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Double>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m4242696456_gshared (JsonResponse_1_t448454793 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m3195743510_gshared (JsonResponse_1_t2761352966 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m3662287789_gshared (JsonResponse_1_t2761352966 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m109506732_gshared (JsonResponse_1_t2761352966 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::get_value()
extern "C"  Nullable_1_t1438485399  JsonResponse_1_get_value_m776780635_gshared (JsonResponse_1_t2761352966 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = (Nullable_1_t1438485399 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m3726866230_gshared (JsonResponse_1_t2761352966 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m1218195926_gshared (JsonResponse_1_t2761352966 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m517647203_gshared (JsonResponse_1_t2761352966 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m2573699927_gshared (JsonResponse_1_t2761353061 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m2878858670_gshared (JsonResponse_1_t2761353061 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m1268525131_gshared (JsonResponse_1_t2761353061 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::get_value()
extern "C"  Nullable_1_t1438485494  JsonResponse_1_get_value_m3815641372_gshared (JsonResponse_1_t2761353061 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485494  L_0 = (Nullable_1_t1438485494 )__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m3442268565_gshared (JsonResponse_1_t2761353061 * __this, Nullable_1_t1438485494  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485494  L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m4257056663_gshared (JsonResponse_1_t2761353061 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int64>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m1937798978_gshared (JsonResponse_1_t2761353061 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Object>::.ctor()
extern "C"  void JsonResponse_1__ctor_m4253934096_gshared (JsonResponse_1_t2159973987 * __this, const MethodInfo* method)
{
	{
		NullCheck((AbstractResponse_t2918001245 *)__this);
		AbstractResponse__ctor_m828474760((AbstractResponse_t2918001245 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Object>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m910009575_gshared (JsonResponse_1_t2159973987 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CkeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Object>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m1504209010_gshared (JsonResponse_1_t2159973987 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// T SponsorPay.SPUser/JsonResponse`1<System.Object>::get_value()
extern "C"  Il2CppObject * JsonResponse_1_get_value_m804226325_gshared (JsonResponse_1_t2159973987 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CvalueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Object>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m222088828_gshared (JsonResponse_1_t2159973987 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String SponsorPay.SPUser/JsonResponse`1<System.Object>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m1978686608_gshared (JsonResponse_1_t2159973987 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Object>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m796740009_gshared (JsonResponse_1_t2159973987 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void StaticCell`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t StaticCell_1__ctor_m240399728_MetadataUsageId;
extern "C"  void StaticCell_1__ctor_m240399728_gshared (StaticCell_1_t175558834 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StaticCell_1__ctor_m240399728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_Value_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StaticCell`1<System.Object>::.ctor(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t StaticCell_1__ctor_m3157425646_MetadataUsageId;
extern "C"  void StaticCell_1__ctor_m3157425646_gshared (StaticCell_1_t175558834 * __this, Il2CppObject * ___initial0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StaticCell_1__ctor_m3157425646_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_Value_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___initial0;
		__this->set_Value_0(L_1);
		return;
	}
}
// T StaticCell`1<System.Object>::get_value()
extern "C"  Il2CppObject * StaticCell_1_get_value_m2541444597_gshared (StaticCell_1_t175558834 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_Value_0();
		return L_0;
	}
}
// System.IDisposable StaticCell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern Il2CppClass* EmptyDisposable_t1512564738_il2cpp_TypeInfo_var;
extern const uint32_t StaticCell_1_ListenUpdates_m3711080542_MetadataUsageId;
extern "C"  Il2CppObject * StaticCell_1_ListenUpdates_m3711080542_gshared (StaticCell_1_t175558834 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StaticCell_1_ListenUpdates_m3711080542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EmptyDisposable_t1512564738 * L_0 = (EmptyDisposable_t1512564738 *)il2cpp_codegen_object_new(EmptyDisposable_t1512564738_il2cpp_TypeInfo_var);
		EmptyDisposable__ctor_m791146077(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.IDisposable StaticCell`1<System.Object>::OnChanged(System.Action,Priority)
extern Il2CppClass* EmptyDisposable_t1512564738_il2cpp_TypeInfo_var;
extern const uint32_t StaticCell_1_OnChanged_m3085514459_MetadataUsageId;
extern "C"  Il2CppObject * StaticCell_1_OnChanged_m3085514459_gshared (StaticCell_1_t175558834 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StaticCell_1_OnChanged_m3085514459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EmptyDisposable_t1512564738 * L_0 = (EmptyDisposable_t1512564738 *)il2cpp_codegen_object_new(EmptyDisposable_t1512564738_il2cpp_TypeInfo_var);
		EmptyDisposable__ctor_m791146077(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Object StaticCell`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * StaticCell_1_get_valueObject_m2609974602_gshared (StaticCell_1_t175558834 * __this, const MethodInfo* method)
{
	{
		NullCheck((StaticCell_1_t175558834 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* T StaticCell`1<System.Object>::get_value() */, (StaticCell_1_t175558834 *)__this);
		return L_0;
	}
}
// System.IDisposable StaticCell`1<System.Object>::Bind(System.Action`1<T>,Priority)
extern Il2CppClass* EmptyDisposable_t1512564738_il2cpp_TypeInfo_var;
extern const uint32_t StaticCell_1_Bind_m2516612132_MetadataUsageId;
extern "C"  Il2CppObject * StaticCell_1_Bind_m2516612132_gshared (StaticCell_1_t175558834 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StaticCell_1_Bind_m2516612132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t985559125 * L_0 = ___action0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_Value_0();
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EmptyDisposable_t1512564738 * L_2 = (EmptyDisposable_t1512564738 *)il2cpp_codegen_object_new(EmptyDisposable_t1512564738_il2cpp_TypeInfo_var);
		EmptyDisposable__ctor_m791146077(L_2, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Boolean>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m3851394505_gshared (U3CListenU3Ec__AnonStorey128_t3737332661 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Boolean>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m2738631782_gshared (U3CListenU3Ec__AnonStorey128_t3737332661 * __this, bool ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.DateTime>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m2582115780_gshared (U3CListenU3Ec__AnonStorey128_t3865361256 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.DateTime>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1130501857_gshared (U3CListenU3Ec__AnonStorey128_t3865361256 * __this, DateTime_t339033936  ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Double>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m1234771834_gshared (U3CListenU3Ec__AnonStorey128_t4060843934 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Double>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m4267406743_gshared (U3CListenU3Ec__AnonStorey128_t4060843934 * __this, double ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Int32>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m3688431535_gshared (U3CListenU3Ec__AnonStorey128_t2078774811 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Int32>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m2962633548_gshared (U3CListenU3Ec__AnonStorey128_t2078774811 * __this, int32_t ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
