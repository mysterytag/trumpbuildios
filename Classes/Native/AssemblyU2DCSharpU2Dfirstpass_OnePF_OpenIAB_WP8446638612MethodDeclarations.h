﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.OpenIAB_WP8
struct OpenIAB_WP8_t446638612;

#include "codegen/il2cpp-codegen.h"

// System.Void OnePF.OpenIAB_WP8::.ctor()
extern "C"  void OpenIAB_WP8__ctor_m3658365417 (OpenIAB_WP8_t446638612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_WP8::.cctor()
extern "C"  void OpenIAB_WP8__cctor_m1258082020 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
