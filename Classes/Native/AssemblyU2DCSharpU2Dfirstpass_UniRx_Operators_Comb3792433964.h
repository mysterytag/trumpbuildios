﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.Operators.ICombineLatestObservable
struct ICombineLatestObservable_t195534285;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CombineLatestObserver`1<System.Boolean>
struct  CombineLatestObserver_1_t3792433964  : public Il2CppObject
{
public:
	// System.Object UniRx.Operators.CombineLatestObserver`1::gate
	Il2CppObject * ___gate_0;
	// UniRx.Operators.ICombineLatestObservable UniRx.Operators.CombineLatestObserver`1::parent
	Il2CppObject * ___parent_1;
	// System.Int32 UniRx.Operators.CombineLatestObserver`1::index
	int32_t ___index_2;
	// T UniRx.Operators.CombineLatestObserver`1::value
	bool ___value_3;

public:
	inline static int32_t get_offset_of_gate_0() { return static_cast<int32_t>(offsetof(CombineLatestObserver_1_t3792433964, ___gate_0)); }
	inline Il2CppObject * get_gate_0() const { return ___gate_0; }
	inline Il2CppObject ** get_address_of_gate_0() { return &___gate_0; }
	inline void set_gate_0(Il2CppObject * value)
	{
		___gate_0 = value;
		Il2CppCodeGenWriteBarrier(&___gate_0, value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(CombineLatestObserver_1_t3792433964, ___parent_1)); }
	inline Il2CppObject * get_parent_1() const { return ___parent_1; }
	inline Il2CppObject ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(Il2CppObject * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(CombineLatestObserver_1_t3792433964, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(CombineLatestObserver_1_t3792433964, ___value_3)); }
	inline bool get_value_3() const { return ___value_3; }
	inline bool* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(bool value)
	{
		___value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
