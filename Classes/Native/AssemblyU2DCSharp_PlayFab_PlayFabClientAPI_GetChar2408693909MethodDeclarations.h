﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataRequestCallback
struct GetCharacterReadOnlyDataRequestCallback_t2408693909;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterDataRequest
struct GetCharacterDataRequest_t572698882;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacter572698882.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterReadOnlyDataRequestCallback__ctor_m2533725860 (GetCharacterReadOnlyDataRequestCallback_t2408693909 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterDataRequest,System.Object)
extern "C"  void GetCharacterReadOnlyDataRequestCallback_Invoke_m3879082285 (GetCharacterReadOnlyDataRequestCallback_t2408693909 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterReadOnlyDataRequestCallback_t2408693909(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterReadOnlyDataRequestCallback_BeginInvoke_m2177237824 (GetCharacterReadOnlyDataRequestCallback_t2408693909 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterReadOnlyDataRequestCallback_EndInvoke_m181795508 (GetCharacterReadOnlyDataRequestCallback_t2408693909 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
