﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.GA_Debug
struct  GA_Debug_t3429711792  : public Il2CppObject
{
public:

public:
};

struct GA_Debug_t3429711792_StaticFields
{
public:
	// System.Int32 GameAnalyticsSDK.GA_Debug::MaxErrorCount
	int32_t ___MaxErrorCount_0;
	// System.Int32 GameAnalyticsSDK.GA_Debug::_errorCount
	int32_t ____errorCount_1;
	// System.Boolean GameAnalyticsSDK.GA_Debug::_showLogOnGUI
	bool ____showLogOnGUI_2;
	// System.Collections.Generic.List`1<System.String> GameAnalyticsSDK.GA_Debug::Messages
	List_1_t1765447871 * ___Messages_3;

public:
	inline static int32_t get_offset_of_MaxErrorCount_0() { return static_cast<int32_t>(offsetof(GA_Debug_t3429711792_StaticFields, ___MaxErrorCount_0)); }
	inline int32_t get_MaxErrorCount_0() const { return ___MaxErrorCount_0; }
	inline int32_t* get_address_of_MaxErrorCount_0() { return &___MaxErrorCount_0; }
	inline void set_MaxErrorCount_0(int32_t value)
	{
		___MaxErrorCount_0 = value;
	}

	inline static int32_t get_offset_of__errorCount_1() { return static_cast<int32_t>(offsetof(GA_Debug_t3429711792_StaticFields, ____errorCount_1)); }
	inline int32_t get__errorCount_1() const { return ____errorCount_1; }
	inline int32_t* get_address_of__errorCount_1() { return &____errorCount_1; }
	inline void set__errorCount_1(int32_t value)
	{
		____errorCount_1 = value;
	}

	inline static int32_t get_offset_of__showLogOnGUI_2() { return static_cast<int32_t>(offsetof(GA_Debug_t3429711792_StaticFields, ____showLogOnGUI_2)); }
	inline bool get__showLogOnGUI_2() const { return ____showLogOnGUI_2; }
	inline bool* get_address_of__showLogOnGUI_2() { return &____showLogOnGUI_2; }
	inline void set__showLogOnGUI_2(bool value)
	{
		____showLogOnGUI_2 = value;
	}

	inline static int32_t get_offset_of_Messages_3() { return static_cast<int32_t>(offsetof(GA_Debug_t3429711792_StaticFields, ___Messages_3)); }
	inline List_1_t1765447871 * get_Messages_3() const { return ___Messages_3; }
	inline List_1_t1765447871 ** get_address_of_Messages_3() { return &___Messages_3; }
	inline void set_Messages_3(List_1_t1765447871 * value)
	{
		___Messages_3 = value;
		Il2CppCodeGenWriteBarrier(&___Messages_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
