﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayerTradesResponseCallback
struct GetPlayerTradesResponseCallback_t2351079404;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayerTradesRequest
struct GetPlayerTradesRequest_t3393757081;
// PlayFab.ClientModels.GetPlayerTradesResponse
struct GetPlayerTradesResponse_t1092460183;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerTr3393757081.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerTr1092460183.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayerTradesResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayerTradesResponseCallback__ctor_m714319483 (GetPlayerTradesResponseCallback_t2351079404 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayerTradesResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayerTradesRequest,PlayFab.ClientModels.GetPlayerTradesResponse,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPlayerTradesResponseCallback_Invoke_m570476054 (GetPlayerTradesResponseCallback_t2351079404 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayerTradesRequest_t3393757081 * ___request2, GetPlayerTradesResponse_t1092460183 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayerTradesResponseCallback_t2351079404(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayerTradesRequest_t3393757081 * ___request2, GetPlayerTradesResponse_t1092460183 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayerTradesResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayerTradesRequest,PlayFab.ClientModels.GetPlayerTradesResponse,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayerTradesResponseCallback_BeginInvoke_m2484282991 (GetPlayerTradesResponseCallback_t2351079404 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayerTradesRequest_t3393757081 * ___request2, GetPlayerTradesResponse_t1092460183 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayerTradesResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayerTradesResponseCallback_EndInvoke_m3969617419 (GetPlayerTradesResponseCallback_t2351079404 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
