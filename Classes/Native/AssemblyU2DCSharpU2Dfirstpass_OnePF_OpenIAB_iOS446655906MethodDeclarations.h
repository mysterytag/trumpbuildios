﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.OpenIAB_iOS
struct OpenIAB_iOS_t446655906;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.String
struct String_t;
// OnePF.Options
struct Options_t3062372690;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// OnePF.Purchase
struct Purchase_t160195573;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Options3062372690.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573.h"

// System.Void OnePF.OpenIAB_iOS::.ctor()
extern "C"  void OpenIAB_iOS__ctor_m2472941083 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::.cctor()
extern "C"  void OpenIAB_iOS__cctor_m3164633330 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::AppStore_requestProducts(System.String[],System.Int32)
extern "C"  void OpenIAB_iOS_AppStore_requestProducts_m1401556628 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___skus0, int32_t ___skusNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::AppStore_startPurchase(System.String)
extern "C"  void OpenIAB_iOS_AppStore_startPurchase_m3350896277 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::AppStore_restorePurchases()
extern "C"  void OpenIAB_iOS_AppStore_restorePurchases_m1704837692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.OpenIAB_iOS::Inventory_hasPurchase(System.String)
extern "C"  bool OpenIAB_iOS_Inventory_hasPurchase_m3366375669 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::Inventory_query()
extern "C"  void OpenIAB_iOS_Inventory_query_m1367326750 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::Inventory_removePurchase(System.String)
extern "C"  void OpenIAB_iOS_Inventory_removePurchase_m653450737 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.OpenIAB_iOS::IsDevice()
extern "C"  bool OpenIAB_iOS_IsDevice_m4101686221 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::init(OnePF.Options)
extern "C"  void OpenIAB_iOS_init_m2526952909 (OpenIAB_iOS_t446655906 * __this, Options_t3062372690 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void OpenIAB_iOS_init_m205509482 (OpenIAB_iOS_t446655906 * __this, Dictionary_2_t2606186806 * ___storeKeys0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::mapSku(System.String,System.String,System.String)
extern "C"  void OpenIAB_iOS_mapSku_m3488441136 (OpenIAB_iOS_t446655906 * __this, String_t* ___sku0, String_t* ___storeName1, String_t* ___storeSku2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::unbindService()
extern "C"  void OpenIAB_iOS_unbindService_m2578881304 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.OpenIAB_iOS::areSubscriptionsSupported()
extern "C"  bool OpenIAB_iOS_areSubscriptionsSupported_m1369726657 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::queryInventory()
extern "C"  void OpenIAB_iOS_queryInventory_m4138450461 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::queryInventory(System.String[])
extern "C"  void OpenIAB_iOS_queryInventory_m312300003 (OpenIAB_iOS_t446655906 * __this, StringU5BU5D_t2956870243* ___skus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::purchaseProduct(System.String,System.String)
extern "C"  void OpenIAB_iOS_purchaseProduct_m491925719 (OpenIAB_iOS_t446655906 * __this, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::purchaseSubscription(System.String,System.String)
extern "C"  void OpenIAB_iOS_purchaseSubscription_m3876771447 (OpenIAB_iOS_t446655906 * __this, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::consumeProduct(OnePF.Purchase)
extern "C"  void OpenIAB_iOS_consumeProduct_m2220225353 (OpenIAB_iOS_t446655906 * __this, Purchase_t160195573 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::restoreTransactions()
extern "C"  void OpenIAB_iOS_restoreTransactions_m2170322844 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.OpenIAB_iOS::isDebugLog()
extern "C"  bool OpenIAB_iOS_isDebugLog_m433406216 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::enableDebugLogging(System.Boolean)
extern "C"  void OpenIAB_iOS_enableDebugLogging_m482342927 (OpenIAB_iOS_t446655906 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_iOS::enableDebugLogging(System.Boolean,System.String)
extern "C"  void OpenIAB_iOS_enableDebugLogging_m1170364939 (OpenIAB_iOS_t446655906 * __this, bool ___enabled0, String_t* ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.OpenIAB_iOS::StoreSku2Sku(System.String)
extern "C"  String_t* OpenIAB_iOS_StoreSku2Sku_m2417310421 (Il2CppObject * __this /* static, unused */, String_t* ___storeSku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.OpenIAB_iOS::Sku2StoreSku(System.String)
extern "C"  String_t* OpenIAB_iOS_Sku2StoreSku_m744454475 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
