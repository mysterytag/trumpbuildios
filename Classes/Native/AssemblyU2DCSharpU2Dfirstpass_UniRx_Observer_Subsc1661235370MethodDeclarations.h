﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe`1<System.Int64>
struct Subscribe_1_t1661235370;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe`1<System.Int64>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m3372227756_gshared (Subscribe_1_t1661235370 * __this, Action_1_t2995867587 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define Subscribe_1__ctor_m3372227756(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (Subscribe_1_t1661235370 *, Action_1_t2995867587 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe_1__ctor_m3372227756_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
// System.Void UniRx.Observer/Subscribe`1<System.Int64>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m4079264649_gshared (Subscribe_1_t1661235370 * __this, int64_t ___value0, const MethodInfo* method);
#define Subscribe_1_OnNext_m4079264649(__this, ___value0, method) ((  void (*) (Subscribe_1_t1661235370 *, int64_t, const MethodInfo*))Subscribe_1_OnNext_m4079264649_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Int64>::OnError(System.Exception)
extern "C"  void Subscribe_1_OnError_m1772784792_gshared (Subscribe_1_t1661235370 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe_1_OnError_m1772784792(__this, ___error0, method) ((  void (*) (Subscribe_1_t1661235370 *, Exception_t1967233988 *, const MethodInfo*))Subscribe_1_OnError_m1772784792_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Int64>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m380986347_gshared (Subscribe_1_t1661235370 * __this, const MethodInfo* method);
#define Subscribe_1_OnCompleted_m380986347(__this, method) ((  void (*) (Subscribe_1_t1661235370 *, const MethodInfo*))Subscribe_1_OnCompleted_m380986347_gshared)(__this, method)
