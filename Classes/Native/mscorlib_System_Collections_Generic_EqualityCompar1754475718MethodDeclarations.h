﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.CollectionAddEvent`1<System.Object>>
struct DefaultComparer_t1754475718;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m3104778554_gshared (DefaultComparer_t1754475718 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3104778554(__this, method) ((  void (*) (DefaultComparer_t1754475718 *, const MethodInfo*))DefaultComparer__ctor_m3104778554_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.CollectionAddEvent`1<System.Object>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3064483889_gshared (DefaultComparer_t1754475718 * __this, CollectionAddEvent_1_t2416521987  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3064483889(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1754475718 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))DefaultComparer_GetHashCode_m3064483889_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.CollectionAddEvent`1<System.Object>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2511740555_gshared (DefaultComparer_t1754475718 * __this, CollectionAddEvent_1_t2416521987  ___x0, CollectionAddEvent_1_t2416521987  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2511740555(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1754475718 *, CollectionAddEvent_1_t2416521987 , CollectionAddEvent_1_t2416521987 , const MethodInfo*))DefaultComparer_Equals_m2511740555_gshared)(__this, ___x0, ___y1, method)
