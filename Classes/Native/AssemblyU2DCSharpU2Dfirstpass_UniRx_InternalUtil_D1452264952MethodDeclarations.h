﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>
struct DisposedObserver_1_t1452264952;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3476395510_gshared (DisposedObserver_1_t1452264952 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m3476395510(__this, method) ((  void (*) (DisposedObserver_1_t1452264952 *, const MethodInfo*))DisposedObserver_1__ctor_m3476395510_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m4206949495_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m4206949495(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m4206949495_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m437909760_gshared (DisposedObserver_1_t1452264952 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m437909760(__this, method) ((  void (*) (DisposedObserver_1_t1452264952 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m437909760_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1251420717_gshared (DisposedObserver_1_t1452264952 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1251420717(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1452264952 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1251420717_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.MasterServerEvent>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2162232094_gshared (DisposedObserver_1_t1452264952 * __this, int32_t ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2162232094(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1452264952 *, int32_t, const MethodInfo*))DisposedObserver_1_OnNext_m2162232094_gshared)(__this, ___value0, method)
