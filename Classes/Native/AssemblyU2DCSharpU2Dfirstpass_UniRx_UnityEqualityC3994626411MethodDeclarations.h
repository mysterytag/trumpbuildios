﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEqualityComparer/RectEqualityComparer
struct RectEqualityComparer_t3994626411;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// System.Void UniRx.UnityEqualityComparer/RectEqualityComparer::.ctor()
extern "C"  void RectEqualityComparer__ctor_m1048199033 (RectEqualityComparer_t3994626411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.UnityEqualityComparer/RectEqualityComparer::Equals(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool RectEqualityComparer_Equals_m3513262368 (RectEqualityComparer_t3994626411 * __this, Rect_t1525428817  ___self0, Rect_t1525428817  ___other1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.UnityEqualityComparer/RectEqualityComparer::GetHashCode(UnityEngine.Rect)
extern "C"  int32_t RectEqualityComparer_GetHashCode_m1134232183 (RectEqualityComparer_t3994626411 * __this, Rect_t1525428817  ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
