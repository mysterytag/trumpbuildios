﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`1/<ToInstance>c__AnonStorey173<System.Object>
struct U3CToInstanceU3Ec__AnonStorey173_t3921245137;
// System.Object
struct Il2CppObject;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"

// System.Void Zenject.IFactoryBinder`1/<ToInstance>c__AnonStorey173<System.Object>::.ctor()
extern "C"  void U3CToInstanceU3Ec__AnonStorey173__ctor_m879505835_gshared (U3CToInstanceU3Ec__AnonStorey173_t3921245137 * __this, const MethodInfo* method);
#define U3CToInstanceU3Ec__AnonStorey173__ctor_m879505835(__this, method) ((  void (*) (U3CToInstanceU3Ec__AnonStorey173_t3921245137 *, const MethodInfo*))U3CToInstanceU3Ec__AnonStorey173__ctor_m879505835_gshared)(__this, method)
// TContract Zenject.IFactoryBinder`1/<ToInstance>c__AnonStorey173<System.Object>::<>m__293(Zenject.DiContainer)
extern "C"  Il2CppObject * U3CToInstanceU3Ec__AnonStorey173_U3CU3Em__293_m3871525710_gshared (U3CToInstanceU3Ec__AnonStorey173_t3921245137 * __this, DiContainer_t2383114449 * ___c0, const MethodInfo* method);
#define U3CToInstanceU3Ec__AnonStorey173_U3CU3Em__293_m3871525710(__this, ___c0, method) ((  Il2CppObject * (*) (U3CToInstanceU3Ec__AnonStorey173_t3921245137 *, DiContainer_t2383114449 *, const MethodInfo*))U3CToInstanceU3Ec__AnonStorey173_U3CU3Em__293_m3871525710_gshared)(__this, ___c0, method)
