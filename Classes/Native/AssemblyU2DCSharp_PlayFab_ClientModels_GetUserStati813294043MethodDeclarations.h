﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetUserStatisticsRequest
struct GetUserStatisticsRequest_t813294043;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetUserStatisticsRequest::.ctor()
extern "C"  void GetUserStatisticsRequest__ctor_m149596866 (GetUserStatisticsRequest_t813294043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
