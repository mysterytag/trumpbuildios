﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Double>
struct List_1_t1331475583;
// System.Collections.Generic.IEnumerable`1<System.Double>
struct IEnumerable_1_t3406670970;
// System.Collections.Generic.IEnumerator`1<System.Double>
struct IEnumerator_1_t2017623062;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Double>
struct ICollection_1_t1000348000;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Double>
struct ReadOnlyCollection_1_t3697661962;
// System.Double[]
struct DoubleU5BU5D_t1048280995;
// System.Predicate`1<System.Double>
struct Predicate_1_t1105480512;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// System.Collections.Generic.IComparer`1<System.Double>
struct IComparer_1_t3234224023;
// System.Comparison`1<System.Double>
struct Comparison_1_t3238191490;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3712225871.h"

// System.Void System.Collections.Generic.List`1<System.Double>::.ctor()
extern "C"  void List_1__ctor_m157990182_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1__ctor_m157990182(__this, method) ((  void (*) (List_1_t1331475583 *, const MethodInfo*))List_1__ctor_m157990182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Double>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m998159257_gshared (List_1_t1331475583 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m998159257(__this, ___collection0, method) ((  void (*) (List_1_t1331475583 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m998159257_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1949821303_gshared (List_1_t1331475583 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1949821303(__this, ___capacity0, method) ((  void (*) (List_1_t1331475583 *, int32_t, const MethodInfo*))List_1__ctor_m1949821303_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::.cctor()
extern "C"  void List_1__cctor_m120632135_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m120632135(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m120632135_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Double>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3873276152_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3873276152(__this, method) ((  Il2CppObject* (*) (List_1_t1331475583 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3873276152_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Double>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1785097950_gshared (List_1_t1331475583 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1785097950(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1331475583 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1785097950_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Double>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3111477081_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3111477081(__this, method) ((  Il2CppObject * (*) (List_1_t1331475583 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3111477081_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Double>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3784699960_gshared (List_1_t1331475583 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3784699960(__this, ___item0, method) ((  int32_t (*) (List_1_t1331475583 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3784699960_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Double>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3499686548_gshared (List_1_t1331475583 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3499686548(__this, ___item0, method) ((  bool (*) (List_1_t1331475583 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3499686548_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Double>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m4151528080_gshared (List_1_t1331475583 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m4151528080(__this, ___item0, method) ((  int32_t (*) (List_1_t1331475583 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m4151528080_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3189916347_gshared (List_1_t1331475583 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3189916347(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1331475583 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3189916347_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Double>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2914292365_gshared (List_1_t1331475583 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2914292365(__this, ___item0, method) ((  void (*) (List_1_t1331475583 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2914292365_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Double>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4245406101_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4245406101(__this, method) ((  bool (*) (List_1_t1331475583 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4245406101_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Double>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3120784456_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3120784456(__this, method) ((  bool (*) (List_1_t1331475583 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3120784456_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Double>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3176968244_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3176968244(__this, method) ((  Il2CppObject * (*) (List_1_t1331475583 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3176968244_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Double>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m155527427_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m155527427(__this, method) ((  bool (*) (List_1_t1331475583 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m155527427_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Double>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3645614486_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3645614486(__this, method) ((  bool (*) (List_1_t1331475583 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3645614486_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Double>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m410346683_gshared (List_1_t1331475583 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m410346683(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1331475583 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m410346683_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3811879890_gshared (List_1_t1331475583 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3811879890(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1331475583 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3811879890_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Double>::Add(T)
extern "C"  void List_1_Add_m3597469337_gshared (List_1_t1331475583 * __this, double ___item0, const MethodInfo* method);
#define List_1_Add_m3597469337(__this, ___item0, method) ((  void (*) (List_1_t1331475583 *, double, const MethodInfo*))List_1_Add_m3597469337_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m650899028_gshared (List_1_t1331475583 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m650899028(__this, ___newCount0, method) ((  void (*) (List_1_t1331475583 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m650899028_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1129182546_gshared (List_1_t1331475583 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1129182546(__this, ___collection0, method) ((  void (*) (List_1_t1331475583 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1129182546_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m390154770_gshared (List_1_t1331475583 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m390154770(__this, ___enumerable0, method) ((  void (*) (List_1_t1331475583 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m390154770_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1859789125_gshared (List_1_t1331475583 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1859789125(__this, ___collection0, method) ((  void (*) (List_1_t1331475583 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1859789125_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Double>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3697661962 * List_1_AsReadOnly_m2375104324_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2375104324(__this, method) ((  ReadOnlyCollection_1_t3697661962 * (*) (List_1_t1331475583 *, const MethodInfo*))List_1_AsReadOnly_m2375104324_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Double>::Clear()
extern "C"  void List_1_Clear_m1859090769_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_Clear_m1859090769(__this, method) ((  void (*) (List_1_t1331475583 *, const MethodInfo*))List_1_Clear_m1859090769_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Double>::Contains(T)
extern "C"  bool List_1_Contains_m2713855487_gshared (List_1_t1331475583 * __this, double ___item0, const MethodInfo* method);
#define List_1_Contains_m2713855487(__this, ___item0, method) ((  bool (*) (List_1_t1331475583 *, double, const MethodInfo*))List_1_Contains_m2713855487_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2876365385_gshared (List_1_t1331475583 * __this, DoubleU5BU5D_t1048280995* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2876365385(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1331475583 *, DoubleU5BU5D_t1048280995*, int32_t, const MethodInfo*))List_1_CopyTo_m2876365385_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Double>::Find(System.Predicate`1<T>)
extern "C"  double List_1_Find_m3131198719_gshared (List_1_t1331475583 * __this, Predicate_1_t1105480512 * ___match0, const MethodInfo* method);
#define List_1_Find_m3131198719(__this, ___match0, method) ((  double (*) (List_1_t1331475583 *, Predicate_1_t1105480512 *, const MethodInfo*))List_1_Find_m3131198719_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1161217722_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1105480512 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1161217722(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1105480512 *, const MethodInfo*))List_1_CheckMatch_m1161217722_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Double>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1045874655_gshared (List_1_t1331475583 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1105480512 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1045874655(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1331475583 *, int32_t, int32_t, Predicate_1_t1105480512 *, const MethodInfo*))List_1_GetIndex_m1045874655_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<System.Double>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1781773742_gshared (List_1_t1331475583 * __this, Action_1_t682969319 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1781773742(__this, ___action0, method) ((  void (*) (List_1_t1331475583 *, Action_1_t682969319 *, const MethodInfo*))List_1_ForEach_m1781773742_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Double>::GetEnumerator()
extern "C"  Enumerator_t3712225871  List_1_GetEnumerator_m885490620_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m885490620(__this, method) ((  Enumerator_t3712225871  (*) (List_1_t1331475583 *, const MethodInfo*))List_1_GetEnumerator_m885490620_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Double>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1289169805_gshared (List_1_t1331475583 * __this, double ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1289169805(__this, ___item0, method) ((  int32_t (*) (List_1_t1331475583 *, double, const MethodInfo*))List_1_IndexOf_m1289169805_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2765313952_gshared (List_1_t1331475583 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2765313952(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1331475583 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2765313952_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Double>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2622732569_gshared (List_1_t1331475583 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2622732569(__this, ___index0, method) ((  void (*) (List_1_t1331475583 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2622732569_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3408129792_gshared (List_1_t1331475583 * __this, int32_t ___index0, double ___item1, const MethodInfo* method);
#define List_1_Insert_m3408129792(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1331475583 *, int32_t, double, const MethodInfo*))List_1_Insert_m3408129792_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Double>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3339336565_gshared (List_1_t1331475583 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3339336565(__this, ___collection0, method) ((  void (*) (List_1_t1331475583 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3339336565_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Double>::Remove(T)
extern "C"  bool List_1_Remove_m3841350074_gshared (List_1_t1331475583 * __this, double ___item0, const MethodInfo* method);
#define List_1_Remove_m3841350074(__this, ___item0, method) ((  bool (*) (List_1_t1331475583 *, double, const MethodInfo*))List_1_Remove_m3841350074_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Double>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1891703696_gshared (List_1_t1331475583 * __this, Predicate_1_t1105480512 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1891703696(__this, ___match0, method) ((  int32_t (*) (List_1_t1331475583 *, Predicate_1_t1105480512 *, const MethodInfo*))List_1_RemoveAll_m1891703696_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1281982662_gshared (List_1_t1331475583 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1281982662(__this, ___index0, method) ((  void (*) (List_1_t1331475583 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1281982662_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::Reverse()
extern "C"  void List_1_Reverse_m1542520806_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_Reverse_m1542520806(__this, method) ((  void (*) (List_1_t1331475583 *, const MethodInfo*))List_1_Reverse_m1542520806_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Double>::Sort()
extern "C"  void List_1_Sort_m4123441532_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_Sort_m4123441532(__this, method) ((  void (*) (List_1_t1331475583 *, const MethodInfo*))List_1_Sort_m4123441532_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Double>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m151545832_gshared (List_1_t1331475583 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m151545832(__this, ___comparer0, method) ((  void (*) (List_1_t1331475583 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m151545832_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m781336783_gshared (List_1_t1331475583 * __this, Comparison_1_t3238191490 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m781336783(__this, ___comparison0, method) ((  void (*) (List_1_t1331475583 *, Comparison_1_t3238191490 *, const MethodInfo*))List_1_Sort_m781336783_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Double>::ToArray()
extern "C"  DoubleU5BU5D_t1048280995* List_1_ToArray_m1332027621_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_ToArray_m1332027621(__this, method) ((  DoubleU5BU5D_t1048280995* (*) (List_1_t1331475583 *, const MethodInfo*))List_1_ToArray_m1332027621_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Double>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4056665109_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m4056665109(__this, method) ((  void (*) (List_1_t1331475583 *, const MethodInfo*))List_1_TrimExcess_m4056665109_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Double>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1328321853_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1328321853(__this, method) ((  int32_t (*) (List_1_t1331475583 *, const MethodInfo*))List_1_get_Capacity_m1328321853_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Double>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2156055142_gshared (List_1_t1331475583 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2156055142(__this, ___value0, method) ((  void (*) (List_1_t1331475583 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2156055142_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Double>::get_Count()
extern "C"  int32_t List_1_get_Count_m1126865806_gshared (List_1_t1331475583 * __this, const MethodInfo* method);
#define List_1_get_Count_m1126865806(__this, method) ((  int32_t (*) (List_1_t1331475583 *, const MethodInfo*))List_1_get_Count_m1126865806_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Double>::get_Item(System.Int32)
extern "C"  double List_1_get_Item_m2752367178_gshared (List_1_t1331475583 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2752367178(__this, ___index0, method) ((  double (*) (List_1_t1331475583 *, int32_t, const MethodInfo*))List_1_get_Item_m2752367178_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Double>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4257247191_gshared (List_1_t1331475583 * __this, int32_t ___index0, double ___value1, const MethodInfo* method);
#define List_1_set_Item_m4257247191(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1331475583 *, int32_t, double, const MethodInfo*))List_1_set_Item_m4257247191_gshared)(__this, ___index0, ___value1, method)
