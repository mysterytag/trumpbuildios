﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OligarchParameters
struct OligarchParameters_t821144443;
// GlobalStat
struct GlobalStat_t1134678199;
// UnityEngine.UI.Image
struct Image_t3354615620;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bucket
struct  Bucket_t2000631306  : public MonoBehaviour_t3012272455
{
public:
	// OligarchParameters Bucket::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_2;
	// GlobalStat Bucket::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_3;
	// UnityEngine.UI.Image Bucket::Content
	Image_t3354615620 * ___Content_4;

public:
	inline static int32_t get_offset_of_OligarchParameters_2() { return static_cast<int32_t>(offsetof(Bucket_t2000631306, ___OligarchParameters_2)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_2() const { return ___OligarchParameters_2; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_2() { return &___OligarchParameters_2; }
	inline void set_OligarchParameters_2(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_2, value);
	}

	inline static int32_t get_offset_of_GlobalStat_3() { return static_cast<int32_t>(offsetof(Bucket_t2000631306, ___GlobalStat_3)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_3() const { return ___GlobalStat_3; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_3() { return &___GlobalStat_3; }
	inline void set_GlobalStat_3(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_3 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_3, value);
	}

	inline static int32_t get_offset_of_Content_4() { return static_cast<int32_t>(offsetof(Bucket_t2000631306, ___Content_4)); }
	inline Image_t3354615620 * get_Content_4() const { return ___Content_4; }
	inline Image_t3354615620 ** get_address_of_Content_4() { return &___Content_4; }
	inline void set_Content_4(Image_t3354615620 * value)
	{
		___Content_4 = value;
		Il2CppCodeGenWriteBarrier(&___Content_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
