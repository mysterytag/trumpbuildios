﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableBlock`2<System.Object,System.Object>
struct TableBlock_2_t3756399434;
// NewTableView
struct NewTableView_t2775249395;
// InstantiationPool`1<System.Object>
struct InstantiationPool_1_t1380750307;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// ZergRush.IObservableCollection`1<System.Object>
struct IObservableCollection_1_t200574308;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4105459918;
// CollectionRemoveEvent`1<System.Object>
struct CollectionRemoveEvent_1_t898259258;
// CollectionReplaceEvent`1<System.Object>
struct CollectionReplaceEvent_1_t2497372070;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NewTableView2775249395.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Object837106420.h"

// System.Void TableBlock`2<System.Object,System.Object>::.ctor(NewTableView,InstantiationPool`1<TPrefab>,System.String)
extern "C"  void TableBlock_2__ctor_m218251931_gshared (TableBlock_2_t3756399434 * __this, NewTableView_t2775249395 * ___newTableView0, InstantiationPool_1_t1380750307 * ___pool1, String_t* ___prefabPath2, const MethodInfo* method);
#define TableBlock_2__ctor_m218251931(__this, ___newTableView0, ___pool1, ___prefabPath2, method) ((  void (*) (TableBlock_2_t3756399434 *, NewTableView_t2775249395 *, InstantiationPool_1_t1380750307 *, String_t*, const MethodInfo*))TableBlock_2__ctor_m218251931_gshared)(__this, ___newTableView0, ___pool1, ___prefabPath2, method)
// TPrefab TableBlock`2<System.Object,System.Object>::CreatePrefab()
extern "C"  Il2CppObject * TableBlock_2_CreatePrefab_m2852969303_gshared (TableBlock_2_t3756399434 * __this, const MethodInfo* method);
#define TableBlock_2_CreatePrefab_m2852969303(__this, method) ((  Il2CppObject * (*) (TableBlock_2_t3756399434 *, const MethodInfo*))TableBlock_2_CreatePrefab_m2852969303_gshared)(__this, method)
// System.Void TableBlock`2<System.Object,System.Object>::Fill(ZergRush.IObservableCollection`1<TData>,System.Action`2<TPrefab,TData>)
extern "C"  void TableBlock_2_Fill_m920501207_gshared (TableBlock_2_t3756399434 * __this, Il2CppObject* ___observableData0, Action_2_t4105459918 * ___fillMethod1, const MethodInfo* method);
#define TableBlock_2_Fill_m920501207(__this, ___observableData0, ___fillMethod1, method) ((  void (*) (TableBlock_2_t3756399434 *, Il2CppObject*, Action_2_t4105459918 *, const MethodInfo*))TableBlock_2_Fill_m920501207_gshared)(__this, ___observableData0, ___fillMethod1, method)
// System.Single TableBlock`2<System.Object,System.Object>::GetSize()
extern "C"  float TableBlock_2_GetSize_m1972660851_gshared (TableBlock_2_t3756399434 * __this, const MethodInfo* method);
#define TableBlock_2_GetSize_m1972660851(__this, method) ((  float (*) (TableBlock_2_t3756399434 *, const MethodInfo*))TableBlock_2_GetSize_m1972660851_gshared)(__this, method)
// System.Void TableBlock`2<System.Object,System.Object>::TryShowCells(System.Single&,System.Single&,System.Single,System.Int32&)
extern "C"  void TableBlock_2_TryShowCells_m1596422709_gshared (TableBlock_2_t3756399434 * __this, float* ___containerLength0, float* ___offset1, float ___startPosition2, int32_t* ___forgetOffset3, const MethodInfo* method);
#define TableBlock_2_TryShowCells_m1596422709(__this, ___containerLength0, ___offset1, ___startPosition2, ___forgetOffset3, method) ((  void (*) (TableBlock_2_t3756399434 *, float*, float*, float, int32_t*, const MethodInfo*))TableBlock_2_TryShowCells_m1596422709_gshared)(__this, ___containerLength0, ___offset1, ___startPosition2, ___forgetOffset3, method)
// System.Void TableBlock`2<System.Object,System.Object>::Dispose()
extern "C"  void TableBlock_2_Dispose_m766308175_gshared (TableBlock_2_t3756399434 * __this, const MethodInfo* method);
#define TableBlock_2_Dispose_m766308175(__this, method) ((  void (*) (TableBlock_2_t3756399434 *, const MethodInfo*))TableBlock_2_Dispose_m766308175_gshared)(__this, method)
// System.Void TableBlock`2<System.Object,System.Object>::Clear(System.Boolean)
extern "C"  void TableBlock_2_Clear_m825508468_gshared (TableBlock_2_t3756399434 * __this, bool ___needRebuildTable0, const MethodInfo* method);
#define TableBlock_2_Clear_m825508468(__this, ___needRebuildTable0, method) ((  void (*) (TableBlock_2_t3756399434 *, bool, const MethodInfo*))TableBlock_2_Clear_m825508468_gshared)(__this, ___needRebuildTable0, method)
// System.Void TableBlock`2<System.Object,System.Object>::<Fill>m__133(UniRx.CollectionAddEvent`1<TData>)
extern "C"  void TableBlock_2_U3CFillU3Em__133_m2005993633_gshared (TableBlock_2_t3756399434 * __this, CollectionAddEvent_1_t2416521987  ___ev0, const MethodInfo* method);
#define TableBlock_2_U3CFillU3Em__133_m2005993633(__this, ___ev0, method) ((  void (*) (TableBlock_2_t3756399434 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))TableBlock_2_U3CFillU3Em__133_m2005993633_gshared)(__this, ___ev0, method)
// System.Void TableBlock`2<System.Object,System.Object>::<Fill>m__134(CollectionRemoveEvent`1<TData>)
extern "C"  void TableBlock_2_U3CFillU3Em__134_m2215213507_gshared (TableBlock_2_t3756399434 * __this, CollectionRemoveEvent_1_t898259258 * ___ev0, const MethodInfo* method);
#define TableBlock_2_U3CFillU3Em__134_m2215213507(__this, ___ev0, method) ((  void (*) (TableBlock_2_t3756399434 *, CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))TableBlock_2_U3CFillU3Em__134_m2215213507_gshared)(__this, ___ev0, method)
// System.Void TableBlock`2<System.Object,System.Object>::<Fill>m__135(UniRx.Unit)
extern "C"  void TableBlock_2_U3CFillU3Em__135_m3174864091_gshared (TableBlock_2_t3756399434 * __this, Unit_t2558286038  ___ev0, const MethodInfo* method);
#define TableBlock_2_U3CFillU3Em__135_m3174864091(__this, ___ev0, method) ((  void (*) (TableBlock_2_t3756399434 *, Unit_t2558286038 , const MethodInfo*))TableBlock_2_U3CFillU3Em__135_m3174864091_gshared)(__this, ___ev0, method)
// System.Void TableBlock`2<System.Object,System.Object>::<Fill>m__136(CollectionReplaceEvent`1<TData>)
extern "C"  void TableBlock_2_U3CFillU3Em__136_m3982713043_gshared (TableBlock_2_t3756399434 * __this, CollectionReplaceEvent_1_t2497372070 * ___ev0, const MethodInfo* method);
#define TableBlock_2_U3CFillU3Em__136_m3982713043(__this, ___ev0, method) ((  void (*) (TableBlock_2_t3756399434 *, CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))TableBlock_2_U3CFillU3Em__136_m3982713043_gshared)(__this, ___ev0, method)
// System.Single TableBlock`2<System.Object,System.Object>::<GetSize>m__137(TPrefab)
extern "C"  float TableBlock_2_U3CGetSizeU3Em__137_m822119827_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method);
#define TableBlock_2_U3CGetSizeU3Em__137_m822119827(__this /* static, unused */, ___prefab0, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TableBlock_2_U3CGetSizeU3Em__137_m822119827_gshared)(__this /* static, unused */, ___prefab0, method)
// System.Boolean TableBlock`2<System.Object,System.Object>::<GetSize>m__138(TPrefab)
extern "C"  bool TableBlock_2_U3CGetSizeU3Em__138_m2967880650_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method);
#define TableBlock_2_U3CGetSizeU3Em__138_m2967880650(__this /* static, unused */, ___prefab0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TableBlock_2_U3CGetSizeU3Em__138_m2967880650_gshared)(__this /* static, unused */, ___prefab0, method)
// System.Boolean TableBlock`2<System.Object,System.Object>::<TryShowCells>m__139(TPrefab)
extern "C"  bool TableBlock_2_U3CTryShowCellsU3Em__139_m1794268649_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method);
#define TableBlock_2_U3CTryShowCellsU3Em__139_m1794268649(__this /* static, unused */, ___prefab0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TableBlock_2_U3CTryShowCellsU3Em__139_m1794268649_gshared)(__this /* static, unused */, ___prefab0, method)
// System.Boolean TableBlock`2<System.Object,System.Object>::<Fill>m__13A(TPrefab)
extern "C"  bool TableBlock_2_U3CFillU3Em__13A_m3090221195_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method);
#define TableBlock_2_U3CFillU3Em__13A_m3090221195(__this /* static, unused */, ___prefab0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TableBlock_2_U3CFillU3Em__13A_m3090221195_gshared)(__this /* static, unused */, ___prefab0, method)
// System.Boolean TableBlock`2<System.Object,System.Object>::<Fill>m__13B(TPrefab)
extern "C"  bool TableBlock_2_U3CFillU3Em__13B_m2893707690_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method);
#define TableBlock_2_U3CFillU3Em__13B_m2893707690(__this /* static, unused */, ___prefab0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TableBlock_2_U3CFillU3Em__13B_m2893707690_gshared)(__this /* static, unused */, ___prefab0, method)
