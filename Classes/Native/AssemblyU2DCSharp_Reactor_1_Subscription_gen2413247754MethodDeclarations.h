﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<System.DateTime>
struct Subscription_t2413247754;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<System.DateTime>::.ctor()
extern "C"  void Subscription__ctor_m1916254692_gshared (Subscription_t2413247754 * __this, const MethodInfo* method);
#define Subscription__ctor_m1916254692(__this, method) ((  void (*) (Subscription_t2413247754 *, const MethodInfo*))Subscription__ctor_m1916254692_gshared)(__this, method)
// System.Void Reactor`1/Subscription<System.DateTime>::Dispose()
extern "C"  void Subscription_Dispose_m3173039393_gshared (Subscription_t2413247754 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3173039393(__this, method) ((  void (*) (Subscription_t2413247754 *, const MethodInfo*))Subscription_Dispose_m3173039393_gshared)(__this, method)
