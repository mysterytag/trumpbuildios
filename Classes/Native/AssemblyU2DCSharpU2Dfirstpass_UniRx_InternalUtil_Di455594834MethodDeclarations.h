﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>
struct DisposedObserver_1_t455594834;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3295416528_gshared (DisposedObserver_1_t455594834 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m3295416528(__this, method) ((  void (*) (DisposedObserver_1_t455594834 *, const MethodInfo*))DisposedObserver_1__ctor_m3295416528_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2891568349_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m2891568349(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m2891568349_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m2468046938_gshared (DisposedObserver_1_t455594834 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m2468046938(__this, method) ((  void (*) (DisposedObserver_1_t455594834 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m2468046938_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m4093272711_gshared (DisposedObserver_1_t455594834 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m4093272711(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t455594834 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m4093272711_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkPlayer>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m40122232_gshared (DisposedObserver_1_t455594834 * __this, NetworkPlayer_t1281137372  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m40122232(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t455594834 *, NetworkPlayer_t1281137372 , const MethodInfo*))DisposedObserver_1_OnNext_m40122232_gshared)(__this, ___value0, method)
