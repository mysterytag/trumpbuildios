﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Action`1<OnePF.Inventory>
struct Action_1_t2779015537;
// System.Action`1<OnePF.Purchase>
struct Action_1_t308648278;
// System.Action`2<System.Int32,System.String>
struct Action_2_t1740334453;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenIABEventManager
struct  OpenIABEventManager_t528054099  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct OpenIABEventManager_t528054099_StaticFields
{
public:
	// System.Action OpenIABEventManager::billingSupportedEvent
	Action_t437523947 * ___billingSupportedEvent_2;
	// System.Action`1<System.String> OpenIABEventManager::billingNotSupportedEvent
	Action_1_t1116941607 * ___billingNotSupportedEvent_3;
	// System.Action`1<OnePF.Inventory> OpenIABEventManager::queryInventorySucceededEvent
	Action_1_t2779015537 * ___queryInventorySucceededEvent_4;
	// System.Action`1<System.String> OpenIABEventManager::queryInventoryFailedEvent
	Action_1_t1116941607 * ___queryInventoryFailedEvent_5;
	// System.Action`1<OnePF.Purchase> OpenIABEventManager::purchaseSucceededEvent
	Action_1_t308648278 * ___purchaseSucceededEvent_6;
	// System.Action`2<System.Int32,System.String> OpenIABEventManager::purchaseFailedEvent
	Action_2_t1740334453 * ___purchaseFailedEvent_7;
	// System.Action`1<OnePF.Purchase> OpenIABEventManager::consumePurchaseSucceededEvent
	Action_1_t308648278 * ___consumePurchaseSucceededEvent_8;
	// System.Action`1<System.String> OpenIABEventManager::consumePurchaseFailedEvent
	Action_1_t1116941607 * ___consumePurchaseFailedEvent_9;
	// System.Action`1<System.String> OpenIABEventManager::transactionRestoredEvent
	Action_1_t1116941607 * ___transactionRestoredEvent_10;
	// System.Action`1<System.String> OpenIABEventManager::restoreFailedEvent
	Action_1_t1116941607 * ___restoreFailedEvent_11;
	// System.Action OpenIABEventManager::restoreSucceededEvent
	Action_t437523947 * ___restoreSucceededEvent_12;

public:
	inline static int32_t get_offset_of_billingSupportedEvent_2() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___billingSupportedEvent_2)); }
	inline Action_t437523947 * get_billingSupportedEvent_2() const { return ___billingSupportedEvent_2; }
	inline Action_t437523947 ** get_address_of_billingSupportedEvent_2() { return &___billingSupportedEvent_2; }
	inline void set_billingSupportedEvent_2(Action_t437523947 * value)
	{
		___billingSupportedEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___billingSupportedEvent_2, value);
	}

	inline static int32_t get_offset_of_billingNotSupportedEvent_3() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___billingNotSupportedEvent_3)); }
	inline Action_1_t1116941607 * get_billingNotSupportedEvent_3() const { return ___billingNotSupportedEvent_3; }
	inline Action_1_t1116941607 ** get_address_of_billingNotSupportedEvent_3() { return &___billingNotSupportedEvent_3; }
	inline void set_billingNotSupportedEvent_3(Action_1_t1116941607 * value)
	{
		___billingNotSupportedEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___billingNotSupportedEvent_3, value);
	}

	inline static int32_t get_offset_of_queryInventorySucceededEvent_4() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___queryInventorySucceededEvent_4)); }
	inline Action_1_t2779015537 * get_queryInventorySucceededEvent_4() const { return ___queryInventorySucceededEvent_4; }
	inline Action_1_t2779015537 ** get_address_of_queryInventorySucceededEvent_4() { return &___queryInventorySucceededEvent_4; }
	inline void set_queryInventorySucceededEvent_4(Action_1_t2779015537 * value)
	{
		___queryInventorySucceededEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___queryInventorySucceededEvent_4, value);
	}

	inline static int32_t get_offset_of_queryInventoryFailedEvent_5() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___queryInventoryFailedEvent_5)); }
	inline Action_1_t1116941607 * get_queryInventoryFailedEvent_5() const { return ___queryInventoryFailedEvent_5; }
	inline Action_1_t1116941607 ** get_address_of_queryInventoryFailedEvent_5() { return &___queryInventoryFailedEvent_5; }
	inline void set_queryInventoryFailedEvent_5(Action_1_t1116941607 * value)
	{
		___queryInventoryFailedEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___queryInventoryFailedEvent_5, value);
	}

	inline static int32_t get_offset_of_purchaseSucceededEvent_6() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___purchaseSucceededEvent_6)); }
	inline Action_1_t308648278 * get_purchaseSucceededEvent_6() const { return ___purchaseSucceededEvent_6; }
	inline Action_1_t308648278 ** get_address_of_purchaseSucceededEvent_6() { return &___purchaseSucceededEvent_6; }
	inline void set_purchaseSucceededEvent_6(Action_1_t308648278 * value)
	{
		___purchaseSucceededEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseSucceededEvent_6, value);
	}

	inline static int32_t get_offset_of_purchaseFailedEvent_7() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___purchaseFailedEvent_7)); }
	inline Action_2_t1740334453 * get_purchaseFailedEvent_7() const { return ___purchaseFailedEvent_7; }
	inline Action_2_t1740334453 ** get_address_of_purchaseFailedEvent_7() { return &___purchaseFailedEvent_7; }
	inline void set_purchaseFailedEvent_7(Action_2_t1740334453 * value)
	{
		___purchaseFailedEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseFailedEvent_7, value);
	}

	inline static int32_t get_offset_of_consumePurchaseSucceededEvent_8() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___consumePurchaseSucceededEvent_8)); }
	inline Action_1_t308648278 * get_consumePurchaseSucceededEvent_8() const { return ___consumePurchaseSucceededEvent_8; }
	inline Action_1_t308648278 ** get_address_of_consumePurchaseSucceededEvent_8() { return &___consumePurchaseSucceededEvent_8; }
	inline void set_consumePurchaseSucceededEvent_8(Action_1_t308648278 * value)
	{
		___consumePurchaseSucceededEvent_8 = value;
		Il2CppCodeGenWriteBarrier(&___consumePurchaseSucceededEvent_8, value);
	}

	inline static int32_t get_offset_of_consumePurchaseFailedEvent_9() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___consumePurchaseFailedEvent_9)); }
	inline Action_1_t1116941607 * get_consumePurchaseFailedEvent_9() const { return ___consumePurchaseFailedEvent_9; }
	inline Action_1_t1116941607 ** get_address_of_consumePurchaseFailedEvent_9() { return &___consumePurchaseFailedEvent_9; }
	inline void set_consumePurchaseFailedEvent_9(Action_1_t1116941607 * value)
	{
		___consumePurchaseFailedEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___consumePurchaseFailedEvent_9, value);
	}

	inline static int32_t get_offset_of_transactionRestoredEvent_10() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___transactionRestoredEvent_10)); }
	inline Action_1_t1116941607 * get_transactionRestoredEvent_10() const { return ___transactionRestoredEvent_10; }
	inline Action_1_t1116941607 ** get_address_of_transactionRestoredEvent_10() { return &___transactionRestoredEvent_10; }
	inline void set_transactionRestoredEvent_10(Action_1_t1116941607 * value)
	{
		___transactionRestoredEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___transactionRestoredEvent_10, value);
	}

	inline static int32_t get_offset_of_restoreFailedEvent_11() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___restoreFailedEvent_11)); }
	inline Action_1_t1116941607 * get_restoreFailedEvent_11() const { return ___restoreFailedEvent_11; }
	inline Action_1_t1116941607 ** get_address_of_restoreFailedEvent_11() { return &___restoreFailedEvent_11; }
	inline void set_restoreFailedEvent_11(Action_1_t1116941607 * value)
	{
		___restoreFailedEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___restoreFailedEvent_11, value);
	}

	inline static int32_t get_offset_of_restoreSucceededEvent_12() { return static_cast<int32_t>(offsetof(OpenIABEventManager_t528054099_StaticFields, ___restoreSucceededEvent_12)); }
	inline Action_t437523947 * get_restoreSucceededEvent_12() const { return ___restoreSucceededEvent_12; }
	inline Action_t437523947 ** get_address_of_restoreSucceededEvent_12() { return &___restoreSucceededEvent_12; }
	inline void set_restoreSucceededEvent_12(Action_t437523947 * value)
	{
		___restoreSucceededEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___restoreSucceededEvent_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
