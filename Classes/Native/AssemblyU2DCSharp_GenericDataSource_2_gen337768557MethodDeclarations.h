﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GenericDataSource_2_gen1679676449MethodDeclarations.h"

// System.Void GenericDataSource`2<SettingsButton,SettingsCell>::.ctor()
#define GenericDataSource_2__ctor_m1965529451(__this, method) ((  void (*) (GenericDataSource_2_t337768557 *, const MethodInfo*))GenericDataSource_2__ctor_m1409035199_gshared)(__this, method)
// System.Void GenericDataSource`2<SettingsButton,SettingsCell>::set_cellPrefab(UnityEngine.GameObject)
#define GenericDataSource_2_set_cellPrefab_m3859979156(__this, ___value0, method) ((  void (*) (GenericDataSource_2_t337768557 *, GameObject_t4012695102 *, const MethodInfo*))GenericDataSource_2_set_cellPrefab_m1627438784_gshared)(__this, ___value0, method)
// System.Int32 GenericDataSource`2<SettingsButton,SettingsCell>::GetNumberOfRowsForTableView(Tacticsoft.TableView)
#define GenericDataSource_2_GetNumberOfRowsForTableView_m1515835309(__this, ___tableView0, method) ((  int32_t (*) (GenericDataSource_2_t337768557 *, TableView_t692333993 *, const MethodInfo*))GenericDataSource_2_GetNumberOfRowsForTableView_m4207950081_gshared)(__this, ___tableView0, method)
// System.Single GenericDataSource`2<SettingsButton,SettingsCell>::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
#define GenericDataSource_2_GetHeightForRowInTableView_m23053369(__this, ___tableView0, ___row1, method) ((  float (*) (GenericDataSource_2_t337768557 *, TableView_t692333993 *, int32_t, const MethodInfo*))GenericDataSource_2_GetHeightForRowInTableView_m2709442445_gshared)(__this, ___tableView0, ___row1, method)
// Tacticsoft.TableViewCell GenericDataSource`2<SettingsButton,SettingsCell>::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
#define GenericDataSource_2_GetCellForRowInTableView_m1489740652(__this, ___tableView0, ___row1, method) ((  TableViewCell_t776419755 * (*) (GenericDataSource_2_t337768557 *, TableView_t692333993 *, int32_t, const MethodInfo*))GenericDataSource_2_GetCellForRowInTableView_m102593472_gshared)(__this, ___tableView0, ___row1, method)
