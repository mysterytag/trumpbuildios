﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct Action_1_t1799351807;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen1650899102.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3514679329_gshared (Action_1_t1799351807 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m3514679329(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t1799351807 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m3514679329_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m1936238851_gshared (Action_1_t1799351807 * __this, Tuple_3_t1650899102  ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m1936238851(__this, ___obj0, method) ((  void (*) (Action_1_t1799351807 *, Tuple_3_t1650899102 , const MethodInfo*))Action_1_Invoke_m1936238851_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3306910040_gshared (Action_1_t1799351807 * __this, Tuple_3_t1650899102  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m3306910040(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t1799351807 *, Tuple_3_t1650899102 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m3306910040_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1479568049_gshared (Action_1_t1799351807 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m1479568049(__this, ___result0, method) ((  void (*) (Action_1_t1799351807 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m1479568049_gshared)(__this, ___result0, method)
