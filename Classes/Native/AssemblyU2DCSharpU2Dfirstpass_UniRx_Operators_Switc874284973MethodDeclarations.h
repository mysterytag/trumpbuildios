﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>
struct Switch_t874284973;
// UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>
struct SwitchObserver_t321759539;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>::.ctor(UniRx.Operators.SwitchObservable`1/SwitchObserver<T>,System.UInt64)
extern "C"  void Switch__ctor_m1572081325_gshared (Switch_t874284973 * __this, SwitchObserver_t321759539 * ___observer0, uint64_t ___id1, const MethodInfo* method);
#define Switch__ctor_m1572081325(__this, ___observer0, ___id1, method) ((  void (*) (Switch_t874284973 *, SwitchObserver_t321759539 *, uint64_t, const MethodInfo*))Switch__ctor_m1572081325_gshared)(__this, ___observer0, ___id1, method)
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>::OnNext(T)
extern "C"  void Switch_OnNext_m237795148_gshared (Switch_t874284973 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Switch_OnNext_m237795148(__this, ___value0, method) ((  void (*) (Switch_t874284973 *, Il2CppObject *, const MethodInfo*))Switch_OnNext_m237795148_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>::OnError(System.Exception)
extern "C"  void Switch_OnError_m168852059_gshared (Switch_t874284973 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Switch_OnError_m168852059(__this, ___error0, method) ((  void (*) (Switch_t874284973 *, Exception_t1967233988 *, const MethodInfo*))Switch_OnError_m168852059_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>::OnCompleted()
extern "C"  void Switch_OnCompleted_m4267154990_gshared (Switch_t874284973 * __this, const MethodInfo* method);
#define Switch_OnCompleted_m4267154990(__this, method) ((  void (*) (Switch_t874284973 *, const MethodInfo*))Switch_OnCompleted_m4267154990_gshared)(__this, method)
