﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// UniRx.Operators.RangeObservable
struct RangeObservable_t282483469;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64
struct  U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64::i
	int32_t ___i_0;
	// UniRx.IObserver`1<System.Int32> UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64::observer
	Il2CppObject* ___observer_1;
	// UniRx.Operators.RangeObservable UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64::<>f__this
	RangeObservable_t282483469 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_observer_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636, ___observer_1)); }
	inline Il2CppObject* get_observer_1() const { return ___observer_1; }
	inline Il2CppObject** get_address_of_observer_1() { return &___observer_1; }
	inline void set_observer_1(Il2CppObject* value)
	{
		___observer_1 = value;
		Il2CppCodeGenWriteBarrier(&___observer_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636, ___U3CU3Ef__this_2)); }
	inline RangeObservable_t282483469 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline RangeObservable_t282483469 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(RangeObservable_t282483469 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
