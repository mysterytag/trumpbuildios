﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.GlobalProxySelection/EmptyWebProxy
struct EmptyWebProxy_t1372946375;
// System.Net.ICredentials
struct ICredentials_t2307785309;
// System.Uri
struct Uri_t2776692961;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri2776692961.h"

// System.Void System.Net.GlobalProxySelection/EmptyWebProxy::.ctor()
extern "C"  void EmptyWebProxy__ctor_m2834600705 (EmptyWebProxy_t1372946375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.GlobalProxySelection/EmptyWebProxy::get_Credentials()
extern "C"  Il2CppObject * EmptyWebProxy_get_Credentials_m1919834338 (EmptyWebProxy_t1372946375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.GlobalProxySelection/EmptyWebProxy::set_Credentials(System.Net.ICredentials)
extern "C"  void EmptyWebProxy_set_Credentials_m2229357073 (EmptyWebProxy_t1372946375 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.GlobalProxySelection/EmptyWebProxy::GetProxy(System.Uri)
extern "C"  Uri_t2776692961 * EmptyWebProxy_GetProxy_m1380759100 (EmptyWebProxy_t1372946375 * __this, Uri_t2776692961 * ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.GlobalProxySelection/EmptyWebProxy::IsBypassed(System.Uri)
extern "C"  bool EmptyWebProxy_IsBypassed_m2224348945 (EmptyWebProxy_t1372946375 * __this, Uri_t2776692961 * ___host0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
