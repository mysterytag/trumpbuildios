﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>
struct CreateObservable_1_t434305878;
// System.Func`2<UniRx.IObserver`1<UnityEngine.Vector2>,System.IDisposable>
struct Func_2_t1054655943;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m1286684063_gshared (CreateObservable_1_t434305878 * __this, Func_2_t1054655943 * ___subscribe0, const MethodInfo* method);
#define CreateObservable_1__ctor_m1286684063(__this, ___subscribe0, method) ((  void (*) (CreateObservable_1_t434305878 *, Func_2_t1054655943 *, const MethodInfo*))CreateObservable_1__ctor_m1286684063_gshared)(__this, ___subscribe0, method)
// System.Void UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m2179351550_gshared (CreateObservable_1_t434305878 * __this, Func_2_t1054655943 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define CreateObservable_1__ctor_m2179351550(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (CreateObservable_1_t434305878 *, Func_2_t1054655943 *, bool, const MethodInfo*))CreateObservable_1__ctor_m2179351550_gshared)(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.IDisposable UniRx.Operators.CreateObservable`1<UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m21051500_gshared (CreateObservable_1_t434305878 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CreateObservable_1_SubscribeCore_m21051500(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CreateObservable_1_t434305878 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CreateObservable_1_SubscribeCore_m21051500_gshared)(__this, ___observer0, ___cancel1, method)
