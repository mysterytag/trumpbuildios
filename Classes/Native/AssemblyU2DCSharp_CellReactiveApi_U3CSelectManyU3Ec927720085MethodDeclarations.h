﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<SelectMany>c__AnonStorey118`2<System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey118_2_t927720085;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<SelectMany>c__AnonStorey118`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey118_2__ctor_m1010833179_gshared (U3CSelectManyU3Ec__AnonStorey118_2_t927720085 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey118_2__ctor_m1010833179(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey118_2_t927720085 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey118_2__ctor_m1010833179_gshared)(__this, method)
// ICell`1<TR> CellReactiveApi/<SelectMany>c__AnonStorey118`2<System.Object,System.Object>::<>m__18D(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey118_2_U3CU3Em__18D_m3944559670_gshared (U3CSelectManyU3Ec__AnonStorey118_2_t927720085 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey118_2_U3CU3Em__18D_m3944559670(__this, ____0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey118_2_t927720085 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey118_2_U3CU3Em__18D_m3944559670_gshared)(__this, ____0, method)
