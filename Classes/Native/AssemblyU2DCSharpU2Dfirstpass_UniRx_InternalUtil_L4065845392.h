﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>
struct ImmutableList_1_t2748463827;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ListObserver`1<UniRx.CountChangedStatus>
struct  ListObserver_1_t4065845392  : public Il2CppObject
{
public:
	// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>> UniRx.InternalUtil.ListObserver`1::_observers
	ImmutableList_1_t2748463827 * ____observers_0;

public:
	inline static int32_t get_offset_of__observers_0() { return static_cast<int32_t>(offsetof(ListObserver_1_t4065845392, ____observers_0)); }
	inline ImmutableList_1_t2748463827 * get__observers_0() const { return ____observers_0; }
	inline ImmutableList_1_t2748463827 ** get_address_of__observers_0() { return &____observers_0; }
	inline void set__observers_0(ImmutableList_1_t2748463827 * value)
	{
		____observers_0 = value;
		Il2CppCodeGenWriteBarrier(&____observers_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
