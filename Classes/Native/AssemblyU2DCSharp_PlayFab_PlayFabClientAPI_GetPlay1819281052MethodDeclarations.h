﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsRequestCallback
struct GetPlayFabIDsFromKongregateIDsRequestCallback_t1819281052;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest
struct GetPlayFabIDsFromKongregateIDsRequest_t4157912391;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI4157912391.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromKongregateIDsRequestCallback__ctor_m709773611 (GetPlayFabIDsFromKongregateIDsRequestCallback_t1819281052 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest,System.Object)
extern "C"  void GetPlayFabIDsFromKongregateIDsRequestCallback_Invoke_m2607688097 (GetPlayFabIDsFromKongregateIDsRequestCallback_t1819281052 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromKongregateIDsRequestCallback_t1819281052(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromKongregateIDsRequestCallback_BeginInvoke_m3744107102 (GetPlayFabIDsFromKongregateIDsRequestCallback_t1819281052 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromKongregateIDsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromKongregateIDsRequestCallback_EndInvoke_m787679419 (GetPlayFabIDsFromKongregateIDsRequestCallback_t1819281052 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
