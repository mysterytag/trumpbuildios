﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AsObservableObservable`1<UniRx.Unit>
struct  AsObservableObservable_1_t1943012577  : public OperatorObservableBase_1_t1622431009
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.AsObservableObservable`1::source
	Il2CppObject* ___source_1;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(AsObservableObservable_1_t1943012577, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
