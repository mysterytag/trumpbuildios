﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.CombineLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_5_t1731983148;
// System.Object
struct Il2CppObject;
// UniRx.Operators.CombineLatestObserver`1<System.Object>
struct CombineLatestObserver_1_t123567747;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthC2199482330.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  CombineLatest_t928221951  : public NthCombineLatestObserverBase_1_t2199482330
{
public:
	// UniRx.Operators.CombineLatestObservable`5<T1,T2,T3,T4,TR> UniRx.Operators.CombineLatestObservable`5/CombineLatest::parent
	CombineLatestObservable_5_t1731983148 * ___parent_6;
	// System.Object UniRx.Operators.CombineLatestObservable`5/CombineLatest::gate
	Il2CppObject * ___gate_7;
	// UniRx.Operators.CombineLatestObserver`1<T1> UniRx.Operators.CombineLatestObservable`5/CombineLatest::c1
	CombineLatestObserver_1_t123567747 * ___c1_8;
	// UniRx.Operators.CombineLatestObserver`1<T2> UniRx.Operators.CombineLatestObservable`5/CombineLatest::c2
	CombineLatestObserver_1_t123567747 * ___c2_9;
	// UniRx.Operators.CombineLatestObserver`1<T3> UniRx.Operators.CombineLatestObservable`5/CombineLatest::c3
	CombineLatestObserver_1_t123567747 * ___c3_10;
	// UniRx.Operators.CombineLatestObserver`1<T4> UniRx.Operators.CombineLatestObservable`5/CombineLatest::c4
	CombineLatestObserver_1_t123567747 * ___c4_11;

public:
	inline static int32_t get_offset_of_parent_6() { return static_cast<int32_t>(offsetof(CombineLatest_t928221951, ___parent_6)); }
	inline CombineLatestObservable_5_t1731983148 * get_parent_6() const { return ___parent_6; }
	inline CombineLatestObservable_5_t1731983148 ** get_address_of_parent_6() { return &___parent_6; }
	inline void set_parent_6(CombineLatestObservable_5_t1731983148 * value)
	{
		___parent_6 = value;
		Il2CppCodeGenWriteBarrier(&___parent_6, value);
	}

	inline static int32_t get_offset_of_gate_7() { return static_cast<int32_t>(offsetof(CombineLatest_t928221951, ___gate_7)); }
	inline Il2CppObject * get_gate_7() const { return ___gate_7; }
	inline Il2CppObject ** get_address_of_gate_7() { return &___gate_7; }
	inline void set_gate_7(Il2CppObject * value)
	{
		___gate_7 = value;
		Il2CppCodeGenWriteBarrier(&___gate_7, value);
	}

	inline static int32_t get_offset_of_c1_8() { return static_cast<int32_t>(offsetof(CombineLatest_t928221951, ___c1_8)); }
	inline CombineLatestObserver_1_t123567747 * get_c1_8() const { return ___c1_8; }
	inline CombineLatestObserver_1_t123567747 ** get_address_of_c1_8() { return &___c1_8; }
	inline void set_c1_8(CombineLatestObserver_1_t123567747 * value)
	{
		___c1_8 = value;
		Il2CppCodeGenWriteBarrier(&___c1_8, value);
	}

	inline static int32_t get_offset_of_c2_9() { return static_cast<int32_t>(offsetof(CombineLatest_t928221951, ___c2_9)); }
	inline CombineLatestObserver_1_t123567747 * get_c2_9() const { return ___c2_9; }
	inline CombineLatestObserver_1_t123567747 ** get_address_of_c2_9() { return &___c2_9; }
	inline void set_c2_9(CombineLatestObserver_1_t123567747 * value)
	{
		___c2_9 = value;
		Il2CppCodeGenWriteBarrier(&___c2_9, value);
	}

	inline static int32_t get_offset_of_c3_10() { return static_cast<int32_t>(offsetof(CombineLatest_t928221951, ___c3_10)); }
	inline CombineLatestObserver_1_t123567747 * get_c3_10() const { return ___c3_10; }
	inline CombineLatestObserver_1_t123567747 ** get_address_of_c3_10() { return &___c3_10; }
	inline void set_c3_10(CombineLatestObserver_1_t123567747 * value)
	{
		___c3_10 = value;
		Il2CppCodeGenWriteBarrier(&___c3_10, value);
	}

	inline static int32_t get_offset_of_c4_11() { return static_cast<int32_t>(offsetof(CombineLatest_t928221951, ___c4_11)); }
	inline CombineLatestObserver_1_t123567747 * get_c4_11() const { return ___c4_11; }
	inline CombineLatestObserver_1_t123567747 ** get_address_of_c4_11() { return &___c4_11; }
	inline void set_c4_11(CombineLatestObserver_1_t123567747 * value)
	{
		___c4_11 = value;
		Il2CppCodeGenWriteBarrier(&___c4_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
