﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct Subject_1_t965355615;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"

// System.Void UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void Subject_1__ctor_m1215605536_gshared (Subject_1_t965355615 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1215605536(__this, method) ((  void (*) (Subject_1_t965355615 *, const MethodInfo*))Subject_1__ctor_m1215605536_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m177815220_gshared (Subject_1_t965355615 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m177815220(__this, method) ((  bool (*) (Subject_1_t965355615 *, const MethodInfo*))Subject_1_get_HasObservers_m177815220_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m4173006506_gshared (Subject_1_t965355615 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m4173006506(__this, method) ((  void (*) (Subject_1_t965355615 *, const MethodInfo*))Subject_1_OnCompleted_m4173006506_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m1242499287_gshared (Subject_1_t965355615 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m1242499287(__this, ___error0, method) ((  void (*) (Subject_1_t965355615 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1242499287_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m2796518856_gshared (Subject_1_t965355615 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m2796518856(__this, ___value0, method) ((  void (*) (Subject_1_t965355615 *, DictionaryRemoveEvent_2_t3322288291 , const MethodInfo*))Subject_1_OnNext_m2796518856_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m1107542645_gshared (Subject_1_t965355615 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m1107542645(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t965355615 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m1107542645_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subject_1_Dispose_m4159065949_gshared (Subject_1_t965355615 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m4159065949(__this, method) ((  void (*) (Subject_1_t965355615 *, const MethodInfo*))Subject_1_Dispose_m4159065949_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m3156298726_gshared (Subject_1_t965355615 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m3156298726(__this, method) ((  void (*) (Subject_1_t965355615 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3156298726_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m2864637611_gshared (Subject_1_t965355615 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2864637611(__this, method) ((  bool (*) (Subject_1_t965355615 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m2864637611_gshared)(__this, method)
