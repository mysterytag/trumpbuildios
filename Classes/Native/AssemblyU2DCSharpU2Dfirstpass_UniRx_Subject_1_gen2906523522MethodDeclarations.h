﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<System.String>::.ctor()
#define Subject_1__ctor_m1820967222(__this, method) ((  void (*) (Subject_1_t2906523522 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.String>::get_HasObservers()
#define Subject_1_get_HasObservers_m314612322(__this, method) ((  bool (*) (Subject_1_t2906523522 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.String>::OnCompleted()
#define Subject_1_OnCompleted_m180861172(__this, method) ((  void (*) (Subject_1_t2906523522 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.String>::OnError(System.Exception)
#define Subject_1_OnError_m3061881889(__this, ___error0, method) ((  void (*) (Subject_1_t2906523522 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<System.String>::OnNext(T)
#define Subject_1_OnNext_m555001585(__this, ___value0, method) ((  void (*) (Subject_1_t2906523522 *, String_t*, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<System.String>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m775338217(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2906523522 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<System.String>::Dispose()
#define Subject_1_Dispose_m1693169319(__this, method) ((  void (*) (Subject_1_t2906523522 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.String>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m3096955184(__this, method) ((  void (*) (Subject_1_t2906523522 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.String>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m4055102169(__this, method) ((  bool (*) (Subject_1_t2906523522 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
