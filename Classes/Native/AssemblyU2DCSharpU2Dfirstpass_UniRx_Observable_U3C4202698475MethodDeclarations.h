﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<SelectMany>c__AnonStorey4E`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4E`1<System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey4E_1__ctor_m4119526441_gshared (U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey4E_1__ctor_m4119526441(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey4E_1__ctor_m4119526441_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4E`1<System.Object>::<>m__4C()
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m345691313_gshared (U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m345691313(__this, method) ((  Il2CppObject * (*) (U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey4E_1_U3CU3Em__4C_m345691313_gshared)(__this, method)
