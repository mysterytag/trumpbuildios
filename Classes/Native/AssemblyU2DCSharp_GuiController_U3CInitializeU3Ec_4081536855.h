﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GuiController/<Initialize>c__AnonStorey56
struct U3CInitializeU3Ec__AnonStorey56_t4081536854;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuiController/<Initialize>c__AnonStorey56/<Initialize>c__AnonStorey57
struct  U3CInitializeU3Ec__AnonStorey57_t4081536855  : public Il2CppObject
{
public:
	// System.Boolean GuiController/<Initialize>c__AnonStorey56/<Initialize>c__AnonStorey57::b
	bool ___b_0;
	// GuiController/<Initialize>c__AnonStorey56 GuiController/<Initialize>c__AnonStorey56/<Initialize>c__AnonStorey57::<>f__ref$86
	U3CInitializeU3Ec__AnonStorey56_t4081536854 * ___U3CU3Ef__refU2486_1;

public:
	inline static int32_t get_offset_of_b_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey57_t4081536855, ___b_0)); }
	inline bool get_b_0() const { return ___b_0; }
	inline bool* get_address_of_b_0() { return &___b_0; }
	inline void set_b_0(bool value)
	{
		___b_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2486_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey57_t4081536855, ___U3CU3Ef__refU2486_1)); }
	inline U3CInitializeU3Ec__AnonStorey56_t4081536854 * get_U3CU3Ef__refU2486_1() const { return ___U3CU3Ef__refU2486_1; }
	inline U3CInitializeU3Ec__AnonStorey56_t4081536854 ** get_address_of_U3CU3Ef__refU2486_1() { return &___U3CU3Ef__refU2486_1; }
	inline void set_U3CU3Ef__refU2486_1(U3CInitializeU3Ec__AnonStorey56_t4081536854 * value)
	{
		___U3CU3Ef__refU2486_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2486_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
