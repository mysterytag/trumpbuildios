﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SampleObservable`1<System.Object>
struct  SampleObservable_1_t2474383436  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.SampleObservable`1::source
	Il2CppObject* ___source_1;
	// System.TimeSpan UniRx.Operators.SampleObservable`1::interval
	TimeSpan_t763862892  ___interval_2;
	// UniRx.IScheduler UniRx.Operators.SampleObservable`1::scheduler
	Il2CppObject * ___scheduler_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(SampleObservable_1_t2474383436, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_interval_2() { return static_cast<int32_t>(offsetof(SampleObservable_1_t2474383436, ___interval_2)); }
	inline TimeSpan_t763862892  get_interval_2() const { return ___interval_2; }
	inline TimeSpan_t763862892 * get_address_of_interval_2() { return &___interval_2; }
	inline void set_interval_2(TimeSpan_t763862892  value)
	{
		___interval_2 = value;
	}

	inline static int32_t get_offset_of_scheduler_3() { return static_cast<int32_t>(offsetof(SampleObservable_1_t2474383436, ___scheduler_3)); }
	inline Il2CppObject * get_scheduler_3() const { return ___scheduler_3; }
	inline Il2CppObject ** get_address_of_scheduler_3() { return &___scheduler_3; }
	inline void set_scheduler_3(Il2CppObject * value)
	{
		___scheduler_3 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
