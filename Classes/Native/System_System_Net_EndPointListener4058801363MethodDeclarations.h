﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.EndPointListener
struct EndPointListener_t4058801363;
// System.Net.IPAddress
struct IPAddress_t3220500535;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.Net.HttpListenerContext
struct HttpListenerContext_t260537341;
// System.Net.HttpListener
struct HttpListener_t1747553510;
// System.String
struct String_t;
// System.Uri
struct Uri_t2776692961;
// System.Net.ListenerPrefix
struct ListenerPrefix_t3774801904;
// System.Collections.ArrayList
struct ArrayList_t2121638921;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress3220500535.h"
#include "System_System_Net_HttpListenerContext260537341.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Uri2776692961.h"
#include "System_System_Net_ListenerPrefix3774801904.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "System_System_Net_HttpListener1747553510.h"

// System.Void System.Net.EndPointListener::.ctor(System.Net.IPAddress,System.Int32,System.Boolean)
extern "C"  void EndPointListener__ctor_m2746959386 (EndPointListener_t4058801363 * __this, IPAddress_t3220500535 * ___addr0, int32_t ___port1, bool ___secure2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::LoadCertificateAndKey(System.Net.IPAddress,System.Int32)
extern "C"  void EndPointListener_LoadCertificateAndKey_m1954576844 (EndPointListener_t4058801363 * __this, IPAddress_t3220500535 * ___addr0, int32_t ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::OnAccept(System.IAsyncResult)
extern "C"  void EndPointListener_OnAccept_m2147704425 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.EndPointListener::BindContext(System.Net.HttpListenerContext)
extern "C"  bool EndPointListener_BindContext_m2300095668 (EndPointListener_t4058801363 * __this, HttpListenerContext_t260537341 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::UnbindContext(System.Net.HttpListenerContext)
extern "C"  void EndPointListener_UnbindContext_m289917541 (EndPointListener_t4058801363 * __this, HttpListenerContext_t260537341 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListener System.Net.EndPointListener::SearchListener(System.String,System.Uri,System.Net.ListenerPrefix&)
extern "C"  HttpListener_t1747553510 * EndPointListener_SearchListener_m1698328087 (EndPointListener_t4058801363 * __this, String_t* ___host0, Uri_t2776692961 * ___uri1, ListenerPrefix_t3774801904 ** ___prefix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListener System.Net.EndPointListener::MatchFromList(System.String,System.String,System.Collections.ArrayList,System.Net.ListenerPrefix&)
extern "C"  HttpListener_t1747553510 * EndPointListener_MatchFromList_m1951438410 (EndPointListener_t4058801363 * __this, String_t* ___host0, String_t* ___path1, ArrayList_t2121638921 * ___list2, ListenerPrefix_t3774801904 ** ___prefix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::AddSpecial(System.Collections.ArrayList,System.Net.ListenerPrefix)
extern "C"  void EndPointListener_AddSpecial_m237568674 (EndPointListener_t4058801363 * __this, ArrayList_t2121638921 * ___coll0, ListenerPrefix_t3774801904 * ___prefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::RemoveSpecial(System.Collections.ArrayList,System.Net.ListenerPrefix)
extern "C"  void EndPointListener_RemoveSpecial_m2123891899 (EndPointListener_t4058801363 * __this, ArrayList_t2121638921 * ___coll0, ListenerPrefix_t3774801904 * ___prefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::CheckIfRemove()
extern "C"  void EndPointListener_CheckIfRemove_m147993848 (EndPointListener_t4058801363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::Close()
extern "C"  void EndPointListener_Close_m4153892871 (EndPointListener_t4058801363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::AddPrefix(System.Net.ListenerPrefix,System.Net.HttpListener)
extern "C"  void EndPointListener_AddPrefix_m2268823002 (EndPointListener_t4058801363 * __this, ListenerPrefix_t3774801904 * ___prefix0, HttpListener_t1747553510 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointListener::RemovePrefix(System.Net.ListenerPrefix,System.Net.HttpListener)
extern "C"  void EndPointListener_RemovePrefix_m792217267 (EndPointListener_t4058801363 * __this, ListenerPrefix_t3774801904 * ___prefix0, HttpListener_t1747553510 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
