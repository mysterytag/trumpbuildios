﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>
struct ThrottleFirstFrame_t517244520;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>
struct  ThrottleFirstFrameTick_t2464530589  : public Il2CppObject
{
public:
	// UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<T> UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick::parent
	ThrottleFirstFrame_t517244520 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(ThrottleFirstFrameTick_t2464530589, ___parent_0)); }
	inline ThrottleFirstFrame_t517244520 * get_parent_0() const { return ___parent_0; }
	inline ThrottleFirstFrame_t517244520 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ThrottleFirstFrame_t517244520 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
