﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ZergRush_InorganicCollection_1_g3450977518MethodDeclarations.h"

// System.Void ZergRush.InorganicCollection`1<DonateButton>::.ctor()
#define InorganicCollection_1__ctor_m2633626747(__this, method) ((  void (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1__ctor_m2562722286_gshared)(__this, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define InorganicCollection_1__ctor_m665983268(__this, ___collection0, method) ((  void (*) (InorganicCollection_1_t3284624539 *, Il2CppObject*, const MethodInfo*))InorganicCollection_1__ctor_m3238279633_gshared)(__this, ___collection0, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::.ctor(System.Collections.Generic.List`1<T>)
#define InorganicCollection_1__ctor_m3239074643(__this, ___list0, method) ((  void (*) (InorganicCollection_1_t3284624539 *, List_1_t1467712410 *, const MethodInfo*))InorganicCollection_1__ctor_m1788080966_gshared)(__this, ___list0, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::ClearItems()
#define InorganicCollection_1_ClearItems_m2415880220(__this, method) ((  void (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_ClearItems_m2583413385_gshared)(__this, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::InsertItem(System.Int32,T)
#define InorganicCollection_1_InsertItem_m86455230(__this, ___index0, ___item1, method) ((  void (*) (InorganicCollection_1_t3284624539 *, int32_t, DonateButton_t670753441 *, const MethodInfo*))InorganicCollection_1_InsertItem_m1273645931_gshared)(__this, ___index0, ___item1, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::Move(System.Int32,System.Int32)
#define InorganicCollection_1_Move_m3127850860(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (InorganicCollection_1_t3284624539 *, int32_t, int32_t, const MethodInfo*))InorganicCollection_1_Move_m2620974047_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::MoveItem(System.Int32,System.Int32)
#define InorganicCollection_1_MoveItem_m382596313(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (InorganicCollection_1_t3284624539 *, int32_t, int32_t, const MethodInfo*))InorganicCollection_1_MoveItem_m1781936076_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::RemoveItem(System.Int32)
#define InorganicCollection_1_RemoveItem_m1027008689(__this, ___index0, method) ((  void (*) (InorganicCollection_1_t3284624539 *, int32_t, const MethodInfo*))InorganicCollection_1_RemoveItem_m3410364318_gshared)(__this, ___index0, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::RemoveItem(System.Func`2<T,System.Boolean>)
#define InorganicCollection_1_RemoveItem_m3817940476(__this, ___filter0, method) ((  void (*) (InorganicCollection_1_t3284624539 *, Func_2_t1032836960 *, const MethodInfo*))InorganicCollection_1_RemoveItem_m2366946799_gshared)(__this, ___filter0, method)
// System.Void ZergRush.InorganicCollection`1<DonateButton>::SetItem(System.Int32,T)
#define InorganicCollection_1_SetItem_m3296423703(__this, ___index0, ___item1, method) ((  void (*) (InorganicCollection_1_t3284624539 *, int32_t, DonateButton_t670753441 *, const MethodInfo*))InorganicCollection_1_SetItem_m2680569482_gshared)(__this, ___index0, ___item1, method)
// IStream`1<System.Int32> ZergRush.InorganicCollection`1<DonateButton>::ObserveCountChanged()
#define InorganicCollection_1_ObserveCountChanged_m3020150100(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_ObserveCountChanged_m3361259667_gshared)(__this, method)
// IStream`1<UniRx.Unit> ZergRush.InorganicCollection`1<DonateButton>::ObserveReset()
#define InorganicCollection_1_ObserveReset_m3709869359(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_ObserveReset_m884902102_gshared)(__this, method)
// IStream`1<UniRx.CollectionAddEvent`1<T>> ZergRush.InorganicCollection`1<DonateButton>::ObserveAdd()
#define InorganicCollection_1_ObserveAdd_m872640145(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_ObserveAdd_m500474968_gshared)(__this, method)
// IStream`1<CollectionMoveEvent`1<T>> ZergRush.InorganicCollection`1<DonateButton>::ObserveMove()
#define InorganicCollection_1_ObserveMove_m3154359301(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_ObserveMove_m2797955546_gshared)(__this, method)
// IStream`1<CollectionRemoveEvent`1<T>> ZergRush.InorganicCollection`1<DonateButton>::ObserveRemove()
#define InorganicCollection_1_ObserveRemove_m2001514149(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_ObserveRemove_m3795636736_gshared)(__this, method)
// IStream`1<CollectionReplaceEvent`1<T>> ZergRush.InorganicCollection`1<DonateButton>::ObserveReplace()
#define InorganicCollection_1_ObserveReplace_m697322799(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_ObserveReplace_m3125593920_gshared)(__this, method)
// System.IDisposable ZergRush.InorganicCollection`1<DonateButton>::OnChanged(System.Action,Priority)
#define InorganicCollection_1_OnChanged_m1598978144(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3284624539 *, Action_t437523947 *, int32_t, const MethodInfo*))InorganicCollection_1_OnChanged_m1222232857_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable ZergRush.InorganicCollection`1<DonateButton>::Bind(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
#define InorganicCollection_1_Bind_m3959790112(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3284624539 *, Action_1_t1285037532 *, int32_t, const MethodInfo*))InorganicCollection_1_Bind_m1502836121_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable ZergRush.InorganicCollection`1<DonateButton>::ListenUpdates(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
#define InorganicCollection_1_ListenUpdates_m2568002800(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3284624539 *, Action_1_t1285037532 *, int32_t, const MethodInfo*))InorganicCollection_1_ListenUpdates_m1688181143_gshared)(__this, ___reaction0, ___p1, method)
// System.Collections.Generic.ICollection`1<T> ZergRush.InorganicCollection`1<DonateButton>::get_value()
#define InorganicCollection_1_get_value_m1999292747(__this, method) ((  Il2CppObject* (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_get_value_m3124552170_gshared)(__this, method)
// System.Object ZergRush.InorganicCollection`1<DonateButton>::get_valueObject()
#define InorganicCollection_1_get_valueObject_m267531083(__this, method) ((  Il2CppObject * (*) (InorganicCollection_1_t3284624539 *, const MethodInfo*))InorganicCollection_1_get_valueObject_m2207754504_gshared)(__this, method)
