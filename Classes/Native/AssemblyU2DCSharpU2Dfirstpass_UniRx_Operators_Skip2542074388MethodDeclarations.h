﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipWhileObservable`1<System.Object>
struct SkipWhileObservable_1_t2542074388;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Func`3<System.Object,System.Int32,System.Boolean>
struct Func_3_t3064567499;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SkipWhileObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void SkipWhileObservable_1__ctor_m3244455415_gshared (SkipWhileObservable_1_t2542074388 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, const MethodInfo* method);
#define SkipWhileObservable_1__ctor_m3244455415(__this, ___source0, ___predicate1, method) ((  void (*) (SkipWhileObservable_1_t2542074388 *, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))SkipWhileObservable_1__ctor_m3244455415_gshared)(__this, ___source0, ___predicate1, method)
// System.Void UniRx.Operators.SkipWhileObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void SkipWhileObservable_1__ctor_m3831277199_gshared (SkipWhileObservable_1_t2542074388 * __this, Il2CppObject* ___source0, Func_3_t3064567499 * ___predicateWithIndex1, const MethodInfo* method);
#define SkipWhileObservable_1__ctor_m3831277199(__this, ___source0, ___predicateWithIndex1, method) ((  void (*) (SkipWhileObservable_1_t2542074388 *, Il2CppObject*, Func_3_t3064567499 *, const MethodInfo*))SkipWhileObservable_1__ctor_m3831277199_gshared)(__this, ___source0, ___predicateWithIndex1, method)
// System.IDisposable UniRx.Operators.SkipWhileObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SkipWhileObservable_1_SubscribeCore_m1795511260_gshared (SkipWhileObservable_1_t2542074388 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SkipWhileObservable_1_SubscribeCore_m1795511260(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SkipWhileObservable_1_t2542074388 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SkipWhileObservable_1_SubscribeCore_m1795511260_gshared)(__this, ___observer0, ___cancel1, method)
