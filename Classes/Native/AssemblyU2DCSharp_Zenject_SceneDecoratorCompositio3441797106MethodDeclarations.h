﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SceneDecoratorCompositionRoot
struct SceneDecoratorCompositionRoot_t3441797106;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"

// System.Void Zenject.SceneDecoratorCompositionRoot::.ctor()
extern "C"  void SceneDecoratorCompositionRoot__ctor_m2449093741 (SceneDecoratorCompositionRoot_t3441797106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneDecoratorCompositionRoot::Awake()
extern "C"  void SceneDecoratorCompositionRoot_Awake_m2686698960 (SceneDecoratorCompositionRoot_t3441797106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.SceneDecoratorCompositionRoot::Init()
extern "C"  Il2CppObject * SceneDecoratorCompositionRoot_Init_m2768056063 (SceneDecoratorCompositionRoot_t3441797106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneDecoratorCompositionRoot::AddPreBindings(Zenject.DiContainer)
extern "C"  void SceneDecoratorCompositionRoot_AddPreBindings_m2636596910 (SceneDecoratorCompositionRoot_t3441797106 * __this, DiContainer_t2383114449 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneDecoratorCompositionRoot::AddPostBindings(Zenject.DiContainer)
extern "C"  void SceneDecoratorCompositionRoot_AddPostBindings_m1020293787 (SceneDecoratorCompositionRoot_t3441797106 * __this, DiContainer_t2383114449 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneDecoratorCompositionRoot::ProcessDecoratorInstallers(Zenject.DiContainer,System.Boolean)
extern "C"  void SceneDecoratorCompositionRoot_ProcessDecoratorInstallers_m1243265334 (SceneDecoratorCompositionRoot_t3441797106 * __this, DiContainer_t2383114449 * ___container0, bool ___isBefore1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
