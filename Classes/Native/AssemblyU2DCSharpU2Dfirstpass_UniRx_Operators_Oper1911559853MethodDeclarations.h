﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<System.Int64>
struct OperatorObservableBase_1_t1911559853;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<System.Int64>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m322881330_gshared (OperatorObservableBase_1_t1911559853 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m322881330(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t1911559853 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m322881330_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Int64>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3602103400_gshared (OperatorObservableBase_1_t1911559853 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3602103400(__this, method) ((  bool (*) (OperatorObservableBase_1_t1911559853 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3602103400_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1755692794_gshared (OperatorObservableBase_1_t1911559853 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m1755692794(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t1911559853 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m1755692794_gshared)(__this, ___observer0, method)
