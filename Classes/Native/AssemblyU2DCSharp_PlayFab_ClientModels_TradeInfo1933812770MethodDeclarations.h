﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1403402234.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"

// System.Void PlayFab.ClientModels.TradeInfo::.ctor()
extern "C"  void TradeInfo__ctor_m3494945351 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.TradeStatus> PlayFab.ClientModels.TradeInfo::get_Status()
extern "C"  Nullable_1_t1403402234  TradeInfo_get_Status_m3862206352 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_Status(System.Nullable`1<PlayFab.ClientModels.TradeStatus>)
extern "C"  void TradeInfo_set_Status_m2628834123 (TradeInfo_t1933812770 * __this, Nullable_1_t1403402234  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.TradeInfo::get_TradeId()
extern "C"  String_t* TradeInfo_get_TradeId_m4260404056 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_TradeId(System.String)
extern "C"  void TradeInfo_set_TradeId_m1421729723 (TradeInfo_t1933812770 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.TradeInfo::get_OfferingPlayerId()
extern "C"  String_t* TradeInfo_get_OfferingPlayerId_m1367589131 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_OfferingPlayerId(System.String)
extern "C"  void TradeInfo_set_OfferingPlayerId_m2533268966 (TradeInfo_t1933812770 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_OfferedInventoryInstanceIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_OfferedInventoryInstanceIds_m444301933 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_OfferedInventoryInstanceIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_OfferedInventoryInstanceIds_m1225186942 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_OfferedCatalogItemIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_OfferedCatalogItemIds_m3633738322 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_OfferedCatalogItemIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_OfferedCatalogItemIds_m62028323 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_RequestedCatalogItemIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_RequestedCatalogItemIds_m1249112389 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_RequestedCatalogItemIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_RequestedCatalogItemIds_m3028202454 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_AllowedPlayerIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_AllowedPlayerIds_m2558003302 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_AllowedPlayerIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_AllowedPlayerIds_m4212998877 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.TradeInfo::get_AcceptedPlayerId()
extern "C"  String_t* TradeInfo_get_AcceptedPlayerId_m1926167404 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_AcceptedPlayerId(System.String)
extern "C"  void TradeInfo_set_AcceptedPlayerId_m4232775973 (TradeInfo_t1933812770 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_AcceptedInventoryInstanceIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_AcceptedInventoryInstanceIds_m2428408197 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_AcceptedInventoryInstanceIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_AcceptedInventoryInstanceIds_m1664951676 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::get_OpenedAt()
extern "C"  Nullable_1_t3225071844  TradeInfo_get_OpenedAt_m2038590640 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_OpenedAt(System.Nullable`1<System.DateTime>)
extern "C"  void TradeInfo_set_OpenedAt_m153537767 (TradeInfo_t1933812770 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::get_FilledAt()
extern "C"  Nullable_1_t3225071844  TradeInfo_get_FilledAt_m1114254633 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_FilledAt(System.Nullable`1<System.DateTime>)
extern "C"  void TradeInfo_set_FilledAt_m76623648 (TradeInfo_t1933812770 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::get_CancelledAt()
extern "C"  Nullable_1_t3225071844  TradeInfo_get_CancelledAt_m1467109458 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_CancelledAt(System.Nullable`1<System.DateTime>)
extern "C"  void TradeInfo_set_CancelledAt_m3033380733 (TradeInfo_t1933812770 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::get_InvalidatedAt()
extern "C"  Nullable_1_t3225071844  TradeInfo_get_InvalidatedAt_m3136666186 (TradeInfo_t1933812770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TradeInfo::set_InvalidatedAt(System.Nullable`1<System.DateTime>)
extern "C"  void TradeInfo_set_InvalidatedAt_m194925813 (TradeInfo_t1933812770 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
