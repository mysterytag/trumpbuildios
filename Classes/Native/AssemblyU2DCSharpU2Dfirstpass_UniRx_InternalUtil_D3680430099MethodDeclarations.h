﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<System.Boolean>
struct DisposedObserver_1_t3680430099;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4030990875_gshared (DisposedObserver_1_t3680430099 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m4030990875(__this, method) ((  void (*) (DisposedObserver_1_t3680430099 *, const MethodInfo*))DisposedObserver_1__ctor_m4030990875_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m4219536626_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m4219536626(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m4219536626_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m2965896549_gshared (DisposedObserver_1_t3680430099 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m2965896549(__this, method) ((  void (*) (DisposedObserver_1_t3680430099 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m2965896549_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1913700114_gshared (DisposedObserver_1_t3680430099 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1913700114(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t3680430099 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1913700114_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Boolean>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2552433155_gshared (DisposedObserver_1_t3680430099 * __this, bool ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2552433155(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t3680430099 *, bool, const MethodInfo*))DisposedObserver_1_OnNext_m2552433155_gshared)(__this, ___value0, method)
