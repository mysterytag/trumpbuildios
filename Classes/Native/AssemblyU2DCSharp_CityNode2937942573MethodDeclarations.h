﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityNode
struct CityNode_t2937942573;

#include "codegen/il2cpp-codegen.h"

// System.Void CityNode::.ctor()
extern "C"  void CityNode__ctor_m2941021966 (CityNode_t2937942573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityNode::.cctor()
extern "C"  void CityNode__cctor_m495271519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityNode::Start()
extern "C"  void CityNode_Start_m1888159758 (CityNode_t2937942573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityNode::Update()
extern "C"  void CityNode_Update_m2704229823 (CityNode_t2937942573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityNode::SetCitySpeed(System.Single)
extern "C"  void CityNode_SetCitySpeed_m3754245979 (CityNode_t2937942573 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityNode::SetCityLevel(System.Int32)
extern "C"  void CityNode_SetCityLevel_m2739580446 (CityNode_t2937942573 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
