﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoneyPackInfo
struct MoneyPackInfo_t3013049127;

#include "codegen/il2cpp-codegen.h"

// System.Void MoneyPackInfo::.ctor()
extern "C"  void MoneyPackInfo__ctor_m3448603172 (MoneyPackInfo_t3013049127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
