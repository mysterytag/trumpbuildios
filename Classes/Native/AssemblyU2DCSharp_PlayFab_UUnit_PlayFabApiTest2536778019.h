﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PlayFab.ClientModels.UserDataRecord
struct UserDataRecord_t163839734;
// PlayFab.ClientModels.CharacterResult
struct CharacterResult_t274624662;

#include "AssemblyU2DCSharp_PlayFab_UUnit_UUnitTestCase14187365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.PlayFabApiTest
struct  PlayFabApiTest_t2536778019  : public UUnitTestCase_t14187365
{
public:
	// System.String PlayFab.UUnit.PlayFabApiTest::lastReceivedMessage
	String_t* ___lastReceivedMessage_15;
	// PlayFab.ClientModels.UserDataRecord PlayFab.UUnit.PlayFabApiTest::testCounterReturn
	UserDataRecord_t163839734 * ___testCounterReturn_16;
	// System.Int32 PlayFab.UUnit.PlayFabApiTest::testStatReturn
	int32_t ___testStatReturn_17;
	// PlayFab.ClientModels.CharacterResult PlayFab.UUnit.PlayFabApiTest::targetCharacter
	CharacterResult_t274624662 * ___targetCharacter_18;

public:
	inline static int32_t get_offset_of_lastReceivedMessage_15() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019, ___lastReceivedMessage_15)); }
	inline String_t* get_lastReceivedMessage_15() const { return ___lastReceivedMessage_15; }
	inline String_t** get_address_of_lastReceivedMessage_15() { return &___lastReceivedMessage_15; }
	inline void set_lastReceivedMessage_15(String_t* value)
	{
		___lastReceivedMessage_15 = value;
		Il2CppCodeGenWriteBarrier(&___lastReceivedMessage_15, value);
	}

	inline static int32_t get_offset_of_testCounterReturn_16() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019, ___testCounterReturn_16)); }
	inline UserDataRecord_t163839734 * get_testCounterReturn_16() const { return ___testCounterReturn_16; }
	inline UserDataRecord_t163839734 ** get_address_of_testCounterReturn_16() { return &___testCounterReturn_16; }
	inline void set_testCounterReturn_16(UserDataRecord_t163839734 * value)
	{
		___testCounterReturn_16 = value;
		Il2CppCodeGenWriteBarrier(&___testCounterReturn_16, value);
	}

	inline static int32_t get_offset_of_testStatReturn_17() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019, ___testStatReturn_17)); }
	inline int32_t get_testStatReturn_17() const { return ___testStatReturn_17; }
	inline int32_t* get_address_of_testStatReturn_17() { return &___testStatReturn_17; }
	inline void set_testStatReturn_17(int32_t value)
	{
		___testStatReturn_17 = value;
	}

	inline static int32_t get_offset_of_targetCharacter_18() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019, ___targetCharacter_18)); }
	inline CharacterResult_t274624662 * get_targetCharacter_18() const { return ___targetCharacter_18; }
	inline CharacterResult_t274624662 ** get_address_of_targetCharacter_18() { return &___targetCharacter_18; }
	inline void set_targetCharacter_18(CharacterResult_t274624662 * value)
	{
		___targetCharacter_18 = value;
		Il2CppCodeGenWriteBarrier(&___targetCharacter_18, value);
	}
};

struct PlayFabApiTest_t2536778019_StaticFields
{
public:
	// System.Boolean PlayFab.UUnit.PlayFabApiTest::EXEC_ONCE
	bool ___EXEC_ONCE_7;
	// System.Boolean PlayFab.UUnit.PlayFabApiTest::TITLE_INFO_SET
	bool ___TITLE_INFO_SET_8;
	// System.Boolean PlayFab.UUnit.PlayFabApiTest::TITLE_CAN_UPDATE_SETTINGS
	bool ___TITLE_CAN_UPDATE_SETTINGS_9;
	// System.String PlayFab.UUnit.PlayFabApiTest::USER_NAME
	String_t* ___USER_NAME_10;
	// System.String PlayFab.UUnit.PlayFabApiTest::USER_EMAIL
	String_t* ___USER_EMAIL_11;
	// System.String PlayFab.UUnit.PlayFabApiTest::USER_PASSWORD
	String_t* ___USER_PASSWORD_12;
	// System.String PlayFab.UUnit.PlayFabApiTest::CHAR_NAME
	String_t* ___CHAR_NAME_13;
	// System.String PlayFab.UUnit.PlayFabApiTest::playFabId
	String_t* ___playFabId_14;

public:
	inline static int32_t get_offset_of_EXEC_ONCE_7() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019_StaticFields, ___EXEC_ONCE_7)); }
	inline bool get_EXEC_ONCE_7() const { return ___EXEC_ONCE_7; }
	inline bool* get_address_of_EXEC_ONCE_7() { return &___EXEC_ONCE_7; }
	inline void set_EXEC_ONCE_7(bool value)
	{
		___EXEC_ONCE_7 = value;
	}

	inline static int32_t get_offset_of_TITLE_INFO_SET_8() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019_StaticFields, ___TITLE_INFO_SET_8)); }
	inline bool get_TITLE_INFO_SET_8() const { return ___TITLE_INFO_SET_8; }
	inline bool* get_address_of_TITLE_INFO_SET_8() { return &___TITLE_INFO_SET_8; }
	inline void set_TITLE_INFO_SET_8(bool value)
	{
		___TITLE_INFO_SET_8 = value;
	}

	inline static int32_t get_offset_of_TITLE_CAN_UPDATE_SETTINGS_9() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019_StaticFields, ___TITLE_CAN_UPDATE_SETTINGS_9)); }
	inline bool get_TITLE_CAN_UPDATE_SETTINGS_9() const { return ___TITLE_CAN_UPDATE_SETTINGS_9; }
	inline bool* get_address_of_TITLE_CAN_UPDATE_SETTINGS_9() { return &___TITLE_CAN_UPDATE_SETTINGS_9; }
	inline void set_TITLE_CAN_UPDATE_SETTINGS_9(bool value)
	{
		___TITLE_CAN_UPDATE_SETTINGS_9 = value;
	}

	inline static int32_t get_offset_of_USER_NAME_10() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019_StaticFields, ___USER_NAME_10)); }
	inline String_t* get_USER_NAME_10() const { return ___USER_NAME_10; }
	inline String_t** get_address_of_USER_NAME_10() { return &___USER_NAME_10; }
	inline void set_USER_NAME_10(String_t* value)
	{
		___USER_NAME_10 = value;
		Il2CppCodeGenWriteBarrier(&___USER_NAME_10, value);
	}

	inline static int32_t get_offset_of_USER_EMAIL_11() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019_StaticFields, ___USER_EMAIL_11)); }
	inline String_t* get_USER_EMAIL_11() const { return ___USER_EMAIL_11; }
	inline String_t** get_address_of_USER_EMAIL_11() { return &___USER_EMAIL_11; }
	inline void set_USER_EMAIL_11(String_t* value)
	{
		___USER_EMAIL_11 = value;
		Il2CppCodeGenWriteBarrier(&___USER_EMAIL_11, value);
	}

	inline static int32_t get_offset_of_USER_PASSWORD_12() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019_StaticFields, ___USER_PASSWORD_12)); }
	inline String_t* get_USER_PASSWORD_12() const { return ___USER_PASSWORD_12; }
	inline String_t** get_address_of_USER_PASSWORD_12() { return &___USER_PASSWORD_12; }
	inline void set_USER_PASSWORD_12(String_t* value)
	{
		___USER_PASSWORD_12 = value;
		Il2CppCodeGenWriteBarrier(&___USER_PASSWORD_12, value);
	}

	inline static int32_t get_offset_of_CHAR_NAME_13() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019_StaticFields, ___CHAR_NAME_13)); }
	inline String_t* get_CHAR_NAME_13() const { return ___CHAR_NAME_13; }
	inline String_t** get_address_of_CHAR_NAME_13() { return &___CHAR_NAME_13; }
	inline void set_CHAR_NAME_13(String_t* value)
	{
		___CHAR_NAME_13 = value;
		Il2CppCodeGenWriteBarrier(&___CHAR_NAME_13, value);
	}

	inline static int32_t get_offset_of_playFabId_14() { return static_cast<int32_t>(offsetof(PlayFabApiTest_t2536778019_StaticFields, ___playFabId_14)); }
	inline String_t* get_playFabId_14() const { return ___playFabId_14; }
	inline String_t** get_address_of_playFabId_14() { return &___playFabId_14; }
	inline void set_playFabId_14(String_t* value)
	{
		___playFabId_14 = value;
		Il2CppCodeGenWriteBarrier(&___playFabId_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
