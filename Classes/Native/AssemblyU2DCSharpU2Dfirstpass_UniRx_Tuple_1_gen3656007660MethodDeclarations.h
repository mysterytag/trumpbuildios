﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_1_gen3656007660.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`1<System.Object>::.ctor(T1)
extern "C"  void Tuple_1__ctor_m146455991_gshared (Tuple_1_t3656007660 * __this, Il2CppObject * ___item10, const MethodInfo* method);
#define Tuple_1__ctor_m146455991(__this, ___item10, method) ((  void (*) (Tuple_1_t3656007660 *, Il2CppObject *, const MethodInfo*))Tuple_1__ctor_m146455991_gshared)(__this, ___item10, method)
// System.Int32 UniRx.Tuple`1<System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_1_System_IComparable_CompareTo_m4226856856_gshared (Tuple_1_t3656007660 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_1_System_IComparable_CompareTo_m4226856856(__this, ___obj0, method) ((  int32_t (*) (Tuple_1_t3656007660 *, Il2CppObject *, const MethodInfo*))Tuple_1_System_IComparable_CompareTo_m4226856856_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`1<System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_1_UniRx_IStructuralComparable_CompareTo_m746929770_gshared (Tuple_1_t3656007660 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_1_UniRx_IStructuralComparable_CompareTo_m746929770(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_1_t3656007660 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_1_UniRx_IStructuralComparable_CompareTo_m746929770_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`1<System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_1_UniRx_IStructuralEquatable_Equals_m263627861_gshared (Tuple_1_t3656007660 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_1_UniRx_IStructuralEquatable_Equals_m263627861(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_1_t3656007660 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_1_UniRx_IStructuralEquatable_Equals_m263627861_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`1<System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_1_UniRx_IStructuralEquatable_GetHashCode_m2911899321_gshared (Tuple_1_t3656007660 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_1_UniRx_IStructuralEquatable_GetHashCode_m2911899321(__this, ___comparer0, method) ((  int32_t (*) (Tuple_1_t3656007660 *, Il2CppObject *, const MethodInfo*))Tuple_1_UniRx_IStructuralEquatable_GetHashCode_m2911899321_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`1<System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_1_UniRx_ITuple_ToString_m2218949202_gshared (Tuple_1_t3656007660 * __this, const MethodInfo* method);
#define Tuple_1_UniRx_ITuple_ToString_m2218949202(__this, method) ((  String_t* (*) (Tuple_1_t3656007660 *, const MethodInfo*))Tuple_1_UniRx_ITuple_ToString_m2218949202_gshared)(__this, method)
// T1 UniRx.Tuple`1<System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_1_get_Item1_m4031709759_gshared (Tuple_1_t3656007660 * __this, const MethodInfo* method);
#define Tuple_1_get_Item1_m4031709759(__this, method) ((  Il2CppObject * (*) (Tuple_1_t3656007660 *, const MethodInfo*))Tuple_1_get_Item1_m4031709759_gshared)(__this, method)
// System.Boolean UniRx.Tuple`1<System.Object>::Equals(System.Object)
extern "C"  bool Tuple_1_Equals_m2311247625_gshared (Tuple_1_t3656007660 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_1_Equals_m2311247625(__this, ___obj0, method) ((  bool (*) (Tuple_1_t3656007660 *, Il2CppObject *, const MethodInfo*))Tuple_1_Equals_m2311247625_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`1<System.Object>::GetHashCode()
extern "C"  int32_t Tuple_1_GetHashCode_m2491159853_gshared (Tuple_1_t3656007660 * __this, const MethodInfo* method);
#define Tuple_1_GetHashCode_m2491159853(__this, method) ((  int32_t (*) (Tuple_1_t3656007660 *, const MethodInfo*))Tuple_1_GetHashCode_m2491159853_gshared)(__this, method)
// System.String UniRx.Tuple`1<System.Object>::ToString()
extern "C"  String_t* Tuple_1_ToString_m2126513055_gshared (Tuple_1_t3656007660 * __this, const MethodInfo* method);
#define Tuple_1_ToString_m2126513055(__this, method) ((  String_t* (*) (Tuple_1_t3656007660 *, const MethodInfo*))Tuple_1_ToString_m2126513055_gshared)(__this, method)
// System.Boolean UniRx.Tuple`1<System.Object>::Equals(UniRx.Tuple`1<T1>)
extern "C"  bool Tuple_1_Equals_m3326043873_gshared (Tuple_1_t3656007660 * __this, Tuple_1_t3656007660  ___other0, const MethodInfo* method);
#define Tuple_1_Equals_m3326043873(__this, ___other0, method) ((  bool (*) (Tuple_1_t3656007660 *, Tuple_1_t3656007660 , const MethodInfo*))Tuple_1_Equals_m3326043873_gshared)(__this, ___other0, method)
