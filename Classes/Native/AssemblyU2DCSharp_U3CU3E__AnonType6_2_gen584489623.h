﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>__AnonType6`2<System.Object,System.Object>
struct  U3CU3E__AnonType6_2_t584489623  : public Il2CppObject
{
public:
	// <money>__T <>__AnonType6`2::<money>
	Il2CppObject * ___U3CmoneyU3E_0;
	// <level>__T <>__AnonType6`2::<level>
	Il2CppObject * ___U3ClevelU3E_1;

public:
	inline static int32_t get_offset_of_U3CmoneyU3E_0() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType6_2_t584489623, ___U3CmoneyU3E_0)); }
	inline Il2CppObject * get_U3CmoneyU3E_0() const { return ___U3CmoneyU3E_0; }
	inline Il2CppObject ** get_address_of_U3CmoneyU3E_0() { return &___U3CmoneyU3E_0; }
	inline void set_U3CmoneyU3E_0(Il2CppObject * value)
	{
		___U3CmoneyU3E_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoneyU3E_0, value);
	}

	inline static int32_t get_offset_of_U3ClevelU3E_1() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType6_2_t584489623, ___U3ClevelU3E_1)); }
	inline Il2CppObject * get_U3ClevelU3E_1() const { return ___U3ClevelU3E_1; }
	inline Il2CppObject ** get_address_of_U3ClevelU3E_1() { return &___U3ClevelU3E_1; }
	inline void set_U3ClevelU3E_1(Il2CppObject * value)
	{
		___U3ClevelU3E_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClevelU3E_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
