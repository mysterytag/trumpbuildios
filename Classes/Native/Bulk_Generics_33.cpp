﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Zenject.PrefabFactory`1<System.Object>
struct PrefabFactory_1_t2803914789;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.PrefabFactory`2<System.Object,System.Object>
struct PrefabFactory_2_t3259451326;
// Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>
struct PrefabFactory_3_t2684195695;
// Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>
struct PrefabFactory_4_t4024024340;
// Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct PrefabFactory_5_t2114963929;
// Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>
struct U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1356416995;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// Zenject.SingletonProviderMap/<AddCreatorFromMethod>c__AnonStorey197`1<System.Object>
struct U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.TaskUpdater`1/<RemoveTask>c__AnonStorey191<System.Object>
struct U3CRemoveTaskU3Ec__AnonStorey191_t949964860;
// Zenject.TaskUpdater`1/TaskInfo<System.Object>
struct TaskInfo_t1064508404;
// Zenject.TaskUpdater`1<System.Object>
struct TaskUpdater_1_t1705995667;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct IEnumerable_1_t3936662760;
// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<System.Object>,System.Object>
struct Func_2_t2584670904;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<System.Object>,System.Boolean>
struct Func_2_t1958569825;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct LinkedList_1_t2804637950;
// ZergEngineTools/UnityActionDisposable`1<System.Object>
struct UnityActionDisposable_1_t3710771925;
// ZergRush.ImmutableList`1<System.Object>
struct ImmutableList_1_t340490602;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Collections.IEnumerable
struct IEnumerable_t287189635;
// ZergRush.InorganicCollection`1<System.Object>
struct InorganicCollection_1_t3450977518;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// IStream`1<System.Int32>
struct IStream_1_t3400105778;
// IStream`1<UniRx.Unit>
struct IStream_1_t3110977029;
// IStream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IStream_1_t2969212978;
// IStream`1<CollectionMoveEvent`1<System.Object>>
struct IStream_1_t4124746372;
// IStream`1<CollectionRemoveEvent`1<System.Object>>
struct IStream_1_t1450950249;
// IStream`1<CollectionReplaceEvent`1<System.Object>>
struct IStream_1_t3050063061;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Action`1<System.Collections.Generic.ICollection`1<System.Object>>
struct Action_1_t1451390511;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>
struct FilterCollection_1_t1215889544;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t2564974692;
// System.Action`1<CollectionRemoveEvent`1<System.Object>>
struct Action_1_t1046711963;
// System.Action`1<CollectionMoveEvent`1<System.Object>>
struct Action_1_t3720508086;
// System.Action`1<CollectionReplaceEvent`1<System.Object>>
struct Action_1_t2645824775;
// ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>
struct FilterCollectionRX_1_t3654614494;
// ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1<System.Object>
struct U3CFilterU3Ec__AnonStorey11E_1_t4290970890;
// ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>
struct U3CFilterU3Ec__AnonStorey11D_1_t441028593;
// ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>
struct U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944;
// CollectionReplaceEvent`1<System.Object>
struct CollectionReplaceEvent_1_t2497372070;
// CollectionRemoveEvent`1<System.Object>
struct CollectionRemoveEvent_1_t898259258;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_1_gen2803914789.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_1_gen2803914789MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Assert1161478012MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_2_gen3259451326.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_2_gen3259451326MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_3_gen2684195695.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_3_gen2684195695MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_4_gen4024024340.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_4_gen4024024340MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_5_gen2114963929.h"
#include "AssemblyU2DCSharp_Zenject_PrefabFactory_5_gen2114963929MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonInstanceHelper_1056901837.h"
#include "AssemblyU2DCSharp_Zenject_SingletonInstanceHelper_1056901837MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap_U3C3364390262.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap_U3C3364390262MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "System_Core_System_Func_2_gen2621245597.h"
#include "System_Core_System_Func_2_gen2621245597MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_U3CRemoveTa949964860.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_U3CRemoveTa949964860MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_TaskInfo_g1064508404.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_TaskInfo_g1064508404MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen1705995667.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen1705995667MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2804637950.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2804637950MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1861467373.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1861467373MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2584670904.h"
#include "System_Core_System_Func_2_gen2584670904MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Core_System_Func_2_gen1958569825.h"
#include "System_Core_System_Func_2_gen1958569825MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "System_System_Collections_Generic_LinkedListNode_1_766499024.h"
#include "System_System_Collections_Generic_LinkedListNode_1_766499024MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4242217661.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4242217661MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergEngineTools_UnityActionDispo3710771925.h"
#include "AssemblyU2DCSharp_ZergEngineTools_UnityActionDispo3710771925MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen4074528527.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen817568325.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen4074528527MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen340490602.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen340490602MethodDeclarations.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_InorganicCollection_1_g3450977518.h"
#include "AssemblyU2DCSharp_ZergRush_InorganicCollection_1_g3450977518MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen4289044124.h"
#include "AssemblyU2DCSharp_Stream_1_gen4289044124MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1249425060MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553809MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1249425060.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553809.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen2263194403.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen2263194403MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3884365576.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3884365576MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "AssemblyU2DCSharp_Stream_1_gen1188511092.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1188511092MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "mscorlib_System_Action_1_gen1451390511.h"
#include "mscorlib_System_Action_1_gen1451390511MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten1215889544.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten1215889544MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2564974692.h"
#include "mscorlib_System_Action_1_gen1046711963.h"
#include "mscorlib_System_Action_1_gen3720508086.h"
#include "mscorlib_System_Action_1_gen2645824775.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten3654614494.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten3654614494MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten4290970890.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten4290970890MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExtens441028593.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExtens441028593MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3061313250.h"
#include "System_Core_System_Func_2_gen3061313250MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge321272808.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge321272808MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten2501101944.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten2501101944MethodDeclarations.h"

// !!0 Zenject.DiContainer::InstantiatePrefabForComponent<System.Object>(UnityEngine.GameObject,System.Object[])
extern "C"  Il2CppObject * DiContainer_InstantiatePrefabForComponent_TisIl2CppObject_m4144750549_gshared (DiContainer_t2383114449 * __this, GameObject_t4012695102 * p0, ObjectU5BU5D_t11523773* p1, const MethodInfo* method);
#define DiContainer_InstantiatePrefabForComponent_TisIl2CppObject_m4144750549(__this, p0, p1, method) ((  Il2CppObject * (*) (DiContainer_t2383114449 *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_InstantiatePrefabForComponent_TisIl2CppObject_m4144750549_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.DiContainer::ValidateObjectGraph<System.Object>(System.Type[])
extern "C"  Il2CppObject* DiContainer_ValidateObjectGraph_TisIl2CppObject_m52669789_gshared (DiContainer_t2383114449 * __this, TypeU5BU5D_t3431720054* p0, const MethodInfo* method);
#define DiContainer_ValidateObjectGraph_TisIl2CppObject_m52669789(__this, p0, method) ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))DiContainer_ValidateObjectGraph_TisIl2CppObject_m52669789_gshared)(__this, p0, method)
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C"  bool Enumerable_Contains_TisIl2CppObject_m2362777420_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define Enumerable_Contains_TisIl2CppObject_m2362777420(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m2362777420_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Contains<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
#define Enumerable_Contains_TisType_t_m2414400775(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Type_t *, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m2362777420_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_Concat_TisIl2CppObject_m3543084802_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, const MethodInfo* method);
#define Enumerable_Concat_TisIl2CppObject_m3543084802(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_Concat_TisIl2CppObject_m3543084802_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<Zenject.TaskUpdater`1/TaskInfo<System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Concat_TisTaskInfo_t1064508404_m345128468(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_Concat_TisIl2CppObject_m3543084802_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<Zenject.TaskUpdater`1/TaskInfo<System.Object>,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTaskInfo_t1064508404_TisIl2CppObject_m767721788(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2584670904 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m3480373697_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_Where_TisIl2CppObject_m3480373697(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<Zenject.TaskUpdater`1/TaskInfo<System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisTaskInfo_t1064508404_m691375862(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1958569825 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::Single<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_Single_TisIl2CppObject_m3651905832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Single_TisIl2CppObject_m3651905832(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m3651905832_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Single<Zenject.TaskUpdater`1/TaskInfo<System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Single_TisTaskInfo_t1064508404_m4050336698(__this /* static, unused */, p0, method) ((  TaskInfo_t1064508404 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m3651905832_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::OfType<System.Object>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_OfType_TisIl2CppObject_m844136824_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_OfType_TisIl2CppObject_m844136824(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_OfType_TisIl2CppObject_m844136824_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  int32_t Enumerable_Count_TisIl2CppObject_m3348086026_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_Count_TisIl2CppObject_m3348086026(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_Count_TisIl2CppObject_m3348086026_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::ElementAt<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject * Enumerable_ElementAt_TisIl2CppObject_m2597679966_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_ElementAt_TisIl2CppObject_m2597679966(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_ElementAt_TisIl2CppObject_m2597679966_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.PrefabFactory`1<System.Object>::.ctor()
extern "C"  void PrefabFactory_1__ctor_m2086357463_gshared (PrefabFactory_1_t2803914789 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Zenject.PrefabFactory`1<System.Object>::Create(UnityEngine.GameObject)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral134398278;
extern const uint32_t PrefabFactory_1_Create_m3186751104_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_1_Create_m3186751104_gshared (PrefabFactory_1_t2803914789 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_1_Create_m3186751104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = ___prefab0;
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)L_1, (String_t*)_stringLiteral134398278, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_4 = (DiContainer_t2383114449 *)__this->get__container_0();
		GameObject_t4012695102 * L_5 = ___prefab0;
		NullCheck((DiContainer_t2383114449 *)L_4);
		Il2CppObject * L_6 = GenericVirtFuncInvoker2< Il2CppObject *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (DiContainer_t2383114449 *)L_4, (GameObject_t4012695102 *)L_5, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		return L_6;
	}
}
// T Zenject.PrefabFactory`1<System.Object>::Create(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843117505;
extern const uint32_t PrefabFactory_1_Create_m1056476922_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_1_Create_m1056476922_gshared (PrefabFactory_1_t2803914789 * __this, String_t* ___prefabResourceName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_1_Create_m1056476922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___prefabResourceName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), (String_t*)_stringLiteral843117505, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___prefabResourceName0;
		Object_t3878351788 * L_5 = Resources_Load_m2079217614(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		NullCheck((PrefabFactory_1_t2803914789 *)__this);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (PrefabFactory_1_t2803914789 *, GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((PrefabFactory_1_t2803914789 *)__this, (GameObject_t4012695102 *)((GameObject_t4012695102 *)Castclass(L_5, GameObject_t4012695102_il2cpp_TypeInfo_var)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_6;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`1<System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t PrefabFactory_1_Validate_m624577986_MetadataUsageId;
extern "C"  Il2CppObject* PrefabFactory_1_Validate_m624577986_gshared (PrefabFactory_1_t2803914789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_1_Validate_m624577986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void Zenject.PrefabFactory`2<System.Object,System.Object>::.ctor()
extern "C"  void PrefabFactory_2__ctor_m1664150090_gshared (PrefabFactory_2_t3259451326 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Zenject.PrefabFactory`2<System.Object,System.Object>::Create(UnityEngine.GameObject,P1)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral134398278;
extern const uint32_t PrefabFactory_2_Create_m4007342584_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_2_Create_m4007342584_gshared (PrefabFactory_2_t3259451326 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject * ___param1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_2_Create_m4007342584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = ___prefab0;
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)L_1, (String_t*)_stringLiteral134398278, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_4 = (DiContainer_t2383114449 *)__this->get__container_0();
		GameObject_t4012695102 * L_5 = ___prefab0;
		ObjectU5BU5D_t11523773* L_6 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_7 = ___param1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		NullCheck((DiContainer_t2383114449 *)L_4);
		Il2CppObject * L_8 = GenericVirtFuncInvoker2< Il2CppObject *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_4, (GameObject_t4012695102 *)L_5, (ObjectU5BU5D_t11523773*)L_6);
		return L_8;
	}
}
// T Zenject.PrefabFactory`2<System.Object,System.Object>::Create(System.String,P1)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843117505;
extern const uint32_t PrefabFactory_2_Create_m2719240184_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_2_Create_m2719240184_gshared (PrefabFactory_2_t3259451326 * __this, String_t* ___prefabResourceName0, Il2CppObject * ___param1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_2_Create_m2719240184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___prefabResourceName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), (String_t*)_stringLiteral843117505, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___prefabResourceName0;
		Object_t3878351788 * L_5 = Resources_Load_m2079217614(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___param1;
		NullCheck((PrefabFactory_2_t3259451326 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, GameObject_t4012695102 *, Il2CppObject * >::Invoke(5 /* T Zenject.PrefabFactory`2<System.Object,System.Object>::Create(UnityEngine.GameObject,P1) */, (PrefabFactory_2_t3259451326 *)__this, (GameObject_t4012695102 *)((GameObject_t4012695102 *)Castclass(L_5, GameObject_t4012695102_il2cpp_TypeInfo_var)), (Il2CppObject *)L_6);
		return L_7;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`2<System.Object,System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PrefabFactory_2_Validate_m1816703535_MetadataUsageId;
extern "C"  Il2CppObject* PrefabFactory_2_Validate_m1816703535_gshared (PrefabFactory_2_t3259451326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_2_Validate_m1816703535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_3 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_3;
	}
}
// System.Void Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void PrefabFactory_3__ctor_m1970864061_gshared (PrefabFactory_3_t2684195695 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral134398278;
extern const uint32_t PrefabFactory_3_Create_m523527129_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_3_Create_m523527129_gshared (PrefabFactory_3_t2684195695 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject * ___param1, Il2CppObject * ___param22, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_3_Create_m523527129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = ___prefab0;
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)L_1, (String_t*)_stringLiteral134398278, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_4 = (DiContainer_t2383114449 *)__this->get__container_0();
		GameObject_t4012695102 * L_5 = ___prefab0;
		ObjectU5BU5D_t11523773* L_6 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_7 = ___param1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_6;
		Il2CppObject * L_9 = ___param22;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		NullCheck((DiContainer_t2383114449 *)L_4);
		Il2CppObject * L_10 = GenericVirtFuncInvoker2< Il2CppObject *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (DiContainer_t2383114449 *)L_4, (GameObject_t4012695102 *)L_5, (ObjectU5BU5D_t11523773*)L_8);
		return L_10;
	}
}
// T Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::Create(System.String,P1,P2)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843117505;
extern const uint32_t PrefabFactory_3_Create_m818429151_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_3_Create_m818429151_gshared (PrefabFactory_3_t2684195695 * __this, String_t* ___prefabResourceName0, Il2CppObject * ___param1, Il2CppObject * ___param22, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_3_Create_m818429151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___prefabResourceName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), (String_t*)_stringLiteral843117505, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___prefabResourceName0;
		Object_t3878351788 * L_5 = Resources_Load_m2079217614(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___param1;
		Il2CppObject * L_7 = ___param22;
		NullCheck((PrefabFactory_3_t2684195695 *)__this);
		Il2CppObject * L_8 = VirtFuncInvoker3< Il2CppObject *, GameObject_t4012695102 *, Il2CppObject *, Il2CppObject * >::Invoke(5 /* T Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2) */, (PrefabFactory_3_t2684195695 *)__this, (GameObject_t4012695102 *)((GameObject_t4012695102 *)Castclass(L_5, GameObject_t4012695102_il2cpp_TypeInfo_var)), (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		return L_8;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`3<System.Object,System.Object,System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PrefabFactory_3_Validate_m882209180_MetadataUsageId;
extern "C"  Il2CppObject* PrefabFactory_3_Validate_m882209180_gshared (PrefabFactory_3_t2684195695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_3_Validate_m882209180_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		TypeU5BU5D_t3431720054* L_3 = (TypeU5BU5D_t3431720054*)L_1;
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_4);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_5 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_5;
	}
}
// System.Void Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void PrefabFactory_4__ctor_m2509232688_gshared (PrefabFactory_4_t4024024340 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2,P3)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral134398278;
extern const uint32_t PrefabFactory_4_Create_m3562910205_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_4_Create_m3562910205_gshared (PrefabFactory_4_t4024024340 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject * ___param1, Il2CppObject * ___param22, Il2CppObject * ___param33, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_4_Create_m3562910205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = ___prefab0;
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)L_1, (String_t*)_stringLiteral134398278, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_4 = (DiContainer_t2383114449 *)__this->get__container_0();
		GameObject_t4012695102 * L_5 = ___prefab0;
		ObjectU5BU5D_t11523773* L_6 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3));
		Il2CppObject * L_7 = ___param1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_6;
		Il2CppObject * L_9 = ___param22;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		Il2CppObject * L_11 = ___param33;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		NullCheck((DiContainer_t2383114449 *)L_4);
		Il2CppObject * L_12 = GenericVirtFuncInvoker2< Il2CppObject *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (DiContainer_t2383114449 *)L_4, (GameObject_t4012695102 *)L_5, (ObjectU5BU5D_t11523773*)L_10);
		return L_12;
	}
}
// T Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(System.String,P1,P2,P3)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843117505;
extern const uint32_t PrefabFactory_4_Create_m1225018481_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_4_Create_m1225018481_gshared (PrefabFactory_4_t4024024340 * __this, String_t* ___prefabResourceName0, Il2CppObject * ___param1, Il2CppObject * ___param22, Il2CppObject * ___param33, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_4_Create_m1225018481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___prefabResourceName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), (String_t*)_stringLiteral843117505, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___prefabResourceName0;
		Object_t3878351788 * L_5 = Resources_Load_m2079217614(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___param1;
		Il2CppObject * L_7 = ___param22;
		Il2CppObject * L_8 = ___param33;
		NullCheck((PrefabFactory_4_t4024024340 *)__this);
		Il2CppObject * L_9 = VirtFuncInvoker4< Il2CppObject *, GameObject_t4012695102 *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(5 /* T Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2,P3) */, (PrefabFactory_4_t4024024340 *)__this, (GameObject_t4012695102 *)((GameObject_t4012695102 *)Castclass(L_5, GameObject_t4012695102_il2cpp_TypeInfo_var)), (Il2CppObject *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8);
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PrefabFactory_4_Validate_m3057531913_MetadataUsageId;
extern "C"  Il2CppObject* PrefabFactory_4_Validate_m3057531913_gshared (PrefabFactory_4_t4024024340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_4_Validate_m3057531913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		TypeU5BU5D_t3431720054* L_3 = (TypeU5BU5D_t3431720054*)L_1;
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_4);
		TypeU5BU5D_t3431720054* L_5 = (TypeU5BU5D_t3431720054*)L_3;
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_6);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_7 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_7;
	}
}
// System.Void Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void PrefabFactory_5__ctor_m300534179_gshared (PrefabFactory_5_t2114963929 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2,P3,P4)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral134398278;
extern const uint32_t PrefabFactory_5_Create_m2507381298_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_5_Create_m2507381298_gshared (PrefabFactory_5_t2114963929 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject * ___param1, Il2CppObject * ___param22, Il2CppObject * ___param33, Il2CppObject * ___param44, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_5_Create_m2507381298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = ___prefab0;
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)L_1, (String_t*)_stringLiteral134398278, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_4 = (DiContainer_t2383114449 *)__this->get__container_0();
		GameObject_t4012695102 * L_5 = ___prefab0;
		ObjectU5BU5D_t11523773* L_6 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		Il2CppObject * L_7 = ___param1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_6;
		Il2CppObject * L_9 = ___param22;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		Il2CppObject * L_11 = ___param33;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		Il2CppObject * L_13 = ___param44;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		NullCheck((DiContainer_t2383114449 *)L_4);
		Il2CppObject * L_14 = GenericVirtFuncInvoker2< Il2CppObject *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (DiContainer_t2383114449 *)L_4, (GameObject_t4012695102 *)L_5, (ObjectU5BU5D_t11523773*)L_12);
		return L_14;
	}
}
// T Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(System.String,P1,P2,P3,P4)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843117505;
extern const uint32_t PrefabFactory_5_Create_m209914308_MetadataUsageId;
extern "C"  Il2CppObject * PrefabFactory_5_Create_m209914308_gshared (PrefabFactory_5_t2114963929 * __this, String_t* ___prefabResourceName0, Il2CppObject * ___param1, Il2CppObject * ___param22, Il2CppObject * ___param33, Il2CppObject * ___param44, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_5_Create_m209914308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___prefabResourceName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), (String_t*)_stringLiteral843117505, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___prefabResourceName0;
		Object_t3878351788 * L_5 = Resources_Load_m2079217614(NULL /*static, unused*/, (String_t*)L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___param1;
		Il2CppObject * L_7 = ___param22;
		Il2CppObject * L_8 = ___param33;
		Il2CppObject * L_9 = ___param44;
		NullCheck((PrefabFactory_5_t2114963929 *)__this);
		Il2CppObject * L_10 = VirtFuncInvoker5< Il2CppObject *, GameObject_t4012695102 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(5 /* T Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2,P3,P4) */, (PrefabFactory_5_t2114963929 *)__this, (GameObject_t4012695102 *)((GameObject_t4012695102 *)Castclass(L_5, GameObject_t4012695102_il2cpp_TypeInfo_var)), (Il2CppObject *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		return L_10;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PrefabFactory_5_Validate_m2773008758_MetadataUsageId;
extern "C"  Il2CppObject* PrefabFactory_5_Validate_m2773008758_gshared (PrefabFactory_5_t2114963929 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrefabFactory_5_Validate_m2773008758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		TypeU5BU5D_t3431720054* L_3 = (TypeU5BU5D_t3431720054*)L_1;
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_4);
		TypeU5BU5D_t3431720054* L_5 = (TypeU5BU5D_t3431720054*)L_3;
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)), /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_6);
		TypeU5BU5D_t3431720054* L_7 = (TypeU5BU5D_t3431720054*)L_5;
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)), /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_8);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_9 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_9;
	}
}
// System.Void Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>::.ctor()
extern "C"  void U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1__ctor_m3509688979_gshared (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>::<>m__2E1(System.Type)
extern const MethodInfo* Enumerable_Contains_TisType_t_m2414400775_MethodInfo_var;
extern const uint32_t U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E1_m264082753_MetadataUsageId;
extern "C"  bool U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E1_m264082753_gshared (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 * __this, Type_t * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E1_m264082753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_ignoreTypes_0();
		Type_t * L_1 = ___x0;
		bool L_2 = Enumerable_Contains_TisType_t_m2414400775(NULL /*static, unused*/, (Il2CppObject*)L_0, (Type_t *)L_1, /*hidden argument*/Enumerable_Contains_TisType_t_m2414400775_MethodInfo_var);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>::<>m__2E4(System.Type)
extern const MethodInfo* Enumerable_Contains_TisType_t_m2414400775_MethodInfo_var;
extern const uint32_t U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E4_m3042374174_MetadataUsageId;
extern "C"  bool U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E4_m3042374174_gshared (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 * __this, Type_t * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E4_m3042374174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_ignoreTypes_0();
		Type_t * L_1 = ___x0;
		bool L_2 = Enumerable_Contains_TisType_t_m2414400775(NULL /*static, unused*/, (Il2CppObject*)L_0, (Type_t *)L_1, /*hidden argument*/Enumerable_Contains_TisType_t_m2414400775_MethodInfo_var);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Zenject.SingletonProviderMap/<AddCreatorFromMethod>c__AnonStorey197`1<System.Object>::.ctor()
extern "C"  void U3CAddCreatorFromMethodU3Ec__AnonStorey197_1__ctor_m2199910836_gshared (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Zenject.SingletonProviderMap/<AddCreatorFromMethod>c__AnonStorey197`1<System.Object>::<>m__307(Zenject.InjectContext)
extern "C"  Il2CppObject * U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_U3CU3Em__307_m1180757087_gshared (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method)
{
	{
		Func_2_t2621245597 * L_0 = (Func_2_t2621245597 *)__this->get_method_0();
		InjectContext_t3456483891 * L_1 = ___context0;
		NullCheck((Func_2_t2621245597 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2621245597 *, InjectContext_t3456483891 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t2621245597 *)L_0, (InjectContext_t3456483891 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Void Zenject.TaskUpdater`1/<RemoveTask>c__AnonStorey191<System.Object>::.ctor()
extern "C"  void U3CRemoveTaskU3Ec__AnonStorey191__ctor_m414817283_gshared (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.TaskUpdater`1/<RemoveTask>c__AnonStorey191<System.Object>::<>m__2E6(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C"  bool U3CRemoveTaskU3Ec__AnonStorey191_U3CU3Em__2E6_m1444281368_gshared (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * __this, TaskInfo_t1064508404 * ___x0, const MethodInfo* method)
{
	{
		TaskInfo_t1064508404 * L_0 = ___x0;
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_Task_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_task_0();
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Zenject.TaskUpdater`1/TaskInfo<System.Object>::.ctor(TTask,System.Int32)
extern "C"  void TaskInfo__ctor_m1167730649_gshared (TaskInfo_t1064508404 * __this, Il2CppObject * ___task0, int32_t ___priority1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___task0;
		__this->set_Task_0(L_0);
		int32_t L_1 = ___priority1;
		__this->set_Priority_1(L_1);
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::.ctor(System.Action`1<TTask>)
extern "C"  void TaskUpdater_1__ctor_m1842795290_gshared (TaskUpdater_1_t1705995667 * __this, Action_1_t985559125 * ___updateFunc0, const MethodInfo* method)
{
	{
		LinkedList_1_t2804637950 * L_0 = (LinkedList_1_t2804637950 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (LinkedList_1_t2804637950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set__tasks_0(L_0);
		List_1_t1861467373 * L_1 = (List_1_t1861467373 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (List_1_t1861467373 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set__queuedTasks_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t985559125 * L_2 = ___updateFunc0;
		__this->set__updateFunc_2(L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<System.Object>::get_AllTasks()
extern "C"  Il2CppObject* TaskUpdater_1_get_AllTasks_m2200620774_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method)
{
	{
		NullCheck((TaskUpdater_1_t1705995667 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((TaskUpdater_1_t1705995667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		List_1_t1861467373 * L_1 = (List_1_t1861467373 *)__this->get__queuedTasks_1();
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<System.Object>::get_ActiveTasks()
extern "C"  Il2CppObject* TaskUpdater_1_get_ActiveTasks_m515292529_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2804637950 * L_0 = (LinkedList_1_t2804637950 *)__this->get__tasks_0();
		return L_0;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddTask(TTask,System.Int32)
extern "C"  void TaskUpdater_1_AddTask_m720801655_gshared (TaskUpdater_1_t1705995667 * __this, Il2CppObject * ___task0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___task0;
		int32_t L_1 = ___priority1;
		NullCheck((TaskUpdater_1_t1705995667 *)__this);
		((  void (*) (TaskUpdater_1_t1705995667 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((TaskUpdater_1_t1705995667 *)__this, (Il2CppObject *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddTaskInternal(TTask,System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2203110440;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t TaskUpdater_1_AddTaskInternal_m1657606612_MetadataUsageId;
extern "C"  void TaskUpdater_1_AddTaskInternal_m1657606612_gshared (TaskUpdater_1_t1705995667 * __this, Il2CppObject * ___task0, int32_t ___priority1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TaskUpdater_1_AddTaskInternal_m1657606612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	{
		NullCheck((TaskUpdater_1_t1705995667 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((TaskUpdater_1_t1705995667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Func_2_t2584670904 * L_1 = ((TaskUpdater_1_t1705995667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Func_2_t2584670904 * L_3 = (Func_2_t2584670904 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Func_2_t2584670904 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_3, (Il2CppObject *)NULL, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((TaskUpdater_1_t1705995667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t2584670904 * L_4 = ((TaskUpdater_1_t1705995667_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		Il2CppObject* L_5 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2584670904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B2_0, (Func_2_t2584670904 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Il2CppObject * L_6 = ___task0;
		bool L_7 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (Il2CppObject*)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		NullCheck((Il2CppObject *)(*(&___task0)));
		Type_t * L_8 = Object_GetType_m2022236990((Il2CppObject *)(*(&___task0)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1825781833(NULL /*static, unused*/, (String_t*)_stringLiteral2203110440, (String_t*)L_9, (String_t*)_stringLiteral39, /*hidden argument*/NULL);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0), (String_t*)L_10, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		List_1_t1861467373 * L_11 = (List_1_t1861467373 *)__this->get__queuedTasks_1();
		Il2CppObject * L_12 = ___task0;
		int32_t L_13 = ___priority1;
		TaskInfo_t1064508404 * L_14 = (TaskInfo_t1064508404 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (TaskInfo_t1064508404 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_14, (Il2CppObject *)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck((List_1_t1861467373 *)L_11);
		VirtActionInvoker1< TaskInfo_t1064508404 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Add(!0) */, (List_1_t1861467373 *)L_11, (TaskInfo_t1064508404 *)L_14);
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::RemoveTask(TTask)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral446852304;
extern const uint32_t TaskUpdater_1_RemoveTask_m1986106881_MetadataUsageId;
extern "C"  void TaskUpdater_1_RemoveTask_m1986106881_gshared (TaskUpdater_1_t1705995667 * __this, Il2CppObject * ___task0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TaskUpdater_1_RemoveTask_m1986106881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TaskInfo_t1064508404 * V_0 = NULL;
	U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * V_1 = NULL;
	{
		U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * L_0 = (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		((  void (*) (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		V_1 = (U3CRemoveTaskU3Ec__AnonStorey191_t949964860 *)L_0;
		U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * L_1 = V_1;
		Il2CppObject * L_2 = ___task0;
		NullCheck(L_1);
		L_1->set_task_0(L_2);
		NullCheck((TaskUpdater_1_t1705995667 *)__this);
		Il2CppObject* L_3 = ((  Il2CppObject* (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((TaskUpdater_1_t1705995667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * L_4 = V_1;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		Func_2_t1958569825 * L_6 = (Func_2_t1958569825 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		((  void (*) (Func_2_t1958569825 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		Il2CppObject* L_7 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1958569825 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(NULL /*static, unused*/, (Il2CppObject*)L_3, (Func_2_t1958569825 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		TaskInfo_t1064508404 * L_8 = ((  TaskInfo_t1064508404 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(NULL /*static, unused*/, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		V_0 = (TaskInfo_t1064508404 *)L_8;
		TaskInfo_t1064508404 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = (bool)L_9->get_IsRemoved_2();
		U3CRemoveTaskU3Ec__AnonStorey191_t949964860 * L_11 = V_1;
		NullCheck(L_11);
		Il2CppObject ** L_12 = (Il2CppObject **)L_11->get_address_of_task_0();
		NullCheck((Il2CppObject *)(*L_12));
		Type_t * L_13 = Object_GetType_m2022236990((Il2CppObject *)(*L_12), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral446852304, (String_t*)L_14, /*hidden argument*/NULL);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0), (String_t*)L_15, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		TaskInfo_t1064508404 * L_16 = V_0;
		NullCheck(L_16);
		L_16->set_IsRemoved_2((bool)1);
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::OnFrameStart()
extern "C"  void TaskUpdater_1_OnFrameStart_m1489365667_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method)
{
	{
		NullCheck((TaskUpdater_1_t1705995667 *)__this);
		((  void (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((TaskUpdater_1_t1705995667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::UpdateAll()
extern "C"  void TaskUpdater_1_UpdateAll_m611209579_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method)
{
	{
		NullCheck((TaskUpdater_1_t1705995667 *)__this);
		((  void (*) (TaskUpdater_1_t1705995667 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((TaskUpdater_1_t1705995667 *)__this, (int32_t)((int32_t)-2147483648LL), (int32_t)((int32_t)2147483647LL), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::UpdateRange(System.Int32,System.Int32)
extern "C"  void TaskUpdater_1_UpdateRange_m1084380479_gshared (TaskUpdater_1_t1705995667 * __this, int32_t ___minPriority0, int32_t ___maxPriority1, const MethodInfo* method)
{
	LinkedListNode_1_t766499024 * V_0 = NULL;
	LinkedListNode_1_t766499024 * V_1 = NULL;
	TaskInfo_t1064508404 * V_2 = NULL;
	{
		LinkedList_1_t2804637950 * L_0 = (LinkedList_1_t2804637950 *)__this->get__tasks_0();
		NullCheck((LinkedList_1_t2804637950 *)L_0);
		LinkedListNode_1_t766499024 * L_1 = ((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((LinkedList_1_t2804637950 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_0 = (LinkedListNode_1_t766499024 *)L_1;
		goto IL_0060;
	}

IL_0011:
	{
		LinkedListNode_1_t766499024 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t766499024 *)L_2);
		LinkedListNode_1_t766499024 * L_3 = ((  LinkedListNode_1_t766499024 * (*) (LinkedListNode_1_t766499024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((LinkedListNode_1_t766499024 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		V_1 = (LinkedListNode_1_t766499024 *)L_3;
		LinkedListNode_1_t766499024 * L_4 = V_0;
		NullCheck((LinkedListNode_1_t766499024 *)L_4);
		TaskInfo_t1064508404 * L_5 = ((  TaskInfo_t1064508404 * (*) (LinkedListNode_1_t766499024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((LinkedListNode_1_t766499024 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		V_2 = (TaskInfo_t1064508404 *)L_5;
		TaskInfo_t1064508404 * L_6 = V_2;
		NullCheck(L_6);
		bool L_7 = (bool)L_6->get_IsRemoved_2();
		if (L_7)
		{
			goto IL_005e;
		}
	}
	{
		TaskInfo_t1064508404 * L_8 = V_2;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)L_8->get_Priority_1();
		int32_t L_10 = ___minPriority0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_11 = ___maxPriority1;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_004d;
		}
	}
	{
		TaskInfo_t1064508404 * L_12 = V_2;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)L_12->get_Priority_1();
		int32_t L_14 = ___maxPriority1;
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_005e;
		}
	}

IL_004d:
	{
		Action_1_t985559125 * L_15 = (Action_1_t985559125 *)__this->get__updateFunc_2();
		TaskInfo_t1064508404 * L_16 = V_2;
		NullCheck(L_16);
		Il2CppObject * L_17 = (Il2CppObject *)L_16->get_Task_0();
		NullCheck((Action_1_t985559125 *)L_15);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)->method)((Action_1_t985559125 *)L_15, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
	}

IL_005e:
	{
		LinkedListNode_1_t766499024 * L_18 = V_1;
		V_0 = (LinkedListNode_1_t766499024 *)L_18;
	}

IL_0060:
	{
		LinkedListNode_1_t766499024 * L_19 = V_0;
		if (L_19)
		{
			goto IL_0011;
		}
	}
	{
		LinkedList_1_t2804637950 * L_20 = (LinkedList_1_t2804637950 *)__this->get__tasks_0();
		NullCheck((TaskUpdater_1_t1705995667 *)__this);
		((  void (*) (TaskUpdater_1_t1705995667 *, LinkedList_1_t2804637950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((TaskUpdater_1_t1705995667 *)__this, (LinkedList_1_t2804637950 *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::ClearRemovedTasks(System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>>)
extern "C"  void TaskUpdater_1_ClearRemovedTasks_m204003072_gshared (TaskUpdater_1_t1705995667 * __this, LinkedList_1_t2804637950 * ___tasks0, const MethodInfo* method)
{
	LinkedListNode_1_t766499024 * V_0 = NULL;
	LinkedListNode_1_t766499024 * V_1 = NULL;
	TaskInfo_t1064508404 * V_2 = NULL;
	{
		LinkedList_1_t2804637950 * L_0 = ___tasks0;
		NullCheck((LinkedList_1_t2804637950 *)L_0);
		LinkedListNode_1_t766499024 * L_1 = ((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((LinkedList_1_t2804637950 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_0 = (LinkedListNode_1_t766499024 *)L_1;
		goto IL_002e;
	}

IL_000c:
	{
		LinkedListNode_1_t766499024 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t766499024 *)L_2);
		LinkedListNode_1_t766499024 * L_3 = ((  LinkedListNode_1_t766499024 * (*) (LinkedListNode_1_t766499024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((LinkedListNode_1_t766499024 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		V_1 = (LinkedListNode_1_t766499024 *)L_3;
		LinkedListNode_1_t766499024 * L_4 = V_0;
		NullCheck((LinkedListNode_1_t766499024 *)L_4);
		TaskInfo_t1064508404 * L_5 = ((  TaskInfo_t1064508404 * (*) (LinkedListNode_1_t766499024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((LinkedListNode_1_t766499024 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		V_2 = (TaskInfo_t1064508404 *)L_5;
		TaskInfo_t1064508404 * L_6 = V_2;
		NullCheck(L_6);
		bool L_7 = (bool)L_6->get_IsRemoved_2();
		if (!L_7)
		{
			goto IL_002c;
		}
	}
	{
		LinkedList_1_t2804637950 * L_8 = ___tasks0;
		LinkedListNode_1_t766499024 * L_9 = V_0;
		NullCheck((LinkedList_1_t2804637950 *)L_8);
		((  void (*) (LinkedList_1_t2804637950 *, LinkedListNode_1_t766499024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((LinkedList_1_t2804637950 *)L_8, (LinkedListNode_1_t766499024 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
	}

IL_002c:
	{
		LinkedListNode_1_t766499024 * L_10 = V_1;
		V_0 = (LinkedListNode_1_t766499024 *)L_10;
	}

IL_002e:
	{
		LinkedListNode_1_t766499024 * L_11 = V_0;
		if (L_11)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddQueuedTasks()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TaskUpdater_1_AddQueuedTasks_m2029046249_MetadataUsageId;
extern "C"  void TaskUpdater_1_AddQueuedTasks_m2029046249_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TaskUpdater_1_AddQueuedTasks_m2029046249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TaskInfo_t1064508404 * V_0 = NULL;
	Enumerator_t4242217661  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1861467373 * L_0 = (List_1_t1861467373 *)__this->get__queuedTasks_1();
		NullCheck((List_1_t1861467373 *)L_0);
		Enumerator_t4242217661  L_1 = ((  Enumerator_t4242217661  (*) (List_1_t1861467373 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)((List_1_t1861467373 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		V_1 = (Enumerator_t4242217661 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_0011:
		{
			TaskInfo_t1064508404 * L_2 = ((  TaskInfo_t1064508404 * (*) (Enumerator_t4242217661 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)((Enumerator_t4242217661 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
			V_0 = (TaskInfo_t1064508404 *)L_2;
			TaskInfo_t1064508404 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = (bool)L_3->get_IsRemoved_2();
			if (L_4)
			{
				goto IL_002b;
			}
		}

IL_0024:
		{
			TaskInfo_t1064508404 * L_5 = V_0;
			NullCheck((TaskUpdater_1_t1705995667 *)__this);
			((  void (*) (TaskUpdater_1_t1705995667 *, TaskInfo_t1064508404 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)->method)((TaskUpdater_1_t1705995667 *)__this, (TaskInfo_t1064508404 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35));
		}

IL_002b:
		{
			bool L_6 = ((  bool (*) (Enumerator_t4242217661 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Enumerator_t4242217661 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x48, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		Enumerator_t4242217661  L_7 = V_1;
		Enumerator_t4242217661  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(60)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		List_1_t1861467373 * L_10 = (List_1_t1861467373 *)__this->get__queuedTasks_1();
		NullCheck((List_1_t1861467373 *)L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Clear() */, (List_1_t1861467373 *)L_10);
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::InsertTaskSorted(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C"  void TaskUpdater_1_InsertTaskSorted_m3714982387_gshared (TaskUpdater_1_t1705995667 * __this, TaskInfo_t1064508404 * ___task0, const MethodInfo* method)
{
	LinkedListNode_1_t766499024 * V_0 = NULL;
	{
		LinkedList_1_t2804637950 * L_0 = (LinkedList_1_t2804637950 *)__this->get__tasks_0();
		NullCheck((LinkedList_1_t2804637950 *)L_0);
		LinkedListNode_1_t766499024 * L_1 = ((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((LinkedList_1_t2804637950 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_0 = (LinkedListNode_1_t766499024 *)L_1;
		goto IL_003d;
	}

IL_0011:
	{
		LinkedListNode_1_t766499024 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t766499024 *)L_2);
		TaskInfo_t1064508404 * L_3 = ((  TaskInfo_t1064508404 * (*) (LinkedListNode_1_t766499024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((LinkedListNode_1_t766499024 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_Priority_1();
		TaskInfo_t1064508404 * L_5 = ___task0;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get_Priority_1();
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_0036;
		}
	}
	{
		LinkedList_1_t2804637950 * L_7 = (LinkedList_1_t2804637950 *)__this->get__tasks_0();
		LinkedListNode_1_t766499024 * L_8 = V_0;
		TaskInfo_t1064508404 * L_9 = ___task0;
		NullCheck((LinkedList_1_t2804637950 *)L_7);
		((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, LinkedListNode_1_t766499024 *, TaskInfo_t1064508404 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((LinkedList_1_t2804637950 *)L_7, (LinkedListNode_1_t766499024 *)L_8, (TaskInfo_t1064508404 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return;
	}

IL_0036:
	{
		LinkedListNode_1_t766499024 * L_10 = V_0;
		NullCheck((LinkedListNode_1_t766499024 *)L_10);
		LinkedListNode_1_t766499024 * L_11 = ((  LinkedListNode_1_t766499024 * (*) (LinkedListNode_1_t766499024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((LinkedListNode_1_t766499024 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		V_0 = (LinkedListNode_1_t766499024 *)L_11;
	}

IL_003d:
	{
		LinkedListNode_1_t766499024 * L_12 = V_0;
		if (L_12)
		{
			goto IL_0011;
		}
	}
	{
		LinkedList_1_t2804637950 * L_13 = (LinkedList_1_t2804637950 *)__this->get__tasks_0();
		TaskInfo_t1064508404 * L_14 = ___task0;
		NullCheck((LinkedList_1_t2804637950 *)L_13);
		((  LinkedListNode_1_t766499024 * (*) (LinkedList_1_t2804637950 *, TaskInfo_t1064508404 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40)->method)((LinkedList_1_t2804637950 *)L_13, (TaskInfo_t1064508404 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40));
		return;
	}
}
// TTask Zenject.TaskUpdater`1<System.Object>::<AddTaskInternal>m__2E5(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C"  Il2CppObject * TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m2398513880_gshared (Il2CppObject * __this /* static, unused */, TaskInfo_t1064508404 * ___x0, const MethodInfo* method)
{
	{
		TaskInfo_t1064508404 * L_0 = ___x0;
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_Task_0();
		return L_1;
	}
}
// System.Void ZergEngineTools/UnityActionDisposable`1<System.Object>::.ctor()
extern "C"  void UnityActionDisposable_1__ctor_m3503343429_gshared (UnityActionDisposable_1_t3710771925 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZergEngineTools/UnityActionDisposable`1<System.Object>::Dispose()
extern "C"  void UnityActionDisposable_1_Dispose_m3651925570_gshared (UnityActionDisposable_1_t3710771925 * __this, const MethodInfo* method)
{
	{
		UnityEvent_1_t4074528527 * L_0 = (UnityEvent_1_t4074528527 *)__this->get_e_0();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		UnityEvent_1_t4074528527 * L_1 = (UnityEvent_1_t4074528527 *)__this->get_e_0();
		UnityAction_1_t817568325 * L_2 = (UnityAction_1_t817568325 *)__this->get_action_1();
		NullCheck((UnityEvent_1_t4074528527 *)L_1);
		((  void (*) (UnityEvent_1_t4074528527 *, UnityAction_1_t817568325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((UnityEvent_1_t4074528527 *)L_1, (UnityAction_1_t817568325 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_e_0((UnityEvent_1_t4074528527 *)NULL);
		__this->set_action_1((UnityAction_1_t817568325 *)NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void ZergRush.ImmutableList`1<System.Object>::.ctor()
extern "C"  void ImmutableList_1__ctor_m99192906_gshared (ImmutableList_1_t340490602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_data_0(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)0)));
		return;
	}
}
// System.Void ZergRush.ImmutableList`1<System.Object>::.ctor(T)
extern "C"  void ImmutableList_1__ctor_m3074981460_gshared (ImmutableList_1_t340490602 * __this, Il2CppObject * ___single0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)1));
		Il2CppObject * L_1 = ___single0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		__this->set_data_0(L_0);
		return;
	}
}
// System.Void ZergRush.ImmutableList`1<System.Object>::.ctor(T[])
extern "C"  void ImmutableList_1__ctor_m119734386_gshared (ImmutableList_1_t340490602 * __this, ObjectU5BU5D_t11523773* ___data0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_0 = ___data0;
		__this->set_data_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator ZergRush.ImmutableList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2591974359_gshared (ImmutableList_1_t340490602 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.IEnumerator System.Array::GetEnumerator() */, (Il2CppArray *)(Il2CppArray *)L_0);
		return L_1;
	}
}
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Object>::Add(T)
extern "C"  ImmutableList_1_t340490602 * ImmutableList_1_Add_m3924978445_gshared (ImmutableList_1_t340490602 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)1))));
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		ObjectU5BU5D_t11523773* L_2 = V_0;
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_3);
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_4 = V_0;
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_5);
		Il2CppObject * L_6 = ___value0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))));
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>((((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))), (Il2CppObject *)L_6);
		ObjectU5BU5D_t11523773* L_7 = V_0;
		ImmutableList_1_t340490602 * L_8 = (ImmutableList_1_t340490602 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ImmutableList_1_t340490602 *, ObjectU5BU5D_t11523773*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_8, (ObjectU5BU5D_t11523773*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ImmutableList_1_GetEnumerator_m364779198_gshared (ImmutableList_1_t340490602 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_1);
		return L_2;
	}
}
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Object>::Remove(T)
extern "C"  ImmutableList_1_t340490602 * ImmutableList_1_Remove_m1092573448_gshared (ImmutableList_1_t340490602 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((ImmutableList_1_t340490602 *)__this);
		int32_t L_1 = ((  int32_t (*) (ImmutableList_1_t340490602 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ImmutableList_1_t340490602 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return __this;
	}

IL_0011:
	{
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_3);
		V_1 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))-(int32_t)1))));
		ObjectU5BU5D_t11523773* L_4 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		ObjectU5BU5D_t11523773* L_5 = V_1;
		int32_t L_6 = V_0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		int32_t L_8 = V_0;
		ObjectU5BU5D_t11523773* L_9 = V_1;
		int32_t L_10 = V_0;
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_11);
		int32_t L_12 = V_0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)1)), (Il2CppArray *)(Il2CppArray *)L_9, (int32_t)L_10, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))-(int32_t)L_12))-(int32_t)1)), /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_13 = V_1;
		ImmutableList_1_t340490602 * L_14 = (ImmutableList_1_t340490602 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ImmutableList_1_t340490602 *, ObjectU5BU5D_t11523773*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_14, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_14;
	}
}
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Object>::Replace(System.Int32,T)
extern "C"  ImmutableList_1_t340490602 * ImmutableList_1_Replace_m2580545005_gshared (ImmutableList_1_t340490602 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		ObjectU5BU5D_t11523773* L_2 = V_0;
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_3);
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_4 = V_0;
		int32_t L_5 = ___index0;
		Il2CppObject * L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		ObjectU5BU5D_t11523773* L_7 = V_0;
		ImmutableList_1_t340490602 * L_8 = (ImmutableList_1_t340490602 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (ImmutableList_1_t340490602 *, ObjectU5BU5D_t11523773*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_8, (ObjectU5BU5D_t11523773*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Int32 ZergRush.ImmutableList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ImmutableList_1_IndexOf_m1554103039_gshared (ImmutableList_1_t340490602 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_002f;
	}

IL_0007:
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		Il2CppObject * L_2 = ___value0;
		NullCheck((Il2CppObject *)(*((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))));
		bool L_3 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))), (Il2CppObject *)L_2);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_4 = V_0;
		return L_4;
	}

IL_002b:
	{
		int32_t L_5 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_6 = V_0;
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return (-1);
	}
}
// System.Int32 ZergRush.ImmutableList`1<System.Object>::get_Count()
extern "C"  int32_t ImmutableList_1_get_Count_m749861468_gshared (ImmutableList_1_t340490602 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_data_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::.ctor()
extern "C"  void InorganicCollection_1__ctor_m2562722286_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stream_1_t4289044124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_updates_8(L_0);
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Collection_1_t2806094150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t InorganicCollection_1__ctor_m3238279633_MetadataUsageId;
extern "C"  void InorganicCollection_1__ctor_m3238279633_gshared (InorganicCollection_1_t3450977518 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InorganicCollection_1__ctor_m3238279633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stream_1_t4289044124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_updates_8(L_0);
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Collection_1_t2806094150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_1 = ___collection0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Il2CppObject* L_3 = ___collection0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3);
		V_1 = (Il2CppObject*)L_4;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003c;
		}

IL_002e:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5);
			V_0 = (Il2CppObject *)L_6;
			Il2CppObject * L_7 = V_0;
			NullCheck((Collection_1_t2806094150 *)__this);
			VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(!0) */, (Collection_1_t2806094150 *)__this, (Il2CppObject *)L_7);
		}

IL_003c:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_002e;
			}
		}

IL_0047:
		{
			IL2CPP_LEAVE(0x57, FINALLY_004c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004c;
	}

FINALLY_004c:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			if (L_10)
			{
				goto IL_0050;
			}
		}

IL_004f:
		{
			IL2CPP_END_FINALLY(76)
		}

IL_0050:
		{
			Il2CppObject* L_11 = V_1;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(76)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(76)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0057:
	{
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void InorganicCollection_1__ctor_m1788080966_gshared (InorganicCollection_1_t3450977518 * __this, List_1_t1634065389 * ___list0, const MethodInfo* method)
{
	InorganicCollection_1_t3450977518 * G_B2_0 = NULL;
	InorganicCollection_1_t3450977518 * G_B1_0 = NULL;
	List_1_t1634065389 * G_B3_0 = NULL;
	InorganicCollection_1_t3450977518 * G_B3_1 = NULL;
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stream_1_t4289044124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_updates_8(L_0);
		List_1_t1634065389 * L_1 = ___list0;
		G_B1_0 = ((InorganicCollection_1_t3450977518 *)(__this));
		if (!L_1)
		{
			G_B2_0 = ((InorganicCollection_1_t3450977518 *)(__this));
			goto IL_001d;
		}
	}
	{
		List_1_t1634065389 * L_2 = ___list0;
		List_1_t1634065389 * L_3 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		G_B3_0 = L_3;
		G_B3_1 = ((InorganicCollection_1_t3450977518 *)(G_B1_0));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = ((List_1_t1634065389 *)(NULL));
		G_B3_1 = ((InorganicCollection_1_t3450977518 *)(G_B2_0));
	}

IL_001e:
	{
		NullCheck((Collection_1_t2806094150 *)G_B3_1);
		((  void (*) (Collection_1_t2806094150 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Collection_1_t2806094150 *)G_B3_1, (Il2CppObject*)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::ClearItems()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Stream_1_Send_m1361871350_MethodInfo_var;
extern const MethodInfo* Stream_1_Send_m1041823273_MethodInfo_var;
extern const uint32_t InorganicCollection_1_ClearItems_m2583413385_MetadataUsageId;
extern "C"  void InorganicCollection_1_ClearItems_m2583413385_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InorganicCollection_1_ClearItems_m2583413385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count() */, (Collection_1_t2806094150 *)__this);
		V_0 = (int32_t)L_0;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Collection_1_t2806094150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Stream_1_t1249425060 * L_1 = (Stream_1_t1249425060 *)__this->get_collectionReset_3();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Stream_1_t1249425060 * L_2 = (Stream_1_t1249425060 *)__this->get_collectionReset_3();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_3 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Stream_1_t1249425060 *)L_2);
		Stream_1_Send_m1361871350((Stream_1_t1249425060 *)L_2, (Unit_t2558286038 )L_3, /*hidden argument*/Stream_1_Send_m1361871350_MethodInfo_var);
	}

IL_0028:
	{
		Stream_1_t4289044124 * L_4 = (Stream_1_t4289044124 *)__this->get_updates_8();
		NullCheck((Stream_1_t4289044124 *)L_4);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Stream_1_t4289044124 *)L_4, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0057;
		}
	}
	{
		Stream_1_t1538553809 * L_6 = (Stream_1_t1538553809 *)__this->get_countChanged_2();
		if (!L_6)
		{
			goto IL_0057;
		}
	}
	{
		Stream_1_t1538553809 * L_7 = (Stream_1_t1538553809 *)__this->get_countChanged_2();
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count() */, (Collection_1_t2806094150 *)__this);
		NullCheck((Stream_1_t1538553809 *)L_7);
		Stream_1_Send_m1041823273((Stream_1_t1538553809 *)L_7, (int32_t)L_8, /*hidden argument*/Stream_1_Send_m1041823273_MethodInfo_var);
	}

IL_0057:
	{
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::InsertItem(System.Int32,T)
extern const MethodInfo* Stream_1_Send_m1041823273_MethodInfo_var;
extern const uint32_t InorganicCollection_1_InsertItem_m1273645931_MetadataUsageId;
extern "C"  void InorganicCollection_1_InsertItem_m1273645931_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InorganicCollection_1_InsertItem_m1273645931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___item1;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		Stream_1_t1107661009 * L_2 = (Stream_1_t1107661009 *)__this->get_collectionAdd_4();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Stream_1_t1107661009 * L_3 = (Stream_1_t1107661009 *)__this->get_collectionAdd_4();
		int32_t L_4 = ___index0;
		Il2CppObject * L_5 = ___item1;
		CollectionAddEvent_1_t2416521987  L_6;
		memset(&L_6, 0, sizeof(L_6));
		((  void (*) (CollectionAddEvent_1_t2416521987 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(&L_6, (int32_t)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck((Stream_1_t1107661009 *)L_3);
		((  void (*) (Stream_1_t1107661009 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Stream_1_t1107661009 *)L_3, (CollectionAddEvent_1_t2416521987 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
	}

IL_0025:
	{
		Stream_1_t1538553809 * L_7 = (Stream_1_t1538553809 *)__this->get_countChanged_2();
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		Stream_1_t1538553809 * L_8 = (Stream_1_t1538553809 *)__this->get_countChanged_2();
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count() */, (Collection_1_t2806094150 *)__this);
		NullCheck((Stream_1_t1538553809 *)L_8);
		Stream_1_Send_m1041823273((Stream_1_t1538553809 *)L_8, (int32_t)L_9, /*hidden argument*/Stream_1_Send_m1041823273_MethodInfo_var);
	}

IL_0041:
	{
		Stream_1_t4289044124 * L_10 = (Stream_1_t4289044124 *)__this->get_updates_8();
		NullCheck((Stream_1_t4289044124 *)L_10);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Stream_1_t4289044124 *)L_10, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::Move(System.Int32,System.Int32)
extern "C"  void InorganicCollection_1_Move_m2620974047_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___oldIndex0;
		int32_t L_1 = ___newIndex1;
		NullCheck((InorganicCollection_1_t3450977518 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(48 /* System.Void ZergRush.InorganicCollection`1<System.Object>::MoveItem(System.Int32,System.Int32) */, (InorganicCollection_1_t3450977518 *)__this, (int32_t)L_0, (int32_t)L_1);
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::MoveItem(System.Int32,System.Int32)
extern "C"  void InorganicCollection_1_MoveItem_m1781936076_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___oldIndex0;
		NullCheck((Collection_1_t2806094150 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0);
		V_0 = (Il2CppObject *)L_1;
		int32_t L_2 = ___oldIndex0;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		int32_t L_3 = ___newIndex1;
		Il2CppObject * L_4 = V_0;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		Stream_1_t2263194403 * L_5 = (Stream_1_t2263194403 *)__this->get_collectionMove_5();
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Stream_1_t2263194403 * L_6 = (Stream_1_t2263194403 *)__this->get_collectionMove_5();
		int32_t L_7 = ___oldIndex0;
		int32_t L_8 = ___newIndex1;
		Il2CppObject * L_9 = V_0;
		CollectionMoveEvent_1_t3572055381 * L_10 = (CollectionMoveEvent_1_t3572055381 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		((  void (*) (CollectionMoveEvent_1_t3572055381 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)(L_10, (int32_t)L_7, (int32_t)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		NullCheck((Stream_1_t2263194403 *)L_6);
		((  void (*) (Stream_1_t2263194403 *, CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((Stream_1_t2263194403 *)L_6, (CollectionMoveEvent_1_t3572055381 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
	}

IL_0035:
	{
		Stream_1_t4289044124 * L_11 = (Stream_1_t4289044124 *)__this->get_updates_8();
		NullCheck((Stream_1_t4289044124 *)L_11);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Stream_1_t4289044124 *)L_11, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::RemoveItem(System.Int32)
extern const MethodInfo* Stream_1_Send_m1041823273_MethodInfo_var;
extern const uint32_t InorganicCollection_1_RemoveItem_m3410364318_MetadataUsageId;
extern "C"  void InorganicCollection_1_RemoveItem_m3410364318_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InorganicCollection_1_RemoveItem_m3410364318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2806094150 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0);
		V_0 = (Il2CppObject *)L_1;
		int32_t L_2 = ___index0;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		Stream_1_t3884365576 * L_3 = (Stream_1_t3884365576 *)__this->get_collectionRemove_6();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Stream_1_t3884365576 * L_4 = (Stream_1_t3884365576 *)__this->get_collectionRemove_6();
		int32_t L_5 = ___index0;
		Il2CppObject * L_6 = V_0;
		CollectionRemoveEvent_1_t898259258 * L_7 = (CollectionRemoveEvent_1_t898259258 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		((  void (*) (CollectionRemoveEvent_1_t898259258 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(L_7, (int32_t)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		NullCheck((Stream_1_t3884365576 *)L_4);
		((  void (*) (Stream_1_t3884365576 *, CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((Stream_1_t3884365576 *)L_4, (CollectionRemoveEvent_1_t898259258 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
	}

IL_002c:
	{
		Stream_1_t1538553809 * L_8 = (Stream_1_t1538553809 *)__this->get_countChanged_2();
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		Stream_1_t1538553809 * L_9 = (Stream_1_t1538553809 *)__this->get_countChanged_2();
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count() */, (Collection_1_t2806094150 *)__this);
		NullCheck((Stream_1_t1538553809 *)L_9);
		Stream_1_Send_m1041823273((Stream_1_t1538553809 *)L_9, (int32_t)L_10, /*hidden argument*/Stream_1_Send_m1041823273_MethodInfo_var);
	}

IL_0048:
	{
		Stream_1_t4289044124 * L_11 = (Stream_1_t4289044124 *)__this->get_updates_8();
		NullCheck((Stream_1_t4289044124 *)L_11);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Stream_1_t4289044124 *)L_11, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::RemoveItem(System.Func`2<T,System.Boolean>)
extern "C"  void InorganicCollection_1_RemoveItem_m2366946799_gshared (InorganicCollection_1_t3450977518 * __this, Func_2_t1509682273 * ___filter0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Func_2_t1509682273 * L_0 = ___filter0;
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (Il2CppObject*)__this, (Func_2_t1509682273 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_4 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(28 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(!0) */, (Collection_1_t2806094150 *)__this, (Il2CppObject *)L_3);
		NullCheck((InorganicCollection_1_t3450977518 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void ZergRush.InorganicCollection`1<System.Object>::RemoveItem(System.Int32) */, (InorganicCollection_1_t3450977518 *)__this, (int32_t)L_4);
	}

IL_0020:
	{
		return;
	}
}
// System.Void ZergRush.InorganicCollection`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void InorganicCollection_1_SetItem_m2680569482_gshared (InorganicCollection_1_t3450977518 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2806094150 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0);
		V_0 = (Il2CppObject *)L_1;
		int32_t L_2 = ___index0;
		Il2CppObject * L_3 = ___item1;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		Stream_1_t1188511092 * L_4 = (Stream_1_t1188511092 *)__this->get_collectionReplace_7();
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		Stream_1_t1188511092 * L_5 = (Stream_1_t1188511092 *)__this->get_collectionReplace_7();
		int32_t L_6 = ___index0;
		Il2CppObject * L_7 = V_0;
		Il2CppObject * L_8 = ___item1;
		CollectionReplaceEvent_1_t2497372070 * L_9 = (CollectionReplaceEvent_1_t2497372070 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		((  void (*) (CollectionReplaceEvent_1_t2497372070 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(L_9, (int32_t)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		NullCheck((Stream_1_t1188511092 *)L_5);
		((  void (*) (Stream_1_t1188511092 *, CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)((Stream_1_t1188511092 *)L_5, (CollectionReplaceEvent_1_t2497372070 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
	}

IL_002e:
	{
		Stream_1_t4289044124 * L_10 = (Stream_1_t4289044124 *)__this->get_updates_8();
		NullCheck((Stream_1_t4289044124 *)L_10);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Stream_1_t4289044124 *)L_10, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return;
	}
}
// IStream`1<System.Int32> ZergRush.InorganicCollection`1<System.Object>::ObserveCountChanged()
extern Il2CppClass* Stream_1_t1538553809_il2cpp_TypeInfo_var;
extern const MethodInfo* Stream_1__ctor_m2512605719_MethodInfo_var;
extern const uint32_t InorganicCollection_1_ObserveCountChanged_m3361259667_MetadataUsageId;
extern "C"  Il2CppObject* InorganicCollection_1_ObserveCountChanged_m3361259667_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InorganicCollection_1_ObserveCountChanged_m3361259667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Stream_1_t1538553809 * V_0 = NULL;
	Stream_1_t1538553809 * G_B2_0 = NULL;
	Stream_1_t1538553809 * G_B1_0 = NULL;
	{
		Stream_1_t1538553809 * L_0 = (Stream_1_t1538553809 *)__this->get_countChanged_2();
		Stream_1_t1538553809 * L_1 = (Stream_1_t1538553809 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1538553809 * L_2 = (Stream_1_t1538553809 *)il2cpp_codegen_object_new(Stream_1_t1538553809_il2cpp_TypeInfo_var);
		Stream_1__ctor_m2512605719(L_2, /*hidden argument*/Stream_1__ctor_m2512605719_MethodInfo_var);
		Stream_1_t1538553809 * L_3 = (Stream_1_t1538553809 *)L_2;
		V_0 = (Stream_1_t1538553809 *)L_3;
		__this->set_countChanged_2(L_3);
		Stream_1_t1538553809 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<UniRx.Unit> ZergRush.InorganicCollection`1<System.Object>::ObserveReset()
extern Il2CppClass* Stream_1_t1249425060_il2cpp_TypeInfo_var;
extern const MethodInfo* Stream_1__ctor_m2832653796_MethodInfo_var;
extern const uint32_t InorganicCollection_1_ObserveReset_m884902102_MetadataUsageId;
extern "C"  Il2CppObject* InorganicCollection_1_ObserveReset_m884902102_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InorganicCollection_1_ObserveReset_m884902102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Stream_1_t1249425060 * V_0 = NULL;
	Stream_1_t1249425060 * G_B2_0 = NULL;
	Stream_1_t1249425060 * G_B1_0 = NULL;
	{
		Stream_1_t1249425060 * L_0 = (Stream_1_t1249425060 *)__this->get_collectionReset_3();
		Stream_1_t1249425060 * L_1 = (Stream_1_t1249425060 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1249425060 * L_2 = (Stream_1_t1249425060 *)il2cpp_codegen_object_new(Stream_1_t1249425060_il2cpp_TypeInfo_var);
		Stream_1__ctor_m2832653796(L_2, /*hidden argument*/Stream_1__ctor_m2832653796_MethodInfo_var);
		Stream_1_t1249425060 * L_3 = (Stream_1_t1249425060 *)L_2;
		V_0 = (Stream_1_t1249425060 *)L_3;
		__this->set_collectionReset_3(L_3);
		Stream_1_t1249425060 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<UniRx.CollectionAddEvent`1<T>> ZergRush.InorganicCollection`1<System.Object>::ObserveAdd()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveAdd_m500474968_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	Stream_1_t1107661009 * V_0 = NULL;
	Stream_1_t1107661009 * G_B2_0 = NULL;
	Stream_1_t1107661009 * G_B1_0 = NULL;
	{
		Stream_1_t1107661009 * L_0 = (Stream_1_t1107661009 *)__this->get_collectionAdd_4();
		Stream_1_t1107661009 * L_1 = (Stream_1_t1107661009 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1107661009 * L_2 = (Stream_1_t1107661009 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		((  void (*) (Stream_1_t1107661009 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35));
		Stream_1_t1107661009 * L_3 = (Stream_1_t1107661009 *)L_2;
		V_0 = (Stream_1_t1107661009 *)L_3;
		__this->set_collectionAdd_4(L_3);
		Stream_1_t1107661009 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<CollectionMoveEvent`1<T>> ZergRush.InorganicCollection`1<System.Object>::ObserveMove()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveMove_m2797955546_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	Stream_1_t2263194403 * V_0 = NULL;
	Stream_1_t2263194403 * G_B2_0 = NULL;
	Stream_1_t2263194403 * G_B1_0 = NULL;
	{
		Stream_1_t2263194403 * L_0 = (Stream_1_t2263194403 *)__this->get_collectionMove_5();
		Stream_1_t2263194403 * L_1 = (Stream_1_t2263194403 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t2263194403 * L_2 = (Stream_1_t2263194403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		((  void (*) (Stream_1_t2263194403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37));
		Stream_1_t2263194403 * L_3 = (Stream_1_t2263194403 *)L_2;
		V_0 = (Stream_1_t2263194403 *)L_3;
		__this->set_collectionMove_5(L_3);
		Stream_1_t2263194403 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<CollectionRemoveEvent`1<T>> ZergRush.InorganicCollection`1<System.Object>::ObserveRemove()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveRemove_m3795636736_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	Stream_1_t3884365576 * V_0 = NULL;
	Stream_1_t3884365576 * G_B2_0 = NULL;
	Stream_1_t3884365576 * G_B1_0 = NULL;
	{
		Stream_1_t3884365576 * L_0 = (Stream_1_t3884365576 *)__this->get_collectionRemove_6();
		Stream_1_t3884365576 * L_1 = (Stream_1_t3884365576 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3884365576 * L_2 = (Stream_1_t3884365576 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		((  void (*) (Stream_1_t3884365576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		Stream_1_t3884365576 * L_3 = (Stream_1_t3884365576 *)L_2;
		V_0 = (Stream_1_t3884365576 *)L_3;
		__this->set_collectionRemove_6(L_3);
		Stream_1_t3884365576 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<CollectionReplaceEvent`1<T>> ZergRush.InorganicCollection`1<System.Object>::ObserveReplace()
extern "C"  Il2CppObject* InorganicCollection_1_ObserveReplace_m3125593920_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	Stream_1_t1188511092 * V_0 = NULL;
	Stream_1_t1188511092 * G_B2_0 = NULL;
	Stream_1_t1188511092 * G_B1_0 = NULL;
	{
		Stream_1_t1188511092 * L_0 = (Stream_1_t1188511092 *)__this->get_collectionReplace_7();
		Stream_1_t1188511092 * L_1 = (Stream_1_t1188511092 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1188511092 * L_2 = (Stream_1_t1188511092 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40));
		((  void (*) (Stream_1_t1188511092 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		Stream_1_t1188511092 * L_3 = (Stream_1_t1188511092 *)L_2;
		V_0 = (Stream_1_t1188511092 *)L_3;
		__this->set_collectionReplace_7(L_3);
		Stream_1_t1188511092 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable ZergRush.InorganicCollection`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * InorganicCollection_1_OnChanged_m1222232857_gshared (InorganicCollection_1_t3450977518 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)__this->get_updates_8();
		Action_t437523947 * L_1 = ___action0;
		int32_t L_2 = ___p1;
		NullCheck((Stream_1_t4289044124 *)L_0);
		Il2CppObject * L_3 = VirtFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(5 /* System.IDisposable Stream`1<System.Collections.Generic.ICollection`1<System.Object>>::Listen(System.Action,Priority) */, (Stream_1_t4289044124 *)L_0, (Action_t437523947 *)L_1, (int32_t)L_2);
		return L_3;
	}
}
// System.IDisposable ZergRush.InorganicCollection`1<System.Object>::Bind(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
extern "C"  Il2CppObject * InorganicCollection_1_Bind_m1502836121_gshared (InorganicCollection_1_t3450977518 * __this, Action_1_t1451390511 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t1451390511 * L_0 = ___action0;
		NullCheck((Action_1_t1451390511 *)L_0);
		((  void (*) (Action_1_t1451390511 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43)->method)((Action_1_t1451390511 *)L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43));
		Stream_1_t4289044124 * L_1 = (Stream_1_t4289044124 *)__this->get_updates_8();
		Action_1_t1451390511 * L_2 = ___action0;
		int32_t L_3 = ___p1;
		NullCheck((Stream_1_t4289044124 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1451390511 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Collections.Generic.ICollection`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t4289044124 *)L_1, (Action_1_t1451390511 *)L_2, (int32_t)L_3);
		return L_4;
	}
}
// System.IDisposable ZergRush.InorganicCollection`1<System.Object>::ListenUpdates(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
extern "C"  Il2CppObject * InorganicCollection_1_ListenUpdates_m1688181143_gshared (InorganicCollection_1_t3450977518 * __this, Action_1_t1451390511 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)__this->get_updates_8();
		Action_1_t1451390511 * L_1 = ___reaction0;
		int32_t L_2 = ___p1;
		NullCheck((Stream_1_t4289044124 *)L_0);
		Il2CppObject * L_3 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1451390511 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Collections.Generic.ICollection`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t4289044124 *)L_0, (Action_1_t1451390511 *)L_1, (int32_t)L_2);
		return L_3;
	}
}
// System.Collections.Generic.ICollection`1<T> ZergRush.InorganicCollection`1<System.Object>::get_value()
extern "C"  Il2CppObject* InorganicCollection_1_get_value_m3124552170_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object ZergRush.InorganicCollection`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * InorganicCollection_1_get_valueObject_m2207754504_gshared (InorganicCollection_1_t3450977518 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::.ctor()
extern "C"  void FilterCollection_1__ctor_m3312569187_gshared (FilterCollection_1_t1215889544 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Collection_1_t2806094150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Collections.IEnumerator ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FilterCollection_1_System_Collections_IEnumerable_GetEnumerator_m3622164014_gshared (FilterCollection_1_t1215889544 * __this, const MethodInfo* method)
{
	{
		NullCheck((FilterCollection_1_t1215889544 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (FilterCollection_1_t1215889544 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((FilterCollection_1_t1215889544 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_0;
	}
}
// System.IDisposable ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::Listen(System.Action`1<UniRx.CollectionAddEvent`1<T>>,System.Action`1<CollectionRemoveEvent`1<T>>,System.Action`1<CollectionMoveEvent`1<T>>,System.Action`1<CollectionReplaceEvent`1<T>>,System.Action)
extern "C"  Il2CppObject * FilterCollection_1_Listen_m3372260382_gshared (FilterCollection_1_t1215889544 * __this, Action_1_t2564974692 * ___onAdd0, Action_1_t1046711963 * ___onRemove1, Action_1_t3720508086 * ___onMove2, Action_1_t2645824775 * ___onReplace3, Action_t437523947 * ___onReset4, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* FilterCollection_1_GetEnumerator_m2686019359_gshared (FilterCollection_1_t1215889544 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_parent_2();
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)__this->get_predicate_3();
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Func_2_t1509682273 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		return L_3;
	}
}
// System.Int32 ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::get_Count()
extern "C"  int32_t FilterCollection_1_get_Count_m3310743833_gshared (FilterCollection_1_t1215889544 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_parent_2();
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)__this->get_predicate_3();
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Func_2_t1509682273 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// T ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::ElementAt(System.Int32)
extern "C"  Il2CppObject * FilterCollection_1_ElementAt_m760621826_gshared (FilterCollection_1_t1215889544 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_parent_2();
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)__this->get_predicate_3();
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Func_2_t1509682273 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_3 = ___index0;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_4;
	}
}
// System.Void ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::.ctor()
extern "C"  void FilterCollectionRX_1__ctor_m1749687145_gshared (FilterCollectionRX_1_t3654614494 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FilterCollectionRX_1_System_Collections_IEnumerable_GetEnumerator_m2985965928_gshared (FilterCollectionRX_1_t3654614494 * __this, const MethodInfo* method)
{
	{
		NullCheck((FilterCollectionRX_1_t3654614494 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<T> ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::GetEnumerator() */, (FilterCollectionRX_1_t3654614494 *)__this);
		return L_0;
	}
}
// System.IDisposable ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::Listen(System.Action`1<UniRx.CollectionAddEvent`1<T>>,System.Action`1<CollectionRemoveEvent`1<T>>,System.Action`1<CollectionMoveEvent`1<T>>,System.Action`1<CollectionReplaceEvent`1<T>>,System.Action)
extern "C"  Il2CppObject * FilterCollectionRX_1_Listen_m4051451108_gshared (FilterCollectionRX_1_t3654614494 * __this, Action_1_t2564974692 * ___onAdd0, Action_1_t1046711963 * ___onRemove1, Action_1_t3720508086 * ___onMove2, Action_1_t2645824775 * ___onReplace3, Action_t437523947 * ___onReset4, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* FilterCollectionRX_1_GetEnumerator_m4175857701_gshared (FilterCollectionRX_1_t3654614494 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_parent_0();
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)__this->get_predicate_1();
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Func_2_t1509682273 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2);
		return L_3;
	}
}
// System.Int32 ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::get_Count()
extern "C"  int32_t FilterCollectionRX_1_get_Count_m2948975007_gshared (FilterCollectionRX_1_t3654614494 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_parent_0();
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)__this->get_predicate_1();
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Func_2_t1509682273 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_2;
	}
}
// T ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::ElementAt(System.Int32)
extern "C"  Il2CppObject * FilterCollectionRX_1_ElementAt_m3272936136_gshared (FilterCollectionRX_1_t3654614494 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_parent_0();
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)__this->get_predicate_1();
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Func_2_t1509682273 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_3 = ___index0;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_4;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1<System.Object>::.ctor()
extern "C"  void U3CFilterU3Ec__AnonStorey11E_1__ctor_m671944929_gshared (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1<System.Object>::<>m__1AB(System.Boolean)
extern "C"  void U3CFilterU3Ec__AnonStorey11E_1_U3CU3Em__1AB_m3060285249_gshared (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * __this, bool ___b0, const MethodInfo* method)
{
	{
		bool L_0 = ___b0;
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		U3CFilterU3Ec__AnonStorey11D_1_t441028593 * L_1 = (U3CFilterU3Ec__AnonStorey11D_1_t441028593 *)__this->get_U3CU3Ef__refU24285_1();
		NullCheck(L_1);
		InorganicCollection_1_t3450977518 * L_2 = (InorganicCollection_1_t3450977518 *)L_1->get_inorganicCollection_1();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_value_0();
		NullCheck((Collection_1_t2806094150 *)L_2);
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(24 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(!0) */, (Collection_1_t2806094150 *)L_2, (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		U3CFilterU3Ec__AnonStorey11D_1_t441028593 * L_5 = (U3CFilterU3Ec__AnonStorey11D_1_t441028593 *)__this->get_U3CU3Ef__refU24285_1();
		NullCheck(L_5);
		InorganicCollection_1_t3450977518 * L_6 = (InorganicCollection_1_t3450977518 *)L_5->get_inorganicCollection_1();
		NullCheck((InorganicCollection_1_t3450977518 *)L_6);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(47 /* System.Collections.Generic.ICollection`1<T> ZergRush.InorganicCollection`1<System.Object>::get_value() */, (InorganicCollection_1_t3450977518 *)L_6);
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_value_0();
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_7, (Il2CppObject *)L_8);
		goto IL_0059;
	}

IL_0042:
	{
		U3CFilterU3Ec__AnonStorey11D_1_t441028593 * L_9 = (U3CFilterU3Ec__AnonStorey11D_1_t441028593 *)__this->get_U3CU3Ef__refU24285_1();
		NullCheck(L_9);
		InorganicCollection_1_t3450977518 * L_10 = (InorganicCollection_1_t3450977518 *)L_9->get_inorganicCollection_1();
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_value_0();
		NullCheck((Collection_1_t2806094150 *)L_10);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(!0) */, (Collection_1_t2806094150 *)L_10, (Il2CppObject *)L_11);
	}

IL_0059:
	{
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>::.ctor()
extern "C"  void U3CFilterU3Ec__AnonStorey11D_1__ctor_m1074412565_gshared (U3CFilterU3Ec__AnonStorey11D_1_t441028593 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>::<>m__1A9(T)
extern Il2CppClass* Action_1_t359458046_il2cpp_TypeInfo_var;
extern Il2CppClass* ICell_1_t1762636318_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m2036539306_MethodInfo_var;
extern const uint32_t U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1A9_m3881350449_MetadataUsageId;
extern "C"  void U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1A9_m3881350449_gshared (U3CFilterU3Ec__AnonStorey11D_1_t441028593 * __this, Il2CppObject * ___objAdd0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1A9_m3881350449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * V_1 = NULL;
	{
		U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * L_0 = (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_1 = (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 *)L_0;
		U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24285_1(__this);
		U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * L_2 = V_1;
		Il2CppObject * L_3 = ___objAdd0;
		NullCheck(L_2);
		L_2->set_value_0(L_3);
		Func_2_t3061313250 * L_4 = (Func_2_t3061313250 *)__this->get_predicate_0();
		U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * L_5 = V_1;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_value_0();
		NullCheck((Func_2_t3061313250 *)L_4);
		Il2CppObject* L_7 = ((  Il2CppObject* (*) (Func_2_t3061313250 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t3061313250 *)L_4, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * L_8 = V_1;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t359458046 * L_10 = (Action_1_t359458046 *)il2cpp_codegen_object_new(Action_1_t359458046_il2cpp_TypeInfo_var);
		Action_1__ctor_m2036539306(L_10, (Il2CppObject *)L_8, (IntPtr_t)L_9, /*hidden argument*/Action_1__ctor_m2036539306_MethodInfo_var);
		NullCheck((Il2CppObject*)L_7);
		Il2CppObject * L_11 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Boolean>::Bind(System.Action`1<T>,Priority) */, ICell_1_t1762636318_il2cpp_TypeInfo_var, (Il2CppObject*)L_7, (Action_1_t359458046 *)L_10, (int32_t)1);
		V_0 = (Il2CppObject *)L_11;
		Dictionary_2_t321272808 * L_12 = (Dictionary_2_t321272808 *)__this->get_disposables_2();
		U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * L_13 = V_1;
		NullCheck(L_13);
		Il2CppObject * L_14 = (Il2CppObject *)L_13->get_value_0();
		Il2CppObject * L_15 = V_0;
		NullCheck((Dictionary_2_t321272808 *)L_12);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.IDisposable>::set_Item(!0,!1) */, (Dictionary_2_t321272808 *)L_12, (Il2CppObject *)L_14, (Il2CppObject *)L_15);
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>::<>m__1AA(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1AA_m3881588777_MetadataUsageId;
extern "C"  void U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1AA_m3881588777_gshared (U3CFilterU3Ec__AnonStorey11D_1_t441028593 * __this, Il2CppObject * ___objRemove0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1AA_m3881588777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		InorganicCollection_1_t3450977518 * L_0 = (InorganicCollection_1_t3450977518 *)__this->get_inorganicCollection_1();
		Il2CppObject * L_1 = ___objRemove0;
		NullCheck((Collection_1_t2806094150 *)L_0);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(!0) */, (Collection_1_t2806094150 *)L_0, (Il2CppObject *)L_1);
		Dictionary_2_t321272808 * L_2 = (Dictionary_2_t321272808 *)__this->get_disposables_2();
		Il2CppObject * L_3 = ___objRemove0;
		NullCheck((Dictionary_2_t321272808 *)L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.Object,System.IDisposable>::get_Item(!0) */, (Dictionary_2_t321272808 *)L_2, (Il2CppObject *)L_3);
		V_0 = (Il2CppObject *)L_4;
		Dictionary_2_t321272808 * L_5 = (Dictionary_2_t321272808 *)__this->get_disposables_2();
		Il2CppObject * L_6 = ___objRemove0;
		NullCheck((Dictionary_2_t321272808 *)L_5);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.IDisposable>::Remove(!0) */, (Dictionary_2_t321272808 *)L_5, (Il2CppObject *)L_6);
		Il2CppObject * L_7 = V_0;
		NullCheck((Il2CppObject *)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_7);
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::.ctor()
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1__ctor_m3438604360_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::<>m__1A5(UniRx.Unit)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A5_m2736562074_MetadataUsageId;
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A5_m2736562074_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A5_m2736562074_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_onRemove_0();
		if (!L_0)
		{
			goto IL_004a;
		}
	}
	{
		Collection_1_t2806094150 * L_1 = (Collection_1_t2806094150 *)__this->get_collection_1();
		NullCheck((Collection_1_t2806094150 *)L_1);
		Il2CppObject* L_2 = VirtFuncInvoker0< Il2CppObject* >::Invoke(27 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator() */, (Collection_1_t2806094150 *)L_1);
		V_1 = (Il2CppObject*)L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_001c:
		{
			Il2CppObject* L_3 = V_1;
			NullCheck((Il2CppObject*)L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_3);
			V_0 = (Il2CppObject *)L_4;
			Action_1_t985559125 * L_5 = (Action_1_t985559125 *)__this->get_onRemove_0();
			Il2CppObject * L_6 = V_0;
			NullCheck((Action_1_t985559125 *)L_5);
			((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		}

IL_002f:
		{
			Il2CppObject* L_7 = V_1;
			NullCheck((Il2CppObject *)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_7);
			if (L_8)
			{
				goto IL_001c;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_9 = V_1;
			if (L_9)
			{
				goto IL_0043;
			}
		}

IL_0042:
		{
			IL2CPP_END_FINALLY(63)
		}

IL_0043:
		{
			Il2CppObject* L_10 = V_1;
			NullCheck((Il2CppObject *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		Collection_1_t2806094150 * L_11 = (Collection_1_t2806094150 *)__this->get_collection_1();
		NullCheck((Collection_1_t2806094150 *)L_11);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear() */, (Collection_1_t2806094150 *)L_11);
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::<>m__1A6(UniRx.CollectionAddEvent`1<T>)
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A6_m2066254805_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, CollectionAddEvent_1_t2416521987  ___add0, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_func_2();
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionAddEvent_1_t2416521987 *)(&___add0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Collection_1_t2806094150 * L_2 = (Collection_1_t2806094150 *)__this->get_collection_1();
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionAddEvent_1_t2416521987 *)(&___add0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2806094150 *)L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(!0) */, (Collection_1_t2806094150 *)L_2, (Il2CppObject *)L_3);
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::<>m__1A7(CollectionReplaceEvent`1<T>)
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A7_m2666331465_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, CollectionReplaceEvent_1_t2497372070 * ___replaceEvent0, const MethodInfo* method)
{
	{
		Collection_1_t2806094150 * L_0 = (Collection_1_t2806094150 *)__this->get_collection_1();
		CollectionReplaceEvent_1_t2497372070 * L_1 = ___replaceEvent0;
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t2497372070 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Collection_1_t2806094150 *)L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(!0) */, (Collection_1_t2806094150 *)L_0, (Il2CppObject *)L_2);
		Collection_1_t2806094150 * L_3 = (Collection_1_t2806094150 *)__this->get_collection_1();
		CollectionReplaceEvent_1_t2497372070 * L_4 = ___replaceEvent0;
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((CollectionReplaceEvent_1_t2497372070 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Collection_1_t2806094150 *)L_3);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(!0) */, (Collection_1_t2806094150 *)L_3, (Il2CppObject *)L_5);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)__this->get_func_2();
		CollectionReplaceEvent_1_t2497372070 * L_7 = ___replaceEvent0;
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t2497372070 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_onRemove_0();
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		Action_1_t985559125 * L_10 = (Action_1_t985559125 *)__this->get_onRemove_0();
		CollectionReplaceEvent_1_t2497372070 * L_11 = ___replaceEvent0;
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)L_11);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((CollectionReplaceEvent_1_t2497372070 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Action_1_t985559125 *)L_10);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_10, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0050:
	{
		return;
	}
}
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::<>m__1A8(CollectionRemoveEvent`1<T>)
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A8_m2901653052_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, CollectionRemoveEvent_1_t898259258 * ___remove0, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_onRemove_0();
		CollectionRemoveEvent_1_t898259258 * L_1 = ___remove0;
		NullCheck((CollectionRemoveEvent_1_t898259258 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((CollectionRemoveEvent_1_t898259258 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Collection_1_t2806094150 * L_3 = (Collection_1_t2806094150 *)__this->get_collection_1();
		CollectionRemoveEvent_1_t898259258 * L_4 = ___remove0;
		NullCheck((CollectionRemoveEvent_1_t898259258 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((CollectionRemoveEvent_1_t898259258 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((Collection_1_t2806094150 *)L_3);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(!0) */, (Collection_1_t2806094150 *)L_3, (Il2CppObject *)L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
