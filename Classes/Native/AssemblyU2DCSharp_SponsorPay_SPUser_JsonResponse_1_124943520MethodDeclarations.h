﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>
struct JsonResponse_1_t124943520;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m2368149244_gshared (JsonResponse_1_t124943520 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m2368149244(__this, method) ((  void (*) (JsonResponse_1_t124943520 *, const MethodInfo*))JsonResponse_1__ctor_m2368149244_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m4147658579_gshared (JsonResponse_1_t124943520 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m4147658579(__this, method) ((  String_t* (*) (JsonResponse_1_t124943520 *, const MethodInfo*))JsonResponse_1_get_key_m4147658579_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m410598918_gshared (JsonResponse_1_t124943520 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m410598918(__this, ___value0, method) ((  void (*) (JsonResponse_1_t124943520 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m410598918_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::get_value()
extern "C"  Nullable_1_t3097043249  JsonResponse_1_get_value_m3315592833_gshared (JsonResponse_1_t124943520 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m3315592833(__this, method) ((  Nullable_1_t3097043249  (*) (JsonResponse_1_t124943520 *, const MethodInfo*))JsonResponse_1_get_value_m3315592833_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m2751188240_gshared (JsonResponse_1_t124943520 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m2751188240(__this, ___value0, method) ((  void (*) (JsonResponse_1_t124943520 *, Nullable_1_t3097043249 , const MethodInfo*))JsonResponse_1_set_value_m2751188240_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m3803057148_gshared (JsonResponse_1_t124943520 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m3803057148(__this, method) ((  String_t* (*) (JsonResponse_1_t124943520 *, const MethodInfo*))JsonResponse_1_get_error_m3803057148_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Boolean>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m2104429117_gshared (JsonResponse_1_t124943520 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m2104429117(__this, ___value0, method) ((  void (*) (JsonResponse_1_t124943520 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m2104429117_gshared)(__this, ___value0, method)
