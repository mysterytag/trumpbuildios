﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.GMFB_327/ObjWithTimes
struct ObjWithTimes_t855662345;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.Internal.GMFB_327/ObjWithTimes::.ctor()
extern "C"  void ObjWithTimes__ctor_m3027272212 (ObjWithTimes_t855662345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
