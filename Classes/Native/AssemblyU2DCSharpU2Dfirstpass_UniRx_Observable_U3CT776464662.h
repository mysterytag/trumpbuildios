﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.AsyncSubject`1<System.Object>
struct AsyncSubject_1_t1536745524;
// UniRx.Observable/<ToAsync>c__AnonStorey38`1<System.Object>
struct U3CToAsyncU3Ec__AnonStorey38_1_t1221489661;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1<System.Object>
struct  U3CToAsyncU3Ec__AnonStorey39_1_t776464662  : public Il2CppObject
{
public:
	// UniRx.AsyncSubject`1<T> UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1::subject
	AsyncSubject_1_t1536745524 * ___subject_0;
	// UniRx.Observable/<ToAsync>c__AnonStorey38`1<T> UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1::<>f__ref$56
	U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 * ___U3CU3Ef__refU2456_1;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStorey39_1_t776464662, ___subject_0)); }
	inline AsyncSubject_1_t1536745524 * get_subject_0() const { return ___subject_0; }
	inline AsyncSubject_1_t1536745524 ** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(AsyncSubject_1_t1536745524 * value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier(&___subject_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2456_1() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStorey39_1_t776464662, ___U3CU3Ef__refU2456_1)); }
	inline U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 * get_U3CU3Ef__refU2456_1() const { return ___U3CU3Ef__refU2456_1; }
	inline U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 ** get_address_of_U3CU3Ef__refU2456_1() { return &___U3CU3Ef__refU2456_1; }
	inline void set_U3CU3Ef__refU2456_1(U3CToAsyncU3Ec__AnonStorey38_1_t1221489661 * value)
	{
		___U3CU3Ef__refU2456_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2456_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
