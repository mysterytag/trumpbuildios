﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey87
struct U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Byte[]>
struct IObserver_1_t2270505063;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey87::.ctor()
extern "C"  void U3CPostAndGetBytesU3Ec__AnonStorey87__ctor_m3980287140 (U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey87::<>m__B3(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CPostAndGetBytesU3Ec__AnonStorey87_U3CU3Em__B3_m1761744227 (U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
