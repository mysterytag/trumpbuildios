﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/AutoDetachObserver`1<System.Object>
struct AutoDetachObserver_1_t2328747674;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/AutoDetachObserver`1<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AutoDetachObserver_1__ctor_m2085995981_gshared (AutoDetachObserver_1_t2328747674 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AutoDetachObserver_1__ctor_m2085995981(__this, ___observer0, ___cancel1, method) ((  void (*) (AutoDetachObserver_1_t2328747674 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AutoDetachObserver_1__ctor_m2085995981_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Observer/AutoDetachObserver`1<System.Object>::OnNext(T)
extern "C"  void AutoDetachObserver_1_OnNext_m224020901_gshared (AutoDetachObserver_1_t2328747674 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AutoDetachObserver_1_OnNext_m224020901(__this, ___value0, method) ((  void (*) (AutoDetachObserver_1_t2328747674 *, Il2CppObject *, const MethodInfo*))AutoDetachObserver_1_OnNext_m224020901_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/AutoDetachObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void AutoDetachObserver_1_OnError_m1919449780_gshared (AutoDetachObserver_1_t2328747674 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AutoDetachObserver_1_OnError_m1919449780(__this, ___error0, method) ((  void (*) (AutoDetachObserver_1_t2328747674 *, Exception_t1967233988 *, const MethodInfo*))AutoDetachObserver_1_OnError_m1919449780_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/AutoDetachObserver`1<System.Object>::OnCompleted()
extern "C"  void AutoDetachObserver_1_OnCompleted_m858954759_gshared (AutoDetachObserver_1_t2328747674 * __this, const MethodInfo* method);
#define AutoDetachObserver_1_OnCompleted_m858954759(__this, method) ((  void (*) (AutoDetachObserver_1_t2328747674 *, const MethodInfo*))AutoDetachObserver_1_OnCompleted_m858954759_gshared)(__this, method)
