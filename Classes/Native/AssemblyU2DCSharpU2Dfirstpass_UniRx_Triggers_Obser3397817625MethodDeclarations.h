﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableDeselectTrigger
struct ObservableDeselectTrigger_t3397817625;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData>
struct IObservable_1_t3305902090;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"

// System.Void UniRx.Triggers.ObservableDeselectTrigger::.ctor()
extern "C"  void ObservableDeselectTrigger__ctor_m853551154 (ObservableDeselectTrigger_t3397817625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableDeselectTrigger::UnityEngine.EventSystems.IDeselectHandler.OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableDeselectTrigger_UnityEngine_EventSystems_IDeselectHandler_OnDeselect_m2075374277 (ObservableDeselectTrigger_t3397817625 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableDeselectTrigger::OnDeselectAsObservable()
extern "C"  Il2CppObject* ObservableDeselectTrigger_OnDeselectAsObservable_m3540534485 (ObservableDeselectTrigger_t3397817625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableDeselectTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableDeselectTrigger_RaiseOnCompletedOnDestroy_m2100359787 (ObservableDeselectTrigger_t3397817625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
