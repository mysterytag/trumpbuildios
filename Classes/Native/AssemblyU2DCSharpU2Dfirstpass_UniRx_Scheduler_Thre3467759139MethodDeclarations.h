﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<UniRx.Unit>
struct U3CScheduleQueueingU3Ec__AnonStorey7A_1_t3467759139;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<UniRx.Unit>::.ctor()
extern "C"  void U3CScheduleQueueingU3Ec__AnonStorey7A_1__ctor_m91826895_gshared (U3CScheduleQueueingU3Ec__AnonStorey7A_1_t3467759139 * __this, const MethodInfo* method);
#define U3CScheduleQueueingU3Ec__AnonStorey7A_1__ctor_m91826895(__this, method) ((  void (*) (U3CScheduleQueueingU3Ec__AnonStorey7A_1_t3467759139 *, const MethodInfo*))U3CScheduleQueueingU3Ec__AnonStorey7A_1__ctor_m91826895_gshared)(__this, method)
// System.Void UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<UniRx.Unit>::<>m__A1(System.Object)
extern "C"  void U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m1453340844_gshared (U3CScheduleQueueingU3Ec__AnonStorey7A_1_t3467759139 * __this, Il2CppObject * ___callBackState0, const MethodInfo* method);
#define U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m1453340844(__this, ___callBackState0, method) ((  void (*) (U3CScheduleQueueingU3Ec__AnonStorey7A_1_t3467759139 *, Il2CppObject *, const MethodInfo*))U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m1453340844_gshared)(__this, ___callBackState0, method)
