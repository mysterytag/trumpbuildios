﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalStat
struct GlobalStat_t1134678199;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AchievControlIOS
struct  AchievControlIOS_t3809172804  : public MonoBehaviour_t3012272455
{
public:
	// GlobalStat AchievControlIOS::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_2;

public:
	inline static int32_t get_offset_of_GlobalStat_2() { return static_cast<int32_t>(offsetof(AchievControlIOS_t3809172804, ___GlobalStat_2)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_2() const { return ___GlobalStat_2; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_2() { return &___GlobalStat_2; }
	inline void set_GlobalStat_2(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_2 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
