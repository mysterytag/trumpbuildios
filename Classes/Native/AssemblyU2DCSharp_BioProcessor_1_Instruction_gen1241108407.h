﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ILiving
struct ILiving_t2639664210;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BioProcessor`1/Instruction<System.Object>
struct  Instruction_t1241108407  : public Il2CppObject
{
public:
	// ILiving BioProcessor`1/Instruction::intruder
	Il2CppObject * ___intruder_0;
	// System.Func`2<T,T> BioProcessor`1/Instruction::instruction
	Func_2_t2135783352 * ___instruction_1;

public:
	inline static int32_t get_offset_of_intruder_0() { return static_cast<int32_t>(offsetof(Instruction_t1241108407, ___intruder_0)); }
	inline Il2CppObject * get_intruder_0() const { return ___intruder_0; }
	inline Il2CppObject ** get_address_of_intruder_0() { return &___intruder_0; }
	inline void set_intruder_0(Il2CppObject * value)
	{
		___intruder_0 = value;
		Il2CppCodeGenWriteBarrier(&___intruder_0, value);
	}

	inline static int32_t get_offset_of_instruction_1() { return static_cast<int32_t>(offsetof(Instruction_t1241108407, ___instruction_1)); }
	inline Func_2_t2135783352 * get_instruction_1() const { return ___instruction_1; }
	inline Func_2_t2135783352 ** get_address_of_instruction_1() { return &___instruction_1; }
	inline void set_instruction_1(Func_2_t2135783352 * value)
	{
		___instruction_1 = value;
		Il2CppCodeGenWriteBarrier(&___instruction_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
