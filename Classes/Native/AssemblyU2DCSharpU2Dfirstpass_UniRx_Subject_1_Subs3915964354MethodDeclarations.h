﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<System.Object>>
struct Subscription_t3915964354;
// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct Subject_1_t3779785156;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m247878534_gshared (Subscription_t3915964354 * __this, Subject_1_t3779785156 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m247878534(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t3915964354 *, Subject_1_t3779785156 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m247878534_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m262938288_gshared (Subscription_t3915964354 * __this, const MethodInfo* method);
#define Subscription_Dispose_m262938288(__this, method) ((  void (*) (Subscription_t3915964354 *, const MethodInfo*))Subscription_Dispose_m262938288_gshared)(__this, method)
