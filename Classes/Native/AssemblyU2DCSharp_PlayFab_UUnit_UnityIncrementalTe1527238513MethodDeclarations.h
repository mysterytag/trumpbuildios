﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.UnityIncrementalTestRunner
struct UnityIncrementalTestRunner_t1527238513;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.UUnit.UnityIncrementalTestRunner::.ctor()
extern "C"  void UnityIncrementalTestRunner__ctor_m2699254756 (UnityIncrementalTestRunner_t1527238513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UnityIncrementalTestRunner::Start()
extern "C"  void UnityIncrementalTestRunner_Start_m1646392548 (UnityIncrementalTestRunner_t1527238513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UnityIncrementalTestRunner::Update()
extern "C"  void UnityIncrementalTestRunner_Update_m3799380905 (UnityIncrementalTestRunner_t1527238513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
