﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey5A
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey5A_t2453868660;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void PlayFab.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey5A::.ctor()
extern "C"  void U3CGetGetMethodByReflectionU3Ec__AnonStorey5A__ctor_m972674491 (U3CGetGetMethodByReflectionU3Ec__AnonStorey5A_t2453868660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey5A::<>m__46(System.Object)
extern "C"  Il2CppObject * U3CGetGetMethodByReflectionU3Ec__AnonStorey5A_U3CU3Em__46_m1139767417 (U3CGetGetMethodByReflectionU3Ec__AnonStorey5A_t2453868660 * __this, Il2CppObject * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
