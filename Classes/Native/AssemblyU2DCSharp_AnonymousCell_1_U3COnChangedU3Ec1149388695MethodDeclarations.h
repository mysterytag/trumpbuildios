﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Boolean>
struct U3COnChangedU3Ec__AnonStoreyFA_t1149388695;

#include "codegen/il2cpp-codegen.h"

// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Boolean>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA__ctor_m1298588278_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyFA__ctor_m1298588278(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyFA__ctor_m1298588278_gshared)(__this, method)
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Boolean>::<>m__170(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m246956209_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 * __this, bool ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m246956209(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 *, bool, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m246956209_gshared)(__this, ____0, method)
