﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/CurrentThreadScheduler
struct CurrentThreadScheduler_t3795510072;
// UniRx.InternalUtil.SchedulerQueue
struct SchedulerQueue_t3710535235;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_S3710535235.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

// System.Void UniRx.Scheduler/CurrentThreadScheduler::.ctor()
extern "C"  void CurrentThreadScheduler__ctor_m2313806315 (CurrentThreadScheduler_t3795510072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.InternalUtil.SchedulerQueue UniRx.Scheduler/CurrentThreadScheduler::GetQueue()
extern "C"  SchedulerQueue_t3710535235 * CurrentThreadScheduler_GetQueue_m3184215866 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/CurrentThreadScheduler::SetQueue(UniRx.InternalUtil.SchedulerQueue)
extern "C"  void CurrentThreadScheduler_SetQueue_m3797564925 (Il2CppObject * __this /* static, unused */, SchedulerQueue_t3710535235 * ___newQueue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan UniRx.Scheduler/CurrentThreadScheduler::get_Time()
extern "C"  TimeSpan_t763862892  CurrentThreadScheduler_get_Time_m3742118796 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Scheduler/CurrentThreadScheduler::get_IsScheduleRequired()
extern "C"  bool CurrentThreadScheduler_get_IsScheduleRequired_m3217444734 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/CurrentThreadScheduler::Schedule(System.Action)
extern "C"  Il2CppObject * CurrentThreadScheduler_Schedule_m4034521618 (CurrentThreadScheduler_t3795510072 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/CurrentThreadScheduler::Schedule(System.TimeSpan,System.Action)
extern "C"  Il2CppObject * CurrentThreadScheduler_Schedule_m3176137192 (CurrentThreadScheduler_t3795510072 * __this, TimeSpan_t763862892  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset UniRx.Scheduler/CurrentThreadScheduler::get_Now()
extern "C"  DateTimeOffset_t3712260035  CurrentThreadScheduler_get_Now_m1113827888 (CurrentThreadScheduler_t3795510072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
