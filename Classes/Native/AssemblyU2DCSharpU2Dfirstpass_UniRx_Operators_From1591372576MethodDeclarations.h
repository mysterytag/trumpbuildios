﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable
struct FromEventObservable_t1591372576;
// System.Action`1<System.Action>
struct Action_1_t585976652;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable::.ctor(System.Action`1<System.Action>,System.Action`1<System.Action>)
extern "C"  void FromEventObservable__ctor_m843502955 (FromEventObservable_t1591372576 * __this, Action_1_t585976652 * ___addHandler0, Action_1_t585976652 * ___removeHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Operators.FromEventObservable::SubscribeCore(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  Il2CppObject * FromEventObservable_SubscribeCore_m3786309221 (FromEventObservable_t1591372576 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
