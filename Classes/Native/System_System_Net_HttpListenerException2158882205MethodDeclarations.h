﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpListenerException
struct HttpListenerException_t2158882205;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Net.HttpListenerException::.ctor()
extern "C"  void HttpListenerException__ctor_m326954073 (HttpListenerException_t2158882205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerException::.ctor(System.Int32)
extern "C"  void HttpListenerException__ctor_m2241193002 (HttpListenerException_t2158882205 * __this, int32_t ___errorCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerException::.ctor(System.Int32,System.String)
extern "C"  void HttpListenerException__ctor_m3589241574 (HttpListenerException_t2158882205 * __this, int32_t ___errorCode0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HttpListenerException__ctor_m425580058 (HttpListenerException_t2158882205 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.HttpListenerException::get_ErrorCode()
extern "C"  int32_t HttpListenerException_get_ErrorCode_m1849873587 (HttpListenerException_t2158882205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
