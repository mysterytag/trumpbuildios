﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlockContainerItemResult
struct UnlockContainerItemResult_t2837935741;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UnlockContainerItemResult::.ctor()
extern "C"  void UnlockContainerItemResult__ctor_m253909580 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerItemResult::get_UnlockedItemInstanceId()
extern "C"  String_t* UnlockContainerItemResult_get_UnlockedItemInstanceId_m2258041066 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::set_UnlockedItemInstanceId(System.String)
extern "C"  void UnlockContainerItemResult_set_UnlockedItemInstanceId_m908884775 (UnlockContainerItemResult_t2837935741 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerItemResult::get_UnlockedWithItemInstanceId()
extern "C"  String_t* UnlockContainerItemResult_get_UnlockedWithItemInstanceId_m1922555600 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::set_UnlockedWithItemInstanceId(System.String)
extern "C"  void UnlockContainerItemResult_set_UnlockedWithItemInstanceId_m984341377 (UnlockContainerItemResult_t2837935741 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.UnlockContainerItemResult::get_GrantedItems()
extern "C"  List_1_t1322103281 * UnlockContainerItemResult_get_GrantedItems_m3369806061 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::set_GrantedItems(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void UnlockContainerItemResult_set_GrantedItems_m693190756 (UnlockContainerItemResult_t2837935741 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.UnlockContainerItemResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t2623623230 * UnlockContainerItemResult_get_VirtualCurrency_m1043778311 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void UnlockContainerItemResult_set_VirtualCurrency_m353700354 (UnlockContainerItemResult_t2837935741 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
