﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionCount
struct XPathFunctionCount_t1359888247;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionCount::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionCount__ctor_m74396489 (XPathFunctionCount_t1359888247 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionCount::get_ReturnType()
extern "C"  int32_t XPathFunctionCount_get_ReturnType_m4130170227 (XPathFunctionCount_t1359888247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionCount::get_Peer()
extern "C"  bool XPathFunctionCount_get_Peer_m2430742375 (XPathFunctionCount_t1359888247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionCount::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionCount_Evaluate_m3315815524 (XPathFunctionCount_t1359888247 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionCount::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool XPathFunctionCount_EvaluateBoolean_m3382145239 (XPathFunctionCount_t1359888247 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionCount::ToString()
extern "C"  String_t* XPathFunctionCount_ToString_m4291378413 (XPathFunctionCount_t1359888247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
