﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1771086527MethodDeclarations.h"

// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::.ctor(System.Object,System.IntPtr)
#define ThreadSafeDictionaryValueFactory_2__ctor_m2473053191(__this, ___object0, ___method1, method) ((  void (*) (ThreadSafeDictionaryValueFactory_2_t360228411 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ThreadSafeDictionaryValueFactory_2__ctor_m191619557_gshared)(__this, ___object0, ___method1, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::Invoke(TKey)
#define ThreadSafeDictionaryValueFactory_2_Invoke_m704328502(__this, ___key0, method) ((  Il2CppObject* (*) (ThreadSafeDictionaryValueFactory_2_t360228411 *, Type_t *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_Invoke_m808333140_gshared)(__this, ___key0, method)
// System.IAsyncResult PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::BeginInvoke(TKey,System.AsyncCallback,System.Object)
#define ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3835393991(__this, ___key0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t360228411 *, Type_t *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3493335453_gshared)(__this, ___key0, ___callback1, ___object2, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::EndInvoke(System.IAsyncResult)
#define ThreadSafeDictionaryValueFactory_2_EndInvoke_m295024055(__this, ___result0, method) ((  Il2CppObject* (*) (ThreadSafeDictionaryValueFactory_2_t360228411 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_EndInvoke_m2172200533_gshared)(__this, ___result0, method)
