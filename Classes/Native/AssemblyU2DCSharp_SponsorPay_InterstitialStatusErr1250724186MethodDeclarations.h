﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.InterstitialStatusErrorHandler
struct InterstitialStatusErrorHandler_t1250724186;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.InterstitialStatusErrorHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void InterstitialStatusErrorHandler__ctor_m579445783 (InterstitialStatusErrorHandler_t1250724186 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialStatusErrorHandler::Invoke(System.String)
extern "C"  void InterstitialStatusErrorHandler_Invoke_m1967380337 (InterstitialStatusErrorHandler_t1250724186 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_InterstitialStatusErrorHandler_t1250724186(Il2CppObject* delegate, String_t* ___message0);
// System.IAsyncResult SponsorPay.InterstitialStatusErrorHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InterstitialStatusErrorHandler_BeginInvoke_m1940212350 (InterstitialStatusErrorHandler_t1250724186 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialStatusErrorHandler::EndInvoke(System.IAsyncResult)
extern "C"  void InterstitialStatusErrorHandler_EndInvoke_m1015593383 (InterstitialStatusErrorHandler_t1250724186 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
