﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Double,System.Double,System.Double>
struct SelectMany_t2490675942;
// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Double,System.Double,System.Double>
struct SelectManyOuterObserver_t3624003377;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Double,System.Double,System.Double>::.ctor(UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<TSource,TCollection,TResult>,TSource,System.IDisposable)
extern "C"  void SelectMany__ctor_m2904621798_gshared (SelectMany_t2490675942 * __this, SelectManyOuterObserver_t3624003377 * ___parent0, double ___value1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectMany__ctor_m2904621798(__this, ___parent0, ___value1, ___cancel2, method) ((  void (*) (SelectMany_t2490675942 *, SelectManyOuterObserver_t3624003377 *, double, Il2CppObject *, const MethodInfo*))SelectMany__ctor_m2904621798_gshared)(__this, ___parent0, ___value1, ___cancel2, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Double,System.Double,System.Double>::OnNext(TCollection)
extern "C"  void SelectMany_OnNext_m1332388786_gshared (SelectMany_t2490675942 * __this, double ___value0, const MethodInfo* method);
#define SelectMany_OnNext_m1332388786(__this, ___value0, method) ((  void (*) (SelectMany_t2490675942 *, double, const MethodInfo*))SelectMany_OnNext_m1332388786_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Double,System.Double,System.Double>::OnError(System.Exception)
extern "C"  void SelectMany_OnError_m2397716095_gshared (SelectMany_t2490675942 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectMany_OnError_m2397716095(__this, ___error0, method) ((  void (*) (SelectMany_t2490675942 *, Exception_t1967233988 *, const MethodInfo*))SelectMany_OnError_m2397716095_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Double,System.Double,System.Double>::OnCompleted()
extern "C"  void SelectMany_OnCompleted_m1639675986_gshared (SelectMany_t2490675942 * __this, const MethodInfo* method);
#define SelectMany_OnCompleted_m1639675986(__this, method) ((  void (*) (SelectMany_t2490675942 *, const MethodInfo*))SelectMany_OnCompleted_m1639675986_gshared)(__this, method)
