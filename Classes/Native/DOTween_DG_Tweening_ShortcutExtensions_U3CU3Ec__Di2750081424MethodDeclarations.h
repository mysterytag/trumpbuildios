﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0
struct U3CU3Ec__DisplayClass19_0_t2750081424;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass19_0__ctor_m1318063238 (U3CU3Ec__DisplayClass19_0_t2750081424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::<DOColor>b__0()
extern "C"  Color_t1588175760  U3CU3Ec__DisplayClass19_0_U3CDOColorU3Eb__0_m951356141 (U3CU3Ec__DisplayClass19_0_t2750081424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass19_0_U3CDOColorU3Eb__1_m2713989809 (U3CU3Ec__DisplayClass19_0_t2750081424 * __this, Color_t1588175760  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
