﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CoroutineExtension/Stub
struct Stub_t2587854;
// System.Action`1<System.Action>
struct Action_1_t585976652;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroutineExtension/<DoAndWaitCallback>c__IteratorA
struct  U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692  : public Il2CppObject
{
public:
	// CoroutineExtension/Stub CoroutineExtension/<DoAndWaitCallback>c__IteratorA::<stub>__0
	Stub_t2587854 * ___U3CstubU3E__0_0;
	// System.Action`1<System.Action> CoroutineExtension/<DoAndWaitCallback>c__IteratorA::action
	Action_1_t585976652 * ___action_1;
	// System.Int32 CoroutineExtension/<DoAndWaitCallback>c__IteratorA::$PC
	int32_t ___U24PC_2;
	// System.Object CoroutineExtension/<DoAndWaitCallback>c__IteratorA::$current
	Il2CppObject * ___U24current_3;
	// System.Action`1<System.Action> CoroutineExtension/<DoAndWaitCallback>c__IteratorA::<$>action
	Action_1_t585976652 * ___U3CU24U3Eaction_4;

public:
	inline static int32_t get_offset_of_U3CstubU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692, ___U3CstubU3E__0_0)); }
	inline Stub_t2587854 * get_U3CstubU3E__0_0() const { return ___U3CstubU3E__0_0; }
	inline Stub_t2587854 ** get_address_of_U3CstubU3E__0_0() { return &___U3CstubU3E__0_0; }
	inline void set_U3CstubU3E__0_0(Stub_t2587854 * value)
	{
		___U3CstubU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstubU3E__0_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692, ___action_1)); }
	inline Action_1_t585976652 * get_action_1() const { return ___action_1; }
	inline Action_1_t585976652 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t585976652 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eaction_4() { return static_cast<int32_t>(offsetof(U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692, ___U3CU24U3Eaction_4)); }
	inline Action_1_t585976652 * get_U3CU24U3Eaction_4() const { return ___U3CU24U3Eaction_4; }
	inline Action_1_t585976652 ** get_address_of_U3CU24U3Eaction_4() { return &___U3CU24U3Eaction_4; }
	inline void set_U3CU24U3Eaction_4(Action_1_t585976652 * value)
	{
		___U3CU24U3Eaction_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eaction_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
