﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TimestampObservable`1<System.Object>
struct TimestampObservable_1_t2503612568;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2869846002.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimestampObservable`1/Timestamp<System.Object>
struct  Timestamp_t3118418687  : public OperatorObserverBase_2_t2869846002
{
public:
	// UniRx.Operators.TimestampObservable`1<T> UniRx.Operators.TimestampObservable`1/Timestamp::parent
	TimestampObservable_1_t2503612568 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Timestamp_t3118418687, ___parent_2)); }
	inline TimestampObservable_1_t2503612568 * get_parent_2() const { return ___parent_2; }
	inline TimestampObservable_1_t2503612568 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(TimestampObservable_1_t2503612568 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
