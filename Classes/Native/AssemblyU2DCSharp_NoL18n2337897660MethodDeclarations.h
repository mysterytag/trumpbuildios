﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoL18n
struct NoL18n_t2337897660;

#include "codegen/il2cpp-codegen.h"

// System.Void NoL18n::.ctor()
extern "C"  void NoL18n__ctor_m1841855327 (NoL18n_t2337897660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoL18n::Start()
extern "C"  void NoL18n_Start_m788993119 (NoL18n_t2337897660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoL18n::Update()
extern "C"  void NoL18n_Update_m2989802382 (NoL18n_t2337897660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
