﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<RegisterForIOSPushNotification>c__AnonStoreyB1
struct U3CRegisterForIOSPushNotificationU3Ec__AnonStoreyB1_t961287584;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<RegisterForIOSPushNotification>c__AnonStoreyB1::.ctor()
extern "C"  void U3CRegisterForIOSPushNotificationU3Ec__AnonStoreyB1__ctor_m1615541779 (U3CRegisterForIOSPushNotificationU3Ec__AnonStoreyB1_t961287584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<RegisterForIOSPushNotification>c__AnonStoreyB1::<>m__9E(PlayFab.CallRequestContainer)
extern "C"  void U3CRegisterForIOSPushNotificationU3Ec__AnonStoreyB1_U3CU3Em__9E_m3175535581 (U3CRegisterForIOSPushNotificationU3Ec__AnonStoreyB1_t961287584 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
