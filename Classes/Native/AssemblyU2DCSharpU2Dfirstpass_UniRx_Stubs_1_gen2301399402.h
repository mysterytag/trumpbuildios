﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Single>
struct Action_1_t1106661726;
// System.Func`2<System.Single,System.Single>
struct Func_2_t3464793460;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<System.Single>
struct  Stubs_1_t2301399402  : public Il2CppObject
{
public:

public:
};

struct Stubs_1_t2301399402_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t1106661726 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t3464793460 * ___Identity_1;
	// System.Action`1<T> UniRx.Stubs`1::<>f__am$cache2
	Action_1_t1106661726 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<T,T> UniRx.Stubs`1::<>f__am$cache3
	Func_2_t3464793460 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t2301399402_StaticFields, ___Ignore_0)); }
	inline Action_1_t1106661726 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t1106661726 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t1106661726 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier(&___Ignore_0, value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t2301399402_StaticFields, ___Identity_1)); }
	inline Func_2_t3464793460 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t3464793460 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t3464793460 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier(&___Identity_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Stubs_1_t2301399402_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Action_1_t1106661726 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Action_1_t1106661726 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Action_1_t1106661726 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Stubs_1_t2301399402_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t3464793460 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t3464793460 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t3464793460 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
