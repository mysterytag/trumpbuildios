﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest
struct GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequest__ctor_m14562139 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::get_PSNAccountIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromPSNAccountIDsRequest_get_PSNAccountIDs_m237230229 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::set_PSNAccountIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequest_set_PSNAccountIDs_m717555686 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::get_IssuerId()
extern "C"  Nullable_1_t1438485399  GetPlayFabIDsFromPSNAccountIDsRequest_get_IssuerId_m22734325 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::set_IssuerId(System.Nullable`1<System.Int32>)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequest_set_IssuerId_m982794196 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
