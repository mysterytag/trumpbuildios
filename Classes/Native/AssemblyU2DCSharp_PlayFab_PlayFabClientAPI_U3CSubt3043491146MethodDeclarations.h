﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<SubtractUserVirtualCurrency>c__AnonStoreyAA
struct U3CSubtractUserVirtualCurrencyU3Ec__AnonStoreyAA_t3043491146;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<SubtractUserVirtualCurrency>c__AnonStoreyAA::.ctor()
extern "C"  void U3CSubtractUserVirtualCurrencyU3Ec__AnonStoreyAA__ctor_m2158312777 (U3CSubtractUserVirtualCurrencyU3Ec__AnonStoreyAA_t3043491146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<SubtractUserVirtualCurrency>c__AnonStoreyAA::<>m__97(PlayFab.CallRequestContainer)
extern "C"  void U3CSubtractUserVirtualCurrencyU3Ec__AnonStoreyAA_U3CU3Em__97_m3858359557 (U3CSubtractUserVirtualCurrencyU3Ec__AnonStoreyAA_t3043491146 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
