﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AddSharedGroupMembersResponseCallback
struct AddSharedGroupMembersResponseCallback_t4255025350;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AddSharedGroupMembersRequest
struct AddSharedGroupMembersRequest_t3205265791;
// PlayFab.ClientModels.AddSharedGroupMembersResult
struct AddSharedGroupMembersResult_t3325132109;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddSharedGr3205265791.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddSharedGr3325132109.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AddSharedGroupMembersResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddSharedGroupMembersResponseCallback__ctor_m2584235605 (AddSharedGroupMembersResponseCallback_t4255025350 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddSharedGroupMembersResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AddSharedGroupMembersRequest,PlayFab.ClientModels.AddSharedGroupMembersResult,PlayFab.PlayFabError,System.Object)
extern "C"  void AddSharedGroupMembersResponseCallback_Invoke_m3761530560 (AddSharedGroupMembersResponseCallback_t4255025350 * __this, String_t* ___urlPath0, int32_t ___callId1, AddSharedGroupMembersRequest_t3205265791 * ___request2, AddSharedGroupMembersResult_t3325132109 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AddSharedGroupMembersResponseCallback_t4255025350(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AddSharedGroupMembersRequest_t3205265791 * ___request2, AddSharedGroupMembersResult_t3325132109 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AddSharedGroupMembersResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AddSharedGroupMembersRequest,PlayFab.ClientModels.AddSharedGroupMembersResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddSharedGroupMembersResponseCallback_BeginInvoke_m2449779641 (AddSharedGroupMembersResponseCallback_t4255025350 * __this, String_t* ___urlPath0, int32_t ___callId1, AddSharedGroupMembersRequest_t3205265791 * ___request2, AddSharedGroupMembersResult_t3325132109 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddSharedGroupMembersResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddSharedGroupMembersResponseCallback_EndInvoke_m1812083429 (AddSharedGroupMembersResponseCallback_t4255025350 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
