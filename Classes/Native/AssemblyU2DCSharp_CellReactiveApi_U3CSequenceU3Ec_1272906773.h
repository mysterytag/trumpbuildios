﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Collections.Generic.IEnumerable`1<System.Object>>
struct Action_1_t3857713481;
// CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>
struct U3CSequenceU3Ec__AnonStorey110_1_t1717931772;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1<System.Object>
struct  U3CSequenceU3Ec__AnonStorey111_1_t1272906773  : public Il2CppObject
{
public:
	// System.Action`1<System.Collections.Generic.IEnumerable`1<T>> CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1::reaction
	Action_1_t3857713481 * ___reaction_0;
	// CellReactiveApi/<Sequence>c__AnonStorey110`1<T> CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1::<>f__ref$272
	U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * ___U3CU3Ef__refU24272_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CSequenceU3Ec__AnonStorey111_1_t1272906773, ___reaction_0)); }
	inline Action_1_t3857713481 * get_reaction_0() const { return ___reaction_0; }
	inline Action_1_t3857713481 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_1_t3857713481 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24272_1() { return static_cast<int32_t>(offsetof(U3CSequenceU3Ec__AnonStorey111_1_t1272906773, ___U3CU3Ef__refU24272_1)); }
	inline U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * get_U3CU3Ef__refU24272_1() const { return ___U3CU3Ef__refU24272_1; }
	inline U3CSequenceU3Ec__AnonStorey110_1_t1717931772 ** get_address_of_U3CU3Ef__refU24272_1() { return &___U3CU3Ef__refU24272_1; }
	inline void set_U3CU3Ef__refU24272_1(U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * value)
	{
		___U3CU3Ef__refU24272_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24272_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
