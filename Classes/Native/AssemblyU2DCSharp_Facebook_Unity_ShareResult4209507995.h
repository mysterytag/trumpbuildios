﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase3940793997.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareResult
struct  ShareResult_t4209507995  : public ResultBase_t3940793997
{
public:
	// System.String Facebook.Unity.ShareResult::<PostId>k__BackingField
	String_t* ___U3CPostIdU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CPostIdU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ShareResult_t4209507995, ___U3CPostIdU3Ek__BackingField_9)); }
	inline String_t* get_U3CPostIdU3Ek__BackingField_9() const { return ___U3CPostIdU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CPostIdU3Ek__BackingField_9() { return &___U3CPostIdU3Ek__BackingField_9; }
	inline void set_U3CPostIdU3Ek__BackingField_9(String_t* value)
	{
		___U3CPostIdU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPostIdU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
