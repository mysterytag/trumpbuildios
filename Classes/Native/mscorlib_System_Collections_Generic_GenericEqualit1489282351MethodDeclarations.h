﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<UniRx.Unit>
struct GenericEqualityComparer_1_t1489282351;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<UniRx.Unit>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1409376492_gshared (GenericEqualityComparer_1_t1489282351 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m1409376492(__this, method) ((  void (*) (GenericEqualityComparer_1_t1489282351 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m1409376492_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<UniRx.Unit>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m846903359_gshared (GenericEqualityComparer_1_t1489282351 * __this, Unit_t2558286038  ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m846903359(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t1489282351 *, Unit_t2558286038 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m846903359_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<UniRx.Unit>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2519384893_gshared (GenericEqualityComparer_1_t1489282351 * __this, Unit_t2558286038  ___x0, Unit_t2558286038  ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m2519384893(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t1489282351 *, Unit_t2558286038 , Unit_t2558286038 , const MethodInfo*))GenericEqualityComparer_1_Equals_m2519384893_gshared)(__this, ___x0, ___y1, method)
