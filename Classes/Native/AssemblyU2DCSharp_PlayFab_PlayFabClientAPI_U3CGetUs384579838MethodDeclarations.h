﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetUserCombinedInfo>c__AnonStorey74
struct U3CGetUserCombinedInfoU3Ec__AnonStorey74_t384579838;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetUserCombinedInfo>c__AnonStorey74::.ctor()
extern "C"  void U3CGetUserCombinedInfoU3Ec__AnonStorey74__ctor_m533380117 (U3CGetUserCombinedInfoU3Ec__AnonStorey74_t384579838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetUserCombinedInfo>c__AnonStorey74::<>m__61(PlayFab.CallRequestContainer)
extern "C"  void U3CGetUserCombinedInfoU3Ec__AnonStorey74_U3CU3Em__61_m1550386606 (U3CGetUserCombinedInfoU3Ec__AnonStorey74_t384579838 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
