﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3998431533(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t140622909 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform>::Invoke(T)
#define Func_2_Invoke_m721650397(__this, ___arg10, method) ((  Transform_t284553113 * (*) (Func_2_t140622909 *, Sample06_ConvertToCoroutine_t617332884 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2606090768(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t140622909 *, Sample06_ConvertToCoroutine_t617332884 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m770186519(__this, ___result0, method) ((  Transform_t284553113 * (*) (Func_2_t140622909 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
