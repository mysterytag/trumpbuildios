﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IStream`1<System.Object>
struct IStream_1_t1389797411;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// CellReactiveApi/MapDisposable
struct MapDisposable_t304581564;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>
struct  U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414  : public Il2CppObject
{
public:
	// IStream`1<T2> CellReactiveApi/<Accumulate>c__AnonStoreyFB`2::stream
	Il2CppObject* ___stream_0;
	// System.Func`3<T,T2,T> CellReactiveApi/<Accumulate>c__AnonStoreyFB`2::accomulator
	Func_3_t1892209229 * ___accomulator_1;
	// CellReactiveApi/MapDisposable CellReactiveApi/<Accumulate>c__AnonStoreyFB`2::disp
	MapDisposable_t304581564 * ___disp_2;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414, ___stream_0)); }
	inline Il2CppObject* get_stream_0() const { return ___stream_0; }
	inline Il2CppObject** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject* value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}

	inline static int32_t get_offset_of_accomulator_1() { return static_cast<int32_t>(offsetof(U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414, ___accomulator_1)); }
	inline Func_3_t1892209229 * get_accomulator_1() const { return ___accomulator_1; }
	inline Func_3_t1892209229 ** get_address_of_accomulator_1() { return &___accomulator_1; }
	inline void set_accomulator_1(Func_3_t1892209229 * value)
	{
		___accomulator_1 = value;
		Il2CppCodeGenWriteBarrier(&___accomulator_1, value);
	}

	inline static int32_t get_offset_of_disp_2() { return static_cast<int32_t>(offsetof(U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414, ___disp_2)); }
	inline MapDisposable_t304581564 * get_disp_2() const { return ___disp_2; }
	inline MapDisposable_t304581564 ** get_address_of_disp_2() { return &___disp_2; }
	inline void set_disp_2(MapDisposable_t304581564 * value)
	{
		___disp_2 = value;
		Il2CppCodeGenWriteBarrier(&___disp_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
