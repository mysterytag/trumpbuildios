﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>
struct JsonResponse_1_t815460875;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3787560604.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m3709767063_gshared (JsonResponse_1_t815460875 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m3709767063(__this, method) ((  void (*) (JsonResponse_1_t815460875 *, const MethodInfo*))JsonResponse_1__ctor_m3709767063_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m2611620782_gshared (JsonResponse_1_t815460875 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m2611620782(__this, method) ((  String_t* (*) (JsonResponse_1_t815460875 *, const MethodInfo*))JsonResponse_1_get_key_m2611620782_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m3409468939_gshared (JsonResponse_1_t815460875 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m3409468939(__this, ___value0, method) ((  void (*) (JsonResponse_1_t815460875 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m3409468939_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::get_value()
extern "C"  Nullable_1_t3787560604  JsonResponse_1_get_value_m1939466780_gshared (JsonResponse_1_t815460875 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m1939466780(__this, method) ((  Nullable_1_t3787560604  (*) (JsonResponse_1_t815460875 *, const MethodInfo*))JsonResponse_1_get_value_m1939466780_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m448610133_gshared (JsonResponse_1_t815460875 * __this, Nullable_1_t3787560604  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m448610133(__this, ___value0, method) ((  void (*) (JsonResponse_1_t815460875 *, Nullable_1_t3787560604 , const MethodInfo*))JsonResponse_1_set_value_m448610133_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m844516759_gshared (JsonResponse_1_t815460875 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m844516759(__this, method) ((  String_t* (*) (JsonResponse_1_t815460875 *, const MethodInfo*))JsonResponse_1_get_error_m844516759_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserSexualOrientation>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m2095463682_gshared (JsonResponse_1_t815460875 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m2095463682(__this, ___value0, method) ((  void (*) (JsonResponse_1_t815460875 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m2095463682_gshared)(__this, ___value0, method)
