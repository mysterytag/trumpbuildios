﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IStream`1<System.Object>
struct IStream_1_t1389797411;
// CellReactiveApi/MapDisposable
struct MapDisposable_t304581564;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>
struct  U3CHoldU3Ec__AnonStoreyFF_1_t2863379238  : public Il2CppObject
{
public:
	// IStream`1<T> CellReactiveApi/<Hold>c__AnonStoreyFF`1::stream
	Il2CppObject* ___stream_0;
	// CellReactiveApi/MapDisposable CellReactiveApi/<Hold>c__AnonStoreyFF`1::disp
	MapDisposable_t304581564 * ___disp_1;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CHoldU3Ec__AnonStoreyFF_1_t2863379238, ___stream_0)); }
	inline Il2CppObject* get_stream_0() const { return ___stream_0; }
	inline Il2CppObject** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject* value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}

	inline static int32_t get_offset_of_disp_1() { return static_cast<int32_t>(offsetof(U3CHoldU3Ec__AnonStoreyFF_1_t2863379238, ___disp_1)); }
	inline MapDisposable_t304581564 * get_disp_1() const { return ___disp_1; }
	inline MapDisposable_t304581564 ** get_address_of_disp_1() { return &___disp_1; }
	inline void set_disp_1(MapDisposable_t304581564 * value)
	{
		___disp_1 = value;
		Il2CppCodeGenWriteBarrier(&___disp_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
