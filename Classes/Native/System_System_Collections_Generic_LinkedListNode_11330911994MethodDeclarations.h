﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_1_539097040MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<System.IDisposable>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m2854282402(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t1330911994 *, LinkedList_1_t3369050920 *, Il2CppObject *, const MethodInfo*))LinkedListNode_1__ctor_m648136130_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.IDisposable>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m3069755394(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t1330911994 *, LinkedList_1_t3369050920 *, Il2CppObject *, LinkedListNode_1_t1330911994 *, LinkedListNode_1_t1330911994 *, const MethodInfo*))LinkedListNode_1__ctor_m448391458_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.IDisposable>::Detach()
#define LinkedListNode_1_Detach_m2076393598(__this, method) ((  void (*) (LinkedListNode_1_t1330911994 *, const MethodInfo*))LinkedListNode_1_Detach_m3406254942_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.IDisposable>::get_List()
#define LinkedListNode_1_get_List_m4040538594(__this, method) ((  LinkedList_1_t3369050920 * (*) (LinkedListNode_1_t1330911994 *, const MethodInfo*))LinkedListNode_1_get_List_m3467110818_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.IDisposable>::get_Next()
#define LinkedListNode_1_get_Next_m2492301077(__this, method) ((  LinkedListNode_1_t1330911994 * (*) (LinkedListNode_1_t1330911994 *, const MethodInfo*))LinkedListNode_1_get_Next_m1427618777_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.IDisposable>::get_Previous()
#define LinkedListNode_1_get_Previous_m469558681(__this, method) ((  LinkedListNode_1_t1330911994 * (*) (LinkedListNode_1_t1330911994 *, const MethodInfo*))LinkedListNode_1_get_Previous_m3755155549_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.IDisposable>::get_Value()
#define LinkedListNode_1_get_Value_m3359244574(__this, method) ((  Il2CppObject * (*) (LinkedListNode_1_t1330911994 *, const MethodInfo*))LinkedListNode_1_get_Value_m702633824_gshared)(__this, method)
