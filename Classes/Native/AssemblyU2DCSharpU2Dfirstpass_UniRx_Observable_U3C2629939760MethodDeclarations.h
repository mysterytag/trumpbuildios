﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey42_2__ctor_m3602729391_gshared (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey42_2__ctor_m3602729391(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey42_2__ctor_m3602729391_gshared)(__this, method)
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,System.Object>::<>m__66(System.IAsyncResult)
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m1517210475_gshared (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 * __this, Il2CppObject * ___iar0, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m1517210475(__this, ___iar0, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t2629939760 *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m1517210475_gshared)(__this, ___iar0, method)
