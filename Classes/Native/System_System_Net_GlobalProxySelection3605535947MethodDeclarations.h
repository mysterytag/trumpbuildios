﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.GlobalProxySelection
struct GlobalProxySelection_t3605535947;
// System.Net.IWebProxy
struct IWebProxy_t49946189;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.GlobalProxySelection::.ctor()
extern "C"  void GlobalProxySelection__ctor_m2473576057 (GlobalProxySelection_t3605535947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.GlobalProxySelection::get_Select()
extern "C"  Il2CppObject * GlobalProxySelection_get_Select_m3561358852 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.GlobalProxySelection::set_Select(System.Net.IWebProxy)
extern "C"  void GlobalProxySelection_set_Select_m1535059441 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.GlobalProxySelection::GetEmptyWebProxy()
extern "C"  Il2CppObject * GlobalProxySelection_GetEmptyWebProxy_m3679496880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
