﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ReturnObservable`1/Return<System.Object>
struct Return_t2908717129;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ReturnObservable`1/Return<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Return__ctor_m3938282771_gshared (Return_t2908717129 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Return__ctor_m3938282771(__this, ___observer0, ___cancel1, method) ((  void (*) (Return_t2908717129 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Return__ctor_m3938282771_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<System.Object>::OnNext(T)
extern "C"  void Return_OnNext_m720311967_gshared (Return_t2908717129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Return_OnNext_m720311967(__this, ___value0, method) ((  void (*) (Return_t2908717129 *, Il2CppObject *, const MethodInfo*))Return_OnNext_m720311967_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<System.Object>::OnError(System.Exception)
extern "C"  void Return_OnError_m180884398_gshared (Return_t2908717129 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Return_OnError_m180884398(__this, ___error0, method) ((  void (*) (Return_t2908717129 *, Exception_t1967233988 *, const MethodInfo*))Return_OnError_m180884398_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<System.Object>::OnCompleted()
extern "C"  void Return_OnCompleted_m2940492801_gshared (Return_t2908717129 * __this, const MethodInfo* method);
#define Return_OnCompleted_m2940492801(__this, method) ((  void (*) (Return_t2908717129 *, const MethodInfo*))Return_OnCompleted_m2940492801_gshared)(__this, method)
