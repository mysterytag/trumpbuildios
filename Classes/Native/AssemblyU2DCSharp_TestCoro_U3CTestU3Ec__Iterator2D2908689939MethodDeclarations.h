﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestCoro/<Test>c__Iterator2D
struct U3CTestU3Ec__Iterator2D_t2908689939;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TestCoro/<Test>c__Iterator2D::.ctor()
extern "C"  void U3CTestU3Ec__Iterator2D__ctor_m3567773692 (U3CTestU3Ec__Iterator2D_t2908689939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestCoro/<Test>c__Iterator2D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestU3Ec__Iterator2D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2627369440 (U3CTestU3Ec__Iterator2D_t2908689939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestCoro/<Test>c__Iterator2D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestU3Ec__Iterator2D_System_Collections_IEnumerator_get_Current_m3871096180 (U3CTestU3Ec__Iterator2D_t2908689939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TestCoro/<Test>c__Iterator2D::MoveNext()
extern "C"  bool U3CTestU3Ec__Iterator2D_MoveNext_m464004704 (U3CTestU3Ec__Iterator2D_t2908689939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Test>c__Iterator2D::Dispose()
extern "C"  void U3CTestU3Ec__Iterator2D_Dispose_m1144898873 (U3CTestU3Ec__Iterator2D_t2908689939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Test>c__Iterator2D::Reset()
extern "C"  void U3CTestU3Ec__Iterator2D_Reset_m1214206633 (U3CTestU3Ec__Iterator2D_t2908689939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
