﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithFacebookResponseCallback
struct LoginWithFacebookResponseCallback_t2510584155;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithFacebookRequest
struct LoginWithFacebookRequest_t58367306;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithFace58367306.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithFacebookResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithFacebookResponseCallback__ctor_m1931493482 (LoginWithFacebookResponseCallback_t2510584155 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithFacebookResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithFacebookRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LoginWithFacebookResponseCallback_Invoke_m750858291 (LoginWithFacebookResponseCallback_t2510584155 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithFacebookRequest_t58367306 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithFacebookResponseCallback_t2510584155(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithFacebookRequest_t58367306 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithFacebookResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithFacebookRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithFacebookResponseCallback_BeginInvoke_m2762143152 (LoginWithFacebookResponseCallback_t2510584155 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithFacebookRequest_t58367306 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithFacebookResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithFacebookResponseCallback_EndInvoke_m3249043322 (LoginWithFacebookResponseCallback_t2510584155 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
