﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest
struct GetLeaderboardAroundCurrentUserRequest_t545060687;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::.ctor()
extern "C"  void GetLeaderboardAroundCurrentUserRequest__ctor_m4226926158 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardAroundCurrentUserRequest_get_StatisticName_m3750163809 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardAroundCurrentUserRequest_set_StatisticName_m2865338616 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetLeaderboardAroundCurrentUserRequest_get_MaxResultsCount_m936918447 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetLeaderboardAroundCurrentUserRequest_set_MaxResultsCount_m757272642 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
