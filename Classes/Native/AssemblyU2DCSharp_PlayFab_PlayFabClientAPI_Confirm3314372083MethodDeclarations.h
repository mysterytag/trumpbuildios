﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ConfirmPurchaseRequestCallback
struct ConfirmPurchaseRequestCallback_t3314372083;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ConfirmPurchaseRequest
struct ConfirmPurchaseRequest_t1360937886;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConfirmPurc1360937886.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ConfirmPurchaseRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ConfirmPurchaseRequestCallback__ctor_m1089609058 (ConfirmPurchaseRequestCallback_t3314372083 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConfirmPurchaseRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ConfirmPurchaseRequest,System.Object)
extern "C"  void ConfirmPurchaseRequestCallback_Invoke_m1397575935 (ConfirmPurchaseRequestCallback_t3314372083 * __this, String_t* ___urlPath0, int32_t ___callId1, ConfirmPurchaseRequest_t1360937886 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ConfirmPurchaseRequestCallback_t3314372083(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ConfirmPurchaseRequest_t1360937886 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ConfirmPurchaseRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ConfirmPurchaseRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConfirmPurchaseRequestCallback_BeginInvoke_m3947909324 (ConfirmPurchaseRequestCallback_t3314372083 * __this, String_t* ___urlPath0, int32_t ___callId1, ConfirmPurchaseRequest_t1360937886 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConfirmPurchaseRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ConfirmPurchaseRequestCallback_EndInvoke_m3273625714 (ConfirmPurchaseRequestCallback_t3314372083 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
