﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,OnePF.SkuDetails>
struct Dictionary_2_t1707827913;
// System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase>
struct Dictionary_2_t1797893477;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnePF.Inventory
struct  Inventory_t2630562832  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,OnePF.SkuDetails> OnePF.Inventory::_skuMap
	Dictionary_2_t1707827913 * ____skuMap_0;
	// System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase> OnePF.Inventory::_purchaseMap
	Dictionary_2_t1797893477 * ____purchaseMap_1;

public:
	inline static int32_t get_offset_of__skuMap_0() { return static_cast<int32_t>(offsetof(Inventory_t2630562832, ____skuMap_0)); }
	inline Dictionary_2_t1707827913 * get__skuMap_0() const { return ____skuMap_0; }
	inline Dictionary_2_t1707827913 ** get_address_of__skuMap_0() { return &____skuMap_0; }
	inline void set__skuMap_0(Dictionary_2_t1707827913 * value)
	{
		____skuMap_0 = value;
		Il2CppCodeGenWriteBarrier(&____skuMap_0, value);
	}

	inline static int32_t get_offset_of__purchaseMap_1() { return static_cast<int32_t>(offsetof(Inventory_t2630562832, ____purchaseMap_1)); }
	inline Dictionary_2_t1797893477 * get__purchaseMap_1() const { return ____purchaseMap_1; }
	inline Dictionary_2_t1797893477 ** get_address_of__purchaseMap_1() { return &____purchaseMap_1; }
	inline void set__purchaseMap_1(Dictionary_2_t1797893477 * value)
	{
		____purchaseMap_1 = value;
		Il2CppCodeGenWriteBarrier(&____purchaseMap_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
