﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>
struct U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::.ctor()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2__ctor_m2826616012_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2__ctor_m2826616012(__this, method) ((  void (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2__ctor_m2826616012_gshared)(__this, method)
// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1065735952_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1065735952(__this, method) ((  Il2CppObject * (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1065735952_gshared)(__this, method)
// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_IEnumerator_get_Current_m3068656292_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_IEnumerator_get_Current_m3068656292(__this, method) ((  Il2CppObject * (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_IEnumerator_get_Current_m3068656292_gshared)(__this, method)
// System.Boolean UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::MoveNext()
extern "C"  bool U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m832957048_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m832957048(__this, method) ((  bool (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m832957048_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::Dispose()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Dispose_m1856939529_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Dispose_m1856939529(__this, method) ((  void (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Dispose_m1856939529_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>::Reset()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m473048953_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m473048953(__this, method) ((  void (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m473048953_gshared)(__this, method)
