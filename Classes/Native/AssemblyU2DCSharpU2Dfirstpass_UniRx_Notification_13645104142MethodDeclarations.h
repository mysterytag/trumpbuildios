﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Notification`1/OnNextNotification<System.Object>
struct OnNextNotification_t3645104142;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;
// System.String
struct String_t;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_NotificationKi3324123761.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Notification`1/OnNextNotification<System.Object>::.ctor(T)
extern "C"  void OnNextNotification__ctor_m832151699_gshared (OnNextNotification_t3645104142 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define OnNextNotification__ctor_m832151699(__this, ___value0, method) ((  void (*) (OnNextNotification_t3645104142 *, Il2CppObject *, const MethodInfo*))OnNextNotification__ctor_m832151699_gshared)(__this, ___value0, method)
// T UniRx.Notification`1/OnNextNotification<System.Object>::get_Value()
extern "C"  Il2CppObject * OnNextNotification_get_Value_m629542322_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method);
#define OnNextNotification_get_Value_m629542322(__this, method) ((  Il2CppObject * (*) (OnNextNotification_t3645104142 *, const MethodInfo*))OnNextNotification_get_Value_m629542322_gshared)(__this, method)
// System.Exception UniRx.Notification`1/OnNextNotification<System.Object>::get_Exception()
extern "C"  Exception_t1967233988 * OnNextNotification_get_Exception_m2844537466_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method);
#define OnNextNotification_get_Exception_m2844537466(__this, method) ((  Exception_t1967233988 * (*) (OnNextNotification_t3645104142 *, const MethodInfo*))OnNextNotification_get_Exception_m2844537466_gshared)(__this, method)
// System.Boolean UniRx.Notification`1/OnNextNotification<System.Object>::get_HasValue()
extern "C"  bool OnNextNotification_get_HasValue_m3054588949_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method);
#define OnNextNotification_get_HasValue_m3054588949(__this, method) ((  bool (*) (OnNextNotification_t3645104142 *, const MethodInfo*))OnNextNotification_get_HasValue_m3054588949_gshared)(__this, method)
// UniRx.NotificationKind UniRx.Notification`1/OnNextNotification<System.Object>::get_Kind()
extern "C"  int32_t OnNextNotification_get_Kind_m30742692_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method);
#define OnNextNotification_get_Kind_m30742692(__this, method) ((  int32_t (*) (OnNextNotification_t3645104142 *, const MethodInfo*))OnNextNotification_get_Kind_m30742692_gshared)(__this, method)
// System.Int32 UniRx.Notification`1/OnNextNotification<System.Object>::GetHashCode()
extern "C"  int32_t OnNextNotification_GetHashCode_m3412995684_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method);
#define OnNextNotification_GetHashCode_m3412995684(__this, method) ((  int32_t (*) (OnNextNotification_t3645104142 *, const MethodInfo*))OnNextNotification_GetHashCode_m3412995684_gshared)(__this, method)
// System.Boolean UniRx.Notification`1/OnNextNotification<System.Object>::Equals(UniRx.Notification`1<T>)
extern "C"  bool OnNextNotification_Equals_m3854378380_gshared (OnNextNotification_t3645104142 * __this, Notification_1_t38356375 * ___other0, const MethodInfo* method);
#define OnNextNotification_Equals_m3854378380(__this, ___other0, method) ((  bool (*) (OnNextNotification_t3645104142 *, Notification_1_t38356375 *, const MethodInfo*))OnNextNotification_Equals_m3854378380_gshared)(__this, ___other0, method)
// System.String UniRx.Notification`1/OnNextNotification<System.Object>::ToString()
extern "C"  String_t* OnNextNotification_ToString_m1677606792_gshared (OnNextNotification_t3645104142 * __this, const MethodInfo* method);
#define OnNextNotification_ToString_m1677606792(__this, method) ((  String_t* (*) (OnNextNotification_t3645104142 *, const MethodInfo*))OnNextNotification_ToString_m1677606792_gshared)(__this, method)
// System.Void UniRx.Notification`1/OnNextNotification<System.Object>::Accept(UniRx.IObserver`1<T>)
extern "C"  void OnNextNotification_Accept_m1191274355_gshared (OnNextNotification_t3645104142 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OnNextNotification_Accept_m1191274355(__this, ___observer0, method) ((  void (*) (OnNextNotification_t3645104142 *, Il2CppObject*, const MethodInfo*))OnNextNotification_Accept_m1191274355_gshared)(__this, ___observer0, method)
// System.Void UniRx.Notification`1/OnNextNotification<System.Object>::Accept(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void OnNextNotification_Accept_m228441676_gshared (OnNextNotification_t3645104142 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define OnNextNotification_Accept_m228441676(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (OnNextNotification_t3645104142 *, Action_1_t985559125 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))OnNextNotification_Accept_m228441676_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
