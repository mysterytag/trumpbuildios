﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlockContainerInstanceResponseCallback
struct UnlockContainerInstanceResponseCallback_t1712768248;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlockContainerInstanceRequest
struct UnlockContainerInstanceRequest_t1476418829;
// PlayFab.ClientModels.UnlockContainerItemResult
struct UnlockContainerItemResult_t2837935741;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockConta1476418829.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockConta2837935741.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlockContainerInstanceResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlockContainerInstanceResponseCallback__ctor_m2285856135 (UnlockContainerInstanceResponseCallback_t1712768248 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlockContainerInstanceResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlockContainerInstanceRequest,PlayFab.ClientModels.UnlockContainerItemResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlockContainerInstanceResponseCallback_Invoke_m2393551636 (UnlockContainerInstanceResponseCallback_t1712768248 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerInstanceRequest_t1476418829 * ___request2, UnlockContainerItemResult_t2837935741 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlockContainerInstanceResponseCallback_t1712768248(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerInstanceRequest_t1476418829 * ___request2, UnlockContainerItemResult_t2837935741 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlockContainerInstanceResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlockContainerInstanceRequest,PlayFab.ClientModels.UnlockContainerItemResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlockContainerInstanceResponseCallback_BeginInvoke_m2645980297 (UnlockContainerInstanceResponseCallback_t1712768248 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerInstanceRequest_t1476418829 * ___request2, UnlockContainerItemResult_t2837935741 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlockContainerInstanceResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlockContainerInstanceResponseCallback_EndInvoke_m2528686871 (UnlockContainerInstanceResponseCallback_t1712768248 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
