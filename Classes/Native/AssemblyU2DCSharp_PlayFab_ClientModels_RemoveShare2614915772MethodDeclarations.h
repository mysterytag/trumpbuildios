﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RemoveSharedGroupMembersRequest
struct RemoveSharedGroupMembersRequest_t2614915772;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.RemoveSharedGroupMembersRequest::.ctor()
extern "C"  void RemoveSharedGroupMembersRequest__ctor_m346139757 (RemoveSharedGroupMembersRequest_t2614915772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RemoveSharedGroupMembersRequest::get_SharedGroupId()
extern "C"  String_t* RemoveSharedGroupMembersRequest_get_SharedGroupId_m1578462068 (RemoveSharedGroupMembersRequest_t2614915772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RemoveSharedGroupMembersRequest::set_SharedGroupId(System.String)
extern "C"  void RemoveSharedGroupMembersRequest_set_SharedGroupId_m626972831 (RemoveSharedGroupMembersRequest_t2614915772 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.RemoveSharedGroupMembersRequest::get_PlayFabIds()
extern "C"  List_1_t1765447871 * RemoveSharedGroupMembersRequest_get_PlayFabIds_m356535414 (RemoveSharedGroupMembersRequest_t2614915772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RemoveSharedGroupMembersRequest::set_PlayFabIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void RemoveSharedGroupMembersRequest_set_PlayFabIds_m847073261 (RemoveSharedGroupMembersRequest_t2614915772 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
