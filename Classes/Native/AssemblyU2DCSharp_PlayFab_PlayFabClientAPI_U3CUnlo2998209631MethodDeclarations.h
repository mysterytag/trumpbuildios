﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlockContainerItem>c__AnonStoreyAC
struct U3CUnlockContainerItemU3Ec__AnonStoreyAC_t2998209631;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlockContainerItem>c__AnonStoreyAC::.ctor()
extern "C"  void U3CUnlockContainerItemU3Ec__AnonStoreyAC__ctor_m1561535956 (U3CUnlockContainerItemU3Ec__AnonStoreyAC_t2998209631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlockContainerItem>c__AnonStoreyAC::<>m__99(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlockContainerItemU3Ec__AnonStoreyAC_U3CU3Em__99_m2136479954 (U3CUnlockContainerItemU3Ec__AnonStoreyAC_t2998209631 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
