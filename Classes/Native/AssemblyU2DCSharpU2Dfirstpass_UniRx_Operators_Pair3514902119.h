﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4181759311.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.PairwiseObservable`1/Pairwise<System.Object>
struct  Pairwise_t3514902119  : public OperatorObserverBase_2_t4181759311
{
public:
	// T UniRx.Operators.PairwiseObservable`1/Pairwise::prev
	Il2CppObject * ___prev_2;
	// System.Boolean UniRx.Operators.PairwiseObservable`1/Pairwise::isFirst
	bool ___isFirst_3;

public:
	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(Pairwise_t3514902119, ___prev_2)); }
	inline Il2CppObject * get_prev_2() const { return ___prev_2; }
	inline Il2CppObject ** get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(Il2CppObject * value)
	{
		___prev_2 = value;
		Il2CppCodeGenWriteBarrier(&___prev_2, value);
	}

	inline static int32_t get_offset_of_isFirst_3() { return static_cast<int32_t>(offsetof(Pairwise_t3514902119, ___isFirst_3)); }
	inline bool get_isFirst_3() const { return ___isFirst_3; }
	inline bool* get_address_of_isFirst_3() { return &___isFirst_3; }
	inline void set_isFirst_3(bool value)
	{
		___isFirst_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
