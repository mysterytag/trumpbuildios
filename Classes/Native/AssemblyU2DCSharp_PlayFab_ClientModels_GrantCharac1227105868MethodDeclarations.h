﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GrantCharacterToUserRequest
struct GrantCharacterToUserRequest_t1227105868;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GrantCharacterToUserRequest::.ctor()
extern "C"  void GrantCharacterToUserRequest__ctor_m4000249309 (GrantCharacterToUserRequest_t1227105868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GrantCharacterToUserRequest::get_CatalogVersion()
extern "C"  String_t* GrantCharacterToUserRequest_get_CatalogVersion_m2595700146 (GrantCharacterToUserRequest_t1227105868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GrantCharacterToUserRequest::set_CatalogVersion(System.String)
extern "C"  void GrantCharacterToUserRequest_set_CatalogVersion_m1373803935 (GrantCharacterToUserRequest_t1227105868 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GrantCharacterToUserRequest::get_ItemId()
extern "C"  String_t* GrantCharacterToUserRequest_get_ItemId_m2590743809 (GrantCharacterToUserRequest_t1227105868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GrantCharacterToUserRequest::set_ItemId(System.String)
extern "C"  void GrantCharacterToUserRequest_set_ItemId_m191974832 (GrantCharacterToUserRequest_t1227105868 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GrantCharacterToUserRequest::get_CharacterName()
extern "C"  String_t* GrantCharacterToUserRequest_get_CharacterName_m2582004003 (GrantCharacterToUserRequest_t1227105868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GrantCharacterToUserRequest::set_CharacterName(System.String)
extern "C"  void GrantCharacterToUserRequest_set_CharacterName_m2504560080 (GrantCharacterToUserRequest_t1227105868 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
