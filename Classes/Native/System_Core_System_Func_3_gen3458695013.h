﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate2585444626.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`3<UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct  Func_3_t3458695013  : public MulticastDelegate_t2585444626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
