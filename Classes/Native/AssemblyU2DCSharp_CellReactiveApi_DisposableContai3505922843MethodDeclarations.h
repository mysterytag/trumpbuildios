﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/DisposableContainer`1<System.Object>
struct DisposableContainer_1_t3505922843;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/DisposableContainer`1<System.Object>::.ctor()
extern "C"  void DisposableContainer_1__ctor_m3323161249_gshared (DisposableContainer_1_t3505922843 * __this, const MethodInfo* method);
#define DisposableContainer_1__ctor_m3323161249(__this, method) ((  void (*) (DisposableContainer_1_t3505922843 *, const MethodInfo*))DisposableContainer_1__ctor_m3323161249_gshared)(__this, method)
// System.Void CellReactiveApi/DisposableContainer`1<System.Object>::Dispose()
extern "C"  void DisposableContainer_1_Dispose_m2295542430_gshared (DisposableContainer_1_t3505922843 * __this, const MethodInfo* method);
#define DisposableContainer_1_Dispose_m2295542430(__this, method) ((  void (*) (DisposableContainer_1_t3505922843 *, const MethodInfo*))DisposableContainer_1_Dispose_m2295542430_gshared)(__this, method)
