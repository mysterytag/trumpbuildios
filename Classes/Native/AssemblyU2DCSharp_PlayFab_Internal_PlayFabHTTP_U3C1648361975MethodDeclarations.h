﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6
struct U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::.ctor()
extern "C"  void U3CMakeRequestViaUnityU3Ec__Iterator6__ctor_m4074143822 (U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeRequestViaUnityU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m781587342 (U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeRequestViaUnityU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m2873946914 (U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::MoveNext()
extern "C"  bool U3CMakeRequestViaUnityU3Ec__Iterator6_MoveNext_m4183041998 (U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::Dispose()
extern "C"  void U3CMakeRequestViaUnityU3Ec__Iterator6_Dispose_m2435289355 (U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::Reset()
extern "C"  void U3CMakeRequestViaUnityU3Ec__Iterator6_Reset_m1720576763 (U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
