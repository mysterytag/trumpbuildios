﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>
struct Timeout_t2052083218;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C<System.Object>
struct  U3CRunTimerU3Ec__AnonStorey6C_t4277735173  : public Il2CppObject
{
public:
	// System.UInt64 UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C::timerId
	uint64_t ___timerId_0;
	// UniRx.Operators.TimeoutObservable`1/Timeout<T> UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C::<>f__this
	Timeout_t2052083218 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_timerId_0() { return static_cast<int32_t>(offsetof(U3CRunTimerU3Ec__AnonStorey6C_t4277735173, ___timerId_0)); }
	inline uint64_t get_timerId_0() const { return ___timerId_0; }
	inline uint64_t* get_address_of_timerId_0() { return &___timerId_0; }
	inline void set_timerId_0(uint64_t value)
	{
		___timerId_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CRunTimerU3Ec__AnonStorey6C_t4277735173, ___U3CU3Ef__this_1)); }
	inline Timeout_t2052083218 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline Timeout_t2052083218 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(Timeout_t2052083218 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
