﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819MethodDeclarations.h"

// System.Void UniRx.Tuple`2<System.String,System.String>::.ctor(T1,T2)
#define Tuple_2__ctor_m3634553114(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t3445990771 *, String_t*, String_t*, const MethodInfo*))Tuple_2__ctor_m101027774_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<System.String,System.String>::System.IComparable.CompareTo(System.Object)
#define Tuple_2_System_IComparable_CompareTo_m2295557487(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t3445990771 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m964946507_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.String,System.String>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m2122832641(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t3445990771 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m1607455453_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<System.String,System.String>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m354125804(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t3445990771 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m2678862280_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<System.String,System.String>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2420486082(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t3445990771 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2682338918_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<System.String,System.String>::UniRx.ITuple.ToString()
#define Tuple_2_UniRx_ITuple_ToString_m3462863977(__this, method) ((  String_t* (*) (Tuple_2_t3445990771 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m2079956805_gshared)(__this, method)
// T1 UniRx.Tuple`2<System.String,System.String>::get_Item1()
#define Tuple_2_get_Item1_m4035136924(__this, method) ((  String_t* (*) (Tuple_2_t3445990771 *, const MethodInfo*))Tuple_2_get_Item1_m425160818_gshared)(__this, method)
// T2 UniRx.Tuple`2<System.String,System.String>::get_Item2()
#define Tuple_2_get_Item2_m370629214(__this, method) ((  String_t* (*) (Tuple_2_t3445990771 *, const MethodInfo*))Tuple_2_get_Item2_m1055620404_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.String,System.String>::Equals(System.Object)
#define Tuple_2_Equals_m1771041440(__this, ___obj0, method) ((  bool (*) (Tuple_2_t3445990771 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m2489154684_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.String,System.String>::GetHashCode()
#define Tuple_2_GetHashCode_m3188116804(__this, method) ((  int32_t (*) (Tuple_2_t3445990771 *, const MethodInfo*))Tuple_2_GetHashCode_m1023013664_gshared)(__this, method)
// System.String UniRx.Tuple`2<System.String,System.String>::ToString()
#define Tuple_2_ToString_m4142581800(__this, method) ((  String_t* (*) (Tuple_2_t3445990771 *, const MethodInfo*))Tuple_2_ToString_m3182481356_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.String,System.String>::Equals(UniRx.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m346043721(__this, ___other0, method) ((  bool (*) (Tuple_2_t3445990771 *, Tuple_2_t3445990771 , const MethodInfo*))Tuple_2_Equals_m1605966829_gshared)(__this, ___other0, method)
