﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ObservableYieldInstruction`1<System.String>
struct ObservableYieldInstruction_1_t3090401786;
// System.Object
struct Il2CppObject;
// UniRx.Examples.Sample06_ConvertToCoroutine
struct Sample06_ConvertToCoroutine_t617332884;
// System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform>
struct Func_2_t140622909;
// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t884531080;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9
struct  U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994  : public Il2CppObject
{
public:
	// UniRx.ObservableYieldInstruction`1<System.String> UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::<o>__0
	ObservableYieldInstruction_1_t3090401786 * ___U3CoU3E__0_0;
	// System.Int32 UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::$PC
	int32_t ___U24PC_1;
	// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::$current
	Il2CppObject * ___U24current_2;
	// UniRx.Examples.Sample06_ConvertToCoroutine UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::<>f__this
	Sample06_ConvertToCoroutine_t617332884 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994, ___U3CoU3E__0_0)); }
	inline ObservableYieldInstruction_1_t3090401786 * get_U3CoU3E__0_0() const { return ___U3CoU3E__0_0; }
	inline ObservableYieldInstruction_1_t3090401786 ** get_address_of_U3CoU3E__0_0() { return &___U3CoU3E__0_0; }
	inline void set_U3CoU3E__0_0(ObservableYieldInstruction_1_t3090401786 * value)
	{
		___U3CoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CoU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994, ___U3CU3Ef__this_3)); }
	inline Sample06_ConvertToCoroutine_t617332884 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline Sample06_ConvertToCoroutine_t617332884 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(Sample06_ConvertToCoroutine_t617332884 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

struct U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields
{
public:
	// System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform> UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::<>f__am$cache4
	Func_2_t140622909 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.Transform,System.Boolean> UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::<>f__am$cache5
	Func_2_t884531080 * ___U3CU3Ef__amU24cache5_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t140622909 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t140622909 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t140622909 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t884531080 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t884531080 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t884531080 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
