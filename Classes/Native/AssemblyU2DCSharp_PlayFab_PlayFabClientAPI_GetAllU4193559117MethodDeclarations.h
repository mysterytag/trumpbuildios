﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetAllUsersCharactersResponseCallback
struct GetAllUsersCharactersResponseCallback_t4193559117;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ListUsersCharactersRequest
struct ListUsersCharactersRequest_t3884929547;
// PlayFab.ClientModels.ListUsersCharactersResult
struct ListUsersCharactersResult_t1961583425;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ListUsersCh3884929547.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ListUsersCh1961583425.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetAllUsersCharactersResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetAllUsersCharactersResponseCallback__ctor_m1686532188 (GetAllUsersCharactersResponseCallback_t4193559117 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetAllUsersCharactersResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ListUsersCharactersRequest,PlayFab.ClientModels.ListUsersCharactersResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetAllUsersCharactersResponseCallback_Invoke_m2328136495 (GetAllUsersCharactersResponseCallback_t4193559117 * __this, String_t* ___urlPath0, int32_t ___callId1, ListUsersCharactersRequest_t3884929547 * ___request2, ListUsersCharactersResult_t1961583425 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetAllUsersCharactersResponseCallback_t4193559117(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ListUsersCharactersRequest_t3884929547 * ___request2, ListUsersCharactersResult_t1961583425 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetAllUsersCharactersResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ListUsersCharactersRequest,PlayFab.ClientModels.ListUsersCharactersResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetAllUsersCharactersResponseCallback_BeginInvoke_m297260888 (GetAllUsersCharactersResponseCallback_t4193559117 * __this, String_t* ___urlPath0, int32_t ___callId1, ListUsersCharactersRequest_t3884929547 * ___request2, ListUsersCharactersResult_t1961583425 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetAllUsersCharactersResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetAllUsersCharactersResponseCallback_EndInvoke_m4269305964 (GetAllUsersCharactersResponseCallback_t4193559117 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
