﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/Intrusion<System.Single,System.Single>
struct Intrusion_t78896424;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/Intrusion<System.Single,System.Single>::.ctor()
extern "C"  void Intrusion__ctor_m1106797134_gshared (Intrusion_t78896424 * __this, const MethodInfo* method);
#define Intrusion__ctor_m1106797134(__this, method) ((  void (*) (Intrusion_t78896424 *, const MethodInfo*))Intrusion__ctor_m1106797134_gshared)(__this, method)
