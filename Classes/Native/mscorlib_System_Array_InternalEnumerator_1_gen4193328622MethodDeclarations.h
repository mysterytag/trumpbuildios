﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen744596923MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3297555572(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4193328622 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3830588588(__this, method) ((  void (*) (InternalEnumerator_1_t4193328622 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4073990680(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4193328622 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>>::Dispose()
#define InternalEnumerator_1_Dispose_m2599911243(__this, method) ((  void (*) (InternalEnumerator_1_t4193328622 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m541032216(__this, method) ((  bool (*) (InternalEnumerator_1_t4193328622 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>>::get_Current()
#define InternalEnumerator_1_get_Current_m2943383867(__this, method) ((  ImmutableList_1_t4285838119 * (*) (InternalEnumerator_1_t4193328622 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
