﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<When>c__AnonStorey10A`1<System.Boolean>
struct U3CWhenU3Ec__AnonStorey10A_1_t872949398;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<When>c__AnonStorey10A`1<System.Boolean>::.ctor()
extern "C"  void U3CWhenU3Ec__AnonStorey10A_1__ctor_m3575667422_gshared (U3CWhenU3Ec__AnonStorey10A_1_t872949398 * __this, const MethodInfo* method);
#define U3CWhenU3Ec__AnonStorey10A_1__ctor_m3575667422(__this, method) ((  void (*) (U3CWhenU3Ec__AnonStorey10A_1_t872949398 *, const MethodInfo*))U3CWhenU3Ec__AnonStorey10A_1__ctor_m3575667422_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<When>c__AnonStorey10A`1<System.Boolean>::<>m__17D(System.Action,Priority)
extern "C"  Il2CppObject * U3CWhenU3Ec__AnonStorey10A_1_U3CU3Em__17D_m1390952753_gshared (U3CWhenU3Ec__AnonStorey10A_1_t872949398 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CWhenU3Ec__AnonStorey10A_1_U3CU3Em__17D_m1390952753(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CWhenU3Ec__AnonStorey10A_1_t872949398 *, Action_t437523947 *, int32_t, const MethodInfo*))U3CWhenU3Ec__AnonStorey10A_1_U3CU3Em__17D_m1390952753_gshared)(__this, ___reaction0, ___p1, method)
