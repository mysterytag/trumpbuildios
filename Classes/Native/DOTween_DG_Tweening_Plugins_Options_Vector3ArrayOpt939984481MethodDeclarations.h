﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Vector3ArrayOptions_t939984481;
struct Vector3ArrayOptions_t939984481_marshaled_pinvoke;

extern "C" void Vector3ArrayOptions_t939984481_marshal_pinvoke(const Vector3ArrayOptions_t939984481& unmarshaled, Vector3ArrayOptions_t939984481_marshaled_pinvoke& marshaled);
extern "C" void Vector3ArrayOptions_t939984481_marshal_pinvoke_back(const Vector3ArrayOptions_t939984481_marshaled_pinvoke& marshaled, Vector3ArrayOptions_t939984481& unmarshaled);
extern "C" void Vector3ArrayOptions_t939984481_marshal_pinvoke_cleanup(Vector3ArrayOptions_t939984481_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Vector3ArrayOptions_t939984481;
struct Vector3ArrayOptions_t939984481_marshaled_com;

extern "C" void Vector3ArrayOptions_t939984481_marshal_com(const Vector3ArrayOptions_t939984481& unmarshaled, Vector3ArrayOptions_t939984481_marshaled_com& marshaled);
extern "C" void Vector3ArrayOptions_t939984481_marshal_com_back(const Vector3ArrayOptions_t939984481_marshaled_com& marshaled, Vector3ArrayOptions_t939984481& unmarshaled);
extern "C" void Vector3ArrayOptions_t939984481_marshal_com_cleanup(Vector3ArrayOptions_t939984481_marshaled_com& marshaled);
