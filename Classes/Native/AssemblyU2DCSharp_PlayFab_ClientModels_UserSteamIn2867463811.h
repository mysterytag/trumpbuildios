﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen2703348181.h"
#include "mscorlib_System_Nullable_1_gen2484541604.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UserSteamInfo
struct  UserSteamInfo_t2867463811  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.UserSteamInfo::<SteamId>k__BackingField
	String_t* ___U3CSteamIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.UserSteamInfo::<SteamCountry>k__BackingField
	String_t* ___U3CSteamCountryU3Ek__BackingField_1;
	// System.Nullable`1<PlayFab.ClientModels.Currency> PlayFab.ClientModels.UserSteamInfo::<SteamCurrency>k__BackingField
	Nullable_1_t2703348181  ___U3CSteamCurrencyU3Ek__BackingField_2;
	// System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus> PlayFab.ClientModels.UserSteamInfo::<SteamActivationStatus>k__BackingField
	Nullable_1_t2484541604  ___U3CSteamActivationStatusU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSteamIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UserSteamInfo_t2867463811, ___U3CSteamIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CSteamIdU3Ek__BackingField_0() const { return ___U3CSteamIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CSteamIdU3Ek__BackingField_0() { return &___U3CSteamIdU3Ek__BackingField_0; }
	inline void set_U3CSteamIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CSteamIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSteamIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CSteamCountryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UserSteamInfo_t2867463811, ___U3CSteamCountryU3Ek__BackingField_1)); }
	inline String_t* get_U3CSteamCountryU3Ek__BackingField_1() const { return ___U3CSteamCountryU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSteamCountryU3Ek__BackingField_1() { return &___U3CSteamCountryU3Ek__BackingField_1; }
	inline void set_U3CSteamCountryU3Ek__BackingField_1(String_t* value)
	{
		___U3CSteamCountryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSteamCountryU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CSteamCurrencyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UserSteamInfo_t2867463811, ___U3CSteamCurrencyU3Ek__BackingField_2)); }
	inline Nullable_1_t2703348181  get_U3CSteamCurrencyU3Ek__BackingField_2() const { return ___U3CSteamCurrencyU3Ek__BackingField_2; }
	inline Nullable_1_t2703348181 * get_address_of_U3CSteamCurrencyU3Ek__BackingField_2() { return &___U3CSteamCurrencyU3Ek__BackingField_2; }
	inline void set_U3CSteamCurrencyU3Ek__BackingField_2(Nullable_1_t2703348181  value)
	{
		___U3CSteamCurrencyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CSteamActivationStatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UserSteamInfo_t2867463811, ___U3CSteamActivationStatusU3Ek__BackingField_3)); }
	inline Nullable_1_t2484541604  get_U3CSteamActivationStatusU3Ek__BackingField_3() const { return ___U3CSteamActivationStatusU3Ek__BackingField_3; }
	inline Nullable_1_t2484541604 * get_address_of_U3CSteamActivationStatusU3Ek__BackingField_3() { return &___U3CSteamActivationStatusU3Ek__BackingField_3; }
	inline void set_U3CSteamActivationStatusU3Ek__BackingField_3(Nullable_1_t2484541604  value)
	{
		___U3CSteamActivationStatusU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
