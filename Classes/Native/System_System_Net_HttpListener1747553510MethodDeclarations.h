﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpListener
struct HttpListener_t1747553510;
// System.Net.AuthenticationSchemeSelector
struct AuthenticationSchemeSelector_t1750022342;
// System.Net.HttpListenerPrefixCollection
struct HttpListenerPrefixCollection_t2280589270;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;
// System.Net.HttpListenerContext
struct HttpListenerContext_t260537341;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_AuthenticationSchemes2907004192.h"
#include "System_System_Net_AuthenticationSchemeSelector1750022342.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Object837106420.h"
#include "System_System_Net_HttpListenerContext260537341.h"

// System.Void System.Net.HttpListener::.ctor()
extern "C"  void HttpListener__ctor_m1872522174 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::System.IDisposable.Dispose()
extern "C"  void HttpListener_System_IDisposable_Dispose_m292472481 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.AuthenticationSchemes System.Net.HttpListener::get_AuthenticationSchemes()
extern "C"  int32_t HttpListener_get_AuthenticationSchemes_m2525898306 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_AuthenticationSchemes(System.Net.AuthenticationSchemes)
extern "C"  void HttpListener_set_AuthenticationSchemes_m983646319 (HttpListener_t1747553510 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.AuthenticationSchemeSelector System.Net.HttpListener::get_AuthenticationSchemeSelectorDelegate()
extern "C"  AuthenticationSchemeSelector_t1750022342 * HttpListener_get_AuthenticationSchemeSelectorDelegate_m1897504617 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_AuthenticationSchemeSelectorDelegate(System.Net.AuthenticationSchemeSelector)
extern "C"  void HttpListener_set_AuthenticationSchemeSelectorDelegate_m249747746 (HttpListener_t1747553510 * __this, AuthenticationSchemeSelector_t1750022342 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListener::get_IgnoreWriteExceptions()
extern "C"  bool HttpListener_get_IgnoreWriteExceptions_m3205603290 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_IgnoreWriteExceptions(System.Boolean)
extern "C"  void HttpListener_set_IgnoreWriteExceptions_m3323537607 (HttpListener_t1747553510 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListener::get_IsListening()
extern "C"  bool HttpListener_get_IsListening_m2702165370 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListener::get_IsSupported()
extern "C"  bool HttpListener_get_IsSupported_m3805841581 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerPrefixCollection System.Net.HttpListener::get_Prefixes()
extern "C"  HttpListenerPrefixCollection_t2280589270 * HttpListener_get_Prefixes_m852249368 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListener::get_Realm()
extern "C"  String_t* HttpListener_get_Realm_m2980353829 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_Realm(System.String)
extern "C"  void HttpListener_set_Realm_m4264083060 (HttpListener_t1747553510 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListener::get_UnsafeConnectionNtlmAuthentication()
extern "C"  bool HttpListener_get_UnsafeConnectionNtlmAuthentication_m4182051228 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::set_UnsafeConnectionNtlmAuthentication(System.Boolean)
extern "C"  void HttpListener_set_UnsafeConnectionNtlmAuthentication_m2548622557 (HttpListener_t1747553510 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Abort()
extern "C"  void HttpListener_Abort_m1522067468 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Close()
extern "C"  void HttpListener_Close_m3583381716 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Close(System.Boolean)
extern "C"  void HttpListener_Close_m2043986763 (HttpListener_t1747553510 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Cleanup(System.Boolean)
extern "C"  void HttpListener_Cleanup_m3960973623 (HttpListener_t1747553510 * __this, bool ___close_existing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.HttpListener::BeginGetContext(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HttpListener_BeginGetContext_m2609932366 (HttpListener_t1747553510 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerContext System.Net.HttpListener::EndGetContext(System.IAsyncResult)
extern "C"  HttpListenerContext_t260537341 * HttpListener_EndGetContext_m2604305815 (HttpListener_t1747553510 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.AuthenticationSchemes System.Net.HttpListener::SelectAuthenticationScheme(System.Net.HttpListenerContext)
extern "C"  int32_t HttpListener_SelectAuthenticationScheme_m1539550051 (HttpListener_t1747553510 * __this, HttpListenerContext_t260537341 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerContext System.Net.HttpListener::GetContext()
extern "C"  HttpListenerContext_t260537341 * HttpListener_GetContext_m447275907 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Start()
extern "C"  void HttpListener_Start_m819659966 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::Stop()
extern "C"  void HttpListener_Stop_m303948104 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::CheckDisposed()
extern "C"  void HttpListener_CheckDisposed_m1959888361 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpListenerContext System.Net.HttpListener::GetContextFromQueue()
extern "C"  HttpListenerContext_t260537341 * HttpListener_GetContextFromQueue_m2915796582 (HttpListener_t1747553510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::RegisterContext(System.Net.HttpListenerContext)
extern "C"  void HttpListener_RegisterContext_m1574797765 (HttpListener_t1747553510 * __this, HttpListenerContext_t260537341 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListener::UnregisterContext(System.Net.HttpListenerContext)
extern "C"  void HttpListener_UnregisterContext_m2948638540 (HttpListener_t1747553510 * __this, HttpListenerContext_t260537341 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
