﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>
struct Zip_t4278583866;
// UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_4_t637587494;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`4<T1,T2,T3,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Zip__ctor_m2487409949_gshared (Zip_t4278583866 * __this, ZipObservable_4_t637587494 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Zip__ctor_m2487409949(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Zip_t4278583866 *, ZipObservable_4_t637587494 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Zip__ctor_m2487409949_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * Zip_Run_m4064333642_gshared (Zip_t4278583866 * __this, const MethodInfo* method);
#define Zip_Run_m4064333642(__this, method) ((  Il2CppObject * (*) (Zip_t4278583866 *, const MethodInfo*))Zip_Run_m4064333642_gshared)(__this, method)
// TR UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * Zip_GetResult_m363978086_gshared (Zip_t4278583866 * __this, const MethodInfo* method);
#define Zip_GetResult_m363978086(__this, method) ((  Il2CppObject * (*) (Zip_t4278583866 *, const MethodInfo*))Zip_GetResult_m363978086_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void Zip_OnNext_m1887941820_gshared (Zip_t4278583866 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Zip_OnNext_m1887941820(__this, ___value0, method) ((  void (*) (Zip_t4278583866 *, Il2CppObject *, const MethodInfo*))Zip_OnNext_m1887941820_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Zip_OnError_m2808856563_gshared (Zip_t4278583866 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Zip_OnError_m2808856563(__this, ___error0, method) ((  void (*) (Zip_t4278583866 *, Exception_t1967233988 *, const MethodInfo*))Zip_OnError_m2808856563_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void Zip_OnCompleted_m2466023366_gshared (Zip_t4278583866 * __this, const MethodInfo* method);
#define Zip_OnCompleted_m2466023366(__this, method) ((  void (*) (Zip_t4278583866 *, const MethodInfo*))Zip_OnCompleted_m2466023366_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>::<Run>m__92()
extern "C"  void Zip_U3CRunU3Em__92_m1659720255_gshared (Zip_t4278583866 * __this, const MethodInfo* method);
#define Zip_U3CRunU3Em__92_m1659720255(__this, method) ((  void (*) (Zip_t4278583866 *, const MethodInfo*))Zip_U3CRunU3Em__92_m1659720255_gshared)(__this, method)
