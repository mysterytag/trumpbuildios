﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.PayForPurchaseRequest
struct PayForPurchaseRequest_t2884624637;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.PayForPurchaseRequest::.ctor()
extern "C"  void PayForPurchaseRequest__ctor_m3658349260 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PayForPurchaseRequest::get_OrderId()
extern "C"  String_t* PayForPurchaseRequest_get_OrderId_m2552895879 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::set_OrderId(System.String)
extern "C"  void PayForPurchaseRequest_set_OrderId_m3355550316 (PayForPurchaseRequest_t2884624637 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PayForPurchaseRequest::get_ProviderName()
extern "C"  String_t* PayForPurchaseRequest_get_ProviderName_m2668221056 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::set_ProviderName(System.String)
extern "C"  void PayForPurchaseRequest_set_ProviderName_m3086878097 (PayForPurchaseRequest_t2884624637 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PayForPurchaseRequest::get_Currency()
extern "C"  String_t* PayForPurchaseRequest_get_Currency_m5353141 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::set_Currency(System.String)
extern "C"  void PayForPurchaseRequest_set_Currency_m2116765436 (PayForPurchaseRequest_t2884624637 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PayForPurchaseRequest::get_ProviderTransactionId()
extern "C"  String_t* PayForPurchaseRequest_get_ProviderTransactionId_m3190669350 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::set_ProviderTransactionId(System.String)
extern "C"  void PayForPurchaseRequest_set_ProviderTransactionId_m2192008301 (PayForPurchaseRequest_t2884624637 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
