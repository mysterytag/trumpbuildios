﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ReportPlayerClientRequest
struct ReportPlayerClientRequest_t3784712447;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ReportPlayerClientRequest::.ctor()
extern "C"  void ReportPlayerClientRequest__ctor_m1155424778 (ReportPlayerClientRequest_t3784712447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ReportPlayerClientRequest::get_ReporteeId()
extern "C"  String_t* ReportPlayerClientRequest_get_ReporteeId_m1161488501 (ReportPlayerClientRequest_t3784712447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ReportPlayerClientRequest::set_ReporteeId(System.String)
extern "C"  void ReportPlayerClientRequest_set_ReporteeId_m3480544252 (ReportPlayerClientRequest_t3784712447 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ReportPlayerClientRequest::get_Comment()
extern "C"  String_t* ReportPlayerClientRequest_get_Comment_m331329435 (ReportPlayerClientRequest_t3784712447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ReportPlayerClientRequest::set_Comment(System.String)
extern "C"  void ReportPlayerClientRequest_set_Comment_m1708799576 (ReportPlayerClientRequest_t3784712447 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
