﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WaitForStreamEvent
struct WaitForStreamEvent_t4156505958;
// IEmptyStream
struct IEmptyStream_t3684082468;

#include "codegen/il2cpp-codegen.h"

// System.Void WaitForStreamEvent::.ctor(IEmptyStream)
extern "C"  void WaitForStreamEvent__ctor_m3297977169 (WaitForStreamEvent_t4156505958 * __this, Il2CppObject * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WaitForStreamEvent::get_finished()
extern "C"  bool WaitForStreamEvent_get_finished_m4209929918 (WaitForStreamEvent_t4156505958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaitForStreamEvent::Tick(System.Single)
extern "C"  void WaitForStreamEvent_Tick_m441454623 (WaitForStreamEvent_t4156505958 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaitForStreamEvent::Dispose()
extern "C"  void WaitForStreamEvent_Dispose_m871593714 (WaitForStreamEvent_t4156505958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaitForStreamEvent::<WaitForStreamEvent>m__1DE()
extern "C"  void WaitForStreamEvent_U3CWaitForStreamEventU3Em__1DE_m2868032240 (WaitForStreamEvent_t4156505958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
