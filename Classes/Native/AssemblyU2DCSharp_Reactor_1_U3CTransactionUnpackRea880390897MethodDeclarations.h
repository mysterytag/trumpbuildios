﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int64>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int64>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m3954622129_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m3954622129(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m3954622129_gshared)(__this, method)
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Int64>::<>m__1D6()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m4025287563_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m4025287563(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t880390897 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m4025287563_gshared)(__this, method)
