﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Object>
struct U3CReportU3Ec__AnonStorey5A_t1267941174;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Object>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey5A__ctor_m841645845_gshared (U3CReportU3Ec__AnonStorey5A_t1267941174 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey5A__ctor_m841645845(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey5A_t1267941174 *, const MethodInfo*))U3CReportU3Ec__AnonStorey5A__ctor_m841645845_gshared)(__this, method)
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Object>::<>m__6E()
extern "C"  void U3CReportU3Ec__AnonStorey5A_U3CU3Em__6E_m3676014509_gshared (U3CReportU3Ec__AnonStorey5A_t1267941174 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey5A_U3CU3Em__6E_m3676014509(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey5A_t1267941174 *, const MethodInfo*))U3CReportU3Ec__AnonStorey5A_U3CU3Em__6E_m3676014509_gshared)(__this, method)
