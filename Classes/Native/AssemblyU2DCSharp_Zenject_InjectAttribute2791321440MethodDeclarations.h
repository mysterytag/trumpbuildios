﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InjectAttribute
struct InjectAttribute_t2791321440;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Zenject.InjectAttribute::.ctor(System.String)
extern "C"  void InjectAttribute__ctor_m2071606371 (InjectAttribute_t2791321440 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectAttribute::.ctor()
extern "C"  void InjectAttribute__ctor_m756760831 (InjectAttribute_t2791321440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Zenject.InjectAttribute::get_Identifier()
extern "C"  String_t* InjectAttribute_get_Identifier_m3727677780 (InjectAttribute_t2791321440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectAttribute::set_Identifier(System.String)
extern "C"  void InjectAttribute_set_Identifier_m2087805527 (InjectAttribute_t2791321440 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
