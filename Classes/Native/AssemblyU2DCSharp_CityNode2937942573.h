﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlaneInfo>
struct List_1_t612754947;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CityNode
struct  CityNode_t2937942573  : public MonoBehaviour_t3012272455
{
public:
	// System.Collections.Generic.List`1<PlaneInfo> CityNode::planes
	List_1_t612754947 * ___planes_2;
	// System.Single CityNode::speedBase
	float ___speedBase_3;
	// System.Single CityNode::width
	float ___width_4;
	// System.Single CityNode::startTime
	float ___startTime_5;
	// System.Single CityNode::speedLevel
	float ___speedLevel_6;
	// System.Single CityNode::duration
	float ___duration_7;
	// System.Single CityNode::targetSpeedLevel
	float ___targetSpeedLevel_8;
	// System.Single CityNode::start_change_time
	float ___start_change_time_9;

public:
	inline static int32_t get_offset_of_planes_2() { return static_cast<int32_t>(offsetof(CityNode_t2937942573, ___planes_2)); }
	inline List_1_t612754947 * get_planes_2() const { return ___planes_2; }
	inline List_1_t612754947 ** get_address_of_planes_2() { return &___planes_2; }
	inline void set_planes_2(List_1_t612754947 * value)
	{
		___planes_2 = value;
		Il2CppCodeGenWriteBarrier(&___planes_2, value);
	}

	inline static int32_t get_offset_of_speedBase_3() { return static_cast<int32_t>(offsetof(CityNode_t2937942573, ___speedBase_3)); }
	inline float get_speedBase_3() const { return ___speedBase_3; }
	inline float* get_address_of_speedBase_3() { return &___speedBase_3; }
	inline void set_speedBase_3(float value)
	{
		___speedBase_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(CityNode_t2937942573, ___width_4)); }
	inline float get_width_4() const { return ___width_4; }
	inline float* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(float value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_startTime_5() { return static_cast<int32_t>(offsetof(CityNode_t2937942573, ___startTime_5)); }
	inline float get_startTime_5() const { return ___startTime_5; }
	inline float* get_address_of_startTime_5() { return &___startTime_5; }
	inline void set_startTime_5(float value)
	{
		___startTime_5 = value;
	}

	inline static int32_t get_offset_of_speedLevel_6() { return static_cast<int32_t>(offsetof(CityNode_t2937942573, ___speedLevel_6)); }
	inline float get_speedLevel_6() const { return ___speedLevel_6; }
	inline float* get_address_of_speedLevel_6() { return &___speedLevel_6; }
	inline void set_speedLevel_6(float value)
	{
		___speedLevel_6 = value;
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(CityNode_t2937942573, ___duration_7)); }
	inline float get_duration_7() const { return ___duration_7; }
	inline float* get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(float value)
	{
		___duration_7 = value;
	}

	inline static int32_t get_offset_of_targetSpeedLevel_8() { return static_cast<int32_t>(offsetof(CityNode_t2937942573, ___targetSpeedLevel_8)); }
	inline float get_targetSpeedLevel_8() const { return ___targetSpeedLevel_8; }
	inline float* get_address_of_targetSpeedLevel_8() { return &___targetSpeedLevel_8; }
	inline void set_targetSpeedLevel_8(float value)
	{
		___targetSpeedLevel_8 = value;
	}

	inline static int32_t get_offset_of_start_change_time_9() { return static_cast<int32_t>(offsetof(CityNode_t2937942573, ___start_change_time_9)); }
	inline float get_start_change_time_9() const { return ___start_change_time_9; }
	inline float* get_address_of_start_change_time_9() { return &___start_change_time_9; }
	inline void set_start_change_time_9(float value)
	{
		___start_change_time_9 = value;
	}
};

struct CityNode_t2937942573_StaticFields
{
public:
	// System.String CityNode::path
	String_t* ___path_10;

public:
	inline static int32_t get_offset_of_path_10() { return static_cast<int32_t>(offsetof(CityNode_t2937942573_StaticFields, ___path_10)); }
	inline String_t* get_path_10() const { return ___path_10; }
	inline String_t** get_address_of_path_10() { return &___path_10; }
	inline void set_path_10(String_t* value)
	{
		___path_10 = value;
		Il2CppCodeGenWriteBarrier(&___path_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
