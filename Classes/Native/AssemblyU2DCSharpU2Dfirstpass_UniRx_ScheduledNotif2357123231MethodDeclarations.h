﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledNotifier`1<System.Object>
struct ScheduledNotifier_1_t2357123231;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

// System.Void UniRx.ScheduledNotifier`1<System.Object>::.ctor()
extern "C"  void ScheduledNotifier_1__ctor_m526978847_gshared (ScheduledNotifier_1_t2357123231 * __this, const MethodInfo* method);
#define ScheduledNotifier_1__ctor_m526978847(__this, method) ((  void (*) (ScheduledNotifier_1_t2357123231 *, const MethodInfo*))ScheduledNotifier_1__ctor_m526978847_gshared)(__this, method)
// System.Void UniRx.ScheduledNotifier`1<System.Object>::.ctor(UniRx.IScheduler)
extern "C"  void ScheduledNotifier_1__ctor_m3595667509_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method);
#define ScheduledNotifier_1__ctor_m3595667509(__this, ___scheduler0, method) ((  void (*) (ScheduledNotifier_1_t2357123231 *, Il2CppObject *, const MethodInfo*))ScheduledNotifier_1__ctor_m3595667509_gshared)(__this, ___scheduler0, method)
// System.Void UniRx.ScheduledNotifier`1<System.Object>::Report(T)
extern "C"  void ScheduledNotifier_1_Report_m3209518853_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ScheduledNotifier_1_Report_m3209518853(__this, ___value0, method) ((  void (*) (ScheduledNotifier_1_t2357123231 *, Il2CppObject *, const MethodInfo*))ScheduledNotifier_1_Report_m3209518853_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ScheduledNotifier`1<System.Object>::Report(T,System.TimeSpan)
extern "C"  Il2CppObject * ScheduledNotifier_1_Report_m1320187968_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject * ___value0, TimeSpan_t763862892  ___dueTime1, const MethodInfo* method);
#define ScheduledNotifier_1_Report_m1320187968(__this, ___value0, ___dueTime1, method) ((  Il2CppObject * (*) (ScheduledNotifier_1_t2357123231 *, Il2CppObject *, TimeSpan_t763862892 , const MethodInfo*))ScheduledNotifier_1_Report_m1320187968_gshared)(__this, ___value0, ___dueTime1, method)
// System.IDisposable UniRx.ScheduledNotifier`1<System.Object>::Report(T,System.DateTimeOffset)
extern "C"  Il2CppObject * ScheduledNotifier_1_Report_m1244727369_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject * ___value0, DateTimeOffset_t3712260035  ___dueTime1, const MethodInfo* method);
#define ScheduledNotifier_1_Report_m1244727369(__this, ___value0, ___dueTime1, method) ((  Il2CppObject * (*) (ScheduledNotifier_1_t2357123231 *, Il2CppObject *, DateTimeOffset_t3712260035 , const MethodInfo*))ScheduledNotifier_1_Report_m1244727369_gshared)(__this, ___value0, ___dueTime1, method)
// System.IDisposable UniRx.ScheduledNotifier`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ScheduledNotifier_1_Subscribe_m4082606558_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ScheduledNotifier_1_Subscribe_m4082606558(__this, ___observer0, method) ((  Il2CppObject * (*) (ScheduledNotifier_1_t2357123231 *, Il2CppObject*, const MethodInfo*))ScheduledNotifier_1_Subscribe_m4082606558_gshared)(__this, ___observer0, method)
