﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct ListObserver_1_t4073544734;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>>
struct ImmutableList_1_t2756163169;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3052252423_gshared (ListObserver_1_t4073544734 * __this, ImmutableList_1_t2756163169 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m3052252423(__this, ___observers0, method) ((  void (*) (ListObserver_1_t4073544734 *, ImmutableList_1_t2756163169 *, const MethodInfo*))ListObserver_1__ctor_m3052252423_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2011051775_gshared (ListObserver_1_t4073544734 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m2011051775(__this, method) ((  void (*) (ListObserver_1_t4073544734 *, const MethodInfo*))ListObserver_1_OnCompleted_m2011051775_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m4016791980_gshared (ListObserver_1_t4073544734 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m4016791980(__this, ___error0, method) ((  void (*) (ListObserver_1_t4073544734 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m4016791980_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m70918301_gshared (ListObserver_1_t4073544734 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m70918301(__this, ___value0, method) ((  void (*) (ListObserver_1_t4073544734 *, DictionaryReplaceEvent_2_t3778927063 , const MethodInfo*))ListObserver_1_OnNext_m70918301_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3488575599_gshared (ListObserver_1_t4073544734 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m3488575599(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t4073544734 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m3488575599_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m3265454204_gshared (ListObserver_1_t4073544734 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m3265454204(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t4073544734 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m3265454204_gshared)(__this, ___observer0, method)
