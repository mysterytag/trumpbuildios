﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<Zenject.InitializableManager/InitializableInfo,Zenject.IInitializable>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1149167462(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2009286642 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Zenject.InitializableManager/InitializableInfo,Zenject.IInitializable>::Invoke(T)
#define Func_2_Invoke_m3453656364(__this, ___arg10, method) ((  Il2CppObject * (*) (Func_2_t2009286642 *, InitializableInfo_t3830632317 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Zenject.InitializableManager/InitializableInfo,Zenject.IInitializable>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3024700379(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2009286642 *, InitializableInfo_t3830632317 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Zenject.InitializableManager/InitializableInfo,Zenject.IInitializable>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2678708776(__this, ___result0, method) ((  Il2CppObject * (*) (Func_2_t2009286642 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
