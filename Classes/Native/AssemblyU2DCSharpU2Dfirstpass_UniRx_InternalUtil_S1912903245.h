﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.BooleanDisposable
struct BooleanDisposable_t3065601722;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ScheduledItem
struct  ScheduledItem_t1912903245  : public Il2CppObject
{
public:
	// UniRx.BooleanDisposable UniRx.InternalUtil.ScheduledItem::_disposable
	BooleanDisposable_t3065601722 * ____disposable_0;
	// System.TimeSpan UniRx.InternalUtil.ScheduledItem::_dueTime
	TimeSpan_t763862892  ____dueTime_1;
	// System.Action UniRx.InternalUtil.ScheduledItem::_action
	Action_t437523947 * ____action_2;

public:
	inline static int32_t get_offset_of__disposable_0() { return static_cast<int32_t>(offsetof(ScheduledItem_t1912903245, ____disposable_0)); }
	inline BooleanDisposable_t3065601722 * get__disposable_0() const { return ____disposable_0; }
	inline BooleanDisposable_t3065601722 ** get_address_of__disposable_0() { return &____disposable_0; }
	inline void set__disposable_0(BooleanDisposable_t3065601722 * value)
	{
		____disposable_0 = value;
		Il2CppCodeGenWriteBarrier(&____disposable_0, value);
	}

	inline static int32_t get_offset_of__dueTime_1() { return static_cast<int32_t>(offsetof(ScheduledItem_t1912903245, ____dueTime_1)); }
	inline TimeSpan_t763862892  get__dueTime_1() const { return ____dueTime_1; }
	inline TimeSpan_t763862892 * get_address_of__dueTime_1() { return &____dueTime_1; }
	inline void set__dueTime_1(TimeSpan_t763862892  value)
	{
		____dueTime_1 = value;
	}

	inline static int32_t get_offset_of__action_2() { return static_cast<int32_t>(offsetof(ScheduledItem_t1912903245, ____action_2)); }
	inline Action_t437523947 * get__action_2() const { return ____action_2; }
	inline Action_t437523947 ** get_address_of__action_2() { return &____action_2; }
	inline void set__action_2(Action_t437523947 * value)
	{
		____action_2 = value;
		Il2CppCodeGenWriteBarrier(&____action_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
