﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String GlobalStat/PushMessages::get_title()
extern "C"  String_t* PushMessages_get_title_m3280143741 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalStat/PushMessages::get_day1()
extern "C"  String_t* PushMessages_get_day1_m610278194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalStat/PushMessages::get_day3()
extern "C"  String_t* PushMessages_get_day3_m610280116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalStat/PushMessages::get_day7()
extern "C"  String_t* PushMessages_get_day7_m610283960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalStat/PushMessages::get_day14()
extern "C"  String_t* PushMessages_get_day14_m1738766372 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalStat/PushMessages::get_safe()
extern "C"  String_t* PushMessages_get_safe_m1039199402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
