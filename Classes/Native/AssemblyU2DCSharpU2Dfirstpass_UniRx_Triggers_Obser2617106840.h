﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.Collision2D>
struct Subject_1_t2390844653;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableCollision2DTrigger
struct  ObservableCollision2DTrigger_t2617106840  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::onCollisionEnter2D
	Subject_1_t2390844653 * ___onCollisionEnter2D_8;
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::onCollisionExit2D
	Subject_1_t2390844653 * ___onCollisionExit2D_9;
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::onCollisionStay2D
	Subject_1_t2390844653 * ___onCollisionStay2D_10;

public:
	inline static int32_t get_offset_of_onCollisionEnter2D_8() { return static_cast<int32_t>(offsetof(ObservableCollision2DTrigger_t2617106840, ___onCollisionEnter2D_8)); }
	inline Subject_1_t2390844653 * get_onCollisionEnter2D_8() const { return ___onCollisionEnter2D_8; }
	inline Subject_1_t2390844653 ** get_address_of_onCollisionEnter2D_8() { return &___onCollisionEnter2D_8; }
	inline void set_onCollisionEnter2D_8(Subject_1_t2390844653 * value)
	{
		___onCollisionEnter2D_8 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionEnter2D_8, value);
	}

	inline static int32_t get_offset_of_onCollisionExit2D_9() { return static_cast<int32_t>(offsetof(ObservableCollision2DTrigger_t2617106840, ___onCollisionExit2D_9)); }
	inline Subject_1_t2390844653 * get_onCollisionExit2D_9() const { return ___onCollisionExit2D_9; }
	inline Subject_1_t2390844653 ** get_address_of_onCollisionExit2D_9() { return &___onCollisionExit2D_9; }
	inline void set_onCollisionExit2D_9(Subject_1_t2390844653 * value)
	{
		___onCollisionExit2D_9 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionExit2D_9, value);
	}

	inline static int32_t get_offset_of_onCollisionStay2D_10() { return static_cast<int32_t>(offsetof(ObservableCollision2DTrigger_t2617106840, ___onCollisionStay2D_10)); }
	inline Subject_1_t2390844653 * get_onCollisionStay2D_10() const { return ___onCollisionStay2D_10; }
	inline Subject_1_t2390844653 ** get_address_of_onCollisionStay2D_10() { return &___onCollisionStay2D_10; }
	inline void set_onCollisionStay2D_10(Subject_1_t2390844653 * value)
	{
		___onCollisionStay2D_10 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionStay2D_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
