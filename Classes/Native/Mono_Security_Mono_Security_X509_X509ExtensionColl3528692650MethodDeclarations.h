﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t3528692651;
// Mono.Security.ASN1
struct ASN1_t1254135647;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// Mono.Security.X509.X509Extension
struct X509Extension_t1510964269;
// Mono.Security.X509.X509Extension[]
struct X509ExtensionU5BU5D_t3939176869;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_ASN11254135646.h"
#include "Mono_Security_Mono_Security_X509_X509Extension1510964268.h"
#include "Mono_Security_Mono_Security_X509_X509ExtensionColl3528692650.h"
#include "mscorlib_System_String968488902.h"

// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor()
extern "C"  void X509ExtensionCollection__ctor_m3777468326 (X509ExtensionCollection_t3528692651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor(Mono.Security.ASN1)
extern "C"  void X509ExtensionCollection__ctor_m2115523054 (X509ExtensionCollection_t3528692651 * __this, ASN1_t1254135647 * ___asn10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Security.X509.X509ExtensionCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m2588592549 (X509ExtensionCollection_t3528692651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::Add(Mono.Security.X509.X509Extension)
extern "C"  int32_t X509ExtensionCollection_Add_m2205399209 (X509ExtensionCollection_t3528692651 * __this, X509Extension_t1510964269 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::AddRange(Mono.Security.X509.X509Extension[])
extern "C"  void X509ExtensionCollection_AddRange_m2564199620 (X509ExtensionCollection_t3528692651 * __this, X509ExtensionU5BU5D_t3939176869* ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::AddRange(Mono.Security.X509.X509ExtensionCollection)
extern "C"  void X509ExtensionCollection_AddRange_m3730961384 (X509ExtensionCollection_t3528692651 * __this, X509ExtensionCollection_t3528692651 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509ExtensionCollection::Contains(Mono.Security.X509.X509Extension)
extern "C"  bool X509ExtensionCollection_Contains_m2787305271 (X509ExtensionCollection_t3528692651 * __this, X509Extension_t1510964269 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509ExtensionCollection::Contains(System.String)
extern "C"  bool X509ExtensionCollection_Contains_m3810894935 (X509ExtensionCollection_t3528692651 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::CopyTo(Mono.Security.X509.X509Extension[],System.Int32)
extern "C"  void X509ExtensionCollection_CopyTo_m2099992095 (X509ExtensionCollection_t3528692651 * __this, X509ExtensionU5BU5D_t3939176869* ___extensions0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(Mono.Security.X509.X509Extension)
extern "C"  int32_t X509ExtensionCollection_IndexOf_m1666537169 (X509ExtensionCollection_t3528692651 * __this, X509Extension_t1510964269 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(System.String)
extern "C"  int32_t X509ExtensionCollection_IndexOf_m1626273533 (X509ExtensionCollection_t3528692651 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Insert(System.Int32,Mono.Security.X509.X509Extension)
extern "C"  void X509ExtensionCollection_Insert_m419740490 (X509ExtensionCollection_t3528692651 * __this, int32_t ___index0, X509Extension_t1510964269 * ___extension1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Remove(Mono.Security.X509.X509Extension)
extern "C"  void X509ExtensionCollection_Remove_m3833643982 (X509ExtensionCollection_t3528692651 * __this, X509Extension_t1510964269 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Remove(System.String)
extern "C"  void X509ExtensionCollection_Remove_m2708248864 (X509ExtensionCollection_t3528692651 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.Int32)
extern "C"  X509Extension_t1510964269 * X509ExtensionCollection_get_Item_m1478504974 (X509ExtensionCollection_t3528692651 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.String)
extern "C"  X509Extension_t1510964269 * X509ExtensionCollection_get_Item_m3340679653 (X509ExtensionCollection_t3528692651 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509ExtensionCollection::GetBytes()
extern "C"  ByteU5BU5D_t58506160* X509ExtensionCollection_GetBytes_m1424675453 (X509ExtensionCollection_t3528692651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
