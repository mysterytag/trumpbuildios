﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<AchievementReporter>c__Iterator16
struct U3CAchievementReporterU3Ec__Iterator16_t1048801228;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<AchievementReporter>c__Iterator16::.ctor()
extern "C"  void U3CAchievementReporterU3Ec__Iterator16__ctor_m2929283038 (U3CAchievementReporterU3Ec__Iterator16_t1048801228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<AchievementReporter>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAchievementReporterU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m580513140 (U3CAchievementReporterU3Ec__Iterator16_t1048801228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<AchievementReporter>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAchievementReporterU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m3256703240 (U3CAchievementReporterU3Ec__Iterator16_t1048801228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<AchievementReporter>c__Iterator16::MoveNext()
extern "C"  bool U3CAchievementReporterU3Ec__Iterator16_MoveNext_m429997654 (U3CAchievementReporterU3Ec__Iterator16_t1048801228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<AchievementReporter>c__Iterator16::Dispose()
extern "C"  void U3CAchievementReporterU3Ec__Iterator16_Dispose_m1735703707 (U3CAchievementReporterU3Ec__Iterator16_t1048801228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<AchievementReporter>c__Iterator16::Reset()
extern "C"  void U3CAchievementReporterU3Ec__Iterator16_Reset_m575715979 (U3CAchievementReporterU3Ec__Iterator16_t1048801228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
