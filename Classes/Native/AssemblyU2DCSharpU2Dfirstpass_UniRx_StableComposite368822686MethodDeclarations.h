﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.StableCompositeDisposable/Quaternary
struct Quaternary_t368822686;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.StableCompositeDisposable/Quaternary::.ctor(System.IDisposable,System.IDisposable,System.IDisposable,System.IDisposable)
extern "C"  void Quaternary__ctor_m3164804208 (Quaternary_t368822686 * __this, Il2CppObject * ___disposable10, Il2CppObject * ___disposable21, Il2CppObject * ___disposable32, Il2CppObject * ___disposable43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.StableCompositeDisposable/Quaternary::get_IsDisposed()
extern "C"  bool Quaternary_get_IsDisposed_m2650001090 (Quaternary_t368822686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.StableCompositeDisposable/Quaternary::Dispose()
extern "C"  void Quaternary_Dispose_m1790277075 (Quaternary_t368822686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
