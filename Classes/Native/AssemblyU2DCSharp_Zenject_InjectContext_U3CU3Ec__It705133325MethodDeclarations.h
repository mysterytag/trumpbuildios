﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InjectContext/<>c__Iterator42
struct U3CU3Ec__Iterator42_t705133325;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext>
struct IEnumerator_1_t644623043;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.InjectContext/<>c__Iterator42::.ctor()
extern "C"  void U3CU3Ec__Iterator42__ctor_m592352782 (U3CU3Ec__Iterator42_t705133325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectContext Zenject.InjectContext/<>c__Iterator42::System.Collections.Generic.IEnumerator<Zenject.InjectContext>.get_Current()
extern "C"  InjectContext_t3456483891 * U3CU3Ec__Iterator42_System_Collections_Generic_IEnumeratorU3CZenject_InjectContextU3E_get_Current_m2053444031 (U3CU3Ec__Iterator42_t705133325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.InjectContext/<>c__Iterator42::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator42_System_Collections_IEnumerator_get_Current_m2398787928 (U3CU3Ec__Iterator42_t705133325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.InjectContext/<>c__Iterator42::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator42_System_Collections_IEnumerable_GetEnumerator_m811530131 (U3CU3Ec__Iterator42_t705133325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext/<>c__Iterator42::System.Collections.Generic.IEnumerable<Zenject.InjectContext>.GetEnumerator()
extern "C"  Il2CppObject* U3CU3Ec__Iterator42_System_Collections_Generic_IEnumerableU3CZenject_InjectContextU3E_GetEnumerator_m4243505976 (U3CU3Ec__Iterator42_t705133325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.InjectContext/<>c__Iterator42::MoveNext()
extern "C"  bool U3CU3Ec__Iterator42_MoveNext_m390159398 (U3CU3Ec__Iterator42_t705133325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext/<>c__Iterator42::Dispose()
extern "C"  void U3CU3Ec__Iterator42_Dispose_m2213623499 (U3CU3Ec__Iterator42_t705133325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext/<>c__Iterator42::Reset()
extern "C"  void U3CU3Ec__Iterator42_Reset_m2533753019 (U3CU3Ec__Iterator42_t705133325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
