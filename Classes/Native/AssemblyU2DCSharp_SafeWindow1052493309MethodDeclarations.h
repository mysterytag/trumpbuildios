﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SafeWindow
struct SafeWindow_t1052493309;
// GlobalStat
struct GlobalStat_t1134678199;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharp_GlobalStat1134678199.h"

// System.Void SafeWindow::.ctor()
extern "C"  void SafeWindow__ctor_m2953985854 (SafeWindow_t1052493309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeWindow::OnEnable()
extern "C"  void SafeWindow_OnEnable_m384259976 (SafeWindow_t1052493309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeWindow::<OnEnable>m__122(UniRx.Unit)
extern "C"  void SafeWindow_U3COnEnableU3Em__122_m4210090990 (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeWindow::<OnEnable>m__123(UniRx.Unit)
extern "C"  void SafeWindow_U3COnEnableU3Em__123_m3916687983 (SafeWindow_t1052493309 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeWindow::<OnEnable>m__124(UniRx.Unit)
extern "C"  void SafeWindow_U3COnEnableU3Em__124_m3623284976 (SafeWindow_t1052493309 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double SafeWindow::<OnEnable>m__125(GlobalStat)
extern "C"  double SafeWindow_U3COnEnableU3Em__125_m212860633 (Il2CppObject * __this /* static, unused */, GlobalStat_t1134678199 * ___globalStat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SafeWindow::<OnEnable>m__126(System.Double)
extern "C"  void SafeWindow_U3COnEnableU3Em__126_m1088479220 (SafeWindow_t1052493309 * __this, double ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
