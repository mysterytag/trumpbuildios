﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Builder
struct X509Builder_t1138312712;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t4236534322;
// Mono.Security.ASN1
struct ASN1_t1254135647;
// System.Security.Cryptography.RSA
struct RSA_t1557565273;
// System.Security.Cryptography.DSA
struct DSA_t1557551819;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAl4236534322.h"
#include "Mono_Security_Mono_Security_ASN11254135646.h"
#include "mscorlib_System_Security_Cryptography_RSA1557565273.h"
#include "mscorlib_System_Security_Cryptography_DSA1557551819.h"

// System.Void Mono.Security.X509.X509Builder::.ctor()
extern "C"  void X509Builder__ctor_m372332040 (X509Builder_t1138312712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Builder::GetOid(System.String)
extern "C"  String_t* X509Builder_GetOid_m3589226095 (X509Builder_t1138312712 * __this, String_t* ___hashName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Builder::get_Hash()
extern "C"  String_t* X509Builder_get_Hash_m3512103158 (X509Builder_t1138312712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Builder::set_Hash(System.String)
extern "C"  void X509Builder_set_Hash_m3295921563 (X509Builder_t1138312712 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Builder::Sign(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C"  ByteU5BU5D_t58506160* X509Builder_Sign_m3870753565 (X509Builder_t1138312712 * __this, AsymmetricAlgorithm_t4236534322 * ___aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Builder::Build(Mono.Security.ASN1,System.String,System.Byte[])
extern "C"  ByteU5BU5D_t58506160* X509Builder_Build_m2810447665 (X509Builder_t1138312712 * __this, ASN1_t1254135647 * ___tbs0, String_t* ___hashoid1, ByteU5BU5D_t58506160* ___signature2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Builder::Sign(System.Security.Cryptography.RSA)
extern "C"  ByteU5BU5D_t58506160* X509Builder_Sign_m3300898006 (X509Builder_t1138312712 * __this, RSA_t1557565273 * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Builder::Sign(System.Security.Cryptography.DSA)
extern "C"  ByteU5BU5D_t58506160* X509Builder_Sign_m3300480932 (X509Builder_t1138312712 * __this, DSA_t1557551819 * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
