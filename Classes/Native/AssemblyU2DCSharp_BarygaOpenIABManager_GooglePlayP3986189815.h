﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaOpenIABManager/GooglePlayPurchaseGuard
struct  GooglePlayPurchaseGuard_t3986189815  : public Il2CppObject
{
public:

public:
};

struct GooglePlayPurchaseGuard_t3986189815_StaticFields
{
public:
	// System.String BarygaOpenIABManager/GooglePlayPurchaseGuard::xmlPublicKey
	String_t* ___xmlPublicKey_0;

public:
	inline static int32_t get_offset_of_xmlPublicKey_0() { return static_cast<int32_t>(offsetof(GooglePlayPurchaseGuard_t3986189815_StaticFields, ___xmlPublicKey_0)); }
	inline String_t* get_xmlPublicKey_0() const { return ___xmlPublicKey_0; }
	inline String_t** get_address_of_xmlPublicKey_0() { return &___xmlPublicKey_0; }
	inline void set_xmlPublicKey_0(String_t* value)
	{
		___xmlPublicKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___xmlPublicKey_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
