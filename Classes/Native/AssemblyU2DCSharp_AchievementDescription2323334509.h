﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AchievementDescription
struct  AchievementDescription_t2323334509  : public Il2CppObject
{
public:
	// System.String AchievementDescription::id
	String_t* ___id_0;
	// System.String AchievementDescription::nameStringId
	String_t* ___nameStringId_1;
	// System.String AchievementDescription::descriptionStringId
	String_t* ___descriptionStringId_2;
	// System.String AchievementDescription::androidId
	String_t* ___androidId_3;
	// System.String AchievementDescription::iosId
	String_t* ___iosId_4;
	// System.String AchievementDescription::urlFbImage
	String_t* ___urlFbImage_5;
	// System.String AchievementDescription::urlVkImage
	String_t* ___urlVkImage_6;
	// System.String AchievementDescription::urlId
	String_t* ___urlId_7;
	// System.String AchievementDescription::NameRu
	String_t* ___NameRu_8;
	// System.String AchievementDescription::NameEn
	String_t* ___NameEn_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_nameStringId_1() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___nameStringId_1)); }
	inline String_t* get_nameStringId_1() const { return ___nameStringId_1; }
	inline String_t** get_address_of_nameStringId_1() { return &___nameStringId_1; }
	inline void set_nameStringId_1(String_t* value)
	{
		___nameStringId_1 = value;
		Il2CppCodeGenWriteBarrier(&___nameStringId_1, value);
	}

	inline static int32_t get_offset_of_descriptionStringId_2() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___descriptionStringId_2)); }
	inline String_t* get_descriptionStringId_2() const { return ___descriptionStringId_2; }
	inline String_t** get_address_of_descriptionStringId_2() { return &___descriptionStringId_2; }
	inline void set_descriptionStringId_2(String_t* value)
	{
		___descriptionStringId_2 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionStringId_2, value);
	}

	inline static int32_t get_offset_of_androidId_3() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___androidId_3)); }
	inline String_t* get_androidId_3() const { return ___androidId_3; }
	inline String_t** get_address_of_androidId_3() { return &___androidId_3; }
	inline void set_androidId_3(String_t* value)
	{
		___androidId_3 = value;
		Il2CppCodeGenWriteBarrier(&___androidId_3, value);
	}

	inline static int32_t get_offset_of_iosId_4() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___iosId_4)); }
	inline String_t* get_iosId_4() const { return ___iosId_4; }
	inline String_t** get_address_of_iosId_4() { return &___iosId_4; }
	inline void set_iosId_4(String_t* value)
	{
		___iosId_4 = value;
		Il2CppCodeGenWriteBarrier(&___iosId_4, value);
	}

	inline static int32_t get_offset_of_urlFbImage_5() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___urlFbImage_5)); }
	inline String_t* get_urlFbImage_5() const { return ___urlFbImage_5; }
	inline String_t** get_address_of_urlFbImage_5() { return &___urlFbImage_5; }
	inline void set_urlFbImage_5(String_t* value)
	{
		___urlFbImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___urlFbImage_5, value);
	}

	inline static int32_t get_offset_of_urlVkImage_6() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___urlVkImage_6)); }
	inline String_t* get_urlVkImage_6() const { return ___urlVkImage_6; }
	inline String_t** get_address_of_urlVkImage_6() { return &___urlVkImage_6; }
	inline void set_urlVkImage_6(String_t* value)
	{
		___urlVkImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___urlVkImage_6, value);
	}

	inline static int32_t get_offset_of_urlId_7() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___urlId_7)); }
	inline String_t* get_urlId_7() const { return ___urlId_7; }
	inline String_t** get_address_of_urlId_7() { return &___urlId_7; }
	inline void set_urlId_7(String_t* value)
	{
		___urlId_7 = value;
		Il2CppCodeGenWriteBarrier(&___urlId_7, value);
	}

	inline static int32_t get_offset_of_NameRu_8() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___NameRu_8)); }
	inline String_t* get_NameRu_8() const { return ___NameRu_8; }
	inline String_t** get_address_of_NameRu_8() { return &___NameRu_8; }
	inline void set_NameRu_8(String_t* value)
	{
		___NameRu_8 = value;
		Il2CppCodeGenWriteBarrier(&___NameRu_8, value);
	}

	inline static int32_t get_offset_of_NameEn_9() { return static_cast<int32_t>(offsetof(AchievementDescription_t2323334509, ___NameEn_9)); }
	inline String_t* get_NameEn_9() const { return ___NameEn_9; }
	inline String_t** get_address_of_NameEn_9() { return &___NameEn_9; }
	inline void set_NameEn_9(String_t* value)
	{
		___NameEn_9 = value;
		Il2CppCodeGenWriteBarrier(&___NameEn_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
