﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryUntypedBinder`1/<ToInstance>c__AnonStorey17E<System.Object>
struct U3CToInstanceU3Ec__AnonStorey17E_t3854809107;
// System.Object
struct Il2CppObject;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"

// System.Void Zenject.IFactoryUntypedBinder`1/<ToInstance>c__AnonStorey17E<System.Object>::.ctor()
extern "C"  void U3CToInstanceU3Ec__AnonStorey17E__ctor_m988856348_gshared (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 * __this, const MethodInfo* method);
#define U3CToInstanceU3Ec__AnonStorey17E__ctor_m988856348(__this, method) ((  void (*) (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 *, const MethodInfo*))U3CToInstanceU3Ec__AnonStorey17E__ctor_m988856348_gshared)(__this, method)
// TContract Zenject.IFactoryUntypedBinder`1/<ToInstance>c__AnonStorey17E<System.Object>::<>m__2A8(Zenject.DiContainer,System.Object[])
extern "C"  Il2CppObject * U3CToInstanceU3Ec__AnonStorey17E_U3CU3Em__2A8_m1660689032_gshared (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 * __this, DiContainer_t2383114449 * ___c0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method);
#define U3CToInstanceU3Ec__AnonStorey17E_U3CU3Em__2A8_m1660689032(__this, ___c0, ___args1, method) ((  Il2CppObject * (*) (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 *, DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))U3CToInstanceU3Ec__AnonStorey17E_U3CU3Em__2A8_m1660689032_gshared)(__this, ___c0, ___args1, method)
