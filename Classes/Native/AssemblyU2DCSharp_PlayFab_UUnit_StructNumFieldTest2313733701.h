﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType4014882752.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_StructNumFieldTest2313733701.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.StructNumFieldTest
struct  StructNumFieldTest_t2313733701 
{
public:
	// System.SByte PlayFab.UUnit.StructNumFieldTest::SbyteValue
	int8_t ___SbyteValue_0;
	// System.Byte PlayFab.UUnit.StructNumFieldTest::ByteValue
	uint8_t ___ByteValue_1;
	// System.Int16 PlayFab.UUnit.StructNumFieldTest::ShortValue
	int16_t ___ShortValue_2;
	// System.UInt16 PlayFab.UUnit.StructNumFieldTest::UshortValue
	uint16_t ___UshortValue_3;
	// System.Int32 PlayFab.UUnit.StructNumFieldTest::IntValue
	int32_t ___IntValue_4;
	// System.UInt32 PlayFab.UUnit.StructNumFieldTest::UintValue
	uint32_t ___UintValue_5;
	// System.Int64 PlayFab.UUnit.StructNumFieldTest::LongValue
	int64_t ___LongValue_6;
	// System.UInt64 PlayFab.UUnit.StructNumFieldTest::UlongValue
	uint64_t ___UlongValue_7;
	// System.Single PlayFab.UUnit.StructNumFieldTest::FloatValue
	float ___FloatValue_8;
	// System.Double PlayFab.UUnit.StructNumFieldTest::DoubleValue
	double ___DoubleValue_9;

public:
	inline static int32_t get_offset_of_SbyteValue_0() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___SbyteValue_0)); }
	inline int8_t get_SbyteValue_0() const { return ___SbyteValue_0; }
	inline int8_t* get_address_of_SbyteValue_0() { return &___SbyteValue_0; }
	inline void set_SbyteValue_0(int8_t value)
	{
		___SbyteValue_0 = value;
	}

	inline static int32_t get_offset_of_ByteValue_1() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___ByteValue_1)); }
	inline uint8_t get_ByteValue_1() const { return ___ByteValue_1; }
	inline uint8_t* get_address_of_ByteValue_1() { return &___ByteValue_1; }
	inline void set_ByteValue_1(uint8_t value)
	{
		___ByteValue_1 = value;
	}

	inline static int32_t get_offset_of_ShortValue_2() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___ShortValue_2)); }
	inline int16_t get_ShortValue_2() const { return ___ShortValue_2; }
	inline int16_t* get_address_of_ShortValue_2() { return &___ShortValue_2; }
	inline void set_ShortValue_2(int16_t value)
	{
		___ShortValue_2 = value;
	}

	inline static int32_t get_offset_of_UshortValue_3() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___UshortValue_3)); }
	inline uint16_t get_UshortValue_3() const { return ___UshortValue_3; }
	inline uint16_t* get_address_of_UshortValue_3() { return &___UshortValue_3; }
	inline void set_UshortValue_3(uint16_t value)
	{
		___UshortValue_3 = value;
	}

	inline static int32_t get_offset_of_IntValue_4() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___IntValue_4)); }
	inline int32_t get_IntValue_4() const { return ___IntValue_4; }
	inline int32_t* get_address_of_IntValue_4() { return &___IntValue_4; }
	inline void set_IntValue_4(int32_t value)
	{
		___IntValue_4 = value;
	}

	inline static int32_t get_offset_of_UintValue_5() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___UintValue_5)); }
	inline uint32_t get_UintValue_5() const { return ___UintValue_5; }
	inline uint32_t* get_address_of_UintValue_5() { return &___UintValue_5; }
	inline void set_UintValue_5(uint32_t value)
	{
		___UintValue_5 = value;
	}

	inline static int32_t get_offset_of_LongValue_6() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___LongValue_6)); }
	inline int64_t get_LongValue_6() const { return ___LongValue_6; }
	inline int64_t* get_address_of_LongValue_6() { return &___LongValue_6; }
	inline void set_LongValue_6(int64_t value)
	{
		___LongValue_6 = value;
	}

	inline static int32_t get_offset_of_UlongValue_7() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___UlongValue_7)); }
	inline uint64_t get_UlongValue_7() const { return ___UlongValue_7; }
	inline uint64_t* get_address_of_UlongValue_7() { return &___UlongValue_7; }
	inline void set_UlongValue_7(uint64_t value)
	{
		___UlongValue_7 = value;
	}

	inline static int32_t get_offset_of_FloatValue_8() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___FloatValue_8)); }
	inline float get_FloatValue_8() const { return ___FloatValue_8; }
	inline float* get_address_of_FloatValue_8() { return &___FloatValue_8; }
	inline void set_FloatValue_8(float value)
	{
		___FloatValue_8 = value;
	}

	inline static int32_t get_offset_of_DoubleValue_9() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701, ___DoubleValue_9)); }
	inline double get_DoubleValue_9() const { return ___DoubleValue_9; }
	inline double* get_address_of_DoubleValue_9() { return &___DoubleValue_9; }
	inline void set_DoubleValue_9(double value)
	{
		___DoubleValue_9 = value;
	}
};

struct StructNumFieldTest_t2313733701_StaticFields
{
public:
	// PlayFab.UUnit.StructNumFieldTest PlayFab.UUnit.StructNumFieldTest::Max
	StructNumFieldTest_t2313733701  ___Max_10;
	// PlayFab.UUnit.StructNumFieldTest PlayFab.UUnit.StructNumFieldTest::Min
	StructNumFieldTest_t2313733701  ___Min_11;
	// PlayFab.UUnit.StructNumFieldTest PlayFab.UUnit.StructNumFieldTest::Zero
	StructNumFieldTest_t2313733701  ___Zero_12;

public:
	inline static int32_t get_offset_of_Max_10() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701_StaticFields, ___Max_10)); }
	inline StructNumFieldTest_t2313733701  get_Max_10() const { return ___Max_10; }
	inline StructNumFieldTest_t2313733701 * get_address_of_Max_10() { return &___Max_10; }
	inline void set_Max_10(StructNumFieldTest_t2313733701  value)
	{
		___Max_10 = value;
	}

	inline static int32_t get_offset_of_Min_11() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701_StaticFields, ___Min_11)); }
	inline StructNumFieldTest_t2313733701  get_Min_11() const { return ___Min_11; }
	inline StructNumFieldTest_t2313733701 * get_address_of_Min_11() { return &___Min_11; }
	inline void set_Min_11(StructNumFieldTest_t2313733701  value)
	{
		___Min_11 = value;
	}

	inline static int32_t get_offset_of_Zero_12() { return static_cast<int32_t>(offsetof(StructNumFieldTest_t2313733701_StaticFields, ___Zero_12)); }
	inline StructNumFieldTest_t2313733701  get_Zero_12() const { return ___Zero_12; }
	inline StructNumFieldTest_t2313733701 * get_address_of_Zero_12() { return &___Zero_12; }
	inline void set_Zero_12(StructNumFieldTest_t2313733701  value)
	{
		___Zero_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: PlayFab.UUnit.StructNumFieldTest
struct StructNumFieldTest_t2313733701_marshaled_pinvoke
{
	int8_t ___SbyteValue_0;
	uint8_t ___ByteValue_1;
	int16_t ___ShortValue_2;
	uint16_t ___UshortValue_3;
	int32_t ___IntValue_4;
	uint32_t ___UintValue_5;
	int64_t ___LongValue_6;
	uint64_t ___UlongValue_7;
	float ___FloatValue_8;
	double ___DoubleValue_9;
};
// Native definition for marshalling of: PlayFab.UUnit.StructNumFieldTest
struct StructNumFieldTest_t2313733701_marshaled_com
{
	int8_t ___SbyteValue_0;
	uint8_t ___ByteValue_1;
	int16_t ___ShortValue_2;
	uint16_t ___UshortValue_3;
	int32_t ___IntValue_4;
	uint32_t ___UintValue_5;
	int64_t ___LongValue_6;
	uint64_t ___UlongValue_7;
	float ___FloatValue_8;
	double ___DoubleValue_9;
};
