﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BottomPanel
struct BottomPanel_t1044499065;

#include "codegen/il2cpp-codegen.h"

// System.Void BottomPanel::.ctor()
extern "C"  void BottomPanel__ctor_m4160973074 (BottomPanel_t1044499065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomPanel::Start()
extern "C"  void BottomPanel_Start_m3108110866 (BottomPanel_t1044499065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomPanel::Update()
extern "C"  void BottomPanel_Update_m1868008507 (BottomPanel_t1044499065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomPanel::ToiletOpen()
extern "C"  void BottomPanel_ToiletOpen_m1974174057 (BottomPanel_t1044499065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomPanel::CaseOpen()
extern "C"  void BottomPanel_CaseOpen_m3319265420 (BottomPanel_t1044499065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomPanel::SafeOpen()
extern "C"  void BottomPanel_SafeOpen_m3288318761 (BottomPanel_t1044499065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomPanel::AbacusOpen()
extern "C"  void BottomPanel_AbacusOpen_m3294120509 (BottomPanel_t1044499065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomPanel::allClose()
extern "C"  void BottomPanel_allClose_m421303401 (BottomPanel_t1044499065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
