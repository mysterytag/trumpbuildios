﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.YieldInstruction
struct YieldInstruction_t3557331758;
struct YieldInstruction_t3557331758_marshaled_pinvoke;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

// UnityEngine.YieldInstruction UniRx.FrameCountTypeExtensions::GetYieldInstruction(UniRx.FrameCountType)
extern "C"  YieldInstruction_t3557331758 * FrameCountTypeExtensions_GetYieldInstruction_m1644022139 (Il2CppObject * __this /* static, unused */, int32_t ___frameCountType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
