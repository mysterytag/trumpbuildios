﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectTransientProviderFromPrefabResource
struct GameObjectTransientProviderFromPrefabResource_t280345019;
// System.Type
struct Type_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.GameObjectTransientProviderFromPrefabResource::.ctor(System.Type,Zenject.DiContainer,System.String)
extern "C"  void GameObjectTransientProviderFromPrefabResource__ctor_m1641581920 (GameObjectTransientProviderFromPrefabResource_t280345019 * __this, Type_t * ___concreteType0, DiContainer_t2383114449 * ___container1, String_t* ___resourcePath2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.GameObjectTransientProviderFromPrefabResource::GetInstanceType()
extern "C"  Type_t * GameObjectTransientProviderFromPrefabResource_GetInstanceType_m335873633 (GameObjectTransientProviderFromPrefabResource_t280345019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.GameObjectTransientProviderFromPrefabResource::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * GameObjectTransientProviderFromPrefabResource_GetInstance_m3339600433 (GameObjectTransientProviderFromPrefabResource_t280345019 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectTransientProviderFromPrefabResource::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* GameObjectTransientProviderFromPrefabResource_ValidateBinding_m3966715331 (GameObjectTransientProviderFromPrefabResource_t280345019 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
