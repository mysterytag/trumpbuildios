﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkPSNAccountResponseCallback
struct LinkPSNAccountResponseCallback_t51233890;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkPSNAccountRequest
struct LinkPSNAccountRequest_t4270489187;
// PlayFab.ClientModels.LinkPSNAccountResult
struct LinkPSNAccountResult_t2251115497;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkPSNAcco4270489187.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkPSNAcco2251115497.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkPSNAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkPSNAccountResponseCallback__ctor_m2679392337 (LinkPSNAccountResponseCallback_t51233890 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkPSNAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkPSNAccountRequest,PlayFab.ClientModels.LinkPSNAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkPSNAccountResponseCallback_Invoke_m1361317150 (LinkPSNAccountResponseCallback_t51233890 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkPSNAccountRequest_t4270489187 * ___request2, LinkPSNAccountResult_t2251115497 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkPSNAccountResponseCallback_t51233890(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkPSNAccountRequest_t4270489187 * ___request2, LinkPSNAccountResult_t2251115497 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkPSNAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkPSNAccountRequest,PlayFab.ClientModels.LinkPSNAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkPSNAccountResponseCallback_BeginInvoke_m4260659659 (LinkPSNAccountResponseCallback_t51233890 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkPSNAccountRequest_t4270489187 * ___request2, LinkPSNAccountResult_t2251115497 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkPSNAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkPSNAccountResponseCallback_EndInvoke_m30575329 (LinkPSNAccountResponseCallback_t51233890 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
