﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GenericDataSource_2_gen1679676449MethodDeclarations.h"

// System.Void GenericDataSource`2<FriendInfo,FriendView>::.ctor()
#define GenericDataSource_2__ctor_m3050237074(__this, method) ((  void (*) (GenericDataSource_2_t4294355128 *, const MethodInfo*))GenericDataSource_2__ctor_m1409035199_gshared)(__this, method)
// System.Void GenericDataSource`2<FriendInfo,FriendView>::set_cellPrefab(UnityEngine.GameObject)
#define GenericDataSource_2_set_cellPrefab_m3030033293(__this, ___value0, method) ((  void (*) (GenericDataSource_2_t4294355128 *, GameObject_t4012695102 *, const MethodInfo*))GenericDataSource_2_set_cellPrefab_m1627438784_gshared)(__this, ___value0, method)
// System.Int32 GenericDataSource`2<FriendInfo,FriendView>::GetNumberOfRowsForTableView(Tacticsoft.TableView)
#define GenericDataSource_2_GetNumberOfRowsForTableView_m1912606612(__this, ___tableView0, method) ((  int32_t (*) (GenericDataSource_2_t4294355128 *, TableView_t692333993 *, const MethodInfo*))GenericDataSource_2_GetNumberOfRowsForTableView_m4207950081_gshared)(__this, ___tableView0, method)
// System.Single GenericDataSource`2<FriendInfo,FriendView>::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
#define GenericDataSource_2_GetHeightForRowInTableView_m1041509152(__this, ___tableView0, ___row1, method) ((  float (*) (GenericDataSource_2_t4294355128 *, TableView_t692333993 *, int32_t, const MethodInfo*))GenericDataSource_2_GetHeightForRowInTableView_m2709442445_gshared)(__this, ___tableView0, ___row1, method)
// Tacticsoft.TableViewCell GenericDataSource`2<FriendInfo,FriendView>::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
#define GenericDataSource_2_GetCellForRowInTableView_m1188262419(__this, ___tableView0, ___row1, method) ((  TableViewCell_t776419755 * (*) (GenericDataSource_2_t4294355128 *, TableView_t692333993 *, int32_t, const MethodInfo*))GenericDataSource_2_GetCellForRowInTableView_m102593472_gshared)(__this, ___tableView0, ___row1, method)
