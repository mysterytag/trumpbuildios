﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkXboxAccountResult
struct LinkXboxAccountResult_t4187130925;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkXboxAccountResult::.ctor()
extern "C"  void LinkXboxAccountResult__ctor_m2667636124 (LinkXboxAccountResult_t4187130925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
