﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>
struct ToYieldInstruction_t1672523157;
// UniRx.ObservableYieldInstruction`1<System.Object>
struct ObservableYieldInstruction_1_t2959019304;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>::.ctor(UniRx.ObservableYieldInstruction`1<T>)
extern "C"  void ToYieldInstruction__ctor_m2997720904_gshared (ToYieldInstruction_t1672523157 * __this, ObservableYieldInstruction_1_t2959019304 * ___parent0, const MethodInfo* method);
#define ToYieldInstruction__ctor_m2997720904(__this, ___parent0, method) ((  void (*) (ToYieldInstruction_t1672523157 *, ObservableYieldInstruction_1_t2959019304 *, const MethodInfo*))ToYieldInstruction__ctor_m2997720904_gshared)(__this, ___parent0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>::OnNext(T)
extern "C"  void ToYieldInstruction_OnNext_m4194015193_gshared (ToYieldInstruction_t1672523157 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ToYieldInstruction_OnNext_m4194015193(__this, ___value0, method) ((  void (*) (ToYieldInstruction_t1672523157 *, Il2CppObject *, const MethodInfo*))ToYieldInstruction_OnNext_m4194015193_gshared)(__this, ___value0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>::OnError(System.Exception)
extern "C"  void ToYieldInstruction_OnError_m556720360_gshared (ToYieldInstruction_t1672523157 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ToYieldInstruction_OnError_m556720360(__this, ___error0, method) ((  void (*) (ToYieldInstruction_t1672523157 *, Exception_t1967233988 *, const MethodInfo*))ToYieldInstruction_OnError_m556720360_gshared)(__this, ___error0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Object>::OnCompleted()
extern "C"  void ToYieldInstruction_OnCompleted_m895070267_gshared (ToYieldInstruction_t1672523157 * __this, const MethodInfo* method);
#define ToYieldInstruction_OnCompleted_m895070267(__this, method) ((  void (*) (ToYieldInstruction_t1672523157 *, const MethodInfo*))ToYieldInstruction_OnCompleted_m895070267_gshared)(__this, method)
