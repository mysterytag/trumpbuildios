﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPurchaseResult
struct GetPurchaseResult_t54283620;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void PlayFab.ClientModels.GetPurchaseResult::.ctor()
extern "C"  void GetPurchaseResult__ctor_m1979796805 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetPurchaseResult::get_OrderId()
extern "C"  String_t* GetPurchaseResult_get_OrderId_m2478117824 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_OrderId(System.String)
extern "C"  void GetPurchaseResult_set_OrderId_m458711891 (GetPurchaseResult_t54283620 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetPurchaseResult::get_PaymentProvider()
extern "C"  String_t* GetPurchaseResult_get_PaymentProvider_m18543438 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_PaymentProvider(System.String)
extern "C"  void GetPurchaseResult_set_PaymentProvider_m92608901 (GetPurchaseResult_t54283620 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetPurchaseResult::get_TransactionId()
extern "C"  String_t* GetPurchaseResult_get_TransactionId_m4052469456 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_TransactionId(System.String)
extern "C"  void GetPurchaseResult_set_TransactionId_m768743811 (GetPurchaseResult_t54283620 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetPurchaseResult::get_TransactionStatus()
extern "C"  String_t* GetPurchaseResult_get_TransactionStatus_m3900134887 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_TransactionStatus(System.String)
extern "C"  void GetPurchaseResult_set_TransactionStatus_m1993996108 (GetPurchaseResult_t54283620 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayFab.ClientModels.GetPurchaseResult::get_PurchaseDate()
extern "C"  DateTime_t339033936  GetPurchaseResult_get_PurchaseDate_m190212752 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_PurchaseDate(System.DateTime)
extern "C"  void GetPurchaseResult_set_PurchaseDate_m3153302125 (GetPurchaseResult_t54283620 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetPurchaseResult::get_Items()
extern "C"  List_1_t1322103281 * GetPurchaseResult_get_Items_m3445802675 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_Items(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void GetPurchaseResult_set_Items_m3405947824 (GetPurchaseResult_t54283620 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
