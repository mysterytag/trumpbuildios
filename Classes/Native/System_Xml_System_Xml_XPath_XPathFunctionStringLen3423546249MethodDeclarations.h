﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionStringLength
struct XPathFunctionStringLength_t3423546249;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionStringLength::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionStringLength__ctor_m2699284393 (XPathFunctionStringLength_t3423546249 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionStringLength::get_ReturnType()
extern "C"  int32_t XPathFunctionStringLength_get_ReturnType_m1348968411 (XPathFunctionStringLength_t3423546249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionStringLength::get_Peer()
extern "C"  bool XPathFunctionStringLength_get_Peer_m3797175831 (XPathFunctionStringLength_t3423546249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionStringLength::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionStringLength_Evaluate_m913298362 (XPathFunctionStringLength_t3423546249 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionStringLength::ToString()
extern "C"  String_t* XPathFunctionStringLength_ToString_m2297973747 (XPathFunctionStringLength_t3423546249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
