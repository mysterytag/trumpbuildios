﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey41_2__ctor_m2289349113_gshared (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey41_2__ctor_m2289349113(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey41_2__ctor_m2289349113_gshared)(__this, method)
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,UniRx.Unit>::<>m__41(T1)
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m2794009697_gshared (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m2794009697(__this, ___x0, method) ((  Il2CppObject* (*) (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t3253588399 *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m2794009697_gshared)(__this, ___x0, method)
