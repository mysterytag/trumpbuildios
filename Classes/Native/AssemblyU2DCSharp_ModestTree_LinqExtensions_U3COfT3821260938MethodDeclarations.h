﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.LinqExtensions/<OfType>c__AnonStorey16C`1<System.Object>
struct U3COfTypeU3Ec__AnonStorey16C_1_t3821260938;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ModestTree.LinqExtensions/<OfType>c__AnonStorey16C`1<System.Object>::.ctor()
extern "C"  void U3COfTypeU3Ec__AnonStorey16C_1__ctor_m513912023_gshared (U3COfTypeU3Ec__AnonStorey16C_1_t3821260938 * __this, const MethodInfo* method);
#define U3COfTypeU3Ec__AnonStorey16C_1__ctor_m513912023(__this, method) ((  void (*) (U3COfTypeU3Ec__AnonStorey16C_1_t3821260938 *, const MethodInfo*))U3COfTypeU3Ec__AnonStorey16C_1__ctor_m513912023_gshared)(__this, method)
// System.Boolean ModestTree.LinqExtensions/<OfType>c__AnonStorey16C`1<System.Object>::<>m__288(T)
extern "C"  bool U3COfTypeU3Ec__AnonStorey16C_1_U3CU3Em__288_m2106715230_gshared (U3COfTypeU3Ec__AnonStorey16C_1_t3821260938 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3COfTypeU3Ec__AnonStorey16C_1_U3CU3Em__288_m2106715230(__this, ___x0, method) ((  bool (*) (U3COfTypeU3Ec__AnonStorey16C_1_t3821260938 *, Il2CppObject *, const MethodInfo*))U3COfTypeU3Ec__AnonStorey16C_1_U3CU3Em__288_m2106715230_gshared)(__this, ___x0, method)
