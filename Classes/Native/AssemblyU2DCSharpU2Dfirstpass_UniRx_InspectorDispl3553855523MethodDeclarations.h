﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InspectorDisplayAttribute
struct InspectorDisplayAttribute_t3553855523;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void UniRx.InspectorDisplayAttribute::.ctor(System.String,System.Boolean)
extern "C"  void InspectorDisplayAttribute__ctor_m3611869529 (InspectorDisplayAttribute_t3553855523 * __this, String_t* ___fieldName0, bool ___notifyPropertyChanged1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.InspectorDisplayAttribute::get_FieldName()
extern "C"  String_t* InspectorDisplayAttribute_get_FieldName_m2931401563 (InspectorDisplayAttribute_t3553855523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InspectorDisplayAttribute::set_FieldName(System.String)
extern "C"  void InspectorDisplayAttribute_set_FieldName_m3937563710 (InspectorDisplayAttribute_t3553855523 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.InspectorDisplayAttribute::get_NotifyPropertyChanged()
extern "C"  bool InspectorDisplayAttribute_get_NotifyPropertyChanged_m348085381 (InspectorDisplayAttribute_t3553855523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InspectorDisplayAttribute::set_NotifyPropertyChanged(System.Boolean)
extern "C"  void InspectorDisplayAttribute_set_NotifyPropertyChanged_m2014822060 (InspectorDisplayAttribute_t3553855523 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
