﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<System.Boolean>
struct Reactor_1_t3765777533;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<System.Boolean>::.ctor()
extern "C"  void Reactor_1__ctor_m2182279135_gshared (Reactor_1_t3765777533 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m2182279135(__this, method) ((  void (*) (Reactor_1_t3765777533 *, const MethodInfo*))Reactor_1__ctor_m2182279135_gshared)(__this, method)
// System.Void Reactor`1<System.Boolean>::React(T)
extern "C"  void Reactor_1_React_m2762838562_gshared (Reactor_1_t3765777533 * __this, bool ___t0, const MethodInfo* method);
#define Reactor_1_React_m2762838562(__this, ___t0, method) ((  void (*) (Reactor_1_t3765777533 *, bool, const MethodInfo*))Reactor_1_React_m2762838562_gshared)(__this, ___t0, method)
// System.Void Reactor`1<System.Boolean>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m219389101_gshared (Reactor_1_t3765777533 * __this, bool ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m219389101(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t3765777533 *, bool, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m219389101_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<System.Boolean>::Empty()
extern "C"  bool Reactor_1_Empty_m2243558590_gshared (Reactor_1_t3765777533 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m2243558590(__this, method) ((  bool (*) (Reactor_1_t3765777533 *, const MethodInfo*))Reactor_1_Empty_m2243558590_gshared)(__this, method)
// System.IDisposable Reactor`1<System.Boolean>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m2324894600_gshared (Reactor_1_t3765777533 * __this, Action_1_t359458046 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m2324894600(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t3765777533 *, Action_1_t359458046 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m2324894600_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<System.Boolean>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m601382043_gshared (Reactor_1_t3765777533 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m601382043(__this, method) ((  void (*) (Reactor_1_t3765777533 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m601382043_gshared)(__this, method)
// System.Void Reactor`1<System.Boolean>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3828726844_gshared (Reactor_1_t3765777533 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m3828726844(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t3765777533 *, Action_1_t359458046 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3828726844_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Boolean>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m1313493808_gshared (Reactor_1_t3765777533 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m1313493808(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t3765777533 *, Action_1_t359458046 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m1313493808_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Boolean>::Clear()
extern "C"  void Reactor_1_Clear_m3883379722_gshared (Reactor_1_t3765777533 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m3883379722(__this, method) ((  void (*) (Reactor_1_t3765777533 *, const MethodInfo*))Reactor_1_Clear_m3883379722_gshared)(__this, method)
