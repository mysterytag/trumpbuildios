﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2860670852MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3874550430(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2992053334 *, Dictionary_2_t1069916240 *, const MethodInfo*))ValueCollection__ctor_m1765980492_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m687177172(__this, ___item0, method) ((  void (*) (ValueCollection_t2992053334 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3944294758_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2245751965(__this, method) ((  void (*) (ValueCollection_t2992053334 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3484924207_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3465717270(__this, ___item0, method) ((  bool (*) (ValueCollection_t2992053334 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2955240900_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2565491771(__this, ___item0, method) ((  bool (*) (ValueCollection_t2992053334 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3454345065_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2107280413(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2992053334 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3891928751_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1838331233(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2992053334 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3193432947_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2855157040(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2992053334 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1817785282_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1740893129(__this, method) ((  bool (*) (ValueCollection_t2992053334 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1230416759_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2319374121(__this, method) ((  bool (*) (ValueCollection_t2992053334 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1403071447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1604913051(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2992053334 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m601618121_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1403092837(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2992053334 *, StringU5BU5D_t2956870243*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4072966803_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3356960718(__this, method) ((  Enumerator_t836944183  (*) (ValueCollection_t2992053334 *, const MethodInfo*))ValueCollection_GetEnumerator_m3082107772_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::get_Count()
#define ValueCollection_get_Count_m1135921379(__this, method) ((  int32_t (*) (ValueCollection_t2992053334 *, const MethodInfo*))ValueCollection_get_Count_m2799428497_gshared)(__this, method)
