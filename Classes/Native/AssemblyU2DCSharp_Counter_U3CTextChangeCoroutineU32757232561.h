﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Counter
struct Counter_t2622483932;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Counter/<TextChangeCoroutine>c__Iterator32
struct  U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561  : public Il2CppObject
{
public:
	// System.Single Counter/<TextChangeCoroutine>c__Iterator32::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Counter/<TextChangeCoroutine>c__Iterator32::<processTime>__1
	float ___U3CprocessTimeU3E__1_1;
	// System.Single Counter/<TextChangeCoroutine>c__Iterator32::<deltaTime>__2
	float ___U3CdeltaTimeU3E__2_2;
	// System.Single Counter/<TextChangeCoroutine>c__Iterator32::<ratio>__3
	float ___U3CratioU3E__3_3;
	// System.Int32 Counter/<TextChangeCoroutine>c__Iterator32::$PC
	int32_t ___U24PC_4;
	// System.Object Counter/<TextChangeCoroutine>c__Iterator32::$current
	Il2CppObject * ___U24current_5;
	// Counter Counter/<TextChangeCoroutine>c__Iterator32::<>f__this
	Counter_t2622483932 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CprocessTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561, ___U3CprocessTimeU3E__1_1)); }
	inline float get_U3CprocessTimeU3E__1_1() const { return ___U3CprocessTimeU3E__1_1; }
	inline float* get_address_of_U3CprocessTimeU3E__1_1() { return &___U3CprocessTimeU3E__1_1; }
	inline void set_U3CprocessTimeU3E__1_1(float value)
	{
		___U3CprocessTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaTimeU3E__2_2() { return static_cast<int32_t>(offsetof(U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561, ___U3CdeltaTimeU3E__2_2)); }
	inline float get_U3CdeltaTimeU3E__2_2() const { return ___U3CdeltaTimeU3E__2_2; }
	inline float* get_address_of_U3CdeltaTimeU3E__2_2() { return &___U3CdeltaTimeU3E__2_2; }
	inline void set_U3CdeltaTimeU3E__2_2(float value)
	{
		___U3CdeltaTimeU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__3_3() { return static_cast<int32_t>(offsetof(U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561, ___U3CratioU3E__3_3)); }
	inline float get_U3CratioU3E__3_3() const { return ___U3CratioU3E__3_3; }
	inline float* get_address_of_U3CratioU3E__3_3() { return &___U3CratioU3E__3_3; }
	inline void set_U3CratioU3E__3_3(float value)
	{
		___U3CratioU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561, ___U3CU3Ef__this_6)); }
	inline Counter_t2622483932 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline Counter_t2622483932 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(Counter_t2622483932 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
