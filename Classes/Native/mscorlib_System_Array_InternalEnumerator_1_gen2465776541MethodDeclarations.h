﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2465776541.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void System.Array/InternalEnumerator`1<UniRx.Unit>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m79883757_gshared (InternalEnumerator_1_t2465776541 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m79883757(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2465776541 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m79883757_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.Unit>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1620418195_gshared (InternalEnumerator_1_t2465776541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1620418195(__this, method) ((  void (*) (InternalEnumerator_1_t2465776541 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1620418195_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UniRx.Unit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2667904191_gshared (InternalEnumerator_1_t2465776541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2667904191(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2465776541 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2667904191_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.Unit>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2106035204_gshared (InternalEnumerator_1_t2465776541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2106035204(__this, method) ((  void (*) (InternalEnumerator_1_t2465776541 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2106035204_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UniRx.Unit>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m404209023_gshared (InternalEnumerator_1_t2465776541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m404209023(__this, method) ((  bool (*) (InternalEnumerator_1_t2465776541 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m404209023_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UniRx.Unit>::get_Current()
extern "C"  Unit_t2558286038  InternalEnumerator_1_get_Current_m3348667252_gshared (InternalEnumerator_1_t2465776541 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3348667252(__this, method) ((  Unit_t2558286038  (*) (InternalEnumerator_1_t2465776541 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3348667252_gshared)(__this, method)
