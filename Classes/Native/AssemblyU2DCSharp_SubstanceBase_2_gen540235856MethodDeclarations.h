﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2<System.Object,System.Object>
struct SubstanceBase_2_t540235856;
// System.Object
struct Il2CppObject;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action
struct Action_t437523947;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// SubstanceBase`2/Intrusion<System.Object,System.Object>
struct Intrusion_t3044853612;
// ILiving
struct ILiving_t2639664210;
// IEmptyStream
struct IEmptyStream_t3684082468;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void SubstanceBase`2<System.Object,System.Object>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m1606804868_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method);
#define SubstanceBase_2__ctor_m1606804868(__this, method) ((  void (*) (SubstanceBase_2_t540235856 *, const MethodInfo*))SubstanceBase_2__ctor_m1606804868_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m2566312026_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___baseVal0, const MethodInfo* method);
#define SubstanceBase_2__ctor_m2566312026(__this, ___baseVal0, method) ((  void (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2__ctor_m2566312026_gshared)(__this, ___baseVal0, method)
// T SubstanceBase`2<System.Object,System.Object>::get_baseValue()
extern "C"  Il2CppObject * SubstanceBase_2_get_baseValue_m1000505434_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_baseValue_m1000505434(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, const MethodInfo*))SubstanceBase_2_get_baseValue_m1000505434_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::set_baseValue(T)
extern "C"  void SubstanceBase_2_set_baseValue_m1167683449_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValue_m1167683449(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_set_baseValue_m1167683449_gshared)(__this, ___value0, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m2901627540_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValueReactive_m2901627540(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t540235856 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_set_baseValueReactive_m2901627540_gshared)(__this, ___value0, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m487587186_gshared (SubstanceBase_2_t540235856 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_Bind_m487587186(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, Action_1_t985559125 *, int32_t, const MethodInfo*))SubstanceBase_2_Bind_m487587186_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m1056489513_gshared (SubstanceBase_2_t540235856 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_OnChanged_m1056489513(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, Action_t437523947 *, int32_t, const MethodInfo*))SubstanceBase_2_OnChanged_m1056489513_gshared)(__this, ___action0, ___p1, method)
// System.Object SubstanceBase`2<System.Object,System.Object>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m2572102932_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_valueObject_m2572102932(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, const MethodInfo*))SubstanceBase_2_get_valueObject_m2572102932_gshared)(__this, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m2465048272_gshared (SubstanceBase_2_t540235856 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_ListenUpdates_m2465048272(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, Action_1_t985559125 *, int32_t, const MethodInfo*))SubstanceBase_2_ListenUpdates_m2465048272_gshared)(__this, ___action0, ___p1, method)
// IStream`1<T> SubstanceBase`2<System.Object,System.Object>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m1292576498_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_updates_m1292576498(__this, method) ((  Il2CppObject* (*) (SubstanceBase_2_t540235856 *, const MethodInfo*))SubstanceBase_2_get_updates_m1292576498_gshared)(__this, method)
// T SubstanceBase`2<System.Object,System.Object>::get_value()
extern "C"  Il2CppObject * SubstanceBase_2_get_value_m3139375339_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_value_m3139375339(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, const MethodInfo*))SubstanceBase_2_get_value_m3139375339_gshared)(__this, method)
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Object,System.Object>::AffectInner(ILiving,InfT)
extern "C"  Intrusion_t3044853612 * SubstanceBase_2_AffectInner_m1664732325_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, const MethodInfo* method);
#define SubstanceBase_2_AffectInner_m1664732325(__this, ___intruder0, ___influence1, method) ((  Intrusion_t3044853612 * (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_AffectInner_m1664732325_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::AffectUnsafe(InfT)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m1801912731_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m1801912731(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m1801912731_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::AffectUnsafe(ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m3688879833_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject* ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m3688879833(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m3688879833_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::Affect(ILiving,InfT)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m971001019_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m971001019(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m971001019_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::Affect(ILiving,ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1389957049_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m1389957049(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_Affect_m1389957049_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::Affect(ILiving,InfT,IEmptyStream)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3822360967_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, Il2CppObject * ___update2, const MethodInfo* method);
#define SubstanceBase_2_Affect_m3822360967(__this, ___intruder0, ___influence1, ___update2, method) ((  Il2CppObject * (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m3822360967_gshared)(__this, ___intruder0, ___influence1, ___update2, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m680391404_gshared (SubstanceBase_2_t540235856 * __this, Intrusion_t3044853612 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnAdd_m680391404(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t540235856 *, Intrusion_t3044853612 *, const MethodInfo*))SubstanceBase_2_OnAdd_m680391404_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m3887629549_gshared (SubstanceBase_2_t540235856 * __this, Intrusion_t3044853612 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnRemove_m3887629549(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t540235856 *, Intrusion_t3044853612 *, const MethodInfo*))SubstanceBase_2_OnRemove_m3887629549_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m3238360712_gshared (SubstanceBase_2_t540235856 * __this, Intrusion_t3044853612 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnUpdate_m3238360712(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t540235856 *, Intrusion_t3044853612 *, const MethodInfo*))SubstanceBase_2_OnUpdate_m3238360712_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m2398295098_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method);
#define SubstanceBase_2_UpdateAll_m2398295098(__this, method) ((  void (*) (SubstanceBase_2_t540235856 *, const MethodInfo*))SubstanceBase_2_UpdateAll_m2398295098_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_ClearIntrusion_m1431780278_gshared (SubstanceBase_2_t540235856 * __this, Intrusion_t3044853612 * ___intr0, const MethodInfo* method);
#define SubstanceBase_2_ClearIntrusion_m1431780278(__this, ___intr0, method) ((  void (*) (SubstanceBase_2_t540235856 *, Intrusion_t3044853612 *, const MethodInfo*))SubstanceBase_2_ClearIntrusion_m1431780278_gshared)(__this, ___intr0, method)
// System.Void SubstanceBase`2<System.Object,System.Object>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m725633703_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m725633703(__this, ___val0, method) ((  void (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m725633703_gshared)(__this, ___val0, method)
