﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`1<System.Object>
struct ZipLatestObservable_1_t2717078458;
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ZipLatestObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>[])
extern "C"  void ZipLatestObservable_1__ctor_m2774219989_gshared (ZipLatestObservable_1_t2717078458 * __this, IObservable_1U5BU5D_t2205425841* ___sources0, const MethodInfo* method);
#define ZipLatestObservable_1__ctor_m2774219989(__this, ___sources0, method) ((  void (*) (ZipLatestObservable_1_t2717078458 *, IObservable_1U5BU5D_t2205425841*, const MethodInfo*))ZipLatestObservable_1__ctor_m2774219989_gshared)(__this, ___sources0, method)
// System.IDisposable UniRx.Operators.ZipLatestObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * ZipLatestObservable_1_SubscribeCore_m2170480445_gshared (ZipLatestObservable_1_t2717078458 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ZipLatestObservable_1_SubscribeCore_m2170480445(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ZipLatestObservable_1_t2717078458 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipLatestObservable_1_SubscribeCore_m2170480445_gshared)(__this, ___observer0, ___cancel1, method)
