﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IProcessExecution
struct IProcessExecution_t2362207186;
// System.Object
struct Il2CppObject;
// TestCoro
struct TestCoro_t3212325755;
// System.Action`1<System.Single>
struct Action_1_t1106661726;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestCoro/<Bearutine>c__Iterator2F
struct  U3CBearutineU3Ec__Iterator2F_t3513655992  : public Il2CppObject
{
public:
	// System.Boolean TestCoro/<Bearutine>c__Iterator2F::<longIsFirst>__0
	bool ___U3ClongIsFirstU3E__0_0;
	// IProcessExecution TestCoro/<Bearutine>c__Iterator2F::<coroChild>__1
	Il2CppObject * ___U3CcoroChildU3E__1_1;
	// System.Int32 TestCoro/<Bearutine>c__Iterator2F::$PC
	int32_t ___U24PC_2;
	// System.Object TestCoro/<Bearutine>c__Iterator2F::$current
	Il2CppObject * ___U24current_3;
	// TestCoro TestCoro/<Bearutine>c__Iterator2F::<>f__this
	TestCoro_t3212325755 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3ClongIsFirstU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBearutineU3Ec__Iterator2F_t3513655992, ___U3ClongIsFirstU3E__0_0)); }
	inline bool get_U3ClongIsFirstU3E__0_0() const { return ___U3ClongIsFirstU3E__0_0; }
	inline bool* get_address_of_U3ClongIsFirstU3E__0_0() { return &___U3ClongIsFirstU3E__0_0; }
	inline void set_U3ClongIsFirstU3E__0_0(bool value)
	{
		___U3ClongIsFirstU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcoroChildU3E__1_1() { return static_cast<int32_t>(offsetof(U3CBearutineU3Ec__Iterator2F_t3513655992, ___U3CcoroChildU3E__1_1)); }
	inline Il2CppObject * get_U3CcoroChildU3E__1_1() const { return ___U3CcoroChildU3E__1_1; }
	inline Il2CppObject ** get_address_of_U3CcoroChildU3E__1_1() { return &___U3CcoroChildU3E__1_1; }
	inline void set_U3CcoroChildU3E__1_1(Il2CppObject * value)
	{
		___U3CcoroChildU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoroChildU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CBearutineU3Ec__Iterator2F_t3513655992, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CBearutineU3Ec__Iterator2F_t3513655992, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CBearutineU3Ec__Iterator2F_t3513655992, ___U3CU3Ef__this_4)); }
	inline TestCoro_t3212325755 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline TestCoro_t3212325755 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(TestCoro_t3212325755 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

struct U3CBearutineU3Ec__Iterator2F_t3513655992_StaticFields
{
public:
	// System.Action`1<System.Single> TestCoro/<Bearutine>c__Iterator2F::<>f__am$cache5
	Action_1_t1106661726 * ___U3CU3Ef__amU24cache5_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(U3CBearutineU3Ec__Iterator2F_t3513655992_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Action_1_t1106661726 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Action_1_t1106661726 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Action_1_t1106661726 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
