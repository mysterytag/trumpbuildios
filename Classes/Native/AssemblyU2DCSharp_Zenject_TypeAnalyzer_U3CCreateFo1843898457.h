﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey184
struct  U3CCreateForMemberU3Ec__AnonStorey184_t1843898457  : public Il2CppObject
{
public:
	// System.Reflection.PropertyInfo Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey184::propInfo
	PropertyInfo_t * ___propInfo_0;

public:
	inline static int32_t get_offset_of_propInfo_0() { return static_cast<int32_t>(offsetof(U3CCreateForMemberU3Ec__AnonStorey184_t1843898457, ___propInfo_0)); }
	inline PropertyInfo_t * get_propInfo_0() const { return ___propInfo_0; }
	inline PropertyInfo_t ** get_address_of_propInfo_0() { return &___propInfo_0; }
	inline void set_propInfo_0(PropertyInfo_t * value)
	{
		___propInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___propInfo_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
