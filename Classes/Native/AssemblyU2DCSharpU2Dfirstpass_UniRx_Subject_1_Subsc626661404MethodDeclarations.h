﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<System.Int64>
struct Subscription_t626661404;
// UniRx.Subject`1<System.Int64>
struct Subject_1_t490482206;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<System.Int64>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m3087521125_gshared (Subscription_t626661404 * __this, Subject_1_t490482206 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m3087521125(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t626661404 *, Subject_1_t490482206 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m3087521125_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<System.Int64>::Dispose()
extern "C"  void Subscription_Dispose_m2597187953_gshared (Subscription_t626661404 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2597187953(__this, method) ((  void (*) (Subscription_t626661404 *, const MethodInfo*))Subscription_Dispose_m2597187953_gshared)(__this, method)
