﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>
struct ReadOnlyCollection_1_t759894031;
// System.Collections.Generic.IList`1<UnityEngine.Quaternion>
struct IList_1_t4058208293;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t3236402666;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Quaternion>
struct IEnumerator_1_t3374822427;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m4035469243_gshared (ReadOnlyCollection_1_t759894031 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m4035469243(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m4035469243_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2809150501_gshared (ReadOnlyCollection_1_t759894031 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2809150501(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, Quaternion_t1891715979 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2809150501_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3191010629_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3191010629(__this, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3191010629_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3688862924_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, Quaternion_t1891715979  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3688862924(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, int32_t, Quaternion_t1891715979 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3688862924_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1916866546_gshared (ReadOnlyCollection_1_t759894031 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1916866546(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t759894031 *, Quaternion_t1891715979 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1916866546_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1562715794_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1562715794(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1562715794_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Quaternion_t1891715979  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3395067064_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3395067064(__this, ___index0, method) ((  Quaternion_t1891715979  (*) (ReadOnlyCollection_1_t759894031 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3395067064_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3458847395_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, Quaternion_t1891715979  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3458847395(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, int32_t, Quaternion_t1891715979 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3458847395_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2215917917_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2215917917(__this, method) ((  bool (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2215917917_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1872758506_gshared (ReadOnlyCollection_1_t759894031 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1872758506(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1872758506_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m752534393_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m752534393(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m752534393_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3099334500_gshared (ReadOnlyCollection_1_t759894031 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3099334500(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t759894031 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3099334500_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4111678968_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4111678968(__this, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4111678968_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3130461276_gshared (ReadOnlyCollection_1_t759894031 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3130461276(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t759894031 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3130461276_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4086954940_gshared (ReadOnlyCollection_1_t759894031 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4086954940(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t759894031 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4086954940_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m200594223_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m200594223(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m200594223_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3124516249_gshared (ReadOnlyCollection_1_t759894031 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3124516249(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3124516249_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3956078911_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3956078911(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3956078911_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2941951360_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2941951360(__this, method) ((  bool (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2941951360_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m725281522_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m725281522(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m725281522_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2290495691_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2290495691(__this, method) ((  bool (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2290495691_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3714484430_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3714484430(__this, method) ((  bool (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3714484430_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2276452345_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2276452345(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t759894031 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2276452345_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m111472454_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m111472454(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m111472454_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2868180791_gshared (ReadOnlyCollection_1_t759894031 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2868180791(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t759894031 *, Quaternion_t1891715979 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2868180791_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3075305045_gshared (ReadOnlyCollection_1_t759894031 * __this, QuaternionU5BU5D_t3236402666* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3075305045(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t759894031 *, QuaternionU5BU5D_t3236402666*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3075305045_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1451185166_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1451185166(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1451185166_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2117844705_gshared (ReadOnlyCollection_1_t759894031 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2117844705(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t759894031 *, Quaternion_t1891715979 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2117844705_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1045983930_gshared (ReadOnlyCollection_1_t759894031 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1045983930(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t759894031 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1045983930_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Quaternion>::get_Item(System.Int32)
extern "C"  Quaternion_t1891715979  ReadOnlyCollection_1_get_Item_m1980971640_gshared (ReadOnlyCollection_1_t759894031 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1980971640(__this, ___index0, method) ((  Quaternion_t1891715979  (*) (ReadOnlyCollection_1_t759894031 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1980971640_gshared)(__this, ___index0, method)
