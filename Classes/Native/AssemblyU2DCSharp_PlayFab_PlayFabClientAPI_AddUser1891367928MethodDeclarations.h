﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AddUsernamePasswordResponseCallback
struct AddUsernamePasswordResponseCallback_t1891367928;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AddUsernamePasswordRequest
struct AddUsernamePasswordRequest_t1599161869;
// PlayFab.ClientModels.AddUsernamePasswordResult
struct AddUsernamePasswordResult_t1056564991;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUsername1599161869.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUsername1056564991.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AddUsernamePasswordResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddUsernamePasswordResponseCallback__ctor_m2484536455 (AddUsernamePasswordResponseCallback_t1891367928 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddUsernamePasswordResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AddUsernamePasswordRequest,PlayFab.ClientModels.AddUsernamePasswordResult,PlayFab.PlayFabError,System.Object)
extern "C"  void AddUsernamePasswordResponseCallback_Invoke_m677860886 (AddUsernamePasswordResponseCallback_t1891367928 * __this, String_t* ___urlPath0, int32_t ___callId1, AddUsernamePasswordRequest_t1599161869 * ___request2, AddUsernamePasswordResult_t1056564991 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AddUsernamePasswordResponseCallback_t1891367928(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AddUsernamePasswordRequest_t1599161869 * ___request2, AddUsernamePasswordResult_t1056564991 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AddUsernamePasswordResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AddUsernamePasswordRequest,PlayFab.ClientModels.AddUsernamePasswordResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddUsernamePasswordResponseCallback_BeginInvoke_m212389831 (AddUsernamePasswordResponseCallback_t1891367928 * __this, String_t* ___urlPath0, int32_t ___callId1, AddUsernamePasswordRequest_t1599161869 * ___request2, AddUsernamePasswordResult_t1056564991 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddUsernamePasswordResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddUsernamePasswordResponseCallback_EndInvoke_m1161286167 (AddUsernamePasswordResponseCallback_t1891367928 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
