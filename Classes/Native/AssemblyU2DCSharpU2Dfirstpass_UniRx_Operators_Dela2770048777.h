﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.MultipleAssignmentDisposable
struct MultipleAssignmentDisposable_t3090232719;
// UniRx.Operators.DelayFrameSubscriptionObservable`1<System.Object>
struct DelayFrameSubscriptionObservable_1_t2645984001;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93<System.Object>
struct  U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93::observer
	Il2CppObject* ___observer_0;
	// UniRx.MultipleAssignmentDisposable UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93::d
	MultipleAssignmentDisposable_t3090232719 * ___d_1;
	// UniRx.Operators.DelayFrameSubscriptionObservable`1<T> UniRx.Operators.DelayFrameSubscriptionObservable`1/<SubscribeCore>c__AnonStorey93::<>f__this
	DelayFrameSubscriptionObservable_1_t2645984001 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_d_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777, ___d_1)); }
	inline MultipleAssignmentDisposable_t3090232719 * get_d_1() const { return ___d_1; }
	inline MultipleAssignmentDisposable_t3090232719 ** get_address_of_d_1() { return &___d_1; }
	inline void set_d_1(MultipleAssignmentDisposable_t3090232719 * value)
	{
		___d_1 = value;
		Il2CppCodeGenWriteBarrier(&___d_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey93_t2770048777, ___U3CU3Ef__this_2)); }
	inline DelayFrameSubscriptionObservable_1_t2645984001 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline DelayFrameSubscriptionObservable_1_t2645984001 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(DelayFrameSubscriptionObservable_1_t2645984001 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
