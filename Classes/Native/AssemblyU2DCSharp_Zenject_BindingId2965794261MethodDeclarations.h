﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindingId
struct BindingId_t2965794261;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_BindingId2965794261.h"

// System.Void Zenject.BindingId::.ctor(System.Type,System.String)
extern "C"  void BindingId__ctor_m1294377931 (BindingId_t2965794261 * __this, Type_t * ___type0, String_t* ___identifier1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.BindingId::GetHashCode()
extern "C"  int32_t BindingId_GetHashCode_m839474855 (BindingId_t2965794261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingId::Equals(System.Object)
extern "C"  bool BindingId_Equals_m3409439375 (BindingId_t2965794261 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingId::Equals(Zenject.BindingId)
extern "C"  bool BindingId_Equals_m2968356268 (BindingId_t2965794261 * __this, BindingId_t2965794261 * ___that0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingId::op_Equality(Zenject.BindingId,Zenject.BindingId)
extern "C"  bool BindingId_op_Equality_m2779918344 (Il2CppObject * __this /* static, unused */, BindingId_t2965794261 * ___left0, BindingId_t2965794261 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingId::op_Inequality(Zenject.BindingId,Zenject.BindingId)
extern "C"  bool BindingId_op_Inequality_m2764903747 (Il2CppObject * __this /* static, unused */, BindingId_t2965794261 * ___left0, BindingId_t2965794261 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
