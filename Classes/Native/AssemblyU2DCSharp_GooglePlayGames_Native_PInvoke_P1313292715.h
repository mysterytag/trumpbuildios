﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.PInvokeUtilities
struct  PInvokeUtilities_t1313292715  : public Il2CppObject
{
public:

public:
};

struct PInvokeUtilities_t1313292715_StaticFields
{
public:
	// System.DateTime GooglePlayGames.Native.PInvoke.PInvokeUtilities::UnixEpoch
	DateTime_t339033936  ___UnixEpoch_0;

public:
	inline static int32_t get_offset_of_UnixEpoch_0() { return static_cast<int32_t>(offsetof(PInvokeUtilities_t1313292715_StaticFields, ___UnixEpoch_0)); }
	inline DateTime_t339033936  get_UnixEpoch_0() const { return ___UnixEpoch_0; }
	inline DateTime_t339033936 * get_address_of_UnixEpoch_0() { return &___UnixEpoch_0; }
	inline void set_UnixEpoch_0(DateTime_t339033936  value)
	{
		___UnixEpoch_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
