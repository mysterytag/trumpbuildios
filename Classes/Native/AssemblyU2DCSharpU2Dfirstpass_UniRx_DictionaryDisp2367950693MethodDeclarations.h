﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.DictionaryDisposable`2<System.Object,System.Object>
struct DictionaryDisposable_2_t2367950693;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t346249057;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t1852733134;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t1451594948;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::.ctor()
extern "C"  void DictionaryDisposable_2__ctor_m1064899917_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2__ctor_m1064899917(__this, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2__ctor_m1064899917_gshared)(__this, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void DictionaryDisposable_2__ctor_m3311603524_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define DictionaryDisposable_2__ctor_m3311603524(__this, ___comparer0, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, Il2CppObject*, const MethodInfo*))DictionaryDisposable_2__ctor_m3311603524_gshared)(__this, ___comparer0, method)
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1974734417_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1974734417(__this, method) ((  bool (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1974734417_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TKey> UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* DictionaryDisposable_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1808589137_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1808589137(__this, method) ((  Il2CppObject* (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1808589137_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* DictionaryDisposable_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3758170669_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3758170669(__this, method) ((  Il2CppObject* (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3758170669_gshared)(__this, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3024250580_gshared (DictionaryDisposable_2_t2367950693 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3024250580(__this, ___item0, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, KeyValuePair_2_t3312956448 , const MethodInfo*))DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3024250580_gshared)(__this, ___item0, method)
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3796158668_gshared (DictionaryDisposable_2_t2367950693 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3796158668(__this, ___item0, method) ((  bool (*) (DictionaryDisposable_2_t2367950693 *, KeyValuePair_2_t3312956448 , const MethodInfo*))DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3796158668_gshared)(__this, ___item0, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3907701176_gshared (DictionaryDisposable_2_t2367950693 * __this, KeyValuePair_2U5BU5D_t346249057* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3907701176(__this, ___array0, ___arrayIndex1, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, KeyValuePair_2U5BU5D_t346249057*, int32_t, const MethodInfo*))DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3907701176_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* DictionaryDisposable_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3417616323_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3417616323(__this, method) ((  Il2CppObject* (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3417616323_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryDisposable_2_System_Collections_IEnumerable_GetEnumerator_m785224076_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_IEnumerable_GetEnumerator_m785224076(__this, method) ((  Il2CppObject * (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_System_Collections_IEnumerable_GetEnumerator_m785224076_gshared)(__this, method)
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2177282801_gshared (DictionaryDisposable_2_t2367950693 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2177282801(__this, ___item0, method) ((  bool (*) (DictionaryDisposable_2_t2367950693 *, KeyValuePair_2_t3312956448 , const MethodInfo*))DictionaryDisposable_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2177282801_gshared)(__this, ___item0, method)
// TValue UniRx.DictionaryDisposable`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * DictionaryDisposable_2_get_Item_m3934025632_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryDisposable_2_get_Item_m3934025632(__this, ___key0, method) ((  Il2CppObject * (*) (DictionaryDisposable_2_t2367950693 *, Il2CppObject *, const MethodInfo*))DictionaryDisposable_2_get_Item_m3934025632_gshared)(__this, ___key0, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void DictionaryDisposable_2_set_Item_m509837389_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryDisposable_2_set_Item_m509837389(__this, ___key0, ___value1, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryDisposable_2_set_Item_m509837389_gshared)(__this, ___key0, ___value1, method)
// System.Int32 UniRx.DictionaryDisposable`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t DictionaryDisposable_2_get_Count_m771800135_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_get_Count_m771800135(__this, method) ((  int32_t (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_get_Count_m771800135_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> UniRx.DictionaryDisposable`2<System.Object,System.Object>::get_Keys()
extern "C"  KeyCollection_t1852733134 * DictionaryDisposable_2_get_Keys_m1722058918_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_get_Keys_m1722058918(__this, method) ((  KeyCollection_t1852733134 * (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_get_Keys_m1722058918_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> UniRx.DictionaryDisposable`2<System.Object,System.Object>::get_Values()
extern "C"  ValueCollection_t1451594948 * DictionaryDisposable_2_get_Values_m293030658_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_get_Values_m293030658(__this, method) ((  ValueCollection_t1451594948 * (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_get_Values_m293030658_gshared)(__this, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void DictionaryDisposable_2_Add_m84723208_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryDisposable_2_Add_m84723208(__this, ___key0, ___value1, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryDisposable_2_Add_m84723208_gshared)(__this, ___key0, ___value1, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::Clear()
extern "C"  void DictionaryDisposable_2_Clear_m2766000504_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_Clear_m2766000504(__this, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_Clear_m2766000504_gshared)(__this, method)
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool DictionaryDisposable_2_Remove_m2460413492_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryDisposable_2_Remove_m2460413492(__this, ___key0, method) ((  bool (*) (DictionaryDisposable_2_t2367950693 *, Il2CppObject *, const MethodInfo*))DictionaryDisposable_2_Remove_m2460413492_gshared)(__this, ___key0, method)
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool DictionaryDisposable_2_ContainsKey_m1420555580_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryDisposable_2_ContainsKey_m1420555580(__this, ___key0, method) ((  bool (*) (DictionaryDisposable_2_t2367950693 *, Il2CppObject *, const MethodInfo*))DictionaryDisposable_2_ContainsKey_m1420555580_gshared)(__this, ___key0, method)
// System.Boolean UniRx.DictionaryDisposable`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool DictionaryDisposable_2_TryGetValue_m3346878869_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define DictionaryDisposable_2_TryGetValue_m3346878869(__this, ___key0, ___value1, method) ((  bool (*) (DictionaryDisposable_2_t2367950693 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))DictionaryDisposable_2_TryGetValue_m3346878869_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> UniRx.DictionaryDisposable`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3591453092  DictionaryDisposable_2_GetEnumerator_m2934309272_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_GetEnumerator_m2934309272(__this, method) ((  Enumerator_t3591453092  (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_GetEnumerator_m2934309272_gshared)(__this, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void DictionaryDisposable_2_GetObjectData_m3504898667_gshared (DictionaryDisposable_2_t2367950693 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define DictionaryDisposable_2_GetObjectData_m3504898667(__this, ___info0, ___context1, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))DictionaryDisposable_2_GetObjectData_m3504898667_gshared)(__this, ___info0, ___context1, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::OnDeserialization(System.Object)
extern "C"  void DictionaryDisposable_2_OnDeserialization_m3906628825_gshared (DictionaryDisposable_2_t2367950693 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define DictionaryDisposable_2_OnDeserialization_m3906628825(__this, ___sender0, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, Il2CppObject *, const MethodInfo*))DictionaryDisposable_2_OnDeserialization_m3906628825_gshared)(__this, ___sender0, method)
// System.Void UniRx.DictionaryDisposable`2<System.Object,System.Object>::Dispose()
extern "C"  void DictionaryDisposable_2_Dispose_m1064886858_gshared (DictionaryDisposable_2_t2367950693 * __this, const MethodInfo* method);
#define DictionaryDisposable_2_Dispose_m1064886858(__this, method) ((  void (*) (DictionaryDisposable_2_t2367950693 *, const MethodInfo*))DictionaryDisposable_2_Dispose_m1064886858_gshared)(__this, method)
