﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetUserPublisherReadOnlyData>c__AnonStorey94
struct U3CGetUserPublisherReadOnlyDataU3Ec__AnonStorey94_t695723719;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetUserPublisherReadOnlyData>c__AnonStorey94::.ctor()
extern "C"  void U3CGetUserPublisherReadOnlyDataU3Ec__AnonStorey94__ctor_m121425100 (U3CGetUserPublisherReadOnlyDataU3Ec__AnonStorey94_t695723719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetUserPublisherReadOnlyData>c__AnonStorey94::<>m__81(PlayFab.CallRequestContainer)
extern "C"  void U3CGetUserPublisherReadOnlyDataU3Ec__AnonStorey94_U3CU3Em__81_m3215375331 (U3CGetUserPublisherReadOnlyDataU3Ec__AnonStorey94_t695723719 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
