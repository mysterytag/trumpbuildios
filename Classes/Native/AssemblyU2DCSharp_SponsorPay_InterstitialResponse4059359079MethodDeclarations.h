﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.InterstitialResponse
struct InterstitialResponse_t4059359079;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SponsorPay.InterstitialResponse::.ctor()
extern "C"  void InterstitialResponse__ctor_m637759422 (InterstitialResponse_t4059359079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.InterstitialResponse::get_error()
extern "C"  String_t* InterstitialResponse_get_error_m1862601022 (InterstitialResponse_t4059359079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialResponse::set_error(System.String)
extern "C"  void InterstitialResponse_set_error_m1415253435 (InterstitialResponse_t4059359079 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SponsorPay.InterstitialResponse::get_adsAvailable()
extern "C"  bool InterstitialResponse_get_adsAvailable_m3372930228 (InterstitialResponse_t4059359079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialResponse::set_adsAvailable(System.Boolean)
extern "C"  void InterstitialResponse_set_adsAvailable_m2853221843 (InterstitialResponse_t4059359079 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
