﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsCell
struct SettingsCell_t698172485;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsCell::.ctor()
extern "C"  void SettingsCell__ctor_m950184950 (SettingsCell_t698172485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsCell::Awake()
extern "C"  void SettingsCell_Awake_m1187790169 (SettingsCell_t698172485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsCell::<Awake>m__1FF()
extern "C"  void SettingsCell_U3CAwakeU3Em__1FF_m3204672365 (SettingsCell_t698172485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
