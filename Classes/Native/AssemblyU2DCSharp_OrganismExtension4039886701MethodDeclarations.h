﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOrganism
struct IOrganism_t2580843579;
// IRoot
struct IRoot_t69970123;

#include "codegen/il2cpp-codegen.h"

// System.Void OrganismExtension::Arise(IOrganism,IOrganism,IRoot)
extern "C"  void OrganismExtension_Arise_m4189093251 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___self0, Il2CppObject * ___carrier1, Il2CppObject * ___root2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
