﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IVkConnector
struct IVkConnector_t2786758927;
// GlobalStat
struct GlobalStat_t1134678199;
// AnalyticsController
struct AnalyticsController_t2265609122;
// Callbacks
struct Callbacks_t960104622;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// UniRx.ReactiveProperty`1<VKProfileInfo>
struct ReactiveProperty_1_t4081982704;
// System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean>
struct Func_2_t2285987078;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VkConnectorBase
struct  VkConnectorBase_t2999232489  : public Il2CppObject
{
public:
	// IVkConnector VkConnectorBase::vkConnector
	Il2CppObject * ___vkConnector_0;
	// GlobalStat VkConnectorBase::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_1;
	// AnalyticsController VkConnectorBase::AnalyticsController
	AnalyticsController_t2265609122 * ___AnalyticsController_2;
	// Callbacks VkConnectorBase::Callbacks
	Callbacks_t960104622 * ___Callbacks_3;
	// UniRx.ReactiveProperty`1<System.Boolean> VkConnectorBase::<Connected>k__BackingField
	ReactiveProperty_1_t819338379 * ___U3CConnectedU3Ek__BackingField_4;
	// UniRx.ReactiveProperty`1<VKProfileInfo> VkConnectorBase::<VkProfile>k__BackingField
	ReactiveProperty_1_t4081982704 * ___U3CVkProfileU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_vkConnector_0() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489, ___vkConnector_0)); }
	inline Il2CppObject * get_vkConnector_0() const { return ___vkConnector_0; }
	inline Il2CppObject ** get_address_of_vkConnector_0() { return &___vkConnector_0; }
	inline void set_vkConnector_0(Il2CppObject * value)
	{
		___vkConnector_0 = value;
		Il2CppCodeGenWriteBarrier(&___vkConnector_0, value);
	}

	inline static int32_t get_offset_of_GlobalStat_1() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489, ___GlobalStat_1)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_1() const { return ___GlobalStat_1; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_1() { return &___GlobalStat_1; }
	inline void set_GlobalStat_1(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_1 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_1, value);
	}

	inline static int32_t get_offset_of_AnalyticsController_2() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489, ___AnalyticsController_2)); }
	inline AnalyticsController_t2265609122 * get_AnalyticsController_2() const { return ___AnalyticsController_2; }
	inline AnalyticsController_t2265609122 ** get_address_of_AnalyticsController_2() { return &___AnalyticsController_2; }
	inline void set_AnalyticsController_2(AnalyticsController_t2265609122 * value)
	{
		___AnalyticsController_2 = value;
		Il2CppCodeGenWriteBarrier(&___AnalyticsController_2, value);
	}

	inline static int32_t get_offset_of_Callbacks_3() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489, ___Callbacks_3)); }
	inline Callbacks_t960104622 * get_Callbacks_3() const { return ___Callbacks_3; }
	inline Callbacks_t960104622 ** get_address_of_Callbacks_3() { return &___Callbacks_3; }
	inline void set_Callbacks_3(Callbacks_t960104622 * value)
	{
		___Callbacks_3 = value;
		Il2CppCodeGenWriteBarrier(&___Callbacks_3, value);
	}

	inline static int32_t get_offset_of_U3CConnectedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489, ___U3CConnectedU3Ek__BackingField_4)); }
	inline ReactiveProperty_1_t819338379 * get_U3CConnectedU3Ek__BackingField_4() const { return ___U3CConnectedU3Ek__BackingField_4; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_U3CConnectedU3Ek__BackingField_4() { return &___U3CConnectedU3Ek__BackingField_4; }
	inline void set_U3CConnectedU3Ek__BackingField_4(ReactiveProperty_1_t819338379 * value)
	{
		___U3CConnectedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConnectedU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CVkProfileU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489, ___U3CVkProfileU3Ek__BackingField_5)); }
	inline ReactiveProperty_1_t4081982704 * get_U3CVkProfileU3Ek__BackingField_5() const { return ___U3CVkProfileU3Ek__BackingField_5; }
	inline ReactiveProperty_1_t4081982704 ** get_address_of_U3CVkProfileU3Ek__BackingField_5() { return &___U3CVkProfileU3Ek__BackingField_5; }
	inline void set_U3CVkProfileU3Ek__BackingField_5(ReactiveProperty_1_t4081982704 * value)
	{
		___U3CVkProfileU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVkProfileU3Ek__BackingField_5, value);
	}
};

struct VkConnectorBase_t2999232489_StaticFields
{
public:
	// System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean> VkConnectorBase::<>f__am$cache6
	Func_2_t2285987078 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean> VkConnectorBase::<>f__am$cache7
	Func_2_t2285987078 * ___U3CU3Ef__amU24cache7_7;
	// System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean> VkConnectorBase::<>f__am$cache8
	Func_2_t2285987078 * ___U3CU3Ef__amU24cache8_8;
	// System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean> VkConnectorBase::<>f__am$cache9
	Func_2_t2285987078 * ___U3CU3Ef__amU24cache9_9;
	// System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean> VkConnectorBase::<>f__am$cacheA
	Func_2_t2285987078 * ___U3CU3Ef__amU24cacheA_10;
	// System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean> VkConnectorBase::<>f__am$cacheB
	Func_2_t2285987078 * ___U3CU3Ef__amU24cacheB_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> VkConnectorBase::<>f__switch$map4
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map4_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t2285987078 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t2285987078 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t2285987078 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t2285987078 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t2285987078 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t2285987078 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline Func_2_t2285987078 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline Func_2_t2285987078 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(Func_2_t2285987078 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline Func_2_t2285987078 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline Func_2_t2285987078 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(Func_2_t2285987078 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline Func_2_t2285987078 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline Func_2_t2285987078 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(Func_2_t2285987078 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline Func_2_t2285987078 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline Func_2_t2285987078 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(Func_2_t2285987078 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_12() { return static_cast<int32_t>(offsetof(VkConnectorBase_t2999232489_StaticFields, ___U3CU3Ef__switchU24map4_12)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map4_12() const { return ___U3CU3Ef__switchU24map4_12; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map4_12() { return &___U3CU3Ef__switchU24map4_12; }
	inline void set_U3CU3Ef__switchU24map4_12(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map4_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
