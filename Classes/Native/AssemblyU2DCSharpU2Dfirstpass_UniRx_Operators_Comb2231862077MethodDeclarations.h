﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>
struct CombineLatestFunc_5_t2231862077;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_5__ctor_m1266321991_gshared (CombineLatestFunc_5_t2231862077 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define CombineLatestFunc_5__ctor_m1266321991(__this, ___object0, ___method1, method) ((  void (*) (CombineLatestFunc_5_t2231862077 *, Il2CppObject *, IntPtr_t, const MethodInfo*))CombineLatestFunc_5__ctor_m1266321991_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * CombineLatestFunc_5_Invoke_m1701543740_gshared (CombineLatestFunc_5_t2231862077 * __this, bool ___arg10, bool ___arg21, bool ___arg32, bool ___arg43, const MethodInfo* method);
#define CombineLatestFunc_5_Invoke_m1701543740(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  Il2CppObject * (*) (CombineLatestFunc_5_t2231862077 *, bool, bool, bool, bool, const MethodInfo*))CombineLatestFunc_5_Invoke_m1701543740_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_5_BeginInvoke_m3236708292_gshared (CombineLatestFunc_5_t2231862077 * __this, bool ___arg10, bool ___arg21, bool ___arg32, bool ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method);
#define CombineLatestFunc_5_BeginInvoke_m3236708292(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (CombineLatestFunc_5_t2231862077 *, bool, bool, bool, bool, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_5_BeginInvoke_m3236708292_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TR UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_5_EndInvoke_m3471871550_gshared (CombineLatestFunc_5_t2231862077 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define CombineLatestFunc_5_EndInvoke_m3471871550(__this, ___result0, method) ((  Il2CppObject * (*) (CombineLatestFunc_5_t2231862077 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_5_EndInvoke_m3471871550_gshared)(__this, ___result0, method)
