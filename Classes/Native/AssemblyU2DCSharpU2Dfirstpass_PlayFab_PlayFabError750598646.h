﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t3403145775;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError2114648451.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.PlayFabError
struct  PlayFabError_t750598646  : public Il2CppObject
{
public:
	// System.Int32 PlayFab.PlayFabError::HttpCode
	int32_t ___HttpCode_0;
	// System.String PlayFab.PlayFabError::HttpStatus
	String_t* ___HttpStatus_1;
	// PlayFab.PlayFabErrorCode PlayFab.PlayFabError::Error
	int32_t ___Error_2;
	// System.String PlayFab.PlayFabError::ErrorMessage
	String_t* ___ErrorMessage_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> PlayFab.PlayFabError::ErrorDetails
	Dictionary_2_t3403145775 * ___ErrorDetails_4;

public:
	inline static int32_t get_offset_of_HttpCode_0() { return static_cast<int32_t>(offsetof(PlayFabError_t750598646, ___HttpCode_0)); }
	inline int32_t get_HttpCode_0() const { return ___HttpCode_0; }
	inline int32_t* get_address_of_HttpCode_0() { return &___HttpCode_0; }
	inline void set_HttpCode_0(int32_t value)
	{
		___HttpCode_0 = value;
	}

	inline static int32_t get_offset_of_HttpStatus_1() { return static_cast<int32_t>(offsetof(PlayFabError_t750598646, ___HttpStatus_1)); }
	inline String_t* get_HttpStatus_1() const { return ___HttpStatus_1; }
	inline String_t** get_address_of_HttpStatus_1() { return &___HttpStatus_1; }
	inline void set_HttpStatus_1(String_t* value)
	{
		___HttpStatus_1 = value;
		Il2CppCodeGenWriteBarrier(&___HttpStatus_1, value);
	}

	inline static int32_t get_offset_of_Error_2() { return static_cast<int32_t>(offsetof(PlayFabError_t750598646, ___Error_2)); }
	inline int32_t get_Error_2() const { return ___Error_2; }
	inline int32_t* get_address_of_Error_2() { return &___Error_2; }
	inline void set_Error_2(int32_t value)
	{
		___Error_2 = value;
	}

	inline static int32_t get_offset_of_ErrorMessage_3() { return static_cast<int32_t>(offsetof(PlayFabError_t750598646, ___ErrorMessage_3)); }
	inline String_t* get_ErrorMessage_3() const { return ___ErrorMessage_3; }
	inline String_t** get_address_of_ErrorMessage_3() { return &___ErrorMessage_3; }
	inline void set_ErrorMessage_3(String_t* value)
	{
		___ErrorMessage_3 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorMessage_3, value);
	}

	inline static int32_t get_offset_of_ErrorDetails_4() { return static_cast<int32_t>(offsetof(PlayFabError_t750598646, ___ErrorDetails_4)); }
	inline Dictionary_2_t3403145775 * get_ErrorDetails_4() const { return ___ErrorDetails_4; }
	inline Dictionary_2_t3403145775 ** get_address_of_ErrorDetails_4() { return &___ErrorDetails_4; }
	inline void set_ErrorDetails_4(Dictionary_2_t3403145775 * value)
	{
		___ErrorDetails_4 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorDetails_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
