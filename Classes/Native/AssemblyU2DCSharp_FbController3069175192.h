﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// GlobalStat
struct GlobalStat_t1134678199;
// AnalyticsController
struct AnalyticsController_t2265609122;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FbController
struct  FbController_t3069175192  : public Il2CppObject
{
public:
	// UniRx.ReactiveProperty`1<System.Boolean> FbController::Connected
	ReactiveProperty_1_t819338379 * ___Connected_0;
	// UniRx.ReactiveProperty`1<System.Boolean> FbController::ConnecGroup
	ReactiveProperty_1_t819338379 * ___ConnecGroup_1;
	// UniRx.ReactiveProperty`1<System.Boolean> FbController::ConnectWallPost
	ReactiveProperty_1_t819338379 * ___ConnectWallPost_2;
	// System.DateTime FbController::TimeToLogOut
	DateTime_t339033936  ___TimeToLogOut_3;
	// GlobalStat FbController::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_4;
	// AnalyticsController FbController::AnalyticsController
	AnalyticsController_t2265609122 * ___AnalyticsController_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> FbController::DictatDictionary
	Dictionary_2_t2606186806 * ___DictatDictionary_6;

public:
	inline static int32_t get_offset_of_Connected_0() { return static_cast<int32_t>(offsetof(FbController_t3069175192, ___Connected_0)); }
	inline ReactiveProperty_1_t819338379 * get_Connected_0() const { return ___Connected_0; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_Connected_0() { return &___Connected_0; }
	inline void set_Connected_0(ReactiveProperty_1_t819338379 * value)
	{
		___Connected_0 = value;
		Il2CppCodeGenWriteBarrier(&___Connected_0, value);
	}

	inline static int32_t get_offset_of_ConnecGroup_1() { return static_cast<int32_t>(offsetof(FbController_t3069175192, ___ConnecGroup_1)); }
	inline ReactiveProperty_1_t819338379 * get_ConnecGroup_1() const { return ___ConnecGroup_1; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_ConnecGroup_1() { return &___ConnecGroup_1; }
	inline void set_ConnecGroup_1(ReactiveProperty_1_t819338379 * value)
	{
		___ConnecGroup_1 = value;
		Il2CppCodeGenWriteBarrier(&___ConnecGroup_1, value);
	}

	inline static int32_t get_offset_of_ConnectWallPost_2() { return static_cast<int32_t>(offsetof(FbController_t3069175192, ___ConnectWallPost_2)); }
	inline ReactiveProperty_1_t819338379 * get_ConnectWallPost_2() const { return ___ConnectWallPost_2; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_ConnectWallPost_2() { return &___ConnectWallPost_2; }
	inline void set_ConnectWallPost_2(ReactiveProperty_1_t819338379 * value)
	{
		___ConnectWallPost_2 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectWallPost_2, value);
	}

	inline static int32_t get_offset_of_TimeToLogOut_3() { return static_cast<int32_t>(offsetof(FbController_t3069175192, ___TimeToLogOut_3)); }
	inline DateTime_t339033936  get_TimeToLogOut_3() const { return ___TimeToLogOut_3; }
	inline DateTime_t339033936 * get_address_of_TimeToLogOut_3() { return &___TimeToLogOut_3; }
	inline void set_TimeToLogOut_3(DateTime_t339033936  value)
	{
		___TimeToLogOut_3 = value;
	}

	inline static int32_t get_offset_of_GlobalStat_4() { return static_cast<int32_t>(offsetof(FbController_t3069175192, ___GlobalStat_4)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_4() const { return ___GlobalStat_4; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_4() { return &___GlobalStat_4; }
	inline void set_GlobalStat_4(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_4 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_4, value);
	}

	inline static int32_t get_offset_of_AnalyticsController_5() { return static_cast<int32_t>(offsetof(FbController_t3069175192, ___AnalyticsController_5)); }
	inline AnalyticsController_t2265609122 * get_AnalyticsController_5() const { return ___AnalyticsController_5; }
	inline AnalyticsController_t2265609122 ** get_address_of_AnalyticsController_5() { return &___AnalyticsController_5; }
	inline void set_AnalyticsController_5(AnalyticsController_t2265609122 * value)
	{
		___AnalyticsController_5 = value;
		Il2CppCodeGenWriteBarrier(&___AnalyticsController_5, value);
	}

	inline static int32_t get_offset_of_DictatDictionary_6() { return static_cast<int32_t>(offsetof(FbController_t3069175192, ___DictatDictionary_6)); }
	inline Dictionary_2_t2606186806 * get_DictatDictionary_6() const { return ___DictatDictionary_6; }
	inline Dictionary_2_t2606186806 ** get_address_of_DictatDictionary_6() { return &___DictatDictionary_6; }
	inline void set_DictatDictionary_6(Dictionary_2_t2606186806 * value)
	{
		___DictatDictionary_6 = value;
		Il2CppCodeGenWriteBarrier(&___DictatDictionary_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
