﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>
struct SingletonMonoBehaviour_1_t312324993;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::.ctor()
extern "C"  void SingletonMonoBehaviour_1__ctor_m4044296597_gshared (SingletonMonoBehaviour_1_t312324993 * __this, const MethodInfo* method);
#define SingletonMonoBehaviour_1__ctor_m4044296597(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t312324993 *, const MethodInfo*))SingletonMonoBehaviour_1__ctor_m4044296597_gshared)(__this, method)
// T PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::get_instance()
extern "C"  Il2CppObject * SingletonMonoBehaviour_1_get_instance_m1518032716_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SingletonMonoBehaviour_1_get_instance_m1518032716(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_instance_m1518032716_gshared)(__this /* static, unused */, method)
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::Awake()
extern "C"  void SingletonMonoBehaviour_1_Awake_m4281901816_gshared (SingletonMonoBehaviour_1_t312324993 * __this, const MethodInfo* method);
#define SingletonMonoBehaviour_1_Awake_m4281901816(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t312324993 *, const MethodInfo*))SingletonMonoBehaviour_1_Awake_m4281901816_gshared)(__this, method)
// System.Boolean PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::get_initialized()
extern "C"  bool SingletonMonoBehaviour_1_get_initialized_m1888521674_gshared (SingletonMonoBehaviour_1_t312324993 * __this, const MethodInfo* method);
#define SingletonMonoBehaviour_1_get_initialized_m1888521674(__this, method) ((  bool (*) (SingletonMonoBehaviour_1_t312324993 *, const MethodInfo*))SingletonMonoBehaviour_1_get_initialized_m1888521674_gshared)(__this, method)
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::set_initialized(System.Boolean)
extern "C"  void SingletonMonoBehaviour_1_set_initialized_m1544018177_gshared (SingletonMonoBehaviour_1_t312324993 * __this, bool ___value0, const MethodInfo* method);
#define SingletonMonoBehaviour_1_set_initialized_m1544018177(__this, ___value0, method) ((  void (*) (SingletonMonoBehaviour_1_t312324993 *, bool, const MethodInfo*))SingletonMonoBehaviour_1_set_initialized_m1544018177_gshared)(__this, ___value0, method)
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<System.Object>::Initialize()
extern "C"  void SingletonMonoBehaviour_1_Initialize_m2506226207_gshared (SingletonMonoBehaviour_1_t312324993 * __this, const MethodInfo* method);
#define SingletonMonoBehaviour_1_Initialize_m2506226207(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t312324993 *, const MethodInfo*))SingletonMonoBehaviour_1_Initialize_m2506226207_gshared)(__this, method)
