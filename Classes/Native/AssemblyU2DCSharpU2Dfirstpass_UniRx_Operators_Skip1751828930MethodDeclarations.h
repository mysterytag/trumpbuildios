﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>
struct Skip__t1751828930;
// UniRx.Operators.SkipObservable`1<System.Boolean>
struct SkipObservable_1_t197279106;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::.ctor(UniRx.Operators.SkipObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Skip___ctor_m2548371359_gshared (Skip__t1751828930 * __this, SkipObservable_1_t197279106 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Skip___ctor_m2548371359(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Skip__t1751828930 *, SkipObservable_1_t197279106 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Skip___ctor_m2548371359_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::Run()
extern "C"  Il2CppObject * Skip__Run_m948916905_gshared (Skip__t1751828930 * __this, const MethodInfo* method);
#define Skip__Run_m948916905(__this, method) ((  Il2CppObject * (*) (Skip__t1751828930 *, const MethodInfo*))Skip__Run_m948916905_gshared)(__this, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::Tick()
extern "C"  void Skip__Tick_m129718054_gshared (Skip__t1751828930 * __this, const MethodInfo* method);
#define Skip__Tick_m129718054(__this, method) ((  void (*) (Skip__t1751828930 *, const MethodInfo*))Skip__Tick_m129718054_gshared)(__this, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::OnNext(T)
extern "C"  void Skip__OnNext_m2273076483_gshared (Skip__t1751828930 * __this, bool ___value0, const MethodInfo* method);
#define Skip__OnNext_m2273076483(__this, ___value0, method) ((  void (*) (Skip__t1751828930 *, bool, const MethodInfo*))Skip__OnNext_m2273076483_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::OnError(System.Exception)
extern "C"  void Skip__OnError_m4048820754_gshared (Skip__t1751828930 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Skip__OnError_m4048820754(__this, ___error0, method) ((  void (*) (Skip__t1751828930 *, Exception_t1967233988 *, const MethodInfo*))Skip__OnError_m4048820754_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>::OnCompleted()
extern "C"  void Skip__OnCompleted_m1308350565_gshared (Skip__t1751828930 * __this, const MethodInfo* method);
#define Skip__OnCompleted_m1308350565(__this, method) ((  void (*) (Skip__t1751828930 *, const MethodInfo*))Skip__OnCompleted_m1308350565_gshared)(__this, method)
