﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady
struct GCMRegisterReady_t3751777615;
// PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete
struct GCMRegisterComplete_t1540081165;
// PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived
struct GCMMessageReceived_t1036884951;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.PlayFabGoogleCloudMessaging
struct  PlayFabGoogleCloudMessaging_t2415988188  : public Il2CppObject
{
public:

public:
};

struct PlayFabGoogleCloudMessaging_t2415988188_StaticFields
{
public:
	// PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady PlayFab.PlayFabGoogleCloudMessaging::_RegistrationReadyCallback
	GCMRegisterReady_t3751777615 * ____RegistrationReadyCallback_0;
	// PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete PlayFab.PlayFabGoogleCloudMessaging::_RegistrationCallback
	GCMRegisterComplete_t1540081165 * ____RegistrationCallback_1;
	// PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived PlayFab.PlayFabGoogleCloudMessaging::_MessageCallback
	GCMMessageReceived_t1036884951 * ____MessageCallback_2;

public:
	inline static int32_t get_offset_of__RegistrationReadyCallback_0() { return static_cast<int32_t>(offsetof(PlayFabGoogleCloudMessaging_t2415988188_StaticFields, ____RegistrationReadyCallback_0)); }
	inline GCMRegisterReady_t3751777615 * get__RegistrationReadyCallback_0() const { return ____RegistrationReadyCallback_0; }
	inline GCMRegisterReady_t3751777615 ** get_address_of__RegistrationReadyCallback_0() { return &____RegistrationReadyCallback_0; }
	inline void set__RegistrationReadyCallback_0(GCMRegisterReady_t3751777615 * value)
	{
		____RegistrationReadyCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&____RegistrationReadyCallback_0, value);
	}

	inline static int32_t get_offset_of__RegistrationCallback_1() { return static_cast<int32_t>(offsetof(PlayFabGoogleCloudMessaging_t2415988188_StaticFields, ____RegistrationCallback_1)); }
	inline GCMRegisterComplete_t1540081165 * get__RegistrationCallback_1() const { return ____RegistrationCallback_1; }
	inline GCMRegisterComplete_t1540081165 ** get_address_of__RegistrationCallback_1() { return &____RegistrationCallback_1; }
	inline void set__RegistrationCallback_1(GCMRegisterComplete_t1540081165 * value)
	{
		____RegistrationCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&____RegistrationCallback_1, value);
	}

	inline static int32_t get_offset_of__MessageCallback_2() { return static_cast<int32_t>(offsetof(PlayFabGoogleCloudMessaging_t2415988188_StaticFields, ____MessageCallback_2)); }
	inline GCMMessageReceived_t1036884951 * get__MessageCallback_2() const { return ____MessageCallback_2; }
	inline GCMMessageReceived_t1036884951 ** get_address_of__MessageCallback_2() { return &____MessageCallback_2; }
	inline void set__MessageCallback_2(GCMMessageReceived_t1036884951 * value)
	{
		____MessageCallback_2 = value;
		Il2CppCodeGenWriteBarrier(&____MessageCallback_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
