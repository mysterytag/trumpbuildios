﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA5`1<System.Object>
struct U3CSubscribeToTextU3Ec__AnonStoreyA5_1_t3655335453;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA5`1<System.Object>::.ctor()
extern "C"  void U3CSubscribeToTextU3Ec__AnonStoreyA5_1__ctor_m2067036646_gshared (U3CSubscribeToTextU3Ec__AnonStoreyA5_1_t3655335453 * __this, const MethodInfo* method);
#define U3CSubscribeToTextU3Ec__AnonStoreyA5_1__ctor_m2067036646(__this, method) ((  void (*) (U3CSubscribeToTextU3Ec__AnonStoreyA5_1_t3655335453 *, const MethodInfo*))U3CSubscribeToTextU3Ec__AnonStoreyA5_1__ctor_m2067036646_gshared)(__this, method)
// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA5`1<System.Object>::<>m__E3(T)
extern "C"  void U3CSubscribeToTextU3Ec__AnonStoreyA5_1_U3CU3Em__E3_m695954625_gshared (U3CSubscribeToTextU3Ec__AnonStoreyA5_1_t3655335453 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3CSubscribeToTextU3Ec__AnonStoreyA5_1_U3CU3Em__E3_m695954625(__this, ___x0, method) ((  void (*) (U3CSubscribeToTextU3Ec__AnonStoreyA5_1_t3655335453 *, Il2CppObject *, const MethodInfo*))U3CSubscribeToTextU3Ec__AnonStoreyA5_1_U3CU3Em__E3_m695954625_gshared)(__this, ___x0, method)
