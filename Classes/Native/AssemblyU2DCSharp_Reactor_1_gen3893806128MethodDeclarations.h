﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<System.DateTime>
struct Reactor_1_t3893806128;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<System.DateTime>::.ctor()
extern "C"  void Reactor_1__ctor_m2379146862_gshared (Reactor_1_t3893806128 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m2379146862(__this, method) ((  void (*) (Reactor_1_t3893806128 *, const MethodInfo*))Reactor_1__ctor_m2379146862_gshared)(__this, method)
// System.Void Reactor`1<System.DateTime>::React(T)
extern "C"  void Reactor_1_React_m275803507_gshared (Reactor_1_t3893806128 * __this, DateTime_t339033936  ___t0, const MethodInfo* method);
#define Reactor_1_React_m275803507(__this, ___t0, method) ((  void (*) (Reactor_1_t3893806128 *, DateTime_t339033936 , const MethodInfo*))Reactor_1_React_m275803507_gshared)(__this, ___t0, method)
// System.Void Reactor`1<System.DateTime>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m3125580862_gshared (Reactor_1_t3893806128 * __this, DateTime_t339033936  ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m3125580862(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t3893806128 *, DateTime_t339033936 , int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m3125580862_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<System.DateTime>::Empty()
extern "C"  bool Reactor_1_Empty_m1893967461_gshared (Reactor_1_t3893806128 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m1893967461(__this, method) ((  bool (*) (Reactor_1_t3893806128 *, const MethodInfo*))Reactor_1_Empty_m1893967461_gshared)(__this, method)
// System.IDisposable Reactor`1<System.DateTime>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m3297589983_gshared (Reactor_1_t3893806128 * __this, Action_1_t487486641 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m3297589983(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t3893806128 *, Action_1_t487486641 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m3297589983_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<System.DateTime>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m2722395628_gshared (Reactor_1_t3893806128 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m2722395628(__this, method) ((  void (*) (Reactor_1_t3893806128 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m2722395628_gshared)(__this, method)
// System.Void Reactor`1<System.DateTime>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3485185995_gshared (Reactor_1_t3893806128 * __this, Action_1_t487486641 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m3485185995(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t3893806128 *, Action_1_t487486641 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3485185995_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.DateTime>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m2558684735_gshared (Reactor_1_t3893806128 * __this, Action_1_t487486641 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m2558684735(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t3893806128 *, Action_1_t487486641 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m2558684735_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.DateTime>::Clear()
extern "C"  void Reactor_1_Clear_m4080247449_gshared (Reactor_1_t3893806128 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m4080247449(__this, method) ((  void (*) (Reactor_1_t3893806128 *, const MethodInfo*))Reactor_1_Clear_m4080247449_gshared)(__this, method)
