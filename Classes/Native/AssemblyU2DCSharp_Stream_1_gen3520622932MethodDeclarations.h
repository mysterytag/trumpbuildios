﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<System.Double>
struct Stream_1_t3520622932;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// System.Action
struct Action_t437523947;
// IStream`1<System.Double>
struct IStream_1_t1087207605;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<System.Double>::.ctor()
extern "C"  void Stream_1__ctor_m3438877202_gshared (Stream_1_t3520622932 * __this, const MethodInfo* method);
#define Stream_1__ctor_m3438877202(__this, method) ((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))Stream_1__ctor_m3438877202_gshared)(__this, method)
// System.Void Stream`1<System.Double>::Send(T)
extern "C"  void Stream_1_Send_m1968094756_gshared (Stream_1_t3520622932 * __this, double ___value0, const MethodInfo* method);
#define Stream_1_Send_m1968094756(__this, ___value0, method) ((  void (*) (Stream_1_t3520622932 *, double, const MethodInfo*))Stream_1_Send_m1968094756_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.Double>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m1535790266_gshared (Stream_1_t3520622932 * __this, double ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m1535790266(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t3520622932 *, double, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m1535790266_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.Double>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1840002556_gshared (Stream_1_t3520622932 * __this, Action_1_t682969319 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m1840002556(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t3520622932 *, Action_1_t682969319 *, int32_t, const MethodInfo*))Stream_1_Listen_m1840002556_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.Double>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1368522155_gshared (Stream_1_t3520622932 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m1368522155(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t3520622932 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m1368522155_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.Double>::Dispose()
extern "C"  void Stream_1_Dispose_m1829423567_gshared (Stream_1_t3520622932 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m1829423567(__this, method) ((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))Stream_1_Dispose_m1829423567_gshared)(__this, method)
// System.Void Stream`1<System.Double>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m3991115054_gshared (Stream_1_t3520622932 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m3991115054(__this, ___stream0, method) ((  void (*) (Stream_1_t3520622932 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3991115054_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.Double>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m806700943_gshared (Stream_1_t3520622932 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m806700943(__this, method) ((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))Stream_1_ClearInputStream_m806700943_gshared)(__this, method)
// System.Void Stream`1<System.Double>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m2348230883_gshared (Stream_1_t3520622932 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m2348230883(__this, method) ((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m2348230883_gshared)(__this, method)
// System.Void Stream`1<System.Double>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m2427340725_gshared (Stream_1_t3520622932 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m2427340725(__this, ___p0, method) ((  void (*) (Stream_1_t3520622932 *, int32_t, const MethodInfo*))Stream_1_Unpack_m2427340725_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.Double>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m3467573295_gshared (Stream_1_t3520622932 * __this, double ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m3467573295(__this, ___val0, method) ((  void (*) (Stream_1_t3520622932 *, double, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3467573295_gshared)(__this, ___val0, method)
