﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Boolean>
struct Select__t2547309513;
// UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>
struct SelectObservable_2_t4244715770;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Boolean>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select___ctor_m212127032_gshared (Select__t2547309513 * __this, SelectObservable_2_t4244715770 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select___ctor_m212127032(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select__t2547309513 *, SelectObservable_2_t4244715770 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select___ctor_m212127032_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Boolean>::OnNext(T)
extern "C"  void Select__OnNext_m1780449872_gshared (Select__t2547309513 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Select__OnNext_m1780449872(__this, ___value0, method) ((  void (*) (Select__t2547309513 *, Il2CppObject *, const MethodInfo*))Select__OnNext_m1780449872_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Boolean>::OnError(System.Exception)
extern "C"  void Select__OnError_m2939485535_gshared (Select__t2547309513 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select__OnError_m2939485535(__this, ___error0, method) ((  void (*) (Select__t2547309513 *, Exception_t1967233988 *, const MethodInfo*))Select__OnError_m2939485535_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Boolean>::OnCompleted()
extern "C"  void Select__OnCompleted_m3288696626_gshared (Select__t2547309513 * __this, const MethodInfo* method);
#define Select__OnCompleted_m3288696626(__this, method) ((  void (*) (Select__t2547309513 *, const MethodInfo*))Select__OnCompleted_m3288696626_gshared)(__this, method)
