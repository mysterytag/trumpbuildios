﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.ValidateAmazonReceiptRequest
struct  ValidateAmazonReceiptRequest_t2422909633  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.ValidateAmazonReceiptRequest::<ReceiptId>k__BackingField
	String_t* ___U3CReceiptIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.ValidateAmazonReceiptRequest::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.ValidateAmazonReceiptRequest::<CatalogVersion>k__BackingField
	String_t* ___U3CCatalogVersionU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.ValidateAmazonReceiptRequest::<CurrencyCode>k__BackingField
	String_t* ___U3CCurrencyCodeU3Ek__BackingField_3;
	// System.Int32 PlayFab.ClientModels.ValidateAmazonReceiptRequest::<PurchasePrice>k__BackingField
	int32_t ___U3CPurchasePriceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CReceiptIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ValidateAmazonReceiptRequest_t2422909633, ___U3CReceiptIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CReceiptIdU3Ek__BackingField_0() const { return ___U3CReceiptIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CReceiptIdU3Ek__BackingField_0() { return &___U3CReceiptIdU3Ek__BackingField_0; }
	inline void set_U3CReceiptIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CReceiptIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReceiptIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ValidateAmazonReceiptRequest_t2422909633, ___U3CUserIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_1() const { return ___U3CUserIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_1() { return &___U3CUserIdU3Ek__BackingField_1; }
	inline void set_U3CUserIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CCatalogVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ValidateAmazonReceiptRequest_t2422909633, ___U3CCatalogVersionU3Ek__BackingField_2)); }
	inline String_t* get_U3CCatalogVersionU3Ek__BackingField_2() const { return ___U3CCatalogVersionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCatalogVersionU3Ek__BackingField_2() { return &___U3CCatalogVersionU3Ek__BackingField_2; }
	inline void set_U3CCatalogVersionU3Ek__BackingField_2(String_t* value)
	{
		___U3CCatalogVersionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCatalogVersionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCurrencyCodeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ValidateAmazonReceiptRequest_t2422909633, ___U3CCurrencyCodeU3Ek__BackingField_3)); }
	inline String_t* get_U3CCurrencyCodeU3Ek__BackingField_3() const { return ___U3CCurrencyCodeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CCurrencyCodeU3Ek__BackingField_3() { return &___U3CCurrencyCodeU3Ek__BackingField_3; }
	inline void set_U3CCurrencyCodeU3Ek__BackingField_3(String_t* value)
	{
		___U3CCurrencyCodeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrencyCodeU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPurchasePriceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ValidateAmazonReceiptRequest_t2422909633, ___U3CPurchasePriceU3Ek__BackingField_4)); }
	inline int32_t get_U3CPurchasePriceU3Ek__BackingField_4() const { return ___U3CPurchasePriceU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CPurchasePriceU3Ek__BackingField_4() { return &___U3CPurchasePriceU3Ek__BackingField_4; }
	inline void set_U3CPurchasePriceU3Ek__BackingField_4(int32_t value)
	{
		___U3CPurchasePriceU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
