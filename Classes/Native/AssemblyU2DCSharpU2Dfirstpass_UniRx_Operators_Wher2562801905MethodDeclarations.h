﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Object>
struct U3CCombinePredicateU3Ec__AnonStorey72_t2562801905;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Object>::.ctor()
extern "C"  void U3CCombinePredicateU3Ec__AnonStorey72__ctor_m3326967392_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t2562801905 * __this, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72__ctor_m3326967392(__this, method) ((  void (*) (U3CCombinePredicateU3Ec__AnonStorey72_t2562801905 *, const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72__ctor_m3326967392_gshared)(__this, method)
// System.Boolean UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Object>::<>m__8F(T)
extern "C"  bool U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m3819369507_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t2562801905 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m3819369507(__this, ___x0, method) ((  bool (*) (U3CCombinePredicateU3Ec__AnonStorey72_t2562801905 *, Il2CppObject *, const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m3819369507_gshared)(__this, ___x0, method)
