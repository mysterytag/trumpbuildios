﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<FetchBytes>c__Iterator22
struct U3CFetchBytesU3Ec__Iterator22_t157331456;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObservableWWW/<FetchBytes>c__Iterator22::.ctor()
extern "C"  void U3CFetchBytesU3Ec__Iterator22__ctor_m396292522 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.ObservableWWW/<FetchBytes>c__Iterator22::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchBytesU3Ec__Iterator22_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1194214312 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.ObservableWWW/<FetchBytes>c__Iterator22::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchBytesU3Ec__Iterator22_System_Collections_IEnumerator_get_Current_m3731658556 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.ObservableWWW/<FetchBytes>c__Iterator22::MoveNext()
extern "C"  bool U3CFetchBytesU3Ec__Iterator22_MoveNext_m869164322 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableWWW/<FetchBytes>c__Iterator22::Dispose()
extern "C"  void U3CFetchBytesU3Ec__Iterator22_Dispose_m2778274663 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableWWW/<FetchBytes>c__Iterator22::Reset()
extern "C"  void U3CFetchBytesU3Ec__Iterator22_Reset_m2337692759 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
