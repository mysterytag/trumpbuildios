﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<Initialize>c__AnonStorey155
struct U3CInitializeU3Ec__AnonStorey155_t1973587068;
// UpgradeButton
struct UpgradeButton_t1475467342;
// UpgradeCell
struct UpgradeCell_t4117746046;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UpgradeButton1475467342.h"
#include "AssemblyU2DCSharp_UpgradeCell4117746046.h"

// System.Void TableButtons/<Initialize>c__AnonStorey155::.ctor()
extern "C"  void U3CInitializeU3Ec__AnonStorey155__ctor_m3162018643 (U3CInitializeU3Ec__AnonStorey155_t1973587068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<Initialize>c__AnonStorey155::<>m__253(UpgradeButton,UpgradeCell,System.Int32)
extern "C"  void U3CInitializeU3Ec__AnonStorey155_U3CU3Em__253_m1606583515 (U3CInitializeU3Ec__AnonStorey155_t1973587068 * __this, UpgradeButton_t1475467342 * ___upgradeButton0, UpgradeCell_t4117746046 * ___upgradeCell1, int32_t ___arg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
