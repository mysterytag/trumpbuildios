﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Double>
struct U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Double>::.ctor()
extern "C"  void U3CObserveEveryValueChangedU3Ec__AnonStorey92_2__ctor_m2580074098_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983 * __this, const MethodInfo* method);
#define U3CObserveEveryValueChangedU3Ec__AnonStorey92_2__ctor_m2580074098(__this, method) ((  void (*) (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983 *, const MethodInfo*))U3CObserveEveryValueChangedU3Ec__AnonStorey92_2__ctor_m2580074098_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Double>::<>m__BE(UniRx.IObserver`1<TProperty>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_U3CU3Em__BE_m2343962371_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method);
#define U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_U3CU3Em__BE_m2343962371(__this, ___observer0, ___cancellationToken1, method) ((  Il2CppObject * (*) (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_U3CU3Em__BE_m2343962371_gshared)(__this, ___observer0, ___cancellationToken1, method)
