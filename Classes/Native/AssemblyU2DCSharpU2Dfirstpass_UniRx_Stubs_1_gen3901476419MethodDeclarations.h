﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Stubs`1<UniRx.Unit>::.cctor()
extern "C"  void Stubs_1__cctor_m2773682490_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Stubs_1__cctor_m2773682490(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m2773682490_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<UniRx.Unit>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m2826503222_gshared (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___t0, const MethodInfo* method);
#define Stubs_1_U3CIgnoreU3Em__71_m2826503222(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, Unit_t2558286038 , const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m2826503222_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<UniRx.Unit>::<Identity>m__72(T)
extern "C"  Unit_t2558286038  Stubs_1_U3CIdentityU3Em__72_m3683983882_gshared (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___t0, const MethodInfo* method);
#define Stubs_1_U3CIdentityU3Em__72_m3683983882(__this /* static, unused */, ___t0, method) ((  Unit_t2558286038  (*) (Il2CppObject * /* static, unused */, Unit_t2558286038 , const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m3683983882_gshared)(__this /* static, unused */, ___t0, method)
