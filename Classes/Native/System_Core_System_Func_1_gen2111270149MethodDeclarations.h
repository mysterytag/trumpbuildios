﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_1_gen1979887667MethodDeclarations.h"

// System.Void System.Func`1<System.String>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m1422427581(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t2111270149 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m2351420511_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<System.String>::Invoke()
#define Func_1_Invoke_m3326986245(__this, method) ((  String_t* (*) (Func_1_t2111270149 *, const MethodInfo*))Func_1_Invoke_m2160158107_gshared)(__this, method)
// System.IAsyncResult System.Func`1<System.String>::BeginInvoke(System.AsyncCallback,System.Object)
#define Func_1_BeginInvoke_m1865406396(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t2111270149 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m4066620266_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<System.String>::EndInvoke(System.IAsyncResult)
#define Func_1_EndInvoke_m2194995711(__this, ___result0, method) ((  String_t* (*) (Func_1_t2111270149 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m3356710033_gshared)(__this, ___result0, method)
