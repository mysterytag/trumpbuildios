﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator3B_1_t3129977787;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::.ctor()
extern "C"  void U3CToEnumerableU3Ec__Iterator3B_1__ctor_m3223225506_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3B_1__ctor_m3223225506(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3B_1__ctor_m3223225506_gshared)(__this, method)
// T ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3658971083_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3658971083(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3658971083_gshared)(__this, method)
// System.Object ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_IEnumerator_get_Current_m1433718350_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_IEnumerator_get_Current_m1433718350(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_IEnumerator_get_Current_m1433718350_gshared)(__this, method)
// System.Collections.IEnumerator ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_IEnumerable_GetEnumerator_m2267628559_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_IEnumerable_GetEnumerator_m2267628559(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_IEnumerable_GetEnumerator_m2267628559_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m92580468_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m92580468(__this, method) ((  Il2CppObject* (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3B_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m92580468_gshared)(__this, method)
// System.Boolean ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::MoveNext()
extern "C"  bool U3CToEnumerableU3Ec__Iterator3B_1_MoveNext_m1834278650_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3B_1_MoveNext_m1834278650(__this, method) ((  bool (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3B_1_MoveNext_m1834278650_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::Dispose()
extern "C"  void U3CToEnumerableU3Ec__Iterator3B_1_Dispose_m746573919_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3B_1_Dispose_m746573919(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3B_1_Dispose_m746573919_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3B`1<System.Object>::Reset()
extern "C"  void U3CToEnumerableU3Ec__Iterator3B_1_Reset_m869658447_gshared (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3B_1_Reset_m869658447(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator3B_1_t3129977787 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3B_1_Reset_m869658447_gshared)(__this, method)
