﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<System.Int64>
struct Stream_1_t1538553904;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// System.Action
struct Action_t437523947;
// IStream`1<System.Int64>
struct IStream_1_t3400105873;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<System.Int64>::.ctor()
extern "C"  void Stream_1__ctor_m3600918454_gshared (Stream_1_t1538553904 * __this, const MethodInfo* method);
#define Stream_1__ctor_m3600918454(__this, method) ((  void (*) (Stream_1_t1538553904 *, const MethodInfo*))Stream_1__ctor_m3600918454_gshared)(__this, method)
// System.Void Stream`1<System.Int64>::Send(T)
extern "C"  void Stream_1_Send_m2130136008_gshared (Stream_1_t1538553904 * __this, int64_t ___value0, const MethodInfo* method);
#define Stream_1_Send_m2130136008(__this, ___value0, method) ((  void (*) (Stream_1_t1538553904 *, int64_t, const MethodInfo*))Stream_1_Send_m2130136008_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.Int64>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m2170246494_gshared (Stream_1_t1538553904 * __this, int64_t ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m2170246494(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t1538553904 *, int64_t, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m2170246494_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.Int64>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2034331546_gshared (Stream_1_t1538553904 * __this, Action_1_t2995867587 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m2034331546(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t1538553904 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))Stream_1_Listen_m2034331546_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.Int64>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m61520205_gshared (Stream_1_t1538553904 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m61520205(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t1538553904 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m61520205_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.Int64>::Dispose()
extern "C"  void Stream_1_Dispose_m2932244083_gshared (Stream_1_t1538553904 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m2932244083(__this, method) ((  void (*) (Stream_1_t1538553904 *, const MethodInfo*))Stream_1_Dispose_m2932244083_gshared)(__this, method)
// System.Void Stream`1<System.Int64>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m3534796298_gshared (Stream_1_t1538553904 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m3534796298(__this, ___stream0, method) ((  void (*) (Stream_1_t1538553904 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3534796298_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.Int64>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m3545271147_gshared (Stream_1_t1538553904 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m3545271147(__this, method) ((  void (*) (Stream_1_t1538553904 *, const MethodInfo*))Stream_1_ClearInputStream_m3545271147_gshared)(__this, method)
// System.Void Stream`1<System.Int64>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m1912570559_gshared (Stream_1_t1538553904 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m1912570559(__this, method) ((  void (*) (Stream_1_t1538553904 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m1912570559_gshared)(__this, method)
// System.Void Stream`1<System.Int64>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m1378354321_gshared (Stream_1_t1538553904 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m1378354321(__this, ___p0, method) ((  void (*) (Stream_1_t1538553904 *, int32_t, const MethodInfo*))Stream_1_Unpack_m1378354321_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.Int64>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m1465181395_gshared (Stream_1_t1538553904 * __this, int64_t ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m1465181395(__this, ___val0, method) ((  void (*) (Stream_1_t1538553904 *, int64_t, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m1465181395_gshared)(__this, ___val0, method)
