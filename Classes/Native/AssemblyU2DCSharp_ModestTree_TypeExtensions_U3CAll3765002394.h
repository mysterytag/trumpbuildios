﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type[]
struct TypeU5BU5D_t3431720054;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<AllAttributes>c__AnonStorey16D
struct  U3CAllAttributesU3Ec__AnonStorey16D_t3765002394  : public Il2CppObject
{
public:
	// System.Type[] ModestTree.TypeExtensions/<AllAttributes>c__AnonStorey16D::attributeTypes
	TypeU5BU5D_t3431720054* ___attributeTypes_0;

public:
	inline static int32_t get_offset_of_attributeTypes_0() { return static_cast<int32_t>(offsetof(U3CAllAttributesU3Ec__AnonStorey16D_t3765002394, ___attributeTypes_0)); }
	inline TypeU5BU5D_t3431720054* get_attributeTypes_0() const { return ___attributeTypes_0; }
	inline TypeU5BU5D_t3431720054** get_address_of_attributeTypes_0() { return &___attributeTypes_0; }
	inline void set_attributeTypes_0(TypeU5BU5D_t3431720054* value)
	{
		___attributeTypes_0 = value;
		Il2CppCodeGenWriteBarrier(&___attributeTypes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
