﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithPlayFabRequest
struct LoginWithPlayFabRequest_t402940283;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LoginWithPlayFabRequest::.ctor()
extern "C"  void LoginWithPlayFabRequest__ctor_m152984206 (LoginWithPlayFabRequest_t402940283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithPlayFabRequest::get_TitleId()
extern "C"  String_t* LoginWithPlayFabRequest_get_TitleId_m2685094547 (LoginWithPlayFabRequest_t402940283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithPlayFabRequest::set_TitleId(System.String)
extern "C"  void LoginWithPlayFabRequest_set_TitleId_m2210197920 (LoginWithPlayFabRequest_t402940283 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithPlayFabRequest::get_Username()
extern "C"  String_t* LoginWithPlayFabRequest_get_Username_m1480220536 (LoginWithPlayFabRequest_t402940283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithPlayFabRequest::set_Username(System.String)
extern "C"  void LoginWithPlayFabRequest_set_Username_m566187609 (LoginWithPlayFabRequest_t402940283 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithPlayFabRequest::get_Password()
extern "C"  String_t* LoginWithPlayFabRequest_get_Password_m425014269 (LoginWithPlayFabRequest_t402940283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithPlayFabRequest::set_Password(System.String)
extern "C"  void LoginWithPlayFabRequest_set_Password_m2115420660 (LoginWithPlayFabRequest_t402940283 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
