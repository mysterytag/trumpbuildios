﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetStoreItemsResult
struct GetStoreItemsResult_t831262658;
// System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>
struct List_1_t3669843069;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetStoreItemsResult::.ctor()
extern "C"  void GetStoreItemsResult__ctor_m1672464167 (GetStoreItemsResult_t831262658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem> PlayFab.ClientModels.GetStoreItemsResult::get_Store()
extern "C"  List_1_t3669843069 * GetStoreItemsResult_get_Store_m3520969940 (GetStoreItemsResult_t831262658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetStoreItemsResult::set_Store(System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>)
extern "C"  void GetStoreItemsResult_set_Store_m2336661695 (GetStoreItemsResult_t831262658 * __this, List_1_t3669843069 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
