﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>
struct ReadOnlyCollection_1_t3532407167;
// System.Collections.Generic.IList`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IList_1_t2535754133;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UniRx.Tuple`2<System.Object,System.Object>[]
struct Tuple_2U5BU5D_t3897934010;
// System.Collections.Generic.IEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IEnumerator_1_t1852368267;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2219923592_gshared (ReadOnlyCollection_1_t3532407167 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2219923592(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2219923592_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3376809586_gshared (ReadOnlyCollection_1_t3532407167 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3376809586(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, Tuple_2_t369261819 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3376809586_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3608573080_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3608573080(__this, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3608573080_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3041567449_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, Tuple_2_t369261819  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3041567449(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3041567449_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1356755333_gshared (ReadOnlyCollection_1_t3532407167 * __this, Tuple_2_t369261819  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1356755333(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3532407167 *, Tuple_2_t369261819 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1356755333_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m915420319_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m915420319(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m915420319_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Tuple_2_t369261819  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m878751813_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m878751813(__this, ___index0, method) ((  Tuple_2_t369261819  (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m878751813_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4178153840_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, Tuple_2_t369261819  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4178153840(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m4178153840_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1663335530_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1663335530(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1663335530_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2478413623_gshared (ReadOnlyCollection_1_t3532407167 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2478413623(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2478413623_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3085255686_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3085255686(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3085255686_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m619260663_gshared (ReadOnlyCollection_1_t3532407167 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m619260663(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3532407167 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m619260663_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2019115589_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2019115589(__this, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2019115589_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m538987433_gshared (ReadOnlyCollection_1_t3532407167 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m538987433(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3532407167 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m538987433_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2501709263_gshared (ReadOnlyCollection_1_t3532407167 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2501709263(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3532407167 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2501709263_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1609270978_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1609270978(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1609270978_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2141867814_gshared (ReadOnlyCollection_1_t3532407167 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2141867814(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2141867814_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3558748498_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3558748498(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3558748498_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2381840147_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2381840147(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2381840147_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m38985541_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m38985541(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m38985541_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2693803480_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2693803480(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2693803480_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1926379041_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1926379041(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1926379041_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4170978188_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4170978188(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4170978188_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m935135769_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m935135769(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m935135769_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1342810058_gshared (ReadOnlyCollection_1_t3532407167 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1342810058(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3532407167 *, Tuple_2_t369261819 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1342810058_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m154079138_gshared (ReadOnlyCollection_1_t3532407167 * __this, Tuple_2U5BU5D_t3897934010* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m154079138(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3532407167 *, Tuple_2U5BU5D_t3897934010*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m154079138_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2428498465_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2428498465(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2428498465_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m339206382_gshared (ReadOnlyCollection_1_t3532407167 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m339206382(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3532407167 *, Tuple_2_t369261819 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m339206382_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1742770765_gshared (ReadOnlyCollection_1_t3532407167 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1742770765(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3532407167 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1742770765_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Item(System.Int32)
extern "C"  Tuple_2_t369261819  ReadOnlyCollection_1_get_Item_m3825504389_gshared (ReadOnlyCollection_1_t3532407167 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3825504389(__this, ___index0, method) ((  Tuple_2_t369261819  (*) (ReadOnlyCollection_1_t3532407167 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3825504389_gshared)(__this, ___index0, method)
