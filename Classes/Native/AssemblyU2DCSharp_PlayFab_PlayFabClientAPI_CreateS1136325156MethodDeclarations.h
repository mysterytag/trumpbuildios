﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/CreateSharedGroupResponseCallback
struct CreateSharedGroupResponseCallback_t1136325156;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.CreateSharedGroupRequest
struct CreateSharedGroupRequest_t972582497;
// PlayFab.ClientModels.CreateSharedGroupResult
struct CreateSharedGroupResult_t2976015403;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CreateShared972582497.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CreateShare2976015403.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/CreateSharedGroupResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CreateSharedGroupResponseCallback__ctor_m1746913459 (CreateSharedGroupResponseCallback_t1136325156 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/CreateSharedGroupResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.CreateSharedGroupRequest,PlayFab.ClientModels.CreateSharedGroupResult,PlayFab.PlayFabError,System.Object)
extern "C"  void CreateSharedGroupResponseCallback_Invoke_m447278170 (CreateSharedGroupResponseCallback_t1136325156 * __this, String_t* ___urlPath0, int32_t ___callId1, CreateSharedGroupRequest_t972582497 * ___request2, CreateSharedGroupResult_t2976015403 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CreateSharedGroupResponseCallback_t1136325156(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, CreateSharedGroupRequest_t972582497 * ___request2, CreateSharedGroupResult_t2976015403 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/CreateSharedGroupResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.CreateSharedGroupRequest,PlayFab.ClientModels.CreateSharedGroupResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CreateSharedGroupResponseCallback_BeginInvoke_m1993950043 (CreateSharedGroupResponseCallback_t1136325156 * __this, String_t* ___urlPath0, int32_t ___callId1, CreateSharedGroupRequest_t972582497 * ___request2, CreateSharedGroupResult_t2976015403 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/CreateSharedGroupResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CreateSharedGroupResponseCallback_EndInvoke_m2252442691 (CreateSharedGroupResponseCallback_t1136325156 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
