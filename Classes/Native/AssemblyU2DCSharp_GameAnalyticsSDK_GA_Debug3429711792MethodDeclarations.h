﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"

// System.Void GameAnalyticsSDK.GA_Debug::.cctor()
extern "C"  void GA_Debug__cctor_m1283637144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Debug::HandleLog(System.String,System.String,UnityEngine.LogType)
extern "C"  void GA_Debug_HandleLog_m463624980 (Il2CppObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Debug::SubmitError(System.String,UnityEngine.LogType)
extern "C"  void GA_Debug_SubmitError_m939904324 (Il2CppObject * __this /* static, unused */, String_t* ___message0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Debug::EnabledLog()
extern "C"  void GA_Debug_EnabledLog_m167738610 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
