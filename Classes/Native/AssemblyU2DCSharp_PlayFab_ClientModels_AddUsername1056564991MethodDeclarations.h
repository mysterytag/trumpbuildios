﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AddUsernamePasswordResult
struct AddUsernamePasswordResult_t1056564991;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.AddUsernamePasswordResult::.ctor()
extern "C"  void AddUsernamePasswordResult__ctor_m341400586 (AddUsernamePasswordResult_t1056564991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddUsernamePasswordResult::get_Username()
extern "C"  String_t* AddUsernamePasswordResult_get_Username_m1403100604 (AddUsernamePasswordResult_t1056564991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddUsernamePasswordResult::set_Username(System.String)
extern "C"  void AddUsernamePasswordResult_set_Username_m4102472149 (AddUsernamePasswordResult_t1056564991 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
