﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Comparison_1_t3072936695;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m2785138369_gshared (Comparison_1_t3072936695 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m2785138369(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3072936695 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2785138369_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<UniRx.Tuple`2<System.Object,System.Object>>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m481548631_gshared (Comparison_1_t3072936695 * __this, Tuple_2_t369261819  ___x0, Tuple_2_t369261819  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m481548631(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3072936695 *, Tuple_2_t369261819 , Tuple_2_t369261819 , const MethodInfo*))Comparison_1_Invoke_m481548631_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<UniRx.Tuple`2<System.Object,System.Object>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m3707532704_gshared (Comparison_1_t3072936695 * __this, Tuple_2_t369261819  ___x0, Tuple_2_t369261819  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m3707532704(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3072936695 *, Tuple_2_t369261819 , Tuple_2_t369261819 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3707532704_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<UniRx.Tuple`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m2672464309_gshared (Comparison_1_t3072936695 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m2672464309(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3072936695 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m2672464309_gshared)(__this, ___result0, method)
