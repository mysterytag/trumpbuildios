﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>
struct GroupByObservable_3_t657897610;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.IGroupedObservable`2<System.Object,System.Object>>
struct IObserver_1_t3518384802;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Nullable`1<System.Int32>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void GroupByObservable_3__ctor_m2517188728_gshared (GroupByObservable_3_t657897610 * __this, Il2CppObject* ___source0, Func_2_t2135783352 * ___keySelector1, Func_2_t2135783352 * ___elementSelector2, Nullable_1_t1438485399  ___capacity3, Il2CppObject* ___comparer4, const MethodInfo* method);
#define GroupByObservable_3__ctor_m2517188728(__this, ___source0, ___keySelector1, ___elementSelector2, ___capacity3, ___comparer4, method) ((  void (*) (GroupByObservable_3_t657897610 *, Il2CppObject*, Func_2_t2135783352 *, Func_2_t2135783352 *, Nullable_1_t1438485399 , Il2CppObject*, const MethodInfo*))GroupByObservable_3__ctor_m2517188728_gshared)(__this, ___source0, ___keySelector1, ___elementSelector2, ___capacity3, ___comparer4, method)
// System.IDisposable UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.IGroupedObservable`2<TKey,TElement>>,System.IDisposable)
extern "C"  Il2CppObject * GroupByObservable_3_SubscribeCore_m2362185847_gshared (GroupByObservable_3_t657897610 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define GroupByObservable_3_SubscribeCore_m2362185847(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (GroupByObservable_3_t657897610 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))GroupByObservable_3_SubscribeCore_m2362185847_gshared)(__this, ___observer0, ___cancel1, method)
