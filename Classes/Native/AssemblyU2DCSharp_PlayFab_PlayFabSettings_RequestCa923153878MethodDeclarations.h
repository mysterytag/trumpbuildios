﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabSettings/RequestCallback`1<System.Object>
struct RequestCallback_1_t923153878;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabSettings/RequestCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestCallback_1__ctor_m804351314_gshared (RequestCallback_1_t923153878 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define RequestCallback_1__ctor_m804351314(__this, ___object0, ___method1, method) ((  void (*) (RequestCallback_1_t923153878 *, Il2CppObject *, IntPtr_t, const MethodInfo*))RequestCallback_1__ctor_m804351314_gshared)(__this, ___object0, ___method1, method)
// System.Void PlayFab.PlayFabSettings/RequestCallback`1<System.Object>::Invoke(System.String,System.Int32,TRequest,System.Object)
extern "C"  void RequestCallback_1_Invoke_m2965292920_gshared (RequestCallback_1_t923153878 * __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, const MethodInfo* method);
#define RequestCallback_1_Invoke_m2965292920(__this, ___urlPath0, ___callId1, ___request2, ___customData3, method) ((  void (*) (RequestCallback_1_t923153878 *, String_t*, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))RequestCallback_1_Invoke_m2965292920_gshared)(__this, ___urlPath0, ___callId1, ___request2, ___customData3, method)
// System.IAsyncResult PlayFab.PlayFabSettings/RequestCallback`1<System.Object>::BeginInvoke(System.String,System.Int32,TRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestCallback_1_BeginInvoke_m2391617785_gshared (RequestCallback_1_t923153878 * __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method);
#define RequestCallback_1_BeginInvoke_m2391617785(__this, ___urlPath0, ___callId1, ___request2, ___customData3, ___callback4, ___object5, method) ((  Il2CppObject * (*) (RequestCallback_1_t923153878 *, String_t*, int32_t, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))RequestCallback_1_BeginInvoke_m2391617785_gshared)(__this, ___urlPath0, ___callId1, ___request2, ___customData3, ___callback4, ___object5, method)
// System.Void PlayFab.PlayFabSettings/RequestCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void RequestCallback_1_EndInvoke_m312760418_gshared (RequestCallback_1_t923153878 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define RequestCallback_1_EndInvoke_m312760418(__this, ___result0, method) ((  void (*) (RequestCallback_1_t923153878 *, Il2CppObject *, const MethodInfo*))RequestCallback_1_EndInvoke_m312760418_gshared)(__this, ___result0, method)
