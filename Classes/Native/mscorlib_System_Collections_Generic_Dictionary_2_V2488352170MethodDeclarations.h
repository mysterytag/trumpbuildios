﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>
struct ValueCollection_t2488352170;
// System.Collections.Generic.Dictionary`2<System.Double,System.Object>
struct Dictionary_2_t566215076;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va333243017.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2792547820_gshared (ValueCollection_t2488352170 * __this, Dictionary_2_t566215076 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2792547820(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2488352170 *, Dictionary_2_t566215076 *, const MethodInfo*))ValueCollection__ctor_m2792547820_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m652273862_gshared (ValueCollection_t2488352170 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m652273862(__this, ___item0, method) ((  void (*) (ValueCollection_t2488352170 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m652273862_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1090922639_gshared (ValueCollection_t2488352170 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1090922639(__this, method) ((  void (*) (ValueCollection_t2488352170 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1090922639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1876897568_gshared (ValueCollection_t2488352170 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1876897568(__this, ___item0, method) ((  bool (*) (ValueCollection_t2488352170 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1876897568_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16355269_gshared (ValueCollection_t2488352170 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16355269(__this, ___item0, method) ((  bool (*) (ValueCollection_t2488352170 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16355269_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m989087389_gshared (ValueCollection_t2488352170 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m989087389(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2488352170 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m989087389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1701487315_gshared (ValueCollection_t2488352170 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1701487315(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2488352170 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1701487315_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3277652878_gshared (ValueCollection_t2488352170 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3277652878(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2488352170 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3277652878_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m152073427_gshared (ValueCollection_t2488352170 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m152073427(__this, method) ((  bool (*) (ValueCollection_t2488352170 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m152073427_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m932781875_gshared (ValueCollection_t2488352170 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m932781875(__this, method) ((  bool (*) (ValueCollection_t2488352170 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m932781875_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2996495583_gshared (ValueCollection_t2488352170 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2996495583(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2488352170 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2996495583_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1725337395_gshared (ValueCollection_t2488352170 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1725337395(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2488352170 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1725337395_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::GetEnumerator()
extern "C"  Enumerator_t333243019  ValueCollection_GetEnumerator_m2842095958_gshared (ValueCollection_t2488352170 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2842095958(__this, method) ((  Enumerator_t333243019  (*) (ValueCollection_t2488352170 *, const MethodInfo*))ValueCollection_GetEnumerator_m2842095958_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2597241465_gshared (ValueCollection_t2488352170 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2597241465(__this, method) ((  int32_t (*) (ValueCollection_t2488352170 *, const MethodInfo*))ValueCollection_get_Count_m2597241465_gshared)(__this, method)
