﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<PayForPurchase>c__AnonStoreyA5
struct U3CPayForPurchaseU3Ec__AnonStoreyA5_t874819163;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<PayForPurchase>c__AnonStoreyA5::.ctor()
extern "C"  void U3CPayForPurchaseU3Ec__AnonStoreyA5__ctor_m786931640 (U3CPayForPurchaseU3Ec__AnonStoreyA5_t874819163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<PayForPurchase>c__AnonStoreyA5::<>m__92(PlayFab.CallRequestContainer)
extern "C"  void U3CPayForPurchaseU3Ec__AnonStoreyA5_U3CU3Em__92_m2043130351 (U3CPayForPurchaseU3Ec__AnonStoreyA5_t874819163 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
