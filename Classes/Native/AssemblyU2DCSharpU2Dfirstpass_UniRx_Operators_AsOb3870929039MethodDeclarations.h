﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>
struct AsObservable_t3870929039;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AsObservable__ctor_m346186224_gshared (AsObservable_t3870929039 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AsObservable__ctor_m346186224(__this, ___observer0, ___cancel1, method) ((  void (*) (AsObservable_t3870929039 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AsObservable__ctor_m346186224_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>::OnNext(T)
extern "C"  void AsObservable_OnNext_m625907618_gshared (AsObservable_t3870929039 * __this, bool ___value0, const MethodInfo* method);
#define AsObservable_OnNext_m625907618(__this, ___value0, method) ((  void (*) (AsObservable_t3870929039 *, bool, const MethodInfo*))AsObservable_OnNext_m625907618_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>::OnError(System.Exception)
extern "C"  void AsObservable_OnError_m4060294833_gshared (AsObservable_t3870929039 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AsObservable_OnError_m4060294833(__this, ___error0, method) ((  void (*) (AsObservable_t3870929039 *, Exception_t1967233988 *, const MethodInfo*))AsObservable_OnError_m4060294833_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Boolean>::OnCompleted()
extern "C"  void AsObservable_OnCompleted_m2082841476_gshared (AsObservable_t3870929039 * __this, const MethodInfo* method);
#define AsObservable_OnCompleted_m2082841476(__this, method) ((  void (*) (AsObservable_t3870929039 *, const MethodInfo*))AsObservable_OnCompleted_m2082841476_gshared)(__this, method)
