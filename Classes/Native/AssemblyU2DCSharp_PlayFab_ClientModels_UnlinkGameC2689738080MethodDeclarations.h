﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkGameCenterAccountResult
struct UnlinkGameCenterAccountResult_t2689738080;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkGameCenterAccountResult::.ctor()
extern "C"  void UnlinkGameCenterAccountResult__ctor_m1655439305 (UnlinkGameCenterAccountResult_t2689738080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
