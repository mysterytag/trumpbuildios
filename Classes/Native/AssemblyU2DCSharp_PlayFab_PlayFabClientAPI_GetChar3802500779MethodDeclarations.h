﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterInventoryRequestCallback
struct GetCharacterInventoryRequestCallback_t3802500779;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterInventoryRequest
struct GetCharacterInventoryRequest_t2835266646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2835266646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterInventoryRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterInventoryRequestCallback__ctor_m1136868890 (GetCharacterInventoryRequestCallback_t3802500779 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterInventoryRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterInventoryRequest,System.Object)
extern "C"  void GetCharacterInventoryRequestCallback_Invoke_m3252455807 (GetCharacterInventoryRequestCallback_t3802500779 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterInventoryRequest_t2835266646 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterInventoryRequestCallback_t3802500779(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterInventoryRequest_t2835266646 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterInventoryRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterInventoryRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterInventoryRequestCallback_BeginInvoke_m207601916 (GetCharacterInventoryRequestCallback_t3802500779 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterInventoryRequest_t2835266646 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterInventoryRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterInventoryRequestCallback_EndInvoke_m894291242 (GetCharacterInventoryRequestCallback_t3802500779 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
