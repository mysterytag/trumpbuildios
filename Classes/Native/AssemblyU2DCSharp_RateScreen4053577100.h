﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// GlobalStat
struct GlobalStat_t1134678199;

#include "AssemblyU2DCSharp_WindowBase3855465217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RateScreen
struct  RateScreen_t4053577100  : public WindowBase_t3855465217
{
public:
	// UniRx.Subject`1<UniRx.Unit> RateScreen::RateNow
	Subject_1_t201353362 * ___RateNow_6;
	// UniRx.Subject`1<UniRx.Unit> RateScreen::RateLater
	Subject_1_t201353362 * ___RateLater_7;
	// GlobalStat RateScreen::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_8;

public:
	inline static int32_t get_offset_of_RateNow_6() { return static_cast<int32_t>(offsetof(RateScreen_t4053577100, ___RateNow_6)); }
	inline Subject_1_t201353362 * get_RateNow_6() const { return ___RateNow_6; }
	inline Subject_1_t201353362 ** get_address_of_RateNow_6() { return &___RateNow_6; }
	inline void set_RateNow_6(Subject_1_t201353362 * value)
	{
		___RateNow_6 = value;
		Il2CppCodeGenWriteBarrier(&___RateNow_6, value);
	}

	inline static int32_t get_offset_of_RateLater_7() { return static_cast<int32_t>(offsetof(RateScreen_t4053577100, ___RateLater_7)); }
	inline Subject_1_t201353362 * get_RateLater_7() const { return ___RateLater_7; }
	inline Subject_1_t201353362 ** get_address_of_RateLater_7() { return &___RateLater_7; }
	inline void set_RateLater_7(Subject_1_t201353362 * value)
	{
		___RateLater_7 = value;
		Il2CppCodeGenWriteBarrier(&___RateLater_7, value);
	}

	inline static int32_t get_offset_of_GlobalStat_8() { return static_cast<int32_t>(offsetof(RateScreen_t4053577100, ___GlobalStat_8)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_8() const { return ___GlobalStat_8; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_8() { return &___GlobalStat_8; }
	inline void set_GlobalStat_8(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_8 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
