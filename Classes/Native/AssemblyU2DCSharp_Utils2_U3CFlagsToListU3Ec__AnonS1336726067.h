﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils2/<FlagsToList>c__AnonStoreyF2`1<System.Object>
struct  U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067  : public Il2CppObject
{
public:
	// System.Int32 Utils2/<FlagsToList>c__AnonStoreyF2`1::mask
	int32_t ___mask_0;

public:
	inline static int32_t get_offset_of_mask_0() { return static_cast<int32_t>(offsetof(U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067, ___mask_0)); }
	inline int32_t get_mask_0() const { return ___mask_0; }
	inline int32_t* get_address_of_mask_0() { return &___mask_0; }
	inline void set_mask_0(int32_t value)
	{
		___mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
