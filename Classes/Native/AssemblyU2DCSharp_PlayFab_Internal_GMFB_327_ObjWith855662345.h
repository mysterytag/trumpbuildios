﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.GMFB_327/ObjWithTimes
struct  ObjWithTimes_t855662345  : public Il2CppObject
{
public:
	// System.DateTime PlayFab.Internal.GMFB_327/ObjWithTimes::timestamp
	DateTime_t339033936  ___timestamp_0;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(ObjWithTimes_t855662345, ___timestamp_0)); }
	inline DateTime_t339033936  get_timestamp_0() const { return ___timestamp_0; }
	inline DateTime_t339033936 * get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(DateTime_t339033936  value)
	{
		___timestamp_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
