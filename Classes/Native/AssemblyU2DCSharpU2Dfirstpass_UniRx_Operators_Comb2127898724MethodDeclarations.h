﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Comb2231862077MethodDeclarations.h"

// System.Void UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,<>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>>::.ctor(System.Object,System.IntPtr)
#define CombineLatestFunc_5__ctor_m642519279(__this, ___object0, ___method1, method) ((  void (*) (CombineLatestFunc_5_t2127898724 *, Il2CppObject *, IntPtr_t, const MethodInfo*))CombineLatestFunc_5__ctor_m1266321991_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,<>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>>::Invoke(T1,T2,T3,T4)
#define CombineLatestFunc_5_Invoke_m1857392206(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  U3CU3E__AnonType0_4_t733143067 * (*) (CombineLatestFunc_5_t2127898724 *, bool, bool, bool, bool, const MethodInfo*))CombineLatestFunc_5_Invoke_m1701543740_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,<>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define CombineLatestFunc_5_BeginInvoke_m1799621232(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (CombineLatestFunc_5_t2127898724 *, bool, bool, bool, bool, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_5_BeginInvoke_m3236708292_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TR UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,<>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>>::EndInvoke(System.IAsyncResult)
#define CombineLatestFunc_5_EndInvoke_m3868508140(__this, ___result0, method) ((  U3CU3E__AnonType0_4_t733143067 * (*) (CombineLatestFunc_5_t2127898724 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_5_EndInvoke_m3471871550_gshared)(__this, ___result0, method)
