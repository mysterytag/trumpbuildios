﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8
struct U3CLazyTaskTestU3Ec__Iterator8_t1612341822;
// System.Object
struct Il2CppObject;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;
// UniRx.LazyTask`1<System.Int32>
struct LazyTask_1_t4247718180;
// UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9
struct U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994;
// UniRx.ObservableYieldInstruction`1<System.Int64>
struct ObservableYieldInstruction_1_t674360470;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// UniRx.ObservableYieldInstruction`1<System.String>
struct ObservableYieldInstruction_1_t3090401786;
// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// UniRx.ObservableYieldInstruction`1<System.Object>
struct ObservableYieldInstruction_1_t2959019304;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IObservable`1<UnityEngine.Transform>
struct IObservable_1_t43351477;
// UniRx.Examples.Sample06_ConvertToCoroutine
struct Sample06_ConvertToCoroutine_t617332884;
// System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform>
struct Func_2_t140622909;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t884531080;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// UniRx.ObservableYieldInstruction`1<UnityEngine.Transform>
struct ObservableYieldInstruction_1_t2406465997;
// UnityEngine.Transform
struct Transform_t284553113;
// UniRx.Examples.Sample07_OrchestratIEnumerator
struct Sample07_OrchestratIEnumerator_t3702211539;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t1429988286;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA
struct U3CAsyncAU3Ec__IteratorA_t2615160205;
// UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB
struct U3CAsyncBU3Ec__IteratorB_t2109601581;
// UniRx.Examples.Sample08_DetectDoubleClick
struct Sample08_DetectDoubleClick_t1711438338;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// UniRx.IObservable`1<System.Collections.Generic.IList`1<System.Int64>>
struct IObservable_1_t477738264;
// System.Func`2<System.Collections.Generic.IList`1<System.Int64>,System.Boolean>
struct Func_2_t2964252473;
// System.Action`1<System.Collections.Generic.IList`1<System.Int64>>
struct Action_1_t867392605;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Collections.Generic.IList`1<System.Int64>
struct IList_1_t718939900;
// UniRx.Examples.Sample09_EventHandling
struct Sample09_EventHandling_t2113606094;
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct EventHandler_1_t3942771990;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// UniRx.IObservable`1<UniRx.EventPattern`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>
struct IObservable_1_t629922973;
// System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>
struct Func_2_t1776554224;
// System.Action`1<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>
struct Action_1_t4091224695;
// UniRx.IObservable`1<UniRx.EventPattern`1<System.Object>>
struct IObservable_1_t2661802342;
// System.Func`2<System.EventHandler`1<System.Object>,System.Object>
struct Func_2_t2937104577;
// System.Collections.Generic.ICollection`1<System.IDisposable>
struct ICollection_1_t2094752760;
// UniRx.IObservable`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct IObservable_1_t2858992711;
// System.Func`2<System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>
struct Func_2_t4235389922;
// System.Func`2<System.Action`1<System.Object>,System.Object>
struct Func_2_t1100972979;
// System.Action`1<System.Action`1<System.Int32>>
struct Action_1_t3144320197;
// System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct Action_1_t3248647052;
// UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey31
struct U3CStartU3Ec__AnonStorey31_t1152509869;
// UniRx.Examples.Sample09_EventHandling/MyEventArgs
struct MyEventArgs_t3100194347;
// UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32
struct U3CStartU3Ec__AnonStorey32_t1152509870;
// UniRx.Examples.Sample10_MainThreadDispatcher
struct Sample10_MainThreadDispatcher_t1609111451;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// System.Func`1<UniRx.Unit>
struct Func_1_t3701067285;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC
struct U3CTestAsyncU3Ec__IteratorC_t488520482;
// UniRx.Examples.Sample11_Logger
struct Sample11_Logger_t602978400;
// UniRx.IObservable`1<UniRx.Diagnostics.LogEntry>
struct IObservable_1_t1649954086;
// System.Func`2<UniRx.Diagnostics.LogEntry,System.Boolean>
struct Func_2_t1476384563;
// System.Action`1<UniRx.Diagnostics.LogEntry>
struct Action_1_t2039608427;
// UniRx.Diagnostics.LogEntry
struct LogEntry_t1891155722;
// UniRx.FloatReactiveProperty
struct FloatReactiveProperty_t3982138748;
// UnityEngine.YieldInstruction
struct YieldInstruction_t3557331758;
struct YieldInstruction_t3557331758_marshaled_pinvoke;
// UniRx.InspectorDisplayAttribute
struct InspectorDisplayAttribute_t3553855523;
// System.String
struct String_t;
// UniRx.InternalUtil.AsyncLock
struct AsyncLock_t4023816884;
// System.Action
struct Action_t437523947;
// UniRx.InternalUtil.ScheduledItem
struct ScheduledItem_t1912903245;
// UniRx.InternalUtil.SchedulerQueue
struct SchedulerQueue_t3710535235;
// UniRx.InternalUtil.ThreadSafeQueueWorker
struct ThreadSafeQueueWorker_t3889225637;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// UniRx.IntReactiveProperty
struct IntReactiveProperty_t928974095;
// UniRx.LazyTask
struct LazyTask_t1365889643;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// UniRx.LazyTask[]
struct LazyTaskU5BU5D_t3442259850;
// System.Collections.Generic.IEnumerable`1<UniRx.LazyTask>
struct IEnumerable_1_t4238043999;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Coroutine>
struct IEnumerable_1_t823779321;
// System.Func`2<UniRx.LazyTask,UnityEngine.Coroutine>
struct Func_2_t2681037974;
// UnityEngine.Coroutine[]
struct CoroutineU5BU5D_t1904733000;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// UniRx.LazyTask/<WhenAllCore>c__Iterator1F
struct U3CWhenAllCoreU3Ec__Iterator1F_t3942062194;
// UniRx.LongReactiveProperty
struct LongReactiveProperty_t1779754460;
// UniRx.MainThreadDispatcher
struct MainThreadDispatcher_t4027217788;
// UnityEngine.Component[]
struct ComponentU5BU5D_t552366831;
// UniRx.MainThreadDispatcher[]
struct MainThreadDispatcherU5BU5D_t933801749;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Exception
struct Exception_t1967233988;
// UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey7D
struct U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792;
// UniRx.MessageBroker
struct MessageBroker_t1308900818;
// UniRx.MultilineReactivePropertyAttribute
struct MultilineReactivePropertyAttribute_t2518291827;
// UniRx.MultipleAssignmentDisposable
struct MultipleAssignmentDisposable_t3090232719;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Func`1<UniRx.IObservable`1<UniRx.Unit>>
struct Func_1_t3459865649;
// System.Action`1<System.Action>
struct Action_1_t585976652;
// System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>
struct Func_3_t3381739440;
// System.Action`1<System.IAsyncResult>
struct Action_1_t686135974;
// System.Func`2<System.IAsyncResult,UniRx.Unit>
struct Func_2_t2571569893;
// System.Func`3<UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t3458695013;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t521084177;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.Func`3<System.Int64,UniRx.Unit,System.Int64>
struct Func_3_t3706795343;
// UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13
struct U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358;
// UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12
struct U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937;
// UniRx.Observable/<EveryUpdateCore>c__Iterator11
struct U3CEveryUpdateCoreU3Ec__Iterator11_t819860698;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey45
struct U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// UniRx.Observable/<FromCoroutine>c__AnonStorey4A
struct U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312;
// UniRx.Observable/<NextFrame>c__AnonStorey52
struct U3CNextFrameU3Ec__AnonStorey52_t2543931332;
// UniRx.Observable/<NextFrameCore>c__Iterator14
struct U3CNextFrameCoreU3Ec__Iterator14_t4059042515;
// UniRx.Observable/<TimerFrame>c__AnonStorey53
struct U3CTimerFrameU3Ec__AnonStorey53_t1969998059;
// UniRx.Observable/<TimerFrame>c__AnonStorey54
struct U3CTimerFrameU3Ec__AnonStorey54_t1969998060;
// UniRx.Observable/<TimerFrameCore>c__Iterator15
struct U3CTimerFrameCoreU3Ec__Iterator15_t2264980090;
// UniRx.Observable/<TimerFrameCore>c__Iterator16
struct U3CTimerFrameCoreU3Ec__Iterator16_t2264980091;
// UniRx.Observable/<ToAsync>c__AnonStorey3A
struct U3CToAsyncU3Ec__AnonStorey3A_t2315298076;
// UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B
struct U3CToAsyncU3Ec__AnonStorey3B_t2315298077;
// UniRx.Observable/<ToObservable>c__AnonStorey51
struct U3CToObservableU3Ec__AnonStorey51_t3647079135;
// UniRx.Observable/<WrapEnumerator>c__IteratorF
struct U3CWrapEnumeratorU3Ec__IteratorF_t3900889545;
// UniRx.Observable/EveryAfterUpdateInvoker
struct EveryAfterUpdateInvoker_t3949333456;
// UniRx.ObservableMonoBehaviour
struct ObservableMonoBehaviour_t2173505449;
// System.Single[]
struct SingleU5BU5D_t1219431280;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Single[],System.Int32>>
struct IObservable_1_t3709910074;
// UnityEngine.Collision
struct Collision_t1119538015;
// UniRx.IObservable`1<UnityEngine.Collision>
struct IObservable_1_t878336379;
// UnityEngine.Collision2D
struct Collision2D_t452810033;
// UniRx.IObservable`1<UnityEngine.Collision2D>
struct IObservable_1_t211608397;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2693066224;
// UniRx.IObservable`1<UnityEngine.ControllerColliderHit>
struct IObservable_1_t2451864588;
// UniRx.IObservable`1<System.Single>
struct IObservable_1_t717007385;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UniRx.IObservable`1<UnityEngine.GameObject>
struct IObservable_1_t3771493466;
// UnityEngine.RenderTexture
struct RenderTexture_t12905170;
// UniRx.IObservable`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>
struct IObservable_1_t1676913119;
// UnityEngine.Collider
struct Collider_t955670625;
// UniRx.IObservable`1<UnityEngine.Collider>
struct IObservable_1_t714468989;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;
// UniRx.IObservable`1<UnityEngine.Collider2D>
struct IObservable_1_t1648836559;
// UniRx.IObservable`1<UnityEngine.NetworkDisconnection>
struct IObservable_1_t97558759;
// UniRx.IObservable`1<UnityEngine.NetworkConnectionError>
struct IObservable_1_t782604357;
// UniRx.IObservable`1<UnityEngine.MasterServerEvent>
struct IObservable_1_t2036605854;
// UniRx.IObservable`1<UnityEngine.NetworkMessageInfo>
struct IObservable_1_t2333143248;
// UniRx.IObservable`1<UnityEngine.NetworkPlayer>
struct IObservable_1_t1039935736;
// UnityEngine.BitStream
struct BitStream_t3159523098;
// UniRx.IObservable`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>
struct IObservable_1_t3034811225;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;
// System.Func`3<UniRx.IObserver`1<System.String>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t212750549;
// System.Func`3<UniRx.IObserver`1<System.Object>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1973235155;
// UniRx.IObservable`1<System.Byte[]>
struct IObservable_1_t4112271820;
// System.Func`3<UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t2061407023;
// UniRx.IObservable`1<UnityEngine.WWW>
struct IObservable_1_t1281770464;
// System.Func`3<UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1491243043;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// UnityEngine.WWWForm
struct WWWForm_t3999572776;
// UniRx.IObservable`1<UnityEngine.AssetBundle>
struct IObservable_1_t3718229467;
// System.Func`3<UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t3855672678;
// UnityEngine.WWW
struct WWW_t1522972100;
// UniRx.IObserver`1<UnityEngine.WWW>
struct IObserver_1_t3734971003;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;
// UniRx.IObserver`1<System.Byte[]>
struct IObserver_1_t2270505063;
// UniRx.IObserver`1<UnityEngine.AssetBundle>
struct IObserver_1_t1876462710;
// UniRx.ObservableWWW/<Fetch>c__Iterator20
struct U3CFetchU3Ec__Iterator20_t2016563951;
// UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23
struct U3CFetchAssetBundleU3Ec__Iterator23_t3024943464;
// UniRx.ObservableWWW/<FetchBytes>c__Iterator22
struct U3CFetchBytesU3Ec__Iterator22_t157331456;
// UniRx.ObservableWWW/<FetchText>c__Iterator21
struct U3CFetchTextU3Ec__Iterator21_t605446781;
// UniRx.ObservableWWW/<Get>c__AnonStorey7E
struct U3CGetU3Ec__AnonStorey7E_t497370161;
// UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey7F
struct U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258;
// UniRx.ObservableWWW/<GetWWW>c__AnonStorey80
struct U3CGetWWWU3Ec__AnonStorey80_t1804159614;
// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8D
struct U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550;
// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8E
struct U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551;
// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8F
struct U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552;
// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90
struct U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561;
// UniRx.ObservableWWW/<Post>c__AnonStorey81
struct U3CPostU3Ec__AnonStorey81_t213725630;
// UniRx.ObservableWWW/<Post>c__AnonStorey82
struct U3CPostU3Ec__AnonStorey82_t213725631;
// UniRx.ObservableWWW/<Post>c__AnonStorey83
struct U3CPostU3Ec__AnonStorey83_t213725632;
// UniRx.ObservableWWW/<Post>c__AnonStorey84
struct U3CPostU3Ec__AnonStorey84_t213725633;
// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey85
struct U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790;
// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey86
struct U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791;
// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey87
struct U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792;
// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey88
struct U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793;
// UniRx.ObservableWWW/<PostWWW>c__AnonStorey89
struct U3CPostWWWU3Ec__AnonStorey89_t4245247429;
// UniRx.ObservableWWW/<PostWWW>c__AnonStorey8A
struct U3CPostWWWU3Ec__AnonStorey8A_t4245247437;
// UniRx.ObservableWWW/<PostWWW>c__AnonStorey8B
struct U3CPostWWWU3Ec__AnonStorey8B_t4245247438;
// UniRx.ObservableWWW/<PostWWW>c__AnonStorey8C
struct U3CPostWWWU3Ec__AnonStorey8C_t4245247439;
// UniRx.Operators.FromEventObservable
struct FromEventObservable_t1591372576;
// UniRx.Operators.FromEventObservable/FromEvent
struct FromEvent_t2060698864;
// UniRx.Operators.RangeObservable
struct RangeObservable_t282483469;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64
struct U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636;
// UniRx.Operators.RangeObservable/Range
struct Range_t78727453;
// UniRx.Operators.TimerObservable
struct TimerObservable_t1697791765;
// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D
struct U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652;
// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E
struct U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653;
// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F
struct U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654;
// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70
struct U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;
// UniRx.Operators.TimerObservable/Timer
struct Timer_t80811813;
// UniRx.PresenterBase
struct PresenterBase_t3597493419;
// UniRx.QuaternionReactiveProperty
struct QuaternionReactiveProperty_t512953982;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion>
struct IEqualityComparer_1_t4215982630;
// UniRx.RangeReactivePropertyAttribute
struct RangeReactivePropertyAttribute_t252243683;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Boolean>>
struct IEnumerable_1_t2841958061;
// UniRx.IObservable`1<System.Collections.Generic.IList`1<System.Boolean>>
struct IObservable_1_t2136296019;
// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean>
struct Func_2_t1721034418;
// System.Collections.Generic.IList`1<System.Boolean>
struct IList_1_t2377497655;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1612341822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1612341822MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_Core_System_Func_1_gen3990196034MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTaskExtens3019974495MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_1_gen4247718180MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_Core_System_Func_1_gen3990196034.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_1_gen4247718180.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTaskExtens3019974495.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2930300994.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2930300994MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW156691558MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel3090401786MethodDeclarations.h"
#include "System_Core_System_Func_2_gen140622909MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi3692409186MethodDeclarations.h"
#include "System_Core_System_Func_2_gen884531080MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYield674360470.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel3090401786.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sample617332884.h"
#include "System_Core_System_Func_2_gen140622909.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserveExtensi3692409186.h"
#include "System_Core_System_Func_2_gen884531080.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableYiel2406465997.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3702211539.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3702211539MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2615160205MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2615160205.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2109601581MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2109601581.h"
#include "System_Core_System_Func_1_gen1429988286MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1429988286.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1917318876MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1917318876.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1711438338.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1711438338MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2251336571MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2964252473MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen867392605MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2251336571.h"
#include "mscorlib_System_Int642847414882.h"
#include "System_Core_System_Func_2_gen2964252473.h"
#include "mscorlib_System_Action_1_gen867392605.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2113606094.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2113606094MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen490482111MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen490482111.h"
#include "mscorlib_System_EventHandler_1_gen3942771990.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1152509870MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1776554224MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen4091224695MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DisposableExten691219686MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4235389922MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3144320197MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1152509870.h"
#include "System_Core_System_Func_2_gen1776554224.h"
#include "mscorlib_System_Action_1_gen4091224695.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DisposableExten691219686.h"
#include "System_Core_System_Func_2_gen4235389922.h"
#include "mscorlib_System_Action_1_gen3248647052.h"
#include "mscorlib_System_Action_1_gen3144320197.h"
#include "mscorlib_System_EventHandler_1_gen3942771990MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3100194347.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1152509869MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1152509869.h"
#include "mscorlib_System_Action_1_gen3248647052MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3100194347MethodDeclarations.h"
#include "mscorlib_System_EventArgs516466188MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1609111451.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1609111451MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp4027217788MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587MethodDeclarations.h"
#include "System_Core_System_Func_1_gen3701067285MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen2995867587.h"
#include "System_Core_System_Func_1_gen3701067285.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Action_1_gen2706738743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sample488520482MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sample488520482.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sample602978400.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sample602978400MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo2118475020MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo2118475020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1038796222MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1476384563MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2039608427MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Obs586897455.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Obs586897455MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1476384563.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1891155722.h"
#include "mscorlib_System_Action_1_gen2039608427.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1891155722MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FloatReactiveP3982138748.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FloatReactiveP3982138748MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1566542059MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType3551601282.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType3551601282MethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction3557331758.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_YieldInstructi1940766803.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_YieldInstructi1940766803MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate896427542.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InspectorDispl3553855523.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InspectorDispl3553855523MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3076083828MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_A4023816884.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_A4023816884MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen2145611487MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen2145611487.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_S1912903245.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_S1912903245MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_S3710535235.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_S3710535235MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P2411179237MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P2411179237.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_T3889225637.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_T3889225637MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2115686693.h"
#include "mscorlib_System_Action_1_gen2115686693MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_IntReactiveProp928974095.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_IntReactiveProp928974095MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3455747825MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask1365889643.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask1365889643MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_TaskS2963002871.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "System_Core_System_Func_2_gen2681037974MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "System_Core_System_Func_2_gen2681037974.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_U3CWh3942062194MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_U3CWh3942062194.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_TaskS2963002871MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LongReactivePr1779754460.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LongReactivePr1779754460MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3455747920MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp4027217788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp1899807443.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDispa335283792MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDispa335283792.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen3310424369.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen3310424369MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3216235232MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3216235232.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen201353362MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen201353362.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2149039961MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2149039961.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp1899807443MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp3692624617.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp3692624617MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MessageBroker1308900818.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MessageBroker1308900818MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2949654135MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2949654135.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MultilineReact2518291827.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MultilineReact2518291827MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MultipleAssign3090232719.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MultipleAssign3090232719MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification3315198013.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Notification3315198013MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_NotificationKi3324123761.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_NotificationKi3324123761MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1182951310MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1182951310.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Defa3564383257MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Range282483469MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Range282483469.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star4083709414MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Star4083709414.h"
#include "mscorlib_System_Nullable_1_gen3649900800MethodDeclarations.h"
#include "System_Core_System_Func_1_gen3459865649.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2315298076MethodDeclarations.h"
#include "System_Core_System_Func_1_gen3459865649MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2315298076.h"
#include "mscorlib_System_Action_1_gen585976652.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1591372576MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1591372576.h"
#include "System_Core_System_Func_3_gen3381739440.h"
#include "mscorlib_System_Action_1_gen686135974.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3402247396MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2571569893MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3402247396.h"
#include "System_Core_System_Func_2_gen2571569893.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1697791765MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1697791765.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2604282312MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3458695013MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2604282312.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "System_Core_System_Func_3_gen3458695013.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3900889545MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3900889545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3647079135MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3647079135.h"
#include "System_Core_System_Func_3_gen521084177MethodDeclarations.h"
#include "System_Core_System_Func_3_gen521084177.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CE819860698MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CE819860698.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CE976881937MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3CE976881937.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3054729358MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C3054729358.h"
#include "System_Core_System_Func_3_gen3706795343MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3706795343.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2543931332MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2543931332.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4059042515MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C4059042515.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1969998059MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1969998059.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1969998060MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C1969998060.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2264980090MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2264980090.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2264980091MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2264980091.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_Eve3949333456MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_Eve3949333456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen686135974MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2315298077MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_13257925142MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable_U3C2315298077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_13257925142.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableMono2173505449.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableMono2173505449MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TypedMonoBehav4014417954MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple2636817114MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1594179034MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1594179034.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen3951111710.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple2636817114.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3057572635MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3057572635.h"
#include "UnityEngine_UnityEngine_Collision2D452810033.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2390844653MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2390844653.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2693066224.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen336133548MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen336133548.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2896243641MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2896243641.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1655762426MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1655762426.h"
#include "UnityEngine_UnityEngine_RenderTexture12905170.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3856149375MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3856149375.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen1918114755.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2893705245MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2893705245.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3828072815MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3828072815.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2276795015MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2276795015.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2961840613MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2961840613.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen4215842110MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen4215842110.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen217412208MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen217412208.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3219171992MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3219171992.h"
#include "UnityEngine_UnityEngine_BitStream3159523098.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen919080185MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen919080185.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen3276012861.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW156691558.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U497370161MethodDeclarations.h"
#include "System_Core_System_Func_3_gen212750549MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U497370161.h"
#include "System_Core_System_Func_3_gen212750549.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U317477258MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2061407023MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U317477258.h"
#include "System_Core_System_Func_3_gen2061407023.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1804159614MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1491243043MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1804159614.h"
#include "System_Core_System_Func_3_gen1491243043.h"
#include "mscorlib_System_Byte2778693821.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U213725630MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U213725630.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U213725631MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U213725631.h"
#include "UnityEngine_UnityEngine_WWWForm3999572776.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U213725632MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U213725632.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U213725633MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm3999572776MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U213725633.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3957496790MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3957496790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3957496791MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3957496791.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3957496792MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3957496792.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3957496793MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3957496793.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_4245247429MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_4245247429.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_4245247437MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_4245247437.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_4245247438MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_4245247438.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_4245247439MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_4245247439.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1059502550MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3855672678MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1059502550.h"
#include "System_Core_System_Func_3_gen3855672678.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1059502551MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1059502551.h"
#include "UnityEngine_UnityEngine_Hash1283885020822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1059502552MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1059502552.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1059502561MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_1059502561.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747.h"
#include "UnityEngine_UnityEngine_WWW1522972100.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_2016563951MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_2016563951.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U605446781MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U605446781.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U157331456MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_U157331456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3024943464MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW_3024943464.h"
#include "UnityEngine_UnityEngine_WWW1522972100MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WWWErrorExcept3758994352MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WWWErrorExcept3758994352.h"
#include "UnityEngine_UnityEngine_AssetBundle3959431103.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer2968896040.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observer2968896040MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserverExtens3515240156.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObserverExtens3515240156MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2060698864MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From2060698864.h"
#include "mscorlib_System_Action_1_gen585976652MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559758MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Rang1995955636MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_RangeO78727453MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Rang1995955636.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_RangeO78727453.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559853MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2303330647MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2303330647.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1995955652MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TimerO80811813MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset3712260035MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1995955653MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1995955654MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SerialDisposab2547852742MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1995955663MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1060768302MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1995955652.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1995955653.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1995955654.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Time1995955663.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_TimerO80811813.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SerialDisposab2547852742.h"
#include "mscorlib_System_Action_1_gen912315597.h"
#include "mscorlib_System_Action_1_gen1060768302.h"
#include "mscorlib_System_Action_1_gen912315597MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_PresenterBase3597493419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_PresenterBase3597493419MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_PresenterBase_3949187127MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_PresenterBase_3949187127.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_QuaternionReact512953982.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_QuaternionReact512953982MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2500049017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_RangeReactivePr252243683.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_RangeReactivePr252243683MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveCollec3400253757.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveCollec3400253757MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveDictio4138429749.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveDictio4138429749MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1053651444.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1053651444MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1721034418MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1721034418.h"

// UniRx.IObservable`1<!!0> UniRx.Observable::Start<System.Int32>(System.Func`1<!!0>)
extern "C"  Il2CppObject* Observable_Start_TisInt32_t2847414787_m935877937_gshared (Il2CppObject * __this /* static, unused */, Func_1_t3990196034 * p0, const MethodInfo* method);
#define Observable_Start_TisInt32_t2847414787_m935877937(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_1_t3990196034 *, const MethodInfo*))Observable_Start_TisInt32_t2847414787_m935877937_gshared)(__this /* static, unused */, p0, method)
// UniRx.LazyTask`1<!!0> UniRx.LazyTaskExtensions::ToLazyTask<System.Int32>(UniRx.IObservable`1<!!0>)
extern "C"  LazyTask_1_t4247718180 * LazyTaskExtensions_ToLazyTask_TisInt32_t2847414787_m1736421255_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define LazyTaskExtensions_ToLazyTask_TisInt32_t2847414787_m1736421255(__this /* static, unused */, p0, method) ((  LazyTask_1_t4247718180 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))LazyTaskExtensions_ToLazyTask_TisInt32_t2847414787_m1736421255_gshared)(__this /* static, unused */, p0, method)
// UniRx.ObservableYieldInstruction`1<!!0> UniRx.Observable::ToYieldInstruction<System.Int64>(UniRx.IObservable`1<!!0>)
extern "C"  ObservableYieldInstruction_1_t674360470 * Observable_ToYieldInstruction_TisInt64_t2847414882_m1001181553_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_ToYieldInstruction_TisInt64_t2847414882_m1001181553(__this /* static, unused */, p0, method) ((  ObservableYieldInstruction_1_t674360470 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_ToYieldInstruction_TisInt64_t2847414882_m1001181553_gshared)(__this /* static, unused */, p0, method)
// UniRx.ObservableYieldInstruction`1<!!0> UniRx.Observable::ToYieldInstruction<System.Object>(UniRx.IObservable`1<!!0>,System.Boolean)
extern "C"  ObservableYieldInstruction_1_t2959019304 * Observable_ToYieldInstruction_TisIl2CppObject_m2265894534_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, bool p1, const MethodInfo* method);
#define Observable_ToYieldInstruction_TisIl2CppObject_m2265894534(__this /* static, unused */, p0, p1, method) ((  ObservableYieldInstruction_1_t2959019304 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, bool, const MethodInfo*))Observable_ToYieldInstruction_TisIl2CppObject_m2265894534_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.ObservableYieldInstruction`1<!!0> UniRx.Observable::ToYieldInstruction<System.String>(UniRx.IObservable`1<!!0>,System.Boolean)
#define Observable_ToYieldInstruction_TisString_t_m2732942552(__this /* static, unused */, p0, p1, method) ((  ObservableYieldInstruction_1_t3090401786 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, bool, const MethodInfo*))Observable_ToYieldInstruction_TisIl2CppObject_m2265894534_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!1> UniRx.ObserveExtensions::ObserveEveryValueChanged<System.Object,System.Object>(!!0,System.Func`2<!!0,!!1>,UniRx.FrameCountType)
extern "C"  Il2CppObject* ObserveExtensions_ObserveEveryValueChanged_TisIl2CppObject_TisIl2CppObject_m1821394904_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Func_2_t2135783352 * p1, int32_t p2, const MethodInfo* method);
#define ObserveExtensions_ObserveEveryValueChanged_TisIl2CppObject_TisIl2CppObject_m1821394904(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Func_2_t2135783352 *, int32_t, const MethodInfo*))ObserveExtensions_ObserveEveryValueChanged_TisIl2CppObject_TisIl2CppObject_m1821394904_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!1> UniRx.ObserveExtensions::ObserveEveryValueChanged<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform>(!!0,System.Func`2<!!0,!!1>,UniRx.FrameCountType)
#define ObserveExtensions_ObserveEveryValueChanged_TisSample06_ConvertToCoroutine_t617332884_TisTransform_t284553113_m3540279691(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Sample06_ConvertToCoroutine_t617332884 *, Func_2_t140622909 *, int32_t, const MethodInfo*))ObserveExtensions_ObserveEveryValueChanged_TisIl2CppObject_TisIl2CppObject_m1821394904_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FirstOrDefault<System.Object>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Observable_FirstOrDefault_TisIl2CppObject_m940328885_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Observable_FirstOrDefault_TisIl2CppObject_m940328885(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Observable_FirstOrDefault_TisIl2CppObject_m940328885_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FirstOrDefault<UnityEngine.Transform>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Observable_FirstOrDefault_TisTransform_t284553113_m2238600158(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t884531080 *, const MethodInfo*))Observable_FirstOrDefault_TisIl2CppObject_m940328885_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.ObservableYieldInstruction`1<!!0> UniRx.Observable::ToYieldInstruction<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  ObservableYieldInstruction_1_t2959019304 * Observable_ToYieldInstruction_TisIl2CppObject_m3240490007_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_ToYieldInstruction_TisIl2CppObject_m3240490007(__this /* static, unused */, p0, method) ((  ObservableYieldInstruction_1_t2959019304 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_ToYieldInstruction_TisIl2CppObject_m3240490007_gshared)(__this /* static, unused */, p0, method)
// UniRx.ObservableYieldInstruction`1<!!0> UniRx.Observable::ToYieldInstruction<UnityEngine.Transform>(UniRx.IObservable`1<!!0>)
#define Observable_ToYieldInstruction_TisTransform_t284553113_m127016576(__this /* static, unused */, p0, method) ((  ObservableYieldInstruction_1_t2406465997 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_ToYieldInstruction_TisIl2CppObject_m3240490007_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::SelectMany<UniRx.Unit>(UniRx.IObservable`1<!!0>,System.Func`1<System.Collections.IEnumerator>,System.Boolean)
extern "C"  Il2CppObject* Observable_SelectMany_TisUnit_t2558286038_m761892958_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_1_t1429988286 * p1, bool p2, const MethodInfo* method);
#define Observable_SelectMany_TisUnit_t2558286038_m761892958(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_1_t1429988286 *, bool, const MethodInfo*))Observable_SelectMany_TisUnit_t2558286038_m761892958_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<UniRx.Unit>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Where<System.Int64>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Observable_Where_TisInt64_t2847414882_m2960877922_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2251336571 * p1, const MethodInfo* method);
#define Observable_Where_TisInt64_t2847414882_m2960877922(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2251336571 *, const MethodInfo*))Observable_Where_TisInt64_t2847414882_m2960877922_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Throttle<System.Int64>(UniRx.IObservable`1<!!0>,System.TimeSpan)
extern "C"  Il2CppObject* Observable_Throttle_TisInt64_t2847414882_m3662844575_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, TimeSpan_t763862892  p1, const MethodInfo* method);
#define Observable_Throttle_TisInt64_t2847414882_m3662844575(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, TimeSpan_t763862892 , const MethodInfo*))Observable_Throttle_TisInt64_t2847414882_m3662844575_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<System.Collections.Generic.IList`1<!!0>> UniRx.Observable::Buffer<System.Int64,System.Int64>(UniRx.IObservable`1<!!0>,UniRx.IObservable`1<!!1>)
extern "C"  Il2CppObject* Observable_Buffer_TisInt64_t2847414882_TisInt64_t2847414882_m817331864_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, const MethodInfo* method);
#define Observable_Buffer_TisInt64_t2847414882_TisInt64_t2847414882_m817331864(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Observable_Buffer_TisInt64_t2847414882_TisInt64_t2847414882_m817331864_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Where<System.Object>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Observable_Where_TisIl2CppObject_m1271830458_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Observable_Where_TisIl2CppObject_m1271830458(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Observable_Where_TisIl2CppObject_m1271830458_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Where<System.Collections.Generic.IList`1<System.Int64>>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Observable_Where_TisIList_1_t718939900_m178084691(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2964252473 *, const MethodInfo*))Observable_Where_TisIl2CppObject_m1271830458_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Object>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t985559125 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t985559125 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Collections.Generic.IList`1<System.Int64>>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
#define ObservableExtensions_Subscribe_TisIList_1_t718939900_m1844099541(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t867392605 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<UniRx.EventPattern`1<!!1>> UniRx.Observable::FromEventPattern<System.Object,System.Object>(System.Func`2<System.EventHandler`1<!!1>,!!0>,System.Action`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject* Observable_FromEventPattern_TisIl2CppObject_TisIl2CppObject_m3025091750_gshared (Il2CppObject * __this /* static, unused */, Func_2_t2937104577 * p0, Action_1_t985559125 * p1, Action_1_t985559125 * p2, const MethodInfo* method);
#define Observable_FromEventPattern_TisIl2CppObject_TisIl2CppObject_m3025091750(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_2_t2937104577 *, Action_1_t985559125 *, Action_1_t985559125 *, const MethodInfo*))Observable_FromEventPattern_TisIl2CppObject_TisIl2CppObject_m3025091750_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<UniRx.EventPattern`1<!!1>> UniRx.Observable::FromEventPattern<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,UniRx.Examples.Sample09_EventHandling/MyEventArgs>(System.Func`2<System.EventHandler`1<!!1>,!!0>,System.Action`1<!!0>,System.Action`1<!!0>)
#define Observable_FromEventPattern_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m2912073386(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_2_t1776554224 *, Action_1_t4091224695 *, Action_1_t4091224695 *, const MethodInfo*))Observable_FromEventPattern_TisIl2CppObject_TisIl2CppObject_m3025091750_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244_gshared)(__this /* static, unused */, p0, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<UniRx.EventPattern`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>(UniRx.IObservable`1<!!0>)
#define ObservableExtensions_Subscribe_TisEventPattern_1_t871124609_m1009340427(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244_gshared)(__this /* static, unused */, p0, method)
// !!0 UniRx.DisposableExtensions::AddTo<System.Object>(!!0,System.Collections.Generic.ICollection`1<System.IDisposable>)
extern "C"  Il2CppObject * DisposableExtensions_AddTo_TisIl2CppObject_m633991815_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject* p1, const MethodInfo* method);
#define DisposableExtensions_AddTo_TisIl2CppObject_m633991815(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject*, const MethodInfo*))DisposableExtensions_AddTo_TisIl2CppObject_m633991815_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UniRx.DisposableExtensions::AddTo<System.IDisposable>(!!0,System.Collections.Generic.ICollection`1<System.IDisposable>)
#define DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject*, const MethodInfo*))DisposableExtensions_AddTo_TisIl2CppObject_m633991815_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!1> UniRx.Observable::FromEvent<System.Object,System.Object>(System.Func`2<System.Action`1<!!1>,!!0>,System.Action`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject* Observable_FromEvent_TisIl2CppObject_TisIl2CppObject_m2595454071_gshared (Il2CppObject * __this /* static, unused */, Func_2_t1100972979 * p0, Action_1_t985559125 * p1, Action_1_t985559125 * p2, const MethodInfo* method);
#define Observable_FromEvent_TisIl2CppObject_TisIl2CppObject_m2595454071(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_2_t1100972979 *, Action_1_t985559125 *, Action_1_t985559125 *, const MethodInfo*))Observable_FromEvent_TisIl2CppObject_TisIl2CppObject_m2595454071_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!1> UniRx.Observable::FromEvent<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,UniRx.Examples.Sample09_EventHandling/MyEventArgs>(System.Func`2<System.Action`1<!!1>,!!0>,System.Action`1<!!0>,System.Action`1<!!0>)
#define Observable_FromEvent_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m4177120813(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_2_t4235389922 *, Action_1_t4091224695 *, Action_1_t4091224695 *, const MethodInfo*))Observable_FromEvent_TisIl2CppObject_TisIl2CppObject_m2595454071_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<UniRx.Examples.Sample09_EventHandling/MyEventArgs>(UniRx.IObservable`1<!!0>)
#define ObservableExtensions_Subscribe_TisMyEventArgs_t3100194347_m2726699918(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromEvent<System.Int32>(System.Action`1<System.Action`1<!!0>>,System.Action`1<System.Action`1<!!0>>)
extern "C"  Il2CppObject* Observable_FromEvent_TisInt32_t2847414787_m521195233_gshared (Il2CppObject * __this /* static, unused */, Action_1_t3144320197 * p0, Action_1_t3144320197 * p1, const MethodInfo* method);
#define Observable_FromEvent_TisInt32_t2847414787_m521195233(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Action_1_t3144320197 *, Action_1_t3144320197 *, const MethodInfo*))Observable_FromEvent_TisInt32_t2847414787_m521195233_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Int32>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisInt32_t2847414787_m2600462747_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisInt32_t2847414787_m2600462747(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ObservableExtensions_Subscribe_TisInt32_t2847414787_m2600462747_gshared)(__this /* static, unused */, p0, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Int64>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t2995867587 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2995867587 *, const MethodInfo*))ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Start<UniRx.Unit>(System.Func`1<!!0>)
extern "C"  Il2CppObject* Observable_Start_TisUnit_t2558286038_m2976392644_gshared (Il2CppObject * __this /* static, unused */, Func_1_t3701067285 * p0, const MethodInfo* method);
#define Observable_Start_TisUnit_t2558286038_m2976392644(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_1_t3701067285 *, const MethodInfo*))Observable_Start_TisUnit_t2558286038_m2976392644_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::ObserveOnMainThread<UniRx.Unit>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject* Observable_ObserveOnMainThread_TisUnit_t2558286038_m2215972113_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_ObserveOnMainThread_TisUnit_t2558286038_m2215972113(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_ObserveOnMainThread_TisUnit_t2558286038_m2215972113_gshared)(__this /* static, unused */, p0, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<UniRx.Unit>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisUnit_t2558286038_m1708333140_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t2706738743 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisUnit_t2558286038_m1708333140(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2706738743 *, const MethodInfo*))ObservableExtensions_Subscribe_TisUnit_t2558286038_m1708333140_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Where<UniRx.Diagnostics.LogEntry>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Observable_Where_TisLogEntry_t1891155722_m486115208(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1476384563 *, const MethodInfo*))Observable_Where_TisIl2CppObject_m1271830458_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<UniRx.Diagnostics.LogEntry>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
#define ObservableExtensions_Subscribe_TisLogEntry_t1891155722_m1858544768(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2039608427 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::AsEnumerable<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_AsEnumerable_TisIl2CppObject_m1714986975_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_AsEnumerable_TisIl2CppObject_m1714986975(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_AsEnumerable_TisIl2CppObject_m1714986975_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::AsEnumerable<UniRx.LazyTask>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_AsEnumerable_TisLazyTask_t1365889643_m2445115720(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_AsEnumerable_TisIl2CppObject_m1714986975_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UniRx.LazyTask,UnityEngine.Coroutine>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisLazyTask_t1365889643_TisCoroutine_t2246592261_m2619238684(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2681037974 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m1058801104(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Coroutine>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisCoroutine_t2246592261_m1325913459(__this /* static, unused */, p0, method) ((  CoroutineU5BU5D_t1904733000* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m1765599783_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m1765599783(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m1765599783_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<UniRx.MainThreadDispatcher>()
#define Object_FindObjectOfType_TisMainThreadDispatcher_t4027217788_m4168215351(__this /* static, unused */, method) ((  MainThreadDispatcher_t4027217788 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m1765599783_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3048268896(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UniRx.MainThreadDispatcher>()
#define GameObject_AddComponent_TisMainThreadDispatcher_t4027217788_m1841224252(__this, method) ((  MainThreadDispatcher_t4027217788 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponents_TisIl2CppObject_m2493106446_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisIl2CppObject_m2493106446(__this, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m2493106446_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<UnityEngine.Component>()
#define GameObject_GetComponents_TisComponent_t2126946602_m2593473862(__this, method) ((  ComponentU5BU5D_t552366831* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m2493106446_gshared)(__this, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Object_FindObjectsOfType_TisIl2CppObject_m3928305500_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m3928305500(__this /* static, unused */, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m3928305500_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<UniRx.MainThreadDispatcher>()
#define Object_FindObjectsOfType_TisMainThreadDispatcher_t4027217788_m3133965346(__this /* static, unused */, method) ((  MainThreadDispatcherU5BU5D_t933801749* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m3928305500_gshared)(__this /* static, unused */, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Return<UniRx.Unit>(!!0)
extern "C"  Il2CppObject* Observable_Return_TisUnit_t2558286038_m1480352924_gshared (Il2CppObject * __this /* static, unused */, Unit_t2558286038  p0, const MethodInfo* method);
#define Observable_Return_TisUnit_t2558286038_m1480352924(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Unit_t2558286038 , const MethodInfo*))Observable_Return_TisUnit_t2558286038_m1480352924_gshared)(__this /* static, unused */, p0, method)
// System.Func`1<UniRx.IObservable`1<!!0>> UniRx.Observable::FromAsyncPattern<UniRx.Unit>(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,!!0>)
extern "C"  Func_1_t3459865649 * Observable_FromAsyncPattern_TisUnit_t2558286038_m4118748633_gshared (Il2CppObject * __this /* static, unused */, Func_3_t3381739440 * p0, Func_2_t2571569893 * p1, const MethodInfo* method);
#define Observable_FromAsyncPattern_TisUnit_t2558286038_m4118748633(__this /* static, unused */, p0, p1, method) ((  Func_1_t3459865649 * (*) (Il2CppObject * /* static, unused */, Func_3_t3381739440 *, Func_2_t2571569893 *, const MethodInfo*))Observable_FromAsyncPattern_TisUnit_t2558286038_m4118748633_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<UniRx.Unit>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  Il2CppObject* Observable_FromCoroutine_TisUnit_t2558286038_m3114512671_gshared (Il2CppObject * __this /* static, unused */, Func_3_t3458695013 * p0, const MethodInfo* method);
#define Observable_FromCoroutine_TisUnit_t2558286038_m3114512671(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t3458695013 *, const MethodInfo*))Observable_FromCoroutine_TisUnit_t2558286038_m3114512671_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<System.Int64>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  Il2CppObject* Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_gshared (Il2CppObject * __this /* static, unused */, Func_3_t521084177 * p0, const MethodInfo* method);
#define Observable_FromCoroutine_TisInt64_t2847414882_m2157755661(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t521084177 *, const MethodInfo*))Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!1> UniRx.Observable::Scan<UniRx.Unit,System.Int64>(UniRx.IObservable`1<!!0>,!!1,System.Func`3<!!1,!!0,!!1>)
extern "C"  Il2CppObject* Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int64_t p1, Func_3_t3706795343 * p2, const MethodInfo* method);
#define Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int64_t, Func_3_t3706795343 *, const MethodInfo*))Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::AsObservable<System.Boolean>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject* Observable_AsObservable_TisBoolean_t211005341_m3575928281_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_AsObservable_TisBoolean_t211005341_m3575928281(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_AsObservable_TisBoolean_t211005341_m3575928281_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Take<UniRx.Unit>(UniRx.IObservable`1<!!0>,System.Int32)
extern "C"  Il2CppObject* Observable_Take_TisUnit_t2558286038_m1136349407_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Observable_Take_TisUnit_t2558286038_m1136349407(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Observable_Take_TisUnit_t2558286038_m1136349407_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::AsObservable<UniRx.Unit>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject* Observable_AsObservable_TisUnit_t2558286038_m1062057990_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_AsObservable_TisUnit_t2558286038_m1062057990(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_AsObservable_TisUnit_t2558286038_m1062057990_gshared)(__this /* static, unused */, p0, method)
// UniRx.Tuple`2<!!0,!!1> UniRx.Tuple::Create<System.Object,System.Int32>(!!0,!!1)
extern "C"  Tuple_2_t2379570186  Tuple_Create_TisIl2CppObject_TisInt32_t2847414787_m2113783841_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
#define Tuple_Create_TisIl2CppObject_TisInt32_t2847414787_m2113783841(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t2379570186  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Tuple_Create_TisIl2CppObject_TisInt32_t2847414787_m2113783841_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.Tuple`2<!!0,!!1> UniRx.Tuple::Create<System.Single[],System.Int32>(!!0,!!1)
#define Tuple_Create_TisSingleU5BU5D_t1219431280_TisInt32_t2847414787_m3644026774(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t3951111710  (*) (Il2CppObject * /* static, unused */, SingleU5BU5D_t1219431280*, int32_t, const MethodInfo*))Tuple_Create_TisIl2CppObject_TisInt32_t2847414787_m2113783841_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.Tuple`2<!!0,!!1> UniRx.Tuple::Create<System.Object,System.Object>(!!0,!!1)
extern "C"  Tuple_2_t369261819  Tuple_Create_TisIl2CppObject_TisIl2CppObject_m4079527944_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
#define Tuple_Create_TisIl2CppObject_TisIl2CppObject_m4079527944(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t369261819  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_Create_TisIl2CppObject_TisIl2CppObject_m4079527944_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.Tuple`2<!!0,!!1> UniRx.Tuple::Create<UnityEngine.RenderTexture,UnityEngine.RenderTexture>(!!0,!!1)
#define Tuple_Create_TisRenderTexture_t12905170_TisRenderTexture_t12905170_m1667340520(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t1918114755  (*) (Il2CppObject * /* static, unused */, RenderTexture_t12905170 *, RenderTexture_t12905170 *, const MethodInfo*))Tuple_Create_TisIl2CppObject_TisIl2CppObject_m4079527944_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.Tuple`2<!!0,!!1> UniRx.Tuple::Create<System.Object,UnityEngine.NetworkMessageInfo>(!!0,!!1)
extern "C"  Tuple_2_t2106500283  Tuple_Create_TisIl2CppObject_TisNetworkMessageInfo_t2574344884_m515192336_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, NetworkMessageInfo_t2574344884  p1, const MethodInfo* method);
#define Tuple_Create_TisIl2CppObject_TisNetworkMessageInfo_t2574344884_m515192336(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t2106500283  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, NetworkMessageInfo_t2574344884 , const MethodInfo*))Tuple_Create_TisIl2CppObject_TisNetworkMessageInfo_t2574344884_m515192336_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.Tuple`2<!!0,!!1> UniRx.Tuple::Create<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>(!!0,!!1)
#define Tuple_Create_TisBitStream_t3159523098_TisNetworkMessageInfo_t2574344884_m886288216(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t3276012861  (*) (Il2CppObject * /* static, unused */, BitStream_t3159523098 *, NetworkMessageInfo_t2574344884 , const MethodInfo*))Tuple_Create_TisIl2CppObject_TisNetworkMessageInfo_t2574344884_m515192336_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<System.Object>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  Il2CppObject* Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared (Il2CppObject * __this /* static, unused */, Func_3_t1973235155 * p0, const MethodInfo* method);
#define Observable_FromCoroutine_TisIl2CppObject_m3257165431(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t1973235155 *, const MethodInfo*))Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<System.String>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
#define Observable_FromCoroutine_TisString_t_m3641581413(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t212750549 *, const MethodInfo*))Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<System.Byte[]>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
#define Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t2061407023 *, const MethodInfo*))Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<UnityEngine.WWW>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
#define Observable_FromCoroutine_TisWWW_t1522972100_m2159439317(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t1491243043 *, const MethodInfo*))Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<UnityEngine.AssetBundle>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
#define Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t3855672678 *, const MethodInfo*))Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<System.Collections.Generic.IList`1<!!0>> UniRx.Observable::CombineLatest<System.Boolean>(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<!!0>>)
extern "C"  Il2CppObject* Observable_CombineLatest_TisBoolean_t211005341_m3684453296_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Observable_CombineLatest_TisBoolean_t211005341_m3684453296(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Observable_CombineLatest_TisBoolean_t211005341_m3684453296_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!1> UniRx.Observable::Select<System.Object,System.Boolean>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Observable_Select_TisIl2CppObject_TisBoolean_t211005341_m1309489123_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Observable_Select_TisIl2CppObject_TisBoolean_t211005341_m1309489123(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Observable_Select_TisIl2CppObject_TisBoolean_t211005341_m1309489123_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!1> UniRx.Observable::Select<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,!!1>)
#define Observable_Select_TisIList_1_t2377497655_TisBoolean_t211005341_m1872695583(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1721034418 *, const MethodInfo*))Observable_Select_TisIl2CppObject_TisBoolean_t211005341_m1309489123_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::.ctor()
extern "C"  void U3CLazyTaskTestU3Ec__Iterator8__ctor_m244270134 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLazyTaskTestU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3516293020 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLazyTaskTestU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m642980656 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::MoveNext()
extern Il2CppClass* U3CLazyTaskTestU3Ec__Iterator8_t1612341822_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t3990196034_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLazyTaskTestU3Ec__Iterator8_U3CU3Em__1E_m4042962089_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m1040440978_MethodInfo_var;
extern const MethodInfo* Observable_Start_TisInt32_t2847414787_m935877937_MethodInfo_var;
extern const MethodInfo* LazyTaskExtensions_ToLazyTask_TisInt32_t2847414787_m1736421255_MethodInfo_var;
extern const MethodInfo* LazyTask_1_get_Result_m2773840719_MethodInfo_var;
extern const uint32_t U3CLazyTaskTestU3Ec__Iterator8_MoveNext_m1380846358_MetadataUsageId;
extern "C"  bool U3CLazyTaskTestU3Ec__Iterator8_MoveNext_m1380846358 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLazyTaskTestU3Ec__Iterator8_MoveNext_m1380846358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * G_B4_0 = NULL;
	U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * G_B3_0 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0087;
	}

IL_0021:
	{
		Func_1_t3990196034 * L_2 = ((U3CLazyTaskTestU3Ec__Iterator8_t1612341822_StaticFields*)U3CLazyTaskTestU3Ec__Iterator8_t1612341822_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		G_B3_0 = __this;
		if (L_2)
		{
			G_B4_0 = __this;
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)U3CLazyTaskTestU3Ec__Iterator8_U3CU3Em__1E_m4042962089_MethodInfo_var);
		Func_1_t3990196034 * L_4 = (Func_1_t3990196034 *)il2cpp_codegen_object_new(Func_1_t3990196034_il2cpp_TypeInfo_var);
		Func_1__ctor_m1040440978(L_4, NULL, L_3, /*hidden argument*/Func_1__ctor_m1040440978_MethodInfo_var);
		((U3CLazyTaskTestU3Ec__Iterator8_t1612341822_StaticFields*)U3CLazyTaskTestU3Ec__Iterator8_t1612341822_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_3(L_4);
		G_B4_0 = G_B3_0;
	}

IL_003a:
	{
		Func_1_t3990196034 * L_5 = ((U3CLazyTaskTestU3Ec__Iterator8_t1612341822_StaticFields*)U3CLazyTaskTestU3Ec__Iterator8_t1612341822_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_6 = Observable_Start_TisInt32_t2847414787_m935877937(NULL /*static, unused*/, L_5, /*hidden argument*/Observable_Start_TisInt32_t2847414787_m935877937_MethodInfo_var);
		LazyTask_1_t4247718180 * L_7 = LazyTaskExtensions_ToLazyTask_TisInt32_t2847414787_m1736421255(NULL /*static, unused*/, L_6, /*hidden argument*/LazyTaskExtensions_ToLazyTask_TisInt32_t2847414787_m1736421255_MethodInfo_var);
		NullCheck(G_B4_0);
		G_B4_0->set_U3CtaskU3E__0_0(L_7);
		LazyTask_1_t4247718180 * L_8 = __this->get_U3CtaskU3E__0_0();
		NullCheck(L_8);
		Coroutine_t2246592261 * L_9 = VirtFuncInvoker0< Coroutine_t2246592261 * >::Invoke(4 /* UnityEngine.Coroutine UniRx.LazyTask`1<System.Int32>::Start() */, L_8);
		__this->set_U24current_2(L_9);
		__this->set_U24PC_1(1);
		goto IL_0089;
	}

IL_006b:
	{
		LazyTask_1_t4247718180 * L_10 = __this->get_U3CtaskU3E__0_0();
		NullCheck(L_10);
		int32_t L_11 = LazyTask_1_get_Result_m2773840719(L_10, /*hidden argument*/LazyTask_1_get_Result_m2773840719_MethodInfo_var);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_0087:
	{
		return (bool)0;
	}

IL_0089:
	{
		return (bool)1;
	}
	// Dead block : IL_008b: ldloc.1
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::Dispose()
extern "C"  void U3CLazyTaskTestU3Ec__Iterator8_Dispose_m2713647859 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CLazyTaskTestU3Ec__Iterator8_Reset_m2185670371_MetadataUsageId;
extern "C"  void U3CLazyTaskTestU3Ec__Iterator8_Reset_m2185670371 (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLazyTaskTestU3Ec__Iterator8_Reset_m2185670371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::<>m__1E()
extern "C"  int32_t U3CLazyTaskTestU3Ec__Iterator8_U3CU3Em__1E_m4042962089 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return ((int32_t)100);
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::.ctor()
extern "C"  void U3CTestNewCustomYieldInstructionU3Ec__Iterator9__ctor_m1174907504 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestNewCustomYieldInstructionU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3558467756 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestNewCustomYieldInstructionU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m2292892736 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::MoveNext()
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t140622909_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t884531080_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_ToYieldInstruction_TisInt64_t2847414882_m1001181553_MethodInfo_var;
extern const MethodInfo* Observable_ToYieldInstruction_TisString_t_m2732942552_MethodInfo_var;
extern const MethodInfo* ObservableYieldInstruction_1_get_HasError_m2772237829_MethodInfo_var;
extern const MethodInfo* ObservableYieldInstruction_1_get_Error_m2578890956_MethodInfo_var;
extern const MethodInfo* ObservableYieldInstruction_1_get_HasResult_m2603143266_MethodInfo_var;
extern const MethodInfo* ObservableYieldInstruction_1_get_Result_m722695953_MethodInfo_var;
extern const MethodInfo* U3CTestNewCustomYieldInstructionU3Ec__Iterator9_U3CU3Em__1F_m2124214084_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3998431533_MethodInfo_var;
extern const MethodInfo* ObserveExtensions_ObserveEveryValueChanged_TisSample06_ConvertToCoroutine_t617332884_TisTransform_t284553113_m3540279691_MethodInfo_var;
extern const MethodInfo* U3CTestNewCustomYieldInstructionU3Ec__Iterator9_U3CU3Em__20_m4040796226_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m642311202_MethodInfo_var;
extern const MethodInfo* Observable_FirstOrDefault_TisTransform_t284553113_m2238600158_MethodInfo_var;
extern const MethodInfo* Observable_ToYieldInstruction_TisTransform_t284553113_m127016576_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2849422856;
extern const uint32_t U3CTestNewCustomYieldInstructionU3Ec__Iterator9_MoveNext_m3094489940_MetadataUsageId;
extern "C"  bool U3CTestNewCustomYieldInstructionU3Ec__Iterator9_MoveNext_m3094489940 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_MoveNext_m3094489940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Sample06_ConvertToCoroutine_t617332884 * G_B11_0 = NULL;
	U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * G_B11_1 = NULL;
	Sample06_ConvertToCoroutine_t617332884 * G_B10_0 = NULL;
	U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * G_B10_1 = NULL;
	Il2CppObject* G_B13_0 = NULL;
	U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * G_B13_1 = NULL;
	Il2CppObject* G_B12_0 = NULL;
	U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * G_B12_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002d;
		}
		if (L_1 == 1)
		{
			goto IL_0057;
		}
		if (L_1 == 2)
		{
			goto IL_0086;
		}
		if (L_1 == 3)
		{
			goto IL_00b6;
		}
		if (L_1 == 4)
		{
			goto IL_015d;
		}
	}
	{
		goto IL_0164;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_2 = TimeSpan_FromSeconds_m1904297940(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_3 = Observable_Timer_m618408927(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		ObservableYieldInstruction_1_t674360470 * L_4 = Observable_ToYieldInstruction_TisInt64_t2847414882_m1001181553(NULL /*static, unused*/, L_3, /*hidden argument*/Observable_ToYieldInstruction_TisInt64_t2847414882_m1001181553_MethodInfo_var);
		__this->set_U24current_2(L_4);
		__this->set_U24PC_1(1);
		goto IL_0166;
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_5 = TimeSpan_FromSeconds_m1904297940(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = Scheduler_get_MainThreadIgnoreTimeScale_m3212577356(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_7 = Observable_Timer_m2482108829(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		ObservableYieldInstruction_1_t674360470 * L_8 = Observable_ToYieldInstruction_TisInt64_t2847414882_m1001181553(NULL /*static, unused*/, L_7, /*hidden argument*/Observable_ToYieldInstruction_TisInt64_t2847414882_m1001181553_MethodInfo_var);
		__this->set_U24current_2(L_8);
		__this->set_U24PC_1(2);
		goto IL_0166;
	}

IL_0086:
	{
		Il2CppObject* L_9 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral2849422856, (Dictionary_2_t2606186806 *)NULL, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		ObservableYieldInstruction_1_t3090401786 * L_10 = Observable_ToYieldInstruction_TisString_t_m2732942552(NULL /*static, unused*/, L_9, (bool)0, /*hidden argument*/Observable_ToYieldInstruction_TisString_t_m2732942552_MethodInfo_var);
		__this->set_U3CoU3E__0_0(L_10);
		ObservableYieldInstruction_1_t3090401786 * L_11 = __this->get_U3CoU3E__0_0();
		__this->set_U24current_2(L_11);
		__this->set_U24PC_1(3);
		goto IL_0166;
	}

IL_00b6:
	{
		ObservableYieldInstruction_1_t3090401786 * L_12 = __this->get_U3CoU3E__0_0();
		NullCheck(L_12);
		bool L_13 = ObservableYieldInstruction_1_get_HasError_m2772237829(L_12, /*hidden argument*/ObservableYieldInstruction_1_get_HasError_m2772237829_MethodInfo_var);
		if (!L_13)
		{
			goto IL_00db;
		}
	}
	{
		ObservableYieldInstruction_1_t3090401786 * L_14 = __this->get_U3CoU3E__0_0();
		NullCheck(L_14);
		Exception_t1967233988 * L_15 = ObservableYieldInstruction_1_get_Error_m2578890956(L_14, /*hidden argument*/ObservableYieldInstruction_1_get_Error_m2578890956_MethodInfo_var);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_00db:
	{
		ObservableYieldInstruction_1_t3090401786 * L_17 = __this->get_U3CoU3E__0_0();
		NullCheck(L_17);
		bool L_18 = ObservableYieldInstruction_1_get_HasResult_m2603143266(L_17, /*hidden argument*/ObservableYieldInstruction_1_get_HasResult_m2603143266_MethodInfo_var);
		if (!L_18)
		{
			goto IL_00fb;
		}
	}
	{
		ObservableYieldInstruction_1_t3090401786 * L_19 = __this->get_U3CoU3E__0_0();
		NullCheck(L_19);
		String_t* L_20 = ObservableYieldInstruction_1_get_Result_m722695953(L_19, /*hidden argument*/ObservableYieldInstruction_1_get_Result_m722695953_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
	}

IL_00fb:
	{
		Sample06_ConvertToCoroutine_t617332884 * L_21 = __this->get_U3CU3Ef__this_3();
		Func_2_t140622909 * L_22 = ((U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields*)U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_4();
		G_B10_0 = L_21;
		G_B10_1 = __this;
		if (L_22)
		{
			G_B11_0 = L_21;
			G_B11_1 = __this;
			goto IL_011a;
		}
	}
	{
		IntPtr_t L_23;
		L_23.set_m_value_0((void*)(void*)U3CTestNewCustomYieldInstructionU3Ec__Iterator9_U3CU3Em__1F_m2124214084_MethodInfo_var);
		Func_2_t140622909 * L_24 = (Func_2_t140622909 *)il2cpp_codegen_object_new(Func_2_t140622909_il2cpp_TypeInfo_var);
		Func_2__ctor_m3998431533(L_24, NULL, L_23, /*hidden argument*/Func_2__ctor_m3998431533_MethodInfo_var);
		((U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields*)U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_4(L_24);
		G_B11_0 = G_B10_0;
		G_B11_1 = G_B10_1;
	}

IL_011a:
	{
		Func_2_t140622909 * L_25 = ((U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields*)U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_4();
		Il2CppObject* L_26 = ObserveExtensions_ObserveEveryValueChanged_TisSample06_ConvertToCoroutine_t617332884_TisTransform_t284553113_m3540279691(NULL /*static, unused*/, G_B11_0, L_25, 0, /*hidden argument*/ObserveExtensions_ObserveEveryValueChanged_TisSample06_ConvertToCoroutine_t617332884_TisTransform_t284553113_m3540279691_MethodInfo_var);
		Func_2_t884531080 * L_27 = ((U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields*)U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_5();
		G_B12_0 = L_26;
		G_B12_1 = G_B11_1;
		if (L_27)
		{
			G_B13_0 = L_26;
			G_B13_1 = G_B11_1;
			goto IL_013d;
		}
	}
	{
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)U3CTestNewCustomYieldInstructionU3Ec__Iterator9_U3CU3Em__20_m4040796226_MethodInfo_var);
		Func_2_t884531080 * L_29 = (Func_2_t884531080 *)il2cpp_codegen_object_new(Func_2_t884531080_il2cpp_TypeInfo_var);
		Func_2__ctor_m642311202(L_29, NULL, L_28, /*hidden argument*/Func_2__ctor_m642311202_MethodInfo_var);
		((U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields*)U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_5(L_29);
		G_B13_0 = G_B12_0;
		G_B13_1 = G_B12_1;
	}

IL_013d:
	{
		Func_2_t884531080 * L_30 = ((U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_StaticFields*)U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_5();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_31 = Observable_FirstOrDefault_TisTransform_t284553113_m2238600158(NULL /*static, unused*/, G_B13_0, L_30, /*hidden argument*/Observable_FirstOrDefault_TisTransform_t284553113_m2238600158_MethodInfo_var);
		ObservableYieldInstruction_1_t2406465997 * L_32 = Observable_ToYieldInstruction_TisTransform_t284553113_m127016576(NULL /*static, unused*/, L_31, /*hidden argument*/Observable_ToYieldInstruction_TisTransform_t284553113_m127016576_MethodInfo_var);
		NullCheck(G_B13_1);
		G_B13_1->set_U24current_2(L_32);
		__this->set_U24PC_1(4);
		goto IL_0166;
	}

IL_015d:
	{
		__this->set_U24PC_1((-1));
	}

IL_0164:
	{
		return (bool)0;
	}

IL_0166:
	{
		return (bool)1;
	}
	// Dead block : IL_0168: ldloc.1
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::Dispose()
extern "C"  void U3CTestNewCustomYieldInstructionU3Ec__Iterator9_Dispose_m3702962861 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CTestNewCustomYieldInstructionU3Ec__Iterator9_Reset_m3116307741_MetadataUsageId;
extern "C"  void U3CTestNewCustomYieldInstructionU3Ec__Iterator9_Reset_m3116307741 (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_Reset_m3116307741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// UnityEngine.Transform UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::<>m__1F(UniRx.Examples.Sample06_ConvertToCoroutine)
extern "C"  Transform_t284553113 * U3CTestNewCustomYieldInstructionU3Ec__Iterator9_U3CU3Em__1F_m2124214084 (Il2CppObject * __this /* static, unused */, Sample06_ConvertToCoroutine_t617332884 * ___x0, const MethodInfo* method)
{
	{
		Sample06_ConvertToCoroutine_t617332884 * L_0 = ___x0;
		NullCheck(L_0);
		Transform_t284553113 * L_1 = Component_get_transform_m2452535634(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator9::<>m__20(UnityEngine.Transform)
extern "C"  bool U3CTestNewCustomYieldInstructionU3Ec__Iterator9_U3CU3Em__20_m4040796226 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___x0, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = ___x0;
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m3716760760(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		return (bool)((((int32_t)((!(((float)L_2) >= ((float)(100.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator::.ctor()
extern "C"  void Sample07_OrchestratIEnumerator__ctor_m2899177426 (Sample07_OrchestratIEnumerator_t3702211539 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Examples.Sample07_OrchestratIEnumerator::AsyncA()
extern Il2CppClass* U3CAsyncAU3Ec__IteratorA_t2615160205_il2cpp_TypeInfo_var;
extern const uint32_t Sample07_OrchestratIEnumerator_AsyncA_m3572713415_MetadataUsageId;
extern "C"  Il2CppObject * Sample07_OrchestratIEnumerator_AsyncA_m3572713415 (Sample07_OrchestratIEnumerator_t3702211539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample07_OrchestratIEnumerator_AsyncA_m3572713415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsyncAU3Ec__IteratorA_t2615160205 * V_0 = NULL;
	{
		U3CAsyncAU3Ec__IteratorA_t2615160205 * L_0 = (U3CAsyncAU3Ec__IteratorA_t2615160205 *)il2cpp_codegen_object_new(U3CAsyncAU3Ec__IteratorA_t2615160205_il2cpp_TypeInfo_var);
		U3CAsyncAU3Ec__IteratorA__ctor_m2825608152(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAsyncAU3Ec__IteratorA_t2615160205 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator UniRx.Examples.Sample07_OrchestratIEnumerator::AsyncB()
extern Il2CppClass* U3CAsyncBU3Ec__IteratorB_t2109601581_il2cpp_TypeInfo_var;
extern const uint32_t Sample07_OrchestratIEnumerator_AsyncB_m3572714376_MetadataUsageId;
extern "C"  Il2CppObject * Sample07_OrchestratIEnumerator_AsyncB_m3572714376 (Sample07_OrchestratIEnumerator_t3702211539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample07_OrchestratIEnumerator_AsyncB_m3572714376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsyncBU3Ec__IteratorB_t2109601581 * V_0 = NULL;
	{
		U3CAsyncBU3Ec__IteratorB_t2109601581 * L_0 = (U3CAsyncBU3Ec__IteratorB_t2109601581 *)il2cpp_codegen_object_new(U3CAsyncBU3Ec__IteratorB_t2109601581_il2cpp_TypeInfo_var);
		U3CAsyncBU3Ec__IteratorB__ctor_m3873859128(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAsyncBU3Ec__IteratorB_t2109601581 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator::Start()
extern Il2CppClass* Func_1_t1429988286_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Sample07_OrchestratIEnumerator_AsyncA_m3572713415_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m4104442892_MethodInfo_var;
extern const MethodInfo* Sample07_OrchestratIEnumerator_AsyncB_m3572714376_MethodInfo_var;
extern const MethodInfo* Observable_SelectMany_TisUnit_t2558286038_m761892958_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022_MethodInfo_var;
extern const uint32_t Sample07_OrchestratIEnumerator_Start_m1846315218_MetadataUsageId;
extern "C"  void Sample07_OrchestratIEnumerator_Start_m1846315218 (Sample07_OrchestratIEnumerator_t3702211539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample07_OrchestratIEnumerator_Start_m1846315218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)Sample07_OrchestratIEnumerator_AsyncA_m3572713415_MethodInfo_var);
		Func_1_t1429988286 * L_1 = (Func_1_t1429988286 *)il2cpp_codegen_object_new(Func_1_t1429988286_il2cpp_TypeInfo_var);
		Func_1__ctor_m4104442892(L_1, __this, L_0, /*hidden argument*/Func_1__ctor_m4104442892_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = Observable_FromCoroutine_m3913040964(NULL /*static, unused*/, L_1, (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)Sample07_OrchestratIEnumerator_AsyncB_m3572714376_MethodInfo_var);
		Func_1_t1429988286 * L_4 = (Func_1_t1429988286 *)il2cpp_codegen_object_new(Func_1_t1429988286_il2cpp_TypeInfo_var);
		Func_1__ctor_m4104442892(L_4, __this, L_3, /*hidden argument*/Func_1__ctor_m4104442892_MethodInfo_var);
		Il2CppObject* L_5 = Observable_SelectMany_TisUnit_t2558286038_m761892958(NULL /*static, unused*/, L_2, L_4, (bool)0, /*hidden argument*/Observable_SelectMany_TisUnit_t2558286038_m761892958_MethodInfo_var);
		Il2CppObject * L_6 = ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022(NULL /*static, unused*/, L_5, /*hidden argument*/ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022_MethodInfo_var);
		V_0 = L_6;
		return;
	}
}
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::.ctor()
extern "C"  void U3CAsyncAU3Ec__IteratorA__ctor_m2825608152 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAsyncAU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3934729412 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsyncAU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3080929368 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::MoveNext()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1214401507;
extern Il2CppCodeGenString* _stringLiteral90635420;
extern const uint32_t U3CAsyncAU3Ec__IteratorA_MoveNext_m3533997804_MetadataUsageId;
extern "C"  bool U3CAsyncAU3Ec__IteratorA_MoveNext_m3533997804 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsyncAU3Ec__IteratorA_MoveNext_m3533997804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0058;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral1214401507, /*hidden argument*/NULL);
		WaitForSeconds_t1291133240 * L_2 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_005a;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral90635420, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0058:
	{
		return (bool)0;
	}

IL_005a:
	{
		return (bool)1;
	}
	// Dead block : IL_005c: ldloc.1
}
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::Dispose()
extern "C"  void U3CAsyncAU3Ec__IteratorA_Dispose_m888386069 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__IteratorA::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsyncAU3Ec__IteratorA_Reset_m472041093_MetadataUsageId;
extern "C"  void U3CAsyncAU3Ec__IteratorA_Reset_m472041093 (U3CAsyncAU3Ec__IteratorA_t2615160205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsyncAU3Ec__IteratorA_Reset_m472041093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::.ctor()
extern "C"  void U3CAsyncBU3Ec__IteratorB__ctor_m3873859128 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAsyncBU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m539879012 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsyncBU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m857813496 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::MoveNext()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForEndOfFrame_t1917318876_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2101905188;
extern Il2CppCodeGenString* _stringLiteral91558941;
extern const uint32_t U3CAsyncBU3Ec__IteratorB_MoveNext_m3271614604_MetadataUsageId;
extern "C"  bool U3CAsyncBU3Ec__IteratorB_MoveNext_m3271614604 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsyncBU3Ec__IteratorB_MoveNext_m3271614604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0053;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral2101905188, /*hidden argument*/NULL);
		WaitForEndOfFrame_t1917318876 * L_2 = (WaitForEndOfFrame_t1917318876 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1917318876_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m4124201226(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0055;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral91558941, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0053:
	{
		return (bool)0;
	}

IL_0055:
	{
		return (bool)1;
	}
	// Dead block : IL_0057: ldloc.1
}
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::Dispose()
extern "C"  void U3CAsyncBU3Ec__IteratorB_Dispose_m3235226741 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsyncBU3Ec__IteratorB_Reset_m1520292069_MetadataUsageId;
extern "C"  void U3CAsyncBU3Ec__IteratorB_Reset_m1520292069 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsyncBU3Ec__IteratorB_Reset_m1520292069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Examples.Sample08_DetectDoubleClick::.ctor()
extern "C"  void Sample08_DetectDoubleClick__ctor_m778062339 (Sample08_DetectDoubleClick_t1711438338 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample08_DetectDoubleClick::Start()
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2251336571_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2964252473_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t867392605_il2cpp_TypeInfo_var;
extern const MethodInfo* Sample08_DetectDoubleClick_U3CStartU3Em__21_m3995005579_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2519360593_MethodInfo_var;
extern const MethodInfo* Observable_Where_TisInt64_t2847414882_m2960877922_MethodInfo_var;
extern const MethodInfo* Observable_Throttle_TisInt64_t2847414882_m3662844575_MethodInfo_var;
extern const MethodInfo* Observable_Buffer_TisInt64_t2847414882_TisInt64_t2847414882_m817331864_MethodInfo_var;
extern const MethodInfo* Sample08_DetectDoubleClick_U3CStartU3Em__22_m691380157_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m429916096_MethodInfo_var;
extern const MethodInfo* Observable_Where_TisIList_1_t718939900_m178084691_MethodInfo_var;
extern const MethodInfo* Sample08_DetectDoubleClick_U3CStartU3Em__23_m3792214938_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3199554870_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisIList_1_t718939900_m1844099541_MethodInfo_var;
extern const uint32_t Sample08_DetectDoubleClick_Start_m4020167427_MetadataUsageId;
extern "C"  void Sample08_DetectDoubleClick_Start_m4020167427 (Sample08_DetectDoubleClick_t1711438338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample08_DetectDoubleClick_Start_m4020167427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	Il2CppObject* G_B6_0 = NULL;
	Il2CppObject* G_B5_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = Observable_EveryUpdate_m1588154268(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t2251336571 * L_1 = ((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Sample08_DetectDoubleClick_U3CStartU3Em__21_m3995005579_MethodInfo_var);
		Func_2_t2251336571 * L_3 = (Func_2_t2251336571 *)il2cpp_codegen_object_new(Func_2_t2251336571_il2cpp_TypeInfo_var);
		Func_2__ctor_m2519360593(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2519360593_MethodInfo_var);
		((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001d:
	{
		Func_2_t2251336571 * L_4 = ((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_5 = Observable_Where_TisInt64_t2847414882_m2960877922(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Observable_Where_TisInt64_t2847414882_m2960877922_MethodInfo_var);
		V_0 = L_5;
		Il2CppObject* L_6 = V_0;
		Il2CppObject* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_8 = TimeSpan_FromMilliseconds_m1386660477(NULL /*static, unused*/, (250.0), /*hidden argument*/NULL);
		Il2CppObject* L_9 = Observable_Throttle_TisInt64_t2847414882_m3662844575(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/Observable_Throttle_TisInt64_t2847414882_m3662844575_MethodInfo_var);
		Il2CppObject* L_10 = Observable_Buffer_TisInt64_t2847414882_TisInt64_t2847414882_m817331864(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/Observable_Buffer_TisInt64_t2847414882_TisInt64_t2847414882_m817331864_MethodInfo_var);
		Func_2_t2964252473 * L_11 = ((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		G_B3_0 = L_10;
		if (L_11)
		{
			G_B4_0 = L_10;
			goto IL_005a;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)Sample08_DetectDoubleClick_U3CStartU3Em__22_m691380157_MethodInfo_var);
		Func_2_t2964252473 * L_13 = (Func_2_t2964252473 *)il2cpp_codegen_object_new(Func_2_t2964252473_il2cpp_TypeInfo_var);
		Func_2__ctor_m429916096(L_13, NULL, L_12, /*hidden argument*/Func_2__ctor_m429916096_MethodInfo_var);
		((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_3(L_13);
		G_B4_0 = G_B3_0;
	}

IL_005a:
	{
		Func_2_t2964252473 * L_14 = ((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_15 = Observable_Where_TisIList_1_t718939900_m178084691(NULL /*static, unused*/, G_B4_0, L_14, /*hidden argument*/Observable_Where_TisIList_1_t718939900_m178084691_MethodInfo_var);
		Action_1_t867392605 * L_16 = ((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		G_B5_0 = L_15;
		if (L_16)
		{
			G_B6_0 = L_15;
			goto IL_007c;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)Sample08_DetectDoubleClick_U3CStartU3Em__23_m3792214938_MethodInfo_var);
		Action_1_t867392605 * L_18 = (Action_1_t867392605 *)il2cpp_codegen_object_new(Action_1_t867392605_il2cpp_TypeInfo_var);
		Action_1__ctor_m3199554870(L_18, NULL, L_17, /*hidden argument*/Action_1__ctor_m3199554870_MethodInfo_var);
		((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_4(L_18);
		G_B6_0 = G_B5_0;
	}

IL_007c:
	{
		Action_1_t867392605 * L_19 = ((Sample08_DetectDoubleClick_t1711438338_StaticFields*)Sample08_DetectDoubleClick_t1711438338_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		ObservableExtensions_Subscribe_TisIList_1_t718939900_m1844099541(NULL /*static, unused*/, G_B6_0, L_19, /*hidden argument*/ObservableExtensions_Subscribe_TisIList_1_t718939900_m1844099541_MethodInfo_var);
		return;
	}
}
// System.Boolean UniRx.Examples.Sample08_DetectDoubleClick::<Start>m__21(System.Int64)
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t Sample08_DetectDoubleClick_U3CStartU3Em__21_m3995005579_MetadataUsageId;
extern "C"  bool Sample08_DetectDoubleClick_U3CStartU3Em__21_m3995005579 (Il2CppObject * __this /* static, unused */, int64_t ____0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample08_DetectDoubleClick_U3CStartU3Em__21_m3995005579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m3552475078(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UniRx.Examples.Sample08_DetectDoubleClick::<Start>m__22(System.Collections.Generic.IList`1<System.Int64>)
extern Il2CppClass* ICollection_1_t3313246268_il2cpp_TypeInfo_var;
extern const uint32_t Sample08_DetectDoubleClick_U3CStartU3Em__22_m691380157_MetadataUsageId;
extern "C"  bool Sample08_DetectDoubleClick_U3CStartU3Em__22_m691380157 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___xs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample08_DetectDoubleClick_U3CStartU3Em__22_m691380157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___xs0;
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int64>::get_Count() */, ICollection_1_t3313246268_il2cpp_TypeInfo_var, L_0);
		return (bool)((((int32_t)((((int32_t)L_1) < ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UniRx.Examples.Sample08_DetectDoubleClick::<Start>m__23(System.Collections.Generic.IList`1<System.Int64>)
extern Il2CppClass* ICollection_1_t3313246268_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral800553877;
extern const uint32_t Sample08_DetectDoubleClick_U3CStartU3Em__23_m3792214938_MetadataUsageId;
extern "C"  void Sample08_DetectDoubleClick_U3CStartU3Em__23_m3792214938 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___xs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample08_DetectDoubleClick_U3CStartU3Em__23_m3792214938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___xs0;
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int64>::get_Count() */, ICollection_1_t3313246268_il2cpp_TypeInfo_var, L_0);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral800553877, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling::.ctor()
extern Il2CppClass* CompositeDisposable_t1894629977_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t490482111_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1321103487_MethodInfo_var;
extern const uint32_t Sample09_EventHandling__ctor_m3366617911_MetadataUsageId;
extern "C"  void Sample09_EventHandling__ctor_m3366617911 (Sample09_EventHandling_t2113606094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample09_EventHandling__ctor_m3366617911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CompositeDisposable_t1894629977 * L_0 = (CompositeDisposable_t1894629977 *)il2cpp_codegen_object_new(CompositeDisposable_t1894629977_il2cpp_TypeInfo_var);
		CompositeDisposable__ctor_m1237994920(L_0, /*hidden argument*/NULL);
		__this->set_disposables_2(L_0);
		Subject_1_t490482111 * L_1 = (Subject_1_t490482111 *)il2cpp_codegen_object_new(Subject_1_t490482111_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1321103487(L_1, /*hidden argument*/Subject_1__ctor_m1321103487_MethodInfo_var);
		__this->set_onBarBar_3(L_1);
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling::add_FooBar(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const uint32_t Sample09_EventHandling_add_FooBar_m1259845978_MetadataUsageId;
extern "C"  void Sample09_EventHandling_add_FooBar_m1259845978 (Sample09_EventHandling_t2113606094 * __this, EventHandler_1_t3942771990 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample09_EventHandling_add_FooBar_m1259845978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t3942771990 * L_0 = __this->get_FooBar_4();
		EventHandler_1_t3942771990 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_FooBar_4(((EventHandler_1_t3942771990 *)CastclassSealed(L_2, EventHandler_1_t3942771990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling::remove_FooBar(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const uint32_t Sample09_EventHandling_remove_FooBar_m3925468735_MetadataUsageId;
extern "C"  void Sample09_EventHandling_remove_FooBar_m3925468735 (Sample09_EventHandling_t2113606094 * __this, EventHandler_1_t3942771990 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample09_EventHandling_remove_FooBar_m3925468735_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t3942771990 * L_0 = __this->get_FooBar_4();
		EventHandler_1_t3942771990 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_FooBar_4(((EventHandler_1_t3942771990 *)CastclassSealed(L_2, EventHandler_1_t3942771990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling::add_FooFoo(System.Action`1<System.Int32>)
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t Sample09_EventHandling_add_FooFoo_m912362574_MetadataUsageId;
extern "C"  void Sample09_EventHandling_add_FooFoo_m912362574 (Sample09_EventHandling_t2113606094 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample09_EventHandling_add_FooFoo_m912362574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2995867492 * L_0 = __this->get_FooFoo_5();
		Action_1_t2995867492 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_FooFoo_5(((Action_1_t2995867492 *)CastclassSealed(L_2, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling::remove_FooFoo(System.Action`1<System.Int32>)
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t Sample09_EventHandling_remove_FooFoo_m1379018889_MetadataUsageId;
extern "C"  void Sample09_EventHandling_remove_FooFoo_m1379018889 (Sample09_EventHandling_t2113606094 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample09_EventHandling_remove_FooFoo_m1379018889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2995867492 * L_0 = __this->get_FooFoo_5();
		Action_1_t2995867492 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_FooFoo_5(((Action_1_t2995867492 *)CastclassSealed(L_2, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// UniRx.IObservable`1<System.Int32> UniRx.Examples.Sample09_EventHandling::get_OnBarBar()
extern "C"  Il2CppObject* Sample09_EventHandling_get_OnBarBar_m2982259802 (Sample09_EventHandling_t2113606094 * __this, const MethodInfo* method)
{
	{
		Subject_1_t490482111 * L_0 = __this->get_onBarBar_3();
		return L_0;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling::Start()
extern Il2CppClass* U3CStartU3Ec__AnonStorey32_t1152509870_il2cpp_TypeInfo_var;
extern Il2CppClass* Sample09_EventHandling_t2113606094_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1776554224_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t4091224695_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t4235389922_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3144320197_il2cpp_TypeInfo_var;
extern const MethodInfo* Sample09_EventHandling_U3CStartU3Em__24_m1633895583_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1718635948_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__25_m1219274304_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3310913301_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__26_m2086326529_MethodInfo_var;
extern const MethodInfo* Observable_FromEventPattern_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m2912073386_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisEventPattern_1_t871124609_m1009340427_MethodInfo_var;
extern const MethodInfo* DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125_MethodInfo_var;
extern const MethodInfo* Sample09_EventHandling_U3CStartU3Em__27_m3241598920_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3856827910_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__28_m3820430979_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__29_m392515908_MethodInfo_var;
extern const MethodInfo* Observable_FromEvent_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m4177120813_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisMyEventArgs_t3100194347_m2726699918_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__2A_m3630825647_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3183528238_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__2B_m1620721806_MethodInfo_var;
extern const MethodInfo* Observable_FromEvent_TisInt32_t2847414787_m521195233_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisInt32_t2847414787_m2600462747_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__2C_m1001324839_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__2D_m1340155791_MethodInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey32_U3CU3Em__2E_m2207208016_MethodInfo_var;
extern const MethodInfo* Subject_1_OnNext_m1100834663_MethodInfo_var;
extern const uint32_t Sample09_EventHandling_Start_m2313755703_MetadataUsageId;
extern "C"  void Sample09_EventHandling_Start_m2313755703 (Sample09_EventHandling_t2113606094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample09_EventHandling_Start_m2313755703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__AnonStorey32_t1152509870 * V_0 = NULL;
	{
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_0 = (U3CStartU3Ec__AnonStorey32_t1152509870 *)il2cpp_codegen_object_new(U3CStartU3Ec__AnonStorey32_t1152509870_il2cpp_TypeInfo_var);
		U3CStartU3Ec__AnonStorey32__ctor_m2620545362(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		Func_2_t1776554224 * L_2 = ((Sample09_EventHandling_t2113606094_StaticFields*)Sample09_EventHandling_t2113606094_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_6();
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)Sample09_EventHandling_U3CStartU3Em__24_m1633895583_MethodInfo_var);
		Func_2_t1776554224 * L_4 = (Func_2_t1776554224 *)il2cpp_codegen_object_new(Func_2_t1776554224_il2cpp_TypeInfo_var);
		Func_2__ctor_m1718635948(L_4, NULL, L_3, /*hidden argument*/Func_2__ctor_m1718635948_MethodInfo_var);
		((Sample09_EventHandling_t2113606094_StaticFields*)Sample09_EventHandling_t2113606094_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_6(L_4);
	}

IL_0025:
	{
		Func_2_t1776554224 * L_5 = ((Sample09_EventHandling_t2113606094_StaticFields*)Sample09_EventHandling_t2113606094_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_6();
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__25_m1219274304_MethodInfo_var);
		Action_1_t4091224695 * L_8 = (Action_1_t4091224695 *)il2cpp_codegen_object_new(Action_1_t4091224695_il2cpp_TypeInfo_var);
		Action_1__ctor_m3310913301(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m3310913301_MethodInfo_var);
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__26_m2086326529_MethodInfo_var);
		Action_1_t4091224695 * L_11 = (Action_1_t4091224695 *)il2cpp_codegen_object_new(Action_1_t4091224695_il2cpp_TypeInfo_var);
		Action_1__ctor_m3310913301(L_11, L_9, L_10, /*hidden argument*/Action_1__ctor_m3310913301_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = Observable_FromEventPattern_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m2912073386(NULL /*static, unused*/, L_5, L_8, L_11, /*hidden argument*/Observable_FromEventPattern_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m2912073386_MethodInfo_var);
		Il2CppObject * L_13 = ObservableExtensions_Subscribe_TisEventPattern_1_t871124609_m1009340427(NULL /*static, unused*/, L_12, /*hidden argument*/ObservableExtensions_Subscribe_TisEventPattern_1_t871124609_m1009340427_MethodInfo_var);
		CompositeDisposable_t1894629977 * L_14 = __this->get_disposables_2();
		DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125_MethodInfo_var);
		Func_2_t4235389922 * L_15 = ((Sample09_EventHandling_t2113606094_StaticFields*)Sample09_EventHandling_t2113606094_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_7();
		if (L_15)
		{
			goto IL_0070;
		}
	}
	{
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)Sample09_EventHandling_U3CStartU3Em__27_m3241598920_MethodInfo_var);
		Func_2_t4235389922 * L_17 = (Func_2_t4235389922 *)il2cpp_codegen_object_new(Func_2_t4235389922_il2cpp_TypeInfo_var);
		Func_2__ctor_m3856827910(L_17, NULL, L_16, /*hidden argument*/Func_2__ctor_m3856827910_MethodInfo_var);
		((Sample09_EventHandling_t2113606094_StaticFields*)Sample09_EventHandling_t2113606094_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_7(L_17);
	}

IL_0070:
	{
		Func_2_t4235389922 * L_18 = ((Sample09_EventHandling_t2113606094_StaticFields*)Sample09_EventHandling_t2113606094_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_7();
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_19 = V_0;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__28_m3820430979_MethodInfo_var);
		Action_1_t4091224695 * L_21 = (Action_1_t4091224695 *)il2cpp_codegen_object_new(Action_1_t4091224695_il2cpp_TypeInfo_var);
		Action_1__ctor_m3310913301(L_21, L_19, L_20, /*hidden argument*/Action_1__ctor_m3310913301_MethodInfo_var);
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_22 = V_0;
		IntPtr_t L_23;
		L_23.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__29_m392515908_MethodInfo_var);
		Action_1_t4091224695 * L_24 = (Action_1_t4091224695 *)il2cpp_codegen_object_new(Action_1_t4091224695_il2cpp_TypeInfo_var);
		Action_1__ctor_m3310913301(L_24, L_22, L_23, /*hidden argument*/Action_1__ctor_m3310913301_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_25 = Observable_FromEvent_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m4177120813(NULL /*static, unused*/, L_18, L_21, L_24, /*hidden argument*/Observable_FromEvent_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m4177120813_MethodInfo_var);
		Il2CppObject * L_26 = ObservableExtensions_Subscribe_TisMyEventArgs_t3100194347_m2726699918(NULL /*static, unused*/, L_25, /*hidden argument*/ObservableExtensions_Subscribe_TisMyEventArgs_t3100194347_m2726699918_MethodInfo_var);
		CompositeDisposable_t1894629977 * L_27 = __this->get_disposables_2();
		DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125_MethodInfo_var);
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_28 = V_0;
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__2A_m3630825647_MethodInfo_var);
		Action_1_t3144320197 * L_30 = (Action_1_t3144320197 *)il2cpp_codegen_object_new(Action_1_t3144320197_il2cpp_TypeInfo_var);
		Action_1__ctor_m3183528238(L_30, L_28, L_29, /*hidden argument*/Action_1__ctor_m3183528238_MethodInfo_var);
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_31 = V_0;
		IntPtr_t L_32;
		L_32.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__2B_m1620721806_MethodInfo_var);
		Action_1_t3144320197 * L_33 = (Action_1_t3144320197 *)il2cpp_codegen_object_new(Action_1_t3144320197_il2cpp_TypeInfo_var);
		Action_1__ctor_m3183528238(L_33, L_31, L_32, /*hidden argument*/Action_1__ctor_m3183528238_MethodInfo_var);
		Il2CppObject* L_34 = Observable_FromEvent_TisInt32_t2847414787_m521195233(NULL /*static, unused*/, L_30, L_33, /*hidden argument*/Observable_FromEvent_TisInt32_t2847414787_m521195233_MethodInfo_var);
		Il2CppObject * L_35 = ObservableExtensions_Subscribe_TisInt32_t2847414787_m2600462747(NULL /*static, unused*/, L_34, /*hidden argument*/ObservableExtensions_Subscribe_TisInt32_t2847414787_m2600462747_MethodInfo_var);
		CompositeDisposable_t1894629977 * L_36 = __this->get_disposables_2();
		DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125_MethodInfo_var);
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_37 = V_0;
		NullCheck(L_37);
		L_37->set_capture_0(0);
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_38 = V_0;
		IntPtr_t L_39;
		L_39.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__2C_m1001324839_MethodInfo_var);
		Func_2_t1776554224 * L_40 = (Func_2_t1776554224 *)il2cpp_codegen_object_new(Func_2_t1776554224_il2cpp_TypeInfo_var);
		Func_2__ctor_m1718635948(L_40, L_38, L_39, /*hidden argument*/Func_2__ctor_m1718635948_MethodInfo_var);
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_41 = V_0;
		IntPtr_t L_42;
		L_42.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__2D_m1340155791_MethodInfo_var);
		Action_1_t4091224695 * L_43 = (Action_1_t4091224695 *)il2cpp_codegen_object_new(Action_1_t4091224695_il2cpp_TypeInfo_var);
		Action_1__ctor_m3310913301(L_43, L_41, L_42, /*hidden argument*/Action_1__ctor_m3310913301_MethodInfo_var);
		U3CStartU3Ec__AnonStorey32_t1152509870 * L_44 = V_0;
		IntPtr_t L_45;
		L_45.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey32_U3CU3Em__2E_m2207208016_MethodInfo_var);
		Action_1_t4091224695 * L_46 = (Action_1_t4091224695 *)il2cpp_codegen_object_new(Action_1_t4091224695_il2cpp_TypeInfo_var);
		Action_1__ctor_m3310913301(L_46, L_44, L_45, /*hidden argument*/Action_1__ctor_m3310913301_MethodInfo_var);
		Il2CppObject* L_47 = Observable_FromEventPattern_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m2912073386(NULL /*static, unused*/, L_40, L_43, L_46, /*hidden argument*/Observable_FromEventPattern_TisEventHandler_1_t3942771990_TisMyEventArgs_t3100194347_m2912073386_MethodInfo_var);
		Il2CppObject * L_48 = ObservableExtensions_Subscribe_TisEventPattern_1_t871124609_m1009340427(NULL /*static, unused*/, L_47, /*hidden argument*/ObservableExtensions_Subscribe_TisEventPattern_1_t871124609_m1009340427_MethodInfo_var);
		CompositeDisposable_t1894629977 * L_49 = __this->get_disposables_2();
		DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125_MethodInfo_var);
		Il2CppObject* L_50 = Sample09_EventHandling_get_OnBarBar_m2982259802(__this, /*hidden argument*/NULL);
		Il2CppObject * L_51 = ObservableExtensions_Subscribe_TisInt32_t2847414787_m2600462747(NULL /*static, unused*/, L_50, /*hidden argument*/ObservableExtensions_Subscribe_TisInt32_t2847414787_m2600462747_MethodInfo_var);
		CompositeDisposable_t1894629977 * L_52 = __this->get_disposables_2();
		DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/DisposableExtensions_AddTo_TisIDisposable_t1628921374_m2556983125_MethodInfo_var);
		Subject_1_t490482111 * L_53 = __this->get_onBarBar_3();
		NullCheck(L_53);
		Subject_1_OnNext_m1100834663(L_53, 1, /*hidden argument*/Subject_1_OnNext_m1100834663_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling::OnDestroy()
extern "C"  void Sample09_EventHandling_OnDestroy_m747234480 (Sample09_EventHandling_t2113606094 * __this, const MethodInfo* method)
{
	{
		CompositeDisposable_t1894629977 * L_0 = __this->get_disposables_2();
		NullCheck(L_0);
		CompositeDisposable_Dispose_m4200427493(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling::<Start>m__24(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m2154207886_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m2372365329_MethodInfo_var;
extern const uint32_t Sample09_EventHandling_U3CStartU3Em__24_m1633895583_MetadataUsageId;
extern "C"  EventHandler_1_t3942771990 * Sample09_EventHandling_U3CStartU3Em__24_m1633895583 (Il2CppObject * __this /* static, unused */, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample09_EventHandling_U3CStartU3Em__24_m1633895583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t3942771990 * L_0 = ___h0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)EventHandler_1_Invoke_m2154207886_MethodInfo_var);
		EventHandler_1_t3942771990 * L_2 = (EventHandler_1_t3942771990 *)il2cpp_codegen_object_new(EventHandler_1_t3942771990_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m2372365329(L_2, L_0, L_1, /*hidden argument*/EventHandler_1__ctor_m2372365329_MethodInfo_var);
		return L_2;
	}
}
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling::<Start>m__27(System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* U3CStartU3Ec__AnonStorey31_t1152509869_il2cpp_TypeInfo_var;
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CStartU3Ec__AnonStorey31_U3CU3Em__2F_m2705133908_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m2372365329_MethodInfo_var;
extern const uint32_t Sample09_EventHandling_U3CStartU3Em__27_m3241598920_MetadataUsageId;
extern "C"  EventHandler_1_t3942771990 * Sample09_EventHandling_U3CStartU3Em__27_m3241598920 (Il2CppObject * __this /* static, unused */, Action_1_t3248647052 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample09_EventHandling_U3CStartU3Em__27_m3241598920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__AnonStorey31_t1152509869 * V_0 = NULL;
	{
		U3CStartU3Ec__AnonStorey31_t1152509869 * L_0 = (U3CStartU3Ec__AnonStorey31_t1152509869 *)il2cpp_codegen_object_new(U3CStartU3Ec__AnonStorey31_t1152509869_il2cpp_TypeInfo_var);
		U3CStartU3Ec__AnonStorey31__ctor_m2817058867(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__AnonStorey31_t1152509869 * L_1 = V_0;
		Action_1_t3248647052 * L_2 = ___h0;
		NullCheck(L_1);
		L_1->set_h_0(L_2);
		U3CStartU3Ec__AnonStorey31_t1152509869 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CStartU3Ec__AnonStorey31_U3CU3Em__2F_m2705133908_MethodInfo_var);
		EventHandler_1_t3942771990 * L_5 = (EventHandler_1_t3942771990 *)il2cpp_codegen_object_new(EventHandler_1_t3942771990_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m2372365329(L_5, L_3, L_4, /*hidden argument*/EventHandler_1__ctor_m2372365329_MethodInfo_var);
		return L_5;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey31::.ctor()
extern "C"  void U3CStartU3Ec__AnonStorey31__ctor_m2817058867 (U3CStartU3Ec__AnonStorey31_t1152509869 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey31::<>m__2F(System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs)
extern const MethodInfo* Action_1_Invoke_m3544156790_MethodInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey31_U3CU3Em__2F_m2705133908_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey31_U3CU3Em__2F_m2705133908 (U3CStartU3Ec__AnonStorey31_t1152509869 * __this, Il2CppObject * ___sender0, MyEventArgs_t3100194347 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey31_U3CU3Em__2F_m2705133908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3248647052 * L_0 = __this->get_h_0();
		MyEventArgs_t3100194347 * L_1 = ___e1;
		NullCheck(L_0);
		Action_1_Invoke_m3544156790(L_0, L_1, /*hidden argument*/Action_1_Invoke_m3544156790_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::.ctor()
extern "C"  void U3CStartU3Ec__AnonStorey32__ctor_m2620545362 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__25(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__25_m1219274304_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__25_m1219274304 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__25_m1219274304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sample09_EventHandling_t2113606094 * L_0 = __this->get_U3CU3Ef__this_1();
		Sample09_EventHandling_t2113606094 * L_1 = L_0;
		NullCheck(L_1);
		EventHandler_1_t3942771990 * L_2 = L_1->get_FooBar_4();
		EventHandler_1_t3942771990 * L_3 = ___h0;
		Delegate_t3660574010 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_FooBar_4(((EventHandler_1_t3942771990 *)CastclassSealed(L_4, EventHandler_1_t3942771990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__26(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__26_m2086326529_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__26_m2086326529 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__26_m2086326529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sample09_EventHandling_t2113606094 * L_0 = __this->get_U3CU3Ef__this_1();
		Sample09_EventHandling_t2113606094 * L_1 = L_0;
		NullCheck(L_1);
		EventHandler_1_t3942771990 * L_2 = L_1->get_FooBar_4();
		EventHandler_1_t3942771990 * L_3 = ___h0;
		Delegate_t3660574010 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_FooBar_4(((EventHandler_1_t3942771990 *)CastclassSealed(L_4, EventHandler_1_t3942771990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__28(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__28_m3820430979_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__28_m3820430979 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__28_m3820430979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sample09_EventHandling_t2113606094 * L_0 = __this->get_U3CU3Ef__this_1();
		Sample09_EventHandling_t2113606094 * L_1 = L_0;
		NullCheck(L_1);
		EventHandler_1_t3942771990 * L_2 = L_1->get_FooBar_4();
		EventHandler_1_t3942771990 * L_3 = ___h0;
		Delegate_t3660574010 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_FooBar_4(((EventHandler_1_t3942771990 *)CastclassSealed(L_4, EventHandler_1_t3942771990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__29(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__29_m392515908_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__29_m392515908 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__29_m392515908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sample09_EventHandling_t2113606094 * L_0 = __this->get_U3CU3Ef__this_1();
		Sample09_EventHandling_t2113606094 * L_1 = L_0;
		NullCheck(L_1);
		EventHandler_1_t3942771990 * L_2 = L_1->get_FooBar_4();
		EventHandler_1_t3942771990 * L_3 = ___h0;
		Delegate_t3660574010 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_FooBar_4(((EventHandler_1_t3942771990 *)CastclassSealed(L_4, EventHandler_1_t3942771990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2A(System.Action`1<System.Int32>)
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__2A_m3630825647_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__2A_m3630825647 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, Action_1_t2995867492 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__2A_m3630825647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sample09_EventHandling_t2113606094 * L_0 = __this->get_U3CU3Ef__this_1();
		Sample09_EventHandling_t2113606094 * L_1 = L_0;
		NullCheck(L_1);
		Action_1_t2995867492 * L_2 = L_1->get_FooFoo_5();
		Action_1_t2995867492 * L_3 = ___h0;
		Delegate_t3660574010 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_FooFoo_5(((Action_1_t2995867492 *)CastclassSealed(L_4, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2B(System.Action`1<System.Int32>)
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__2B_m1620721806_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__2B_m1620721806 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, Action_1_t2995867492 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__2B_m1620721806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sample09_EventHandling_t2113606094 * L_0 = __this->get_U3CU3Ef__this_1();
		Sample09_EventHandling_t2113606094 * L_1 = L_0;
		NullCheck(L_1);
		Action_1_t2995867492 * L_2 = L_1->get_FooFoo_5();
		Action_1_t2995867492 * L_3 = ___h0;
		Delegate_t3660574010 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_FooFoo_5(((Action_1_t2995867492 *)CastclassSealed(L_4, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2C(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const MethodInfo* EventHandler_1_Invoke_m2154207886_MethodInfo_var;
extern const MethodInfo* EventHandler_1__ctor_m2372365329_MethodInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__2C_m1001324839_MetadataUsageId;
extern "C"  EventHandler_1_t3942771990 * U3CStartU3Ec__AnonStorey32_U3CU3Em__2C_m1001324839 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__2C_m1001324839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = __this->get_address_of_capture_0();
		Int32_GetHashCode_m3396943446(L_0, /*hidden argument*/NULL);
		EventHandler_1_t3942771990 * L_1 = ___h0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)EventHandler_1_Invoke_m2154207886_MethodInfo_var);
		EventHandler_1_t3942771990 * L_3 = (EventHandler_1_t3942771990 *)il2cpp_codegen_object_new(EventHandler_1_t3942771990_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m2372365329(L_3, L_1, L_2, /*hidden argument*/EventHandler_1__ctor_m2372365329_MethodInfo_var);
		return L_3;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2D(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__2D_m1340155791_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__2D_m1340155791 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__2D_m1340155791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sample09_EventHandling_t2113606094 * L_0 = __this->get_U3CU3Ef__this_1();
		Sample09_EventHandling_t2113606094 * L_1 = L_0;
		NullCheck(L_1);
		EventHandler_1_t3942771990 * L_2 = L_1->get_FooBar_4();
		EventHandler_1_t3942771990 * L_3 = ___h0;
		Delegate_t3660574010 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_FooBar_4(((EventHandler_1_t3942771990 *)CastclassSealed(L_4, EventHandler_1_t3942771990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2E(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern Il2CppClass* EventHandler_1_t3942771990_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__AnonStorey32_U3CU3Em__2E_m2207208016_MetadataUsageId;
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__2E_m2207208016 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__AnonStorey32_U3CU3Em__2E_m2207208016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sample09_EventHandling_t2113606094 * L_0 = __this->get_U3CU3Ef__this_1();
		Sample09_EventHandling_t2113606094 * L_1 = L_0;
		NullCheck(L_1);
		EventHandler_1_t3942771990 * L_2 = L_1->get_FooBar_4();
		EventHandler_1_t3942771990 * L_3 = ___h0;
		Delegate_t3660574010 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_FooBar_4(((EventHandler_1_t3942771990 *)CastclassSealed(L_4, EventHandler_1_t3942771990_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/MyEventArgs::.ctor()
extern Il2CppClass* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t MyEventArgs__ctor_m2051191323_MetadataUsageId;
extern "C"  void MyEventArgs__ctor_m2051191323 (MyEventArgs_t3100194347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MyEventArgs__ctor_m2051191323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UniRx.Examples.Sample09_EventHandling/MyEventArgs::get_MyProperty()
extern "C"  int32_t MyEventArgs_get_MyProperty_m1029180861 (MyEventArgs_t3100194347 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CMyPropertyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.Examples.Sample09_EventHandling/MyEventArgs::set_MyProperty(System.Int32)
extern "C"  void MyEventArgs_set_MyProperty_m1957490872 (MyEventArgs_t3100194347 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CMyPropertyU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::.ctor()
extern "C"  void Sample10_MainThreadDispatcher__ctor_m699156572 (Sample10_MainThreadDispatcher_t1609111451 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::Run()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t1429988286_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2995867587_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t3701067285_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2706738743_il2cpp_TypeInfo_var;
extern const MethodInfo* Sample10_MainThreadDispatcher_TestAsync_m497817684_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m4104442892_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022_MethodInfo_var;
extern const MethodInfo* Sample10_MainThreadDispatcher_U3CRunU3Em__30_m1490611473_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const MethodInfo* Sample10_MainThreadDispatcher_U3CRunU3Em__31_m1672289270_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1589437765_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_MethodInfo_var;
extern const MethodInfo* Sample10_MainThreadDispatcher_U3CRunU3Em__32_m426223150_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m2615142501_MethodInfo_var;
extern const MethodInfo* Observable_Start_TisUnit_t2558286038_m2976392644_MethodInfo_var;
extern const MethodInfo* Observable_ObserveOnMainThread_TisUnit_t2558286038_m2215972113_MethodInfo_var;
extern const MethodInfo* Sample10_MainThreadDispatcher_U3CRunU3Em__33_m2418357898_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1024136343_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisUnit_t2558286038_m1708333140_MethodInfo_var;
extern const uint32_t Sample10_MainThreadDispatcher_Run_m3238968197_MetadataUsageId;
extern "C"  void Sample10_MainThreadDispatcher_Run_m3238968197 (Sample10_MainThreadDispatcher_t1609111451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample10_MainThreadDispatcher_Run_m3238968197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	Il2CppObject* G_B8_0 = NULL;
	Il2CppObject* G_B7_0 = NULL;
	{
		Il2CppObject * L_0 = Sample10_MainThreadDispatcher_TestAsync_m497817684(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_StartCoroutine_m3648496618(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Sample10_MainThreadDispatcher_TestAsync_m497817684_MethodInfo_var);
		Func_1_t1429988286 * L_2 = (Func_1_t1429988286 *)il2cpp_codegen_object_new(Func_1_t1429988286_il2cpp_TypeInfo_var);
		Func_1__ctor_m4104442892(L_2, __this, L_1, /*hidden argument*/Func_1__ctor_m4104442892_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_3 = Observable_FromCoroutine_m3913040964(NULL /*static, unused*/, L_2, (bool)0, /*hidden argument*/NULL);
		ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022(NULL /*static, unused*/, L_3, /*hidden argument*/ObservableExtensions_Subscribe_TisUnit_t2558286038_m590527022_MethodInfo_var);
		Action_1_t985559125 * L_4 = ((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		if (L_4)
		{
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)Sample10_MainThreadDispatcher_U3CRunU3Em__30_m1490611473_MethodInfo_var);
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_6, NULL, L_5, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_0(L_6);
	}

IL_003c:
	{
		Action_1_t985559125 * L_7 = ((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_Post_m2114450895(NULL /*static, unused*/, L_7, NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_8 = TimeSpan_FromSeconds_m1904297940(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_9 = Observable_Interval_m2492905901(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Action_1_t2995867587 * L_10 = ((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B3_0 = L_9;
		if (L_10)
		{
			G_B4_0 = L_9;
			goto IL_0072;
		}
	}
	{
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)Sample10_MainThreadDispatcher_U3CRunU3Em__31_m1672289270_MethodInfo_var);
		Action_1_t2995867587 * L_12 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(Action_1_t2995867587_il2cpp_TypeInfo_var);
		Action_1__ctor_m1589437765(L_12, NULL, L_11, /*hidden argument*/Action_1__ctor_m1589437765_MethodInfo_var);
		((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_12);
		G_B4_0 = G_B3_0;
	}

IL_0072:
	{
		Action_1_t2995867587 * L_13 = ((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470(NULL /*static, unused*/, G_B4_0, L_13, /*hidden argument*/ObservableExtensions_Subscribe_TisInt64_t2847414882_m1751770470_MethodInfo_var);
		Func_1_t3701067285 * L_14 = ((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_14)
		{
			goto IL_0095;
		}
	}
	{
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)Sample10_MainThreadDispatcher_U3CRunU3Em__32_m426223150_MethodInfo_var);
		Func_1_t3701067285 * L_16 = (Func_1_t3701067285 *)il2cpp_codegen_object_new(Func_1_t3701067285_il2cpp_TypeInfo_var);
		Func_1__ctor_m2615142501(L_16, NULL, L_15, /*hidden argument*/Func_1__ctor_m2615142501_MethodInfo_var);
		((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_16);
	}

IL_0095:
	{
		Func_1_t3701067285 * L_17 = ((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_18 = Observable_Start_TisUnit_t2558286038_m2976392644(NULL /*static, unused*/, L_17, /*hidden argument*/Observable_Start_TisUnit_t2558286038_m2976392644_MethodInfo_var);
		Il2CppObject* L_19 = Observable_ObserveOnMainThread_TisUnit_t2558286038_m2215972113(NULL /*static, unused*/, L_18, /*hidden argument*/Observable_ObserveOnMainThread_TisUnit_t2558286038_m2215972113_MethodInfo_var);
		Action_1_t2706738743 * L_20 = ((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		G_B7_0 = L_19;
		if (L_20)
		{
			G_B8_0 = L_19;
			goto IL_00bc;
		}
	}
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)Sample10_MainThreadDispatcher_U3CRunU3Em__33_m2418357898_MethodInfo_var);
		Action_1_t2706738743 * L_22 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(Action_1_t2706738743_il2cpp_TypeInfo_var);
		Action_1__ctor_m1024136343(L_22, NULL, L_21, /*hidden argument*/Action_1__ctor_m1024136343_MethodInfo_var);
		((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_3(L_22);
		G_B8_0 = G_B7_0;
	}

IL_00bc:
	{
		Action_1_t2706738743 * L_23 = ((Sample10_MainThreadDispatcher_t1609111451_StaticFields*)Sample10_MainThreadDispatcher_t1609111451_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		ObservableExtensions_Subscribe_TisUnit_t2558286038_m1708333140(NULL /*static, unused*/, G_B8_0, L_23, /*hidden argument*/ObservableExtensions_Subscribe_TisUnit_t2558286038_m1708333140_MethodInfo_var);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Examples.Sample10_MainThreadDispatcher::TestAsync()
extern Il2CppClass* U3CTestAsyncU3Ec__IteratorC_t488520482_il2cpp_TypeInfo_var;
extern const uint32_t Sample10_MainThreadDispatcher_TestAsync_m497817684_MetadataUsageId;
extern "C"  Il2CppObject * Sample10_MainThreadDispatcher_TestAsync_m497817684 (Sample10_MainThreadDispatcher_t1609111451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample10_MainThreadDispatcher_TestAsync_m497817684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTestAsyncU3Ec__IteratorC_t488520482 * V_0 = NULL;
	{
		U3CTestAsyncU3Ec__IteratorC_t488520482 * L_0 = (U3CTestAsyncU3Ec__IteratorC_t488520482 *)il2cpp_codegen_object_new(U3CTestAsyncU3Ec__IteratorC_t488520482_il2cpp_TypeInfo_var);
		U3CTestAsyncU3Ec__IteratorC__ctor_m186727593(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTestAsyncU3Ec__IteratorC_t488520482 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::<Run>m__30(System.Object)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3556498;
extern const uint32_t Sample10_MainThreadDispatcher_U3CRunU3Em__30_m1490611473_MetadataUsageId;
extern "C"  void Sample10_MainThreadDispatcher_U3CRunU3Em__30_m1490611473 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample10_MainThreadDispatcher_U3CRunU3Em__30_m1490611473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral3556498, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::<Run>m__31(System.Int64)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Sample10_MainThreadDispatcher_U3CRunU3Em__31_m1672289270_MetadataUsageId;
extern "C"  void Sample10_MainThreadDispatcher_U3CRunU3Em__31_m1672289270 (Il2CppObject * __this /* static, unused */, int64_t ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample10_MainThreadDispatcher_U3CRunU3Em__31_m1672289270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___x0;
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.Unit UniRx.Examples.Sample10_MainThreadDispatcher::<Run>m__32()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Sample10_MainThreadDispatcher_U3CRunU3Em__32_m426223150_MetadataUsageId;
extern "C"  Unit_t2558286038  Sample10_MainThreadDispatcher_U3CRunU3Em__32_m426223150 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample10_MainThreadDispatcher_U3CRunU3Em__32_m426223150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_0 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::<Run>m__33(UniRx.Unit)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Sample10_MainThreadDispatcher_U3CRunU3Em__33_m2418357898_MetadataUsageId;
extern "C"  void Sample10_MainThreadDispatcher_U3CRunU3Em__33_m2418357898 (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample10_MainThreadDispatcher_U3CRunU3Em__33_m2418357898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Unit_t2558286038  L_0 = ___x0;
		Unit_t2558286038  L_1 = L_0;
		Il2CppObject * L_2 = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::.ctor()
extern "C"  void U3CTestAsyncU3Ec__IteratorC__ctor_m186727593 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestAsyncU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4032043859 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestAsyncU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3872728295 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::MoveNext()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral97;
extern Il2CppCodeGenString* _stringLiteral98;
extern Il2CppCodeGenString* _stringLiteral99;
extern Il2CppCodeGenString* _stringLiteral100;
extern const uint32_t U3CTestAsyncU3Ec__IteratorC_MoveNext_m3384603003_MetadataUsageId;
extern "C"  bool U3CTestAsyncU3Ec__IteratorC_MoveNext_m3384603003 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTestAsyncU3Ec__IteratorC_MoveNext_m3384603003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
		if (L_1 == 2)
		{
			goto IL_0075;
		}
		if (L_1 == 3)
		{
			goto IL_009b;
		}
	}
	{
		goto IL_00ac;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral97, /*hidden argument*/NULL);
		WaitForSeconds_t1291133240 * L_2 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_00ae;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral98, /*hidden argument*/NULL);
		WaitForSeconds_t1291133240 * L_3 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_3, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_3);
		__this->set_U24PC_0(2);
		goto IL_00ae;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral99, /*hidden argument*/NULL);
		WaitForSeconds_t1291133240 * L_4 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(3);
		goto IL_00ae;
	}

IL_009b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral100, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_00ac:
	{
		return (bool)0;
	}

IL_00ae:
	{
		return (bool)1;
	}
	// Dead block : IL_00b0: ldloc.1
}
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::Dispose()
extern "C"  void U3CTestAsyncU3Ec__IteratorC_Dispose_m3249840806 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CTestAsyncU3Ec__IteratorC_Reset_m2128127830_MetadataUsageId;
extern "C"  void U3CTestAsyncU3Ec__IteratorC_Reset_m2128127830 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTestAsyncU3Ec__IteratorC_Reset_m2128127830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Examples.Sample11_Logger::.ctor()
extern "C"  void Sample11_Logger__ctor_m2291555575 (Sample11_Logger_t602978400 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample11_Logger::.cctor()
extern Il2CppClass* Logger_t2118475020_il2cpp_TypeInfo_var;
extern Il2CppClass* Sample11_Logger_t602978400_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2040208746;
extern const uint32_t Sample11_Logger__cctor_m1836649878_MetadataUsageId;
extern "C"  void Sample11_Logger__cctor_m1836649878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample11_Logger__cctor_m1836649878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Logger_t2118475020 * L_0 = (Logger_t2118475020 *)il2cpp_codegen_object_new(Logger_t2118475020_il2cpp_TypeInfo_var);
		Logger__ctor_m3165661341(L_0, _stringLiteral2040208746, /*hidden argument*/NULL);
		((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->set_logger_0(L_0);
		return;
	}
}
// System.Void UniRx.Examples.Sample11_Logger::ApplicationInitialize()
extern Il2CppClass* ObservableLogger_t586897455_il2cpp_TypeInfo_var;
extern Il2CppClass* Sample11_Logger_t602978400_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1476384563_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2039608427_il2cpp_TypeInfo_var;
extern const MethodInfo* Sample11_Logger_U3CApplicationInitializeU3Em__34_m295725365_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2453788459_MethodInfo_var;
extern const MethodInfo* Observable_Where_TisLogEntry_t1891155722_m486115208_MethodInfo_var;
extern const MethodInfo* Sample11_Logger_U3CApplicationInitializeU3Em__35_m188187674_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3129516011_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisLogEntry_t1891155722_m1858544768_MethodInfo_var;
extern const uint32_t Sample11_Logger_ApplicationInitialize_m264119061_MetadataUsageId;
extern "C"  void Sample11_Logger_ApplicationInitialize_m264119061 (Sample11_Logger_t602978400 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample11_Logger_ApplicationInitialize_m264119061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObservableLogger_t586897455 * G_B2_0 = NULL;
	ObservableLogger_t586897455 * G_B1_0 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObservableLogger_t586897455_il2cpp_TypeInfo_var);
		ObservableLogger_t586897455 * L_0 = ((ObservableLogger_t586897455_StaticFields*)ObservableLogger_t586897455_il2cpp_TypeInfo_var->static_fields)->get_Listener_1();
		LogEntryExtensions_LogToUnityDebug_m3186420352(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ObservableLogger_t586897455 * L_1 = ((ObservableLogger_t586897455_StaticFields*)ObservableLogger_t586897455_il2cpp_TypeInfo_var->static_fields)->get_Listener_1();
		IL2CPP_RUNTIME_CLASS_INIT(Sample11_Logger_t602978400_il2cpp_TypeInfo_var);
		Func_2_t1476384563 * L_2 = ((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = L_1;
		if (L_2)
		{
			G_B2_0 = L_1;
			goto IL_0028;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)Sample11_Logger_U3CApplicationInitializeU3Em__34_m295725365_MethodInfo_var);
		Func_2_t1476384563 * L_4 = (Func_2_t1476384563 *)il2cpp_codegen_object_new(Func_2_t1476384563_il2cpp_TypeInfo_var);
		Func_2__ctor_m2453788459(L_4, NULL, L_3, /*hidden argument*/Func_2__ctor_m2453788459_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Sample11_Logger_t602978400_il2cpp_TypeInfo_var);
		((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_4);
		G_B2_0 = G_B1_0;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Sample11_Logger_t602978400_il2cpp_TypeInfo_var);
		Func_2_t1476384563 * L_5 = ((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_6 = Observable_Where_TisLogEntry_t1891155722_m486115208(NULL /*static, unused*/, G_B2_0, L_5, /*hidden argument*/Observable_Where_TisLogEntry_t1891155722_m486115208_MethodInfo_var);
		Action_1_t2039608427 * L_7 = ((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		G_B3_0 = L_6;
		if (L_7)
		{
			G_B4_0 = L_6;
			goto IL_004a;
		}
	}
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)Sample11_Logger_U3CApplicationInitializeU3Em__35_m188187674_MethodInfo_var);
		Action_1_t2039608427 * L_9 = (Action_1_t2039608427 *)il2cpp_codegen_object_new(Action_1_t2039608427_il2cpp_TypeInfo_var);
		Action_1__ctor_m3129516011(L_9, NULL, L_8, /*hidden argument*/Action_1__ctor_m3129516011_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Sample11_Logger_t602978400_il2cpp_TypeInfo_var);
		((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_9);
		G_B4_0 = G_B3_0;
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Sample11_Logger_t602978400_il2cpp_TypeInfo_var);
		Action_1_t2039608427 * L_10 = ((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		ObservableExtensions_Subscribe_TisLogEntry_t1891155722_m1858544768(NULL /*static, unused*/, G_B4_0, L_10, /*hidden argument*/ObservableExtensions_Subscribe_TisLogEntry_t1891155722_m1858544768_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Examples.Sample11_Logger::Run()
extern Il2CppClass* Sample11_Logger_t602978400_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1221785338;
extern Il2CppCodeGenString* _stringLiteral2619578343;
extern Il2CppCodeGenString* _stringLiteral1075653185;
extern const uint32_t Sample11_Logger_Run_m4045093600_MetadataUsageId;
extern "C"  void Sample11_Logger_Run_m4045093600 (Sample11_Logger_t602978400 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample11_Logger_Run_m4045093600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Sample11_Logger_t602978400_il2cpp_TypeInfo_var);
		Logger_t2118475020 * L_0 = ((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->get_logger_0();
		NullCheck(L_0);
		VirtActionInvoker2< Il2CppObject *, Object_t3878351788 * >::Invoke(4 /* System.Void UniRx.Diagnostics.Logger::Debug(System.Object,UnityEngine.Object) */, L_0, _stringLiteral1221785338, (Object_t3878351788 *)NULL);
		Logger_t2118475020 * L_1 = ((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->get_logger_0();
		NullCheck(L_1);
		VirtActionInvoker2< Il2CppObject *, Object_t3878351788 * >::Invoke(6 /* System.Void UniRx.Diagnostics.Logger::Log(System.Object,UnityEngine.Object) */, L_1, _stringLiteral2619578343, (Object_t3878351788 *)NULL);
		Logger_t2118475020 * L_2 = ((Sample11_Logger_t602978400_StaticFields*)Sample11_Logger_t602978400_il2cpp_TypeInfo_var->static_fields)->get_logger_0();
		Exception_t1967233988 * L_3 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_3, _stringLiteral1075653185, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< Exception_t1967233988 *, Object_t3878351788 * >::Invoke(12 /* System.Void UniRx.Diagnostics.Logger::Exception(System.Exception,UnityEngine.Object) */, L_2, L_3, (Object_t3878351788 *)NULL);
		return;
	}
}
// System.Boolean UniRx.Examples.Sample11_Logger::<ApplicationInitialize>m__34(UniRx.Diagnostics.LogEntry)
extern "C"  bool Sample11_Logger_U3CApplicationInitializeU3Em__34_m295725365 (Il2CppObject * __this /* static, unused */, LogEntry_t1891155722 * ___x0, const MethodInfo* method)
{
	{
		LogEntry_t1891155722 * L_0 = ___x0;
		NullCheck(L_0);
		int32_t L_1 = LogEntry_get_LogType_m344512238(L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)4))? 1 : 0);
	}
}
// System.Void UniRx.Examples.Sample11_Logger::<ApplicationInitialize>m__35(UniRx.Diagnostics.LogEntry)
extern "C"  void Sample11_Logger_U3CApplicationInitializeU3Em__35_m188187674 (Il2CppObject * __this /* static, unused */, LogEntry_t1891155722 * ___x0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.FloatReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t1566542059_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1840047419_MethodInfo_var;
extern const uint32_t FloatReactiveProperty__ctor_m4216839909_MetadataUsageId;
extern "C"  void FloatReactiveProperty__ctor_m4216839909 (FloatReactiveProperty_t3982138748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatReactiveProperty__ctor_m4216839909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t1566542059_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1840047419(__this, /*hidden argument*/ReactiveProperty_1__ctor_m1840047419_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.FloatReactiveProperty::.ctor(System.Single)
extern Il2CppClass* ReactiveProperty_1_t1566542059_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1206896515_MethodInfo_var;
extern const uint32_t FloatReactiveProperty__ctor_m2376679142_MetadataUsageId;
extern "C"  void FloatReactiveProperty__ctor_m2376679142 (FloatReactiveProperty_t3982138748 * __this, float ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatReactiveProperty__ctor_m2376679142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t1566542059_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1206896515(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m1206896515_MethodInfo_var);
		return;
	}
}
// UnityEngine.YieldInstruction UniRx.FrameCountTypeExtensions::GetYieldInstruction(UniRx.FrameCountType)
extern Il2CppClass* YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var;
extern const uint32_t FrameCountTypeExtensions_GetYieldInstruction_m1644022139_MetadataUsageId;
extern "C"  YieldInstruction_t3557331758 * FrameCountTypeExtensions_GetYieldInstruction_m1644022139 (Il2CppObject * __this /* static, unused */, int32_t ___frameCountType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FrameCountTypeExtensions_GetYieldInstruction_m1644022139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___frameCountType0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0019;
		}
		if (L_1 == 2)
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0025;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var);
		WaitForFixedUpdate_t896427542 * L_2 = ((YieldInstructionCache_t1940766803_StaticFields*)YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var->static_fields)->get_WaitForFixedUpdate_1();
		return L_2;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var);
		WaitForEndOfFrame_t1917318876 * L_3 = ((YieldInstructionCache_t1940766803_StaticFields*)YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var->static_fields)->get_WaitForEndOfFrame_0();
		return L_3;
	}

IL_0025:
	{
		return (YieldInstruction_t3557331758 *)NULL;
	}
}
// System.Void UniRx.InspectorDisplayAttribute::.ctor(System.String,System.Boolean)
extern "C"  void InspectorDisplayAttribute__ctor_m3611869529 (InspectorDisplayAttribute_t3553855523 * __this, String_t* ___fieldName0, bool ___notifyPropertyChanged1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___fieldName0;
		InspectorDisplayAttribute_set_FieldName_m3937563710(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___notifyPropertyChanged1;
		InspectorDisplayAttribute_set_NotifyPropertyChanged_m2014822060(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UniRx.InspectorDisplayAttribute::get_FieldName()
extern "C"  String_t* InspectorDisplayAttribute_get_FieldName_m2931401563 (InspectorDisplayAttribute_t3553855523 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFieldNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.InspectorDisplayAttribute::set_FieldName(System.String)
extern "C"  void InspectorDisplayAttribute_set_FieldName_m3937563710 (InspectorDisplayAttribute_t3553855523 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFieldNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UniRx.InspectorDisplayAttribute::get_NotifyPropertyChanged()
extern "C"  bool InspectorDisplayAttribute_get_NotifyPropertyChanged_m348085381 (InspectorDisplayAttribute_t3553855523 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CNotifyPropertyChangedU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.InspectorDisplayAttribute::set_NotifyPropertyChanged(System.Boolean)
extern "C"  void InspectorDisplayAttribute_set_NotifyPropertyChanged_m2014822060 (InspectorDisplayAttribute_t3553855523 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CNotifyPropertyChangedU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UniRx.InternalUtil.AsyncLock::.ctor()
extern Il2CppClass* Queue_1_t2145611487_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m2872649797_MethodInfo_var;
extern const uint32_t AsyncLock__ctor_m3596353695_MetadataUsageId;
extern "C"  void AsyncLock__ctor_m3596353695 (AsyncLock_t4023816884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncLock__ctor_m3596353695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Queue_1_t2145611487 * L_0 = (Queue_1_t2145611487 *)il2cpp_codegen_object_new(Queue_1_t2145611487_il2cpp_TypeInfo_var);
		Queue_1__ctor_m2872649797(L_0, /*hidden argument*/Queue_1__ctor_m2872649797_MethodInfo_var);
		__this->set_queue_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.AsyncLock::Wait(System.Action)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m125010460_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1828561961_MethodInfo_var;
extern const MethodInfo* Queue_1_Clear_m278783088_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2872016438;
extern const uint32_t AsyncLock_Wait_m1580205763_MetadataUsageId;
extern "C"  void AsyncLock_Wait_m1580205763 (AsyncLock_t4023816884 * __this, Action_t437523947 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncLock_Wait_m1580205763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Queue_1_t2145611487 * V_1 = NULL;
	Action_t437523947 * V_2 = NULL;
	Queue_1_t2145611487 * V_3 = NULL;
	Queue_1_t2145611487 * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Action_t437523947 * L_0 = ___action0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2872016438, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (bool)0;
		Queue_1_t2145611487 * L_2 = __this->get_queue_0();
		V_1 = L_2;
		Queue_1_t2145611487 * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			bool L_4 = __this->get_hasFaulted_2();
			if (L_4)
			{
				goto IL_0048;
			}
		}

IL_002b:
		{
			Queue_1_t2145611487 * L_5 = __this->get_queue_0();
			Action_t437523947 * L_6 = ___action0;
			NullCheck(L_5);
			Queue_1_Enqueue_m125010460(L_5, L_6, /*hidden argument*/Queue_1_Enqueue_m125010460_MethodInfo_var);
			bool L_7 = __this->get_isAcquired_1();
			V_0 = (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
			__this->set_isAcquired_1((bool)1);
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x54, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Queue_1_t2145611487 * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0054:
	{
		bool L_9 = V_0;
		if (!L_9)
		{
			goto IL_00e9;
		}
	}

IL_005a:
	{
		V_2 = (Action_t437523947 *)NULL;
		Queue_1_t2145611487 * L_10 = __this->get_queue_0();
		V_3 = L_10;
		Queue_1_t2145611487 * L_11 = V_3;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0069:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t2145611487 * L_12 = __this->get_queue_0();
			NullCheck(L_12);
			int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<System.Action>::get_Count() */, L_12);
			if ((((int32_t)L_13) <= ((int32_t)0)))
			{
				goto IL_008b;
			}
		}

IL_007a:
		{
			Queue_1_t2145611487 * L_14 = __this->get_queue_0();
			NullCheck(L_14);
			Action_t437523947 * L_15 = Queue_1_Dequeue_m1828561961(L_14, /*hidden argument*/Queue_1_Dequeue_m1828561961_MethodInfo_var);
			V_2 = L_15;
			goto IL_0097;
		}

IL_008b:
		{
			__this->set_isAcquired_1((bool)0);
			IL2CPP_LEAVE(0xE9, FINALLY_009c);
		}

IL_0097:
		{
			IL2CPP_LEAVE(0xA3, FINALLY_009c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_009c;
	}

FINALLY_009c:
	{ // begin finally (depth: 1)
		Queue_1_t2145611487 * L_16 = V_3;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(156)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(156)
	{
		IL2CPP_JUMP_TBL(0xE9, IL_00e9)
		IL2CPP_JUMP_TBL(0xA3, IL_00a3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a3:
	try
	{ // begin try (depth: 1)
		Action_t437523947 * L_17 = V_2;
		NullCheck(L_17);
		Action_Invoke_m1445970038(L_17, /*hidden argument*/NULL);
		goto IL_00e4;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ae;
		throw e;
	}

CATCH_00ae:
	{ // begin catch(System.Object)
		{
			Queue_1_t2145611487 * L_18 = __this->get_queue_0();
			V_4 = L_18;
			Queue_1_t2145611487 * L_19 = V_4;
			Monitor_Enter_m476686225(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		}

IL_00be:
		try
		{ // begin try (depth: 2)
			Queue_1_t2145611487 * L_20 = __this->get_queue_0();
			NullCheck(L_20);
			Queue_1_Clear_m278783088(L_20, /*hidden argument*/Queue_1_Clear_m278783088_MethodInfo_var);
			__this->set_hasFaulted_2((bool)1);
			IL2CPP_LEAVE(0xDD, FINALLY_00d5);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00d5;
		}

FINALLY_00d5:
		{ // begin finally (depth: 2)
			Queue_1_t2145611487 * L_21 = V_4;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(213)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(213)
		{
			IL2CPP_JUMP_TBL(0xDD, IL_00dd)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00dd:
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_00df:
		{
			goto IL_00e4;
		}
	} // end catch (depth: 1)

IL_00e4:
	{
		goto IL_005a;
	}

IL_00e9:
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.AsyncLock::Dispose()
extern const MethodInfo* Queue_1_Clear_m278783088_MethodInfo_var;
extern const uint32_t AsyncLock_Dispose_m2840477980_MetadataUsageId;
extern "C"  void AsyncLock_Dispose_m2840477980 (AsyncLock_t4023816884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncLock_Dispose_m2840477980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Queue_1_t2145611487 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t2145611487 * L_0 = __this->get_queue_0();
		V_0 = L_0;
		Queue_1_t2145611487 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t2145611487 * L_2 = __this->get_queue_0();
		NullCheck(L_2);
		Queue_1_Clear_m278783088(L_2, /*hidden argument*/Queue_1_Clear_m278783088_MethodInfo_var);
		__this->set_hasFaulted_2((bool)1);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Queue_1_t2145611487 * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ScheduledItem::.ctor(System.Action,System.TimeSpan)
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern const uint32_t ScheduledItem__ctor_m3322249709_MetadataUsageId;
extern "C"  void ScheduledItem__ctor_m3322249709 (ScheduledItem_t1912903245 * __this, Action_t437523947 * ___action0, TimeSpan_t763862892  ___dueTime1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledItem__ctor_m3322249709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BooleanDisposable_t3065601722 * L_0 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_0, /*hidden argument*/NULL);
		__this->set__disposable_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_1 = ___dueTime1;
		__this->set__dueTime_1(L_1);
		Action_t437523947 * L_2 = ___action0;
		__this->set__action_2(L_2);
		return;
	}
}
// System.TimeSpan UniRx.InternalUtil.ScheduledItem::get_DueTime()
extern "C"  TimeSpan_t763862892  ScheduledItem_get_DueTime_m852343711 (ScheduledItem_t1912903245 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t763862892  L_0 = __this->get__dueTime_1();
		return L_0;
	}
}
// System.Void UniRx.InternalUtil.ScheduledItem::Invoke()
extern "C"  void ScheduledItem_Invoke_m1551717398 (ScheduledItem_t1912903245 * __this, const MethodInfo* method)
{
	{
		BooleanDisposable_t3065601722 * L_0 = __this->get__disposable_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, L_0);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		Action_t437523947 * L_2 = __this->get__action_2();
		NullCheck(L_2);
		Action_Invoke_m1445970038(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Int32 UniRx.InternalUtil.ScheduledItem::CompareTo(UniRx.InternalUtil.ScheduledItem)
extern "C"  int32_t ScheduledItem_CompareTo_m2869093541 (ScheduledItem_t1912903245 * __this, ScheduledItem_t1912903245 * ___other0, const MethodInfo* method)
{
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ScheduledItem_t1912903245 * L_0 = ___other0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_0, NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 1;
	}

IL_000e:
	{
		TimeSpan_t763862892  L_2 = ScheduledItem_get_DueTime_m852343711(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		ScheduledItem_t1912903245 * L_3 = ___other0;
		NullCheck(L_3);
		TimeSpan_t763862892  L_4 = ScheduledItem_get_DueTime_m852343711(L_3, /*hidden argument*/NULL);
		int32_t L_5 = TimeSpan_CompareTo_m2960988804((&V_0), L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UniRx.InternalUtil.ScheduledItem::Equals(System.Object)
extern "C"  bool ScheduledItem_Equals_m1129861627 (ScheduledItem_t1912903245 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UniRx.InternalUtil.ScheduledItem::GetHashCode()
extern "C"  int32_t ScheduledItem_GetHashCode_m1775533855 (ScheduledItem_t1912903245 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetHashCode_m500842593(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.IDisposable UniRx.InternalUtil.ScheduledItem::get_Cancellation()
extern "C"  Il2CppObject * ScheduledItem_get_Cancellation_m836388197 (ScheduledItem_t1912903245 * __this, const MethodInfo* method)
{
	{
		BooleanDisposable_t3065601722 * L_0 = __this->get__disposable_0();
		return L_0;
	}
}
// System.Boolean UniRx.InternalUtil.ScheduledItem::get_IsCanceled()
extern "C"  bool ScheduledItem_get_IsCanceled_m950977702 (ScheduledItem_t1912903245 * __this, const MethodInfo* method)
{
	{
		BooleanDisposable_t3065601722 * L_0 = __this->get__disposable_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, L_0);
		return L_1;
	}
}
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_LessThan(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_LessThan_m3567740998 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method)
{
	{
		ScheduledItem_t1912903245 * L_0 = ___left0;
		ScheduledItem_t1912903245 * L_1 = ___right1;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, ScheduledItem_t1912903245 * >::Invoke(4 /* System.Int32 UniRx.InternalUtil.ScheduledItem::CompareTo(UniRx.InternalUtil.ScheduledItem) */, L_0, L_1);
		return (bool)((((int32_t)L_2) < ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_LessThanOrEqual(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_LessThanOrEqual_m452202771 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method)
{
	{
		ScheduledItem_t1912903245 * L_0 = ___left0;
		ScheduledItem_t1912903245 * L_1 = ___right1;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, ScheduledItem_t1912903245 * >::Invoke(4 /* System.Int32 UniRx.InternalUtil.ScheduledItem::CompareTo(UniRx.InternalUtil.ScheduledItem) */, L_0, L_1);
		return (bool)((((int32_t)((((int32_t)L_2) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_GreaterThan(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_GreaterThan_m1585086255 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method)
{
	{
		ScheduledItem_t1912903245 * L_0 = ___left0;
		ScheduledItem_t1912903245 * L_1 = ___right1;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, ScheduledItem_t1912903245 * >::Invoke(4 /* System.Int32 UniRx.InternalUtil.ScheduledItem::CompareTo(UniRx.InternalUtil.ScheduledItem) */, L_0, L_1);
		return (bool)((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_GreaterThanOrEqual(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_GreaterThanOrEqual_m1401819658 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method)
{
	{
		ScheduledItem_t1912903245 * L_0 = ___left0;
		ScheduledItem_t1912903245 * L_1 = ___right1;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, ScheduledItem_t1912903245 * >::Invoke(4 /* System.Int32 UniRx.InternalUtil.ScheduledItem::CompareTo(UniRx.InternalUtil.ScheduledItem) */, L_0, L_1);
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_Equality(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_Equality_m868724262 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method)
{
	{
		ScheduledItem_t1912903245 * L_0 = ___left0;
		ScheduledItem_t1912903245 * L_1 = ___right1;
		bool L_2 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UniRx.InternalUtil.ScheduledItem::op_Inequality(UniRx.InternalUtil.ScheduledItem,UniRx.InternalUtil.ScheduledItem)
extern "C"  bool ScheduledItem_op_Inequality_m373540129 (Il2CppObject * __this /* static, unused */, ScheduledItem_t1912903245 * ___left0, ScheduledItem_t1912903245 * ___right1, const MethodInfo* method)
{
	{
		ScheduledItem_t1912903245 * L_0 = ___left0;
		ScheduledItem_t1912903245 * L_1 = ___right1;
		bool L_2 = ScheduledItem_op_Equality_m868724262(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UniRx.InternalUtil.SchedulerQueue::.ctor()
extern "C"  void SchedulerQueue__ctor_m2206597130 (SchedulerQueue_t3710535235 * __this, const MethodInfo* method)
{
	{
		SchedulerQueue__ctor_m3526489691(__this, ((int32_t)1024), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.SchedulerQueue::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppClass* PriorityQueue_1_t2411179237_il2cpp_TypeInfo_var;
extern const MethodInfo* PriorityQueue_1__ctor_m900436096_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t SchedulerQueue__ctor_m3526489691_MetadataUsageId;
extern "C"  void SchedulerQueue__ctor_m3526489691 (SchedulerQueue_t3710535235 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SchedulerQueue__ctor_m3526489691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, _stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		PriorityQueue_1_t2411179237 * L_3 = (PriorityQueue_1_t2411179237 *)il2cpp_codegen_object_new(PriorityQueue_1_t2411179237_il2cpp_TypeInfo_var);
		PriorityQueue_1__ctor_m900436096(L_3, L_2, /*hidden argument*/PriorityQueue_1__ctor_m900436096_MethodInfo_var);
		__this->set__queue_0(L_3);
		return;
	}
}
// System.Int32 UniRx.InternalUtil.SchedulerQueue::get_Count()
extern const MethodInfo* PriorityQueue_1_get_Count_m1827540733_MethodInfo_var;
extern const uint32_t SchedulerQueue_get_Count_m661226116_MetadataUsageId;
extern "C"  int32_t SchedulerQueue_get_Count_m661226116 (SchedulerQueue_t3710535235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SchedulerQueue_get_Count_m661226116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PriorityQueue_1_t2411179237 * L_0 = __this->get__queue_0();
		NullCheck(L_0);
		int32_t L_1 = PriorityQueue_1_get_Count_m1827540733(L_0, /*hidden argument*/PriorityQueue_1_get_Count_m1827540733_MethodInfo_var);
		return L_1;
	}
}
// System.Void UniRx.InternalUtil.SchedulerQueue::Enqueue(UniRx.InternalUtil.ScheduledItem)
extern const MethodInfo* PriorityQueue_1_Enqueue_m871286825_MethodInfo_var;
extern const uint32_t SchedulerQueue_Enqueue_m1094543559_MetadataUsageId;
extern "C"  void SchedulerQueue_Enqueue_m1094543559 (SchedulerQueue_t3710535235 * __this, ScheduledItem_t1912903245 * ___scheduledItem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SchedulerQueue_Enqueue_m1094543559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PriorityQueue_1_t2411179237 * L_0 = __this->get__queue_0();
		ScheduledItem_t1912903245 * L_1 = ___scheduledItem0;
		NullCheck(L_0);
		PriorityQueue_1_Enqueue_m871286825(L_0, L_1, /*hidden argument*/PriorityQueue_1_Enqueue_m871286825_MethodInfo_var);
		return;
	}
}
// System.Boolean UniRx.InternalUtil.SchedulerQueue::Remove(UniRx.InternalUtil.ScheduledItem)
extern const MethodInfo* PriorityQueue_1_Remove_m441804585_MethodInfo_var;
extern const uint32_t SchedulerQueue_Remove_m4278708089_MetadataUsageId;
extern "C"  bool SchedulerQueue_Remove_m4278708089 (SchedulerQueue_t3710535235 * __this, ScheduledItem_t1912903245 * ___scheduledItem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SchedulerQueue_Remove_m4278708089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PriorityQueue_1_t2411179237 * L_0 = __this->get__queue_0();
		ScheduledItem_t1912903245 * L_1 = ___scheduledItem0;
		NullCheck(L_0);
		bool L_2 = PriorityQueue_1_Remove_m441804585(L_0, L_1, /*hidden argument*/PriorityQueue_1_Remove_m441804585_MethodInfo_var);
		return L_2;
	}
}
// UniRx.InternalUtil.ScheduledItem UniRx.InternalUtil.SchedulerQueue::Dequeue()
extern const MethodInfo* PriorityQueue_1_Dequeue_m770213022_MethodInfo_var;
extern const uint32_t SchedulerQueue_Dequeue_m1988603796_MetadataUsageId;
extern "C"  ScheduledItem_t1912903245 * SchedulerQueue_Dequeue_m1988603796 (SchedulerQueue_t3710535235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SchedulerQueue_Dequeue_m1988603796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PriorityQueue_1_t2411179237 * L_0 = __this->get__queue_0();
		NullCheck(L_0);
		ScheduledItem_t1912903245 * L_1 = PriorityQueue_1_Dequeue_m770213022(L_0, /*hidden argument*/PriorityQueue_1_Dequeue_m770213022_MethodInfo_var);
		return L_1;
	}
}
// UniRx.InternalUtil.ScheduledItem UniRx.InternalUtil.SchedulerQueue::Peek()
extern const MethodInfo* PriorityQueue_1_Peek_m279483471_MethodInfo_var;
extern const uint32_t SchedulerQueue_Peek_m652492057_MetadataUsageId;
extern "C"  ScheduledItem_t1912903245 * SchedulerQueue_Peek_m652492057 (SchedulerQueue_t3710535235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SchedulerQueue_Peek_m652492057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PriorityQueue_1_t2411179237 * L_0 = __this->get__queue_0();
		NullCheck(L_0);
		ScheduledItem_t1912903245 * L_1 = PriorityQueue_1_Peek_m279483471(L_0, /*hidden argument*/PriorityQueue_1_Peek_m279483471_MethodInfo_var);
		return L_1;
	}
}
// System.Void UniRx.InternalUtil.ThreadSafeQueueWorker::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1U5BU5D_t3271680696_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeQueueWorker__ctor_m3609458254_MetadataUsageId;
extern "C"  void ThreadSafeQueueWorker__ctor_m3609458254 (ThreadSafeQueueWorker_t3889225637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeQueueWorker__ctor_m3609458254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_2(L_0);
		__this->set_actionList_5(((Action_1U5BU5D_t3271680696*)SZArrayNew(Action_1U5BU5D_t3271680696_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16))));
		__this->set_actionStates_6(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16))));
		__this->set_waitingList_8(((Action_1U5BU5D_t3271680696*)SZArrayNew(Action_1U5BU5D_t3271680696_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16))));
		__this->set_waitingStates_9(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16))));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.InternalUtil.ThreadSafeQueueWorker::Enqueue(System.Action`1<System.Object>,System.Object)
extern Il2CppClass* Action_1U5BU5D_t3271680696_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeQueueWorker_Enqueue_m1680239436_MetadataUsageId;
extern "C"  void ThreadSafeQueueWorker_Enqueue_m1680239436 (ThreadSafeQueueWorker_t3889225637 * __this, Action_1_t985559125 * ___action0, Il2CppObject * ___state1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeQueueWorker_Enqueue_m1680239436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Action_1U5BU5D_t3271680696* V_2 = NULL;
	ObjectU5BU5D_t11523773* V_3 = NULL;
	int32_t V_4 = 0;
	Action_1U5BU5D_t3271680696* V_5 = NULL;
	ObjectU5BU5D_t11523773* V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get_gate_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = __this->get_dequing_3();
			if (!L_2)
			{
				goto IL_00b4;
			}
		}

IL_0018:
		{
			Action_1U5BU5D_t3271680696* L_3 = __this->get_waitingList_8();
			NullCheck(L_3);
			int32_t L_4 = __this->get_waitingListCount_7();
			if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((uint32_t)L_4))))
			{
				goto IL_0085;
			}
		}

IL_002b:
		{
			int32_t L_5 = __this->get_waitingListCount_7();
			V_1 = ((int32_t)((int32_t)L_5*(int32_t)2));
			int32_t L_6 = V_1;
			if ((!(((uint32_t)L_6) > ((uint32_t)((int32_t)2146435071)))))
			{
				goto IL_0045;
			}
		}

IL_003f:
		{
			V_1 = ((int32_t)2146435071);
		}

IL_0045:
		{
			int32_t L_7 = V_1;
			V_2 = ((Action_1U5BU5D_t3271680696*)SZArrayNew(Action_1U5BU5D_t3271680696_il2cpp_TypeInfo_var, (uint32_t)L_7));
			int32_t L_8 = V_1;
			V_3 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_8));
			Action_1U5BU5D_t3271680696* L_9 = __this->get_waitingList_8();
			Action_1U5BU5D_t3271680696* L_10 = V_2;
			int32_t L_11 = __this->get_waitingListCount_7();
			Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, (Il2CppArray *)(Il2CppArray *)L_10, L_11, /*hidden argument*/NULL);
			ObjectU5BU5D_t11523773* L_12 = __this->get_waitingStates_9();
			ObjectU5BU5D_t11523773* L_13 = V_3;
			int32_t L_14 = __this->get_waitingListCount_7();
			Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_12, (Il2CppArray *)(Il2CppArray *)L_13, L_14, /*hidden argument*/NULL);
			Action_1U5BU5D_t3271680696* L_15 = V_2;
			__this->set_waitingList_8(L_15);
			ObjectU5BU5D_t11523773* L_16 = V_3;
			__this->set_waitingStates_9(L_16);
		}

IL_0085:
		{
			Action_1U5BU5D_t3271680696* L_17 = __this->get_waitingList_8();
			int32_t L_18 = __this->get_waitingListCount_7();
			Action_1_t985559125 * L_19 = ___action0;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
			ArrayElementTypeCheck (L_17, L_19);
			(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Action_1_t985559125 *)L_19);
			ObjectU5BU5D_t11523773* L_20 = __this->get_waitingStates_9();
			int32_t L_21 = __this->get_waitingListCount_7();
			Il2CppObject * L_22 = ___state1;
			NullCheck(L_20);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
			ArrayElementTypeCheck (L_20, L_22);
			(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_21), (Il2CppObject *)L_22);
			int32_t L_23 = __this->get_waitingListCount_7();
			__this->set_waitingListCount_7(((int32_t)((int32_t)L_23+(int32_t)1)));
			goto IL_0156;
		}

IL_00b4:
		{
			Action_1U5BU5D_t3271680696* L_24 = __this->get_actionList_5();
			NullCheck(L_24);
			int32_t L_25 = __this->get_actionListCount_4();
			if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))))) == ((uint32_t)L_25))))
			{
				goto IL_012c;
			}
		}

IL_00c7:
		{
			int32_t L_26 = __this->get_actionListCount_4();
			V_4 = ((int32_t)((int32_t)L_26*(int32_t)2));
			int32_t L_27 = V_4;
			if ((!(((uint32_t)L_27) > ((uint32_t)((int32_t)2146435071)))))
			{
				goto IL_00e4;
			}
		}

IL_00dd:
		{
			V_4 = ((int32_t)2146435071);
		}

IL_00e4:
		{
			int32_t L_28 = V_4;
			V_5 = ((Action_1U5BU5D_t3271680696*)SZArrayNew(Action_1U5BU5D_t3271680696_il2cpp_TypeInfo_var, (uint32_t)L_28));
			int32_t L_29 = V_4;
			V_6 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_29));
			Action_1U5BU5D_t3271680696* L_30 = __this->get_actionList_5();
			Action_1U5BU5D_t3271680696* L_31 = V_5;
			int32_t L_32 = __this->get_actionListCount_4();
			Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_30, (Il2CppArray *)(Il2CppArray *)L_31, L_32, /*hidden argument*/NULL);
			ObjectU5BU5D_t11523773* L_33 = __this->get_actionStates_6();
			ObjectU5BU5D_t11523773* L_34 = V_6;
			int32_t L_35 = __this->get_actionListCount_4();
			Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_33, (Il2CppArray *)(Il2CppArray *)L_34, L_35, /*hidden argument*/NULL);
			Action_1U5BU5D_t3271680696* L_36 = V_5;
			__this->set_actionList_5(L_36);
			ObjectU5BU5D_t11523773* L_37 = V_6;
			__this->set_actionStates_6(L_37);
		}

IL_012c:
		{
			Action_1U5BU5D_t3271680696* L_38 = __this->get_actionList_5();
			int32_t L_39 = __this->get_actionListCount_4();
			Action_1_t985559125 * L_40 = ___action0;
			NullCheck(L_38);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
			ArrayElementTypeCheck (L_38, L_40);
			(L_38)->SetAt(static_cast<il2cpp_array_size_t>(L_39), (Action_1_t985559125 *)L_40);
			ObjectU5BU5D_t11523773* L_41 = __this->get_actionStates_6();
			int32_t L_42 = __this->get_actionListCount_4();
			Il2CppObject * L_43 = ___state1;
			NullCheck(L_41);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
			ArrayElementTypeCheck (L_41, L_43);
			(L_41)->SetAt(static_cast<il2cpp_array_size_t>(L_42), (Il2CppObject *)L_43);
			int32_t L_44 = __this->get_actionListCount_4();
			__this->set_actionListCount_4(((int32_t)((int32_t)L_44+(int32_t)1)));
		}

IL_0156:
		{
			IL2CPP_LEAVE(0x162, FINALLY_015b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_015b;
	}

FINALLY_015b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_45 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(347)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(347)
	{
		IL2CPP_JUMP_TBL(0x162, IL_0162)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0162:
	{
		return;
	}
}
// System.Void UniRx.InternalUtil.ThreadSafeQueueWorker::ExecuteAll(System.Action`1<System.Exception>)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2789055860_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t ThreadSafeQueueWorker_ExecuteAll_m429924344_MetadataUsageId;
extern "C"  void ThreadSafeQueueWorker_ExecuteAll_m429924344 (ThreadSafeQueueWorker_t3889225637 * __this, Action_1_t2115686693 * ___unhandledExceptionCallback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeQueueWorker_ExecuteAll_m429924344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	Action_1_t985559125 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * V_5 = NULL;
	Action_1U5BU5D_t3271680696* V_6 = NULL;
	ObjectU5BU5D_t11523773* V_7 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get_gate_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = __this->get_actionListCount_4();
			if (L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0xF4, FINALLY_0029);
		}

IL_001d:
		{
			__this->set_dequing_3((bool)1);
			IL2CPP_LEAVE(0x30, FINALLY_0029);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(41)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0xF4, IL_00f4)
		IL2CPP_JUMP_TBL(0x30, IL_0030)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0030:
	{
		V_2 = 0;
		goto IL_007d;
	}

IL_0037:
	{
		Action_1U5BU5D_t3271680696* L_4 = __this->get_actionList_5();
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		ObjectU5BU5D_t11523773* L_7 = __this->get_actionStates_6();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_4 = ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			Action_1_t985559125 * L_10 = V_3;
			Il2CppObject * L_11 = V_4;
			NullCheck(L_10);
			Action_1_Invoke_m2789055860(L_10, L_11, /*hidden argument*/Action_1_Invoke_m2789055860_MethodInfo_var);
			IL2CPP_LEAVE(0x79, FINALLY_0066);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0057;
			throw e;
		}

CATCH_0057:
		{ // begin catch(System.Exception)
			V_5 = ((Exception_t1967233988 *)__exception_local);
			Action_1_t2115686693 * L_12 = ___unhandledExceptionCallback0;
			Exception_t1967233988 * L_13 = V_5;
			NullCheck(L_12);
			Action_1_Invoke_m1286428014(L_12, L_13, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
			IL2CPP_LEAVE(0x79, FINALLY_0066);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		Action_1U5BU5D_t3271680696* L_14 = __this->get_actionList_5();
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		ArrayElementTypeCheck (L_14, NULL);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Action_1_t985559125 *)NULL);
		ObjectU5BU5D_t11523773* L_16 = __this->get_actionStates_6();
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		ArrayElementTypeCheck (L_16, NULL);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Il2CppObject *)NULL);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0079:
	{
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_007d:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = __this->get_actionListCount_4();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0037;
		}
	}
	{
		Il2CppObject * L_21 = __this->get_gate_2();
		V_1 = L_21;
		Il2CppObject * L_22 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_0096:
	try
	{ // begin try (depth: 1)
		__this->set_dequing_3((bool)0);
		Action_1U5BU5D_t3271680696* L_23 = __this->get_actionList_5();
		V_6 = L_23;
		ObjectU5BU5D_t11523773* L_24 = __this->get_actionStates_6();
		V_7 = L_24;
		int32_t L_25 = __this->get_waitingListCount_7();
		__this->set_actionListCount_4(L_25);
		Action_1U5BU5D_t3271680696* L_26 = __this->get_waitingList_8();
		__this->set_actionList_5(L_26);
		ObjectU5BU5D_t11523773* L_27 = __this->get_waitingStates_9();
		__this->set_actionStates_6(L_27);
		__this->set_waitingListCount_7(0);
		Action_1U5BU5D_t3271680696* L_28 = V_6;
		__this->set_waitingList_8(L_28);
		ObjectU5BU5D_t11523773* L_29 = V_7;
		__this->set_waitingStates_9(L_29);
		IL2CPP_LEAVE(0xF4, FINALLY_00ed);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ed;
	}

FINALLY_00ed:
	{ // begin finally (depth: 1)
		Il2CppObject * L_30 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(237)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(237)
	{
		IL2CPP_JUMP_TBL(0xF4, IL_00f4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00f4:
	{
		return;
	}
}
// System.Void UniRx.IntReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t3455747825_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1270444133_MethodInfo_var;
extern const uint32_t IntReactiveProperty__ctor_m3453597618_MetadataUsageId;
extern "C"  void IntReactiveProperty__ctor_m3453597618 (IntReactiveProperty_t928974095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntReactiveProperty__ctor_m3453597618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t3455747825_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1270444133(__this, /*hidden argument*/ReactiveProperty_1__ctor_m1270444133_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.IntReactiveProperty::.ctor(System.Int32)
extern Il2CppClass* ReactiveProperty_1_t3455747825_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m729063833_MethodInfo_var;
extern const uint32_t IntReactiveProperty__ctor_m1739026947_MetadataUsageId;
extern "C"  void IntReactiveProperty__ctor_m1739026947 (IntReactiveProperty_t928974095 * __this, int32_t ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntReactiveProperty__ctor_m1739026947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t3455747825_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m729063833(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m729063833_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.LazyTask::.ctor()
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern const uint32_t LazyTask__ctor_m1095466110_MetadataUsageId;
extern "C"  void LazyTask__ctor_m1095466110 (LazyTask_t1365889643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask__ctor_m1095466110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BooleanDisposable_t3065601722 * L_0 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_0, /*hidden argument*/NULL);
		__this->set_cancellation_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.LazyTask/TaskStatus UniRx.LazyTask::get_Status()
extern "C"  int32_t LazyTask_get_Status_m3296471199 (LazyTask_t1365889643 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStatusU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.LazyTask::set_Status(UniRx.LazyTask/TaskStatus)
extern "C"  void LazyTask_set_Status_m280253608 (LazyTask_t1365889643 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStatusU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UniRx.LazyTask::Cancel()
extern "C"  void LazyTask_Cancel_m4175770560 (LazyTask_t1365889643 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = LazyTask_get_Status_m3296471199(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = LazyTask_get_Status_m3296471199(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0029;
		}
	}

IL_0017:
	{
		LazyTask_set_Status_m280253608(__this, 3, /*hidden argument*/NULL);
		BooleanDisposable_t3065601722 * L_2 = __this->get_cancellation_0();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.BooleanDisposable::Dispose() */, L_2);
	}

IL_0029:
	{
		return;
	}
}
// UnityEngine.Coroutine UniRx.LazyTask::WhenAll(UniRx.LazyTask[])
extern const MethodInfo* Enumerable_AsEnumerable_TisLazyTask_t1365889643_m2445115720_MethodInfo_var;
extern const uint32_t LazyTask_WhenAll_m1432274494_MetadataUsageId;
extern "C"  Coroutine_t2246592261 * LazyTask_WhenAll_m1432274494 (Il2CppObject * __this /* static, unused */, LazyTaskU5BU5D_t3442259850* ___tasks0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_WhenAll_m1432274494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LazyTaskU5BU5D_t3442259850* L_0 = ___tasks0;
		Il2CppObject* L_1 = Enumerable_AsEnumerable_TisLazyTask_t1365889643_m2445115720(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_0, /*hidden argument*/Enumerable_AsEnumerable_TisLazyTask_t1365889643_m2445115720_MethodInfo_var);
		Coroutine_t2246592261 * L_2 = LazyTask_WhenAll_m935861197(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Coroutine UniRx.LazyTask::WhenAll(System.Collections.Generic.IEnumerable`1<UniRx.LazyTask>)
extern Il2CppClass* LazyTask_t1365889643_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2681037974_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const MethodInfo* LazyTask_U3CWhenAllU3Em__A5_m3212194645_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1181321916_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisLazyTask_t1365889643_TisCoroutine_t2246592261_m2619238684_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisCoroutine_t2246592261_m1325913459_MethodInfo_var;
extern const uint32_t LazyTask_WhenAll_m935861197_MetadataUsageId;
extern "C"  Coroutine_t2246592261 * LazyTask_WhenAll_m935861197 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___tasks0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_WhenAll_m935861197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CoroutineU5BU5D_t1904733000* V_0 = NULL;
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___tasks0;
		Func_2_t2681037974 * L_1 = ((LazyTask_t1365889643_StaticFields*)LazyTask_t1365889643_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)LazyTask_U3CWhenAllU3Em__A5_m3212194645_MethodInfo_var);
		Func_2_t2681037974 * L_3 = (Func_2_t2681037974 *)il2cpp_codegen_object_new(Func_2_t2681037974_il2cpp_TypeInfo_var);
		Func_2__ctor_m1181321916(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m1181321916_MethodInfo_var);
		((LazyTask_t1365889643_StaticFields*)LazyTask_t1365889643_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_3);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t2681037974 * L_4 = ((LazyTask_t1365889643_StaticFields*)LazyTask_t1365889643_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		Il2CppObject* L_5 = Enumerable_Select_TisLazyTask_t1365889643_TisCoroutine_t2246592261_m2619238684(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Select_TisLazyTask_t1365889643_TisCoroutine_t2246592261_m2619238684_MethodInfo_var);
		CoroutineU5BU5D_t1904733000* L_6 = Enumerable_ToArray_TisCoroutine_t2246592261_m1325913459(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_ToArray_TisCoroutine_t2246592261_m1325913459_MethodInfo_var);
		V_0 = L_6;
		CoroutineU5BU5D_t1904733000* L_7 = V_0;
		Il2CppObject * L_8 = LazyTask_WhenAllCore_m3243040509(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Coroutine_t2246592261 * L_9 = MainThreadDispatcher_StartCoroutine_m3648496618(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Collections.IEnumerator UniRx.LazyTask::WhenAllCore(UnityEngine.Coroutine[])
extern Il2CppClass* U3CWhenAllCoreU3Ec__Iterator1F_t3942062194_il2cpp_TypeInfo_var;
extern const uint32_t LazyTask_WhenAllCore_m3243040509_MetadataUsageId;
extern "C"  Il2CppObject * LazyTask_WhenAllCore_m3243040509 (Il2CppObject * __this /* static, unused */, CoroutineU5BU5D_t1904733000* ___coroutines0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LazyTask_WhenAllCore_m3243040509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * V_0 = NULL;
	{
		U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * L_0 = (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 *)il2cpp_codegen_object_new(U3CWhenAllCoreU3Ec__Iterator1F_t3942062194_il2cpp_TypeInfo_var);
		U3CWhenAllCoreU3Ec__Iterator1F__ctor_m2052034791(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * L_1 = V_0;
		CoroutineU5BU5D_t1904733000* L_2 = ___coroutines0;
		NullCheck(L_1);
		L_1->set_coroutines_0(L_2);
		U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * L_3 = V_0;
		CoroutineU5BU5D_t1904733000* L_4 = ___coroutines0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ecoroutines_6(L_4);
		U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Coroutine UniRx.LazyTask::<WhenAll>m__A5(UniRx.LazyTask)
extern "C"  Coroutine_t2246592261 * LazyTask_U3CWhenAllU3Em__A5_m3212194645 (Il2CppObject * __this /* static, unused */, LazyTask_t1365889643 * ___x0, const MethodInfo* method)
{
	{
		LazyTask_t1365889643 * L_0 = ___x0;
		NullCheck(L_0);
		Coroutine_t2246592261 * L_1 = VirtFuncInvoker0< Coroutine_t2246592261 * >::Invoke(4 /* UnityEngine.Coroutine UniRx.LazyTask::Start() */, L_0);
		return L_1;
	}
}
// System.Void UniRx.LazyTask/<WhenAllCore>c__Iterator1F::.ctor()
extern "C"  void U3CWhenAllCoreU3Ec__Iterator1F__ctor_m2052034791 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.LazyTask/<WhenAllCore>c__Iterator1F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWhenAllCoreU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2963570187 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object UniRx.LazyTask/<WhenAllCore>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWhenAllCoreU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m217184159 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Boolean UniRx.LazyTask/<WhenAllCore>c__Iterator1F::MoveNext()
extern "C"  bool U3CWhenAllCoreU3Ec__Iterator1F_MoveNext_m3635524293 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_008c;
	}

IL_0021:
	{
		CoroutineU5BU5D_t1904733000* L_2 = __this->get_coroutines_0();
		__this->set_U3CU24s_339U3E__0_1(L_2);
		__this->set_U3CU24s_340U3E__1_2(0);
		goto IL_0072;
	}

IL_0039:
	{
		CoroutineU5BU5D_t1904733000* L_3 = __this->get_U3CU24s_339U3E__0_1();
		int32_t L_4 = __this->get_U3CU24s_340U3E__1_2();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U3CitemU3E__2_3(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		Coroutine_t2246592261 * L_6 = __this->get_U3CitemU3E__2_3();
		__this->set_U24current_5(L_6);
		__this->set_U24PC_4(1);
		goto IL_008e;
	}

IL_0064:
	{
		int32_t L_7 = __this->get_U3CU24s_340U3E__1_2();
		__this->set_U3CU24s_340U3E__1_2(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0072:
	{
		int32_t L_8 = __this->get_U3CU24s_340U3E__1_2();
		CoroutineU5BU5D_t1904733000* L_9 = __this->get_U3CU24s_339U3E__0_1();
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0039;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_008c:
	{
		return (bool)0;
	}

IL_008e:
	{
		return (bool)1;
	}
	// Dead block : IL_0090: ldloc.1
}
// System.Void UniRx.LazyTask/<WhenAllCore>c__Iterator1F::Dispose()
extern "C"  void U3CWhenAllCoreU3Ec__Iterator1F_Dispose_m513728356 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void UniRx.LazyTask/<WhenAllCore>c__Iterator1F::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CWhenAllCoreU3Ec__Iterator1F_Reset_m3993435028_MetadataUsageId;
extern "C"  void U3CWhenAllCoreU3Ec__Iterator1F_Reset_m3993435028 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWhenAllCoreU3Ec__Iterator1F_Reset_m3993435028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.LongReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t3455747920_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m2358756868_MethodInfo_var;
extern const uint32_t LongReactiveProperty__ctor_m1986312749_MetadataUsageId;
extern "C"  void LongReactiveProperty__ctor_m1986312749 (LongReactiveProperty_t1779754460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LongReactiveProperty__ctor_m1986312749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t3455747920_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m2358756868(__this, /*hidden argument*/ReactiveProperty_1__ctor_m2358756868_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.LongReactiveProperty::.ctor(System.Int64)
extern Il2CppClass* ReactiveProperty_1_t3455747920_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m107020250_MethodInfo_var;
extern const uint32_t LongReactiveProperty__ctor_m917741439_MetadataUsageId;
extern "C"  void LongReactiveProperty__ctor_m917741439 (LongReactiveProperty_t1779754460 * __this, int64_t ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LongReactiveProperty__ctor_m917741439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t3455747920_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m107020250(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m107020250_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::.ctor()
extern Il2CppClass* ThreadSafeQueueWorker_t3889225637_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2115686693_il2cpp_TypeInfo_var;
extern const MethodInfo* MainThreadDispatcher_U3CunhandledExceptionCallbackU3Em__A8_m3817152526_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m15890083_MethodInfo_var;
extern const uint32_t MainThreadDispatcher__ctor_m453573773_MetadataUsageId;
extern "C"  void MainThreadDispatcher__ctor_m453573773 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher__ctor_m453573773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MainThreadDispatcher_t4027217788 * G_B2_0 = NULL;
	MainThreadDispatcher_t4027217788 * G_B1_0 = NULL;
	{
		ThreadSafeQueueWorker_t3889225637 * L_0 = (ThreadSafeQueueWorker_t3889225637 *)il2cpp_codegen_object_new(ThreadSafeQueueWorker_t3889225637_il2cpp_TypeInfo_var);
		ThreadSafeQueueWorker__ctor_m3609458254(L_0, /*hidden argument*/NULL);
		__this->set_queueWorker_3(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Action_1_t2115686693 * L_1 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_14();
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_0024;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)MainThreadDispatcher_U3CunhandledExceptionCallbackU3Em__A8_m3817152526_MethodInfo_var);
		Action_1_t2115686693 * L_3 = (Action_1_t2115686693 *)il2cpp_codegen_object_new(Action_1_t2115686693_il2cpp_TypeInfo_var);
		Action_1__ctor_m15890083(L_3, NULL, L_2, /*hidden argument*/Action_1__ctor_m15890083_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheC_14(L_3);
		G_B2_0 = G_B1_0;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Action_1_t2115686693 * L_4 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_14();
		NullCheck(G_B2_0);
		G_B2_0->set_unhandledExceptionCallback_4(L_4);
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::.cctor()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher__cctor_m693788864_MetadataUsageId;
extern "C"  void MainThreadDispatcher__cctor_m693788864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher__cctor_m693788864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_cullingMode_2(1);
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::Post(System.Action`1<System.Object>,System.Object)
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher_Post_m2114450895_MetadataUsageId;
extern "C"  void MainThreadDispatcher_Post_m2114450895 (Il2CppObject * __this /* static, unused */, Action_1_t985559125 * ___action0, Il2CppObject * ___state1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Post_m2114450895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MainThreadDispatcher_t4027217788 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_isQuitting_7();
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_2 = V_0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_2, NULL, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_4 = V_0;
		NullCheck(L_4);
		ThreadSafeQueueWorker_t3889225637 * L_5 = L_4->get_queueWorker_3();
		Action_1_t985559125 * L_6 = ___action0;
		Il2CppObject * L_7 = ___state1;
		NullCheck(L_5);
		ThreadSafeQueueWorker_Enqueue_m1680239436(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::Send(System.Action`1<System.Object>,System.Object)
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2789055860_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_Send_m3742193335_MetadataUsageId;
extern "C"  void MainThreadDispatcher_Send_m3742193335 (Il2CppObject * __this /* static, unused */, Action_1_t985559125 * ___action0, Il2CppObject * ___state1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Send_m3742193335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	MainThreadDispatcher_t4027217788 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((MainThreadDispatcher_t4027217788_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var))->get_mainThreadToken_8();
		if (!L_0)
		{
			goto IL_003f;
		}
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		Action_1_t985559125 * L_1 = ___action0;
		Il2CppObject * L_2 = ___state1;
		NullCheck(L_1);
		Action_1_Invoke_m2789055860(L_1, L_2, /*hidden argument*/Action_1_Invoke_m2789055860_MethodInfo_var);
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t1967233988 *)__exception_local);
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
			MainThreadDispatcher_t4027217788 * L_3 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_1 = L_3;
			MainThreadDispatcher_t4027217788 * L_4 = V_1;
			bool L_5 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0035;
			}
		}

IL_0029:
		{
			MainThreadDispatcher_t4027217788 * L_6 = V_1;
			NullCheck(L_6);
			Action_1_t2115686693 * L_7 = L_6->get_unhandledExceptionCallback_4();
			Exception_t1967233988 * L_8 = V_0;
			NullCheck(L_7);
			Action_1_Invoke_m1286428014(L_7, L_8, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
		}

IL_0035:
		{
			goto IL_003a;
		}
	} // end catch (depth: 1)

IL_003a:
	{
		goto IL_0046;
	}

IL_003f:
	{
		Action_1_t985559125 * L_9 = ___action0;
		Il2CppObject * L_10 = ___state1;
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_Post_m2114450895(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::UnsafeSend(System.Action)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_UnsafeSend_m2650599480_MetadataUsageId;
extern "C"  void MainThreadDispatcher_UnsafeSend_m2650599480 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_UnsafeSend_m2650599480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	MainThreadDispatcher_t4027217788 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Action_t437523947 * L_0 = ___action0;
		NullCheck(L_0);
		Action_Invoke_m1445970038(L_0, /*hidden argument*/NULL);
		goto IL_002f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000b;
		throw e;
	}

CATCH_000b:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t1967233988 *)__exception_local);
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
			MainThreadDispatcher_t4027217788 * L_1 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_1 = L_1;
			MainThreadDispatcher_t4027217788 * L_2 = V_1;
			bool L_3 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_002a;
			}
		}

IL_001e:
		{
			MainThreadDispatcher_t4027217788 * L_4 = V_1;
			NullCheck(L_4);
			Action_1_t2115686693 * L_5 = L_4->get_unhandledExceptionCallback_4();
			Exception_t1967233988 * L_6 = V_0;
			NullCheck(L_5);
			Action_1_Invoke_m1286428014(L_5, L_6, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
		}

IL_002a:
		{
			goto IL_002f;
		}
	} // end catch (depth: 1)

IL_002f:
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::UnsafeSend(System.Action`1<System.Object>,System.Object)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2789055860_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_UnsafeSend_m3257912957_MetadataUsageId;
extern "C"  void MainThreadDispatcher_UnsafeSend_m3257912957 (Il2CppObject * __this /* static, unused */, Action_1_t985559125 * ___action0, Il2CppObject * ___state1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_UnsafeSend_m3257912957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	MainThreadDispatcher_t4027217788 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Action_1_t985559125 * L_0 = ___action0;
		Il2CppObject * L_1 = ___state1;
		NullCheck(L_0);
		Action_1_Invoke_m2789055860(L_0, L_1, /*hidden argument*/Action_1_Invoke_m2789055860_MethodInfo_var);
		goto IL_0030;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000c;
		throw e;
	}

CATCH_000c:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t1967233988 *)__exception_local);
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
			MainThreadDispatcher_t4027217788 * L_2 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_1 = L_2;
			MainThreadDispatcher_t4027217788 * L_3 = V_1;
			bool L_4 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_002b;
			}
		}

IL_001f:
		{
			MainThreadDispatcher_t4027217788 * L_5 = V_1;
			NullCheck(L_5);
			Action_1_t2115686693 * L_6 = L_5->get_unhandledExceptionCallback_4();
			Exception_t1967233988 * L_7 = V_0;
			NullCheck(L_6);
			Action_1_Invoke_m1286428014(L_6, L_7, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
		}

IL_002b:
		{
			goto IL_0030;
		}
	} // end catch (depth: 1)

IL_0030:
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::SendStartCoroutine(System.Collections.IEnumerator)
extern Il2CppClass* U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792_il2cpp_TypeInfo_var;
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSendStartCoroutineU3Ec__AnonStorey7D_U3CU3Em__A9_m1760013287_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_SendStartCoroutine_m2702036384_MetadataUsageId;
extern "C"  void MainThreadDispatcher_SendStartCoroutine_m2702036384 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_SendStartCoroutine_m2702036384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MainThreadDispatcher_t4027217788 * V_0 = NULL;
	U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * V_1 = NULL;
	{
		U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * L_0 = (U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 *)il2cpp_codegen_object_new(U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792_il2cpp_TypeInfo_var);
		U3CSendStartCoroutineU3Ec__AnonStorey7D__ctor_m252643564(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * L_1 = V_1;
		Il2CppObject * L_2 = ___routine0;
		NullCheck(L_1);
		L_1->set_routine_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Il2CppObject * L_3 = ((MainThreadDispatcher_t4027217788_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var))->get_mainThreadToken_8();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * L_4 = V_1;
		NullCheck(L_4);
		Il2CppObject * L_5 = L_4->get_routine_0();
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_StartCoroutine_m3648496618(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_005c;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_6 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_6;
		bool L_7 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_isQuitting_7();
		if (L_7)
		{
			goto IL_005c;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_8 = V_0;
		bool L_9 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_8, NULL, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_005c;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_10 = V_0;
		NullCheck(L_10);
		ThreadSafeQueueWorker_t3889225637 * L_11 = L_10->get_queueWorker_3();
		U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * L_12 = V_1;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CSendStartCoroutineU3Ec__AnonStorey7D_U3CU3Em__A9_m1760013287_MethodInfo_var);
		Action_1_t985559125 * L_14 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_14, L_12, L_13, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		NullCheck(L_11);
		ThreadSafeQueueWorker_Enqueue_m1680239436(L_11, L_14, NULL, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// UnityEngine.Coroutine UniRx.MainThreadDispatcher::StartCoroutine(System.Collections.IEnumerator)
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher_StartCoroutine_m3648496618_MetadataUsageId;
extern "C"  Coroutine_t2246592261 * MainThreadDispatcher_StartCoroutine_m3648496618 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_StartCoroutine_m3648496618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MainThreadDispatcher_t4027217788 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		MainThreadDispatcher_t4027217788 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_3 = V_0;
		Il2CppObject * L_4 = ___routine0;
		NullCheck(L_3);
		Coroutine_t2246592261 * L_5 = MonoBehaviour_StartCoroutine_Auto_m1831125106(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		return (Coroutine_t2246592261 *)NULL;
	}
}
// System.Void UniRx.MainThreadDispatcher::RegisterUnhandledExceptionCallback(System.Action`1<System.Exception>)
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Stubs_1_t3310424369_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher_RegisterUnhandledExceptionCallback_m1658193775_MetadataUsageId;
extern "C"  void MainThreadDispatcher_RegisterUnhandledExceptionCallback_m1658193775 (Il2CppObject * __this /* static, unused */, Action_1_t2115686693 * ___exceptionCallback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_RegisterUnhandledExceptionCallback_m1658193775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2115686693 * L_0 = ___exceptionCallback0;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_1 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Stubs_1_t3310424369_il2cpp_TypeInfo_var);
		Action_1_t2115686693 * L_2 = ((Stubs_1_t3310424369_StaticFields*)Stubs_1_t3310424369_il2cpp_TypeInfo_var->static_fields)->get_Ignore_0();
		NullCheck(L_1);
		L_1->set_unhandledExceptionCallback_4(L_2);
		goto IL_0025;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_3 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t2115686693 * L_4 = ___exceptionCallback0;
		NullCheck(L_3);
		L_3->set_unhandledExceptionCallback_4(L_4);
	}

IL_0025:
	{
		return;
	}
}
// System.String UniRx.MainThreadDispatcher::get_InstanceName()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral842556263;
extern const uint32_t MainThreadDispatcher_get_InstanceName_m1664795843_MetadataUsageId;
extern "C"  String_t* MainThreadDispatcher_get_InstanceName_m1664795843 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_get_InstanceName_m1664795843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		NullReferenceException_t3216235232 * L_2 = (NullReferenceException_t3216235232 *)il2cpp_codegen_object_new(NullReferenceException_t3216235232_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2208732056(L_2, _stringLiteral842556263, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_3 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m3709440845(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UniRx.MainThreadDispatcher::get_IsInitialized()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher_get_IsInitialized_m500759408_MetadataUsageId;
extern "C"  bool MainThreadDispatcher_get_IsInitialized_m500759408 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_get_IsInitialized_m500759408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		bool L_0 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_initialized_6();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_1 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_2 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// UniRx.MainThreadDispatcher UniRx.MainThreadDispatcher::get_Instance()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher_get_Instance_m2309518296_MetadataUsageId;
extern "C"  MainThreadDispatcher_t4027217788 * MainThreadDispatcher_get_Instance_m2309518296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_get_Instance_m2309518296_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_Initialize_m3023841831(NULL /*static, unused*/, /*hidden argument*/NULL);
		MainThreadDispatcher_t4027217788 * L_0 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		return L_0;
	}
}
// System.Void UniRx.MainThreadDispatcher::Initialize()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisMainThreadDispatcher_t4027217788_m4168215351_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMainThreadDispatcher_t4027217788_m1841224252_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral907652961;
extern Il2CppCodeGenString* _stringLiteral1471573066;
extern const uint32_t MainThreadDispatcher_Initialize_m3023841831_MetadataUsageId;
extern "C"  void MainThreadDispatcher_Initialize_m3023841831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Initialize_m3023841831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MainThreadDispatcher_t4027217788 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		bool L_0 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_initialized_6();
		if (L_0)
		{
			goto IL_0080;
		}
	}
	{
		V_0 = (MainThreadDispatcher_t4027217788 *)NULL;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		MainThreadDispatcher_t4027217788 * L_1 = Object_FindObjectOfType_TisMainThreadDispatcher_t4027217788_m4168215351(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisMainThreadDispatcher_t4027217788_m4168215351_MethodInfo_var);
		V_0 = L_1;
		goto IL_0030;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Object)
		{
			Exception_t1967233988 * L_2 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
			Exception__ctor_m3870771296(L_2, _stringLiteral907652961, /*hidden argument*/NULL);
			V_1 = L_2;
			Exception_t1967233988 * L_3 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogException_m248970745(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			Exception_t1967233988 * L_4 = V_1;
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
		}

IL_002b:
		{
			goto IL_0030;
		}
	} // end catch (depth: 1)

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		bool L_5 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_isQuitting_7();
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		MainThreadDispatcher_t4027217788 * L_6 = V_0;
		bool L_7 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0060;
		}
	}
	{
		GameObject_t4012695102 * L_8 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
		GameObject__ctor_m1336994365(L_8, _stringLiteral1471573066, /*hidden argument*/NULL);
		NullCheck(L_8);
		MainThreadDispatcher_t4027217788 * L_9 = GameObject_AddComponent_TisMainThreadDispatcher_t4027217788_m1841224252(L_8, /*hidden argument*/GameObject_AddComponent_TisMainThreadDispatcher_t4027217788_m1841224252_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_instance_5(L_9);
		goto IL_0066;
	}

IL_0060:
	{
		MainThreadDispatcher_t4027217788 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_instance_5(L_10);
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_11 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Il2CppObject * L_12 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_12, /*hidden argument*/NULL);
		((MainThreadDispatcher_t4027217788_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var))->set_mainThreadToken_8(L_12);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_initialized_6((bool)1);
	}

IL_0080:
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::Awake()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1378689472;
extern Il2CppCodeGenString* _stringLiteral3314041991;
extern Il2CppCodeGenString* _stringLiteral3403418743;
extern const uint32_t MainThreadDispatcher_Awake_m691178992_MetadataUsageId;
extern "C"  void MainThreadDispatcher_Awake_m691178992 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Awake_m691178992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_instance_5(__this);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_2, /*hidden argument*/NULL);
		((MainThreadDispatcher_t4027217788_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var))->set_mainThreadToken_8(L_2);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_initialized_6((bool)1);
		GameObject_t4012695102 * L_3 = Component_get_gameObject_m2112202034(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		int32_t L_4 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_cullingMode_2();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, _stringLiteral1378689472, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_DestroyDispatcher_m1615739690(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		int32_t L_5 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_cullingMode_2();
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, _stringLiteral3314041991, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_CullAllExcessDispatchers_m3500057855(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, _stringLiteral3403418743, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::DestroyDispatcher(UniRx.MainThreadDispatcher)
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t284553113_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponents_TisComponent_t2126946602_m2593473862_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_DestroyDispatcher_m1615739690_MetadataUsageId;
extern "C"  void MainThreadDispatcher_DestroyDispatcher_m1615739690 (Il2CppObject * __this /* static, unused */, MainThreadDispatcher_t4027217788 * ___aDispatcher0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_DestroyDispatcher_m1615739690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ComponentU5BU5D_t552366831* V_0 = NULL;
	{
		MainThreadDispatcher_t4027217788 * L_0 = ___aDispatcher0;
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_1 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_2 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_006a;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_3 = ___aDispatcher0;
		NullCheck(L_3);
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m2112202034(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		ComponentU5BU5D_t552366831* L_5 = GameObject_GetComponents_TisComponent_t2126946602_m2593473862(L_4, /*hidden argument*/GameObject_GetComponents_TisComponent_t2126946602_m2593473862_MethodInfo_var);
		V_0 = L_5;
		MainThreadDispatcher_t4027217788 * L_6 = ___aDispatcher0;
		NullCheck(L_6);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m2112202034(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t284553113 * L_8 = GameObject_get_transform_m1258834620(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m2107810675(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0064;
		}
	}
	{
		ComponentU5BU5D_t552366831* L_10 = V_0;
		NullCheck(L_10);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_0064;
		}
	}
	{
		ComponentU5BU5D_t552366831* L_11 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		if (!((Transform_t284553113 *)IsInstClass(((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12))), Transform_t284553113_il2cpp_TypeInfo_var)))
		{
			goto IL_005f;
		}
	}
	{
		ComponentU5BU5D_t552366831* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		if (!((MainThreadDispatcher_t4027217788 *)IsInstSealed(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))), MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var)))
		{
			goto IL_005f;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_15 = ___aDispatcher0;
		NullCheck(L_15);
		GameObject_t4012695102 * L_16 = Component_get_gameObject_m2112202034(L_15, /*hidden argument*/NULL);
		Object_Destroy_m3720418393(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_005f:
	{
		goto IL_006a;
	}

IL_0064:
	{
		MainThreadDispatcher_t4027217788 * L_17 = ___aDispatcher0;
		Object_Destroy_m3720418393(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::CullAllExcessDispatchers()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectsOfType_TisMainThreadDispatcher_t4027217788_m3133965346_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_CullAllExcessDispatchers_m3500057855_MetadataUsageId;
extern "C"  void MainThreadDispatcher_CullAllExcessDispatchers_m3500057855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_CullAllExcessDispatchers_m3500057855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MainThreadDispatcherU5BU5D_t933801749* V_0 = NULL;
	int32_t V_1 = 0;
	{
		MainThreadDispatcherU5BU5D_t933801749* L_0 = Object_FindObjectsOfType_TisMainThreadDispatcher_t4027217788_m3133965346(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisMainThreadDispatcher_t4027217788_m3133965346_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0019;
	}

IL_000d:
	{
		MainThreadDispatcherU5BU5D_t933801749* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_DestroyDispatcher_m1615739690(NULL /*static, unused*/, ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))), /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0019:
	{
		int32_t L_5 = V_1;
		MainThreadDispatcherU5BU5D_t933801749* L_6 = V_0;
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::OnDestroy()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisMainThreadDispatcher_t4027217788_m4168215351_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_OnDestroy_m3451896582_MetadataUsageId;
extern "C"  void MainThreadDispatcher_OnDestroy_m3451896582 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_OnDestroy_m3451896582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_2 = Object_FindObjectOfType_TisMainThreadDispatcher_t4027217788_m4168215351(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisMainThreadDispatcher_t4027217788_m4168215351_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_instance_5(L_2);
		MainThreadDispatcher_t4027217788 * L_3 = ((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->get_instance_5();
		bool L_4 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_initialized_6(L_4);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher::Update()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1286428014_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_Update_m2902747168_MetadataUsageId;
extern "C"  void MainThreadDispatcher_Update_m2902747168 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Update_m2902747168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Subject_1_t201353362 * L_0 = __this->get_update_9();
		if (!L_0)
		{
			goto IL_0032;
		}
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		Subject_1_t201353362 * L_1 = __this->get_update_9();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
		goto IL_0032;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0020;
		throw e;
	}

CATCH_0020:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t1967233988 *)__exception_local);
		Action_1_t2115686693 * L_3 = __this->get_unhandledExceptionCallback_4();
		Exception_t1967233988 * L_4 = V_0;
		NullCheck(L_3);
		Action_1_Invoke_m1286428014(L_3, L_4, /*hidden argument*/Action_1_Invoke_m1286428014_MethodInfo_var);
		goto IL_0032;
	} // end catch (depth: 1)

IL_0032:
	{
		ThreadSafeQueueWorker_t3889225637 * L_5 = __this->get_queueWorker_3();
		Action_1_t2115686693 * L_6 = __this->get_unhandledExceptionCallback_4();
		NullCheck(L_5);
		ThreadSafeQueueWorker_ExecuteAll_m429924344(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.MainThreadDispatcher::UpdateAsObservable()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_UpdateAsObservable_m46968669_MetadataUsageId;
extern "C"  Il2CppObject* MainThreadDispatcher_UpdateAsObservable_m46968669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_UpdateAsObservable_m46968669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Subject_1_t201353362 * L_1 = L_0->get_update_9();
		Subject_1_t201353362 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_3 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		Subject_1_t201353362 * L_4 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_4, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_5 = L_4;
		V_0 = L_5;
		NullCheck(L_3);
		L_3->set_update_9(L_5);
		Subject_1_t201353362 * L_6 = V_0;
		G_B2_0 = L_6;
	}

IL_0023:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.MainThreadDispatcher::LateUpdate()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_LateUpdate_m4171570662_MetadataUsageId;
extern "C"  void MainThreadDispatcher_LateUpdate_m4171570662 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_LateUpdate_m4171570662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_lateUpdate_10();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_lateUpdate_10();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.MainThreadDispatcher::LateUpdateAsObservable()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_LateUpdateAsObservable_m4009347107_MetadataUsageId;
extern "C"  Il2CppObject* MainThreadDispatcher_LateUpdateAsObservable_m4009347107 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_LateUpdateAsObservable_m4009347107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Subject_1_t201353362 * L_1 = L_0->get_lateUpdate_10();
		Subject_1_t201353362 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_3 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		Subject_1_t201353362 * L_4 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_4, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_5 = L_4;
		V_0 = L_5;
		NullCheck(L_3);
		L_3->set_lateUpdate_10(L_5);
		Subject_1_t201353362 * L_6 = V_0;
		G_B2_0 = L_6;
	}

IL_0023:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.MainThreadDispatcher::OnApplicationFocus(System.Boolean)
extern const MethodInfo* Subject_1_OnNext_m385413889_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_OnApplicationFocus_m1250469173_MetadataUsageId;
extern "C"  void MainThreadDispatcher_OnApplicationFocus_m1250469173 (MainThreadDispatcher_t4027217788 * __this, bool ___focus0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_OnApplicationFocus_m1250469173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2149039961 * L_0 = __this->get_onApplicationFocus_11();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2149039961 * L_1 = __this->get_onApplicationFocus_11();
		bool L_2 = ___focus0;
		NullCheck(L_1);
		Subject_1_OnNext_m385413889(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m385413889_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<System.Boolean> UniRx.MainThreadDispatcher::OnApplicationFocusAsObservable()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t2149039961_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m886839961_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_OnApplicationFocusAsObservable_m251107528_MetadataUsageId;
extern "C"  Il2CppObject* MainThreadDispatcher_OnApplicationFocusAsObservable_m251107528 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_OnApplicationFocusAsObservable_m251107528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2149039961 * V_0 = NULL;
	Subject_1_t2149039961 * G_B2_0 = NULL;
	Subject_1_t2149039961 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Subject_1_t2149039961 * L_1 = L_0->get_onApplicationFocus_11();
		Subject_1_t2149039961 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_3 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		Subject_1_t2149039961 * L_4 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(Subject_1_t2149039961_il2cpp_TypeInfo_var);
		Subject_1__ctor_m886839961(L_4, /*hidden argument*/Subject_1__ctor_m886839961_MethodInfo_var);
		Subject_1_t2149039961 * L_5 = L_4;
		V_0 = L_5;
		NullCheck(L_3);
		L_3->set_onApplicationFocus_11(L_5);
		Subject_1_t2149039961 * L_6 = V_0;
		G_B2_0 = L_6;
	}

IL_0023:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.MainThreadDispatcher::OnApplicationPause(System.Boolean)
extern const MethodInfo* Subject_1_OnNext_m385413889_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_OnApplicationPause_m2704636499_MetadataUsageId;
extern "C"  void MainThreadDispatcher_OnApplicationPause_m2704636499 (MainThreadDispatcher_t4027217788 * __this, bool ___pause0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_OnApplicationPause_m2704636499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2149039961 * L_0 = __this->get_onApplicationPause_12();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2149039961 * L_1 = __this->get_onApplicationPause_12();
		bool L_2 = ___pause0;
		NullCheck(L_1);
		Subject_1_OnNext_m385413889(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m385413889_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<System.Boolean> UniRx.MainThreadDispatcher::OnApplicationPauseAsObservable()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t2149039961_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m886839961_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_OnApplicationPauseAsObservable_m1450384742_MetadataUsageId;
extern "C"  Il2CppObject* MainThreadDispatcher_OnApplicationPauseAsObservable_m1450384742 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_OnApplicationPauseAsObservable_m1450384742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2149039961 * V_0 = NULL;
	Subject_1_t2149039961 * G_B2_0 = NULL;
	Subject_1_t2149039961 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Subject_1_t2149039961 * L_1 = L_0->get_onApplicationPause_12();
		Subject_1_t2149039961 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_3 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		Subject_1_t2149039961 * L_4 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(Subject_1_t2149039961_il2cpp_TypeInfo_var);
		Subject_1__ctor_m886839961(L_4, /*hidden argument*/Subject_1__ctor_m886839961_MethodInfo_var);
		Subject_1_t2149039961 * L_5 = L_4;
		V_0 = L_5;
		NullCheck(L_3);
		L_3->set_onApplicationPause_12(L_5);
		Subject_1_t2149039961 * L_6 = V_0;
		G_B2_0 = L_6;
	}

IL_0023:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.MainThreadDispatcher::OnApplicationQuit()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_OnApplicationQuit_m346160779_MetadataUsageId;
extern "C"  void MainThreadDispatcher_OnApplicationQuit_m346160779 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_OnApplicationQuit_m346160779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		((MainThreadDispatcher_t4027217788_StaticFields*)MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var->static_fields)->set_isQuitting_7((bool)1);
		Subject_1_t201353362 * L_0 = __this->get_onApplicationQuit_13();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onApplicationQuit_13();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_0021:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.MainThreadDispatcher::OnApplicationQuitAsObservable()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_OnApplicationQuitAsObservable_m1999083704_MetadataUsageId;
extern "C"  Il2CppObject* MainThreadDispatcher_OnApplicationQuitAsObservable_m1999083704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_OnApplicationQuitAsObservable_m1999083704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Subject_1_t201353362 * L_1 = L_0->get_onApplicationQuit_13();
		Subject_1_t201353362 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_3 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		Subject_1_t201353362 * L_4 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_4, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_5 = L_4;
		V_0 = L_5;
		NullCheck(L_3);
		L_3->set_onApplicationQuit_13(L_5);
		Subject_1_t201353362 * L_6 = V_0;
		G_B2_0 = L_6;
	}

IL_0023:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.MainThreadDispatcher::<unhandledExceptionCallback>m__A8(System.Exception)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher_U3CunhandledExceptionCallbackU3Em__A8_m3817152526_MetadataUsageId;
extern "C"  void MainThreadDispatcher_U3CunhandledExceptionCallbackU3Em__A8_m3817152526 (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * ___ex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_U3CunhandledExceptionCallbackU3Em__A8_m3817152526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = ___ex0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogException_m248970745(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey7D::.ctor()
extern "C"  void U3CSendStartCoroutineU3Ec__AnonStorey7D__ctor_m252643564 (U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey7D::<>m__A9(System.Object)
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendStartCoroutineU3Ec__AnonStorey7D_U3CU3Em__A9_m1760013287_MetadataUsageId;
extern "C"  void U3CSendStartCoroutineU3Ec__AnonStorey7D_U3CU3Em__A9_m1760013287 (U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSendStartCoroutineU3Ec__AnonStorey7D_U3CU3Em__A9_m1760013287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MainThreadDispatcher_t4027217788 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		MainThreadDispatcher_t4027217788 * L_0 = MainThreadDispatcher_get_Instance_m2309518296(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		MainThreadDispatcher_t4027217788 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		MainThreadDispatcher_t4027217788 * L_3 = V_0;
		Il2CppObject * L_4 = __this->get_routine_0();
		NullCheck(L_3);
		MonoBehaviour_StartCoroutine_Auto_m1831125106(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void UniRx.MessageBroker::.ctor()
extern Il2CppClass* Dictionary_2_t2949654135_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2428970071_MethodInfo_var;
extern const uint32_t MessageBroker__ctor_m402680655_MetadataUsageId;
extern "C"  void MessageBroker__ctor_m402680655 (MessageBroker_t1308900818 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageBroker__ctor_m402680655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2949654135 * L_0 = (Dictionary_2_t2949654135 *)il2cpp_codegen_object_new(Dictionary_2_t2949654135_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2428970071(L_0, /*hidden argument*/Dictionary_2__ctor_m2428970071_MethodInfo_var);
		__this->set_notifiers_2(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.MessageBroker::.cctor()
extern Il2CppClass* MessageBroker_t1308900818_il2cpp_TypeInfo_var;
extern const uint32_t MessageBroker__cctor_m3411069502_MetadataUsageId;
extern "C"  void MessageBroker__cctor_m3411069502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MessageBroker__cctor_m3411069502_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MessageBroker_t1308900818 * L_0 = (MessageBroker_t1308900818 *)il2cpp_codegen_object_new(MessageBroker_t1308900818_il2cpp_TypeInfo_var);
		MessageBroker__ctor_m402680655(L_0, /*hidden argument*/NULL);
		((MessageBroker_t1308900818_StaticFields*)MessageBroker_t1308900818_il2cpp_TypeInfo_var->static_fields)->set_Default_0(L_0);
		return;
	}
}
// System.Void UniRx.MessageBroker::Dispose()
extern "C"  void MessageBroker_Dispose_m327335884 (MessageBroker_t1308900818 * __this, const MethodInfo* method)
{
	Dictionary_2_t2949654135 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2949654135 * L_0 = __this->get_notifiers_2();
		V_0 = L_0;
		Dictionary_2_t2949654135 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = __this->get_isDisposed_1();
			if (L_2)
			{
				goto IL_002a;
			}
		}

IL_0018:
		{
			__this->set_isDisposed_1((bool)1);
			Dictionary_2_t2949654135 * L_3 = __this->get_notifiers_2();
			NullCheck(L_3);
			VirtActionInvoker0::Invoke(33 /* System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::Clear() */, L_3);
		}

IL_002a:
		{
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		Dictionary_2_t2949654135 * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0036:
	{
		return;
	}
}
// System.Void UniRx.MultilineReactivePropertyAttribute::.ctor()
extern "C"  void MultilineReactivePropertyAttribute__ctor_m117944694 (MultilineReactivePropertyAttribute_t2518291827 * __this, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		MultilineReactivePropertyAttribute_set_Lines_m1542825415(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.MultilineReactivePropertyAttribute::.ctor(System.Int32)
extern "C"  void MultilineReactivePropertyAttribute__ctor_m1392043463 (MultilineReactivePropertyAttribute_t2518291827 * __this, int32_t ___lines0, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___lines0;
		MultilineReactivePropertyAttribute_set_Lines_m1542825415(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UniRx.MultilineReactivePropertyAttribute::get_Lines()
extern "C"  int32_t MultilineReactivePropertyAttribute_get_Lines_m2975535156 (MultilineReactivePropertyAttribute_t2518291827 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CLinesU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.MultilineReactivePropertyAttribute::set_Lines(System.Int32)
extern "C"  void MultilineReactivePropertyAttribute_set_Lines_m1542825415 (MultilineReactivePropertyAttribute_t2518291827 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CLinesU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UniRx.MultipleAssignmentDisposable::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t MultipleAssignmentDisposable__ctor_m4117172186_MetadataUsageId;
extern "C"  void MultipleAssignmentDisposable__ctor_m4117172186 (MultipleAssignmentDisposable_t3090232719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultipleAssignmentDisposable__ctor_m4117172186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_1(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.MultipleAssignmentDisposable::.cctor()
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern Il2CppClass* MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var;
extern const uint32_t MultipleAssignmentDisposable__cctor_m2596189971_MetadataUsageId;
extern "C"  void MultipleAssignmentDisposable__cctor_m2596189971 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultipleAssignmentDisposable__cctor_m2596189971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BooleanDisposable_t3065601722 * L_0 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m3878547102(L_0, (bool)1, /*hidden argument*/NULL);
		((MultipleAssignmentDisposable_t3090232719_StaticFields*)MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var->static_fields)->set_True_0(L_0);
		return;
	}
}
// System.Boolean UniRx.MultipleAssignmentDisposable::get_IsDisposed()
extern Il2CppClass* MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var;
extern const uint32_t MultipleAssignmentDisposable_get_IsDisposed_m3534532222_MetadataUsageId;
extern "C"  bool MultipleAssignmentDisposable_get_IsDisposed_m3534532222 (MultipleAssignmentDisposable_t3090232719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultipleAssignmentDisposable_get_IsDisposed_m3534532222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get_gate_1();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_current_2();
			IL2CPP_RUNTIME_CLASS_INIT(MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var);
			BooleanDisposable_t3065601722 * L_3 = ((MultipleAssignmentDisposable_t3090232719_StaticFields*)MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var->static_fields)->get_True_0();
			V_1 = (bool)((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(BooleanDisposable_t3065601722 *)L_3))? 1 : 0);
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		}

IL_0020:
		{
			; // IL_0020: leave IL_002c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002c:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.IDisposable UniRx.MultipleAssignmentDisposable::get_Disposable()
extern Il2CppClass* MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t MultipleAssignmentDisposable_get_Disposable_m1668944462_MetadataUsageId;
extern "C"  Il2CppObject * MultipleAssignmentDisposable_get_Disposable_m1668944462 (MultipleAssignmentDisposable_t3090232719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultipleAssignmentDisposable_get_Disposable_m1668944462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject * G_B4_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_gate_1();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_current_2();
			IL2CPP_RUNTIME_CLASS_INIT(MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var);
			BooleanDisposable_t3065601722 * L_3 = ((MultipleAssignmentDisposable_t3090232719_StaticFields*)MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var->static_fields)->get_True_0();
			if ((!(((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(BooleanDisposable_t3065601722 *)L_3))))
			{
				goto IL_0029;
			}
		}

IL_001d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
			Il2CppObject * L_4 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
			V_1 = L_4;
			Il2CppObject * L_5 = V_1;
			G_B4_0 = L_5;
			goto IL_002f;
		}

IL_0029:
		{
			Il2CppObject * L_6 = __this->get_current_2();
			G_B4_0 = L_6;
		}

IL_002f:
		{
			V_2 = G_B4_0;
			IL2CPP_LEAVE(0x41, FINALLY_003a);
		}

IL_0035:
		{
			; // IL_0035: leave IL_0041
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		Il2CppObject * L_8 = V_2;
		return L_8;
	}
}
// System.Void UniRx.MultipleAssignmentDisposable::set_Disposable(System.IDisposable)
extern Il2CppClass* MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t MultipleAssignmentDisposable_set_Disposable_m2693870941_MetadataUsageId;
extern "C"  void MultipleAssignmentDisposable_set_Disposable_m2693870941 (MultipleAssignmentDisposable_t3090232719 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultipleAssignmentDisposable_set_Disposable_m2693870941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		Il2CppObject * L_0 = __this->get_gate_1();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_current_2();
			IL2CPP_RUNTIME_CLASS_INIT(MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var);
			BooleanDisposable_t3065601722 * L_3 = ((MultipleAssignmentDisposable_t3090232719_StaticFields*)MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var->static_fields)->get_True_0();
			V_0 = (bool)((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(BooleanDisposable_t3065601722 *)L_3))? 1 : 0);
			bool L_4 = V_0;
			if (L_4)
			{
				goto IL_002a;
			}
		}

IL_0023:
		{
			Il2CppObject * L_5 = ___value0;
			__this->set_current_2(L_5);
		}

IL_002a:
		{
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0036:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject * L_8 = ___value0;
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject * L_9 = ___value0;
		NullCheck(L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_9);
	}

IL_0048:
	{
		return;
	}
}
// System.Void UniRx.MultipleAssignmentDisposable::Dispose()
extern Il2CppClass* MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t MultipleAssignmentDisposable_Dispose_m835874199_MetadataUsageId;
extern "C"  void MultipleAssignmentDisposable_Dispose_m835874199 (MultipleAssignmentDisposable_t3090232719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MultipleAssignmentDisposable_Dispose_m835874199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Il2CppObject *)NULL;
		Il2CppObject * L_0 = __this->get_gate_1();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_current_2();
			IL2CPP_RUNTIME_CLASS_INIT(MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var);
			BooleanDisposable_t3065601722 * L_3 = ((MultipleAssignmentDisposable_t3090232719_StaticFields*)MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var->static_fields)->get_True_0();
			if ((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(BooleanDisposable_t3065601722 *)L_3)))
			{
				goto IL_0031;
			}
		}

IL_001f:
		{
			Il2CppObject * L_4 = __this->get_current_2();
			V_0 = L_4;
			IL2CPP_RUNTIME_CLASS_INIT(MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var);
			BooleanDisposable_t3065601722 * L_5 = ((MultipleAssignmentDisposable_t3090232719_StaticFields*)MultipleAssignmentDisposable_t3090232719_il2cpp_TypeInfo_var->static_fields)->get_True_0();
			__this->set_current_2(L_5);
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		Il2CppObject * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Il2CppObject * L_8 = V_0;
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_8);
	}

IL_0049:
	{
		return;
	}
}
// System.Void UniRx.Observable::.cctor()
extern const Il2CppType* WWW_t1522972100_0_0_0_var;
extern const Il2CppType* WaitForEndOfFrame_t1917318876_0_0_0_var;
extern const Il2CppType* WaitForFixedUpdate_t896427542_0_0_0_var;
extern const Il2CppType* WaitForSeconds_t1291133240_0_0_0_var;
extern const Il2CppType* AsyncOperation_t3374395064_0_0_0_var;
extern const Il2CppType* Coroutine_t2246592261_0_0_0_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* HashSet_1_t1182951310_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m2497519596_MethodInfo_var;
extern const MethodInfo* HashSet_1_Add_m3512972152_MethodInfo_var;
extern const uint32_t Observable__cctor_m277909913_MetadataUsageId;
extern "C"  void Observable__cctor_m277909913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable__cctor_m277909913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HashSet_1_t1182951310 * V_0 = NULL;
	{
		TimeSpan_t763862892  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TimeSpan__ctor_m3191091631(&L_0, 0, 0, 0, 0, (-1), /*hidden argument*/NULL);
		((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->set_InfiniteTimeSpan_0(L_0);
		HashSet_1_t1182951310 * L_1 = (HashSet_1_t1182951310 *)il2cpp_codegen_object_new(HashSet_1_t1182951310_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m2497519596(L_1, /*hidden argument*/HashSet_1__ctor_m2497519596_MethodInfo_var);
		V_0 = L_1;
		HashSet_1_t1182951310 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(WWW_t1522972100_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		HashSet_1_Add_m3512972152(L_2, L_3, /*hidden argument*/HashSet_1_Add_m3512972152_MethodInfo_var);
		HashSet_1_t1182951310 * L_4 = V_0;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(WaitForEndOfFrame_t1917318876_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		HashSet_1_Add_m3512972152(L_4, L_5, /*hidden argument*/HashSet_1_Add_m3512972152_MethodInfo_var);
		HashSet_1_t1182951310 * L_6 = V_0;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(WaitForFixedUpdate_t896427542_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		HashSet_1_Add_m3512972152(L_6, L_7, /*hidden argument*/HashSet_1_Add_m3512972152_MethodInfo_var);
		HashSet_1_t1182951310 * L_8 = V_0;
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(WaitForSeconds_t1291133240_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		HashSet_1_Add_m3512972152(L_8, L_9, /*hidden argument*/HashSet_1_Add_m3512972152_MethodInfo_var);
		HashSet_1_t1182951310 * L_10 = V_0;
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AsyncOperation_t3374395064_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		HashSet_1_Add_m3512972152(L_10, L_11, /*hidden argument*/HashSet_1_Add_m3512972152_MethodInfo_var);
		HashSet_1_t1182951310 * L_12 = V_0;
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Coroutine_t2246592261_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_12);
		HashSet_1_Add_m3512972152(L_12, L_13, /*hidden argument*/HashSet_1_Add_m3512972152_MethodInfo_var);
		HashSet_1_t1182951310 * L_14 = V_0;
		((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->set_YieldInstructionTypes_1(L_14);
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::ReturnUnit()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var;
extern const uint32_t Observable_ReturnUnit_m2015160972_MetadataUsageId;
extern "C"  Il2CppObject* Observable_ReturnUnit_m2015160972 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_ReturnUnit_m2015160972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_0 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_1 = Observable_Return_TisUnit_t2558286038_m1480352924(NULL /*static, unused*/, L_0, /*hidden argument*/Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var);
		return L_1;
	}
}
// UniRx.IObservable`1<System.Int32> UniRx.Observable::Range(System.Int32,System.Int32)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t Observable_Range_m751165906_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Range_m751165906 (Il2CppObject * __this /* static, unused */, int32_t ___start0, int32_t ___count1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Range_m751165906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___start0;
		int32_t L_1 = ___count1;
		Il2CppObject * L_2 = DefaultSchedulers_get_Iteration_m2327009079(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_3 = Observable_Range_m4135749642(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UniRx.IObservable`1<System.Int32> UniRx.Observable::Range(System.Int32,System.Int32,UniRx.IScheduler)
extern Il2CppClass* RangeObservable_t282483469_il2cpp_TypeInfo_var;
extern const uint32_t Observable_Range_m4135749642_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Range_m4135749642 (Il2CppObject * __this /* static, unused */, int32_t ___start0, int32_t ___count1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Range_m4135749642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___start0;
		int32_t L_1 = ___count1;
		Il2CppObject * L_2 = ___scheduler2;
		RangeObservable_t282483469 * L_3 = (RangeObservable_t282483469 *)il2cpp_codegen_object_new(RangeObservable_t282483469_il2cpp_TypeInfo_var);
		RangeObservable__ctor_m1043085892(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::Start(System.Action)
extern Il2CppClass* Nullable_1_t3649900800_il2cpp_TypeInfo_var;
extern Il2CppClass* StartObservable_1_t4083709414_il2cpp_TypeInfo_var;
extern const MethodInfo* StartObservable_1__ctor_m1869350548_MethodInfo_var;
extern const uint32_t Observable_Start_m81987121_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Start_m81987121 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Start_m81987121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Action_t437523947 * L_0 = ___action0;
		Initobj (Nullable_1_t3649900800_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t3649900800  L_1 = V_0;
		Il2CppObject * L_2 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
		StartObservable_1_t4083709414 * L_3 = (StartObservable_1_t4083709414 *)il2cpp_codegen_object_new(StartObservable_1_t4083709414_il2cpp_TypeInfo_var);
		StartObservable_1__ctor_m1869350548(L_3, L_0, L_1, L_2, /*hidden argument*/StartObservable_1__ctor_m1869350548_MethodInfo_var);
		return L_3;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::Start(System.Action,System.TimeSpan)
extern Il2CppClass* StartObservable_1_t4083709414_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const MethodInfo* StartObservable_1__ctor_m1869350548_MethodInfo_var;
extern const uint32_t Observable_Start_m775422087_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Start_m775422087 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, TimeSpan_t763862892  ___timeSpan1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Start_m775422087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ___action0;
		TimeSpan_t763862892  L_1 = ___timeSpan1;
		Nullable_1_t3649900800  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m4008503583(&L_2, L_1, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		Il2CppObject * L_3 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
		StartObservable_1_t4083709414 * L_4 = (StartObservable_1_t4083709414 *)il2cpp_codegen_object_new(StartObservable_1_t4083709414_il2cpp_TypeInfo_var);
		StartObservable_1__ctor_m1869350548(L_4, L_0, L_2, L_3, /*hidden argument*/StartObservable_1__ctor_m1869350548_MethodInfo_var);
		return L_4;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::Start(System.Action,UniRx.IScheduler)
extern Il2CppClass* Nullable_1_t3649900800_il2cpp_TypeInfo_var;
extern Il2CppClass* StartObservable_1_t4083709414_il2cpp_TypeInfo_var;
extern const MethodInfo* StartObservable_1__ctor_m1869350548_MethodInfo_var;
extern const uint32_t Observable_Start_m838126987_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Start_m838126987 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Start_m838126987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Action_t437523947 * L_0 = ___action0;
		Initobj (Nullable_1_t3649900800_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t3649900800  L_1 = V_0;
		Il2CppObject * L_2 = ___scheduler1;
		StartObservable_1_t4083709414 * L_3 = (StartObservable_1_t4083709414 *)il2cpp_codegen_object_new(StartObservable_1_t4083709414_il2cpp_TypeInfo_var);
		StartObservable_1__ctor_m1869350548(L_3, L_0, L_1, L_2, /*hidden argument*/StartObservable_1__ctor_m1869350548_MethodInfo_var);
		return L_3;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::Start(System.Action,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* StartObservable_1_t4083709414_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const MethodInfo* StartObservable_1__ctor_m1869350548_MethodInfo_var;
extern const uint32_t Observable_Start_m3673491957_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Start_m3673491957 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, TimeSpan_t763862892  ___timeSpan1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Start_m3673491957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ___action0;
		TimeSpan_t763862892  L_1 = ___timeSpan1;
		Nullable_1_t3649900800  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m4008503583(&L_2, L_1, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		Il2CppObject * L_3 = ___scheduler2;
		StartObservable_1_t4083709414 * L_4 = (StartObservable_1_t4083709414 *)il2cpp_codegen_object_new(StartObservable_1_t4083709414_il2cpp_TypeInfo_var);
		StartObservable_1__ctor_m1869350548(L_4, L_0, L_2, L_3, /*hidden argument*/StartObservable_1__ctor_m1869350548_MethodInfo_var);
		return L_4;
	}
}
// System.Func`1<UniRx.IObservable`1<UniRx.Unit>> UniRx.Observable::ToAsync(System.Action)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t Observable_ToAsync_m2679241012_MetadataUsageId;
extern "C"  Func_1_t3459865649 * Observable_ToAsync_m2679241012 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_ToAsync_m2679241012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ___action0;
		Il2CppObject * L_1 = DefaultSchedulers_get_AsyncConversions_m4176597513(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_1_t3459865649 * L_2 = Observable_ToAsync_m2487214312(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Func`1<UniRx.IObservable`1<UniRx.Unit>> UniRx.Observable::ToAsync(System.Action,UniRx.IScheduler)
extern Il2CppClass* U3CToAsyncU3Ec__AnonStorey3A_t2315298076_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t3459865649_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CToAsyncU3Ec__AnonStorey3A_U3CU3Em__3D_m2211096745_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m3527858030_MethodInfo_var;
extern const uint32_t Observable_ToAsync_m2487214312_MetadataUsageId;
extern "C"  Func_1_t3459865649 * Observable_ToAsync_m2487214312 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_ToAsync_m2487214312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * V_0 = NULL;
	{
		U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * L_0 = (U3CToAsyncU3Ec__AnonStorey3A_t2315298076 *)il2cpp_codegen_object_new(U3CToAsyncU3Ec__AnonStorey3A_t2315298076_il2cpp_TypeInfo_var);
		U3CToAsyncU3Ec__AnonStorey3A__ctor_m3205604775(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * L_1 = V_0;
		Il2CppObject * L_2 = ___scheduler1;
		NullCheck(L_1);
		L_1->set_scheduler_0(L_2);
		U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * L_3 = V_0;
		Action_t437523947 * L_4 = ___action0;
		NullCheck(L_3);
		L_3->set_action_1(L_4);
		U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CToAsyncU3Ec__AnonStorey3A_U3CU3Em__3D_m2211096745_MethodInfo_var);
		Func_1_t3459865649 * L_7 = (Func_1_t3459865649 *)il2cpp_codegen_object_new(Func_1_t3459865649_il2cpp_TypeInfo_var);
		Func_1__ctor_m3527858030(L_7, L_5, L_6, /*hidden argument*/Func_1__ctor_m3527858030_MethodInfo_var);
		return L_7;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::FromEvent(System.Action`1<System.Action>,System.Action`1<System.Action>)
extern Il2CppClass* FromEventObservable_t1591372576_il2cpp_TypeInfo_var;
extern const uint32_t Observable_FromEvent_m3570768044_MetadataUsageId;
extern "C"  Il2CppObject* Observable_FromEvent_m3570768044 (Il2CppObject * __this /* static, unused */, Action_1_t585976652 * ___addHandler0, Action_1_t585976652 * ___removeHandler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_FromEvent_m3570768044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t585976652 * L_0 = ___addHandler0;
		Action_1_t585976652 * L_1 = ___removeHandler1;
		FromEventObservable_t1591372576 * L_2 = (FromEventObservable_t1591372576 *)il2cpp_codegen_object_new(FromEventObservable_t1591372576_il2cpp_TypeInfo_var);
		FromEventObservable__ctor_m843502955(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Func`1<UniRx.IObservable`1<UniRx.Unit>> UniRx.Observable::FromAsyncPattern(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Action`1<System.IAsyncResult>)
extern Il2CppClass* U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2571569893_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CFromAsyncPatternU3Ec__AnonStorey45_U3CU3Em__43_m1918899173_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1937720617_MethodInfo_var;
extern const MethodInfo* Observable_FromAsyncPattern_TisUnit_t2558286038_m4118748633_MethodInfo_var;
extern const uint32_t Observable_FromAsyncPattern_m1320550440_MetadataUsageId;
extern "C"  Func_1_t3459865649 * Observable_FromAsyncPattern_m1320550440 (Il2CppObject * __this /* static, unused */, Func_3_t3381739440 * ___begin0, Action_1_t686135974 * ___end1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_FromAsyncPattern_m1320550440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 * V_0 = NULL;
	{
		U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 * L_0 = (U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 *)il2cpp_codegen_object_new(U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396_il2cpp_TypeInfo_var);
		U3CFromAsyncPatternU3Ec__AnonStorey45__ctor_m2339631455(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 * L_1 = V_0;
		Action_1_t686135974 * L_2 = ___end1;
		NullCheck(L_1);
		L_1->set_end_0(L_2);
		Func_3_t3381739440 * L_3 = ___begin0;
		U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CFromAsyncPatternU3Ec__AnonStorey45_U3CU3Em__43_m1918899173_MethodInfo_var);
		Func_2_t2571569893 * L_6 = (Func_2_t2571569893 *)il2cpp_codegen_object_new(Func_2_t2571569893_il2cpp_TypeInfo_var);
		Func_2__ctor_m1937720617(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m1937720617_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_1_t3459865649 * L_7 = Observable_FromAsyncPattern_TisUnit_t2558286038_m4118748633(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Observable_FromAsyncPattern_TisUnit_t2558286038_m4118748633_MethodInfo_var);
		return L_7;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Interval(System.TimeSpan)
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t Observable_Interval_m2492905901_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Interval_m2492905901 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___period0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Interval_m2492905901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpan_t763862892  L_0 = ___period0;
		TimeSpan_t763862892  L_1 = ___period0;
		Nullable_1_t3649900800  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m4008503583(&L_2, L_1, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		Il2CppObject * L_3 = DefaultSchedulers_get_TimeBasedOperations_m2396324172(NULL /*static, unused*/, /*hidden argument*/NULL);
		TimerObservable_t1697791765 * L_4 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m1563113823(L_4, L_0, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Interval(System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t Observable_Interval_m3597074831_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Interval_m3597074831 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___period0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Interval_m3597074831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpan_t763862892  L_0 = ___period0;
		TimeSpan_t763862892  L_1 = ___period0;
		Nullable_1_t3649900800  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m4008503583(&L_2, L_1, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		Il2CppObject * L_3 = ___scheduler1;
		TimerObservable_t1697791765 * L_4 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m1563113823(L_4, L_0, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.TimeSpan)
extern Il2CppClass* Nullable_1_t3649900800_il2cpp_TypeInfo_var;
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const uint32_t Observable_Timer_m618408927_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Timer_m618408927 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___dueTime0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timer_m618408927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TimeSpan_t763862892  L_0 = ___dueTime0;
		Initobj (Nullable_1_t3649900800_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t3649900800  L_1 = V_0;
		Il2CppObject * L_2 = DefaultSchedulers_get_TimeBasedOperations_m2396324172(NULL /*static, unused*/, /*hidden argument*/NULL);
		TimerObservable_t1697791765 * L_3 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m1563113823(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.DateTimeOffset)
extern Il2CppClass* Nullable_1_t3649900800_il2cpp_TypeInfo_var;
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const uint32_t Observable_Timer_m1787381928_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Timer_m1787381928 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___dueTime0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timer_m1787381928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTimeOffset_t3712260035  L_0 = ___dueTime0;
		Initobj (Nullable_1_t3649900800_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t3649900800  L_1 = V_0;
		Il2CppObject * L_2 = DefaultSchedulers_get_TimeBasedOperations_m2396324172(NULL /*static, unused*/, /*hidden argument*/NULL);
		TimerObservable_t1697791765 * L_3 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m2474548840(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.TimeSpan,System.TimeSpan)
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t Observable_Timer_m2629569077_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Timer_m2629569077 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___dueTime0, TimeSpan_t763862892  ___period1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timer_m2629569077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpan_t763862892  L_0 = ___dueTime0;
		TimeSpan_t763862892  L_1 = ___period1;
		Nullable_1_t3649900800  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m4008503583(&L_2, L_1, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		Il2CppObject * L_3 = DefaultSchedulers_get_TimeBasedOperations_m2396324172(NULL /*static, unused*/, /*hidden argument*/NULL);
		TimerObservable_t1697791765 * L_4 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m1563113823(L_4, L_0, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.DateTimeOffset,System.TimeSpan)
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t Observable_Timer_m3524293886_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Timer_m3524293886 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___dueTime0, TimeSpan_t763862892  ___period1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timer_m3524293886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTimeOffset_t3712260035  L_0 = ___dueTime0;
		TimeSpan_t763862892  L_1 = ___period1;
		Nullable_1_t3649900800  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m4008503583(&L_2, L_1, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		Il2CppObject * L_3 = DefaultSchedulers_get_TimeBasedOperations_m2396324172(NULL /*static, unused*/, /*hidden argument*/NULL);
		TimerObservable_t1697791765 * L_4 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m2474548840(L_4, L_0, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Nullable_1_t3649900800_il2cpp_TypeInfo_var;
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const uint32_t Observable_Timer_m2482108829_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Timer_m2482108829 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___dueTime0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timer_m2482108829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TimeSpan_t763862892  L_0 = ___dueTime0;
		Initobj (Nullable_1_t3649900800_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t3649900800  L_1 = V_0;
		Il2CppObject * L_2 = ___scheduler1;
		TimerObservable_t1697791765 * L_3 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m1563113823(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.DateTimeOffset,UniRx.IScheduler)
extern Il2CppClass* Nullable_1_t3649900800_il2cpp_TypeInfo_var;
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const uint32_t Observable_Timer_m153806836_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Timer_m153806836 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___dueTime0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timer_m153806836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3649900800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTimeOffset_t3712260035  L_0 = ___dueTime0;
		Initobj (Nullable_1_t3649900800_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t3649900800  L_1 = V_0;
		Il2CppObject * L_2 = ___scheduler1;
		TimerObservable_t1697791765 * L_3 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m2474548840(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.TimeSpan,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t Observable_Timer_m2165035527_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Timer_m2165035527 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___dueTime0, TimeSpan_t763862892  ___period1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timer_m2165035527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpan_t763862892  L_0 = ___dueTime0;
		TimeSpan_t763862892  L_1 = ___period1;
		Nullable_1_t3649900800  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m4008503583(&L_2, L_1, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		Il2CppObject * L_3 = ___scheduler2;
		TimerObservable_t1697791765 * L_4 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m1563113823(L_4, L_0, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::Timer(System.DateTimeOffset,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* TimerObservable_t1697791765_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t Observable_Timer_m3697488478_MetadataUsageId;
extern "C"  Il2CppObject* Observable_Timer_m3697488478 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___dueTime0, TimeSpan_t763862892  ___period1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timer_m3697488478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTimeOffset_t3712260035  L_0 = ___dueTime0;
		TimeSpan_t763862892  L_1 = ___period1;
		Nullable_1_t3649900800  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m4008503583(&L_2, L_1, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		Il2CppObject * L_3 = ___scheduler2;
		TimerObservable_t1697791765 * L_4 = (TimerObservable_t1697791765 *)il2cpp_codegen_object_new(TimerObservable_t1697791765_il2cpp_TypeInfo_var);
		TimerObservable__ctor_m2474548840(L_4, L_0, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::FromCoroutine(System.Func`1<System.Collections.IEnumerator>,System.Boolean)
extern Il2CppClass* U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3458695013_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CFromCoroutineU3Ec__AnonStorey4A_U3CU3Em__48_m3829821962_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m503419218_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisUnit_t2558286038_m3114512671_MethodInfo_var;
extern const uint32_t Observable_FromCoroutine_m3913040964_MetadataUsageId;
extern "C"  Il2CppObject* Observable_FromCoroutine_m3913040964 (Il2CppObject * __this /* static, unused */, Func_1_t1429988286 * ___coroutine0, bool ___publishEveryYield1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_FromCoroutine_m3913040964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * V_0 = NULL;
	{
		U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * L_0 = (U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 *)il2cpp_codegen_object_new(U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312_il2cpp_TypeInfo_var);
		U3CFromCoroutineU3Ec__AnonStorey4A__ctor_m2466286203(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * L_1 = V_0;
		Func_1_t1429988286 * L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->set_coroutine_0(L_2);
		U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * L_3 = V_0;
		bool L_4 = ___publishEveryYield1;
		NullCheck(L_3);
		L_3->set_publishEveryYield_1(L_4);
		U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CFromCoroutineU3Ec__AnonStorey4A_U3CU3Em__48_m3829821962_MethodInfo_var);
		Func_3_t3458695013 * L_7 = (Func_3_t3458695013 *)il2cpp_codegen_object_new(Func_3_t3458695013_il2cpp_TypeInfo_var);
		Func_3__ctor_m503419218(L_7, L_5, L_6, /*hidden argument*/Func_3__ctor_m503419218_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_8 = Observable_FromCoroutine_TisUnit_t2558286038_m3114512671(NULL /*static, unused*/, L_7, /*hidden argument*/Observable_FromCoroutine_TisUnit_t2558286038_m3114512671_MethodInfo_var);
		return L_8;
	}
}
// System.Collections.IEnumerator UniRx.Observable::WrapEnumerator(System.Collections.IEnumerator,UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Boolean)
extern Il2CppClass* U3CWrapEnumeratorU3Ec__IteratorF_t3900889545_il2cpp_TypeInfo_var;
extern const uint32_t Observable_WrapEnumerator_m2325795528_MetadataUsageId;
extern "C"  Il2CppObject * Observable_WrapEnumerator_m2325795528 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, Il2CppObject* ___observer1, CancellationToken_t1439151560  ___cancellationToken2, bool ___publishEveryYield3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_WrapEnumerator_m2325795528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * V_0 = NULL;
	{
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_0 = (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 *)il2cpp_codegen_object_new(U3CWrapEnumeratorU3Ec__IteratorF_t3900889545_il2cpp_TypeInfo_var);
		U3CWrapEnumeratorU3Ec__IteratorF__ctor_m1766589338(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_1 = V_0;
		Il2CppObject * L_2 = ___enumerator0;
		NullCheck(L_1);
		L_1->set_enumerator_2(L_2);
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_3 = V_0;
		Il2CppObject* L_4 = ___observer1;
		NullCheck(L_3);
		L_3->set_observer_3(L_4);
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_5 = V_0;
		bool L_6 = ___publishEveryYield3;
		NullCheck(L_5);
		L_5->set_publishEveryYield_6(L_6);
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_7 = V_0;
		CancellationToken_t1439151560  L_8 = ___cancellationToken2;
		NullCheck(L_7);
		L_7->set_cancellationToken_8(L_8);
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_9 = V_0;
		Il2CppObject * L_10 = ___enumerator0;
		NullCheck(L_9);
		L_9->set_U3CU24U3Eenumerator_12(L_10);
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_11 = V_0;
		Il2CppObject* L_12 = ___observer1;
		NullCheck(L_11);
		L_11->set_U3CU24U3Eobserver_13(L_12);
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_13 = V_0;
		bool L_14 = ___publishEveryYield3;
		NullCheck(L_13);
		L_13->set_U3CU24U3EpublishEveryYield_14(L_14);
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_15 = V_0;
		CancellationToken_t1439151560  L_16 = ___cancellationToken2;
		NullCheck(L_15);
		L_15->set_U3CU24U3EcancellationToken_15(L_16);
		U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * L_17 = V_0;
		return L_17;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::ToObservable(System.Collections.IEnumerator,System.Boolean)
extern Il2CppClass* U3CToObservableU3Ec__AnonStorey51_t3647079135_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3458695013_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CToObservableU3Ec__AnonStorey51_U3CU3Em__4E_m3556682324_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m503419218_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisUnit_t2558286038_m3114512671_MethodInfo_var;
extern const uint32_t Observable_ToObservable_m1825470780_MetadataUsageId;
extern "C"  Il2CppObject* Observable_ToObservable_m1825470780 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___coroutine0, bool ___publishEveryYield1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_ToObservable_m1825470780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToObservableU3Ec__AnonStorey51_t3647079135 * V_0 = NULL;
	{
		U3CToObservableU3Ec__AnonStorey51_t3647079135 * L_0 = (U3CToObservableU3Ec__AnonStorey51_t3647079135 *)il2cpp_codegen_object_new(U3CToObservableU3Ec__AnonStorey51_t3647079135_il2cpp_TypeInfo_var);
		U3CToObservableU3Ec__AnonStorey51__ctor_m3092923844(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CToObservableU3Ec__AnonStorey51_t3647079135 * L_1 = V_0;
		Il2CppObject * L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->set_coroutine_0(L_2);
		U3CToObservableU3Ec__AnonStorey51_t3647079135 * L_3 = V_0;
		bool L_4 = ___publishEveryYield1;
		NullCheck(L_3);
		L_3->set_publishEveryYield_1(L_4);
		U3CToObservableU3Ec__AnonStorey51_t3647079135 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CToObservableU3Ec__AnonStorey51_U3CU3Em__4E_m3556682324_MethodInfo_var);
		Func_3_t3458695013 * L_7 = (Func_3_t3458695013 *)il2cpp_codegen_object_new(Func_3_t3458695013_il2cpp_TypeInfo_var);
		Func_3__ctor_m503419218(L_7, L_5, L_6, /*hidden argument*/Func_3__ctor_m503419218_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_8 = Observable_FromCoroutine_TisUnit_t2558286038_m3114512671(NULL /*static, unused*/, L_7, /*hidden argument*/Observable_FromCoroutine_TisUnit_t2558286038_m3114512671_MethodInfo_var);
		return L_8;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryUpdate()
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t521084177_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_U3CEveryUpdateU3Em__4F_m2940893163_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1363846400_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var;
extern const uint32_t Observable_EveryUpdate_m1588154268_MetadataUsageId;
extern "C"  Il2CppObject* Observable_EveryUpdate_m1588154268 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryUpdate_m1588154268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t521084177 * L_0 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Observable_U3CEveryUpdateU3Em__4F_m2940893163_MethodInfo_var);
		Func_3_t521084177 * L_2 = (Func_3_t521084177 *)il2cpp_codegen_object_new(Func_3_t521084177_il2cpp_TypeInfo_var);
		Func_3__ctor_m1363846400(L_2, NULL, L_1, /*hidden argument*/Func_3__ctor_m1363846400_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t521084177 * L_3 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		Il2CppObject* L_4 = Observable_FromCoroutine_TisInt64_t2847414882_m2157755661(NULL /*static, unused*/, L_3, /*hidden argument*/Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var);
		return L_4;
	}
}
// System.Collections.IEnumerator UniRx.Observable::EveryUpdateCore(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* U3CEveryUpdateCoreU3Ec__Iterator11_t819860698_il2cpp_TypeInfo_var;
extern const uint32_t Observable_EveryUpdateCore_m4013844463_MetadataUsageId;
extern "C"  Il2CppObject * Observable_EveryUpdateCore_m4013844463 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryUpdateCore_m4013844463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * V_0 = NULL;
	{
		U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * L_0 = (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 *)il2cpp_codegen_object_new(U3CEveryUpdateCoreU3Ec__Iterator11_t819860698_il2cpp_TypeInfo_var);
		U3CEveryUpdateCoreU3Ec__Iterator11__ctor_m4046846889(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * L_1 = V_0;
		CancellationToken_t1439151560  L_2 = ___cancellationToken1;
		NullCheck(L_1);
		L_1->set_cancellationToken_0(L_2);
		U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * L_3 = V_0;
		Il2CppObject* L_4 = ___observer0;
		NullCheck(L_3);
		L_3->set_observer_2(L_4);
		U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * L_5 = V_0;
		CancellationToken_t1439151560  L_6 = ___cancellationToken1;
		NullCheck(L_5);
		L_5->set_U3CU24U3EcancellationToken_5(L_6);
		U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Eobserver_6(L_8);
		U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * L_9 = V_0;
		return L_9;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryFixedUpdate()
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t521084177_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_U3CEveryFixedUpdateU3Em__50_m2960750058_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1363846400_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var;
extern const uint32_t Observable_EveryFixedUpdate_m136372812_MetadataUsageId;
extern "C"  Il2CppObject* Observable_EveryFixedUpdate_m136372812 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryFixedUpdate_m136372812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t521084177 * L_0 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Observable_U3CEveryFixedUpdateU3Em__50_m2960750058_MethodInfo_var);
		Func_3_t521084177 * L_2 = (Func_3_t521084177 *)il2cpp_codegen_object_new(Func_3_t521084177_il2cpp_TypeInfo_var);
		Func_3__ctor_m1363846400(L_2, NULL, L_1, /*hidden argument*/Func_3__ctor_m1363846400_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_3(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t521084177 * L_3 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		Il2CppObject* L_4 = Observable_FromCoroutine_TisInt64_t2847414882_m2157755661(NULL /*static, unused*/, L_3, /*hidden argument*/Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var);
		return L_4;
	}
}
// System.Collections.IEnumerator UniRx.Observable::EveryFixedUpdateCore(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937_il2cpp_TypeInfo_var;
extern const uint32_t Observable_EveryFixedUpdateCore_m3711089491_MetadataUsageId;
extern "C"  Il2CppObject * Observable_EveryFixedUpdateCore_m3711089491 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryFixedUpdateCore_m3711089491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * V_0 = NULL;
	{
		U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * L_0 = (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 *)il2cpp_codegen_object_new(U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937_il2cpp_TypeInfo_var);
		U3CEveryFixedUpdateCoreU3Ec__Iterator12__ctor_m3456378834(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * L_1 = V_0;
		CancellationToken_t1439151560  L_2 = ___cancellationToken1;
		NullCheck(L_1);
		L_1->set_cancellationToken_0(L_2);
		U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * L_3 = V_0;
		Il2CppObject* L_4 = ___observer0;
		NullCheck(L_3);
		L_3->set_observer_3(L_4);
		U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * L_5 = V_0;
		CancellationToken_t1439151560  L_6 = ___cancellationToken1;
		NullCheck(L_5);
		L_5->set_U3CU24U3EcancellationToken_6(L_6);
		U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Eobserver_7(L_8);
		U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * L_9 = V_0;
		return L_9;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryEndOfFrame()
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t521084177_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_U3CEveryEndOfFrameU3Em__51_m2316557359_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1363846400_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var;
extern const uint32_t Observable_EveryEndOfFrame_m2645149134_MetadataUsageId;
extern "C"  Il2CppObject* Observable_EveryEndOfFrame_m2645149134 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryEndOfFrame_m2645149134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t521084177 * L_0 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_4();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Observable_U3CEveryEndOfFrameU3Em__51_m2316557359_MethodInfo_var);
		Func_3_t521084177 * L_2 = (Func_3_t521084177 *)il2cpp_codegen_object_new(Func_3_t521084177_il2cpp_TypeInfo_var);
		Func_3__ctor_m1363846400(L_2, NULL, L_1, /*hidden argument*/Func_3__ctor_m1363846400_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_4(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t521084177 * L_3 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_4();
		Il2CppObject* L_4 = Observable_FromCoroutine_TisInt64_t2847414882_m2157755661(NULL /*static, unused*/, L_3, /*hidden argument*/Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var);
		return L_4;
	}
}
// System.Collections.IEnumerator UniRx.Observable::EveryEndOfFrameCore(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358_il2cpp_TypeInfo_var;
extern const uint32_t Observable_EveryEndOfFrameCore_m470912893_MetadataUsageId;
extern "C"  Il2CppObject * Observable_EveryEndOfFrameCore_m470912893 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryEndOfFrameCore_m470912893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * V_0 = NULL;
	{
		U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * L_0 = (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 *)il2cpp_codegen_object_new(U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358_il2cpp_TypeInfo_var);
		U3CEveryEndOfFrameCoreU3Ec__Iterator13__ctor_m1961632373(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * L_1 = V_0;
		CancellationToken_t1439151560  L_2 = ___cancellationToken1;
		NullCheck(L_1);
		L_1->set_cancellationToken_0(L_2);
		U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * L_3 = V_0;
		Il2CppObject* L_4 = ___observer0;
		NullCheck(L_3);
		L_3->set_observer_3(L_4);
		U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * L_5 = V_0;
		CancellationToken_t1439151560  L_6 = ___cancellationToken1;
		NullCheck(L_5);
		L_5->set_U3CU24U3EcancellationToken_6(L_6);
		U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Eobserver_7(L_8);
		U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * L_9 = V_0;
		return L_9;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryGameObjectUpdate()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3706795343_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_U3CEveryGameObjectUpdateU3Em__52_m2463372556_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m33277251_MethodInfo_var;
extern const MethodInfo* Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454_MethodInfo_var;
extern const uint32_t Observable_EveryGameObjectUpdate_m1953989133_MetadataUsageId;
extern "C"  Il2CppObject* Observable_EveryGameObjectUpdate_m1953989133 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryGameObjectUpdate_m1953989133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t G_B2_0 = 0;
	Il2CppObject* G_B2_1 = NULL;
	int64_t G_B1_0 = 0;
	Il2CppObject* G_B1_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = MainThreadDispatcher_UpdateAsObservable_m46968669(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t3706795343 * L_1 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_5();
		G_B1_0 = (((int64_t)((int64_t)(-1))));
		G_B1_1 = L_0;
		if (L_1)
		{
			G_B2_0 = (((int64_t)((int64_t)(-1))));
			G_B2_1 = L_0;
			goto IL_001f;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Observable_U3CEveryGameObjectUpdateU3Em__52_m2463372556_MethodInfo_var);
		Func_3_t3706795343 * L_3 = (Func_3_t3706795343 *)il2cpp_codegen_object_new(Func_3_t3706795343_il2cpp_TypeInfo_var);
		Func_3__ctor_m33277251(L_3, NULL, L_2, /*hidden argument*/Func_3__ctor_m33277251_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_5(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t3706795343 * L_4 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_5();
		Il2CppObject* L_5 = Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454(NULL /*static, unused*/, G_B2_1, G_B2_0, L_4, /*hidden argument*/Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454_MethodInfo_var);
		return L_5;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryLateUpdate()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3706795343_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_U3CEveryLateUpdateU3Em__53_m2395028662_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m33277251_MethodInfo_var;
extern const MethodInfo* Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454_MethodInfo_var;
extern const uint32_t Observable_EveryLateUpdate_m4132562786_MetadataUsageId;
extern "C"  Il2CppObject* Observable_EveryLateUpdate_m4132562786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryLateUpdate_m4132562786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t G_B2_0 = 0;
	Il2CppObject* G_B2_1 = NULL;
	int64_t G_B1_0 = 0;
	Il2CppObject* G_B1_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = MainThreadDispatcher_LateUpdateAsObservable_m4009347107(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t3706795343 * L_1 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_6();
		G_B1_0 = (((int64_t)((int64_t)(-1))));
		G_B1_1 = L_0;
		if (L_1)
		{
			G_B2_0 = (((int64_t)((int64_t)(-1))));
			G_B2_1 = L_0;
			goto IL_001f;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Observable_U3CEveryLateUpdateU3Em__53_m2395028662_MethodInfo_var);
		Func_3_t3706795343 * L_3 = (Func_3_t3706795343 *)il2cpp_codegen_object_new(Func_3_t3706795343_il2cpp_TypeInfo_var);
		Func_3__ctor_m33277251(L_3, NULL, L_2, /*hidden argument*/Func_3__ctor_m33277251_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_6(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t3706795343 * L_4 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_6();
		Il2CppObject* L_5 = Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454(NULL /*static, unused*/, G_B2_1, G_B2_0, L_4, /*hidden argument*/Observable_Scan_TisUnit_t2558286038_TisInt64_t2847414882_m3582058454_MethodInfo_var);
		return L_5;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::EveryAfterUpdate()
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t521084177_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_U3CEveryAfterUpdateU3Em__54_m502248702_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1363846400_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var;
extern const uint32_t Observable_EveryAfterUpdate_m279451828_MetadataUsageId;
extern "C"  Il2CppObject* Observable_EveryAfterUpdate_m279451828 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryAfterUpdate_m279451828_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t521084177 * L_0 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_7();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Observable_U3CEveryAfterUpdateU3Em__54_m502248702_MethodInfo_var);
		Func_3_t521084177 * L_2 = (Func_3_t521084177 *)il2cpp_codegen_object_new(Func_3_t521084177_il2cpp_TypeInfo_var);
		Func_3__ctor_m1363846400(L_2, NULL, L_1, /*hidden argument*/Func_3__ctor_m1363846400_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_7(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Func_3_t521084177 * L_3 = ((Observable_t258071701_StaticFields*)Observable_t258071701_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_7();
		Il2CppObject* L_4 = Observable_FromCoroutine_TisInt64_t2847414882_m2157755661(NULL /*static, unused*/, L_3, /*hidden argument*/Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var);
		return L_4;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::NextFrame(UniRx.FrameCountType)
extern Il2CppClass* U3CNextFrameU3Ec__AnonStorey52_t2543931332_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3458695013_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CNextFrameU3Ec__AnonStorey52_U3CU3Em__55_m4159195242_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m503419218_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisUnit_t2558286038_m3114512671_MethodInfo_var;
extern const uint32_t Observable_NextFrame_m1403194704_MetadataUsageId;
extern "C"  Il2CppObject* Observable_NextFrame_m1403194704 (Il2CppObject * __this /* static, unused */, int32_t ___frameCountType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_NextFrame_m1403194704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CNextFrameU3Ec__AnonStorey52_t2543931332 * V_0 = NULL;
	{
		U3CNextFrameU3Ec__AnonStorey52_t2543931332 * L_0 = (U3CNextFrameU3Ec__AnonStorey52_t2543931332 *)il2cpp_codegen_object_new(U3CNextFrameU3Ec__AnonStorey52_t2543931332_il2cpp_TypeInfo_var);
		U3CNextFrameU3Ec__AnonStorey52__ctor_m2220673023(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CNextFrameU3Ec__AnonStorey52_t2543931332 * L_1 = V_0;
		int32_t L_2 = ___frameCountType0;
		NullCheck(L_1);
		L_1->set_frameCountType_0(L_2);
		U3CNextFrameU3Ec__AnonStorey52_t2543931332 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CNextFrameU3Ec__AnonStorey52_U3CU3Em__55_m4159195242_MethodInfo_var);
		Func_3_t3458695013 * L_5 = (Func_3_t3458695013 *)il2cpp_codegen_object_new(Func_3_t3458695013_il2cpp_TypeInfo_var);
		Func_3__ctor_m503419218(L_5, L_3, L_4, /*hidden argument*/Func_3__ctor_m503419218_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_6 = Observable_FromCoroutine_TisUnit_t2558286038_m3114512671(NULL /*static, unused*/, L_5, /*hidden argument*/Observable_FromCoroutine_TisUnit_t2558286038_m3114512671_MethodInfo_var);
		return L_6;
	}
}
// System.Collections.IEnumerator UniRx.Observable::NextFrameCore(UniRx.IObserver`1<UniRx.Unit>,UniRx.FrameCountType,UniRx.CancellationToken)
extern Il2CppClass* U3CNextFrameCoreU3Ec__Iterator14_t4059042515_il2cpp_TypeInfo_var;
extern const uint32_t Observable_NextFrameCore_m1980127623_MetadataUsageId;
extern "C"  Il2CppObject * Observable_NextFrameCore_m1980127623 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, int32_t ___frameCountType1, CancellationToken_t1439151560  ___cancellation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_NextFrameCore_m1980127623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * V_0 = NULL;
	{
		U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * L_0 = (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 *)il2cpp_codegen_object_new(U3CNextFrameCoreU3Ec__Iterator14_t4059042515_il2cpp_TypeInfo_var);
		U3CNextFrameCoreU3Ec__Iterator14__ctor_m2408404432(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * L_1 = V_0;
		int32_t L_2 = ___frameCountType1;
		NullCheck(L_1);
		L_1->set_frameCountType_0(L_2);
		U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * L_3 = V_0;
		CancellationToken_t1439151560  L_4 = ___cancellation2;
		NullCheck(L_3);
		L_3->set_cancellation_1(L_4);
		U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * L_5 = V_0;
		Il2CppObject* L_6 = ___observer0;
		NullCheck(L_5);
		L_5->set_observer_2(L_6);
		U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * L_7 = V_0;
		int32_t L_8 = ___frameCountType1;
		NullCheck(L_7);
		L_7->set_U3CU24U3EframeCountType_5(L_8);
		U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * L_9 = V_0;
		CancellationToken_t1439151560  L_10 = ___cancellation2;
		NullCheck(L_9);
		L_9->set_U3CU24U3Ecancellation_6(L_10);
		U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * L_11 = V_0;
		Il2CppObject* L_12 = ___observer0;
		NullCheck(L_11);
		L_11->set_U3CU24U3Eobserver_7(L_12);
		U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * L_13 = V_0;
		return L_13;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::IntervalFrame(System.Int32,UniRx.FrameCountType)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t Observable_IntervalFrame_m1197807937_MetadataUsageId;
extern "C"  Il2CppObject* Observable_IntervalFrame_m1197807937 (Il2CppObject * __this /* static, unused */, int32_t ___intervalFrameCount0, int32_t ___frameCountType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_IntervalFrame_m1197807937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___intervalFrameCount0;
		int32_t L_1 = ___intervalFrameCount0;
		int32_t L_2 = ___frameCountType1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_3 = Observable_TimerFrame_m1110259006(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::TimerFrame(System.Int32,UniRx.FrameCountType)
extern Il2CppClass* U3CTimerFrameU3Ec__AnonStorey53_t1969998059_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t521084177_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CTimerFrameU3Ec__AnonStorey53_U3CU3Em__56_m685773346_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1363846400_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var;
extern const uint32_t Observable_TimerFrame_m594366159_MetadataUsageId;
extern "C"  Il2CppObject* Observable_TimerFrame_m594366159 (Il2CppObject * __this /* static, unused */, int32_t ___dueTimeFrameCount0, int32_t ___frameCountType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_TimerFrame_m594366159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * V_0 = NULL;
	{
		U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * L_0 = (U3CTimerFrameU3Ec__AnonStorey53_t1969998059 *)il2cpp_codegen_object_new(U3CTimerFrameU3Ec__AnonStorey53_t1969998059_il2cpp_TypeInfo_var);
		U3CTimerFrameU3Ec__AnonStorey53__ctor_m3135036728(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * L_1 = V_0;
		int32_t L_2 = ___dueTimeFrameCount0;
		NullCheck(L_1);
		L_1->set_dueTimeFrameCount_0(L_2);
		U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * L_3 = V_0;
		int32_t L_4 = ___frameCountType1;
		NullCheck(L_3);
		L_3->set_frameCountType_1(L_4);
		U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CTimerFrameU3Ec__AnonStorey53_U3CU3Em__56_m685773346_MethodInfo_var);
		Func_3_t521084177 * L_7 = (Func_3_t521084177 *)il2cpp_codegen_object_new(Func_3_t521084177_il2cpp_TypeInfo_var);
		Func_3__ctor_m1363846400(L_7, L_5, L_6, /*hidden argument*/Func_3__ctor_m1363846400_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_8 = Observable_FromCoroutine_TisInt64_t2847414882_m2157755661(NULL /*static, unused*/, L_7, /*hidden argument*/Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var);
		return L_8;
	}
}
// UniRx.IObservable`1<System.Int64> UniRx.Observable::TimerFrame(System.Int32,System.Int32,UniRx.FrameCountType)
extern Il2CppClass* U3CTimerFrameU3Ec__AnonStorey54_t1969998060_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t521084177_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CTimerFrameU3Ec__AnonStorey54_U3CU3Em__57_m2442044674_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1363846400_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var;
extern const uint32_t Observable_TimerFrame_m1110259006_MetadataUsageId;
extern "C"  Il2CppObject* Observable_TimerFrame_m1110259006 (Il2CppObject * __this /* static, unused */, int32_t ___dueTimeFrameCount0, int32_t ___periodFrameCount1, int32_t ___frameCountType2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_TimerFrame_m1110259006_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimerFrameU3Ec__AnonStorey54_t1969998060 * V_0 = NULL;
	{
		U3CTimerFrameU3Ec__AnonStorey54_t1969998060 * L_0 = (U3CTimerFrameU3Ec__AnonStorey54_t1969998060 *)il2cpp_codegen_object_new(U3CTimerFrameU3Ec__AnonStorey54_t1969998060_il2cpp_TypeInfo_var);
		U3CTimerFrameU3Ec__AnonStorey54__ctor_m2938523223(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerFrameU3Ec__AnonStorey54_t1969998060 * L_1 = V_0;
		int32_t L_2 = ___dueTimeFrameCount0;
		NullCheck(L_1);
		L_1->set_dueTimeFrameCount_0(L_2);
		U3CTimerFrameU3Ec__AnonStorey54_t1969998060 * L_3 = V_0;
		int32_t L_4 = ___periodFrameCount1;
		NullCheck(L_3);
		L_3->set_periodFrameCount_1(L_4);
		U3CTimerFrameU3Ec__AnonStorey54_t1969998060 * L_5 = V_0;
		int32_t L_6 = ___frameCountType2;
		NullCheck(L_5);
		L_5->set_frameCountType_2(L_6);
		U3CTimerFrameU3Ec__AnonStorey54_t1969998060 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CTimerFrameU3Ec__AnonStorey54_U3CU3Em__57_m2442044674_MethodInfo_var);
		Func_3_t521084177 * L_9 = (Func_3_t521084177 *)il2cpp_codegen_object_new(Func_3_t521084177_il2cpp_TypeInfo_var);
		Func_3__ctor_m1363846400(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1363846400_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisInt64_t2847414882_m2157755661(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisInt64_t2847414882_m2157755661_MethodInfo_var);
		return L_10;
	}
}
// System.Collections.IEnumerator UniRx.Observable::TimerFrameCore(UniRx.IObserver`1<System.Int64>,System.Int32,UniRx.FrameCountType,UniRx.CancellationToken)
extern Il2CppClass* U3CTimerFrameCoreU3Ec__Iterator15_t2264980090_il2cpp_TypeInfo_var;
extern const uint32_t Observable_TimerFrameCore_m1545514344_MetadataUsageId;
extern "C"  Il2CppObject * Observable_TimerFrameCore_m1545514344 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, int32_t ___dueTimeFrameCount1, int32_t ___frameCountType2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_TimerFrameCore_m1545514344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * V_0 = NULL;
	{
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_0 = (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 *)il2cpp_codegen_object_new(U3CTimerFrameCoreU3Ec__Iterator15_t2264980090_il2cpp_TypeInfo_var);
		U3CTimerFrameCoreU3Ec__Iterator15__ctor_m318033033(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_1 = V_0;
		int32_t L_2 = ___dueTimeFrameCount1;
		NullCheck(L_1);
		L_1->set_dueTimeFrameCount_0(L_2);
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_3 = V_0;
		int32_t L_4 = ___frameCountType2;
		NullCheck(L_3);
		L_3->set_frameCountType_2(L_4);
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_5 = V_0;
		CancellationToken_t1439151560  L_6 = ___cancel3;
		NullCheck(L_5);
		L_5->set_cancel_4(L_6);
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck(L_7);
		L_7->set_observer_5(L_8);
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_9 = V_0;
		int32_t L_10 = ___dueTimeFrameCount1;
		NullCheck(L_9);
		L_9->set_U3CU24U3EdueTimeFrameCount_8(L_10);
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_11 = V_0;
		int32_t L_12 = ___frameCountType2;
		NullCheck(L_11);
		L_11->set_U3CU24U3EframeCountType_9(L_12);
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_13 = V_0;
		CancellationToken_t1439151560  L_14 = ___cancel3;
		NullCheck(L_13);
		L_13->set_U3CU24U3Ecancel_10(L_14);
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_15 = V_0;
		Il2CppObject* L_16 = ___observer0;
		NullCheck(L_15);
		L_15->set_U3CU24U3Eobserver_11(L_16);
		U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * L_17 = V_0;
		return L_17;
	}
}
// System.Collections.IEnumerator UniRx.Observable::TimerFrameCore(UniRx.IObserver`1<System.Int64>,System.Int32,System.Int32,UniRx.FrameCountType,UniRx.CancellationToken)
extern Il2CppClass* U3CTimerFrameCoreU3Ec__Iterator16_t2264980091_il2cpp_TypeInfo_var;
extern const uint32_t Observable_TimerFrameCore_m2679907233_MetadataUsageId;
extern "C"  Il2CppObject * Observable_TimerFrameCore_m2679907233 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, int32_t ___dueTimeFrameCount1, int32_t ___periodFrameCount2, int32_t ___frameCountType3, CancellationToken_t1439151560  ___cancel4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_TimerFrameCore_m2679907233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * V_0 = NULL;
	{
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_0 = (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 *)il2cpp_codegen_object_new(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091_il2cpp_TypeInfo_var);
		U3CTimerFrameCoreU3Ec__Iterator16__ctor_m121519528(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_1 = V_0;
		int32_t L_2 = ___dueTimeFrameCount1;
		NullCheck(L_1);
		L_1->set_dueTimeFrameCount_0(L_2);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_3 = V_0;
		int32_t L_4 = ___periodFrameCount2;
		NullCheck(L_3);
		L_3->set_periodFrameCount_1(L_4);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_5 = V_0;
		int32_t L_6 = ___frameCountType3;
		NullCheck(L_5);
		L_5->set_frameCountType_4(L_6);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_7 = V_0;
		CancellationToken_t1439151560  L_8 = ___cancel4;
		NullCheck(L_7);
		L_7->set_cancel_6(L_8);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_9 = V_0;
		Il2CppObject* L_10 = ___observer0;
		NullCheck(L_9);
		L_9->set_observer_7(L_10);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_11 = V_0;
		int32_t L_12 = ___dueTimeFrameCount1;
		NullCheck(L_11);
		L_11->set_U3CU24U3EdueTimeFrameCount_10(L_12);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_13 = V_0;
		int32_t L_14 = ___periodFrameCount2;
		NullCheck(L_13);
		L_13->set_U3CU24U3EperiodFrameCount_11(L_14);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_15 = V_0;
		int32_t L_16 = ___frameCountType3;
		NullCheck(L_15);
		L_15->set_U3CU24U3EframeCountType_12(L_16);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_17 = V_0;
		CancellationToken_t1439151560  L_18 = ___cancel4;
		NullCheck(L_17);
		L_17->set_U3CU24U3Ecancel_13(L_18);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_19 = V_0;
		Il2CppObject* L_20 = ___observer0;
		NullCheck(L_19);
		L_19->set_U3CU24U3Eobserver_14(L_20);
		U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * L_21 = V_0;
		return L_21;
	}
}
// UniRx.IObservable`1<System.Boolean> UniRx.Observable::EveryApplicationPause()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_AsObservable_TisBoolean_t211005341_m3575928281_MethodInfo_var;
extern const uint32_t Observable_EveryApplicationPause_m3046827838_MetadataUsageId;
extern "C"  Il2CppObject* Observable_EveryApplicationPause_m3046827838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryApplicationPause_m3046827838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = MainThreadDispatcher_OnApplicationPauseAsObservable_m1450384742(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_1 = Observable_AsObservable_TisBoolean_t211005341_m3575928281(NULL /*static, unused*/, L_0, /*hidden argument*/Observable_AsObservable_TisBoolean_t211005341_m3575928281_MethodInfo_var);
		return L_1;
	}
}
// UniRx.IObservable`1<System.Boolean> UniRx.Observable::EveryApplicationFocus()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_AsObservable_TisBoolean_t211005341_m3575928281_MethodInfo_var;
extern const uint32_t Observable_EveryApplicationFocus_m3145983392_MetadataUsageId;
extern "C"  Il2CppObject* Observable_EveryApplicationFocus_m3145983392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_EveryApplicationFocus_m3145983392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = MainThreadDispatcher_OnApplicationFocusAsObservable_m251107528(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_1 = Observable_AsObservable_TisBoolean_t211005341_m3575928281(NULL /*static, unused*/, L_0, /*hidden argument*/Observable_AsObservable_TisBoolean_t211005341_m3575928281_MethodInfo_var);
		return L_1;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::OnceApplicationQuit()
extern Il2CppClass* MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_Take_TisUnit_t2558286038_m1136349407_MethodInfo_var;
extern const uint32_t Observable_OnceApplicationQuit_m328482472_MetadataUsageId;
extern "C"  Il2CppObject* Observable_OnceApplicationQuit_m328482472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_OnceApplicationQuit_m328482472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t4027217788_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = MainThreadDispatcher_OnApplicationQuitAsObservable_m1999083704(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_1 = Observable_Take_TisUnit_t2558286038_m1136349407(NULL /*static, unused*/, L_0, 1, /*hidden argument*/Observable_Take_TisUnit_t2558286038_m1136349407_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.IEnumerator UniRx.Observable::<EveryUpdate>m__4F(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t Observable_U3CEveryUpdateU3Em__4F_m2940893163_MetadataUsageId;
extern "C"  Il2CppObject * Observable_U3CEveryUpdateU3Em__4F_m2940893163 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_U3CEveryUpdateU3Em__4F_m2940893163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		CancellationToken_t1439151560  L_1 = ___cancellationToken1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = Observable_EveryUpdateCore_m4013844463(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.IEnumerator UniRx.Observable::<EveryFixedUpdate>m__50(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t Observable_U3CEveryFixedUpdateU3Em__50_m2960750058_MetadataUsageId;
extern "C"  Il2CppObject * Observable_U3CEveryFixedUpdateU3Em__50_m2960750058 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_U3CEveryFixedUpdateU3Em__50_m2960750058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		CancellationToken_t1439151560  L_1 = ___cancellationToken1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = Observable_EveryFixedUpdateCore_m3711089491(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.IEnumerator UniRx.Observable::<EveryEndOfFrame>m__51(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t Observable_U3CEveryEndOfFrameU3Em__51_m2316557359_MetadataUsageId;
extern "C"  Il2CppObject * Observable_U3CEveryEndOfFrameU3Em__51_m2316557359 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_U3CEveryEndOfFrameU3Em__51_m2316557359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		CancellationToken_t1439151560  L_1 = ___cancellationToken1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = Observable_EveryEndOfFrameCore_m470912893(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int64 UniRx.Observable::<EveryGameObjectUpdate>m__52(System.Int64,UniRx.Unit)
extern "C"  int64_t Observable_U3CEveryGameObjectUpdateU3Em__52_m2463372556 (Il2CppObject * __this /* static, unused */, int64_t ___x0, Unit_t2558286038  ___y1, const MethodInfo* method)
{
	{
		int64_t L_0 = ___x0;
		return ((int64_t)((int64_t)L_0+(int64_t)(((int64_t)((int64_t)1)))));
	}
}
// System.Int64 UniRx.Observable::<EveryLateUpdate>m__53(System.Int64,UniRx.Unit)
extern "C"  int64_t Observable_U3CEveryLateUpdateU3Em__53_m2395028662 (Il2CppObject * __this /* static, unused */, int64_t ___x0, Unit_t2558286038  ___y1, const MethodInfo* method)
{
	{
		int64_t L_0 = ___x0;
		return ((int64_t)((int64_t)L_0+(int64_t)(((int64_t)((int64_t)1)))));
	}
}
// System.Collections.IEnumerator UniRx.Observable::<EveryAfterUpdate>m__54(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* EveryAfterUpdateInvoker_t3949333456_il2cpp_TypeInfo_var;
extern const uint32_t Observable_U3CEveryAfterUpdateU3Em__54_m502248702_MetadataUsageId;
extern "C"  Il2CppObject * Observable_U3CEveryAfterUpdateU3Em__54_m502248702 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Observable_U3CEveryAfterUpdateU3Em__54_m502248702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		CancellationToken_t1439151560  L_1 = ___cancellationToken1;
		EveryAfterUpdateInvoker_t3949333456 * L_2 = (EveryAfterUpdateInvoker_t3949333456 *)il2cpp_codegen_object_new(EveryAfterUpdateInvoker_t3949333456_il2cpp_TypeInfo_var);
		EveryAfterUpdateInvoker__ctor_m2456117825(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::.ctor()
extern "C"  void U3CEveryEndOfFrameCoreU3Ec__Iterator13__ctor_m1961632373 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEveryEndOfFrameCoreU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3623432061 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEveryEndOfFrameCoreU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m82562321 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::MoveNext()
extern Il2CppClass* YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern const uint32_t U3CEveryEndOfFrameCoreU3Ec__Iterator13_MoveNext_m1379762551_MetadataUsageId;
extern "C"  bool U3CEveryEndOfFrameCoreU3Ec__Iterator13_MoveNext_m1379762551 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEveryEndOfFrameCoreU3Ec__Iterator13_MoveNext_m1379762551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int64_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0061;
		}
	}
	{
		goto IL_009f;
	}

IL_0021:
	{
		CancellationToken_t1439151560 * L_2 = __this->get_address_of_cancellationToken_0();
		bool L_3 = CancellationToken_get_IsCancellationRequested_m1021497867(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_009f;
	}

IL_0036:
	{
		__this->set_U3CcountU3E__0_1((((int64_t)((int64_t)0))));
		IL2CPP_RUNTIME_CLASS_INIT(YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var);
		WaitForEndOfFrame_t1917318876 * L_4 = ((YieldInstructionCache_t1940766803_StaticFields*)YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var->static_fields)->get_WaitForEndOfFrame_0();
		__this->set_U3CyieldInstructionU3E__1_2(L_4);
	}

IL_0049:
	{
		WaitForEndOfFrame_t1917318876 * L_5 = __this->get_U3CyieldInstructionU3E__1_2();
		__this->set_U24current_5(L_5);
		__this->set_U24PC_4(1);
		goto IL_00a1;
	}

IL_0061:
	{
		CancellationToken_t1439151560 * L_6 = __this->get_address_of_cancellationToken_0();
		bool L_7 = CancellationToken_get_IsCancellationRequested_m1021497867(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_009f;
	}

IL_0076:
	{
		Il2CppObject* L_8 = __this->get_observer_3();
		int64_t L_9 = __this->get_U3CcountU3E__0_1();
		int64_t L_10 = L_9;
		V_1 = L_10;
		__this->set_U3CcountU3E__0_1(((int64_t)((int64_t)L_10+(int64_t)(((int64_t)((int64_t)1))))));
		int64_t L_11 = V_1;
		NullCheck(L_8);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_8, L_11);
		goto IL_0049;
	}
	// Dead block : IL_0098: ldarg.0

IL_009f:
	{
		return (bool)0;
	}

IL_00a1:
	{
		return (bool)1;
	}
}
// System.Void UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::Dispose()
extern "C"  void U3CEveryEndOfFrameCoreU3Ec__Iterator13_Dispose_m3831317874 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CEveryEndOfFrameCoreU3Ec__Iterator13_Reset_m3903032610_MetadataUsageId;
extern "C"  void U3CEveryEndOfFrameCoreU3Ec__Iterator13_Reset_m3903032610 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEveryEndOfFrameCoreU3Ec__Iterator13_Reset_m3903032610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::.ctor()
extern "C"  void U3CEveryFixedUpdateCoreU3Ec__Iterator12__ctor_m3456378834 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEveryFixedUpdateCoreU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3689341386 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEveryFixedUpdateCoreU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m655997278 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::MoveNext()
extern Il2CppClass* YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern const uint32_t U3CEveryFixedUpdateCoreU3Ec__Iterator12_MoveNext_m3260514738_MetadataUsageId;
extern "C"  bool U3CEveryFixedUpdateCoreU3Ec__Iterator12_MoveNext_m3260514738 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEveryFixedUpdateCoreU3Ec__Iterator12_MoveNext_m3260514738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int64_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0061;
		}
	}
	{
		goto IL_009f;
	}

IL_0021:
	{
		CancellationToken_t1439151560 * L_2 = __this->get_address_of_cancellationToken_0();
		bool L_3 = CancellationToken_get_IsCancellationRequested_m1021497867(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_009f;
	}

IL_0036:
	{
		__this->set_U3CcountU3E__0_1((((int64_t)((int64_t)0))));
		IL2CPP_RUNTIME_CLASS_INIT(YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var);
		WaitForFixedUpdate_t896427542 * L_4 = ((YieldInstructionCache_t1940766803_StaticFields*)YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var->static_fields)->get_WaitForFixedUpdate_1();
		__this->set_U3CyieldInstructionU3E__1_2(L_4);
	}

IL_0049:
	{
		WaitForFixedUpdate_t896427542 * L_5 = __this->get_U3CyieldInstructionU3E__1_2();
		__this->set_U24current_5(L_5);
		__this->set_U24PC_4(1);
		goto IL_00a1;
	}

IL_0061:
	{
		CancellationToken_t1439151560 * L_6 = __this->get_address_of_cancellationToken_0();
		bool L_7 = CancellationToken_get_IsCancellationRequested_m1021497867(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_009f;
	}

IL_0076:
	{
		Il2CppObject* L_8 = __this->get_observer_3();
		int64_t L_9 = __this->get_U3CcountU3E__0_1();
		int64_t L_10 = L_9;
		V_1 = L_10;
		__this->set_U3CcountU3E__0_1(((int64_t)((int64_t)L_10+(int64_t)(((int64_t)((int64_t)1))))));
		int64_t L_11 = V_1;
		NullCheck(L_8);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_8, L_11);
		goto IL_0049;
	}
	// Dead block : IL_0098: ldarg.0

IL_009f:
	{
		return (bool)0;
	}

IL_00a1:
	{
		return (bool)1;
	}
}
// System.Void UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::Dispose()
extern "C"  void U3CEveryFixedUpdateCoreU3Ec__Iterator12_Dispose_m1468622735 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void UniRx.Observable/<EveryFixedUpdateCore>c__Iterator12::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CEveryFixedUpdateCoreU3Ec__Iterator12_Reset_m1102811775_MetadataUsageId;
extern "C"  void U3CEveryFixedUpdateCoreU3Ec__Iterator12_Reset_m1102811775 (U3CEveryFixedUpdateCoreU3Ec__Iterator12_t976881937 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEveryFixedUpdateCoreU3Ec__Iterator12_Reset_m1102811775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<EveryUpdateCore>c__Iterator11::.ctor()
extern "C"  void U3CEveryUpdateCoreU3Ec__Iterator11__ctor_m4046846889 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<EveryUpdateCore>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEveryUpdateCoreU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m824106057 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object UniRx.Observable/<EveryUpdateCore>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEveryUpdateCoreU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m2830930397 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<EveryUpdateCore>c__Iterator11::MoveNext()
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern const uint32_t U3CEveryUpdateCoreU3Ec__Iterator11_MoveNext_m2399911363_MetadataUsageId;
extern "C"  bool U3CEveryUpdateCoreU3Ec__Iterator11_MoveNext_m2399911363 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEveryUpdateCoreU3Ec__Iterator11_MoveNext_m2399911363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int64_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0051;
		}
	}
	{
		goto IL_008f;
	}

IL_0021:
	{
		CancellationToken_t1439151560 * L_2 = __this->get_address_of_cancellationToken_0();
		bool L_3 = CancellationToken_get_IsCancellationRequested_m1021497867(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_008f;
	}

IL_0036:
	{
		__this->set_U3CcountU3E__0_1((((int64_t)((int64_t)0))));
	}

IL_003e:
	{
		__this->set_U24current_4(NULL);
		__this->set_U24PC_3(1);
		goto IL_0091;
	}

IL_0051:
	{
		CancellationToken_t1439151560 * L_4 = __this->get_address_of_cancellationToken_0();
		bool L_5 = CancellationToken_get_IsCancellationRequested_m1021497867(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0066;
		}
	}
	{
		goto IL_008f;
	}

IL_0066:
	{
		Il2CppObject* L_6 = __this->get_observer_2();
		int64_t L_7 = __this->get_U3CcountU3E__0_1();
		int64_t L_8 = L_7;
		V_1 = L_8;
		__this->set_U3CcountU3E__0_1(((int64_t)((int64_t)L_8+(int64_t)(((int64_t)((int64_t)1))))));
		int64_t L_9 = V_1;
		NullCheck(L_6);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_6, L_9);
		goto IL_003e;
	}
	// Dead block : IL_0088: ldarg.0

IL_008f:
	{
		return (bool)0;
	}

IL_0091:
	{
		return (bool)1;
	}
}
// System.Void UniRx.Observable/<EveryUpdateCore>c__Iterator11::Dispose()
extern "C"  void U3CEveryUpdateCoreU3Ec__Iterator11_Dispose_m1972740518 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UniRx.Observable/<EveryUpdateCore>c__Iterator11::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CEveryUpdateCoreU3Ec__Iterator11_Reset_m1693279830_MetadataUsageId;
extern "C"  void U3CEveryUpdateCoreU3Ec__Iterator11_Reset_m1693279830 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEveryUpdateCoreU3Ec__Iterator11_Reset_m1693279830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey45::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey45__ctor_m2339631455 (U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.Unit UniRx.Observable/<FromAsyncPattern>c__AnonStorey45::<>m__43(System.IAsyncResult)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m140874371_MethodInfo_var;
extern const uint32_t U3CFromAsyncPatternU3Ec__AnonStorey45_U3CU3Em__43_m1918899173_MetadataUsageId;
extern "C"  Unit_t2558286038  U3CFromAsyncPatternU3Ec__AnonStorey45_U3CU3Em__43_m1918899173 (U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396 * __this, Il2CppObject * ___iar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromAsyncPatternU3Ec__AnonStorey45_U3CU3Em__43_m1918899173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t686135974 * L_0 = __this->get_end_0();
		Il2CppObject * L_1 = ___iar0;
		NullCheck(L_0);
		Action_1_Invoke_m140874371(L_0, L_1, /*hidden argument*/Action_1_Invoke_m140874371_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UniRx.Observable/<FromCoroutine>c__AnonStorey4A::.ctor()
extern "C"  void U3CFromCoroutineU3Ec__AnonStorey4A__ctor_m2466286203 (U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<FromCoroutine>c__AnonStorey4A::<>m__48(UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1_Invoke_m2952332176_MethodInfo_var;
extern const uint32_t U3CFromCoroutineU3Ec__AnonStorey4A_U3CU3Em__48_m3829821962_MetadataUsageId;
extern "C"  Il2CppObject * U3CFromCoroutineU3Ec__AnonStorey4A_U3CU3Em__48_m3829821962 (U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFromCoroutineU3Ec__AnonStorey4A_U3CU3Em__48_m3829821962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_1_t1429988286 * L_0 = __this->get_coroutine_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = Func_1_Invoke_m2952332176(L_0, /*hidden argument*/Func_1_Invoke_m2952332176_MethodInfo_var);
		Il2CppObject* L_2 = ___observer0;
		CancellationToken_t1439151560  L_3 = ___cancellationToken1;
		bool L_4 = __this->get_publishEveryYield_1();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = Observable_WrapEnumerator_m2325795528(NULL /*static, unused*/, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UniRx.Observable/<NextFrame>c__AnonStorey52::.ctor()
extern "C"  void U3CNextFrameU3Ec__AnonStorey52__ctor_m2220673023 (U3CNextFrameU3Ec__AnonStorey52_t2543931332 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<NextFrame>c__AnonStorey52::<>m__55(UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CNextFrameU3Ec__AnonStorey52_U3CU3Em__55_m4159195242_MetadataUsageId;
extern "C"  Il2CppObject * U3CNextFrameU3Ec__AnonStorey52_U3CU3Em__55_m4159195242 (U3CNextFrameU3Ec__AnonStorey52_t2543931332 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CNextFrameU3Ec__AnonStorey52_U3CU3Em__55_m4159195242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		int32_t L_1 = __this->get_frameCountType_0();
		CancellationToken_t1439151560  L_2 = ___cancellation1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_3 = Observable_NextFrameCore_m1980127623(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UniRx.Observable/<NextFrameCore>c__Iterator14::.ctor()
extern "C"  void U3CNextFrameCoreU3Ec__Iterator14__ctor_m2408404432 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<NextFrameCore>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CNextFrameCoreU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2240490306 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object UniRx.Observable/<NextFrameCore>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNextFrameCoreU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m447738582 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<NextFrameCore>c__Iterator14::MoveNext()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t U3CNextFrameCoreU3Ec__Iterator14_MoveNext_m3131492156_MetadataUsageId;
extern "C"  bool U3CNextFrameCoreU3Ec__Iterator14_MoveNext_m3131492156 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CNextFrameCoreU3Ec__Iterator14_MoveNext_m3131492156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0070;
	}

IL_0021:
	{
		int32_t L_2 = __this->get_frameCountType_0();
		YieldInstruction_t3557331758 * L_3 = FrameCountTypeExtensions_GetYieldInstruction_m1644022139(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_U24current_4(L_3);
		__this->set_U24PC_3(1);
		goto IL_0072;
	}

IL_003e:
	{
		CancellationToken_t1439151560 * L_4 = __this->get_address_of_cancellation_1();
		bool L_5 = CancellationToken_get_IsCancellationRequested_m1021497867(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0069;
		}
	}
	{
		Il2CppObject* L_6 = __this->get_observer_2();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_7 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, L_6, L_7);
		Il2CppObject* L_8 = __this->get_observer_2();
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IObserver_1_t475317645_il2cpp_TypeInfo_var, L_8);
	}

IL_0069:
	{
		__this->set_U24PC_3((-1));
	}

IL_0070:
	{
		return (bool)0;
	}

IL_0072:
	{
		return (bool)1;
	}
	// Dead block : IL_0074: ldloc.1
}
// System.Void UniRx.Observable/<NextFrameCore>c__Iterator14::Dispose()
extern "C"  void U3CNextFrameCoreU3Ec__Iterator14_Dispose_m3682536973 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UniRx.Observable/<NextFrameCore>c__Iterator14::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CNextFrameCoreU3Ec__Iterator14_Reset_m54837373_MetadataUsageId;
extern "C"  void U3CNextFrameCoreU3Ec__Iterator14_Reset_m54837373 (U3CNextFrameCoreU3Ec__Iterator14_t4059042515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CNextFrameCoreU3Ec__Iterator14_Reset_m54837373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<TimerFrame>c__AnonStorey53::.ctor()
extern "C"  void U3CTimerFrameU3Ec__AnonStorey53__ctor_m3135036728 (U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<TimerFrame>c__AnonStorey53::<>m__56(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerFrameU3Ec__AnonStorey53_U3CU3Em__56_m685773346_MetadataUsageId;
extern "C"  Il2CppObject * U3CTimerFrameU3Ec__AnonStorey53_U3CU3Em__56_m685773346 (U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerFrameU3Ec__AnonStorey53_U3CU3Em__56_m685773346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		int32_t L_1 = __this->get_dueTimeFrameCount_0();
		int32_t L_2 = __this->get_frameCountType_1();
		CancellationToken_t1439151560  L_3 = ___cancellation1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = Observable_TimerFrameCore_m1545514344(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UniRx.Observable/<TimerFrame>c__AnonStorey54::.ctor()
extern "C"  void U3CTimerFrameU3Ec__AnonStorey54__ctor_m2938523223 (U3CTimerFrameU3Ec__AnonStorey54_t1969998060 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<TimerFrame>c__AnonStorey54::<>m__57(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerFrameU3Ec__AnonStorey54_U3CU3Em__57_m2442044674_MetadataUsageId;
extern "C"  Il2CppObject * U3CTimerFrameU3Ec__AnonStorey54_U3CU3Em__57_m2442044674 (U3CTimerFrameU3Ec__AnonStorey54_t1969998060 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerFrameU3Ec__AnonStorey54_U3CU3Em__57_m2442044674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		int32_t L_1 = __this->get_dueTimeFrameCount_0();
		int32_t L_2 = __this->get_periodFrameCount_1();
		int32_t L_3 = __this->get_frameCountType_2();
		CancellationToken_t1439151560  L_4 = ___cancellation1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = Observable_TimerFrameCore_m2679907233(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator15::.ctor()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator15__ctor_m318033033 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerFrameCoreU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1653767475 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerFrameCoreU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m2597537479 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<TimerFrameCore>c__Iterator15::MoveNext()
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerFrameCoreU3Ec__Iterator15_MoveNext_m1362424475_MetadataUsageId;
extern "C"  bool U3CTimerFrameCoreU3Ec__Iterator15_MoveNext_m1362424475 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerFrameCoreU3Ec__Iterator15_MoveNext_m1362424475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00a2;
		}
	}
	{
		goto IL_00b9;
	}

IL_0021:
	{
		int32_t L_2 = __this->get_dueTimeFrameCount_0();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		__this->set_dueTimeFrameCount_0(0);
	}

IL_0034:
	{
		__this->set_U3CcurrentFrameU3E__0_1(0);
		int32_t L_3 = __this->get_frameCountType_2();
		YieldInstruction_t3557331758 * L_4 = FrameCountTypeExtensions_GetYieldInstruction_m1644022139(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_U3CyieldInstructionU3E__1_3(L_4);
		goto IL_00a2;
	}

IL_0051:
	{
		int32_t L_5 = __this->get_U3CcurrentFrameU3E__0_1();
		int32_t L_6 = L_5;
		V_1 = L_6;
		__this->set_U3CcurrentFrameU3E__0_1(((int32_t)((int32_t)L_6+(int32_t)1)));
		int32_t L_7 = V_1;
		int32_t L_8 = __this->get_dueTimeFrameCount_0();
		if ((!(((uint32_t)L_7) == ((uint32_t)L_8))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject* L_9 = __this->get_observer_5();
		NullCheck(L_9);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_9, (((int64_t)((int64_t)0))));
		Il2CppObject* L_10 = __this->get_observer_5();
		NullCheck(L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_10);
		goto IL_00b2;
	}

IL_008a:
	{
		YieldInstruction_t3557331758 * L_11 = __this->get_U3CyieldInstructionU3E__1_3();
		__this->set_U24current_7(L_11);
		__this->set_U24PC_6(1);
		goto IL_00bb;
	}

IL_00a2:
	{
		CancellationToken_t1439151560 * L_12 = __this->get_address_of_cancel_4();
		bool L_13 = CancellationToken_get_IsCancellationRequested_m1021497867(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0051;
		}
	}

IL_00b2:
	{
		__this->set_U24PC_6((-1));
	}

IL_00b9:
	{
		return (bool)0;
	}

IL_00bb:
	{
		return (bool)1;
	}
	// Dead block : IL_00bd: ldloc.2
}
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator15::Dispose()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator15_Dispose_m585349766 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator15::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerFrameCoreU3Ec__Iterator15_Reset_m2259433270_MetadataUsageId;
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator15_Reset_m2259433270 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerFrameCoreU3Ec__Iterator15_Reset_m2259433270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator16::.ctor()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator16__ctor_m121519528 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerFrameCoreU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1445069172 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerFrameCoreU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m273509640 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<TimerFrameCore>c__Iterator16::MoveNext()
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerFrameCoreU3Ec__Iterator16_MoveNext_m1069021468_MetadataUsageId;
extern "C"  bool U3CTimerFrameCoreU3Ec__Iterator16_MoveNext_m1069021468 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerFrameCoreU3Ec__Iterator16_MoveNext_m1069021468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	bool V_3 = false;
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_00cd;
		}
		if (L_1 == 2)
		{
			goto IL_013a;
		}
	}
	{
		goto IL_0151;
	}

IL_0025:
	{
		int32_t L_2 = __this->get_dueTimeFrameCount_0();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		__this->set_dueTimeFrameCount_0(0);
	}

IL_0038:
	{
		int32_t L_3 = __this->get_periodFrameCount_1();
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_004b;
		}
	}
	{
		__this->set_periodFrameCount_1(1);
	}

IL_004b:
	{
		__this->set_U3CsendCountU3E__0_2((((int64_t)((int64_t)0))));
		__this->set_U3CcurrentFrameU3E__1_3(0);
		int32_t L_4 = __this->get_frameCountType_4();
		YieldInstruction_t3557331758 * L_5 = FrameCountTypeExtensions_GetYieldInstruction_m1644022139(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_U3CyieldInstructionU3E__2_5(L_5);
		goto IL_00cd;
	}

IL_0070:
	{
		int32_t L_6 = __this->get_U3CcurrentFrameU3E__1_3();
		int32_t L_7 = L_6;
		V_1 = L_7;
		__this->set_U3CcurrentFrameU3E__1_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_1;
		int32_t L_9 = __this->get_dueTimeFrameCount_0();
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_00b5;
		}
	}
	{
		Il2CppObject* L_10 = __this->get_observer_7();
		int64_t L_11 = __this->get_U3CsendCountU3E__0_2();
		int64_t L_12 = L_11;
		V_2 = L_12;
		__this->set_U3CsendCountU3E__0_2(((int64_t)((int64_t)L_12+(int64_t)(((int64_t)((int64_t)1))))));
		int64_t L_13 = V_2;
		NullCheck(L_10);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_10, L_13);
		__this->set_U3CcurrentFrameU3E__1_3((-1));
		goto IL_00dd;
	}

IL_00b5:
	{
		YieldInstruction_t3557331758 * L_14 = __this->get_U3CyieldInstructionU3E__2_5();
		__this->set_U24current_9(L_14);
		__this->set_U24PC_8(1);
		goto IL_0153;
	}

IL_00cd:
	{
		CancellationToken_t1439151560 * L_15 = __this->get_address_of_cancel_6();
		bool L_16 = CancellationToken_get_IsCancellationRequested_m1021497867(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0070;
		}
	}

IL_00dd:
	{
		goto IL_013a;
	}

IL_00e2:
	{
		int32_t L_17 = __this->get_U3CcurrentFrameU3E__1_3();
		int32_t L_18 = ((int32_t)((int32_t)L_17+(int32_t)1));
		V_1 = L_18;
		__this->set_U3CcurrentFrameU3E__1_3(L_18);
		int32_t L_19 = V_1;
		int32_t L_20 = __this->get_periodFrameCount_1();
		if ((!(((uint32_t)L_19) == ((uint32_t)L_20))))
		{
			goto IL_0122;
		}
	}
	{
		Il2CppObject* L_21 = __this->get_observer_7();
		int64_t L_22 = __this->get_U3CsendCountU3E__0_2();
		int64_t L_23 = L_22;
		V_2 = L_23;
		__this->set_U3CsendCountU3E__0_2(((int64_t)((int64_t)L_23+(int64_t)(((int64_t)((int64_t)1))))));
		int64_t L_24 = V_2;
		NullCheck(L_21);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_21, L_24);
		__this->set_U3CcurrentFrameU3E__1_3(0);
	}

IL_0122:
	{
		YieldInstruction_t3557331758 * L_25 = __this->get_U3CyieldInstructionU3E__2_5();
		__this->set_U24current_9(L_25);
		__this->set_U24PC_8(2);
		goto IL_0153;
	}

IL_013a:
	{
		CancellationToken_t1439151560 * L_26 = __this->get_address_of_cancel_6();
		bool L_27 = CancellationToken_get_IsCancellationRequested_m1021497867(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00e2;
		}
	}
	{
		__this->set_U24PC_8((-1));
	}

IL_0151:
	{
		return (bool)0;
	}

IL_0153:
	{
		return (bool)1;
	}
	// Dead block : IL_0155: ldloc.3
}
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator16::Dispose()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator16_Dispose_m714432485 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_8((-1));
		return;
	}
}
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator16::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerFrameCoreU3Ec__Iterator16_Reset_m2062919765_MetadataUsageId;
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator16_Reset_m2062919765 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerFrameCoreU3Ec__Iterator16_Reset_m2062919765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<ToAsync>c__AnonStorey3A::.ctor()
extern "C"  void U3CToAsyncU3Ec__AnonStorey3A__ctor_m3205604775 (U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable/<ToAsync>c__AnonStorey3A::<>m__3D()
extern Il2CppClass* U3CToAsyncU3Ec__AnonStorey3B_t2315298077_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncSubject_1_t3257925142_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* AsyncSubject_1__ctor_m781162576_MethodInfo_var;
extern const MethodInfo* U3CToAsyncU3Ec__AnonStorey3B_U3CU3Em__63_m3825452697_MethodInfo_var;
extern const MethodInfo* Observable_AsObservable_TisUnit_t2558286038_m1062057990_MethodInfo_var;
extern const uint32_t U3CToAsyncU3Ec__AnonStorey3A_U3CU3Em__3D_m2211096745_MetadataUsageId;
extern "C"  Il2CppObject* U3CToAsyncU3Ec__AnonStorey3A_U3CU3Em__3D_m2211096745 (U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToAsyncU3Ec__AnonStorey3A_U3CU3Em__3D_m2211096745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * V_0 = NULL;
	{
		U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * L_0 = (U3CToAsyncU3Ec__AnonStorey3B_t2315298077 *)il2cpp_codegen_object_new(U3CToAsyncU3Ec__AnonStorey3B_t2315298077_il2cpp_TypeInfo_var);
		U3CToAsyncU3Ec__AnonStorey3B__ctor_m2410514707(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2458_1(__this);
		U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * L_2 = V_0;
		AsyncSubject_1_t3257925142 * L_3 = (AsyncSubject_1_t3257925142 *)il2cpp_codegen_object_new(AsyncSubject_1_t3257925142_il2cpp_TypeInfo_var);
		AsyncSubject_1__ctor_m781162576(L_3, /*hidden argument*/AsyncSubject_1__ctor_m781162576_MethodInfo_var);
		NullCheck(L_2);
		L_2->set_subject_0(L_3);
		Il2CppObject * L_4 = __this->get_scheduler_0();
		U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CToAsyncU3Ec__AnonStorey3B_U3CU3Em__63_m3825452697_MethodInfo_var);
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, L_4, L_7);
		U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * L_8 = V_0;
		NullCheck(L_8);
		AsyncSubject_1_t3257925142 * L_9 = L_8->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_AsObservable_TisUnit_t2558286038_m1062057990(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_AsObservable_TisUnit_t2558286038_m1062057990_MethodInfo_var);
		return L_10;
	}
}
// System.Void UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B::.ctor()
extern "C"  void U3CToAsyncU3Ec__AnonStorey3B__ctor_m2410514707 (U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B::<>m__63()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* AsyncSubject_1_OnError_m3927640583_MethodInfo_var;
extern const MethodInfo* AsyncSubject_1_OnNext_m1908662008_MethodInfo_var;
extern const MethodInfo* AsyncSubject_1_OnCompleted_m1935364058_MethodInfo_var;
extern const uint32_t U3CToAsyncU3Ec__AnonStorey3B_U3CU3Em__63_m3825452697_MetadataUsageId;
extern "C"  void U3CToAsyncU3Ec__AnonStorey3B_U3CU3Em__63_m3825452697 (U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToAsyncU3Ec__AnonStorey3B_U3CU3Em__63_m3825452697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		U3CToAsyncU3Ec__AnonStorey3A_t2315298076 * L_0 = __this->get_U3CU3Ef__refU2458_1();
		NullCheck(L_0);
		Action_t437523947 * L_1 = L_0->get_action_1();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
		goto IL_002c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0015;
		throw e;
	}

CATCH_0015:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t1967233988 *)__exception_local);
			AsyncSubject_1_t3257925142 * L_2 = __this->get_subject_0();
			Exception_t1967233988 * L_3 = V_0;
			NullCheck(L_2);
			AsyncSubject_1_OnError_m3927640583(L_2, L_3, /*hidden argument*/AsyncSubject_1_OnError_m3927640583_MethodInfo_var);
			goto IL_0047;
		}

IL_0027:
		{
			; // IL_0027: leave IL_002c
		}
	} // end catch (depth: 1)

IL_002c:
	{
		AsyncSubject_1_t3257925142 * L_4 = __this->get_subject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_5 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		AsyncSubject_1_OnNext_m1908662008(L_4, L_5, /*hidden argument*/AsyncSubject_1_OnNext_m1908662008_MethodInfo_var);
		AsyncSubject_1_t3257925142 * L_6 = __this->get_subject_0();
		NullCheck(L_6);
		AsyncSubject_1_OnCompleted_m1935364058(L_6, /*hidden argument*/AsyncSubject_1_OnCompleted_m1935364058_MethodInfo_var);
	}

IL_0047:
	{
		return;
	}
}
// System.Void UniRx.Observable/<ToObservable>c__AnonStorey51::.ctor()
extern "C"  void U3CToObservableU3Ec__AnonStorey51__ctor_m3092923844 (U3CToObservableU3Ec__AnonStorey51_t3647079135 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Observable/<ToObservable>c__AnonStorey51::<>m__4E(UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const uint32_t U3CToObservableU3Ec__AnonStorey51_U3CU3Em__4E_m3556682324_MetadataUsageId;
extern "C"  Il2CppObject * U3CToObservableU3Ec__AnonStorey51_U3CU3Em__4E_m3556682324 (U3CToObservableU3Ec__AnonStorey51_t3647079135 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToObservableU3Ec__AnonStorey51_U3CU3Em__4E_m3556682324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_coroutine_0();
		Il2CppObject* L_1 = ___observer0;
		CancellationToken_t1439151560  L_2 = ___cancellationToken1;
		bool L_3 = __this->get_publishEveryYield_1();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = Observable_WrapEnumerator_m2325795528(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::.ctor()
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF__ctor_m1766589338 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Observable/<WrapEnumerator>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWrapEnumeratorU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1959823288 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Object UniRx.Observable/<WrapEnumerator>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWrapEnumeratorU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3199793996 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Boolean UniRx.Observable/<WrapEnumerator>c__IteratorF::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CWrapEnumeratorU3Ec__IteratorF_MoveNext_m4012428594_MetadataUsageId;
extern "C"  bool U3CWrapEnumeratorU3Ec__IteratorF_MoveNext_m4012428594 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWrapEnumeratorU3Ec__IteratorF_MoveNext_m4012428594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00fd;
		}
	}
	{
		goto IL_0161;
	}

IL_0021:
	{
		__this->set_U3ChasNextU3E__0_0((bool)0);
		__this->set_U3CraisedErrorU3E__1_1((bool)0);
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_2 = __this->get_enumerator_2();
		NullCheck(L_2);
		bool L_3 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_2);
		__this->set_U3ChasNextU3E__0_0(L_3);
		goto IL_007b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0045;
		throw e;
	}

CATCH_0045:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_4 = V_1;
			__this->set_U3CexU3E__2_4(L_4);
		}

IL_004d:
		try
		{ // begin try (depth: 2)
			__this->set_U3CraisedErrorU3E__1_1((bool)1);
			Il2CppObject* L_5 = __this->get_observer_3();
			Exception_t1967233988 * L_6 = __this->get_U3CexU3E__2_4();
			NullCheck(L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, L_5, L_6);
			IL2CPP_LEAVE(0x71, FINALLY_006a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_006a;
		}

FINALLY_006a:
		{ // begin finally (depth: 2)
			U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally0_m1302742425(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(106)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(106)
		{
			IL2CPP_JUMP_TBL(0x71, IL_0071)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0071:
		{
			goto IL_0161;
		}

IL_0076:
		{
			; // IL_0076: leave IL_007b
		}
	} // end catch (depth: 1)

IL_007b:
	{
		bool L_7 = __this->get_U3ChasNextU3E__0_0();
		if (!L_7)
		{
			goto IL_00d5;
		}
	}
	{
		bool L_8 = __this->get_publishEveryYield_6();
		if (!L_8)
		{
			goto IL_00d5;
		}
	}

IL_0091:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = __this->get_observer_3();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_10 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, L_9, L_10);
		goto IL_00d5;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00a6;
		throw e;
	}

CATCH_00a6:
	{ // begin catch(System.Object)
		{
			Il2CppObject * L_11 = __this->get_enumerator_2();
			__this->set_U3CdU3E__4_7(((Il2CppObject *)IsInst(L_11, IDisposable_t1628921374_il2cpp_TypeInfo_var)));
			Il2CppObject * L_12 = __this->get_U3CdU3E__4_7();
			if (!L_12)
			{
				goto IL_00ce;
			}
		}

IL_00c3:
		{
			Il2CppObject * L_13 = __this->get_U3CdU3E__4_7();
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_13);
		}

IL_00ce:
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_00d0:
		{
			goto IL_00d5;
		}
	} // end catch (depth: 1)

IL_00d5:
	{
		bool L_14 = __this->get_U3ChasNextU3E__0_0();
		if (!L_14)
		{
			goto IL_00fd;
		}
	}
	{
		Il2CppObject * L_15 = __this->get_enumerator_2();
		NullCheck(L_15);
		Il2CppObject * L_16 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_15);
		__this->set_U24current_11(L_16);
		__this->set_U24PC_10(1);
		goto IL_0163;
	}

IL_00fd:
	{
		bool L_17 = __this->get_U3ChasNextU3E__0_0();
		if (!L_17)
		{
			goto IL_0118;
		}
	}
	{
		CancellationToken_t1439151560 * L_18 = __this->get_address_of_cancellationToken_8();
		bool L_19 = CancellationToken_get_IsCancellationRequested_m1021497867(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_002f;
		}
	}

IL_0118:
	try
	{ // begin try (depth: 1)
		{
			bool L_20 = __this->get_U3CraisedErrorU3E__1_1();
			if (L_20)
			{
				goto IL_014e;
			}
		}

IL_0123:
		{
			CancellationToken_t1439151560 * L_21 = __this->get_address_of_cancellationToken_8();
			bool L_22 = CancellationToken_get_IsCancellationRequested_m1021497867(L_21, /*hidden argument*/NULL);
			if (L_22)
			{
				goto IL_014e;
			}
		}

IL_0133:
		{
			Il2CppObject* L_23 = __this->get_observer_3();
			IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
			Unit_t2558286038  L_24 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_23);
			InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, L_23, L_24);
			Il2CppObject* L_25 = __this->get_observer_3();
			NullCheck(L_25);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IObserver_1_t475317645_il2cpp_TypeInfo_var, L_25);
		}

IL_014e:
		{
			IL2CPP_LEAVE(0x15A, FINALLY_0153);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0153;
	}

FINALLY_0153:
	{ // begin finally (depth: 1)
		U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally1_m1302743386(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(339)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(339)
	{
		IL2CPP_JUMP_TBL(0x15A, IL_015a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_015a:
	{
		__this->set_U24PC_10((-1));
	}

IL_0161:
	{
		return (bool)0;
	}

IL_0163:
	{
		return (bool)1;
	}
	// Dead block : IL_0165: ldloc.2
}
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::Dispose()
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF_Dispose_m1078554967 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_10((-1));
		return;
	}
}
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CWrapEnumeratorU3Ec__IteratorF_Reset_m3707989575_MetadataUsageId;
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF_Reset_m3707989575 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWrapEnumeratorU3Ec__IteratorF_Reset_m3707989575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::<>__Finally0()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally0_m1302742425_MetadataUsageId;
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally0_m1302742425 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally0_m1302742425_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_enumerator_2();
		__this->set_U3CdU3E__3_5(((Il2CppObject *)IsInst(L_0, IDisposable_t1628921374_il2cpp_TypeInfo_var)));
		Il2CppObject * L_1 = __this->get_U3CdU3E__3_5();
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_2 = __this->get_U3CdU3E__3_5();
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_2);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UniRx.Observable/<WrapEnumerator>c__IteratorF::<>__Finally1()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally1_m1302743386_MetadataUsageId;
extern "C"  void U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally1_m1302743386 (U3CWrapEnumeratorU3Ec__IteratorF_t3900889545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWrapEnumeratorU3Ec__IteratorF_U3CU3E__Finally1_m1302743386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_enumerator_2();
		__this->set_U3CdU3E__5_9(((Il2CppObject *)IsInst(L_0, IDisposable_t1628921374_il2cpp_TypeInfo_var)));
		Il2CppObject * L_1 = __this->get_U3CdU3E__5_9();
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_2 = __this->get_U3CdU3E__5_9();
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_2);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UniRx.Observable/EveryAfterUpdateInvoker::.ctor(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  void EveryAfterUpdateInvoker__ctor_m2456117825 (EveryAfterUpdateInvoker_t3949333456 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	{
		__this->set_count_0((((int64_t)((int64_t)(-1)))));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		__this->set_observer_1(L_0);
		CancellationToken_t1439151560  L_1 = ___cancellationToken1;
		__this->set_cancellationToken_2(L_1);
		return;
	}
}
// System.Boolean UniRx.Observable/EveryAfterUpdateInvoker::MoveNext()
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern const uint32_t EveryAfterUpdateInvoker_MoveNext_m3416459249_MetadataUsageId;
extern "C"  bool EveryAfterUpdateInvoker_MoveNext_m3416459249 (EveryAfterUpdateInvoker_t3949333456 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EveryAfterUpdateInvoker_MoveNext_m3416459249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CancellationToken_t1439151560  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int64_t V_1 = 0;
	{
		CancellationToken_t1439151560  L_0 = __this->get_cancellationToken_2();
		V_0 = L_0;
		bool L_1 = CancellationToken_get_IsCancellationRequested_m1021497867((&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0053;
		}
	}
	{
		int64_t L_2 = __this->get_count_0();
		if ((((int64_t)L_2) == ((int64_t)(((int64_t)((int64_t)(-1)))))))
		{
			goto IL_0042;
		}
	}
	{
		Il2CppObject* L_3 = __this->get_observer_1();
		int64_t L_4 = __this->get_count_0();
		int64_t L_5 = L_4;
		V_1 = L_5;
		__this->set_count_0(((int64_t)((int64_t)L_5+(int64_t)(((int64_t)((int64_t)1))))));
		int64_t L_6 = V_1;
		NullCheck(L_3);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_3, L_6);
		goto IL_0051;
	}

IL_0042:
	{
		int64_t L_7 = __this->get_count_0();
		__this->set_count_0(((int64_t)((int64_t)L_7+(int64_t)(((int64_t)((int64_t)1))))));
	}

IL_0051:
	{
		return (bool)1;
	}

IL_0053:
	{
		return (bool)0;
	}
}
// System.Object UniRx.Observable/EveryAfterUpdateInvoker::get_Current()
extern "C"  Il2CppObject * EveryAfterUpdateInvoker_get_Current_m1905716556 (EveryAfterUpdateInvoker_t3949333456 * __this, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Void UniRx.Observable/EveryAfterUpdateInvoker::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t EveryAfterUpdateInvoker_Reset_m2871727776_MetadataUsageId;
extern "C"  void EveryAfterUpdateInvoker_Reset_m2871727776 (EveryAfterUpdateInvoker_t3949333456 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EveryAfterUpdateInvoker_Reset_m2871727776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.ObservableMonoBehaviour::.ctor()
extern "C"  void ObservableMonoBehaviour__ctor_m1531372632 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	{
		TypedMonoBehaviour__ctor_m895605671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::Awake()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const MethodInfo* Subject_1_OnCompleted_m4016750806_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_Awake_m1768977851_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_Awake_m1768977851 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_Awake_m1768977851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_calledAwake_2((bool)1);
		Subject_1_t201353362 * L_0 = __this->get_awake_3();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_awake_3();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
		Subject_1_t201353362 * L_3 = __this->get_awake_3();
		NullCheck(L_3);
		Subject_1_OnCompleted_m4016750806(L_3, /*hidden argument*/Subject_1_OnCompleted_m4016750806_MethodInfo_var);
	}

IL_002d:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::AwakeAsObservable()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_AwakeAsObservable_m2790939256_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_AwakeAsObservable_m2790939256 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_AwakeAsObservable_m2790939256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B4_0 = NULL;
	Subject_1_t201353362 * G_B3_0 = NULL;
	{
		bool L_0 = __this->get_calledAwake_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_1 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = Observable_Return_TisUnit_t2558286038_m1480352924(NULL /*static, unused*/, L_1, /*hidden argument*/Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var);
		return L_2;
	}

IL_0016:
	{
		Subject_1_t201353362 * L_3 = __this->get_awake_3();
		Subject_1_t201353362 * L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_0031;
		}
	}
	{
		Subject_1_t201353362 * L_5 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_5, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_6 = L_5;
		V_0 = L_6;
		__this->set_awake_3(L_6);
		Subject_1_t201353362 * L_7 = V_0;
		G_B4_0 = L_7;
	}

IL_0031:
	{
		return G_B4_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::FixedUpdate()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_FixedUpdate_m1622810579_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_FixedUpdate_m1622810579 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_FixedUpdate_m1622810579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_fixedUpdate_4();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_fixedUpdate_4();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::FixedUpdateAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_FixedUpdateAsObservable_m330377872_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_FixedUpdateAsObservable_m330377872 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_FixedUpdateAsObservable_m330377872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_fixedUpdate_4();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_fixedUpdate_4(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::LateUpdate()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_LateUpdate_m3764870395_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_LateUpdate_m3764870395 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_LateUpdate_m3764870395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_lateUpdate_5();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_lateUpdate_5();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::LateUpdateAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_LateUpdateAsObservable_m2981047080_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_LateUpdateAsObservable_m2981047080 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_LateUpdateAsObservable_m2981047080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_lateUpdate_5();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_lateUpdate_5(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnAnimatorIK(System.Int32)
extern const MethodInfo* Subject_1_OnNext_m1100834663_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnAnimatorIK_m2050465221_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnAnimatorIK_m2050465221 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnAnimatorIK_m2050465221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t490482111 * L_0 = __this->get_onAnimatorIK_6();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t490482111 * L_1 = __this->get_onAnimatorIK_6();
		int32_t L_2 = ___layerIndex0;
		NullCheck(L_1);
		Subject_1_OnNext_m1100834663(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m1100834663_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<System.Int32> UniRx.ObservableMonoBehaviour::OnAnimatorIKAsObservable()
extern Il2CppClass* Subject_1_t490482111_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1321103487_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnAnimatorIKAsObservable_m2682374158_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnAnimatorIKAsObservable_m2682374158 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnAnimatorIKAsObservable_m2682374158_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t490482111 * V_0 = NULL;
	Subject_1_t490482111 * G_B2_0 = NULL;
	Subject_1_t490482111 * G_B1_0 = NULL;
	{
		Subject_1_t490482111 * L_0 = __this->get_onAnimatorIK_6();
		Subject_1_t490482111 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t490482111 * L_2 = (Subject_1_t490482111 *)il2cpp_codegen_object_new(Subject_1_t490482111_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1321103487(L_2, /*hidden argument*/Subject_1__ctor_m1321103487_MethodInfo_var);
		Subject_1_t490482111 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onAnimatorIK_6(L_3);
		Subject_1_t490482111 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnAnimatorMove()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnAnimatorMove_m4133172547_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnAnimatorMove_m4133172547 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnAnimatorMove_m4133172547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onAnimatorMove_7();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onAnimatorMove_7();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnAnimatorMoveAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnAnimatorMoveAsObservable_m2632743792_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnAnimatorMoveAsObservable_m2632743792 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnAnimatorMoveAsObservable_m2632743792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onAnimatorMove_7();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onAnimatorMove_7(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnApplicationFocus(System.Boolean)
extern const MethodInfo* Subject_1_OnNext_m385413889_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnApplicationFocus_m1603483274_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnApplicationFocus_m1603483274 (ObservableMonoBehaviour_t2173505449 * __this, bool ___focus0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnApplicationFocus_m1603483274_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2149039961 * L_0 = __this->get_onApplicationFocus_8();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2149039961 * L_1 = __this->get_onApplicationFocus_8();
		bool L_2 = ___focus0;
		NullCheck(L_1);
		Subject_1_OnNext_m385413889(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m385413889_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<System.Boolean> UniRx.ObservableMonoBehaviour::OnApplicationFocusAsObservable()
extern Il2CppClass* Subject_1_t2149039961_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m886839961_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnApplicationFocusAsObservable_m3673414419_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnApplicationFocusAsObservable_m3673414419 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnApplicationFocusAsObservable_m3673414419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2149039961 * V_0 = NULL;
	Subject_1_t2149039961 * G_B2_0 = NULL;
	Subject_1_t2149039961 * G_B1_0 = NULL;
	{
		Subject_1_t2149039961 * L_0 = __this->get_onApplicationFocus_8();
		Subject_1_t2149039961 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2149039961 * L_2 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(Subject_1_t2149039961_il2cpp_TypeInfo_var);
		Subject_1__ctor_m886839961(L_2, /*hidden argument*/Subject_1__ctor_m886839961_MethodInfo_var);
		Subject_1_t2149039961 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onApplicationFocus_8(L_3);
		Subject_1_t2149039961 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnApplicationPause(System.Boolean)
extern const MethodInfo* Subject_1_OnNext_m385413889_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnApplicationPause_m3057650600_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnApplicationPause_m3057650600 (ObservableMonoBehaviour_t2173505449 * __this, bool ___pause0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnApplicationPause_m3057650600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2149039961 * L_0 = __this->get_onApplicationPause_9();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2149039961 * L_1 = __this->get_onApplicationPause_9();
		bool L_2 = ___pause0;
		NullCheck(L_1);
		Subject_1_OnNext_m385413889(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m385413889_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<System.Boolean> UniRx.ObservableMonoBehaviour::OnApplicationPauseAsObservable()
extern Il2CppClass* Subject_1_t2149039961_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m886839961_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnApplicationPauseAsObservable_m577724337_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnApplicationPauseAsObservable_m577724337 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnApplicationPauseAsObservable_m577724337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2149039961 * V_0 = NULL;
	Subject_1_t2149039961 * G_B2_0 = NULL;
	Subject_1_t2149039961 * G_B1_0 = NULL;
	{
		Subject_1_t2149039961 * L_0 = __this->get_onApplicationPause_9();
		Subject_1_t2149039961 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2149039961 * L_2 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(Subject_1_t2149039961_il2cpp_TypeInfo_var);
		Subject_1__ctor_m886839961(L_2, /*hidden argument*/Subject_1__ctor_m886839961_MethodInfo_var);
		Subject_1_t2149039961 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onApplicationPause_9(L_3);
		Subject_1_t2149039961 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnApplicationQuit()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnApplicationQuit_m1042517462_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnApplicationQuit_m1042517462 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnApplicationQuit_m1042517462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onApplicationQuit_10();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onApplicationQuit_10();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnApplicationQuitAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnApplicationQuitAsObservable_m1251128851_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnApplicationQuitAsObservable_m1251128851 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnApplicationQuitAsObservable_m1251128851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onApplicationQuit_10();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onApplicationQuit_10(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnAudioFilterRead(System.Single[],System.Int32)
extern const MethodInfo* Tuple_Create_TisSingleU5BU5D_t1219431280_TisInt32_t2847414787_m3644026774_MethodInfo_var;
extern const MethodInfo* Subject_1_OnNext_m2738199892_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnAudioFilterRead_m428845417_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnAudioFilterRead_m428845417 (ObservableMonoBehaviour_t2173505449 * __this, SingleU5BU5D_t1219431280* ___data0, int32_t ___channels1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnAudioFilterRead_m428845417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t1594179034 * L_0 = __this->get_onAudioFilterRead_11();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Subject_1_t1594179034 * L_1 = __this->get_onAudioFilterRead_11();
		SingleU5BU5D_t1219431280* L_2 = ___data0;
		int32_t L_3 = ___channels1;
		Tuple_2_t3951111710  L_4 = Tuple_Create_TisSingleU5BU5D_t1219431280_TisInt32_t2847414787_m3644026774(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/Tuple_Create_TisSingleU5BU5D_t1219431280_TisInt32_t2847414787_m3644026774_MethodInfo_var);
		NullCheck(L_1);
		Subject_1_OnNext_m2738199892(L_1, L_4, /*hidden argument*/Subject_1_OnNext_m2738199892_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Tuple`2<System.Single[],System.Int32>> UniRx.ObservableMonoBehaviour::OnAudioFilterReadAsObservable()
extern Il2CppClass* Subject_1_t1594179034_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1523924396_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnAudioFilterReadAsObservable_m595639890_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnAudioFilterReadAsObservable_m595639890 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnAudioFilterReadAsObservable_m595639890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t1594179034 * V_0 = NULL;
	Subject_1_t1594179034 * G_B2_0 = NULL;
	Subject_1_t1594179034 * G_B1_0 = NULL;
	{
		Subject_1_t1594179034 * L_0 = __this->get_onAudioFilterRead_11();
		Subject_1_t1594179034 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t1594179034 * L_2 = (Subject_1_t1594179034 *)il2cpp_codegen_object_new(Subject_1_t1594179034_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1523924396(L_2, /*hidden argument*/Subject_1__ctor_m1523924396_MethodInfo_var);
		Subject_1_t1594179034 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onAudioFilterRead_11(L_3);
		Subject_1_t1594179034 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnBecameInvisible()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnBecameInvisible_m4223550283_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnBecameInvisible_m4223550283 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnBecameInvisible_m4223550283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onBecameInvisible_12();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onBecameInvisible_12();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnBecameInvisibleAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnBecameInvisibleAsObservable_m380101640_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnBecameInvisibleAsObservable_m380101640 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnBecameInvisibleAsObservable_m380101640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onBecameInvisible_12();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onBecameInvisible_12(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnBecameVisible()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnBecameVisible_m2724211216_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnBecameVisible_m2724211216 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnBecameVisible_m2724211216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onBecameVisible_13();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onBecameVisible_13();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnBecameVisibleAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnBecameVisibleAsObservable_m1725013325_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnBecameVisibleAsObservable_m1725013325 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnBecameVisibleAsObservable_m1725013325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onBecameVisible_13();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onBecameVisible_13(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionEnter(UnityEngine.Collision)
extern const MethodInfo* Subject_1_OnNext_m4081039037_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionEnter_m3972548134_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnCollisionEnter_m3972548134 (ObservableMonoBehaviour_t2173505449 * __this, Collision_t1119538015 * ___collision0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionEnter_m3972548134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3057572635 * L_0 = __this->get_onCollisionEnter_14();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t3057572635 * L_1 = __this->get_onCollisionEnter_14();
		Collision_t1119538015 * L_2 = ___collision0;
		NullCheck(L_1);
		Subject_1_OnNext_m4081039037(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m4081039037_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::OnCollisionEnterAsObservable()
extern Il2CppClass* Subject_1_t3057572635_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m4130905429_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionEnterAsObservable_m3681595323_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionEnterAsObservable_m3681595323 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionEnterAsObservable_m3681595323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3057572635 * V_0 = NULL;
	Subject_1_t3057572635 * G_B2_0 = NULL;
	Subject_1_t3057572635 * G_B1_0 = NULL;
	{
		Subject_1_t3057572635 * L_0 = __this->get_onCollisionEnter_14();
		Subject_1_t3057572635 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3057572635 * L_2 = (Subject_1_t3057572635 *)il2cpp_codegen_object_new(Subject_1_t3057572635_il2cpp_TypeInfo_var);
		Subject_1__ctor_m4130905429(L_2, /*hidden argument*/Subject_1__ctor_m4130905429_MethodInfo_var);
		Subject_1_t3057572635 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onCollisionEnter_14(L_3);
		Subject_1_t3057572635 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionEnter2D(UnityEngine.Collision2D)
extern const MethodInfo* Subject_1_OnNext_m2655337999_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionEnter2D_m1819542722_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnCollisionEnter2D_m1819542722 (ObservableMonoBehaviour_t2173505449 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionEnter2D_m1819542722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2390844653 * L_0 = __this->get_onCollisionEnter2D_15();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2390844653 * L_1 = __this->get_onCollisionEnter2D_15();
		Collision2D_t452810033 * L_2 = ___coll0;
		NullCheck(L_1);
		Subject_1_OnNext_m2655337999(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m2655337999_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::OnCollisionEnter2DAsObservable()
extern Il2CppClass* Subject_1_t2390844653_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1429983527_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionEnter2DAsObservable_m933896315_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionEnter2DAsObservable_m933896315 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionEnter2DAsObservable_m933896315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2390844653 * V_0 = NULL;
	Subject_1_t2390844653 * G_B2_0 = NULL;
	Subject_1_t2390844653 * G_B1_0 = NULL;
	{
		Subject_1_t2390844653 * L_0 = __this->get_onCollisionEnter2D_15();
		Subject_1_t2390844653 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2390844653 * L_2 = (Subject_1_t2390844653 *)il2cpp_codegen_object_new(Subject_1_t2390844653_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1429983527(L_2, /*hidden argument*/Subject_1__ctor_m1429983527_MethodInfo_var);
		Subject_1_t2390844653 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onCollisionEnter2D_15(L_3);
		Subject_1_t2390844653 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionExit(UnityEngine.Collision)
extern const MethodInfo* Subject_1_OnNext_m4081039037_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionExit_m373064656_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnCollisionExit_m373064656 (ObservableMonoBehaviour_t2173505449 * __this, Collision_t1119538015 * ___collisionInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionExit_m373064656_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3057572635 * L_0 = __this->get_onCollisionExit_16();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t3057572635 * L_1 = __this->get_onCollisionExit_16();
		Collision_t1119538015 * L_2 = ___collisionInfo0;
		NullCheck(L_1);
		Subject_1_OnNext_m4081039037(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m4081039037_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::OnCollisionExitAsObservable()
extern Il2CppClass* Subject_1_t3057572635_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m4130905429_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionExitAsObservable_m496272903_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionExitAsObservable_m496272903 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionExitAsObservable_m496272903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3057572635 * V_0 = NULL;
	Subject_1_t3057572635 * G_B2_0 = NULL;
	Subject_1_t3057572635 * G_B1_0 = NULL;
	{
		Subject_1_t3057572635 * L_0 = __this->get_onCollisionExit_16();
		Subject_1_t3057572635 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3057572635 * L_2 = (Subject_1_t3057572635 *)il2cpp_codegen_object_new(Subject_1_t3057572635_il2cpp_TypeInfo_var);
		Subject_1__ctor_m4130905429(L_2, /*hidden argument*/Subject_1__ctor_m4130905429_MethodInfo_var);
		Subject_1_t3057572635 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onCollisionExit_16(L_3);
		Subject_1_t3057572635 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionExit2D(UnityEngine.Collision2D)
extern const MethodInfo* Subject_1_OnNext_m2655337999_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionExit2D_m551378284_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnCollisionExit2D_m551378284 (ObservableMonoBehaviour_t2173505449 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionExit2D_m551378284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2390844653 * L_0 = __this->get_onCollisionExit2D_17();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2390844653 * L_1 = __this->get_onCollisionExit2D_17();
		Collision2D_t452810033 * L_2 = ___coll0;
		NullCheck(L_1);
		Subject_1_OnNext_m2655337999(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m2655337999_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::OnCollisionExit2DAsObservable()
extern Il2CppClass* Subject_1_t2390844653_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1429983527_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionExit2DAsObservable_m704116651_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionExit2DAsObservable_m704116651 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionExit2DAsObservable_m704116651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2390844653 * V_0 = NULL;
	Subject_1_t2390844653 * G_B2_0 = NULL;
	Subject_1_t2390844653 * G_B1_0 = NULL;
	{
		Subject_1_t2390844653 * L_0 = __this->get_onCollisionExit2D_17();
		Subject_1_t2390844653 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2390844653 * L_2 = (Subject_1_t2390844653 *)il2cpp_codegen_object_new(Subject_1_t2390844653_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1429983527(L_2, /*hidden argument*/Subject_1__ctor_m1429983527_MethodInfo_var);
		Subject_1_t2390844653 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onCollisionExit2D_17(L_3);
		Subject_1_t2390844653 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionStay(UnityEngine.Collision)
extern const MethodInfo* Subject_1_OnNext_m4081039037_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionStay_m4224418869_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnCollisionStay_m4224418869 (ObservableMonoBehaviour_t2173505449 * __this, Collision_t1119538015 * ___collisionInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionStay_m4224418869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3057572635 * L_0 = __this->get_onCollisionStay_18();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t3057572635 * L_1 = __this->get_onCollisionStay_18();
		Collision_t1119538015 * L_2 = ___collisionInfo0;
		NullCheck(L_1);
		Subject_1_OnNext_m4081039037(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m4081039037_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::OnCollisionStayAsObservable()
extern Il2CppClass* Subject_1_t3057572635_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m4130905429_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionStayAsObservable_m1335988226_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionStayAsObservable_m1335988226 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionStayAsObservable_m1335988226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3057572635 * V_0 = NULL;
	Subject_1_t3057572635 * G_B2_0 = NULL;
	Subject_1_t3057572635 * G_B1_0 = NULL;
	{
		Subject_1_t3057572635 * L_0 = __this->get_onCollisionStay_18();
		Subject_1_t3057572635 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3057572635 * L_2 = (Subject_1_t3057572635 *)il2cpp_codegen_object_new(Subject_1_t3057572635_il2cpp_TypeInfo_var);
		Subject_1__ctor_m4130905429(L_2, /*hidden argument*/Subject_1__ctor_m4130905429_MethodInfo_var);
		Subject_1_t3057572635 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onCollisionStay_18(L_3);
		Subject_1_t3057572635 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionStay2D(UnityEngine.Collision2D)
extern const MethodInfo* Subject_1_OnNext_m2655337999_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionStay2D_m2893783889_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnCollisionStay2D_m2893783889 (ObservableMonoBehaviour_t2173505449 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionStay2D_m2893783889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2390844653 * L_0 = __this->get_onCollisionStay2D_19();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2390844653 * L_1 = __this->get_onCollisionStay2D_19();
		Collision2D_t452810033 * L_2 = ___coll0;
		NullCheck(L_1);
		Subject_1_OnNext_m2655337999(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m2655337999_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::OnCollisionStay2DAsObservable()
extern Il2CppClass* Subject_1_t2390844653_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1429983527_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnCollisionStay2DAsObservable_m216690406_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionStay2DAsObservable_m216690406 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnCollisionStay2DAsObservable_m216690406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2390844653 * V_0 = NULL;
	Subject_1_t2390844653 * G_B2_0 = NULL;
	Subject_1_t2390844653 * G_B1_0 = NULL;
	{
		Subject_1_t2390844653 * L_0 = __this->get_onCollisionStay2D_19();
		Subject_1_t2390844653 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2390844653 * L_2 = (Subject_1_t2390844653 *)il2cpp_codegen_object_new(Subject_1_t2390844653_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1429983527(L_2, /*hidden argument*/Subject_1__ctor_m1429983527_MethodInfo_var);
		Subject_1_t2390844653 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onCollisionStay2D_19(L_3);
		Subject_1_t2390844653 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnConnectedToServer()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnConnectedToServer_m2381232734_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnConnectedToServer_m2381232734 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnConnectedToServer_m2381232734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onConnectedToServer_20();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onConnectedToServer_20();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnConnectedToServerAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnConnectedToServerAsObservable_m3492218011_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnConnectedToServerAsObservable_m3492218011 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnConnectedToServerAsObservable_m3492218011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onConnectedToServer_20();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onConnectedToServer_20(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern const MethodInfo* Subject_1_OnNext_m1689426638_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnControllerColliderHit_m4134442156_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnControllerColliderHit_m4134442156 (ObservableMonoBehaviour_t2173505449 * __this, ControllerColliderHit_t2693066224 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnControllerColliderHit_m4134442156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t336133548 * L_0 = __this->get_onControllerColliderHit_21();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t336133548 * L_1 = __this->get_onControllerColliderHit_21();
		ControllerColliderHit_t2693066224 * L_2 = ___hit0;
		NullCheck(L_1);
		Subject_1_OnNext_m1689426638(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m1689426638_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.ControllerColliderHit> UniRx.ObservableMonoBehaviour::OnControllerColliderHitAsObservable()
extern Il2CppClass* Subject_1_t336133548_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m3230093734_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnControllerColliderHitAsObservable_m3711125355_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnControllerColliderHitAsObservable_m3711125355 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnControllerColliderHitAsObservable_m3711125355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t336133548 * V_0 = NULL;
	Subject_1_t336133548 * G_B2_0 = NULL;
	Subject_1_t336133548 * G_B1_0 = NULL;
	{
		Subject_1_t336133548 * L_0 = __this->get_onControllerColliderHit_21();
		Subject_1_t336133548 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t336133548 * L_2 = (Subject_1_t336133548 *)il2cpp_codegen_object_new(Subject_1_t336133548_il2cpp_TypeInfo_var);
		Subject_1__ctor_m3230093734(L_2, /*hidden argument*/Subject_1__ctor_m3230093734_MethodInfo_var);
		Subject_1_t336133548 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onControllerColliderHit_21(L_3);
		Subject_1_t336133548 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnDestroy()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const MethodInfo* Subject_1_OnCompleted_m4016750806_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDestroy_m1776209233_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnDestroy_m1776209233 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDestroy_m1776209233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_calledDestroy_22((bool)1);
		Subject_1_t201353362 * L_0 = __this->get_onDestroy_23();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onDestroy_23();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
		Subject_1_t201353362 * L_3 = __this->get_onDestroy_23();
		NullCheck(L_3);
		Subject_1_OnCompleted_m4016750806(L_3, /*hidden argument*/Subject_1_OnCompleted_m4016750806_MethodInfo_var);
	}

IL_002d:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnDestroyAsObservable()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDestroyAsObservable_m966384910_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDestroyAsObservable_m966384910 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDestroyAsObservable_m966384910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B6_0 = NULL;
	Subject_1_t201353362 * G_B5_0 = NULL;
	{
		bool L_0 = Object_op_Equality_m1690293457(NULL /*static, unused*/, __this, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_1 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = Observable_Return_TisUnit_t2558286038_m1480352924(NULL /*static, unused*/, L_1, /*hidden argument*/Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var);
		return L_2;
	}

IL_0017:
	{
		bool L_3 = __this->get_calledDestroy_22();
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_4 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_5 = Observable_Return_TisUnit_t2558286038_m1480352924(NULL /*static, unused*/, L_4, /*hidden argument*/Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var);
		return L_5;
	}

IL_002d:
	{
		Subject_1_t201353362 * L_6 = __this->get_onDestroy_23();
		Subject_1_t201353362 * L_7 = L_6;
		G_B5_0 = L_7;
		if (L_7)
		{
			G_B6_0 = L_7;
			goto IL_0048;
		}
	}
	{
		Subject_1_t201353362 * L_8 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_8, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_9 = L_8;
		V_0 = L_9;
		__this->set_onDestroy_23(L_9);
		Subject_1_t201353362 * L_10 = V_0;
		G_B6_0 = L_10;
	}

IL_0048:
	{
		return G_B6_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnDisable()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDisable_m3893644479_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnDisable_m3893644479 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDisable_m3893644479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onDisable_24();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onDisable_24();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnDisableAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDisableAsObservable_m2362812284_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDisableAsObservable_m2362812284 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDisableAsObservable_m2362812284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onDisable_24();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onDisable_24(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnDrawGizmos()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDrawGizmos_m3009550792_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnDrawGizmos_m3009550792 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDrawGizmos_m3009550792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onDrawGizmos_25();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onDrawGizmos_25();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnDrawGizmosAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDrawGizmosAsObservable_m2079600245_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDrawGizmosAsObservable_m2079600245 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDrawGizmosAsObservable_m2079600245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onDrawGizmos_25();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onDrawGizmos_25(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnDrawGizmosSelected()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDrawGizmosSelected_m3748001475_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnDrawGizmosSelected_m3748001475 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDrawGizmosSelected_m3748001475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onDrawGizmosSelected_26();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onDrawGizmosSelected_26();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnDrawGizmosSelectedAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDrawGizmosSelectedAsObservable_m197702384_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDrawGizmosSelectedAsObservable_m197702384 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDrawGizmosSelectedAsObservable_m197702384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onDrawGizmosSelected_26();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onDrawGizmosSelected_26(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnEnable()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnEnable_m2051040302_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnEnable_m2051040302 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnEnable_m2051040302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onEnable_27();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onEnable_27();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnEnableAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnEnableAsObservable_m1977314267_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnEnableAsObservable_m1977314267 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnEnableAsObservable_m1977314267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onEnable_27();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onEnable_27(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnJointBreak(System.Single)
extern const MethodInfo* Subject_1_OnNext_m3249041097_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnJointBreak_m3735926251_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnJointBreak_m3735926251 (ObservableMonoBehaviour_t2173505449 * __this, float ___breakForce0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnJointBreak_m3735926251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2896243641 * L_0 = __this->get_onJointBreak_28();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2896243641 * L_1 = __this->get_onJointBreak_28();
		float L_2 = ___breakForce0;
		NullCheck(L_1);
		Subject_1_OnNext_m3249041097(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m3249041097_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<System.Single> UniRx.ObservableMonoBehaviour::OnJointBreakAsObservable()
extern Il2CppClass* Subject_1_t2896243641_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m3410487393_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnJointBreakAsObservable_m1818163774_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnJointBreakAsObservable_m1818163774 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnJointBreakAsObservable_m1818163774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2896243641 * V_0 = NULL;
	Subject_1_t2896243641 * G_B2_0 = NULL;
	Subject_1_t2896243641 * G_B1_0 = NULL;
	{
		Subject_1_t2896243641 * L_0 = __this->get_onJointBreak_28();
		Subject_1_t2896243641 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2896243641 * L_2 = (Subject_1_t2896243641 *)il2cpp_codegen_object_new(Subject_1_t2896243641_il2cpp_TypeInfo_var);
		Subject_1__ctor_m3410487393(L_2, /*hidden argument*/Subject_1__ctor_m3410487393_MethodInfo_var);
		Subject_1_t2896243641 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onJointBreak_28(L_3);
		Subject_1_t2896243641 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnLevelWasLoaded(System.Int32)
extern const MethodInfo* Subject_1_OnNext_m1100834663_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnLevelWasLoaded_m1194111462_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnLevelWasLoaded_m1194111462 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnLevelWasLoaded_m1194111462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t490482111 * L_0 = __this->get_onLevelWasLoaded_29();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t490482111 * L_1 = __this->get_onLevelWasLoaded_29();
		int32_t L_2 = ___level0;
		NullCheck(L_1);
		Subject_1_OnNext_m1100834663(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m1100834663_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<System.Int32> UniRx.ObservableMonoBehaviour::OnLevelWasLoadedAsObservable()
extern Il2CppClass* Subject_1_t490482111_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1321103487_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnLevelWasLoadedAsObservable_m1159610799_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnLevelWasLoadedAsObservable_m1159610799 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnLevelWasLoadedAsObservable_m1159610799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t490482111 * V_0 = NULL;
	Subject_1_t490482111 * G_B2_0 = NULL;
	Subject_1_t490482111 * G_B1_0 = NULL;
	{
		Subject_1_t490482111 * L_0 = __this->get_onLevelWasLoaded_29();
		Subject_1_t490482111 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t490482111 * L_2 = (Subject_1_t490482111 *)il2cpp_codegen_object_new(Subject_1_t490482111_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1321103487(L_2, /*hidden argument*/Subject_1__ctor_m1321103487_MethodInfo_var);
		Subject_1_t490482111 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onLevelWasLoaded_29(L_3);
		Subject_1_t490482111 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnParticleCollision(UnityEngine.GameObject)
extern const MethodInfo* Subject_1_OnNext_m172481632_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnParticleCollision_m3194519515_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnParticleCollision_m3194519515 (ObservableMonoBehaviour_t2173505449 * __this, GameObject_t4012695102 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnParticleCollision_m3194519515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t1655762426 * L_0 = __this->get_onParticleCollision_30();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t1655762426 * L_1 = __this->get_onParticleCollision_30();
		GameObject_t4012695102 * L_2 = ___other0;
		NullCheck(L_1);
		Subject_1_OnNext_m172481632(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m172481632_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.GameObject> UniRx.ObservableMonoBehaviour::OnParticleCollisionAsObservable()
extern Il2CppClass* Subject_1_t1655762426_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m591646648_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnParticleCollisionAsObservable_m1577085004_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnParticleCollisionAsObservable_m1577085004 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnParticleCollisionAsObservable_m1577085004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t1655762426 * V_0 = NULL;
	Subject_1_t1655762426 * G_B2_0 = NULL;
	Subject_1_t1655762426 * G_B1_0 = NULL;
	{
		Subject_1_t1655762426 * L_0 = __this->get_onParticleCollision_30();
		Subject_1_t1655762426 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t1655762426 * L_2 = (Subject_1_t1655762426 *)il2cpp_codegen_object_new(Subject_1_t1655762426_il2cpp_TypeInfo_var);
		Subject_1__ctor_m591646648(L_2, /*hidden argument*/Subject_1__ctor_m591646648_MethodInfo_var);
		Subject_1_t1655762426 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onParticleCollision_30(L_3);
		Subject_1_t1655762426 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnPostRender()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPostRender_m1333452609_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnPostRender_m1333452609 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPostRender_m1333452609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onPostRender_31();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onPostRender_31();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnPostRenderAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPostRenderAsObservable_m1140557422_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPostRenderAsObservable_m1140557422 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPostRenderAsObservable_m1140557422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onPostRender_31();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onPostRender_31(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnPreCull()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPreCull_m1802980684_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnPreCull_m1802980684 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPreCull_m1802980684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onPreCull_32();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onPreCull_32();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnPreCullAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPreCullAsObservable_m1910717577_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPreCullAsObservable_m1910717577 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPreCullAsObservable_m1910717577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onPreCull_32();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onPreCull_32(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnPreRender()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPreRender_m901570096_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnPreRender_m901570096 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPreRender_m901570096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onPreRender_33();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onPreRender_33();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnPreRenderAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPreRenderAsObservable_m1512064365_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPreRenderAsObservable_m1512064365 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPreRenderAsObservable_m1512064365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onPreRender_33();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onPreRender_33(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo* Tuple_Create_TisRenderTexture_t12905170_TisRenderTexture_t12905170_m1667340520_MethodInfo_var;
extern const MethodInfo* Subject_1_OnNext_m2285870880_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnRenderImage_m1046315270_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnRenderImage_m1046315270 (ObservableMonoBehaviour_t2173505449 * __this, RenderTexture_t12905170 * ___src0, RenderTexture_t12905170 * ___dest1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnRenderImage_m1046315270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3856149375 * L_0 = __this->get_onRenderImage_34();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Subject_1_t3856149375 * L_1 = __this->get_onRenderImage_34();
		RenderTexture_t12905170 * L_2 = ___src0;
		RenderTexture_t12905170 * L_3 = ___dest1;
		Tuple_2_t1918114755  L_4 = Tuple_Create_TisRenderTexture_t12905170_TisRenderTexture_t12905170_m1667340520(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/Tuple_Create_TisRenderTexture_t12905170_TisRenderTexture_t12905170_m1667340520_MethodInfo_var);
		NullCheck(L_1);
		Subject_1_OnNext_m2285870880(L_1, L_4, /*hidden argument*/Subject_1_OnNext_m2285870880_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>> UniRx.ObservableMonoBehaviour::OnRenderImageAsObservable()
extern Il2CppClass* Subject_1_t3856149375_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m499991160_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnRenderImageAsObservable_m3614982245_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnRenderImageAsObservable_m3614982245 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnRenderImageAsObservable_m3614982245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3856149375 * V_0 = NULL;
	Subject_1_t3856149375 * G_B2_0 = NULL;
	Subject_1_t3856149375 * G_B1_0 = NULL;
	{
		Subject_1_t3856149375 * L_0 = __this->get_onRenderImage_34();
		Subject_1_t3856149375 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3856149375 * L_2 = (Subject_1_t3856149375 *)il2cpp_codegen_object_new(Subject_1_t3856149375_il2cpp_TypeInfo_var);
		Subject_1__ctor_m499991160(L_2, /*hidden argument*/Subject_1__ctor_m499991160_MethodInfo_var);
		Subject_1_t3856149375 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onRenderImage_34(L_3);
		Subject_1_t3856149375 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnRenderObject()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnRenderObject_m216210208_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnRenderObject_m216210208 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnRenderObject_m216210208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onRenderObject_35();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onRenderObject_35();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnRenderObjectAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnRenderObjectAsObservable_m1021410765_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnRenderObjectAsObservable_m1021410765 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnRenderObjectAsObservable_m1021410765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onRenderObject_35();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onRenderObject_35(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnServerInitialized()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnServerInitialized_m2228721576_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnServerInitialized_m2228721576 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnServerInitialized_m2228721576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onServerInitialized_36();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onServerInitialized_36();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnServerInitializedAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnServerInitializedAsObservable_m3112977125_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnServerInitializedAsObservable_m3112977125 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnServerInitializedAsObservable_m3112977125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onServerInitialized_36();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onServerInitialized_36(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo* Subject_1_OnNext_m3189537411_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerEnter_m2622129696_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnTriggerEnter_m2622129696 (ObservableMonoBehaviour_t2173505449 * __this, Collider_t955670625 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerEnter_m2622129696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2893705245 * L_0 = __this->get_onTriggerEnter_37();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2893705245 * L_1 = __this->get_onTriggerEnter_37();
		Collider_t955670625 * L_2 = ___other0;
		NullCheck(L_1);
		Subject_1_OnNext_m3189537411(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m3189537411_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::OnTriggerEnterAsObservable()
extern Il2CppClass* Subject_1_t2893705245_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1323276955_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerEnterAsObservable_m3105090857_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerEnterAsObservable_m3105090857 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerEnterAsObservable_m3105090857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2893705245 * V_0 = NULL;
	Subject_1_t2893705245 * G_B2_0 = NULL;
	Subject_1_t2893705245 * G_B1_0 = NULL;
	{
		Subject_1_t2893705245 * L_0 = __this->get_onTriggerEnter_37();
		Subject_1_t2893705245 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2893705245 * L_2 = (Subject_1_t2893705245 *)il2cpp_codegen_object_new(Subject_1_t2893705245_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1323276955(L_2, /*hidden argument*/Subject_1__ctor_m1323276955_MethodInfo_var);
		Subject_1_t2893705245 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onTriggerEnter_37(L_3);
		Subject_1_t2893705245 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerEnter2D(UnityEngine.Collider2D)
extern const MethodInfo* Subject_1_OnNext_m620767317_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerEnter2D_m1897276960_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnTriggerEnter2D_m1897276960 (ObservableMonoBehaviour_t2173505449 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerEnter2D_m1897276960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3828072815 * L_0 = __this->get_onTriggerEnter2D_38();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t3828072815 * L_1 = __this->get_onTriggerEnter2D_38();
		Collider2D_t1890038195 * L_2 = ___other0;
		NullCheck(L_1);
		Subject_1_OnNext_m620767317(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m620767317_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::OnTriggerEnter2DAsObservable()
extern Il2CppClass* Subject_1_t3828072815_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m538481901_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerEnter2DAsObservable_m4281614569_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerEnter2DAsObservable_m4281614569 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerEnter2DAsObservable_m4281614569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3828072815 * V_0 = NULL;
	Subject_1_t3828072815 * G_B2_0 = NULL;
	Subject_1_t3828072815 * G_B1_0 = NULL;
	{
		Subject_1_t3828072815 * L_0 = __this->get_onTriggerEnter2D_38();
		Subject_1_t3828072815 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3828072815 * L_2 = (Subject_1_t3828072815 *)il2cpp_codegen_object_new(Subject_1_t3828072815_il2cpp_TypeInfo_var);
		Subject_1__ctor_m538481901(L_2, /*hidden argument*/Subject_1__ctor_m538481901_MethodInfo_var);
		Subject_1_t3828072815 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onTriggerEnter2D_38(L_3);
		Subject_1_t3828072815 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerExit(UnityEngine.Collider)
extern const MethodInfo* Subject_1_OnNext_m3189537411_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerExit_m427244418_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnTriggerExit_m427244418 (ObservableMonoBehaviour_t2173505449 * __this, Collider_t955670625 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerExit_m427244418_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2893705245 * L_0 = __this->get_onTriggerExit_39();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2893705245 * L_1 = __this->get_onTriggerExit_39();
		Collider_t955670625 * L_2 = ___other0;
		NullCheck(L_1);
		Subject_1_OnNext_m3189537411(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m3189537411_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::OnTriggerExitAsObservable()
extern Il2CppClass* Subject_1_t2893705245_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1323276955_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerExitAsObservable_m754770649_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerExitAsObservable_m754770649 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerExitAsObservable_m754770649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2893705245 * V_0 = NULL;
	Subject_1_t2893705245 * G_B2_0 = NULL;
	Subject_1_t2893705245 * G_B1_0 = NULL;
	{
		Subject_1_t2893705245 * L_0 = __this->get_onTriggerExit_39();
		Subject_1_t2893705245 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2893705245 * L_2 = (Subject_1_t2893705245 *)il2cpp_codegen_object_new(Subject_1_t2893705245_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1323276955(L_2, /*hidden argument*/Subject_1__ctor_m1323276955_MethodInfo_var);
		Subject_1_t2893705245 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onTriggerExit_39(L_3);
		Subject_1_t2893705245 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerExit2D(UnityEngine.Collider2D)
extern const MethodInfo* Subject_1_OnNext_m620767317_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerExit2D_m1950702210_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnTriggerExit2D_m1950702210 (ObservableMonoBehaviour_t2173505449 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerExit2D_m1950702210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3828072815 * L_0 = __this->get_onTriggerExit2D_40();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t3828072815 * L_1 = __this->get_onTriggerExit2D_40();
		Collider2D_t1890038195 * L_2 = ___other0;
		NullCheck(L_1);
		Subject_1_OnNext_m620767317(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m620767317_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::OnTriggerExit2DAsObservable()
extern Il2CppClass* Subject_1_t3828072815_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m538481901_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerExit2DAsObservable_m3583054205_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerExit2DAsObservable_m3583054205 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerExit2DAsObservable_m3583054205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3828072815 * V_0 = NULL;
	Subject_1_t3828072815 * G_B2_0 = NULL;
	Subject_1_t3828072815 * G_B1_0 = NULL;
	{
		Subject_1_t3828072815 * L_0 = __this->get_onTriggerExit2D_40();
		Subject_1_t3828072815 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3828072815 * L_2 = (Subject_1_t3828072815 *)il2cpp_codegen_object_new(Subject_1_t3828072815_il2cpp_TypeInfo_var);
		Subject_1__ctor_m538481901(L_2, /*hidden argument*/Subject_1__ctor_m538481901_MethodInfo_var);
		Subject_1_t3828072815 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onTriggerExit2D_40(L_3);
		Subject_1_t3828072815 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerStay(UnityEngine.Collider)
extern const MethodInfo* Subject_1_OnNext_m3189537411_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerStay_m3045333629_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnTriggerStay_m3045333629 (ObservableMonoBehaviour_t2173505449 * __this, Collider_t955670625 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerStay_m3045333629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2893705245 * L_0 = __this->get_onTriggerStay_41();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2893705245 * L_1 = __this->get_onTriggerStay_41();
		Collider_t955670625 * L_2 = ___other0;
		NullCheck(L_1);
		Subject_1_OnNext_m3189537411(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m3189537411_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::OnTriggerStayAsObservable()
extern Il2CppClass* Subject_1_t2893705245_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1323276955_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerStayAsObservable_m1594485972_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerStayAsObservable_m1594485972 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerStayAsObservable_m1594485972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2893705245 * V_0 = NULL;
	Subject_1_t2893705245 * G_B2_0 = NULL;
	Subject_1_t2893705245 * G_B1_0 = NULL;
	{
		Subject_1_t2893705245 * L_0 = __this->get_onTriggerStay_41();
		Subject_1_t2893705245 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2893705245 * L_2 = (Subject_1_t2893705245 *)il2cpp_codegen_object_new(Subject_1_t2893705245_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1323276955(L_2, /*hidden argument*/Subject_1__ctor_m1323276955_MethodInfo_var);
		Subject_1_t2893705245 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onTriggerStay_41(L_3);
		Subject_1_t2893705245 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerStay2D(UnityEngine.Collider2D)
extern const MethodInfo* Subject_1_OnNext_m620767317_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerStay2D_m1887716349_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnTriggerStay2D_m1887716349 (ObservableMonoBehaviour_t2173505449 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerStay2D_m1887716349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3828072815 * L_0 = __this->get_onTriggerStay2D_42();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t3828072815 * L_1 = __this->get_onTriggerStay2D_42();
		Collider2D_t1890038195 * L_2 = ___other0;
		NullCheck(L_1);
		Subject_1_OnNext_m620767317(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m620767317_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::OnTriggerStay2DAsObservable()
extern Il2CppClass* Subject_1_t3828072815_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m538481901_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnTriggerStay2DAsObservable_m3095627960_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerStay2DAsObservable_m3095627960 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnTriggerStay2DAsObservable_m3095627960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3828072815 * V_0 = NULL;
	Subject_1_t3828072815 * G_B2_0 = NULL;
	Subject_1_t3828072815 * G_B1_0 = NULL;
	{
		Subject_1_t3828072815 * L_0 = __this->get_onTriggerStay2D_42();
		Subject_1_t3828072815 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3828072815 * L_2 = (Subject_1_t3828072815 *)il2cpp_codegen_object_new(Subject_1_t3828072815_il2cpp_TypeInfo_var);
		Subject_1__ctor_m538481901(L_2, /*hidden argument*/Subject_1__ctor_m538481901_MethodInfo_var);
		Subject_1_t3828072815 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onTriggerStay2D_42(L_3);
		Subject_1_t3828072815 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnValidate()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnValidate_m2019282049_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnValidate_m2019282049 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnValidate_m2019282049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onValidate_43();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onValidate_43();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnValidateAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnValidateAsObservable_m2752151982_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnValidateAsObservable_m2752151982 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnValidateAsObservable_m2752151982_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onValidate_43();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onValidate_43(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnWillRenderObject()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnWillRenderObject_m526842514_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnWillRenderObject_m526842514 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnWillRenderObject_m526842514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_onWillRenderObject_44();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_onWillRenderObject_44();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnWillRenderObjectAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnWillRenderObjectAsObservable_m2565432895_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnWillRenderObjectAsObservable_m2565432895 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnWillRenderObjectAsObservable_m2565432895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_onWillRenderObject_44();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onWillRenderObject_44(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::Reset()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_Reset_m3472772869_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_Reset_m3472772869 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_Reset_m3472772869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_reset_45();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_reset_45();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::ResetAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_ResetAsObservable_m583701698_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_ResetAsObservable_m583701698 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_ResetAsObservable_m583701698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_reset_45();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_reset_45(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::Start()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const MethodInfo* Subject_1_OnCompleted_m4016750806_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_Start_m478510424_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_Start_m478510424 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_Start_m478510424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_calledStart_46((bool)1);
		Subject_1_t201353362 * L_0 = __this->get_start_47();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_start_47();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
		Subject_1_t201353362 * L_3 = __this->get_start_47();
		NullCheck(L_3);
		Subject_1_OnCompleted_m4016750806(L_3, /*hidden argument*/Subject_1_OnCompleted_m4016750806_MethodInfo_var);
	}

IL_002d:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::StartAsObservable()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_StartAsObservable_m1031265941_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_StartAsObservable_m1031265941 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_StartAsObservable_m1031265941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B4_0 = NULL;
	Subject_1_t201353362 * G_B3_0 = NULL;
	{
		bool L_0 = __this->get_calledStart_46();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_1 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = Observable_Return_TisUnit_t2558286038_m1480352924(NULL /*static, unused*/, L_1, /*hidden argument*/Observable_Return_TisUnit_t2558286038_m1480352924_MethodInfo_var);
		return L_2;
	}

IL_0016:
	{
		Subject_1_t201353362 * L_3 = __this->get_start_47();
		Subject_1_t201353362 * L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_0031;
		}
	}
	{
		Subject_1_t201353362 * L_5 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_5, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_6 = L_5;
		V_0 = L_6;
		__this->set_start_47(L_6);
		Subject_1_t201353362 * L_7 = V_0;
		G_B4_0 = L_7;
	}

IL_0031:
	{
		return G_B4_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::Update()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m810233332_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_Update_m1954773429_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_Update_m1954773429 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_Update_m1954773429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t201353362 * L_0 = __this->get_update_48();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_1 = __this->get_update_48();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Subject_1_OnNext_m810233332(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m810233332_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::UpdateAsObservable()
extern Il2CppClass* Subject_1_t201353362_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m55998028_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_UpdateAsObservable_m550395106_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_UpdateAsObservable_m550395106 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_UpdateAsObservable_m550395106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t201353362 * V_0 = NULL;
	Subject_1_t201353362 * G_B2_0 = NULL;
	Subject_1_t201353362 * G_B1_0 = NULL;
	{
		Subject_1_t201353362 * L_0 = __this->get_update_48();
		Subject_1_t201353362 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)il2cpp_codegen_object_new(Subject_1_t201353362_il2cpp_TypeInfo_var);
		Subject_1__ctor_m55998028(L_2, /*hidden argument*/Subject_1__ctor_m55998028_MethodInfo_var);
		Subject_1_t201353362 * L_3 = L_2;
		V_0 = L_3;
		__this->set_update_48(L_3);
		Subject_1_t201353362 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnDisconnectedFromServer(UnityEngine.NetworkDisconnection)
extern const MethodInfo* Subject_1_OnNext_m4212746989_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDisconnectedFromServer_m1195767326_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnDisconnectedFromServer_m1195767326 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDisconnectedFromServer_m1195767326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2276795015 * L_0 = __this->get_onDisconnectedFromServer_49();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2276795015 * L_1 = __this->get_onDisconnectedFromServer_49();
		int32_t L_2 = ___info0;
		NullCheck(L_1);
		Subject_1_OnNext_m4212746989(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m4212746989_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.NetworkDisconnection> UniRx.ObservableMonoBehaviour::OnDisconnectedFromServerAsObservable()
extern Il2CppClass* Subject_1_t2276795015_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m542219653_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnDisconnectedFromServerAsObservable_m2701963623_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDisconnectedFromServerAsObservable_m2701963623 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnDisconnectedFromServerAsObservable_m2701963623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2276795015 * V_0 = NULL;
	Subject_1_t2276795015 * G_B2_0 = NULL;
	Subject_1_t2276795015 * G_B1_0 = NULL;
	{
		Subject_1_t2276795015 * L_0 = __this->get_onDisconnectedFromServer_49();
		Subject_1_t2276795015 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2276795015 * L_2 = (Subject_1_t2276795015 *)il2cpp_codegen_object_new(Subject_1_t2276795015_il2cpp_TypeInfo_var);
		Subject_1__ctor_m542219653(L_2, /*hidden argument*/Subject_1__ctor_m542219653_MethodInfo_var);
		Subject_1_t2276795015 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onDisconnectedFromServer_49(L_3);
		Subject_1_t2276795015 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnFailedToConnect(UnityEngine.NetworkConnectionError)
extern const MethodInfo* Subject_1_OnNext_m3456945355_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnFailedToConnect_m3518302774_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnFailedToConnect_m3518302774 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnFailedToConnect_m3518302774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2961840613 * L_0 = __this->get_onFailedToConnect_50();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2961840613 * L_1 = __this->get_onFailedToConnect_50();
		int32_t L_2 = ___error0;
		NullCheck(L_1);
		Subject_1_OnNext_m3456945355(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m3456945355_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.NetworkConnectionError> UniRx.ObservableMonoBehaviour::OnFailedToConnectAsObservable()
extern Il2CppClass* Subject_1_t2961840613_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1506795235_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnFailedToConnectAsObservable_m1266481053_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnFailedToConnectAsObservable_m1266481053 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnFailedToConnectAsObservable_m1266481053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2961840613 * V_0 = NULL;
	Subject_1_t2961840613 * G_B2_0 = NULL;
	Subject_1_t2961840613 * G_B1_0 = NULL;
	{
		Subject_1_t2961840613 * L_0 = __this->get_onFailedToConnect_50();
		Subject_1_t2961840613 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2961840613 * L_2 = (Subject_1_t2961840613 *)il2cpp_codegen_object_new(Subject_1_t2961840613_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1506795235(L_2, /*hidden argument*/Subject_1__ctor_m1506795235_MethodInfo_var);
		Subject_1_t2961840613 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onFailedToConnect_50(L_3);
		Subject_1_t2961840613 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnFailedToConnectToMasterServer(UnityEngine.NetworkConnectionError)
extern const MethodInfo* Subject_1_OnNext_m3456945355_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnFailedToConnectToMasterServer_m160169654_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnFailedToConnectToMasterServer_m160169654 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnFailedToConnectToMasterServer_m160169654_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2961840613 * L_0 = __this->get_onFailedToConnectToMasterServer_51();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t2961840613 * L_1 = __this->get_onFailedToConnectToMasterServer_51();
		int32_t L_2 = ___info0;
		NullCheck(L_1);
		Subject_1_OnNext_m3456945355(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m3456945355_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.NetworkConnectionError> UniRx.ObservableMonoBehaviour::OnFailedToConnectToMasterServerAsObservable()
extern Il2CppClass* Subject_1_t2961840613_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1506795235_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnFailedToConnectToMasterServerAsObservable_m2464000477_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnFailedToConnectToMasterServerAsObservable_m2464000477 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnFailedToConnectToMasterServerAsObservable_m2464000477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2961840613 * V_0 = NULL;
	Subject_1_t2961840613 * G_B2_0 = NULL;
	Subject_1_t2961840613 * G_B1_0 = NULL;
	{
		Subject_1_t2961840613 * L_0 = __this->get_onFailedToConnectToMasterServer_51();
		Subject_1_t2961840613 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t2961840613 * L_2 = (Subject_1_t2961840613 *)il2cpp_codegen_object_new(Subject_1_t2961840613_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1506795235(L_2, /*hidden argument*/Subject_1__ctor_m1506795235_MethodInfo_var);
		Subject_1_t2961840613 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onFailedToConnectToMasterServer_51(L_3);
		Subject_1_t2961840613 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnMasterServerEvent(UnityEngine.MasterServerEvent)
extern const MethodInfo* Subject_1_OnNext_m3711452768_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnMasterServerEvent_m2416233864_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnMasterServerEvent_m2416233864 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___msEvent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnMasterServerEvent_m2416233864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t4215842110 * L_0 = __this->get_onMasterServerEvent_52();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t4215842110 * L_1 = __this->get_onMasterServerEvent_52();
		int32_t L_2 = ___msEvent0;
		NullCheck(L_1);
		Subject_1_OnNext_m3711452768(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m3711452768_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.MasterServerEvent> UniRx.ObservableMonoBehaviour::OnMasterServerEventAsObservable()
extern Il2CppClass* Subject_1_t4215842110_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m3947280824_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnMasterServerEventAsObservable_m3495966991_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnMasterServerEventAsObservable_m3495966991 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnMasterServerEventAsObservable_m3495966991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t4215842110 * V_0 = NULL;
	Subject_1_t4215842110 * G_B2_0 = NULL;
	Subject_1_t4215842110 * G_B1_0 = NULL;
	{
		Subject_1_t4215842110 * L_0 = __this->get_onMasterServerEvent_52();
		Subject_1_t4215842110 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t4215842110 * L_2 = (Subject_1_t4215842110 *)il2cpp_codegen_object_new(Subject_1_t4215842110_il2cpp_TypeInfo_var);
		Subject_1__ctor_m3947280824(L_2, /*hidden argument*/Subject_1__ctor_m3947280824_MethodInfo_var);
		Subject_1_t4215842110 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onMasterServerEvent_52(L_3);
		Subject_1_t4215842110 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnNetworkInstantiate(UnityEngine.NetworkMessageInfo)
extern const MethodInfo* Subject_1_OnNext_m1868807638_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnNetworkInstantiate_m140847305_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnNetworkInstantiate_m140847305 (ObservableMonoBehaviour_t2173505449 * __this, NetworkMessageInfo_t2574344884  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnNetworkInstantiate_m140847305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t217412208 * L_0 = __this->get_onNetworkInstantiate_53();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t217412208 * L_1 = __this->get_onNetworkInstantiate_53();
		NetworkMessageInfo_t2574344884  L_2 = ___info0;
		NullCheck(L_1);
		Subject_1_OnNext_m1868807638(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m1868807638_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.NetworkMessageInfo> UniRx.ObservableMonoBehaviour::OnNetworkInstantiateAsObservable()
extern Il2CppClass* Subject_1_t217412208_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1572181678_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnNetworkInstantiateAsObservable_m3722898642_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnNetworkInstantiateAsObservable_m3722898642 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnNetworkInstantiateAsObservable_m3722898642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t217412208 * V_0 = NULL;
	Subject_1_t217412208 * G_B2_0 = NULL;
	Subject_1_t217412208 * G_B1_0 = NULL;
	{
		Subject_1_t217412208 * L_0 = __this->get_onNetworkInstantiate_53();
		Subject_1_t217412208 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t217412208 * L_2 = (Subject_1_t217412208 *)il2cpp_codegen_object_new(Subject_1_t217412208_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1572181678(L_2, /*hidden argument*/Subject_1__ctor_m1572181678_MethodInfo_var);
		Subject_1_t217412208 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onNetworkInstantiate_53(L_3);
		Subject_1_t217412208 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnPlayerConnected(UnityEngine.NetworkPlayer)
extern const MethodInfo* Subject_1_OnNext_m4237255610_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPlayerConnected_m748806203_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnPlayerConnected_m748806203 (ObservableMonoBehaviour_t2173505449 * __this, NetworkPlayer_t1281137372  ___player0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPlayerConnected_m748806203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3219171992 * L_0 = __this->get_onPlayerConnected_54();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t3219171992 * L_1 = __this->get_onPlayerConnected_54();
		NetworkPlayer_t1281137372  L_2 = ___player0;
		NullCheck(L_1);
		Subject_1_OnNext_m4237255610(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m4237255610_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.NetworkPlayer> UniRx.ObservableMonoBehaviour::OnPlayerConnectedAsObservable()
extern Il2CppClass* Subject_1_t3219171992_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m345597330_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPlayerConnectedAsObservable_m4277027708_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPlayerConnectedAsObservable_m4277027708 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPlayerConnectedAsObservable_m4277027708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3219171992 * V_0 = NULL;
	Subject_1_t3219171992 * G_B2_0 = NULL;
	Subject_1_t3219171992 * G_B1_0 = NULL;
	{
		Subject_1_t3219171992 * L_0 = __this->get_onPlayerConnected_54();
		Subject_1_t3219171992 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3219171992 * L_2 = (Subject_1_t3219171992 *)il2cpp_codegen_object_new(Subject_1_t3219171992_il2cpp_TypeInfo_var);
		Subject_1__ctor_m345597330(L_2, /*hidden argument*/Subject_1__ctor_m345597330_MethodInfo_var);
		Subject_1_t3219171992 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onPlayerConnected_54(L_3);
		Subject_1_t3219171992 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnPlayerDisconnected(UnityEngine.NetworkPlayer)
extern const MethodInfo* Subject_1_OnNext_m4237255610_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPlayerDisconnected_m3687566611_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnPlayerDisconnected_m3687566611 (ObservableMonoBehaviour_t2173505449 * __this, NetworkPlayer_t1281137372  ___player0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPlayerDisconnected_m3687566611_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3219171992 * L_0 = __this->get_onPlayerDisconnected_55();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Subject_1_t3219171992 * L_1 = __this->get_onPlayerDisconnected_55();
		NetworkPlayer_t1281137372  L_2 = ___player0;
		NullCheck(L_1);
		Subject_1_OnNext_m4237255610(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m4237255610_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.NetworkPlayer> UniRx.ObservableMonoBehaviour::OnPlayerDisconnectedAsObservable()
extern Il2CppClass* Subject_1_t3219171992_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m345597330_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnPlayerDisconnectedAsObservable_m3565600660_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPlayerDisconnectedAsObservable_m3565600660 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnPlayerDisconnectedAsObservable_m3565600660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3219171992 * V_0 = NULL;
	Subject_1_t3219171992 * G_B2_0 = NULL;
	Subject_1_t3219171992 * G_B1_0 = NULL;
	{
		Subject_1_t3219171992 * L_0 = __this->get_onPlayerDisconnected_55();
		Subject_1_t3219171992 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t3219171992 * L_2 = (Subject_1_t3219171992 *)il2cpp_codegen_object_new(Subject_1_t3219171992_il2cpp_TypeInfo_var);
		Subject_1__ctor_m345597330(L_2, /*hidden argument*/Subject_1__ctor_m345597330_MethodInfo_var);
		Subject_1_t3219171992 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onPlayerDisconnected_55(L_3);
		Subject_1_t3219171992 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void UniRx.ObservableMonoBehaviour::OnSerializeNetworkView(UnityEngine.BitStream,UnityEngine.NetworkMessageInfo)
extern const MethodInfo* Tuple_Create_TisBitStream_t3159523098_TisNetworkMessageInfo_t2574344884_m886288216_MethodInfo_var;
extern const MethodInfo* Subject_1_OnNext_m4197563218_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnSerializeNetworkView_m749135242_MetadataUsageId;
extern "C"  void ObservableMonoBehaviour_OnSerializeNetworkView_m749135242 (ObservableMonoBehaviour_t2173505449 * __this, BitStream_t3159523098 * ___stream0, NetworkMessageInfo_t2574344884  ___info1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnSerializeNetworkView_m749135242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t919080185 * L_0 = __this->get_onSerializeNetworkView_56();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Subject_1_t919080185 * L_1 = __this->get_onSerializeNetworkView_56();
		BitStream_t3159523098 * L_2 = ___stream0;
		NetworkMessageInfo_t2574344884  L_3 = ___info1;
		Tuple_2_t3276012861  L_4 = Tuple_Create_TisBitStream_t3159523098_TisNetworkMessageInfo_t2574344884_m886288216(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/Tuple_Create_TisBitStream_t3159523098_TisNetworkMessageInfo_t2574344884_m886288216_MethodInfo_var);
		NullCheck(L_1);
		Subject_1_OnNext_m4197563218(L_1, L_4, /*hidden argument*/Subject_1_OnNext_m4197563218_MethodInfo_var);
	}

IL_001d:
	{
		return;
	}
}
// UniRx.IObservable`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>> UniRx.ObservableMonoBehaviour::OnSerializeNetworkViewAsObservable()
extern Il2CppClass* Subject_1_t919080185_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m1167901482_MethodInfo_var;
extern const uint32_t ObservableMonoBehaviour_OnSerializeNetworkViewAsObservable_m3383906739_MetadataUsageId;
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnSerializeNetworkViewAsObservable_m3383906739 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableMonoBehaviour_OnSerializeNetworkViewAsObservable_m3383906739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t919080185 * V_0 = NULL;
	Subject_1_t919080185 * G_B2_0 = NULL;
	Subject_1_t919080185 * G_B1_0 = NULL;
	{
		Subject_1_t919080185 * L_0 = __this->get_onSerializeNetworkView_56();
		Subject_1_t919080185 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Subject_1_t919080185 * L_2 = (Subject_1_t919080185 *)il2cpp_codegen_object_new(Subject_1_t919080185_il2cpp_TypeInfo_var);
		Subject_1__ctor_m1167901482(L_2, /*hidden argument*/Subject_1__ctor_m1167901482_MethodInfo_var);
		Subject_1_t919080185 * L_3 = L_2;
		V_0 = L_3;
		__this->set_onSerializeNetworkView_56(L_3);
		Subject_1_t919080185 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Get(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CGetU3Ec__AnonStorey7E_t497370161_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t212750549_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetU3Ec__AnonStorey7E_U3CU3Em__AA_m1036982784_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1557367802_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var;
extern const uint32_t ObservableWWW_Get_m2633702238_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_Get_m2633702238 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Dictionary_2_t2606186806 * ___headers1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_Get_m2633702238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetU3Ec__AnonStorey7E_t497370161 * V_0 = NULL;
	{
		U3CGetU3Ec__AnonStorey7E_t497370161 * L_0 = (U3CGetU3Ec__AnonStorey7E_t497370161 *)il2cpp_codegen_object_new(U3CGetU3Ec__AnonStorey7E_t497370161_il2cpp_TypeInfo_var);
		U3CGetU3Ec__AnonStorey7E__ctor_m3918657003(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetU3Ec__AnonStorey7E_t497370161 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CGetU3Ec__AnonStorey7E_t497370161 * L_3 = V_0;
		Dictionary_2_t2606186806 * L_4 = ___headers1;
		NullCheck(L_3);
		L_3->set_headers_1(L_4);
		U3CGetU3Ec__AnonStorey7E_t497370161 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CGetU3Ec__AnonStorey7E_t497370161 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CGetU3Ec__AnonStorey7E_U3CU3Em__AA_m1036982784_MethodInfo_var);
		Func_3_t212750549 * L_9 = (Func_3_t212750549 *)il2cpp_codegen_object_new(Func_3_t212750549_il2cpp_TypeInfo_var);
		Func_3__ctor_m1557367802(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1557367802_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisString_t_m3641581413(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::GetAndGetBytes(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t2061407023_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetAndGetBytesU3Ec__AnonStorey7F_U3CU3Em__AB_m1487153967_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4178629697_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var;
extern const uint32_t ObservableWWW_GetAndGetBytes_m2190868423_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_GetAndGetBytes_m2190868423 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Dictionary_2_t2606186806 * ___headers1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_GetAndGetBytes_m2190868423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * V_0 = NULL;
	{
		U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * L_0 = (U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 *)il2cpp_codegen_object_new(U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258_il2cpp_TypeInfo_var);
		U3CGetAndGetBytesU3Ec__AnonStorey7F__ctor_m4018533664(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * L_3 = V_0;
		Dictionary_2_t2606186806 * L_4 = ___headers1;
		NullCheck(L_3);
		L_3->set_headers_1(L_4);
		U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CGetAndGetBytesU3Ec__AnonStorey7F_U3CU3Em__AB_m1487153967_MethodInfo_var);
		Func_3_t2061407023 * L_9 = (Func_3_t2061407023 *)il2cpp_codegen_object_new(Func_3_t2061407023_il2cpp_TypeInfo_var);
		Func_3__ctor_m4178629697(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m4178629697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::GetWWW(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CGetWWWU3Ec__AnonStorey80_t1804159614_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t1491243043_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetWWWU3Ec__AnonStorey80_U3CU3Em__AC_m4003425075_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1056795178_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var;
extern const uint32_t ObservableWWW_GetWWW_m3629130099_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_GetWWW_m3629130099 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Dictionary_2_t2606186806 * ___headers1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_GetWWW_m3629130099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetWWWU3Ec__AnonStorey80_t1804159614 * V_0 = NULL;
	{
		U3CGetWWWU3Ec__AnonStorey80_t1804159614 * L_0 = (U3CGetWWWU3Ec__AnonStorey80_t1804159614 *)il2cpp_codegen_object_new(U3CGetWWWU3Ec__AnonStorey80_t1804159614_il2cpp_TypeInfo_var);
		U3CGetWWWU3Ec__AnonStorey80__ctor_m1478189740(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetWWWU3Ec__AnonStorey80_t1804159614 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CGetWWWU3Ec__AnonStorey80_t1804159614 * L_3 = V_0;
		Dictionary_2_t2606186806 * L_4 = ___headers1;
		NullCheck(L_3);
		L_3->set_headers_1(L_4);
		U3CGetWWWU3Ec__AnonStorey80_t1804159614 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CGetWWWU3Ec__AnonStorey80_t1804159614 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CGetWWWU3Ec__AnonStorey80_U3CU3Em__AC_m4003425075_MethodInfo_var);
		Func_3_t1491243043 * L_9 = (Func_3_t1491243043 *)il2cpp_codegen_object_new(Func_3_t1491243043_il2cpp_TypeInfo_var);
		Func_3__ctor_m1056795178(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1056795178_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisWWW_t1522972100_m2159439317(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Post(System.String,System.Byte[],UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostU3Ec__AnonStorey81_t213725630_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t212750549_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostU3Ec__AnonStorey81_U3CU3Em__AD_m2809010340_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1557367802_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var;
extern const uint32_t ObservableWWW_Post_m989780006_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_Post_m989780006 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_Post_m989780006_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostU3Ec__AnonStorey81_t213725630 * V_0 = NULL;
	{
		U3CPostU3Ec__AnonStorey81_t213725630 * L_0 = (U3CPostU3Ec__AnonStorey81_t213725630 *)il2cpp_codegen_object_new(U3CPostU3Ec__AnonStorey81_t213725630_il2cpp_TypeInfo_var);
		U3CPostU3Ec__AnonStorey81__ctor_m3231068460(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostU3Ec__AnonStorey81_t213725630 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostU3Ec__AnonStorey81_t213725630 * L_3 = V_0;
		ByteU5BU5D_t58506160* L_4 = ___postData1;
		NullCheck(L_3);
		L_3->set_postData_1(L_4);
		U3CPostU3Ec__AnonStorey81_t213725630 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CPostU3Ec__AnonStorey81_t213725630 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CPostU3Ec__AnonStorey81_U3CU3Em__AD_m2809010340_MethodInfo_var);
		Func_3_t212750549 * L_9 = (Func_3_t212750549 *)il2cpp_codegen_object_new(Func_3_t212750549_il2cpp_TypeInfo_var);
		Func_3__ctor_m1557367802(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1557367802_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisString_t_m3641581413(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Post(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostU3Ec__AnonStorey82_t213725631_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t212750549_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostU3Ec__AnonStorey82_U3CU3Em__AE_m1418846660_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1557367802_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var;
extern const uint32_t ObservableWWW_Post_m1174276551_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_Post_m1174276551 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_Post_m1174276551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostU3Ec__AnonStorey82_t213725631 * V_0 = NULL;
	{
		U3CPostU3Ec__AnonStorey82_t213725631 * L_0 = (U3CPostU3Ec__AnonStorey82_t213725631 *)il2cpp_codegen_object_new(U3CPostU3Ec__AnonStorey82_t213725631_il2cpp_TypeInfo_var);
		U3CPostU3Ec__AnonStorey82__ctor_m3034554955(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostU3Ec__AnonStorey82_t213725631 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostU3Ec__AnonStorey82_t213725631 * L_3 = V_0;
		ByteU5BU5D_t58506160* L_4 = ___postData1;
		NullCheck(L_3);
		L_3->set_postData_1(L_4);
		U3CPostU3Ec__AnonStorey82_t213725631 * L_5 = V_0;
		Dictionary_2_t2606186806 * L_6 = ___headers2;
		NullCheck(L_5);
		L_5->set_headers_2(L_6);
		U3CPostU3Ec__AnonStorey82_t213725631 * L_7 = V_0;
		Il2CppObject* L_8 = ___progress3;
		NullCheck(L_7);
		L_7->set_progress_3(L_8);
		U3CPostU3Ec__AnonStorey82_t213725631 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CPostU3Ec__AnonStorey82_U3CU3Em__AE_m1418846660_MethodInfo_var);
		Func_3_t212750549 * L_11 = (Func_3_t212750549 *)il2cpp_codegen_object_new(Func_3_t212750549_il2cpp_TypeInfo_var);
		Func_3__ctor_m1557367802(L_11, L_9, L_10, /*hidden argument*/Func_3__ctor_m1557367802_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = Observable_FromCoroutine_TisString_t_m3641581413(NULL /*static, unused*/, L_11, /*hidden argument*/Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var);
		return L_12;
	}
}
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Post(System.String,UnityEngine.WWWForm,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostU3Ec__AnonStorey83_t213725632_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t212750549_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostU3Ec__AnonStorey83_U3CU3Em__AF_m28682980_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1557367802_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var;
extern const uint32_t ObservableWWW_Post_m265292097_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_Post_m265292097 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_Post_m265292097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostU3Ec__AnonStorey83_t213725632 * V_0 = NULL;
	{
		U3CPostU3Ec__AnonStorey83_t213725632 * L_0 = (U3CPostU3Ec__AnonStorey83_t213725632 *)il2cpp_codegen_object_new(U3CPostU3Ec__AnonStorey83_t213725632_il2cpp_TypeInfo_var);
		U3CPostU3Ec__AnonStorey83__ctor_m2838041450(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostU3Ec__AnonStorey83_t213725632 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostU3Ec__AnonStorey83_t213725632 * L_3 = V_0;
		WWWForm_t3999572776 * L_4 = ___content1;
		NullCheck(L_3);
		L_3->set_content_1(L_4);
		U3CPostU3Ec__AnonStorey83_t213725632 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CPostU3Ec__AnonStorey83_t213725632 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CPostU3Ec__AnonStorey83_U3CU3Em__AF_m28682980_MethodInfo_var);
		Func_3_t212750549 * L_9 = (Func_3_t212750549 *)il2cpp_codegen_object_new(Func_3_t212750549_il2cpp_TypeInfo_var);
		Func_3__ctor_m1557367802(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1557367802_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisString_t_m3641581413(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<System.String> UniRx.ObservableWWW::Post(System.String,UnityEngine.WWWForm,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostU3Ec__AnonStorey84_t213725633_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t212750549_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostU3Ec__AnonStorey84_U3CU3Em__B0_m881318412_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1557367802_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var;
extern const uint32_t ObservableWWW_Post_m3321211276_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_Post_m3321211276 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_Post_m3321211276_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostU3Ec__AnonStorey84_t213725633 * V_0 = NULL;
	{
		U3CPostU3Ec__AnonStorey84_t213725633 * L_0 = (U3CPostU3Ec__AnonStorey84_t213725633 *)il2cpp_codegen_object_new(U3CPostU3Ec__AnonStorey84_t213725633_il2cpp_TypeInfo_var);
		U3CPostU3Ec__AnonStorey84__ctor_m2641527945(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostU3Ec__AnonStorey84_t213725633 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostU3Ec__AnonStorey84_t213725633 * L_3 = V_0;
		WWWForm_t3999572776 * L_4 = ___content1;
		NullCheck(L_3);
		L_3->set_content_1(L_4);
		U3CPostU3Ec__AnonStorey84_t213725633 * L_5 = V_0;
		Dictionary_2_t2606186806 * L_6 = ___headers2;
		NullCheck(L_5);
		L_5->set_headers_3(L_6);
		U3CPostU3Ec__AnonStorey84_t213725633 * L_7 = V_0;
		Il2CppObject* L_8 = ___progress3;
		NullCheck(L_7);
		L_7->set_progress_4(L_8);
		U3CPostU3Ec__AnonStorey84_t213725633 * L_9 = V_0;
		U3CPostU3Ec__AnonStorey84_t213725633 * L_10 = V_0;
		NullCheck(L_10);
		WWWForm_t3999572776 * L_11 = L_10->get_content_1();
		NullCheck(L_11);
		Dictionary_2_t2606186806 * L_12 = WWWForm_get_headers_m370408569(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_contentHeaders_2(L_12);
		U3CPostU3Ec__AnonStorey84_t213725633 * L_13 = V_0;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)U3CPostU3Ec__AnonStorey84_U3CU3Em__B0_m881318412_MethodInfo_var);
		Func_3_t212750549 * L_15 = (Func_3_t212750549 *)il2cpp_codegen_object_new(Func_3_t212750549_il2cpp_TypeInfo_var);
		Func_3__ctor_m1557367802(L_15, L_13, L_14, /*hidden argument*/Func_3__ctor_m1557367802_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_16 = Observable_FromCoroutine_TisString_t_m3641581413(NULL /*static, unused*/, L_15, /*hidden argument*/Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var);
		return L_16;
	}
}
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::PostAndGetBytes(System.String,System.Byte[],UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t2061407023_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostAndGetBytesU3Ec__AnonStorey85_U3CU3Em__B1_m247104291_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4178629697_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var;
extern const uint32_t ObservableWWW_PostAndGetBytes_m3304440251_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_PostAndGetBytes_m3304440251 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_PostAndGetBytes_m3304440251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * V_0 = NULL;
	{
		U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * L_0 = (U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 *)il2cpp_codegen_object_new(U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790_il2cpp_TypeInfo_var);
		U3CPostAndGetBytesU3Ec__AnonStorey85__ctor_m78346854(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * L_3 = V_0;
		ByteU5BU5D_t58506160* L_4 = ___postData1;
		NullCheck(L_3);
		L_3->set_postData_1(L_4);
		U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CPostAndGetBytesU3Ec__AnonStorey85_U3CU3Em__B1_m247104291_MethodInfo_var);
		Func_3_t2061407023 * L_9 = (Func_3_t2061407023 *)il2cpp_codegen_object_new(Func_3_t2061407023_il2cpp_TypeInfo_var);
		Func_3__ctor_m4178629697(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m4178629697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::PostAndGetBytes(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t2061407023_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostAndGetBytesU3Ec__AnonStorey86_U3CU3Em__B2_m3151907907_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4178629697_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var;
extern const uint32_t ObservableWWW_PostAndGetBytes_m2287009106_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_PostAndGetBytes_m2287009106 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_PostAndGetBytes_m2287009106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * V_0 = NULL;
	{
		U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * L_0 = (U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 *)il2cpp_codegen_object_new(U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791_il2cpp_TypeInfo_var);
		U3CPostAndGetBytesU3Ec__AnonStorey86__ctor_m4176800645(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * L_3 = V_0;
		ByteU5BU5D_t58506160* L_4 = ___postData1;
		NullCheck(L_3);
		L_3->set_postData_1(L_4);
		U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * L_5 = V_0;
		Dictionary_2_t2606186806 * L_6 = ___headers2;
		NullCheck(L_5);
		L_5->set_headers_2(L_6);
		U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * L_7 = V_0;
		Il2CppObject* L_8 = ___progress3;
		NullCheck(L_7);
		L_7->set_progress_3(L_8);
		U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CPostAndGetBytesU3Ec__AnonStorey86_U3CU3Em__B2_m3151907907_MethodInfo_var);
		Func_3_t2061407023 * L_11 = (Func_3_t2061407023 *)il2cpp_codegen_object_new(Func_3_t2061407023_il2cpp_TypeInfo_var);
		Func_3__ctor_m4178629697(L_11, L_9, L_10, /*hidden argument*/Func_3__ctor_m4178629697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756(NULL /*static, unused*/, L_11, /*hidden argument*/Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var);
		return L_12;
	}
}
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::PostAndGetBytes(System.String,UnityEngine.WWWForm,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t2061407023_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostAndGetBytesU3Ec__AnonStorey87_U3CU3Em__B3_m1761744227_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4178629697_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var;
extern const uint32_t ObservableWWW_PostAndGetBytes_m523546390_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_PostAndGetBytes_m523546390 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_PostAndGetBytes_m523546390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * V_0 = NULL;
	{
		U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * L_0 = (U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 *)il2cpp_codegen_object_new(U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792_il2cpp_TypeInfo_var);
		U3CPostAndGetBytesU3Ec__AnonStorey87__ctor_m3980287140(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * L_3 = V_0;
		WWWForm_t3999572776 * L_4 = ___content1;
		NullCheck(L_3);
		L_3->set_content_1(L_4);
		U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CPostAndGetBytesU3Ec__AnonStorey87_U3CU3Em__B3_m1761744227_MethodInfo_var);
		Func_3_t2061407023 * L_9 = (Func_3_t2061407023 *)il2cpp_codegen_object_new(Func_3_t2061407023_il2cpp_TypeInfo_var);
		Func_3__ctor_m4178629697(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m4178629697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<System.Byte[]> UniRx.ObservableWWW::PostAndGetBytes(System.String,UnityEngine.WWWForm,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t2061407023_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostAndGetBytesU3Ec__AnonStorey88_U3CU3Em__B4_m371580547_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4178629697_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var;
extern const uint32_t ObservableWWW_PostAndGetBytes_m3322448087_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_PostAndGetBytes_m3322448087 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_PostAndGetBytes_m3322448087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * V_0 = NULL;
	{
		U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * L_0 = (U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 *)il2cpp_codegen_object_new(U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793_il2cpp_TypeInfo_var);
		U3CPostAndGetBytesU3Ec__AnonStorey88__ctor_m3783773635(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * L_3 = V_0;
		WWWForm_t3999572776 * L_4 = ___content1;
		NullCheck(L_3);
		L_3->set_content_1(L_4);
		U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * L_5 = V_0;
		Dictionary_2_t2606186806 * L_6 = ___headers2;
		NullCheck(L_5);
		L_5->set_headers_3(L_6);
		U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * L_7 = V_0;
		Il2CppObject* L_8 = ___progress3;
		NullCheck(L_7);
		L_7->set_progress_4(L_8);
		U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * L_9 = V_0;
		U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * L_10 = V_0;
		NullCheck(L_10);
		WWWForm_t3999572776 * L_11 = L_10->get_content_1();
		NullCheck(L_11);
		Dictionary_2_t2606186806 * L_12 = WWWForm_get_headers_m370408569(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_contentHeaders_2(L_12);
		U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * L_13 = V_0;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)U3CPostAndGetBytesU3Ec__AnonStorey88_U3CU3Em__B4_m371580547_MethodInfo_var);
		Func_3_t2061407023 * L_15 = (Func_3_t2061407023 *)il2cpp_codegen_object_new(Func_3_t2061407023_il2cpp_TypeInfo_var);
		Func_3__ctor_m4178629697(L_15, L_13, L_14, /*hidden argument*/Func_3__ctor_m4178629697_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_16 = Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756(NULL /*static, unused*/, L_15, /*hidden argument*/Observable_FromCoroutine_TisByteU5BU5D_t58506160_m1528459756_MethodInfo_var);
		return L_16;
	}
}
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::PostWWW(System.String,System.Byte[],UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostWWWU3Ec__AnonStorey89_t4245247429_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t1491243043_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostWWWU3Ec__AnonStorey89_U3CU3Em__B5_m3080128655_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1056795178_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var;
extern const uint32_t ObservableWWW_PostWWW_m3467875933_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_PostWWW_m3467875933 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_PostWWW_m3467875933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostWWWU3Ec__AnonStorey89_t4245247429 * V_0 = NULL;
	{
		U3CPostWWWU3Ec__AnonStorey89_t4245247429 * L_0 = (U3CPostWWWU3Ec__AnonStorey89_t4245247429 *)il2cpp_codegen_object_new(U3CPostWWWU3Ec__AnonStorey89_t4245247429_il2cpp_TypeInfo_var);
		U3CPostWWWU3Ec__AnonStorey89__ctor_m1685751383(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostWWWU3Ec__AnonStorey89_t4245247429 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostWWWU3Ec__AnonStorey89_t4245247429 * L_3 = V_0;
		ByteU5BU5D_t58506160* L_4 = ___postData1;
		NullCheck(L_3);
		L_3->set_postData_1(L_4);
		U3CPostWWWU3Ec__AnonStorey89_t4245247429 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CPostWWWU3Ec__AnonStorey89_t4245247429 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CPostWWWU3Ec__AnonStorey89_U3CU3Em__B5_m3080128655_MethodInfo_var);
		Func_3_t1491243043 * L_9 = (Func_3_t1491243043 *)il2cpp_codegen_object_new(Func_3_t1491243043_il2cpp_TypeInfo_var);
		Func_3__ctor_m1056795178(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1056795178_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisWWW_t1522972100_m2159439317(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::PostWWW(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostWWWU3Ec__AnonStorey8A_t4245247437_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t1491243043_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostWWWU3Ec__AnonStorey8A_U3CU3Em__B6_m957328904_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1056795178_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var;
extern const uint32_t ObservableWWW_PostWWW_m1119145456_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_PostWWW_m1119145456 (Il2CppObject * __this /* static, unused */, String_t* ___url0, ByteU5BU5D_t58506160* ___postData1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_PostWWW_m1119145456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * V_0 = NULL;
	{
		U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * L_0 = (U3CPostWWWU3Ec__AnonStorey8A_t4245247437 *)il2cpp_codegen_object_new(U3CPostWWWU3Ec__AnonStorey8A_t4245247437_il2cpp_TypeInfo_var);
		U3CPostWWWU3Ec__AnonStorey8A__ctor_m113643343(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * L_3 = V_0;
		ByteU5BU5D_t58506160* L_4 = ___postData1;
		NullCheck(L_3);
		L_3->set_postData_1(L_4);
		U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * L_5 = V_0;
		Dictionary_2_t2606186806 * L_6 = ___headers2;
		NullCheck(L_5);
		L_5->set_headers_2(L_6);
		U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * L_7 = V_0;
		Il2CppObject* L_8 = ___progress3;
		NullCheck(L_7);
		L_7->set_progress_3(L_8);
		U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CPostWWWU3Ec__AnonStorey8A_U3CU3Em__B6_m957328904_MethodInfo_var);
		Func_3_t1491243043 * L_11 = (Func_3_t1491243043 *)il2cpp_codegen_object_new(Func_3_t1491243043_il2cpp_TypeInfo_var);
		Func_3__ctor_m1056795178(L_11, L_9, L_10, /*hidden argument*/Func_3__ctor_m1056795178_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = Observable_FromCoroutine_TisWWW_t1522972100_m2159439317(NULL /*static, unused*/, L_11, /*hidden argument*/Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var);
		return L_12;
	}
}
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::PostWWW(System.String,UnityEngine.WWWForm,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostWWWU3Ec__AnonStorey8B_t4245247438_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t1491243043_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostWWWU3Ec__AnonStorey8B_U3CU3Em__B7_m744861480_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1056795178_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var;
extern const uint32_t ObservableWWW_PostWWW_m1240793656_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_PostWWW_m1240793656 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_PostWWW_m1240793656_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostWWWU3Ec__AnonStorey8B_t4245247438 * V_0 = NULL;
	{
		U3CPostWWWU3Ec__AnonStorey8B_t4245247438 * L_0 = (U3CPostWWWU3Ec__AnonStorey8B_t4245247438 *)il2cpp_codegen_object_new(U3CPostWWWU3Ec__AnonStorey8B_t4245247438_il2cpp_TypeInfo_var);
		U3CPostWWWU3Ec__AnonStorey8B__ctor_m4212097134(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostWWWU3Ec__AnonStorey8B_t4245247438 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostWWWU3Ec__AnonStorey8B_t4245247438 * L_3 = V_0;
		WWWForm_t3999572776 * L_4 = ___content1;
		NullCheck(L_3);
		L_3->set_content_1(L_4);
		U3CPostWWWU3Ec__AnonStorey8B_t4245247438 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CPostWWWU3Ec__AnonStorey8B_t4245247438 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CPostWWWU3Ec__AnonStorey8B_U3CU3Em__B7_m744861480_MethodInfo_var);
		Func_3_t1491243043 * L_9 = (Func_3_t1491243043 *)il2cpp_codegen_object_new(Func_3_t1491243043_il2cpp_TypeInfo_var);
		Func_3__ctor_m1056795178(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1056795178_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisWWW_t1522972100_m2159439317(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<UnityEngine.WWW> UniRx.ObservableWWW::PostWWW(System.String,UnityEngine.WWWForm,System.Collections.Generic.Dictionary`2<System.String,System.String>,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CPostWWWU3Ec__AnonStorey8C_t4245247439_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t1491243043_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPostWWWU3Ec__AnonStorey8C_U3CU3Em__B8_m532394056_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1056795178_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var;
extern const uint32_t ObservableWWW_PostWWW_m2691230453_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_PostWWW_m2691230453 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t3999572776 * ___content1, Dictionary_2_t2606186806 * ___headers2, Il2CppObject* ___progress3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_PostWWW_m2691230453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * V_0 = NULL;
	{
		U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * L_0 = (U3CPostWWWU3Ec__AnonStorey8C_t4245247439 *)il2cpp_codegen_object_new(U3CPostWWWU3Ec__AnonStorey8C_t4245247439_il2cpp_TypeInfo_var);
		U3CPostWWWU3Ec__AnonStorey8C__ctor_m4015583629(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * L_3 = V_0;
		WWWForm_t3999572776 * L_4 = ___content1;
		NullCheck(L_3);
		L_3->set_content_1(L_4);
		U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * L_5 = V_0;
		Dictionary_2_t2606186806 * L_6 = ___headers2;
		NullCheck(L_5);
		L_5->set_headers_3(L_6);
		U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * L_7 = V_0;
		Il2CppObject* L_8 = ___progress3;
		NullCheck(L_7);
		L_7->set_progress_4(L_8);
		U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * L_9 = V_0;
		U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * L_10 = V_0;
		NullCheck(L_10);
		WWWForm_t3999572776 * L_11 = L_10->get_content_1();
		NullCheck(L_11);
		Dictionary_2_t2606186806 * L_12 = WWWForm_get_headers_m370408569(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_contentHeaders_2(L_12);
		U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * L_13 = V_0;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)U3CPostWWWU3Ec__AnonStorey8C_U3CU3Em__B8_m532394056_MethodInfo_var);
		Func_3_t1491243043 * L_15 = (Func_3_t1491243043 *)il2cpp_codegen_object_new(Func_3_t1491243043_il2cpp_TypeInfo_var);
		Func_3__ctor_m1056795178(L_15, L_13, L_14, /*hidden argument*/Func_3__ctor_m1056795178_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_16 = Observable_FromCoroutine_TisWWW_t1522972100_m2159439317(NULL /*static, unused*/, L_15, /*hidden argument*/Observable_FromCoroutine_TisWWW_t1522972100_m2159439317_MethodInfo_var);
		return L_16;
	}
}
// UniRx.IObservable`1<UnityEngine.AssetBundle> UniRx.ObservableWWW::LoadFromCacheOrDownload(System.String,System.Int32,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3855672678_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_U3CU3Em__B9_m4015006557_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4184979727_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154_MethodInfo_var;
extern const uint32_t ObservableWWW_LoadFromCacheOrDownload_m1210134636_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_LoadFromCacheOrDownload_m1210134636 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___version1, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_LoadFromCacheOrDownload_m1210134636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * V_0 = NULL;
	{
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * L_0 = (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 *)il2cpp_codegen_object_new(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550_il2cpp_TypeInfo_var);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D__ctor_m2740786022(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * L_3 = V_0;
		int32_t L_4 = ___version1;
		NullCheck(L_3);
		L_3->set_version_1(L_4);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_U3CU3Em__B9_m4015006557_MethodInfo_var);
		Func_3_t3855672678 * L_9 = (Func_3_t3855672678 *)il2cpp_codegen_object_new(Func_3_t3855672678_il2cpp_TypeInfo_var);
		Func_3__ctor_m4184979727(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m4184979727_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<UnityEngine.AssetBundle> UniRx.ObservableWWW::LoadFromCacheOrDownload(System.String,System.Int32,System.UInt32,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3855672678_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_U3CU3Em__BA_m2464018692_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4184979727_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154_MethodInfo_var;
extern const uint32_t ObservableWWW_LoadFromCacheOrDownload_m3883885368_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_LoadFromCacheOrDownload_m3883885368 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___version1, uint32_t ___crc2, Il2CppObject* ___progress3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_LoadFromCacheOrDownload_m3883885368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * V_0 = NULL;
	{
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * L_0 = (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 *)il2cpp_codegen_object_new(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551_il2cpp_TypeInfo_var);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E__ctor_m2544272517(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * L_3 = V_0;
		int32_t L_4 = ___version1;
		NullCheck(L_3);
		L_3->set_version_1(L_4);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * L_5 = V_0;
		uint32_t L_6 = ___crc2;
		NullCheck(L_5);
		L_5->set_crc_2(L_6);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * L_7 = V_0;
		Il2CppObject* L_8 = ___progress3;
		NullCheck(L_7);
		L_7->set_progress_3(L_8);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_U3CU3Em__BA_m2464018692_MethodInfo_var);
		Func_3_t3855672678 * L_11 = (Func_3_t3855672678 *)il2cpp_codegen_object_new(Func_3_t3855672678_il2cpp_TypeInfo_var);
		Func_3__ctor_m4184979727(L_11, L_9, L_10, /*hidden argument*/Func_3__ctor_m4184979727_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154(NULL /*static, unused*/, L_11, /*hidden argument*/Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154_MethodInfo_var);
		return L_12;
	}
}
// UniRx.IObservable`1<UnityEngine.AssetBundle> UniRx.ObservableWWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3855672678_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_U3CU3Em__BB_m2637222436_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4184979727_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154_MethodInfo_var;
extern const uint32_t ObservableWWW_LoadFromCacheOrDownload_m579420519_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_LoadFromCacheOrDownload_m579420519 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t3885020822  ___hash1281, Il2CppObject* ___progress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_LoadFromCacheOrDownload_m579420519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 * V_0 = NULL;
	{
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 * L_0 = (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 *)il2cpp_codegen_object_new(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552_il2cpp_TypeInfo_var);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F__ctor_m2347759012(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 * L_3 = V_0;
		Hash128_t3885020822  L_4 = ___hash1281;
		NullCheck(L_3);
		L_3->set_hash128_1(L_4);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 * L_5 = V_0;
		Il2CppObject* L_6 = ___progress2;
		NullCheck(L_5);
		L_5->set_progress_2(L_6);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_U3CU3Em__BB_m2637222436_MethodInfo_var);
		Func_3_t3855672678 * L_9 = (Func_3_t3855672678 *)il2cpp_codegen_object_new(Func_3_t3855672678_il2cpp_TypeInfo_var);
		Func_3__ctor_m4184979727(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m4184979727_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154(NULL /*static, unused*/, L_9, /*hidden argument*/Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154_MethodInfo_var);
		return L_10;
	}
}
// UniRx.IObservable`1<UnityEngine.AssetBundle> UniRx.ObservableWWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,System.UInt32,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3855672678_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_U3CU3Em__BC_m1871593532_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m4184979727_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154_MethodInfo_var;
extern const uint32_t ObservableWWW_LoadFromCacheOrDownload_m1017946355_MetadataUsageId;
extern "C"  Il2CppObject* ObservableWWW_LoadFromCacheOrDownload_m1017946355 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t3885020822  ___hash1281, uint32_t ___crc2, Il2CppObject* ___progress3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_LoadFromCacheOrDownload_m1017946355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * V_0 = NULL;
	{
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * L_0 = (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 *)il2cpp_codegen_object_new(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561_il2cpp_TypeInfo_var);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90__ctor_m579137467(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * L_3 = V_0;
		Hash128_t3885020822  L_4 = ___hash1281;
		NullCheck(L_3);
		L_3->set_hash128_1(L_4);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * L_5 = V_0;
		uint32_t L_6 = ___crc2;
		NullCheck(L_5);
		L_5->set_crc_2(L_6);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * L_7 = V_0;
		Il2CppObject* L_8 = ___progress3;
		NullCheck(L_7);
		L_7->set_progress_3(L_8);
		U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_U3CU3Em__BC_m1871593532_MethodInfo_var);
		Func_3_t3855672678 * L_11 = (Func_3_t3855672678 *)il2cpp_codegen_object_new(Func_3_t3855672678_il2cpp_TypeInfo_var);
		Func_3__ctor_m4184979727(L_11, L_9, L_10, /*hidden argument*/Func_3__ctor_m4184979727_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154(NULL /*static, unused*/, L_11, /*hidden argument*/Observable_FromCoroutine_TisAssetBundle_t3959431103_m490924154_MethodInfo_var);
		return L_12;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW::MergeHash(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* Enumerator_t2373214747_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2871721525_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1739472607_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2577713898_MethodInfo_var;
extern const uint32_t ObservableWWW_MergeHash_m4254025137_MetadataUsageId;
extern "C"  Dictionary_2_t2606186806 * ObservableWWW_MergeHash_m4254025137 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___wwwFormHeaders0, Dictionary_2_t2606186806 * ___externalHeaders1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_MergeHash_m4254025137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2094718104  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t2373214747  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2606186806 * L_0 = ___externalHeaders1;
		NullCheck(L_0);
		Enumerator_t2373214747  L_1 = Dictionary_2_GetEnumerator_m2759194411(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0028;
		}

IL_000c:
		{
			KeyValuePair_2_t2094718104  L_2 = Enumerator_get_Current_m2871721525((&V_1), /*hidden argument*/Enumerator_get_Current_m2871721525_MethodInfo_var);
			V_0 = L_2;
			Dictionary_2_t2606186806 * L_3 = ___wwwFormHeaders0;
			String_t* L_4 = KeyValuePair_2_get_Key_m1739472607((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1739472607_MethodInfo_var);
			String_t* L_5 = KeyValuePair_2_get_Value_m730091314((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
			NullCheck(L_3);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_3, L_4, L_5);
		}

IL_0028:
		{
			bool L_6 = Enumerator_MoveNext_m2577713898((&V_1), /*hidden argument*/Enumerator_MoveNext_m2577713898_MethodInfo_var);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x45, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		Enumerator_t2373214747  L_7 = V_1;
		Enumerator_t2373214747  L_8 = L_7;
		Il2CppObject * L_9 = Box(Enumerator_t2373214747_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		Dictionary_2_t2606186806 * L_10 = ___wwwFormHeaders0;
		return L_10;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW::Fetch(UnityEngine.WWW,UniRx.IObserver`1<UnityEngine.WWW>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern Il2CppClass* U3CFetchU3Ec__Iterator20_t2016563951_il2cpp_TypeInfo_var;
extern const uint32_t ObservableWWW_Fetch_m4100415835_MetadataUsageId;
extern "C"  Il2CppObject * ObservableWWW_Fetch_m4100415835 (Il2CppObject * __this /* static, unused */, WWW_t1522972100 * ___www0, Il2CppObject* ___observer1, Il2CppObject* ___reportProgress2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_Fetch_m4100415835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchU3Ec__Iterator20_t2016563951 * V_0 = NULL;
	{
		U3CFetchU3Ec__Iterator20_t2016563951 * L_0 = (U3CFetchU3Ec__Iterator20_t2016563951 *)il2cpp_codegen_object_new(U3CFetchU3Ec__Iterator20_t2016563951_il2cpp_TypeInfo_var);
		U3CFetchU3Ec__Iterator20__ctor_m1884724461(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchU3Ec__Iterator20_t2016563951 * L_1 = V_0;
		WWW_t1522972100 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CFetchU3Ec__Iterator20_t2016563951 * L_3 = V_0;
		Il2CppObject* L_4 = ___reportProgress2;
		NullCheck(L_3);
		L_3->set_reportProgress_2(L_4);
		U3CFetchU3Ec__Iterator20_t2016563951 * L_5 = V_0;
		CancellationToken_t1439151560  L_6 = ___cancel3;
		NullCheck(L_5);
		L_5->set_cancel_3(L_6);
		U3CFetchU3Ec__Iterator20_t2016563951 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer1;
		NullCheck(L_7);
		L_7->set_observer_4(L_8);
		U3CFetchU3Ec__Iterator20_t2016563951 * L_9 = V_0;
		WWW_t1522972100 * L_10 = ___www0;
		NullCheck(L_9);
		L_9->set_U3CU24U3Ewww_9(L_10);
		U3CFetchU3Ec__Iterator20_t2016563951 * L_11 = V_0;
		Il2CppObject* L_12 = ___reportProgress2;
		NullCheck(L_11);
		L_11->set_U3CU24U3EreportProgress_10(L_12);
		U3CFetchU3Ec__Iterator20_t2016563951 * L_13 = V_0;
		CancellationToken_t1439151560  L_14 = ___cancel3;
		NullCheck(L_13);
		L_13->set_U3CU24U3Ecancel_11(L_14);
		U3CFetchU3Ec__Iterator20_t2016563951 * L_15 = V_0;
		Il2CppObject* L_16 = ___observer1;
		NullCheck(L_15);
		L_15->set_U3CU24U3Eobserver_12(L_16);
		U3CFetchU3Ec__Iterator20_t2016563951 * L_17 = V_0;
		return L_17;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW::FetchText(UnityEngine.WWW,UniRx.IObserver`1<System.String>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern Il2CppClass* U3CFetchTextU3Ec__Iterator21_t605446781_il2cpp_TypeInfo_var;
extern const uint32_t ObservableWWW_FetchText_m220208830_MetadataUsageId;
extern "C"  Il2CppObject * ObservableWWW_FetchText_m220208830 (Il2CppObject * __this /* static, unused */, WWW_t1522972100 * ___www0, Il2CppObject* ___observer1, Il2CppObject* ___reportProgress2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_FetchText_m220208830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchTextU3Ec__Iterator21_t605446781 * V_0 = NULL;
	{
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_0 = (U3CFetchTextU3Ec__Iterator21_t605446781 *)il2cpp_codegen_object_new(U3CFetchTextU3Ec__Iterator21_t605446781_il2cpp_TypeInfo_var);
		U3CFetchTextU3Ec__Iterator21__ctor_m3794028703(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_1 = V_0;
		WWW_t1522972100 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_3 = V_0;
		Il2CppObject* L_4 = ___reportProgress2;
		NullCheck(L_3);
		L_3->set_reportProgress_2(L_4);
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_5 = V_0;
		CancellationToken_t1439151560  L_6 = ___cancel3;
		NullCheck(L_5);
		L_5->set_cancel_3(L_6);
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer1;
		NullCheck(L_7);
		L_7->set_observer_4(L_8);
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_9 = V_0;
		WWW_t1522972100 * L_10 = ___www0;
		NullCheck(L_9);
		L_9->set_U3CU24U3Ewww_9(L_10);
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_11 = V_0;
		Il2CppObject* L_12 = ___reportProgress2;
		NullCheck(L_11);
		L_11->set_U3CU24U3EreportProgress_10(L_12);
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_13 = V_0;
		CancellationToken_t1439151560  L_14 = ___cancel3;
		NullCheck(L_13);
		L_13->set_U3CU24U3Ecancel_11(L_14);
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_15 = V_0;
		Il2CppObject* L_16 = ___observer1;
		NullCheck(L_15);
		L_15->set_U3CU24U3Eobserver_12(L_16);
		U3CFetchTextU3Ec__Iterator21_t605446781 * L_17 = V_0;
		return L_17;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW::FetchBytes(UnityEngine.WWW,UniRx.IObserver`1<System.Byte[]>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern Il2CppClass* U3CFetchBytesU3Ec__Iterator22_t157331456_il2cpp_TypeInfo_var;
extern const uint32_t ObservableWWW_FetchBytes_m789535179_MetadataUsageId;
extern "C"  Il2CppObject * ObservableWWW_FetchBytes_m789535179 (Il2CppObject * __this /* static, unused */, WWW_t1522972100 * ___www0, Il2CppObject* ___observer1, Il2CppObject* ___reportProgress2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_FetchBytes_m789535179_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchBytesU3Ec__Iterator22_t157331456 * V_0 = NULL;
	{
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_0 = (U3CFetchBytesU3Ec__Iterator22_t157331456 *)il2cpp_codegen_object_new(U3CFetchBytesU3Ec__Iterator22_t157331456_il2cpp_TypeInfo_var);
		U3CFetchBytesU3Ec__Iterator22__ctor_m396292522(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_1 = V_0;
		WWW_t1522972100 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_3 = V_0;
		Il2CppObject* L_4 = ___reportProgress2;
		NullCheck(L_3);
		L_3->set_reportProgress_2(L_4);
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_5 = V_0;
		CancellationToken_t1439151560  L_6 = ___cancel3;
		NullCheck(L_5);
		L_5->set_cancel_3(L_6);
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer1;
		NullCheck(L_7);
		L_7->set_observer_4(L_8);
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_9 = V_0;
		WWW_t1522972100 * L_10 = ___www0;
		NullCheck(L_9);
		L_9->set_U3CU24U3Ewww_9(L_10);
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_11 = V_0;
		Il2CppObject* L_12 = ___reportProgress2;
		NullCheck(L_11);
		L_11->set_U3CU24U3EreportProgress_10(L_12);
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_13 = V_0;
		CancellationToken_t1439151560  L_14 = ___cancel3;
		NullCheck(L_13);
		L_13->set_U3CU24U3Ecancel_11(L_14);
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_15 = V_0;
		Il2CppObject* L_16 = ___observer1;
		NullCheck(L_15);
		L_15->set_U3CU24U3Eobserver_12(L_16);
		U3CFetchBytesU3Ec__Iterator22_t157331456 * L_17 = V_0;
		return L_17;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW::FetchAssetBundle(UnityEngine.WWW,UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern Il2CppClass* U3CFetchAssetBundleU3Ec__Iterator23_t3024943464_il2cpp_TypeInfo_var;
extern const uint32_t ObservableWWW_FetchAssetBundle_m2288014994_MetadataUsageId;
extern "C"  Il2CppObject * ObservableWWW_FetchAssetBundle_m2288014994 (Il2CppObject * __this /* static, unused */, WWW_t1522972100 * ___www0, Il2CppObject* ___observer1, Il2CppObject* ___reportProgress2, CancellationToken_t1439151560  ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableWWW_FetchAssetBundle_m2288014994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * V_0 = NULL;
	{
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_0 = (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 *)il2cpp_codegen_object_new(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464_il2cpp_TypeInfo_var);
		U3CFetchAssetBundleU3Ec__Iterator23__ctor_m3639018498(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_1 = V_0;
		WWW_t1522972100 * L_2 = ___www0;
		NullCheck(L_1);
		L_1->set_www_0(L_2);
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_3 = V_0;
		Il2CppObject* L_4 = ___reportProgress2;
		NullCheck(L_3);
		L_3->set_reportProgress_2(L_4);
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_5 = V_0;
		CancellationToken_t1439151560  L_6 = ___cancel3;
		NullCheck(L_5);
		L_5->set_cancel_3(L_6);
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer1;
		NullCheck(L_7);
		L_7->set_observer_4(L_8);
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_9 = V_0;
		WWW_t1522972100 * L_10 = ___www0;
		NullCheck(L_9);
		L_9->set_U3CU24U3Ewww_9(L_10);
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_11 = V_0;
		Il2CppObject* L_12 = ___reportProgress2;
		NullCheck(L_11);
		L_11->set_U3CU24U3EreportProgress_10(L_12);
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_13 = V_0;
		CancellationToken_t1439151560  L_14 = ___cancel3;
		NullCheck(L_13);
		L_13->set_U3CU24U3Ecancel_11(L_14);
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_15 = V_0;
		Il2CppObject* L_16 = ___observer1;
		NullCheck(L_15);
		L_15->set_U3CU24U3Eobserver_12(L_16);
		U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * L_17 = V_0;
		return L_17;
	}
}
// System.Void UniRx.ObservableWWW/<Fetch>c__Iterator20::.ctor()
extern "C"  void U3CFetchU3Ec__Iterator20__ctor_m1884724461 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.ObservableWWW/<Fetch>c__Iterator20::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1972252431 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object UniRx.ObservableWWW/<Fetch>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m3838193827 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Boolean UniRx.ObservableWWW/<Fetch>c__Iterator20::MoveNext()
extern Il2CppClass* IProgress_1_t4173802291_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t3734971003_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWErrorException_t3758994352_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchU3Ec__Iterator20_MoveNext_m3171663031_MetadataUsageId;
extern "C"  bool U3CFetchU3Ec__Iterator20_MoveNext_m3171663031 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchU3Ec__Iterator20_MoveNext_m3171663031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_003a;
		}
		if (L_1 == 2)
		{
			goto IL_003a;
		}
		if (L_1 == 3)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_01fb;
	}

IL_002b:
	{
		WWW_t1522972100 * L_2 = __this->get_www_0();
		__this->set_U3CU24s_342U3E__0_1(L_2);
		V_0 = ((int32_t)-3);
	}

IL_003a:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_3 = V_0;
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
			{
				goto IL_00b1;
			}
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
			{
				goto IL_0100;
			}
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
			{
				goto IL_013a;
			}
		}

IL_004e:
		{
			Il2CppObject* L_4 = __this->get_reportProgress_2();
			if (!L_4)
			{
				goto IL_00d6;
			}
		}

IL_0059:
		{
			goto IL_00b1;
		}

IL_005e:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = __this->get_reportProgress_2();
			WWW_t1522972100 * L_6 = __this->get_www_0();
			NullCheck(L_6);
			float L_7 = WWW_get_progress_m3186358572(L_6, /*hidden argument*/NULL);
			NullCheck(L_5);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, L_5, L_7);
			goto IL_009c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0079;
			throw e;
		}

CATCH_0079:
		{ // begin catch(System.Exception)
			{
				V_2 = ((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_8 = V_2;
				__this->set_U3CexU3E__1_5(L_8);
				Il2CppObject* L_9 = __this->get_observer_4();
				Exception_t1967233988 * L_10 = __this->get_U3CexU3E__1_5();
				NullCheck(L_9);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.WWW>::OnError(System.Exception) */, IObserver_1_t3734971003_il2cpp_TypeInfo_var, L_9, L_10);
				IL2CPP_LEAVE(0x1FB, FINALLY_01d9);
			}

IL_0097:
			{
				; // IL_0097: leave IL_009c
			}
		} // end catch (depth: 2)

IL_009c:
		{
			__this->set_U24current_8(NULL);
			__this->set_U24PC_7(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1FD, FINALLY_01d9);
		}

IL_00b1:
		{
			WWW_t1522972100 * L_11 = __this->get_www_0();
			NullCheck(L_11);
			bool L_12 = WWW_get_isDone_m634060017(L_11, /*hidden argument*/NULL);
			if (L_12)
			{
				goto IL_00d1;
			}
		}

IL_00c1:
		{
			CancellationToken_t1439151560 * L_13 = __this->get_address_of_cancel_3();
			bool L_14 = CancellationToken_get_IsCancellationRequested_m1021497867(L_13, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_005e;
			}
		}

IL_00d1:
		{
			goto IL_0100;
		}

IL_00d6:
		{
			WWW_t1522972100 * L_15 = __this->get_www_0();
			NullCheck(L_15);
			bool L_16 = WWW_get_isDone_m634060017(L_15, /*hidden argument*/NULL);
			if (L_16)
			{
				goto IL_0100;
			}
		}

IL_00e6:
		{
			WWW_t1522972100 * L_17 = __this->get_www_0();
			__this->set_U24current_8(L_17);
			__this->set_U24PC_7(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1FD, FINALLY_01d9);
		}

IL_0100:
		{
			CancellationToken_t1439151560 * L_18 = __this->get_address_of_cancel_3();
			bool L_19 = CancellationToken_get_IsCancellationRequested_m1021497867(L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_013f;
			}
		}

IL_0110:
		{
			WWW_t1522972100 * L_20 = __this->get_www_0();
			NullCheck(L_20);
			bool L_21 = WWW_get_isDone_m634060017(L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_013a;
			}
		}

IL_0120:
		{
			WWW_t1522972100 * L_22 = __this->get_www_0();
			__this->set_U24current_8(L_22);
			__this->set_U24PC_7(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1FD, FINALLY_01d9);
		}

IL_013a:
		{
			IL2CPP_LEAVE(0x1FB, FINALLY_01d9);
		}

IL_013f:
		{
			Il2CppObject* L_23 = __this->get_reportProgress_2();
			if (!L_23)
			{
				goto IL_0188;
			}
		}

IL_014a:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_24 = __this->get_reportProgress_2();
			WWW_t1522972100 * L_25 = __this->get_www_0();
			NullCheck(L_25);
			float L_26 = WWW_get_progress_m3186358572(L_25, /*hidden argument*/NULL);
			NullCheck(L_24);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, L_24, L_26);
			goto IL_0188;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0165;
			throw e;
		}

CATCH_0165:
		{ // begin catch(System.Exception)
			{
				V_3 = ((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_27 = V_3;
				__this->set_U3CexU3E__2_6(L_27);
				Il2CppObject* L_28 = __this->get_observer_4();
				Exception_t1967233988 * L_29 = __this->get_U3CexU3E__2_6();
				NullCheck(L_28);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.WWW>::OnError(System.Exception) */, IObserver_1_t3734971003_il2cpp_TypeInfo_var, L_28, L_29);
				IL2CPP_LEAVE(0x1FB, FINALLY_01d9);
			}

IL_0183:
			{
				; // IL_0183: leave IL_0188
			}
		} // end catch (depth: 2)

IL_0188:
		{
			WWW_t1522972100 * L_30 = __this->get_www_0();
			NullCheck(L_30);
			String_t* L_31 = WWW_get_error_m1787423074(L_30, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_32 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
			if (L_32)
			{
				goto IL_01b8;
			}
		}

IL_019d:
		{
			Il2CppObject* L_33 = __this->get_observer_4();
			WWW_t1522972100 * L_34 = __this->get_www_0();
			WWWErrorException_t3758994352 * L_35 = (WWWErrorException_t3758994352 *)il2cpp_codegen_object_new(WWWErrorException_t3758994352_il2cpp_TypeInfo_var);
			WWWErrorException__ctor_m2195240225(L_35, L_34, /*hidden argument*/NULL);
			NullCheck(L_33);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.WWW>::OnError(System.Exception) */, IObserver_1_t3734971003_il2cpp_TypeInfo_var, L_33, L_35);
			goto IL_01d4;
		}

IL_01b8:
		{
			Il2CppObject* L_36 = __this->get_observer_4();
			WWW_t1522972100 * L_37 = __this->get_www_0();
			NullCheck(L_36);
			InterfaceActionInvoker1< WWW_t1522972100 * >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.WWW>::OnNext(T) */, IObserver_1_t3734971003_il2cpp_TypeInfo_var, L_36, L_37);
			Il2CppObject* L_38 = __this->get_observer_4();
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.WWW>::OnCompleted() */, IObserver_1_t3734971003_il2cpp_TypeInfo_var, L_38);
		}

IL_01d4:
		{
			IL2CPP_LEAVE(0x1F4, FINALLY_01d9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01d9;
	}

FINALLY_01d9:
	{ // begin finally (depth: 1)
		{
			bool L_39 = V_1;
			if (!L_39)
			{
				goto IL_01dd;
			}
		}

IL_01dc:
		{
			IL2CPP_END_FINALLY(473)
		}

IL_01dd:
		{
			WWW_t1522972100 * L_40 = __this->get_U3CU24s_342U3E__0_1();
			if (!L_40)
			{
				goto IL_01f3;
			}
		}

IL_01e8:
		{
			WWW_t1522972100 * L_41 = __this->get_U3CU24s_342U3E__0_1();
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_41);
		}

IL_01f3:
		{
			IL2CPP_END_FINALLY(473)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(473)
	{
		IL2CPP_JUMP_TBL(0x1FB, IL_01fb)
		IL2CPP_JUMP_TBL(0x1FD, IL_01fd)
		IL2CPP_JUMP_TBL(0x1F4, IL_01f4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01f4:
	{
		__this->set_U24PC_7((-1));
	}

IL_01fb:
	{
		return (bool)0;
	}

IL_01fd:
	{
		return (bool)1;
	}
	// Dead block : IL_01ff: ldloc.s V_4
}
// System.Void UniRx.ObservableWWW/<Fetch>c__Iterator20::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchU3Ec__Iterator20_Dispose_m2937258474_MetadataUsageId;
extern "C"  void U3CFetchU3Ec__Iterator20_Dispose_m2937258474 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchU3Ec__Iterator20_Dispose_m2937258474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0045;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0045;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x45, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			WWW_t1522972100 * L_2 = __this->get_U3CU24s_342U3E__0_1();
			if (!L_2)
			{
				goto IL_0044;
			}
		}

IL_0039:
		{
			WWW_t1522972100 * L_3 = __this->get_U3CU24s_342U3E__0_1();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
		}

IL_0044:
		{
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.ObservableWWW/<Fetch>c__Iterator20::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchU3Ec__Iterator20_Reset_m3826124698_MetadataUsageId;
extern "C"  void U3CFetchU3Ec__Iterator20_Reset_m3826124698 (U3CFetchU3Ec__Iterator20_t2016563951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchU3Ec__Iterator20_Reset_m3826124698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::.ctor()
extern "C"  void U3CFetchAssetBundleU3Ec__Iterator23__ctor_m3639018498 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchAssetBundleU3Ec__Iterator23_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2276305936 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchAssetBundleU3Ec__Iterator23_System_Collections_IEnumerator_get_Current_m2874789796 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Boolean UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::MoveNext()
extern Il2CppClass* IProgress_1_t4173802291_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t1876462710_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWErrorException_t3758994352_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchAssetBundleU3Ec__Iterator23_MoveNext_m445323466_MetadataUsageId;
extern "C"  bool U3CFetchAssetBundleU3Ec__Iterator23_MoveNext_m445323466 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchAssetBundleU3Ec__Iterator23_MoveNext_m445323466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_003a;
		}
		if (L_1 == 2)
		{
			goto IL_003a;
		}
		if (L_1 == 3)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0200;
	}

IL_002b:
	{
		WWW_t1522972100 * L_2 = __this->get_www_0();
		__this->set_U3CU24s_345U3E__0_1(L_2);
		V_0 = ((int32_t)-3);
	}

IL_003a:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_3 = V_0;
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
			{
				goto IL_00b1;
			}
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
			{
				goto IL_0100;
			}
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
			{
				goto IL_013a;
			}
		}

IL_004e:
		{
			Il2CppObject* L_4 = __this->get_reportProgress_2();
			if (!L_4)
			{
				goto IL_00d6;
			}
		}

IL_0059:
		{
			goto IL_00b1;
		}

IL_005e:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = __this->get_reportProgress_2();
			WWW_t1522972100 * L_6 = __this->get_www_0();
			NullCheck(L_6);
			float L_7 = WWW_get_progress_m3186358572(L_6, /*hidden argument*/NULL);
			NullCheck(L_5);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, L_5, L_7);
			goto IL_009c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0079;
			throw e;
		}

CATCH_0079:
		{ // begin catch(System.Exception)
			{
				V_2 = ((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_8 = V_2;
				__this->set_U3CexU3E__1_5(L_8);
				Il2CppObject* L_9 = __this->get_observer_4();
				Exception_t1967233988 * L_10 = __this->get_U3CexU3E__1_5();
				NullCheck(L_9);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.AssetBundle>::OnError(System.Exception) */, IObserver_1_t1876462710_il2cpp_TypeInfo_var, L_9, L_10);
				IL2CPP_LEAVE(0x200, FINALLY_01de);
			}

IL_0097:
			{
				; // IL_0097: leave IL_009c
			}
		} // end catch (depth: 2)

IL_009c:
		{
			__this->set_U24current_8(NULL);
			__this->set_U24PC_7(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_00b1:
		{
			WWW_t1522972100 * L_11 = __this->get_www_0();
			NullCheck(L_11);
			bool L_12 = WWW_get_isDone_m634060017(L_11, /*hidden argument*/NULL);
			if (L_12)
			{
				goto IL_00d1;
			}
		}

IL_00c1:
		{
			CancellationToken_t1439151560 * L_13 = __this->get_address_of_cancel_3();
			bool L_14 = CancellationToken_get_IsCancellationRequested_m1021497867(L_13, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_005e;
			}
		}

IL_00d1:
		{
			goto IL_0100;
		}

IL_00d6:
		{
			WWW_t1522972100 * L_15 = __this->get_www_0();
			NullCheck(L_15);
			bool L_16 = WWW_get_isDone_m634060017(L_15, /*hidden argument*/NULL);
			if (L_16)
			{
				goto IL_0100;
			}
		}

IL_00e6:
		{
			WWW_t1522972100 * L_17 = __this->get_www_0();
			__this->set_U24current_8(L_17);
			__this->set_U24PC_7(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_0100:
		{
			CancellationToken_t1439151560 * L_18 = __this->get_address_of_cancel_3();
			bool L_19 = CancellationToken_get_IsCancellationRequested_m1021497867(L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_013f;
			}
		}

IL_0110:
		{
			WWW_t1522972100 * L_20 = __this->get_www_0();
			NullCheck(L_20);
			bool L_21 = WWW_get_isDone_m634060017(L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_013a;
			}
		}

IL_0120:
		{
			WWW_t1522972100 * L_22 = __this->get_www_0();
			__this->set_U24current_8(L_22);
			__this->set_U24PC_7(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_013a:
		{
			IL2CPP_LEAVE(0x200, FINALLY_01de);
		}

IL_013f:
		{
			Il2CppObject* L_23 = __this->get_reportProgress_2();
			if (!L_23)
			{
				goto IL_0188;
			}
		}

IL_014a:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_24 = __this->get_reportProgress_2();
			WWW_t1522972100 * L_25 = __this->get_www_0();
			NullCheck(L_25);
			float L_26 = WWW_get_progress_m3186358572(L_25, /*hidden argument*/NULL);
			NullCheck(L_24);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, L_24, L_26);
			goto IL_0188;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0165;
			throw e;
		}

CATCH_0165:
		{ // begin catch(System.Exception)
			{
				V_3 = ((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_27 = V_3;
				__this->set_U3CexU3E__2_6(L_27);
				Il2CppObject* L_28 = __this->get_observer_4();
				Exception_t1967233988 * L_29 = __this->get_U3CexU3E__2_6();
				NullCheck(L_28);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.AssetBundle>::OnError(System.Exception) */, IObserver_1_t1876462710_il2cpp_TypeInfo_var, L_28, L_29);
				IL2CPP_LEAVE(0x200, FINALLY_01de);
			}

IL_0183:
			{
				; // IL_0183: leave IL_0188
			}
		} // end catch (depth: 2)

IL_0188:
		{
			WWW_t1522972100 * L_30 = __this->get_www_0();
			NullCheck(L_30);
			String_t* L_31 = WWW_get_error_m1787423074(L_30, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_32 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
			if (L_32)
			{
				goto IL_01b8;
			}
		}

IL_019d:
		{
			Il2CppObject* L_33 = __this->get_observer_4();
			WWW_t1522972100 * L_34 = __this->get_www_0();
			WWWErrorException_t3758994352 * L_35 = (WWWErrorException_t3758994352 *)il2cpp_codegen_object_new(WWWErrorException_t3758994352_il2cpp_TypeInfo_var);
			WWWErrorException__ctor_m2195240225(L_35, L_34, /*hidden argument*/NULL);
			NullCheck(L_33);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UnityEngine.AssetBundle>::OnError(System.Exception) */, IObserver_1_t1876462710_il2cpp_TypeInfo_var, L_33, L_35);
			goto IL_01d9;
		}

IL_01b8:
		{
			Il2CppObject* L_36 = __this->get_observer_4();
			WWW_t1522972100 * L_37 = __this->get_www_0();
			NullCheck(L_37);
			AssetBundle_t3959431103 * L_38 = WWW_get_assetBundle_m2674678273(L_37, /*hidden argument*/NULL);
			NullCheck(L_36);
			InterfaceActionInvoker1< AssetBundle_t3959431103 * >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.AssetBundle>::OnNext(T) */, IObserver_1_t1876462710_il2cpp_TypeInfo_var, L_36, L_38);
			Il2CppObject* L_39 = __this->get_observer_4();
			NullCheck(L_39);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.AssetBundle>::OnCompleted() */, IObserver_1_t1876462710_il2cpp_TypeInfo_var, L_39);
		}

IL_01d9:
		{
			IL2CPP_LEAVE(0x1F9, FINALLY_01de);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01de;
	}

FINALLY_01de:
	{ // begin finally (depth: 1)
		{
			bool L_40 = V_1;
			if (!L_40)
			{
				goto IL_01e2;
			}
		}

IL_01e1:
		{
			IL2CPP_END_FINALLY(478)
		}

IL_01e2:
		{
			WWW_t1522972100 * L_41 = __this->get_U3CU24s_345U3E__0_1();
			if (!L_41)
			{
				goto IL_01f8;
			}
		}

IL_01ed:
		{
			WWW_t1522972100 * L_42 = __this->get_U3CU24s_345U3E__0_1();
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_42);
		}

IL_01f8:
		{
			IL2CPP_END_FINALLY(478)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(478)
	{
		IL2CPP_JUMP_TBL(0x200, IL_0200)
		IL2CPP_JUMP_TBL(0x202, IL_0202)
		IL2CPP_JUMP_TBL(0x1F9, IL_01f9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01f9:
	{
		__this->set_U24PC_7((-1));
	}

IL_0200:
	{
		return (bool)0;
	}

IL_0202:
	{
		return (bool)1;
	}
	// Dead block : IL_0204: ldloc.s V_4
}
// System.Void UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchAssetBundleU3Ec__Iterator23_Dispose_m891680703_MetadataUsageId;
extern "C"  void U3CFetchAssetBundleU3Ec__Iterator23_Dispose_m891680703 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchAssetBundleU3Ec__Iterator23_Dispose_m891680703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0045;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0045;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x45, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			WWW_t1522972100 * L_2 = __this->get_U3CU24s_345U3E__0_1();
			if (!L_2)
			{
				goto IL_0044;
			}
		}

IL_0039:
		{
			WWW_t1522972100 * L_3 = __this->get_U3CU24s_345U3E__0_1();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
		}

IL_0044:
		{
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchAssetBundleU3Ec__Iterator23_Reset_m1285451439_MetadataUsageId;
extern "C"  void U3CFetchAssetBundleU3Ec__Iterator23_Reset_m1285451439 (U3CFetchAssetBundleU3Ec__Iterator23_t3024943464 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchAssetBundleU3Ec__Iterator23_Reset_m1285451439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.ObservableWWW/<FetchBytes>c__Iterator22::.ctor()
extern "C"  void U3CFetchBytesU3Ec__Iterator22__ctor_m396292522 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.ObservableWWW/<FetchBytes>c__Iterator22::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchBytesU3Ec__Iterator22_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1194214312 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object UniRx.ObservableWWW/<FetchBytes>c__Iterator22::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchBytesU3Ec__Iterator22_System_Collections_IEnumerator_get_Current_m3731658556 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Boolean UniRx.ObservableWWW/<FetchBytes>c__Iterator22::MoveNext()
extern Il2CppClass* IProgress_1_t4173802291_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t2270505063_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWErrorException_t3758994352_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchBytesU3Ec__Iterator22_MoveNext_m869164322_MetadataUsageId;
extern "C"  bool U3CFetchBytesU3Ec__Iterator22_MoveNext_m869164322 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchBytesU3Ec__Iterator22_MoveNext_m869164322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_003a;
		}
		if (L_1 == 2)
		{
			goto IL_003a;
		}
		if (L_1 == 3)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0200;
	}

IL_002b:
	{
		WWW_t1522972100 * L_2 = __this->get_www_0();
		__this->set_U3CU24s_344U3E__0_1(L_2);
		V_0 = ((int32_t)-3);
	}

IL_003a:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_3 = V_0;
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
			{
				goto IL_00b1;
			}
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
			{
				goto IL_0100;
			}
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
			{
				goto IL_013a;
			}
		}

IL_004e:
		{
			Il2CppObject* L_4 = __this->get_reportProgress_2();
			if (!L_4)
			{
				goto IL_00d6;
			}
		}

IL_0059:
		{
			goto IL_00b1;
		}

IL_005e:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = __this->get_reportProgress_2();
			WWW_t1522972100 * L_6 = __this->get_www_0();
			NullCheck(L_6);
			float L_7 = WWW_get_progress_m3186358572(L_6, /*hidden argument*/NULL);
			NullCheck(L_5);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, L_5, L_7);
			goto IL_009c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0079;
			throw e;
		}

CATCH_0079:
		{ // begin catch(System.Exception)
			{
				V_2 = ((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_8 = V_2;
				__this->set_U3CexU3E__1_5(L_8);
				Il2CppObject* L_9 = __this->get_observer_4();
				Exception_t1967233988 * L_10 = __this->get_U3CexU3E__1_5();
				NullCheck(L_9);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Byte[]>::OnError(System.Exception) */, IObserver_1_t2270505063_il2cpp_TypeInfo_var, L_9, L_10);
				IL2CPP_LEAVE(0x200, FINALLY_01de);
			}

IL_0097:
			{
				; // IL_0097: leave IL_009c
			}
		} // end catch (depth: 2)

IL_009c:
		{
			__this->set_U24current_8(NULL);
			__this->set_U24PC_7(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_00b1:
		{
			WWW_t1522972100 * L_11 = __this->get_www_0();
			NullCheck(L_11);
			bool L_12 = WWW_get_isDone_m634060017(L_11, /*hidden argument*/NULL);
			if (L_12)
			{
				goto IL_00d1;
			}
		}

IL_00c1:
		{
			CancellationToken_t1439151560 * L_13 = __this->get_address_of_cancel_3();
			bool L_14 = CancellationToken_get_IsCancellationRequested_m1021497867(L_13, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_005e;
			}
		}

IL_00d1:
		{
			goto IL_0100;
		}

IL_00d6:
		{
			WWW_t1522972100 * L_15 = __this->get_www_0();
			NullCheck(L_15);
			bool L_16 = WWW_get_isDone_m634060017(L_15, /*hidden argument*/NULL);
			if (L_16)
			{
				goto IL_0100;
			}
		}

IL_00e6:
		{
			WWW_t1522972100 * L_17 = __this->get_www_0();
			__this->set_U24current_8(L_17);
			__this->set_U24PC_7(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_0100:
		{
			CancellationToken_t1439151560 * L_18 = __this->get_address_of_cancel_3();
			bool L_19 = CancellationToken_get_IsCancellationRequested_m1021497867(L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_013f;
			}
		}

IL_0110:
		{
			WWW_t1522972100 * L_20 = __this->get_www_0();
			NullCheck(L_20);
			bool L_21 = WWW_get_isDone_m634060017(L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_013a;
			}
		}

IL_0120:
		{
			WWW_t1522972100 * L_22 = __this->get_www_0();
			__this->set_U24current_8(L_22);
			__this->set_U24PC_7(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_013a:
		{
			IL2CPP_LEAVE(0x200, FINALLY_01de);
		}

IL_013f:
		{
			Il2CppObject* L_23 = __this->get_reportProgress_2();
			if (!L_23)
			{
				goto IL_0188;
			}
		}

IL_014a:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_24 = __this->get_reportProgress_2();
			WWW_t1522972100 * L_25 = __this->get_www_0();
			NullCheck(L_25);
			float L_26 = WWW_get_progress_m3186358572(L_25, /*hidden argument*/NULL);
			NullCheck(L_24);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, L_24, L_26);
			goto IL_0188;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0165;
			throw e;
		}

CATCH_0165:
		{ // begin catch(System.Exception)
			{
				V_3 = ((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_27 = V_3;
				__this->set_U3CexU3E__2_6(L_27);
				Il2CppObject* L_28 = __this->get_observer_4();
				Exception_t1967233988 * L_29 = __this->get_U3CexU3E__2_6();
				NullCheck(L_28);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Byte[]>::OnError(System.Exception) */, IObserver_1_t2270505063_il2cpp_TypeInfo_var, L_28, L_29);
				IL2CPP_LEAVE(0x200, FINALLY_01de);
			}

IL_0183:
			{
				; // IL_0183: leave IL_0188
			}
		} // end catch (depth: 2)

IL_0188:
		{
			WWW_t1522972100 * L_30 = __this->get_www_0();
			NullCheck(L_30);
			String_t* L_31 = WWW_get_error_m1787423074(L_30, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_32 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
			if (L_32)
			{
				goto IL_01b8;
			}
		}

IL_019d:
		{
			Il2CppObject* L_33 = __this->get_observer_4();
			WWW_t1522972100 * L_34 = __this->get_www_0();
			WWWErrorException_t3758994352 * L_35 = (WWWErrorException_t3758994352 *)il2cpp_codegen_object_new(WWWErrorException_t3758994352_il2cpp_TypeInfo_var);
			WWWErrorException__ctor_m2195240225(L_35, L_34, /*hidden argument*/NULL);
			NullCheck(L_33);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Byte[]>::OnError(System.Exception) */, IObserver_1_t2270505063_il2cpp_TypeInfo_var, L_33, L_35);
			goto IL_01d9;
		}

IL_01b8:
		{
			Il2CppObject* L_36 = __this->get_observer_4();
			WWW_t1522972100 * L_37 = __this->get_www_0();
			NullCheck(L_37);
			ByteU5BU5D_t58506160* L_38 = WWW_get_bytes_m2080623436(L_37, /*hidden argument*/NULL);
			NullCheck(L_36);
			InterfaceActionInvoker1< ByteU5BU5D_t58506160* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Byte[]>::OnNext(T) */, IObserver_1_t2270505063_il2cpp_TypeInfo_var, L_36, L_38);
			Il2CppObject* L_39 = __this->get_observer_4();
			NullCheck(L_39);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Byte[]>::OnCompleted() */, IObserver_1_t2270505063_il2cpp_TypeInfo_var, L_39);
		}

IL_01d9:
		{
			IL2CPP_LEAVE(0x1F9, FINALLY_01de);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01de;
	}

FINALLY_01de:
	{ // begin finally (depth: 1)
		{
			bool L_40 = V_1;
			if (!L_40)
			{
				goto IL_01e2;
			}
		}

IL_01e1:
		{
			IL2CPP_END_FINALLY(478)
		}

IL_01e2:
		{
			WWW_t1522972100 * L_41 = __this->get_U3CU24s_344U3E__0_1();
			if (!L_41)
			{
				goto IL_01f8;
			}
		}

IL_01ed:
		{
			WWW_t1522972100 * L_42 = __this->get_U3CU24s_344U3E__0_1();
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_42);
		}

IL_01f8:
		{
			IL2CPP_END_FINALLY(478)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(478)
	{
		IL2CPP_JUMP_TBL(0x200, IL_0200)
		IL2CPP_JUMP_TBL(0x202, IL_0202)
		IL2CPP_JUMP_TBL(0x1F9, IL_01f9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01f9:
	{
		__this->set_U24PC_7((-1));
	}

IL_0200:
	{
		return (bool)0;
	}

IL_0202:
	{
		return (bool)1;
	}
	// Dead block : IL_0204: ldloc.s V_4
}
// System.Void UniRx.ObservableWWW/<FetchBytes>c__Iterator22::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchBytesU3Ec__Iterator22_Dispose_m2778274663_MetadataUsageId;
extern "C"  void U3CFetchBytesU3Ec__Iterator22_Dispose_m2778274663 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchBytesU3Ec__Iterator22_Dispose_m2778274663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0045;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0045;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x45, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			WWW_t1522972100 * L_2 = __this->get_U3CU24s_344U3E__0_1();
			if (!L_2)
			{
				goto IL_0044;
			}
		}

IL_0039:
		{
			WWW_t1522972100 * L_3 = __this->get_U3CU24s_344U3E__0_1();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
		}

IL_0044:
		{
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.ObservableWWW/<FetchBytes>c__Iterator22::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchBytesU3Ec__Iterator22_Reset_m2337692759_MetadataUsageId;
extern "C"  void U3CFetchBytesU3Ec__Iterator22_Reset_m2337692759 (U3CFetchBytesU3Ec__Iterator22_t157331456 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchBytesU3Ec__Iterator22_Reset_m2337692759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.ObservableWWW/<FetchText>c__Iterator21::.ctor()
extern "C"  void U3CFetchTextU3Ec__Iterator21__ctor_m3794028703 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.ObservableWWW/<FetchText>c__Iterator21::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchTextU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3473370141 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object UniRx.ObservableWWW/<FetchText>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchTextU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m1615284657 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Boolean UniRx.ObservableWWW/<FetchText>c__Iterator21::MoveNext()
extern Il2CppClass* IProgress_1_t4173802291_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t3180487805_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWErrorException_t3758994352_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchTextU3Ec__Iterator21_MoveNext_m4172672453_MetadataUsageId;
extern "C"  bool U3CFetchTextU3Ec__Iterator21_MoveNext_m4172672453 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchTextU3Ec__Iterator21_MoveNext_m4172672453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_003a;
		}
		if (L_1 == 2)
		{
			goto IL_003a;
		}
		if (L_1 == 3)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0200;
	}

IL_002b:
	{
		WWW_t1522972100 * L_2 = __this->get_www_0();
		__this->set_U3CU24s_343U3E__0_1(L_2);
		V_0 = ((int32_t)-3);
	}

IL_003a:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_3 = V_0;
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
			{
				goto IL_00b1;
			}
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
			{
				goto IL_0100;
			}
			if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
			{
				goto IL_013a;
			}
		}

IL_004e:
		{
			Il2CppObject* L_4 = __this->get_reportProgress_2();
			if (!L_4)
			{
				goto IL_00d6;
			}
		}

IL_0059:
		{
			goto IL_00b1;
		}

IL_005e:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = __this->get_reportProgress_2();
			WWW_t1522972100 * L_6 = __this->get_www_0();
			NullCheck(L_6);
			float L_7 = WWW_get_progress_m3186358572(L_6, /*hidden argument*/NULL);
			NullCheck(L_5);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, L_5, L_7);
			goto IL_009c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0079;
			throw e;
		}

CATCH_0079:
		{ // begin catch(System.Exception)
			{
				V_2 = ((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_8 = V_2;
				__this->set_U3CexU3E__1_5(L_8);
				Il2CppObject* L_9 = __this->get_observer_4();
				Exception_t1967233988 * L_10 = __this->get_U3CexU3E__1_5();
				NullCheck(L_9);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.String>::OnError(System.Exception) */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_9, L_10);
				IL2CPP_LEAVE(0x200, FINALLY_01de);
			}

IL_0097:
			{
				; // IL_0097: leave IL_009c
			}
		} // end catch (depth: 2)

IL_009c:
		{
			__this->set_U24current_8(NULL);
			__this->set_U24PC_7(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_00b1:
		{
			WWW_t1522972100 * L_11 = __this->get_www_0();
			NullCheck(L_11);
			bool L_12 = WWW_get_isDone_m634060017(L_11, /*hidden argument*/NULL);
			if (L_12)
			{
				goto IL_00d1;
			}
		}

IL_00c1:
		{
			CancellationToken_t1439151560 * L_13 = __this->get_address_of_cancel_3();
			bool L_14 = CancellationToken_get_IsCancellationRequested_m1021497867(L_13, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_005e;
			}
		}

IL_00d1:
		{
			goto IL_0100;
		}

IL_00d6:
		{
			WWW_t1522972100 * L_15 = __this->get_www_0();
			NullCheck(L_15);
			bool L_16 = WWW_get_isDone_m634060017(L_15, /*hidden argument*/NULL);
			if (L_16)
			{
				goto IL_0100;
			}
		}

IL_00e6:
		{
			WWW_t1522972100 * L_17 = __this->get_www_0();
			__this->set_U24current_8(L_17);
			__this->set_U24PC_7(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_0100:
		{
			CancellationToken_t1439151560 * L_18 = __this->get_address_of_cancel_3();
			bool L_19 = CancellationToken_get_IsCancellationRequested_m1021497867(L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_013f;
			}
		}

IL_0110:
		{
			WWW_t1522972100 * L_20 = __this->get_www_0();
			NullCheck(L_20);
			bool L_21 = WWW_get_isDone_m634060017(L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_013a;
			}
		}

IL_0120:
		{
			WWW_t1522972100 * L_22 = __this->get_www_0();
			__this->set_U24current_8(L_22);
			__this->set_U24PC_7(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x202, FINALLY_01de);
		}

IL_013a:
		{
			IL2CPP_LEAVE(0x200, FINALLY_01de);
		}

IL_013f:
		{
			Il2CppObject* L_23 = __this->get_reportProgress_2();
			if (!L_23)
			{
				goto IL_0188;
			}
		}

IL_014a:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_24 = __this->get_reportProgress_2();
			WWW_t1522972100 * L_25 = __this->get_www_0();
			NullCheck(L_25);
			float L_26 = WWW_get_progress_m3186358572(L_25, /*hidden argument*/NULL);
			NullCheck(L_24);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, L_24, L_26);
			goto IL_0188;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0165;
			throw e;
		}

CATCH_0165:
		{ // begin catch(System.Exception)
			{
				V_3 = ((Exception_t1967233988 *)__exception_local);
				Exception_t1967233988 * L_27 = V_3;
				__this->set_U3CexU3E__2_6(L_27);
				Il2CppObject* L_28 = __this->get_observer_4();
				Exception_t1967233988 * L_29 = __this->get_U3CexU3E__2_6();
				NullCheck(L_28);
				InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.String>::OnError(System.Exception) */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_28, L_29);
				IL2CPP_LEAVE(0x200, FINALLY_01de);
			}

IL_0183:
			{
				; // IL_0183: leave IL_0188
			}
		} // end catch (depth: 2)

IL_0188:
		{
			WWW_t1522972100 * L_30 = __this->get_www_0();
			NullCheck(L_30);
			String_t* L_31 = WWW_get_error_m1787423074(L_30, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_32 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
			if (L_32)
			{
				goto IL_01b8;
			}
		}

IL_019d:
		{
			Il2CppObject* L_33 = __this->get_observer_4();
			WWW_t1522972100 * L_34 = __this->get_www_0();
			WWWErrorException_t3758994352 * L_35 = (WWWErrorException_t3758994352 *)il2cpp_codegen_object_new(WWWErrorException_t3758994352_il2cpp_TypeInfo_var);
			WWWErrorException__ctor_m2195240225(L_35, L_34, /*hidden argument*/NULL);
			NullCheck(L_33);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.String>::OnError(System.Exception) */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_33, L_35);
			goto IL_01d9;
		}

IL_01b8:
		{
			Il2CppObject* L_36 = __this->get_observer_4();
			WWW_t1522972100 * L_37 = __this->get_www_0();
			NullCheck(L_37);
			String_t* L_38 = WWW_get_text_m4216049525(L_37, /*hidden argument*/NULL);
			NullCheck(L_36);
			InterfaceActionInvoker1< String_t* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.String>::OnNext(T) */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_36, L_38);
			Il2CppObject* L_39 = __this->get_observer_4();
			NullCheck(L_39);
			InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.String>::OnCompleted() */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_39);
		}

IL_01d9:
		{
			IL2CPP_LEAVE(0x1F9, FINALLY_01de);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01de;
	}

FINALLY_01de:
	{ // begin finally (depth: 1)
		{
			bool L_40 = V_1;
			if (!L_40)
			{
				goto IL_01e2;
			}
		}

IL_01e1:
		{
			IL2CPP_END_FINALLY(478)
		}

IL_01e2:
		{
			WWW_t1522972100 * L_41 = __this->get_U3CU24s_343U3E__0_1();
			if (!L_41)
			{
				goto IL_01f8;
			}
		}

IL_01ed:
		{
			WWW_t1522972100 * L_42 = __this->get_U3CU24s_343U3E__0_1();
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_42);
		}

IL_01f8:
		{
			IL2CPP_END_FINALLY(478)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(478)
	{
		IL2CPP_JUMP_TBL(0x200, IL_0200)
		IL2CPP_JUMP_TBL(0x202, IL_0202)
		IL2CPP_JUMP_TBL(0x1F9, IL_01f9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01f9:
	{
		__this->set_U24PC_7((-1));
	}

IL_0200:
	{
		return (bool)0;
	}

IL_0202:
	{
		return (bool)1;
	}
	// Dead block : IL_0204: ldloc.s V_4
}
// System.Void UniRx.ObservableWWW/<FetchText>c__Iterator21::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchTextU3Ec__Iterator21_Dispose_m3827599644_MetadataUsageId;
extern "C"  void U3CFetchTextU3Ec__Iterator21_Dispose_m3827599644 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchTextU3Ec__Iterator21_Dispose_m3827599644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0045;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0045;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x45, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			WWW_t1522972100 * L_2 = __this->get_U3CU24s_343U3E__0_1();
			if (!L_2)
			{
				goto IL_0044;
			}
		}

IL_0039:
		{
			WWW_t1522972100 * L_3 = __this->get_U3CU24s_343U3E__0_1();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
		}

IL_0044:
		{
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		return;
	}
}
// System.Void UniRx.ObservableWWW/<FetchText>c__Iterator21::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchTextU3Ec__Iterator21_Reset_m1440461644_MetadataUsageId;
extern "C"  void U3CFetchTextU3Ec__Iterator21_Reset_m1440461644 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchTextU3Ec__Iterator21_Reset_m1440461644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.ObservableWWW/<Get>c__AnonStorey7E::.ctor()
extern "C"  void U3CGetU3Ec__AnonStorey7E__ctor_m3918657003 (U3CGetU3Ec__AnonStorey7E_t497370161 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<Get>c__AnonStorey7E::<>m__AA(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const uint32_t U3CGetU3Ec__AnonStorey7E_U3CU3Em__AA_m1036982784_MetadataUsageId;
extern "C"  Il2CppObject * U3CGetU3Ec__AnonStorey7E_U3CU3Em__AA_m1036982784 (U3CGetU3Ec__AnonStorey7E_t497370161 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetU3Ec__AnonStorey7E_U3CU3Em__AA_m1036982784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2606186806 * G_B2_0 = NULL;
	Il2CppObject * G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	Dictionary_2_t2606186806 * G_B1_0 = NULL;
	Il2CppObject * G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	{
		String_t* L_0 = __this->get_url_0();
		Dictionary_2_t2606186806 * L_1 = __this->get_headers_1();
		Dictionary_2_t2606186806 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = NULL;
		G_B1_2 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = NULL;
			G_B2_2 = L_0;
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t2606186806 * L_3 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_3, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0019:
	{
		WWW_t1522972100 * L_4 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_4, G_B2_2, (ByteU5BU5D_t58506160*)(ByteU5BU5D_t58506160*)G_B2_1, G_B2_0, /*hidden argument*/NULL);
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject* L_6 = __this->get_progress_2();
		CancellationToken_t1439151560  L_7 = ___cancellation1;
		Il2CppObject * L_8 = ObservableWWW_FetchText_m220208830(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey7F::.ctor()
extern "C"  void U3CGetAndGetBytesU3Ec__AnonStorey7F__ctor_m4018533664 (U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey7F::<>m__AB(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const uint32_t U3CGetAndGetBytesU3Ec__AnonStorey7F_U3CU3Em__AB_m1487153967_MetadataUsageId;
extern "C"  Il2CppObject * U3CGetAndGetBytesU3Ec__AnonStorey7F_U3CU3Em__AB_m1487153967 (U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAndGetBytesU3Ec__AnonStorey7F_U3CU3Em__AB_m1487153967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2606186806 * G_B2_0 = NULL;
	Il2CppObject * G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	Dictionary_2_t2606186806 * G_B1_0 = NULL;
	Il2CppObject * G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	{
		String_t* L_0 = __this->get_url_0();
		Dictionary_2_t2606186806 * L_1 = __this->get_headers_1();
		Dictionary_2_t2606186806 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = NULL;
		G_B1_2 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = NULL;
			G_B2_2 = L_0;
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t2606186806 * L_3 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_3, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0019:
	{
		WWW_t1522972100 * L_4 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_4, G_B2_2, (ByteU5BU5D_t58506160*)(ByteU5BU5D_t58506160*)G_B2_1, G_B2_0, /*hidden argument*/NULL);
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject* L_6 = __this->get_progress_2();
		CancellationToken_t1439151560  L_7 = ___cancellation1;
		Il2CppObject * L_8 = ObservableWWW_FetchBytes_m789535179(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UniRx.ObservableWWW/<GetWWW>c__AnonStorey80::.ctor()
extern "C"  void U3CGetWWWU3Ec__AnonStorey80__ctor_m1478189740 (U3CGetWWWU3Ec__AnonStorey80_t1804159614 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<GetWWW>c__AnonStorey80::<>m__AC(UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken)
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const uint32_t U3CGetWWWU3Ec__AnonStorey80_U3CU3Em__AC_m4003425075_MetadataUsageId;
extern "C"  Il2CppObject * U3CGetWWWU3Ec__AnonStorey80_U3CU3Em__AC_m4003425075 (U3CGetWWWU3Ec__AnonStorey80_t1804159614 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetWWWU3Ec__AnonStorey80_U3CU3Em__AC_m4003425075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2606186806 * G_B2_0 = NULL;
	Il2CppObject * G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	Dictionary_2_t2606186806 * G_B1_0 = NULL;
	Il2CppObject * G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	{
		String_t* L_0 = __this->get_url_0();
		Dictionary_2_t2606186806 * L_1 = __this->get_headers_1();
		Dictionary_2_t2606186806 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = NULL;
		G_B1_2 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = NULL;
			G_B2_2 = L_0;
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t2606186806 * L_3 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_3, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0019:
	{
		WWW_t1522972100 * L_4 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_4, G_B2_2, (ByteU5BU5D_t58506160*)(ByteU5BU5D_t58506160*)G_B2_1, G_B2_0, /*hidden argument*/NULL);
		Il2CppObject* L_5 = ___observer0;
		Il2CppObject* L_6 = __this->get_progress_2();
		CancellationToken_t1439151560  L_7 = ___cancellation1;
		Il2CppObject * L_8 = ObservableWWW_Fetch_m4100415835(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8D::.ctor()
extern "C"  void U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D__ctor_m2740786022 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8D::<>m__B9(UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_U3CU3Em__B9_m4015006557 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8D_t1059502550 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_url_0();
		int32_t L_1 = __this->get_version_1();
		WWW_t1522972100 * L_2 = WWW_LoadFromCacheOrDownload_m197364101(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject* L_4 = __this->get_progress_2();
		CancellationToken_t1439151560  L_5 = ___cancellation1;
		Il2CppObject * L_6 = ObservableWWW_FetchAssetBundle_m2288014994(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8E::.ctor()
extern "C"  void U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E__ctor_m2544272517 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8E::<>m__BA(UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_U3CU3Em__BA_m2464018692 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8E_t1059502551 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_url_0();
		int32_t L_1 = __this->get_version_1();
		uint32_t L_2 = __this->get_crc_2();
		WWW_t1522972100 * L_3 = WWW_LoadFromCacheOrDownload_m101644025(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		Il2CppObject* L_4 = ___observer0;
		Il2CppObject* L_5 = __this->get_progress_3();
		CancellationToken_t1439151560  L_6 = ___cancellation1;
		Il2CppObject * L_7 = ObservableWWW_FetchAssetBundle_m2288014994(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8F::.ctor()
extern "C"  void U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F__ctor_m2347759012 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey8F::<>m__BB(UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_U3CU3Em__BB_m2637222436 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey8F_t1059502552 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_url_0();
		Hash128_t3885020822  L_1 = __this->get_hash128_1();
		WWW_t1522972100 * L_2 = WWW_LoadFromCacheOrDownload_m2986731820(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject* L_4 = __this->get_progress_2();
		CancellationToken_t1439151560  L_5 = ___cancellation1;
		Il2CppObject * L_6 = ObservableWWW_FetchAssetBundle_m2288014994(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90::.ctor()
extern "C"  void U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90__ctor_m579137467 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90::<>m__BC(UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_U3CU3Em__BC_m1871593532 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_url_0();
		Hash128_t3885020822  L_1 = __this->get_hash128_1();
		uint32_t L_2 = __this->get_crc_2();
		WWW_t1522972100 * L_3 = WWW_LoadFromCacheOrDownload_m2606851680(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		Il2CppObject* L_4 = ___observer0;
		Il2CppObject* L_5 = __this->get_progress_3();
		CancellationToken_t1439151560  L_6 = ___cancellation1;
		Il2CppObject * L_7 = ObservableWWW_FetchAssetBundle_m2288014994(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.ObservableWWW/<Post>c__AnonStorey81::.ctor()
extern "C"  void U3CPostU3Ec__AnonStorey81__ctor_m3231068460 (U3CPostU3Ec__AnonStorey81_t213725630 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<Post>c__AnonStorey81::<>m__AD(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostU3Ec__AnonStorey81_U3CU3Em__AD_m2809010340_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostU3Ec__AnonStorey81_U3CU3Em__AD_m2809010340 (U3CPostU3Ec__AnonStorey81_t213725630 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostU3Ec__AnonStorey81_U3CU3Em__AD_m2809010340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		ByteU5BU5D_t58506160* L_1 = __this->get_postData_1();
		WWW_t1522972100 * L_2 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m2485385635(L_2, L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject* L_4 = __this->get_progress_2();
		CancellationToken_t1439151560  L_5 = ___cancellation1;
		Il2CppObject * L_6 = ObservableWWW_FetchText_m220208830(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.ObservableWWW/<Post>c__AnonStorey82::.ctor()
extern "C"  void U3CPostU3Ec__AnonStorey82__ctor_m3034554955 (U3CPostU3Ec__AnonStorey82_t213725631 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<Post>c__AnonStorey82::<>m__AE(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostU3Ec__AnonStorey82_U3CU3Em__AE_m1418846660_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostU3Ec__AnonStorey82_U3CU3Em__AE_m1418846660 (U3CPostU3Ec__AnonStorey82_t213725631 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostU3Ec__AnonStorey82_U3CU3Em__AE_m1418846660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		ByteU5BU5D_t58506160* L_1 = __this->get_postData_1();
		Dictionary_2_t2606186806 * L_2 = __this->get_headers_2();
		WWW_t1522972100 * L_3 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Il2CppObject* L_4 = ___observer0;
		Il2CppObject* L_5 = __this->get_progress_3();
		CancellationToken_t1439151560  L_6 = ___cancellation1;
		Il2CppObject * L_7 = ObservableWWW_FetchText_m220208830(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.ObservableWWW/<Post>c__AnonStorey83::.ctor()
extern "C"  void U3CPostU3Ec__AnonStorey83__ctor_m2838041450 (U3CPostU3Ec__AnonStorey83_t213725632 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<Post>c__AnonStorey83::<>m__AF(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostU3Ec__AnonStorey83_U3CU3Em__AF_m28682980_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostU3Ec__AnonStorey83_U3CU3Em__AF_m28682980 (U3CPostU3Ec__AnonStorey83_t213725632 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostU3Ec__AnonStorey83_U3CU3Em__AF_m28682980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		WWWForm_t3999572776 * L_1 = __this->get_content_1();
		WWW_t1522972100 * L_2 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m3203953640(L_2, L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject* L_4 = __this->get_progress_2();
		CancellationToken_t1439151560  L_5 = ___cancellation1;
		Il2CppObject * L_6 = ObservableWWW_FetchText_m220208830(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.ObservableWWW/<Post>c__AnonStorey84::.ctor()
extern "C"  void U3CPostU3Ec__AnonStorey84__ctor_m2641527945 (U3CPostU3Ec__AnonStorey84_t213725633 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<Post>c__AnonStorey84::<>m__B0(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostU3Ec__AnonStorey84_U3CU3Em__B0_m881318412_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostU3Ec__AnonStorey84_U3CU3Em__B0_m881318412 (U3CPostU3Ec__AnonStorey84_t213725633 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostU3Ec__AnonStorey84_U3CU3Em__B0_m881318412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		WWWForm_t3999572776 * L_1 = __this->get_content_1();
		NullCheck(L_1);
		ByteU5BU5D_t58506160* L_2 = WWWForm_get_data_m2893811951(L_1, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_3 = __this->get_contentHeaders_2();
		Dictionary_2_t2606186806 * L_4 = __this->get_headers_3();
		Dictionary_2_t2606186806 * L_5 = ObservableWWW_MergeHash_m4254025137(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		WWW_t1522972100 * L_6 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_6, L_0, L_2, L_5, /*hidden argument*/NULL);
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject* L_8 = __this->get_progress_4();
		CancellationToken_t1439151560  L_9 = ___cancellation1;
		Il2CppObject * L_10 = ObservableWWW_FetchText_m220208830(NULL /*static, unused*/, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey85::.ctor()
extern "C"  void U3CPostAndGetBytesU3Ec__AnonStorey85__ctor_m78346854 (U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey85::<>m__B1(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostAndGetBytesU3Ec__AnonStorey85_U3CU3Em__B1_m247104291_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostAndGetBytesU3Ec__AnonStorey85_U3CU3Em__B1_m247104291 (U3CPostAndGetBytesU3Ec__AnonStorey85_t3957496790 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostAndGetBytesU3Ec__AnonStorey85_U3CU3Em__B1_m247104291_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		ByteU5BU5D_t58506160* L_1 = __this->get_postData_1();
		WWW_t1522972100 * L_2 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m2485385635(L_2, L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject* L_4 = __this->get_progress_2();
		CancellationToken_t1439151560  L_5 = ___cancellation1;
		Il2CppObject * L_6 = ObservableWWW_FetchBytes_m789535179(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey86::.ctor()
extern "C"  void U3CPostAndGetBytesU3Ec__AnonStorey86__ctor_m4176800645 (U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey86::<>m__B2(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostAndGetBytesU3Ec__AnonStorey86_U3CU3Em__B2_m3151907907_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostAndGetBytesU3Ec__AnonStorey86_U3CU3Em__B2_m3151907907 (U3CPostAndGetBytesU3Ec__AnonStorey86_t3957496791 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostAndGetBytesU3Ec__AnonStorey86_U3CU3Em__B2_m3151907907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		ByteU5BU5D_t58506160* L_1 = __this->get_postData_1();
		Dictionary_2_t2606186806 * L_2 = __this->get_headers_2();
		WWW_t1522972100 * L_3 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Il2CppObject* L_4 = ___observer0;
		Il2CppObject* L_5 = __this->get_progress_3();
		CancellationToken_t1439151560  L_6 = ___cancellation1;
		Il2CppObject * L_7 = ObservableWWW_FetchBytes_m789535179(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey87::.ctor()
extern "C"  void U3CPostAndGetBytesU3Ec__AnonStorey87__ctor_m3980287140 (U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey87::<>m__B3(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostAndGetBytesU3Ec__AnonStorey87_U3CU3Em__B3_m1761744227_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostAndGetBytesU3Ec__AnonStorey87_U3CU3Em__B3_m1761744227 (U3CPostAndGetBytesU3Ec__AnonStorey87_t3957496792 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostAndGetBytesU3Ec__AnonStorey87_U3CU3Em__B3_m1761744227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		WWWForm_t3999572776 * L_1 = __this->get_content_1();
		WWW_t1522972100 * L_2 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m3203953640(L_2, L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject* L_4 = __this->get_progress_2();
		CancellationToken_t1439151560  L_5 = ___cancellation1;
		Il2CppObject * L_6 = ObservableWWW_FetchBytes_m789535179(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey88::.ctor()
extern "C"  void U3CPostAndGetBytesU3Ec__AnonStorey88__ctor_m3783773635 (U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStorey88::<>m__B4(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostAndGetBytesU3Ec__AnonStorey88_U3CU3Em__B4_m371580547_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostAndGetBytesU3Ec__AnonStorey88_U3CU3Em__B4_m371580547 (U3CPostAndGetBytesU3Ec__AnonStorey88_t3957496793 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostAndGetBytesU3Ec__AnonStorey88_U3CU3Em__B4_m371580547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		WWWForm_t3999572776 * L_1 = __this->get_content_1();
		NullCheck(L_1);
		ByteU5BU5D_t58506160* L_2 = WWWForm_get_data_m2893811951(L_1, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_3 = __this->get_contentHeaders_2();
		Dictionary_2_t2606186806 * L_4 = __this->get_headers_3();
		Dictionary_2_t2606186806 * L_5 = ObservableWWW_MergeHash_m4254025137(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		WWW_t1522972100 * L_6 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_6, L_0, L_2, L_5, /*hidden argument*/NULL);
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject* L_8 = __this->get_progress_4();
		CancellationToken_t1439151560  L_9 = ___cancellation1;
		Il2CppObject * L_10 = ObservableWWW_FetchBytes_m789535179(NULL /*static, unused*/, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.ObservableWWW/<PostWWW>c__AnonStorey89::.ctor()
extern "C"  void U3CPostWWWU3Ec__AnonStorey89__ctor_m1685751383 (U3CPostWWWU3Ec__AnonStorey89_t4245247429 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostWWW>c__AnonStorey89::<>m__B5(UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostWWWU3Ec__AnonStorey89_U3CU3Em__B5_m3080128655_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostWWWU3Ec__AnonStorey89_U3CU3Em__B5_m3080128655 (U3CPostWWWU3Ec__AnonStorey89_t4245247429 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostWWWU3Ec__AnonStorey89_U3CU3Em__B5_m3080128655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		ByteU5BU5D_t58506160* L_1 = __this->get_postData_1();
		WWW_t1522972100 * L_2 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m2485385635(L_2, L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject* L_4 = __this->get_progress_2();
		CancellationToken_t1439151560  L_5 = ___cancellation1;
		Il2CppObject * L_6 = ObservableWWW_Fetch_m4100415835(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.ObservableWWW/<PostWWW>c__AnonStorey8A::.ctor()
extern "C"  void U3CPostWWWU3Ec__AnonStorey8A__ctor_m113643343 (U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostWWW>c__AnonStorey8A::<>m__B6(UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostWWWU3Ec__AnonStorey8A_U3CU3Em__B6_m957328904_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostWWWU3Ec__AnonStorey8A_U3CU3Em__B6_m957328904 (U3CPostWWWU3Ec__AnonStorey8A_t4245247437 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostWWWU3Ec__AnonStorey8A_U3CU3Em__B6_m957328904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		ByteU5BU5D_t58506160* L_1 = __this->get_postData_1();
		Dictionary_2_t2606186806 * L_2 = __this->get_headers_2();
		WWW_t1522972100 * L_3 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Il2CppObject* L_4 = ___observer0;
		Il2CppObject* L_5 = __this->get_progress_3();
		CancellationToken_t1439151560  L_6 = ___cancellation1;
		Il2CppObject * L_7 = ObservableWWW_Fetch_m4100415835(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.ObservableWWW/<PostWWW>c__AnonStorey8B::.ctor()
extern "C"  void U3CPostWWWU3Ec__AnonStorey8B__ctor_m4212097134 (U3CPostWWWU3Ec__AnonStorey8B_t4245247438 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostWWW>c__AnonStorey8B::<>m__B7(UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostWWWU3Ec__AnonStorey8B_U3CU3Em__B7_m744861480_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostWWWU3Ec__AnonStorey8B_U3CU3Em__B7_m744861480 (U3CPostWWWU3Ec__AnonStorey8B_t4245247438 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostWWWU3Ec__AnonStorey8B_U3CU3Em__B7_m744861480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		WWWForm_t3999572776 * L_1 = __this->get_content_1();
		WWW_t1522972100 * L_2 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m3203953640(L_2, L_0, L_1, /*hidden argument*/NULL);
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject* L_4 = __this->get_progress_2();
		CancellationToken_t1439151560  L_5 = ___cancellation1;
		Il2CppObject * L_6 = ObservableWWW_Fetch_m4100415835(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.ObservableWWW/<PostWWW>c__AnonStorey8C::.ctor()
extern "C"  void U3CPostWWWU3Ec__AnonStorey8C__ctor_m4015583629 (U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.ObservableWWW/<PostWWW>c__AnonStorey8C::<>m__B8(UniRx.IObserver`1<UnityEngine.WWW>,UniRx.CancellationToken)
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const uint32_t U3CPostWWWU3Ec__AnonStorey8C_U3CU3Em__B8_m532394056_MetadataUsageId;
extern "C"  Il2CppObject * U3CPostWWWU3Ec__AnonStorey8C_U3CU3Em__B8_m532394056 (U3CPostWWWU3Ec__AnonStorey8C_t4245247439 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPostWWWU3Ec__AnonStorey8C_U3CU3Em__B8_m532394056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_url_0();
		WWWForm_t3999572776 * L_1 = __this->get_content_1();
		NullCheck(L_1);
		ByteU5BU5D_t58506160* L_2 = WWWForm_get_data_m2893811951(L_1, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_3 = __this->get_contentHeaders_2();
		Dictionary_2_t2606186806 * L_4 = __this->get_headers_3();
		Dictionary_2_t2606186806 * L_5 = ObservableWWW_MergeHash_m4254025137(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		WWW_t1522972100 * L_6 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m4133031156(L_6, L_0, L_2, L_5, /*hidden argument*/NULL);
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject* L_8 = __this->get_progress_4();
		CancellationToken_t1439151560  L_9 = ___cancellation1;
		Il2CppObject * L_10 = ObservableWWW_Fetch_m4100415835(NULL /*static, unused*/, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UniRx.Operators.FromEventObservable::.ctor(System.Action`1<System.Action>,System.Action`1<System.Action>)
extern const MethodInfo* OperatorObservableBase_1__ctor_m284357152_MethodInfo_var;
extern const uint32_t FromEventObservable__ctor_m843502955_MetadataUsageId;
extern "C"  void FromEventObservable__ctor_m843502955 (FromEventObservable_t1591372576 * __this, Action_1_t585976652 * ___addHandler0, Action_1_t585976652 * ___removeHandler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable__ctor_m843502955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OperatorObservableBase_1__ctor_m284357152(__this, (bool)0, /*hidden argument*/OperatorObservableBase_1__ctor_m284357152_MethodInfo_var);
		Action_1_t585976652 * L_0 = ___addHandler0;
		__this->set_addHandler_1(L_0);
		Action_1_t585976652 * L_1 = ___removeHandler1;
		__this->set_removeHandler_2(L_1);
		return;
	}
}
// System.IDisposable UniRx.Operators.FromEventObservable::SubscribeCore(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern Il2CppClass* FromEvent_t2060698864_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t FromEventObservable_SubscribeCore_m3786309221_MetadataUsageId;
extern "C"  Il2CppObject * FromEventObservable_SubscribeCore_m3786309221 (FromEventObservable_t1591372576 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEventObservable_SubscribeCore_m3786309221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FromEvent_t2060698864 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject* L_0 = ___observer0;
		FromEvent_t2060698864 * L_1 = (FromEvent_t2060698864 *)il2cpp_codegen_object_new(FromEvent_t2060698864_il2cpp_TypeInfo_var);
		FromEvent__ctor_m2262758506(L_1, __this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FromEvent_t2060698864 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = FromEvent_Register_m31338865(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		FromEvent_t2060698864 * L_4 = V_0;
		V_1 = L_4;
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		G_B3_0 = L_6;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Void UniRx.Operators.FromEventObservable/FromEvent::.ctor(UniRx.Operators.FromEventObservable,UniRx.IObserver`1<UniRx.Unit>)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const MethodInfo* FromEvent_OnNext_m3700796636_MethodInfo_var;
extern const uint32_t FromEvent__ctor_m2262758506_MetadataUsageId;
extern "C"  void FromEvent__ctor_m2262758506 (FromEvent_t2060698864 * __this, FromEventObservable_t1591372576 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent__ctor_m2262758506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		FromEventObservable_t1591372576 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		Il2CppObject* L_1 = ___observer1;
		__this->set_observer_1(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)FromEvent_OnNext_m3700796636_MethodInfo_var);
		Action_t437523947 * L_3 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_3, __this, L_2, /*hidden argument*/NULL);
		__this->set_handler_2(L_3);
		return;
	}
}
// System.Boolean UniRx.Operators.FromEventObservable/FromEvent::Register()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3279850461_MethodInfo_var;
extern const uint32_t FromEvent_Register_m31338865_MetadataUsageId;
extern "C"  bool FromEvent_Register_m31338865 (FromEvent_t2060698864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Register_m31338865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FromEventObservable_t1591372576 * L_0 = __this->get_parent_0();
		NullCheck(L_0);
		Action_1_t585976652 * L_1 = L_0->get_addHandler_1();
		Action_t437523947 * L_2 = __this->get_handler_2();
		NullCheck(L_1);
		Action_1_Invoke_m3279850461(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3279850461_MethodInfo_var);
		goto IL_0034;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001b;
		throw e;
	}

CATCH_001b:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t1967233988 *)__exception_local);
			Il2CppObject* L_3 = __this->get_observer_1();
			Exception_t1967233988 * L_4 = V_0;
			NullCheck(L_3);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, L_3, L_4);
			V_1 = (bool)0;
			goto IL_0036;
		}

IL_002f:
		{
			; // IL_002f: leave IL_0034
		}
	} // end catch (depth: 1)

IL_0034:
	{
		return (bool)1;
	}

IL_0036:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Void UniRx.Operators.FromEventObservable/FromEvent::OnNext()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t475317645_il2cpp_TypeInfo_var;
extern const uint32_t FromEvent_OnNext_m3700796636_MetadataUsageId;
extern "C"  void FromEvent_OnNext_m3700796636 (FromEvent_t2060698864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_OnNext_m3700796636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = __this->get_observer_1();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_1 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IObserver_1_t475317645_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void UniRx.Operators.FromEventObservable/FromEvent::Dispose()
extern const MethodInfo* Action_1_Invoke_m3279850461_MethodInfo_var;
extern const uint32_t FromEvent_Dispose_m123127191_MetadataUsageId;
extern "C"  void FromEvent_Dispose_m123127191 (FromEvent_t2060698864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FromEvent_Dispose_m123127191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_handler_2();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		FromEventObservable_t1591372576 * L_1 = __this->get_parent_0();
		NullCheck(L_1);
		Action_1_t585976652 * L_2 = L_1->get_removeHandler_2();
		Action_t437523947 * L_3 = __this->get_handler_2();
		NullCheck(L_2);
		Action_1_Invoke_m3279850461(L_2, L_3, /*hidden argument*/Action_1_Invoke_m3279850461_MethodInfo_var);
		__this->set_handler_2((Action_t437523947 *)NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UniRx.Operators.RangeObservable::.ctor(System.Int32,System.Int32,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern const MethodInfo* OperatorObservableBase_1__ctor_m89273299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1350148795;
extern const uint32_t RangeObservable__ctor_m1043085892_MetadataUsageId;
extern "C"  void RangeObservable__ctor_m1043085892 (RangeObservable_t282483469 * __this, int32_t ___start0, int32_t ___count1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RangeObservable__ctor_m1043085892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		OperatorObservableBase_1__ctor_m89273299(__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/OperatorObservableBase_1__ctor_m89273299_MethodInfo_var);
		int32_t L_2 = ___count1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, _stringLiteral1350148795, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		int32_t L_4 = ___start0;
		__this->set_start_1(L_4);
		int32_t L_5 = ___count1;
		__this->set_count_2(L_5);
		Il2CppObject * L_6 = ___scheduler2;
		__this->set_scheduler_3(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.RangeObservable::SubscribeCore(UniRx.IObserver`1<System.Int32>,System.IDisposable)
extern Il2CppClass* U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636_il2cpp_TypeInfo_var;
extern Il2CppClass* Range_t78727453_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t585976652_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSubscribeCoreU3Ec__AnonStorey64_U3CU3Em__81_m1584142790_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3589476370_MethodInfo_var;
extern const uint32_t RangeObservable_SubscribeCore_m1549044613_MetadataUsageId;
extern "C"  Il2CppObject * RangeObservable_SubscribeCore_m1549044613 (RangeObservable_t282483469 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RangeObservable_SubscribeCore_m1549044613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * V_0 = NULL;
	{
		U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * L_0 = (U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 *)il2cpp_codegen_object_new(U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636_il2cpp_TypeInfo_var);
		U3CSubscribeCoreU3Ec__AnonStorey64__ctor_m1501246165(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_1(L_2);
		U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * L_4 = V_0;
		U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject* L_6 = L_5->get_observer_1();
		Il2CppObject * L_7 = ___cancel1;
		Range_t78727453 * L_8 = (Range_t78727453 *)il2cpp_codegen_object_new(Range_t78727453_il2cpp_TypeInfo_var);
		Range__ctor_m976364437(L_8, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_observer_1(L_8);
		U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_i_0(0);
		Il2CppObject * L_10 = __this->get_scheduler_3();
		U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)U3CSubscribeCoreU3Ec__AnonStorey64_U3CU3Em__81_m1584142790_MethodInfo_var);
		Action_1_t585976652 * L_13 = (Action_1_t585976652 *)il2cpp_codegen_object_new(Action_1_t585976652_il2cpp_TypeInfo_var);
		Action_1__ctor_m3589476370(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m3589476370_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_14 = Scheduler_Schedule_m2919836901(NULL /*static, unused*/, L_10, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey64__ctor_m1501246165 (U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64::<>m__81(System.Action)
extern Il2CppClass* IObserver_1_t764446394_il2cpp_TypeInfo_var;
extern const uint32_t U3CSubscribeCoreU3Ec__AnonStorey64_U3CU3Em__81_m1584142790_MetadataUsageId;
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey64_U3CU3Em__81_m1584142790 (U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * __this, Action_t437523947 * ___self0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSubscribeCoreU3Ec__AnonStorey64_U3CU3Em__81_m1584142790_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_i_0();
		RangeObservable_t282483469 * L_1 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_count_2();
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_004e;
		}
	}
	{
		RangeObservable_t282483469 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_start_1();
		int32_t L_5 = __this->get_i_0();
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)L_5));
		Il2CppObject* L_6 = __this->get_observer_1();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IObserver_1_t764446394_il2cpp_TypeInfo_var, L_6, L_7);
		int32_t L_8 = __this->get_i_0();
		__this->set_i_0(((int32_t)((int32_t)L_8+(int32_t)1)));
		Action_t437523947 * L_9 = ___self0;
		NullCheck(L_9);
		Action_Invoke_m1445970038(L_9, /*hidden argument*/NULL);
		goto IL_0059;
	}

IL_004e:
	{
		Il2CppObject* L_10 = __this->get_observer_1();
		NullCheck(L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IObserver_1_t764446394_il2cpp_TypeInfo_var, L_10);
	}

IL_0059:
	{
		return;
	}
}
// System.Void UniRx.Operators.RangeObservable/Range::.ctor(UniRx.IObserver`1<System.Int32>,System.IDisposable)
extern const MethodInfo* OperatorObserverBase_2__ctor_m3978315088_MethodInfo_var;
extern const uint32_t Range__ctor_m976364437_MetadataUsageId;
extern "C"  void Range__ctor_m976364437 (Range_t78727453 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Range__ctor_m976364437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		OperatorObserverBase_2__ctor_m3978315088(__this, L_0, L_1, /*hidden argument*/OperatorObserverBase_2__ctor_m3978315088_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Operators.RangeObservable/Range::OnNext(System.Int32)
extern Il2CppClass* IObserver_1_t764446394_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Range_OnNext_m3042627079_MetadataUsageId;
extern "C"  void Range_OnNext_m3042627079 (Range_t78727453 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Range_OnNext_m3042627079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = ((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int32_t L_1 = ___value0;
		NullCheck(L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IObserver_1_t764446394_il2cpp_TypeInfo_var, L_0, L_1);
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		{
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, __this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_001c:
		{
			goto IL_0021;
		}
	} // end catch (depth: 1)

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Operators.RangeObservable/Range::OnError(System.Exception)
extern Il2CppClass* IObserver_1_t764446394_il2cpp_TypeInfo_var;
extern const uint32_t Range_OnError_m2670562679_MetadataUsageId;
extern "C"  void Range_OnError_m2670562679 (Range_t78727453 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Range_OnError_m2670562679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = ((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IObserver_1_t764446394_il2cpp_TypeInfo_var, L_0, L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, __this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.RangeObservable/Range::OnCompleted()
extern Il2CppClass* IObserver_1_t764446394_il2cpp_TypeInfo_var;
extern const uint32_t Range_OnCompleted_m151223114_MetadataUsageId;
extern "C"  void Range_OnCompleted_m151223114 (Range_t78727453 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Range_OnCompleted_m151223114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = ((OperatorObserverBase_2_t701568569 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IObserver_1_t764446394_il2cpp_TypeInfo_var, L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose() */, __this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable::.ctor(System.DateTimeOffset,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* OperatorObservableBase_1__ctor_m322881330_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m522950596_MethodInfo_var;
extern const uint32_t TimerObservable__ctor_m2474548840_MetadataUsageId;
extern "C"  void TimerObservable__ctor_m2474548840 (TimerObservable_t1697791765 * __this, DateTimeOffset_t3712260035  ___dueTime0, Nullable_1_t3649900800  ___period1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimerObservable__ctor_m2474548840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		OperatorObservableBase_1__ctor_m322881330(__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/OperatorObservableBase_1__ctor_m322881330_MethodInfo_var);
		DateTimeOffset_t3712260035  L_2 = ___dueTime0;
		Nullable_1_t2303330647  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m522950596(&L_3, L_2, /*hidden argument*/Nullable_1__ctor_m522950596_MethodInfo_var);
		__this->set_dueTimeA_1(L_3);
		Nullable_1_t3649900800  L_4 = ___period1;
		__this->set_period_3(L_4);
		Il2CppObject * L_5 = ___scheduler2;
		__this->set_scheduler_4(L_5);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable::.ctor(System.TimeSpan,System.Nullable`1<System.TimeSpan>,UniRx.IScheduler)
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const MethodInfo* OperatorObservableBase_1__ctor_m322881330_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4008503583_MethodInfo_var;
extern const uint32_t TimerObservable__ctor_m1563113823_MetadataUsageId;
extern "C"  void TimerObservable__ctor_m1563113823 (TimerObservable_t1697791765 * __this, TimeSpan_t763862892  ___dueTime0, Nullable_1_t3649900800  ___period1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimerObservable__ctor_m1563113823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		OperatorObservableBase_1__ctor_m322881330(__this, (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))? 1 : 0), /*hidden argument*/OperatorObservableBase_1__ctor_m322881330_MethodInfo_var);
		TimeSpan_t763862892  L_2 = ___dueTime0;
		Nullable_1_t3649900800  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m4008503583(&L_3, L_2, /*hidden argument*/Nullable_1__ctor_m4008503583_MethodInfo_var);
		__this->set_dueTimeB_2(L_3);
		Nullable_1_t3649900800  L_4 = ___period1;
		__this->set_period_3(L_4);
		Il2CppObject * L_5 = ___scheduler2;
		__this->set_scheduler_4(L_5);
		return;
	}
}
// System.IDisposable UniRx.Operators.TimerObservable::SubscribeCore(UniRx.IObserver`1<System.Int64>,System.IDisposable)
extern Il2CppClass* U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652_il2cpp_TypeInfo_var;
extern Il2CppClass* Timer_t80811813_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeOffset_t3712260035_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653_il2cpp_TypeInfo_var;
extern Il2CppClass* ISchedulerPeriodic_t2870089311_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654_il2cpp_TypeInfo_var;
extern Il2CppClass* SerialDisposable_t2547852742_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1060768302_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2074669485_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m4125167011_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3338249190_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2797118855_MethodInfo_var;
extern const MethodInfo* U3CSubscribeCoreU3Ec__AnonStorey6D_U3CU3Em__8B_m259250400_MethodInfo_var;
extern const MethodInfo* Timer_OnNext_m2727522758_MethodInfo_var;
extern const MethodInfo* U3CSubscribeCoreU3Ec__AnonStorey6F_U3CU3Em__8C_m517416799_MethodInfo_var;
extern const MethodInfo* U3CSubscribeCoreU3Ec__AnonStorey70_U3CU3Em__8D_m809751865_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3957693621_MethodInfo_var;
extern const uint32_t TimerObservable_SubscribeCore_m32394110_MetadataUsageId;
extern "C"  Il2CppObject * TimerObservable_SubscribeCore_m32394110 (TimerObservable_t1697791765 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimerObservable_SubscribeCore_m32394110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * V_1 = NULL;
	Nullable_1_t2303330647  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Nullable_1_t2303330647  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Nullable_1_t3649900800  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Nullable_1_t3649900800  V_5;
	memset(&V_5, 0, sizeof(V_5));
	U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * V_6 = NULL;
	Nullable_1_t3649900800  V_7;
	memset(&V_7, 0, sizeof(V_7));
	U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * V_8 = NULL;
	U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * V_9 = NULL;
	Nullable_1_t3649900800  V_10;
	memset(&V_10, 0, sizeof(V_10));
	TimeSpan_t763862892  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_0 = (U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 *)il2cpp_codegen_object_new(U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652_il2cpp_TypeInfo_var);
		U3CSubscribeCoreU3Ec__AnonStorey6D__ctor_m1186693325(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_2 = V_1;
		Il2CppObject* L_3 = ___observer0;
		Il2CppObject * L_4 = ___cancel1;
		Timer_t80811813 * L_5 = (Timer_t80811813 *)il2cpp_codegen_object_new(Timer_t80811813_il2cpp_TypeInfo_var);
		Timer__ctor_m970264198(L_5, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_timerObserver_0(L_5);
		Nullable_1_t2303330647  L_6 = __this->get_dueTimeA_1();
		V_2 = L_6;
		bool L_7 = Nullable_1_get_HasValue_m2074669485((&V_2), /*hidden argument*/Nullable_1_get_HasValue_m2074669485_MethodInfo_var);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		Nullable_1_t2303330647  L_8 = __this->get_dueTimeA_1();
		V_3 = L_8;
		DateTimeOffset_t3712260035  L_9 = Nullable_1_get_Value_m4125167011((&V_3), /*hidden argument*/Nullable_1_get_Value_m4125167011_MethodInfo_var);
		Il2CppObject * L_10 = __this->get_scheduler_4();
		NullCheck(L_10);
		DateTimeOffset_t3712260035  L_11 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeOffset_t3712260035_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_12 = DateTimeOffset_op_Subtraction_m3815865550(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		G_B3_0 = L_12;
		goto IL_005f;
	}

IL_0050:
	{
		Nullable_1_t3649900800  L_13 = __this->get_dueTimeB_2();
		V_4 = L_13;
		TimeSpan_t763862892  L_14 = Nullable_1_get_Value_m3338249190((&V_4), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		G_B3_0 = L_14;
	}

IL_005f:
	{
		V_0 = G_B3_0;
		Nullable_1_t3649900800  L_15 = __this->get_period_3();
		V_5 = L_15;
		bool L_16 = Nullable_1_get_HasValue_m2797118855((&V_5), /*hidden argument*/Nullable_1_get_HasValue_m2797118855_MethodInfo_var);
		if (!((((int32_t)L_16) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0095;
		}
	}
	{
		Il2CppObject * L_17 = __this->get_scheduler_4();
		TimeSpan_t763862892  L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_19 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_20 = V_1;
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)U3CSubscribeCoreU3Ec__AnonStorey6D_U3CU3Em__8B_m259250400_MethodInfo_var);
		Action_t437523947 * L_22 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_22, L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_17);
		Il2CppObject * L_23 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, L_17, L_19, L_22);
		return L_23;
	}

IL_0095:
	{
		U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * L_24 = (U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 *)il2cpp_codegen_object_new(U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653_il2cpp_TypeInfo_var);
		U3CSubscribeCoreU3Ec__AnonStorey6E__ctor_m990179820(L_24, /*hidden argument*/NULL);
		V_6 = L_24;
		U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * L_25 = V_6;
		NullCheck(L_25);
		L_25->set_U3CU3Ef__this_1(__this);
		U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * L_26 = V_6;
		Il2CppObject * L_27 = __this->get_scheduler_4();
		NullCheck(L_26);
		L_26->set_periodicScheduler_0(((Il2CppObject *)IsInst(L_27, ISchedulerPeriodic_t2870089311_il2cpp_TypeInfo_var)));
		U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * L_28 = V_6;
		NullCheck(L_28);
		Il2CppObject * L_29 = L_28->get_periodicScheduler_0();
		if (!L_29)
		{
			goto IL_015e;
		}
	}
	{
		TimeSpan_t763862892  L_30 = V_0;
		Nullable_1_t3649900800  L_31 = __this->get_period_3();
		V_7 = L_31;
		TimeSpan_t763862892  L_32 = Nullable_1_get_Value_m3338249190((&V_7), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_33 = TimeSpan_op_Equality_m2213378780(NULL /*static, unused*/, L_30, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0100;
		}
	}
	{
		U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * L_34 = V_6;
		NullCheck(L_34);
		Il2CppObject * L_35 = L_34->get_periodicScheduler_0();
		TimeSpan_t763862892  L_36 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_37 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_38 = V_1;
		NullCheck(L_38);
		Timer_t80811813 * L_39 = L_38->get_timerObserver_0();
		IntPtr_t L_40;
		L_40.set_m_value_0((void*)(void*)Timer_OnNext_m2727522758_MethodInfo_var);
		Action_t437523947 * L_41 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_41, L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_35);
		Il2CppObject * L_42 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(0 /* System.IDisposable UniRx.ISchedulerPeriodic::SchedulePeriodic(System.TimeSpan,System.Action) */, ISchedulerPeriodic_t2870089311_il2cpp_TypeInfo_var, L_35, L_37, L_41);
		return L_42;
	}

IL_0100:
	{
		U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * L_43 = (U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 *)il2cpp_codegen_object_new(U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654_il2cpp_TypeInfo_var);
		U3CSubscribeCoreU3Ec__AnonStorey6F__ctor_m793666315(L_43, /*hidden argument*/NULL);
		V_8 = L_43;
		U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * L_44 = V_8;
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_45 = V_1;
		NullCheck(L_44);
		L_44->set_U3CU3Ef__refU24109_1(L_45);
		U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * L_46 = V_8;
		U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * L_47 = V_6;
		NullCheck(L_46);
		L_46->set_U3CU3Ef__refU24110_2(L_47);
		U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * L_48 = V_8;
		NullCheck(L_48);
		L_48->set_U3CU3Ef__this_3(__this);
		U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * L_49 = V_8;
		SerialDisposable_t2547852742 * L_50 = (SerialDisposable_t2547852742 *)il2cpp_codegen_object_new(SerialDisposable_t2547852742_il2cpp_TypeInfo_var);
		SerialDisposable__ctor_m2977852291(L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		L_49->set_disposable_0(L_50);
		U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * L_51 = V_8;
		NullCheck(L_51);
		SerialDisposable_t2547852742 * L_52 = L_51->get_disposable_0();
		Il2CppObject * L_53 = __this->get_scheduler_4();
		TimeSpan_t763862892  L_54 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_55 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * L_56 = V_8;
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)U3CSubscribeCoreU3Ec__AnonStorey6F_U3CU3Em__8C_m517416799_MethodInfo_var);
		Action_t437523947 * L_58 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_58, L_56, L_57, /*hidden argument*/NULL);
		NullCheck(L_53);
		Il2CppObject * L_59 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, L_53, L_55, L_58);
		NullCheck(L_52);
		SerialDisposable_set_Disposable_m1438046228(L_52, L_59, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * L_60 = V_8;
		NullCheck(L_60);
		SerialDisposable_t2547852742 * L_61 = L_60->get_disposable_0();
		return L_61;
	}

IL_015e:
	{
		U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * L_62 = (U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 *)il2cpp_codegen_object_new(U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663_il2cpp_TypeInfo_var);
		U3CSubscribeCoreU3Ec__AnonStorey70__ctor_m3320012066(L_62, /*hidden argument*/NULL);
		V_9 = L_62;
		U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * L_63 = V_9;
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_64 = V_1;
		NullCheck(L_63);
		L_63->set_U3CU3Ef__refU24109_1(L_64);
		U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * L_65 = V_9;
		Nullable_1_t3649900800  L_66 = __this->get_period_3();
		V_10 = L_66;
		TimeSpan_t763862892  L_67 = Nullable_1_get_Value_m3338249190((&V_10), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_68 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		NullCheck(L_65);
		L_65->set_timeP_0(L_68);
		Il2CppObject * L_69 = __this->get_scheduler_4();
		TimeSpan_t763862892  L_70 = V_0;
		TimeSpan_t763862892  L_71 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * L_72 = V_9;
		IntPtr_t L_73;
		L_73.set_m_value_0((void*)(void*)U3CSubscribeCoreU3Ec__AnonStorey70_U3CU3Em__8D_m809751865_MethodInfo_var);
		Action_1_t1060768302 * L_74 = (Action_1_t1060768302 *)il2cpp_codegen_object_new(Action_1_t1060768302_il2cpp_TypeInfo_var);
		Action_1__ctor_m3957693621(L_74, L_72, L_73, /*hidden argument*/Action_1__ctor_m3957693621_MethodInfo_var);
		Il2CppObject * L_75 = Scheduler_Schedule_m279164218(NULL /*static, unused*/, L_69, L_71, L_74, /*hidden argument*/NULL);
		return L_75;
	}
}
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6D__ctor_m1186693325 (U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D::<>m__8B()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6D_U3CU3Em__8B_m259250400 (U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * __this, const MethodInfo* method)
{
	{
		Timer_t80811813 * L_0 = __this->get_timerObserver_0();
		NullCheck(L_0);
		Timer_OnNext_m2727522758(L_0, /*hidden argument*/NULL);
		Timer_t80811813 * L_1 = __this->get_timerObserver_0();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.TimerObservable/Timer::OnCompleted() */, L_1);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6E__ctor_m990179820 (U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6F__ctor_m793666315 (U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F::<>m__8C()
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* ISchedulerPeriodic_t2870089311_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3338249190_MethodInfo_var;
extern const MethodInfo* Timer_OnNext_m2727522758_MethodInfo_var;
extern const uint32_t U3CSubscribeCoreU3Ec__AnonStorey6F_U3CU3Em__8C_m517416799_MetadataUsageId;
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6F_U3CU3Em__8C_m517416799 (U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSubscribeCoreU3Ec__AnonStorey6F_U3CU3Em__8C_m517416799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Nullable_1_t3649900800  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_0 = __this->get_U3CU3Ef__refU24109_1();
		NullCheck(L_0);
		Timer_t80811813 * L_1 = L_0->get_timerObserver_0();
		NullCheck(L_1);
		Timer_OnNext_m2727522758(L_1, /*hidden argument*/NULL);
		TimerObservable_t1697791765 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Nullable_1_t3649900800  L_3 = L_2->get_period_3();
		V_1 = L_3;
		TimeSpan_t763862892  L_4 = Nullable_1_get_Value_m3338249190((&V_1), /*hidden argument*/Nullable_1_get_Value_m3338249190_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_5 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		SerialDisposable_t2547852742 * L_6 = __this->get_disposable_0();
		U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * L_7 = __this->get_U3CU3Ef__refU24110_2();
		NullCheck(L_7);
		Il2CppObject * L_8 = L_7->get_periodicScheduler_0();
		TimeSpan_t763862892  L_9 = V_0;
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_10 = __this->get_U3CU3Ef__refU24109_1();
		NullCheck(L_10);
		Timer_t80811813 * L_11 = L_10->get_timerObserver_0();
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)Timer_OnNext_m2727522758_MethodInfo_var);
		Action_t437523947 * L_13 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_13, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		Il2CppObject * L_14 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(0 /* System.IDisposable UniRx.ISchedulerPeriodic::SchedulePeriodic(System.TimeSpan,System.Action) */, ISchedulerPeriodic_t2870089311_il2cpp_TypeInfo_var, L_8, L_9, L_13);
		NullCheck(L_6);
		SerialDisposable_set_Disposable_m1438046228(L_6, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey70__ctor_m3320012066 (U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70::<>m__8D(System.Action`1<System.TimeSpan>)
extern const MethodInfo* Action_1_Invoke_m613956924_MethodInfo_var;
extern const uint32_t U3CSubscribeCoreU3Ec__AnonStorey70_U3CU3Em__8D_m809751865_MetadataUsageId;
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey70_U3CU3Em__8D_m809751865 (U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * __this, Action_1_t912315597 * ___self0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSubscribeCoreU3Ec__AnonStorey70_U3CU3Em__8D_m809751865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * L_0 = __this->get_U3CU3Ef__refU24109_1();
		NullCheck(L_0);
		Timer_t80811813 * L_1 = L_0->get_timerObserver_0();
		NullCheck(L_1);
		Timer_OnNext_m2727522758(L_1, /*hidden argument*/NULL);
		Action_1_t912315597 * L_2 = ___self0;
		TimeSpan_t763862892  L_3 = __this->get_timeP_0();
		NullCheck(L_2);
		Action_1_Invoke_m613956924(L_2, L_3, /*hidden argument*/Action_1_Invoke_m613956924_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/Timer::.ctor(UniRx.IObserver`1<System.Int64>,System.IDisposable)
extern const MethodInfo* OperatorObserverBase_2__ctor_m3985458928_MethodInfo_var;
extern const uint32_t Timer__ctor_m970264198_MetadataUsageId;
extern "C"  void Timer__ctor_m970264198 (Timer_t80811813 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timer__ctor_m970264198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		OperatorObserverBase_2__ctor_m3985458928(__this, L_0, L_1, /*hidden argument*/OperatorObserverBase_2__ctor_m3985458928_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/Timer::OnNext()
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Timer_OnNext_m2727522758_MetadataUsageId;
extern "C"  void Timer_OnNext_m2727522758 (Timer_t80811813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timer_OnNext_m2727522758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = ((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int64_t L_1 = __this->get_index_2();
		int64_t L_2 = L_1;
		V_0 = L_2;
		__this->set_index_2(((int64_t)((int64_t)L_2+(int64_t)(((int64_t)((int64_t)1))))));
		int64_t L_3 = V_0;
		NullCheck(L_0);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_0, L_3);
		goto IL_0032;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_0024:
	{ // begin catch(System.Object)
		{
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, __this);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_002d:
		{
			goto IL_0032;
		}
	} // end catch (depth: 1)

IL_0032:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/Timer::OnNext(System.Int64)
extern "C"  void Timer_OnNext_m2130049432 (Timer_t80811813 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/Timer::OnError(System.Exception)
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern const uint32_t Timer_OnError_m1423177575_MetadataUsageId;
extern "C"  void Timer_OnError_m1423177575 (Timer_t80811813 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timer_OnError_m1423177575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = ((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_0, L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, __this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.TimerObservable/Timer::OnCompleted()
extern Il2CppClass* IObserver_1_t764446489_il2cpp_TypeInfo_var;
extern const uint32_t Timer_OnCompleted_m621104442_MetadataUsageId;
extern "C"  void Timer_OnCompleted_m621104442 (Timer_t80811813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Timer_OnCompleted_m621104442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = ((OperatorObserverBase_2_t3939730909 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IObserver_1_t764446489_il2cpp_TypeInfo_var, L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose() */, __this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.PresenterBase::.ctor()
extern Il2CppClass* PresenterBase_1_t3949187127_il2cpp_TypeInfo_var;
extern const MethodInfo* PresenterBase_1__ctor_m304455231_MethodInfo_var;
extern const uint32_t PresenterBase__ctor_m4005750934_MetadataUsageId;
extern "C"  void PresenterBase__ctor_m4005750934 (PresenterBase_t3597493419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PresenterBase__ctor_m4005750934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PresenterBase_1_t3949187127_il2cpp_TypeInfo_var);
		PresenterBase_1__ctor_m304455231(__this, /*hidden argument*/PresenterBase_1__ctor_m304455231_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.PresenterBase::BeforeInitialize(UniRx.Unit)
extern "C"  void PresenterBase_BeforeInitialize_m1220651073 (PresenterBase_t3597493419 * __this, Unit_t2558286038  ___argument0, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(16 /* System.Void UniRx.PresenterBase::BeforeInitialize() */, __this);
		return;
	}
}
// System.Void UniRx.PresenterBase::Initialize(UniRx.Unit)
extern "C"  void PresenterBase_Initialize_m462658466 (PresenterBase_t3597493419 * __this, Unit_t2558286038  ___argument0, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(17 /* System.Void UniRx.PresenterBase::Initialize() */, __this);
		return;
	}
}
// System.Void UniRx.PresenterBase::ForceInitialize()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t PresenterBase_ForceInitialize_m97601551_MetadataUsageId;
extern "C"  void PresenterBase_ForceInitialize_m97601551 (PresenterBase_t3597493419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PresenterBase_ForceInitialize_m97601551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_0 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< Unit_t2558286038  >::Invoke(13 /* System.Void UniRx.PresenterBase`1<UniRx.Unit>::ForceInitialize(T) */, __this, L_0);
		return;
	}
}
// System.Void UniRx.QuaternionReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t2500049017_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m79599915_MethodInfo_var;
extern const uint32_t QuaternionReactiveProperty__ctor_m2144602315_MetadataUsageId;
extern "C"  void QuaternionReactiveProperty__ctor_m2144602315 (QuaternionReactiveProperty_t512953982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QuaternionReactiveProperty__ctor_m2144602315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t2500049017_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m79599915(__this, /*hidden argument*/ReactiveProperty_1__ctor_m79599915_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.QuaternionReactiveProperty::.ctor(UnityEngine.Quaternion)
extern Il2CppClass* ReactiveProperty_1_t2500049017_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m2467598739_MethodInfo_var;
extern const uint32_t QuaternionReactiveProperty__ctor_m3790748246_MetadataUsageId;
extern "C"  void QuaternionReactiveProperty__ctor_m3790748246 (QuaternionReactiveProperty_t512953982 * __this, Quaternion_t1891715979  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QuaternionReactiveProperty__ctor_m3790748246_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Quaternion_t1891715979  L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t2500049017_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m2467598739(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m2467598739_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion> UniRx.QuaternionReactiveProperty::get_EqualityComparer()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t QuaternionReactiveProperty_get_EqualityComparer_m3490441263_MetadataUsageId;
extern "C"  Il2CppObject* QuaternionReactiveProperty_get_EqualityComparer_m3490441263 (QuaternionReactiveProperty_t512953982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QuaternionReactiveProperty_get_EqualityComparer_m3490441263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((UnityEqualityComparer_t3801418734_StaticFields*)UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var->static_fields)->get_Quaternion_6();
		return L_0;
	}
}
// System.Void UniRx.RangeReactivePropertyAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeReactivePropertyAttribute__ctor_m1724076426 (RangeReactivePropertyAttribute_t252243683 * __this, float ___min0, float ___max1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		RangeReactivePropertyAttribute_set_Min_m2699366962(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___max1;
		RangeReactivePropertyAttribute_set_Max_m3947416800(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UniRx.RangeReactivePropertyAttribute::get_Min()
extern "C"  float RangeReactivePropertyAttribute_get_Min_m1545646273 (RangeReactivePropertyAttribute_t252243683 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3CMinU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.RangeReactivePropertyAttribute::set_Min(System.Single)
extern "C"  void RangeReactivePropertyAttribute_set_Min_m2699366962 (RangeReactivePropertyAttribute_t252243683 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CMinU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Single UniRx.RangeReactivePropertyAttribute::get_Max()
extern "C"  float RangeReactivePropertyAttribute_get_Max_m1545417555 (RangeReactivePropertyAttribute_t252243683 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3CMaxU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.RangeReactivePropertyAttribute::set_Max(System.Single)
extern "C"  void RangeReactivePropertyAttribute_set_Max_m3947416800 (RangeReactivePropertyAttribute_t252243683 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CMaxU3Ek__BackingField_1(L_0);
		return;
	}
}
// UniRx.IObservable`1<System.Boolean> UniRx.ReactivePropertyExtensions::CombineLatestValuesAreAllTrue(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Boolean>>)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* ReactivePropertyExtensions_t1053651444_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1721034418_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_CombineLatest_TisBoolean_t211005341_m3684453296_MethodInfo_var;
extern const MethodInfo* ReactivePropertyExtensions_U3CCombineLatestValuesAreAllTrueU3Em__C4_m2362911536_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1028844453_MethodInfo_var;
extern const MethodInfo* Observable_Select_TisIList_1_t2377497655_TisBoolean_t211005341_m1872695583_MethodInfo_var;
extern const uint32_t ReactivePropertyExtensions_CombineLatestValuesAreAllTrue_m2288590888_MetadataUsageId;
extern "C"  Il2CppObject* ReactivePropertyExtensions_CombineLatestValuesAreAllTrue_m2288590888 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___sources0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactivePropertyExtensions_CombineLatestValuesAreAllTrue_m2288590888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___sources0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_1 = Observable_CombineLatest_TisBoolean_t211005341_m3684453296(NULL /*static, unused*/, L_0, /*hidden argument*/Observable_CombineLatest_TisBoolean_t211005341_m3684453296_MethodInfo_var);
		Func_2_t1721034418 * L_2 = ((ReactivePropertyExtensions_t1053651444_StaticFields*)ReactivePropertyExtensions_t1053651444_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		G_B1_0 = L_1;
		if (L_2)
		{
			G_B2_0 = L_1;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ReactivePropertyExtensions_U3CCombineLatestValuesAreAllTrueU3Em__C4_m2362911536_MethodInfo_var);
		Func_2_t1721034418 * L_4 = (Func_2_t1721034418 *)il2cpp_codegen_object_new(Func_2_t1721034418_il2cpp_TypeInfo_var);
		Func_2__ctor_m1028844453(L_4, NULL, L_3, /*hidden argument*/Func_2__ctor_m1028844453_MethodInfo_var);
		((ReactivePropertyExtensions_t1053651444_StaticFields*)ReactivePropertyExtensions_t1053651444_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_0(L_4);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t1721034418 * L_5 = ((ReactivePropertyExtensions_t1053651444_StaticFields*)ReactivePropertyExtensions_t1053651444_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_6 = Observable_Select_TisIList_1_t2377497655_TisBoolean_t211005341_m1872695583(NULL /*static, unused*/, G_B2_0, L_5, /*hidden argument*/Observable_Select_TisIList_1_t2377497655_TisBoolean_t211005341_m1872695583_MethodInfo_var);
		return L_6;
	}
}
// UniRx.IObservable`1<System.Boolean> UniRx.ReactivePropertyExtensions::CombineLatestValuesAreAllFalse(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Boolean>>)
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* ReactivePropertyExtensions_t1053651444_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1721034418_il2cpp_TypeInfo_var;
extern const MethodInfo* Observable_CombineLatest_TisBoolean_t211005341_m3684453296_MethodInfo_var;
extern const MethodInfo* ReactivePropertyExtensions_U3CCombineLatestValuesAreAllFalseU3Em__C5_m2400503264_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1028844453_MethodInfo_var;
extern const MethodInfo* Observable_Select_TisIList_1_t2377497655_TisBoolean_t211005341_m1872695583_MethodInfo_var;
extern const uint32_t ReactivePropertyExtensions_CombineLatestValuesAreAllFalse_m3124093143_MetadataUsageId;
extern "C"  Il2CppObject* ReactivePropertyExtensions_CombineLatestValuesAreAllFalse_m3124093143 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___sources0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactivePropertyExtensions_CombineLatestValuesAreAllFalse_m3124093143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = ___sources0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_1 = Observable_CombineLatest_TisBoolean_t211005341_m3684453296(NULL /*static, unused*/, L_0, /*hidden argument*/Observable_CombineLatest_TisBoolean_t211005341_m3684453296_MethodInfo_var);
		Func_2_t1721034418 * L_2 = ((ReactivePropertyExtensions_t1053651444_StaticFields*)ReactivePropertyExtensions_t1053651444_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = L_1;
		if (L_2)
		{
			G_B2_0 = L_1;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ReactivePropertyExtensions_U3CCombineLatestValuesAreAllFalseU3Em__C5_m2400503264_MethodInfo_var);
		Func_2_t1721034418 * L_4 = (Func_2_t1721034418 *)il2cpp_codegen_object_new(Func_2_t1721034418_il2cpp_TypeInfo_var);
		Func_2__ctor_m1028844453(L_4, NULL, L_3, /*hidden argument*/Func_2__ctor_m1028844453_MethodInfo_var);
		((ReactivePropertyExtensions_t1053651444_StaticFields*)ReactivePropertyExtensions_t1053651444_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_4);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t1721034418 * L_5 = ((ReactivePropertyExtensions_t1053651444_StaticFields*)ReactivePropertyExtensions_t1053651444_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_6 = Observable_Select_TisIList_1_t2377497655_TisBoolean_t211005341_m1872695583(NULL /*static, unused*/, G_B2_0, L_5, /*hidden argument*/Observable_Select_TisIList_1_t2377497655_TisBoolean_t211005341_m1872695583_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean UniRx.ReactivePropertyExtensions::<CombineLatestValuesAreAllTrue>m__C4(System.Collections.Generic.IList`1<System.Boolean>)
extern Il2CppClass* IEnumerable_1_t3083159697_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1694111789_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactivePropertyExtensions_U3CCombineLatestValuesAreAllTrueU3Em__C4_m2362911536_MetadataUsageId;
extern "C"  bool ReactivePropertyExtensions_U3CCombineLatestValuesAreAllTrueU3Em__C4_m2362911536 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___xs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactivePropertyExtensions_U3CCombineLatestValuesAreAllTrueU3Em__C4_m2362911536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___xs0;
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Boolean>::GetEnumerator() */, IEnumerable_1_t3083159697_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0020;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck(L_2);
			bool L_3 = InterfaceFuncInvoker0< bool >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Boolean>::get_Current() */, IEnumerator_1_t1694111789_il2cpp_TypeInfo_var, L_2);
			V_0 = L_3;
			bool L_4 = V_0;
			if (L_4)
			{
				goto IL_0020;
			}
		}

IL_0019:
		{
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x3D, FINALLY_0030);
		}

IL_0020:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck(L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x3B, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_0034;
			}
		}

IL_0033:
		{
			IL2CPP_END_FINALLY(48)
		}

IL_0034:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_8);
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return (bool)1;
	}

IL_003d:
	{
		bool L_9 = V_2;
		return L_9;
	}
}
// System.Boolean UniRx.ReactivePropertyExtensions::<CombineLatestValuesAreAllFalse>m__C5(System.Collections.Generic.IList`1<System.Boolean>)
extern Il2CppClass* IEnumerable_1_t3083159697_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1694111789_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactivePropertyExtensions_U3CCombineLatestValuesAreAllFalseU3Em__C5_m2400503264_MetadataUsageId;
extern "C"  bool ReactivePropertyExtensions_U3CCombineLatestValuesAreAllFalseU3Em__C5_m2400503264 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___xs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactivePropertyExtensions_U3CCombineLatestValuesAreAllFalseU3Em__C5_m2400503264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___xs0;
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Boolean>::GetEnumerator() */, IEnumerable_1_t3083159697_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0020;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck(L_2);
			bool L_3 = InterfaceFuncInvoker0< bool >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Boolean>::get_Current() */, IEnumerator_1_t1694111789_il2cpp_TypeInfo_var, L_2);
			V_0 = L_3;
			bool L_4 = V_0;
			if (!L_4)
			{
				goto IL_0020;
			}
		}

IL_0019:
		{
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x3D, FINALLY_0030);
		}

IL_0020:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck(L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x3B, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_0034;
			}
		}

IL_0033:
		{
			IL2CPP_END_FINALLY(48)
		}

IL_0034:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_8);
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return (bool)1;
	}

IL_003d:
	{
		bool L_9 = V_2;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
