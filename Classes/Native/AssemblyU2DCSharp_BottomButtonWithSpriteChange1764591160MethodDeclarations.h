﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BottomButtonWithSpriteChange
struct BottomButtonWithSpriteChange_t1764591160;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void BottomButtonWithSpriteChange::.ctor()
extern "C"  void BottomButtonWithSpriteChange__ctor_m2388310883 (BottomButtonWithSpriteChange_t1764591160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonWithSpriteChange::PostInject()
extern "C"  void BottomButtonWithSpriteChange_PostInject_m1459355538 (BottomButtonWithSpriteChange_t1764591160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonWithSpriteChange::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BottomButtonWithSpriteChange_OnPointerClick_m3935667731 (BottomButtonWithSpriteChange_t1764591160 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonWithSpriteChange::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BottomButtonWithSpriteChange_OnPointerDown_m3251211517 (BottomButtonWithSpriteChange_t1764591160 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonWithSpriteChange::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BottomButtonWithSpriteChange_OnPointerUp_m3771591140 (BottomButtonWithSpriteChange_t1764591160 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BottomButtonWithSpriteChange::<PostInject>m__0(System.Boolean)
extern "C"  bool BottomButtonWithSpriteChange_U3CPostInjectU3Em__0_m474002240 (Il2CppObject * __this /* static, unused */, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonWithSpriteChange::<PostInject>m__1(System.Boolean)
extern "C"  void BottomButtonWithSpriteChange_U3CPostInjectU3Em__1_m52176269 (BottomButtonWithSpriteChange_t1764591160 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonWithSpriteChange::<OnPointerClick>m__2(UniRx.Unit)
extern "C"  void BottomButtonWithSpriteChange_U3COnPointerClickU3Em__2_m2713992098 (BottomButtonWithSpriteChange_t1764591160 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
