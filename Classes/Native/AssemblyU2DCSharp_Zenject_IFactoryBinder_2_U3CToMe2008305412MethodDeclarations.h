﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`2/<ToMethod>c__AnonStorey176<System.Object,System.Object>
struct U3CToMethodU3Ec__AnonStorey176_t2008305412;
// Zenject.IFactory`2<System.Object,System.Object>
struct IFactory_2_t972313871;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`2/<ToMethod>c__AnonStorey176<System.Object,System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey176__ctor_m3304531251_gshared (U3CToMethodU3Ec__AnonStorey176_t2008305412 * __this, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey176__ctor_m3304531251(__this, method) ((  void (*) (U3CToMethodU3Ec__AnonStorey176_t2008305412 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey176__ctor_m3304531251_gshared)(__this, method)
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2/<ToMethod>c__AnonStorey176<System.Object,System.Object>::<>m__298(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey176_U3CU3Em__298_m4065912767_gshared (U3CToMethodU3Ec__AnonStorey176_t2008305412 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToMethodU3Ec__AnonStorey176_U3CU3Em__298_m4065912767(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToMethodU3Ec__AnonStorey176_t2008305412 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToMethodU3Ec__AnonStorey176_U3CU3Em__298_m4065912767_gshared)(__this, ___ctx0, method)
