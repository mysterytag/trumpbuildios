﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>
struct List_1_t2481898271;
// PlayFab.Internal.GMFB_327/testRegion[]
struct testRegionU5BU5D_t485442179;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"
#include "mscorlib_System_Nullable_1_gen276009914.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.GMFB_327/EnumConversionTestClass
struct  EnumConversionTestClass_t3943265455  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion> PlayFab.Internal.GMFB_327/EnumConversionTestClass::enumList
	List_1_t2481898271 * ___enumList_0;
	// PlayFab.Internal.GMFB_327/testRegion[] PlayFab.Internal.GMFB_327/EnumConversionTestClass::enumArray
	testRegionU5BU5D_t485442179* ___enumArray_1;
	// PlayFab.Internal.GMFB_327/testRegion PlayFab.Internal.GMFB_327/EnumConversionTestClass::enumValue
	int32_t ___enumValue_2;
	// System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion> PlayFab.Internal.GMFB_327/EnumConversionTestClass::optEnumValue
	Nullable_1_t276009914  ___optEnumValue_3;

public:
	inline static int32_t get_offset_of_enumList_0() { return static_cast<int32_t>(offsetof(EnumConversionTestClass_t3943265455, ___enumList_0)); }
	inline List_1_t2481898271 * get_enumList_0() const { return ___enumList_0; }
	inline List_1_t2481898271 ** get_address_of_enumList_0() { return &___enumList_0; }
	inline void set_enumList_0(List_1_t2481898271 * value)
	{
		___enumList_0 = value;
		Il2CppCodeGenWriteBarrier(&___enumList_0, value);
	}

	inline static int32_t get_offset_of_enumArray_1() { return static_cast<int32_t>(offsetof(EnumConversionTestClass_t3943265455, ___enumArray_1)); }
	inline testRegionU5BU5D_t485442179* get_enumArray_1() const { return ___enumArray_1; }
	inline testRegionU5BU5D_t485442179** get_address_of_enumArray_1() { return &___enumArray_1; }
	inline void set_enumArray_1(testRegionU5BU5D_t485442179* value)
	{
		___enumArray_1 = value;
		Il2CppCodeGenWriteBarrier(&___enumArray_1, value);
	}

	inline static int32_t get_offset_of_enumValue_2() { return static_cast<int32_t>(offsetof(EnumConversionTestClass_t3943265455, ___enumValue_2)); }
	inline int32_t get_enumValue_2() const { return ___enumValue_2; }
	inline int32_t* get_address_of_enumValue_2() { return &___enumValue_2; }
	inline void set_enumValue_2(int32_t value)
	{
		___enumValue_2 = value;
	}

	inline static int32_t get_offset_of_optEnumValue_3() { return static_cast<int32_t>(offsetof(EnumConversionTestClass_t3943265455, ___optEnumValue_3)); }
	inline Nullable_1_t276009914  get_optEnumValue_3() const { return ___optEnumValue_3; }
	inline Nullable_1_t276009914 * get_address_of_optEnumValue_3() { return &___optEnumValue_3; }
	inline void set_optEnumValue_3(Nullable_1_t276009914  value)
	{
		___optEnumValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
