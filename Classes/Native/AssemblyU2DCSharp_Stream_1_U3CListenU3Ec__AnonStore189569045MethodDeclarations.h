﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<System.Single>
struct U3CListenU3Ec__AnonStorey128_t189569045;

#include "codegen/il2cpp-codegen.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<System.Single>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m3783212849_gshared (U3CListenU3Ec__AnonStorey128_t189569045 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m3783212849(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t189569045 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m3783212849_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Single>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m4063027662_gshared (U3CListenU3Ec__AnonStorey128_t189569045 * __this, float ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m4063027662(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t189569045 *, float, const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m4063027662_gshared)(__this, ____0, method)
