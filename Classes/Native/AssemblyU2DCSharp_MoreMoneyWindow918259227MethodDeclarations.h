﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoreMoneyWindow
struct MoreMoneyWindow_t918259227;
// MoneyPackInfo
struct MoneyPackInfo_t3013049127;

#include "codegen/il2cpp-codegen.h"

// System.Void MoreMoneyWindow::.ctor()
extern "C"  void MoreMoneyWindow__ctor_m851570608 (MoreMoneyWindow_t918259227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoreMoneyWindow::Init(System.Int32)
extern "C"  void MoreMoneyWindow_Init_m542722037 (MoreMoneyWindow_t918259227 * __this, int32_t ___packNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MoneyPackInfo MoreMoneyWindow::GetMoneyPackInfo(System.Int32)
extern "C"  MoneyPackInfo_t3013049127 * MoreMoneyWindow_GetMoneyPackInfo_m3854564394 (MoreMoneyWindow_t918259227 * __this, int32_t ___pack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoreMoneyWindow::Click()
extern "C"  void MoreMoneyWindow_Click_m2556418134 (MoreMoneyWindow_t918259227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
