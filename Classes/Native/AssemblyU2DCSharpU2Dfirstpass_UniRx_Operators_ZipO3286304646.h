﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.Operators.IZipObservable
struct IZipObservable_t4005421640;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2545193960;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipObserver`1<System.Object>
struct  ZipObserver_1_t3286304646  : public Il2CppObject
{
public:
	// System.Object UniRx.Operators.ZipObserver`1::gate
	Il2CppObject * ___gate_0;
	// UniRx.Operators.IZipObservable UniRx.Operators.ZipObserver`1::parent
	Il2CppObject * ___parent_1;
	// System.Int32 UniRx.Operators.ZipObserver`1::index
	int32_t ___index_2;
	// System.Collections.Generic.Queue`1<T> UniRx.Operators.ZipObserver`1::queue
	Queue_1_t2545193960 * ___queue_3;

public:
	inline static int32_t get_offset_of_gate_0() { return static_cast<int32_t>(offsetof(ZipObserver_1_t3286304646, ___gate_0)); }
	inline Il2CppObject * get_gate_0() const { return ___gate_0; }
	inline Il2CppObject ** get_address_of_gate_0() { return &___gate_0; }
	inline void set_gate_0(Il2CppObject * value)
	{
		___gate_0 = value;
		Il2CppCodeGenWriteBarrier(&___gate_0, value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(ZipObserver_1_t3286304646, ___parent_1)); }
	inline Il2CppObject * get_parent_1() const { return ___parent_1; }
	inline Il2CppObject ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(Il2CppObject * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(ZipObserver_1_t3286304646, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_queue_3() { return static_cast<int32_t>(offsetof(ZipObserver_1_t3286304646, ___queue_3)); }
	inline Queue_1_t2545193960 * get_queue_3() const { return ___queue_3; }
	inline Queue_1_t2545193960 ** get_address_of_queue_3() { return &___queue_3; }
	inline void set_queue_3(Queue_1_t2545193960 * value)
	{
		___queue_3 = value;
		Il2CppCodeGenWriteBarrier(&___queue_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
