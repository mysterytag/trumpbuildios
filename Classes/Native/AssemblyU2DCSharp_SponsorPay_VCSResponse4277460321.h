﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SponsorPay.RequestError
struct RequestError_t1597064339;
// SponsorPay.SuccessfulCurrencyResponse
struct SuccessfulCurrencyResponse_t823827974;

#include "AssemblyU2DCSharp_SponsorPay_AbstractResponse2918001245.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.VCSResponse
struct  VCSResponse_t4277460321  : public AbstractResponse_t2918001245
{
public:
	// SponsorPay.RequestError SponsorPay.VCSResponse::<error>k__BackingField
	RequestError_t1597064339 * ___U3CerrorU3Ek__BackingField_1;
	// SponsorPay.SuccessfulCurrencyResponse SponsorPay.VCSResponse::<transaction>k__BackingField
	SuccessfulCurrencyResponse_t823827974 * ___U3CtransactionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VCSResponse_t4277460321, ___U3CerrorU3Ek__BackingField_1)); }
	inline RequestError_t1597064339 * get_U3CerrorU3Ek__BackingField_1() const { return ___U3CerrorU3Ek__BackingField_1; }
	inline RequestError_t1597064339 ** get_address_of_U3CerrorU3Ek__BackingField_1() { return &___U3CerrorU3Ek__BackingField_1; }
	inline void set_U3CerrorU3Ek__BackingField_1(RequestError_t1597064339 * value)
	{
		___U3CerrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CtransactionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VCSResponse_t4277460321, ___U3CtransactionU3Ek__BackingField_2)); }
	inline SuccessfulCurrencyResponse_t823827974 * get_U3CtransactionU3Ek__BackingField_2() const { return ___U3CtransactionU3Ek__BackingField_2; }
	inline SuccessfulCurrencyResponse_t823827974 ** get_address_of_U3CtransactionU3Ek__BackingField_2() { return &___U3CtransactionU3Ek__BackingField_2; }
	inline void set_U3CtransactionU3Ek__BackingField_2(SuccessfulCurrencyResponse_t823827974 * value)
	{
		___U3CtransactionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtransactionU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
