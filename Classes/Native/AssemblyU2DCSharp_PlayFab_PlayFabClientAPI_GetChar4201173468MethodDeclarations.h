﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterStatisticsResponseCallback
struct GetCharacterStatisticsResponseCallback_t4201173468;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterStatisticsRequest
struct GetCharacterStatisticsRequest_t3373629353;
// PlayFab.ClientModels.GetCharacterStatisticsResult
struct GetCharacterStatisticsResult_t2637826531;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte3373629353.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2637826531.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterStatisticsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterStatisticsResponseCallback__ctor_m2805559499 (GetCharacterStatisticsResponseCallback_t4201173468 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterStatisticsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterStatisticsRequest,PlayFab.ClientModels.GetCharacterStatisticsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetCharacterStatisticsResponseCallback_Invoke_m2965593688 (GetCharacterStatisticsResponseCallback_t4201173468 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterStatisticsRequest_t3373629353 * ___request2, GetCharacterStatisticsResult_t2637826531 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterStatisticsResponseCallback_t4201173468(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterStatisticsRequest_t3373629353 * ___request2, GetCharacterStatisticsResult_t2637826531 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterStatisticsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterStatisticsRequest,PlayFab.ClientModels.GetCharacterStatisticsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterStatisticsResponseCallback_BeginInvoke_m1364265349 (GetCharacterStatisticsResponseCallback_t4201173468 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterStatisticsRequest_t3373629353 * ___request2, GetCharacterStatisticsResult_t2637826531 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterStatisticsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterStatisticsResponseCallback_EndInvoke_m434307163 (GetCharacterStatisticsResponseCallback_t4201173468 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
