﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Transaction/<Test>c__AnonStorey14B
struct U3CTestU3Ec__AnonStorey14B_t2969199272;
// ICell`1<System.Int32>
struct ICell_1_t104078468;

#include "codegen/il2cpp-codegen.h"

// System.Void Transaction/<Test>c__AnonStorey14B::.ctor()
extern "C"  void U3CTestU3Ec__AnonStorey14B__ctor_m3649349538 (U3CTestU3Ec__AnonStorey14B_t2969199272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction/<Test>c__AnonStorey14B::<>m__1EF(System.Int32)
extern "C"  void U3CTestU3Ec__AnonStorey14B_U3CU3Em__1EF_m3195156794 (U3CTestU3Ec__AnonStorey14B_t2969199272 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICell`1<System.Int32> Transaction/<Test>c__AnonStorey14B::<>m__1F3(System.Int32)
extern "C"  Il2CppObject* U3CTestU3Ec__AnonStorey14B_U3CU3Em__1F3_m936361034 (U3CTestU3Ec__AnonStorey14B_t2969199272 * __this, int32_t ___v10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction/<Test>c__AnonStorey14B::<>m__1F6()
extern "C"  void U3CTestU3Ec__AnonStorey14B_U3CU3Em__1F6_m2095354296 (U3CTestU3Ec__AnonStorey14B_t2969199272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
