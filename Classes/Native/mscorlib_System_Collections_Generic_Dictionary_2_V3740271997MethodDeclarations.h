﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t3973244056;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3740271997.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4077345339_gshared (Enumerator_t3740271999 * __this, Dictionary_2_t3973244056 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4077345339(__this, ___host0, method) ((  void (*) (Enumerator_t3740271999 *, Dictionary_2_t3973244056 *, const MethodInfo*))Enumerator__ctor_m4077345339_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4129788870_gshared (Enumerator_t3740271999 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4129788870(__this, method) ((  Il2CppObject * (*) (Enumerator_t3740271999 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4129788870_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1927014298_gshared (Enumerator_t3740271999 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1927014298(__this, method) ((  void (*) (Enumerator_t3740271999 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1927014298_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt32>::Dispose()
extern "C"  void Enumerator_Dispose_m245921181_gshared (Enumerator_t3740271999 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m245921181(__this, method) ((  void (*) (Enumerator_t3740271999 *, const MethodInfo*))Enumerator_Dispose_m245921181_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3650845574_gshared (Enumerator_t3740271999 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3650845574(__this, method) ((  bool (*) (Enumerator_t3740271999 *, const MethodInfo*))Enumerator_MoveNext_m3650845574_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt32>::get_Current()
extern "C"  uint32_t Enumerator_get_Current_m582465852_gshared (Enumerator_t3740271999 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m582465852(__this, method) ((  uint32_t (*) (Enumerator_t3740271999 *, const MethodInfo*))Enumerator_get_Current_m582465852_gshared)(__this, method)
