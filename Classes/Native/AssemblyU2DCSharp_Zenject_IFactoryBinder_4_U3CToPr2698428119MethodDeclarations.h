﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`4/<ToPrefab>c__AnonStorey17B<System.Object,System.Object,System.Object,System.Object>
struct U3CToPrefabU3Ec__AnonStorey17B_t2698428119;
// Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>
struct IFactory_4_t1894544701;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`4/<ToPrefab>c__AnonStorey17B<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey17B__ctor_m813662430_gshared (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * __this, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey17B__ctor_m813662430(__this, method) ((  void (*) (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey17B__ctor_m813662430_gshared)(__this, method)
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4/<ToPrefab>c__AnonStorey17B<System.Object,System.Object,System.Object,System.Object>::<>m__2A3(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey17B_U3CU3Em__2A3_m3199267610_gshared (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey17B_U3CU3Em__2A3_m3199267610(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey17B_U3CU3Em__2A3_m3199267610_gshared)(__this, ___ctx0, method)
