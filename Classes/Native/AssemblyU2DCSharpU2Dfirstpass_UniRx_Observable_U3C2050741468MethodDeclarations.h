﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromCoroutine>c__AnonStorey4C`1<System.Object>
struct U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Observable/<FromCoroutine>c__AnonStorey4C`1<System.Object>::.ctor()
extern "C"  void U3CFromCoroutineU3Ec__AnonStorey4C_1__ctor_m39769090_gshared (U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468 * __this, const MethodInfo* method);
#define U3CFromCoroutineU3Ec__AnonStorey4C_1__ctor_m39769090(__this, method) ((  void (*) (U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468 *, const MethodInfo*))U3CFromCoroutineU3Ec__AnonStorey4C_1__ctor_m39769090_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<FromCoroutine>c__AnonStorey4C`1<System.Object>::<>m__4A(UniRx.IObserver`1<T>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CFromCoroutineU3Ec__AnonStorey4C_1_U3CU3Em__4A_m3991795880_gshared (U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ____1, const MethodInfo* method);
#define U3CFromCoroutineU3Ec__AnonStorey4C_1_U3CU3Em__4A_m3991795880(__this, ___observer0, ____1, method) ((  Il2CppObject * (*) (U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))U3CFromCoroutineU3Ec__AnonStorey4C_1_U3CU3Em__4A_m3991795880_gshared)(__this, ___observer0, ____1, method)
