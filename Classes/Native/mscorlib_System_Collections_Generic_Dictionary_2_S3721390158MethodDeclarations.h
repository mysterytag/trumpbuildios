﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<SponsorPay.SPLogLevel,System.Int32>
struct ShimEnumerator_t3721390158;
// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct Dictionary_2_t2290665470;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m171786292_gshared (ShimEnumerator_t3721390158 * __this, Dictionary_2_t2290665470 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m171786292(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3721390158 *, Dictionary_2_t2290665470 *, const MethodInfo*))ShimEnumerator__ctor_m171786292_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<SponsorPay.SPLogLevel,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3812775473_gshared (ShimEnumerator_t3721390158 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3812775473(__this, method) ((  bool (*) (ShimEnumerator_t3721390158 *, const MethodInfo*))ShimEnumerator_MoveNext_m3812775473_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<SponsorPay.SPLogLevel,System.Int32>::get_Entry()
extern "C"  DictionaryEntry_t130027246  ShimEnumerator_get_Entry_m3299818009_gshared (ShimEnumerator_t3721390158 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3299818009(__this, method) ((  DictionaryEntry_t130027246  (*) (ShimEnumerator_t3721390158 *, const MethodInfo*))ShimEnumerator_get_Entry_m3299818009_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<SponsorPay.SPLogLevel,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3307900056_gshared (ShimEnumerator_t3721390158 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3307900056(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3721390158 *, const MethodInfo*))ShimEnumerator_get_Key_m3307900056_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<SponsorPay.SPLogLevel,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1664591146_gshared (ShimEnumerator_t3721390158 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1664591146(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3721390158 *, const MethodInfo*))ShimEnumerator_get_Value_m1664591146_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<SponsorPay.SPLogLevel,System.Int32>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3368948018_gshared (ShimEnumerator_t3721390158 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3368948018(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3721390158 *, const MethodInfo*))ShimEnumerator_get_Current_m3368948018_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<SponsorPay.SPLogLevel,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m3853446_gshared (ShimEnumerator_t3721390158 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3853446(__this, method) ((  void (*) (ShimEnumerator_t3721390158 *, const MethodInfo*))ShimEnumerator_Reset_m3853446_gshared)(__this, method)
