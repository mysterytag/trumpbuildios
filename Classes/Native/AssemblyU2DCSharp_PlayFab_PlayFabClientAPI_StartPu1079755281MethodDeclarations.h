﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/StartPurchaseRequestCallback
struct StartPurchaseRequestCallback_t1079755281;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.StartPurchaseRequest
struct StartPurchaseRequest_t2105128380;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartPurcha2105128380.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/StartPurchaseRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void StartPurchaseRequestCallback__ctor_m3528746624 (StartPurchaseRequestCallback_t1079755281 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/StartPurchaseRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.StartPurchaseRequest,System.Object)
extern "C"  void StartPurchaseRequestCallback_Invoke_m1843839935 (StartPurchaseRequestCallback_t1079755281 * __this, String_t* ___urlPath0, int32_t ___callId1, StartPurchaseRequest_t2105128380 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_StartPurchaseRequestCallback_t1079755281(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, StartPurchaseRequest_t2105128380 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/StartPurchaseRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.StartPurchaseRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StartPurchaseRequestCallback_BeginInvoke_m4265622600 (StartPurchaseRequestCallback_t1079755281 * __this, String_t* ___urlPath0, int32_t ___callId1, StartPurchaseRequest_t2105128380 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/StartPurchaseRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void StartPurchaseRequestCallback_EndInvoke_m1306466448 (StartPurchaseRequestCallback_t1079755281 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
