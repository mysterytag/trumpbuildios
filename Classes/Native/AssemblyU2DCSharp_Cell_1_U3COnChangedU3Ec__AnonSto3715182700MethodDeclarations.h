﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1/<OnChanged>c__AnonStoreyF3<System.DateTime>
struct U3COnChangedU3Ec__AnonStoreyF3_t3715182700;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.DateTime>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m1831197126_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3__ctor_m1831197126(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3__ctor_m1831197126_gshared)(__this, method)
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.DateTime>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m3162366270_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 * __this, DateTime_t339033936  ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m3162366270(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 *, DateTime_t339033936 , const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m3162366270_gshared)(__this, ____0, method)
