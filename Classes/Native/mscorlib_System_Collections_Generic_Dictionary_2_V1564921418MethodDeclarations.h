﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,OnePF.Purchase>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3090350558(__this, ___host0, method) ((  void (*) (Enumerator_t1564921419 *, Dictionary_2_t1797893477 *, const MethodInfo*))Enumerator__ctor_m76754913_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,OnePF.Purchase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19579021(__this, method) ((  Il2CppObject * (*) (Enumerator_t1564921419 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3118196448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,OnePF.Purchase>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m928473111(__this, method) ((  void (*) (Enumerator_t1564921419 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3702199860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,OnePF.Purchase>::Dispose()
#define Enumerator_Dispose_m2898074624(__this, method) ((  void (*) (Enumerator_t1564921419 *, const MethodInfo*))Enumerator_Dispose_m1628348611_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,OnePF.Purchase>::MoveNext()
#define Enumerator_MoveNext_m47107108(__this, method) ((  bool (*) (Enumerator_t1564921419 *, const MethodInfo*))Enumerator_MoveNext_m3556422944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,OnePF.Purchase>::get_Current()
#define Enumerator_get_Current_m780193209(__this, method) ((  Purchase_t160195573 * (*) (Enumerator_t1564921419 *, const MethodInfo*))Enumerator_get_Current_m841474402_gshared)(__this, method)
