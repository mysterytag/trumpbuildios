﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<UnityEngine.Vector2>
struct ReactiveProperty_1_t4133662826;
// UniRx.IObservable`1<UnityEngine.Vector2>
struct IObservable_1_t3284128152;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2>
struct IEqualityComparer_1_t1554629143;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m3508624908_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3508624908(__this, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, const MethodInfo*))ReactiveProperty_1__ctor_m3508624908_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m1393191122_gshared (ReactiveProperty_1_t4133662826 * __this, Vector2_t3525329788  ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1393191122(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, Vector2_t3525329788 , const MethodInfo*))ReactiveProperty_1__ctor_m1393191122_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m1935995627_gshared (ReactiveProperty_1_t4133662826 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1935995627(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m1935995627_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m770963907_gshared (ReactiveProperty_1_t4133662826 * __this, Il2CppObject* ___source0, Vector2_t3525329788  ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m770963907(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, Il2CppObject*, Vector2_t3525329788 , const MethodInfo*))ReactiveProperty_1__ctor_m770963907_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m911093537_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m911093537(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m911093537_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector2>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m333484169_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m333484169(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t4133662826 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m333484169_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.Vector2>::get_Value()
extern "C"  Vector2_t3525329788  ReactiveProperty_1_get_Value_m2822045105_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m2822045105(__this, method) ((  Vector2_t3525329788  (*) (ReactiveProperty_1_t4133662826 *, const MethodInfo*))ReactiveProperty_1_get_Value_m2822045105_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m327013408_gshared (ReactiveProperty_1_t4133662826 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m327013408(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, Vector2_t3525329788 , const MethodInfo*))ReactiveProperty_1_set_Value_m327013408_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m3282824599_gshared (ReactiveProperty_1_t4133662826 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m3282824599(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, Vector2_t3525329788 , const MethodInfo*))ReactiveProperty_1_SetValue_m3282824599_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m2516180762_gshared (ReactiveProperty_1_t4133662826 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m2516180762(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, Vector2_t3525329788 , const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m2516180762_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Vector2>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m1213422241_gshared (ReactiveProperty_1_t4133662826 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m1213422241(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t4133662826 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m1213422241_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m137492297_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m137492297(__this, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, const MethodInfo*))ReactiveProperty_1_Dispose_m137492297_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m3018701056_gshared (ReactiveProperty_1_t4133662826 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m3018701056(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t4133662826 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m3018701056_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.Vector2>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m3980354529_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m3980354529(__this, method) ((  String_t* (*) (ReactiveProperty_1_t4133662826 *, const MethodInfo*))ReactiveProperty_1_ToString_m3980354529_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Vector2>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m955929151_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m955929151(__this, method) ((  bool (*) (ReactiveProperty_1_t4133662826 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m955929151_gshared)(__this, method)
