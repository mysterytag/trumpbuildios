﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;
// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification`1/<ToObservable>c__AnonStorey55<System.Object>
struct  U3CToObservableU3Ec__AnonStorey55_t1984678244  : public Il2CppObject
{
public:
	// UniRx.IScheduler UniRx.Notification`1/<ToObservable>c__AnonStorey55::scheduler
	Il2CppObject * ___scheduler_0;
	// UniRx.Notification`1<T> UniRx.Notification`1/<ToObservable>c__AnonStorey55::<>f__this
	Notification_1_t38356375 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_scheduler_0() { return static_cast<int32_t>(offsetof(U3CToObservableU3Ec__AnonStorey55_t1984678244, ___scheduler_0)); }
	inline Il2CppObject * get_scheduler_0() const { return ___scheduler_0; }
	inline Il2CppObject ** get_address_of_scheduler_0() { return &___scheduler_0; }
	inline void set_scheduler_0(Il2CppObject * value)
	{
		___scheduler_0 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CToObservableU3Ec__AnonStorey55_t1984678244, ___U3CU3Ef__this_1)); }
	inline Notification_1_t38356375 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline Notification_1_t38356375 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(Notification_1_t38356375 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
