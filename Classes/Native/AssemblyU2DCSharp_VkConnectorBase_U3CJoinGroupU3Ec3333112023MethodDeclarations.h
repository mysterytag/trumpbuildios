﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VkConnectorBase/<JoinGroup>c__AnonStoreyD9
struct U3CJoinGroupU3Ec__AnonStoreyD9_t3333112023;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen3445990771.h"

// System.Void VkConnectorBase/<JoinGroup>c__AnonStoreyD9::.ctor()
extern "C"  void U3CJoinGroupU3Ec__AnonStoreyD9__ctor_m2257058430 (U3CJoinGroupU3Ec__AnonStoreyD9_t3333112023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase/<JoinGroup>c__AnonStoreyD9::<>m__E2(UniRx.Tuple`2<System.String,System.String>)
extern "C"  void U3CJoinGroupU3Ec__AnonStoreyD9_U3CU3Em__E2_m3889948944 (U3CJoinGroupU3Ec__AnonStoreyD9_t3333112023 * __this, Tuple_2_t3445990771  ___tuple0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
