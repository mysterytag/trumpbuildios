﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t1522972100;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;
// UniRx.IObserver`1<UnityEngine.AssetBundle>
struct IObserver_1_t1876462710;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23
struct  U3CFetchAssetBundleU3Ec__Iterator23_t3024943464  : public Il2CppObject
{
public:
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::www
	WWW_t1522972100 * ___www_0;
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::<$s_345>__0
	WWW_t1522972100 * ___U3CU24s_345U3E__0_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::reportProgress
	Il2CppObject* ___reportProgress_2;
	// UniRx.CancellationToken UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::cancel
	CancellationToken_t1439151560  ___cancel_3;
	// UniRx.IObserver`1<UnityEngine.AssetBundle> UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::observer
	Il2CppObject* ___observer_4;
	// System.Exception UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::<ex>__1
	Exception_t1967233988 * ___U3CexU3E__1_5;
	// System.Exception UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::<ex>__2
	Exception_t1967233988 * ___U3CexU3E__2_6;
	// System.Int32 UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::$PC
	int32_t ___U24PC_7;
	// System.Object UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::$current
	Il2CppObject * ___U24current_8;
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::<$>www
	WWW_t1522972100 * ___U3CU24U3Ewww_9;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::<$>reportProgress
	Il2CppObject* ___U3CU24U3EreportProgress_10;
	// UniRx.CancellationToken UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::<$>cancel
	CancellationToken_t1439151560  ___U3CU24U3Ecancel_11;
	// UniRx.IObserver`1<UnityEngine.AssetBundle> UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator23::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_12;

public:
	inline static int32_t get_offset_of_www_0() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___www_0)); }
	inline WWW_t1522972100 * get_www_0() const { return ___www_0; }
	inline WWW_t1522972100 ** get_address_of_www_0() { return &___www_0; }
	inline void set_www_0(WWW_t1522972100 * value)
	{
		___www_0 = value;
		Il2CppCodeGenWriteBarrier(&___www_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_345U3E__0_1() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U3CU24s_345U3E__0_1)); }
	inline WWW_t1522972100 * get_U3CU24s_345U3E__0_1() const { return ___U3CU24s_345U3E__0_1; }
	inline WWW_t1522972100 ** get_address_of_U3CU24s_345U3E__0_1() { return &___U3CU24s_345U3E__0_1; }
	inline void set_U3CU24s_345U3E__0_1(WWW_t1522972100 * value)
	{
		___U3CU24s_345U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_345U3E__0_1, value);
	}

	inline static int32_t get_offset_of_reportProgress_2() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___reportProgress_2)); }
	inline Il2CppObject* get_reportProgress_2() const { return ___reportProgress_2; }
	inline Il2CppObject** get_address_of_reportProgress_2() { return &___reportProgress_2; }
	inline void set_reportProgress_2(Il2CppObject* value)
	{
		___reportProgress_2 = value;
		Il2CppCodeGenWriteBarrier(&___reportProgress_2, value);
	}

	inline static int32_t get_offset_of_cancel_3() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___cancel_3)); }
	inline CancellationToken_t1439151560  get_cancel_3() const { return ___cancel_3; }
	inline CancellationToken_t1439151560 * get_address_of_cancel_3() { return &___cancel_3; }
	inline void set_cancel_3(CancellationToken_t1439151560  value)
	{
		___cancel_3 = value;
	}

	inline static int32_t get_offset_of_observer_4() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___observer_4)); }
	inline Il2CppObject* get_observer_4() const { return ___observer_4; }
	inline Il2CppObject** get_address_of_observer_4() { return &___observer_4; }
	inline void set_observer_4(Il2CppObject* value)
	{
		___observer_4 = value;
		Il2CppCodeGenWriteBarrier(&___observer_4, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__1_5() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U3CexU3E__1_5)); }
	inline Exception_t1967233988 * get_U3CexU3E__1_5() const { return ___U3CexU3E__1_5; }
	inline Exception_t1967233988 ** get_address_of_U3CexU3E__1_5() { return &___U3CexU3E__1_5; }
	inline void set_U3CexU3E__1_5(Exception_t1967233988 * value)
	{
		___U3CexU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__2_6() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U3CexU3E__2_6)); }
	inline Exception_t1967233988 * get_U3CexU3E__2_6() const { return ___U3CexU3E__2_6; }
	inline Exception_t1967233988 ** get_address_of_U3CexU3E__2_6() { return &___U3CexU3E__2_6; }
	inline void set_U3CexU3E__2_6(Exception_t1967233988 * value)
	{
		___U3CexU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__2_6, value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ewww_9() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U3CU24U3Ewww_9)); }
	inline WWW_t1522972100 * get_U3CU24U3Ewww_9() const { return ___U3CU24U3Ewww_9; }
	inline WWW_t1522972100 ** get_address_of_U3CU24U3Ewww_9() { return &___U3CU24U3Ewww_9; }
	inline void set_U3CU24U3Ewww_9(WWW_t1522972100 * value)
	{
		___U3CU24U3Ewww_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ewww_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EreportProgress_10() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U3CU24U3EreportProgress_10)); }
	inline Il2CppObject* get_U3CU24U3EreportProgress_10() const { return ___U3CU24U3EreportProgress_10; }
	inline Il2CppObject** get_address_of_U3CU24U3EreportProgress_10() { return &___U3CU24U3EreportProgress_10; }
	inline void set_U3CU24U3EreportProgress_10(Il2CppObject* value)
	{
		___U3CU24U3EreportProgress_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EreportProgress_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecancel_11() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U3CU24U3Ecancel_11)); }
	inline CancellationToken_t1439151560  get_U3CU24U3Ecancel_11() const { return ___U3CU24U3Ecancel_11; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3Ecancel_11() { return &___U3CU24U3Ecancel_11; }
	inline void set_U3CU24U3Ecancel_11(CancellationToken_t1439151560  value)
	{
		___U3CU24U3Ecancel_11 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_12() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator23_t3024943464, ___U3CU24U3Eobserver_12)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_12() const { return ___U3CU24U3Eobserver_12; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_12() { return &___U3CU24U3Eobserver_12; }
	inline void set_U3CU24U3Eobserver_12(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
