﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WallPostWindow
struct WallPostWindow_t251424314;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void WallPostWindow::.ctor()
extern "C"  void WallPostWindow__ctor_m1507377057 (WallPostWindow_t251424314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallPostWindow::PostInject()
extern "C"  void WallPostWindow_PostInject_m1198322196 (WallPostWindow_t251424314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallPostWindow::AraWallpost()
extern "C"  void WallPostWindow_AraWallpost_m857084825 (WallPostWindow_t251424314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallPostWindow::Closes()
extern "C"  void WallPostWindow_Closes_m981158846 (WallPostWindow_t251424314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallPostWindow::<PostInject>m__280(UniRx.Unit)
extern "C"  void WallPostWindow_U3CPostInjectU3Em__280_m566727285 (WallPostWindow_t251424314 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallPostWindow::<PostInject>m__281(UniRx.Unit)
extern "C"  void WallPostWindow_U3CPostInjectU3Em__281_m273324278 (WallPostWindow_t251424314 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallPostWindow::<AraWallpost>m__282(System.Int64)
extern "C"  void WallPostWindow_U3CAraWallpostU3Em__282_m4103751920 (WallPostWindow_t251424314 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
