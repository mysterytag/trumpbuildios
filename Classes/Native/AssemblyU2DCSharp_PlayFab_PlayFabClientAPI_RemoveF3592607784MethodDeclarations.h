﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RemoveFriendResponseCallback
struct RemoveFriendResponseCallback_t3592607784;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RemoveFriendRequest
struct RemoveFriendRequest_t4041798621;
// PlayFab.ClientModels.RemoveFriendResult
struct RemoveFriendResult_t3352117039;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveFrien4041798621.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveFrien3352117039.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RemoveFriendResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RemoveFriendResponseCallback__ctor_m2161933591 (RemoveFriendResponseCallback_t3592607784 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RemoveFriendResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RemoveFriendRequest,PlayFab.ClientModels.RemoveFriendResult,PlayFab.PlayFabError,System.Object)
extern "C"  void RemoveFriendResponseCallback_Invoke_m2862645604 (RemoveFriendResponseCallback_t3592607784 * __this, String_t* ___urlPath0, int32_t ___callId1, RemoveFriendRequest_t4041798621 * ___request2, RemoveFriendResult_t3352117039 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RemoveFriendResponseCallback_t3592607784(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RemoveFriendRequest_t4041798621 * ___request2, RemoveFriendResult_t3352117039 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RemoveFriendResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RemoveFriendRequest,PlayFab.ClientModels.RemoveFriendResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RemoveFriendResponseCallback_BeginInvoke_m2980789649 (RemoveFriendResponseCallback_t3592607784 * __this, String_t* ___urlPath0, int32_t ___callId1, RemoveFriendRequest_t4041798621 * ___request2, RemoveFriendResult_t3352117039 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RemoveFriendResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RemoveFriendResponseCallback_EndInvoke_m3761246887 (RemoveFriendResponseCallback_t3592607784 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
