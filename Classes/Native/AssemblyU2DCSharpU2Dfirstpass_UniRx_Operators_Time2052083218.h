﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TimeoutObservable`1<System.Object>
struct TimeoutObservable_1_t1895307051;
// System.Object
struct Il2CppObject;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>
struct  Timeout_t2052083218  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.TimeoutObservable`1<T> UniRx.Operators.TimeoutObservable`1/Timeout::parent
	TimeoutObservable_1_t1895307051 * ___parent_2;
	// System.Object UniRx.Operators.TimeoutObservable`1/Timeout::gate
	Il2CppObject * ___gate_3;
	// System.UInt64 UniRx.Operators.TimeoutObservable`1/Timeout::objectId
	uint64_t ___objectId_4;
	// System.Boolean UniRx.Operators.TimeoutObservable`1/Timeout::isTimeout
	bool ___isTimeout_5;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.TimeoutObservable`1/Timeout::sourceSubscription
	SingleAssignmentDisposable_t2336378823 * ___sourceSubscription_6;
	// UniRx.SerialDisposable UniRx.Operators.TimeoutObservable`1/Timeout::timerSubscription
	SerialDisposable_t2547852742 * ___timerSubscription_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Timeout_t2052083218, ___parent_2)); }
	inline TimeoutObservable_1_t1895307051 * get_parent_2() const { return ___parent_2; }
	inline TimeoutObservable_1_t1895307051 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(TimeoutObservable_1_t1895307051 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Timeout_t2052083218, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_objectId_4() { return static_cast<int32_t>(offsetof(Timeout_t2052083218, ___objectId_4)); }
	inline uint64_t get_objectId_4() const { return ___objectId_4; }
	inline uint64_t* get_address_of_objectId_4() { return &___objectId_4; }
	inline void set_objectId_4(uint64_t value)
	{
		___objectId_4 = value;
	}

	inline static int32_t get_offset_of_isTimeout_5() { return static_cast<int32_t>(offsetof(Timeout_t2052083218, ___isTimeout_5)); }
	inline bool get_isTimeout_5() const { return ___isTimeout_5; }
	inline bool* get_address_of_isTimeout_5() { return &___isTimeout_5; }
	inline void set_isTimeout_5(bool value)
	{
		___isTimeout_5 = value;
	}

	inline static int32_t get_offset_of_sourceSubscription_6() { return static_cast<int32_t>(offsetof(Timeout_t2052083218, ___sourceSubscription_6)); }
	inline SingleAssignmentDisposable_t2336378823 * get_sourceSubscription_6() const { return ___sourceSubscription_6; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_sourceSubscription_6() { return &___sourceSubscription_6; }
	inline void set_sourceSubscription_6(SingleAssignmentDisposable_t2336378823 * value)
	{
		___sourceSubscription_6 = value;
		Il2CppCodeGenWriteBarrier(&___sourceSubscription_6, value);
	}

	inline static int32_t get_offset_of_timerSubscription_7() { return static_cast<int32_t>(offsetof(Timeout_t2052083218, ___timerSubscription_7)); }
	inline SerialDisposable_t2547852742 * get_timerSubscription_7() const { return ___timerSubscription_7; }
	inline SerialDisposable_t2547852742 ** get_address_of_timerSubscription_7() { return &___timerSubscription_7; }
	inline void set_timerSubscription_7(SerialDisposable_t2547852742 * value)
	{
		___timerSubscription_7 = value;
		Il2CppCodeGenWriteBarrier(&___timerSubscription_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
