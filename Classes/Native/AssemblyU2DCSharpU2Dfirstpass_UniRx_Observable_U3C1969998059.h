﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<TimerFrame>c__AnonStorey53
struct  U3CTimerFrameU3Ec__AnonStorey53_t1969998059  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Observable/<TimerFrame>c__AnonStorey53::dueTimeFrameCount
	int32_t ___dueTimeFrameCount_0;
	// UniRx.FrameCountType UniRx.Observable/<TimerFrame>c__AnonStorey53::frameCountType
	int32_t ___frameCountType_1;

public:
	inline static int32_t get_offset_of_dueTimeFrameCount_0() { return static_cast<int32_t>(offsetof(U3CTimerFrameU3Ec__AnonStorey53_t1969998059, ___dueTimeFrameCount_0)); }
	inline int32_t get_dueTimeFrameCount_0() const { return ___dueTimeFrameCount_0; }
	inline int32_t* get_address_of_dueTimeFrameCount_0() { return &___dueTimeFrameCount_0; }
	inline void set_dueTimeFrameCount_0(int32_t value)
	{
		___dueTimeFrameCount_0 = value;
	}

	inline static int32_t get_offset_of_frameCountType_1() { return static_cast<int32_t>(offsetof(U3CTimerFrameU3Ec__AnonStorey53_t1969998059, ___frameCountType_1)); }
	inline int32_t get_frameCountType_1() const { return ___frameCountType_1; }
	inline int32_t* get_address_of_frameCountType_1() { return &___frameCountType_1; }
	inline void set_frameCountType_1(int32_t value)
	{
		___frameCountType_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
