﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardResult
struct GetLeaderboardResult_t2566099316;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>
struct List_1_t3038850335;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetLeaderboardResult::.ctor()
extern "C"  void GetLeaderboardResult__ctor_m1790106697 (GetLeaderboardResult_t2566099316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetLeaderboardResult_get_Leaderboard_m3503071248 (GetLeaderboardResult_t2566099316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetLeaderboardResult_set_Leaderboard_m3945022495 (GetLeaderboardResult_t2566099316 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
