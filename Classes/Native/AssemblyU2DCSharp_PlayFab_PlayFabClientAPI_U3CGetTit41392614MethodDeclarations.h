﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetTitleNews>c__AnonStorey9E
struct U3CGetTitleNewsU3Ec__AnonStorey9E_t41392614;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetTitleNews>c__AnonStorey9E::.ctor()
extern "C"  void U3CGetTitleNewsU3Ec__AnonStorey9E__ctor_m3008859277 (U3CGetTitleNewsU3Ec__AnonStorey9E_t41392614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetTitleNews>c__AnonStorey9E::<>m__8B(PlayFab.CallRequestContainer)
extern "C"  void U3CGetTitleNewsU3Ec__AnonStorey9E_U3CU3Em__8B_m2526904373 (U3CGetTitleNewsU3Ec__AnonStorey9E_t41392614 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
