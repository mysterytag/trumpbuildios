﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1163962996MethodDeclarations.h"

// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::.ctor(PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
#define ThreadSafeDictionary_2__ctor_m4262806326(__this, ___valueFactory0, method) ((  void (*) (ThreadSafeDictionary_2_t3525035192 *, ThreadSafeDictionaryValueFactory_2_t4132158723 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m145995086_gshared)(__this, ___valueFactory0, method)
// System.Collections.IEnumerator PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3662016677(__this, method) ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t3525035192 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m647792317_gshared)(__this, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m284100869(__this, ___key0, method) ((  ConstructorDelegate_t4072949631 * (*) (ThreadSafeDictionary_2_t3525035192 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m3585103853_gshared)(__this, ___key0, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m2156958893(__this, ___key0, method) ((  ConstructorDelegate_t4072949631 * (*) (ThreadSafeDictionary_2_t3525035192 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m844388037_gshared)(__this, ___key0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m374104313(__this, ___key0, ___value1, method) ((  void (*) (ThreadSafeDictionary_2_t3525035192 *, Type_t *, ConstructorDelegate_t4072949631 *, const MethodInfo*))ThreadSafeDictionary_2_Add_m3884150033_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::ContainsKey(TKey)
#define ThreadSafeDictionary_2_ContainsKey_m615721603(__this, ___key0, method) ((  bool (*) (ThreadSafeDictionary_2_t3525035192 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_ContainsKey_m2111272811_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m1431798248(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t3525035192 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m79078016_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::Remove(TKey)
#define ThreadSafeDictionary_2_Remove_m1856456845(__this, ___key0, method) ((  bool (*) (ThreadSafeDictionary_2_t3525035192 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m3287321253_gshared)(__this, ___key0, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m3933884380(__this, ___key0, ___value1, method) ((  bool (*) (ThreadSafeDictionary_2_t3525035192 *, Type_t *, ConstructorDelegate_t4072949631 **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m2392274116_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m1055372932(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t3525035192 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m2528035356_gshared)(__this, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m2441351353(__this, ___key0, method) ((  ConstructorDelegate_t4072949631 * (*) (ThreadSafeDictionary_2_t3525035192 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m1128780497_gshared)(__this, ___key0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m1645483004(__this, ___key0, ___value1, method) ((  void (*) (ThreadSafeDictionary_2_t3525035192 *, Type_t *, ConstructorDelegate_t4072949631 *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m1582880484_gshared)(__this, ___key0, ___value1, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m3556083660(__this, ___item0, method) ((  void (*) (ThreadSafeDictionary_2_t3525035192 *, KeyValuePair_2_t1379061348 , const MethodInfo*))ThreadSafeDictionary_2_Add_m1635310772_gshared)(__this, ___item0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::Clear()
#define ThreadSafeDictionary_2_Clear_m2525229927(__this, method) ((  void (*) (ThreadSafeDictionary_2_t3525035192 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m3600968783_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m849798404(__this, ___item0, method) ((  bool (*) (ThreadSafeDictionary_2_t3525035192 *, KeyValuePair_2_t1379061348 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m78891804_gshared)(__this, ___item0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m723819696(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ThreadSafeDictionary_2_t3525035192 *, KeyValuePair_2U5BU5D_t4122597261*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m3361965976_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m473393230(__this, method) ((  int32_t (*) (ThreadSafeDictionary_2_t3525035192 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m187477558_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m3003415177(__this, method) ((  bool (*) (ThreadSafeDictionary_2_t3525035192 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m3744395425_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m770866473(__this, ___item0, method) ((  bool (*) (ThreadSafeDictionary_2_t3525035192 *, KeyValuePair_2_t1379061348 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m4054976833_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,PlayFab.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m1081561551(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t3525035192 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m2011494711_gshared)(__this, method)
