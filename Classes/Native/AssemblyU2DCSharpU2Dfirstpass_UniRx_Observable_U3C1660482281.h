﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.AsyncSubject`1<System.Object>
struct AsyncSubject_1_t1536745524;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<System.Object>
struct  U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281  : public Il2CppObject
{
public:
	// UniRx.AsyncSubject`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1::subject
	AsyncSubject_1_t1536745524 * ___subject_0;
	// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1::<>f__ref$63
	U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 * ___U3CU3Ef__refU2463_1;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281, ___subject_0)); }
	inline AsyncSubject_1_t1536745524 * get_subject_0() const { return ___subject_0; }
	inline AsyncSubject_1_t1536745524 ** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(AsyncSubject_1_t1536745524 * value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier(&___subject_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2463_1() { return static_cast<int32_t>(offsetof(U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281, ___U3CU3Ef__refU2463_1)); }
	inline U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 * get_U3CU3Ef__refU2463_1() const { return ___U3CU3Ef__refU2463_1; }
	inline U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 ** get_address_of_U3CU3Ef__refU2463_1() { return &___U3CU3Ef__refU2463_1; }
	inline void set_U3CU3Ef__refU2463_1(U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 * value)
	{
		___U3CU3Ef__refU2463_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2463_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
