﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::.ctor()
#define Subject_1__ctor_m221326712(__this, method) ((  void (*) (Subject_1_t3829190342 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::get_HasObservers()
#define Subject_1_get_HasObservers_m2864506204(__this, method) ((  bool (*) (Subject_1_t3829190342 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::OnCompleted()
#define Subject_1_OnCompleted_m1504864002(__this, method) ((  void (*) (Subject_1_t3829190342 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::OnError(System.Exception)
#define Subject_1_OnError_m268477743(__this, ___error0, method) ((  void (*) (Subject_1_t3829190342 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::OnNext(T)
#define Subject_1_OnNext_m777308704(__this, ___value0, method) ((  void (*) (Subject_1_t3829190342 *, LogEntry_t1891155722 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m319187149(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3829190342 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::Dispose()
#define Subject_1_Dispose_m2139855797(__this, method) ((  void (*) (Subject_1_t3829190342 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m390119486(__this, method) ((  void (*) (Subject_1_t3829190342 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Diagnostics.LogEntry>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m458568531(__this, method) ((  bool (*) (Subject_1_t3829190342 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
