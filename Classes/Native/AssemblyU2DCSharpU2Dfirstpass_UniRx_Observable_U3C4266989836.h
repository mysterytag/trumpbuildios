﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<System.Object,System.Object>
struct  U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836  : public Il2CppObject
{
public:
	// System.TimeSpan UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2::delay
	TimeSpan_t763862892  ___delay_0;
	// UniRx.IObservable`1<TSource> UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2::source
	Il2CppObject* ___source_1;
	// System.Action`1<TException> UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2::onError
	Action_1_t985559125 * ___onError_2;
	// System.Int32 UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2::retryCount
	int32_t ___retryCount_3;
	// UniRx.IScheduler UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2::delayScheduler
	Il2CppObject * ___delayScheduler_4;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836, ___delay_0)); }
	inline TimeSpan_t763862892  get_delay_0() const { return ___delay_0; }
	inline TimeSpan_t763862892 * get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(TimeSpan_t763862892  value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_onError_2() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836, ___onError_2)); }
	inline Action_1_t985559125 * get_onError_2() const { return ___onError_2; }
	inline Action_1_t985559125 ** get_address_of_onError_2() { return &___onError_2; }
	inline void set_onError_2(Action_1_t985559125 * value)
	{
		___onError_2 = value;
		Il2CppCodeGenWriteBarrier(&___onError_2, value);
	}

	inline static int32_t get_offset_of_retryCount_3() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836, ___retryCount_3)); }
	inline int32_t get_retryCount_3() const { return ___retryCount_3; }
	inline int32_t* get_address_of_retryCount_3() { return &___retryCount_3; }
	inline void set_retryCount_3(int32_t value)
	{
		___retryCount_3 = value;
	}

	inline static int32_t get_offset_of_delayScheduler_4() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836, ___delayScheduler_4)); }
	inline Il2CppObject * get_delayScheduler_4() const { return ___delayScheduler_4; }
	inline Il2CppObject ** get_address_of_delayScheduler_4() { return &___delayScheduler_4; }
	inline void set_delayScheduler_4(Il2CppObject * value)
	{
		___delayScheduler_4 = value;
		Il2CppCodeGenWriteBarrier(&___delayScheduler_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
