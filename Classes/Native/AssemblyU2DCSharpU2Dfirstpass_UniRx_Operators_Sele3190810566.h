﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Int64,System.Object>
struct SelectManyOuterObserver_t29170705;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1929422447.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Int64,System.Object>
struct  SelectMany_t3190810566  : public OperatorObserverBase_2_t1929422447
{
public:
	// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<TSource,TCollection,TResult> UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany::parent
	SelectManyOuterObserver_t29170705 * ___parent_2;
	// TSource UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany::sourceValue
	Il2CppObject * ___sourceValue_3;
	// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany::cancel
	Il2CppObject * ___cancel_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectMany_t3190810566, ___parent_2)); }
	inline SelectManyOuterObserver_t29170705 * get_parent_2() const { return ___parent_2; }
	inline SelectManyOuterObserver_t29170705 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyOuterObserver_t29170705 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_sourceValue_3() { return static_cast<int32_t>(offsetof(SelectMany_t3190810566, ___sourceValue_3)); }
	inline Il2CppObject * get_sourceValue_3() const { return ___sourceValue_3; }
	inline Il2CppObject ** get_address_of_sourceValue_3() { return &___sourceValue_3; }
	inline void set_sourceValue_3(Il2CppObject * value)
	{
		___sourceValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___sourceValue_3, value);
	}

	inline static int32_t get_offset_of_cancel_4() { return static_cast<int32_t>(offsetof(SelectMany_t3190810566, ___cancel_4)); }
	inline Il2CppObject * get_cancel_4() const { return ___cancel_4; }
	inline Il2CppObject ** get_address_of_cancel_4() { return &___cancel_4; }
	inline void set_cancel_4(Il2CppObject * value)
	{
		___cancel_4 = value;
		Il2CppCodeGenWriteBarrier(&___cancel_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
