﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType0`2<System.Object,System.Object>
struct U3CU3E__AnonType0_2_t2589238341;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType0`2<System.Object,System.Object>::.ctor(<google>__T,<bing>__T)
extern "C"  void U3CU3E__AnonType0_2__ctor_m1360322370_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, Il2CppObject * ___google0, Il2CppObject * ___bing1, const MethodInfo* method);
#define U3CU3E__AnonType0_2__ctor_m1360322370(__this, ___google0, ___bing1, method) ((  void (*) (U3CU3E__AnonType0_2_t2589238341 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_2__ctor_m1360322370_gshared)(__this, ___google0, ___bing1, method)
// <google>__T <>__AnonType0`2<System.Object,System.Object>::get_google()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_google_m2704299663_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_get_google_m2704299663(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_2_t2589238341 *, const MethodInfo*))U3CU3E__AnonType0_2_get_google_m2704299663_gshared)(__this, method)
// <bing>__T <>__AnonType0`2<System.Object,System.Object>::get_bing()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_bing_m3212623407_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_get_bing_m3212623407(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_2_t2589238341 *, const MethodInfo*))U3CU3E__AnonType0_2_get_bing_m3212623407_gshared)(__this, method)
// System.Boolean <>__AnonType0`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_2_Equals_m1009921940_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType0_2_Equals_m1009921940(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType0_2_t2589238341 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_2_Equals_m1009921940_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType0`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_2_GetHashCode_m903781560_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_GetHashCode_m903781560(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_2_t2589238341 *, const MethodInfo*))U3CU3E__AnonType0_2_GetHashCode_m903781560_gshared)(__this, method)
// System.String <>__AnonType0`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType0_2_ToString_m781886964_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_2_ToString_m781886964(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_2_t2589238341 *, const MethodInfo*))U3CU3E__AnonType0_2_ToString_m781886964_gshared)(__this, method)
