﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadStringCompletedEventArgs
struct UploadStringCompletedEventArgs_t4015562024;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.UploadStringCompletedEventArgs::.ctor(System.String,System.Exception,System.Boolean,System.Object)
extern "C"  void UploadStringCompletedEventArgs__ctor_m803554843 (UploadStringCompletedEventArgs_t4015562024 * __this, String_t* ___result0, Exception_t1967233988 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.UploadStringCompletedEventArgs::get_Result()
extern "C"  String_t* UploadStringCompletedEventArgs_get_Result_m3472022635 (UploadStringCompletedEventArgs_t4015562024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
