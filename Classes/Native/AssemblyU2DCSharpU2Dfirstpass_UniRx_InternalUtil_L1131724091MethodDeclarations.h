﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<System.Object>
struct ListObserver_1_t1131724091;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Object>>
struct ImmutableList_1_t4109309822;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.InternalUtil.ListObserver`1<System.Object>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1171659873_gshared (ListObserver_1_t1131724091 * __this, ImmutableList_1_t4109309822 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m1171659873(__this, ___observers0, method) ((  void (*) (ListObserver_1_t1131724091 *, ImmutableList_1_t4109309822 *, const MethodInfo*))ListObserver_1__ctor_m1171659873_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Object>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1312264409_gshared (ListObserver_1_t1131724091 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m1312264409(__this, method) ((  void (*) (ListObserver_1_t1131724091 *, const MethodInfo*))ListObserver_1_OnCompleted_m1312264409_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2971321478_gshared (ListObserver_1_t1131724091 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2971321478(__this, ___error0, method) ((  void (*) (ListObserver_1_t1131724091 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2971321478_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Object>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1823373175_gshared (ListObserver_1_t1131724091 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1823373175(__this, ___value0, method) ((  void (*) (ListObserver_1_t1131724091 *, Il2CppObject *, const MethodInfo*))ListObserver_1_OnNext_m1823373175_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Object>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1317440265_gshared (ListObserver_1_t1131724091 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1317440265(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1317440265_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Object>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m885229474_gshared (ListObserver_1_t1131724091 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m885229474(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m885229474_gshared)(__this, ___observer0, method)
