﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>
struct DoOnCancel_t3043692509;
// UniRx.Operators.DoOnCancelObservable`1<System.Object>
struct DoOnCancelObservable_1_t3202318454;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::.ctor(UniRx.Operators.DoOnCancelObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnCancel__ctor_m207837736_gshared (DoOnCancel_t3043692509 * __this, DoOnCancelObservable_1_t3202318454 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DoOnCancel__ctor_m207837736(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DoOnCancel_t3043692509 *, DoOnCancelObservable_1_t3202318454 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnCancel__ctor_m207837736_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::Run()
extern "C"  Il2CppObject * DoOnCancel_Run_m3849669381_gshared (DoOnCancel_t3043692509 * __this, const MethodInfo* method);
#define DoOnCancel_Run_m3849669381(__this, method) ((  Il2CppObject * (*) (DoOnCancel_t3043692509 *, const MethodInfo*))DoOnCancel_Run_m3849669381_gshared)(__this, method)
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::OnNext(T)
extern "C"  void DoOnCancel_OnNext_m291764639_gshared (DoOnCancel_t3043692509 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DoOnCancel_OnNext_m291764639(__this, ___value0, method) ((  void (*) (DoOnCancel_t3043692509 *, Il2CppObject *, const MethodInfo*))DoOnCancel_OnNext_m291764639_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::OnError(System.Exception)
extern "C"  void DoOnCancel_OnError_m689370798_gshared (DoOnCancel_t3043692509 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DoOnCancel_OnError_m689370798(__this, ___error0, method) ((  void (*) (DoOnCancel_t3043692509 *, Exception_t1967233988 *, const MethodInfo*))DoOnCancel_OnError_m689370798_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::OnCompleted()
extern "C"  void DoOnCancel_OnCompleted_m3129982721_gshared (DoOnCancel_t3043692509 * __this, const MethodInfo* method);
#define DoOnCancel_OnCompleted_m3129982721(__this, method) ((  void (*) (DoOnCancel_t3043692509 *, const MethodInfo*))DoOnCancel_OnCompleted_m3129982721_gshared)(__this, method)
// System.Void UniRx.Operators.DoOnCancelObservable`1/DoOnCancel<System.Object>::<Run>m__7B()
extern "C"  void DoOnCancel_U3CRunU3Em__7B_m3066567414_gshared (DoOnCancel_t3043692509 * __this, const MethodInfo* method);
#define DoOnCancel_U3CRunU3Em__7B_m3066567414(__this, method) ((  void (*) (DoOnCancel_t3043692509 *, const MethodInfo*))DoOnCancel_U3CRunU3Em__7B_m3066567414_gshared)(__this, method)
