﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsRequest
struct GetPlayFabIDsFromGameCenterIDsRequest_t1196231507;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromGameCenterIDsRequest__ctor_m1283204406 (GetPlayFabIDsFromGameCenterIDsRequest_t1196231507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsRequest::get_GameCenterIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromGameCenterIDsRequest_get_GameCenterIDs_m3524220683 (GetPlayFabIDsFromGameCenterIDsRequest_t1196231507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsRequest::set_GameCenterIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromGameCenterIDsRequest_set_GameCenterIDs_m3488186972 (GetPlayFabIDsFromGameCenterIDsRequest_t1196231507 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
