﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType4_2_gen3892302468MethodDeclarations.h"

// System.Void <>__AnonType4`2<System.Single,System.String>::.ctor(<f>__T,<valuta>__T)
#define U3CU3E__AnonType4_2__ctor_m4023610307(__this, ___f0, ___valuta1, method) ((  void (*) (U3CU3E__AnonType4_2_t4023684950 *, float, String_t*, const MethodInfo*))U3CU3E__AnonType4_2__ctor_m2661760497_gshared)(__this, ___f0, ___valuta1, method)
// <f>__T <>__AnonType4`2<System.Single,System.String>::get_f()
#define U3CU3E__AnonType4_2_get_f_m2193790632(__this, method) ((  float (*) (U3CU3E__AnonType4_2_t4023684950 *, const MethodInfo*))U3CU3E__AnonType4_2_get_f_m487471190_gshared)(__this, method)
// <valuta>__T <>__AnonType4`2<System.Single,System.String>::get_valuta()
#define U3CU3E__AnonType4_2_get_valuta_m2576865988(__this, method) ((  String_t* (*) (U3CU3E__AnonType4_2_t4023684950 *, const MethodInfo*))U3CU3E__AnonType4_2_get_valuta_m2605979094_gshared)(__this, method)
// System.Boolean <>__AnonType4`2<System.Single,System.String>::Equals(System.Object)
#define U3CU3E__AnonType4_2_Equals_m548360167(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType4_2_t4023684950 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType4_2_Equals_m1823435029_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType4`2<System.Single,System.String>::GetHashCode()
#define U3CU3E__AnonType4_2_GetHashCode_m4208114955(__this, method) ((  int32_t (*) (U3CU3E__AnonType4_2_t4023684950 *, const MethodInfo*))U3CU3E__AnonType4_2_GetHashCode_m815653945_gshared)(__this, method)
// System.String <>__AnonType4`2<System.Single,System.String>::ToString()
#define U3CU3E__AnonType4_2_ToString_m2962486633(__this, method) ((  String_t* (*) (U3CU3E__AnonType4_2_t4023684950 *, const MethodInfo*))U3CU3E__AnonType4_2_ToString_m937938171_gshared)(__this, method)
