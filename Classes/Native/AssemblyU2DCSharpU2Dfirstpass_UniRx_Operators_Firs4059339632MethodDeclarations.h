﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First_<System.Object>
struct First__t4059339632;
// UniRx.Operators.FirstObservable`1<System.Object>
struct FirstObservable_1_t2859660834;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First_<System.Object>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m2754608375_gshared (First__t4059339632 * __this, FirstObservable_1_t2859660834 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First___ctor_m2754608375(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First__t4059339632 *, FirstObservable_1_t2859660834 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First___ctor_m2754608375_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Object>::OnNext(T)
extern "C"  void First__OnNext_m3676607188_gshared (First__t4059339632 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define First__OnNext_m3676607188(__this, ___value0, method) ((  void (*) (First__t4059339632 *, Il2CppObject *, const MethodInfo*))First__OnNext_m3676607188_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Object>::OnError(System.Exception)
extern "C"  void First__OnError_m2880472547_gshared (First__t4059339632 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First__OnError_m2880472547(__this, ___error0, method) ((  void (*) (First__t4059339632 *, Exception_t1967233988 *, const MethodInfo*))First__OnError_m2880472547_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Object>::OnCompleted()
extern "C"  void First__OnCompleted_m323401142_gshared (First__t4059339632 * __this, const MethodInfo* method);
#define First__OnCompleted_m323401142(__this, method) ((  void (*) (First__t4059339632 *, const MethodInfo*))First__OnCompleted_m323401142_gshared)(__this, method)
