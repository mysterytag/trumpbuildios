﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>
struct SkipWhile__t3820853950;
// UniRx.Operators.SkipWhileObservable`1<System.Object>
struct SkipWhileObservable_1_t2542074388;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::.ctor(UniRx.Operators.SkipWhileObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SkipWhile___ctor_m3293816885_gshared (SkipWhile__t3820853950 * __this, SkipWhileObservable_1_t2542074388 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SkipWhile___ctor_m3293816885(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SkipWhile__t3820853950 *, SkipWhileObservable_1_t2542074388 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SkipWhile___ctor_m3293816885_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::Run()
extern "C"  Il2CppObject * SkipWhile__Run_m3489040716_gshared (SkipWhile__t3820853950 * __this, const MethodInfo* method);
#define SkipWhile__Run_m3489040716(__this, method) ((  Il2CppObject * (*) (SkipWhile__t3820853950 *, const MethodInfo*))SkipWhile__Run_m3489040716_gshared)(__this, method)
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::OnNext(T)
extern "C"  void SkipWhile__OnNext_m1916940496_gshared (SkipWhile__t3820853950 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SkipWhile__OnNext_m1916940496(__this, ___value0, method) ((  void (*) (SkipWhile__t3820853950 *, Il2CppObject *, const MethodInfo*))SkipWhile__OnNext_m1916940496_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::OnError(System.Exception)
extern "C"  void SkipWhile__OnError_m4247432159_gshared (SkipWhile__t3820853950 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SkipWhile__OnError_m4247432159(__this, ___error0, method) ((  void (*) (SkipWhile__t3820853950 *, Exception_t1967233988 *, const MethodInfo*))SkipWhile__OnError_m4247432159_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipWhileObservable`1/SkipWhile_<System.Object>::OnCompleted()
extern "C"  void SkipWhile__OnCompleted_m2251093426_gshared (SkipWhile__t3820853950 * __this, const MethodInfo* method);
#define SkipWhile__OnCompleted_m2251093426(__this, method) ((  void (*) (SkipWhile__t3820853950 *, const MethodInfo*))SkipWhile__OnCompleted_m2251093426_gshared)(__this, method)
