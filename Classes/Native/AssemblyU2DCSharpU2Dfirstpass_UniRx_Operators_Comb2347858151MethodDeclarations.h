﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>
struct CombineLatest_t2347858151;
// UniRx.Operators.CombineLatestObservable`3<System.Object,System.Object,System.Object>
struct CombineLatestObservable_3_t2740954106;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void CombineLatest__ctor_m375266749_gshared (CombineLatest_t2347858151 * __this, CombineLatestObservable_3_t2740954106 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define CombineLatest__ctor_m375266749(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (CombineLatest_t2347858151 *, CombineLatestObservable_3_t2740954106 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatest__ctor_m375266749_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m338529697_gshared (CombineLatest_t2347858151 * __this, const MethodInfo* method);
#define CombineLatest_Run_m338529697(__this, method) ((  Il2CppObject * (*) (CombineLatest_t2347858151 *, const MethodInfo*))CombineLatest_Run_m338529697_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::Publish()
extern "C"  void CombineLatest_Publish_m193631232_gshared (CombineLatest_t2347858151 * __this, const MethodInfo* method);
#define CombineLatest_Publish_m193631232(__this, method) ((  void (*) (CombineLatest_t2347858151 *, const MethodInfo*))CombineLatest_Publish_m193631232_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnNext(TResult)
extern "C"  void CombineLatest_OnNext_m441701726_gshared (CombineLatest_t2347858151 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CombineLatest_OnNext_m441701726(__this, ___value0, method) ((  void (*) (CombineLatest_t2347858151 *, Il2CppObject *, const MethodInfo*))CombineLatest_OnNext_m441701726_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m4168486026_gshared (CombineLatest_t2347858151 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatest_OnError_m4168486026(__this, ___error0, method) ((  void (*) (CombineLatest_t2347858151 *, Exception_t1967233988 *, const MethodInfo*))CombineLatest_OnError_m4168486026_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1717288669_gshared (CombineLatest_t2347858151 * __this, const MethodInfo* method);
#define CombineLatest_OnCompleted_m1717288669(__this, method) ((  void (*) (CombineLatest_t2347858151 *, const MethodInfo*))CombineLatest_OnCompleted_m1717288669_gshared)(__this, method)
