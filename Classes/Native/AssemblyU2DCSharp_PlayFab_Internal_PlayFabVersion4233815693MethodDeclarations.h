﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.PlayFabVersion
struct PlayFabVersion_t4233815693;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.Internal.PlayFabVersion::.ctor()
extern "C"  void PlayFabVersion__ctor_m1645646384 (PlayFabVersion_t4233815693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabVersion::.cctor()
extern "C"  void PlayFabVersion__cctor_m3288301437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.Internal.PlayFabVersion::getVersionString()
extern "C"  String_t* PlayFabVersion_getVersionString_m249008132 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
