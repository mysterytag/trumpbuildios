﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableCollisionTrigger
struct ObservableCollisionTrigger_t2000881770;
// UnityEngine.Collision
struct Collision_t1119538015;
// UniRx.IObservable`1<UnityEngine.Collision>
struct IObservable_1_t878336379;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"

// System.Void UniRx.Triggers.ObservableCollisionTrigger::.ctor()
extern "C"  void ObservableCollisionTrigger__ctor_m270416595 (ObservableCollisionTrigger_t2000881770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCollisionTrigger::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void ObservableCollisionTrigger_OnCollisionEnter_m1970204833 (ObservableCollisionTrigger_t2000881770 * __this, Collision_t1119538015 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::OnCollisionEnterAsObservable()
extern "C"  Il2CppObject* ObservableCollisionTrigger_OnCollisionEnterAsObservable_m3434432352 (ObservableCollisionTrigger_t2000881770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCollisionTrigger::OnCollisionExit(UnityEngine.Collision)
extern "C"  void ObservableCollisionTrigger_OnCollisionExit_m724114933 (ObservableCollisionTrigger_t2000881770 * __this, Collision_t1119538015 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::OnCollisionExitAsObservable()
extern "C"  Il2CppObject* ObservableCollisionTrigger_OnCollisionExitAsObservable_m2705057218 (ObservableCollisionTrigger_t2000881770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCollisionTrigger::OnCollisionStay(UnityEngine.Collision)
extern "C"  void ObservableCollisionTrigger_OnCollisionStay_m280501850 (ObservableCollisionTrigger_t2000881770 * __this, Collision_t1119538015 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::OnCollisionStayAsObservable()
extern "C"  Il2CppObject* ObservableCollisionTrigger_OnCollisionStayAsObservable_m3544772541 (ObservableCollisionTrigger_t2000881770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCollisionTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableCollisionTrigger_RaiseOnCompletedOnDestroy_m4242827916 (ObservableCollisionTrigger_t2000881770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
