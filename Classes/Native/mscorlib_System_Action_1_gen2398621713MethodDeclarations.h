﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2564974692MethodDeclarations.h"

// System.Void System.Action`1<UniRx.CollectionAddEvent`1<DonateButton>>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m895187669(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2398621713 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m3043521032_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<UniRx.CollectionAddEvent`1<DonateButton>>::Invoke(T)
#define Action_1_Invoke_m91712463(__this, ___obj0, method) ((  void (*) (Action_1_t2398621713 *, CollectionAddEvent_1_t2250169008 , const MethodInfo*))Action_1_Invoke_m3668670780_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<UniRx.CollectionAddEvent`1<DonateButton>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m3465281572(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2398621713 *, CollectionAddEvent_1_t2250169008 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m3862211593_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<UniRx.CollectionAddEvent`1<DonateButton>>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m3302989669(__this, ___result0, method) ((  void (*) (Action_1_t2398621713 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m160835608_gshared)(__this, ___result0, method)
