﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<System.Object>
struct U3COnCompletedU3Ec__AnonStorey63_t4115224287;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<System.Object>::.ctor()
extern "C"  void U3COnCompletedU3Ec__AnonStorey63__ctor_m170323964_gshared (U3COnCompletedU3Ec__AnonStorey63_t4115224287 * __this, const MethodInfo* method);
#define U3COnCompletedU3Ec__AnonStorey63__ctor_m170323964(__this, method) ((  void (*) (U3COnCompletedU3Ec__AnonStorey63_t4115224287 *, const MethodInfo*))U3COnCompletedU3Ec__AnonStorey63__ctor_m170323964_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<System.Object>::<>m__80()
extern "C"  void U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m2780820669_gshared (U3COnCompletedU3Ec__AnonStorey63_t4115224287 * __this, const MethodInfo* method);
#define U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m2780820669(__this, method) ((  void (*) (U3COnCompletedU3Ec__AnonStorey63_t4115224287 *, const MethodInfo*))U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m2780820669_gshared)(__this, method)
