﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ReturnObservable`1<UniRx.Unit>
struct ReturnObservable_1_t3247033540;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Operators.ReturnObservable`1<UniRx.Unit>::.ctor(T,UniRx.IScheduler)
extern "C"  void ReturnObservable_1__ctor_m1914543660_gshared (ReturnObservable_1_t3247033540 * __this, Unit_t2558286038  ___value0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ReturnObservable_1__ctor_m1914543660(__this, ___value0, ___scheduler1, method) ((  void (*) (ReturnObservable_1_t3247033540 *, Unit_t2558286038 , Il2CppObject *, const MethodInfo*))ReturnObservable_1__ctor_m1914543660_gshared)(__this, ___value0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.ReturnObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ReturnObservable_1_SubscribeCore_m4199795824_gshared (ReturnObservable_1_t3247033540 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ReturnObservable_1_SubscribeCore_m4199795824(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ReturnObservable_1_t3247033540 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ReturnObservable_1_SubscribeCore_m4199795824_gshared)(__this, ___observer0, ___cancel1, method)
