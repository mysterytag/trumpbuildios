﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ScanObservable`2<UniRx.Unit,System.Int64>
struct ScanObservable_2_t850793272;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Func`3<System.Int64,UniRx.Unit,System.Int64>
struct Func_3_t3706795343;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ScanObservable`2<UniRx.Unit,System.Int64>::.ctor(UniRx.IObservable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern "C"  void ScanObservable_2__ctor_m2368683345_gshared (ScanObservable_2_t850793272 * __this, Il2CppObject* ___source0, int64_t ___seed1, Func_3_t3706795343 * ___accumulator2, const MethodInfo* method);
#define ScanObservable_2__ctor_m2368683345(__this, ___source0, ___seed1, ___accumulator2, method) ((  void (*) (ScanObservable_2_t850793272 *, Il2CppObject*, int64_t, Func_3_t3706795343 *, const MethodInfo*))ScanObservable_2__ctor_m2368683345_gshared)(__this, ___source0, ___seed1, ___accumulator2, method)
// System.IDisposable UniRx.Operators.ScanObservable`2<UniRx.Unit,System.Int64>::SubscribeCore(UniRx.IObserver`1<TAccumulate>,System.IDisposable)
extern "C"  Il2CppObject * ScanObservable_2_SubscribeCore_m2573881442_gshared (ScanObservable_2_t850793272 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ScanObservable_2_SubscribeCore_m2573881442(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ScanObservable_2_t850793272 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ScanObservable_2_SubscribeCore_m2573881442_gshared)(__this, ___observer0, ___cancel1, method)
