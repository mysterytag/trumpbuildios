﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayerTradesResponse
struct GetPlayerTradesResponse_t1092460183;
// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>
struct List_1_t2730771739;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayerTradesResponse::.ctor()
extern "C"  void GetPlayerTradesResponse__ctor_m1143086322 (GetPlayerTradesResponse_t1092460183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo> PlayFab.ClientModels.GetPlayerTradesResponse::get_OpenedTrades()
extern "C"  List_1_t2730771739 * GetPlayerTradesResponse_get_OpenedTrades_m3320506074 (GetPlayerTradesResponse_t1092460183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayerTradesResponse::set_OpenedTrades(System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>)
extern "C"  void GetPlayerTradesResponse_set_OpenedTrades_m638280767 (GetPlayerTradesResponse_t1092460183 * __this, List_1_t2730771739 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo> PlayFab.ClientModels.GetPlayerTradesResponse::get_AcceptedTrades()
extern "C"  List_1_t2730771739 * GetPlayerTradesResponse_get_AcceptedTrades_m1090847704 (GetPlayerTradesResponse_t1092460183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayerTradesResponse::set_AcceptedTrades(System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>)
extern "C"  void GetPlayerTradesResponse_set_AcceptedTrades_m878075457 (GetPlayerTradesResponse_t1092460183 * __this, List_1_t2730771739 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
