﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkFacebookAccountRequestCallback
struct UnlinkFacebookAccountRequestCallback_t651647872;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkFacebookAccountRequest
struct UnlinkFacebookAccountRequest_t3175165483;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkFaceb3175165483.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkFacebookAccountRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkFacebookAccountRequestCallback__ctor_m446825583 (UnlinkFacebookAccountRequestCallback_t651647872 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkFacebookAccountRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkFacebookAccountRequest,System.Object)
extern "C"  void UnlinkFacebookAccountRequestCallback_Invoke_m1089024351 (UnlinkFacebookAccountRequestCallback_t651647872 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkFacebookAccountRequest_t3175165483 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkFacebookAccountRequestCallback_t651647872(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkFacebookAccountRequest_t3175165483 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkFacebookAccountRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkFacebookAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkFacebookAccountRequestCallback_BeginInvoke_m2885523686 (UnlinkFacebookAccountRequestCallback_t651647872 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkFacebookAccountRequest_t3175165483 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkFacebookAccountRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkFacebookAccountRequestCallback_EndInvoke_m332334591 (UnlinkFacebookAccountRequestCallback_t651647872 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
