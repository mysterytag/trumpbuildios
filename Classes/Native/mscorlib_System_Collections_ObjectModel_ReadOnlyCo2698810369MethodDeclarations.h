﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4000251768MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m3634370437(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3388145391(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, InitializableInfo_t3830632317 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3959983035(__this, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m265085718(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, InitializableInfo_t3830632317 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4133386984(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2698810369 *, InitializableInfo_t3830632317 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2433905884(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1989089282(__this, ___index0, method) ((  InitializableInfo_t3830632317 * (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3153901165(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, InitializableInfo_t3830632317 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2756488615(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2926048692(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2455491267(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2162797146(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2698810369 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1400427714(__this, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m805074214(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2698810369 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4097385394(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2698810369 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1437683237(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m4215620323(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3420566837(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m863504502(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m907812200(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2905897237(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2903052100(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1594774767(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3543041212(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::Contains(T)
#define ReadOnlyCollection_1_Contains_m2770407469(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2698810369 *, InitializableInfo_t3830632317 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m3719524127(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2698810369 *, InitializableInfoU5BU5D_t104241648*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m4213363716(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1875942699(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2698810369 *, InitializableInfo_t3830632317 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2136956336(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2698810369 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InitializableManager/InitializableInfo>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m613323458(__this, ___index0, method) ((  InitializableInfo_t3830632317 * (*) (ReadOnlyCollection_1_t2698810369 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
