﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpgradeButton
struct  UpgradeButton_t1475467342  : public Il2CppObject
{
public:
	// System.String UpgradeButton::TitleId
	String_t* ___TitleId_0;
	// System.Int32 UpgradeButton::id
	int32_t ___id_1;

public:
	inline static int32_t get_offset_of_TitleId_0() { return static_cast<int32_t>(offsetof(UpgradeButton_t1475467342, ___TitleId_0)); }
	inline String_t* get_TitleId_0() const { return ___TitleId_0; }
	inline String_t** get_address_of_TitleId_0() { return &___TitleId_0; }
	inline void set_TitleId_0(String_t* value)
	{
		___TitleId_0 = value;
		Il2CppCodeGenWriteBarrier(&___TitleId_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(UpgradeButton_t1475467342, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
