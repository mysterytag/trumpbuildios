﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetSharedGroupDataResult
struct GetSharedGroupDataResult_t3615413147;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.SharedGroupDataRecord>
struct Dictionary_2_t444340917;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetSharedGroupDataResult::.ctor()
extern "C"  void GetSharedGroupDataResult__ctor_m1626976514 (GetSharedGroupDataResult_t3615413147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.SharedGroupDataRecord> PlayFab.ClientModels.GetSharedGroupDataResult::get_Data()
extern "C"  Dictionary_2_t444340917 * GetSharedGroupDataResult_get_Data_m2210963048 (GetSharedGroupDataResult_t3615413147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetSharedGroupDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.SharedGroupDataRecord>)
extern "C"  void GetSharedGroupDataResult_set_Data_m2801475523 (GetSharedGroupDataResult_t3615413147 * __this, Dictionary_2_t444340917 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetSharedGroupDataResult::get_Members()
extern "C"  List_1_t1765447871 * GetSharedGroupDataResult_get_Members_m1361970817 (GetSharedGroupDataResult_t3615413147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetSharedGroupDataResult::set_Members(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetSharedGroupDataResult_set_Members_m2652825168 (GetSharedGroupDataResult_t3615413147 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
