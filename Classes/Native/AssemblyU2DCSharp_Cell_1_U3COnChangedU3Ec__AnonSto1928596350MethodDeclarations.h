﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1/<OnChanged>c__AnonStoreyF3<System.Int64>
struct U3COnChangedU3Ec__AnonStoreyF3_t1928596350;

#include "codegen/il2cpp-codegen.h"

// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Int64>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m3136929932_gshared (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3__ctor_m3136929932(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3__ctor_m3136929932_gshared)(__this, method)
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Int64>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m2631202052_gshared (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 * __this, int64_t ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m2631202052(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 *, int64_t, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m2631202052_gshared)(__this, ____0, method)
