﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// System.Object
struct Il2CppObject;
// GameController
struct GameController_t2782302542;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/<UpdateCityLevel>c__IteratorE
struct  U3CUpdateCityLevelU3Ec__IteratorE_t308347614  : public Il2CppObject
{
public:
	// System.Single GameController/<UpdateCityLevel>c__IteratorE::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single GameController/<UpdateCityLevel>c__IteratorE::<processTime>__1
	float ___U3CprocessTimeU3E__1_1;
	// System.Single GameController/<UpdateCityLevel>c__IteratorE::<deltaTime>__2
	float ___U3CdeltaTimeU3E__2_2;
	// System.Single GameController/<UpdateCityLevel>c__IteratorE::<ratio>__3
	float ___U3CratioU3E__3_3;
	// UnityEngine.SpriteRenderer GameController/<UpdateCityLevel>c__IteratorE::cityRenderer
	SpriteRenderer_t2223784725 * ___cityRenderer_4;
	// System.Int32 GameController/<UpdateCityLevel>c__IteratorE::$PC
	int32_t ___U24PC_5;
	// System.Object GameController/<UpdateCityLevel>c__IteratorE::$current
	Il2CppObject * ___U24current_6;
	// UnityEngine.SpriteRenderer GameController/<UpdateCityLevel>c__IteratorE::<$>cityRenderer
	SpriteRenderer_t2223784725 * ___U3CU24U3EcityRenderer_7;
	// GameController GameController/<UpdateCityLevel>c__IteratorE::<>f__this
	GameController_t2782302542 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CprocessTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___U3CprocessTimeU3E__1_1)); }
	inline float get_U3CprocessTimeU3E__1_1() const { return ___U3CprocessTimeU3E__1_1; }
	inline float* get_address_of_U3CprocessTimeU3E__1_1() { return &___U3CprocessTimeU3E__1_1; }
	inline void set_U3CprocessTimeU3E__1_1(float value)
	{
		___U3CprocessTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaTimeU3E__2_2() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___U3CdeltaTimeU3E__2_2)); }
	inline float get_U3CdeltaTimeU3E__2_2() const { return ___U3CdeltaTimeU3E__2_2; }
	inline float* get_address_of_U3CdeltaTimeU3E__2_2() { return &___U3CdeltaTimeU3E__2_2; }
	inline void set_U3CdeltaTimeU3E__2_2(float value)
	{
		___U3CdeltaTimeU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__3_3() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___U3CratioU3E__3_3)); }
	inline float get_U3CratioU3E__3_3() const { return ___U3CratioU3E__3_3; }
	inline float* get_address_of_U3CratioU3E__3_3() { return &___U3CratioU3E__3_3; }
	inline void set_U3CratioU3E__3_3(float value)
	{
		___U3CratioU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_cityRenderer_4() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___cityRenderer_4)); }
	inline SpriteRenderer_t2223784725 * get_cityRenderer_4() const { return ___cityRenderer_4; }
	inline SpriteRenderer_t2223784725 ** get_address_of_cityRenderer_4() { return &___cityRenderer_4; }
	inline void set_cityRenderer_4(SpriteRenderer_t2223784725 * value)
	{
		___cityRenderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___cityRenderer_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcityRenderer_7() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___U3CU24U3EcityRenderer_7)); }
	inline SpriteRenderer_t2223784725 * get_U3CU24U3EcityRenderer_7() const { return ___U3CU24U3EcityRenderer_7; }
	inline SpriteRenderer_t2223784725 ** get_address_of_U3CU24U3EcityRenderer_7() { return &___U3CU24U3EcityRenderer_7; }
	inline void set_U3CU24U3EcityRenderer_7(SpriteRenderer_t2223784725 * value)
	{
		___U3CU24U3EcityRenderer_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EcityRenderer_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CUpdateCityLevelU3Ec__IteratorE_t308347614, ___U3CU3Ef__this_8)); }
	inline GameController_t2782302542 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline GameController_t2782302542 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(GameController_t2782302542 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
