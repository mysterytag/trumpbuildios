﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<System.Object>>
struct Subscription_t695680369;
// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct Subject_1_t559501171;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1098239611_gshared (Subscription_t695680369 * __this, Subject_1_t559501171 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1098239611(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t695680369 *, Subject_1_t559501171 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1098239611_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m1564560027_gshared (Subscription_t695680369 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1564560027(__this, method) ((  void (*) (Subscription_t695680369 *, const MethodInfo*))Subscription_Dispose_m1564560027_gshared)(__this, method)
