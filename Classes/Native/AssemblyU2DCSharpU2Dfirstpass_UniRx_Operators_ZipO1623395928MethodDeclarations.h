﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`1/Zip/ZipObserver<System.Object>
struct ZipObserver_t1623395928;
// UniRx.Operators.ZipObservable`1/Zip<System.Object>
struct Zip_t3754418642;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObservable`1/Zip/ZipObserver<System.Object>::.ctor(UniRx.Operators.ZipObservable`1/Zip<T>,System.Int32)
extern "C"  void ZipObserver__ctor_m1726694306_gshared (ZipObserver_t1623395928 * __this, Zip_t3754418642 * ___parent0, int32_t ___index1, const MethodInfo* method);
#define ZipObserver__ctor_m1726694306(__this, ___parent0, ___index1, method) ((  void (*) (ZipObserver_t1623395928 *, Zip_t3754418642 *, int32_t, const MethodInfo*))ZipObserver__ctor_m1726694306_gshared)(__this, ___parent0, ___index1, method)
// System.Void UniRx.Operators.ZipObservable`1/Zip/ZipObserver<System.Object>::OnNext(T)
extern "C"  void ZipObserver_OnNext_m103689395_gshared (ZipObserver_t1623395928 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ZipObserver_OnNext_m103689395(__this, ___value0, method) ((  void (*) (ZipObserver_t1623395928 *, Il2CppObject *, const MethodInfo*))ZipObserver_OnNext_m103689395_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObservable`1/Zip/ZipObserver<System.Object>::OnError(System.Exception)
extern "C"  void ZipObserver_OnError_m1851818434_gshared (ZipObserver_t1623395928 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method);
#define ZipObserver_OnError_m1851818434(__this, ___ex0, method) ((  void (*) (ZipObserver_t1623395928 *, Exception_t1967233988 *, const MethodInfo*))ZipObserver_OnError_m1851818434_gshared)(__this, ___ex0, method)
// System.Void UniRx.Operators.ZipObservable`1/Zip/ZipObserver<System.Object>::OnCompleted()
extern "C"  void ZipObserver_OnCompleted_m170018837_gshared (ZipObserver_t1623395928 * __this, const MethodInfo* method);
#define ZipObserver_OnCompleted_m170018837(__this, method) ((  void (*) (ZipObserver_t1623395928 *, const MethodInfo*))ZipObserver_OnCompleted_m170018837_gshared)(__this, method)
