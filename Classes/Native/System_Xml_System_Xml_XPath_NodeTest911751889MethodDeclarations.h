﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.NodeTest
struct NodeTest_t911751889;
// System.Xml.XPath.AxisSpecifier
struct AxisSpecifier_t4216515578;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Axes4021066818.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.NodeTest::.ctor(System.Xml.XPath.Axes)
extern "C"  void NodeTest__ctor_m3652977216 (NodeTest_t911751889 * __this, int32_t ___axis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.AxisSpecifier System.Xml.XPath.NodeTest::get_Axis()
extern "C"  AxisSpecifier_t4216515578 * NodeTest_get_Axis_m243839149 (NodeTest_t911751889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.NodeTest::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * NodeTest_Evaluate_m2924662090 (NodeTest_t911751889 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.NodeTest::get_RequireSorting()
extern "C"  bool NodeTest_get_RequireSorting_m101109566 (NodeTest_t911751889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.NodeTest::get_Peer()
extern "C"  bool NodeTest_get_Peer_m1939429633 (NodeTest_t911751889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.NodeTest::get_Subtree()
extern "C"  bool NodeTest_get_Subtree_m100870017 (NodeTest_t911751889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
