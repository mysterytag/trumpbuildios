﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/PurchaseItemRequestCallback
struct PurchaseItemRequestCallback_t647441792;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.PurchaseItemRequest
struct PurchaseItemRequest_t3673358379;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PurchaseIte3673358379.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/PurchaseItemRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PurchaseItemRequestCallback__ctor_m3174425103 (PurchaseItemRequestCallback_t647441792 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/PurchaseItemRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.PurchaseItemRequest,System.Object)
extern "C"  void PurchaseItemRequestCallback_Invoke_m3411885849 (PurchaseItemRequestCallback_t647441792 * __this, String_t* ___urlPath0, int32_t ___callId1, PurchaseItemRequest_t3673358379 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_PurchaseItemRequestCallback_t647441792(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, PurchaseItemRequest_t3673358379 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/PurchaseItemRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.PurchaseItemRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PurchaseItemRequestCallback_BeginInvoke_m3755253982 (PurchaseItemRequestCallback_t647441792 * __this, String_t* ___urlPath0, int32_t ___callId1, PurchaseItemRequest_t3673358379 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/PurchaseItemRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PurchaseItemRequestCallback_EndInvoke_m2436019103 (PurchaseItemRequestCallback_t647441792 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
