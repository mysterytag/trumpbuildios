﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.BindScope
struct BindScope_t2945157996;

#include "AssemblyU2DCSharp_Zenject_BinderUntyped2430239676.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindScope/CustomScopeUntypedBinder
struct  CustomScopeUntypedBinder_t2439484920  : public BinderUntyped_t2430239676
{
public:
	// Zenject.BindScope Zenject.BindScope/CustomScopeUntypedBinder::_owner
	BindScope_t2945157996 * ____owner_4;

public:
	inline static int32_t get_offset_of__owner_4() { return static_cast<int32_t>(offsetof(CustomScopeUntypedBinder_t2439484920, ____owner_4)); }
	inline BindScope_t2945157996 * get__owner_4() const { return ____owner_4; }
	inline BindScope_t2945157996 ** get_address_of__owner_4() { return &____owner_4; }
	inline void set__owner_4(BindScope_t2945157996 * value)
	{
		____owner_4 = value;
		Il2CppCodeGenWriteBarrier(&____owner_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
