﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameAnalyticsSDK.Game
struct Game_t3902568756;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate2585444626.h"
#include "mscorlib_System_Void2779279689.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<GameAnalyticsSDK.Game>
struct  Action_1_t4051021461  : public MulticastDelegate_t2585444626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
