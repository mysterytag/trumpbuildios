﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetTitleNewsResponseCallback
struct GetTitleNewsResponseCallback_t2934766715;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetTitleNewsRequest
struct GetTitleNewsRequest_t3432781354;
// PlayFab.ClientModels.GetTitleNewsResult
struct GetTitleNewsResult_t422977346;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleNew3432781354.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleNews422977346.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetTitleNewsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetTitleNewsResponseCallback__ctor_m902706154 (GetTitleNewsResponseCallback_t2934766715 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTitleNewsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetTitleNewsRequest,PlayFab.ClientModels.GetTitleNewsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetTitleNewsResponseCallback_Invoke_m4230557207 (GetTitleNewsResponseCallback_t2934766715 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTitleNewsRequest_t3432781354 * ___request2, GetTitleNewsResult_t422977346 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetTitleNewsResponseCallback_t2934766715(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetTitleNewsRequest_t3432781354 * ___request2, GetTitleNewsResult_t422977346 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetTitleNewsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetTitleNewsRequest,PlayFab.ClientModels.GetTitleNewsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetTitleNewsResponseCallback_BeginInvoke_m1025301636 (GetTitleNewsResponseCallback_t2934766715 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTitleNewsRequest_t3432781354 * ___request2, GetTitleNewsResult_t422977346 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTitleNewsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetTitleNewsResponseCallback_EndInvoke_m878455546 (GetTitleNewsResponseCallback_t2934766715 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
