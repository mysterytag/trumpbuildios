﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Core.SequenceCallback
struct SequenceCallback_t575784399;
// DG.Tweening.TweenCallback
struct TweenCallback_t3786476454;

#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_TweenCallback3786476454.h"

// System.Void DG.Tweening.Core.SequenceCallback::.ctor(System.Single,DG.Tweening.TweenCallback)
extern "C"  void SequenceCallback__ctor_m2183614979 (SequenceCallback_t575784399 * __this, float ___sequencedPosition0, TweenCallback_t3786476454 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
