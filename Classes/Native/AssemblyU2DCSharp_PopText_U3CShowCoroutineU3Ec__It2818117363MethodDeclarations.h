﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopText/<ShowCoroutine>c__Iterator34
struct U3CShowCoroutineU3Ec__Iterator34_t2818117363;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PopText/<ShowCoroutine>c__Iterator34::.ctor()
extern "C"  void U3CShowCoroutineU3Ec__Iterator34__ctor_m3752280759 (U3CShowCoroutineU3Ec__Iterator34_t2818117363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PopText/<ShowCoroutine>c__Iterator34::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowCoroutineU3Ec__Iterator34_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m361732357 (U3CShowCoroutineU3Ec__Iterator34_t2818117363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PopText/<ShowCoroutine>c__Iterator34::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowCoroutineU3Ec__Iterator34_System_Collections_IEnumerator_get_Current_m844514969 (U3CShowCoroutineU3Ec__Iterator34_t2818117363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PopText/<ShowCoroutine>c__Iterator34::MoveNext()
extern "C"  bool U3CShowCoroutineU3Ec__Iterator34_MoveNext_m1977326021 (U3CShowCoroutineU3Ec__Iterator34_t2818117363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopText/<ShowCoroutine>c__Iterator34::Dispose()
extern "C"  void U3CShowCoroutineU3Ec__Iterator34_Dispose_m2362531124 (U3CShowCoroutineU3Ec__Iterator34_t2818117363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopText/<ShowCoroutine>c__Iterator34::Reset()
extern "C"  void U3CShowCoroutineU3Ec__Iterator34_Reset_m1398713700 (U3CShowCoroutineU3Ec__Iterator34_t2818117363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
