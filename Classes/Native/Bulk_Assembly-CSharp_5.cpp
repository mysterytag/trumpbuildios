﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// PlayFab.ClientModels.EmptyResult
struct EmptyResult_t1985806266;
// PlayFab.ClientModels.FacebookPlayFabIdPair
struct FacebookPlayFabIdPair_t2068825714;
// System.String
struct String_t;
// PlayFab.ClientModels.FriendInfo
struct FriendInfo_t3708701852;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// PlayFab.ClientModels.UserFacebookInfo
struct UserFacebookInfo_t1491589167;
// PlayFab.ClientModels.UserSteamInfo
struct UserSteamInfo_t2867463811;
// PlayFab.ClientModels.UserGameCenterInfo
struct UserGameCenterInfo_t386684176;
// PlayFab.ClientModels.GameCenterPlayFabIdPair
struct GameCenterPlayFabIdPair_t3063128049;
// PlayFab.ClientModels.GameInfo
struct GameInfo_t1767394288;
// PlayFab.ClientModels.GameServerRegionsRequest
struct GameServerRegionsRequest_t2476765941;
// PlayFab.ClientModels.GameServerRegionsResult
struct GameServerRegionsResult_t253590807;
// System.Collections.Generic.List`1<PlayFab.ClientModels.RegionInfo>
struct List_1_t4271828715;
// PlayFab.ClientModels.GetAccountInfoRequest
struct GetAccountInfoRequest_t2417218426;
// PlayFab.ClientModels.GetAccountInfoResult
struct GetAccountInfoResult_t3022616562;
// PlayFab.ClientModels.UserAccountInfo
struct UserAccountInfo_t960776096;
// PlayFab.ClientModels.GetCatalogItemsRequest
struct GetCatalogItemsRequest_t2824754786;
// PlayFab.ClientModels.GetCatalogItemsResult
struct GetCatalogItemsResult_t680458250;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>
struct List_1_t634580917;
// PlayFab.ClientModels.GetCharacterDataRequest
struct GetCharacterDataRequest_t572698882;
// PlayFab.ClientModels.GetCharacterDataResult
struct GetCharacterDataResult_t1300547946;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>
struct Dictionary_2_t1801537638;
// PlayFab.ClientModels.GetCharacterInventoryRequest
struct GetCharacterInventoryRequest_t2835266646;
// PlayFab.ClientModels.GetCharacterInventoryResult
struct GetCharacterInventoryResult_t403702678;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>
struct Dictionary_2_t3222006224;
// PlayFab.ClientModels.GetCharacterLeaderboardRequest
struct GetCharacterLeaderboardRequest_t2906494549;
// PlayFab.ClientModels.GetCharacterLeaderboardResult
struct GetCharacterLeaderboardResult_t3315494327;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>
struct List_1_t3914494247;
// PlayFab.ClientModels.GetCharacterStatisticsRequest
struct GetCharacterStatisticsRequest_t3373629353;
// PlayFab.ClientModels.GetCharacterStatisticsResult
struct GetCharacterStatisticsResult_t2637826531;
// PlayFab.ClientModels.GetCloudScriptUrlRequest
struct GetCloudScriptUrlRequest_t841424922;
// PlayFab.ClientModels.GetCloudScriptUrlResult
struct GetCloudScriptUrlResult_t3525973842;
// PlayFab.ClientModels.GetContentDownloadUrlRequest
struct GetContentDownloadUrlRequest_t2056984219;
// PlayFab.ClientModels.GetContentDownloadUrlResult
struct GetContentDownloadUrlResult_t101502129;
// PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest
struct GetFriendLeaderboardAroundCurrentUserRequest_t2179115089;
// PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult
struct GetFriendLeaderboardAroundCurrentUserResult_t659631163;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>
struct List_1_t3038850335;
// PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest
struct GetFriendLeaderboardAroundPlayerRequest_t16887688;
// PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult
struct GetFriendLeaderboardAroundPlayerResult_t589881892;
// PlayFab.ClientModels.GetFriendLeaderboardRequest
struct GetFriendLeaderboardRequest_t2919058678;
// PlayFab.ClientModels.GetFriendsListRequest
struct GetFriendsListRequest_t3925362978;
// PlayFab.ClientModels.GetFriendsListResult
struct GetFriendsListResult_t2794171722;
// System.Collections.Generic.List`1<PlayFab.ClientModels.FriendInfo>
struct List_1_t210693525;
// PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest
struct GetLeaderboardAroundCharacterRequest_t1108200042;
// PlayFab.ClientModels.GetLeaderboardAroundCharacterResult
struct GetLeaderboardAroundCharacterResult_t2426200834;
// PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest
struct GetLeaderboardAroundCurrentUserRequest_t545060687;
// PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult
struct GetLeaderboardAroundCurrentUserResult_t3100771709;
// PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest
struct GetLeaderboardAroundPlayerRequest_t299203018;
// PlayFab.ClientModels.GetLeaderboardAroundPlayerResult
struct GetLeaderboardAroundPlayerResult_t460441506;
// PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest
struct GetLeaderboardForUsersCharactersRequest_t3978815279;
// PlayFab.ClientModels.GetLeaderboardForUsersCharactersResult
struct GetLeaderboardForUsersCharactersResult_t1964611997;
// PlayFab.ClientModels.GetLeaderboardRequest
struct GetLeaderboardRequest_t1150085688;
// PlayFab.ClientModels.GetLeaderboardResult
struct GetLeaderboardResult_t2566099316;
// PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest
struct GetPhotonAuthenticationTokenRequest_t2686429680;
// PlayFab.ClientModels.GetPhotonAuthenticationTokenResult
struct GetPhotonAuthenticationTokenResult_t3031300796;
// PlayFab.ClientModels.GetPlayerStatisticsRequest
struct GetPlayerStatisticsRequest_t3788214021;
// PlayFab.ClientModels.GetPlayerStatisticsResult
struct GetPlayerStatisticsResult_t3759578887;
// System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticValue>
struct List_1_t3990614826;
// PlayFab.ClientModels.GetPlayerTradesRequest
struct GetPlayerTradesRequest_t3393757081;
// PlayFab.ClientModels.GetPlayerTradesResponse
struct GetPlayerTradesResponse_t1092460183;
// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>
struct List_1_t2730771739;
// PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest
struct GetPlayFabIDsFromFacebookIDsRequest_t270576050;
// PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult
struct GetPlayFabIDsFromFacebookIDsResult_t3230464698;
// System.Collections.Generic.List`1<PlayFab.ClientModels.FacebookPlayFabIdPair>
struct List_1_t2865784683;
// PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsRequest
struct GetPlayFabIDsFromGameCenterIDsRequest_t1196231507;
// PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsResult
struct GetPlayFabIDsFromGameCenterIDsResult_t627925241;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GameCenterPlayFabIdPair>
struct List_1_t3860087018;
// PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest
struct GetPlayFabIDsFromGoogleIDsRequest_t3277397669;
// PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult
struct GetPlayFabIDsFromGoogleIDsResult_t2496174951;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GooglePlayFabIdPair>
struct List_1_t1460113112;
// PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest
struct GetPlayFabIDsFromKongregateIDsRequest_t4157912391;
// PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult
struct GetPlayFabIDsFromKongregateIDsResult_t3910051973;
// System.Collections.Generic.List`1<PlayFab.ClientModels.KongregatePlayFabIdPair>
struct List_1_t3782140534;
// PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest
struct GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734;
// PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult
struct GetPlayFabIDsFromPSNAccountIDsResult_t3051284830;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PSNAccountPlayFabIdPair>
struct List_1_t1477512911;
// PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest
struct GetPlayFabIDsFromSteamIDsRequest_t241435282;
// System.Collections.Generic.List`1<System.UInt64>
struct List_1_t1782884390;
// PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsResult
struct GetPlayFabIDsFromSteamIDsResult_t4060808666;
// System.Collections.Generic.List`1<PlayFab.ClientModels.SteamPlayFabIdPair>
struct List_1_t3481275745;
// PlayFab.ClientModels.GetPublisherDataRequest
struct GetPublisherDataRequest_t1266178127;
// PlayFab.ClientModels.GetPublisherDataResult
struct GetPublisherDataResult_t2154202237;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// PlayFab.ClientModels.GetPurchaseRequest
struct GetPurchaseRequest_t593210440;
// PlayFab.ClientModels.GetPurchaseResult
struct GetPurchaseResult_t54283620;
// PlayFab.ClientModels.GetSharedGroupDataRequest
struct GetSharedGroupDataRequest_t3614043377;
// PlayFab.ClientModels.GetSharedGroupDataResult
struct GetSharedGroupDataResult_t3615413147;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.SharedGroupDataRecord>
struct Dictionary_2_t444340917;
// PlayFab.ClientModels.GetStoreItemsRequest
struct GetStoreItemsRequest_t3204724138;
// PlayFab.ClientModels.GetStoreItemsResult
struct GetStoreItemsResult_t831262658;
// System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>
struct List_1_t3669843069;
// PlayFab.ClientModels.GetTitleDataRequest
struct GetTitleDataRequest_t535093619;
// PlayFab.ClientModels.GetTitleDataResult
struct GetTitleDataResult_t1299334873;
// PlayFab.ClientModels.GetTitleNewsRequest
struct GetTitleNewsRequest_t3432781354;
// PlayFab.ClientModels.GetTitleNewsResult
struct GetTitleNewsResult_t422977346;
// System.Collections.Generic.List`1<PlayFab.ClientModels.TitleNewsItem>
struct List_1_t3320275943;
// PlayFab.ClientModels.GetTradeStatusRequest
struct GetTradeStatusRequest_t4199327007;
// PlayFab.ClientModels.GetTradeStatusResponse
struct GetTradeStatusResponse_t295324113;
// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;
// PlayFab.ClientModels.GetUserCombinedInfoRequest
struct GetUserCombinedInfoRequest_t3431761899;
// PlayFab.ClientModels.GetUserCombinedInfoResult
struct GetUserCombinedInfoResult_t1115681121;
// PlayFab.ClientModels.GetUserDataRequest
struct GetUserDataRequest_t2084642996;
// PlayFab.ClientModels.GetUserDataResult
struct GetUserDataResult_t518036344;
// PlayFab.ClientModels.GetUserInventoryRequest
struct GetUserInventoryRequest_t2337033188;
// PlayFab.ClientModels.GetUserInventoryResult
struct GetUserInventoryResult_t1496009288;
// PlayFab.ClientModels.GetUserStatisticsRequest
struct GetUserStatisticsRequest_t813294043;
// PlayFab.ClientModels.GetUserStatisticsResult
struct GetUserStatisticsResult_t2139593073;
// PlayFab.ClientModels.GooglePlayFabIdPair
struct GooglePlayFabIdPair_t663154143;
// PlayFab.ClientModels.GrantCharacterToUserRequest
struct GrantCharacterToUserRequest_t1227105868;
// PlayFab.ClientModels.GrantCharacterToUserResult
struct GrantCharacterToUserResult_t490373856;
// PlayFab.ClientModels.ItemInstance
struct ItemInstance_t525144312;
// PlayFab.ClientModels.ItemPurchaseRequest
struct ItemPurchaseRequest_t731462315;
// PlayFab.ClientModels.KongregatePlayFabIdPair
struct KongregatePlayFabIdPair_t2985181565;
// PlayFab.ClientModels.LinkAndroidDeviceIDRequest
struct LinkAndroidDeviceIDRequest_t2075009465;
// PlayFab.ClientModels.LinkAndroidDeviceIDResult
struct LinkAndroidDeviceIDResult_t2734482899;
// PlayFab.ClientModels.LinkCustomIDRequest
struct LinkCustomIDRequest_t3838085433;
// PlayFab.ClientModels.LinkCustomIDResult
struct LinkCustomIDResult_t297504339;
// PlayFab.ClientModels.LinkFacebookAccountRequest
struct LinkFacebookAccountRequest_t3956942034;
// PlayFab.ClientModels.LinkFacebookAccountResult
struct LinkFacebookAccountResult_t3626474394;
// PlayFab.ClientModels.LinkGameCenterAccountRequest
struct LinkGameCenterAccountRequest_t355908787;
// PlayFab.ClientModels.LinkGameCenterAccountResult
struct LinkGameCenterAccountResult_t1155007385;
// PlayFab.ClientModels.LinkGoogleAccountRequest
struct LinkGoogleAccountRequest_t117459333;
// PlayFab.ClientModels.LinkGoogleAccountResult
struct LinkGoogleAccountResult_t1008768135;
// PlayFab.ClientModels.LinkIOSDeviceIDRequest
struct LinkIOSDeviceIDRequest_t130515291;
// PlayFab.ClientModels.LinkIOSDeviceIDResult
struct LinkIOSDeviceIDResult_t3225946609;
// PlayFab.ClientModels.LinkKongregateAccountRequest
struct LinkKongregateAccountRequest_t2234535079;
// PlayFab.ClientModels.LinkKongregateAccountResult
struct LinkKongregateAccountResult_t1077060901;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_EmptyResult1985806266.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_EmptyResult1985806266MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_FacebookPla2068825714.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_FacebookPla2068825714MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_FriendInfo3708701852.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_FriendInfo3708701852MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserFaceboo1491589167.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserSteamIn2867463811.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserGameCent386684176.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameCenterP3063128049.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameCenterP3063128049MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameInfo1767394288.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameInfo1767394288MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_UInt32985925326.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameServerR2476765941.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameServerR2476765941MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameServerRe253590807.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameServerRe253590807MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4271828715.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetAccountI2417218426.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetAccountI2417218426MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetAccountI3022616562.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetAccountI3022616562MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserAccountI960776096.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCatalogI2824754786.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCatalogI2824754786MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCatalogIt680458250.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCatalogIt680458250MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen634580917.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacter572698882.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacter572698882MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte1300547946.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte1300547946MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1801537638.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2835266646.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2835266646MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacter403702678.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacter403702678MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1322103281.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3222006224.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2906494549.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2906494549MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte3315494327.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte3315494327MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3914494247.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte3373629353.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte3373629353MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2637826531.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2637826531MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCloudScri841424922.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCloudScri841424922MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCloudScr3525973842.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCloudScr3525973842MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetContentD2056984219.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetContentD2056984219MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetContentDo101502129.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetContentDo101502129MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLe2179115089.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLe2179115089MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLea659631163.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLea659631163MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3038850335.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLead16887688.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLead16887688MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLea589881892.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLea589881892MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLe2919058678.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLe2919058678MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendsL3925362978.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendsL3925362978MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendsL2794171722.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendsL2794171722MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen210693525.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1108200042.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1108200042MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo2426200834.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo2426200834MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa545060687.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa545060687MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo3100771709.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo3100771709MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa299203018.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa299203018MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa460441506.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa460441506MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo3978815279.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo3978815279MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1964611997.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1964611997MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1150085688.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1150085688MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo2566099316.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo2566099316MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPhotonAu2686429680.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPhotonAu2686429680MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPhotonAu3031300796.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPhotonAu3031300796MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerSt3788214021.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerSt3788214021MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerSt3759578887.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerSt3759578887MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3990614826.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerTr3393757081.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerTr3393757081MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1403402234.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerTr1092460183.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerTr1092460183MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2730771739.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID270576050.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID270576050MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3230464698.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3230464698MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2865784683.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI1196231507.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI1196231507MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID627925241.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID627925241MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3860087018.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3277397669.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3277397669MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI2496174951.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI2496174951MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1460113112.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI4157912391.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI4157912391MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3910051973.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3910051973MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3782140534.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3305934734.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3305934734MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3051284830.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3051284830MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1477512911.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID241435282.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID241435282MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1782884390.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI4060808666.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI4060808666MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3481275745.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPublishe1266178127.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPublishe1266178127MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPublishe2154202237.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPublishe2154202237MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPurchaseR593210440.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPurchaseR593210440MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPurchaseRe54283620.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPurchaseRe54283620MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetSharedGr3614043377.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetSharedGr3614043377MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetSharedGr3615413147.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetSharedGr3615413147MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge444340917.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetStoreIte3204724138.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetStoreIte3204724138MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetStoreItem831262658.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetStoreItem831262658MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3669843069.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleData535093619.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleData535093619MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleDat1299334873.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleDat1299334873MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleNew3432781354.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleNew3432781354MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleNews422977346.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleNews422977346MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3320275943.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTradeSta4199327007.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTradeSta4199327007MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTradeStat295324113.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTradeStat295324113MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserComb3431761899.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserComb3431761899MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserComb1115681121.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserComb1115681121MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserData2084642996.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserData2084642996MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserDataR518036344.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserDataR518036344MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserInve2337033188.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserInve2337033188MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserInve1496009288.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserInve1496009288MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserStati813294043.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserStati813294043MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserStat2139593073.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserStat2139593073MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GooglePlayFa663154143.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GooglePlayFa663154143MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GrantCharac1227105868.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GrantCharac1227105868MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GrantCharact490373856.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GrantCharact490373856MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ItemInstance525144312.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ItemInstance525144312MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ItemPurchase731462315.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ItemPurchase731462315MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_KongregateP2985181565.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_KongregateP2985181565MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkAndroid2075009465.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkAndroid2075009465MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkAndroid2734482899.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkAndroid2734482899MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkCustomI3838085433.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkCustomI3838085433MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkCustomID297504339.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkCustomID297504339MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkFaceboo3956942034.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkFaceboo3956942034MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkFaceboo3626474394.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkFaceboo3626474394MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGameCent355908787.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGameCent355908787MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGameCen1155007385.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGameCen1155007385MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGoogleAc117459333.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGoogleAc117459333MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGoogleA1008768135.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGoogleA1008768135MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkIOSDevic130515291.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkIOSDevic130515291MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkIOSDevi3225946609.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkIOSDevi3225946609MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkKongreg2234535079.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkKongreg2234535079MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkKongreg1077060901.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkKongreg1077060901MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayFab.ClientModels.EmptyResult::.ctor()
extern "C"  void EmptyResult__ctor_m1265615919 (EmptyResult_t1985806266 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.FacebookPlayFabIdPair::.ctor()
extern "C"  void FacebookPlayFabIdPair__ctor_m1028679927 (FacebookPlayFabIdPair_t2068825714 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.FacebookPlayFabIdPair::get_FacebookId()
extern "C"  String_t* FacebookPlayFabIdPair_get_FacebookId_m2668057242 (FacebookPlayFabIdPair_t2068825714 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFacebookIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FacebookPlayFabIdPair::set_FacebookId(System.String)
extern "C"  void FacebookPlayFabIdPair_set_FacebookId_m205256567 (FacebookPlayFabIdPair_t2068825714 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFacebookIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.FacebookPlayFabIdPair::get_PlayFabId()
extern "C"  String_t* FacebookPlayFabIdPair_get_PlayFabId_m3553163991 (FacebookPlayFabIdPair_t2068825714 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FacebookPlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void FacebookPlayFabIdPair_set_PlayFabId_m3091415772 (FacebookPlayFabIdPair_t2068825714 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::.ctor()
extern "C"  void FriendInfo__ctor_m1180506785 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.FriendInfo::get_FriendPlayFabId()
extern "C"  String_t* FriendInfo_get_FriendPlayFabId_m872270633 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFriendPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::set_FriendPlayFabId(System.String)
extern "C"  void FriendInfo_set_FriendPlayFabId_m938911984 (FriendInfo_t3708701852 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFriendPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.FriendInfo::get_Username()
extern "C"  String_t* FriendInfo_get_Username_m1030582239 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::set_Username(System.String)
extern "C"  void FriendInfo_set_Username_m4218965740 (FriendInfo_t3708701852 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.FriendInfo::get_TitleDisplayName()
extern "C"  String_t* FriendInfo_get_TitleDisplayName_m4291708990 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleDisplayNameU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::set_TitleDisplayName(System.String)
extern "C"  void FriendInfo_set_TitleDisplayName_m69783533 (FriendInfo_t3708701852 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleDisplayNameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.FriendInfo::get_Tags()
extern "C"  List_1_t1765447871 * FriendInfo_get_Tags_m3880108500 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CTagsU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::set_Tags(System.Collections.Generic.List`1<System.String>)
extern "C"  void FriendInfo_set_Tags_m1288459277 (FriendInfo_t3708701852 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CTagsU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.FriendInfo::get_CurrentMatchmakerLobbyId()
extern "C"  String_t* FriendInfo_get_CurrentMatchmakerLobbyId_m3246713538 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::set_CurrentMatchmakerLobbyId(System.String)
extern "C"  void FriendInfo_set_CurrentMatchmakerLobbyId_m944958185 (FriendInfo_t3708701852 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCurrentMatchmakerLobbyIdU3Ek__BackingField_4(L_0);
		return;
	}
}
// PlayFab.ClientModels.UserFacebookInfo PlayFab.ClientModels.FriendInfo::get_FacebookInfo()
extern "C"  UserFacebookInfo_t1491589167 * FriendInfo_get_FacebookInfo_m1029124362 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		UserFacebookInfo_t1491589167 * L_0 = __this->get_U3CFacebookInfoU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::set_FacebookInfo(PlayFab.ClientModels.UserFacebookInfo)
extern "C"  void FriendInfo_set_FacebookInfo_m3869497633 (FriendInfo_t3708701852 * __this, UserFacebookInfo_t1491589167 * ___value0, const MethodInfo* method)
{
	{
		UserFacebookInfo_t1491589167 * L_0 = ___value0;
		__this->set_U3CFacebookInfoU3Ek__BackingField_5(L_0);
		return;
	}
}
// PlayFab.ClientModels.UserSteamInfo PlayFab.ClientModels.FriendInfo::get_SteamInfo()
extern "C"  UserSteamInfo_t2867463811 * FriendInfo_get_SteamInfo_m131235292 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		UserSteamInfo_t2867463811 * L_0 = __this->get_U3CSteamInfoU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::set_SteamInfo(PlayFab.ClientModels.UserSteamInfo)
extern "C"  void FriendInfo_set_SteamInfo_m1500091051 (FriendInfo_t3708701852 * __this, UserSteamInfo_t2867463811 * ___value0, const MethodInfo* method)
{
	{
		UserSteamInfo_t2867463811 * L_0 = ___value0;
		__this->set_U3CSteamInfoU3Ek__BackingField_6(L_0);
		return;
	}
}
// PlayFab.ClientModels.UserGameCenterInfo PlayFab.ClientModels.FriendInfo::get_GameCenterInfo()
extern "C"  UserGameCenterInfo_t386684176 * FriendInfo_get_GameCenterInfo_m3422190732 (FriendInfo_t3708701852 * __this, const MethodInfo* method)
{
	{
		UserGameCenterInfo_t386684176 * L_0 = __this->get_U3CGameCenterInfoU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.FriendInfo::set_GameCenterInfo(PlayFab.ClientModels.UserGameCenterInfo)
extern "C"  void FriendInfo_set_GameCenterInfo_m2788939743 (FriendInfo_t3708701852 * __this, UserGameCenterInfo_t386684176 * ___value0, const MethodInfo* method)
{
	{
		UserGameCenterInfo_t386684176 * L_0 = ___value0;
		__this->set_U3CGameCenterInfoU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GameCenterPlayFabIdPair::.ctor()
extern "C"  void GameCenterPlayFabIdPair__ctor_m3199140056 (GameCenterPlayFabIdPair_t3063128049 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GameCenterPlayFabIdPair::get_GameCenterId()
extern "C"  String_t* GameCenterPlayFabIdPair_get_GameCenterId_m1283802362 (GameCenterPlayFabIdPair_t3063128049 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CGameCenterIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameCenterPlayFabIdPair::set_GameCenterId(System.String)
extern "C"  void GameCenterPlayFabIdPair_set_GameCenterId_m2286187671 (GameCenterPlayFabIdPair_t3063128049 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CGameCenterIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GameCenterPlayFabIdPair::get_PlayFabId()
extern "C"  String_t* GameCenterPlayFabIdPair_get_PlayFabId_m3143702136 (GameCenterPlayFabIdPair_t3063128049 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameCenterPlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void GameCenterPlayFabIdPair_set_PlayFabId_m2051150235 (GameCenterPlayFabIdPair_t3063128049 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::.ctor()
extern "C"  void GameInfo__ctor_m3710540877 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.GameInfo::get_Region()
extern "C"  Nullable_1_t212373688  GameInfo_get_Region_m56677550 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t212373688  L_0 = __this->get_U3CRegionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_Region(System.Nullable`1<PlayFab.ClientModels.Region>)
extern "C"  void GameInfo_set_Region_m142143473 (GameInfo_t1767394288 * __this, Nullable_1_t212373688  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t212373688  L_0 = ___value0;
		__this->set_U3CRegionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GameInfo::get_LobbyID()
extern "C"  String_t* GameInfo_get_LobbyID_m476287254 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CLobbyIDU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_LobbyID(System.String)
extern "C"  void GameInfo_set_LobbyID_m2655991779 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLobbyIDU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GameInfo::get_BuildVersion()
extern "C"  String_t* GameInfo_get_BuildVersion_m959845031 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CBuildVersionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_BuildVersion(System.String)
extern "C"  void GameInfo_set_BuildVersion_m2716166116 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBuildVersionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GameInfo::get_GameMode()
extern "C"  String_t* GameInfo_get_GameMode_m1532140562 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CGameModeU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_GameMode(System.String)
extern "C"  void GameInfo_set_GameMode_m4217192665 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CGameModeU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GameInfo::get_StatisticName()
extern "C"  String_t* GameInfo_get_StatisticName_m3982185504 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_StatisticName(System.String)
extern "C"  void GameInfo_set_StatisticName_m1835823065 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GameInfo::get_MaxPlayers()
extern "C"  Nullable_1_t1438485399  GameInfo_get_MaxPlayers_m3258606975 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxPlayersU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_MaxPlayers(System.Nullable`1<System.Int32>)
extern "C"  void GameInfo_set_MaxPlayers_m210157900 (GameInfo_t1767394288 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxPlayersU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GameInfo::get_PlayerUserIds()
extern "C"  List_1_t1765447871 * GameInfo_get_PlayerUserIds_m4219838719 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CPlayerUserIdsU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_PlayerUserIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void GameInfo_set_PlayerUserIds_m2512317070 (GameInfo_t1767394288 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CPlayerUserIdsU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.GameInfo::get_RunTime()
extern "C"  uint32_t GameInfo_get_RunTime_m1536837077 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CRunTimeU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_RunTime(System.UInt32)
extern "C"  void GameInfo_set_RunTime_m4028010100 (GameInfo_t1767394288 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CRunTimeU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GameInfo::get_GameServerState()
extern "C"  String_t* GameInfo_get_GameServerState_m3066545825 (GameInfo_t1767394288 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CGameServerStateU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameInfo::set_GameServerState(System.String)
extern "C"  void GameInfo_set_GameServerState_m3501421880 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CGameServerStateU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GameServerRegionsRequest::.ctor()
extern "C"  void GameServerRegionsRequest__ctor_m957134568 (GameServerRegionsRequest_t2476765941 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GameServerRegionsRequest::get_BuildVersion()
extern "C"  String_t* GameServerRegionsRequest_get_BuildVersion_m3813179180 (GameServerRegionsRequest_t2476765941 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CBuildVersionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameServerRegionsRequest::set_BuildVersion(System.String)
extern "C"  void GameServerRegionsRequest_set_BuildVersion_m4229456255 (GameServerRegionsRequest_t2476765941 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBuildVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GameServerRegionsRequest::get_TitleId()
extern "C"  String_t* GameServerRegionsRequest_get_TitleId_m862606099 (GameServerRegionsRequest_t2476765941 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameServerRegionsRequest::set_TitleId(System.String)
extern "C"  void GameServerRegionsRequest_set_TitleId_m3640554246 (GameServerRegionsRequest_t2476765941 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GameServerRegionsResult::.ctor()
extern "C"  void GameServerRegionsResult__ctor_m2580844146 (GameServerRegionsResult_t253590807 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.RegionInfo> PlayFab.ClientModels.GameServerRegionsResult::get_Regions()
extern "C"  List_1_t4271828715 * GameServerRegionsResult_get_Regions_m3612773221 (GameServerRegionsResult_t253590807 * __this, const MethodInfo* method)
{
	{
		List_1_t4271828715 * L_0 = __this->get_U3CRegionsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GameServerRegionsResult::set_Regions(System.Collections.Generic.List`1<PlayFab.ClientModels.RegionInfo>)
extern "C"  void GameServerRegionsResult_set_Regions_m360066326 (GameServerRegionsResult_t253590807 * __this, List_1_t4271828715 * ___value0, const MethodInfo* method)
{
	{
		List_1_t4271828715 * L_0 = ___value0;
		__this->set_U3CRegionsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::.ctor()
extern "C"  void GetAccountInfoRequest__ctor_m2719384815 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetAccountInfoRequest::get_PlayFabId()
extern "C"  String_t* GetAccountInfoRequest_get_PlayFabId_m3968835279 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::set_PlayFabId(System.String)
extern "C"  void GetAccountInfoRequest_set_PlayFabId_m2541277668 (GetAccountInfoRequest_t2417218426 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetAccountInfoRequest::get_Username()
extern "C"  String_t* GetAccountInfoRequest_get_Username_m35488439 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::set_Username(System.String)
extern "C"  void GetAccountInfoRequest_set_Username_m1827898938 (GetAccountInfoRequest_t2417218426 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetAccountInfoRequest::get_Email()
extern "C"  String_t* GetAccountInfoRequest_get_Email_m4219675453 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::set_Email(System.String)
extern "C"  void GetAccountInfoRequest_set_Email_m2222702774 (GetAccountInfoRequest_t2417218426 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetAccountInfoRequest::get_TitleDisplayName()
extern "C"  String_t* GetAccountInfoRequest_get_TitleDisplayName_m4262069014 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleDisplayNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::set_TitleDisplayName(System.String)
extern "C"  void GetAccountInfoRequest_set_TitleDisplayName_m2874595643 (GetAccountInfoRequest_t2417218426 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleDisplayNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetAccountInfoResult::.ctor()
extern "C"  void GetAccountInfoResult__ctor_m1390764939 (GetAccountInfoResult_t3022616562 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// PlayFab.ClientModels.UserAccountInfo PlayFab.ClientModels.GetAccountInfoResult::get_AccountInfo()
extern "C"  UserAccountInfo_t960776096 * GetAccountInfoResult_get_AccountInfo_m1818471270 (GetAccountInfoResult_t3022616562 * __this, const MethodInfo* method)
{
	{
		UserAccountInfo_t960776096 * L_0 = __this->get_U3CAccountInfoU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetAccountInfoResult::set_AccountInfo(PlayFab.ClientModels.UserAccountInfo)
extern "C"  void GetAccountInfoResult_set_AccountInfo_m4040180661 (GetAccountInfoResult_t3022616562 * __this, UserAccountInfo_t960776096 * ___value0, const MethodInfo* method)
{
	{
		UserAccountInfo_t960776096 * L_0 = ___value0;
		__this->set_U3CAccountInfoU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCatalogItemsRequest::.ctor()
extern "C"  void GetCatalogItemsRequest__ctor_m2643214491 (GetCatalogItemsRequest_t2824754786 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCatalogItemsRequest::get_CatalogVersion()
extern "C"  String_t* GetCatalogItemsRequest_get_CatalogVersion_m3129163278 (GetCatalogItemsRequest_t2824754786 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCatalogItemsRequest::set_CatalogVersion(System.String)
extern "C"  void GetCatalogItemsRequest_set_CatalogVersion_m532795101 (GetCatalogItemsRequest_t2824754786 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCatalogItemsResult::.ctor()
extern "C"  void GetCatalogItemsResult__ctor_m557023839 (GetCatalogItemsResult_t680458250 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem> PlayFab.ClientModels.GetCatalogItemsResult::get_Catalog()
extern "C"  List_1_t634580917 * GetCatalogItemsResult_get_Catalog_m2249827724 (GetCatalogItemsResult_t680458250 * __this, const MethodInfo* method)
{
	{
		List_1_t634580917 * L_0 = __this->get_U3CCatalogU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCatalogItemsResult::set_Catalog(System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>)
extern "C"  void GetCatalogItemsResult_set_Catalog_m3126539079 (GetCatalogItemsResult_t680458250 * __this, List_1_t634580917 * ___value0, const MethodInfo* method)
{
	{
		List_1_t634580917 * L_0 = ___value0;
		__this->set_U3CCatalogU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::.ctor()
extern "C"  void GetCharacterDataRequest__ctor_m4251161319 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterDataRequest::get_PlayFabId()
extern "C"  String_t* GetCharacterDataRequest_get_PlayFabId_m3177589127 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::set_PlayFabId(System.String)
extern "C"  void GetCharacterDataRequest_set_PlayFabId_m1515368172 (GetCharacterDataRequest_t572698882 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterDataRequest::get_CharacterId()
extern "C"  String_t* GetCharacterDataRequest_get_CharacterId_m1859713149 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::set_CharacterId(System.String)
extern "C"  void GetCharacterDataRequest_set_CharacterId_m1556422518 (GetCharacterDataRequest_t572698882 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetCharacterDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetCharacterDataRequest_get_Keys_m2026630251 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CKeysU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetCharacterDataRequest_set_Keys_m1758150434 (GetCharacterDataRequest_t572698882 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CKeysU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetCharacterDataRequest::get_IfChangedFromDataVersion()
extern "C"  Nullable_1_t1438485399  GetCharacterDataRequest_get_IfChangedFromDataVersion_m2303425314 (GetCharacterDataRequest_t572698882 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CIfChangedFromDataVersionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataRequest::set_IfChangedFromDataVersion(System.Nullable`1<System.Int32>)
extern "C"  void GetCharacterDataRequest_set_IfChangedFromDataVersion_m3310937415 (GetCharacterDataRequest_t572698882 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CIfChangedFromDataVersionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataResult::.ctor()
extern "C"  void GetCharacterDataResult__ctor_m54703763 (GetCharacterDataResult_t1300547946 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterDataResult::get_CharacterId()
extern "C"  String_t* GetCharacterDataResult_get_CharacterId_m2587702927 (GetCharacterDataResult_t1300547946 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataResult::set_CharacterId(System.String)
extern "C"  void GetCharacterDataResult_set_CharacterId_m3013253706 (GetCharacterDataResult_t1300547946 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetCharacterDataResult::get_Data()
extern "C"  Dictionary_2_t1801537638 * GetCharacterDataResult_get_Data_m763931554 (GetCharacterDataResult_t1300547946 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1801537638 * L_0 = __this->get_U3CDataU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>)
extern "C"  void GetCharacterDataResult_set_Data_m2932110941 (GetCharacterDataResult_t1300547946 * __this, Dictionary_2_t1801537638 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t1801537638 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.GetCharacterDataResult::get_DataVersion()
extern "C"  uint32_t GetCharacterDataResult_get_DataVersion_m3983620017 (GetCharacterDataResult_t1300547946 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CDataVersionU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterDataResult::set_DataVersion(System.UInt32)
extern "C"  void GetCharacterDataResult_set_DataVersion_m2692042584 (GetCharacterDataResult_t1300547946 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CDataVersionU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryRequest::.ctor()
extern "C"  void GetCharacterInventoryRequest__ctor_m1431733671 (GetCharacterInventoryRequest_t2835266646 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterInventoryRequest::get_PlayFabId()
extern "C"  String_t* GetCharacterInventoryRequest_get_PlayFabId_m2141173997 (GetCharacterInventoryRequest_t2835266646 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryRequest::set_PlayFabId(System.String)
extern "C"  void GetCharacterInventoryRequest_set_PlayFabId_m475460652 (GetCharacterInventoryRequest_t2835266646 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterInventoryRequest::get_CharacterId()
extern "C"  String_t* GetCharacterInventoryRequest_get_CharacterId_m2297185891 (GetCharacterInventoryRequest_t2835266646 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryRequest::set_CharacterId(System.String)
extern "C"  void GetCharacterInventoryRequest_set_CharacterId_m2932675766 (GetCharacterInventoryRequest_t2835266646 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterInventoryRequest::get_CatalogVersion()
extern "C"  String_t* GetCharacterInventoryRequest_get_CatalogVersion_m3736152770 (GetCharacterInventoryRequest_t2835266646 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryRequest::set_CatalogVersion(System.String)
extern "C"  void GetCharacterInventoryRequest_set_CatalogVersion_m3629948137 (GetCharacterInventoryRequest_t2835266646 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::.ctor()
extern "C"  void GetCharacterInventoryResult__ctor_m3150343123 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterInventoryResult::get_PlayFabId()
extern "C"  String_t* GetCharacterInventoryResult_get_PlayFabId_m3033182195 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_PlayFabId(System.String)
extern "C"  void GetCharacterInventoryResult_set_PlayFabId_m2783895936 (GetCharacterInventoryResult_t403702678 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterInventoryResult::get_CharacterId()
extern "C"  String_t* GetCharacterInventoryResult_get_CharacterId_m523604969 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_CharacterId(System.String)
extern "C"  void GetCharacterInventoryResult_set_CharacterId_m840891658 (GetCharacterInventoryResult_t403702678 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetCharacterInventoryResult::get_Inventory()
extern "C"  List_1_t1322103281 * GetCharacterInventoryResult_get_Inventory_m2001290653 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CInventoryU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_Inventory(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void GetCharacterInventoryResult_set_Inventory_m141594458 (GetCharacterInventoryResult_t403702678 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CInventoryU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetCharacterInventoryResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t190145395 * GetCharacterInventoryResult_get_VirtualCurrency_m2401886175 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetCharacterInventoryResult_set_VirtualCurrency_m3685507796 (GetCharacterInventoryResult_t403702678 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime> PlayFab.ClientModels.GetCharacterInventoryResult::get_VirtualCurrencyRechargeTimes()
extern "C"  Dictionary_2_t3222006224 * GetCharacterInventoryResult_get_VirtualCurrencyRechargeTimes_m933667877 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3222006224 * L_0 = __this->get_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_VirtualCurrencyRechargeTimes(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>)
extern "C"  void GetCharacterInventoryResult_set_VirtualCurrencyRechargeTimes_m128372790 (GetCharacterInventoryResult_t403702678 * __this, Dictionary_2_t3222006224 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t3222006224 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::.ctor()
extern "C"  void GetCharacterLeaderboardRequest__ctor_m1835956232 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterLeaderboardRequest::get_CharacterType()
extern "C"  String_t* GetCharacterLeaderboardRequest_get_CharacterType_m3630568227 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::set_CharacterType(System.String)
extern "C"  void GetCharacterLeaderboardRequest_set_CharacterType_m2156484982 (GetCharacterLeaderboardRequest_t2906494549 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterLeaderboardRequest::get_StatisticName()
extern "C"  String_t* GetCharacterLeaderboardRequest_get_StatisticName_m2182656795 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::set_StatisticName(System.String)
extern "C"  void GetCharacterLeaderboardRequest_set_StatisticName_m925658238 (GetCharacterLeaderboardRequest_t2906494549 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.GetCharacterLeaderboardRequest::get_StartPosition()
extern "C"  int32_t GetCharacterLeaderboardRequest_get_StartPosition_m1705966518 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStartPositionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::set_StartPosition(System.Int32)
extern "C"  void GetCharacterLeaderboardRequest_set_StartPosition_m1286607877 (GetCharacterLeaderboardRequest_t2906494549 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStartPositionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetCharacterLeaderboardRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetCharacterLeaderboardRequest_get_MaxResultsCount_m1417150697 (GetCharacterLeaderboardRequest_t2906494549 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetCharacterLeaderboardRequest_set_MaxResultsCount_m1592714440 (GetCharacterLeaderboardRequest_t2906494549 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardResult::.ctor()
extern "C"  void GetCharacterLeaderboardResult__ctor_m530983250 (GetCharacterLeaderboardResult_t3315494327 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry> PlayFab.ClientModels.GetCharacterLeaderboardResult::get_Leaderboard()
extern "C"  List_1_t3914494247 * GetCharacterLeaderboardResult_get_Leaderboard_m3630628017 (GetCharacterLeaderboardResult_t3315494327 * __this, const MethodInfo* method)
{
	{
		List_1_t3914494247 * L_0 = __this->get_U3CLeaderboardU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>)
extern "C"  void GetCharacterLeaderboardResult_set_Leaderboard_m1342814978 (GetCharacterLeaderboardResult_t3315494327 * __this, List_1_t3914494247 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3914494247 * L_0 = ___value0;
		__this->set_U3CLeaderboardU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterStatisticsRequest::.ctor()
extern "C"  void GetCharacterStatisticsRequest__ctor_m3758040992 (GetCharacterStatisticsRequest_t3373629353 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCharacterStatisticsRequest::get_CharacterId()
extern "C"  String_t* GetCharacterStatisticsRequest_get_CharacterId_m3504203830 (GetCharacterStatisticsRequest_t3373629353 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterStatisticsRequest::set_CharacterId(System.String)
extern "C"  void GetCharacterStatisticsRequest_set_CharacterId_m3213052573 (GetCharacterStatisticsRequest_t3373629353 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterStatisticsResult::.ctor()
extern "C"  void GetCharacterStatisticsResult__ctor_m2532648634 (GetCharacterStatisticsResult_t2637826531 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetCharacterStatisticsResult::get_CharacterStatistics()
extern "C"  Dictionary_2_t190145395 * GetCharacterStatisticsResult_get_CharacterStatistics_m3378762912 (GetCharacterStatisticsResult_t2637826531 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CCharacterStatisticsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCharacterStatisticsResult::set_CharacterStatistics(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetCharacterStatisticsResult_set_CharacterStatistics_m1367997661 (GetCharacterStatisticsResult_t2637826531 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CCharacterStatisticsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCloudScriptUrlRequest::.ctor()
extern "C"  void GetCloudScriptUrlRequest__ctor_m2169897827 (GetCloudScriptUrlRequest_t841424922 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetCloudScriptUrlRequest::get_Version()
extern "C"  Nullable_1_t1438485399  GetCloudScriptUrlRequest_get_Version_m1028552223 (GetCloudScriptUrlRequest_t841424922 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CVersionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCloudScriptUrlRequest::set_Version(System.Nullable`1<System.Int32>)
extern "C"  void GetCloudScriptUrlRequest_set_Version_m2112336658 (GetCloudScriptUrlRequest_t841424922 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetCloudScriptUrlRequest::get_Testing()
extern "C"  Nullable_1_t3097043249  GetCloudScriptUrlRequest_get_Testing_m2762042289 (GetCloudScriptUrlRequest_t841424922 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CTestingU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCloudScriptUrlRequest::set_Testing(System.Nullable`1<System.Boolean>)
extern "C"  void GetCloudScriptUrlRequest_set_Testing_m677392308 (GetCloudScriptUrlRequest_t841424922 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CTestingU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetCloudScriptUrlResult::.ctor()
extern "C"  void GetCloudScriptUrlResult__ctor_m4282533527 (GetCloudScriptUrlResult_t3525973842 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetCloudScriptUrlResult::get_Url()
extern "C"  String_t* GetCloudScriptUrlResult_get_Url_m859101368 (GetCloudScriptUrlResult_t3525973842 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUrlU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetCloudScriptUrlResult::set_Url(System.String)
extern "C"  void GetCloudScriptUrlResult_set_Url_m3887732763 (GetCloudScriptUrlResult_t3525973842 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUrlU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetContentDownloadUrlRequest::.ctor()
extern "C"  void GetContentDownloadUrlRequest__ctor_m1019930882 (GetContentDownloadUrlRequest_t2056984219 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetContentDownloadUrlRequest::get_Key()
extern "C"  String_t* GetContentDownloadUrlRequest_get_Key_m476396153 (GetContentDownloadUrlRequest_t2056984219 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CKeyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetContentDownloadUrlRequest::set_Key(System.String)
extern "C"  void GetContentDownloadUrlRequest_set_Key_m765860704 (GetContentDownloadUrlRequest_t2056984219 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CKeyU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetContentDownloadUrlRequest::get_HttpMethod()
extern "C"  String_t* GetContentDownloadUrlRequest_get_HttpMethod_m202677585 (GetContentDownloadUrlRequest_t2056984219 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CHttpMethodU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetContentDownloadUrlRequest::set_HttpMethod(System.String)
extern "C"  void GetContentDownloadUrlRequest_set_HttpMethod_m3715573562 (GetContentDownloadUrlRequest_t2056984219 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CHttpMethodU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetContentDownloadUrlRequest::get_ThruCDN()
extern "C"  Nullable_1_t3097043249  GetContentDownloadUrlRequest_get_ThruCDN_m3779461878 (GetContentDownloadUrlRequest_t2056984219 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CThruCDNU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetContentDownloadUrlRequest::set_ThruCDN(System.Nullable`1<System.Boolean>)
extern "C"  void GetContentDownloadUrlRequest_set_ThruCDN_m675764367 (GetContentDownloadUrlRequest_t2056984219 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CThruCDNU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetContentDownloadUrlResult::.ctor()
extern "C"  void GetContentDownloadUrlResult__ctor_m920301848 (GetContentDownloadUrlResult_t101502129 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetContentDownloadUrlResult::get_URL()
extern "C"  String_t* GetContentDownloadUrlResult_get_URL_m2591169657 (GetContentDownloadUrlResult_t101502129 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CURLU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetContentDownloadUrlResult::set_URL(System.String)
extern "C"  void GetContentDownloadUrlResult_set_URL_m1443479418 (GetContentDownloadUrlResult_t101502129 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CURLU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::.ctor()
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest__ctor_m205410316 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::get_StatisticName()
extern "C"  String_t* GetFriendLeaderboardAroundCurrentUserRequest_get_StatisticName_m1941564383 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::set_StatisticName(System.String)
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest_set_StatisticName_m4106543354 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetFriendLeaderboardAroundCurrentUserRequest_get_MaxResultsCount_m1044677549 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest_set_MaxResultsCount_m3386655556 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::get_IncludeSteamFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardAroundCurrentUserRequest_get_IncludeSteamFriends_m1161150263 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CIncludeSteamFriendsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::set_IncludeSteamFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest_set_IncludeSteamFriends_m2909565678 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CIncludeSteamFriendsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::get_IncludeFacebookFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardAroundCurrentUserRequest_get_IncludeFacebookFriends_m3429366335 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CIncludeFacebookFriendsU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::set_IncludeFacebookFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest_set_IncludeFacebookFriends_m2479440268 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CIncludeFacebookFriendsU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult::.ctor()
extern "C"  void GetFriendLeaderboardAroundCurrentUserResult__ctor_m4080615630 (GetFriendLeaderboardAroundCurrentUserResult_t659631163 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetFriendLeaderboardAroundCurrentUserResult_get_Leaderboard_m1620700299 (GetFriendLeaderboardAroundCurrentUserResult_t659631163 * __this, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = __this->get_U3CLeaderboardU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetFriendLeaderboardAroundCurrentUserResult_set_Leaderboard_m2879914404 (GetFriendLeaderboardAroundCurrentUserResult_t659631163 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = ___value0;
		__this->set_U3CLeaderboardU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::.ctor()
extern "C"  void GetFriendLeaderboardAroundPlayerRequest__ctor_m4046364449 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_StatisticName()
extern "C"  String_t* GetFriendLeaderboardAroundPlayerRequest_get_StatisticName_m3917264334 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_StatisticName(System.String)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_StatisticName_m3041870725 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetFriendLeaderboardAroundPlayerRequest_get_MaxResultsCount_m4251969732 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_MaxResultsCount_m1181676111 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_PlayFabId()
extern "C"  String_t* GetFriendLeaderboardAroundPlayerRequest_get_PlayFabId_m1270825409 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_PlayFabId(System.String)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_PlayFabId_m41118450 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_IncludeSteamFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardAroundPlayerRequest_get_IncludeSteamFriends_m954459098 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CIncludeSteamFriendsU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_IncludeSteamFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_IncludeSteamFriends_m2632215993 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CIncludeSteamFriendsU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::get_IncludeFacebookFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardAroundPlayerRequest_get_IncludeFacebookFriends_m581004988 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CIncludeFacebookFriendsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest::set_IncludeFacebookFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardAroundPlayerRequest_set_IncludeFacebookFriends_m3472051937 (GetFriendLeaderboardAroundPlayerRequest_t16887688 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CIncludeFacebookFriendsU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult::.ctor()
extern "C"  void GetFriendLeaderboardAroundPlayerResult__ctor_m740834073 (GetFriendLeaderboardAroundPlayerResult_t589881892 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetFriendLeaderboardAroundPlayerResult_get_Leaderboard_m262406048 (GetFriendLeaderboardAroundPlayerResult_t589881892 * __this, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = __this->get_U3CLeaderboardU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetFriendLeaderboardAroundPlayerResult_set_Leaderboard_m3862934255 (GetFriendLeaderboardAroundPlayerResult_t589881892 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = ___value0;
		__this->set_U3CLeaderboardU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::.ctor()
extern "C"  void GetFriendLeaderboardRequest__ctor_m222670451 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetFriendLeaderboardRequest::get_StatisticName()
extern "C"  String_t* GetFriendLeaderboardRequest_get_StatisticName_m3396472224 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_StatisticName(System.String)
extern "C"  void GetFriendLeaderboardRequest_set_StatisticName_m3834885235 (GetFriendLeaderboardRequest_t2919058678 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.GetFriendLeaderboardRequest::get_StartPosition()
extern "C"  int32_t GetFriendLeaderboardRequest_get_StartPosition_m1841859653 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStartPositionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_StartPosition(System.Int32)
extern "C"  void GetFriendLeaderboardRequest_set_StartPosition_m133527920 (GetFriendLeaderboardRequest_t2919058678 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStartPositionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetFriendLeaderboardRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetFriendLeaderboardRequest_get_MaxResultsCount_m716341782 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetFriendLeaderboardRequest_set_MaxResultsCount_m3444547645 (GetFriendLeaderboardRequest_t2919058678 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardRequest::get_IncludeSteamFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardRequest_get_IncludeSteamFriends_m3244027180 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CIncludeSteamFriendsU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_IncludeSteamFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardRequest_set_IncludeSteamFriends_m1071280423 (GetFriendLeaderboardRequest_t2919058678 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CIncludeSteamFriendsU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardRequest::get_IncludeFacebookFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardRequest_get_IncludeFacebookFriends_m728108074 (GetFriendLeaderboardRequest_t2919058678 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CIncludeFacebookFriendsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendLeaderboardRequest::set_IncludeFacebookFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardRequest_set_IncludeFacebookFriends_m3251399859 (GetFriendLeaderboardRequest_t2919058678 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CIncludeFacebookFriendsU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetFriendsListRequest::.ctor()
extern "C"  void GetFriendsListRequest__ctor_m160951367 (GetFriendsListRequest_t3925362978 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendsListRequest::get_IncludeSteamFriends()
extern "C"  Nullable_1_t3097043249  GetFriendsListRequest_get_IncludeSteamFriends_m2702834496 (GetFriendsListRequest_t3925362978 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CIncludeSteamFriendsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendsListRequest::set_IncludeSteamFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendsListRequest_set_IncludeSteamFriends_m3396397779 (GetFriendsListRequest_t3925362978 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CIncludeSteamFriendsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendsListRequest::get_IncludeFacebookFriends()
extern "C"  Nullable_1_t3097043249  GetFriendsListRequest_get_IncludeFacebookFriends_m1364088214 (GetFriendsListRequest_t3925362978 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CIncludeFacebookFriendsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendsListRequest::set_IncludeFacebookFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendsListRequest_set_IncludeFacebookFriends_m1590002567 (GetFriendsListRequest_t3925362978 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CIncludeFacebookFriendsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetFriendsListResult::.ctor()
extern "C"  void GetFriendsListResult__ctor_m476950835 (GetFriendsListResult_t2794171722 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.FriendInfo> PlayFab.ClientModels.GetFriendsListResult::get_Friends()
extern "C"  List_1_t210693525 * GetFriendsListResult_get_Friends_m1569731336 (GetFriendsListResult_t2794171722 * __this, const MethodInfo* method)
{
	{
		List_1_t210693525 * L_0 = __this->get_U3CFriendsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetFriendsListResult::set_Friends(System.Collections.Generic.List`1<PlayFab.ClientModels.FriendInfo>)
extern "C"  void GetFriendsListResult_set_Friends_m4063336855 (GetFriendsListResult_t2794171722 * __this, List_1_t210693525 * ___value0, const MethodInfo* method)
{
	{
		List_1_t210693525 * L_0 = ___value0;
		__this->set_U3CFriendsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::.ctor()
extern "C"  void GetLeaderboardAroundCharacterRequest__ctor_m2917647379 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardAroundCharacterRequest_get_StatisticName_m425992294 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardAroundCharacterRequest_set_StatisticName_m3654759123 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::get_CharacterId()
extern "C"  String_t* GetLeaderboardAroundCharacterRequest_get_CharacterId_m314448591 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::set_CharacterId(System.String)
extern "C"  void GetLeaderboardAroundCharacterRequest_set_CharacterId_m4099824842 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::get_CharacterType()
extern "C"  String_t* GetLeaderboardAroundCharacterRequest_get_CharacterType_m1873903726 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterTypeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::set_CharacterType(System.String)
extern "C"  void GetLeaderboardAroundCharacterRequest_set_CharacterType_m590618571 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterTypeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetLeaderboardAroundCharacterRequest_get_MaxResultsCount_m2778807924 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetLeaderboardAroundCharacterRequest_set_MaxResultsCount_m2235838109 (GetLeaderboardAroundCharacterRequest_t1108200042 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterResult::.ctor()
extern "C"  void GetLeaderboardAroundCharacterResult__ctor_m2921181159 (GetLeaderboardAroundCharacterResult_t2426200834 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardAroundCharacterResult::get_Leaderboard()
extern "C"  List_1_t3914494247 * GetLeaderboardAroundCharacterResult_get_Leaderboard_m3439994694 (GetLeaderboardAroundCharacterResult_t2426200834 * __this, const MethodInfo* method)
{
	{
		List_1_t3914494247 * L_0 = __this->get_U3CLeaderboardU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCharacterResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>)
extern "C"  void GetLeaderboardAroundCharacterResult_set_Leaderboard_m1471145805 (GetLeaderboardAroundCharacterResult_t2426200834 * __this, List_1_t3914494247 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3914494247 * L_0 = ___value0;
		__this->set_U3CLeaderboardU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::.ctor()
extern "C"  void GetLeaderboardAroundCurrentUserRequest__ctor_m4226926158 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardAroundCurrentUserRequest_get_StatisticName_m3750163809 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardAroundCurrentUserRequest_set_StatisticName_m2865338616 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetLeaderboardAroundCurrentUserRequest_get_MaxResultsCount_m936918447 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetLeaderboardAroundCurrentUserRequest_set_MaxResultsCount_m757272642 (GetLeaderboardAroundCurrentUserRequest_t545060687 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult::.ctor()
extern "C"  void GetLeaderboardAroundCurrentUserResult__ctor_m469563980 (GetLeaderboardAroundCurrentUserResult_t3100771709 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetLeaderboardAroundCurrentUserResult_get_Leaderboard_m3075470537 (GetLeaderboardAroundCurrentUserResult_t3100771709 * __this, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = __this->get_U3CLeaderboardU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundCurrentUserResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetLeaderboardAroundCurrentUserResult_set_Leaderboard_m849064226 (GetLeaderboardAroundCurrentUserResult_t3100771709 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = ___value0;
		__this->set_U3CLeaderboardU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::.ctor()
extern "C"  void GetLeaderboardAroundPlayerRequest__ctor_m861325215 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::get_PlayFabId()
extern "C"  String_t* GetLeaderboardAroundPlayerRequest_get_PlayFabId_m475209215 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::set_PlayFabId(System.String)
extern "C"  void GetLeaderboardAroundPlayerRequest_set_PlayFabId_m1145445684 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardAroundPlayerRequest_get_StatisticName_m1479295756 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardAroundPlayerRequest_set_StatisticName_m2384135367 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetLeaderboardAroundPlayerRequest_get_MaxResultsCount_m1879864706 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetLeaderboardAroundPlayerRequest_set_MaxResultsCount_m1547665041 (GetLeaderboardAroundPlayerRequest_t299203018 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerResult::.ctor()
extern "C"  void GetLeaderboardAroundPlayerResult__ctor_m3963226843 (GetLeaderboardAroundPlayerResult_t460441506 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardAroundPlayerResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetLeaderboardAroundPlayerResult_get_Leaderboard_m901597346 (GetLeaderboardAroundPlayerResult_t460441506 * __this, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = __this->get_U3CLeaderboardU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardAroundPlayerResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetLeaderboardAroundPlayerResult_set_Leaderboard_m2608843953 (GetLeaderboardAroundPlayerResult_t460441506 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = ___value0;
		__this->set_U3CLeaderboardU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::.ctor()
extern "C"  void GetLeaderboardForUsersCharactersRequest__ctor_m1929319258 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardForUsersCharactersRequest_get_StatisticName_m2909510791 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardForUsersCharactersRequest_set_StatisticName_m3230454124 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::get_MaxResultsCount()
extern "C"  int32_t GetLeaderboardForUsersCharactersRequest_get_MaxResultsCount_m1455258046 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::set_MaxResultsCount(System.Int32)
extern "C"  void GetLeaderboardForUsersCharactersRequest_set_MaxResultsCount_m2549597673 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersResult::.ctor()
extern "C"  void GetLeaderboardForUsersCharactersResult__ctor_m4274772928 (GetLeaderboardForUsersCharactersResult_t1964611997 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardForUsersCharactersResult::get_Leaderboard()
extern "C"  List_1_t3914494247 * GetLeaderboardForUsersCharactersResult_get_Leaderboard_m1402436709 (GetLeaderboardForUsersCharactersResult_t1964611997 * __this, const MethodInfo* method)
{
	{
		List_1_t3914494247 * L_0 = __this->get_U3CLeaderboardU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>)
extern "C"  void GetLeaderboardForUsersCharactersResult_set_Leaderboard_m1007051732 (GetLeaderboardForUsersCharactersResult_t1964611997 * __this, List_1_t3914494247 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3914494247 * L_0 = ___value0;
		__this->set_U3CLeaderboardU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardRequest::.ctor()
extern "C"  void GetLeaderboardRequest__ctor_m2214077425 (GetLeaderboardRequest_t1150085688 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetLeaderboardRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardRequest_get_StatisticName_m403507166 (GetLeaderboardRequest_t1150085688 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardRequest_set_StatisticName_m907397301 (GetLeaderboardRequest_t1150085688 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.GetLeaderboardRequest::get_StartPosition()
extern "C"  int32_t GetLeaderboardRequest_get_StartPosition_m2087199299 (GetLeaderboardRequest_t1150085688 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStartPositionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardRequest::set_StartPosition(System.Int32)
extern "C"  void GetLeaderboardRequest_set_StartPosition_m731829486 (GetLeaderboardRequest_t1150085688 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStartPositionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetLeaderboardRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetLeaderboardRequest_get_MaxResultsCount_m874363860 (GetLeaderboardRequest_t1150085688 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CMaxResultsCountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetLeaderboardRequest_set_MaxResultsCount_m2087649151 (GetLeaderboardRequest_t1150085688 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CMaxResultsCountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardResult::.ctor()
extern "C"  void GetLeaderboardResult__ctor_m1790106697 (GetLeaderboardResult_t2566099316 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetLeaderboardResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetLeaderboardResult_get_Leaderboard_m3503071248 (GetLeaderboardResult_t2566099316 * __this, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = __this->get_U3CLeaderboardU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetLeaderboardResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetLeaderboardResult_set_Leaderboard_m3945022495 (GetLeaderboardResult_t2566099316 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3038850335 * L_0 = ___value0;
		__this->set_U3CLeaderboardU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest::.ctor()
extern "C"  void GetPhotonAuthenticationTokenRequest__ctor_m3667973305 (GetPhotonAuthenticationTokenRequest_t2686429680 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest::get_PhotonApplicationId()
extern "C"  String_t* GetPhotonAuthenticationTokenRequest_get_PhotonApplicationId_m738460890 (GetPhotonAuthenticationTokenRequest_t2686429680 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPhotonApplicationIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest::set_PhotonApplicationId(System.String)
extern "C"  void GetPhotonAuthenticationTokenRequest_set_PhotonApplicationId_m2534886841 (GetPhotonAuthenticationTokenRequest_t2686429680 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPhotonApplicationIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPhotonAuthenticationTokenResult::.ctor()
extern "C"  void GetPhotonAuthenticationTokenResult__ctor_m2806837889 (GetPhotonAuthenticationTokenResult_t3031300796 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetPhotonAuthenticationTokenResult::get_PhotonCustomAuthenticationToken()
extern "C"  String_t* GetPhotonAuthenticationTokenResult_get_PhotonCustomAuthenticationToken_m181991533 (GetPhotonAuthenticationTokenResult_t3031300796 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPhotonAuthenticationTokenResult::set_PhotonCustomAuthenticationToken(System.String)
extern "C"  void GetPhotonAuthenticationTokenResult_set_PhotonCustomAuthenticationToken_m1124090668 (GetPhotonAuthenticationTokenResult_t3031300796 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPhotonCustomAuthenticationTokenU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerStatisticsRequest::.ctor()
extern "C"  void GetPlayerStatisticsRequest__ctor_m81652312 (GetPlayerStatisticsRequest_t3788214021 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayerStatisticsRequest::get_StatisticNames()
extern "C"  List_1_t1765447871 * GetPlayerStatisticsRequest_get_StatisticNames_m3522747644 (GetPlayerStatisticsRequest_t3788214021 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CStatisticNamesU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerStatisticsRequest::set_StatisticNames(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayerStatisticsRequest_set_StatisticNames_m1979562741 (GetPlayerStatisticsRequest_t3788214021 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CStatisticNamesU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerStatisticsResult::.ctor()
extern "C"  void GetPlayerStatisticsResult__ctor_m1582771458 (GetPlayerStatisticsResult_t3759578887 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticValue> PlayFab.ClientModels.GetPlayerStatisticsResult::get_Statistics()
extern "C"  List_1_t3990614826 * GetPlayerStatisticsResult_get_Statistics_m3488335438 (GetPlayerStatisticsResult_t3759578887 * __this, const MethodInfo* method)
{
	{
		List_1_t3990614826 * L_0 = __this->get_U3CStatisticsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerStatisticsResult::set_Statistics(System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticValue>)
extern "C"  void GetPlayerStatisticsResult_set_Statistics_m3130283013 (GetPlayerStatisticsResult_t3759578887 * __this, List_1_t3990614826 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3990614826 * L_0 = ___value0;
		__this->set_U3CStatisticsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerTradesRequest::.ctor()
extern "C"  void GetPlayerTradesRequest__ctor_m1877697860 (GetPlayerTradesRequest_t3393757081 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.TradeStatus> PlayFab.ClientModels.GetPlayerTradesRequest::get_StatusFilter()
extern "C"  Nullable_1_t1403402234  GetPlayerTradesRequest_get_StatusFilter_m1847635291 (GetPlayerTradesRequest_t3393757081 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1403402234  L_0 = __this->get_U3CStatusFilterU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerTradesRequest::set_StatusFilter(System.Nullable`1<PlayFab.ClientModels.TradeStatus>)
extern "C"  void GetPlayerTradesRequest_set_StatusFilter_m4170321200 (GetPlayerTradesRequest_t3393757081 * __this, Nullable_1_t1403402234  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1403402234  L_0 = ___value0;
		__this->set_U3CStatusFilterU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerTradesResponse::.ctor()
extern "C"  void GetPlayerTradesResponse__ctor_m1143086322 (GetPlayerTradesResponse_t1092460183 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo> PlayFab.ClientModels.GetPlayerTradesResponse::get_OpenedTrades()
extern "C"  List_1_t2730771739 * GetPlayerTradesResponse_get_OpenedTrades_m3320506074 (GetPlayerTradesResponse_t1092460183 * __this, const MethodInfo* method)
{
	{
		List_1_t2730771739 * L_0 = __this->get_U3COpenedTradesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerTradesResponse::set_OpenedTrades(System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>)
extern "C"  void GetPlayerTradesResponse_set_OpenedTrades_m638280767 (GetPlayerTradesResponse_t1092460183 * __this, List_1_t2730771739 * ___value0, const MethodInfo* method)
{
	{
		List_1_t2730771739 * L_0 = ___value0;
		__this->set_U3COpenedTradesU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo> PlayFab.ClientModels.GetPlayerTradesResponse::get_AcceptedTrades()
extern "C"  List_1_t2730771739 * GetPlayerTradesResponse_get_AcceptedTrades_m1090847704 (GetPlayerTradesResponse_t1092460183 * __this, const MethodInfo* method)
{
	{
		List_1_t2730771739 * L_0 = __this->get_U3CAcceptedTradesU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayerTradesResponse::set_AcceptedTrades(System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>)
extern "C"  void GetPlayerTradesResponse_set_AcceptedTrades_m878075457 (GetPlayerTradesResponse_t1092460183 * __this, List_1_t2730771739 * ___value0, const MethodInfo* method)
{
	{
		List_1_t2730771739 * L_0 = ___value0;
		__this->set_U3CAcceptedTradesU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromFacebookIDsRequest__ctor_m2985717559 (GetPlayFabIDsFromFacebookIDsRequest_t270576050 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest::get_FacebookIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromFacebookIDsRequest_get_FacebookIDs_m2155500877 (GetPlayFabIDsFromFacebookIDsRequest_t270576050 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CFacebookIDsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest::set_FacebookIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromFacebookIDsRequest_set_FacebookIDs_m1677113374 (GetPlayFabIDsFromFacebookIDsRequest_t270576050 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CFacebookIDsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromFacebookIDsResult__ctor_m2646282307 (GetPlayFabIDsFromFacebookIDsResult_t3230464698 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.FacebookPlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult::get_Data()
extern "C"  List_1_t2865784683 * GetPlayFabIDsFromFacebookIDsResult_get_Data_m326376125 (GetPlayFabIDsFromFacebookIDsResult_t3230464698 * __this, const MethodInfo* method)
{
	{
		List_1_t2865784683 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.FacebookPlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromFacebookIDsResult_set_Data_m705063502 (GetPlayFabIDsFromFacebookIDsResult_t3230464698 * __this, List_1_t2865784683 * ___value0, const MethodInfo* method)
{
	{
		List_1_t2865784683 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromGameCenterIDsRequest__ctor_m1283204406 (GetPlayFabIDsFromGameCenterIDsRequest_t1196231507 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsRequest::get_GameCenterIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromGameCenterIDsRequest_get_GameCenterIDs_m3524220683 (GetPlayFabIDsFromGameCenterIDsRequest_t1196231507 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CGameCenterIDsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsRequest::set_GameCenterIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromGameCenterIDsRequest_set_GameCenterIDs_m3488186972 (GetPlayFabIDsFromGameCenterIDsRequest_t1196231507 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CGameCenterIDsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromGameCenterIDsResult__ctor_m2729909860 (GetPlayFabIDsFromGameCenterIDsResult_t627925241 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.GameCenterPlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsResult::get_Data()
extern "C"  List_1_t3860087018 * GetPlayFabIDsFromGameCenterIDsResult_get_Data_m1742347037 (GetPlayFabIDsFromGameCenterIDsResult_t627925241 * __this, const MethodInfo* method)
{
	{
		List_1_t3860087018 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.GameCenterPlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromGameCenterIDsResult_set_Data_m3641332654 (GetPlayFabIDsFromGameCenterIDsResult_t627925241 * __this, List_1_t3860087018 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3860087018 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromGoogleIDsRequest__ctor_m2357160228 (GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest::get_GoogleIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromGoogleIDsRequest_get_GoogleIDs_m1138810087 (GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CGoogleIDsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest::set_GoogleIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromGoogleIDsRequest_set_GoogleIDs_m3866847032 (GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CGoogleIDsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromGoogleIDsResult__ctor_m409248950 (GetPlayFabIDsFromGoogleIDsResult_t2496174951 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.GooglePlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult::get_Data()
extern "C"  List_1_t1460113112 * GetPlayFabIDsFromGoogleIDsResult_get_Data_m3121458781 (GetPlayFabIDsFromGoogleIDsResult_t2496174951 * __this, const MethodInfo* method)
{
	{
		List_1_t1460113112 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.GooglePlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromGoogleIDsResult_set_Data_m1896799726 (GetPlayFabIDsFromGoogleIDsResult_t2496174951 * __this, List_1_t1460113112 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1460113112 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromKongregateIDsRequest__ctor_m2508352962 (GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest::get_KongregateIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromKongregateIDsRequest_get_KongregateIDs_m1622940451 (GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CKongregateIDsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest::set_KongregateIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromKongregateIDsRequest_set_KongregateIDs_m627357300 (GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CKongregateIDsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromKongregateIDsResult__ctor_m1522504792 (GetPlayFabIDsFromKongregateIDsResult_t3910051973 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.KongregatePlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult::get_Data()
extern "C"  List_1_t3782140534 * GetPlayFabIDsFromKongregateIDsResult_get_Data_m7444893 (GetPlayFabIDsFromKongregateIDsResult_t3910051973 * __this, const MethodInfo* method)
{
	{
		List_1_t3782140534 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.KongregatePlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromKongregateIDsResult_set_Data_m719029038 (GetPlayFabIDsFromKongregateIDsResult_t3910051973 * __this, List_1_t3782140534 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3782140534 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequest__ctor_m14562139 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::get_PSNAccountIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromPSNAccountIDsRequest_get_PSNAccountIDs_m237230229 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CPSNAccountIDsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::set_PSNAccountIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequest_set_PSNAccountIDs_m717555686 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CPSNAccountIDsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::get_IssuerId()
extern "C"  Nullable_1_t1438485399  GetPlayFabIDsFromPSNAccountIDsRequest_get_IssuerId_m22734325 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CIssuerIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsRequest::set_IssuerId(System.Nullable`1<System.Int32>)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsRequest_set_IssuerId_m982794196 (GetPlayFabIDsFromPSNAccountIDsRequest_t3305934734 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CIssuerIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromPSNAccountIDsResult__ctor_m1719154591 (GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.PSNAccountPlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult::get_Data()
extern "C"  List_1_t1477512911 * GetPlayFabIDsFromPSNAccountIDsResult_get_Data_m1713345149 (GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * __this, const MethodInfo* method)
{
	{
		List_1_t1477512911 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromPSNAccountIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.PSNAccountPlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromPSNAccountIDsResult_set_Data_m2656781518 (GetPlayFabIDsFromPSNAccountIDsResult_t3051284830 * __this, List_1_t1477512911 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1477512911 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromSteamIDsRequest__ctor_m3523419627 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.UInt64> PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::get_SteamIDs()
extern "C"  List_1_t1782884390 * GetPlayFabIDsFromSteamIDsRequest_get_SteamIDs_m3835998034 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, const MethodInfo* method)
{
	{
		List_1_t1782884390 * L_0 = __this->get_U3CSteamIDsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::set_SteamIDs(System.Collections.Generic.List`1<System.UInt64>)
extern "C"  void GetPlayFabIDsFromSteamIDsRequest_set_SteamIDs_m1144414521 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, List_1_t1782884390 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1782884390 * L_0 = ___value0;
		__this->set_U3CSteamIDsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::get_SteamStringIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromSteamIDsRequest_get_SteamStringIDs_m2616253192 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CSteamStringIDsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::set_SteamStringIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromSteamIDsRequest_set_SteamStringIDs_m1377890369 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CSteamStringIDsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromSteamIDsResult__ctor_m2663627535 (GetPlayFabIDsFromSteamIDsResult_t4060808666 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.SteamPlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsResult::get_Data()
extern "C"  List_1_t3481275745 * GetPlayFabIDsFromSteamIDsResult_get_Data_m2220093279 (GetPlayFabIDsFromSteamIDsResult_t4060808666 * __this, const MethodInfo* method)
{
	{
		List_1_t3481275745 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.SteamPlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromSteamIDsResult_set_Data_m1770862998 (GetPlayFabIDsFromSteamIDsResult_t4060808666 * __this, List_1_t3481275745 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3481275745 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPublisherDataRequest::.ctor()
extern "C"  void GetPublisherDataRequest__ctor_m3868250682 (GetPublisherDataRequest_t1266178127 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPublisherDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetPublisherDataRequest_get_Keys_m2168981560 (GetPublisherDataRequest_t1266178127 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CKeysU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPublisherDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPublisherDataRequest_set_Keys_m766881007 (GetPublisherDataRequest_t1266178127 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CKeysU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPublisherDataResult::.ctor()
extern "C"  void GetPublisherDataResult__ctor_m1150730464 (GetPublisherDataResult_t2154202237 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.GetPublisherDataResult::get_Data()
extern "C"  Dictionary_2_t2606186806 * GetPublisherDataResult_get_Data_m3083559785 (GetPublisherDataResult_t2154202237 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPublisherDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetPublisherDataResult_set_Data_m750927932 (GetPublisherDataResult_t2154202237 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseRequest::.ctor()
extern "C"  void GetPurchaseRequest__ctor_m3799503477 (GetPurchaseRequest_t593210440 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetPurchaseRequest::get_OrderId()
extern "C"  String_t* GetPurchaseRequest_get_OrderId_m3474981142 (GetPurchaseRequest_t593210440 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COrderIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseRequest::set_OrderId(System.String)
extern "C"  void GetPurchaseRequest_set_OrderId_m2178478115 (GetPurchaseRequest_t593210440 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrderIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseResult::.ctor()
extern "C"  void GetPurchaseResult__ctor_m1979796805 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetPurchaseResult::get_OrderId()
extern "C"  String_t* GetPurchaseResult_get_OrderId_m2478117824 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COrderIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_OrderId(System.String)
extern "C"  void GetPurchaseResult_set_OrderId_m458711891 (GetPurchaseResult_t54283620 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrderIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetPurchaseResult::get_PaymentProvider()
extern "C"  String_t* GetPurchaseResult_get_PaymentProvider_m18543438 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPaymentProviderU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_PaymentProvider(System.String)
extern "C"  void GetPurchaseResult_set_PaymentProvider_m92608901 (GetPurchaseResult_t54283620 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPaymentProviderU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetPurchaseResult::get_TransactionId()
extern "C"  String_t* GetPurchaseResult_get_TransactionId_m4052469456 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTransactionIdU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_TransactionId(System.String)
extern "C"  void GetPurchaseResult_set_TransactionId_m768743811 (GetPurchaseResult_t54283620 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTransactionIdU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetPurchaseResult::get_TransactionStatus()
extern "C"  String_t* GetPurchaseResult_get_TransactionStatus_m3900134887 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTransactionStatusU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_TransactionStatus(System.String)
extern "C"  void GetPurchaseResult_set_TransactionStatus_m1993996108 (GetPurchaseResult_t54283620 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTransactionStatusU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.DateTime PlayFab.ClientModels.GetPurchaseResult::get_PurchaseDate()
extern "C"  DateTime_t339033936  GetPurchaseResult_get_PurchaseDate_m190212752 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = __this->get_U3CPurchaseDateU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_PurchaseDate(System.DateTime)
extern "C"  void GetPurchaseResult_set_PurchaseDate_m3153302125 (GetPurchaseResult_t54283620 * __this, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ___value0;
		__this->set_U3CPurchaseDateU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetPurchaseResult::get_Items()
extern "C"  List_1_t1322103281 * GetPurchaseResult_get_Items_m3445802675 (GetPurchaseResult_t54283620 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CItemsU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetPurchaseResult::set_Items(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void GetPurchaseResult_set_Items_m3405947824 (GetPurchaseResult_t54283620 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CItemsU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetSharedGroupDataRequest::.ctor()
extern "C"  void GetSharedGroupDataRequest__ctor_m1452009048 (GetSharedGroupDataRequest_t3614043377 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetSharedGroupDataRequest::get_SharedGroupId()
extern "C"  String_t* GetSharedGroupDataRequest_get_SharedGroupId_m3186429599 (GetSharedGroupDataRequest_t3614043377 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSharedGroupIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetSharedGroupDataRequest::set_SharedGroupId(System.String)
extern "C"  void GetSharedGroupDataRequest_set_SharedGroupId_m347475220 (GetSharedGroupDataRequest_t3614043377 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSharedGroupIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetSharedGroupDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetSharedGroupDataRequest_get_Keys_m1853890330 (GetSharedGroupDataRequest_t3614043377 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CKeysU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetSharedGroupDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetSharedGroupDataRequest_set_Keys_m88879121 (GetSharedGroupDataRequest_t3614043377 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CKeysU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetSharedGroupDataRequest::get_GetMembers()
extern "C"  Nullable_1_t3097043249  GetSharedGroupDataRequest_get_GetMembers_m3879334625 (GetSharedGroupDataRequest_t3614043377 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CGetMembersU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetSharedGroupDataRequest::set_GetMembers(System.Nullable`1<System.Boolean>)
extern "C"  void GetSharedGroupDataRequest_set_GetMembers_m4191086236 (GetSharedGroupDataRequest_t3614043377 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CGetMembersU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetSharedGroupDataResult::.ctor()
extern "C"  void GetSharedGroupDataResult__ctor_m1626976514 (GetSharedGroupDataResult_t3615413147 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.SharedGroupDataRecord> PlayFab.ClientModels.GetSharedGroupDataResult::get_Data()
extern "C"  Dictionary_2_t444340917 * GetSharedGroupDataResult_get_Data_m2210963048 (GetSharedGroupDataResult_t3615413147 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t444340917 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetSharedGroupDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.SharedGroupDataRecord>)
extern "C"  void GetSharedGroupDataResult_set_Data_m2801475523 (GetSharedGroupDataResult_t3615413147 * __this, Dictionary_2_t444340917 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t444340917 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetSharedGroupDataResult::get_Members()
extern "C"  List_1_t1765447871 * GetSharedGroupDataResult_get_Members_m1361970817 (GetSharedGroupDataResult_t3615413147 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CMembersU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetSharedGroupDataResult::set_Members(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetSharedGroupDataResult_set_Members_m2652825168 (GetSharedGroupDataResult_t3615413147 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CMembersU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetStoreItemsRequest::.ctor()
extern "C"  void GetStoreItemsRequest__ctor_m2862126291 (GetStoreItemsRequest_t3204724138 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetStoreItemsRequest::get_StoreId()
extern "C"  String_t* GetStoreItemsRequest_get_StoreId_m1961920583 (GetStoreItemsRequest_t3204724138 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStoreIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetStoreItemsRequest::set_StoreId(System.String)
extern "C"  void GetStoreItemsRequest_set_StoreId_m3574934482 (GetStoreItemsRequest_t3204724138 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStoreIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetStoreItemsRequest::get_CatalogVersion()
extern "C"  String_t* GetStoreItemsRequest_get_CatalogVersion_m3708874774 (GetStoreItemsRequest_t3204724138 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetStoreItemsRequest::set_CatalogVersion(System.String)
extern "C"  void GetStoreItemsRequest_set_CatalogVersion_m3641232661 (GetStoreItemsRequest_t3204724138 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetStoreItemsResult::.ctor()
extern "C"  void GetStoreItemsResult__ctor_m1672464167 (GetStoreItemsResult_t831262658 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem> PlayFab.ClientModels.GetStoreItemsResult::get_Store()
extern "C"  List_1_t3669843069 * GetStoreItemsResult_get_Store_m3520969940 (GetStoreItemsResult_t831262658 * __this, const MethodInfo* method)
{
	{
		List_1_t3669843069 * L_0 = __this->get_U3CStoreU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetStoreItemsResult::set_Store(System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>)
extern "C"  void GetStoreItemsResult_set_Store_m2336661695 (GetStoreItemsResult_t831262658 * __this, List_1_t3669843069 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3669843069 * L_0 = ___value0;
		__this->set_U3CStoreU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetTitleDataRequest::.ctor()
extern "C"  void GetTitleDataRequest__ctor_m4101590166 (GetTitleDataRequest_t535093619 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetTitleDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetTitleDataRequest_get_Keys_m1158299868 (GetTitleDataRequest_t535093619 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CKeysU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetTitleDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetTitleDataRequest_set_Keys_m2587258131 (GetTitleDataRequest_t535093619 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CKeysU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetTitleDataResult::.ctor()
extern "C"  void GetTitleDataResult__ctor_m1019710212 (GetTitleDataResult_t1299334873 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.GetTitleDataResult::get_Data()
extern "C"  Dictionary_2_t2606186806 * GetTitleDataResult_get_Data_m4083068869 (GetTitleDataResult_t1299334873 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetTitleDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetTitleDataResult_set_Data_m1674290584 (GetTitleDataResult_t1299334873 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetTitleNewsRequest::.ctor()
extern "C"  void GetTitleNewsRequest__ctor_m823948735 (GetTitleNewsRequest_t3432781354 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetTitleNewsRequest::get_Count()
extern "C"  Nullable_1_t1438485399  GetTitleNewsRequest_get_Count_m430093492 (GetTitleNewsRequest_t3432781354 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CCountU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetTitleNewsRequest::set_Count(System.Nullable`1<System.Int32>)
extern "C"  void GetTitleNewsRequest_set_Count_m2625469727 (GetTitleNewsRequest_t3432781354 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CCountU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetTitleNewsResult::.ctor()
extern "C"  void GetTitleNewsResult__ctor_m636885179 (GetTitleNewsResult_t422977346 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.TitleNewsItem> PlayFab.ClientModels.GetTitleNewsResult::get_News()
extern "C"  List_1_t3320275943 * GetTitleNewsResult_get_News_m2836968498 (GetTitleNewsResult_t422977346 * __this, const MethodInfo* method)
{
	{
		List_1_t3320275943 * L_0 = __this->get_U3CNewsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetTitleNewsResult::set_News(System.Collections.Generic.List`1<PlayFab.ClientModels.TitleNewsItem>)
extern "C"  void GetTitleNewsResult_set_News_m1842271737 (GetTitleNewsResult_t422977346 * __this, List_1_t3320275943 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3320275943 * L_0 = ___value0;
		__this->set_U3CNewsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetTradeStatusRequest::.ctor()
extern "C"  void GetTradeStatusRequest__ctor_m2676931818 (GetTradeStatusRequest_t4199327007 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetTradeStatusRequest::get_OfferingPlayerId()
extern "C"  String_t* GetTradeStatusRequest_get_OfferingPlayerId_m173272968 (GetTradeStatusRequest_t4199327007 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COfferingPlayerIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetTradeStatusRequest::set_OfferingPlayerId(System.String)
extern "C"  void GetTradeStatusRequest_set_OfferingPlayerId_m2823829001 (GetTradeStatusRequest_t4199327007 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COfferingPlayerIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetTradeStatusRequest::get_TradeId()
extern "C"  String_t* GetTradeStatusRequest_get_TradeId_m767802427 (GetTradeStatusRequest_t4199327007 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTradeIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetTradeStatusRequest::set_TradeId(System.String)
extern "C"  void GetTradeStatusRequest_set_TradeId_m4237362232 (GetTradeStatusRequest_t4199327007 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTradeIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetTradeStatusResponse::.ctor()
extern "C"  void GetTradeStatusResponse__ctor_m149535244 (GetTradeStatusResponse_t295324113 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.GetTradeStatusResponse::get_Trade()
extern "C"  TradeInfo_t1933812770 * GetTradeStatusResponse_get_Trade_m3820808398 (GetTradeStatusResponse_t295324113 * __this, const MethodInfo* method)
{
	{
		TradeInfo_t1933812770 * L_0 = __this->get_U3CTradeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetTradeStatusResponse::set_Trade(PlayFab.ClientModels.TradeInfo)
extern "C"  void GetTradeStatusResponse_set_Trade_m2966654941 (GetTradeStatusResponse_t295324113 * __this, TradeInfo_t1933812770 * ___value0, const MethodInfo* method)
{
	{
		TradeInfo_t1933812770 * L_0 = ___value0;
		__this->set_U3CTradeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::.ctor()
extern "C"  void GetUserCombinedInfoRequest__ctor_m3517944882 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::get_PlayFabId()
extern "C"  String_t* GetUserCombinedInfoRequest_get_PlayFabId_m309528888 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_PlayFabId(System.String)
extern "C"  void GetUserCombinedInfoRequest_set_PlayFabId_m1550776961 (GetUserCombinedInfoRequest_t3431761899 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::get_Username()
extern "C"  String_t* GetUserCombinedInfoRequest_get_Username_m610182958 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_Username(System.String)
extern "C"  void GetUserCombinedInfoRequest_set_Username_m410473981 (GetUserCombinedInfoRequest_t3431761899 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::get_Email()
extern "C"  String_t* GetUserCombinedInfoRequest_get_Email_m2038835750 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_Email(System.String)
extern "C"  void GetUserCombinedInfoRequest_set_Email_m1483784147 (GetUserCombinedInfoRequest_t3431761899 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::get_TitleDisplayName()
extern "C"  String_t* GetUserCombinedInfoRequest_get_TitleDisplayName_m3669409933 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleDisplayNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_TitleDisplayName(System.String)
extern "C"  void GetUserCombinedInfoRequest_set_TitleDisplayName_m1773942270 (GetUserCombinedInfoRequest_t3431761899 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleDisplayNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetAccountInfo()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetAccountInfo_m2409247799 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CGetAccountInfoU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetAccountInfo(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetAccountInfo_m2619418964 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CGetAccountInfoU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetInventory()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetInventory_m2281453464 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CGetInventoryU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetInventory(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetInventory_m4180974291 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CGetInventoryU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetVirtualCurrency()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetVirtualCurrency_m3644900920 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CGetVirtualCurrencyU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetVirtualCurrency(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetVirtualCurrency_m4098073075 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CGetVirtualCurrencyU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetUserData()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetUserData_m818336315 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CGetUserDataU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetUserData(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetUserData_m3368863146 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CGetUserDataU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_UserDataKeys()
extern "C"  List_1_t1765447871 * GetUserCombinedInfoRequest_get_UserDataKeys_m4138181939 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CUserDataKeysU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_UserDataKeys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetUserCombinedInfoRequest_set_UserDataKeys_m3378071148 (GetUserCombinedInfoRequest_t3431761899 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CUserDataKeysU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetReadOnlyData()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetReadOnlyData_m3967930642 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CGetReadOnlyDataU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetReadOnlyData(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetReadOnlyData_m1707722547 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CGetReadOnlyDataU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_ReadOnlyDataKeys()
extern "C"  List_1_t1765447871 * GetUserCombinedInfoRequest_get_ReadOnlyDataKeys_m1165840010 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CReadOnlyDataKeysU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_ReadOnlyDataKeys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetUserCombinedInfoRequest_set_ReadOnlyDataKeys_m1903770435 (GetUserCombinedInfoRequest_t3431761899 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CReadOnlyDataKeysU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::.ctor()
extern "C"  void GetUserCombinedInfoResult__ctor_m169598952 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GetUserCombinedInfoResult::get_PlayFabId()
extern "C"  String_t* GetUserCombinedInfoResult_get_PlayFabId_m2004265544 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_PlayFabId(System.String)
extern "C"  void GetUserCombinedInfoResult_set_PlayFabId_m2957130891 (GetUserCombinedInfoResult_t1115681121 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// PlayFab.ClientModels.UserAccountInfo PlayFab.ClientModels.GetUserCombinedInfoResult::get_AccountInfo()
extern "C"  UserAccountInfo_t960776096 * GetUserCombinedInfoResult_get_AccountInfo_m3596483085 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		UserAccountInfo_t960776096 * L_0 = __this->get_U3CAccountInfoU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_AccountInfo(PlayFab.ClientModels.UserAccountInfo)
extern "C"  void GetUserCombinedInfoResult_set_AccountInfo_m1366140242 (GetUserCombinedInfoResult_t1115681121 * __this, UserAccountInfo_t960776096 * ___value0, const MethodInfo* method)
{
	{
		UserAccountInfo_t960776096 * L_0 = ___value0;
		__this->set_U3CAccountInfoU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetUserCombinedInfoResult::get_Inventory()
extern "C"  List_1_t1322103281 * GetUserCombinedInfoResult_get_Inventory_m2142760690 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CInventoryU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_Inventory(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void GetUserCombinedInfoResult_set_Inventory_m4186465263 (GetUserCombinedInfoResult_t1115681121 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CInventoryU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserCombinedInfoResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t190145395 * GetUserCombinedInfoResult_get_VirtualCurrency_m680322548 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetUserCombinedInfoResult_set_VirtualCurrency_m3264947039 (GetUserCombinedInfoResult_t1115681121 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime> PlayFab.ClientModels.GetUserCombinedInfoResult::get_VirtualCurrencyRechargeTimes()
extern "C"  Dictionary_2_t3222006224 * GetUserCombinedInfoResult_get_VirtualCurrencyRechargeTimes_m755440880 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3222006224 * L_0 = __this->get_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_VirtualCurrencyRechargeTimes(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>)
extern "C"  void GetUserCombinedInfoResult_set_VirtualCurrencyRechargeTimes_m3324855051 (GetUserCombinedInfoResult_t1115681121 * __this, Dictionary_2_t3222006224 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t3222006224 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetUserCombinedInfoResult::get_Data()
extern "C"  Dictionary_2_t1801537638 * GetUserCombinedInfoResult_get_Data_m2255212337 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1801537638 * L_0 = __this->get_U3CDataU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>)
extern "C"  void GetUserCombinedInfoResult_set_Data_m3986755880 (GetUserCombinedInfoResult_t1115681121 * __this, Dictionary_2_t1801537638 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t1801537638 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.GetUserCombinedInfoResult::get_DataVersion()
extern "C"  uint32_t GetUserCombinedInfoResult_get_DataVersion_m1558072112 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CDataVersionU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_DataVersion(System.UInt32)
extern "C"  void GetUserCombinedInfoResult_set_DataVersion_m3789715043 (GetUserCombinedInfoResult_t1115681121 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CDataVersionU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetUserCombinedInfoResult::get_ReadOnlyData()
extern "C"  Dictionary_2_t1801537638 * GetUserCombinedInfoResult_get_ReadOnlyData_m1576777267 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1801537638 * L_0 = __this->get_U3CReadOnlyDataU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_ReadOnlyData(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>)
extern "C"  void GetUserCombinedInfoResult_set_ReadOnlyData_m1914329002 (GetUserCombinedInfoResult_t1115681121 * __this, Dictionary_2_t1801537638 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t1801537638 * L_0 = ___value0;
		__this->set_U3CReadOnlyDataU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.GetUserCombinedInfoResult::get_ReadOnlyDataVersion()
extern "C"  uint32_t GetUserCombinedInfoResult_get_ReadOnlyDataVersion_m476333806 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CReadOnlyDataVersionU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_ReadOnlyDataVersion(System.UInt32)
extern "C"  void GetUserCombinedInfoResult_set_ReadOnlyDataVersion_m367810149 (GetUserCombinedInfoResult_t1115681121 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CReadOnlyDataVersionU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetUserDataRequest::.ctor()
extern "C"  void GetUserDataRequest__ctor_m292591497 (GetUserDataRequest_t2084642996 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetUserDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetUserDataRequest_get_Keys_m2869316615 (GetUserDataRequest_t2084642996 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CKeysU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetUserDataRequest_set_Keys_m286878528 (GetUserDataRequest_t2084642996 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CKeysU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GetUserDataRequest::get_PlayFabId()
extern "C"  String_t* GetUserDataRequest_get_PlayFabId_m3391920271 (GetUserDataRequest_t2084642996 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserDataRequest::set_PlayFabId(System.String)
extern "C"  void GetUserDataRequest_set_PlayFabId_m2012234634 (GetUserDataRequest_t2084642996 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetUserDataRequest::get_IfChangedFromDataVersion()
extern "C"  Nullable_1_t1438485399  GetUserDataRequest_get_IfChangedFromDataVersion_m2814284290 (GetUserDataRequest_t2084642996 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CIfChangedFromDataVersionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserDataRequest::set_IfChangedFromDataVersion(System.Nullable`1<System.Int32>)
extern "C"  void GetUserDataRequest_set_IfChangedFromDataVersion_m2790841449 (GetUserDataRequest_t2084642996 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CIfChangedFromDataVersionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetUserDataResult::.ctor()
extern "C"  void GetUserDataResult__ctor_m2836501937 (GetUserDataResult_t518036344 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetUserDataResult::get_Data()
extern "C"  Dictionary_2_t1801537638 * GetUserDataResult_get_Data_m671783624 (GetUserDataResult_t518036344 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1801537638 * L_0 = __this->get_U3CDataU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>)
extern "C"  void GetUserDataResult_set_Data_m2033452031 (GetUserDataResult_t518036344 * __this, Dictionary_2_t1801537638 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t1801537638 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.GetUserDataResult::get_DataVersion()
extern "C"  uint32_t GetUserDataResult_get_DataVersion_m3199741625 (GetUserDataResult_t518036344 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CDataVersionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserDataResult::set_DataVersion(System.UInt32)
extern "C"  void GetUserDataResult_set_DataVersion_m915033722 (GetUserDataResult_t518036344 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CDataVersionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetUserInventoryRequest::.ctor()
extern "C"  void GetUserInventoryRequest__ctor_m2839352901 (GetUserInventoryRequest_t2337033188 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetUserInventoryResult::.ctor()
extern "C"  void GetUserInventoryResult__ctor_m1117540213 (GetUserInventoryResult_t1496009288 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetUserInventoryResult::get_Inventory()
extern "C"  List_1_t1322103281 * GetUserInventoryResult_get_Inventory_m784641069 (GetUserInventoryResult_t1496009288 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CInventoryU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserInventoryResult::set_Inventory(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void GetUserInventoryResult_set_Inventory_m1822294012 (GetUserInventoryResult_t1496009288 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CInventoryU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserInventoryResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t190145395 * GetUserInventoryResult_get_VirtualCurrency_m57097835 (GetUserInventoryResult_t1496009288 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserInventoryResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetUserInventoryResult_set_VirtualCurrency_m1054214002 (GetUserInventoryResult_t1496009288 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime> PlayFab.ClientModels.GetUserInventoryResult::get_VirtualCurrencyRechargeTimes()
extern "C"  Dictionary_2_t3222006224 * GetUserInventoryResult_get_VirtualCurrencyRechargeTimes_m33878707 (GetUserInventoryResult_t1496009288 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3222006224 * L_0 = __this->get_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserInventoryResult::set_VirtualCurrencyRechargeTimes(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>)
extern "C"  void GetUserInventoryResult_set_VirtualCurrencyRechargeTimes_m1958622040 (GetUserInventoryResult_t1496009288 * __this, Dictionary_2_t3222006224 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t3222006224 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetUserStatisticsRequest::.ctor()
extern "C"  void GetUserStatisticsRequest__ctor_m149596866 (GetUserStatisticsRequest_t813294043 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.GetUserStatisticsResult::.ctor()
extern "C"  void GetUserStatisticsResult__ctor_m3940267864 (GetUserStatisticsResult_t2139593073 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserStatisticsResult::get_UserStatistics()
extern "C"  Dictionary_2_t190145395 * GetUserStatisticsResult_get_UserStatistics_m4167672328 (GetUserStatisticsResult_t2139593073 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CUserStatisticsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GetUserStatisticsResult::set_UserStatistics(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetUserStatisticsResult_set_UserStatistics_m749750093 (GetUserStatisticsResult_t2139593073 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CUserStatisticsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GooglePlayFabIdPair::.ctor()
extern "C"  void GooglePlayFabIdPair__ctor_m3814750634 (GooglePlayFabIdPair_t663154143 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GooglePlayFabIdPair::get_GoogleId()
extern "C"  String_t* GooglePlayFabIdPair_get_GoogleId_m3102549818 (GooglePlayFabIdPair_t663154143 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CGoogleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GooglePlayFabIdPair::set_GoogleId(System.String)
extern "C"  void GooglePlayFabIdPair_set_GoogleId_m3425473367 (GooglePlayFabIdPair_t663154143 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CGoogleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GooglePlayFabIdPair::get_PlayFabId()
extern "C"  String_t* GooglePlayFabIdPair_get_PlayFabId_m374440906 (GooglePlayFabIdPair_t663154143 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GooglePlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void GooglePlayFabIdPair_set_PlayFabId_m3881742345 (GooglePlayFabIdPair_t663154143 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GrantCharacterToUserRequest::.ctor()
extern "C"  void GrantCharacterToUserRequest__ctor_m4000249309 (GrantCharacterToUserRequest_t1227105868 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GrantCharacterToUserRequest::get_CatalogVersion()
extern "C"  String_t* GrantCharacterToUserRequest_get_CatalogVersion_m2595700146 (GrantCharacterToUserRequest_t1227105868 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GrantCharacterToUserRequest::set_CatalogVersion(System.String)
extern "C"  void GrantCharacterToUserRequest_set_CatalogVersion_m1373803935 (GrantCharacterToUserRequest_t1227105868 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GrantCharacterToUserRequest::get_ItemId()
extern "C"  String_t* GrantCharacterToUserRequest_get_ItemId_m2590743809 (GrantCharacterToUserRequest_t1227105868 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GrantCharacterToUserRequest::set_ItemId(System.String)
extern "C"  void GrantCharacterToUserRequest_set_ItemId_m191974832 (GrantCharacterToUserRequest_t1227105868 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GrantCharacterToUserRequest::get_CharacterName()
extern "C"  String_t* GrantCharacterToUserRequest_get_CharacterName_m2582004003 (GrantCharacterToUserRequest_t1227105868 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterNameU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GrantCharacterToUserRequest::set_CharacterName(System.String)
extern "C"  void GrantCharacterToUserRequest_set_CharacterName_m2504560080 (GrantCharacterToUserRequest_t1227105868 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterNameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.GrantCharacterToUserResult::.ctor()
extern "C"  void GrantCharacterToUserResult__ctor_m1986272477 (GrantCharacterToUserResult_t490373856 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.GrantCharacterToUserResult::get_CharacterId()
extern "C"  String_t* GrantCharacterToUserResult_get_CharacterId_m4221214425 (GrantCharacterToUserResult_t490373856 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GrantCharacterToUserResult::set_CharacterId(System.String)
extern "C"  void GrantCharacterToUserResult_set_CharacterId_m3266953408 (GrantCharacterToUserResult_t490373856 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.GrantCharacterToUserResult::get_CharacterType()
extern "C"  String_t* GrantCharacterToUserResult_get_CharacterType_m2474453496 (GrantCharacterToUserResult_t490373856 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterTypeU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GrantCharacterToUserResult::set_CharacterType(System.String)
extern "C"  void GrantCharacterToUserResult_set_CharacterType_m3360054849 (GrantCharacterToUserResult_t490373856 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterTypeU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.GrantCharacterToUserResult::get_Result()
extern "C"  bool GrantCharacterToUserResult_get_Result_m2990057913 (GrantCharacterToUserResult_t490373856 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CResultU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.GrantCharacterToUserResult::set_Result(System.Boolean)
extern "C"  void GrantCharacterToUserResult_set_Result_m1499075608 (GrantCharacterToUserResult_t490373856 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CResultU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::.ctor()
extern "C"  void ItemInstance__ctor_m745986885 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemInstance::get_ItemId()
extern "C"  String_t* ItemInstance_get_ItemId_m4027756851 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_ItemId(System.String)
extern "C"  void ItemInstance_set_ItemId_m1818339608 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemInstance::get_ItemInstanceId()
extern "C"  String_t* ItemInstance_get_ItemInstanceId_m819241064 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemInstanceIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_ItemInstanceId(System.String)
extern "C"  void ItemInstance_set_ItemInstanceId_m2357747459 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemInstanceIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemInstance::get_ItemClass()
extern "C"  String_t* ItemInstance_get_ItemClass_m1656935234 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemClassU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_ItemClass(System.String)
extern "C"  void ItemInstance_set_ItemClass_m627667447 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemClassU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.ItemInstance::get_PurchaseDate()
extern "C"  Nullable_1_t3225071844  ItemInstance_get_PurchaseDate_m3779668649 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = __this->get_U3CPurchaseDateU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_PurchaseDate(System.Nullable`1<System.DateTime>)
extern "C"  void ItemInstance_set_PurchaseDate_m4169104508 (ItemInstance_t525144312 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = ___value0;
		__this->set_U3CPurchaseDateU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.ItemInstance::get_Expiration()
extern "C"  Nullable_1_t3225071844  ItemInstance_get_Expiration_m2634469193 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = __this->get_U3CExpirationU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_Expiration(System.Nullable`1<System.DateTime>)
extern "C"  void ItemInstance_set_Expiration_m1065673116 (ItemInstance_t525144312 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = ___value0;
		__this->set_U3CExpirationU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.ItemInstance::get_RemainingUses()
extern "C"  Nullable_1_t1438485399  ItemInstance_get_RemainingUses_m1309735051 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CRemainingUsesU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_RemainingUses(System.Nullable`1<System.Int32>)
extern "C"  void ItemInstance_set_RemainingUses_m1693934438 (ItemInstance_t525144312 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CRemainingUsesU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.ItemInstance::get_UsesIncrementedBy()
extern "C"  Nullable_1_t1438485399  ItemInstance_get_UsesIncrementedBy_m693228738 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CUsesIncrementedByU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_UsesIncrementedBy(System.Nullable`1<System.Int32>)
extern "C"  void ItemInstance_set_UsesIncrementedBy_m3067840207 (ItemInstance_t525144312 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CUsesIncrementedByU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemInstance::get_Annotation()
extern "C"  String_t* ItemInstance_get_Annotation_m3051322004 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAnnotationU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_Annotation(System.String)
extern "C"  void ItemInstance_set_Annotation_m1354533079 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAnnotationU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemInstance::get_CatalogVersion()
extern "C"  String_t* ItemInstance_get_CatalogVersion_m2808610788 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_CatalogVersion(System.String)
extern "C"  void ItemInstance_set_CatalogVersion_m1377110279 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemInstance::get_BundleParent()
extern "C"  String_t* ItemInstance_get_BundleParent_m775233265 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CBundleParentU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_BundleParent(System.String)
extern "C"  void ItemInstance_set_BundleParent_m950947162 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBundleParentU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemInstance::get_DisplayName()
extern "C"  String_t* ItemInstance_get_DisplayName_m3211107978 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_DisplayName(System.String)
extern "C"  void ItemInstance_set_DisplayName_m1895979503 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemInstance::get_UnitCurrency()
extern "C"  String_t* ItemInstance_get_UnitCurrency_m4058609306 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUnitCurrencyU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_UnitCurrency(System.String)
extern "C"  void ItemInstance_set_UnitCurrency_m2489938001 (ItemInstance_t525144312 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUnitCurrencyU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.ItemInstance::get_UnitPrice()
extern "C"  uint32_t ItemInstance_get_UnitPrice_m215629626 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CUnitPriceU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_UnitPrice(System.UInt32)
extern "C"  void ItemInstance_set_UnitPrice_m603798511 (ItemInstance_t525144312 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CUnitPriceU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.ItemInstance::get_BundleContents()
extern "C"  List_1_t1765447871 * ItemInstance_get_BundleContents_m3858466899 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CBundleContentsU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_BundleContents(System.Collections.Generic.List`1<System.String>)
extern "C"  void ItemInstance_set_BundleContents_m271379980 (ItemInstance_t525144312 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CBundleContentsU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.ItemInstance::get_CustomData()
extern "C"  Dictionary_2_t2606186806 * ItemInstance_get_CustomData_m1704108405 (ItemInstance_t525144312 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = __this->get_U3CCustomDataU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemInstance::set_CustomData(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void ItemInstance_set_CustomData_m1351768136 (ItemInstance_t525144312 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = ___value0;
		__this->set_U3CCustomDataU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::.ctor()
extern "C"  void ItemPurchaseRequest__ctor_m3416904286 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemPurchaseRequest::get_ItemId()
extern "C"  String_t* ItemPurchaseRequest_get_ItemId_m1987968032 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::set_ItemId(System.String)
extern "C"  void ItemPurchaseRequest_set_ItemId_m3708316657 (ItemPurchaseRequest_t731462315 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.ItemPurchaseRequest::get_Quantity()
extern "C"  uint32_t ItemPurchaseRequest_get_Quantity_m916788117 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CQuantityU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::set_Quantity(System.UInt32)
extern "C"  void ItemPurchaseRequest_set_Quantity_m2237283276 (ItemPurchaseRequest_t731462315 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CQuantityU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ItemPurchaseRequest::get_Annotation()
extern "C"  String_t* ItemPurchaseRequest_get_Annotation_m782337793 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAnnotationU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::set_Annotation(System.String)
extern "C"  void ItemPurchaseRequest_set_Annotation_m3089381168 (ItemPurchaseRequest_t731462315 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAnnotationU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.ItemPurchaseRequest::get_UpgradeFromItems()
extern "C"  List_1_t1765447871 * ItemPurchaseRequest_get_UpgradeFromItems_m3305117594 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CUpgradeFromItemsU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::set_UpgradeFromItems(System.Collections.Generic.List`1<System.String>)
extern "C"  void ItemPurchaseRequest_set_UpgradeFromItems_m1540072273 (ItemPurchaseRequest_t731462315 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CUpgradeFromItemsU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.KongregatePlayFabIdPair::.ctor()
extern "C"  void KongregatePlayFabIdPair__ctor_m2967690444 (KongregatePlayFabIdPair_t2985181565 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.KongregatePlayFabIdPair::get_KongregateId()
extern "C"  String_t* KongregatePlayFabIdPair_get_KongregateId_m522639738 (KongregatePlayFabIdPair_t2985181565 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CKongregateIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.KongregatePlayFabIdPair::set_KongregateId(System.String)
extern "C"  void KongregatePlayFabIdPair_set_KongregateId_m3937939479 (KongregatePlayFabIdPair_t2985181565 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CKongregateIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.KongregatePlayFabIdPair::get_PlayFabId()
extern "C"  String_t* KongregatePlayFabIdPair_get_PlayFabId_m3729014380 (KongregatePlayFabIdPair_t2985181565 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.KongregatePlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void KongregatePlayFabIdPair_set_PlayFabId_m1901753895 (KongregatePlayFabIdPair_t2985181565 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDRequest::.ctor()
extern "C"  void LinkAndroidDeviceIDRequest__ctor_m258155044 (LinkAndroidDeviceIDRequest_t2075009465 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkAndroidDeviceIDRequest::get_AndroidDeviceId()
extern "C"  String_t* LinkAndroidDeviceIDRequest_get_AndroidDeviceId_m1336801884 (LinkAndroidDeviceIDRequest_t2075009465 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAndroidDeviceIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDRequest::set_AndroidDeviceId(System.String)
extern "C"  void LinkAndroidDeviceIDRequest_set_AndroidDeviceId_m2976226973 (LinkAndroidDeviceIDRequest_t2075009465 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAndroidDeviceIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkAndroidDeviceIDRequest::get_OS()
extern "C"  String_t* LinkAndroidDeviceIDRequest_get_OS_m436307498 (LinkAndroidDeviceIDRequest_t2075009465 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COSU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDRequest::set_OS(System.String)
extern "C"  void LinkAndroidDeviceIDRequest_set_OS_m499954497 (LinkAndroidDeviceIDRequest_t2075009465 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COSU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkAndroidDeviceIDRequest::get_AndroidDevice()
extern "C"  String_t* LinkAndroidDeviceIDRequest_get_AndroidDevice_m2110884833 (LinkAndroidDeviceIDRequest_t2075009465 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAndroidDeviceU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDRequest::set_AndroidDevice(System.String)
extern "C"  void LinkAndroidDeviceIDRequest_set_AndroidDevice_m3803455224 (LinkAndroidDeviceIDRequest_t2075009465 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAndroidDeviceU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDResult::.ctor()
extern "C"  void LinkAndroidDeviceIDResult__ctor_m1034275766 (LinkAndroidDeviceIDResult_t2734482899 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkCustomIDRequest::.ctor()
extern "C"  void LinkCustomIDRequest__ctor_m336790416 (LinkCustomIDRequest_t3838085433 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkCustomIDRequest::get_CustomId()
extern "C"  String_t* LinkCustomIDRequest_get_CustomId_m2391187404 (LinkCustomIDRequest_t3838085433 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkCustomIDRequest::set_CustomId(System.String)
extern "C"  void LinkCustomIDRequest_set_CustomId_m1580406789 (LinkCustomIDRequest_t3838085433 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkCustomIDResult::.ctor()
extern "C"  void LinkCustomIDResult__ctor_m66981066 (LinkCustomIDResult_t297504339 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkFacebookAccountRequest::.ctor()
extern "C"  void LinkFacebookAccountRequest__ctor_m3359017771 (LinkFacebookAccountRequest_t3956942034 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkFacebookAccountRequest::get_AccessToken()
extern "C"  String_t* LinkFacebookAccountRequest_get_AccessToken_m1859143736 (LinkFacebookAccountRequest_t3956942034 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAccessTokenU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkFacebookAccountRequest::set_AccessToken(System.String)
extern "C"  void LinkFacebookAccountRequest_set_AccessToken_m688687553 (LinkFacebookAccountRequest_t3956942034 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAccessTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LinkFacebookAccountRequest::get_ForceLink()
extern "C"  Nullable_1_t3097043249  LinkFacebookAccountRequest_get_ForceLink_m511904622 (LinkFacebookAccountRequest_t3956942034 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CForceLinkU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkFacebookAccountRequest::set_ForceLink(System.Nullable`1<System.Boolean>)
extern "C"  void LinkFacebookAccountRequest_set_ForceLink_m4019466775 (LinkFacebookAccountRequest_t3956942034 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CForceLinkU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkFacebookAccountResult::.ctor()
extern "C"  void LinkFacebookAccountResult__ctor_m164472271 (LinkFacebookAccountResult_t3626474394 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkGameCenterAccountRequest::.ctor()
extern "C"  void LinkGameCenterAccountRequest__ctor_m3872169450 (LinkGameCenterAccountRequest_t355908787 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkGameCenterAccountRequest::get_GameCenterId()
extern "C"  String_t* LinkGameCenterAccountRequest_get_GameCenterId_m891929154 (LinkGameCenterAccountRequest_t355908787 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CGameCenterIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkGameCenterAccountRequest::set_GameCenterId(System.String)
extern "C"  void LinkGameCenterAccountRequest_set_GameCenterId_m1409317801 (LinkGameCenterAccountRequest_t355908787 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CGameCenterIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkGameCenterAccountResult::.ctor()
extern "C"  void LinkGameCenterAccountResult__ctor_m1289404208 (LinkGameCenterAccountResult_t1155007385 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkGoogleAccountRequest::.ctor()
extern "C"  void LinkGoogleAccountRequest__ctor_m4000137816 (LinkGoogleAccountRequest_t117459333 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkGoogleAccountRequest::get_AccessToken()
extern "C"  String_t* LinkGoogleAccountRequest_get_AccessToken_m2982258661 (LinkGoogleAccountRequest_t117459333 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAccessTokenU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkGoogleAccountRequest::set_AccessToken(System.String)
extern "C"  void LinkGoogleAccountRequest_set_AccessToken_m3481145332 (LinkGoogleAccountRequest_t117459333 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAccessTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkGoogleAccountResult::.ctor()
extern "C"  void LinkGoogleAccountResult__ctor_m3648836866 (LinkGoogleAccountResult_t1008768135 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkIOSDeviceIDRequest::.ctor()
extern "C"  void LinkIOSDeviceIDRequest__ctor_m742481346 (LinkIOSDeviceIDRequest_t130515291 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkIOSDeviceIDRequest::get_DeviceId()
extern "C"  String_t* LinkIOSDeviceIDRequest_get_DeviceId_m535183513 (LinkIOSDeviceIDRequest_t130515291 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkIOSDeviceIDRequest::set_DeviceId(System.String)
extern "C"  void LinkIOSDeviceIDRequest_set_DeviceId_m447830258 (LinkIOSDeviceIDRequest_t130515291 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDeviceIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkIOSDeviceIDRequest::get_OS()
extern "C"  String_t* LinkIOSDeviceIDRequest_get_OS_m989193420 (LinkIOSDeviceIDRequest_t130515291 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COSU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkIOSDeviceIDRequest::set_OS(System.String)
extern "C"  void LinkIOSDeviceIDRequest_set_OS_m1695760991 (LinkIOSDeviceIDRequest_t130515291 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COSU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkIOSDeviceIDRequest::get_DeviceModel()
extern "C"  String_t* LinkIOSDeviceIDRequest_get_DeviceModel_m360704909 (LinkIOSDeviceIDRequest_t130515291 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceModelU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkIOSDeviceIDRequest::set_DeviceModel(System.String)
extern "C"  void LinkIOSDeviceIDRequest_set_DeviceModel_m527069708 (LinkIOSDeviceIDRequest_t130515291 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDeviceModelU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkIOSDeviceIDResult::.ctor()
extern "C"  void LinkIOSDeviceIDResult__ctor_m2019730520 (LinkIOSDeviceIDResult_t3225946609 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkKongregateAccountRequest::.ctor()
extern "C"  void LinkKongregateAccountRequest__ctor_m992198774 (LinkKongregateAccountRequest_t2234535079 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkKongregateAccountRequest::get_KongregateId()
extern "C"  String_t* LinkKongregateAccountRequest_get_KongregateId_m1405813034 (LinkKongregateAccountRequest_t2234535079 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CKongregateIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkKongregateAccountRequest::set_KongregateId(System.String)
extern "C"  void LinkKongregateAccountRequest_set_KongregateId_m718452161 (LinkKongregateAccountRequest_t2234535079 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CKongregateIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkKongregateAccountRequest::get_AuthTicket()
extern "C"  String_t* LinkKongregateAccountRequest_get_AuthTicket_m1523538152 (LinkKongregateAccountRequest_t2234535079 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAuthTicketU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkKongregateAccountRequest::set_AuthTicket(System.String)
extern "C"  void LinkKongregateAccountRequest_set_AuthTicket_m152451331 (LinkKongregateAccountRequest_t2234535079 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAuthTicketU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkKongregateAccountResult::.ctor()
extern "C"  void LinkKongregateAccountResult__ctor_m1057954596 (LinkKongregateAccountResult_t1077060901 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
