﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GrantCharacterToUserResult
struct GrantCharacterToUserResult_t490373856;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GrantCharacterToUserResult::.ctor()
extern "C"  void GrantCharacterToUserResult__ctor_m1986272477 (GrantCharacterToUserResult_t490373856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GrantCharacterToUserResult::get_CharacterId()
extern "C"  String_t* GrantCharacterToUserResult_get_CharacterId_m4221214425 (GrantCharacterToUserResult_t490373856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GrantCharacterToUserResult::set_CharacterId(System.String)
extern "C"  void GrantCharacterToUserResult_set_CharacterId_m3266953408 (GrantCharacterToUserResult_t490373856 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GrantCharacterToUserResult::get_CharacterType()
extern "C"  String_t* GrantCharacterToUserResult_get_CharacterType_m2474453496 (GrantCharacterToUserResult_t490373856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GrantCharacterToUserResult::set_CharacterType(System.String)
extern "C"  void GrantCharacterToUserResult_set_CharacterType_m3360054849 (GrantCharacterToUserResult_t490373856 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.GrantCharacterToUserResult::get_Result()
extern "C"  bool GrantCharacterToUserResult_get_Result_m2990057913 (GrantCharacterToUserResult_t490373856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GrantCharacterToUserResult::set_Result(System.Boolean)
extern "C"  void GrantCharacterToUserResult_set_Result_m1499075608 (GrantCharacterToUserResult_t490373856 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
