﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpListenerBasicIdentity
struct HttpListenerBasicIdentity_t2250909274;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.Net.HttpListenerBasicIdentity::.ctor(System.String,System.String)
extern "C"  void HttpListenerBasicIdentity__ctor_m67775106 (HttpListenerBasicIdentity_t2250909274 * __this, String_t* ___username0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerBasicIdentity::get_Password()
extern "C"  String_t* HttpListenerBasicIdentity_get_Password_m1782921807 (HttpListenerBasicIdentity_t2250909274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
