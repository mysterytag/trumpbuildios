﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<System.Int32>
struct Stream_1_t1538553809;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Action
struct Action_t437523947;
// IStream`1<System.Int32>
struct IStream_1_t3400105778;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<System.Int32>::.ctor()
extern "C"  void Stream_1__ctor_m2512605719_gshared (Stream_1_t1538553809 * __this, const MethodInfo* method);
#define Stream_1__ctor_m2512605719(__this, method) ((  void (*) (Stream_1_t1538553809 *, const MethodInfo*))Stream_1__ctor_m2512605719_gshared)(__this, method)
// System.Void Stream`1<System.Int32>::Send(T)
extern "C"  void Stream_1_Send_m1041823273_gshared (Stream_1_t1538553809 * __this, int32_t ___value0, const MethodInfo* method);
#define Stream_1_Send_m1041823273(__this, ___value0, method) ((  void (*) (Stream_1_t1538553809 *, int32_t, const MethodInfo*))Stream_1_Send_m1041823273_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.Int32>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m907270271_gshared (Stream_1_t1538553809 * __this, int32_t ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m907270271(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t1538553809 *, int32_t, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m907270271_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.Int32>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m3789926011_gshared (Stream_1_t1538553809 * __this, Action_1_t2995867492 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m3789926011(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t1538553809 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))Stream_1_Listen_m3789926011_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.Int32>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1424028108_gshared (Stream_1_t1538553809 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m1424028108(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t1538553809 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m1424028108_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.Int32>::Dispose()
extern "C"  void Stream_1_Dispose_m740758676_gshared (Stream_1_t1538553809 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m740758676(__this, method) ((  void (*) (Stream_1_t1538553809 *, const MethodInfo*))Stream_1_Dispose_m740758676_gshared)(__this, method)
// System.Void Stream`1<System.Int32>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m1221132873_gshared (Stream_1_t1538553809 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m1221132873(__this, ___stream0, method) ((  void (*) (Stream_1_t1538553809 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m1221132873_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.Int32>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m3887090282_gshared (Stream_1_t1538553809 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m3887090282(__this, method) ((  void (*) (Stream_1_t1538553809 *, const MethodInfo*))Stream_1_ClearInputStream_m3887090282_gshared)(__this, method)
// System.Void Stream`1<System.Int32>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m3275078462_gshared (Stream_1_t1538553809 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m3275078462(__this, method) ((  void (*) (Stream_1_t1538553809 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3275078462_gshared)(__this, method)
// System.Void Stream`1<System.Int32>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m3449028560_gshared (Stream_1_t1538553809 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m3449028560(__this, ___p0, method) ((  void (*) (Stream_1_t1538553809 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3449028560_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.Int32>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m45029620_gshared (Stream_1_t1538553809 * __this, int32_t ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m45029620(__this, ___val0, method) ((  void (*) (Stream_1_t1538553809 *, int32_t, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m45029620_gshared)(__this, ___val0, method)
