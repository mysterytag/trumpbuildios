﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GrantCharacterToUserResponseCallback
struct GrantCharacterToUserResponseCallback_t3560436377;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GrantCharacterToUserRequest
struct GrantCharacterToUserRequest_t1227105868;
// PlayFab.ClientModels.GrantCharacterToUserResult
struct GrantCharacterToUserResult_t490373856;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GrantCharac1227105868.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GrantCharact490373856.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GrantCharacterToUserResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GrantCharacterToUserResponseCallback__ctor_m2166801160 (GrantCharacterToUserResponseCallback_t3560436377 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GrantCharacterToUserResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GrantCharacterToUserRequest,PlayFab.ClientModels.GrantCharacterToUserResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GrantCharacterToUserResponseCallback_Invoke_m1359773173 (GrantCharacterToUserResponseCallback_t3560436377 * __this, String_t* ___urlPath0, int32_t ___callId1, GrantCharacterToUserRequest_t1227105868 * ___request2, GrantCharacterToUserResult_t490373856 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GrantCharacterToUserResponseCallback_t3560436377(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GrantCharacterToUserRequest_t1227105868 * ___request2, GrantCharacterToUserResult_t490373856 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GrantCharacterToUserResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GrantCharacterToUserRequest,PlayFab.ClientModels.GrantCharacterToUserResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GrantCharacterToUserResponseCallback_BeginInvoke_m103936226 (GrantCharacterToUserResponseCallback_t3560436377 * __this, String_t* ___urlPath0, int32_t ___callId1, GrantCharacterToUserRequest_t1227105868 * ___request2, GrantCharacterToUserResult_t490373856 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GrantCharacterToUserResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GrantCharacterToUserResponseCallback_EndInvoke_m3408197912 (GrantCharacterToUserResponseCallback_t3560436377 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
