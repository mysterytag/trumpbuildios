﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IPresenter[]
struct IPresenterU5BU5D_t656077580;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// UniRx.IPresenter
struct IPresenter_t2177085329;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.PresenterBase`1<UniRx.Unit>
struct  PresenterBase_1_t3949187127  : public MonoBehaviour_t3012272455
{
public:
	// System.Int32 UniRx.PresenterBase`1::childrenCount
	int32_t ___childrenCount_3;
	// System.Int32 UniRx.PresenterBase`1::currentCalledCount
	int32_t ___currentCalledCount_4;
	// System.Boolean UniRx.PresenterBase`1::isAwaken
	bool ___isAwaken_5;
	// System.Boolean UniRx.PresenterBase`1::isInitialized
	bool ___isInitialized_6;
	// System.Boolean UniRx.PresenterBase`1::isStartedCapturePhase
	bool ___isStartedCapturePhase_7;
	// UniRx.Subject`1<UniRx.Unit> UniRx.PresenterBase`1::initializeSubject
	Subject_1_t201353362 * ___initializeSubject_8;
	// UniRx.IPresenter[] UniRx.PresenterBase`1::children
	IPresenterU5BU5D_t656077580* ___children_9;
	// UniRx.IPresenter UniRx.PresenterBase`1::parent
	Il2CppObject * ___parent_10;
	// T UniRx.PresenterBase`1::argument
	Unit_t2558286038  ___argument_11;

public:
	inline static int32_t get_offset_of_childrenCount_3() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___childrenCount_3)); }
	inline int32_t get_childrenCount_3() const { return ___childrenCount_3; }
	inline int32_t* get_address_of_childrenCount_3() { return &___childrenCount_3; }
	inline void set_childrenCount_3(int32_t value)
	{
		___childrenCount_3 = value;
	}

	inline static int32_t get_offset_of_currentCalledCount_4() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___currentCalledCount_4)); }
	inline int32_t get_currentCalledCount_4() const { return ___currentCalledCount_4; }
	inline int32_t* get_address_of_currentCalledCount_4() { return &___currentCalledCount_4; }
	inline void set_currentCalledCount_4(int32_t value)
	{
		___currentCalledCount_4 = value;
	}

	inline static int32_t get_offset_of_isAwaken_5() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___isAwaken_5)); }
	inline bool get_isAwaken_5() const { return ___isAwaken_5; }
	inline bool* get_address_of_isAwaken_5() { return &___isAwaken_5; }
	inline void set_isAwaken_5(bool value)
	{
		___isAwaken_5 = value;
	}

	inline static int32_t get_offset_of_isInitialized_6() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___isInitialized_6)); }
	inline bool get_isInitialized_6() const { return ___isInitialized_6; }
	inline bool* get_address_of_isInitialized_6() { return &___isInitialized_6; }
	inline void set_isInitialized_6(bool value)
	{
		___isInitialized_6 = value;
	}

	inline static int32_t get_offset_of_isStartedCapturePhase_7() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___isStartedCapturePhase_7)); }
	inline bool get_isStartedCapturePhase_7() const { return ___isStartedCapturePhase_7; }
	inline bool* get_address_of_isStartedCapturePhase_7() { return &___isStartedCapturePhase_7; }
	inline void set_isStartedCapturePhase_7(bool value)
	{
		___isStartedCapturePhase_7 = value;
	}

	inline static int32_t get_offset_of_initializeSubject_8() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___initializeSubject_8)); }
	inline Subject_1_t201353362 * get_initializeSubject_8() const { return ___initializeSubject_8; }
	inline Subject_1_t201353362 ** get_address_of_initializeSubject_8() { return &___initializeSubject_8; }
	inline void set_initializeSubject_8(Subject_1_t201353362 * value)
	{
		___initializeSubject_8 = value;
		Il2CppCodeGenWriteBarrier(&___initializeSubject_8, value);
	}

	inline static int32_t get_offset_of_children_9() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___children_9)); }
	inline IPresenterU5BU5D_t656077580* get_children_9() const { return ___children_9; }
	inline IPresenterU5BU5D_t656077580** get_address_of_children_9() { return &___children_9; }
	inline void set_children_9(IPresenterU5BU5D_t656077580* value)
	{
		___children_9 = value;
		Il2CppCodeGenWriteBarrier(&___children_9, value);
	}

	inline static int32_t get_offset_of_parent_10() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___parent_10)); }
	inline Il2CppObject * get_parent_10() const { return ___parent_10; }
	inline Il2CppObject ** get_address_of_parent_10() { return &___parent_10; }
	inline void set_parent_10(Il2CppObject * value)
	{
		___parent_10 = value;
		Il2CppCodeGenWriteBarrier(&___parent_10, value);
	}

	inline static int32_t get_offset_of_argument_11() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127, ___argument_11)); }
	inline Unit_t2558286038  get_argument_11() const { return ___argument_11; }
	inline Unit_t2558286038 * get_address_of_argument_11() { return &___argument_11; }
	inline void set_argument_11(Unit_t2558286038  value)
	{
		___argument_11 = value;
	}
};

struct PresenterBase_1_t3949187127_StaticFields
{
public:
	// UniRx.IPresenter[] UniRx.PresenterBase`1::EmptyChildren
	IPresenterU5BU5D_t656077580* ___EmptyChildren_2;

public:
	inline static int32_t get_offset_of_EmptyChildren_2() { return static_cast<int32_t>(offsetof(PresenterBase_1_t3949187127_StaticFields, ___EmptyChildren_2)); }
	inline IPresenterU5BU5D_t656077580* get_EmptyChildren_2() const { return ___EmptyChildren_2; }
	inline IPresenterU5BU5D_t656077580** get_address_of_EmptyChildren_2() { return &___EmptyChildren_2; }
	inline void set_EmptyChildren_2(IPresenterU5BU5D_t656077580* value)
	{
		___EmptyChildren_2 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyChildren_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
