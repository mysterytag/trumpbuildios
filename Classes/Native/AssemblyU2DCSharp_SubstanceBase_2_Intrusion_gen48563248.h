﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ILiving
struct ILiving_t2639664210;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubstanceBase`2/Intrusion<System.Object,System.Func`2<System.Object,System.Object>>
struct  Intrusion_t48563248  : public Il2CppObject
{
public:
	// ILiving SubstanceBase`2/Intrusion::intruder
	Il2CppObject * ___intruder_0;
	// InfT SubstanceBase`2/Intrusion::influence
	Func_2_t2135783352 * ___influence_1;
	// System.IDisposable SubstanceBase`2/Intrusion::awakeConnection
	Il2CppObject * ___awakeConnection_2;
	// System.IDisposable SubstanceBase`2/Intrusion::updateConnection
	Il2CppObject * ___updateConnection_3;

public:
	inline static int32_t get_offset_of_intruder_0() { return static_cast<int32_t>(offsetof(Intrusion_t48563248, ___intruder_0)); }
	inline Il2CppObject * get_intruder_0() const { return ___intruder_0; }
	inline Il2CppObject ** get_address_of_intruder_0() { return &___intruder_0; }
	inline void set_intruder_0(Il2CppObject * value)
	{
		___intruder_0 = value;
		Il2CppCodeGenWriteBarrier(&___intruder_0, value);
	}

	inline static int32_t get_offset_of_influence_1() { return static_cast<int32_t>(offsetof(Intrusion_t48563248, ___influence_1)); }
	inline Func_2_t2135783352 * get_influence_1() const { return ___influence_1; }
	inline Func_2_t2135783352 ** get_address_of_influence_1() { return &___influence_1; }
	inline void set_influence_1(Func_2_t2135783352 * value)
	{
		___influence_1 = value;
		Il2CppCodeGenWriteBarrier(&___influence_1, value);
	}

	inline static int32_t get_offset_of_awakeConnection_2() { return static_cast<int32_t>(offsetof(Intrusion_t48563248, ___awakeConnection_2)); }
	inline Il2CppObject * get_awakeConnection_2() const { return ___awakeConnection_2; }
	inline Il2CppObject ** get_address_of_awakeConnection_2() { return &___awakeConnection_2; }
	inline void set_awakeConnection_2(Il2CppObject * value)
	{
		___awakeConnection_2 = value;
		Il2CppCodeGenWriteBarrier(&___awakeConnection_2, value);
	}

	inline static int32_t get_offset_of_updateConnection_3() { return static_cast<int32_t>(offsetof(Intrusion_t48563248, ___updateConnection_3)); }
	inline Il2CppObject * get_updateConnection_3() const { return ___updateConnection_3; }
	inline Il2CppObject ** get_address_of_updateConnection_3() { return &___updateConnection_3; }
	inline void set_updateConnection_3(Il2CppObject * value)
	{
		___updateConnection_3 = value;
		Il2CppCodeGenWriteBarrier(&___updateConnection_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
