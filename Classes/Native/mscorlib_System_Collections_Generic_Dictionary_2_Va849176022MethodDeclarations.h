﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1451594948MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2639163460(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t849176022 *, Dictionary_2_t3222006224 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3418272622(__this, ___item0, method) ((  void (*) (ValueCollection_t849176022 *, VirtualCurrencyRechargeTime_t1584308320 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3062073143(__this, method) ((  void (*) (ValueCollection_t849176022 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2588444732(__this, ___item0, method) ((  bool (*) (ValueCollection_t849176022 *, VirtualCurrencyRechargeTime_t1584308320 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2430500833(__this, ___item0, method) ((  bool (*) (ValueCollection_t849176022 *, VirtualCurrencyRechargeTime_t1584308320 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1977138807(__this, method) ((  Il2CppObject* (*) (ValueCollection_t849176022 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2455752571(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t849176022 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1738205002(__this, method) ((  Il2CppObject * (*) (ValueCollection_t849176022 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m863620591(__this, method) ((  bool (*) (ValueCollection_t849176022 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1665811023(__this, method) ((  bool (*) (ValueCollection_t849176022 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2232981121(__this, method) ((  Il2CppObject * (*) (ValueCollection_t849176022 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2258649995(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t849176022 *, VirtualCurrencyRechargeTimeU5BU5D_t3890213409*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2772125108(__this, method) ((  Enumerator_t2989034167  (*) (ValueCollection_t849176022 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>::get_Count()
#define ValueCollection_get_Count_m2382467977(__this, method) ((  int32_t (*) (ValueCollection_t849176022 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
