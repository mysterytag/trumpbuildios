﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t1434350349;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3012272455;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t3422778292;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3644373756;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t639075880;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t3057020574;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t27321461;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t27321462;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t27321463;
// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t2954595302;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t817568325;
// System.Object
struct Il2CppObject;
// Utils2/<FlagsToList>c__AnonStoreyF2`1<System.Object>
struct U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067;
// Utils2/<FormatEach>c__AnonStoreyEF`1<System.Object>
struct U3CFormatEachU3Ec__AnonStoreyEF_1_t2861012304;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// Utils2/<Lift>c__Iterator2A`1<System.Object>
struct U3CLiftU3Ec__Iterator2A_1_t2546142915;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>
struct U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525;
// Utils2/Wrapper`1<System.Object>
struct Wrapper_1_t1353841757;
// WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145<System.Object>
struct U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624;
// WaitForStreamEvent`1<System.Object>
struct WaitForStreamEvent_1_t1294890264;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2<System.Object,System.Object>
struct U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.Binder/<ToLookupBase>c__AnonStorey16E`1<System.Object>
struct U3CToLookupBaseU3Ec__AnonStorey16E_1_t2894210511;
// Zenject.BinderGeneric`1<System.Object>
struct BinderGeneric_1_t520705716;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;
// Zenject.BindScope/CustomScopeBinder`1<System.Object>
struct CustomScopeBinder_1_t4252514087;
// Zenject.BindScope
struct BindScope_t2945157996;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// Zenject.DiContainer/<BindGameObjectFactory>c__AnonStorey18D`1<System.Object>
struct U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// Zenject.DiContainer/<Unbind>c__AnonStorey18C`1<System.Object>
struct U3CUnbindU3Ec__AnonStorey18C_1_t2822308123;
// Zenject.Factory`1<System.Object>
struct Factory_1_t250748329;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_10_t1264205109;
// Zenject.TypeValuePair
struct TypeValuePair_t620932390;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t1417891359;
// Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_11_t399731484;
// Zenject.Factory`2<System.Object,System.Object>
struct Factory_2_t2762000298;
// Zenject.Factory`3<System.Object,System.Object,System.Object>
struct Factory_3_t2717473363;
// Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>
struct Factory_4_t549530144;
// Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_5_t2495851293;
// Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_6_t3697694390;
// Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_7_t2859822983;
// Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_8_t2931077612;
// Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_9_t1966270225;
// Zenject.FactoryMethod`1<System.Object>
struct FactoryMethod_1_t672889922;
// Zenject.FactoryMethod`2<System.Object,System.Object>
struct FactoryMethod_2_t1984661133;
// Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>
struct FactoryMethod_3_t1233407108;
// Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>
struct FactoryMethod_4_t1102797835;
// Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryMethod_5_t2447447334;
// Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>
struct U3CValidateU3Ec__Iterator45_t1669021519;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;
// Zenject.FactoryMethodUntyped`1<System.Object>
struct FactoryMethodUntyped_1_t1598907365;
// System.Func`3<Zenject.DiContainer,System.Object[],System.Object>
struct Func_3_t2319412757;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// Zenject.FactoryNested`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_10_t2299834384;
// Zenject.IFactory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_9_t1437178096;
// Zenject.FactoryNested`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_11_t14697077;
// Zenject.IFactory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_10_t1298394640;
// Zenject.FactoryNested`12<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_12_t4045899478;
// Zenject.IFactory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_11_t2716224629;
// Zenject.FactoryNested`2<System.Object,System.Object>
struct FactoryNested_2_t2234585871;
// Zenject.IFactory`1<System.Object>
struct IFactory_1_t2124284520;
// Zenject.FactoryNested`3<System.Object,System.Object,System.Object>
struct FactoryNested_3_t3116483514;
// Zenject.IFactory`2<System.Object,System.Object>
struct IFactory_2_t972313871;
// Zenject.FactoryNested`4<System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_4_t2122089277;
// Zenject.IFactory`3<System.Object,System.Object,System.Object>
struct IFactory_3_t1619999162;
// Zenject.FactoryNested`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_5_t2227126508;
// Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>
struct IFactory_4_t1894544701;
// Zenject.FactoryNested`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_6_t1301445835;
// Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_5_t363284204;
// Zenject.FactoryNested`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_7_t3889515902;
// Zenject.IFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_6_t4091256523;
// Zenject.FactoryNested`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_8_t2707677753;
// Zenject.IFactory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_7_t2749981566;
// Zenject.FactoryNested`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_9_t2228482800;
// Zenject.IFactory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_8_t3095311417;
// Zenject.FactoryUntyped`1<System.Object>
struct FactoryUntyped_1_t2760421534;
// Zenject.FactoryUntyped`2<System.Object,System.Object>
struct FactoryUntyped_2_t884050241;
// Zenject.GameObjectFactory`1<System.Object>
struct GameObjectFactory_1_t2417718272;
// Zenject.GameObjectFactory`2<System.Object,System.Object>
struct GameObjectFactory_2_t546746903;
// Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>
struct GameObjectFactory_3_t639885202;
// Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>
struct GameObjectFactory_4_t486081797;
// Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct GameObjectFactory_5_t2159204100;
// Zenject.IFactoryBinder`1/<ToInstance>c__AnonStorey173<System.Object>
struct U3CToInstanceU3Ec__AnonStorey173_t3921245137;
// Zenject.IFactoryBinder`1/<ToMethod>c__AnonStorey174<System.Object>
struct U3CToMethodU3Ec__AnonStorey174_t1867793022;
// Zenject.IFactoryBinder`1/<ToPrefab>c__AnonStorey175<System.Object>
struct U3CToPrefabU3Ec__AnonStorey175_t1205708684;
// Zenject.IFactoryBinder`1<System.Object>
struct IFactoryBinder_1_t1064970850;
// System.Func`2<Zenject.DiContainer,System.Object>
struct Func_2_t1206216135;
// Zenject.BinderGeneric`1<Zenject.IFactory`1<System.Object>>
struct BinderGeneric_1_t1807883816;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Zenject.IFactoryBinder`2/<ToMethod>c__AnonStorey176<System.Object,System.Object>
struct U3CToMethodU3Ec__AnonStorey176_t2008305412;
// Zenject.IFactoryBinder`2/<ToPrefab>c__AnonStorey177<System.Object,System.Object>
struct U3CToPrefabU3Ec__AnonStorey177_t3739000318;
// Zenject.IFactoryBinder`2<System.Object,System.Object>
struct IFactoryBinder_2_t3921329133;
// System.Func`3<Zenject.DiContainer,System.Object,System.Object>
struct Func_3_t1309472546;
// Zenject.BinderGeneric`1<Zenject.IFactory`2<System.Object,System.Object>>
struct BinderGeneric_1_t655913167;
// Zenject.IFactoryBinder`3/<ToMethod>c__AnonStorey178<System.Object,System.Object,System.Object>
struct U3CToMethodU3Ec__AnonStorey178_t3714372930;
// Zenject.IFactoryBinder`3/<ToPrefab>c__AnonStorey179<System.Object,System.Object,System.Object>
struct U3CToPrefabU3Ec__AnonStorey179_t385784096;
// Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>
struct IFactoryBinder_3_t153990564;
// System.Func`4<Zenject.DiContainer,System.Object,System.Object,System.Object>
struct Func_4_t2229882165;
// Zenject.BinderGeneric`1<Zenject.IFactory`3<System.Object,System.Object,System.Object>>
struct BinderGeneric_1_t1303598458;
// Zenject.IFactoryBinder`4/<ToMethod>c__AnonStorey17A<System.Object,System.Object,System.Object,System.Object>
struct U3CToMethodU3Ec__AnonStorey17A_t2106438637;
// Zenject.IFactoryBinder`4/<ToPrefab>c__AnonStorey17B<System.Object,System.Object,System.Object,System.Object>
struct U3CToPrefabU3Ec__AnonStorey17B_t2698428119;
// Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>
struct IFactoryBinder_4_t2507049579;
// System.Func`5<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object>
struct Func_5_t936310804;
// Zenject.BinderGeneric`1<Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>>
struct BinderGeneric_1_t1578143997;
// Zenject.IFactoryBinder`5/<ToMethod>c__AnonStorey17C<System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CToMethodU3Ec__AnonStorey17C_t3805965893;
// Zenject.IFactoryBinder`5/<ToPrefab>c__AnonStorey17D<System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CToPrefabU3Ec__AnonStorey17D_t2344802419;
// Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactoryBinder_5_t1497530694;
// ModestTree.Util.Func`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_6_t3355333698;
// Zenject.BinderGeneric`1<Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>
struct BinderGeneric_1_t46883500;
// Zenject.IFactoryUntypedBinder`1/<ToInstance>c__AnonStorey17E<System.Object>
struct U3CToInstanceU3Ec__AnonStorey17E_t3854809107;
// Zenject.IFactoryUntypedBinder`1/<ToMethod>c__AnonStorey17F<System.Object>
struct U3CToMethodU3Ec__AnonStorey17F_t1801356992;
// Zenject.IFactoryUntyped`1<System.Object>
struct IFactoryUntyped_1_t1534379775;
// Zenject.IFactoryUntypedBinder`1<System.Object>
struct IFactoryUntypedBinder_1_t486203833;
// Zenject.BinderGeneric`1<Zenject.IFactoryUntyped`1<System.Object>>
struct BinderGeneric_1_t1217979071;
// Zenject.KeyedFactory`2<System.Object,System.Object>
struct KeyedFactory_2_t3889101264;
// Zenject.KeyedFactory`3<System.Object,System.Object,System.Object>
struct KeyedFactory_3_t2002144469;
// Zenject.KeyedFactory`4<System.Object,System.Object,System.Object,System.Object>
struct KeyedFactory_4_t2209276758;
// Zenject.KeyedFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct KeyedFactory_5_t1813071951;
// Zenject.KeyedFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct KeyedFactory_6_t605647996;
// Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>
struct U3CValidateU3Ec__Iterator46_t3243744428;
// Zenject.KeyedFactoryBase`2<System.Object,System.Object>
struct KeyedFactoryBase_2_t2864689411;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.Dictionary`2<System.Object,System.Type>
struct Dictionary_2_t1471581369;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerable`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>
struct IEnumerable_1_t3103322274;
// System.Func`2<ModestTree.Util.Tuple`2<System.Object,System.Type>,System.Object>
struct Func_2_t361610966;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3840643258;
// System.Func`2<System.Object,System.String>
struct Func_2_t2267165834;
// System.Func`2<ModestTree.Util.Tuple`2<System.Object,System.Type>,System.Type>
struct Func_2_t2303734481;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3824425150;
// ModestTree.Util.Tuple`2<System.Object,System.Type>
struct Tuple_2_t231167918;
// Zenject.ListFactory`1<System.Object>
struct ListFactory_1_t3970372955;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// Zenject.MethodProvider`1/<GetInstance>c__AnonStorey195<System.Object>
struct U3CGetInstanceU3Ec__AnonStorey195_t1883175366;
// Zenject.MethodProvider`1<System.Object>
struct MethodProvider_1_t512585361;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween1434350349.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween1434350349MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color2894377818.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2376956030.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2376956030MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color2894377818MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3422778292.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3422778292MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatT587838465.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_TweenRu70416677.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_TweenRu70416677MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatT587838465MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen2461429948.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen2461429948MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3624835661.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3624835661MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1466895342.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1466895342MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen451121581.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen451121581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1614527294.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1614527294MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3751554271.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3751554271MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3751099368.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3751099368MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen619537785.h"
#include "mscorlib_System_Collections_Generic_List_1_gen639075880.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen619537785MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2756564762.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2756564762MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen639075880MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen1874076766.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen1874076766MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3037482479.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3057020574.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3037482479MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen879542160.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen879542160MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3057020574MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3139344949.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3139344949MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen7783366.h"
#include "mscorlib_System_Collections_Generic_List_1_gen27321461.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen7783366MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2144810343.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2144810343MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen27321461MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3139344950.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3139344950MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen7783367.h"
#include "mscorlib_System_Collections_Generic_List_1_gen27321462.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen7783367MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2144810344.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2144810344MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen27321462MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3139344951.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3139344951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen7783368.h"
#include "mscorlib_System_Collections_Generic_List_1_gen27321463.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen7783368MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2144810345.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2144810345MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen27321463MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2954595302.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2954595302MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen817568325.h"
#include "System_System_Collections_Generic_Stack_1_gen3407512455.h"
#include "System_System_Collections_Generic_Stack_1_gen3407512455MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Activator690001546MethodDeclarations.h"
#include "mscorlib_System_Activator690001546.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen817568325MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils2_U3CFlagsToListU3Ec__AnonS1336726067.h"
#include "AssemblyU2DCSharp_Utils2_U3CFlagsToListU3Ec__AnonS1336726067MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils2_U3CFormatEachU3Ec__AnonSt2861012304.h"
#include "AssemblyU2DCSharp_Utils2_U3CFormatEachU3Ec__AnonSt2861012304MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils2_U3CLiftU3Ec__Iterator2A_12546142915.h"
#include "AssemblyU2DCSharp_Utils2_U3CLiftU3Ec__Iterator2A_12546142915MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_Utils2_U3CTakeAllButLastU3Ec__It3815074525.h"
#include "AssemblyU2DCSharp_Utils2_U3CTakeAllButLastU3Ec__It3815074525MethodDeclarations.h"
#include "AssemblyU2DCSharp_Utils2_Wrapper_1_gen1353841757.h"
#include "AssemblyU2DCSharp_Utils2_Wrapper_1_gen1353841757MethodDeclarations.h"
#include "AssemblyU2DCSharp_WaitForStreamEvent_1_U3CWaitForSt533111624.h"
#include "AssemblyU2DCSharp_WaitForStreamEvent_1_U3CWaitForSt533111624MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharp_WaitForStreamEvent_1_gen1294890264.h"
#include "AssemblyU2DCSharp_WaitForStreamEvent_1_gen1294890264MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI1688313434MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI1688313434.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "AssemblyU2DCSharp_Zenject_Binder_U3CToGetterBaseU33259896156.h"
#include "AssemblyU2DCSharp_Zenject_Binder_U3CToGetterBaseU33259896156MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Binder_U3CToLookupBaseU32894210511.h"
#include "AssemblyU2DCSharp_Zenject_Binder_U3CToLookupBaseU32894210511MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen520705716.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen520705716MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"
#include "AssemblyU2DCSharp_Zenject_Binder3872662847MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BindingConditionSetter259147722.h"
#include "System_Core_System_Func_2_gen2621245597.h"
#include "AssemblyU2DCSharp_Zenject_Binder3872662847.h"
#include "AssemblyU2DCSharp_Zenject_BindScope_CustomScopeBin4252514087.h"
#include "AssemblyU2DCSharp_Zenject_BindScope_CustomScopeBin4252514087MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BindScope2945157996.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"
#include "AssemblyU2DCSharp_Zenject_BindScope2945157996MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer_U3CBindGameOb866492662.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer_U3CBindGameOb866492662MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer_U3CUnbindU3E2822308123.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer_U3CUnbindU3E2822308123MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22091358871.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22091358871MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2424453360.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2424453360MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_1_gen250748329.h"
#include "AssemblyU2DCSharp_Zenject_Factory_1_gen250748329MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_10_gen1264205109.h"
#include "AssemblyU2DCSharp_Zenject_Factory_10_gen1264205109MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1417891359MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1417891359.h"
#include "AssemblyU2DCSharp_Zenject_InstantiateUtil3253484897MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TypeValuePair620932390.h"
#include "AssemblyU2DCSharp_Zenject_InstantiateUtil3253484897.h"
#include "AssemblyU2DCSharp_Zenject_Factory_11_gen399731484.h"
#include "AssemblyU2DCSharp_Zenject_Factory_11_gen399731484MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_2_gen2762000298.h"
#include "AssemblyU2DCSharp_Zenject_Factory_2_gen2762000298MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_3_gen2717473363.h"
#include "AssemblyU2DCSharp_Zenject_Factory_3_gen2717473363MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_4_gen549530144.h"
#include "AssemblyU2DCSharp_Zenject_Factory_4_gen549530144MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_5_gen2495851293.h"
#include "AssemblyU2DCSharp_Zenject_Factory_5_gen2495851293MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_6_gen3697694390.h"
#include "AssemblyU2DCSharp_Zenject_Factory_6_gen3697694390MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_7_gen2859822983.h"
#include "AssemblyU2DCSharp_Zenject_Factory_7_gen2859822983MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_8_gen2931077612.h"
#include "AssemblyU2DCSharp_Zenject_Factory_8_gen2931077612MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Factory_9_gen1966270225.h"
#include "AssemblyU2DCSharp_Zenject_Factory_9_gen1966270225MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_1_gen672889922.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_1_gen672889922MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1206216135.h"
#include "System_Core_System_Func_2_gen1206216135MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_2_gen1984661133.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_2_gen1984661133MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1309472546.h"
#include "System_Core_System_Func_3_gen1309472546MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_3_gen1233407108.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_3_gen1233407108MethodDeclarations.h"
#include "System_Core_System_Func_4_gen2229882165.h"
#include "System_Core_System_Func_4_gen2229882165MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_4_gen1102797835.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_4_gen1102797835MethodDeclarations.h"
#include "System_Core_System_Func_5_gen936310804.h"
#include "System_Core_System_Func_5_gen936310804MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_5_gen2447447334.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethod_5_gen2447447334MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_6_gen3355333698.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_6_gen3355333698MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethodUntyped_1_U1669021519.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethodUntyped_1_U1669021519MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectResolveException1201052999.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethodUntyped_1_g1598907365.h"
#include "AssemblyU2DCSharp_Zenject_FactoryMethodUntyped_1_g1598907365MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2319412757.h"
#include "System_Core_System_Func_3_gen2319412757MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_10_gen2299834384.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_10_gen2299834384MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_11_gen14697077.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_11_gen14697077MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_12_gen4045899478.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_12_gen4045899478MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_2_gen2234585871.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_2_gen2234585871MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_3_gen3116483514.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_3_gen3116483514MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_4_gen2122089277.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_4_gen2122089277MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_5_gen2227126508.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_5_gen2227126508MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_6_gen1301445835.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_6_gen1301445835MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_7_gen3889515902.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_7_gen3889515902MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_8_gen2707677753.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_8_gen2707677753MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_9_gen2228482800.h"
#include "AssemblyU2DCSharp_Zenject_FactoryNested_9_gen2228482800MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryUntyped_1_gen2760421534.h"
#include "AssemblyU2DCSharp_Zenject_FactoryUntyped_1_gen2760421534MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions3479487268MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_MiscExtensions2809094742MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectResolveException1201052999MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_FactoryUntyped_2_gen884050241.h"
#include "AssemblyU2DCSharp_Zenject_FactoryUntyped_2_gen884050241MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_1_gen2417718272.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_1_gen2417718272MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory3309736270MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Assert1161478012MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions3479487268.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory3309736270.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_2_gen546746903.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_2_gen546746903MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_3_gen639885202.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_3_gen639885202MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_4_gen486081797.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_4_gen486081797MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_5_gen2159204100.h"
#include "AssemblyU2DCSharp_Zenject_GameObjectFactory_5_gen2159204100MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_U3CToIn3921245137.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_U3CToIn3921245137MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_U3CToMe1867793022.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_U3CToMe1867793022MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_U3CToPr1205708684.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_U3CToPr1205708684MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_gen1064970850.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_1_gen1064970850MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1807883816.h"
#include "System_Core_System_Func_2_gen3908423697.h"
#include "System_Core_System_Func_2_gen3908423697MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1807883816MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectBindException3550319704MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectBindException3550319704.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_2_U3CToMe2008305412.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_2_U3CToMe2008305412MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_2_U3CToPr3739000318.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_2_U3CToPr3739000318MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_2_gen3921329133.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_2_gen3921329133MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen655913167.h"
#include "System_Core_System_Func_2_gen2756453048.h"
#include "System_Core_System_Func_2_gen2756453048MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen655913167MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_3_U3CToMe3714372930.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_3_U3CToMe3714372930MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_3_U3CToPre385784096.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_3_U3CToPre385784096MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_3_gen153990564.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_3_gen153990564MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1303598458.h"
#include "System_Core_System_Func_2_gen3404138339.h"
#include "System_Core_System_Func_2_gen3404138339MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1303598458MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_4_U3CToMe2106438637.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_4_U3CToMe2106438637MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_4_U3CToPr2698428119.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_4_U3CToPr2698428119MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_4_gen2507049579.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_4_gen2507049579MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1578143997.h"
#include "System_Core_System_Func_2_gen3678683878.h"
#include "System_Core_System_Func_2_gen3678683878MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1578143997MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_5_U3CToMe3805965893.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_5_U3CToMe3805965893MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_5_U3CToPr2344802419.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_5_U3CToPr2344802419MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_5_gen1497530694.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryBinder_5_gen1497530694MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen46883500.h"
#include "System_Core_System_Func_2_gen2147423381.h"
#include "System_Core_System_Func_2_gen2147423381MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen46883500MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryUntypedBinder_1_3854809107.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryUntypedBinder_1_3854809107MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryUntypedBinder_1_1801356992.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryUntypedBinder_1_1801356992MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryUntypedBinder_1_g486203833.h"
#include "AssemblyU2DCSharp_Zenject_IFactoryUntypedBinder_1_g486203833MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1217979071.h"
#include "System_Core_System_Func_2_gen3318518952.h"
#include "System_Core_System_Func_2_gen3318518952MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1217979071MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_2_gen3889101264.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_2_gen3889101264MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactoryBase_2_gen2864689411MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_3_gen2002144469.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_3_gen2002144469MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_4_gen2209276758.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_4_gen2209276758MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_5_gen1813071951.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_5_gen1813071951MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_6_gen605647996.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactory_6_gen605647996MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactoryBase_2_U3CVa3243744428.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactoryBase_2_U3CVa3243744428MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_KeyedFactoryBase_2_gen2864689411.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1471581369.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1471581369MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3393718463.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3393718463MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1238609310.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1238609310MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3794856649.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1028126887.h"
#include "System_Core_System_Func_2_gen361610966.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen231167918.h"
#include "System_Core_System_Func_2_gen361610966MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions3281724522MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions3281724522.h"
#include "System_Core_System_Func_2_gen2267165834.h"
#include "System_Core_System_Func_2_gen2267165834MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2303734481.h"
#include "System_Core_System_Func_2_gen2303734481MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1028126887MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ListFactory_1_gen3970372955.h"
#include "AssemblyU2DCSharp_Zenject_ListFactory_1_gen3970372955MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3576188904.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3576188904MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1661971896MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1661971896.h"
#include "AssemblyU2DCSharp_Zenject_MethodProvider_1_U3CGetI1883175366.h"
#include "AssemblyU2DCSharp_Zenject_MethodProvider_1_U3CGetI1883175366MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_MethodProvider_1_gen512585361.h"
#include "AssemblyU2DCSharp_Zenject_MethodProvider_1_gen512585361MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391MethodDeclarations.h"
#include "System_Core_System_Func_1_gen2111270149MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2621245597MethodDeclarations.h"
#include "System_Core_System_Func_1_gen2111270149.h"

// !!0 System.Activator::CreateInstance<System.Object>()
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m3881739293_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisIl2CppObject_m3881739293(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisIl2CppObject_m3881739293_gshared)(__this /* static, unused */, method)
// IStream`1<!!0> StreamAPI::FirstOnly<System.Object>(IStream`1<!!0>)
extern "C"  Il2CppObject* StreamAPI_FirstOnly_TisIl2CppObject_m1083216406_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define StreamAPI_FirstOnly_TisIl2CppObject_m1083216406(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))StreamAPI_FirstOnly_TisIl2CppObject_m1083216406_gshared)(__this /* static, unused */, p0, method)
// !!0 Zenject.DiContainer::Resolve<System.Object>(Zenject.InjectContext)
extern "C"  Il2CppObject * DiContainer_Resolve_TisIl2CppObject_m1365387674_gshared (DiContainer_t2383114449 * __this, InjectContext_t3456483891 * p0, const MethodInfo* method);
#define DiContainer_Resolve_TisIl2CppObject_m1365387674(__this, p0, method) ((  Il2CppObject * (*) (DiContainer_t2383114449 *, InjectContext_t3456483891 *, const MethodInfo*))DiContainer_Resolve_TisIl2CppObject_m1365387674_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.Binder::ToMethodBase<System.Object>(System.Func`2<Zenject.InjectContext,!!0>)
extern "C"  BindingConditionSetter_t259147722 * Binder_ToMethodBase_TisIl2CppObject_m2822883896_gshared (Binder_t3872662847 * __this, Func_2_t2621245597 * p0, const MethodInfo* method);
#define Binder_ToMethodBase_TisIl2CppObject_m2822883896(__this, p0, method) ((  BindingConditionSetter_t259147722 * (*) (Binder_t3872662847 *, Func_2_t2621245597 *, const MethodInfo*))Binder_ToMethodBase_TisIl2CppObject_m2822883896_gshared)(__this, p0, method)
// !!0 Zenject.DiContainer::Instantiate<System.Object>(System.Object[])
extern "C"  Il2CppObject * DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared (DiContainer_t2383114449 * __this, ObjectU5BU5D_t11523773* p0, const MethodInfo* method);
#define DiContainer_Instantiate_TisIl2CppObject_m1682369513(__this, p0, method) ((  Il2CppObject * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// Zenject.TypeValuePair Zenject.InstantiateUtil::CreateTypePair<System.Object>(!!0)
extern "C"  TypeValuePair_t620932390 * InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397(__this /* static, unused */, p0, method) ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))InstantiateUtil_CreateTypePair_TisIl2CppObject_m1693637397_gshared)(__this /* static, unused */, p0, method)
// !!0 Zenject.DiContainer::InstantiateExplicit<System.Object>(System.Collections.Generic.List`1<Zenject.TypeValuePair>)
extern "C"  Il2CppObject * DiContainer_InstantiateExplicit_TisIl2CppObject_m2003017163_gshared (DiContainer_t2383114449 * __this, List_1_t1417891359 * p0, const MethodInfo* method);
#define DiContainer_InstantiateExplicit_TisIl2CppObject_m2003017163(__this, p0, method) ((  Il2CppObject * (*) (DiContainer_t2383114449 *, List_1_t1417891359 *, const MethodInfo*))DiContainer_InstantiateExplicit_TisIl2CppObject_m2003017163_gshared)(__this, p0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.DiContainer::ValidateObjectGraph<System.Object>(System.Type[])
extern "C"  Il2CppObject* DiContainer_ValidateObjectGraph_TisIl2CppObject_m52669789_gshared (DiContainer_t2383114449 * __this, TypeU5BU5D_t3431720054* p0, const MethodInfo* method);
#define DiContainer_ValidateObjectGraph_TisIl2CppObject_m52669789(__this, p0, method) ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))DiContainer_ValidateObjectGraph_TisIl2CppObject_m52669789_gshared)(__this, p0, method)
// System.Boolean ModestTree.TypeExtensions::DerivesFrom<System.Object>(System.Type)
extern "C"  bool TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940_gshared (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method);
#define TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.TypeExtensions::DerivesFrom<UnityEngine.Component>(System.Type)
#define TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940_gshared)(__this /* static, unused */, p0, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.FactoryMethod`1<System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisFactoryMethod_1_t672889922_m3287146390(__this, p0, method) ((  FactoryMethod_1_t672889922 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.GameObjectFactory`1<System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisGameObjectFactory_1_t2417718272_m3229003752(__this, p0, method) ((  GameObjectFactory_1_t2417718272 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<System.Object>(System.String)
extern "C"  BinderGeneric_1_t520705716 * DiContainer_Bind_TisIl2CppObject_m288115653_gshared (DiContainer_t2383114449 * __this, String_t* p0, const MethodInfo* method);
#define DiContainer_Bind_TisIl2CppObject_m288115653(__this, p0, method) ((  BinderGeneric_1_t520705716 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`1<System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_1_t2124284520_m2089390002(__this, p0, method) ((  BinderGeneric_1_t1807883816 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToTransient<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared (BinderGeneric_1_t520705716 * __this, const MethodInfo* method);
#define BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`1<System.Object>>::ToTransient<Zenject.Factory`1<System.Object>>()
#define BinderGeneric_1_ToTransient_TisFactory_1_t250748329_m945758033(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1807883816 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.FactoryMethod`2<System.Object,System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisFactoryMethod_2_t1984661133_m1256959043(__this, p0, method) ((  FactoryMethod_2_t1984661133 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.GameObjectFactory`2<System.Object,System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisGameObjectFactory_2_t546746903_m3356525845(__this, p0, method) ((  GameObjectFactory_2_t546746903 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`2<System.Object,System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_2_t972313871_m3824815775(__this, p0, method) ((  BinderGeneric_1_t655913167 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`2<System.Object,System.Object>>::ToTransient<Zenject.Factory`2<System.Object,System.Object>>()
#define BinderGeneric_1_ToTransient_TisFactory_2_t2762000298_m1726055921(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t655913167 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisFactoryMethod_3_t1233407108_m1066341360(__this, p0, method) ((  FactoryMethod_3_t1233407108 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisGameObjectFactory_3_t639885202_m3310818626(__this, p0, method) ((  GameObjectFactory_3_t639885202 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`3<System.Object,System.Object,System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_3_t1619999162_m40085644(__this, p0, method) ((  BinderGeneric_1_t1303598458 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`3<System.Object,System.Object,System.Object>>::ToTransient<Zenject.Factory`3<System.Object,System.Object,System.Object>>()
#define BinderGeneric_1_ToTransient_TisFactory_3_t2717473363_m4059117329(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1303598458 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisFactoryMethod_4_t1102797835_m2312226461(__this, p0, method) ((  FactoryMethod_4_t1102797835 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisGameObjectFactory_4_t486081797_m4115402863(__this, p0, method) ((  GameObjectFactory_4_t486081797 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_4_t1894544701_m2619084153(__this, p0, method) ((  BinderGeneric_1_t1578143997 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>>::ToTransient<Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>>()
#define BinderGeneric_1_ToTransient_TisFactory_4_t549530144_m457040561(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1578143997 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisFactoryMethod_5_t2447447334_m4153766986(__this, p0, method) ((  FactoryMethod_5_t2447447334 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// !!0 Zenject.DiContainer::Instantiate<Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>(System.Object[])
#define DiContainer_Instantiate_TisGameObjectFactory_5_t2159204100_m215557788(__this, p0, method) ((  GameObjectFactory_5_t2159204100 * (*) (DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))DiContainer_Instantiate_TisIl2CppObject_m1682369513_gshared)(__this, p0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>(System.String)
#define DiContainer_Bind_TisIFactory_5_t363284204_m23129446(__this, p0, method) ((  BinderGeneric_1_t46883500 * (*) (DiContainer_t2383114449 *, String_t*, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m288115653_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>::ToTransient<Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>>()
#define BinderGeneric_1_ToTransient_TisFactory_5_t2495851293_m773273809(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t46883500 *, const MethodInfo*))BinderGeneric_1_ToTransient_TisIl2CppObject_m4254792828_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<System.Object>()
extern "C"  BinderGeneric_1_t520705716 * DiContainer_Bind_TisIl2CppObject_m3651981917_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_Bind_TisIl2CppObject_m3651981917(__this, method) ((  BinderGeneric_1_t520705716 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IFactoryUntyped`1<System.Object>>()
#define DiContainer_Bind_TisIFactoryUntyped_1_t1534379775_m836629225(__this, method) ((  BinderGeneric_1_t1217979071 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToSingle<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToSingle_TisIl2CppObject_m3066969676_gshared (BinderGeneric_1_t520705716 * __this, const MethodInfo* method);
#define BinderGeneric_1_ToSingle_TisIl2CppObject_m3066969676(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, const MethodInfo*))BinderGeneric_1_ToSingle_TisIl2CppObject_m3066969676_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IFactoryUntyped`1<System.Object>>::ToSingle<Zenject.FactoryUntyped`1<System.Object>>()
#define BinderGeneric_1_ToSingle_TisFactoryUntyped_1_t2760421534_m3281385277(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1217979071 *, const MethodInfo*))BinderGeneric_1_ToSingle_TisIl2CppObject_m3066969676_gshared)(__this, method)
// System.Boolean ModestTree.TypeExtensions::DerivesFromOrEqual<System.Object>(System.Type)
extern "C"  bool TypeExtensions_DerivesFromOrEqual_TisIl2CppObject_m3995372629_gshared (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method);
#define TypeExtensions_DerivesFromOrEqual_TisIl2CppObject_m3995372629(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))TypeExtensions_DerivesFromOrEqual_TisIl2CppObject_m3995372629_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<ModestTree.Util.Tuple`2<System.Object,System.Type>,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTuple_2_t231167918_TisIl2CppObject_m3166785775(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t361610966 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> ModestTree.LinqExtensions::GetDuplicates<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* LinqExtensions_GetDuplicates_TisIl2CppObject_m4146101632_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define LinqExtensions_GetDuplicates_TisIl2CppObject_m4146101632(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))LinqExtensions_GetDuplicates_TisIl2CppObject_m4146101632_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.LinqExtensions::IsEmpty<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisIl2CppObject_TisString_t_m3843025788(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2267165834 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.Dictionary`2<!!1,!!2> System.Linq.Enumerable::ToDictionary<System.Object,System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>,System.Func`2<!!0,!!2>)
extern "C"  Dictionary_2_t3824425150 * Enumerable_ToDictionary_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3788944539_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, Func_2_t2135783352 * p2, const MethodInfo* method);
#define Enumerable_ToDictionary_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3788944539(__this /* static, unused */, p0, p1, p2, method) ((  Dictionary_2_t3824425150 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, Func_2_t2135783352 *, const MethodInfo*))Enumerable_ToDictionary_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3788944539_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Collections.Generic.Dictionary`2<!!1,!!2> System.Linq.Enumerable::ToDictionary<ModestTree.Util.Tuple`2<System.Object,System.Type>,System.Object,System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>,System.Func`2<!!0,!!2>)
#define Enumerable_ToDictionary_TisTuple_2_t231167918_TisIl2CppObject_TisType_t_m2926346732(__this /* static, unused */, p0, p1, p2, method) ((  Dictionary_2_t1471581369 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t361610966 *, Func_2_t2303734481 *, const MethodInfo*))Enumerable_ToDictionary_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m3788944539_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<System.Object>()
extern "C"  Il2CppObject* Enumerable_Empty_TisIl2CppObject_m301282091_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Enumerable_Empty_TisIl2CppObject_m301282091(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisIl2CppObject_m301282091_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<Zenject.ZenjectResolveException>()
#define Enumerable_Empty_TisZenjectResolveException_t1201052999_m2785882006(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisIl2CppObject_m301282091_gshared)(__this /* static, unused */, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m3171479704_gshared (TweenRunner_1_t1434350349 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C"  Il2CppObject * TweenRunner_1_Start_m4199917220_gshared (Il2CppObject * __this /* static, unused */, ColorTween_t2894377818  ___tweenInfo0, const MethodInfo* method)
{
	U3CStartU3Ec__Iterator0_t2376956030 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t2376956030 * L_0 = (U3CStartU3Ec__Iterator0_t2376956030 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t2376956030 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t2376956030 *)L_0;
		U3CStartU3Ec__Iterator0_t2376956030 * L_1 = V_0;
		ColorTween_t2894377818  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t2376956030 * L_3 = V_0;
		ColorTween_t2894377818  L_4 = ___tweenInfo0;
		NullCheck(L_3);
		L_3->set_U3CU24U3EtweenInfo_5(L_4);
		U3CStartU3Ec__Iterator0_t2376956030 * L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m3753968659_gshared (TweenRunner_1_t1434350349 * __this, MonoBehaviour_t3012272455 * ___coroutineContainer0, const MethodInfo* method)
{
	{
		MonoBehaviour_t3012272455 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2341039237;
extern const uint32_t TweenRunner_1_StartTween_m3060079017_MetadataUsageId;
extern "C"  void TweenRunner_1_StartTween_m3060079017_gshared (TweenRunner_1_t1434350349 * __this, ColorTween_t2894377818  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m3060079017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_t3012272455 * L_0 = (MonoBehaviour_t3012272455 *)__this->get_m_CoroutineContainer_0();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2341039237, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_Tween_1();
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		MonoBehaviour_t3012272455 * L_3 = (MonoBehaviour_t3012272455 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t3012272455 *)L_3);
		MonoBehaviour_StopCoroutine_m1340700766((MonoBehaviour_t3012272455 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_m_Tween_1((Il2CppObject *)NULL);
	}

IL_003f:
	{
		MonoBehaviour_t3012272455 * L_5 = (MonoBehaviour_t3012272455 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t2126946602 *)L_5);
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m2112202034((Component_t2126946602 *)L_5, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_6);
		bool L_7 = GameObject_get_activeInHierarchy_m612450965((GameObject_t4012695102 *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0067;
		}
	}
	{
		ColorTween_TweenValue_m3209849337((ColorTween_t2894377818 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		return;
	}

IL_0067:
	{
		ColorTween_t2894377818  L_8 = ___info0;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ColorTween_t2894377818 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (ColorTween_t2894377818 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_m_Tween_1(L_9);
		MonoBehaviour_t3012272455 * L_10 = (MonoBehaviour_t3012272455 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t3012272455 *)L_10);
		MonoBehaviour_StartCoroutine_m2942381565((MonoBehaviour_t3012272455 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m2994637951_gshared (TweenRunner_1_t3422778292 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
extern "C"  Il2CppObject * TweenRunner_1_Start_m3012790173_gshared (Il2CppObject * __this /* static, unused */, FloatTween_t587838465  ___tweenInfo0, const MethodInfo* method)
{
	U3CStartU3Ec__Iterator0_t70416677 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t70416677 * L_0 = (U3CStartU3Ec__Iterator0_t70416677 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t70416677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t70416677 *)L_0;
		U3CStartU3Ec__Iterator0_t70416677 * L_1 = V_0;
		FloatTween_t587838465  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t70416677 * L_3 = V_0;
		FloatTween_t587838465  L_4 = ___tweenInfo0;
		NullCheck(L_3);
		L_3->set_U3CU24U3EtweenInfo_5(L_4);
		U3CStartU3Ec__Iterator0_t70416677 * L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m2355829498_gshared (TweenRunner_1_t3422778292 * __this, MonoBehaviour_t3012272455 * ___coroutineContainer0, const MethodInfo* method)
{
	{
		MonoBehaviour_t3012272455 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2341039237;
extern const uint32_t TweenRunner_1_StartTween_m565832784_MetadataUsageId;
extern "C"  void TweenRunner_1_StartTween_m565832784_gshared (TweenRunner_1_t3422778292 * __this, FloatTween_t587838465  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m565832784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_t3012272455 * L_0 = (MonoBehaviour_t3012272455 *)__this->get_m_CoroutineContainer_0();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2341039237, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_Tween_1();
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		MonoBehaviour_t3012272455 * L_3 = (MonoBehaviour_t3012272455 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t3012272455 *)L_3);
		MonoBehaviour_StopCoroutine_m1340700766((MonoBehaviour_t3012272455 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_m_Tween_1((Il2CppObject *)NULL);
	}

IL_003f:
	{
		MonoBehaviour_t3012272455 * L_5 = (MonoBehaviour_t3012272455 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t2126946602 *)L_5);
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m2112202034((Component_t2126946602 *)L_5, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_6);
		bool L_7 = GameObject_get_activeInHierarchy_m612450965((GameObject_t4012695102 *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0067;
		}
	}
	{
		FloatTween_TweenValue_m1980315890((FloatTween_t587838465 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		return;
	}

IL_0067:
	{
		FloatTween_t587838465  L_8 = ___info0;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, FloatTween_t587838465 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (FloatTween_t587838465 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_m_Tween_1(L_9);
		MonoBehaviour_t3012272455 * L_10 = (MonoBehaviour_t3012272455 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t3012272455 *)L_10);
		MonoBehaviour_StartCoroutine_m2942381565((MonoBehaviour_t3012272455 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C"  void ListPool_1__cctor_m3344713728_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t3624835661 * L_0 = ((ListPool_1_t2461429948_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t3624835661 * L_2 = (UnityAction_1_t3624835661 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t3624835661 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t2461429948_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t3624835661 * L_3 = ((ListPool_1_t2461429948_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t1466895342 * L_4 = (ObjectPool_1_t1466895342 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t1466895342 *, UnityAction_1_t3624835661 *, UnityAction_1_t3624835661 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t3624835661 *)G_B2_0, (UnityAction_1_t3624835661 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t2461429948_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C"  List_1_t3644373756 * ListPool_1_Get_m1671355248_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1466895342 * L_0 = ((ListPool_1_t2461429948_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t1466895342 *)L_0);
		List_1_t3644373756 * L_1 = ((  List_1_t3644373756 * (*) (ObjectPool_1_t1466895342 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t1466895342 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1485191562_gshared (Il2CppObject * __this /* static, unused */, List_1_t3644373756 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1466895342 * L_0 = ((ListPool_1_t2461429948_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t3644373756 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t1466895342 *)L_0);
		((  void (*) (ObjectPool_1_t1466895342 *, List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t1466895342 *)L_0, (List_1_t3644373756 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m1194038743_gshared (Il2CppObject * __this /* static, unused */, List_1_t3644373756 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3644373756 * L_0 = ___l0;
		NullCheck((List_1_t3644373756 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Int32>::Clear() */, (List_1_t3644373756 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
extern "C"  void ListPool_1__cctor_m4278935907_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t1614527294 * L_0 = ((ListPool_1_t451121581_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t1614527294 * L_2 = (UnityAction_1_t1614527294 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t1614527294 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t451121581_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t1614527294 * L_3 = ((ListPool_1_t451121581_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t3751554271 * L_4 = (ObjectPool_1_t3751554271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t3751554271 *, UnityAction_1_t1614527294 *, UnityAction_1_t1614527294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t1614527294 *)G_B2_0, (UnityAction_1_t1614527294 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t451121581_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
extern "C"  List_1_t1634065389 * ListPool_1_Get_m2234302831_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3751554271 * L_0 = ((ListPool_1_t451121581_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3751554271 *)L_0);
		List_1_t1634065389 * L_1 = ((  List_1_t1634065389 * (*) (ObjectPool_1_t3751554271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t3751554271 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1182093831_gshared (Il2CppObject * __this /* static, unused */, List_1_t1634065389 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3751554271 * L_0 = ((ListPool_1_t451121581_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1634065389 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3751554271 *)L_0);
		((  void (*) (ObjectPool_1_t3751554271 *, List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t3751554271 *)L_0, (List_1_t1634065389 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m4207408660_gshared (Il2CppObject * __this /* static, unused */, List_1_t1634065389 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1634065389 * L_0 = ___l0;
		NullCheck((List_1_t1634065389 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Object>::Clear() */, (List_1_t1634065389 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C"  void ListPool_1__cctor_m2225857654_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t619537785 * L_0 = ((ListPool_1_t3751099368_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t619537785 * L_2 = (UnityAction_1_t619537785 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t619537785 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3751099368_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t619537785 * L_3 = ((ListPool_1_t3751099368_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t2756564762 * L_4 = (ObjectPool_1_t2756564762 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t2756564762 *, UnityAction_1_t619537785 *, UnityAction_1_t619537785 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t619537785 *)G_B2_0, (UnityAction_1_t619537785 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3751099368_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C"  List_1_t639075880 * ListPool_1_Get_m1848305276_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2756564762 * L_0 = ((ListPool_1_t3751099368_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2756564762 *)L_0);
		List_1_t639075880 * L_1 = ((  List_1_t639075880 * (*) (ObjectPool_1_t2756564762 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t2756564762 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1142705876_gshared (Il2CppObject * __this /* static, unused */, List_1_t639075880 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2756564762 * L_0 = ((ListPool_1_t3751099368_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t639075880 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2756564762 *)L_0);
		((  void (*) (ObjectPool_1_t2756564762 *, List_1_t639075880 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t2756564762 *)L_0, (List_1_t639075880 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m4228322977_gshared (Il2CppObject * __this /* static, unused */, List_1_t639075880 * ___l0, const MethodInfo* method)
{
	{
		List_1_t639075880 * L_0 = ___l0;
		NullCheck((List_1_t639075880 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Clear() */, (List_1_t639075880 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void ListPool_1__cctor_m3740250016_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t3037482479 * L_0 = ((ListPool_1_t1874076766_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t3037482479 * L_2 = (UnityAction_1_t3037482479 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t3037482479 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t1874076766_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t3037482479 * L_3 = ((ListPool_1_t1874076766_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t879542160 * L_4 = (ObjectPool_1_t879542160 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t879542160 *, UnityAction_1_t3037482479 *, UnityAction_1_t3037482479 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t3037482479 *)G_B2_0, (UnityAction_1_t3037482479 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t1874076766_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
extern "C"  List_1_t3057020574 * ListPool_1_Get_m3130095824_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t879542160 * L_0 = ((ListPool_1_t1874076766_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t879542160 *)L_0);
		List_1_t3057020574 * L_1 = ((  List_1_t3057020574 * (*) (ObjectPool_1_t879542160 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t879542160 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m2709067242_gshared (Il2CppObject * __this /* static, unused */, List_1_t3057020574 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t879542160 * L_0 = ((ListPool_1_t1874076766_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t3057020574 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t879542160 *)L_0);
		((  void (*) (ObjectPool_1_t879542160 *, List_1_t3057020574 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t879542160 *)L_0, (List_1_t3057020574 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m3908991543_gshared (Il2CppObject * __this /* static, unused */, List_1_t3057020574 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3057020574 * L_0 = ___l0;
		NullCheck((List_1_t3057020574 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear() */, (List_1_t3057020574 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C"  void ListPool_1__cctor_m3567944713_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t7783366 * L_0 = ((ListPool_1_t3139344949_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t7783366 * L_2 = (UnityAction_1_t7783366 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t7783366 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3139344949_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t7783366 * L_3 = ((ListPool_1_t3139344949_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t2144810343 * L_4 = (ObjectPool_1_t2144810343 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t2144810343 *, UnityAction_1_t7783366 *, UnityAction_1_t7783366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t7783366 *)G_B2_0, (UnityAction_1_t7783366 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3139344949_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C"  List_1_t27321461 * ListPool_1_Get_m1779148745_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2144810343 * L_0 = ((ListPool_1_t3139344949_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2144810343 *)L_0);
		List_1_t27321461 * L_1 = ((  List_1_t27321461 * (*) (ObjectPool_1_t2144810343 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t2144810343 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3969187617_gshared (Il2CppObject * __this /* static, unused */, List_1_t27321461 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2144810343 * L_0 = ((ListPool_1_t3139344949_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t27321461 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2144810343 *)L_0);
		((  void (*) (ObjectPool_1_t2144810343 *, List_1_t27321461 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t2144810343 *)L_0, (List_1_t27321461 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m1915587246_gshared (Il2CppObject * __this /* static, unused */, List_1_t27321461 * ___l0, const MethodInfo* method)
{
	{
		List_1_t27321461 * L_0 = ___l0;
		NullCheck((List_1_t27321461 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, (List_1_t27321461 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C"  void ListPool_1__cctor_m3697027432_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t7783367 * L_0 = ((ListPool_1_t3139344950_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t7783367 * L_2 = (UnityAction_1_t7783367 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t7783367 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3139344950_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t7783367 * L_3 = ((ListPool_1_t3139344950_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t2144810344 * L_4 = (ObjectPool_1_t2144810344 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t2144810344 *, UnityAction_1_t7783367 *, UnityAction_1_t7783367 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t7783367 *)G_B2_0, (UnityAction_1_t7783367 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3139344950_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
extern "C"  List_1_t27321462 * ListPool_1_Get_m4266661578_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2144810344 * L_0 = ((ListPool_1_t3139344950_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2144810344 *)L_0);
		List_1_t27321462 * L_1 = ((  List_1_t27321462 * (*) (ObjectPool_1_t2144810344 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t2144810344 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3961428258_gshared (Il2CppObject * __this /* static, unused */, List_1_t27321462 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2144810344 * L_0 = ((ListPool_1_t3139344950_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t27321462 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2144810344 *)L_0);
		((  void (*) (ObjectPool_1_t2144810344 *, List_1_t27321462 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t2144810344 *)L_0, (List_1_t27321462 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m2195937135_gshared (Il2CppObject * __this /* static, unused */, List_1_t27321462 * ___l0, const MethodInfo* method)
{
	{
		List_1_t27321462 * L_0 = ___l0;
		NullCheck((List_1_t27321462 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear() */, (List_1_t27321462 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
extern "C"  void ListPool_1__cctor_m3826110151_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t7783368 * L_0 = ((ListPool_1_t3139344951_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t7783368 * L_2 = (UnityAction_1_t7783368 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t7783368 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3139344951_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t7783368 * L_3 = ((ListPool_1_t3139344951_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t2144810345 * L_4 = (ObjectPool_1_t2144810345 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t2144810345 *, UnityAction_1_t7783368 *, UnityAction_1_t7783368 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t7783368 *)G_B2_0, (UnityAction_1_t7783368 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3139344951_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
extern "C"  List_1_t27321463 * ListPool_1_Get_m2459207115_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2144810345 * L_0 = ((ListPool_1_t3139344951_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2144810345 *)L_0);
		List_1_t27321463 * L_1 = ((  List_1_t27321463 * (*) (ObjectPool_1_t2144810345 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t2144810345 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3953668899_gshared (Il2CppObject * __this /* static, unused */, List_1_t27321463 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2144810345 * L_0 = ((ListPool_1_t3139344951_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t27321463 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2144810345 *)L_0);
		((  void (*) (ObjectPool_1_t2144810345 *, List_1_t27321463 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t2144810345 *)L_0, (List_1_t27321463 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m2476287024_gshared (Il2CppObject * __this /* static, unused */, List_1_t27321463 * ___l0, const MethodInfo* method)
{
	{
		List_1_t27321463 * L_0 = ___l0;
		NullCheck((List_1_t27321463 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Clear() */, (List_1_t27321463 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void ObjectPool_1__ctor_m2532712771_gshared (ObjectPool_1_t2954595302 * __this, UnityAction_1_t817568325 * ___actionOnGet0, UnityAction_1_t817568325 * ___actionOnRelease1, const MethodInfo* method)
{
	{
		Stack_1_t3407512455 * L_0 = (Stack_1_t3407512455 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stack_1_t3407512455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_m_Stack_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		UnityAction_1_t817568325 * L_1 = ___actionOnGet0;
		__this->set_m_ActionOnGet_1(L_1);
		UnityAction_1_t817568325 * L_2 = ___actionOnRelease1;
		__this->set_m_ActionOnRelease_2(L_2);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C"  int32_t ObjectPool_1_get_countAll_m3327915100_gshared (ObjectPool_1_t2954595302 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CcountAllU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C"  void ObjectPool_1_set_countAll_m2125882937_gshared (ObjectPool_1_t2954595302 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C"  int32_t ObjectPool_1_get_countActive_m2082506317_gshared (ObjectPool_1_t2954595302 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObjectPool_1_t2954595302 *)__this);
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t2954595302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ObjectPool_1_t2954595302 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((ObjectPool_1_t2954595302 *)__this);
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t2954595302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ObjectPool_1_t2954595302 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return ((int32_t)((int32_t)L_0-(int32_t)L_1));
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C"  int32_t ObjectPool_1_get_countInactive_m19645682_gshared (ObjectPool_1_t2954595302 * __this, const MethodInfo* method)
{
	{
		Stack_1_t3407512455 * L_0 = (Stack_1_t3407512455 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3407512455 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count() */, (Stack_1_t3407512455 *)L_0);
		return L_1;
	}
}
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ObjectPool_1_Get_m3052664832_MetadataUsageId;
extern "C"  Il2CppObject * ObjectPool_1_Get_m3052664832_gshared (ObjectPool_1_t2954595302 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Get_m3052664832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		Stack_1_t3407512455 * L_0 = (Stack_1_t3407512455 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3407512455 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count() */, (Stack_1_t3407512455 *)L_0);
		if (L_1)
		{
			goto IL_0047;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_2 = V_1;
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_3 = V_1;
		G_B4_0 = L_3;
		goto IL_0033;
	}

IL_002e:
	{
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		G_B4_0 = L_4;
	}

IL_0033:
	{
		V_0 = (Il2CppObject *)G_B4_0;
		NullCheck((ObjectPool_1_t2954595302 *)__this);
		int32_t L_5 = ((  int32_t (*) (ObjectPool_1_t2954595302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ObjectPool_1_t2954595302 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((ObjectPool_1_t2954595302 *)__this);
		((  void (*) (ObjectPool_1_t2954595302 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t2954595302 *)__this, (int32_t)((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		goto IL_0053;
	}

IL_0047:
	{
		Stack_1_t3407512455 * L_6 = (Stack_1_t3407512455 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3407512455 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (Stack_1_t3407512455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Stack_1_t3407512455 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (Il2CppObject *)L_7;
	}

IL_0053:
	{
		UnityAction_1_t817568325 * L_8 = (UnityAction_1_t817568325 *)__this->get_m_ActionOnGet_1();
		if (!L_8)
		{
			goto IL_006a;
		}
	}
	{
		UnityAction_1_t817568325 * L_9 = (UnityAction_1_t817568325 *)__this->get_m_ActionOnGet_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((UnityAction_1_t817568325 *)L_9);
		((  void (*) (UnityAction_1_t817568325 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((UnityAction_1_t817568325 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_006a:
	{
		Il2CppObject * L_11 = V_0;
		return L_11;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3474434835;
extern const uint32_t ObjectPool_1_Release_m1110976910_MetadataUsageId;
extern "C"  void ObjectPool_1_Release_m1110976910_gshared (ObjectPool_1_t2954595302 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_m1110976910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stack_1_t3407512455 * L_0 = (Stack_1_t3407512455 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3407512455 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count() */, (Stack_1_t3407512455 *)L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		Stack_1_t3407512455 * L_2 = (Stack_1_t3407512455 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3407512455 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Stack_1_t3407512455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Stack_1_t3407512455 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Il2CppObject * L_4 = ___element0;
		bool L_5 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral3474434835, /*hidden argument*/NULL);
	}

IL_003b:
	{
		UnityAction_1_t817568325 * L_6 = (UnityAction_1_t817568325 *)__this->get_m_ActionOnRelease_2();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		UnityAction_1_t817568325 * L_7 = (UnityAction_1_t817568325 *)__this->get_m_ActionOnRelease_2();
		Il2CppObject * L_8 = ___element0;
		NullCheck((UnityAction_1_t817568325 *)L_7);
		((  void (*) (UnityAction_1_t817568325 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((UnityAction_1_t817568325 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_0052:
	{
		Stack_1_t3407512455 * L_9 = (Stack_1_t3407512455 *)__this->get_m_Stack_0();
		Il2CppObject * L_10 = ___element0;
		NullCheck((Stack_1_t3407512455 *)L_9);
		((  void (*) (Stack_1_t3407512455 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Stack_1_t3407512455 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return;
	}
}
// System.Void Utils2/<FlagsToList>c__AnonStoreyF2`1<System.Object>::.ctor()
extern "C"  void U3CFlagsToListU3Ec__AnonStoreyF2_1__ctor_m2532669463_gshared (U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Utils2/<FlagsToList>c__AnonStoreyF2`1<System.Object>::<>m__15A(System.Int32)
extern "C"  bool U3CFlagsToListU3Ec__AnonStoreyF2_1_U3CU3Em__15A_m1099480996_gshared (U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067 * __this, int32_t ___m0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_mask_0();
		int32_t L_1 = ___m0;
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)L_1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Utils2/<FormatEach>c__AnonStoreyEF`1<System.Object>::.ctor()
extern "C"  void U3CFormatEachU3Ec__AnonStoreyEF_1__ctor_m3984047472_gshared (U3CFormatEachU3Ec__AnonStoreyEF_1_t2861012304 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Utils2/<FormatEach>c__AnonStoreyEF`1<System.Object>::<>m__155(System.Func`2<T,System.Object>)
extern "C"  Il2CppObject * U3CFormatEachU3Ec__AnonStoreyEF_1_U3CU3Em__155_m2580837254_gshared (U3CFormatEachU3Ec__AnonStoreyEF_1_t2861012304 * __this, Func_2_t2135783352 * ___func0, const MethodInfo* method)
{
	{
		Func_2_t2135783352 * L_0 = ___func0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_value_0();
		NullCheck((Func_2_t2135783352 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t2135783352 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Void Utils2/<Lift>c__Iterator2A`1<System.Object>::.ctor()
extern "C"  void U3CLiftU3Ec__Iterator2A_1__ctor_m878985243_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Utils2/<Lift>c__Iterator2A`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CLiftU3Ec__Iterator2A_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m572408994_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object Utils2/<Lift>c__Iterator2A`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLiftU3Ec__Iterator2A_1_System_Collections_IEnumerator_get_Current_m4138424107_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Collections.IEnumerator Utils2/<Lift>c__Iterator2A`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CLiftU3Ec__Iterator2A_1_System_Collections_IEnumerable_GetEnumerator_m4066860774_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CLiftU3Ec__Iterator2A_1_t2546142915 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CLiftU3Ec__Iterator2A_1_t2546142915 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Utils2/<Lift>c__Iterator2A`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CLiftU3Ec__Iterator2A_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3934413669_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method)
{
	U3CLiftU3Ec__Iterator2A_1_t2546142915 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_1();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CLiftU3Ec__Iterator2A_1_t2546142915 * L_2 = (U3CLiftU3Ec__Iterator2A_1_t2546142915 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CLiftU3Ec__Iterator2A_1_t2546142915 *)L_2;
		U3CLiftU3Ec__Iterator2A_1_t2546142915 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Eself_3();
		NullCheck(L_3);
		L_3->set_self_0(L_4);
		U3CLiftU3Ec__Iterator2A_1_t2546142915 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Utils2/<Lift>c__Iterator2A`1<System.Object>::MoveNext()
extern "C"  bool U3CLiftU3Ec__Iterator2A_1_MoveNext_m2812105209_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0040;
	}

IL_0021:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_self_0();
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_0042;
	}

IL_0039:
	{
		__this->set_U24PC_1((-1));
	}

IL_0040:
	{
		return (bool)0;
	}

IL_0042:
	{
		return (bool)1;
	}
	// Dead block : IL_0044: ldloc.1
}
// System.Void Utils2/<Lift>c__Iterator2A`1<System.Object>::Dispose()
extern "C"  void U3CLiftU3Ec__Iterator2A_1_Dispose_m2789511576_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void Utils2/<Lift>c__Iterator2A`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CLiftU3Ec__Iterator2A_1_Reset_m2820385480_MetadataUsageId;
extern "C"  void U3CLiftU3Ec__Iterator2A_1_Reset_m2820385480_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLiftU3Ec__Iterator2A_1_Reset_m2820385480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::.ctor()
extern "C"  void U3CTakeAllButLastU3Ec__Iterator29_1__ctor_m1968373061_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m256217868_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Object Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_IEnumerator_get_Current_m224952961_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Collections.IEnumerator Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_IEnumerable_GetEnumerator_m2039414908_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CTakeAllButLastU3Ec__Iterator29_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3024146191_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method)
{
	U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * L_2 = (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 *)L_2;
		U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_7();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::MoveNext()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const uint32_t U3CTakeAllButLastU3Ec__Iterator29_1_MoveNext_m4259806607_MetadataUsageId;
extern "C"  bool U3CTakeAllButLastU3Ec__Iterator29_1_MoveNext_m4259806607_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeAllButLastU3Ec__Iterator29_1_MoveNext_m4259806607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_008e;
		}
	}
	{
		goto IL_00b8;
	}

IL_0021:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CitU3E__0_1(L_3);
		__this->set_U3ChasRemainingItemsU3E__1_2((bool)0);
		__this->set_U3CisFirstU3E__2_3((bool)1);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_4 = V_1;
		__this->set_U3CitemU3E__3_4(L_4);
	}

IL_004f:
	{
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CitU3E__0_1();
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
		__this->set_U3ChasRemainingItemsU3E__1_2(L_6);
		bool L_7 = (bool)__this->get_U3ChasRemainingItemsU3E__1_2();
		if (!L_7)
		{
			goto IL_00a6;
		}
	}
	{
		bool L_8 = (bool)__this->get_U3CisFirstU3E__2_3();
		if (L_8)
		{
			goto IL_008e;
		}
	}
	{
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CitemU3E__3_4();
		__this->set_U24current_6(L_9);
		__this->set_U24PC_5(1);
		goto IL_00ba;
	}

IL_008e:
	{
		Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CitU3E__0_1();
		NullCheck((Il2CppObject*)L_10);
		Il2CppObject * L_11 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_10);
		__this->set_U3CitemU3E__3_4(L_11);
		__this->set_U3CisFirstU3E__2_3((bool)0);
	}

IL_00a6:
	{
		bool L_12 = (bool)__this->get_U3ChasRemainingItemsU3E__1_2();
		if (L_12)
		{
			goto IL_004f;
		}
	}
	{
		__this->set_U24PC_5((-1));
	}

IL_00b8:
	{
		return (bool)0;
	}

IL_00ba:
	{
		return (bool)1;
	}
	// Dead block : IL_00bc: ldloc.2
}
// System.Void Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::Dispose()
extern "C"  void U3CTakeAllButLastU3Ec__Iterator29_1_Dispose_m1719184450_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CTakeAllButLastU3Ec__Iterator29_1_Reset_m3909773298_MetadataUsageId;
extern "C"  void U3CTakeAllButLastU3Ec__Iterator29_1_Reset_m3909773298_gshared (U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeAllButLastU3Ec__Iterator29_1_Reset_m3909773298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Utils2/Wrapper`1<System.Object>::.ctor()
extern "C"  void Wrapper_1__ctor_m3548568133_gshared (Wrapper_1_t1353841757 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145<System.Object>::.ctor()
extern "C"  void U3CWaitForStreamEventU3Ec__AnonStorey145__ctor_m3877960318_gshared (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145<System.Object>::<>m__1DF(T)
extern "C"  void U3CWaitForStreamEventU3Ec__AnonStorey145_U3CU3Em__1DF_m3973858928_gshared (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_act_0();
		Il2CppObject * L_1 = ___val0;
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		WaitForStreamEvent_1_t1294890264 * L_2 = (WaitForStreamEvent_1_t1294890264 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_2);
		L_2->set_finished__1((bool)1);
		return;
	}
}
// System.Void WaitForStreamEvent`1<System.Object>::.ctor(IStream`1<T>,System.Action`1<T>)
extern "C"  void WaitForStreamEvent_1__ctor_m3706988780_gshared (WaitForStreamEvent_1_t1294890264 * __this, Il2CppObject* ___stream0, Action_1_t985559125 * ___act1, const MethodInfo* method)
{
	U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * V_0 = NULL;
	{
		U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * L_0 = (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 *)L_0;
		U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * L_1 = V_0;
		Action_1_t985559125 * L_2 = ___act1;
		NullCheck(L_1);
		L_1->set_act_0(L_2);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		Il2CppObject* L_4 = ___stream0;
		Il2CppObject* L_5 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t985559125 * L_8 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_5, (Action_1_t985559125 *)L_8, (int32_t)1);
		__this->set_connection_0(L_9);
		return;
	}
}
// System.Boolean WaitForStreamEvent`1<System.Object>::get_finished()
extern "C"  bool WaitForStreamEvent_1_get_finished_m303596645_gshared (WaitForStreamEvent_1_t1294890264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_finished__1();
		return L_0;
	}
}
// System.Void WaitForStreamEvent`1<System.Object>::Tick(System.Single)
extern "C"  void WaitForStreamEvent_1_Tick_m3307853552_gshared (WaitForStreamEvent_1_t1294890264 * __this, float ___dt0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void WaitForStreamEvent`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t WaitForStreamEvent_1_Dispose_m3277807363_MetadataUsageId;
extern "C"  void WaitForStreamEvent_1_Dispose_m3277807363_gshared (WaitForStreamEvent_1_t1294890264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaitForStreamEvent_1_Dispose_m3277807363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_connection_0();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToGetterBaseU3Ec__AnonStorey16F_2__ctor_m3687156782_gshared (U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult Zenject.Binder/<ToGetterBase>c__AnonStorey16F`2<System.Object,System.Object>::<>m__28D(Zenject.InjectContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InjectContext_t3456483891_il2cpp_TypeInfo_var;
extern const uint32_t U3CToGetterBaseU3Ec__AnonStorey16F_2_U3CU3Em__28D_m1322718792_MetadataUsageId;
extern "C"  Il2CppObject * U3CToGetterBaseU3Ec__AnonStorey16F_2_U3CU3Em__28D_m1322718792_gshared (U3CToGetterBaseU3Ec__AnonStorey16F_2_t3259896156 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToGetterBaseU3Ec__AnonStorey16F_2_U3CU3Em__28D_m1322718792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t2135783352 * L_0 = (Func_2_t2135783352 *)__this->get_method_0();
		InjectContext_t3456483891 * L_1 = ___ctx0;
		NullCheck(L_1);
		DiContainer_t2383114449 * L_2 = (DiContainer_t2383114449 *)L_1->get_Container_9();
		InjectContext_t3456483891 * L_3 = ___ctx0;
		NullCheck(L_3);
		DiContainer_t2383114449 * L_4 = (DiContainer_t2383114449 *)L_3->get_Container_9();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_6 = (String_t*)__this->get_identifier_1();
		InjectContext_t3456483891 * L_7 = ___ctx0;
		NullCheck(L_7);
		Type_t * L_8 = (Type_t *)L_7->get_ObjectType_0();
		InjectContext_t3456483891 * L_9 = ___ctx0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_ObjectInstance_2();
		InjectContext_t3456483891 * L_11 = ___ctx0;
		NullCheck(L_11);
		String_t* L_12 = (String_t*)L_11->get_MemberName_5();
		InjectContext_t3456483891 * L_13 = ___ctx0;
		InjectContext_t3456483891 * L_14 = ___ctx0;
		NullCheck(L_14);
		Il2CppObject * L_15 = (Il2CppObject *)L_14->get_FallBackValue_8();
		InjectContext_t3456483891 * L_16 = (InjectContext_t3456483891 *)il2cpp_codegen_object_new(InjectContext_t3456483891_il2cpp_TypeInfo_var);
		InjectContext__ctor_m3247737529(L_16, (DiContainer_t2383114449 *)L_4, (Type_t *)L_5, (String_t*)L_6, (bool)0, (Type_t *)L_8, (Il2CppObject *)L_10, (String_t*)L_12, (InjectContext_t3456483891 *)L_13, (String_t*)NULL, (Il2CppObject *)L_15, /*hidden argument*/NULL);
		NullCheck((DiContainer_t2383114449 *)L_2);
		Il2CppObject * L_17 = GenericVirtFuncInvoker1< Il2CppObject *, InjectContext_t3456483891 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (DiContainer_t2383114449 *)L_2, (InjectContext_t3456483891 *)L_16);
		NullCheck((Func_2_t2135783352 *)L_0);
		Il2CppObject * L_18 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2135783352 *)L_0, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_18;
	}
}
// System.Void Zenject.Binder/<ToLookupBase>c__AnonStorey16E`1<System.Object>::.ctor()
extern "C"  void U3CToLookupBaseU3Ec__AnonStorey16E_1__ctor_m3869616555_gshared (U3CToLookupBaseU3Ec__AnonStorey16E_1_t2894210511 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TConcrete Zenject.Binder/<ToLookupBase>c__AnonStorey16E`1<System.Object>::<>m__28C(Zenject.InjectContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InjectContext_t3456483891_il2cpp_TypeInfo_var;
extern const uint32_t U3CToLookupBaseU3Ec__AnonStorey16E_1_U3CU3Em__28C_m1798277774_MetadataUsageId;
extern "C"  Il2CppObject * U3CToLookupBaseU3Ec__AnonStorey16E_1_U3CU3Em__28C_m1798277774_gshared (U3CToLookupBaseU3Ec__AnonStorey16E_1_t2894210511 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToLookupBaseU3Ec__AnonStorey16E_1_U3CU3Em__28C_m1798277774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		InjectContext_t3456483891 * L_2 = ___ctx0;
		NullCheck(L_2);
		DiContainer_t2383114449 * L_3 = (DiContainer_t2383114449 *)L_2->get_Container_9();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_5 = (String_t*)__this->get_identifier_0();
		InjectContext_t3456483891 * L_6 = ___ctx0;
		NullCheck(L_6);
		Type_t * L_7 = (Type_t *)L_6->get_ObjectType_0();
		InjectContext_t3456483891 * L_8 = ___ctx0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_ObjectInstance_2();
		InjectContext_t3456483891 * L_10 = ___ctx0;
		NullCheck(L_10);
		String_t* L_11 = (String_t*)L_10->get_MemberName_5();
		InjectContext_t3456483891 * L_12 = ___ctx0;
		InjectContext_t3456483891 * L_13 = ___ctx0;
		NullCheck(L_13);
		Il2CppObject * L_14 = (Il2CppObject *)L_13->get_FallBackValue_8();
		InjectContext_t3456483891 * L_15 = (InjectContext_t3456483891 *)il2cpp_codegen_object_new(InjectContext_t3456483891_il2cpp_TypeInfo_var);
		InjectContext__ctor_m3247737529(L_15, (DiContainer_t2383114449 *)L_3, (Type_t *)L_4, (String_t*)L_5, (bool)0, (Type_t *)L_7, (Il2CppObject *)L_9, (String_t*)L_11, (InjectContext_t3456483891 *)L_12, (String_t*)NULL, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject * L_16 = GenericVirtFuncInvoker1< Il2CppObject *, InjectContext_t3456483891 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (DiContainer_t2383114449 *)L_1, (InjectContext_t3456483891 *)L_15);
		return L_16;
	}
}
// System.Void Zenject.BinderGeneric`1<System.Object>::.ctor(Zenject.DiContainer,System.String,Zenject.SingletonProviderMap)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t BinderGeneric_1__ctor_m467851124_MetadataUsageId;
extern "C"  void BinderGeneric_1__ctor_m467851124_gshared (BinderGeneric_1_t520705716 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, SingletonProviderMap_t1557411893 * ___singletonMap2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BinderGeneric_1__ctor_m467851124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = ___container0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_2 = ___identifier1;
		SingletonProviderMap_t1557411893 * L_3 = ___singletonMap2;
		NullCheck((Binder_t3872662847 *)__this);
		Binder__ctor_m406646005((Binder_t3872662847 *)__this, (DiContainer_t2383114449 *)L_0, (Type_t *)L_1, (String_t*)L_2, (SingletonProviderMap_t1557411893 *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToMethod(System.Func`2<Zenject.InjectContext,TContract>)
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToMethod_m4174284639_gshared (BinderGeneric_1_t520705716 * __this, Func_2_t2621245597 * ___method0, const MethodInfo* method)
{
	{
		Func_2_t2621245597 * L_0 = ___method0;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_1 = ((  BindingConditionSetter_t259147722 * (*) (Binder_t3872662847 *, Func_2_t2621245597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Binder_t3872662847 *)__this, (Func_2_t2621245597 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void Zenject.BindScope/CustomScopeBinder`1<System.Object>::.ctor(Zenject.BindScope,System.String,Zenject.DiContainer,Zenject.SingletonProviderMap)
extern "C"  void CustomScopeBinder_1__ctor_m1386595656_gshared (CustomScopeBinder_1_t4252514087 * __this, BindScope_t2945157996 * ___owner0, String_t* ___identifier1, DiContainer_t2383114449 * ___container2, SingletonProviderMap_t1557411893 * ___singletonMap3, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = ___container2;
		String_t* L_1 = ___identifier1;
		SingletonProviderMap_t1557411893 * L_2 = ___singletonMap3;
		NullCheck((BinderGeneric_1_t520705716 *)__this);
		((  void (*) (BinderGeneric_1_t520705716 *, DiContainer_t2383114449 *, String_t*, SingletonProviderMap_t1557411893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((BinderGeneric_1_t520705716 *)__this, (DiContainer_t2383114449 *)L_0, (String_t*)L_1, (SingletonProviderMap_t1557411893 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BindScope_t2945157996 * L_3 = ___owner0;
		__this->set__owner_4(L_3);
		return;
	}
}
// Zenject.BindingConditionSetter Zenject.BindScope/CustomScopeBinder`1<System.Object>::ToProvider(Zenject.ProviderBase)
extern "C"  BindingConditionSetter_t259147722 * CustomScopeBinder_1_ToProvider_m1928424210_gshared (CustomScopeBinder_1_t4252514087 * __this, ProviderBase_t1627494391 * ___provider0, const MethodInfo* method)
{
	{
		BindScope_t2945157996 * L_0 = (BindScope_t2945157996 *)__this->get__owner_4();
		ProviderBase_t1627494391 * L_1 = ___provider0;
		NullCheck((BindScope_t2945157996 *)L_0);
		BindScope_AddProvider_m1601391874((BindScope_t2945157996 *)L_0, (ProviderBase_t1627494391 *)L_1, /*hidden argument*/NULL);
		ProviderBase_t1627494391 * L_2 = ___provider0;
		NullCheck((Binder_t3872662847 *)__this);
		BindingConditionSetter_t259147722 * L_3 = Binder_ToProvider_m2864916614((Binder_t3872662847 *)__this, (ProviderBase_t1627494391 *)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Zenject.DiContainer/<BindGameObjectFactory>c__AnonStorey18D`1<System.Object>::.ctor()
extern "C"  void U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1__ctor_m4261405394_gshared (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Zenject.DiContainer/<BindGameObjectFactory>c__AnonStorey18D`1<System.Object>::<>m__2CD(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_U3CU3Em__2CD_m3082394938_MetadataUsageId;
extern "C"  Il2CppObject * U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_U3CU3Em__2CD_m3082394938_gshared (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_U3CU3Em__2CD_m3082394938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t4012695102 * L_3 = (GameObject_t4012695102 *)__this->get_prefab_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		Il2CppObject * L_4 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.DiContainer/<Unbind>c__AnonStorey18C`1<System.Object>::.ctor()
extern "C"  void U3CUnbindU3Ec__AnonStorey18C_1__ctor_m1667170675_gshared (U3CUnbindU3Ec__AnonStorey18C_1_t2822308123 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.DiContainer/<Unbind>c__AnonStorey18C`1<System.Object>::<>m__2CA(System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>)
extern const MethodInfo* KeyValuePair_2_get_Value_m4034925883_MethodInfo_var;
extern const uint32_t U3CUnbindU3Ec__AnonStorey18C_1_U3CU3Em__2CA_m1903931049_MetadataUsageId;
extern "C"  bool U3CUnbindU3Ec__AnonStorey18C_1_U3CU3Em__2CA_m1903931049_gshared (U3CUnbindU3Ec__AnonStorey18C_1_t2822308123 * __this, KeyValuePair_2_t2091358871  ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CUnbindU3Ec__AnonStorey18C_1_U3CU3Em__2CA_m1903931049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2424453360 * L_0 = KeyValuePair_2_get_Value_m4034925883((KeyValuePair_2_t2091358871 *)(&___x0), /*hidden argument*/KeyValuePair_2_get_Value_m4034925883_MethodInfo_var);
		ProviderBase_t1627494391 * L_1 = (ProviderBase_t1627494391 *)__this->get_provider_0();
		NullCheck((List_1_t2424453360 *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, ProviderBase_t1627494391 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Zenject.ProviderBase>::Contains(!0) */, (List_1_t2424453360 *)L_0, (ProviderBase_t1627494391 *)L_1);
		return L_2;
	}
}
// System.Void Zenject.Factory`1<System.Object>::.ctor()
extern "C"  void Factory_1__ctor_m2283776443_gshared (Factory_1_t250748329 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`1<System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_1_get_ConstructedType_m2970522342_MetadataUsageId;
extern "C"  Type_t * Factory_1_get_ConstructedType_m2970522342_gshared (Factory_1_t250748329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_1_get_ConstructedType_m2970522342_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`1<System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t Factory_1_get_ProvidedTypes_m1882595918_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_1_get_ProvidedTypes_m1882595918_gshared (Factory_1_t250748329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_1_get_ProvidedTypes_m1882595918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// Zenject.DiContainer Zenject.Factory`1<System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_1_get_Container_m3180103707_gshared (Factory_1_t250748329 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// T Zenject.Factory`1<System.Object>::Create()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t Factory_1_Create_m3665880292_MetadataUsageId;
extern "C"  Il2CppObject * Factory_1_Create_m3665880292_gshared (Factory_1_t250748329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_1_Create_m3665880292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_1 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (DiContainer_t2383114449 *)L_0, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		return L_1;
	}
}
// System.Void Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_10__ctor_m1091867141_gshared (Factory_10_t1264205109 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_10_get_ConstructedType_m295591332_MetadataUsageId;
extern "C"  Type_t * Factory_10_get_ConstructedType_m295591332_gshared (Factory_10_t1264205109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_10_get_ConstructedType_m295591332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_10_get_ProvidedTypes_m2918396808_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_10_get_ProvidedTypes_m2918396808_gshared (Factory_10_t1264205109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_10_get_ProvidedTypes_m2918396808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		TypeU5BU5D_t3431720054* L_8 = (TypeU5BU5D_t3431720054*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Type_t *)L_9);
		TypeU5BU5D_t3431720054* L_10 = (TypeU5BU5D_t3431720054*)L_8;
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Type_t *)L_11);
		TypeU5BU5D_t3431720054* L_12 = (TypeU5BU5D_t3431720054*)L_10;
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (Type_t *)L_13);
		TypeU5BU5D_t3431720054* L_14 = (TypeU5BU5D_t3431720054*)L_12;
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(7), (Type_t *)L_15);
		TypeU5BU5D_t3431720054* L_16 = (TypeU5BU5D_t3431720054*)L_14;
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)), /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(8), (Type_t *)L_17);
		return L_16;
	}
}
// Zenject.DiContainer Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_10_get_Container_m3806906449_gshared (Factory_10_t1264205109 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_10_Create_m633316187_MetadataUsageId;
extern "C"  Il2CppObject * Factory_10_Create_m633316187_gshared (Factory_10_t1264205109 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, Il2CppObject * ___param98, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_10_Create_m633316187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		Il2CppObject * L_9 = ___param32;
		TypeValuePair_t620932390 * L_10 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((List_1_t1417891359 *)L_8);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_8, (TypeValuePair_t620932390 *)L_10);
		List_1_t1417891359 * L_11 = V_0;
		Il2CppObject * L_12 = ___param43;
		TypeValuePair_t620932390 * L_13 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		NullCheck((List_1_t1417891359 *)L_11);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_11, (TypeValuePair_t620932390 *)L_13);
		List_1_t1417891359 * L_14 = V_0;
		Il2CppObject * L_15 = ___param54;
		TypeValuePair_t620932390 * L_16 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((List_1_t1417891359 *)L_14);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_14, (TypeValuePair_t620932390 *)L_16);
		List_1_t1417891359 * L_17 = V_0;
		Il2CppObject * L_18 = ___param65;
		TypeValuePair_t620932390 * L_19 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(NULL /*static, unused*/, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck((List_1_t1417891359 *)L_17);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_17, (TypeValuePair_t620932390 *)L_19);
		List_1_t1417891359 * L_20 = V_0;
		Il2CppObject * L_21 = ___param76;
		TypeValuePair_t620932390 * L_22 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck((List_1_t1417891359 *)L_20);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_20, (TypeValuePair_t620932390 *)L_22);
		List_1_t1417891359 * L_23 = V_0;
		Il2CppObject * L_24 = ___param87;
		TypeValuePair_t620932390 * L_25 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)(NULL /*static, unused*/, (Il2CppObject *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		NullCheck((List_1_t1417891359 *)L_23);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_23, (TypeValuePair_t620932390 *)L_25);
		List_1_t1417891359 * L_26 = V_0;
		Il2CppObject * L_27 = ___param98;
		TypeValuePair_t620932390 * L_28 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		NullCheck((List_1_t1417891359 *)L_26);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_26, (TypeValuePair_t620932390 *)L_28);
		List_1_t1417891359 * L_29 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_30 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_29);
		return L_30;
	}
}
// System.Void Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_11__ctor_m3374782008_gshared (Factory_11_t399731484 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_11_get_ConstructedType_m2790504215_MetadataUsageId;
extern "C"  Type_t * Factory_11_get_ConstructedType_m2790504215_gshared (Factory_11_t399731484 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_11_get_ConstructedType_m2790504215_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_11_get_ProvidedTypes_m847810875_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_11_get_ProvidedTypes_m847810875_gshared (Factory_11_t399731484 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_11_get_ProvidedTypes_m847810875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		TypeU5BU5D_t3431720054* L_8 = (TypeU5BU5D_t3431720054*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Type_t *)L_9);
		TypeU5BU5D_t3431720054* L_10 = (TypeU5BU5D_t3431720054*)L_8;
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Type_t *)L_11);
		TypeU5BU5D_t3431720054* L_12 = (TypeU5BU5D_t3431720054*)L_10;
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (Type_t *)L_13);
		TypeU5BU5D_t3431720054* L_14 = (TypeU5BU5D_t3431720054*)L_12;
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(7), (Type_t *)L_15);
		TypeU5BU5D_t3431720054* L_16 = (TypeU5BU5D_t3431720054*)L_14;
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)), /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(8), (Type_t *)L_17);
		TypeU5BU5D_t3431720054* L_18 = (TypeU5BU5D_t3431720054*)L_16;
		Type_t * L_19 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)), /*hidden argument*/NULL);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Type_t *)L_19);
		return L_18;
	}
}
// Zenject.DiContainer Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_11_get_Container_m125868804_gshared (Factory_11_t399731484 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_11_Create_m2516102464_MetadataUsageId;
extern "C"  Il2CppObject * Factory_11_Create_m2516102464_gshared (Factory_11_t399731484 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, Il2CppObject * ___param98, Il2CppObject * ___param109, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_11_Create_m2516102464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		Il2CppObject * L_9 = ___param32;
		TypeValuePair_t620932390 * L_10 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		NullCheck((List_1_t1417891359 *)L_8);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_8, (TypeValuePair_t620932390 *)L_10);
		List_1_t1417891359 * L_11 = V_0;
		Il2CppObject * L_12 = ___param43;
		TypeValuePair_t620932390 * L_13 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((List_1_t1417891359 *)L_11);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_11, (TypeValuePair_t620932390 *)L_13);
		List_1_t1417891359 * L_14 = V_0;
		Il2CppObject * L_15 = ___param54;
		TypeValuePair_t620932390 * L_16 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck((List_1_t1417891359 *)L_14);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_14, (TypeValuePair_t620932390 *)L_16);
		List_1_t1417891359 * L_17 = V_0;
		Il2CppObject * L_18 = ___param65;
		TypeValuePair_t620932390 * L_19 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(NULL /*static, unused*/, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck((List_1_t1417891359 *)L_17);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_17, (TypeValuePair_t620932390 *)L_19);
		List_1_t1417891359 * L_20 = V_0;
		Il2CppObject * L_21 = ___param76;
		TypeValuePair_t620932390 * L_22 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		NullCheck((List_1_t1417891359 *)L_20);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_20, (TypeValuePair_t620932390 *)L_22);
		List_1_t1417891359 * L_23 = V_0;
		Il2CppObject * L_24 = ___param87;
		TypeValuePair_t620932390 * L_25 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(NULL /*static, unused*/, (Il2CppObject *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		NullCheck((List_1_t1417891359 *)L_23);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_23, (TypeValuePair_t620932390 *)L_25);
		List_1_t1417891359 * L_26 = V_0;
		Il2CppObject * L_27 = ___param98;
		TypeValuePair_t620932390 * L_28 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		NullCheck((List_1_t1417891359 *)L_26);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_26, (TypeValuePair_t620932390 *)L_28);
		List_1_t1417891359 * L_29 = V_0;
		Il2CppObject * L_30 = ___param109;
		TypeValuePair_t620932390 * L_31 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)(NULL /*static, unused*/, (Il2CppObject *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		NullCheck((List_1_t1417891359 *)L_29);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_29, (TypeValuePair_t620932390 *)L_31);
		List_1_t1417891359 * L_32 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_33 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_32);
		return L_33;
	}
}
// System.Void Zenject.Factory`2<System.Object,System.Object>::.ctor()
extern "C"  void Factory_2__ctor_m4224559918_gshared (Factory_2_t2762000298 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`2<System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_2_get_ConstructedType_m3202430105_MetadataUsageId;
extern "C"  Type_t * Factory_2_get_ConstructedType_m3202430105_gshared (Factory_2_t2762000298 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_2_get_ConstructedType_m3202430105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`2<System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_2_get_ProvidedTypes_m1772732737_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_2_get_ProvidedTypes_m1772732737_gshared (Factory_2_t2762000298 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_2_get_ProvidedTypes_m1772732737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// Zenject.DiContainer Zenject.Factory`2<System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_2_get_Container_m168715534_gshared (Factory_2_t2762000298 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`2<System.Object,System.Object>::Create(TParam1)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_2_Create_m1691441992_MetadataUsageId;
extern "C"  Il2CppObject * Factory_2_Create_m1691441992_gshared (Factory_2_t2762000298 * __this, Il2CppObject * ___param0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_2_Create_m1691441992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param0;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_6 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_5);
		return L_6;
	}
}
// System.Void Zenject.Factory`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_3__ctor_m1680892321_gshared (Factory_3_t2717473363 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`3<System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_3_get_ConstructedType_m1442327372_MetadataUsageId;
extern "C"  Type_t * Factory_3_get_ConstructedType_m1442327372_gshared (Factory_3_t2717473363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_3_get_ConstructedType_m1442327372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`3<System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_3_get_ProvidedTypes_m518722868_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_3_get_ProvidedTypes_m518722868_gshared (Factory_3_t2717473363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_3_get_ProvidedTypes_m518722868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		return L_2;
	}
}
// Zenject.DiContainer Zenject.Factory`3<System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_3_get_Container_m6330113_gshared (Factory_3_t2717473363 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`3<System.Object,System.Object,System.Object>::Create(TParam1,TParam2)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_3_Create_m3393227086_MetadataUsageId;
extern "C"  Il2CppObject * Factory_3_Create_m3393227086_gshared (Factory_3_t2717473363 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_3_Create_m3393227086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_9 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_8);
		return L_9;
	}
}
// System.Void Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_4__ctor_m582229268_gshared (Factory_4_t549530144 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_4_get_ConstructedType_m2144061183_MetadataUsageId;
extern "C"  Type_t * Factory_4_get_ConstructedType_m2144061183_gshared (Factory_4_t549530144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_4_get_ConstructedType_m2144061183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_4_get_ProvidedTypes_m915303975_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_4_get_ProvidedTypes_m915303975_gshared (Factory_4_t549530144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_4_get_ProvidedTypes_m915303975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		return L_4;
	}
}
// Zenject.DiContainer Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_4_get_Container_m1124560372_gshared (Factory_4_t549530144 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_4_Create_m626144115_MetadataUsageId;
extern "C"  Il2CppObject * Factory_4_Create_m626144115_gshared (Factory_4_t549530144 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_4_Create_m626144115_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		Il2CppObject * L_9 = ___param32;
		TypeValuePair_t620932390 * L_10 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((List_1_t1417891359 *)L_8);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_8, (TypeValuePair_t620932390 *)L_10);
		List_1_t1417891359 * L_11 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_12 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_11);
		return L_12;
	}
}
// System.Void Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_5__ctor_m1759325575_gshared (Factory_5_t2495851293 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_5_get_ConstructedType_m1911314354_MetadataUsageId;
extern "C"  Type_t * Factory_5_get_ConstructedType_m1911314354_gshared (Factory_5_t2495851293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_5_get_ConstructedType_m1911314354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_5_get_ProvidedTypes_m1262492698_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_5_get_ProvidedTypes_m1262492698_gshared (Factory_5_t2495851293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_5_get_ProvidedTypes_m1262492698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		return L_6;
	}
}
// Zenject.DiContainer Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_5_get_Container_m1453275623_gshared (Factory_5_t2495851293 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_5_Create_m3103857335_MetadataUsageId;
extern "C"  Il2CppObject * Factory_5_Create_m3103857335_gshared (Factory_5_t2495851293 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_5_Create_m3103857335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		Il2CppObject * L_9 = ___param32;
		TypeValuePair_t620932390 * L_10 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((List_1_t1417891359 *)L_8);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_8, (TypeValuePair_t620932390 *)L_10);
		List_1_t1417891359 * L_11 = V_0;
		Il2CppObject * L_12 = ___param43;
		TypeValuePair_t620932390 * L_13 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((List_1_t1417891359 *)L_11);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_11, (TypeValuePair_t620932390 *)L_13);
		List_1_t1417891359 * L_14 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_15 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_14);
		return L_15;
	}
}
// System.Void Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_6__ctor_m2403853050_gshared (Factory_6_t3697694390 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_6_get_ConstructedType_m620899685_MetadataUsageId;
extern "C"  Type_t * Factory_6_get_ConstructedType_m620899685_gshared (Factory_6_t3697694390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_6_get_ConstructedType_m620899685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_6_get_ProvidedTypes_m3267653389_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_6_get_ProvidedTypes_m3267653389_gshared (Factory_6_t3697694390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_6_get_ProvidedTypes_m3267653389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)5));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		TypeU5BU5D_t3431720054* L_8 = (TypeU5BU5D_t3431720054*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Type_t *)L_9);
		return L_8;
	}
}
// Zenject.DiContainer Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_6_get_Container_m2027703002_gshared (Factory_6_t3697694390 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_6_Create_m1562312730_MetadataUsageId;
extern "C"  Il2CppObject * Factory_6_Create_m1562312730_gshared (Factory_6_t3697694390 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_6_Create_m1562312730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		Il2CppObject * L_9 = ___param32;
		TypeValuePair_t620932390 * L_10 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((List_1_t1417891359 *)L_8);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_8, (TypeValuePair_t620932390 *)L_10);
		List_1_t1417891359 * L_11 = V_0;
		Il2CppObject * L_12 = ___param43;
		TypeValuePair_t620932390 * L_13 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((List_1_t1417891359 *)L_11);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_11, (TypeValuePair_t620932390 *)L_13);
		List_1_t1417891359 * L_14 = V_0;
		Il2CppObject * L_15 = ___param54;
		TypeValuePair_t620932390 * L_16 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((List_1_t1417891359 *)L_14);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_14, (TypeValuePair_t620932390 *)L_16);
		List_1_t1417891359 * L_17 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_18 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_17);
		return L_18;
	}
}
// System.Void Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_7__ctor_m1822985581_gshared (Factory_7_t2859822983 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_7_get_ConstructedType_m3956119576_MetadataUsageId;
extern "C"  Type_t * Factory_7_get_ConstructedType_m3956119576_gshared (Factory_7_t2859822983 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_7_get_ConstructedType_m3956119576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_7_get_ProvidedTypes_m2767697664_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_7_get_ProvidedTypes_m2767697664_gshared (Factory_7_t2859822983 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_7_get_ProvidedTypes_m2767697664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)6));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		TypeU5BU5D_t3431720054* L_8 = (TypeU5BU5D_t3431720054*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Type_t *)L_9);
		TypeU5BU5D_t3431720054* L_10 = (TypeU5BU5D_t3431720054*)L_8;
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Type_t *)L_11);
		return L_10;
	}
}
// Zenject.DiContainer Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_7_get_Container_m2005594317_gshared (Factory_7_t2859822983 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_7_Create_m2387911836_MetadataUsageId;
extern "C"  Il2CppObject * Factory_7_Create_m2387911836_gshared (Factory_7_t2859822983 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_7_Create_m2387911836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		Il2CppObject * L_9 = ___param32;
		TypeValuePair_t620932390 * L_10 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((List_1_t1417891359 *)L_8);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_8, (TypeValuePair_t620932390 *)L_10);
		List_1_t1417891359 * L_11 = V_0;
		Il2CppObject * L_12 = ___param43;
		TypeValuePair_t620932390 * L_13 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((List_1_t1417891359 *)L_11);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_11, (TypeValuePair_t620932390 *)L_13);
		List_1_t1417891359 * L_14 = V_0;
		Il2CppObject * L_15 = ___param54;
		TypeValuePair_t620932390 * L_16 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((List_1_t1417891359 *)L_14);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_14, (TypeValuePair_t620932390 *)L_16);
		List_1_t1417891359 * L_17 = V_0;
		Il2CppObject * L_18 = ___param65;
		TypeValuePair_t620932390 * L_19 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((List_1_t1417891359 *)L_17);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_17, (TypeValuePair_t620932390 *)L_19);
		List_1_t1417891359 * L_20 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_21 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_20);
		return L_21;
	}
}
// System.Void Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_8__ctor_m2899016928_gshared (Factory_8_t2931077612 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_8_get_ConstructedType_m170321867_MetadataUsageId;
extern "C"  Type_t * Factory_8_get_ConstructedType_m170321867_gshared (Factory_8_t2931077612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_8_get_ConstructedType_m170321867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_8_get_ProvidedTypes_m1926120435_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_8_get_ProvidedTypes_m1926120435_gshared (Factory_8_t2931077612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_8_get_ProvidedTypes_m1926120435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)7));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		TypeU5BU5D_t3431720054* L_8 = (TypeU5BU5D_t3431720054*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Type_t *)L_9);
		TypeU5BU5D_t3431720054* L_10 = (TypeU5BU5D_t3431720054*)L_8;
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Type_t *)L_11);
		TypeU5BU5D_t3431720054* L_12 = (TypeU5BU5D_t3431720054*)L_10;
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (Type_t *)L_13);
		return L_12;
	}
}
// Zenject.DiContainer Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_8_get_Container_m2274327488_gshared (Factory_8_t2931077612 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_8_Create_m2116143421_MetadataUsageId;
extern "C"  Il2CppObject * Factory_8_Create_m2116143421_gshared (Factory_8_t2931077612 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_8_Create_m2116143421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		Il2CppObject * L_9 = ___param32;
		TypeValuePair_t620932390 * L_10 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((List_1_t1417891359 *)L_8);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_8, (TypeValuePair_t620932390 *)L_10);
		List_1_t1417891359 * L_11 = V_0;
		Il2CppObject * L_12 = ___param43;
		TypeValuePair_t620932390 * L_13 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((List_1_t1417891359 *)L_11);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_11, (TypeValuePair_t620932390 *)L_13);
		List_1_t1417891359 * L_14 = V_0;
		Il2CppObject * L_15 = ___param54;
		TypeValuePair_t620932390 * L_16 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((List_1_t1417891359 *)L_14);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_14, (TypeValuePair_t620932390 *)L_16);
		List_1_t1417891359 * L_17 = V_0;
		Il2CppObject * L_18 = ___param65;
		TypeValuePair_t620932390 * L_19 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		NullCheck((List_1_t1417891359 *)L_17);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_17, (TypeValuePair_t620932390 *)L_19);
		List_1_t1417891359 * L_20 = V_0;
		Il2CppObject * L_21 = ___param76;
		TypeValuePair_t620932390 * L_22 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((List_1_t1417891359 *)L_20);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_20, (TypeValuePair_t620932390 *)L_22);
		List_1_t1417891359 * L_23 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_24 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_23);
		return L_24;
	}
}
// System.Void Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_9__ctor_m664076627_gshared (Factory_9_t1966270225 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_9_get_ConstructedType_m1274965118_MetadataUsageId;
extern "C"  Type_t * Factory_9_get_ConstructedType_m1274965118_gshared (Factory_9_t1966270225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_9_get_ConstructedType_m1274965118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Factory_9_get_ProvidedTypes_m4250166758_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* Factory_9_get_ProvidedTypes_m4250166758_gshared (Factory_9_t1966270225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_9_get_ProvidedTypes_m4250166758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)8));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		TypeU5BU5D_t3431720054* L_8 = (TypeU5BU5D_t3431720054*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Type_t *)L_9);
		TypeU5BU5D_t3431720054* L_10 = (TypeU5BU5D_t3431720054*)L_8;
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Type_t *)L_11);
		TypeU5BU5D_t3431720054* L_12 = (TypeU5BU5D_t3431720054*)L_10;
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (Type_t *)L_13);
		TypeU5BU5D_t3431720054* L_14 = (TypeU5BU5D_t3431720054*)L_12;
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(7), (Type_t *)L_15);
		return L_14;
	}
}
// Zenject.DiContainer Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_9_get_Container_m468073395_gshared (Factory_9_t1966270225 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// TValue Zenject.Factory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t Factory_9_Create_m584920829_MetadataUsageId;
extern "C"  Il2CppObject * Factory_9_Create_m584920829_gshared (Factory_9_t1966270225 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_9_Create_m584920829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		List_1_t1417891359 * L_1 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_1, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_1;
		List_1_t1417891359 * L_2 = V_0;
		Il2CppObject * L_3 = ___param10;
		TypeValuePair_t620932390 * L_4 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((List_1_t1417891359 *)L_2);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_2, (TypeValuePair_t620932390 *)L_4);
		List_1_t1417891359 * L_5 = V_0;
		Il2CppObject * L_6 = ___param21;
		TypeValuePair_t620932390 * L_7 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((List_1_t1417891359 *)L_5);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_5, (TypeValuePair_t620932390 *)L_7);
		List_1_t1417891359 * L_8 = V_0;
		Il2CppObject * L_9 = ___param32;
		TypeValuePair_t620932390 * L_10 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((List_1_t1417891359 *)L_8);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_8, (TypeValuePair_t620932390 *)L_10);
		List_1_t1417891359 * L_11 = V_0;
		Il2CppObject * L_12 = ___param43;
		TypeValuePair_t620932390 * L_13 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((List_1_t1417891359 *)L_11);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_11, (TypeValuePair_t620932390 *)L_13);
		List_1_t1417891359 * L_14 = V_0;
		Il2CppObject * L_15 = ___param54;
		TypeValuePair_t620932390 * L_16 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		NullCheck((List_1_t1417891359 *)L_14);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_14, (TypeValuePair_t620932390 *)L_16);
		List_1_t1417891359 * L_17 = V_0;
		Il2CppObject * L_18 = ___param65;
		TypeValuePair_t620932390 * L_19 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(NULL /*static, unused*/, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((List_1_t1417891359 *)L_17);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_17, (TypeValuePair_t620932390 *)L_19);
		List_1_t1417891359 * L_20 = V_0;
		Il2CppObject * L_21 = ___param76;
		TypeValuePair_t620932390 * L_22 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck((List_1_t1417891359 *)L_20);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_20, (TypeValuePair_t620932390 *)L_22);
		List_1_t1417891359 * L_23 = V_0;
		Il2CppObject * L_24 = ___param87;
		TypeValuePair_t620932390 * L_25 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(NULL /*static, unused*/, (Il2CppObject *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck((List_1_t1417891359 *)L_23);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_23, (TypeValuePair_t620932390 *)L_25);
		List_1_t1417891359 * L_26 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_27 = GenericVirtFuncInvoker1< Il2CppObject *, List_1_t1417891359 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (DiContainer_t2383114449 *)L_0, (List_1_t1417891359 *)L_26);
		return L_27;
	}
}
// System.Void Zenject.FactoryMethod`1<System.Object>::.ctor()
extern "C"  void FactoryMethod_1__ctor_m3264198972_gshared (FactoryMethod_1_t672889922 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.FactoryMethod`1<System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_1_get_ConstructedType_m209262631_MetadataUsageId;
extern "C"  Type_t * FactoryMethod_1_get_ConstructedType_m209262631_gshared (FactoryMethod_1_t672889922 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_1_get_ConstructedType_m209262631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.FactoryMethod`1<System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_1_get_ProvidedTypes_m3925029455_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_1_get_ProvidedTypes_m3925029455_gshared (FactoryMethod_1_t672889922 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_1_get_ProvidedTypes_m3925029455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// TValue Zenject.FactoryMethod`1<System.Object>::Create()
extern "C"  Il2CppObject * FactoryMethod_1_Create_m3635440868_gshared (FactoryMethod_1_t672889922 * __this, const MethodInfo* method)
{
	{
		Func_2_t1206216135 * L_0 = (Func_2_t1206216135 *)__this->get__method_1();
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)__this->get__container_0();
		NullCheck((Func_2_t1206216135 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t1206216135 *, DiContainer_t2383114449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_2_t1206216135 *)L_0, (DiContainer_t2383114449 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}
}
// System.Void Zenject.FactoryMethod`2<System.Object,System.Object>::.ctor()
extern "C"  void FactoryMethod_2__ctor_m1426588911_gshared (FactoryMethod_2_t1984661133 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.FactoryMethod`2<System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_2_get_ConstructedType_m2813654042_MetadataUsageId;
extern "C"  Type_t * FactoryMethod_2_get_ConstructedType_m2813654042_gshared (FactoryMethod_2_t1984661133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_2_get_ConstructedType_m2813654042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.FactoryMethod`2<System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_2_get_ProvidedTypes_m2770746242_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_2_get_ProvidedTypes_m2770746242_gshared (FactoryMethod_2_t1984661133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_2_get_ProvidedTypes_m2770746242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// TValue Zenject.FactoryMethod`2<System.Object,System.Object>::Create(TParam1)
extern "C"  Il2CppObject * FactoryMethod_2_Create_m3412655113_gshared (FactoryMethod_2_t1984661133 * __this, Il2CppObject * ___param0, const MethodInfo* method)
{
	{
		Func_3_t1309472546 * L_0 = (Func_3_t1309472546 *)__this->get__method_1();
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)__this->get__container_0();
		Il2CppObject * L_2 = ___param0;
		NullCheck((Func_3_t1309472546 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_3_t1309472546 *, DiContainer_t2383114449 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t1309472546 *)L_0, (DiContainer_t2383114449 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_3;
	}
}
// System.Void Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void FactoryMethod_3__ctor_m373691810_gshared (FactoryMethod_3_t1233407108 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_3_get_ConstructedType_m3445765389_MetadataUsageId;
extern "C"  Type_t * FactoryMethod_3_get_ConstructedType_m3445765389_gshared (FactoryMethod_3_t1233407108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_3_get_ConstructedType_m3445765389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_3_get_ProvidedTypes_m4072671669_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_3_get_ProvidedTypes_m4072671669_gshared (FactoryMethod_3_t1233407108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_3_get_ProvidedTypes_m4072671669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		return L_2;
	}
}
// TValue Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>::Create(TParam1,TParam2)
extern "C"  Il2CppObject * FactoryMethod_3_Create_m79999823_gshared (FactoryMethod_3_t1233407108 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, const MethodInfo* method)
{
	{
		Func_4_t2229882165 * L_0 = (Func_4_t2229882165 *)__this->get__method_1();
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)__this->get__container_0();
		Il2CppObject * L_2 = ___param10;
		Il2CppObject * L_3 = ___param21;
		NullCheck((Func_4_t2229882165 *)L_0);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_4_t2229882165 *, DiContainer_t2383114449 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_4_t2229882165 *)L_0, (DiContainer_t2383114449 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_4;
	}
}
// System.Void Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void FactoryMethod_4__ctor_m2895788885_gshared (FactoryMethod_4_t1102797835 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_4_get_ConstructedType_m719137536_MetadataUsageId;
extern "C"  Type_t * FactoryMethod_4_get_ConstructedType_m719137536_gshared (FactoryMethod_4_t1102797835 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_4_get_ConstructedType_m719137536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_4_get_ProvidedTypes_m809037032_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_4_get_ProvidedTypes_m809037032_gshared (FactoryMethod_4_t1102797835 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_4_get_ProvidedTypes_m809037032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		return L_4;
	}
}
// TValue Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3)
extern "C"  Il2CppObject * FactoryMethod_4_Create_m1378645684_gshared (FactoryMethod_4_t1102797835 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, const MethodInfo* method)
{
	{
		Func_5_t936310804 * L_0 = (Func_5_t936310804 *)__this->get__method_1();
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)__this->get__container_0();
		Il2CppObject * L_2 = ___param10;
		Il2CppObject * L_3 = ___param21;
		Il2CppObject * L_4 = ___param32;
		NullCheck((Func_5_t936310804 *)L_0);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_5_t936310804 *, DiContainer_t2383114449 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_5_t936310804 *)L_0, (DiContainer_t2383114449 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_5;
	}
}
// System.Void Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void FactoryMethod_5__ctor_m795657736_gshared (FactoryMethod_5_t2447447334 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_5_get_ConstructedType_m4204406259_MetadataUsageId;
extern "C"  Type_t * FactoryMethod_5_get_ConstructedType_m4204406259_gshared (FactoryMethod_5_t2447447334 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_5_get_ConstructedType_m4204406259_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type[] Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t FactoryMethod_5_get_ProvidedTypes_m3491837211_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_5_get_ProvidedTypes_m3491837211_gshared (FactoryMethod_5_t2447447334 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryMethod_5_get_ProvidedTypes_m3491837211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		return L_6;
	}
}
// TValue Zenject.FactoryMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4)
extern "C"  Il2CppObject * FactoryMethod_5_Create_m940508984_gshared (FactoryMethod_5_t2447447334 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, const MethodInfo* method)
{
	{
		Func_6_t3355333698 * L_0 = (Func_6_t3355333698 *)__this->get__method_1();
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)__this->get__container_0();
		Il2CppObject * L_2 = ___param10;
		Il2CppObject * L_3 = ___param21;
		Il2CppObject * L_4 = ___param32;
		Il2CppObject * L_5 = ___param43;
		NullCheck((Func_6_t3355333698 *)L_0);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Func_6_t3355333698 *, DiContainer_t2383114449 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_6_t3355333698 *)L_0, (DiContainer_t2383114449 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_6;
	}
}
// System.Void Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::.ctor()
extern "C"  void U3CValidateU3Ec__Iterator45__ctor_m3732071676_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.ZenjectResolveException Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateU3Ec__Iterator45_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m2958685317_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method)
{
	{
		ZenjectResolveException_t1201052999 * L_0 = (ZenjectResolveException_t1201052999 *)__this->get_U24current_1();
		return L_0;
	}
}
// System.Object Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateU3Ec__Iterator45_System_Collections_IEnumerator_get_Current_m1332034346_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method)
{
	{
		ZenjectResolveException_t1201052999 * L_0 = (ZenjectResolveException_t1201052999 *)__this->get_U24current_1();
		return L_0;
	}
}
// System.Collections.IEnumerator Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateU3Ec__Iterator45_System_Collections_IEnumerable_GetEnumerator_m831256805_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CValidateU3Ec__Iterator45_t1669021519 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((U3CValidateU3Ec__Iterator45_t1669021519 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateU3Ec__Iterator45_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m2937777190_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_0();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CValidateU3Ec__Iterator45_t1669021519 * L_2 = (U3CValidateU3Ec__Iterator45_t1669021519 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_2;
	}
}
// System.Boolean Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::MoveNext()
extern "C"  bool U3CValidateU3Ec__Iterator45_MoveNext_m2737589880_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_0();
		__this->set_U24PC_0((-1));
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0017;
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Void Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::Dispose()
extern "C"  void U3CValidateU3Ec__Iterator45_Dispose_m121471545_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CValidateU3Ec__Iterator45_Reset_m1378504617_MetadataUsageId;
extern "C"  void U3CValidateU3Ec__Iterator45_Reset_m1378504617_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CValidateU3Ec__Iterator45_Reset_m1378504617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Zenject.FactoryMethodUntyped`1<System.Object>::.ctor(Zenject.DiContainer,System.Func`3<Zenject.DiContainer,System.Object[],TContract>)
extern "C"  void FactoryMethodUntyped_1__ctor_m4245491325_gshared (FactoryMethodUntyped_1_t1598907365 * __this, DiContainer_t2383114449 * ___container0, Func_3_t2319412757 * ___method1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_0(L_0);
		Func_3_t2319412757 * L_1 = ___method1;
		__this->set__method_1(L_1);
		return;
	}
}
// TContract Zenject.FactoryMethodUntyped`1<System.Object>::Create(System.Object[])
extern "C"  Il2CppObject * FactoryMethodUntyped_1_Create_m2411369028_gshared (FactoryMethodUntyped_1_t1598907365 * __this, ObjectU5BU5D_t11523773* ___constructorArgs0, const MethodInfo* method)
{
	{
		Func_3_t2319412757 * L_0 = (Func_3_t2319412757 *)__this->get__method_1();
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)__this->get__container_0();
		ObjectU5BU5D_t11523773* L_2 = ___constructorArgs0;
		NullCheck((Func_3_t2319412757 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_3_t2319412757 *, DiContainer_t2383114449 *, ObjectU5BU5D_t11523773*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_3_t2319412757 *)L_0, (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.FactoryMethodUntyped`1<System.Object>::Validate(System.Type[])
extern "C"  Il2CppObject* FactoryMethodUntyped_1_Validate_m65064609_gshared (FactoryMethodUntyped_1_t1598907365 * __this, TypeU5BU5D_t3431720054* ___extras0, const MethodInfo* method)
{
	U3CValidateU3Ec__Iterator45_t1669021519 * V_0 = NULL;
	{
		U3CValidateU3Ec__Iterator45_t1669021519 * L_0 = (U3CValidateU3Ec__Iterator45_t1669021519 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (U3CValidateU3Ec__Iterator45_t1669021519 *)L_0;
		U3CValidateU3Ec__Iterator45_t1669021519 * L_1 = V_0;
		U3CValidateU3Ec__Iterator45_t1669021519 * L_2 = (U3CValidateU3Ec__Iterator45_t1669021519 *)L_1;
		NullCheck(L_2);
		L_2->set_U24PC_0(((int32_t)-2));
		return L_2;
	}
}
// System.Void Zenject.FactoryNested`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`9<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TConcrete>)
extern "C"  void FactoryNested_10__ctor_m1934166616_gshared (FactoryNested_10_t2299834384 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8)
extern "C"  Il2CppObject * FactoryNested_10_Create_m1507100581_gshared (FactoryNested_10_t2299834384 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		Il2CppObject * L_3 = ___param32;
		Il2CppObject * L_4 = ___param43;
		Il2CppObject * L_5 = ___param54;
		Il2CppObject * L_6 = ___param65;
		Il2CppObject * L_7 = ___param76;
		Il2CppObject * L_8 = ___param87;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_9 = InterfaceFuncInvoker8< Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8);
		return ((Il2CppObject *)Castclass(L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`10<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TConcrete>)
extern "C"  void FactoryNested_11__ctor_m2844431087_gshared (FactoryNested_11_t14697077 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9)
extern "C"  Il2CppObject * FactoryNested_11_Create_m1736997828_gshared (FactoryNested_11_t14697077 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, Il2CppObject * ___param98, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		Il2CppObject * L_3 = ___param32;
		Il2CppObject * L_4 = ___param43;
		Il2CppObject * L_5 = ___param54;
		Il2CppObject * L_6 = ___param65;
		Il2CppObject * L_7 = ___param76;
		Il2CppObject * L_8 = ___param87;
		Il2CppObject * L_9 = ___param98;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_10 = InterfaceFuncInvoker9< Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		return ((Il2CppObject *)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`12<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`11<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10,TConcrete>)
extern "C"  void FactoryNested_12__ctor_m3724480721_gshared (FactoryNested_12_t4045899478 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`12<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10)
extern "C"  Il2CppObject * FactoryNested_12_Create_m172551287_gshared (FactoryNested_12_t4045899478 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, Il2CppObject * ___param98, Il2CppObject * ___param109, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		Il2CppObject * L_3 = ___param32;
		Il2CppObject * L_4 = ___param43;
		Il2CppObject * L_5 = ___param54;
		Il2CppObject * L_6 = ___param65;
		Il2CppObject * L_7 = ___param76;
		Il2CppObject * L_8 = ___param87;
		Il2CppObject * L_9 = ___param98;
		Il2CppObject * L_10 = ___param109;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_11 = InterfaceFuncInvoker10< Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		return ((Il2CppObject *)Castclass(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`2<System.Object,System.Object>::.ctor(Zenject.IFactory`1<TConcrete>)
extern "C"  void FactoryNested_2__ctor_m1422373693_gshared (FactoryNested_2_t2234585871 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`2<System.Object,System.Object>::Create()
extern "C"  Il2CppObject * FactoryNested_2_Create_m3988050152_gshared (FactoryNested_2_t2234585871 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`1<System.Object>::Create() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`3<System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`2<TParam1,TConcrete>)
extern "C"  void FactoryNested_3__ctor_m1830744183_gshared (FactoryNested_3_t3116483514 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`3<System.Object,System.Object,System.Object>::Create(TParam1)
extern "C"  Il2CppObject * FactoryNested_3_Create_m1558019269_gshared (FactoryNested_3_t3116483514 * __this, Il2CppObject * ___param0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`2<System.Object,System.Object>::Create(TParam1) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`4<System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`3<TParam1,TParam2,TConcrete>)
extern "C"  void FactoryNested_4__ctor_m202307378_gshared (FactoryNested_4_t2122089277 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2)
extern "C"  Il2CppObject * FactoryNested_4_Create_m1181747531_gshared (FactoryNested_4_t2122089277 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_3 = InterfaceFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`3<System.Object,System.Object,System.Object>::Create(TParam1,TParam2) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		return ((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`4<TParam1,TParam2,TParam3,TConcrete>)
extern "C"  void FactoryNested_5__ctor_m755263086_gshared (FactoryNested_5_t2227126508 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3)
extern "C"  Il2CppObject * FactoryNested_5_Create_m4020896240_gshared (FactoryNested_5_t2227126508 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		Il2CppObject * L_3 = ___param32;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker3< Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3);
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TConcrete>)
extern "C"  void FactoryNested_6__ctor_m2385829163_gshared (FactoryNested_6_t1301445835 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4)
extern "C"  Il2CppObject * FactoryNested_6_Create_m4214630324_gshared (FactoryNested_6_t1301445835 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		Il2CppObject * L_3 = ___param32;
		Il2CppObject * L_4 = ___param43;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_5 = InterfaceFuncInvoker4< Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		return ((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`6<TParam1,TParam2,TParam3,TParam4,TParam5,TConcrete>)
extern "C"  void FactoryNested_7__ctor_m2694773353_gshared (FactoryNested_7_t3889515902 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5)
extern "C"  Il2CppObject * FactoryNested_7_Create_m3784719255_gshared (FactoryNested_7_t3889515902 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		Il2CppObject * L_3 = ___param32;
		Il2CppObject * L_4 = ___param43;
		Il2CppObject * L_5 = ___param54;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_6 = InterfaceFuncInvoker5< Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return ((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`7<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TConcrete>)
extern "C"  void FactoryNested_8__ctor_m2013945128_gshared (FactoryNested_8_t2707677753 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6)
extern "C"  Il2CppObject * FactoryNested_8_Create_m2166554265_gshared (FactoryNested_8_t2707677753 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		Il2CppObject * L_3 = ___param32;
		Il2CppObject * L_4 = ___param43;
		Il2CppObject * L_5 = ___param54;
		Il2CppObject * L_6 = ___param65;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_7 = InterfaceFuncInvoker6< Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
		return ((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryNested`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`8<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TConcrete>)
extern "C"  void FactoryNested_9__ctor_m3137840232_gshared (FactoryNested_9_t2228482800 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___concreteFactory0;
		__this->set__concreteFactory_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryNested`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7)
extern "C"  Il2CppObject * FactoryNested_9_Create_m2920034746_gshared (FactoryNested_9_t2228482800 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__concreteFactory_0();
		Il2CppObject * L_1 = ___param10;
		Il2CppObject * L_2 = ___param21;
		Il2CppObject * L_3 = ___param32;
		Il2CppObject * L_4 = ___param43;
		Il2CppObject * L_5 = ___param54;
		Il2CppObject * L_6 = ___param65;
		Il2CppObject * L_7 = ___param76;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_8 = InterfaceFuncInvoker7< Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(0 /* T Zenject.IFactory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		return ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Void Zenject.FactoryUntyped`1<System.Object>::.ctor()
extern "C"  void FactoryUntyped_1__ctor_m3424962022_gshared (FactoryUntyped_1_t2760421534 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.DiContainer Zenject.FactoryUntyped`1<System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * FactoryUntyped_1_get_Container_m425604402_gshared (FactoryUntyped_1_t2760421534 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// System.Void Zenject.FactoryUntyped`1<System.Object>::set_Container(Zenject.DiContainer)
extern "C"  void FactoryUntyped_1_set_Container_m2834492365_gshared (FactoryUntyped_1_t2760421534 * __this, DiContainer_t2383114449 * ___value0, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = ___value0;
		__this->set__container_0(L_0);
		return;
	}
}
// System.Type Zenject.FactoryUntyped`1<System.Object>::get_ConcreteType()
extern "C"  Type_t * FactoryUntyped_1_get_ConcreteType_m3473729388_gshared (FactoryUntyped_1_t2760421534 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = (Type_t *)__this->get__concreteType_1();
		return L_0;
	}
}
// System.Void Zenject.FactoryUntyped`1<System.Object>::set_ConcreteType(System.Type)
extern "C"  void FactoryUntyped_1_set_ConcreteType_m3693397759_gshared (FactoryUntyped_1_t2760421534 * __this, Type_t * ___value0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set__concreteType_1(L_0);
		return;
	}
}
// System.Void Zenject.FactoryUntyped`1<System.Object>::Initialize()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectResolveException_t1201052999_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3721310013;
extern const uint32_t FactoryUntyped_1_Initialize_m301289582_MetadataUsageId;
extern "C"  void FactoryUntyped_1_Initialize_m301289582_gshared (FactoryUntyped_1_t2760421534 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FactoryUntyped_1_Initialize_m301289582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = (Type_t *)__this->get__concreteType_1();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		__this->set__concreteType_1(L_1);
	}

IL_001b:
	{
		Type_t * L_2 = (Type_t *)__this->get__concreteType_1();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_4 = TypeExtensions_DerivesFromOrEqual_m3526502226(NULL /*static, unused*/, (Type_t *)L_2, (Type_t *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_006b;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		Type_t * L_6 = (Type_t *)__this->get__concreteType_1();
		String_t* L_7 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_5;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_10 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_10);
		String_t* L_11 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral3721310013, (ObjectU5BU5D_t11523773*)L_8, /*hidden argument*/NULL);
		ZenjectResolveException_t1201052999 * L_12 = (ZenjectResolveException_t1201052999 *)il2cpp_codegen_object_new(ZenjectResolveException_t1201052999_il2cpp_TypeInfo_var);
		ZenjectResolveException__ctor_m3382034954(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_006b:
	{
		return;
	}
}
// TContract Zenject.FactoryUntyped`1<System.Object>::Create(System.Object[])
extern "C"  Il2CppObject * FactoryUntyped_1_Create_m2845155493_gshared (FactoryUntyped_1_t2760421534 * __this, ObjectU5BU5D_t11523773* ___constructorArgs0, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		Type_t * L_1 = (Type_t *)__this->get__concreteType_1();
		ObjectU5BU5D_t11523773* L_2 = ___constructorArgs0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_3 = VirtFuncInvoker2< Il2CppObject *, Type_t *, ObjectU5BU5D_t11523773* >::Invoke(32 /* System.Object Zenject.DiContainer::Instantiate(System.Type,System.Object[]) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return ((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.FactoryUntyped`1<System.Object>::Validate(System.Type[])
extern "C"  Il2CppObject* FactoryUntyped_1_Validate_m1573356162_gshared (FactoryUntyped_1_t2760421534 * __this, TypeU5BU5D_t3431720054* ___extras0, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		Type_t * L_1 = (Type_t *)__this->get__concreteType_1();
		TypeU5BU5D_t3431720054* L_2 = ___extras0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_3 = DiContainer_ValidateObjectGraph_m579741446((DiContainer_t2383114449 *)L_0, (Type_t *)L_1, (TypeU5BU5D_t3431720054*)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Zenject.FactoryUntyped`2<System.Object,System.Object>::.ctor()
extern "C"  void FactoryUntyped_2__ctor_m1787600921_gshared (FactoryUntyped_2_t884050241 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.DiContainer Zenject.FactoryUntyped`2<System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * FactoryUntyped_2_get_Container_m1034149349_gshared (FactoryUntyped_2_t884050241 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// System.Void Zenject.FactoryUntyped`2<System.Object,System.Object>::set_Container(Zenject.DiContainer)
extern "C"  void FactoryUntyped_2_set_Container_m1008159162_gshared (FactoryUntyped_2_t884050241 * __this, DiContainer_t2383114449 * ___value0, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = ___value0;
		__this->set__container_0(L_0);
		return;
	}
}
// TContract Zenject.FactoryUntyped`2<System.Object,System.Object>::Create(System.Object[])
extern "C"  Il2CppObject * FactoryUntyped_2_Create_m3733682712_gshared (FactoryUntyped_2_t884050241 * __this, ObjectU5BU5D_t11523773* ___constructorArgs0, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		ObjectU5BU5D_t11523773* L_1 = ___constructorArgs0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_2 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_0, (ObjectU5BU5D_t11523773*)L_1);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.FactoryUntyped`2<System.Object,System.Object>::Validate(System.Type[])
extern "C"  Il2CppObject* FactoryUntyped_2_Validate_m1558660213_gshared (FactoryUntyped_2_t884050241 * __this, TypeU5BU5D_t3431720054* ___extras0, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = ___extras0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_2;
	}
}
// System.Void Zenject.GameObjectFactory`1<System.Object>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern const uint32_t GameObjectFactory_1__ctor_m100447914_MetadataUsageId;
extern "C"  void GameObjectFactory_1__ctor_m100447914_gshared (GameObjectFactory_1_t2417718272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_1__ctor_m100447914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((GameObjectFactory_t3309736270 *)__this);
		GameObjectFactory__ctor_m1998996753((GameObjectFactory_t3309736270 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_1, /*hidden argument*/NULL);
		return;
	}
}
// TValue Zenject.GameObjectFactory`1<System.Object>::Create()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_1_Create_m1300499766_MetadataUsageId;
extern "C"  Il2CppObject * GameObjectFactory_1_Create_m1300499766_gshared (GameObjectFactory_1_t2417718272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_1_Create_m1300499766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = (GameObject_t4012695102 *)((GameObjectFactory_t3309736270 *)__this)->get__prefab_1();
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_3 = VirtFuncInvoker3< Il2CppObject *, Type_t *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(45 /* System.Object Zenject.DiContainer::InstantiatePrefabForComponent(System.Type,UnityEngine.GameObject,System.Object[]) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_1, (GameObject_t4012695102 *)L_2, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		return ((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`1<System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_1_Validate_m335307919_MetadataUsageId;
extern "C"  Il2CppObject* GameObjectFactory_1_Validate_m335307919_gshared (GameObjectFactory_1_t2417718272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_1_Validate_m335307919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
// System.Void Zenject.GameObjectFactory`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern const uint32_t GameObjectFactory_2__ctor_m2680023517_MetadataUsageId;
extern "C"  void GameObjectFactory_2__ctor_m2680023517_gshared (GameObjectFactory_2_t546746903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_2__ctor_m2680023517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((GameObjectFactory_t3309736270 *)__this);
		GameObjectFactory__ctor_m1998996753((GameObjectFactory_t3309736270 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_1, /*hidden argument*/NULL);
		return;
	}
}
// TValue Zenject.GameObjectFactory`2<System.Object,System.Object>::Create(TParam1)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_2_Create_m1408180983_MetadataUsageId;
extern "C"  Il2CppObject * GameObjectFactory_2_Create_m1408180983_gshared (GameObjectFactory_2_t546746903 * __this, Il2CppObject * ___param0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_2_Create_m1408180983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = (GameObject_t4012695102 *)((GameObjectFactory_t3309736270 *)__this)->get__prefab_1();
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_4 = ___param0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_5 = VirtFuncInvoker3< Il2CppObject *, Type_t *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(45 /* System.Object Zenject.DiContainer::InstantiatePrefabForComponent(System.Type,UnityEngine.GameObject,System.Object[]) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_1, (GameObject_t4012695102 *)L_2, (ObjectU5BU5D_t11523773*)L_3);
		return ((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`2<System.Object,System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_2_Validate_m794844732_MetadataUsageId;
extern "C"  Il2CppObject* GameObjectFactory_2_Validate_m794844732_gshared (GameObjectFactory_2_t546746903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_2_Validate_m794844732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_3 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_3;
	}
}
// System.Void Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern const uint32_t GameObjectFactory_3__ctor_m4107911696_MetadataUsageId;
extern "C"  void GameObjectFactory_3__ctor_m4107911696_gshared (GameObjectFactory_3_t639885202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_3__ctor_m4107911696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((GameObjectFactory_t3309736270 *)__this);
		GameObjectFactory__ctor_m1998996753((GameObjectFactory_t3309736270 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_1, /*hidden argument*/NULL);
		return;
	}
}
// TValue Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>::Create(TParam1,TParam2)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_3_Create_m463918013_MetadataUsageId;
extern "C"  Il2CppObject * GameObjectFactory_3_Create_m463918013_gshared (GameObjectFactory_3_t639885202 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_3_Create_m463918013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = (GameObject_t4012695102 *)((GameObjectFactory_t3309736270 *)__this)->get__prefab_1();
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_4 = ___param10;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)L_3;
		Il2CppObject * L_6 = ___param21;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_7 = VirtFuncInvoker3< Il2CppObject *, Type_t *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(45 /* System.Object Zenject.DiContainer::InstantiatePrefabForComponent(System.Type,UnityEngine.GameObject,System.Object[]) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_1, (GameObject_t4012695102 *)L_2, (ObjectU5BU5D_t11523773*)L_5);
		return ((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_3_Validate_m1056211689_MetadataUsageId;
extern "C"  Il2CppObject* GameObjectFactory_3_Validate_m1056211689_gshared (GameObjectFactory_3_t639885202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_3_Validate_m1056211689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		TypeU5BU5D_t3431720054* L_3 = (TypeU5BU5D_t3431720054*)L_1;
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_4);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_5 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// System.Void Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern const uint32_t GameObjectFactory_4__ctor_m362319683_MetadataUsageId;
extern "C"  void GameObjectFactory_4__ctor_m362319683_gshared (GameObjectFactory_4_t486081797 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_4__ctor_m362319683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((GameObjectFactory_t3309736270 *)__this);
		GameObjectFactory__ctor_m1998996753((GameObjectFactory_t3309736270 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_1, /*hidden argument*/NULL);
		return;
	}
}
// TValue Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_4_Create_m1208381602_MetadataUsageId;
extern "C"  Il2CppObject * GameObjectFactory_4_Create_m1208381602_gshared (GameObjectFactory_4_t486081797 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_4_Create_m1208381602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = (GameObject_t4012695102 *)((GameObjectFactory_t3309736270 *)__this)->get__prefab_1();
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3));
		Il2CppObject * L_4 = ___param10;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)L_3;
		Il2CppObject * L_6 = ___param21;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)L_5;
		Il2CppObject * L_8 = ___param32;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_8);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_9 = VirtFuncInvoker3< Il2CppObject *, Type_t *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(45 /* System.Object Zenject.DiContainer::InstantiatePrefabForComponent(System.Type,UnityEngine.GameObject,System.Object[]) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_1, (GameObject_t4012695102 *)L_2, (ObjectU5BU5D_t11523773*)L_7);
		return ((Il2CppObject *)Castclass(L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`4<System.Object,System.Object,System.Object,System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_4_Validate_m1198162582_MetadataUsageId;
extern "C"  Il2CppObject* GameObjectFactory_4_Validate_m1198162582_gshared (GameObjectFactory_4_t486081797 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_4_Validate_m1198162582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		TypeU5BU5D_t3431720054* L_3 = (TypeU5BU5D_t3431720054*)L_1;
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_4);
		TypeU5BU5D_t3431720054* L_5 = (TypeU5BU5D_t3431720054*)L_3;
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_6);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_7 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_7;
	}
}
// System.Void Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern const uint32_t GameObjectFactory_5__ctor_m2975506806_MetadataUsageId;
extern "C"  void GameObjectFactory_5__ctor_m2975506806_gshared (GameObjectFactory_5_t2159204100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_5__ctor_m2975506806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((GameObjectFactory_t3309736270 *)__this);
		GameObjectFactory__ctor_m1998996753((GameObjectFactory_t3309736270 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_1, /*hidden argument*/NULL);
		return;
	}
}
// TValue Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_5_Create_m2371140262_MetadataUsageId;
extern "C"  Il2CppObject * GameObjectFactory_5_Create_m2371140262_gshared (GameObjectFactory_5_t2159204100 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_5_Create_m2371140262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = (GameObject_t4012695102 *)((GameObjectFactory_t3309736270 *)__this)->get__prefab_1();
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		Il2CppObject * L_4 = ___param10;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)L_3;
		Il2CppObject * L_6 = ___param21;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)L_5;
		Il2CppObject * L_8 = ___param32;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_8);
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)L_7;
		Il2CppObject * L_10 = ___param43;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_11 = VirtFuncInvoker3< Il2CppObject *, Type_t *, GameObject_t4012695102 *, ObjectU5BU5D_t11523773* >::Invoke(45 /* System.Object Zenject.DiContainer::InstantiatePrefabForComponent(System.Type,UnityEngine.GameObject,System.Object[]) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_1, (GameObject_t4012695102 *)L_2, (ObjectU5BU5D_t11523773*)L_9);
		return ((Il2CppObject *)Castclass(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)));
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Validate()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectFactory_5_Validate_m912002371_MetadataUsageId;
extern "C"  Il2CppObject* GameObjectFactory_5_Validate_m912002371_gshared (GameObjectFactory_5_t2159204100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectFactory_5_Validate_m912002371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)((GameObjectFactory_t3309736270 *)__this)->get__container_0();
		TypeU5BU5D_t3431720054* L_1 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		TypeU5BU5D_t3431720054* L_3 = (TypeU5BU5D_t3431720054*)L_1;
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_4);
		TypeU5BU5D_t3431720054* L_5 = (TypeU5BU5D_t3431720054*)L_3;
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_6);
		TypeU5BU5D_t3431720054* L_7 = (TypeU5BU5D_t3431720054*)L_5;
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)), /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_8);
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject* L_9 = ((  Il2CppObject* (*) (DiContainer_t2383114449 *, TypeU5BU5D_t3431720054*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DiContainer_t2383114449 *)L_0, (TypeU5BU5D_t3431720054*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return L_9;
	}
}
// System.Void Zenject.IFactoryBinder`1/<ToInstance>c__AnonStorey173<System.Object>::.ctor()
extern "C"  void U3CToInstanceU3Ec__AnonStorey173__ctor_m879505835_gshared (U3CToInstanceU3Ec__AnonStorey173_t3921245137 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TContract Zenject.IFactoryBinder`1/<ToInstance>c__AnonStorey173<System.Object>::<>m__293(Zenject.DiContainer)
extern "C"  Il2CppObject * U3CToInstanceU3Ec__AnonStorey173_U3CU3Em__293_m3871525710_gshared (U3CToInstanceU3Ec__AnonStorey173_t3921245137 * __this, DiContainer_t2383114449 * ___c0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_instance_0();
		return L_0;
	}
}
// System.Void Zenject.IFactoryBinder`1/<ToMethod>c__AnonStorey174<System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey174__ctor_m1094580512_gshared (U3CToMethodU3Ec__AnonStorey174_t1867793022 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1/<ToMethod>c__AnonStorey174<System.Object>::<>m__294(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToMethodU3Ec__AnonStorey174_U3CU3Em__294_m226769437_MetadataUsageId;
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey174_U3CU3Em__294_m226769437_gshared (U3CToMethodU3Ec__AnonStorey174_t1867793022 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToMethodU3Ec__AnonStorey174_U3CU3Em__294_m226769437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Func_2_t1206216135 * L_3 = (Func_2_t1206216135 *)__this->get_method_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		FactoryMethod_1_t672889922 * L_4 = GenericVirtFuncInvoker1< FactoryMethod_1_t672889922 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`1/<ToPrefab>c__AnonStorey175<System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey175__ctor_m1665939678_gshared (U3CToPrefabU3Ec__AnonStorey175_t1205708684 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1/<ToPrefab>c__AnonStorey175<System.Object>::<>m__297(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToPrefabU3Ec__AnonStorey175_U3CU3Em__297_m1482902648_MetadataUsageId;
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey175_U3CU3Em__297_m1482902648_gshared (U3CToPrefabU3Ec__AnonStorey175_t1205708684 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToPrefabU3Ec__AnonStorey175_U3CU3Em__297_m1482902648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t4012695102 * L_3 = (GameObject_t4012695102 *)__this->get_prefab_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		GameObjectFactory_1_t2417718272 * L_4 = GenericVirtFuncInvoker1< GameObjectFactory_1_t2417718272 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`1<System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_1__ctor_m1944281127_gshared (IFactoryBinder_1_t1064970850 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_0(L_0);
		String_t* L_1 = ___identifier1;
		__this->set__identifier_1(L_1);
		return;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToInstance(TContract)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToInstance_m20710155_gshared (IFactoryBinder_1_t1064970850 * __this, Il2CppObject * ___instance0, const MethodInfo* method)
{
	U3CToInstanceU3Ec__AnonStorey173_t3921245137 * V_0 = NULL;
	{
		U3CToInstanceU3Ec__AnonStorey173_t3921245137 * L_0 = (U3CToInstanceU3Ec__AnonStorey173_t3921245137 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToInstanceU3Ec__AnonStorey173_t3921245137 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToInstanceU3Ec__AnonStorey173_t3921245137 *)L_0;
		U3CToInstanceU3Ec__AnonStorey173_t3921245137 * L_1 = V_0;
		Il2CppObject * L_2 = ___instance0;
		NullCheck(L_1);
		L_1->set_instance_0(L_2);
		U3CToInstanceU3Ec__AnonStorey173_t3921245137 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Func_2_t1206216135 * L_5 = (Func_2_t1206216135 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Func_2_t1206216135 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((IFactoryBinder_1_t1064970850 *)__this);
		BindingConditionSetter_t259147722 * L_6 = ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_1_t1064970850 *, Func_2_t1206216135 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((IFactoryBinder_1_t1064970850 *)__this, (Func_2_t1206216135 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_6;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToMethod(System.Func`2<Zenject.DiContainer,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToMethod_m1972193855_gshared (IFactoryBinder_1_t1064970850 * __this, Func_2_t1206216135 * ___method0, const MethodInfo* method)
{
	U3CToMethodU3Ec__AnonStorey174_t1867793022 * V_0 = NULL;
	{
		U3CToMethodU3Ec__AnonStorey174_t1867793022 * L_0 = (U3CToMethodU3Ec__AnonStorey174_t1867793022 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (U3CToMethodU3Ec__AnonStorey174_t1867793022 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		V_0 = (U3CToMethodU3Ec__AnonStorey174_t1867793022 *)L_0;
		U3CToMethodU3Ec__AnonStorey174_t1867793022 * L_1 = V_0;
		Func_2_t1206216135 * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		DiContainer_t2383114449 * L_3 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_4 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_3);
		BinderGeneric_1_t1807883816 * L_5 = GenericVirtFuncInvoker1< BinderGeneric_1_t1807883816 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_3, (String_t*)L_4);
		U3CToMethodU3Ec__AnonStorey174_t1867793022 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Func_2_t3908423697 * L_8 = (Func_2_t3908423697 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Func_2_t3908423697 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((BinderGeneric_1_t1807883816 *)L_5);
		BindingConditionSetter_t259147722 * L_9 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1807883816 *, Func_2_t3908423697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((BinderGeneric_1_t1807883816 *)L_5, (Func_2_t3908423697 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_9;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToFactory()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1577427421;
extern const uint32_t IFactoryBinder_1_ToFactory_m555729664_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToFactory_m555729664_gshared (IFactoryBinder_1_t1064970850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_1_ToFactory_m555729664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsAbstract() */, (Type_t *)L_0);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		String_t* L_4 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), (String_t*)_stringLiteral1577427421, (ObjectU5BU5D_t11523773*)L_2, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_5 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_6 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_5);
		BinderGeneric_1_t1807883816 * L_7 = GenericVirtFuncInvoker1< BinderGeneric_1_t1807883816 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_5, (String_t*)L_6);
		NullCheck((BinderGeneric_1_t1807883816 *)L_7);
		BindingConditionSetter_t259147722 * L_8 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1807883816 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((BinderGeneric_1_t1807883816 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return L_8;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`1<System.Object>::ToPrefab(UnityEngine.GameObject)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectBindException_t3550319704_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3661015907;
extern const uint32_t IFactoryBinder_1_ToPrefab_m3306141416_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_1_ToPrefab_m3306141416_gshared (IFactoryBinder_1_t1064970850 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_1_ToPrefab_m3306141416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToPrefabU3Ec__AnonStorey175_t1205708684 * V_0 = NULL;
	{
		U3CToPrefabU3Ec__AnonStorey175_t1205708684 * L_0 = (U3CToPrefabU3Ec__AnonStorey175_t1205708684 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (U3CToPrefabU3Ec__AnonStorey175_t1205708684 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_0 = (U3CToPrefabU3Ec__AnonStorey175_t1205708684 *)L_0;
		U3CToPrefabU3Ec__AnonStorey175_t1205708684 * L_1 = V_0;
		GameObject_t4012695102 * L_2 = ___prefab0;
		NullCheck(L_1);
		L_1->set_prefab_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		bool L_4 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_4, /*hidden argument*/NULL);
		U3CToPrefabU3Ec__AnonStorey175_t1205708684 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t4012695102 * L_6 = (GameObject_t4012695102 *)L_5->get_prefab_0();
		bool L_7 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		String_t* L_10 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		String_t* L_11 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral3661015907, (ObjectU5BU5D_t11523773*)L_8, /*hidden argument*/NULL);
		ZenjectBindException_t3550319704 * L_12 = (ZenjectBindException_t3550319704 *)il2cpp_codegen_object_new(ZenjectBindException_t3550319704_il2cpp_TypeInfo_var);
		ZenjectBindException__ctor_m3054227529(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_005a:
	{
		DiContainer_t2383114449 * L_13 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_14 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_13);
		BinderGeneric_1_t1807883816 * L_15 = GenericVirtFuncInvoker1< BinderGeneric_1_t1807883816 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_13, (String_t*)L_14);
		U3CToPrefabU3Ec__AnonStorey175_t1205708684 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Func_2_t3908423697 * L_18 = (Func_2_t3908423697 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Func_2_t3908423697 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_18, (Il2CppObject *)L_16, (IntPtr_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((BinderGeneric_1_t1807883816 *)L_15);
		BindingConditionSetter_t259147722 * L_19 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1807883816 *, Func_2_t3908423697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((BinderGeneric_1_t1807883816 *)L_15, (Func_2_t3908423697 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_19;
	}
}
// System.Void Zenject.IFactoryBinder`2/<ToMethod>c__AnonStorey176<System.Object,System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey176__ctor_m3304531251_gshared (U3CToMethodU3Ec__AnonStorey176_t2008305412 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2/<ToMethod>c__AnonStorey176<System.Object,System.Object>::<>m__298(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToMethodU3Ec__AnonStorey176_U3CU3Em__298_m4065912767_MetadataUsageId;
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey176_U3CU3Em__298_m4065912767_gshared (U3CToMethodU3Ec__AnonStorey176_t2008305412 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToMethodU3Ec__AnonStorey176_U3CU3Em__298_m4065912767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Func_3_t1309472546 * L_3 = (Func_3_t1309472546 *)__this->get_method_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		FactoryMethod_2_t1984661133 * L_4 = GenericVirtFuncInvoker1< FactoryMethod_2_t1984661133 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`2/<ToPrefab>c__AnonStorey177<System.Object,System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey177__ctor_m3867410545_gshared (U3CToPrefabU3Ec__AnonStorey177_t3739000318 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`2<TParam1,TContract> Zenject.IFactoryBinder`2/<ToPrefab>c__AnonStorey177<System.Object,System.Object>::<>m__29B(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToPrefabU3Ec__AnonStorey177_U3CU3Em__29B_m1569127859_MetadataUsageId;
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey177_U3CU3Em__29B_m1569127859_gshared (U3CToPrefabU3Ec__AnonStorey177_t3739000318 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToPrefabU3Ec__AnonStorey177_U3CU3Em__29B_m1569127859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t4012695102 * L_3 = (GameObject_t4012695102 *)__this->get_prefab_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		GameObjectFactory_2_t546746903 * L_4 = GenericVirtFuncInvoker1< GameObjectFactory_2_t546746903 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`2<System.Object,System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_2__ctor_m1276753748_gshared (IFactoryBinder_2_t3921329133 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_0(L_0);
		String_t* L_1 = ___identifier1;
		__this->set__identifier_1(L_1);
		return;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToMethod(System.Func`3<Zenject.DiContainer,TParam1,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToMethod_m28043641_gshared (IFactoryBinder_2_t3921329133 * __this, Func_3_t1309472546 * ___method0, const MethodInfo* method)
{
	U3CToMethodU3Ec__AnonStorey176_t2008305412 * V_0 = NULL;
	{
		U3CToMethodU3Ec__AnonStorey176_t2008305412 * L_0 = (U3CToMethodU3Ec__AnonStorey176_t2008305412 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToMethodU3Ec__AnonStorey176_t2008305412 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToMethodU3Ec__AnonStorey176_t2008305412 *)L_0;
		U3CToMethodU3Ec__AnonStorey176_t2008305412 * L_1 = V_0;
		Func_3_t1309472546 * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		DiContainer_t2383114449 * L_3 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_4 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_3);
		BinderGeneric_1_t655913167 * L_5 = GenericVirtFuncInvoker1< BinderGeneric_1_t655913167 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_3, (String_t*)L_4);
		U3CToMethodU3Ec__AnonStorey176_t2008305412 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Func_2_t2756453048 * L_8 = (Func_2_t2756453048 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2756453048 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t655913167 *)L_5);
		BindingConditionSetter_t259147722 * L_9 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t655913167 *, Func_2_t2756453048 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t655913167 *)L_5, (Func_2_t2756453048 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_9;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToFactory()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_2_ToFactory_m1579464947_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToFactory_m1579464947_gshared (IFactoryBinder_2_t3921329133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_2_ToFactory_m1579464947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsAbstract() */, (Type_t *)L_0);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_2 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_3 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_2);
		BinderGeneric_1_t655913167 * L_4 = GenericVirtFuncInvoker1< BinderGeneric_1_t655913167 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_2, (String_t*)L_3);
		NullCheck((BinderGeneric_1_t655913167 *)L_4);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t655913167 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((BinderGeneric_1_t655913167 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`2<System.Object,System.Object>::ToPrefab(UnityEngine.GameObject)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectBindException_t3550319704_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3661015907;
extern const uint32_t IFactoryBinder_2_ToPrefab_m1058408149_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_2_ToPrefab_m1058408149_gshared (IFactoryBinder_2_t3921329133 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_2_ToPrefab_m1058408149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToPrefabU3Ec__AnonStorey177_t3739000318 * V_0 = NULL;
	{
		U3CToPrefabU3Ec__AnonStorey177_t3739000318 * L_0 = (U3CToPrefabU3Ec__AnonStorey177_t3739000318 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3CToPrefabU3Ec__AnonStorey177_t3739000318 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3CToPrefabU3Ec__AnonStorey177_t3739000318 *)L_0;
		U3CToPrefabU3Ec__AnonStorey177_t3739000318 * L_1 = V_0;
		GameObject_t4012695102 * L_2 = ___prefab0;
		NullCheck(L_1);
		L_1->set_prefab_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		bool L_4 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_4, /*hidden argument*/NULL);
		U3CToPrefabU3Ec__AnonStorey177_t3739000318 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t4012695102 * L_6 = (GameObject_t4012695102 *)L_5->get_prefab_0();
		bool L_7 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		String_t* L_10 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		String_t* L_11 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral3661015907, (ObjectU5BU5D_t11523773*)L_8, /*hidden argument*/NULL);
		ZenjectBindException_t3550319704 * L_12 = (ZenjectBindException_t3550319704 *)il2cpp_codegen_object_new(ZenjectBindException_t3550319704_il2cpp_TypeInfo_var);
		ZenjectBindException__ctor_m3054227529(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_005a:
	{
		DiContainer_t2383114449 * L_13 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_14 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_13);
		BinderGeneric_1_t655913167 * L_15 = GenericVirtFuncInvoker1< BinderGeneric_1_t655913167 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_13, (String_t*)L_14);
		U3CToPrefabU3Ec__AnonStorey177_t3739000318 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Func_2_t2756453048 * L_18 = (Func_2_t2756453048 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2756453048 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_18, (Il2CppObject *)L_16, (IntPtr_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t655913167 *)L_15);
		BindingConditionSetter_t259147722 * L_19 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t655913167 *, Func_2_t2756453048 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t655913167 *)L_15, (Func_2_t2756453048 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_19;
	}
}
// System.Void Zenject.IFactoryBinder`3/<ToMethod>c__AnonStorey178<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey178__ctor_m783517254_gshared (U3CToMethodU3Ec__AnonStorey178_t3714372930 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3/<ToMethod>c__AnonStorey178<System.Object,System.Object,System.Object>::<>m__29C(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToMethodU3Ec__AnonStorey178_U3CU3Em__29C_m839474009_MetadataUsageId;
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey178_U3CU3Em__29C_m839474009_gshared (U3CToMethodU3Ec__AnonStorey178_t3714372930 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToMethodU3Ec__AnonStorey178_U3CU3Em__29C_m839474009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Func_4_t2229882165 * L_3 = (Func_4_t2229882165 *)__this->get_method_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		FactoryMethod_3_t1233407108 * L_4 = GenericVirtFuncInvoker1< FactoryMethod_3_t1233407108 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`3/<ToPrefab>c__AnonStorey179<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey179__ctor_m575200516_gshared (U3CToPrefabU3Ec__AnonStorey179_t385784096 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`3<TParam1,TParam2,TContract> Zenject.IFactoryBinder`3/<ToPrefab>c__AnonStorey179<System.Object,System.Object,System.Object>::<>m__29F(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToPrefabU3Ec__AnonStorey179_U3CU3Em__29F_m1079674036_MetadataUsageId;
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey179_U3CU3Em__29F_m1079674036_gshared (U3CToPrefabU3Ec__AnonStorey179_t385784096 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToPrefabU3Ec__AnonStorey179_U3CU3Em__29F_m1079674036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t4012695102 * L_3 = (GameObject_t4012695102 *)__this->get_prefab_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		GameObjectFactory_3_t639885202 * L_4 = GenericVirtFuncInvoker1< GameObjectFactory_3_t639885202 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_3__ctor_m2916354433_gshared (IFactoryBinder_3_t153990564 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_0(L_0);
		String_t* L_1 = ___identifier1;
		__this->set__identifier_1(L_1);
		return;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToMethod(System.Func`4<Zenject.DiContainer,TParam1,TParam2,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToMethod_m1504760372_gshared (IFactoryBinder_3_t153990564 * __this, Func_4_t2229882165 * ___method0, const MethodInfo* method)
{
	U3CToMethodU3Ec__AnonStorey178_t3714372930 * V_0 = NULL;
	{
		U3CToMethodU3Ec__AnonStorey178_t3714372930 * L_0 = (U3CToMethodU3Ec__AnonStorey178_t3714372930 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToMethodU3Ec__AnonStorey178_t3714372930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToMethodU3Ec__AnonStorey178_t3714372930 *)L_0;
		U3CToMethodU3Ec__AnonStorey178_t3714372930 * L_1 = V_0;
		Func_4_t2229882165 * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		DiContainer_t2383114449 * L_3 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_4 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_3);
		BinderGeneric_1_t1303598458 * L_5 = GenericVirtFuncInvoker1< BinderGeneric_1_t1303598458 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_3, (String_t*)L_4);
		U3CToMethodU3Ec__AnonStorey178_t3714372930 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Func_2_t3404138339 * L_8 = (Func_2_t3404138339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t3404138339 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t1303598458 *)L_5);
		BindingConditionSetter_t259147722 * L_9 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1303598458 *, Func_2_t3404138339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t1303598458 *)L_5, (Func_2_t3404138339 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_9;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToFactory()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_3_ToFactory_m208757734_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToFactory_m208757734_gshared (IFactoryBinder_3_t153990564 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_3_ToFactory_m208757734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsAbstract() */, (Type_t *)L_0);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_2 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_3 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_2);
		BinderGeneric_1_t1303598458 * L_4 = GenericVirtFuncInvoker1< BinderGeneric_1_t1303598458 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_2, (String_t*)L_3);
		NullCheck((BinderGeneric_1_t1303598458 *)L_4);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1303598458 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((BinderGeneric_1_t1303598458 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>::ToPrefab(UnityEngine.GameObject)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectBindException_t3550319704_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3661015907;
extern const uint32_t IFactoryBinder_3_ToPrefab_m925352386_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_3_ToPrefab_m925352386_gshared (IFactoryBinder_3_t153990564 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_3_ToPrefab_m925352386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToPrefabU3Ec__AnonStorey179_t385784096 * V_0 = NULL;
	{
		U3CToPrefabU3Ec__AnonStorey179_t385784096 * L_0 = (U3CToPrefabU3Ec__AnonStorey179_t385784096 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3CToPrefabU3Ec__AnonStorey179_t385784096 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3CToPrefabU3Ec__AnonStorey179_t385784096 *)L_0;
		U3CToPrefabU3Ec__AnonStorey179_t385784096 * L_1 = V_0;
		GameObject_t4012695102 * L_2 = ___prefab0;
		NullCheck(L_1);
		L_1->set_prefab_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		bool L_4 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_4, /*hidden argument*/NULL);
		U3CToPrefabU3Ec__AnonStorey179_t385784096 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t4012695102 * L_6 = (GameObject_t4012695102 *)L_5->get_prefab_0();
		bool L_7 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		String_t* L_10 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		String_t* L_11 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral3661015907, (ObjectU5BU5D_t11523773*)L_8, /*hidden argument*/NULL);
		ZenjectBindException_t3550319704 * L_12 = (ZenjectBindException_t3550319704 *)il2cpp_codegen_object_new(ZenjectBindException_t3550319704_il2cpp_TypeInfo_var);
		ZenjectBindException__ctor_m3054227529(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_005a:
	{
		DiContainer_t2383114449 * L_13 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_14 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_13);
		BinderGeneric_1_t1303598458 * L_15 = GenericVirtFuncInvoker1< BinderGeneric_1_t1303598458 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_13, (String_t*)L_14);
		U3CToPrefabU3Ec__AnonStorey179_t385784096 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Func_2_t3404138339 * L_18 = (Func_2_t3404138339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t3404138339 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_18, (Il2CppObject *)L_16, (IntPtr_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t1303598458 *)L_15);
		BindingConditionSetter_t259147722 * L_19 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1303598458 *, Func_2_t3404138339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t1303598458 *)L_15, (Func_2_t3404138339 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_19;
	}
}
// System.Void Zenject.IFactoryBinder`4/<ToMethod>c__AnonStorey17A<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey17A__ctor_m1059049120_gshared (U3CToMethodU3Ec__AnonStorey17A_t2106438637 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4/<ToMethod>c__AnonStorey17A<System.Object,System.Object,System.Object,System.Object>::<>m__2A0(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToMethodU3Ec__AnonStorey17A_U3CU3Em__2A0_m2903502399_MetadataUsageId;
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey17A_U3CU3Em__2A0_m2903502399_gshared (U3CToMethodU3Ec__AnonStorey17A_t2106438637 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToMethodU3Ec__AnonStorey17A_U3CU3Em__2A0_m2903502399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Func_5_t936310804 * L_3 = (Func_5_t936310804 *)__this->get_method_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		FactoryMethod_4_t1102797835 * L_4 = GenericVirtFuncInvoker1< FactoryMethod_4_t1102797835 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`4/<ToPrefab>c__AnonStorey17B<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey17B__ctor_m813662430_gshared (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`4<TParam1,TParam2,TParam3,TContract> Zenject.IFactoryBinder`4/<ToPrefab>c__AnonStorey17B<System.Object,System.Object,System.Object,System.Object>::<>m__2A3(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToPrefabU3Ec__AnonStorey17B_U3CU3Em__2A3_m3199267610_MetadataUsageId;
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey17B_U3CU3Em__2A3_m3199267610_gshared (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToPrefabU3Ec__AnonStorey17B_U3CU3Em__2A3_m3199267610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t4012695102 * L_3 = (GameObject_t4012695102 *)__this->get_prefab_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		GameObjectFactory_4_t486081797 * L_4 = GenericVirtFuncInvoker1< GameObjectFactory_4_t486081797 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_4__ctor_m143918766_gshared (IFactoryBinder_4_t2507049579 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_0(L_0);
		String_t* L_1 = ___identifier1;
		__this->set__identifier_1(L_1);
		return;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToMethod(System.Func`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToMethod_m2184225648_gshared (IFactoryBinder_4_t2507049579 * __this, Func_5_t936310804 * ___method0, const MethodInfo* method)
{
	U3CToMethodU3Ec__AnonStorey17A_t2106438637 * V_0 = NULL;
	{
		U3CToMethodU3Ec__AnonStorey17A_t2106438637 * L_0 = (U3CToMethodU3Ec__AnonStorey17A_t2106438637 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToMethodU3Ec__AnonStorey17A_t2106438637 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToMethodU3Ec__AnonStorey17A_t2106438637 *)L_0;
		U3CToMethodU3Ec__AnonStorey17A_t2106438637 * L_1 = V_0;
		Func_5_t936310804 * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		DiContainer_t2383114449 * L_3 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_4 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_3);
		BinderGeneric_1_t1578143997 * L_5 = GenericVirtFuncInvoker1< BinderGeneric_1_t1578143997 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_3, (String_t*)L_4);
		U3CToMethodU3Ec__AnonStorey17A_t2106438637 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Func_2_t3678683878 * L_8 = (Func_2_t3678683878 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t3678683878 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t1578143997 *)L_5);
		BindingConditionSetter_t259147722 * L_9 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1578143997 *, Func_2_t3678683878 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t1578143997 *)L_5, (Func_2_t3678683878 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_9;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToFactory()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_4_ToFactory_m2200572889_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToFactory_m2200572889_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_4_ToFactory_m2200572889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsAbstract() */, (Type_t *)L_0);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_2 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_3 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_2);
		BinderGeneric_1_t1578143997 * L_4 = GenericVirtFuncInvoker1< BinderGeneric_1_t1578143997 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_2, (String_t*)L_3);
		NullCheck((BinderGeneric_1_t1578143997 *)L_4);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1578143997 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((BinderGeneric_1_t1578143997 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToPrefab(UnityEngine.GameObject)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectBindException_t3550319704_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3661015907;
extern const uint32_t IFactoryBinder_4_ToPrefab_m3203569583_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToPrefab_m3203569583_gshared (IFactoryBinder_4_t2507049579 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_4_ToPrefab_m3203569583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * V_0 = NULL;
	{
		U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * L_0 = (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3CToPrefabU3Ec__AnonStorey17B_t2698428119 *)L_0;
		U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * L_1 = V_0;
		GameObject_t4012695102 * L_2 = ___prefab0;
		NullCheck(L_1);
		L_1->set_prefab_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		bool L_4 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_4, /*hidden argument*/NULL);
		U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t4012695102 * L_6 = (GameObject_t4012695102 *)L_5->get_prefab_0();
		bool L_7 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		String_t* L_10 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		String_t* L_11 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral3661015907, (ObjectU5BU5D_t11523773*)L_8, /*hidden argument*/NULL);
		ZenjectBindException_t3550319704 * L_12 = (ZenjectBindException_t3550319704 *)il2cpp_codegen_object_new(ZenjectBindException_t3550319704_il2cpp_TypeInfo_var);
		ZenjectBindException__ctor_m3054227529(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_005a:
	{
		DiContainer_t2383114449 * L_13 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_14 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_13);
		BinderGeneric_1_t1578143997 * L_15 = GenericVirtFuncInvoker1< BinderGeneric_1_t1578143997 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_13, (String_t*)L_14);
		U3CToPrefabU3Ec__AnonStorey17B_t2698428119 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Func_2_t3678683878 * L_18 = (Func_2_t3678683878 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t3678683878 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_18, (Il2CppObject *)L_16, (IntPtr_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t1578143997 *)L_15);
		BindingConditionSetter_t259147722 * L_19 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1578143997 *, Func_2_t3678683878 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t1578143997 *)L_15, (Func_2_t3678683878 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_19;
	}
}
// System.Void Zenject.IFactoryBinder`5/<ToMethod>c__AnonStorey17C<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey17C__ctor_m1814593907_gshared (U3CToMethodU3Ec__AnonStorey17C_t3805965893 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5/<ToMethod>c__AnonStorey17C<System.Object,System.Object,System.Object,System.Object,System.Object>::<>m__2A4(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToMethodU3Ec__AnonStorey17C_U3CU3Em__2A4_m2349341694_MetadataUsageId;
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey17C_U3CU3Em__2A4_m2349341694_gshared (U3CToMethodU3Ec__AnonStorey17C_t3805965893 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToMethodU3Ec__AnonStorey17C_U3CU3Em__2A4_m2349341694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Func_6_t3355333698 * L_3 = (Func_6_t3355333698 *)__this->get_method_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		FactoryMethod_5_t2447447334 * L_4 = GenericVirtFuncInvoker1< FactoryMethod_5_t2447447334 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`5/<ToPrefab>c__AnonStorey17D<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey17D__ctor_m1582067505_gshared (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5/<ToPrefab>c__AnonStorey17D<System.Object,System.Object,System.Object,System.Object,System.Object>::<>m__2A7(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t U3CToPrefabU3Ec__AnonStorey17D_U3CU3Em__2A7_m3427982425_MetadataUsageId;
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey17D_U3CU3Em__2A7_m3427982425_gshared (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToPrefabU3Ec__AnonStorey17D_U3CU3Em__2A7_m3427982425_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InjectContext_t3456483891 * L_0 = ___ctx0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		GameObject_t4012695102 * L_3 = (GameObject_t4012695102 *)__this->get_prefab_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck((DiContainer_t2383114449 *)L_1);
		GameObjectFactory_5_t2159204100 * L_4 = GenericVirtFuncInvoker1< GameObjectFactory_5_t2159204100 *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (DiContainer_t2383114449 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		return L_4;
	}
}
// System.Void Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_5__ctor_m3485687515_gshared (IFactoryBinder_5_t1497530694 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_0(L_0);
		String_t* L_1 = ___identifier1;
		__this->set__identifier_1(L_1);
		return;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToMethod(ModestTree.Util.Func`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToMethod_m448282366_gshared (IFactoryBinder_5_t1497530694 * __this, Func_6_t3355333698 * ___method0, const MethodInfo* method)
{
	U3CToMethodU3Ec__AnonStorey17C_t3805965893 * V_0 = NULL;
	{
		U3CToMethodU3Ec__AnonStorey17C_t3805965893 * L_0 = (U3CToMethodU3Ec__AnonStorey17C_t3805965893 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToMethodU3Ec__AnonStorey17C_t3805965893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToMethodU3Ec__AnonStorey17C_t3805965893 *)L_0;
		U3CToMethodU3Ec__AnonStorey17C_t3805965893 * L_1 = V_0;
		Func_6_t3355333698 * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		DiContainer_t2383114449 * L_3 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_4 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_3);
		BinderGeneric_1_t46883500 * L_5 = GenericVirtFuncInvoker1< BinderGeneric_1_t46883500 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_3, (String_t*)L_4);
		U3CToMethodU3Ec__AnonStorey17C_t3805965893 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Func_2_t2147423381 * L_8 = (Func_2_t2147423381 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2147423381 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t46883500 *)L_5);
		BindingConditionSetter_t259147722 * L_9 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t46883500 *, Func_2_t2147423381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t46883500 *)L_5, (Func_2_t2147423381 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_9;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToFactory()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t IFactoryBinder_5_ToFactory_m4119533772_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToFactory_m4119533772_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_5_ToFactory_m4119533772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsAbstract() */, (Type_t *)L_0);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_2 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_3 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_2);
		BinderGeneric_1_t46883500 * L_4 = GenericVirtFuncInvoker1< BinderGeneric_1_t46883500 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_2, (String_t*)L_3);
		NullCheck((BinderGeneric_1_t46883500 *)L_4);
		BindingConditionSetter_t259147722 * L_5 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t46883500 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((BinderGeneric_1_t46883500 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_5;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToPrefab(UnityEngine.GameObject)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectBindException_t3550319704_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3661015907;
extern const uint32_t IFactoryBinder_5_ToPrefab_m185350300_MetadataUsageId;
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToPrefab_m185350300_gshared (IFactoryBinder_5_t1497530694 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IFactoryBinder_5_ToPrefab_m185350300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * V_0 = NULL;
	{
		U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * L_0 = (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 *)L_0;
		U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * L_1 = V_0;
		GameObject_t4012695102 * L_2 = ___prefab0;
		NullCheck(L_1);
		L_1->set_prefab_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		bool L_4 = TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/TypeExtensions_DerivesFrom_TisComponent_t2126946602_m1762899932_MethodInfo_var);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_4, /*hidden argument*/NULL);
		U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t4012695102 * L_6 = (GameObject_t4012695102 *)L_5->get_prefab_0();
		bool L_7 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)), /*hidden argument*/NULL);
		String_t* L_10 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		String_t* L_11 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral3661015907, (ObjectU5BU5D_t11523773*)L_8, /*hidden argument*/NULL);
		ZenjectBindException_t3550319704 * L_12 = (ZenjectBindException_t3550319704 *)il2cpp_codegen_object_new(ZenjectBindException_t3550319704_il2cpp_TypeInfo_var);
		ZenjectBindException__ctor_m3054227529(L_12, (String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_005a:
	{
		DiContainer_t2383114449 * L_13 = (DiContainer_t2383114449 *)__this->get__container_0();
		String_t* L_14 = (String_t*)__this->get__identifier_1();
		NullCheck((DiContainer_t2383114449 *)L_13);
		BinderGeneric_1_t46883500 * L_15 = GenericVirtFuncInvoker1< BinderGeneric_1_t46883500 *, String_t* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (DiContainer_t2383114449 *)L_13, (String_t*)L_14);
		U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Func_2_t2147423381 * L_18 = (Func_2_t2147423381 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2147423381 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_18, (Il2CppObject *)L_16, (IntPtr_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((BinderGeneric_1_t46883500 *)L_15);
		BindingConditionSetter_t259147722 * L_19 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t46883500 *, Func_2_t2147423381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BinderGeneric_1_t46883500 *)L_15, (Func_2_t2147423381 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_19;
	}
}
// System.Void Zenject.IFactoryUntypedBinder`1/<ToInstance>c__AnonStorey17E<System.Object>::.ctor()
extern "C"  void U3CToInstanceU3Ec__AnonStorey17E__ctor_m988856348_gshared (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TContract Zenject.IFactoryUntypedBinder`1/<ToInstance>c__AnonStorey17E<System.Object>::<>m__2A8(Zenject.DiContainer,System.Object[])
extern "C"  Il2CppObject * U3CToInstanceU3Ec__AnonStorey17E_U3CU3Em__2A8_m1660689032_gshared (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 * __this, DiContainer_t2383114449 * ___c0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_instance_0();
		return L_0;
	}
}
// System.Void Zenject.IFactoryUntypedBinder`1/<ToMethod>c__AnonStorey17F<System.Object>::.ctor()
extern "C"  void U3CToMethodU3Ec__AnonStorey17F__ctor_m3752727377_gshared (U3CToMethodU3Ec__AnonStorey17F_t1801356992 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.IFactoryUntyped`1<TContract> Zenject.IFactoryUntypedBinder`1/<ToMethod>c__AnonStorey17F<System.Object>::<>m__2A9(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToMethodU3Ec__AnonStorey17F_U3CU3Em__2A9_m1647927756_gshared (U3CToMethodU3Ec__AnonStorey17F_t1801356992 * __this, InjectContext_t3456483891 * ___c0, const MethodInfo* method)
{
	{
		InjectContext_t3456483891 * L_0 = ___c0;
		NullCheck(L_0);
		DiContainer_t2383114449 * L_1 = (DiContainer_t2383114449 *)L_0->get_Container_9();
		Func_3_t2319412757 * L_2 = (Func_3_t2319412757 *)__this->get_method_0();
		FactoryMethodUntyped_1_t1598907365 * L_3 = (FactoryMethodUntyped_1_t1598907365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (FactoryMethodUntyped_1_t1598907365 *, DiContainer_t2383114449 *, Func_3_t2319412757 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_3, (DiContainer_t2383114449 *)L_1, (Func_3_t2319412757 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_3;
	}
}
// System.Void Zenject.IFactoryUntypedBinder`1<System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryUntypedBinder_1__ctor_m2282437894_gshared (IFactoryUntypedBinder_1_t486203833 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_0(L_0);
		String_t* L_1 = ___identifier1;
		__this->set__identifier_1(L_1);
		return;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToInstance(TContract)
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToInstance_m3479808858_gshared (IFactoryUntypedBinder_1_t486203833 * __this, Il2CppObject * ___instance0, const MethodInfo* method)
{
	U3CToInstanceU3Ec__AnonStorey17E_t3854809107 * V_0 = NULL;
	{
		U3CToInstanceU3Ec__AnonStorey17E_t3854809107 * L_0 = (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToInstanceU3Ec__AnonStorey17E_t3854809107 *)L_0;
		U3CToInstanceU3Ec__AnonStorey17E_t3854809107 * L_1 = V_0;
		Il2CppObject * L_2 = ___instance0;
		NullCheck(L_1);
		L_1->set_instance_0(L_2);
		U3CToInstanceU3Ec__AnonStorey17E_t3854809107 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Func_3_t2319412757 * L_5 = (Func_3_t2319412757 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Func_3_t2319412757 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((IFactoryUntypedBinder_1_t486203833 *)__this);
		BindingConditionSetter_t259147722 * L_6 = ((  BindingConditionSetter_t259147722 * (*) (IFactoryUntypedBinder_1_t486203833 *, Func_3_t2319412757 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((IFactoryUntypedBinder_1_t486203833 *)__this, (Func_3_t2319412757 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_6;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToMethod(System.Func`3<Zenject.DiContainer,System.Object[],TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToMethod_m844622821_gshared (IFactoryUntypedBinder_1_t486203833 * __this, Func_3_t2319412757 * ___method0, const MethodInfo* method)
{
	U3CToMethodU3Ec__AnonStorey17F_t1801356992 * V_0 = NULL;
	{
		U3CToMethodU3Ec__AnonStorey17F_t1801356992 * L_0 = (U3CToMethodU3Ec__AnonStorey17F_t1801356992 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (U3CToMethodU3Ec__AnonStorey17F_t1801356992 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		V_0 = (U3CToMethodU3Ec__AnonStorey17F_t1801356992 *)L_0;
		U3CToMethodU3Ec__AnonStorey17F_t1801356992 * L_1 = V_0;
		Func_3_t2319412757 * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		DiContainer_t2383114449 * L_3 = (DiContainer_t2383114449 *)__this->get__container_0();
		NullCheck((DiContainer_t2383114449 *)L_3);
		BinderGeneric_1_t1217979071 * L_4 = GenericVirtFuncInvoker0< BinderGeneric_1_t1217979071 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_3);
		U3CToMethodU3Ec__AnonStorey17F_t1801356992 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Func_2_t3318518952 * L_7 = (Func_2_t3318518952 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Func_2_t3318518952 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((BinderGeneric_1_t1217979071 *)L_4);
		BindingConditionSetter_t259147722 * L_8 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1217979071 *, Func_2_t3318518952 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((BinderGeneric_1_t1217979071 *)L_4, (Func_2_t3318518952 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_8;
	}
}
// Zenject.BindingConditionSetter Zenject.IFactoryUntypedBinder`1<System.Object>::ToFactory()
extern "C"  BindingConditionSetter_t259147722 * IFactoryUntypedBinder_1_ToFactory_m3712332303_gshared (IFactoryUntypedBinder_1_t486203833 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		NullCheck((DiContainer_t2383114449 *)L_0);
		BinderGeneric_1_t1217979071 * L_1 = GenericVirtFuncInvoker0< BinderGeneric_1_t1217979071 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (DiContainer_t2383114449 *)L_0);
		NullCheck((BinderGeneric_1_t1217979071 *)L_1);
		BindingConditionSetter_t259147722 * L_2 = ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t1217979071 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((BinderGeneric_1_t1217979071 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return L_2;
	}
}
// System.Void Zenject.KeyedFactory`2<System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_2__ctor_m3172930494_gshared (KeyedFactory_2_t3889101264 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		((  void (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Type[] Zenject.KeyedFactory`2<System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t KeyedFactory_2_get_ProvidedTypes_m3187342273_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_2_get_ProvidedTypes_m3187342273_gshared (KeyedFactory_2_t3889101264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_2_get_ProvidedTypes_m3187342273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// TBase Zenject.KeyedFactory`2<System.Object,System.Object>::Create(TKey)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t KeyedFactory_2_Create_m1040458889_MetadataUsageId;
extern "C"  Il2CppObject * KeyedFactory_2_Create_m1040458889_gshared (KeyedFactory_2_t3889101264 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_2_Create_m1040458889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		Type_t * L_1 = ((  Type_t * (*) (KeyedFactoryBase_2_t2864689411 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyedFactoryBase_2_t2864689411 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Type_t *)L_1;
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		DiContainer_t2383114449 * L_2 = ((  DiContainer_t2383114449 * (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Type_t * L_3 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_2);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Type_t *, ObjectU5BU5D_t11523773* >::Invoke(32 /* System.Object Zenject.DiContainer::Instantiate(System.Type,System.Object[]) */, (DiContainer_t2383114449 *)L_2, (Type_t *)L_3, (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)));
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
	}
}
// System.Void Zenject.KeyedFactory`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_3__ctor_m2992082481_gshared (KeyedFactory_3_t2002144469 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		((  void (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Type[] Zenject.KeyedFactory`3<System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t KeyedFactory_3_get_ProvidedTypes_m3041128372_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_3_get_ProvidedTypes_m3041128372_gshared (KeyedFactory_3_t2002144469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_3_get_ProvidedTypes_m3041128372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// TBase Zenject.KeyedFactory`3<System.Object,System.Object,System.Object>::Create(TKey,TParam1)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t KeyedFactory_3_Create_m478300650_MetadataUsageId;
extern "C"  Il2CppObject * KeyedFactory_3_Create_m478300650_gshared (KeyedFactory_3_t2002144469 * __this, Il2CppObject * ___key0, Il2CppObject * ___param11, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_3_Create_m478300650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		DiContainer_t2383114449 * L_0 = ((  DiContainer_t2383114449 * (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Il2CppObject * L_1 = ___key0;
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		Type_t * L_2 = ((  Type_t * (*) (KeyedFactoryBase_2_t2864689411 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyedFactoryBase_2_t2864689411 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		List_1_t1417891359 * L_3 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_3, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_3;
		List_1_t1417891359 * L_4 = V_0;
		Il2CppObject * L_5 = ___param11;
		TypeValuePair_t620932390 * L_6 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((List_1_t1417891359 *)L_4);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_4, (TypeValuePair_t620932390 *)L_6);
		List_1_t1417891359 * L_7 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_8 = VirtFuncInvoker2< Il2CppObject *, Type_t *, List_1_t1417891359 * >::Invoke(34 /* System.Object Zenject.DiContainer::InstantiateExplicit(System.Type,System.Collections.Generic.List`1<Zenject.TypeValuePair>) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_2, (List_1_t1417891359 *)L_7);
		return ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)));
	}
}
// System.Void Zenject.KeyedFactory`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_4__ctor_m1649020324_gshared (KeyedFactory_4_t2209276758 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		((  void (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Type[] Zenject.KeyedFactory`4<System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t KeyedFactory_4_get_ProvidedTypes_m2568415399_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_4_get_ProvidedTypes_m2568415399_gshared (KeyedFactory_4_t2209276758 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_4_get_ProvidedTypes_m2568415399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		return L_2;
	}
}
// TBase Zenject.KeyedFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(TKey,TParam1,TParam2)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t KeyedFactory_4_Create_m2897902698_MetadataUsageId;
extern "C"  Il2CppObject * KeyedFactory_4_Create_m2897902698_gshared (KeyedFactory_4_t2209276758 * __this, Il2CppObject * ___key0, Il2CppObject * ___param11, Il2CppObject * ___param22, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_4_Create_m2897902698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		DiContainer_t2383114449 * L_0 = ((  DiContainer_t2383114449 * (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_1 = ___key0;
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		Type_t * L_2 = ((  Type_t * (*) (KeyedFactoryBase_2_t2864689411 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyedFactoryBase_2_t2864689411 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		List_1_t1417891359 * L_3 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_3, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_3;
		List_1_t1417891359 * L_4 = V_0;
		Il2CppObject * L_5 = ___param11;
		TypeValuePair_t620932390 * L_6 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((List_1_t1417891359 *)L_4);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_4, (TypeValuePair_t620932390 *)L_6);
		List_1_t1417891359 * L_7 = V_0;
		Il2CppObject * L_8 = ___param22;
		TypeValuePair_t620932390 * L_9 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((List_1_t1417891359 *)L_7);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_7, (TypeValuePair_t620932390 *)L_9);
		List_1_t1417891359 * L_10 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_11 = VirtFuncInvoker2< Il2CppObject *, Type_t *, List_1_t1417891359 * >::Invoke(34 /* System.Object Zenject.DiContainer::InstantiateExplicit(System.Type,System.Collections.Generic.List`1<Zenject.TypeValuePair>) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_2, (List_1_t1417891359 *)L_10);
		return ((Il2CppObject *)Castclass(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)));
	}
}
// System.Void Zenject.KeyedFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_5__ctor_m2126176791_gshared (KeyedFactory_5_t1813071951 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		((  void (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Type[] Zenject.KeyedFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t KeyedFactory_5_get_ProvidedTypes_m505427610_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_5_get_ProvidedTypes_m505427610_gshared (KeyedFactory_5_t1813071951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_5_get_ProvidedTypes_m505427610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		return L_4;
	}
}
// TBase Zenject.KeyedFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TKey,TParam1,TParam2,TParam3)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t KeyedFactory_5_Create_m2159402249_MetadataUsageId;
extern "C"  Il2CppObject * KeyedFactory_5_Create_m2159402249_gshared (KeyedFactory_5_t1813071951 * __this, Il2CppObject * ___key0, Il2CppObject * ___param11, Il2CppObject * ___param22, Il2CppObject * ___param33, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_5_Create_m2159402249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		DiContainer_t2383114449 * L_0 = ((  DiContainer_t2383114449 * (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject * L_1 = ___key0;
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		Type_t * L_2 = ((  Type_t * (*) (KeyedFactoryBase_2_t2864689411 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyedFactoryBase_2_t2864689411 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		List_1_t1417891359 * L_3 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_3, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_3;
		List_1_t1417891359 * L_4 = V_0;
		Il2CppObject * L_5 = ___param11;
		TypeValuePair_t620932390 * L_6 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((List_1_t1417891359 *)L_4);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_4, (TypeValuePair_t620932390 *)L_6);
		List_1_t1417891359 * L_7 = V_0;
		Il2CppObject * L_8 = ___param22;
		TypeValuePair_t620932390 * L_9 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((List_1_t1417891359 *)L_7);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_7, (TypeValuePair_t620932390 *)L_9);
		List_1_t1417891359 * L_10 = V_0;
		Il2CppObject * L_11 = ___param33;
		TypeValuePair_t620932390 * L_12 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((List_1_t1417891359 *)L_10);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_10, (TypeValuePair_t620932390 *)L_12);
		List_1_t1417891359 * L_13 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_14 = VirtFuncInvoker2< Il2CppObject *, Type_t *, List_1_t1417891359 * >::Invoke(34 /* System.Object Zenject.DiContainer::InstantiateExplicit(System.Type,System.Collections.Generic.List`1<Zenject.TypeValuePair>) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_2, (List_1_t1417891359 *)L_13);
		return ((Il2CppObject *)Castclass(L_14, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)));
	}
}
// System.Void Zenject.KeyedFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_6__ctor_m1887853450_gshared (KeyedFactory_6_t605647996 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		((  void (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Type[] Zenject.KeyedFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t KeyedFactory_6_get_ProvidedTypes_m1143220621_MetadataUsageId;
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_6_get_ProvidedTypes_m1143220621_gshared (KeyedFactory_6_t605647996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_6_get_ProvidedTypes_m1143220621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeU5BU5D_t3431720054* L_0 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		TypeU5BU5D_t3431720054* L_2 = (TypeU5BU5D_t3431720054*)L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_3);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_5);
		TypeU5BU5D_t3431720054* L_6 = (TypeU5BU5D_t3431720054*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_7);
		return L_6;
	}
}
// TBase Zenject.KeyedFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TKey,TParam1,TParam2,TParam3,TParam4)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t KeyedFactory_6_Create_m2275708103_MetadataUsageId;
extern "C"  Il2CppObject * KeyedFactory_6_Create_m2275708103_gshared (KeyedFactory_6_t605647996 * __this, Il2CppObject * ___key0, Il2CppObject * ___param11, Il2CppObject * ___param22, Il2CppObject * ___param33, Il2CppObject * ___param44, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactory_6_Create_m2275708103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1417891359 * V_0 = NULL;
	{
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		DiContainer_t2383114449 * L_0 = ((  DiContainer_t2383114449 * (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyedFactoryBase_2_t2864689411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject * L_1 = ___key0;
		NullCheck((KeyedFactoryBase_2_t2864689411 *)__this);
		Type_t * L_2 = ((  Type_t * (*) (KeyedFactoryBase_2_t2864689411 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((KeyedFactoryBase_2_t2864689411 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		List_1_t1417891359 * L_3 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_3, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		V_0 = (List_1_t1417891359 *)L_3;
		List_1_t1417891359 * L_4 = V_0;
		Il2CppObject * L_5 = ___param11;
		TypeValuePair_t620932390 * L_6 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((List_1_t1417891359 *)L_4);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_4, (TypeValuePair_t620932390 *)L_6);
		List_1_t1417891359 * L_7 = V_0;
		Il2CppObject * L_8 = ___param22;
		TypeValuePair_t620932390 * L_9 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((List_1_t1417891359 *)L_7);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_7, (TypeValuePair_t620932390 *)L_9);
		List_1_t1417891359 * L_10 = V_0;
		Il2CppObject * L_11 = ___param33;
		TypeValuePair_t620932390 * L_12 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck((List_1_t1417891359 *)L_10);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_10, (TypeValuePair_t620932390 *)L_12);
		List_1_t1417891359 * L_13 = V_0;
		Il2CppObject * L_14 = ___param44;
		TypeValuePair_t620932390 * L_15 = ((  TypeValuePair_t620932390 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((List_1_t1417891359 *)L_13);
		VirtActionInvoker1< TypeValuePair_t620932390 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.TypeValuePair>::Add(!0) */, (List_1_t1417891359 *)L_13, (TypeValuePair_t620932390 *)L_15);
		List_1_t1417891359 * L_16 = V_0;
		NullCheck((DiContainer_t2383114449 *)L_0);
		Il2CppObject * L_17 = VirtFuncInvoker2< Il2CppObject *, Type_t *, List_1_t1417891359 * >::Invoke(34 /* System.Object Zenject.DiContainer::InstantiateExplicit(System.Type,System.Collections.Generic.List`1<Zenject.TypeValuePair>) */, (DiContainer_t2383114449 *)L_0, (Type_t *)L_2, (List_1_t1417891359 *)L_16);
		return ((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)));
	}
}
// System.Void Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::.ctor()
extern "C"  void U3CValidateU3Ec__Iterator46__ctor_m1860271335_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.ZenjectResolveException Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateU3Ec__Iterator46_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m187790640_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method)
{
	{
		ZenjectResolveException_t1201052999 * L_0 = (ZenjectResolveException_t1201052999 *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateU3Ec__Iterator46_System_Collections_IEnumerator_get_Current_m996118623_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method)
{
	{
		ZenjectResolveException_t1201052999 * L_0 = (ZenjectResolveException_t1201052999 *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateU3Ec__Iterator46_System_Collections_IEnumerable_GetEnumerator_m3628112026_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CValidateU3Ec__Iterator46_t3243744428 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((U3CValidateU3Ec__Iterator46_t3243744428 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateU3Ec__Iterator46_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m291295313_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method)
{
	U3CValidateU3Ec__Iterator46_t3243744428 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CValidateU3Ec__Iterator46_t3243744428 * L_2 = (U3CValidateU3Ec__Iterator46_t3243744428 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (U3CValidateU3Ec__Iterator46_t3243744428 *)L_2;
		U3CValidateU3Ec__Iterator46_t3243744428 * L_3 = V_0;
		KeyedFactoryBase_2_t2864689411 * L_4 = (KeyedFactoryBase_2_t2864689411 *)__this->get_U3CU3Ef__this_6();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_6(L_4);
		U3CValidateU3Ec__Iterator46_t3243744428 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_1_t4073207355_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2684159447_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CValidateU3Ec__Iterator46_MoveNext_m1431100333_MetadataUsageId;
extern "C"  bool U3CValidateU3Ec__Iterator46_MoveNext_m1431100333_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CValidateU3Ec__Iterator46_MoveNext_m1431100333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_012d;
	}

IL_0023:
	{
		KeyedFactoryBase_2_t2864689411 * L_2 = (KeyedFactoryBase_2_t2864689411 *)__this->get_U3CU3Ef__this_6();
		NullCheck(L_2);
		Dictionary_2_t1471581369 * L_3 = (Dictionary_2_t1471581369 *)L_2->get__typeMap_2();
		NullCheck((Dictionary_2_t1471581369 *)L_3);
		ValueCollection_t3393718463 * L_4 = ((  ValueCollection_t3393718463 * (*) (Dictionary_2_t1471581369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Dictionary_2_t1471581369 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((ValueCollection_t3393718463 *)L_4);
		Enumerator_t1238609311  L_5 = ((  Enumerator_t1238609311  (*) (ValueCollection_t3393718463 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ValueCollection_t3393718463 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_U3CU24s_264U3E__0_0(L_5);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_0092;
			}
		}

IL_004d:
		{
			goto IL_00fc;
		}

IL_0052:
		{
			Enumerator_t1238609311 * L_7 = (Enumerator_t1238609311 *)__this->get_address_of_U3CU24s_264U3E__0_0();
			Type_t * L_8 = ((  Type_t * (*) (Enumerator_t1238609311 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Enumerator_t1238609311 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_U3CconstructTypeU3E__1_1(L_8);
			KeyedFactoryBase_2_t2864689411 * L_9 = (KeyedFactoryBase_2_t2864689411 *)__this->get_U3CU3Ef__this_6();
			NullCheck(L_9);
			DiContainer_t2383114449 * L_10 = (DiContainer_t2383114449 *)L_9->get__container_0();
			Type_t * L_11 = (Type_t *)__this->get_U3CconstructTypeU3E__1_1();
			KeyedFactoryBase_2_t2864689411 * L_12 = (KeyedFactoryBase_2_t2864689411 *)__this->get_U3CU3Ef__this_6();
			NullCheck((KeyedFactoryBase_2_t2864689411 *)L_12);
			TypeU5BU5D_t3431720054* L_13 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(5 /* System.Type[] Zenject.KeyedFactoryBase`2<System.Object,System.Object>::get_ProvidedTypes() */, (KeyedFactoryBase_2_t2864689411 *)L_12);
			NullCheck((DiContainer_t2383114449 *)L_10);
			Il2CppObject* L_14 = DiContainer_ValidateObjectGraph_m579741446((DiContainer_t2383114449 *)L_10, (Type_t *)L_11, (TypeU5BU5D_t3431720054*)L_13, /*hidden argument*/NULL);
			NullCheck((Il2CppObject*)L_14);
			Il2CppObject* L_15 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>::GetEnumerator() */, IEnumerable_1_t4073207355_il2cpp_TypeInfo_var, (Il2CppObject*)L_14);
			__this->set_U3CU24s_265U3E__2_2(L_15);
			V_0 = (uint32_t)((int32_t)-3);
		}

IL_0092:
		try
		{ // begin try (depth: 2)
			{
				uint32_t L_16 = V_0;
				if (((int32_t)((int32_t)L_16-(int32_t)1)) == 0)
				{
					goto IL_00ce;
				}
			}

IL_009e:
			{
				goto IL_00ce;
			}

IL_00a3:
			{
				Il2CppObject* L_17 = (Il2CppObject*)__this->get_U3CU24s_265U3E__2_2();
				NullCheck((Il2CppObject*)L_17);
				ZenjectResolveException_t1201052999 * L_18 = InterfaceFuncInvoker0< ZenjectResolveException_t1201052999 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>::get_Current() */, IEnumerator_1_t2684159447_il2cpp_TypeInfo_var, (Il2CppObject*)L_17);
				__this->set_U3CerrorU3E__3_3(L_18);
				ZenjectResolveException_t1201052999 * L_19 = (ZenjectResolveException_t1201052999 *)__this->get_U3CerrorU3E__3_3();
				__this->set_U24current_5(L_19);
				__this->set_U24PC_4(1);
				V_1 = (bool)1;
				IL2CPP_LEAVE(0x12F, FINALLY_00e3);
			}

IL_00ce:
			{
				Il2CppObject* L_20 = (Il2CppObject*)__this->get_U3CU24s_265U3E__2_2();
				NullCheck((Il2CppObject *)L_20);
				bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_20);
				if (L_21)
				{
					goto IL_00a3;
				}
			}

IL_00de:
			{
				IL2CPP_LEAVE(0xFC, FINALLY_00e3);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00e3;
		}

FINALLY_00e3:
		{ // begin finally (depth: 2)
			{
				bool L_22 = V_1;
				if (!L_22)
				{
					goto IL_00e7;
				}
			}

IL_00e6:
			{
				IL2CPP_END_FINALLY(227)
			}

IL_00e7:
			{
				Il2CppObject* L_23 = (Il2CppObject*)__this->get_U3CU24s_265U3E__2_2();
				if (L_23)
				{
					goto IL_00f0;
				}
			}

IL_00ef:
			{
				IL2CPP_END_FINALLY(227)
			}

IL_00f0:
			{
				Il2CppObject* L_24 = (Il2CppObject*)__this->get_U3CU24s_265U3E__2_2();
				NullCheck((Il2CppObject *)L_24);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_24);
				IL2CPP_END_FINALLY(227)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(227)
		{
			IL2CPP_END_CLEANUP(0x12F, FINALLY_0111);
			IL2CPP_JUMP_TBL(0xFC, IL_00fc)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00fc:
		{
			Enumerator_t1238609311 * L_25 = (Enumerator_t1238609311 *)__this->get_address_of_U3CU24s_264U3E__0_0();
			bool L_26 = ((  bool (*) (Enumerator_t1238609311 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t1238609311 *)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			if (L_26)
			{
				goto IL_0052;
			}
		}

IL_010c:
		{
			IL2CPP_LEAVE(0x126, FINALLY_0111);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0111;
	}

FINALLY_0111:
	{ // begin finally (depth: 1)
		{
			bool L_27 = V_1;
			if (!L_27)
			{
				goto IL_0115;
			}
		}

IL_0114:
		{
			IL2CPP_END_FINALLY(273)
		}

IL_0115:
		{
			Enumerator_t1238609311  L_28 = (Enumerator_t1238609311 )__this->get_U3CU24s_264U3E__0_0();
			Enumerator_t1238609311  L_29 = L_28;
			Il2CppObject * L_30 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_29);
			NullCheck((Il2CppObject *)L_30);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_30);
			IL2CPP_END_FINALLY(273)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(273)
	{
		IL2CPP_JUMP_TBL(0x12F, IL_012f)
		IL2CPP_JUMP_TBL(0x126, IL_0126)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0126:
	{
		__this->set_U24PC_4((-1));
	}

IL_012d:
	{
		return (bool)0;
	}

IL_012f:
	{
		return (bool)1;
	}
	// Dead block : IL_0131: ldloc.2
}
// System.Void Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CValidateU3Ec__Iterator46_Dispose_m912640868_MetadataUsageId;
extern "C"  void U3CValidateU3Ec__Iterator46_Dispose_m912640868_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CValidateU3Ec__Iterator46_Dispose_m912640868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0051;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0051;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_LEAVE(0x3B, FINALLY_0026);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0026;
		}

FINALLY_0026:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_265U3E__2_2();
				if (L_2)
				{
					goto IL_002f;
				}
			}

IL_002e:
			{
				IL2CPP_END_FINALLY(38)
			}

IL_002f:
			{
				Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_265U3E__2_2();
				NullCheck((Il2CppObject *)L_3);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
				IL2CPP_END_FINALLY(38)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(38)
		{
			IL2CPP_JUMP_TBL(0x3B, IL_003b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x51, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_t1238609311  L_4 = (Enumerator_t1238609311 )__this->get_U3CU24s_264U3E__0_0();
		Enumerator_t1238609311  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_5);
		NullCheck((Il2CppObject *)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0051:
	{
		return;
	}
}
// System.Void Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CValidateU3Ec__Iterator46_Reset_m3801671572_MetadataUsageId;
extern "C"  void U3CValidateU3Ec__Iterator46_Reset_m3801671572_gshared (U3CValidateU3Ec__Iterator46_t3243744428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CValidateU3Ec__Iterator46_Reset_m3801671572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Zenject.KeyedFactoryBase`2<System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactoryBase_2__ctor_m1400309807_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.DiContainer Zenject.KeyedFactoryBase`2<System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * KeyedFactoryBase_2_get_Container_m199406203_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = (DiContainer_t2383114449 *)__this->get__container_0();
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TKey> Zenject.KeyedFactoryBase`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* KeyedFactoryBase_2_get_Keys_m1582078485_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1471581369 * L_0 = (Dictionary_2_t1471581369 *)__this->get__typeMap_2();
		NullCheck((Dictionary_2_t1471581369 *)L_0);
		KeyCollection_t3794856649 * L_1 = ((  KeyCollection_t3794856649 * (*) (Dictionary_2_t1471581369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1471581369 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_1;
	}
}
// System.Collections.Generic.Dictionary`2<TKey,System.Type> Zenject.KeyedFactoryBase`2<System.Object,System.Object>::get_TypeMap()
extern "C"  Dictionary_2_t1471581369 * KeyedFactoryBase_2_get_TypeMap_m2764580472_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1471581369 * L_0 = (Dictionary_2_t1471581369 *)__this->get__typeMap_2();
		return L_0;
	}
}
// System.Void Zenject.KeyedFactoryBase`2<System.Object,System.Object>::Initialize()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2156514191;
extern Il2CppCodeGenString* _stringLiteral152512710;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t KeyedFactoryBase_2_Initialize_m1783501765_MetadataUsageId;
extern "C"  void KeyedFactoryBase_2_Initialize_m1783501765_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactoryBase_2_Initialize_m1783501765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	int32_t G_B3_0 = 0;
	List_1_t1028126887 * G_B5_0 = NULL;
	List_1_t1028126887 * G_B4_0 = NULL;
	Il2CppObject* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t11523773* G_B8_2 = NULL;
	ObjectU5BU5D_t11523773* G_B8_3 = NULL;
	String_t* G_B8_4 = NULL;
	Il2CppObject* G_B7_0 = NULL;
	int32_t G_B7_1 = 0;
	ObjectU5BU5D_t11523773* G_B7_2 = NULL;
	ObjectU5BU5D_t11523773* G_B7_3 = NULL;
	String_t* G_B7_4 = NULL;
	List_1_t1028126887 * G_B11_0 = NULL;
	KeyedFactoryBase_2_t2864689411 * G_B11_1 = NULL;
	List_1_t1028126887 * G_B10_0 = NULL;
	KeyedFactoryBase_2_t2864689411 * G_B10_1 = NULL;
	Func_2_t361610966 * G_B13_0 = NULL;
	List_1_t1028126887 * G_B13_1 = NULL;
	KeyedFactoryBase_2_t2864689411 * G_B13_2 = NULL;
	Func_2_t361610966 * G_B12_0 = NULL;
	List_1_t1028126887 * G_B12_1 = NULL;
	KeyedFactoryBase_2_t2864689411 * G_B12_2 = NULL;
	{
		Type_t * L_0 = (Type_t *)__this->get__fallbackType_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Type_t * L_1 = (Type_t *)__this->get__fallbackType_3();
		bool L_2 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Type_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
	}

IL_0019:
	{
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		Type_t * L_4 = (Type_t *)__this->get__fallbackType_3();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)G_B3_0, (String_t*)_stringLiteral2156514191, (ObjectU5BU5D_t11523773*)L_5, /*hidden argument*/NULL);
		List_1_t1028126887 * L_7 = (List_1_t1028126887 *)__this->get__typePairs_1();
		Func_2_t361610966 * L_8 = ((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_U3CU3Ef__amU24cache4_4();
		G_B4_0 = L_7;
		if (L_8)
		{
			G_B5_0 = L_7;
			goto IL_005d;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t361610966 * L_10 = (Func_2_t361610966 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t361610966 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_10, (Il2CppObject *)NULL, (IntPtr_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_U3CU3Ef__amU24cache4_4(L_10);
		G_B5_0 = G_B4_0;
	}

IL_005d:
	{
		Func_2_t361610966 * L_11 = ((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_U3CU3Ef__amU24cache4_4();
		Il2CppObject* L_12 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t361610966 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B5_0, (Func_2_t361610966 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject* L_13 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (Il2CppObject*)L_13;
		Il2CppObject* L_14 = V_0;
		bool L_15 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		if (L_15)
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_16 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject* L_17 = V_0;
		Func_2_t2267165834 * L_18 = ((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_U3CU3Ef__amU24cache5_5();
		G_B7_0 = L_17;
		G_B7_1 = 0;
		G_B7_2 = L_16;
		G_B7_3 = L_16;
		G_B7_4 = _stringLiteral152512710;
		if (L_18)
		{
			G_B8_0 = L_17;
			G_B8_1 = 0;
			G_B8_2 = L_16;
			G_B8_3 = L_16;
			G_B8_4 = _stringLiteral152512710;
			goto IL_009e;
		}
	}
	{
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Func_2_t2267165834 * L_20 = (Func_2_t2267165834 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (Func_2_t2267165834 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_20, (Il2CppObject *)NULL, (IntPtr_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_U3CU3Ef__amU24cache5_5(L_20);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
		G_B8_2 = G_B7_2;
		G_B8_3 = G_B7_3;
		G_B8_4 = G_B7_4;
	}

IL_009e:
	{
		Func_2_t2267165834 * L_21 = ((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_U3CU3Ef__amU24cache5_5();
		Il2CppObject* L_22 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2267165834 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B8_0, (Func_2_t2267165834 *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		String_t* L_23 = MiscExtensions_Join_m3896482411(NULL /*static, unused*/, (Il2CppObject*)L_22, (String_t*)_stringLiteral1396, /*hidden argument*/NULL);
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, L_23);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)L_23);
		Assert_Throw_m753765393(NULL /*static, unused*/, (String_t*)G_B8_4, (ObjectU5BU5D_t11523773*)G_B8_3, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		List_1_t1028126887 * L_24 = (List_1_t1028126887 *)__this->get__typePairs_1();
		Func_2_t361610966 * L_25 = ((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_U3CU3Ef__amU24cache6_6();
		G_B10_0 = L_24;
		G_B10_1 = ((KeyedFactoryBase_2_t2864689411 *)(__this));
		if (L_25)
		{
			G_B11_0 = L_24;
			G_B11_1 = ((KeyedFactoryBase_2_t2864689411 *)(__this));
			goto IL_00d7;
		}
	}
	{
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		Func_2_t361610966 * L_27 = (Func_2_t361610966 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t361610966 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_27, (Il2CppObject *)NULL, (IntPtr_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_U3CU3Ef__amU24cache6_6(L_27);
		G_B11_0 = G_B10_0;
		G_B11_1 = ((KeyedFactoryBase_2_t2864689411 *)(G_B10_1));
	}

IL_00d7:
	{
		Func_2_t361610966 * L_28 = ((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_U3CU3Ef__amU24cache6_6();
		Func_2_t2303734481 * L_29 = ((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_U3CU3Ef__amU24cache7_7();
		G_B12_0 = L_28;
		G_B12_1 = G_B11_0;
		G_B12_2 = ((KeyedFactoryBase_2_t2864689411 *)(G_B11_1));
		if (L_29)
		{
			G_B13_0 = L_28;
			G_B13_1 = G_B11_0;
			G_B13_2 = ((KeyedFactoryBase_2_t2864689411 *)(G_B11_1));
			goto IL_00f4;
		}
	}
	{
		IntPtr_t L_30;
		L_30.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Func_2_t2303734481 * L_31 = (Func_2_t2303734481 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		((  void (*) (Func_2_t2303734481 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)(L_31, (Il2CppObject *)NULL, (IntPtr_t)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_U3CU3Ef__amU24cache7_7(L_31);
		G_B13_0 = G_B12_0;
		G_B13_1 = G_B12_1;
		G_B13_2 = ((KeyedFactoryBase_2_t2864689411 *)(G_B12_2));
	}

IL_00f4:
	{
		Func_2_t2303734481 * L_32 = ((KeyedFactoryBase_2_t2864689411_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_U3CU3Ef__amU24cache7_7();
		Dictionary_2_t1471581369 * L_33 = ((  Dictionary_2_t1471581369 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t361610966 *, Func_2_t2303734481 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B13_1, (Func_2_t361610966 *)G_B13_0, (Func_2_t2303734481 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		NullCheck(G_B13_2);
		G_B13_2->set__typeMap_2(L_33);
		List_1_t1028126887 * L_34 = (List_1_t1028126887 *)__this->get__typePairs_1();
		NullCheck((List_1_t1028126887 *)L_34);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>::Clear() */, (List_1_t1028126887 *)L_34);
		return;
	}
}
// System.Boolean Zenject.KeyedFactoryBase`2<System.Object,System.Object>::HasKey(TKey)
extern "C"  bool KeyedFactoryBase_2_HasKey_m976801691_gshared (KeyedFactoryBase_2_t2864689411 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t1471581369 * L_0 = (Dictionary_2_t1471581369 *)__this->get__typeMap_2();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t1471581369 *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Type>::ContainsKey(!0) */, (Dictionary_2_t1471581369 *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Type Zenject.KeyedFactoryBase`2<System.Object,System.Object>::GetTypeForKey(TKey)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral37843166;
extern const uint32_t KeyedFactoryBase_2_GetTypeForKey_m3482229634_MetadataUsageId;
extern "C"  Type_t * KeyedFactoryBase_2_GetTypeForKey_m3482229634_gshared (KeyedFactoryBase_2_t2864689411 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyedFactoryBase_2_GetTypeForKey_m3482229634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Dictionary_2_t1471581369 * L_0 = (Dictionary_2_t1471581369 *)__this->get__typeMap_2();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t1471581369 *)L_0);
		bool L_2 = VirtFuncInvoker2< bool, Il2CppObject *, Type_t ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Type>::TryGetValue(!0,!1&) */, (Dictionary_2_t1471581369 *)L_0, (Il2CppObject *)L_1, (Type_t **)(&V_0));
		if (L_2)
		{
			goto IL_0039;
		}
	}
	{
		Type_t * L_3 = (Type_t *)__this->get__fallbackType_3();
		ObjectU5BU5D_t11523773* L_4 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_5 = ___key0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		Assert_IsNotNull_m3308289621(NULL /*static, unused*/, (Il2CppObject *)L_3, (String_t*)_stringLiteral37843166, (ObjectU5BU5D_t11523773*)L_4, /*hidden argument*/NULL);
		Type_t * L_6 = (Type_t *)__this->get__fallbackType_3();
		return L_6;
	}

IL_0039:
	{
		Type_t * L_7 = V_0;
		return L_7;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.KeyedFactoryBase`2<System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* KeyedFactoryBase_2_Validate_m1994894092_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method)
{
	U3CValidateU3Ec__Iterator46_t3243744428 * V_0 = NULL;
	{
		U3CValidateU3Ec__Iterator46_t3243744428 * L_0 = (U3CValidateU3Ec__Iterator46_t3243744428 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		((  void (*) (U3CValidateU3Ec__Iterator46_t3243744428 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		V_0 = (U3CValidateU3Ec__Iterator46_t3243744428 *)L_0;
		U3CValidateU3Ec__Iterator46_t3243744428 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_6(__this);
		U3CValidateU3Ec__Iterator46_t3243744428 * L_2 = V_0;
		U3CValidateU3Ec__Iterator46_t3243744428 * L_3 = (U3CValidateU3Ec__Iterator46_t3243744428 *)L_2;
		NullCheck(L_3);
		L_3->set_U24PC_4(((int32_t)-2));
		return L_3;
	}
}
// TKey Zenject.KeyedFactoryBase`2<System.Object,System.Object>::<Initialize>m__2AC(ModestTree.Util.Tuple`2<TKey,System.Type>)
extern "C"  Il2CppObject * KeyedFactoryBase_2_U3CInitializeU3Em__2AC_m1544969010_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t231167918 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t231167918 * L_0 = ___x0;
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_First_0();
		return L_1;
	}
}
// System.String Zenject.KeyedFactoryBase`2<System.Object,System.Object>::<Initialize>m__2AD(TKey)
extern "C"  String_t* KeyedFactoryBase_2_U3CInitializeU3Em__2AD_m1089584577_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___x0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)(*(&___x0)));
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&___x0)));
		return L_0;
	}
}
// TKey Zenject.KeyedFactoryBase`2<System.Object,System.Object>::<Initialize>m__2AE(ModestTree.Util.Tuple`2<TKey,System.Type>)
extern "C"  Il2CppObject * KeyedFactoryBase_2_U3CInitializeU3Em__2AE_m3790548464_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t231167918 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t231167918 * L_0 = ___x0;
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_First_0();
		return L_1;
	}
}
// System.Type Zenject.KeyedFactoryBase`2<System.Object,System.Object>::<Initialize>m__2AF(ModestTree.Util.Tuple`2<TKey,System.Type>)
extern "C"  Type_t * KeyedFactoryBase_2_U3CInitializeU3Em__2AF_m2468757815_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t231167918 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t231167918 * L_0 = ___x0;
		NullCheck(L_0);
		Type_t * L_1 = (Type_t *)L_0->get_Second_1();
		return L_1;
	}
}
// System.Void Zenject.ListFactory`1<System.Object>::.ctor(System.Collections.Generic.List`1<System.Type>,Zenject.DiContainer)
extern Il2CppClass* Enumerator_t1661971896_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3898856315_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1436972411_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1892553461_MethodInfo_var;
extern const uint32_t ListFactory_1__ctor_m3964182025_MetadataUsageId;
extern "C"  void ListFactory_1__ctor_m3964182025_gshared (ListFactory_1_t3970372955 * __this, List_1_t3576188904 * ___implTypes0, DiContainer_t2383114449 * ___container1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListFactory_1__ctor_m3964182025_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Enumerator_t1661971896  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t3576188904 * L_0 = ___implTypes0;
		NullCheck((List_1_t3576188904 *)L_0);
		Enumerator_t1661971896  L_1 = List_1_GetEnumerator_m3898856315((List_1_t3576188904 *)L_0, /*hidden argument*/List_1_GetEnumerator_m3898856315_MethodInfo_var);
		V_1 = (Enumerator_t1661971896 )L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0012:
		{
			Type_t * L_2 = Enumerator_get_Current_m1436972411((Enumerator_t1661971896 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m1436972411_MethodInfo_var);
			V_0 = (Type_t *)L_2;
			Type_t * L_3 = V_0;
			bool L_4 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_4, /*hidden argument*/NULL);
		}

IL_0025:
		{
			bool L_5 = Enumerator_MoveNext_m1892553461((Enumerator_t1661971896 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m1892553461_MethodInfo_var);
			if (L_5)
			{
				goto IL_0012;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x42, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Enumerator_t1661971896  L_6 = V_1;
		Enumerator_t1661971896  L_7 = L_6;
		Il2CppObject * L_8 = Box(Enumerator_t1661971896_il2cpp_TypeInfo_var, &L_7);
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0042:
	{
		List_1_t3576188904 * L_9 = ___implTypes0;
		__this->set__implTypes_1(L_9);
		DiContainer_t2383114449 * L_10 = ___container1;
		__this->set__container_0(L_10);
		return;
	}
}
// System.Void Zenject.ListFactory`1<System.Object>::Bind()
extern "C"  void ListFactory_1_Bind_m219167332_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.Generic.List`1<T> Zenject.ListFactory`1<System.Object>::Create(System.Object[])
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1661971896_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3898856315_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1436972411_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1892553461_MethodInfo_var;
extern const uint32_t ListFactory_1_Create_m2666892410_MetadataUsageId;
extern "C"  List_1_t1634065389 * ListFactory_1_Create_m2666892410_gshared (ListFactory_1_t3970372955 * __this, ObjectU5BU5D_t11523773* ___constructorArgs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListFactory_1_Create_m2666892410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1634065389 * V_0 = NULL;
	Type_t * V_1 = NULL;
	Enumerator_t1661971896  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (List_1_t1634065389 *)L_0;
		List_1_t3576188904 * L_1 = (List_1_t3576188904 *)__this->get__implTypes_1();
		NullCheck((List_1_t3576188904 *)L_1);
		Enumerator_t1661971896  L_2 = List_1_GetEnumerator_m3898856315((List_1_t3576188904 *)L_1, /*hidden argument*/List_1_GetEnumerator_m3898856315_MethodInfo_var);
		V_2 = (Enumerator_t1661971896 )L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0017:
		{
			Type_t * L_3 = Enumerator_get_Current_m1436972411((Enumerator_t1661971896 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m1436972411_MethodInfo_var);
			V_1 = (Type_t *)L_3;
			List_1_t1634065389 * L_4 = V_0;
			DiContainer_t2383114449 * L_5 = (DiContainer_t2383114449 *)__this->get__container_0();
			ObjectU5BU5D_t11523773* L_6 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
			Type_t * L_7 = V_1;
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
			ArrayElementTypeCheck (L_6, L_7);
			(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
			ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_6;
			ObjectU5BU5D_t11523773* L_9 = ___constructorArgs0;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
			ArrayElementTypeCheck (L_8, L_9);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
			NullCheck((DiContainer_t2383114449 *)L_5);
			Il2CppObject * L_10 = GenericVirtFuncInvoker1< Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (DiContainer_t2383114449 *)L_5, (ObjectU5BU5D_t11523773*)L_8);
			NullCheck((List_1_t1634065389 *)L_4);
			VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_4, (Il2CppObject *)L_10);
		}

IL_003e:
		{
			bool L_11 = Enumerator_MoveNext_m1892553461((Enumerator_t1661971896 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m1892553461_MethodInfo_var);
			if (L_11)
			{
				goto IL_0017;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Enumerator_t1661971896  L_12 = V_2;
		Enumerator_t1661971896  L_13 = L_12;
		Il2CppObject * L_14 = Box(Enumerator_t1661971896_il2cpp_TypeInfo_var, &L_13);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005b:
	{
		List_1_t1634065389 * L_15 = V_0;
		return L_15;
	}
}
// System.Void Zenject.MethodProvider`1/<GetInstance>c__AnonStorey195<System.Object>::.ctor()
extern "C"  void U3CGetInstanceU3Ec__AnonStorey195__ctor_m2793557463_gshared (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Zenject.MethodProvider`1/<GetInstance>c__AnonStorey195<System.Object>::<>m__303()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4149858892;
extern const uint32_t U3CGetInstanceU3Ec__AnonStorey195_U3CU3Em__303_m42889013_MetadataUsageId;
extern "C"  String_t* U3CGetInstanceU3Ec__AnonStorey195_U3CU3Em__303_m42889013_gshared (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetInstanceU3Ec__AnonStorey195_U3CU3Em__303_m42889013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		String_t* L_2 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, (Type_t *)L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)L_0;
		InjectContext_t3456483891 * L_4 = (InjectContext_t3456483891 *)__this->get_context_0();
		NullCheck((InjectContext_t3456483891 *)L_4);
		String_t* L_5 = InjectContext_GetObjectGraphString_m708666943((InjectContext_t3456483891 *)L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		String_t* L_6 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, (String_t*)_stringLiteral4149858892, (ObjectU5BU5D_t11523773*)L_3, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void Zenject.MethodProvider`1<System.Object>::.ctor(System.Func`2<Zenject.InjectContext,T>)
extern "C"  void MethodProvider_1__ctor_m1654905075_gshared (MethodProvider_1_t512585361 * __this, Func_2_t2621245597 * ___method0, const MethodInfo* method)
{
	{
		NullCheck((ProviderBase_t1627494391 *)__this);
		ProviderBase__ctor_m796348314((ProviderBase_t1627494391 *)__this, /*hidden argument*/NULL);
		Func_2_t2621245597 * L_0 = ___method0;
		__this->set__method_2(L_0);
		return;
	}
}
// System.Type Zenject.MethodProvider`1<System.Object>::GetInstanceType()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t MethodProvider_1_GetInstanceType_m1846484974_MetadataUsageId;
extern "C"  Type_t * MethodProvider_1_GetInstanceType_m1846484974_gshared (MethodProvider_1_t512585361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodProvider_1_GetInstanceType_m1846484974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Object Zenject.MethodProvider`1<System.Object>::GetInstance(Zenject.InjectContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t2111270149_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1__ctor_m1422427581_MethodInfo_var;
extern const uint32_t MethodProvider_1_GetInstance_m2279203076_MetadataUsageId;
extern "C"  Il2CppObject * MethodProvider_1_GetInstance_m2279203076_gshared (MethodProvider_1_t512585361 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodProvider_1_GetInstance_m2279203076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * V_1 = NULL;
	{
		U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * L_0 = (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_1 = (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 *)L_0;
		U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * L_1 = V_1;
		InjectContext_t3456483891 * L_2 = ___context0;
		NullCheck(L_1);
		L_1->set_context_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * L_4 = V_1;
		NullCheck(L_4);
		InjectContext_t3456483891 * L_5 = (InjectContext_t3456483891 *)L_4->get_context_0();
		NullCheck(L_5);
		Type_t * L_6 = (Type_t *)L_5->get_MemberType_6();
		bool L_7 = TypeExtensions_DerivesFromOrEqual_m3526502226(NULL /*static, unused*/, (Type_t *)L_3, (Type_t *)L_6, /*hidden argument*/NULL);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)L_7, /*hidden argument*/NULL);
		Func_2_t2621245597 * L_8 = (Func_2_t2621245597 *)__this->get__method_2();
		U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * L_9 = V_1;
		NullCheck(L_9);
		InjectContext_t3456483891 * L_10 = (InjectContext_t3456483891 *)L_9->get_context_0();
		NullCheck((Func_2_t2621245597 *)L_8);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_2_t2621245597 *, InjectContext_t3456483891 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t2621245597 *)L_8, (InjectContext_t3456483891 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (Il2CppObject *)L_11;
		Il2CppObject * L_12 = V_0;
		U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * L_13 = V_1;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Func_1_t2111270149 * L_15 = (Func_1_t2111270149 *)il2cpp_codegen_object_new(Func_1_t2111270149_il2cpp_TypeInfo_var);
		Func_1__ctor_m1422427581(L_15, (Il2CppObject *)L_13, (IntPtr_t)L_14, /*hidden argument*/Func_1__ctor_m1422427581_MethodInfo_var);
		Assert_That_m1170495047(NULL /*static, unused*/, (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_12) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), (Func_1_t2111270149 *)L_15, /*hidden argument*/NULL);
		Il2CppObject * L_16 = V_0;
		return L_16;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.MethodProvider`1<System.Object>::ValidateBinding(Zenject.InjectContext)
extern const MethodInfo* Enumerable_Empty_TisZenjectResolveException_t1201052999_m2785882006_MethodInfo_var;
extern const uint32_t MethodProvider_1_ValidateBinding_m871735702_MetadataUsageId;
extern "C"  Il2CppObject* MethodProvider_1_ValidateBinding_m871735702_gshared (MethodProvider_1_t512585361 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodProvider_1_ValidateBinding_m871735702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = Enumerable_Empty_TisZenjectResolveException_t1201052999_m2785882006(NULL /*static, unused*/, /*hidden argument*/Enumerable_Empty_TisZenjectResolveException_t1201052999_m2785882006_MethodInfo_var);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
