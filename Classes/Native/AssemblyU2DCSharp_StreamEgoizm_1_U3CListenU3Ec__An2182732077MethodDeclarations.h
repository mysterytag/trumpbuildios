﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamEgoizm`1/<Listen>c__AnonStorey12A<System.Object>
struct U3CListenU3Ec__AnonStorey12A_t2182732077;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamEgoizm`1/<Listen>c__AnonStorey12A<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey12A__ctor_m1029590466_gshared (U3CListenU3Ec__AnonStorey12A_t2182732077 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey12A__ctor_m1029590466(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey12A_t2182732077 *, const MethodInfo*))U3CListenU3Ec__AnonStorey12A__ctor_m1029590466_gshared)(__this, method)
// System.Void StreamEgoizm`1/<Listen>c__AnonStorey12A<System.Object>::<>m__1BC(T)
extern "C"  void U3CListenU3Ec__AnonStorey12A_U3CU3Em__1BC_m332712853_gshared (U3CListenU3Ec__AnonStorey12A_t2182732077 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey12A_U3CU3Em__1BC_m332712853(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey12A_t2182732077 *, Il2CppObject *, const MethodInfo*))U3CListenU3Ec__AnonStorey12A_U3CU3Em__1BC_m332712853_gshared)(__this, ____0, method)
