﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// ICell`1<System.Object>
struct ICell_1_t2388737397;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>
struct  U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226  : public Il2CppObject
{
public:
	// System.Func`2<T,System.Boolean> CellReactiveApi/<WhenOnce>c__AnonStorey107`1::filter
	Func_2_t1509682273 * ___filter_0;
	// ICell`1<T> CellReactiveApi/<WhenOnce>c__AnonStorey107`1::cell
	Il2CppObject* ___cell_1;

public:
	inline static int32_t get_offset_of_filter_0() { return static_cast<int32_t>(offsetof(U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226, ___filter_0)); }
	inline Func_2_t1509682273 * get_filter_0() const { return ___filter_0; }
	inline Func_2_t1509682273 ** get_address_of_filter_0() { return &___filter_0; }
	inline void set_filter_0(Func_2_t1509682273 * value)
	{
		___filter_0 = value;
		Il2CppCodeGenWriteBarrier(&___filter_0, value);
	}

	inline static int32_t get_offset_of_cell_1() { return static_cast<int32_t>(offsetof(U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226, ___cell_1)); }
	inline Il2CppObject* get_cell_1() const { return ___cell_1; }
	inline Il2CppObject** get_address_of_cell_1() { return &___cell_1; }
	inline void set_cell_1(Il2CppObject* value)
	{
		___cell_1 = value;
		Il2CppCodeGenWriteBarrier(&___cell_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
