﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.PrecedingSiblingIterator
struct PrecedingSiblingIterator_t3194309418;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_PrecedingSiblingIterat3194309418.h"

// System.Void System.Xml.XPath.PrecedingSiblingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void PrecedingSiblingIterator__ctor_m3406853599 (PrecedingSiblingIterator_t3194309418 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.PrecedingSiblingIterator::.ctor(System.Xml.XPath.PrecedingSiblingIterator)
extern "C"  void PrecedingSiblingIterator__ctor_m2932109425 (PrecedingSiblingIterator_t3194309418 * __this, PrecedingSiblingIterator_t3194309418 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.PrecedingSiblingIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * PrecedingSiblingIterator_Clone_m1927481107 (PrecedingSiblingIterator_t3194309418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.PrecedingSiblingIterator::MoveNextCore()
extern "C"  bool PrecedingSiblingIterator_MoveNextCore_m2380337234 (PrecedingSiblingIterator_t3194309418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.PrecedingSiblingIterator::get_ReverseAxis()
extern "C"  bool PrecedingSiblingIterator_get_ReverseAxis_m872373773 (PrecedingSiblingIterator_t3194309418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
