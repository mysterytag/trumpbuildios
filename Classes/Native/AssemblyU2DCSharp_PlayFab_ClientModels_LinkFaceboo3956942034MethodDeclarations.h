﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkFacebookAccountRequest
struct LinkFacebookAccountRequest_t3956942034;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LinkFacebookAccountRequest::.ctor()
extern "C"  void LinkFacebookAccountRequest__ctor_m3359017771 (LinkFacebookAccountRequest_t3956942034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkFacebookAccountRequest::get_AccessToken()
extern "C"  String_t* LinkFacebookAccountRequest_get_AccessToken_m1859143736 (LinkFacebookAccountRequest_t3956942034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkFacebookAccountRequest::set_AccessToken(System.String)
extern "C"  void LinkFacebookAccountRequest_set_AccessToken_m688687553 (LinkFacebookAccountRequest_t3956942034 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LinkFacebookAccountRequest::get_ForceLink()
extern "C"  Nullable_1_t3097043249  LinkFacebookAccountRequest_get_ForceLink_m511904622 (LinkFacebookAccountRequest_t3956942034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkFacebookAccountRequest::set_ForceLink(System.Nullable`1<System.Boolean>)
extern "C"  void LinkFacebookAccountRequest_set_ForceLink_m4019466775 (LinkFacebookAccountRequest_t3956942034 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
