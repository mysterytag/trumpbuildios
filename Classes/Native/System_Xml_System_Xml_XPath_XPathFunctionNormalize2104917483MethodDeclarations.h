﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionNormalizeSpace
struct XPathFunctionNormalizeSpace_t2104917483;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionNormalizeSpace::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionNormalizeSpace__ctor_m3177496007 (XPathFunctionNormalizeSpace_t2104917483 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionNormalizeSpace::get_ReturnType()
extern "C"  int32_t XPathFunctionNormalizeSpace_get_ReturnType_m873947901 (XPathFunctionNormalizeSpace_t2104917483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionNormalizeSpace::get_Peer()
extern "C"  bool XPathFunctionNormalizeSpace_get_Peer_m2843429305 (XPathFunctionNormalizeSpace_t2104917483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionNormalizeSpace::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionNormalizeSpace_Evaluate_m3453465240 (XPathFunctionNormalizeSpace_t2104917483 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionNormalizeSpace::ToString()
extern "C"  String_t* XPathFunctionNormalizeSpace_ToString_m1950784725 (XPathFunctionNormalizeSpace_t2104917483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
