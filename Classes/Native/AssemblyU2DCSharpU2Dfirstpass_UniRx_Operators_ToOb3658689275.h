﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// UniRx.Operators.ToObservableObservable`1/ToObservable<System.Object>
struct ToObservable_t1484005639;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ToObservableObservable`1/ToObservable/<Run>c__AnonStorey71<System.Object>
struct  U3CRunU3Ec__AnonStorey71_t3658689275  : public Il2CppObject
{
public:
	// UniRx.SingleAssignmentDisposable UniRx.Operators.ToObservableObservable`1/ToObservable/<Run>c__AnonStorey71::flag
	SingleAssignmentDisposable_t2336378823 * ___flag_0;
	// System.Collections.Generic.IEnumerator`1<T> UniRx.Operators.ToObservableObservable`1/ToObservable/<Run>c__AnonStorey71::e
	Il2CppObject* ___e_1;
	// UniRx.Operators.ToObservableObservable`1/ToObservable<T> UniRx.Operators.ToObservableObservable`1/ToObservable/<Run>c__AnonStorey71::<>f__this
	ToObservable_t1484005639 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_flag_0() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey71_t3658689275, ___flag_0)); }
	inline SingleAssignmentDisposable_t2336378823 * get_flag_0() const { return ___flag_0; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_flag_0() { return &___flag_0; }
	inline void set_flag_0(SingleAssignmentDisposable_t2336378823 * value)
	{
		___flag_0 = value;
		Il2CppCodeGenWriteBarrier(&___flag_0, value);
	}

	inline static int32_t get_offset_of_e_1() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey71_t3658689275, ___e_1)); }
	inline Il2CppObject* get_e_1() const { return ___e_1; }
	inline Il2CppObject** get_address_of_e_1() { return &___e_1; }
	inline void set_e_1(Il2CppObject* value)
	{
		___e_1 = value;
		Il2CppCodeGenWriteBarrier(&___e_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey71_t3658689275, ___U3CU3Ef__this_2)); }
	inline ToObservable_t1484005639 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline ToObservable_t1484005639 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(ToObservable_t1484005639 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
