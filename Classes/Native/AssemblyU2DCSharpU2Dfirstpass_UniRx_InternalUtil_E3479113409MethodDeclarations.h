﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObserver_1_t3479113409;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3874884805_gshared (EmptyObserver_1_t3479113409 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m3874884805(__this, method) ((  void (*) (EmptyObserver_1_t3479113409 *, const MethodInfo*))EmptyObserver_1__ctor_m3874884805_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3675215752_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3675215752(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3675215752_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1196677263_gshared (EmptyObserver_1_t3479113409 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1196677263(__this, method) ((  void (*) (EmptyObserver_1_t3479113409 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1196677263_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3358272316_gshared (EmptyObserver_1_t3479113409 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3358272316(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t3479113409 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3358272316_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2858355245_gshared (EmptyObserver_1_t3479113409 * __this, CollectionRemoveEvent_1_t4069760419  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2858355245(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t3479113409 *, CollectionRemoveEvent_1_t4069760419 , const MethodInfo*))EmptyObserver_1_OnNext_m2858355245_gshared)(__this, ___value0, method)
