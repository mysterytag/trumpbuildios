﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3176669691.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"

// System.Void System.Array/InternalEnumerator`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2331964149_gshared (InternalEnumerator_1_t3176669691 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2331964149(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3176669691 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2331964149_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3206032523_gshared (InternalEnumerator_1_t3176669691 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3206032523(__this, method) ((  void (*) (InternalEnumerator_1_t3176669691 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3206032523_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3466893111_gshared (InternalEnumerator_1_t3176669691 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3466893111(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3176669691 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3466893111_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2181872396_gshared (InternalEnumerator_1_t3176669691 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2181872396(__this, method) ((  void (*) (InternalEnumerator_1_t3176669691 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2181872396_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2205296759_gshared (InternalEnumerator_1_t3176669691 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2205296759(__this, method) ((  bool (*) (InternalEnumerator_1_t3176669691 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2205296759_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m4161475324_gshared (InternalEnumerator_1_t3176669691 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4161475324(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3176669691 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4161475324_gshared)(__this, method)
