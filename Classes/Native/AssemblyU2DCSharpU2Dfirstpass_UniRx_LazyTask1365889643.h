﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.BooleanDisposable
struct BooleanDisposable_t3065601722;
// System.Func`2<UniRx.LazyTask,UnityEngine.Coroutine>
struct Func_2_t2681037974;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask_TaskS2963002871.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask
struct  LazyTask_t1365889643  : public Il2CppObject
{
public:
	// UniRx.BooleanDisposable UniRx.LazyTask::cancellation
	BooleanDisposable_t3065601722 * ___cancellation_0;
	// UniRx.LazyTask/TaskStatus UniRx.LazyTask::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_cancellation_0() { return static_cast<int32_t>(offsetof(LazyTask_t1365889643, ___cancellation_0)); }
	inline BooleanDisposable_t3065601722 * get_cancellation_0() const { return ___cancellation_0; }
	inline BooleanDisposable_t3065601722 ** get_address_of_cancellation_0() { return &___cancellation_0; }
	inline void set_cancellation_0(BooleanDisposable_t3065601722 * value)
	{
		___cancellation_0 = value;
		Il2CppCodeGenWriteBarrier(&___cancellation_0, value);
	}

	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LazyTask_t1365889643, ___U3CStatusU3Ek__BackingField_1)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_1() const { return ___U3CStatusU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_1() { return &___U3CStatusU3Ek__BackingField_1; }
	inline void set_U3CStatusU3Ek__BackingField_1(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_1 = value;
	}
};

struct LazyTask_t1365889643_StaticFields
{
public:
	// System.Func`2<UniRx.LazyTask,UnityEngine.Coroutine> UniRx.LazyTask::<>f__am$cache2
	Func_2_t2681037974 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LazyTask_t1365889643_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t2681037974 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t2681037974 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t2681037974 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
