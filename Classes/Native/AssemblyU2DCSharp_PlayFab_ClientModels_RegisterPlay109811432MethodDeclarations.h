﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RegisterPlayFabUserResult
struct RegisterPlayFabUserResult_t109811432;
// System.String
struct String_t;
// PlayFab.ClientModels.UserSettings
struct UserSettings_t8286526;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserSettings8286526.h"

// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::.ctor()
extern "C"  void RegisterPlayFabUserResult__ctor_m3407118913 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::get_PlayFabId()
extern "C"  String_t* RegisterPlayFabUserResult_get_PlayFabId_m2591548321 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::set_PlayFabId(System.String)
extern "C"  void RegisterPlayFabUserResult_set_PlayFabId_m518044626 (RegisterPlayFabUserResult_t109811432 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::get_SessionTicket()
extern "C"  String_t* RegisterPlayFabUserResult_get_SessionTicket_m409142069 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::set_SessionTicket(System.String)
extern "C"  void RegisterPlayFabUserResult_set_SessionTicket_m3699424318 (RegisterPlayFabUserResult_t109811432 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::get_Username()
extern "C"  String_t* RegisterPlayFabUserResult_get_Username_m1653627813 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::set_Username(System.String)
extern "C"  void RegisterPlayFabUserResult_set_Username_m1762633356 (RegisterPlayFabUserResult_t109811432 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserSettings PlayFab.ClientModels.RegisterPlayFabUserResult::get_SettingsForUser()
extern "C"  UserSettings_t8286526 * RegisterPlayFabUserResult_get_SettingsForUser_m685996864 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::set_SettingsForUser(PlayFab.ClientModels.UserSettings)
extern "C"  void RegisterPlayFabUserResult_set_SettingsForUser_m2089266003 (RegisterPlayFabUserResult_t109811432 * __this, UserSettings_t8286526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
