﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Binder/<ToLookupBase>c__AnonStorey16E`1<System.Object>
struct U3CToLookupBaseU3Ec__AnonStorey16E_1_t2894210511;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.Binder/<ToLookupBase>c__AnonStorey16E`1<System.Object>::.ctor()
extern "C"  void U3CToLookupBaseU3Ec__AnonStorey16E_1__ctor_m3869616555_gshared (U3CToLookupBaseU3Ec__AnonStorey16E_1_t2894210511 * __this, const MethodInfo* method);
#define U3CToLookupBaseU3Ec__AnonStorey16E_1__ctor_m3869616555(__this, method) ((  void (*) (U3CToLookupBaseU3Ec__AnonStorey16E_1_t2894210511 *, const MethodInfo*))U3CToLookupBaseU3Ec__AnonStorey16E_1__ctor_m3869616555_gshared)(__this, method)
// TConcrete Zenject.Binder/<ToLookupBase>c__AnonStorey16E`1<System.Object>::<>m__28C(Zenject.InjectContext)
extern "C"  Il2CppObject * U3CToLookupBaseU3Ec__AnonStorey16E_1_U3CU3Em__28C_m1798277774_gshared (U3CToLookupBaseU3Ec__AnonStorey16E_1_t2894210511 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToLookupBaseU3Ec__AnonStorey16E_1_U3CU3Em__28C_m1798277774(__this, ___ctx0, method) ((  Il2CppObject * (*) (U3CToLookupBaseU3Ec__AnonStorey16E_1_t2894210511 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToLookupBaseU3Ec__AnonStorey16E_1_U3CU3Em__28C_m1798277774_gshared)(__this, ___ctx0, method)
