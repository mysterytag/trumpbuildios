﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>
struct SelectObservable_2_t4005727219;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Func`2<System.Int64,UnityEngine.Vector2>
struct Func_2_t1270693722;
// System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>
struct Func_3_t631773672;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m3268121001_gshared (SelectObservable_2_t4005727219 * __this, Il2CppObject* ___source0, Func_2_t1270693722 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m3268121001(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t4005727219 *, Il2CppObject*, Func_2_t1270693722 *, const MethodInfo*))SelectObservable_2__ctor_m3268121001_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m1825772199_gshared (SelectObservable_2_t4005727219 * __this, Il2CppObject* ___source0, Func_3_t631773672 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m1825772199(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t4005727219 *, Il2CppObject*, Func_3_t631773672 *, const MethodInfo*))SelectObservable_2__ctor_m1825772199_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m2470106417_gshared (SelectObservable_2_t4005727219 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectObservable_2_SubscribeCore_m2470106417(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectObservable_2_t4005727219 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectObservable_2_SubscribeCore_m2470106417_gshared)(__this, ___observer0, ___cancel1, method)
