﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>
struct ZipLatest_t2793383209;
// UniRx.Operators.ZipLatestObservable`3<System.Object,System.Object,System.Object>
struct ZipLatestObservable_3_t1540586812;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void ZipLatest__ctor_m3459730391_gshared (ZipLatest_t2793383209 * __this, ZipLatestObservable_3_t1540586812 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ZipLatest__ctor_m3459730391(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ZipLatest_t2793383209 *, ZipLatestObservable_3_t1540586812 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipLatest__ctor_m3459730391_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * ZipLatest_Run_m3869730917_gshared (ZipLatest_t2793383209 * __this, const MethodInfo* method);
#define ZipLatest_Run_m3869730917(__this, method) ((  Il2CppObject * (*) (ZipLatest_t2793383209 *, const MethodInfo*))ZipLatest_Run_m3869730917_gshared)(__this, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>::Publish()
extern "C"  void ZipLatest_Publish_m97515460_gshared (ZipLatest_t2793383209 * __this, const MethodInfo* method);
#define ZipLatest_Publish_m97515460(__this, method) ((  void (*) (ZipLatest_t2793383209 *, const MethodInfo*))ZipLatest_Publish_m97515460_gshared)(__this, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>::OnNext(TResult)
extern "C"  void ZipLatest_OnNext_m369645090_gshared (ZipLatest_t2793383209 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ZipLatest_OnNext_m369645090(__this, ___value0, method) ((  void (*) (ZipLatest_t2793383209 *, Il2CppObject *, const MethodInfo*))ZipLatest_OnNext_m369645090_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void ZipLatest_OnError_m1000564814_gshared (ZipLatest_t2793383209 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ZipLatest_OnError_m1000564814(__this, ___error0, method) ((  void (*) (ZipLatest_t2793383209 *, Exception_t1967233988 *, const MethodInfo*))ZipLatest_OnError_m1000564814_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void ZipLatest_OnCompleted_m872521889_gshared (ZipLatest_t2793383209 * __this, const MethodInfo* method);
#define ZipLatest_OnCompleted_m872521889(__this, method) ((  void (*) (ZipLatest_t2793383209 *, const MethodInfo*))ZipLatest_OnCompleted_m872521889_gshared)(__this, method)
