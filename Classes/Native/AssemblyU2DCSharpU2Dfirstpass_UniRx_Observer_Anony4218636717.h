﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/AnonymousObserver`1<System.Object>
struct  AnonymousObserver_1_t4218636717  : public Il2CppObject
{
public:
	// System.Action`1<T> UniRx.Observer/AnonymousObserver`1::onNext
	Action_1_t985559125 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/AnonymousObserver`1::onError
	Action_1_t2115686693 * ___onError_1;
	// System.Action UniRx.Observer/AnonymousObserver`1::onCompleted
	Action_t437523947 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/AnonymousObserver`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(AnonymousObserver_1_t4218636717, ___onNext_0)); }
	inline Action_1_t985559125 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t985559125 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t985559125 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier(&___onNext_0, value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(AnonymousObserver_1_t4218636717, ___onError_1)); }
	inline Action_1_t2115686693 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t2115686693 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t2115686693 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier(&___onError_1, value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(AnonymousObserver_1_t4218636717, ___onCompleted_2)); }
	inline Action_t437523947 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t437523947 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t437523947 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier(&___onCompleted_2, value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(AnonymousObserver_1_t4218636717, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
