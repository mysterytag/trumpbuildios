﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Collection_1_t90542421;
// System.Collections.Generic.IList`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IList_1_t288047005;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UniRx.CollectionAddEvent`1<System.Object>[]
struct CollectionAddEvent_1U5BU5D_t789875090;
// System.Collections.Generic.IEnumerator`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IEnumerator_1_t3899628435;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void Collection_1__ctor_m1521779071_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1521779071(__this, method) ((  void (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1__ctor_m1521779071_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m1762425078_gshared (Collection_1_t90542421 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m1762425078(__this, ___list0, method) ((  void (*) (Collection_1_t90542421 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m1762425078_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m893654812_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m893654812(__this, method) ((  bool (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m893654812_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1432237221_gshared (Collection_1_t90542421 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1432237221(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t90542421 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1432237221_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m495892576_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m495892576(__this, method) ((  Il2CppObject * (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m495892576_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3218608401_gshared (Collection_1_t90542421 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3218608401(__this, ___value0, method) ((  int32_t (*) (Collection_1_t90542421 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3218608401_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3045997787_gshared (Collection_1_t90542421 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3045997787(__this, ___value0, method) ((  bool (*) (Collection_1_t90542421 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3045997787_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3013039849_gshared (Collection_1_t90542421 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3013039849(__this, ___value0, method) ((  int32_t (*) (Collection_1_t90542421 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3013039849_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m909753492_gshared (Collection_1_t90542421 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m909753492(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t90542421 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m909753492_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m572460052_gshared (Collection_1_t90542421 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m572460052(__this, ___value0, method) ((  void (*) (Collection_1_t90542421 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m572460052_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3540986017_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3540986017(__this, method) ((  bool (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3540986017_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3972063629_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3972063629(__this, method) ((  Il2CppObject * (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3972063629_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2530176138_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2530176138(__this, method) ((  bool (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2530176138_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1228364079_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1228364079(__this, method) ((  bool (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1228364079_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m4287800276_gshared (Collection_1_t90542421 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m4287800276(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t90542421 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m4287800276_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3008697195_gshared (Collection_1_t90542421 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3008697195(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t90542421 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3008697195_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::Add(T)
extern "C"  void Collection_1_Add_m454873888_gshared (Collection_1_t90542421 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define Collection_1_Add_m454873888(__this, ___item0, method) ((  void (*) (Collection_1_t90542421 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Collection_1_Add_m454873888_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::Clear()
extern "C"  void Collection_1_Clear_m3222879658_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3222879658(__this, method) ((  void (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_Clear_m3222879658_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m4171783576_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m4171783576(__this, method) ((  void (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_ClearItems_m4171783576_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::Contains(T)
extern "C"  bool Collection_1_Contains_m688125784_gshared (Collection_1_t90542421 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m688125784(__this, ___item0, method) ((  bool (*) (Collection_1_t90542421 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Collection_1_Contains_m688125784_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m4246745872_gshared (Collection_1_t90542421 * __this, CollectionAddEvent_1U5BU5D_t789875090* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m4246745872(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t90542421 *, CollectionAddEvent_1U5BU5D_t789875090*, int32_t, const MethodInfo*))Collection_1_CopyTo_m4246745872_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3861110075_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3861110075(__this, method) ((  Il2CppObject* (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_GetEnumerator_m3861110075_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2061166484_gshared (Collection_1_t90542421 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2061166484(__this, ___item0, method) ((  int32_t (*) (Collection_1_t90542421 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Collection_1_IndexOf_m2061166484_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3092237703_gshared (Collection_1_t90542421 * __this, int32_t ___index0, CollectionAddEvent_1_t2416521987  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3092237703(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t90542421 *, int32_t, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Collection_1_Insert_m3092237703_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2865160250_gshared (Collection_1_t90542421 * __this, int32_t ___index0, CollectionAddEvent_1_t2416521987  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2865160250(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t90542421 *, int32_t, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Collection_1_InsertItem_m2865160250_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::Remove(T)
extern "C"  bool Collection_1_Remove_m1291758931_gshared (Collection_1_t90542421 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1291758931(__this, ___item0, method) ((  bool (*) (Collection_1_t90542421 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Collection_1_Remove_m1291758931_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m966090573_gshared (Collection_1_t90542421 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m966090573(__this, ___index0, method) ((  void (*) (Collection_1_t90542421 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m966090573_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3599729709_gshared (Collection_1_t90542421 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3599729709(__this, ___index0, method) ((  void (*) (Collection_1_t90542421 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3599729709_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3583926375_gshared (Collection_1_t90542421 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3583926375(__this, method) ((  int32_t (*) (Collection_1_t90542421 *, const MethodInfo*))Collection_1_get_Count_m3583926375_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::get_Item(System.Int32)
extern "C"  CollectionAddEvent_1_t2416521987  Collection_1_get_Item_m2426003857_gshared (Collection_1_t90542421 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2426003857(__this, ___index0, method) ((  CollectionAddEvent_1_t2416521987  (*) (Collection_1_t90542421 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2426003857_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1332660382_gshared (Collection_1_t90542421 * __this, int32_t ___index0, CollectionAddEvent_1_t2416521987  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1332660382(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t90542421 *, int32_t, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Collection_1_set_Item_m1332660382_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2686678043_gshared (Collection_1_t90542421 * __this, int32_t ___index0, CollectionAddEvent_1_t2416521987  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2686678043(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t90542421 *, int32_t, CollectionAddEvent_1_t2416521987 , const MethodInfo*))Collection_1_SetItem_m2686678043_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1259593684_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1259593684(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1259593684_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::ConvertItem(System.Object)
extern "C"  CollectionAddEvent_1_t2416521987  Collection_1_ConvertItem_m1509318640_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1509318640(__this /* static, unused */, ___item0, method) ((  CollectionAddEvent_1_t2416521987  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1509318640_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m675284176_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m675284176(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m675284176_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1790549616_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1790549616(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1790549616_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.CollectionAddEvent`1<System.Object>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2442051119_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2442051119(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2442051119_gshared)(__this /* static, unused */, ___list0, method)
