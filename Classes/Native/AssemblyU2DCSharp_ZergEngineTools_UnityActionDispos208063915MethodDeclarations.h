﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergEngineTools/UnityActionDisposable
struct UnityActionDisposable_t208063915;

#include "codegen/il2cpp-codegen.h"

// System.Void ZergEngineTools/UnityActionDisposable::.ctor()
extern "C"  void UnityActionDisposable__ctor_m3467829014 (UnityActionDisposable_t208063915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZergEngineTools/UnityActionDisposable::Dispose()
extern "C"  void UnityActionDisposable_Dispose_m3882311123 (UnityActionDisposable_t208063915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
