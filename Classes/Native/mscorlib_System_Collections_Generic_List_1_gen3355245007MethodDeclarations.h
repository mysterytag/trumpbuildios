﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UniRx.Unit>
struct List_1_t3355245007;
// System.Collections.Generic.IEnumerable`1<UniRx.Unit>
struct IEnumerable_1_t1135473098;
// System.Collections.Generic.IEnumerator`1<UniRx.Unit>
struct IEnumerator_1_t4041392486;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UniRx.Unit>
struct ICollection_1_t3024117424;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.Unit>
struct ReadOnlyCollection_1_t1426464090;
// UniRx.Unit[]
struct UnitU5BU5D_t1267952403;
// System.Predicate`1<UniRx.Unit>
struct Predicate_1_t3129249936;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// System.Collections.Generic.IComparer`1<UniRx.Unit>
struct IComparer_1_t963026151;
// System.Comparison`1<UniRx.Unit>
struct Comparison_1_t966993618;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1441027999.h"

// System.Void System.Collections.Generic.List`1<UniRx.Unit>::.ctor()
extern "C"  void List_1__ctor_m3227136848_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1__ctor_m3227136848(__this, method) ((  void (*) (List_1_t3355245007 *, const MethodInfo*))List_1__ctor_m3227136848_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m411748143_gshared (List_1_t3355245007 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m411748143(__this, ___collection0, method) ((  void (*) (List_1_t3355245007 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m411748143_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3274010785_gshared (List_1_t3355245007 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3274010785(__this, ___capacity0, method) ((  void (*) (List_1_t3355245007 *, int32_t, const MethodInfo*))List_1__ctor_m3274010785_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::.cctor()
extern "C"  void List_1__cctor_m774898269_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m774898269(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m774898269_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m102712410_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m102712410(__this, method) ((  Il2CppObject* (*) (List_1_t3355245007 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m102712410_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1910866932_gshared (List_1_t3355245007 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1910866932(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3355245007 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1910866932_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1679012995_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1679012995(__this, method) ((  Il2CppObject * (*) (List_1_t3355245007 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1679012995_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2223317274_gshared (List_1_t3355245007 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2223317274(__this, ___item0, method) ((  int32_t (*) (List_1_t3355245007 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2223317274_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1910717926_gshared (List_1_t3355245007 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1910717926(__this, ___item0, method) ((  bool (*) (List_1_t3355245007 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1910717926_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1002125938_gshared (List_1_t3355245007 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1002125938(__this, ___item0, method) ((  int32_t (*) (List_1_t3355245007 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1002125938_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1694750693_gshared (List_1_t3355245007 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1694750693(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3355245007 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1694750693_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m802545955_gshared (List_1_t3355245007 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m802545955(__this, ___item0, method) ((  void (*) (List_1_t3355245007 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m802545955_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2514454119_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2514454119(__this, method) ((  bool (*) (List_1_t3355245007 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2514454119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m991135670_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m991135670(__this, method) ((  bool (*) (List_1_t3355245007 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m991135670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3999015144_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3999015144(__this, method) ((  Il2CppObject * (*) (List_1_t3355245007 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3999015144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m4077965781_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m4077965781(__this, method) ((  bool (*) (List_1_t3355245007 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m4077965781_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m31366788_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m31366788(__this, method) ((  bool (*) (List_1_t3355245007 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m31366788_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m123435119_gshared (List_1_t3355245007 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m123435119(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3355245007 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m123435119_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1476763260_gshared (List_1_t3355245007 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1476763260(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3355245007 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1476763260_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::Add(T)
extern "C"  void List_1_Add_m2449548079_gshared (List_1_t3355245007 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define List_1_Add_m2449548079(__this, ___item0, method) ((  void (*) (List_1_t3355245007 *, Unit_t2558286038 , const MethodInfo*))List_1_Add_m2449548079_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1826218986_gshared (List_1_t3355245007 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1826218986(__this, ___newCount0, method) ((  void (*) (List_1_t3355245007 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1826218986_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1219498728_gshared (List_1_t3355245007 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1219498728(__this, ___collection0, method) ((  void (*) (List_1_t3355245007 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1219498728_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m480470952_gshared (List_1_t3355245007 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m480470952(__this, ___enumerable0, method) ((  void (*) (List_1_t3355245007 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m480470952_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4013252079_gshared (List_1_t3355245007 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4013252079(__this, ___collection0, method) ((  void (*) (List_1_t3355245007 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4013252079_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UniRx.Unit>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1426464090 * List_1_AsReadOnly_m2203028662_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2203028662(__this, method) ((  ReadOnlyCollection_1_t1426464090 * (*) (List_1_t3355245007 *, const MethodInfo*))List_1_AsReadOnly_m2203028662_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::Clear()
extern "C"  void List_1_Clear_m633270139_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_Clear_m633270139(__this, method) ((  void (*) (List_1_t3355245007 *, const MethodInfo*))List_1_Clear_m633270139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Unit>::Contains(T)
extern "C"  bool List_1_Contains_m1537003373_gshared (List_1_t3355245007 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define List_1_Contains_m1537003373(__this, ___item0, method) ((  bool (*) (List_1_t3355245007 *, Unit_t2558286038 , const MethodInfo*))List_1_Contains_m1537003373_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1317813599_gshared (List_1_t3355245007 * __this, UnitU5BU5D_t1267952403* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1317813599(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3355245007 *, UnitU5BU5D_t1267952403*, int32_t, const MethodInfo*))List_1_CopyTo_m1317813599_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UniRx.Unit>::Find(System.Predicate`1<T>)
extern "C"  Unit_t2558286038  List_1_Find_m1989930631_gshared (List_1_t3355245007 * __this, Predicate_1_t3129249936 * ___match0, const MethodInfo* method);
#define List_1_Find_m1989930631(__this, ___match0, method) ((  Unit_t2558286038  (*) (List_1_t3355245007 *, Predicate_1_t3129249936 *, const MethodInfo*))List_1_Find_m1989930631_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3832249444_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3129249936 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3832249444(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3129249936 *, const MethodInfo*))List_1_CheckMatch_m3832249444_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Unit>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m178636097_gshared (List_1_t3355245007 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3129249936 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m178636097(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3355245007 *, int32_t, int32_t, Predicate_1_t3129249936 *, const MethodInfo*))List_1_GetIndex_m178636097_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m3856954072_gshared (List_1_t3355245007 * __this, Action_1_t2706738743 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m3856954072(__this, ___action0, method) ((  void (*) (List_1_t3355245007 *, Action_1_t2706738743 *, const MethodInfo*))List_1_ForEach_m3856954072_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UniRx.Unit>::GetEnumerator()
extern "C"  Enumerator_t1441027999  List_1_GetEnumerator_m484938282_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m484938282(__this, method) ((  Enumerator_t1441027999  (*) (List_1_t3355245007 *, const MethodInfo*))List_1_GetEnumerator_m484938282_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Unit>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1084247659_gshared (List_1_t3355245007 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1084247659(__this, ___item0, method) ((  int32_t (*) (List_1_t3355245007 *, Unit_t2558286038 , const MethodInfo*))List_1_IndexOf_m1084247659_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3267118262_gshared (List_1_t3355245007 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3267118262(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3355245007 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3267118262_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1064180783_gshared (List_1_t3355245007 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1064180783(__this, ___index0, method) ((  void (*) (List_1_t3355245007 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1064180783_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3062374294_gshared (List_1_t3355245007 * __this, int32_t ___index0, Unit_t2558286038  ___item1, const MethodInfo* method);
#define List_1_Insert_m3062374294(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3355245007 *, int32_t, Unit_t2558286038 , const MethodInfo*))List_1_Insert_m3062374294_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m4233841547_gshared (List_1_t3355245007 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m4233841547(__this, ___collection0, method) ((  void (*) (List_1_t3355245007 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m4233841547_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Unit>::Remove(T)
extern "C"  bool List_1_Remove_m1118340776_gshared (List_1_t3355245007 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define List_1_Remove_m1118340776(__this, ___item0, method) ((  bool (*) (List_1_t3355245007 *, Unit_t2558286038 , const MethodInfo*))List_1_Remove_m1118340776_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Unit>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1697096686_gshared (List_1_t3355245007 * __this, Predicate_1_t3129249936 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1697096686(__this, ___match0, method) ((  int32_t (*) (List_1_t3355245007 *, Predicate_1_t3129249936 *, const MethodInfo*))List_1_RemoveAll_m1697096686_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m936227164_gshared (List_1_t3355245007 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m936227164(__this, ___index0, method) ((  void (*) (List_1_t3355245007 *, int32_t, const MethodInfo*))List_1_RemoveAt_m936227164_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::Reverse()
extern "C"  void List_1_Reverse_m349934480_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_Reverse_m349934480(__this, method) ((  void (*) (List_1_t3355245007 *, const MethodInfo*))List_1_Reverse_m349934480_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::Sort()
extern "C"  void List_1_Sort_m2975520274_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_Sort_m2975520274(__this, method) ((  void (*) (List_1_t3355245007 *, const MethodInfo*))List_1_Sort_m2975520274_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3406883858_gshared (List_1_t3355245007 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3406883858(__this, ___comparer0, method) ((  void (*) (List_1_t3355245007 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3406883858_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m687417573_gshared (List_1_t3355245007 * __this, Comparison_1_t966993618 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m687417573(__this, ___comparison0, method) ((  void (*) (List_1_t3355245007 *, Comparison_1_t966993618 *, const MethodInfo*))List_1_Sort_m687417573_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UniRx.Unit>::ToArray()
extern "C"  UnitU5BU5D_t1267952403* List_1_ToArray_m133512937_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_ToArray_m133512937(__this, method) ((  UnitU5BU5D_t1267952403* (*) (List_1_t3355245007 *, const MethodInfo*))List_1_ToArray_m133512937_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3686899755_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3686899755(__this, method) ((  void (*) (List_1_t3355245007 *, const MethodInfo*))List_1_TrimExcess_m3686899755_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Unit>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m567089435_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m567089435(__this, method) ((  int32_t (*) (List_1_t3355245007 *, const MethodInfo*))List_1_get_Capacity_m567089435_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3331375100_gshared (List_1_t3355245007 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3331375100(__this, ___value0, method) ((  void (*) (List_1_t3355245007 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3331375100_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Unit>::get_Count()
extern "C"  int32_t List_1_get_Count_m3364213872_gshared (List_1_t3355245007 * __this, const MethodInfo* method);
#define List_1_get_Count_m3364213872(__this, method) ((  int32_t (*) (List_1_t3355245007 *, const MethodInfo*))List_1_get_Count_m3364213872_gshared)(__this, method)
// T System.Collections.Generic.List`1<UniRx.Unit>::get_Item(System.Int32)
extern "C"  Unit_t2558286038  List_1_get_Item_m3429323714_gshared (List_1_t3355245007 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3429323714(__this, ___index0, method) ((  Unit_t2558286038  (*) (List_1_t3355245007 *, int32_t, const MethodInfo*))List_1_get_Item_m3429323714_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Unit>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2698695405_gshared (List_1_t3355245007 * __this, int32_t ___index0, Unit_t2558286038  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2698695405(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3355245007 *, int32_t, Unit_t2558286038 , const MethodInfo*))List_1_set_Item_m2698695405_gshared)(__this, ___index0, ___value1, method)
