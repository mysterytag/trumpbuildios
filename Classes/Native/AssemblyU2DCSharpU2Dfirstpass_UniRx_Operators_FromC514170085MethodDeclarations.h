﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromCoroutine`1<System.Object>
struct FromCoroutine_1_t514170085;
// System.Func`3<UniRx.IObserver`1<System.Object>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1973235155;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromCoroutine`1<System.Object>::.ctor(System.Func`3<UniRx.IObserver`1<T>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  void FromCoroutine_1__ctor_m1636771704_gshared (FromCoroutine_1_t514170085 * __this, Func_3_t1973235155 * ___coroutine0, const MethodInfo* method);
#define FromCoroutine_1__ctor_m1636771704(__this, ___coroutine0, method) ((  void (*) (FromCoroutine_1_t514170085 *, Func_3_t1973235155 *, const MethodInfo*))FromCoroutine_1__ctor_m1636771704_gshared)(__this, ___coroutine0, method)
// System.IDisposable UniRx.Operators.FromCoroutine`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FromCoroutine_1_SubscribeCore_m3803616771_gshared (FromCoroutine_1_t514170085 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromCoroutine_1_SubscribeCore_m3803616771(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromCoroutine_1_t514170085 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromCoroutine_1_SubscribeCore_m3803616771_gshared)(__this, ___observer0, ___cancel1, method)
