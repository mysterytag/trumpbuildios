﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct Action_1_t1175682986;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_4_gen1027230281.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3306708628_gshared (Action_1_t1175682986 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m3306708628(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t1175682986 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m3306708628_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m770926384_gshared (Action_1_t1175682986 * __this, Tuple_4_t1027230281  ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m770926384(__this, ___obj0, method) ((  void (*) (Action_1_t1175682986 *, Tuple_4_t1027230281 , const MethodInfo*))Action_1_Invoke_m770926384_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1426098053_gshared (Action_1_t1175682986 * __this, Tuple_4_t1027230281  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m1426098053(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t1175682986 *, Tuple_4_t1027230281 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1426098053_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1575631524_gshared (Action_1_t1175682986 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m1575631524(__this, ___result0, method) ((  void (*) (Action_1_t1175682986 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m1575631524_gshared)(__this, ___result0, method)
