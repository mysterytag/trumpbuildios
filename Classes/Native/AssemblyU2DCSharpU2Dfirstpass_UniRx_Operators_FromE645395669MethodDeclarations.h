﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2<System.Object,UnityEngine.Vector2>
struct FromEventObservable_2_t645395669;
// System.Func`2<System.Action`1<UnityEngine.Vector2>,System.Object>
struct Func_2_t3830833163;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable`2<System.Object,UnityEngine.Vector2>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m1920963834_gshared (FromEventObservable_2_t645395669 * __this, Func_2_t3830833163 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method);
#define FromEventObservable_2__ctor_m1920963834(__this, ___conversion0, ___addHandler1, ___removeHandler2, method) ((  void (*) (FromEventObservable_2_t645395669 *, Func_2_t3830833163 *, Action_1_t985559125 *, Action_1_t985559125 *, const MethodInfo*))FromEventObservable_2__ctor_m1920963834_gshared)(__this, ___conversion0, ___addHandler1, ___removeHandler2, method)
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m521755578_gshared (FromEventObservable_2_t645395669 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromEventObservable_2_SubscribeCore_m521755578(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromEventObservable_2_t645395669 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromEventObservable_2_SubscribeCore_m521755578_gshared)(__this, ___observer0, ___cancel1, method)
