﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.List`1<FriendInfo>
struct List_1_t1033429381;
// System.Action`1<System.Collections.Generic.List`1<FriendInfo>>
struct Action_1_t1181882086;
// System.Func`2<System.Object,System.String>
struct Func_2_t2267165834;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VkConnectorBase/<GetFriends>c__AnonStoreyDB
struct  U3CGetFriendsU3Ec__AnonStoreyDB_t472681730  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> VkConnectorBase/<GetFriends>c__AnonStoreyDB::exceptIds
	List_1_t1765447871 * ___exceptIds_0;
	// System.Collections.Generic.List`1<FriendInfo> VkConnectorBase/<GetFriends>c__AnonStoreyDB::newFriends
	List_1_t1033429381 * ___newFriends_1;
	// System.Action`1<System.Collections.Generic.List`1<FriendInfo>> VkConnectorBase/<GetFriends>c__AnonStoreyDB::withFriends
	Action_1_t1181882086 * ___withFriends_2;

public:
	inline static int32_t get_offset_of_exceptIds_0() { return static_cast<int32_t>(offsetof(U3CGetFriendsU3Ec__AnonStoreyDB_t472681730, ___exceptIds_0)); }
	inline List_1_t1765447871 * get_exceptIds_0() const { return ___exceptIds_0; }
	inline List_1_t1765447871 ** get_address_of_exceptIds_0() { return &___exceptIds_0; }
	inline void set_exceptIds_0(List_1_t1765447871 * value)
	{
		___exceptIds_0 = value;
		Il2CppCodeGenWriteBarrier(&___exceptIds_0, value);
	}

	inline static int32_t get_offset_of_newFriends_1() { return static_cast<int32_t>(offsetof(U3CGetFriendsU3Ec__AnonStoreyDB_t472681730, ___newFriends_1)); }
	inline List_1_t1033429381 * get_newFriends_1() const { return ___newFriends_1; }
	inline List_1_t1033429381 ** get_address_of_newFriends_1() { return &___newFriends_1; }
	inline void set_newFriends_1(List_1_t1033429381 * value)
	{
		___newFriends_1 = value;
		Il2CppCodeGenWriteBarrier(&___newFriends_1, value);
	}

	inline static int32_t get_offset_of_withFriends_2() { return static_cast<int32_t>(offsetof(U3CGetFriendsU3Ec__AnonStoreyDB_t472681730, ___withFriends_2)); }
	inline Action_1_t1181882086 * get_withFriends_2() const { return ___withFriends_2; }
	inline Action_1_t1181882086 ** get_address_of_withFriends_2() { return &___withFriends_2; }
	inline void set_withFriends_2(Action_1_t1181882086 * value)
	{
		___withFriends_2 = value;
		Il2CppCodeGenWriteBarrier(&___withFriends_2, value);
	}
};

struct U3CGetFriendsU3Ec__AnonStoreyDB_t472681730_StaticFields
{
public:
	// System.Func`2<System.Object,System.String> VkConnectorBase/<GetFriends>c__AnonStoreyDB::<>f__am$cache3
	Func_2_t2267165834 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(U3CGetFriendsU3Ec__AnonStoreyDB_t472681730_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t2267165834 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t2267165834 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t2267165834 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
