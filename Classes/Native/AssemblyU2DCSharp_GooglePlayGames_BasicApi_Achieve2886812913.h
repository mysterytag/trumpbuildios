﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Achievement
struct  Achievement_t2886812913  : public Il2CppObject
{
public:
	// System.String GooglePlayGames.BasicApi.Achievement::mId
	String_t* ___mId_0;
	// System.Boolean GooglePlayGames.BasicApi.Achievement::mIsIncremental
	bool ___mIsIncremental_1;
	// System.Boolean GooglePlayGames.BasicApi.Achievement::mIsRevealed
	bool ___mIsRevealed_2;
	// System.Boolean GooglePlayGames.BasicApi.Achievement::mIsUnlocked
	bool ___mIsUnlocked_3;
	// System.Int32 GooglePlayGames.BasicApi.Achievement::mCurrentSteps
	int32_t ___mCurrentSteps_4;
	// System.Int32 GooglePlayGames.BasicApi.Achievement::mTotalSteps
	int32_t ___mTotalSteps_5;
	// System.String GooglePlayGames.BasicApi.Achievement::mDescription
	String_t* ___mDescription_6;
	// System.String GooglePlayGames.BasicApi.Achievement::mName
	String_t* ___mName_7;

public:
	inline static int32_t get_offset_of_mId_0() { return static_cast<int32_t>(offsetof(Achievement_t2886812913, ___mId_0)); }
	inline String_t* get_mId_0() const { return ___mId_0; }
	inline String_t** get_address_of_mId_0() { return &___mId_0; }
	inline void set_mId_0(String_t* value)
	{
		___mId_0 = value;
		Il2CppCodeGenWriteBarrier(&___mId_0, value);
	}

	inline static int32_t get_offset_of_mIsIncremental_1() { return static_cast<int32_t>(offsetof(Achievement_t2886812913, ___mIsIncremental_1)); }
	inline bool get_mIsIncremental_1() const { return ___mIsIncremental_1; }
	inline bool* get_address_of_mIsIncremental_1() { return &___mIsIncremental_1; }
	inline void set_mIsIncremental_1(bool value)
	{
		___mIsIncremental_1 = value;
	}

	inline static int32_t get_offset_of_mIsRevealed_2() { return static_cast<int32_t>(offsetof(Achievement_t2886812913, ___mIsRevealed_2)); }
	inline bool get_mIsRevealed_2() const { return ___mIsRevealed_2; }
	inline bool* get_address_of_mIsRevealed_2() { return &___mIsRevealed_2; }
	inline void set_mIsRevealed_2(bool value)
	{
		___mIsRevealed_2 = value;
	}

	inline static int32_t get_offset_of_mIsUnlocked_3() { return static_cast<int32_t>(offsetof(Achievement_t2886812913, ___mIsUnlocked_3)); }
	inline bool get_mIsUnlocked_3() const { return ___mIsUnlocked_3; }
	inline bool* get_address_of_mIsUnlocked_3() { return &___mIsUnlocked_3; }
	inline void set_mIsUnlocked_3(bool value)
	{
		___mIsUnlocked_3 = value;
	}

	inline static int32_t get_offset_of_mCurrentSteps_4() { return static_cast<int32_t>(offsetof(Achievement_t2886812913, ___mCurrentSteps_4)); }
	inline int32_t get_mCurrentSteps_4() const { return ___mCurrentSteps_4; }
	inline int32_t* get_address_of_mCurrentSteps_4() { return &___mCurrentSteps_4; }
	inline void set_mCurrentSteps_4(int32_t value)
	{
		___mCurrentSteps_4 = value;
	}

	inline static int32_t get_offset_of_mTotalSteps_5() { return static_cast<int32_t>(offsetof(Achievement_t2886812913, ___mTotalSteps_5)); }
	inline int32_t get_mTotalSteps_5() const { return ___mTotalSteps_5; }
	inline int32_t* get_address_of_mTotalSteps_5() { return &___mTotalSteps_5; }
	inline void set_mTotalSteps_5(int32_t value)
	{
		___mTotalSteps_5 = value;
	}

	inline static int32_t get_offset_of_mDescription_6() { return static_cast<int32_t>(offsetof(Achievement_t2886812913, ___mDescription_6)); }
	inline String_t* get_mDescription_6() const { return ___mDescription_6; }
	inline String_t** get_address_of_mDescription_6() { return &___mDescription_6; }
	inline void set_mDescription_6(String_t* value)
	{
		___mDescription_6 = value;
		Il2CppCodeGenWriteBarrier(&___mDescription_6, value);
	}

	inline static int32_t get_offset_of_mName_7() { return static_cast<int32_t>(offsetof(Achievement_t2886812913, ___mName_7)); }
	inline String_t* get_mName_7() const { return ___mName_7; }
	inline String_t** get_address_of_mName_7() { return &___mName_7; }
	inline void set_mName_7(String_t* value)
	{
		___mName_7 = value;
		Il2CppCodeGenWriteBarrier(&___mName_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
