﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ListObserver_1_t69410794;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>
struct ImmutableList_1_t3046996525;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t1986792026;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2918058190_gshared (ListObserver_1_t69410794 * __this, ImmutableList_1_t3046996525 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2918058190(__this, ___observers0, method) ((  void (*) (ListObserver_1_t69410794 *, ImmutableList_1_t3046996525 *, const MethodInfo*))ListObserver_1__ctor_m2918058190_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3776094854_gshared (ListObserver_1_t69410794 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m3776094854(__this, method) ((  void (*) (ListObserver_1_t69410794 *, const MethodInfo*))ListObserver_1_OnCompleted_m3776094854_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3043998387_gshared (ListObserver_1_t69410794 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m3043998387(__this, ___error0, method) ((  void (*) (ListObserver_1_t69410794 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m3043998387_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3003606948_gshared (ListObserver_1_t69410794 * __this, CollectionRemoveEvent_1_t4069760419  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3003606948(__this, ___value0, method) ((  void (*) (ListObserver_1_t69410794 *, CollectionRemoveEvent_1_t4069760419 , const MethodInfo*))ListObserver_1_OnNext_m3003606948_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2030765494_gshared (ListObserver_1_t69410794 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2030765494(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t69410794 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2030765494_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m58946005_gshared (ListObserver_1_t69410794 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m58946005(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t69410794 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m58946005_gshared)(__this, ___observer0, method)
