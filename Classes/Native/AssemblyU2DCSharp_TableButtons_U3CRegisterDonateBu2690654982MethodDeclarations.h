﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey15F
struct U3CRegisterDonateButtonsU3Ec__AnonStorey15F_t2690654982;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey15F::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey15F__ctor_m26237704 (U3CRegisterDonateButtonsU3Ec__AnonStorey15F_t2690654982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey15F::<>m__25D(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey15F_U3CU3Em__25D_m4216849718 (U3CRegisterDonateButtonsU3Ec__AnonStorey15F_t2690654982 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey15F::<>m__25E(System.Boolean)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey15F_U3CU3Em__25E_m1504512586 (U3CRegisterDonateButtonsU3Ec__AnonStorey15F_t2690654982 * __this, bool ___connected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
