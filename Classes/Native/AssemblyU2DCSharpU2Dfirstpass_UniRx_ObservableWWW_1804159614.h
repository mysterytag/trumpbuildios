﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<GetWWW>c__AnonStorey80
struct  U3CGetWWWU3Ec__AnonStorey80_t1804159614  : public Il2CppObject
{
public:
	// System.String UniRx.ObservableWWW/<GetWWW>c__AnonStorey80::url
	String_t* ___url_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<GetWWW>c__AnonStorey80::headers
	Dictionary_2_t2606186806 * ___headers_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<GetWWW>c__AnonStorey80::progress
	Il2CppObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetWWWU3Ec__AnonStorey80_t1804159614, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(U3CGetWWWU3Ec__AnonStorey80_t1804159614, ___headers_1)); }
	inline Dictionary_2_t2606186806 * get_headers_1() const { return ___headers_1; }
	inline Dictionary_2_t2606186806 ** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(Dictionary_2_t2606186806 * value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier(&___headers_1, value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CGetWWWU3Ec__AnonStorey80_t1804159614, ___progress_2)); }
	inline Il2CppObject* get_progress_2() const { return ___progress_2; }
	inline Il2CppObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(Il2CppObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier(&___progress_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
