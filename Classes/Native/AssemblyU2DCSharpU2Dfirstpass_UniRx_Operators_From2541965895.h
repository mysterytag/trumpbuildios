﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.FromEventObservable`2<System.Object,System.Object>
struct FromEventObservable_2_t2252139597;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>
struct  FromEvent_t2541965895  : public Il2CppObject
{
public:
	// UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs> UniRx.Operators.FromEventObservable`2/FromEvent::parent
	FromEventObservable_2_t2252139597 * ___parent_0;
	// UniRx.IObserver`1<TEventArgs> UniRx.Operators.FromEventObservable`2/FromEvent::observer
	Il2CppObject* ___observer_1;
	// TDelegate UniRx.Operators.FromEventObservable`2/FromEvent::handler
	Il2CppObject * ___handler_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(FromEvent_t2541965895, ___parent_0)); }
	inline FromEventObservable_2_t2252139597 * get_parent_0() const { return ___parent_0; }
	inline FromEventObservable_2_t2252139597 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(FromEventObservable_2_t2252139597 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_observer_1() { return static_cast<int32_t>(offsetof(FromEvent_t2541965895, ___observer_1)); }
	inline Il2CppObject* get_observer_1() const { return ___observer_1; }
	inline Il2CppObject** get_address_of_observer_1() { return &___observer_1; }
	inline void set_observer_1(Il2CppObject* value)
	{
		___observer_1 = value;
		Il2CppCodeGenWriteBarrier(&___observer_1, value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(FromEvent_t2541965895, ___handler_2)); }
	inline Il2CppObject * get_handler_2() const { return ___handler_2; }
	inline Il2CppObject ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(Il2CppObject * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier(&___handler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
