﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/EveryAfterUpdateInvoker
struct  EveryAfterUpdateInvoker_t3949333456  : public Il2CppObject
{
public:
	// System.Int64 UniRx.Observable/EveryAfterUpdateInvoker::count
	int64_t ___count_0;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/EveryAfterUpdateInvoker::observer
	Il2CppObject* ___observer_1;
	// UniRx.CancellationToken UniRx.Observable/EveryAfterUpdateInvoker::cancellationToken
	CancellationToken_t1439151560  ___cancellationToken_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(EveryAfterUpdateInvoker_t3949333456, ___count_0)); }
	inline int64_t get_count_0() const { return ___count_0; }
	inline int64_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int64_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_observer_1() { return static_cast<int32_t>(offsetof(EveryAfterUpdateInvoker_t3949333456, ___observer_1)); }
	inline Il2CppObject* get_observer_1() const { return ___observer_1; }
	inline Il2CppObject** get_address_of_observer_1() { return &___observer_1; }
	inline void set_observer_1(Il2CppObject* value)
	{
		___observer_1 = value;
		Il2CppCodeGenWriteBarrier(&___observer_1, value);
	}

	inline static int32_t get_offset_of_cancellationToken_2() { return static_cast<int32_t>(offsetof(EveryAfterUpdateInvoker_t3949333456, ___cancellationToken_2)); }
	inline CancellationToken_t1439151560  get_cancellationToken_2() const { return ___cancellationToken_2; }
	inline CancellationToken_t1439151560 * get_address_of_cancellationToken_2() { return &___cancellationToken_2; }
	inline void set_cancellationToken_2(CancellationToken_t1439151560  value)
	{
		___cancellationToken_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
