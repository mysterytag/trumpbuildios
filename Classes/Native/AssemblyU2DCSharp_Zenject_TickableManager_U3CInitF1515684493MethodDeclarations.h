﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TickableManager/<InitFixedTickables>c__AnonStorey192
struct U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.TickableManager/<InitFixedTickables>c__AnonStorey192::.ctor()
extern "C"  void U3CInitFixedTickablesU3Ec__AnonStorey192__ctor_m4133438902 (U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TickableManager/<InitFixedTickables>c__AnonStorey192::<>m__2E9(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  bool U3CInitFixedTickablesU3Ec__AnonStorey192_U3CU3Em__2E9_m2563333851 (U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * __this, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
