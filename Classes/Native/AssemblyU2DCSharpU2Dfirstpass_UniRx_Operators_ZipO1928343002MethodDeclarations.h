﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Zip_t1928343002;
// UniRx.Operators.ZipObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_6_t2421455052;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`6<T1,T2,T3,T4,T5,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Zip__ctor_m81469942_gshared (Zip_t1928343002 * __this, ZipObservable_6_t2421455052 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Zip__ctor_m81469942(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Zip_t1928343002 *, ZipObservable_6_t2421455052 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Zip__ctor_m81469942_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * Zip_Run_m1673240624_gshared (Zip_t1928343002 * __this, const MethodInfo* method);
#define Zip_Run_m1673240624(__this, method) ((  Il2CppObject * (*) (Zip_t1928343002 *, const MethodInfo*))Zip_Run_m1673240624_gshared)(__this, method)
// TR UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * Zip_GetResult_m1553646540_gshared (Zip_t1928343002 * __this, const MethodInfo* method);
#define Zip_GetResult_m1553646540(__this, method) ((  Il2CppObject * (*) (Zip_t1928343002 *, const MethodInfo*))Zip_GetResult_m1553646540_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void Zip_OnNext_m4005943830_gshared (Zip_t1928343002 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Zip_OnNext_m4005943830(__this, ___value0, method) ((  void (*) (Zip_t1928343002 *, Il2CppObject *, const MethodInfo*))Zip_OnNext_m4005943830_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Zip_OnError_m2277964633_gshared (Zip_t1928343002 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Zip_OnError_m2277964633(__this, ___error0, method) ((  void (*) (Zip_t1928343002 *, Exception_t1967233988 *, const MethodInfo*))Zip_OnError_m2277964633_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void Zip_OnCompleted_m2499357740_gshared (Zip_t1928343002 * __this, const MethodInfo* method);
#define Zip_OnCompleted_m2499357740(__this, method) ((  void (*) (Zip_t1928343002 *, const MethodInfo*))Zip_OnCompleted_m2499357740_gshared)(__this, method)
// System.Void UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::<Run>m__94()
extern "C"  void Zip_U3CRunU3Em__94_m1245155483_gshared (Zip_t1928343002 * __this, const MethodInfo* method);
#define Zip_U3CRunU3Em__94_m1245155483(__this, method) ((  void (*) (Zip_t1928343002 *, const MethodInfo*))Zip_U3CRunU3Em__94_m1245155483_gshared)(__this, method)
