﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Join>c__AnonStorey117`1<System.Object>
struct U3CJoinU3Ec__AnonStorey117_1_t818129546;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Join>c__AnonStorey117`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey117_1__ctor_m1889892760_gshared (U3CJoinU3Ec__AnonStorey117_1_t818129546 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey117_1__ctor_m1889892760(__this, method) ((  void (*) (U3CJoinU3Ec__AnonStorey117_1_t818129546 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey117_1__ctor_m1889892760_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Join>c__AnonStorey117`1<System.Object>::<>m__18A(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey117_1_U3CU3Em__18A_m3958291610_gshared (U3CJoinU3Ec__AnonStorey117_1_t818129546 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey117_1_U3CU3Em__18A_m3958291610(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CJoinU3Ec__AnonStorey117_1_t818129546 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CJoinU3Ec__AnonStorey117_1_U3CU3Em__18A_m3958291610_gshared)(__this, ___reaction0, ___p1, method)
