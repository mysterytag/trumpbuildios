﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>[]
struct Action_1U5BU5D_t3271680696;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ThreadSafeQueueWorker
struct  ThreadSafeQueueWorker_t3889225637  : public Il2CppObject
{
public:
	// System.Object UniRx.InternalUtil.ThreadSafeQueueWorker::gate
	Il2CppObject * ___gate_2;
	// System.Boolean UniRx.InternalUtil.ThreadSafeQueueWorker::dequing
	bool ___dequing_3;
	// System.Int32 UniRx.InternalUtil.ThreadSafeQueueWorker::actionListCount
	int32_t ___actionListCount_4;
	// System.Action`1<System.Object>[] UniRx.InternalUtil.ThreadSafeQueueWorker::actionList
	Action_1U5BU5D_t3271680696* ___actionList_5;
	// System.Object[] UniRx.InternalUtil.ThreadSafeQueueWorker::actionStates
	ObjectU5BU5D_t11523773* ___actionStates_6;
	// System.Int32 UniRx.InternalUtil.ThreadSafeQueueWorker::waitingListCount
	int32_t ___waitingListCount_7;
	// System.Action`1<System.Object>[] UniRx.InternalUtil.ThreadSafeQueueWorker::waitingList
	Action_1U5BU5D_t3271680696* ___waitingList_8;
	// System.Object[] UniRx.InternalUtil.ThreadSafeQueueWorker::waitingStates
	ObjectU5BU5D_t11523773* ___waitingStates_9;

public:
	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t3889225637, ___gate_2)); }
	inline Il2CppObject * get_gate_2() const { return ___gate_2; }
	inline Il2CppObject ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(Il2CppObject * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier(&___gate_2, value);
	}

	inline static int32_t get_offset_of_dequing_3() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t3889225637, ___dequing_3)); }
	inline bool get_dequing_3() const { return ___dequing_3; }
	inline bool* get_address_of_dequing_3() { return &___dequing_3; }
	inline void set_dequing_3(bool value)
	{
		___dequing_3 = value;
	}

	inline static int32_t get_offset_of_actionListCount_4() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t3889225637, ___actionListCount_4)); }
	inline int32_t get_actionListCount_4() const { return ___actionListCount_4; }
	inline int32_t* get_address_of_actionListCount_4() { return &___actionListCount_4; }
	inline void set_actionListCount_4(int32_t value)
	{
		___actionListCount_4 = value;
	}

	inline static int32_t get_offset_of_actionList_5() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t3889225637, ___actionList_5)); }
	inline Action_1U5BU5D_t3271680696* get_actionList_5() const { return ___actionList_5; }
	inline Action_1U5BU5D_t3271680696** get_address_of_actionList_5() { return &___actionList_5; }
	inline void set_actionList_5(Action_1U5BU5D_t3271680696* value)
	{
		___actionList_5 = value;
		Il2CppCodeGenWriteBarrier(&___actionList_5, value);
	}

	inline static int32_t get_offset_of_actionStates_6() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t3889225637, ___actionStates_6)); }
	inline ObjectU5BU5D_t11523773* get_actionStates_6() const { return ___actionStates_6; }
	inline ObjectU5BU5D_t11523773** get_address_of_actionStates_6() { return &___actionStates_6; }
	inline void set_actionStates_6(ObjectU5BU5D_t11523773* value)
	{
		___actionStates_6 = value;
		Il2CppCodeGenWriteBarrier(&___actionStates_6, value);
	}

	inline static int32_t get_offset_of_waitingListCount_7() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t3889225637, ___waitingListCount_7)); }
	inline int32_t get_waitingListCount_7() const { return ___waitingListCount_7; }
	inline int32_t* get_address_of_waitingListCount_7() { return &___waitingListCount_7; }
	inline void set_waitingListCount_7(int32_t value)
	{
		___waitingListCount_7 = value;
	}

	inline static int32_t get_offset_of_waitingList_8() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t3889225637, ___waitingList_8)); }
	inline Action_1U5BU5D_t3271680696* get_waitingList_8() const { return ___waitingList_8; }
	inline Action_1U5BU5D_t3271680696** get_address_of_waitingList_8() { return &___waitingList_8; }
	inline void set_waitingList_8(Action_1U5BU5D_t3271680696* value)
	{
		___waitingList_8 = value;
		Il2CppCodeGenWriteBarrier(&___waitingList_8, value);
	}

	inline static int32_t get_offset_of_waitingStates_9() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t3889225637, ___waitingStates_9)); }
	inline ObjectU5BU5D_t11523773* get_waitingStates_9() const { return ___waitingStates_9; }
	inline ObjectU5BU5D_t11523773** get_address_of_waitingStates_9() { return &___waitingStates_9; }
	inline void set_waitingStates_9(ObjectU5BU5D_t11523773* value)
	{
		___waitingStates_9 = value;
		Il2CppCodeGenWriteBarrier(&___waitingStates_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
