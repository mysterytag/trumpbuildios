﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t490482111;
// UniRx.Subject`1<System.Boolean>
struct Subject_1_t2149039961;
// UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>
struct Subject_1_t1594179034;
// UniRx.Subject`1<UnityEngine.Collision>
struct Subject_1_t3057572635;
// UniRx.Subject`1<UnityEngine.Collision2D>
struct Subject_1_t2390844653;
// UniRx.Subject`1<UnityEngine.ControllerColliderHit>
struct Subject_1_t336133548;
// UniRx.Subject`1<System.Single>
struct Subject_1_t2896243641;
// UniRx.Subject`1<UnityEngine.GameObject>
struct Subject_1_t1655762426;
// UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>
struct Subject_1_t3856149375;
// UniRx.Subject`1<UnityEngine.Collider>
struct Subject_1_t2893705245;
// UniRx.Subject`1<UnityEngine.Collider2D>
struct Subject_1_t3828072815;
// UniRx.Subject`1<UnityEngine.NetworkDisconnection>
struct Subject_1_t2276795015;
// UniRx.Subject`1<UnityEngine.NetworkConnectionError>
struct Subject_1_t2961840613;
// UniRx.Subject`1<UnityEngine.MasterServerEvent>
struct Subject_1_t4215842110;
// UniRx.Subject`1<UnityEngine.NetworkMessageInfo>
struct Subject_1_t217412208;
// UniRx.Subject`1<UnityEngine.NetworkPlayer>
struct Subject_1_t3219171992;
// UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>
struct Subject_1_t919080185;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TypedMonoBehav4014417954.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableMonoBehaviour
struct  ObservableMonoBehaviour_t2173505449  : public TypedMonoBehaviour_t4014417954
{
public:
	// System.Boolean UniRx.ObservableMonoBehaviour::calledAwake
	bool ___calledAwake_2;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::awake
	Subject_1_t201353362 * ___awake_3;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::fixedUpdate
	Subject_1_t201353362 * ___fixedUpdate_4;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::lateUpdate
	Subject_1_t201353362 * ___lateUpdate_5;
	// UniRx.Subject`1<System.Int32> UniRx.ObservableMonoBehaviour::onAnimatorIK
	Subject_1_t490482111 * ___onAnimatorIK_6;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onAnimatorMove
	Subject_1_t201353362 * ___onAnimatorMove_7;
	// UniRx.Subject`1<System.Boolean> UniRx.ObservableMonoBehaviour::onApplicationFocus
	Subject_1_t2149039961 * ___onApplicationFocus_8;
	// UniRx.Subject`1<System.Boolean> UniRx.ObservableMonoBehaviour::onApplicationPause
	Subject_1_t2149039961 * ___onApplicationPause_9;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onApplicationQuit
	Subject_1_t201353362 * ___onApplicationQuit_10;
	// UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>> UniRx.ObservableMonoBehaviour::onAudioFilterRead
	Subject_1_t1594179034 * ___onAudioFilterRead_11;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onBecameInvisible
	Subject_1_t201353362 * ___onBecameInvisible_12;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onBecameVisible
	Subject_1_t201353362 * ___onBecameVisible_13;
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::onCollisionEnter
	Subject_1_t3057572635 * ___onCollisionEnter_14;
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::onCollisionEnter2D
	Subject_1_t2390844653 * ___onCollisionEnter2D_15;
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::onCollisionExit
	Subject_1_t3057572635 * ___onCollisionExit_16;
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::onCollisionExit2D
	Subject_1_t2390844653 * ___onCollisionExit2D_17;
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::onCollisionStay
	Subject_1_t3057572635 * ___onCollisionStay_18;
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::onCollisionStay2D
	Subject_1_t2390844653 * ___onCollisionStay2D_19;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onConnectedToServer
	Subject_1_t201353362 * ___onConnectedToServer_20;
	// UniRx.Subject`1<UnityEngine.ControllerColliderHit> UniRx.ObservableMonoBehaviour::onControllerColliderHit
	Subject_1_t336133548 * ___onControllerColliderHit_21;
	// System.Boolean UniRx.ObservableMonoBehaviour::calledDestroy
	bool ___calledDestroy_22;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onDestroy
	Subject_1_t201353362 * ___onDestroy_23;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onDisable
	Subject_1_t201353362 * ___onDisable_24;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onDrawGizmos
	Subject_1_t201353362 * ___onDrawGizmos_25;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onDrawGizmosSelected
	Subject_1_t201353362 * ___onDrawGizmosSelected_26;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onEnable
	Subject_1_t201353362 * ___onEnable_27;
	// UniRx.Subject`1<System.Single> UniRx.ObservableMonoBehaviour::onJointBreak
	Subject_1_t2896243641 * ___onJointBreak_28;
	// UniRx.Subject`1<System.Int32> UniRx.ObservableMonoBehaviour::onLevelWasLoaded
	Subject_1_t490482111 * ___onLevelWasLoaded_29;
	// UniRx.Subject`1<UnityEngine.GameObject> UniRx.ObservableMonoBehaviour::onParticleCollision
	Subject_1_t1655762426 * ___onParticleCollision_30;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onPostRender
	Subject_1_t201353362 * ___onPostRender_31;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onPreCull
	Subject_1_t201353362 * ___onPreCull_32;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onPreRender
	Subject_1_t201353362 * ___onPreRender_33;
	// UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>> UniRx.ObservableMonoBehaviour::onRenderImage
	Subject_1_t3856149375 * ___onRenderImage_34;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onRenderObject
	Subject_1_t201353362 * ___onRenderObject_35;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onServerInitialized
	Subject_1_t201353362 * ___onServerInitialized_36;
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::onTriggerEnter
	Subject_1_t2893705245 * ___onTriggerEnter_37;
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::onTriggerEnter2D
	Subject_1_t3828072815 * ___onTriggerEnter2D_38;
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::onTriggerExit
	Subject_1_t2893705245 * ___onTriggerExit_39;
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::onTriggerExit2D
	Subject_1_t3828072815 * ___onTriggerExit2D_40;
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::onTriggerStay
	Subject_1_t2893705245 * ___onTriggerStay_41;
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::onTriggerStay2D
	Subject_1_t3828072815 * ___onTriggerStay2D_42;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onValidate
	Subject_1_t201353362 * ___onValidate_43;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::onWillRenderObject
	Subject_1_t201353362 * ___onWillRenderObject_44;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::reset
	Subject_1_t201353362 * ___reset_45;
	// System.Boolean UniRx.ObservableMonoBehaviour::calledStart
	bool ___calledStart_46;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::start
	Subject_1_t201353362 * ___start_47;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::update
	Subject_1_t201353362 * ___update_48;
	// UniRx.Subject`1<UnityEngine.NetworkDisconnection> UniRx.ObservableMonoBehaviour::onDisconnectedFromServer
	Subject_1_t2276795015 * ___onDisconnectedFromServer_49;
	// UniRx.Subject`1<UnityEngine.NetworkConnectionError> UniRx.ObservableMonoBehaviour::onFailedToConnect
	Subject_1_t2961840613 * ___onFailedToConnect_50;
	// UniRx.Subject`1<UnityEngine.NetworkConnectionError> UniRx.ObservableMonoBehaviour::onFailedToConnectToMasterServer
	Subject_1_t2961840613 * ___onFailedToConnectToMasterServer_51;
	// UniRx.Subject`1<UnityEngine.MasterServerEvent> UniRx.ObservableMonoBehaviour::onMasterServerEvent
	Subject_1_t4215842110 * ___onMasterServerEvent_52;
	// UniRx.Subject`1<UnityEngine.NetworkMessageInfo> UniRx.ObservableMonoBehaviour::onNetworkInstantiate
	Subject_1_t217412208 * ___onNetworkInstantiate_53;
	// UniRx.Subject`1<UnityEngine.NetworkPlayer> UniRx.ObservableMonoBehaviour::onPlayerConnected
	Subject_1_t3219171992 * ___onPlayerConnected_54;
	// UniRx.Subject`1<UnityEngine.NetworkPlayer> UniRx.ObservableMonoBehaviour::onPlayerDisconnected
	Subject_1_t3219171992 * ___onPlayerDisconnected_55;
	// UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>> UniRx.ObservableMonoBehaviour::onSerializeNetworkView
	Subject_1_t919080185 * ___onSerializeNetworkView_56;

public:
	inline static int32_t get_offset_of_calledAwake_2() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___calledAwake_2)); }
	inline bool get_calledAwake_2() const { return ___calledAwake_2; }
	inline bool* get_address_of_calledAwake_2() { return &___calledAwake_2; }
	inline void set_calledAwake_2(bool value)
	{
		___calledAwake_2 = value;
	}

	inline static int32_t get_offset_of_awake_3() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___awake_3)); }
	inline Subject_1_t201353362 * get_awake_3() const { return ___awake_3; }
	inline Subject_1_t201353362 ** get_address_of_awake_3() { return &___awake_3; }
	inline void set_awake_3(Subject_1_t201353362 * value)
	{
		___awake_3 = value;
		Il2CppCodeGenWriteBarrier(&___awake_3, value);
	}

	inline static int32_t get_offset_of_fixedUpdate_4() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___fixedUpdate_4)); }
	inline Subject_1_t201353362 * get_fixedUpdate_4() const { return ___fixedUpdate_4; }
	inline Subject_1_t201353362 ** get_address_of_fixedUpdate_4() { return &___fixedUpdate_4; }
	inline void set_fixedUpdate_4(Subject_1_t201353362 * value)
	{
		___fixedUpdate_4 = value;
		Il2CppCodeGenWriteBarrier(&___fixedUpdate_4, value);
	}

	inline static int32_t get_offset_of_lateUpdate_5() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___lateUpdate_5)); }
	inline Subject_1_t201353362 * get_lateUpdate_5() const { return ___lateUpdate_5; }
	inline Subject_1_t201353362 ** get_address_of_lateUpdate_5() { return &___lateUpdate_5; }
	inline void set_lateUpdate_5(Subject_1_t201353362 * value)
	{
		___lateUpdate_5 = value;
		Il2CppCodeGenWriteBarrier(&___lateUpdate_5, value);
	}

	inline static int32_t get_offset_of_onAnimatorIK_6() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onAnimatorIK_6)); }
	inline Subject_1_t490482111 * get_onAnimatorIK_6() const { return ___onAnimatorIK_6; }
	inline Subject_1_t490482111 ** get_address_of_onAnimatorIK_6() { return &___onAnimatorIK_6; }
	inline void set_onAnimatorIK_6(Subject_1_t490482111 * value)
	{
		___onAnimatorIK_6 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimatorIK_6, value);
	}

	inline static int32_t get_offset_of_onAnimatorMove_7() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onAnimatorMove_7)); }
	inline Subject_1_t201353362 * get_onAnimatorMove_7() const { return ___onAnimatorMove_7; }
	inline Subject_1_t201353362 ** get_address_of_onAnimatorMove_7() { return &___onAnimatorMove_7; }
	inline void set_onAnimatorMove_7(Subject_1_t201353362 * value)
	{
		___onAnimatorMove_7 = value;
		Il2CppCodeGenWriteBarrier(&___onAnimatorMove_7, value);
	}

	inline static int32_t get_offset_of_onApplicationFocus_8() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onApplicationFocus_8)); }
	inline Subject_1_t2149039961 * get_onApplicationFocus_8() const { return ___onApplicationFocus_8; }
	inline Subject_1_t2149039961 ** get_address_of_onApplicationFocus_8() { return &___onApplicationFocus_8; }
	inline void set_onApplicationFocus_8(Subject_1_t2149039961 * value)
	{
		___onApplicationFocus_8 = value;
		Il2CppCodeGenWriteBarrier(&___onApplicationFocus_8, value);
	}

	inline static int32_t get_offset_of_onApplicationPause_9() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onApplicationPause_9)); }
	inline Subject_1_t2149039961 * get_onApplicationPause_9() const { return ___onApplicationPause_9; }
	inline Subject_1_t2149039961 ** get_address_of_onApplicationPause_9() { return &___onApplicationPause_9; }
	inline void set_onApplicationPause_9(Subject_1_t2149039961 * value)
	{
		___onApplicationPause_9 = value;
		Il2CppCodeGenWriteBarrier(&___onApplicationPause_9, value);
	}

	inline static int32_t get_offset_of_onApplicationQuit_10() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onApplicationQuit_10)); }
	inline Subject_1_t201353362 * get_onApplicationQuit_10() const { return ___onApplicationQuit_10; }
	inline Subject_1_t201353362 ** get_address_of_onApplicationQuit_10() { return &___onApplicationQuit_10; }
	inline void set_onApplicationQuit_10(Subject_1_t201353362 * value)
	{
		___onApplicationQuit_10 = value;
		Il2CppCodeGenWriteBarrier(&___onApplicationQuit_10, value);
	}

	inline static int32_t get_offset_of_onAudioFilterRead_11() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onAudioFilterRead_11)); }
	inline Subject_1_t1594179034 * get_onAudioFilterRead_11() const { return ___onAudioFilterRead_11; }
	inline Subject_1_t1594179034 ** get_address_of_onAudioFilterRead_11() { return &___onAudioFilterRead_11; }
	inline void set_onAudioFilterRead_11(Subject_1_t1594179034 * value)
	{
		___onAudioFilterRead_11 = value;
		Il2CppCodeGenWriteBarrier(&___onAudioFilterRead_11, value);
	}

	inline static int32_t get_offset_of_onBecameInvisible_12() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onBecameInvisible_12)); }
	inline Subject_1_t201353362 * get_onBecameInvisible_12() const { return ___onBecameInvisible_12; }
	inline Subject_1_t201353362 ** get_address_of_onBecameInvisible_12() { return &___onBecameInvisible_12; }
	inline void set_onBecameInvisible_12(Subject_1_t201353362 * value)
	{
		___onBecameInvisible_12 = value;
		Il2CppCodeGenWriteBarrier(&___onBecameInvisible_12, value);
	}

	inline static int32_t get_offset_of_onBecameVisible_13() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onBecameVisible_13)); }
	inline Subject_1_t201353362 * get_onBecameVisible_13() const { return ___onBecameVisible_13; }
	inline Subject_1_t201353362 ** get_address_of_onBecameVisible_13() { return &___onBecameVisible_13; }
	inline void set_onBecameVisible_13(Subject_1_t201353362 * value)
	{
		___onBecameVisible_13 = value;
		Il2CppCodeGenWriteBarrier(&___onBecameVisible_13, value);
	}

	inline static int32_t get_offset_of_onCollisionEnter_14() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onCollisionEnter_14)); }
	inline Subject_1_t3057572635 * get_onCollisionEnter_14() const { return ___onCollisionEnter_14; }
	inline Subject_1_t3057572635 ** get_address_of_onCollisionEnter_14() { return &___onCollisionEnter_14; }
	inline void set_onCollisionEnter_14(Subject_1_t3057572635 * value)
	{
		___onCollisionEnter_14 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionEnter_14, value);
	}

	inline static int32_t get_offset_of_onCollisionEnter2D_15() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onCollisionEnter2D_15)); }
	inline Subject_1_t2390844653 * get_onCollisionEnter2D_15() const { return ___onCollisionEnter2D_15; }
	inline Subject_1_t2390844653 ** get_address_of_onCollisionEnter2D_15() { return &___onCollisionEnter2D_15; }
	inline void set_onCollisionEnter2D_15(Subject_1_t2390844653 * value)
	{
		___onCollisionEnter2D_15 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionEnter2D_15, value);
	}

	inline static int32_t get_offset_of_onCollisionExit_16() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onCollisionExit_16)); }
	inline Subject_1_t3057572635 * get_onCollisionExit_16() const { return ___onCollisionExit_16; }
	inline Subject_1_t3057572635 ** get_address_of_onCollisionExit_16() { return &___onCollisionExit_16; }
	inline void set_onCollisionExit_16(Subject_1_t3057572635 * value)
	{
		___onCollisionExit_16 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionExit_16, value);
	}

	inline static int32_t get_offset_of_onCollisionExit2D_17() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onCollisionExit2D_17)); }
	inline Subject_1_t2390844653 * get_onCollisionExit2D_17() const { return ___onCollisionExit2D_17; }
	inline Subject_1_t2390844653 ** get_address_of_onCollisionExit2D_17() { return &___onCollisionExit2D_17; }
	inline void set_onCollisionExit2D_17(Subject_1_t2390844653 * value)
	{
		___onCollisionExit2D_17 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionExit2D_17, value);
	}

	inline static int32_t get_offset_of_onCollisionStay_18() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onCollisionStay_18)); }
	inline Subject_1_t3057572635 * get_onCollisionStay_18() const { return ___onCollisionStay_18; }
	inline Subject_1_t3057572635 ** get_address_of_onCollisionStay_18() { return &___onCollisionStay_18; }
	inline void set_onCollisionStay_18(Subject_1_t3057572635 * value)
	{
		___onCollisionStay_18 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionStay_18, value);
	}

	inline static int32_t get_offset_of_onCollisionStay2D_19() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onCollisionStay2D_19)); }
	inline Subject_1_t2390844653 * get_onCollisionStay2D_19() const { return ___onCollisionStay2D_19; }
	inline Subject_1_t2390844653 ** get_address_of_onCollisionStay2D_19() { return &___onCollisionStay2D_19; }
	inline void set_onCollisionStay2D_19(Subject_1_t2390844653 * value)
	{
		___onCollisionStay2D_19 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionStay2D_19, value);
	}

	inline static int32_t get_offset_of_onConnectedToServer_20() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onConnectedToServer_20)); }
	inline Subject_1_t201353362 * get_onConnectedToServer_20() const { return ___onConnectedToServer_20; }
	inline Subject_1_t201353362 ** get_address_of_onConnectedToServer_20() { return &___onConnectedToServer_20; }
	inline void set_onConnectedToServer_20(Subject_1_t201353362 * value)
	{
		___onConnectedToServer_20 = value;
		Il2CppCodeGenWriteBarrier(&___onConnectedToServer_20, value);
	}

	inline static int32_t get_offset_of_onControllerColliderHit_21() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onControllerColliderHit_21)); }
	inline Subject_1_t336133548 * get_onControllerColliderHit_21() const { return ___onControllerColliderHit_21; }
	inline Subject_1_t336133548 ** get_address_of_onControllerColliderHit_21() { return &___onControllerColliderHit_21; }
	inline void set_onControllerColliderHit_21(Subject_1_t336133548 * value)
	{
		___onControllerColliderHit_21 = value;
		Il2CppCodeGenWriteBarrier(&___onControllerColliderHit_21, value);
	}

	inline static int32_t get_offset_of_calledDestroy_22() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___calledDestroy_22)); }
	inline bool get_calledDestroy_22() const { return ___calledDestroy_22; }
	inline bool* get_address_of_calledDestroy_22() { return &___calledDestroy_22; }
	inline void set_calledDestroy_22(bool value)
	{
		___calledDestroy_22 = value;
	}

	inline static int32_t get_offset_of_onDestroy_23() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onDestroy_23)); }
	inline Subject_1_t201353362 * get_onDestroy_23() const { return ___onDestroy_23; }
	inline Subject_1_t201353362 ** get_address_of_onDestroy_23() { return &___onDestroy_23; }
	inline void set_onDestroy_23(Subject_1_t201353362 * value)
	{
		___onDestroy_23 = value;
		Il2CppCodeGenWriteBarrier(&___onDestroy_23, value);
	}

	inline static int32_t get_offset_of_onDisable_24() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onDisable_24)); }
	inline Subject_1_t201353362 * get_onDisable_24() const { return ___onDisable_24; }
	inline Subject_1_t201353362 ** get_address_of_onDisable_24() { return &___onDisable_24; }
	inline void set_onDisable_24(Subject_1_t201353362 * value)
	{
		___onDisable_24 = value;
		Il2CppCodeGenWriteBarrier(&___onDisable_24, value);
	}

	inline static int32_t get_offset_of_onDrawGizmos_25() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onDrawGizmos_25)); }
	inline Subject_1_t201353362 * get_onDrawGizmos_25() const { return ___onDrawGizmos_25; }
	inline Subject_1_t201353362 ** get_address_of_onDrawGizmos_25() { return &___onDrawGizmos_25; }
	inline void set_onDrawGizmos_25(Subject_1_t201353362 * value)
	{
		___onDrawGizmos_25 = value;
		Il2CppCodeGenWriteBarrier(&___onDrawGizmos_25, value);
	}

	inline static int32_t get_offset_of_onDrawGizmosSelected_26() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onDrawGizmosSelected_26)); }
	inline Subject_1_t201353362 * get_onDrawGizmosSelected_26() const { return ___onDrawGizmosSelected_26; }
	inline Subject_1_t201353362 ** get_address_of_onDrawGizmosSelected_26() { return &___onDrawGizmosSelected_26; }
	inline void set_onDrawGizmosSelected_26(Subject_1_t201353362 * value)
	{
		___onDrawGizmosSelected_26 = value;
		Il2CppCodeGenWriteBarrier(&___onDrawGizmosSelected_26, value);
	}

	inline static int32_t get_offset_of_onEnable_27() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onEnable_27)); }
	inline Subject_1_t201353362 * get_onEnable_27() const { return ___onEnable_27; }
	inline Subject_1_t201353362 ** get_address_of_onEnable_27() { return &___onEnable_27; }
	inline void set_onEnable_27(Subject_1_t201353362 * value)
	{
		___onEnable_27 = value;
		Il2CppCodeGenWriteBarrier(&___onEnable_27, value);
	}

	inline static int32_t get_offset_of_onJointBreak_28() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onJointBreak_28)); }
	inline Subject_1_t2896243641 * get_onJointBreak_28() const { return ___onJointBreak_28; }
	inline Subject_1_t2896243641 ** get_address_of_onJointBreak_28() { return &___onJointBreak_28; }
	inline void set_onJointBreak_28(Subject_1_t2896243641 * value)
	{
		___onJointBreak_28 = value;
		Il2CppCodeGenWriteBarrier(&___onJointBreak_28, value);
	}

	inline static int32_t get_offset_of_onLevelWasLoaded_29() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onLevelWasLoaded_29)); }
	inline Subject_1_t490482111 * get_onLevelWasLoaded_29() const { return ___onLevelWasLoaded_29; }
	inline Subject_1_t490482111 ** get_address_of_onLevelWasLoaded_29() { return &___onLevelWasLoaded_29; }
	inline void set_onLevelWasLoaded_29(Subject_1_t490482111 * value)
	{
		___onLevelWasLoaded_29 = value;
		Il2CppCodeGenWriteBarrier(&___onLevelWasLoaded_29, value);
	}

	inline static int32_t get_offset_of_onParticleCollision_30() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onParticleCollision_30)); }
	inline Subject_1_t1655762426 * get_onParticleCollision_30() const { return ___onParticleCollision_30; }
	inline Subject_1_t1655762426 ** get_address_of_onParticleCollision_30() { return &___onParticleCollision_30; }
	inline void set_onParticleCollision_30(Subject_1_t1655762426 * value)
	{
		___onParticleCollision_30 = value;
		Il2CppCodeGenWriteBarrier(&___onParticleCollision_30, value);
	}

	inline static int32_t get_offset_of_onPostRender_31() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onPostRender_31)); }
	inline Subject_1_t201353362 * get_onPostRender_31() const { return ___onPostRender_31; }
	inline Subject_1_t201353362 ** get_address_of_onPostRender_31() { return &___onPostRender_31; }
	inline void set_onPostRender_31(Subject_1_t201353362 * value)
	{
		___onPostRender_31 = value;
		Il2CppCodeGenWriteBarrier(&___onPostRender_31, value);
	}

	inline static int32_t get_offset_of_onPreCull_32() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onPreCull_32)); }
	inline Subject_1_t201353362 * get_onPreCull_32() const { return ___onPreCull_32; }
	inline Subject_1_t201353362 ** get_address_of_onPreCull_32() { return &___onPreCull_32; }
	inline void set_onPreCull_32(Subject_1_t201353362 * value)
	{
		___onPreCull_32 = value;
		Il2CppCodeGenWriteBarrier(&___onPreCull_32, value);
	}

	inline static int32_t get_offset_of_onPreRender_33() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onPreRender_33)); }
	inline Subject_1_t201353362 * get_onPreRender_33() const { return ___onPreRender_33; }
	inline Subject_1_t201353362 ** get_address_of_onPreRender_33() { return &___onPreRender_33; }
	inline void set_onPreRender_33(Subject_1_t201353362 * value)
	{
		___onPreRender_33 = value;
		Il2CppCodeGenWriteBarrier(&___onPreRender_33, value);
	}

	inline static int32_t get_offset_of_onRenderImage_34() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onRenderImage_34)); }
	inline Subject_1_t3856149375 * get_onRenderImage_34() const { return ___onRenderImage_34; }
	inline Subject_1_t3856149375 ** get_address_of_onRenderImage_34() { return &___onRenderImage_34; }
	inline void set_onRenderImage_34(Subject_1_t3856149375 * value)
	{
		___onRenderImage_34 = value;
		Il2CppCodeGenWriteBarrier(&___onRenderImage_34, value);
	}

	inline static int32_t get_offset_of_onRenderObject_35() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onRenderObject_35)); }
	inline Subject_1_t201353362 * get_onRenderObject_35() const { return ___onRenderObject_35; }
	inline Subject_1_t201353362 ** get_address_of_onRenderObject_35() { return &___onRenderObject_35; }
	inline void set_onRenderObject_35(Subject_1_t201353362 * value)
	{
		___onRenderObject_35 = value;
		Il2CppCodeGenWriteBarrier(&___onRenderObject_35, value);
	}

	inline static int32_t get_offset_of_onServerInitialized_36() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onServerInitialized_36)); }
	inline Subject_1_t201353362 * get_onServerInitialized_36() const { return ___onServerInitialized_36; }
	inline Subject_1_t201353362 ** get_address_of_onServerInitialized_36() { return &___onServerInitialized_36; }
	inline void set_onServerInitialized_36(Subject_1_t201353362 * value)
	{
		___onServerInitialized_36 = value;
		Il2CppCodeGenWriteBarrier(&___onServerInitialized_36, value);
	}

	inline static int32_t get_offset_of_onTriggerEnter_37() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onTriggerEnter_37)); }
	inline Subject_1_t2893705245 * get_onTriggerEnter_37() const { return ___onTriggerEnter_37; }
	inline Subject_1_t2893705245 ** get_address_of_onTriggerEnter_37() { return &___onTriggerEnter_37; }
	inline void set_onTriggerEnter_37(Subject_1_t2893705245 * value)
	{
		___onTriggerEnter_37 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerEnter_37, value);
	}

	inline static int32_t get_offset_of_onTriggerEnter2D_38() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onTriggerEnter2D_38)); }
	inline Subject_1_t3828072815 * get_onTriggerEnter2D_38() const { return ___onTriggerEnter2D_38; }
	inline Subject_1_t3828072815 ** get_address_of_onTriggerEnter2D_38() { return &___onTriggerEnter2D_38; }
	inline void set_onTriggerEnter2D_38(Subject_1_t3828072815 * value)
	{
		___onTriggerEnter2D_38 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerEnter2D_38, value);
	}

	inline static int32_t get_offset_of_onTriggerExit_39() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onTriggerExit_39)); }
	inline Subject_1_t2893705245 * get_onTriggerExit_39() const { return ___onTriggerExit_39; }
	inline Subject_1_t2893705245 ** get_address_of_onTriggerExit_39() { return &___onTriggerExit_39; }
	inline void set_onTriggerExit_39(Subject_1_t2893705245 * value)
	{
		___onTriggerExit_39 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerExit_39, value);
	}

	inline static int32_t get_offset_of_onTriggerExit2D_40() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onTriggerExit2D_40)); }
	inline Subject_1_t3828072815 * get_onTriggerExit2D_40() const { return ___onTriggerExit2D_40; }
	inline Subject_1_t3828072815 ** get_address_of_onTriggerExit2D_40() { return &___onTriggerExit2D_40; }
	inline void set_onTriggerExit2D_40(Subject_1_t3828072815 * value)
	{
		___onTriggerExit2D_40 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerExit2D_40, value);
	}

	inline static int32_t get_offset_of_onTriggerStay_41() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onTriggerStay_41)); }
	inline Subject_1_t2893705245 * get_onTriggerStay_41() const { return ___onTriggerStay_41; }
	inline Subject_1_t2893705245 ** get_address_of_onTriggerStay_41() { return &___onTriggerStay_41; }
	inline void set_onTriggerStay_41(Subject_1_t2893705245 * value)
	{
		___onTriggerStay_41 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerStay_41, value);
	}

	inline static int32_t get_offset_of_onTriggerStay2D_42() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onTriggerStay2D_42)); }
	inline Subject_1_t3828072815 * get_onTriggerStay2D_42() const { return ___onTriggerStay2D_42; }
	inline Subject_1_t3828072815 ** get_address_of_onTriggerStay2D_42() { return &___onTriggerStay2D_42; }
	inline void set_onTriggerStay2D_42(Subject_1_t3828072815 * value)
	{
		___onTriggerStay2D_42 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerStay2D_42, value);
	}

	inline static int32_t get_offset_of_onValidate_43() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onValidate_43)); }
	inline Subject_1_t201353362 * get_onValidate_43() const { return ___onValidate_43; }
	inline Subject_1_t201353362 ** get_address_of_onValidate_43() { return &___onValidate_43; }
	inline void set_onValidate_43(Subject_1_t201353362 * value)
	{
		___onValidate_43 = value;
		Il2CppCodeGenWriteBarrier(&___onValidate_43, value);
	}

	inline static int32_t get_offset_of_onWillRenderObject_44() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onWillRenderObject_44)); }
	inline Subject_1_t201353362 * get_onWillRenderObject_44() const { return ___onWillRenderObject_44; }
	inline Subject_1_t201353362 ** get_address_of_onWillRenderObject_44() { return &___onWillRenderObject_44; }
	inline void set_onWillRenderObject_44(Subject_1_t201353362 * value)
	{
		___onWillRenderObject_44 = value;
		Il2CppCodeGenWriteBarrier(&___onWillRenderObject_44, value);
	}

	inline static int32_t get_offset_of_reset_45() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___reset_45)); }
	inline Subject_1_t201353362 * get_reset_45() const { return ___reset_45; }
	inline Subject_1_t201353362 ** get_address_of_reset_45() { return &___reset_45; }
	inline void set_reset_45(Subject_1_t201353362 * value)
	{
		___reset_45 = value;
		Il2CppCodeGenWriteBarrier(&___reset_45, value);
	}

	inline static int32_t get_offset_of_calledStart_46() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___calledStart_46)); }
	inline bool get_calledStart_46() const { return ___calledStart_46; }
	inline bool* get_address_of_calledStart_46() { return &___calledStart_46; }
	inline void set_calledStart_46(bool value)
	{
		___calledStart_46 = value;
	}

	inline static int32_t get_offset_of_start_47() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___start_47)); }
	inline Subject_1_t201353362 * get_start_47() const { return ___start_47; }
	inline Subject_1_t201353362 ** get_address_of_start_47() { return &___start_47; }
	inline void set_start_47(Subject_1_t201353362 * value)
	{
		___start_47 = value;
		Il2CppCodeGenWriteBarrier(&___start_47, value);
	}

	inline static int32_t get_offset_of_update_48() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___update_48)); }
	inline Subject_1_t201353362 * get_update_48() const { return ___update_48; }
	inline Subject_1_t201353362 ** get_address_of_update_48() { return &___update_48; }
	inline void set_update_48(Subject_1_t201353362 * value)
	{
		___update_48 = value;
		Il2CppCodeGenWriteBarrier(&___update_48, value);
	}

	inline static int32_t get_offset_of_onDisconnectedFromServer_49() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onDisconnectedFromServer_49)); }
	inline Subject_1_t2276795015 * get_onDisconnectedFromServer_49() const { return ___onDisconnectedFromServer_49; }
	inline Subject_1_t2276795015 ** get_address_of_onDisconnectedFromServer_49() { return &___onDisconnectedFromServer_49; }
	inline void set_onDisconnectedFromServer_49(Subject_1_t2276795015 * value)
	{
		___onDisconnectedFromServer_49 = value;
		Il2CppCodeGenWriteBarrier(&___onDisconnectedFromServer_49, value);
	}

	inline static int32_t get_offset_of_onFailedToConnect_50() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onFailedToConnect_50)); }
	inline Subject_1_t2961840613 * get_onFailedToConnect_50() const { return ___onFailedToConnect_50; }
	inline Subject_1_t2961840613 ** get_address_of_onFailedToConnect_50() { return &___onFailedToConnect_50; }
	inline void set_onFailedToConnect_50(Subject_1_t2961840613 * value)
	{
		___onFailedToConnect_50 = value;
		Il2CppCodeGenWriteBarrier(&___onFailedToConnect_50, value);
	}

	inline static int32_t get_offset_of_onFailedToConnectToMasterServer_51() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onFailedToConnectToMasterServer_51)); }
	inline Subject_1_t2961840613 * get_onFailedToConnectToMasterServer_51() const { return ___onFailedToConnectToMasterServer_51; }
	inline Subject_1_t2961840613 ** get_address_of_onFailedToConnectToMasterServer_51() { return &___onFailedToConnectToMasterServer_51; }
	inline void set_onFailedToConnectToMasterServer_51(Subject_1_t2961840613 * value)
	{
		___onFailedToConnectToMasterServer_51 = value;
		Il2CppCodeGenWriteBarrier(&___onFailedToConnectToMasterServer_51, value);
	}

	inline static int32_t get_offset_of_onMasterServerEvent_52() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onMasterServerEvent_52)); }
	inline Subject_1_t4215842110 * get_onMasterServerEvent_52() const { return ___onMasterServerEvent_52; }
	inline Subject_1_t4215842110 ** get_address_of_onMasterServerEvent_52() { return &___onMasterServerEvent_52; }
	inline void set_onMasterServerEvent_52(Subject_1_t4215842110 * value)
	{
		___onMasterServerEvent_52 = value;
		Il2CppCodeGenWriteBarrier(&___onMasterServerEvent_52, value);
	}

	inline static int32_t get_offset_of_onNetworkInstantiate_53() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onNetworkInstantiate_53)); }
	inline Subject_1_t217412208 * get_onNetworkInstantiate_53() const { return ___onNetworkInstantiate_53; }
	inline Subject_1_t217412208 ** get_address_of_onNetworkInstantiate_53() { return &___onNetworkInstantiate_53; }
	inline void set_onNetworkInstantiate_53(Subject_1_t217412208 * value)
	{
		___onNetworkInstantiate_53 = value;
		Il2CppCodeGenWriteBarrier(&___onNetworkInstantiate_53, value);
	}

	inline static int32_t get_offset_of_onPlayerConnected_54() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onPlayerConnected_54)); }
	inline Subject_1_t3219171992 * get_onPlayerConnected_54() const { return ___onPlayerConnected_54; }
	inline Subject_1_t3219171992 ** get_address_of_onPlayerConnected_54() { return &___onPlayerConnected_54; }
	inline void set_onPlayerConnected_54(Subject_1_t3219171992 * value)
	{
		___onPlayerConnected_54 = value;
		Il2CppCodeGenWriteBarrier(&___onPlayerConnected_54, value);
	}

	inline static int32_t get_offset_of_onPlayerDisconnected_55() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onPlayerDisconnected_55)); }
	inline Subject_1_t3219171992 * get_onPlayerDisconnected_55() const { return ___onPlayerDisconnected_55; }
	inline Subject_1_t3219171992 ** get_address_of_onPlayerDisconnected_55() { return &___onPlayerDisconnected_55; }
	inline void set_onPlayerDisconnected_55(Subject_1_t3219171992 * value)
	{
		___onPlayerDisconnected_55 = value;
		Il2CppCodeGenWriteBarrier(&___onPlayerDisconnected_55, value);
	}

	inline static int32_t get_offset_of_onSerializeNetworkView_56() { return static_cast<int32_t>(offsetof(ObservableMonoBehaviour_t2173505449, ___onSerializeNetworkView_56)); }
	inline Subject_1_t919080185 * get_onSerializeNetworkView_56() const { return ___onSerializeNetworkView_56; }
	inline Subject_1_t919080185 ** get_address_of_onSerializeNetworkView_56() { return &___onSerializeNetworkView_56; }
	inline void set_onSerializeNetworkView_56(Subject_1_t919080185 * value)
	{
		___onSerializeNetworkView_56 = value;
		Il2CppCodeGenWriteBarrier(&___onSerializeNetworkView_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
