﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.VirtualCurrencyRechargeTime
struct VirtualCurrencyRechargeTime_t1584308320;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void PlayFab.ClientModels.VirtualCurrencyRechargeTime::.ctor()
extern "C"  void VirtualCurrencyRechargeTime__ctor_m1107989065 (VirtualCurrencyRechargeTime_t1584308320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.VirtualCurrencyRechargeTime::get_SecondsToRecharge()
extern "C"  int32_t VirtualCurrencyRechargeTime_get_SecondsToRecharge_m250004625 (VirtualCurrencyRechargeTime_t1584308320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.VirtualCurrencyRechargeTime::set_SecondsToRecharge(System.Int32)
extern "C"  void VirtualCurrencyRechargeTime_set_SecondsToRecharge_m2521195196 (VirtualCurrencyRechargeTime_t1584308320 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayFab.ClientModels.VirtualCurrencyRechargeTime::get_RechargeTime()
extern "C"  DateTime_t339033936  VirtualCurrencyRechargeTime_get_RechargeTime_m3015044177 (VirtualCurrencyRechargeTime_t1584308320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.VirtualCurrencyRechargeTime::set_RechargeTime(System.DateTime)
extern "C"  void VirtualCurrencyRechargeTime_set_RechargeTime_m3690816332 (VirtualCurrencyRechargeTime_t1584308320 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.VirtualCurrencyRechargeTime::get_RechargeMax()
extern "C"  int32_t VirtualCurrencyRechargeTime_get_RechargeMax_m1655069677 (VirtualCurrencyRechargeTime_t1584308320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.VirtualCurrencyRechargeTime::set_RechargeMax(System.Int32)
extern "C"  void VirtualCurrencyRechargeTime_set_RechargeMax_m3122338712 (VirtualCurrencyRechargeTime_t1584308320 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
