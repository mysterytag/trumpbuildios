﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<System.String>::.ctor()
#define Stream_1__ctor_m3740708434(__this, method) ((  void (*) (Stream_1_t3954595220 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<System.String>::Send(T)
#define Stream_1_Send_m2269925988(__this, ___value0, method) ((  void (*) (Stream_1_t3954595220 *, String_t*, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.String>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m3980071162(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t3954595220 *, String_t*, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.String>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m1446832700(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t3954595220 *, Action_1_t1116941607 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.String>::Listen(System.Action,Priority)
#define Stream_1_Listen_m972136811(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t3954595220 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.String>::Dispose()
#define Stream_1_Dispose_m4126428687(__this, method) ((  void (*) (Stream_1_t3954595220 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<System.String>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m3713607918(__this, ___stream0, method) ((  void (*) (Stream_1_t3954595220 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.String>::ClearInputStream()
#define Stream_1_ClearInputStream_m2382138191(__this, method) ((  void (*) (Stream_1_t3954595220 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<System.String>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m1951845539(__this, method) ((  void (*) (Stream_1_t3954595220 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<System.String>::Unpack(System.Int32)
#define Stream_1_Unpack_m299080565(__this, ___p0, method) ((  void (*) (Stream_1_t3954595220 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.String>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m646446191(__this, ___val0, method) ((  void (*) (Stream_1_t3954595220 *, String_t*, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
