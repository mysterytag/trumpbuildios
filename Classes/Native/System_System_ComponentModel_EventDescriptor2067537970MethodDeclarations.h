﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.EventDescriptor
struct EventDescriptor_t2067537970;
// System.ComponentModel.MemberDescriptor
struct MemberDescriptor_t1680589938;
// System.Attribute[]
struct AttributeU5BU5D_t4076389004;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_MemberDescriptor1680589938.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.ComponentModel.EventDescriptor::.ctor(System.ComponentModel.MemberDescriptor)
extern "C"  void EventDescriptor__ctor_m1905167128 (EventDescriptor_t2067537970 * __this, MemberDescriptor_t1680589938 * ___desc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptor::.ctor(System.ComponentModel.MemberDescriptor,System.Attribute[])
extern "C"  void EventDescriptor__ctor_m1988143951 (EventDescriptor_t2067537970 * __this, MemberDescriptor_t1680589938 * ___desc0, AttributeU5BU5D_t4076389004* ___attrs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EventDescriptor::.ctor(System.String,System.Attribute[])
extern "C"  void EventDescriptor__ctor_m2517078153 (EventDescriptor_t2067537970 * __this, String_t* ___str0, AttributeU5BU5D_t4076389004* ___attrs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
