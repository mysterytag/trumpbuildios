﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey183
struct U3CCreateForMemberU3Ec__AnonStorey183_t1843898456;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey183::.ctor()
extern "C"  void U3CCreateForMemberU3Ec__AnonStorey183__ctor_m578813331 (U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey183::<>m__2B4(System.Object,System.Object)
extern "C"  void U3CCreateForMemberU3Ec__AnonStorey183_U3CU3Em__2B4_m2405414520 (U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * __this, Il2CppObject * ___injectable0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
