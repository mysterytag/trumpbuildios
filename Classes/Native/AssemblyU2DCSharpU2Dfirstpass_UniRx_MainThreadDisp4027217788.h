﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.ThreadSafeQueueWorker
struct ThreadSafeQueueWorker_t3889225637;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// UniRx.MainThreadDispatcher
struct MainThreadDispatcher_t4027217788;
// System.Object
struct Il2CppObject;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// UniRx.Subject`1<System.Boolean>
struct Subject_1_t2149039961;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp1899807443.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatcher
struct  MainThreadDispatcher_t4027217788  : public MonoBehaviour_t3012272455
{
public:
	// UniRx.InternalUtil.ThreadSafeQueueWorker UniRx.MainThreadDispatcher::queueWorker
	ThreadSafeQueueWorker_t3889225637 * ___queueWorker_3;
	// System.Action`1<System.Exception> UniRx.MainThreadDispatcher::unhandledExceptionCallback
	Action_1_t2115686693 * ___unhandledExceptionCallback_4;
	// UniRx.Subject`1<UniRx.Unit> UniRx.MainThreadDispatcher::update
	Subject_1_t201353362 * ___update_9;
	// UniRx.Subject`1<UniRx.Unit> UniRx.MainThreadDispatcher::lateUpdate
	Subject_1_t201353362 * ___lateUpdate_10;
	// UniRx.Subject`1<System.Boolean> UniRx.MainThreadDispatcher::onApplicationFocus
	Subject_1_t2149039961 * ___onApplicationFocus_11;
	// UniRx.Subject`1<System.Boolean> UniRx.MainThreadDispatcher::onApplicationPause
	Subject_1_t2149039961 * ___onApplicationPause_12;
	// UniRx.Subject`1<UniRx.Unit> UniRx.MainThreadDispatcher::onApplicationQuit
	Subject_1_t201353362 * ___onApplicationQuit_13;

public:
	inline static int32_t get_offset_of_queueWorker_3() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788, ___queueWorker_3)); }
	inline ThreadSafeQueueWorker_t3889225637 * get_queueWorker_3() const { return ___queueWorker_3; }
	inline ThreadSafeQueueWorker_t3889225637 ** get_address_of_queueWorker_3() { return &___queueWorker_3; }
	inline void set_queueWorker_3(ThreadSafeQueueWorker_t3889225637 * value)
	{
		___queueWorker_3 = value;
		Il2CppCodeGenWriteBarrier(&___queueWorker_3, value);
	}

	inline static int32_t get_offset_of_unhandledExceptionCallback_4() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788, ___unhandledExceptionCallback_4)); }
	inline Action_1_t2115686693 * get_unhandledExceptionCallback_4() const { return ___unhandledExceptionCallback_4; }
	inline Action_1_t2115686693 ** get_address_of_unhandledExceptionCallback_4() { return &___unhandledExceptionCallback_4; }
	inline void set_unhandledExceptionCallback_4(Action_1_t2115686693 * value)
	{
		___unhandledExceptionCallback_4 = value;
		Il2CppCodeGenWriteBarrier(&___unhandledExceptionCallback_4, value);
	}

	inline static int32_t get_offset_of_update_9() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788, ___update_9)); }
	inline Subject_1_t201353362 * get_update_9() const { return ___update_9; }
	inline Subject_1_t201353362 ** get_address_of_update_9() { return &___update_9; }
	inline void set_update_9(Subject_1_t201353362 * value)
	{
		___update_9 = value;
		Il2CppCodeGenWriteBarrier(&___update_9, value);
	}

	inline static int32_t get_offset_of_lateUpdate_10() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788, ___lateUpdate_10)); }
	inline Subject_1_t201353362 * get_lateUpdate_10() const { return ___lateUpdate_10; }
	inline Subject_1_t201353362 ** get_address_of_lateUpdate_10() { return &___lateUpdate_10; }
	inline void set_lateUpdate_10(Subject_1_t201353362 * value)
	{
		___lateUpdate_10 = value;
		Il2CppCodeGenWriteBarrier(&___lateUpdate_10, value);
	}

	inline static int32_t get_offset_of_onApplicationFocus_11() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788, ___onApplicationFocus_11)); }
	inline Subject_1_t2149039961 * get_onApplicationFocus_11() const { return ___onApplicationFocus_11; }
	inline Subject_1_t2149039961 ** get_address_of_onApplicationFocus_11() { return &___onApplicationFocus_11; }
	inline void set_onApplicationFocus_11(Subject_1_t2149039961 * value)
	{
		___onApplicationFocus_11 = value;
		Il2CppCodeGenWriteBarrier(&___onApplicationFocus_11, value);
	}

	inline static int32_t get_offset_of_onApplicationPause_12() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788, ___onApplicationPause_12)); }
	inline Subject_1_t2149039961 * get_onApplicationPause_12() const { return ___onApplicationPause_12; }
	inline Subject_1_t2149039961 ** get_address_of_onApplicationPause_12() { return &___onApplicationPause_12; }
	inline void set_onApplicationPause_12(Subject_1_t2149039961 * value)
	{
		___onApplicationPause_12 = value;
		Il2CppCodeGenWriteBarrier(&___onApplicationPause_12, value);
	}

	inline static int32_t get_offset_of_onApplicationQuit_13() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788, ___onApplicationQuit_13)); }
	inline Subject_1_t201353362 * get_onApplicationQuit_13() const { return ___onApplicationQuit_13; }
	inline Subject_1_t201353362 ** get_address_of_onApplicationQuit_13() { return &___onApplicationQuit_13; }
	inline void set_onApplicationQuit_13(Subject_1_t201353362 * value)
	{
		___onApplicationQuit_13 = value;
		Il2CppCodeGenWriteBarrier(&___onApplicationQuit_13, value);
	}
};

struct MainThreadDispatcher_t4027217788_StaticFields
{
public:
	// UniRx.MainThreadDispatcher/CullingMode UniRx.MainThreadDispatcher::cullingMode
	int32_t ___cullingMode_2;
	// UniRx.MainThreadDispatcher UniRx.MainThreadDispatcher::instance
	MainThreadDispatcher_t4027217788 * ___instance_5;
	// System.Boolean UniRx.MainThreadDispatcher::initialized
	bool ___initialized_6;
	// System.Boolean UniRx.MainThreadDispatcher::isQuitting
	bool ___isQuitting_7;
	// System.Action`1<System.Exception> UniRx.MainThreadDispatcher::<>f__am$cacheC
	Action_1_t2115686693 * ___U3CU3Ef__amU24cacheC_14;

public:
	inline static int32_t get_offset_of_cullingMode_2() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788_StaticFields, ___cullingMode_2)); }
	inline int32_t get_cullingMode_2() const { return ___cullingMode_2; }
	inline int32_t* get_address_of_cullingMode_2() { return &___cullingMode_2; }
	inline void set_cullingMode_2(int32_t value)
	{
		___cullingMode_2 = value;
	}

	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788_StaticFields, ___instance_5)); }
	inline MainThreadDispatcher_t4027217788 * get_instance_5() const { return ___instance_5; }
	inline MainThreadDispatcher_t4027217788 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(MainThreadDispatcher_t4027217788 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}

	inline static int32_t get_offset_of_initialized_6() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788_StaticFields, ___initialized_6)); }
	inline bool get_initialized_6() const { return ___initialized_6; }
	inline bool* get_address_of_initialized_6() { return &___initialized_6; }
	inline void set_initialized_6(bool value)
	{
		___initialized_6 = value;
	}

	inline static int32_t get_offset_of_isQuitting_7() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788_StaticFields, ___isQuitting_7)); }
	inline bool get_isQuitting_7() const { return ___isQuitting_7; }
	inline bool* get_address_of_isQuitting_7() { return &___isQuitting_7; }
	inline void set_isQuitting_7(bool value)
	{
		___isQuitting_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_14() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788_StaticFields, ___U3CU3Ef__amU24cacheC_14)); }
	inline Action_1_t2115686693 * get_U3CU3Ef__amU24cacheC_14() const { return ___U3CU3Ef__amU24cacheC_14; }
	inline Action_1_t2115686693 ** get_address_of_U3CU3Ef__amU24cacheC_14() { return &___U3CU3Ef__amU24cacheC_14; }
	inline void set_U3CU3Ef__amU24cacheC_14(Action_1_t2115686693 * value)
	{
		___U3CU3Ef__amU24cacheC_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_14, value);
	}
};

struct MainThreadDispatcher_t4027217788_ThreadStaticFields
{
public:
	// System.Object UniRx.MainThreadDispatcher::mainThreadToken
	Il2CppObject * ___mainThreadToken_8;

public:
	inline static int32_t get_offset_of_mainThreadToken_8() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t4027217788_ThreadStaticFields, ___mainThreadToken_8)); }
	inline Il2CppObject * get_mainThreadToken_8() const { return ___mainThreadToken_8; }
	inline Il2CppObject ** get_address_of_mainThreadToken_8() { return &___mainThreadToken_8; }
	inline void set_mainThreadToken_8(Il2CppObject * value)
	{
		___mainThreadToken_8 = value;
		Il2CppCodeGenWriteBarrier(&___mainThreadToken_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
