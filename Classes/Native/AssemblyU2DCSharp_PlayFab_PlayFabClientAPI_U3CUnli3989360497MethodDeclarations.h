﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkKongregate>c__AnonStorey86
struct U3CUnlinkKongregateU3Ec__AnonStorey86_t3989360497;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkKongregate>c__AnonStorey86::.ctor()
extern "C"  void U3CUnlinkKongregateU3Ec__AnonStorey86__ctor_m103893858 (U3CUnlinkKongregateU3Ec__AnonStorey86_t3989360497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkKongregate>c__AnonStorey86::<>m__73(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkKongregateU3Ec__AnonStorey86_U3CU3Em__73_m2419155100 (U3CUnlinkKongregateU3Ec__AnonStorey86_t3989360497 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
