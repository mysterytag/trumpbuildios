﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UnityEngine.ControllerColliderHit>::.ctor()
#define Subject_1__ctor_m3230093734(__this, method) ((  void (*) (Subject_1_t336133548 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.ControllerColliderHit>::get_HasObservers()
#define Subject_1_get_HasObservers_m1644704294(__this, method) ((  bool (*) (Subject_1_t336133548 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.ControllerColliderHit>::OnCompleted()
#define Subject_1_OnCompleted_m2520527024(__this, method) ((  void (*) (Subject_1_t336133548 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.ControllerColliderHit>::OnError(System.Exception)
#define Subject_1_OnError_m3291699677(__this, ___error0, method) ((  void (*) (Subject_1_t336133548 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.ControllerColliderHit>::OnNext(T)
#define Subject_1_OnNext_m1689426638(__this, ___value0, method) ((  void (*) (Subject_1_t336133548 *, ControllerColliderHit_t2693066224 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.ControllerColliderHit>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m1237947685(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t336133548 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.ControllerColliderHit>::Dispose()
#define Subject_1_Dispose_m3051973731(__this, method) ((  void (*) (Subject_1_t336133548 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.ControllerColliderHit>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m22151916(__this, method) ((  void (*) (Subject_1_t336133548 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.ControllerColliderHit>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2471928221(__this, method) ((  bool (*) (Subject_1_t336133548 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
