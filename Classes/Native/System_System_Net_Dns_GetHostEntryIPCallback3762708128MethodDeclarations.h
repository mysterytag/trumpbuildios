﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Dns/GetHostEntryIPCallback
struct GetHostEntryIPCallback_t3762708128;
// System.Object
struct Il2CppObject;
// System.Net.IPHostEntry
struct IPHostEntry_t4205702573;
// System.Net.IPAddress
struct IPAddress_t3220500535;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_IPAddress3220500535.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.Dns/GetHostEntryIPCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetHostEntryIPCallback__ctor_m716082638 (GetHostEntryIPCallback_t3762708128 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostEntryIPCallback::Invoke(System.Net.IPAddress)
extern "C"  IPHostEntry_t4205702573 * GetHostEntryIPCallback_Invoke_m2649061503 (GetHostEntryIPCallback_t3762708128 * __this, IPAddress_t3220500535 * ___hostAddress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" IPHostEntry_t4205702573 * pinvoke_delegate_wrapper_GetHostEntryIPCallback_t3762708128(Il2CppObject* delegate, IPAddress_t3220500535 * ___hostAddress0);
// System.IAsyncResult System.Net.Dns/GetHostEntryIPCallback::BeginInvoke(System.Net.IPAddress,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetHostEntryIPCallback_BeginInvoke_m280883480 (GetHostEntryIPCallback_t3762708128 * __this, IPAddress_t3220500535 * ___hostAddress0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostEntryIPCallback::EndInvoke(System.IAsyncResult)
extern "C"  IPHostEntry_t4205702573 * GetHostEntryIPCallback_EndInvoke_m1691402450 (GetHostEntryIPCallback_t3762708128 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
