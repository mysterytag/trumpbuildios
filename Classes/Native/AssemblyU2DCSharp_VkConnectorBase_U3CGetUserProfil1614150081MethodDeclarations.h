﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VkConnectorBase/<GetUserProfile>c__AnonStoreyD8
struct U3CGetUserProfileU3Ec__AnonStoreyD8_t1614150081;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen3445990771.h"

// System.Void VkConnectorBase/<GetUserProfile>c__AnonStoreyD8::.ctor()
extern "C"  void U3CGetUserProfileU3Ec__AnonStoreyD8__ctor_m1525354032 (U3CGetUserProfileU3Ec__AnonStoreyD8_t1614150081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase/<GetUserProfile>c__AnonStoreyD8::<>m__DE(UniRx.Tuple`2<System.String,System.String>)
extern "C"  void U3CGetUserProfileU3Ec__AnonStoreyD8_U3CU3Em__DE_m644651958 (U3CGetUserProfileU3Ec__AnonStoreyD8_t1614150081 * __this, Tuple_2_t3445990771  ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
