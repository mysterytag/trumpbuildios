﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayFab.ClientModels.RegionInfo>
struct List_1_t4271828715;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GameServerRegionsResult
struct  GameServerRegionsResult_t253590807  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.List`1<PlayFab.ClientModels.RegionInfo> PlayFab.ClientModels.GameServerRegionsResult::<Regions>k__BackingField
	List_1_t4271828715 * ___U3CRegionsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRegionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GameServerRegionsResult_t253590807, ___U3CRegionsU3Ek__BackingField_2)); }
	inline List_1_t4271828715 * get_U3CRegionsU3Ek__BackingField_2() const { return ___U3CRegionsU3Ek__BackingField_2; }
	inline List_1_t4271828715 ** get_address_of_U3CRegionsU3Ek__BackingField_2() { return &___U3CRegionsU3Ek__BackingField_2; }
	inline void set_U3CRegionsU3Ek__BackingField_2(List_1_t4271828715 * value)
	{
		___U3CRegionsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRegionsU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
