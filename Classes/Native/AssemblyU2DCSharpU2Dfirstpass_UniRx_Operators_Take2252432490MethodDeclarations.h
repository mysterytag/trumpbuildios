﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>
struct Take_t2252432490;
// UniRx.Operators.TakeObservable`1<UniRx.Unit>
struct TakeObservable_1_t505031811;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take__ctor_m2056496917_gshared (Take_t2252432490 * __this, TakeObservable_1_t505031811 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Take__ctor_m2056496917(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Take_t2252432490 *, TakeObservable_1_t505031811 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Take__ctor_m2056496917_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>::OnNext(T)
extern "C"  void Take_OnNext_m3317440725_gshared (Take_t2252432490 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Take_OnNext_m3317440725(__this, ___value0, method) ((  void (*) (Take_t2252432490 *, Unit_t2558286038 , const MethodInfo*))Take_OnNext_m3317440725_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Take_OnError_m303428068_gshared (Take_t2252432490 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Take_OnError_m303428068(__this, ___error0, method) ((  void (*) (Take_t2252432490 *, Exception_t1967233988 *, const MethodInfo*))Take_OnError_m303428068_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<UniRx.Unit>::OnCompleted()
extern "C"  void Take_OnCompleted_m2876594999_gshared (Take_t2252432490 * __this, const MethodInfo* method);
#define Take_OnCompleted_m2876594999(__this, method) ((  void (*) (Take_t2252432490 *, const MethodInfo*))Take_OnCompleted_m2876594999_gshared)(__this, method)
