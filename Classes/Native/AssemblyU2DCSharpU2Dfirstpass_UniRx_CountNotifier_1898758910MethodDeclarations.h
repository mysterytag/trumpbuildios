﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.CountNotifier/<Increment>c__AnonStorey57
struct U3CIncrementU3Ec__AnonStorey57_t1898758910;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.CountNotifier/<Increment>c__AnonStorey57::.ctor()
extern "C"  void U3CIncrementU3Ec__AnonStorey57__ctor_m3673366783 (U3CIncrementU3Ec__AnonStorey57_t1898758910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CountNotifier/<Increment>c__AnonStorey57::<>m__6B()
extern "C"  void U3CIncrementU3Ec__AnonStorey57_U3CU3Em__6B_m1950567380 (U3CIncrementU3Ec__AnonStorey57_t1898758910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
