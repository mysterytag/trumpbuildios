﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback
struct LogCallback_t3235662730;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback::.ctor()
extern "C"  void LogCallback__ctor_m1262722548 (LogCallback_t3235662730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
