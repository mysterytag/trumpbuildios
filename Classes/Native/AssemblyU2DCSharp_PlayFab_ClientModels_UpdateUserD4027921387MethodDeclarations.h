﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateUserDataResult
struct UpdateUserDataResult_t4027921387;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UpdateUserDataResult::.ctor()
extern "C"  void UpdateUserDataResult__ctor_m2422935474 (UpdateUserDataResult_t4027921387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.UpdateUserDataResult::get_DataVersion()
extern "C"  uint32_t UpdateUserDataResult_get_DataVersion_m1936191696 (UpdateUserDataResult_t4027921387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateUserDataResult::set_DataVersion(System.UInt32)
extern "C"  void UpdateUserDataResult_set_DataVersion_m3576243033 (UpdateUserDataResult_t4027921387 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
