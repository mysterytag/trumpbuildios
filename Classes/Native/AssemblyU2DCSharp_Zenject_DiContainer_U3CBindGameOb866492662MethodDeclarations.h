﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<BindGameObjectFactory>c__AnonStorey18D`1<System.Object>
struct U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.DiContainer/<BindGameObjectFactory>c__AnonStorey18D`1<System.Object>::.ctor()
extern "C"  void U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1__ctor_m4261405394_gshared (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * __this, const MethodInfo* method);
#define U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1__ctor_m4261405394(__this, method) ((  void (*) (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 *, const MethodInfo*))U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1__ctor_m4261405394_gshared)(__this, method)
// T Zenject.DiContainer/<BindGameObjectFactory>c__AnonStorey18D`1<System.Object>::<>m__2CD(Zenject.InjectContext)
extern "C"  Il2CppObject * U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_U3CU3Em__2CD_m3082394938_gshared (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_U3CU3Em__2CD_m3082394938(__this, ___ctx0, method) ((  Il2CppObject * (*) (U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_t866492662 *, InjectContext_t3456483891 *, const MethodInfo*))U3CBindGameObjectFactoryU3Ec__AnonStorey18D_1_U3CU3Em__2CD_m3082394938_gshared)(__this, ___ctx0, method)
