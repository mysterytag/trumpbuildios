﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>
struct Buffer_t3527656087;
// UniRx.Operators.BufferObservable`2<System.Object,System.Object>
struct BufferObservable_2_t2093442205;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::.ctor(UniRx.Operators.BufferObservable`2<TSource,TWindowBoundary>,UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern "C"  void Buffer__ctor_m934385027_gshared (Buffer_t3527656087 * __this, BufferObservable_2_t2093442205 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Buffer__ctor_m934385027(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Buffer_t3527656087 *, BufferObservable_2_t2093442205 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Buffer__ctor_m934385027_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::.cctor()
extern "C"  void Buffer__cctor_m4192606757_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Buffer__cctor_m4192606757(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Buffer__cctor_m4192606757_gshared)(__this /* static, unused */, method)
// System.IDisposable UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * Buffer_Run_m1539816790_gshared (Buffer_t3527656087 * __this, const MethodInfo* method);
#define Buffer_Run_m1539816790(__this, method) ((  Il2CppObject * (*) (Buffer_t3527656087 *, const MethodInfo*))Buffer_Run_m1539816790_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::OnNext(TSource)
extern "C"  void Buffer_OnNext_m3779144981_gshared (Buffer_t3527656087 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Buffer_OnNext_m3779144981(__this, ___value0, method) ((  void (*) (Buffer_t3527656087 *, Il2CppObject *, const MethodInfo*))Buffer_OnNext_m3779144981_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Buffer_OnError_m2330482239_gshared (Buffer_t3527656087 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Buffer_OnError_m2330482239(__this, ___error0, method) ((  void (*) (Buffer_t3527656087 *, Exception_t1967233988 *, const MethodInfo*))Buffer_OnError_m2330482239_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>::OnCompleted()
extern "C"  void Buffer_OnCompleted_m374288402_gshared (Buffer_t3527656087 * __this, const MethodInfo* method);
#define Buffer_OnCompleted_m374288402(__this, method) ((  void (*) (Buffer_t3527656087 *, const MethodInfo*))Buffer_OnCompleted_m374288402_gshared)(__this, method)
