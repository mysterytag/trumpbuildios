﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.StartObservable`1/StartObserver<System.Object>
struct StartObserver_t822136817;
// UniRx.Operators.StartObservable`1<System.Object>
struct StartObservable_1_t2362529796;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::.ctor(UniRx.Operators.StartObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartObserver__ctor_m278673816_gshared (StartObserver_t822136817 * __this, StartObservable_1_t2362529796 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define StartObserver__ctor_m278673816(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (StartObserver_t822136817 *, StartObservable_1_t2362529796 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartObserver__ctor_m278673816_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::Run()
extern "C"  void StartObserver_Run_m1779710341_gshared (StartObserver_t822136817 * __this, const MethodInfo* method);
#define StartObserver_Run_m1779710341(__this, method) ((  void (*) (StartObserver_t822136817 *, const MethodInfo*))StartObserver_Run_m1779710341_gshared)(__this, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::OnNext(T)
extern "C"  void StartObserver_OnNext_m2793218691_gshared (StartObserver_t822136817 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define StartObserver_OnNext_m2793218691(__this, ___value0, method) ((  void (*) (StartObserver_t822136817 *, Il2CppObject *, const MethodInfo*))StartObserver_OnNext_m2793218691_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::OnError(System.Exception)
extern "C"  void StartObserver_OnError_m3458848658_gshared (StartObserver_t822136817 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define StartObserver_OnError_m3458848658(__this, ___error0, method) ((  void (*) (StartObserver_t822136817 *, Exception_t1967233988 *, const MethodInfo*))StartObserver_OnError_m3458848658_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.StartObservable`1/StartObserver<System.Object>::OnCompleted()
extern "C"  void StartObserver_OnCompleted_m1533138405_gshared (StartObserver_t822136817 * __this, const MethodInfo* method);
#define StartObserver_OnCompleted_m1533138405(__this, method) ((  void (*) (StartObserver_t822136817 *, const MethodInfo*))StartObserver_OnCompleted_m1533138405_gshared)(__this, method)
