﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tacticsoft.TableView/<AsyncLoad>c__Iterator20
struct U3CAsyncLoadU3Ec__Iterator20_t1531820471;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Tacticsoft.TableView/<AsyncLoad>c__Iterator20::.ctor()
extern "C"  void U3CAsyncLoadU3Ec__Iterator20__ctor_m2306889612 (U3CAsyncLoadU3Ec__Iterator20_t1531820471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Tacticsoft.TableView/<AsyncLoad>c__Iterator20::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAsyncLoadU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3311494150 (U3CAsyncLoadU3Ec__Iterator20_t1531820471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Tacticsoft.TableView/<AsyncLoad>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsyncLoadU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m2969056154 (U3CAsyncLoadU3Ec__Iterator20_t1531820471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tacticsoft.TableView/<AsyncLoad>c__Iterator20::MoveNext()
extern "C"  bool U3CAsyncLoadU3Ec__Iterator20_MoveNext_m2396960232 (U3CAsyncLoadU3Ec__Iterator20_t1531820471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView/<AsyncLoad>c__Iterator20::Dispose()
extern "C"  void U3CAsyncLoadU3Ec__Iterator20_Dispose_m616075465 (U3CAsyncLoadU3Ec__Iterator20_t1531820471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tacticsoft.TableView/<AsyncLoad>c__Iterator20::Reset()
extern "C"  void U3CAsyncLoadU3Ec__Iterator20_Reset_m4248289849 (U3CAsyncLoadU3Ec__Iterator20_t1531820471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
