﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkIOSDeviceIDResponseCallback
struct LinkIOSDeviceIDResponseCallback_t1215862890;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkIOSDeviceIDRequest
struct LinkIOSDeviceIDRequest_t130515291;
// PlayFab.ClientModels.LinkIOSDeviceIDResult
struct LinkIOSDeviceIDResult_t3225946609;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkIOSDevic130515291.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkIOSDevi3225946609.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkIOSDeviceIDResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkIOSDeviceIDResponseCallback__ctor_m1642575865 (LinkIOSDeviceIDResponseCallback_t1215862890 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkIOSDeviceIDResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkIOSDeviceIDRequest,PlayFab.ClientModels.LinkIOSDeviceIDResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkIOSDeviceIDResponseCallback_Invoke_m1767646060 (LinkIOSDeviceIDResponseCallback_t1215862890 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkIOSDeviceIDRequest_t130515291 * ___request2, LinkIOSDeviceIDResult_t3225946609 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkIOSDeviceIDResponseCallback_t1215862890(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkIOSDeviceIDRequest_t130515291 * ___request2, LinkIOSDeviceIDResult_t3225946609 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkIOSDeviceIDResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkIOSDeviceIDRequest,PlayFab.ClientModels.LinkIOSDeviceIDResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkIOSDeviceIDResponseCallback_BeginInvoke_m513494229 (LinkIOSDeviceIDResponseCallback_t1215862890 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkIOSDeviceIDRequest_t130515291 * ___request2, LinkIOSDeviceIDResult_t3225946609 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkIOSDeviceIDResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkIOSDeviceIDResponseCallback_EndInvoke_m54529673 (LinkIOSDeviceIDResponseCallback_t1215862890 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
