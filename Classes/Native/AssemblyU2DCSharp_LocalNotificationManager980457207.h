﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// INotificationManager
struct INotificationManager_t3349952825;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalNotificationManager
struct  LocalNotificationManager_t980457207  : public Il2CppObject
{
public:

public:
};

struct LocalNotificationManager_t980457207_StaticFields
{
public:
	// INotificationManager LocalNotificationManager::_manager
	Il2CppObject * ____manager_0;

public:
	inline static int32_t get_offset_of__manager_0() { return static_cast<int32_t>(offsetof(LocalNotificationManager_t980457207_StaticFields, ____manager_0)); }
	inline Il2CppObject * get__manager_0() const { return ____manager_0; }
	inline Il2CppObject ** get_address_of__manager_0() { return &____manager_0; }
	inline void set__manager_0(Il2CppObject * value)
	{
		____manager_0 = value;
		Il2CppCodeGenWriteBarrier(&____manager_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
