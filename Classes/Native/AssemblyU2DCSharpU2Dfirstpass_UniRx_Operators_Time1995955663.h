﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D
struct U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70
struct  U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663  : public Il2CppObject
{
public:
	// System.TimeSpan UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70::timeP
	TimeSpan_t763862892  ___timeP_0;
	// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70::<>f__ref$109
	U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * ___U3CU3Ef__refU24109_1;

public:
	inline static int32_t get_offset_of_timeP_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663, ___timeP_0)); }
	inline TimeSpan_t763862892  get_timeP_0() const { return ___timeP_0; }
	inline TimeSpan_t763862892 * get_address_of_timeP_0() { return &___timeP_0; }
	inline void set_timeP_0(TimeSpan_t763862892  value)
	{
		___timeP_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24109_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663, ___U3CU3Ef__refU24109_1)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * get_U3CU3Ef__refU24109_1() const { return ___U3CU3Ef__refU24109_1; }
	inline U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 ** get_address_of_U3CU3Ef__refU24109_1() { return &___U3CU3Ef__refU24109_1; }
	inline void set_U3CU3Ef__refU24109_1(U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * value)
	{
		___U3CU3Ef__refU24109_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24109_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
