﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult
struct GetPlayFabIDsFromGoogleIDsResult_t2496174951;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GooglePlayFabIdPair>
struct List_1_t1460113112;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromGoogleIDsResult__ctor_m409248950 (GetPlayFabIDsFromGoogleIDsResult_t2496174951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GooglePlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult::get_Data()
extern "C"  List_1_t1460113112 * GetPlayFabIDsFromGoogleIDsResult_get_Data_m3121458781 (GetPlayFabIDsFromGoogleIDsResult_t2496174951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.GooglePlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromGoogleIDsResult_set_Data_m1896799726 (GetPlayFabIDsFromGoogleIDsResult_t2496174951 * __this, List_1_t1460113112 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
