﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.OpenIAB_Android
struct OpenIAB_Android_t58679140;

#include "codegen/il2cpp-codegen.h"

// System.Void OnePF.OpenIAB_Android::.ctor()
extern "C"  void OpenIAB_Android__ctor_m2526296985 (OpenIAB_Android_t58679140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB_Android::.cctor()
extern "C"  void OpenIAB_Android__cctor_m523698996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
