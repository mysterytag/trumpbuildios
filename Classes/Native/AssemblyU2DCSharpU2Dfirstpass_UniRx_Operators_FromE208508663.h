﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>
struct FromEventPatternObservable_2_t3019221245;
// UniRx.IObserver`1<UniRx.EventPattern`1<System.Object>>
struct IObserver_1_t820035585;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>
struct  FromEventPattern_t208508663  : public Il2CppObject
{
public:
	// UniRx.Operators.FromEventPatternObservable`2<TDelegate,TEventArgs> UniRx.Operators.FromEventPatternObservable`2/FromEventPattern::parent
	FromEventPatternObservable_2_t3019221245 * ___parent_0;
	// UniRx.IObserver`1<UniRx.EventPattern`1<TEventArgs>> UniRx.Operators.FromEventPatternObservable`2/FromEventPattern::observer
	Il2CppObject* ___observer_1;
	// TDelegate UniRx.Operators.FromEventPatternObservable`2/FromEventPattern::handler
	Il2CppObject * ___handler_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(FromEventPattern_t208508663, ___parent_0)); }
	inline FromEventPatternObservable_2_t3019221245 * get_parent_0() const { return ___parent_0; }
	inline FromEventPatternObservable_2_t3019221245 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(FromEventPatternObservable_2_t3019221245 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_observer_1() { return static_cast<int32_t>(offsetof(FromEventPattern_t208508663, ___observer_1)); }
	inline Il2CppObject* get_observer_1() const { return ___observer_1; }
	inline Il2CppObject** get_address_of_observer_1() { return &___observer_1; }
	inline void set_observer_1(Il2CppObject* value)
	{
		___observer_1 = value;
		Il2CppCodeGenWriteBarrier(&___observer_1, value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(FromEventPattern_t208508663, ___handler_2)); }
	inline Il2CppObject * get_handler_2() const { return ___handler_2; }
	inline Il2CppObject ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(Il2CppObject * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier(&___handler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
