﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<System.Double>
struct OperatorObservableBase_1_t3893628881;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<System.Double>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m1397930020_gshared (OperatorObservableBase_1_t3893628881 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m1397930020(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t3893628881 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m1397930020_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Double>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1223935358_gshared (OperatorObservableBase_1_t3893628881 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1223935358(__this, method) ((  bool (*) (OperatorObservableBase_1_t3893628881 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1223935358_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Double>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1301098818_gshared (OperatorObservableBase_1_t3893628881 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m1301098818(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t3893628881 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m1301098818_gshared)(__this, ___observer0, method)
