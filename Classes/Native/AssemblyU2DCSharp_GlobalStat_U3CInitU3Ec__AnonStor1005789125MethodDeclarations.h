﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/<Init>c__AnonStoreyE5
struct U3CInitU3Ec__AnonStoreyE5_t1005789125;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/<Init>c__AnonStoreyE5::.ctor()
extern "C"  void U3CInitU3Ec__AnonStoreyE5__ctor_m2403885262 (U3CInitU3Ec__AnonStoreyE5_t1005789125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<Init>c__AnonStoreyE5::<>m__10B(System.Int32)
extern "C"  void U3CInitU3Ec__AnonStoreyE5_U3CU3Em__10B_m2958206527 (U3CInitU3Ec__AnonStoreyE5_t1005789125 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
