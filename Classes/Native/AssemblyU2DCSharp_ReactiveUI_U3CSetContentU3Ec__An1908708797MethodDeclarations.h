﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReactiveUI/<SetContent>c__AnonStorey120`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey120_1_t1908708797;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ReactiveUI/<SetContent>c__AnonStorey120`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey120_1__ctor_m2428989317_gshared (U3CSetContentU3Ec__AnonStorey120_1_t1908708797 * __this, const MethodInfo* method);
#define U3CSetContentU3Ec__AnonStorey120_1__ctor_m2428989317(__this, method) ((  void (*) (U3CSetContentU3Ec__AnonStorey120_1_t1908708797 *, const MethodInfo*))U3CSetContentU3Ec__AnonStorey120_1__ctor_m2428989317_gshared)(__this, method)
// System.Void ReactiveUI/<SetContent>c__AnonStorey120`1<System.Object>::<>m__1B1(T)
extern "C"  void U3CSetContentU3Ec__AnonStorey120_1_U3CU3Em__1B1_m1719215402_gshared (U3CSetContentU3Ec__AnonStorey120_1_t1908708797 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CSetContentU3Ec__AnonStorey120_1_U3CU3Em__1B1_m1719215402(__this, ___obj0, method) ((  void (*) (U3CSetContentU3Ec__AnonStorey120_1_t1908708797 *, Il2CppObject *, const MethodInfo*))U3CSetContentU3Ec__AnonStorey120_1_U3CU3Em__1B1_m1719215402_gshared)(__this, ___obj0, method)
