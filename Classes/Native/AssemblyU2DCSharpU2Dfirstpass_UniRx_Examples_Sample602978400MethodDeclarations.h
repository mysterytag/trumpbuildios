﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample11_Logger
struct Sample11_Logger_t602978400;
// UniRx.Diagnostics.LogEntry
struct LogEntry_t1891155722;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1891155722.h"

// System.Void UniRx.Examples.Sample11_Logger::.ctor()
extern "C"  void Sample11_Logger__ctor_m2291555575 (Sample11_Logger_t602978400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample11_Logger::.cctor()
extern "C"  void Sample11_Logger__cctor_m1836649878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample11_Logger::ApplicationInitialize()
extern "C"  void Sample11_Logger_ApplicationInitialize_m264119061 (Sample11_Logger_t602978400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample11_Logger::Run()
extern "C"  void Sample11_Logger_Run_m4045093600 (Sample11_Logger_t602978400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample11_Logger::<ApplicationInitialize>m__34(UniRx.Diagnostics.LogEntry)
extern "C"  bool Sample11_Logger_U3CApplicationInitializeU3Em__34_m295725365 (Il2CppObject * __this /* static, unused */, LogEntry_t1891155722 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample11_Logger::<ApplicationInitialize>m__35(UniRx.Diagnostics.LogEntry)
extern "C"  void Sample11_Logger_U3CApplicationInitializeU3Em__35_m188187674 (Il2CppObject * __this /* static, unused */, LogEntry_t1891155722 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
