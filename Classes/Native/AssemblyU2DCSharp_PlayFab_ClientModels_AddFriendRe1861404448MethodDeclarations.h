﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AddFriendRequest
struct AddFriendRequest_t1861404448;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.AddFriendRequest::.ctor()
extern "C"  void AddFriendRequest__ctor_m2100964637 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddFriendRequest::get_FriendPlayFabId()
extern "C"  String_t* AddFriendRequest_get_FriendPlayFabId_m2085920357 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddFriendRequest::set_FriendPlayFabId(System.String)
extern "C"  void AddFriendRequest_set_FriendPlayFabId_m4139641076 (AddFriendRequest_t1861404448 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddFriendRequest::get_FriendUsername()
extern "C"  String_t* AddFriendRequest_get_FriendUsername_m1083127905 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddFriendRequest::set_FriendUsername(System.String)
extern "C"  void AddFriendRequest_set_FriendUsername_m4096216362 (AddFriendRequest_t1861404448 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddFriendRequest::get_FriendEmail()
extern "C"  String_t* AddFriendRequest_get_FriendEmail_m3920846291 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddFriendRequest::set_FriendEmail(System.String)
extern "C"  void AddFriendRequest_set_FriendEmail_m2877743046 (AddFriendRequest_t1861404448 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddFriendRequest::get_FriendTitleDisplayName()
extern "C"  String_t* AddFriendRequest_get_FriendTitleDisplayName_m325611200 (AddFriendRequest_t1861404448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddFriendRequest::set_FriendTitleDisplayName(System.String)
extern "C"  void AddFriendRequest_set_FriendTitleDisplayName_m1032630315 (AddFriendRequest_t1861404448 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
