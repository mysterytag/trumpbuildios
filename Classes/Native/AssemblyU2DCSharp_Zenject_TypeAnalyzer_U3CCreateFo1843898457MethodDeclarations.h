﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey184
struct U3CCreateForMemberU3Ec__AnonStorey184_t1843898457;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey184::.ctor()
extern "C"  void U3CCreateForMemberU3Ec__AnonStorey184__ctor_m382299826 (U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey184::<>m__2B5(System.Object,System.Object)
extern "C"  void U3CCreateForMemberU3Ec__AnonStorey184_U3CU3Em__2B5_m3878310646 (U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * __this, Il2CppObject * ___injectable0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
