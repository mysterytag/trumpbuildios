﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<System.Object>
struct Reactor_1_t96911316;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<System.Object>::.ctor()
extern "C"  void Reactor_1__ctor_m1161914514_gshared (Reactor_1_t96911316 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m1161914514(__this, method) ((  void (*) (Reactor_1_t96911316 *, const MethodInfo*))Reactor_1__ctor_m1161914514_gshared)(__this, method)
// System.Void Reactor`1<System.Object>::React(T)
extern "C"  void Reactor_1_React_m1196306383_gshared (Reactor_1_t96911316 * __this, Il2CppObject * ___t0, const MethodInfo* method);
#define Reactor_1_React_m1196306383(__this, ___t0, method) ((  void (*) (Reactor_1_t96911316 *, Il2CppObject *, const MethodInfo*))Reactor_1_React_m1196306383_gshared)(__this, ___t0, method)
// System.Void Reactor`1<System.Object>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m1003670938_gshared (Reactor_1_t96911316 * __this, Il2CppObject * ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m1003670938(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t96911316 *, Il2CppObject *, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1003670938_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<System.Object>::Empty()
extern "C"  bool Reactor_1_Empty_m132443017_gshared (Reactor_1_t96911316 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m132443017(__this, method) ((  bool (*) (Reactor_1_t96911316 *, const MethodInfo*))Reactor_1_Empty_m132443017_gshared)(__this, method)
// System.IDisposable Reactor`1<System.Object>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m1495770619_gshared (Reactor_1_t96911316 * __this, Action_1_t985559125 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m1495770619(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t96911316 *, Action_1_t985559125 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m1495770619_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<System.Object>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m912916552_gshared (Reactor_1_t96911316 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m912916552(__this, method) ((  void (*) (Reactor_1_t96911316 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m912916552_gshared)(__this, method)
// System.Void Reactor`1<System.Object>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3381282287_gshared (Reactor_1_t96911316 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m3381282287(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t96911316 *, Action_1_t985559125 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3381282287_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Object>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m4227182179_gshared (Reactor_1_t96911316 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m4227182179(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t96911316 *, Action_1_t985559125 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4227182179_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Object>::Clear()
extern "C"  void Reactor_1_Clear_m2863015101_gshared (Reactor_1_t96911316 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m2863015101(__this, method) ((  void (*) (Reactor_1_t96911316 *, const MethodInfo*))Reactor_1_Clear_m2863015101_gshared)(__this, method)
