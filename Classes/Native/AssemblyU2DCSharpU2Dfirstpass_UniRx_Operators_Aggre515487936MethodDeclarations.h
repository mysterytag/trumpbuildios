﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>
struct Aggregate_t515487936;
// UniRx.Operators.AggregateObservable`1<System.Object>
struct AggregateObservable_1_t497085657;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>::.ctor(UniRx.Operators.AggregateObservable`1<TSource>,UniRx.IObserver`1<TSource>,System.IDisposable)
extern "C"  void Aggregate__ctor_m969920345_gshared (Aggregate_t515487936 * __this, AggregateObservable_1_t497085657 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Aggregate__ctor_m969920345(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Aggregate_t515487936 *, AggregateObservable_1_t497085657 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Aggregate__ctor_m969920345_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>::OnNext(TSource)
extern "C"  void Aggregate_OnNext_m475303852_gshared (Aggregate_t515487936 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Aggregate_OnNext_m475303852(__this, ___value0, method) ((  void (*) (Aggregate_t515487936 *, Il2CppObject *, const MethodInfo*))Aggregate_OnNext_m475303852_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>::OnError(System.Exception)
extern "C"  void Aggregate_OnError_m2844698646_gshared (Aggregate_t515487936 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Aggregate_OnError_m2844698646(__this, ___error0, method) ((  void (*) (Aggregate_t515487936 *, Exception_t1967233988 *, const MethodInfo*))Aggregate_OnError_m2844698646_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AggregateObservable`1/Aggregate<System.Object>::OnCompleted()
extern "C"  void Aggregate_OnCompleted_m3879226473_gshared (Aggregate_t515487936 * __this, const MethodInfo* method);
#define Aggregate_OnCompleted_m3879226473(__this, method) ((  void (*) (Aggregate_t515487936 *, const MethodInfo*))Aggregate_OnCompleted_m3879226473_gshared)(__this, method)
