﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>
struct EmptyObserver_1_t2917748807;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m100393469_gshared (EmptyObserver_1_t2917748807 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m100393469(__this, method) ((  void (*) (EmptyObserver_1_t2917748807 *, const MethodInfo*))EmptyObserver_1__ctor_m100393469_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2630101328_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m2630101328(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m2630101328_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1883431879_gshared (EmptyObserver_1_t2917748807 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1883431879(__this, method) ((  void (*) (EmptyObserver_1_t2917748807 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1883431879_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3498779252_gshared (EmptyObserver_1_t2917748807 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3498779252(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2917748807 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3498779252_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m524579173_gshared (EmptyObserver_1_t2917748807 * __this, int32_t ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m524579173(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2917748807 *, int32_t, const MethodInfo*))EmptyObserver_1_OnNext_m524579173_gshared)(__this, ___value0, method)
