﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BioProcessor`1/Instruction<System.Object>
struct Instruction_t1241108407;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate2585444626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Comparison`1<BioProcessor`1/Instruction<System.Object>>
struct  Comparison_1_t3944783283  : public MulticastDelegate_t2585444626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
