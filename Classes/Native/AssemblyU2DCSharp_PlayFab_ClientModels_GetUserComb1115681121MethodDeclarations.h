﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetUserCombinedInfoResult
struct GetUserCombinedInfoResult_t1115681121;
// System.String
struct String_t;
// PlayFab.ClientModels.UserAccountInfo
struct UserAccountInfo_t960776096;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>
struct Dictionary_2_t3222006224;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>
struct Dictionary_2_t1801537638;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserAccountI960776096.h"

// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::.ctor()
extern "C"  void GetUserCombinedInfoResult__ctor_m169598952 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetUserCombinedInfoResult::get_PlayFabId()
extern "C"  String_t* GetUserCombinedInfoResult_get_PlayFabId_m2004265544 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_PlayFabId(System.String)
extern "C"  void GetUserCombinedInfoResult_set_PlayFabId_m2957130891 (GetUserCombinedInfoResult_t1115681121 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserAccountInfo PlayFab.ClientModels.GetUserCombinedInfoResult::get_AccountInfo()
extern "C"  UserAccountInfo_t960776096 * GetUserCombinedInfoResult_get_AccountInfo_m3596483085 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_AccountInfo(PlayFab.ClientModels.UserAccountInfo)
extern "C"  void GetUserCombinedInfoResult_set_AccountInfo_m1366140242 (GetUserCombinedInfoResult_t1115681121 * __this, UserAccountInfo_t960776096 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetUserCombinedInfoResult::get_Inventory()
extern "C"  List_1_t1322103281 * GetUserCombinedInfoResult_get_Inventory_m2142760690 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_Inventory(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void GetUserCombinedInfoResult_set_Inventory_m4186465263 (GetUserCombinedInfoResult_t1115681121 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserCombinedInfoResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t190145395 * GetUserCombinedInfoResult_get_VirtualCurrency_m680322548 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetUserCombinedInfoResult_set_VirtualCurrency_m3264947039 (GetUserCombinedInfoResult_t1115681121 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime> PlayFab.ClientModels.GetUserCombinedInfoResult::get_VirtualCurrencyRechargeTimes()
extern "C"  Dictionary_2_t3222006224 * GetUserCombinedInfoResult_get_VirtualCurrencyRechargeTimes_m755440880 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_VirtualCurrencyRechargeTimes(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>)
extern "C"  void GetUserCombinedInfoResult_set_VirtualCurrencyRechargeTimes_m3324855051 (GetUserCombinedInfoResult_t1115681121 * __this, Dictionary_2_t3222006224 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetUserCombinedInfoResult::get_Data()
extern "C"  Dictionary_2_t1801537638 * GetUserCombinedInfoResult_get_Data_m2255212337 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>)
extern "C"  void GetUserCombinedInfoResult_set_Data_m3986755880 (GetUserCombinedInfoResult_t1115681121 * __this, Dictionary_2_t1801537638 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.GetUserCombinedInfoResult::get_DataVersion()
extern "C"  uint32_t GetUserCombinedInfoResult_get_DataVersion_m1558072112 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_DataVersion(System.UInt32)
extern "C"  void GetUserCombinedInfoResult_set_DataVersion_m3789715043 (GetUserCombinedInfoResult_t1115681121 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetUserCombinedInfoResult::get_ReadOnlyData()
extern "C"  Dictionary_2_t1801537638 * GetUserCombinedInfoResult_get_ReadOnlyData_m1576777267 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_ReadOnlyData(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>)
extern "C"  void GetUserCombinedInfoResult_set_ReadOnlyData_m1914329002 (GetUserCombinedInfoResult_t1115681121 * __this, Dictionary_2_t1801537638 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.GetUserCombinedInfoResult::get_ReadOnlyDataVersion()
extern "C"  uint32_t GetUserCombinedInfoResult_get_ReadOnlyDataVersion_m476333806 (GetUserCombinedInfoResult_t1115681121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoResult::set_ReadOnlyDataVersion(System.UInt32)
extern "C"  void GetUserCombinedInfoResult_set_ReadOnlyDataVersion_m367810149 (GetUserCombinedInfoResult_t1115681121 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
