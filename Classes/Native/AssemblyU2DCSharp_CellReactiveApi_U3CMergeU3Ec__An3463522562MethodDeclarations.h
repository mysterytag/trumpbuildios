﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>
struct U3CMergeU3Ec__AnonStorey112_3_t3463522562;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey112_3__ctor_m3750145404_gshared (U3CMergeU3Ec__AnonStorey112_3_t3463522562 * __this, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey112_3__ctor_m3750145404(__this, method) ((  void (*) (U3CMergeU3Ec__AnonStorey112_3_t3463522562 *, const MethodInfo*))U3CMergeU3Ec__AnonStorey112_3__ctor_m3750145404_gshared)(__this, method)
// T3 CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>::<>m__186()
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m2750346858_gshared (U3CMergeU3Ec__AnonStorey112_3_t3463522562 * __this, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m2750346858(__this, method) ((  Il2CppObject * (*) (U3CMergeU3Ec__AnonStorey112_3_t3463522562 *, const MethodInfo*))U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m2750346858_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>::<>m__187(System.Action`1<T3>,Priority)
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m2280052025_gshared (U3CMergeU3Ec__AnonStorey112_3_t3463522562 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m2280052025(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CMergeU3Ec__AnonStorey112_3_t3463522562 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m2280052025_gshared)(__this, ___reaction0, ___p1, method)
