﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Double[]
struct DoubleU5BU5D_t1048280995;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpgradeDescriptor
struct  UpgradeDescriptor_t2646513291  : public Il2CppObject
{
public:
	// System.String UpgradeDescriptor::name
	String_t* ___name_0;
	// System.String UpgradeDescriptor::stringID
	String_t* ___stringID_1;
	// System.Double UpgradeDescriptor::price
	double ___price_2;
	// System.Double[] UpgradeDescriptor::priceLevels
	DoubleU5BU5D_t1048280995* ___priceLevels_3;
	// System.Double UpgradeDescriptor::amount
	double ___amount_4;
	// System.Double[] UpgradeDescriptor::amountLevels
	DoubleU5BU5D_t1048280995* ___amountLevels_5;
	// System.Boolean UpgradeDescriptor::isCapacity
	bool ___isCapacity_6;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(UpgradeDescriptor_t2646513291, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_stringID_1() { return static_cast<int32_t>(offsetof(UpgradeDescriptor_t2646513291, ___stringID_1)); }
	inline String_t* get_stringID_1() const { return ___stringID_1; }
	inline String_t** get_address_of_stringID_1() { return &___stringID_1; }
	inline void set_stringID_1(String_t* value)
	{
		___stringID_1 = value;
		Il2CppCodeGenWriteBarrier(&___stringID_1, value);
	}

	inline static int32_t get_offset_of_price_2() { return static_cast<int32_t>(offsetof(UpgradeDescriptor_t2646513291, ___price_2)); }
	inline double get_price_2() const { return ___price_2; }
	inline double* get_address_of_price_2() { return &___price_2; }
	inline void set_price_2(double value)
	{
		___price_2 = value;
	}

	inline static int32_t get_offset_of_priceLevels_3() { return static_cast<int32_t>(offsetof(UpgradeDescriptor_t2646513291, ___priceLevels_3)); }
	inline DoubleU5BU5D_t1048280995* get_priceLevels_3() const { return ___priceLevels_3; }
	inline DoubleU5BU5D_t1048280995** get_address_of_priceLevels_3() { return &___priceLevels_3; }
	inline void set_priceLevels_3(DoubleU5BU5D_t1048280995* value)
	{
		___priceLevels_3 = value;
		Il2CppCodeGenWriteBarrier(&___priceLevels_3, value);
	}

	inline static int32_t get_offset_of_amount_4() { return static_cast<int32_t>(offsetof(UpgradeDescriptor_t2646513291, ___amount_4)); }
	inline double get_amount_4() const { return ___amount_4; }
	inline double* get_address_of_amount_4() { return &___amount_4; }
	inline void set_amount_4(double value)
	{
		___amount_4 = value;
	}

	inline static int32_t get_offset_of_amountLevels_5() { return static_cast<int32_t>(offsetof(UpgradeDescriptor_t2646513291, ___amountLevels_5)); }
	inline DoubleU5BU5D_t1048280995* get_amountLevels_5() const { return ___amountLevels_5; }
	inline DoubleU5BU5D_t1048280995** get_address_of_amountLevels_5() { return &___amountLevels_5; }
	inline void set_amountLevels_5(DoubleU5BU5D_t1048280995* value)
	{
		___amountLevels_5 = value;
		Il2CppCodeGenWriteBarrier(&___amountLevels_5, value);
	}

	inline static int32_t get_offset_of_isCapacity_6() { return static_cast<int32_t>(offsetof(UpgradeDescriptor_t2646513291, ___isCapacity_6)); }
	inline bool get_isCapacity_6() const { return ___isCapacity_6; }
	inline bool* get_address_of_isCapacity_6() { return &___isCapacity_6; }
	inline void set_isCapacity_6(bool value)
	{
		___isCapacity_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
