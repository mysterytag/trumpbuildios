﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<System.Object>
struct SampleFrameTick_t1568093561;
// UniRx.Operators.SampleFrameObservable`1/SampleFrame<System.Object>
struct SampleFrame_t1580910916;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<System.Object>::.ctor(UniRx.Operators.SampleFrameObservable`1/SampleFrame<T>)
extern "C"  void SampleFrameTick__ctor_m2176462886_gshared (SampleFrameTick_t1568093561 * __this, SampleFrame_t1580910916 * ___parent0, const MethodInfo* method);
#define SampleFrameTick__ctor_m2176462886(__this, ___parent0, method) ((  void (*) (SampleFrameTick_t1568093561 *, SampleFrame_t1580910916 *, const MethodInfo*))SampleFrameTick__ctor_m2176462886_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<System.Object>::OnCompleted()
extern "C"  void SampleFrameTick_OnCompleted_m454390114_gshared (SampleFrameTick_t1568093561 * __this, const MethodInfo* method);
#define SampleFrameTick_OnCompleted_m454390114(__this, method) ((  void (*) (SampleFrameTick_t1568093561 *, const MethodInfo*))SampleFrameTick_OnCompleted_m454390114_gshared)(__this, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<System.Object>::OnError(System.Exception)
extern "C"  void SampleFrameTick_OnError_m1682382735_gshared (SampleFrameTick_t1568093561 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SampleFrameTick_OnError_m1682382735(__this, ___error0, method) ((  void (*) (SampleFrameTick_t1568093561 *, Exception_t1967233988 *, const MethodInfo*))SampleFrameTick_OnError_m1682382735_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SampleFrameObservable`1/SampleFrame/SampleFrameTick<System.Object>::OnNext(System.Int64)
extern "C"  void SampleFrameTick_OnNext_m2086951024_gshared (SampleFrameTick_t1568093561 * __this, int64_t ____0, const MethodInfo* method);
#define SampleFrameTick_OnNext_m2086951024(__this, ____0, method) ((  void (*) (SampleFrameTick_t1568093561 *, int64_t, const MethodInfo*))SampleFrameTick_OnNext_m2086951024_gshared)(__this, ____0, method)
