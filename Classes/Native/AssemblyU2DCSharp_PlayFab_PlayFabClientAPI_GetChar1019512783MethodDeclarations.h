﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterInventoryResponseCallback
struct GetCharacterInventoryResponseCallback_t1019512783;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterInventoryRequest
struct GetCharacterInventoryRequest_t2835266646;
// PlayFab.ClientModels.GetCharacterInventoryResult
struct GetCharacterInventoryResult_t403702678;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2835266646.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacter403702678.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterInventoryResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterInventoryResponseCallback__ctor_m3342941406 (GetCharacterInventoryResponseCallback_t1019512783 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterInventoryResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterInventoryRequest,PlayFab.ClientModels.GetCharacterInventoryResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetCharacterInventoryResponseCallback_Invoke_m3066806747 (GetCharacterInventoryResponseCallback_t1019512783 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterInventoryRequest_t2835266646 * ___request2, GetCharacterInventoryResult_t403702678 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterInventoryResponseCallback_t1019512783(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterInventoryRequest_t2835266646 * ___request2, GetCharacterInventoryResult_t403702678 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterInventoryResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterInventoryRequest,PlayFab.ClientModels.GetCharacterInventoryResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterInventoryResponseCallback_BeginInvoke_m4235192816 (GetCharacterInventoryResponseCallback_t1019512783 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterInventoryRequest_t2835266646 * ___request2, GetCharacterInventoryResult_t403702678 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterInventoryResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterInventoryResponseCallback_EndInvoke_m671206894 (GetCharacterInventoryResponseCallback_t1019512783 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
