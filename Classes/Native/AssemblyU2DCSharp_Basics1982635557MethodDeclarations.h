﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Basics
struct Basics_t1982635557;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void Basics::.ctor()
extern "C"  void Basics__ctor_m2852773910 (Basics_t1982635557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Basics::Start()
extern "C"  void Basics_Start_m1799911702 (Basics_t1982635557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Basics::<Start>m__4()
extern "C"  Vector3_t3525329789  Basics_U3CStartU3Em__4_m1871315545 (Basics_t1982635557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Basics::<Start>m__5(UnityEngine.Vector3)
extern "C"  void Basics_U3CStartU3Em__5_m4051811333 (Basics_t1982635557 * __this, Vector3_t3525329789  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
