﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<EveryUpdateCore>c__Iterator11
struct U3CEveryUpdateCoreU3Ec__Iterator11_t819860698;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<EveryUpdateCore>c__Iterator11::.ctor()
extern "C"  void U3CEveryUpdateCoreU3Ec__Iterator11__ctor_m4046846889 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<EveryUpdateCore>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEveryUpdateCoreU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m824106057 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<EveryUpdateCore>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEveryUpdateCoreU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m2830930397 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Observable/<EveryUpdateCore>c__Iterator11::MoveNext()
extern "C"  bool U3CEveryUpdateCoreU3Ec__Iterator11_MoveNext_m2399911363 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<EveryUpdateCore>c__Iterator11::Dispose()
extern "C"  void U3CEveryUpdateCoreU3Ec__Iterator11_Dispose_m1972740518 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<EveryUpdateCore>c__Iterator11::Reset()
extern "C"  void U3CEveryUpdateCoreU3Ec__Iterator11_Reset_m1693279830 (U3CEveryUpdateCoreU3Ec__Iterator11_t819860698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
