﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1549138861MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m19725615(__this, ___dictionary0, method) ((  void (*) (Enumerator_t199518035 *, Dictionary_2_t432490094 *, const MethodInfo*))Enumerator__ctor_m2830618433_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m809524124(__this, method) ((  Il2CppObject * (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4162971594_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4098085926(__this, method) ((  void (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1989515988_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2873399645(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m321263499_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3777721208(__this, method) ((  Il2CppObject * (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3859981606_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2191152138(__this, method) ((  Il2CppObject * (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3933983288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::MoveNext()
#define Enumerator_MoveNext_m1597398230(__this, method) ((  bool (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_MoveNext_m3250803844_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::get_Current()
#define Enumerator_get_Current_m934944294(__this, method) ((  KeyValuePair_2_t4215988688  (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_get_Current_m2856640440_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3350998239(__this, method) ((  String_t* (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_get_CurrentKey_m686828045_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1205629279(__this, method) ((  int32_t (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_get_CurrentValue_m738581261_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::Reset()
#define Enumerator_Reset_m1854488833(__this, method) ((  void (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_Reset_m1247877139_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::VerifyState()
#define Enumerator_VerifyState_m2144209994(__this, method) ((  void (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_VerifyState_m4065906140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1746373938(__this, method) ((  void (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_VerifyCurrent_m1660432964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.UUnit.Region>::Dispose()
#define Enumerator_Dispose_m2275769745(__this, method) ((  void (*) (Enumerator_t199518035 *, const MethodInfo*))Enumerator_Dispose_m3437484067_gshared)(__this, method)
