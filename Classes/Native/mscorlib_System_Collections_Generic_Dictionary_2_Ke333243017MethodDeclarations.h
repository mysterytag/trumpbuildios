﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Double,System.Object>
struct Dictionary_2_t566215076;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke333243017.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Double,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1276896517_gshared (Enumerator_t333243017 * __this, Dictionary_2_t566215076 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1276896517(__this, ___host0, method) ((  void (*) (Enumerator_t333243017 *, Dictionary_2_t566215076 *, const MethodInfo*))Enumerator__ctor_m1276896517_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Double,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1291058428_gshared (Enumerator_t333243017 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1291058428(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243017 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1291058428_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Double,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4227003024_gshared (Enumerator_t333243017 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4227003024(__this, method) ((  void (*) (Enumerator_t333243017 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4227003024_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Double,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1674880743_gshared (Enumerator_t333243017 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1674880743(__this, method) ((  void (*) (Enumerator_t333243017 *, const MethodInfo*))Enumerator_Dispose_m1674880743_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Double,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1929857788_gshared (Enumerator_t333243017 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1929857788(__this, method) ((  bool (*) (Enumerator_t333243017 *, const MethodInfo*))Enumerator_MoveNext_m1929857788_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Double,System.Object>::get_Current()
extern "C"  double Enumerator_get_Current_m3203115736_gshared (Enumerator_t333243017 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3203115736(__this, method) ((  double (*) (Enumerator_t333243017 *, const MethodInfo*))Enumerator_get_Current_m3203115736_gshared)(__this, method)
