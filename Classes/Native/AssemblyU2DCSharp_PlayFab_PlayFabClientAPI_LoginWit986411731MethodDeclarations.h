﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithSteamRequestCallback
struct LoginWithSteamRequestCallback_t986411731;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithSteamRequest
struct LoginWithSteamRequest_t700401790;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithSte700401790.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithSteamRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithSteamRequestCallback__ctor_m58312162 (LoginWithSteamRequestCallback_t986411731 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithSteamRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithSteamRequest,System.Object)
extern "C"  void LoginWithSteamRequestCallback_Invoke_m1690341043 (LoginWithSteamRequestCallback_t986411731 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithSteamRequest_t700401790 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithSteamRequestCallback_t986411731(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithSteamRequest_t700401790 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithSteamRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithSteamRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithSteamRequestCallback_BeginInvoke_m3704602622 (LoginWithSteamRequestCallback_t986411731 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithSteamRequest_t700401790 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithSteamRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithSteamRequestCallback_EndInvoke_m684685554 (LoginWithSteamRequestCallback_t986411731 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
