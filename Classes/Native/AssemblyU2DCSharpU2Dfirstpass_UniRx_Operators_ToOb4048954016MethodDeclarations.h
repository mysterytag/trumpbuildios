﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ToObservableObservable`1<System.Object>
struct ToObservableObservable_1_t4048954016;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ToObservableObservable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>,UniRx.IScheduler)
extern "C"  void ToObservableObservable_1__ctor_m1869527713_gshared (ToObservableObservable_1_t4048954016 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ToObservableObservable_1__ctor_m1869527713(__this, ___source0, ___scheduler1, method) ((  void (*) (ToObservableObservable_1_t4048954016 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ToObservableObservable_1__ctor_m1869527713_gshared)(__this, ___source0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.ToObservableObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ToObservableObservable_1_SubscribeCore_m2642246180_gshared (ToObservableObservable_1_t4048954016 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ToObservableObservable_1_SubscribeCore_m2642246180(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ToObservableObservable_1_t4048954016 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ToObservableObservable_1_SubscribeCore_m2642246180_gshared)(__this, ___observer0, ___cancel1, method)
