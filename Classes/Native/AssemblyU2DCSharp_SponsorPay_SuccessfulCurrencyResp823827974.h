﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.SuccessfulCurrencyResponse
struct  SuccessfulCurrencyResponse_t823827974  : public Il2CppObject
{
public:
	// System.String SponsorPay.SuccessfulCurrencyResponse::<LatestTransactionId>k__BackingField
	String_t* ___U3CLatestTransactionIdU3Ek__BackingField_0;
	// System.Double SponsorPay.SuccessfulCurrencyResponse::<DeltaOfCoins>k__BackingField
	double ___U3CDeltaOfCoinsU3Ek__BackingField_1;
	// System.String SponsorPay.SuccessfulCurrencyResponse::<CurrencyId>k__BackingField
	String_t* ___U3CCurrencyIdU3Ek__BackingField_2;
	// System.String SponsorPay.SuccessfulCurrencyResponse::<CurrencyName>k__BackingField
	String_t* ___U3CCurrencyNameU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CLatestTransactionIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SuccessfulCurrencyResponse_t823827974, ___U3CLatestTransactionIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CLatestTransactionIdU3Ek__BackingField_0() const { return ___U3CLatestTransactionIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLatestTransactionIdU3Ek__BackingField_0() { return &___U3CLatestTransactionIdU3Ek__BackingField_0; }
	inline void set_U3CLatestTransactionIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CLatestTransactionIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLatestTransactionIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CDeltaOfCoinsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SuccessfulCurrencyResponse_t823827974, ___U3CDeltaOfCoinsU3Ek__BackingField_1)); }
	inline double get_U3CDeltaOfCoinsU3Ek__BackingField_1() const { return ___U3CDeltaOfCoinsU3Ek__BackingField_1; }
	inline double* get_address_of_U3CDeltaOfCoinsU3Ek__BackingField_1() { return &___U3CDeltaOfCoinsU3Ek__BackingField_1; }
	inline void set_U3CDeltaOfCoinsU3Ek__BackingField_1(double value)
	{
		___U3CDeltaOfCoinsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCurrencyIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SuccessfulCurrencyResponse_t823827974, ___U3CCurrencyIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CCurrencyIdU3Ek__BackingField_2() const { return ___U3CCurrencyIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCurrencyIdU3Ek__BackingField_2() { return &___U3CCurrencyIdU3Ek__BackingField_2; }
	inline void set_U3CCurrencyIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CCurrencyIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrencyIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCurrencyNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SuccessfulCurrencyResponse_t823827974, ___U3CCurrencyNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CCurrencyNameU3Ek__BackingField_3() const { return ___U3CCurrencyNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CCurrencyNameU3Ek__BackingField_3() { return &___U3CCurrencyNameU3Ek__BackingField_3; }
	inline void set_U3CCurrencyNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CCurrencyNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrencyNameU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
