﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Collections.Generic.List`1<Counter>
struct List_1_t3419442901;
// System.String
struct String_t;
// GlobalStat
struct GlobalStat_t1134678199;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoneyCounter
struct  MoneyCounter_t199543100  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject MoneyCounter::counterSample
	GameObject_t4012695102 * ___counterSample_2;
	// System.Collections.Generic.List`1<Counter> MoneyCounter::counters
	List_1_t3419442901 * ___counters_3;
	// System.String MoneyCounter::currentValue
	String_t* ___currentValue_4;
	// GlobalStat MoneyCounter::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_5;

public:
	inline static int32_t get_offset_of_counterSample_2() { return static_cast<int32_t>(offsetof(MoneyCounter_t199543100, ___counterSample_2)); }
	inline GameObject_t4012695102 * get_counterSample_2() const { return ___counterSample_2; }
	inline GameObject_t4012695102 ** get_address_of_counterSample_2() { return &___counterSample_2; }
	inline void set_counterSample_2(GameObject_t4012695102 * value)
	{
		___counterSample_2 = value;
		Il2CppCodeGenWriteBarrier(&___counterSample_2, value);
	}

	inline static int32_t get_offset_of_counters_3() { return static_cast<int32_t>(offsetof(MoneyCounter_t199543100, ___counters_3)); }
	inline List_1_t3419442901 * get_counters_3() const { return ___counters_3; }
	inline List_1_t3419442901 ** get_address_of_counters_3() { return &___counters_3; }
	inline void set_counters_3(List_1_t3419442901 * value)
	{
		___counters_3 = value;
		Il2CppCodeGenWriteBarrier(&___counters_3, value);
	}

	inline static int32_t get_offset_of_currentValue_4() { return static_cast<int32_t>(offsetof(MoneyCounter_t199543100, ___currentValue_4)); }
	inline String_t* get_currentValue_4() const { return ___currentValue_4; }
	inline String_t** get_address_of_currentValue_4() { return &___currentValue_4; }
	inline void set_currentValue_4(String_t* value)
	{
		___currentValue_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentValue_4, value);
	}

	inline static int32_t get_offset_of_GlobalStat_5() { return static_cast<int32_t>(offsetof(MoneyCounter_t199543100, ___GlobalStat_5)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_5() const { return ___GlobalStat_5; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_5() { return &___GlobalStat_5; }
	inline void set_GlobalStat_5(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_5 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
