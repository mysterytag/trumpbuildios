﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IStream`1<UnityEngine.EventSystems.BaseEventData>
struct IStream_1_t4099794717;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t2937500249;
// IEmptyStream
struct IEmptyStream_t3684082468;
// UnityEngine.UI.Button
struct Button_t990034267;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2937500249.h"
#include "UnityEngine_UI_UnityEngine_UI_Button990034267.h"

// IStream`1<UnityEngine.EventSystems.BaseEventData> ZergEngineTools::PressStream(UnityEngine.EventSystems.EventTrigger)
extern "C"  Il2CppObject* ZergEngineTools_PressStream_m2131367255 (Il2CppObject * __this /* static, unused */, EventTrigger_t2937500249 * ___trigger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IEmptyStream ZergEngineTools::ClickStream(UnityEngine.UI.Button)
extern "C"  Il2CppObject * ZergEngineTools_ClickStream_m2271378604 (Il2CppObject * __this /* static, unused */, Button_t990034267 * ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IEmptyStream ZergEngineTools::HoldStream(UnityEngine.UI.Button)
extern "C"  Il2CppObject * ZergEngineTools_HoldStream_m641685669 (Il2CppObject * __this /* static, unused */, Button_t990034267 * ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
