﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UnityEngine.GameObject>::.ctor()
#define Subject_1__ctor_m591646648(__this, method) ((  void (*) (Subject_1_t1655762426 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.GameObject>::get_HasObservers()
#define Subject_1_get_HasObservers_m1937075484(__this, method) ((  bool (*) (Subject_1_t1655762426 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.GameObject>::OnCompleted()
#define Subject_1_OnCompleted_m89282882(__this, method) ((  void (*) (Subject_1_t1655762426 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.GameObject>::OnError(System.Exception)
#define Subject_1_OnError_m3255252847(__this, ___error0, method) ((  void (*) (Subject_1_t1655762426 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.GameObject>::OnNext(T)
#define Subject_1_OnNext_m172481632(__this, ___value0, method) ((  void (*) (Subject_1_t1655762426 *, GameObject_t4012695102 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.GameObject>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m1447485581(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1655762426 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.GameObject>::Dispose()
#define Subject_1_Dispose_m1535028725(__this, method) ((  void (*) (Subject_1_t1655762426 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.GameObject>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m824021630(__this, method) ((  void (*) (Subject_1_t1655762426 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.GameObject>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1063471891(__this, method) ((  bool (*) (Subject_1_t1655762426 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
