﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1/<OnChanged>c__AnonStoreyF3<System.Object>
struct U3COnChangedU3Ec__AnonStoreyF3_t4213255184;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Object>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m535646698_gshared (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3__ctor_m535646698(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3__ctor_m535646698_gshared)(__this, method)
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Object>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m1355065186_gshared (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m1355065186(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 *, Il2CppObject *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m1355065186_gshared)(__this, ____0, method)
