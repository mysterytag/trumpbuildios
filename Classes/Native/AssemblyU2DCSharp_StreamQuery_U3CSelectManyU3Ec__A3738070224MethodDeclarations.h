﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamQuery/<SelectMany>c__AnonStorey13C`2<System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamQuery/<SelectMany>c__AnonStorey13C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey13C_2__ctor_m3152285819_gshared (U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey13C_2__ctor_m3152285819(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey13C_2__ctor_m3152285819_gshared)(__this, method)
// IStream`1<TR> StreamQuery/<SelectMany>c__AnonStorey13C`2<System.Object,System.Object>::<>m__1D0(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey13C_2_U3CU3Em__1D0_m3907841112_gshared (U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey13C_2_U3CU3Em__1D0_m3907841112(__this, ____0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey13C_2_U3CU3Em__1D0_m3907841112_gshared)(__this, ____0, method)
