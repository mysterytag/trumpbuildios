﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_5_t2227126508;
// Zenject.IFactory`4<System.Object,System.Object,System.Object,System.Object>
struct IFactory_4_t1894544701;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`4<TParam1,TParam2,TParam3,TConcrete>)
extern "C"  void FactoryNested_5__ctor_m755263086_gshared (FactoryNested_5_t2227126508 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_5__ctor_m755263086(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_5_t2227126508 *, Il2CppObject*, const MethodInfo*))FactoryNested_5__ctor_m755263086_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3)
extern "C"  Il2CppObject * FactoryNested_5_Create_m4020896240_gshared (FactoryNested_5_t2227126508 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, const MethodInfo* method);
#define FactoryNested_5_Create_m4020896240(__this, ___param10, ___param21, ___param32, method) ((  Il2CppObject * (*) (FactoryNested_5_t2227126508 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryNested_5_Create_m4020896240_gshared)(__this, ___param10, ___param21, ___param32, method)
