﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Ionic.Zlib.ZlibBaseStream
struct ZlibBaseStream_t221596009;

#include "mscorlib_System_IO_Stream219029575.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZlibStream
struct  ZlibStream_t1994478776  : public Stream_t219029575
{
public:
	// Ionic.Zlib.ZlibBaseStream Ionic.Zlib.ZlibStream::_baseStream
	ZlibBaseStream_t221596009 * ____baseStream_1;
	// System.Boolean Ionic.Zlib.ZlibStream::_disposed
	bool ____disposed_2;

public:
	inline static int32_t get_offset_of__baseStream_1() { return static_cast<int32_t>(offsetof(ZlibStream_t1994478776, ____baseStream_1)); }
	inline ZlibBaseStream_t221596009 * get__baseStream_1() const { return ____baseStream_1; }
	inline ZlibBaseStream_t221596009 ** get_address_of__baseStream_1() { return &____baseStream_1; }
	inline void set__baseStream_1(ZlibBaseStream_t221596009 * value)
	{
		____baseStream_1 = value;
		Il2CppCodeGenWriteBarrier(&____baseStream_1, value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(ZlibStream_t1994478776, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
