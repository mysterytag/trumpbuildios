﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PreviewIconProvider
struct PreviewIconProvider_t2704585042;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3012272455;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// System.Void PreviewIconProvider::.ctor()
extern "C"  void PreviewIconProvider__ctor_m572964441 (PreviewIconProvider_t2704585042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PreviewIconProvider::Awake(UnityEngine.MonoBehaviour)
extern "C"  void PreviewIconProvider_Awake_m3179085587 (PreviewIconProvider_t2704585042 * __this, MonoBehaviour_t3012272455 * ___baseClass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
