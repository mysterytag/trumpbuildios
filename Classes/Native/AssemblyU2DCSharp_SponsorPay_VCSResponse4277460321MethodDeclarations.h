﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.VCSResponse
struct VCSResponse_t4277460321;
// SponsorPay.RequestError
struct RequestError_t1597064339;
// SponsorPay.SuccessfulCurrencyResponse
struct SuccessfulCurrencyResponse_t823827974;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SponsorPay_RequestError1597064339.h"
#include "AssemblyU2DCSharp_SponsorPay_SuccessfulCurrencyResp823827974.h"

// System.Void SponsorPay.VCSResponse::.ctor()
extern "C"  void VCSResponse__ctor_m2504108404 (VCSResponse_t4277460321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SponsorPay.RequestError SponsorPay.VCSResponse::get_error()
extern "C"  RequestError_t1597064339 * VCSResponse_get_error_m3702254935 (VCSResponse_t4277460321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.VCSResponse::set_error(SponsorPay.RequestError)
extern "C"  void VCSResponse_set_error_m678609436 (VCSResponse_t4277460321 * __this, RequestError_t1597064339 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SponsorPay.SuccessfulCurrencyResponse SponsorPay.VCSResponse::get_transaction()
extern "C"  SuccessfulCurrencyResponse_t823827974 * VCSResponse_get_transaction_m1036391200 (VCSResponse_t4277460321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.VCSResponse::set_transaction(SponsorPay.SuccessfulCurrencyResponse)
extern "C"  void VCSResponse_set_transaction_m2149630835 (VCSResponse_t4277460321 * __this, SuccessfulCurrencyResponse_t823827974 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
