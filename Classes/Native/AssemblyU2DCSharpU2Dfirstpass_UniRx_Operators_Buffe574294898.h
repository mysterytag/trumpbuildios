﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2067743705.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`1<System.Object>
struct  BufferObservable_1_t574294898  : public OperatorObservableBase_1_t2067743705
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.BufferObservable`1::source
	Il2CppObject* ___source_1;
	// System.Int32 UniRx.Operators.BufferObservable`1::count
	int32_t ___count_2;
	// System.Int32 UniRx.Operators.BufferObservable`1::skip
	int32_t ___skip_3;
	// System.TimeSpan UniRx.Operators.BufferObservable`1::timeSpan
	TimeSpan_t763862892  ___timeSpan_4;
	// System.TimeSpan UniRx.Operators.BufferObservable`1::timeShift
	TimeSpan_t763862892  ___timeShift_5;
	// UniRx.IScheduler UniRx.Operators.BufferObservable`1::scheduler
	Il2CppObject * ___scheduler_6;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(BufferObservable_1_t574294898, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(BufferObservable_1_t574294898, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_skip_3() { return static_cast<int32_t>(offsetof(BufferObservable_1_t574294898, ___skip_3)); }
	inline int32_t get_skip_3() const { return ___skip_3; }
	inline int32_t* get_address_of_skip_3() { return &___skip_3; }
	inline void set_skip_3(int32_t value)
	{
		___skip_3 = value;
	}

	inline static int32_t get_offset_of_timeSpan_4() { return static_cast<int32_t>(offsetof(BufferObservable_1_t574294898, ___timeSpan_4)); }
	inline TimeSpan_t763862892  get_timeSpan_4() const { return ___timeSpan_4; }
	inline TimeSpan_t763862892 * get_address_of_timeSpan_4() { return &___timeSpan_4; }
	inline void set_timeSpan_4(TimeSpan_t763862892  value)
	{
		___timeSpan_4 = value;
	}

	inline static int32_t get_offset_of_timeShift_5() { return static_cast<int32_t>(offsetof(BufferObservable_1_t574294898, ___timeShift_5)); }
	inline TimeSpan_t763862892  get_timeShift_5() const { return ___timeShift_5; }
	inline TimeSpan_t763862892 * get_address_of_timeShift_5() { return &___timeShift_5; }
	inline void set_timeShift_5(TimeSpan_t763862892  value)
	{
		___timeShift_5 = value;
	}

	inline static int32_t get_offset_of_scheduler_6() { return static_cast<int32_t>(offsetof(BufferObservable_1_t574294898, ___scheduler_6)); }
	inline Il2CppObject * get_scheduler_6() const { return ___scheduler_6; }
	inline Il2CppObject ** get_address_of_scheduler_6() { return &___scheduler_6; }
	inline void set_scheduler_6(Il2CppObject * value)
	{
		___scheduler_6 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
