﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SuccessfulCurrencyResponse
struct SuccessfulCurrencyResponse_t823827974;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SponsorPay.SuccessfulCurrencyResponse::.ctor()
extern "C"  void SuccessfulCurrencyResponse__ctor_m1242260991 (SuccessfulCurrencyResponse_t823827974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SuccessfulCurrencyResponse::get_LatestTransactionId()
extern "C"  String_t* SuccessfulCurrencyResponse_get_LatestTransactionId_m3728293353 (SuccessfulCurrencyResponse_t823827974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SuccessfulCurrencyResponse::set_LatestTransactionId(System.String)
extern "C"  void SuccessfulCurrencyResponse_set_LatestTransactionId_m3754470512 (SuccessfulCurrencyResponse_t823827974 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double SponsorPay.SuccessfulCurrencyResponse::get_DeltaOfCoins()
extern "C"  double SuccessfulCurrencyResponse_get_DeltaOfCoins_m600023358 (SuccessfulCurrencyResponse_t823827974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SuccessfulCurrencyResponse::set_DeltaOfCoins(System.Double)
extern "C"  void SuccessfulCurrencyResponse_set_DeltaOfCoins_m3523758509 (SuccessfulCurrencyResponse_t823827974 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SuccessfulCurrencyResponse::get_CurrencyId()
extern "C"  String_t* SuccessfulCurrencyResponse_get_CurrencyId_m3764643127 (SuccessfulCurrencyResponse_t823827974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SuccessfulCurrencyResponse::set_CurrencyId(System.String)
extern "C"  void SuccessfulCurrencyResponse_set_CurrencyId_m1927252116 (SuccessfulCurrencyResponse_t823827974 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.SuccessfulCurrencyResponse::get_CurrencyName()
extern "C"  String_t* SuccessfulCurrencyResponse_get_CurrencyName_m1602071527 (SuccessfulCurrencyResponse_t823827974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SuccessfulCurrencyResponse::set_CurrencyName(System.String)
extern "C"  void SuccessfulCurrencyResponse_set_CurrencyName_m3564136868 (SuccessfulCurrencyResponse_t823827974 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
