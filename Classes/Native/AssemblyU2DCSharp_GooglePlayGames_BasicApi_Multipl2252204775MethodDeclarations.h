﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.BasicApi.Multiplayer.Player
struct Player_t2252204775;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void GooglePlayGames.BasicApi.Multiplayer.Player::.ctor(System.String,System.String,System.String)
extern "C"  void Player__ctor_m3194827276 (Player_t2252204775 * __this, String_t* ___displayName0, String_t* ___playerId1, String_t* ___avatarUrl2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_DisplayName()
extern "C"  String_t* Player_get_DisplayName_m1265507699 (Player_t2252204775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_PlayerId()
extern "C"  String_t* Player_get_PlayerId_m3285308088 (Player_t2252204775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_AvatarURL()
extern "C"  String_t* Player_get_AvatarURL_m2911050588 (Player_t2252204775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::ToString()
extern "C"  String_t* Player_ToString_m1794196927 (Player_t2252204775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Player::Equals(System.Object)
extern "C"  bool Player_Equals_m2985335891 (Player_t2252204775 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Player::GetHashCode()
extern "C"  int32_t Player_GetHashCode_m3054122987 (Player_t2252204775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
