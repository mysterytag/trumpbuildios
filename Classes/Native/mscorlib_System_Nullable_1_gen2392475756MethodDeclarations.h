﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2392475756.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserMaritalStatus3801405144.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<SponsorPay.SPUserMaritalStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m309386687_gshared (Nullable_1_t2392475756 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m309386687(__this, ___value0, method) ((  void (*) (Nullable_1_t2392475756 *, int32_t, const MethodInfo*))Nullable_1__ctor_m309386687_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserMaritalStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1851021539_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1851021539(__this, method) ((  bool (*) (Nullable_1_t2392475756 *, const MethodInfo*))Nullable_1_get_HasValue_m1851021539_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserMaritalStatus>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2109224548_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2109224548(__this, method) ((  int32_t (*) (Nullable_1_t2392475756 *, const MethodInfo*))Nullable_1_get_Value_m2109224548_gshared)(__this, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserMaritalStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3422417970_gshared (Nullable_1_t2392475756 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3422417970(__this, ___other0, method) ((  bool (*) (Nullable_1_t2392475756 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3422417970_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserMaritalStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1772348109_gshared (Nullable_1_t2392475756 * __this, Nullable_1_t2392475756  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1772348109(__this, ___other0, method) ((  bool (*) (Nullable_1_t2392475756 *, Nullable_1_t2392475756 , const MethodInfo*))Nullable_1_Equals_m1772348109_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<SponsorPay.SPUserMaritalStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m341695754_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m341695754(__this, method) ((  int32_t (*) (Nullable_1_t2392475756 *, const MethodInfo*))Nullable_1_GetHashCode_m341695754_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserMaritalStatus>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2775427775_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2775427775(__this, method) ((  int32_t (*) (Nullable_1_t2392475756 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2775427775_gshared)(__this, method)
// System.String System.Nullable`1<SponsorPay.SPUserMaritalStatus>::ToString()
extern "C"  String_t* Nullable_1_ToString_m4163853902_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m4163853902(__this, method) ((  String_t* (*) (Nullable_1_t2392475756 *, const MethodInfo*))Nullable_1_ToString_m4163853902_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserMaritalStatus>::op_Implicit(T)
extern "C"  Nullable_1_t2392475756  Nullable_1_op_Implicit_m3654641032_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m3654641032(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2392475756  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m3654641032_gshared)(__this /* static, unused */, ___value0, method)
