﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ScanObservable`2<System.Object,System.Object>
struct ScanObservable_2_t1879023476;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.ScanObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
extern "C"  void ScanObservable_2__ctor_m1747570239_gshared (ScanObservable_2_t1879023476 * __this, Il2CppObject* ___source0, Il2CppObject * ___seed1, Func_3_t1892209229 * ___accumulator2, const MethodInfo* method);
#define ScanObservable_2__ctor_m1747570239(__this, ___source0, ___seed1, ___accumulator2, method) ((  void (*) (ScanObservable_2_t1879023476 *, Il2CppObject*, Il2CppObject *, Func_3_t1892209229 *, const MethodInfo*))ScanObservable_2__ctor_m1747570239_gshared)(__this, ___source0, ___seed1, ___accumulator2, method)
// System.IDisposable UniRx.Operators.ScanObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TAccumulate>,System.IDisposable)
extern "C"  Il2CppObject * ScanObservable_2_SubscribeCore_m400497488_gshared (ScanObservable_2_t1879023476 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ScanObservable_2_SubscribeCore_m400497488(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ScanObservable_2_t1879023476 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ScanObservable_2_SubscribeCore_m400497488_gshared)(__this, ___observer0, ___cancel1, method)
