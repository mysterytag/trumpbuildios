﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergEngineTools/<ClickStream>c__AnonStorey14F
struct U3CClickStreamU3Ec__AnonStorey14F_t3915235342;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void ZergEngineTools/<ClickStream>c__AnonStorey14F::.ctor()
extern "C"  void U3CClickStreamU3Ec__AnonStorey14F__ctor_m998780435 (U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable ZergEngineTools/<ClickStream>c__AnonStorey14F::<>m__1FC(System.Action,Priority)
extern "C"  Il2CppObject * U3CClickStreamU3Ec__AnonStorey14F_U3CU3Em__1FC_m2237704742 (U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
