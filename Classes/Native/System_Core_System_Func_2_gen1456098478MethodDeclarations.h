﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<Zenject.ITickable,System.Type>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m26258046(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1456098478 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Zenject.ITickable,System.Type>::Invoke(T)
#define Func_2_Invoke_m1142158612(__this, ___arg10, method) ((  Type_t * (*) (Func_2_t1456098478 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Zenject.ITickable,System.Type>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m845040067(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1456098478 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Zenject.ITickable,System.Type>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3534619456(__this, ___result0, method) ((  Type_t * (*) (Func_2_t1456098478 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
