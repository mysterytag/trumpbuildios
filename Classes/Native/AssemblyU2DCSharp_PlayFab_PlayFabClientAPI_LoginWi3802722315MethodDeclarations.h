﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithGoogleAccountResponseCallback
struct LoginWithGoogleAccountResponseCallback_t3802722315;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithGoogleAccountRequest
struct LoginWithGoogleAccountRequest_t692379802;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithGoo692379802.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithGoogleAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithGoogleAccountResponseCallback__ctor_m3054658426 (LoginWithGoogleAccountResponseCallback_t3802722315 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithGoogleAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithGoogleAccountRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LoginWithGoogleAccountResponseCallback_Invoke_m948801623 (LoginWithGoogleAccountResponseCallback_t3802722315 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithGoogleAccountRequest_t692379802 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithGoogleAccountResponseCallback_t3802722315(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithGoogleAccountRequest_t692379802 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithGoogleAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithGoogleAccountRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithGoogleAccountResponseCallback_BeginInvoke_m3322933490 (LoginWithGoogleAccountResponseCallback_t3802722315 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithGoogleAccountRequest_t692379802 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithGoogleAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithGoogleAccountResponseCallback_EndInvoke_m1690739338 (LoginWithGoogleAccountResponseCallback_t3802722315 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
