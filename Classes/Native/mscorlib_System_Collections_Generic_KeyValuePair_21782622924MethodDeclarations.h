﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3205944487(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1782622924 *, Type_t *, SetDelegate_t181543911 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m2734305540(__this, method) ((  Type_t * (*) (KeyValuePair_2_t1782622924 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2439857802(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1782622924 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m255295895(__this, method) ((  SetDelegate_t181543911 * (*) (KeyValuePair_2_t1782622924 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3914220938(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1782622924 *, SetDelegate_t181543911 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>::ToString()
#define KeyValuePair_2_ToString_m2755195198(__this, method) ((  String_t* (*) (KeyValuePair_2_t1782622924 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
