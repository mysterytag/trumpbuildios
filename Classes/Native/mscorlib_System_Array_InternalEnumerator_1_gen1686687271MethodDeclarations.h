﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1686687271.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21779196768.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1857822422_gshared (InternalEnumerator_1_t1686687271 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1857822422(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1686687271 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1857822422_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m75139850_gshared (InternalEnumerator_1_t1686687271 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m75139850(__this, method) ((  void (*) (InternalEnumerator_1_t1686687271 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m75139850_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4178616128_gshared (InternalEnumerator_1_t1686687271 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4178616128(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1686687271 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4178616128_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3109297197_gshared (InternalEnumerator_1_t1686687271 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3109297197(__this, method) ((  void (*) (InternalEnumerator_1_t1686687271 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3109297197_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1949340474_gshared (InternalEnumerator_1_t1686687271 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1949340474(__this, method) ((  bool (*) (InternalEnumerator_1_t1686687271 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1949340474_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t1779196768  InternalEnumerator_1_get_Current_m4220560959_gshared (InternalEnumerator_1_t1686687271 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4220560959(__this, method) ((  KeyValuePair_2_t1779196768  (*) (InternalEnumerator_1_t1686687271 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4220560959_gshared)(__this, method)
