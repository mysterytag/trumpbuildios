﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetStoreItems>c__AnonStorey9C
struct U3CGetStoreItemsU3Ec__AnonStorey9C_t3359217004;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetStoreItems>c__AnonStorey9C::.ctor()
extern "C"  void U3CGetStoreItemsU3Ec__AnonStorey9C__ctor_m1423103847 (U3CGetStoreItemsU3Ec__AnonStorey9C_t3359217004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetStoreItems>c__AnonStorey9C::<>m__89(PlayFab.CallRequestContainer)
extern "C"  void U3CGetStoreItemsU3Ec__AnonStorey9C_U3CU3Em__89_m180799302 (U3CGetStoreItemsU3Ec__AnonStorey9C_t3359217004 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
