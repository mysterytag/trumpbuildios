﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>
struct ResponseCallback_2_t3240762047;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ResponseCallback_2__ctor_m3836631829_gshared (ResponseCallback_2_t3240762047 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ResponseCallback_2__ctor_m3836631829(__this, ___object0, ___method1, method) ((  void (*) (ResponseCallback_2_t3240762047 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ResponseCallback_2__ctor_m3836631829_gshared)(__this, ___object0, ___method1, method)
// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>::Invoke(System.String,System.Int32,TRequest,TResult,PlayFab.PlayFabError,System.Object)
extern "C"  void ResponseCallback_2_Invoke_m2226430690_gshared (ResponseCallback_2_t3240762047 * __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method);
#define ResponseCallback_2_Invoke_m2226430690(__this, ___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, method) ((  void (*) (ResponseCallback_2_t3240762047 *, String_t*, int32_t, Il2CppObject *, Il2CppObject *, PlayFabError_t750598646 *, Il2CppObject *, const MethodInfo*))ResponseCallback_2_Invoke_m2226430690_gshared)(__this, ___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, method)
// System.IAsyncResult PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>::BeginInvoke(System.String,System.Int32,TRequest,TResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResponseCallback_2_BeginInvoke_m1492811023_gshared (ResponseCallback_2_t3240762047 * __this, String_t* ___urlPath0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method);
#define ResponseCallback_2_BeginInvoke_m1492811023(__this, ___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, ___callback6, ___object7, method) ((  Il2CppObject * (*) (ResponseCallback_2_t3240762047 *, String_t*, int32_t, Il2CppObject *, Il2CppObject *, PlayFabError_t750598646 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ResponseCallback_2_BeginInvoke_m1492811023_gshared)(__this, ___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, ___callback6, ___object7, method)
// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ResponseCallback_2_EndInvoke_m1596759973_gshared (ResponseCallback_2_t3240762047 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ResponseCallback_2_EndInvoke_m1596759973(__this, ___result0, method) ((  void (*) (ResponseCallback_2_t3240762047 *, Il2CppObject *, const MethodInfo*))ResponseCallback_2_EndInvoke_m1596759973_gshared)(__this, ___result0, method)
