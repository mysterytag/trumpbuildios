﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UpdateUserStatisticsRequest
struct  UpdateUserStatisticsRequest_t3113316872  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.UpdateUserStatisticsRequest::<UserStatistics>k__BackingField
	Dictionary_2_t190145395 * ___U3CUserStatisticsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CUserStatisticsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UpdateUserStatisticsRequest_t3113316872, ___U3CUserStatisticsU3Ek__BackingField_0)); }
	inline Dictionary_2_t190145395 * get_U3CUserStatisticsU3Ek__BackingField_0() const { return ___U3CUserStatisticsU3Ek__BackingField_0; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CUserStatisticsU3Ek__BackingField_0() { return &___U3CUserStatisticsU3Ek__BackingField_0; }
	inline void set_U3CUserStatisticsU3Ek__BackingField_0(Dictionary_2_t190145395 * value)
	{
		___U3CUserStatisticsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserStatisticsU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
