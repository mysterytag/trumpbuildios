﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableEnableTrigger
struct ObservableEnableTrigger_t1753982163;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableEnableTrigger::.ctor()
extern "C"  void ObservableEnableTrigger__ctor_m3562749816 (ObservableEnableTrigger_t1753982163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEnableTrigger::OnEnable()
extern "C"  void ObservableEnableTrigger_OnEnable_m2719528206 (ObservableEnableTrigger_t1753982163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableEnableTrigger::OnEnableAsObservable()
extern "C"  Il2CppObject* ObservableEnableTrigger_OnEnableAsObservable_m2309933387 (ObservableEnableTrigger_t1753982163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEnableTrigger::OnDisable()
extern "C"  void ObservableEnableTrigger_OnDisable_m3141933023 (ObservableEnableTrigger_t1753982163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableEnableTrigger::OnDisableAsObservable()
extern "C"  Il2CppObject* ObservableEnableTrigger_OnDisableAsObservable_m4084070412 (ObservableEnableTrigger_t1753982163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEnableTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableEnableTrigger_RaiseOnCompletedOnDestroy_m3272452785 (ObservableEnableTrigger_t1753982163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
