﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t3999572776;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostWWW>c__AnonStorey8B
struct  U3CPostWWWU3Ec__AnonStorey8B_t4245247438  : public Il2CppObject
{
public:
	// System.String UniRx.ObservableWWW/<PostWWW>c__AnonStorey8B::url
	String_t* ___url_0;
	// UnityEngine.WWWForm UniRx.ObservableWWW/<PostWWW>c__AnonStorey8B::content
	WWWForm_t3999572776 * ___content_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostWWW>c__AnonStorey8B::progress
	Il2CppObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey8B_t4245247438, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey8B_t4245247438, ___content_1)); }
	inline WWWForm_t3999572776 * get_content_1() const { return ___content_1; }
	inline WWWForm_t3999572776 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(WWWForm_t3999572776 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier(&___content_1, value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey8B_t4245247438, ___progress_2)); }
	inline Il2CppObject* get_progress_2() const { return ___progress_2; }
	inline Il2CppObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(Il2CppObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier(&___progress_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
