﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserDataRecord
struct UserDataRecord_t163839734;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"

// System.Void PlayFab.ClientModels.UserDataRecord::.ctor()
extern "C"  void UserDataRecord__ctor_m2056177799 (UserDataRecord_t163839734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserDataRecord::get_Value()
extern "C"  String_t* UserDataRecord_get_Value_m4165692688 (UserDataRecord_t163839734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserDataRecord::set_Value(System.String)
extern "C"  void UserDataRecord_set_Value_m1557190057 (UserDataRecord_t163839734 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayFab.ClientModels.UserDataRecord::get_LastUpdated()
extern "C"  DateTime_t339033936  UserDataRecord_get_LastUpdated_m3083927450 (UserDataRecord_t163839734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserDataRecord::set_LastUpdated(System.DateTime)
extern "C"  void UserDataRecord_set_LastUpdated_m846232331 (UserDataRecord_t163839734 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.UserDataPermission> PlayFab.ClientModels.UserDataRecord::get_Permission()
extern "C"  Nullable_1_t2611574664  UserDataRecord_get_Permission_m2143047935 (UserDataRecord_t163839734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserDataRecord::set_Permission(System.Nullable`1<PlayFab.ClientModels.UserDataPermission>)
extern "C"  void UserDataRecord_set_Permission_m4129027106 (UserDataRecord_t163839734 * __this, Nullable_1_t2611574664  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
