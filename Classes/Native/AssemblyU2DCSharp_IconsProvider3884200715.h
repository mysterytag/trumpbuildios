﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t503173063;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconsProvider
struct  IconsProvider_t3884200715  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Sprite[] IconsProvider::Sprites
	SpriteU5BU5D_t503173063* ___Sprites_2;

public:
	inline static int32_t get_offset_of_Sprites_2() { return static_cast<int32_t>(offsetof(IconsProvider_t3884200715, ___Sprites_2)); }
	inline SpriteU5BU5D_t503173063* get_Sprites_2() const { return ___Sprites_2; }
	inline SpriteU5BU5D_t503173063** get_address_of_Sprites_2() { return &___Sprites_2; }
	inline void set_Sprites_2(SpriteU5BU5D_t503173063* value)
	{
		___Sprites_2 = value;
		Il2CppCodeGenWriteBarrier(&___Sprites_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
