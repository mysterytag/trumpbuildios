﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PlayFab.ClientModels.CreateSharedGroupRequest
struct CreateSharedGroupRequest_t972582497;
// PlayFab.ClientModels.CreateSharedGroupResult
struct CreateSharedGroupResult_t2976015403;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "mscorlib_System_MulticastDelegate2585444626.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Int322847414787.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.PlayFabClientAPI/CreateSharedGroupResponseCallback
struct  CreateSharedGroupResponseCallback_t1136325156  : public MulticastDelegate_t2585444626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
