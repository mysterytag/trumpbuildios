﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UnlockContainerInstanceRequest
struct  UnlockContainerInstanceRequest_t1476418829  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::<CharacterId>k__BackingField
	String_t* ___U3CCharacterIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::<ContainerItemInstanceId>k__BackingField
	String_t* ___U3CContainerItemInstanceIdU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::<KeyItemInstanceId>k__BackingField
	String_t* ___U3CKeyItemInstanceIdU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::<CatalogVersion>k__BackingField
	String_t* ___U3CCatalogVersionU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCharacterIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnlockContainerInstanceRequest_t1476418829, ___U3CCharacterIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CCharacterIdU3Ek__BackingField_0() const { return ___U3CCharacterIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCharacterIdU3Ek__BackingField_0() { return &___U3CCharacterIdU3Ek__BackingField_0; }
	inline void set_U3CCharacterIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CCharacterIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CContainerItemInstanceIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnlockContainerInstanceRequest_t1476418829, ___U3CContainerItemInstanceIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CContainerItemInstanceIdU3Ek__BackingField_1() const { return ___U3CContainerItemInstanceIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CContainerItemInstanceIdU3Ek__BackingField_1() { return &___U3CContainerItemInstanceIdU3Ek__BackingField_1; }
	inline void set_U3CContainerItemInstanceIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CContainerItemInstanceIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContainerItemInstanceIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CKeyItemInstanceIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnlockContainerInstanceRequest_t1476418829, ___U3CKeyItemInstanceIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CKeyItemInstanceIdU3Ek__BackingField_2() const { return ___U3CKeyItemInstanceIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CKeyItemInstanceIdU3Ek__BackingField_2() { return &___U3CKeyItemInstanceIdU3Ek__BackingField_2; }
	inline void set_U3CKeyItemInstanceIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CKeyItemInstanceIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeyItemInstanceIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCatalogVersionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnlockContainerInstanceRequest_t1476418829, ___U3CCatalogVersionU3Ek__BackingField_3)); }
	inline String_t* get_U3CCatalogVersionU3Ek__BackingField_3() const { return ___U3CCatalogVersionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CCatalogVersionU3Ek__BackingField_3() { return &___U3CCatalogVersionU3Ek__BackingField_3; }
	inline void set_U3CCatalogVersionU3Ek__BackingField_3(String_t* value)
	{
		___U3CCatalogVersionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCatalogVersionU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
