﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AchievementDescription
struct AchievementDescription_t2323334509;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// WindowManager
struct WindowManager_t3316821373;
// AchievementWindow
struct AchievementWindow_t4147984351;
// System.Object
struct Il2CppObject;
// AchievementController
struct AchievementController_t3677499403;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AchievementController/<Report>c__Iterator9
struct  U3CReportU3Ec__Iterator9_t3537486326  : public Il2CppObject
{
public:
	// System.String AchievementController/<Report>c__Iterator9::id
	String_t* ___id_0;
	// AchievementDescription AchievementController/<Report>c__Iterator9::<a>__0
	AchievementDescription_t2323334509 * ___U3CaU3E__0_1;
	// System.Action`1<System.Boolean> AchievementController/<Report>c__Iterator9::<callback>__1
	Action_1_t359458046 * ___U3CcallbackU3E__1_2;
	// WindowManager AchievementController/<Report>c__Iterator9::<manager>__2
	WindowManager_t3316821373 * ___U3CmanagerU3E__2_3;
	// AchievementWindow AchievementController/<Report>c__Iterator9::<window>__3
	AchievementWindow_t4147984351 * ___U3CwindowU3E__3_4;
	// System.Int32 AchievementController/<Report>c__Iterator9::$PC
	int32_t ___U24PC_5;
	// System.Object AchievementController/<Report>c__Iterator9::$current
	Il2CppObject * ___U24current_6;
	// System.String AchievementController/<Report>c__Iterator9::<$>id
	String_t* ___U3CU24U3Eid_7;
	// AchievementController AchievementController/<Report>c__Iterator9::<>f__this
	AchievementController_t3677499403 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_U3CaU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___U3CaU3E__0_1)); }
	inline AchievementDescription_t2323334509 * get_U3CaU3E__0_1() const { return ___U3CaU3E__0_1; }
	inline AchievementDescription_t2323334509 ** get_address_of_U3CaU3E__0_1() { return &___U3CaU3E__0_1; }
	inline void set_U3CaU3E__0_1(AchievementDescription_t2323334509 * value)
	{
		___U3CaU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CaU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CcallbackU3E__1_2() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___U3CcallbackU3E__1_2)); }
	inline Action_1_t359458046 * get_U3CcallbackU3E__1_2() const { return ___U3CcallbackU3E__1_2; }
	inline Action_1_t359458046 ** get_address_of_U3CcallbackU3E__1_2() { return &___U3CcallbackU3E__1_2; }
	inline void set_U3CcallbackU3E__1_2(Action_1_t359458046 * value)
	{
		___U3CcallbackU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcallbackU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CmanagerU3E__2_3() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___U3CmanagerU3E__2_3)); }
	inline WindowManager_t3316821373 * get_U3CmanagerU3E__2_3() const { return ___U3CmanagerU3E__2_3; }
	inline WindowManager_t3316821373 ** get_address_of_U3CmanagerU3E__2_3() { return &___U3CmanagerU3E__2_3; }
	inline void set_U3CmanagerU3E__2_3(WindowManager_t3316821373 * value)
	{
		___U3CmanagerU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmanagerU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CwindowU3E__3_4() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___U3CwindowU3E__3_4)); }
	inline AchievementWindow_t4147984351 * get_U3CwindowU3E__3_4() const { return ___U3CwindowU3E__3_4; }
	inline AchievementWindow_t4147984351 ** get_address_of_U3CwindowU3E__3_4() { return &___U3CwindowU3E__3_4; }
	inline void set_U3CwindowU3E__3_4(AchievementWindow_t4147984351 * value)
	{
		___U3CwindowU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwindowU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eid_7() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___U3CU24U3Eid_7)); }
	inline String_t* get_U3CU24U3Eid_7() const { return ___U3CU24U3Eid_7; }
	inline String_t** get_address_of_U3CU24U3Eid_7() { return &___U3CU24U3Eid_7; }
	inline void set_U3CU24U3Eid_7(String_t* value)
	{
		___U3CU24U3Eid_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eid_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__Iterator9_t3537486326, ___U3CU3Ef__this_8)); }
	inline AchievementController_t3677499403 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline AchievementController_t3677499403 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(AchievementController_t3677499403 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
