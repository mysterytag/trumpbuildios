﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.KongregatePlayFabIdPair>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1086019586(__this, ___l0, method) ((  void (*) (Enumerator_t1867923526 *, List_1_t3782140534 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.KongregatePlayFabIdPair>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2392408848(__this, method) ((  void (*) (Enumerator_t1867923526 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.KongregatePlayFabIdPair>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m461140102(__this, method) ((  Il2CppObject * (*) (Enumerator_t1867923526 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.KongregatePlayFabIdPair>::Dispose()
#define Enumerator_Dispose_m1331988071(__this, method) ((  void (*) (Enumerator_t1867923526 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.KongregatePlayFabIdPair>::VerifyState()
#define Enumerator_VerifyState_m3432036896(__this, method) ((  void (*) (Enumerator_t1867923526 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.KongregatePlayFabIdPair>::MoveNext()
#define Enumerator_MoveNext_m2403950016(__this, method) ((  bool (*) (Enumerator_t1867923526 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.KongregatePlayFabIdPair>::get_Current()
#define Enumerator_get_Current_m2061410873(__this, method) ((  KongregatePlayFabIdPair_t2985181565 * (*) (Enumerator_t1867923526 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
