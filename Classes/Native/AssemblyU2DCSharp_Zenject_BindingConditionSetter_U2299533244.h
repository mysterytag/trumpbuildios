﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type[]
struct TypeU5BU5D_t3431720054;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171
struct  U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244  : public Il2CppObject
{
public:
	// System.Type[] Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171::targets
	TypeU5BU5D_t3431720054* ___targets_0;

public:
	inline static int32_t get_offset_of_targets_0() { return static_cast<int32_t>(offsetof(U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244, ___targets_0)); }
	inline TypeU5BU5D_t3431720054* get_targets_0() const { return ___targets_0; }
	inline TypeU5BU5D_t3431720054** get_address_of_targets_0() { return &___targets_0; }
	inline void set_targets_0(TypeU5BU5D_t3431720054* value)
	{
		___targets_0 = value;
		Il2CppCodeGenWriteBarrier(&___targets_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
