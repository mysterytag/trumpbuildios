﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>
struct ValueCollection_t4212802564;
// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct Dictionary_2_t2290665470;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Int32[]
struct Int32U5BU5D_t1809983122;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2057693411.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1726443476_gshared (ValueCollection_t4212802564 * __this, Dictionary_2_t2290665470 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1726443476(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4212802564 *, Dictionary_2_t2290665470 *, const MethodInfo*))ValueCollection__ctor_m1726443476_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3527377374_gshared (ValueCollection_t4212802564 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3527377374(__this, ___item0, method) ((  void (*) (ValueCollection_t4212802564 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3527377374_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3272412583_gshared (ValueCollection_t4212802564 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3272412583(__this, method) ((  void (*) (ValueCollection_t4212802564 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3272412583_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3400758988_gshared (ValueCollection_t4212802564 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3400758988(__this, ___item0, method) ((  bool (*) (ValueCollection_t4212802564 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3400758988_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m156488305_gshared (ValueCollection_t4212802564 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m156488305(__this, ___item0, method) ((  bool (*) (ValueCollection_t4212802564 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m156488305_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1693523047_gshared (ValueCollection_t4212802564 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1693523047(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4212802564 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1693523047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m558369259_gshared (ValueCollection_t4212802564 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m558369259(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4212802564 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m558369259_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3717570746_gshared (ValueCollection_t4212802564 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3717570746(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4212802564 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3717570746_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1675934847_gshared (ValueCollection_t4212802564 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1675934847(__this, method) ((  bool (*) (ValueCollection_t4212802564 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1675934847_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2117765343_gshared (ValueCollection_t4212802564 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2117765343(__this, method) ((  bool (*) (ValueCollection_t4212802564 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2117765343_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3635331985_gshared (ValueCollection_t4212802564 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3635331985(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4212802564 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3635331985_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3385450267_gshared (ValueCollection_t4212802564 * __this, Int32U5BU5D_t1809983122* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3385450267(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4212802564 *, Int32U5BU5D_t1809983122*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3385450267_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2057693413  ValueCollection_GetEnumerator_m3428120516_gshared (ValueCollection_t4212802564 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3428120516(__this, method) ((  Enumerator_t2057693413  (*) (ValueCollection_t4212802564 *, const MethodInfo*))ValueCollection_GetEnumerator_m3428120516_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m240710937_gshared (ValueCollection_t4212802564 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m240710937(__this, method) ((  int32_t (*) (ValueCollection_t4212802564 *, const MethodInfo*))ValueCollection_get_Count_m240710937_gshared)(__this, method)
