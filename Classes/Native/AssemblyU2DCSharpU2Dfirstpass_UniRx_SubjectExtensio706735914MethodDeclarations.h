﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.SubjectExtensions/AnonymousSubject`2<System.Object,System.Object>
struct AnonymousSubject_2_t706735914;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.SubjectExtensions/AnonymousSubject`2<System.Object,System.Object>::.ctor(UniRx.IObserver`1<T>,UniRx.IObservable`1<U>)
extern "C"  void AnonymousSubject_2__ctor_m2458638191_gshared (AnonymousSubject_2_t706735914 * __this, Il2CppObject* ___observer0, Il2CppObject* ___observable1, const MethodInfo* method);
#define AnonymousSubject_2__ctor_m2458638191(__this, ___observer0, ___observable1, method) ((  void (*) (AnonymousSubject_2_t706735914 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))AnonymousSubject_2__ctor_m2458638191_gshared)(__this, ___observer0, ___observable1, method)
// System.Void UniRx.SubjectExtensions/AnonymousSubject`2<System.Object,System.Object>::OnCompleted()
extern "C"  void AnonymousSubject_2_OnCompleted_m3186247023_gshared (AnonymousSubject_2_t706735914 * __this, const MethodInfo* method);
#define AnonymousSubject_2_OnCompleted_m3186247023(__this, method) ((  void (*) (AnonymousSubject_2_t706735914 *, const MethodInfo*))AnonymousSubject_2_OnCompleted_m3186247023_gshared)(__this, method)
// System.Void UniRx.SubjectExtensions/AnonymousSubject`2<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void AnonymousSubject_2_OnError_m119883804_gshared (AnonymousSubject_2_t706735914 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AnonymousSubject_2_OnError_m119883804(__this, ___error0, method) ((  void (*) (AnonymousSubject_2_t706735914 *, Exception_t1967233988 *, const MethodInfo*))AnonymousSubject_2_OnError_m119883804_gshared)(__this, ___error0, method)
// System.Void UniRx.SubjectExtensions/AnonymousSubject`2<System.Object,System.Object>::OnNext(T)
extern "C"  void AnonymousSubject_2_OnNext_m1475274509_gshared (AnonymousSubject_2_t706735914 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AnonymousSubject_2_OnNext_m1475274509(__this, ___value0, method) ((  void (*) (AnonymousSubject_2_t706735914 *, Il2CppObject *, const MethodInfo*))AnonymousSubject_2_OnNext_m1475274509_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.SubjectExtensions/AnonymousSubject`2<System.Object,System.Object>::Subscribe(UniRx.IObserver`1<U>)
extern "C"  Il2CppObject * AnonymousSubject_2_Subscribe_m1676104507_gshared (AnonymousSubject_2_t706735914 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define AnonymousSubject_2_Subscribe_m1676104507(__this, ___observer0, method) ((  Il2CppObject * (*) (AnonymousSubject_2_t706735914 *, Il2CppObject*, const MethodInfo*))AnonymousSubject_2_Subscribe_m1676104507_gshared)(__this, ___observer0, method)
