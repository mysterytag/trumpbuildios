﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>
struct DisposedObserver_1_t2682853279;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m607057855_gshared (DisposedObserver_1_t2682853279 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m607057855(__this, method) ((  void (*) (DisposedObserver_1_t2682853279 *, const MethodInfo*))DisposedObserver_1__ctor_m607057855_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1156828110_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m1156828110(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m1156828110_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3582664713_gshared (DisposedObserver_1_t2682853279 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3582664713(__this, method) ((  void (*) (DisposedObserver_1_t2682853279 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3582664713_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m3173059510_gshared (DisposedObserver_1_t2682853279 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m3173059510(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2682853279 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m3173059510_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<TableButtons/StatkaPoTableViev>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2097749671_gshared (DisposedObserver_1_t2682853279 * __this, int32_t ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2097749671(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2682853279 *, int32_t, const MethodInfo*))DisposedObserver_1_OnNext_m2097749671_gshared)(__this, ___value0, method)
