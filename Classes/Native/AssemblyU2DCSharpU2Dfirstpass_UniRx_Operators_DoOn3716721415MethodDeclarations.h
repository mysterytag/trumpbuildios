﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>
struct DoOnError_t3716721415;
// UniRx.Operators.DoOnErrorObservable`1<System.Object>
struct DoOnErrorObservable_1_t434875552;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::.ctor(UniRx.Operators.DoOnErrorObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnError__ctor_m3425299350_gshared (DoOnError_t3716721415 * __this, DoOnErrorObservable_1_t434875552 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DoOnError__ctor_m3425299350(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DoOnError_t3716721415 *, DoOnErrorObservable_1_t434875552 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnError__ctor_m3425299350_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::Run()
extern "C"  Il2CppObject * DoOnError_Run_m1586640683_gshared (DoOnError_t3716721415 * __this, const MethodInfo* method);
#define DoOnError_Run_m1586640683(__this, method) ((  Il2CppObject * (*) (DoOnError_t3716721415 *, const MethodInfo*))DoOnError_Run_m1586640683_gshared)(__this, method)
// System.Void UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::OnNext(T)
extern "C"  void DoOnError_OnNext_m288173445_gshared (DoOnError_t3716721415 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DoOnError_OnNext_m288173445(__this, ___value0, method) ((  void (*) (DoOnError_t3716721415 *, Il2CppObject *, const MethodInfo*))DoOnError_OnNext_m288173445_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::OnError(System.Exception)
extern "C"  void DoOnError_OnError_m3333922452_gshared (DoOnError_t3716721415 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DoOnError_OnError_m3333922452(__this, ___error0, method) ((  void (*) (DoOnError_t3716721415 *, Exception_t1967233988 *, const MethodInfo*))DoOnError_OnError_m3333922452_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>::OnCompleted()
extern "C"  void DoOnError_OnCompleted_m2301661159_gshared (DoOnError_t3716721415 * __this, const MethodInfo* method);
#define DoOnError_OnCompleted_m2301661159(__this, method) ((  void (*) (DoOnError_t3716721415 *, const MethodInfo*))DoOnError_OnCompleted_m2301661159_gshared)(__this, method)
