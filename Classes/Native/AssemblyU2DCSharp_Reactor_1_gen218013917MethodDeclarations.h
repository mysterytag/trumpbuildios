﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<System.Single>
struct Reactor_1_t218013917;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Single>
struct Action_1_t1106661726;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<System.Single>::.ctor()
extern "C"  void Reactor_1__ctor_m819876443_gshared (Reactor_1_t218013917 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m819876443(__this, method) ((  void (*) (Reactor_1_t218013917 *, const MethodInfo*))Reactor_1__ctor_m819876443_gshared)(__this, method)
// System.Void Reactor`1<System.Single>::React(T)
extern "C"  void Reactor_1_React_m3478028070_gshared (Reactor_1_t218013917 * __this, float ___t0, const MethodInfo* method);
#define Reactor_1_React_m3478028070(__this, ___t0, method) ((  void (*) (Reactor_1_t218013917 *, float, const MethodInfo*))Reactor_1_React_m3478028070_gshared)(__this, ___t0, method)
// System.Void Reactor`1<System.Single>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m3032148657_gshared (Reactor_1_t218013917 * __this, float ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m3032148657(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t218013917 *, float, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m3032148657_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<System.Single>::Empty()
extern "C"  bool Reactor_1_Empty_m4085372242_gshared (Reactor_1_t218013917 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m4085372242(__this, method) ((  bool (*) (Reactor_1_t218013917 *, const MethodInfo*))Reactor_1_Empty_m4085372242_gshared)(__this, method)
// System.IDisposable Reactor`1<System.Single>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m947706194_gshared (Reactor_1_t218013917 * __this, Action_1_t1106661726 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m947706194(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t218013917 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m947706194_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<System.Single>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m2477179295_gshared (Reactor_1_t218013917 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m2477179295(__this, method) ((  void (*) (Reactor_1_t218013917 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m2477179295_gshared)(__this, method)
// System.Void Reactor`1<System.Single>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m436990648_gshared (Reactor_1_t218013917 * __this, Action_1_t1106661726 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m436990648(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t218013917 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m436990648_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Single>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m2010589100_gshared (Reactor_1_t218013917 * __this, Action_1_t1106661726 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m2010589100(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t218013917 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m2010589100_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Single>::Clear()
extern "C"  void Reactor_1_Clear_m2520977030_gshared (Reactor_1_t218013917 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m2520977030(__this, method) ((  void (*) (Reactor_1_t218013917 *, const MethodInfo*))Reactor_1_Clear_m2520977030_gshared)(__this, method)
