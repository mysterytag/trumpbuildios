﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.PlayFabApiTest
struct PlayFabApiTest_t2536778019;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.ClientModels.RegisterPlayFabUserResult
struct RegisterPlayFabUserResult_t109811432;
// PlayFab.ClientModels.GetUserDataResult
struct GetUserDataResult_t518036344;
// PlayFab.ClientModels.UpdateUserDataResult
struct UpdateUserDataResult_t4027921387;
// PlayFab.ClientModels.GetUserStatisticsResult
struct GetUserStatisticsResult_t2139593073;
// PlayFab.ClientModels.UpdateUserStatisticsResult
struct UpdateUserStatisticsResult_t274124708;
// PlayFab.ClientModels.ListUsersCharactersResult
struct ListUsersCharactersResult_t1961583425;
// PlayFab.ClientModels.GetLeaderboardResult
struct GetLeaderboardResult_t2566099316;
// PlayFab.ClientModels.GetAccountInfoResult
struct GetAccountInfoResult_t3022616562;
// PlayFab.ClientModels.GetCloudScriptUrlResult
struct GetCloudScriptUrlResult_t3525973842;
// PlayFab.ClientModels.RunCloudScriptResult
struct RunCloudScriptResult_t3227572354;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterPlay109811432.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserDataR518036344.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserD4027921387.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserStat2139593073.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserSt274124708.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ListUsersCh1961583425.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo2566099316.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetAccountI3022616562.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCloudScr3525973842.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RunCloudScr3227572354.h"

// System.Void PlayFab.UUnit.PlayFabApiTest::.ctor()
extern "C"  void PlayFabApiTest__ctor_m1689720434 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::.cctor()
extern "C"  void PlayFabApiTest__cctor_m359629691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::SetTitleInfo(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void PlayFabApiTest_SetTitleInfo_m3585007527 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___testInputs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::SetUp()
extern "C"  void PlayFabApiTest_SetUp_m224100077 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::TearDown()
extern "C"  void PlayFabApiTest_TearDown_m1612165430 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::WaitForApiCalls()
extern "C"  void PlayFabApiTest_WaitForApiCalls_m1928861727 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::SharedErrorCallback(PlayFab.PlayFabError)
extern "C"  void PlayFabApiTest_SharedErrorCallback_m1908738088 (PlayFabApiTest_t2536778019 * __this, PlayFabError_t750598646 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::InvalidLogin()
extern "C"  void PlayFabApiTest_InvalidLogin_m480144068 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::LoginCallback(PlayFab.ClientModels.LoginResult)
extern "C"  void PlayFabApiTest_LoginCallback_m4028811222 (PlayFabApiTest_t2536778019 * __this, LoginResult_t2117559510 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::LoginOrRegister()
extern "C"  void PlayFabApiTest_LoginOrRegister_m992640767 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::RegisterCallback(PlayFab.ClientModels.RegisterPlayFabUserResult)
extern "C"  void PlayFabApiTest_RegisterCallback_m1740360448 (PlayFabApiTest_t2536778019 * __this, RegisterPlayFabUserResult_t109811432 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::LoginWithAdvertisingId()
extern "C"  void PlayFabApiTest_LoginWithAdvertisingId_m524349986 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::UserDataApi()
extern "C"  void PlayFabApiTest_UserDataApi_m2972979925 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::GetUserDataCallback(PlayFab.ClientModels.GetUserDataResult)
extern "C"  void PlayFabApiTest_GetUserDataCallback_m3779661846 (PlayFabApiTest_t2536778019 * __this, GetUserDataResult_t518036344 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::UpdateUserDataCallback(PlayFab.ClientModels.UpdateUserDataResult)
extern "C"  void PlayFabApiTest_UpdateUserDataCallback_m331667748 (PlayFabApiTest_t2536778019 * __this, UpdateUserDataResult_t4027921387 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::UserStatisticsApi()
extern "C"  void PlayFabApiTest_UserStatisticsApi_m3080286716 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::GetUserStatsCallback(PlayFab.ClientModels.GetUserStatisticsResult)
extern "C"  void PlayFabApiTest_GetUserStatsCallback_m4242620626 (PlayFabApiTest_t2536778019 * __this, GetUserStatisticsResult_t2139593073 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::UpdateUserStatsCallback(PlayFab.ClientModels.UpdateUserStatisticsResult)
extern "C"  void PlayFabApiTest_UpdateUserStatsCallback_m4063414944 (PlayFabApiTest_t2536778019 * __this, UpdateUserStatisticsResult_t274124708 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::UserCharacter()
extern "C"  void PlayFabApiTest_UserCharacter_m3116896430 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::GetCharsCallback(PlayFab.ClientModels.ListUsersCharactersResult)
extern "C"  void PlayFabApiTest_GetCharsCallback_m4275469643 (PlayFabApiTest_t2536778019 * __this, ListUsersCharactersResult_t1961583425 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::LeaderBoard()
extern "C"  void PlayFabApiTest_LeaderBoard_m4206962797 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::GetClientLbCallback(PlayFab.ClientModels.GetLeaderboardResult)
extern "C"  void PlayFabApiTest_GetClientLbCallback_m1327104196 (PlayFabApiTest_t2536778019 * __this, GetLeaderboardResult_t2566099316 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::AccountInfo()
extern "C"  void PlayFabApiTest_AccountInfo_m239166987 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::AcctInfoCallback(PlayFab.ClientModels.GetAccountInfoResult)
extern "C"  void PlayFabApiTest_AcctInfoCallback_m1838446330 (PlayFabApiTest_t2536778019 * __this, GetAccountInfoResult_t3022616562 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::CloudScript()
extern "C"  void PlayFabApiTest_CloudScript_m3658947504 (PlayFabApiTest_t2536778019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::CloudScriptUrlCallback(PlayFab.ClientModels.GetCloudScriptUrlResult)
extern "C"  void PlayFabApiTest_CloudScriptUrlCallback_m869259554 (PlayFabApiTest_t2536778019 * __this, GetCloudScriptUrlResult_t3525973842 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.PlayFabApiTest::CloudScriptHwCallback(PlayFab.ClientModels.RunCloudScriptResult)
extern "C"  void PlayFabApiTest_CloudScriptHwCallback_m2702900158 (PlayFabApiTest_t2536778019 * __this, RunCloudScriptResult_t3227572354 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
