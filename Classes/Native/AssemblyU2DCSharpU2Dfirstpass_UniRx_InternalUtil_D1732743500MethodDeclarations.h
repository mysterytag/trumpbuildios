﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>
struct DisposedObserver_1_t1732743500;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3616962254_gshared (DisposedObserver_1_t1732743500 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m3616962254(__this, method) ((  void (*) (DisposedObserver_1_t1732743500 *, const MethodInfo*))DisposedObserver_1__ctor_m3616962254_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m4269551263_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m4269551263(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m4269551263_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m1888802776_gshared (DisposedObserver_1_t1732743500 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m1888802776(__this, method) ((  void (*) (DisposedObserver_1_t1732743500 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m1888802776_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m2554496261_gshared (DisposedObserver_1_t1732743500 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m2554496261(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1732743500 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m2554496261_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Unit>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m4102886902_gshared (DisposedObserver_1_t1732743500 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m4102886902(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1732743500 *, Unit_t2558286038 , const MethodInfo*))DisposedObserver_1_OnNext_m4102886902_gshared)(__this, ___value0, method)
