﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeoutFrameObservable`1<System.Object>
struct TimeoutFrameObservable_1_t2457127902;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

// System.Void UniRx.Operators.TimeoutFrameObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void TimeoutFrameObservable_1__ctor_m705515884_gshared (TimeoutFrameObservable_1_t2457127902 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method);
#define TimeoutFrameObservable_1__ctor_m705515884(__this, ___source0, ___frameCount1, ___frameCountType2, method) ((  void (*) (TimeoutFrameObservable_1_t2457127902 *, Il2CppObject*, int32_t, int32_t, const MethodInfo*))TimeoutFrameObservable_1__ctor_m705515884_gshared)(__this, ___source0, ___frameCount1, ___frameCountType2, method)
// System.IDisposable UniRx.Operators.TimeoutFrameObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TimeoutFrameObservable_1_SubscribeCore_m925296886_gshared (TimeoutFrameObservable_1_t2457127902 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TimeoutFrameObservable_1_SubscribeCore_m925296886(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TimeoutFrameObservable_1_t2457127902 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TimeoutFrameObservable_1_SubscribeCore_m925296886_gshared)(__this, ___observer0, ___cancel1, method)
