﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UnityEngine.Events.UnityAction
struct UnityAction_t909267611;
// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E
struct U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_t2584692431;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E/<DirtyLayoutCallbackAsObservable>c__AnonStorey9D
struct  U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_t2584692430  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<UniRx.Unit> UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E/<DirtyLayoutCallbackAsObservable>c__AnonStorey9D::observer
	Il2CppObject* ___observer_0;
	// UnityEngine.Events.UnityAction UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E/<DirtyLayoutCallbackAsObservable>c__AnonStorey9D::registerHandler
	UnityAction_t909267611 * ___registerHandler_1;
	// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E/<DirtyLayoutCallbackAsObservable>c__AnonStorey9D::<>f__ref$158
	U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_t2584692431 * ___U3CU3Ef__refU24158_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_t2584692430, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_registerHandler_1() { return static_cast<int32_t>(offsetof(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_t2584692430, ___registerHandler_1)); }
	inline UnityAction_t909267611 * get_registerHandler_1() const { return ___registerHandler_1; }
	inline UnityAction_t909267611 ** get_address_of_registerHandler_1() { return &___registerHandler_1; }
	inline void set_registerHandler_1(UnityAction_t909267611 * value)
	{
		___registerHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___registerHandler_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24158_2() { return static_cast<int32_t>(offsetof(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9D_t2584692430, ___U3CU3Ef__refU24158_2)); }
	inline U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_t2584692431 * get_U3CU3Ef__refU24158_2() const { return ___U3CU3Ef__refU24158_2; }
	inline U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_t2584692431 ** get_address_of_U3CU3Ef__refU24158_2() { return &___U3CU3Ef__refU24158_2; }
	inline void set_U3CU3Ef__refU24158_2(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_t2584692431 * value)
	{
		___U3CU3Ef__refU24158_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24158_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
