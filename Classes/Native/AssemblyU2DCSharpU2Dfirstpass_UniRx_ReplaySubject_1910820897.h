﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>
struct Queue_1_t2416153082;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReplaySubject`1<System.Object>
struct  ReplaySubject_1_t1910820897  : public Il2CppObject
{
public:
	// System.Object UniRx.ReplaySubject`1::observerLock
	Il2CppObject * ___observerLock_0;
	// System.Boolean UniRx.ReplaySubject`1::isStopped
	bool ___isStopped_1;
	// System.Boolean UniRx.ReplaySubject`1::isDisposed
	bool ___isDisposed_2;
	// System.Exception UniRx.ReplaySubject`1::lastError
	Exception_t1967233988 * ___lastError_3;
	// UniRx.IObserver`1<T> UniRx.ReplaySubject`1::outObserver
	Il2CppObject* ___outObserver_4;
	// System.Int32 UniRx.ReplaySubject`1::bufferSize
	int32_t ___bufferSize_5;
	// System.TimeSpan UniRx.ReplaySubject`1::window
	TimeSpan_t763862892  ___window_6;
	// System.DateTimeOffset UniRx.ReplaySubject`1::startTime
	DateTimeOffset_t3712260035  ___startTime_7;
	// UniRx.IScheduler UniRx.ReplaySubject`1::scheduler
	Il2CppObject * ___scheduler_8;
	// System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<T>> UniRx.ReplaySubject`1::queue
	Queue_1_t2416153082 * ___queue_9;

public:
	inline static int32_t get_offset_of_observerLock_0() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___observerLock_0)); }
	inline Il2CppObject * get_observerLock_0() const { return ___observerLock_0; }
	inline Il2CppObject ** get_address_of_observerLock_0() { return &___observerLock_0; }
	inline void set_observerLock_0(Il2CppObject * value)
	{
		___observerLock_0 = value;
		Il2CppCodeGenWriteBarrier(&___observerLock_0, value);
	}

	inline static int32_t get_offset_of_isStopped_1() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___isStopped_1)); }
	inline bool get_isStopped_1() const { return ___isStopped_1; }
	inline bool* get_address_of_isStopped_1() { return &___isStopped_1; }
	inline void set_isStopped_1(bool value)
	{
		___isStopped_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_lastError_3() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___lastError_3)); }
	inline Exception_t1967233988 * get_lastError_3() const { return ___lastError_3; }
	inline Exception_t1967233988 ** get_address_of_lastError_3() { return &___lastError_3; }
	inline void set_lastError_3(Exception_t1967233988 * value)
	{
		___lastError_3 = value;
		Il2CppCodeGenWriteBarrier(&___lastError_3, value);
	}

	inline static int32_t get_offset_of_outObserver_4() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___outObserver_4)); }
	inline Il2CppObject* get_outObserver_4() const { return ___outObserver_4; }
	inline Il2CppObject** get_address_of_outObserver_4() { return &___outObserver_4; }
	inline void set_outObserver_4(Il2CppObject* value)
	{
		___outObserver_4 = value;
		Il2CppCodeGenWriteBarrier(&___outObserver_4, value);
	}

	inline static int32_t get_offset_of_bufferSize_5() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___bufferSize_5)); }
	inline int32_t get_bufferSize_5() const { return ___bufferSize_5; }
	inline int32_t* get_address_of_bufferSize_5() { return &___bufferSize_5; }
	inline void set_bufferSize_5(int32_t value)
	{
		___bufferSize_5 = value;
	}

	inline static int32_t get_offset_of_window_6() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___window_6)); }
	inline TimeSpan_t763862892  get_window_6() const { return ___window_6; }
	inline TimeSpan_t763862892 * get_address_of_window_6() { return &___window_6; }
	inline void set_window_6(TimeSpan_t763862892  value)
	{
		___window_6 = value;
	}

	inline static int32_t get_offset_of_startTime_7() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___startTime_7)); }
	inline DateTimeOffset_t3712260035  get_startTime_7() const { return ___startTime_7; }
	inline DateTimeOffset_t3712260035 * get_address_of_startTime_7() { return &___startTime_7; }
	inline void set_startTime_7(DateTimeOffset_t3712260035  value)
	{
		___startTime_7 = value;
	}

	inline static int32_t get_offset_of_scheduler_8() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___scheduler_8)); }
	inline Il2CppObject * get_scheduler_8() const { return ___scheduler_8; }
	inline Il2CppObject ** get_address_of_scheduler_8() { return &___scheduler_8; }
	inline void set_scheduler_8(Il2CppObject * value)
	{
		___scheduler_8 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_8, value);
	}

	inline static int32_t get_offset_of_queue_9() { return static_cast<int32_t>(offsetof(ReplaySubject_1_t1910820897, ___queue_9)); }
	inline Queue_1_t2416153082 * get_queue_9() const { return ___queue_9; }
	inline Queue_1_t2416153082 ** get_address_of_queue_9() { return &___queue_9; }
	inline void set_queue_9(Queue_1_t2416153082 * value)
	{
		___queue_9 = value;
		Il2CppCodeGenWriteBarrier(&___queue_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
