﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey59
struct U3CGetConstructorByReflectionU3Ec__AnonStorey59_t3607371919;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey59::.ctor()
extern "C"  void U3CGetConstructorByReflectionU3Ec__AnonStorey59__ctor_m3075447040 (U3CGetConstructorByReflectionU3Ec__AnonStorey59_t3607371919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey59::<>m__45(System.Object[])
extern "C"  Il2CppObject * U3CGetConstructorByReflectionU3Ec__AnonStorey59_U3CU3Em__45_m1703203795 (U3CGetConstructorByReflectionU3Ec__AnonStorey59_t3607371919 * __this, ObjectU5BU5D_t11523773* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
