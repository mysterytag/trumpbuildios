﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Elastic
struct Elastic_t4293282485;
// FiniteTimeProc
struct FiniteTimeProc_t2933053874;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FiniteTimeProc2933053874.h"

// System.Void Elastic::.ctor(FiniteTimeProc)
extern "C"  void Elastic__ctor_m4073007652 (Elastic_t4293282485 * __this, FiniteTimeProc_t2933053874 * ___inProc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Elastic::Update(System.Single)
extern "C"  void Elastic_Update_m912792980 (Elastic_t4293282485 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
