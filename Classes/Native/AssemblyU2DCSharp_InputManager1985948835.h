﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InputManager
struct InputManager_t1985948835;
// System.Collections.Generic.List`1<System.Action`1<InputEventDescriptor>>
struct List_1_t914409209;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputManager
struct  InputManager_t1985948835  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean InputManager::touchInput
	bool ___touchInput_3;
	// System.Collections.Generic.List`1<System.Action`1<InputEventDescriptor>> InputManager::inputEvent
	List_1_t914409209 * ___inputEvent_5;
	// System.Boolean InputManager::mousePressed
	bool ___mousePressed_6;
	// UnityEngine.Vector3 InputManager::lastInputPosition
	Vector3_t3525329789  ___lastInputPosition_7;
	// System.Boolean InputManager::inputStarted
	bool ___inputStarted_9;

public:
	inline static int32_t get_offset_of_touchInput_3() { return static_cast<int32_t>(offsetof(InputManager_t1985948835, ___touchInput_3)); }
	inline bool get_touchInput_3() const { return ___touchInput_3; }
	inline bool* get_address_of_touchInput_3() { return &___touchInput_3; }
	inline void set_touchInput_3(bool value)
	{
		___touchInput_3 = value;
	}

	inline static int32_t get_offset_of_inputEvent_5() { return static_cast<int32_t>(offsetof(InputManager_t1985948835, ___inputEvent_5)); }
	inline List_1_t914409209 * get_inputEvent_5() const { return ___inputEvent_5; }
	inline List_1_t914409209 ** get_address_of_inputEvent_5() { return &___inputEvent_5; }
	inline void set_inputEvent_5(List_1_t914409209 * value)
	{
		___inputEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___inputEvent_5, value);
	}

	inline static int32_t get_offset_of_mousePressed_6() { return static_cast<int32_t>(offsetof(InputManager_t1985948835, ___mousePressed_6)); }
	inline bool get_mousePressed_6() const { return ___mousePressed_6; }
	inline bool* get_address_of_mousePressed_6() { return &___mousePressed_6; }
	inline void set_mousePressed_6(bool value)
	{
		___mousePressed_6 = value;
	}

	inline static int32_t get_offset_of_lastInputPosition_7() { return static_cast<int32_t>(offsetof(InputManager_t1985948835, ___lastInputPosition_7)); }
	inline Vector3_t3525329789  get_lastInputPosition_7() const { return ___lastInputPosition_7; }
	inline Vector3_t3525329789 * get_address_of_lastInputPosition_7() { return &___lastInputPosition_7; }
	inline void set_lastInputPosition_7(Vector3_t3525329789  value)
	{
		___lastInputPosition_7 = value;
	}

	inline static int32_t get_offset_of_inputStarted_9() { return static_cast<int32_t>(offsetof(InputManager_t1985948835, ___inputStarted_9)); }
	inline bool get_inputStarted_9() const { return ___inputStarted_9; }
	inline bool* get_address_of_inputStarted_9() { return &___inputStarted_9; }
	inline void set_inputStarted_9(bool value)
	{
		___inputStarted_9 = value;
	}
};

struct InputManager_t1985948835_StaticFields
{
public:
	// System.Boolean InputManager::disable
	bool ___disable_2;
	// InputManager InputManager::instance_
	InputManager_t1985948835 * ___instance__4;
	// System.Boolean InputManager::destroyed
	bool ___destroyed_8;

public:
	inline static int32_t get_offset_of_disable_2() { return static_cast<int32_t>(offsetof(InputManager_t1985948835_StaticFields, ___disable_2)); }
	inline bool get_disable_2() const { return ___disable_2; }
	inline bool* get_address_of_disable_2() { return &___disable_2; }
	inline void set_disable_2(bool value)
	{
		___disable_2 = value;
	}

	inline static int32_t get_offset_of_instance__4() { return static_cast<int32_t>(offsetof(InputManager_t1985948835_StaticFields, ___instance__4)); }
	inline InputManager_t1985948835 * get_instance__4() const { return ___instance__4; }
	inline InputManager_t1985948835 ** get_address_of_instance__4() { return &___instance__4; }
	inline void set_instance__4(InputManager_t1985948835 * value)
	{
		___instance__4 = value;
		Il2CppCodeGenWriteBarrier(&___instance__4, value);
	}

	inline static int32_t get_offset_of_destroyed_8() { return static_cast<int32_t>(offsetof(InputManager_t1985948835_StaticFields, ___destroyed_8)); }
	inline bool get_destroyed_8() const { return ___destroyed_8; }
	inline bool* get_address_of_destroyed_8() { return &___destroyed_8; }
	inline void set_destroyed_8(bool value)
	{
		___destroyed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
