﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_7_t2859822983;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_7__ctor_m1822985581_gshared (Factory_7_t2859822983 * __this, const MethodInfo* method);
#define Factory_7__ctor_m1822985581(__this, method) ((  void (*) (Factory_7_t2859822983 *, const MethodInfo*))Factory_7__ctor_m1822985581_gshared)(__this, method)
// System.Type Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_7_get_ConstructedType_m3956119576_gshared (Factory_7_t2859822983 * __this, const MethodInfo* method);
#define Factory_7_get_ConstructedType_m3956119576(__this, method) ((  Type_t * (*) (Factory_7_t2859822983 *, const MethodInfo*))Factory_7_get_ConstructedType_m3956119576_gshared)(__this, method)
// System.Type[] Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_7_get_ProvidedTypes_m2767697664_gshared (Factory_7_t2859822983 * __this, const MethodInfo* method);
#define Factory_7_get_ProvidedTypes_m2767697664(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_7_t2859822983 *, const MethodInfo*))Factory_7_get_ProvidedTypes_m2767697664_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_7_get_Container_m2005594317_gshared (Factory_7_t2859822983 * __this, const MethodInfo* method);
#define Factory_7_get_Container_m2005594317(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_7_t2859822983 *, const MethodInfo*))Factory_7_get_Container_m2005594317_gshared)(__this, method)
// TValue Zenject.Factory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6)
extern "C"  Il2CppObject * Factory_7_Create_m2387911836_gshared (Factory_7_t2859822983 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, const MethodInfo* method);
#define Factory_7_Create_m2387911836(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, method) ((  Il2CppObject * (*) (Factory_7_t2859822983 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_7_Create_m2387911836_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, method)
