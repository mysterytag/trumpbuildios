﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<Map>c__AnonStorey130`2<System.Object,System.Object>
struct U3CMapU3Ec__AnonStorey130_2_t2728082812;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void StreamAPI/<Map>c__AnonStorey130`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey130_2__ctor_m1735843443_gshared (U3CMapU3Ec__AnonStorey130_2_t2728082812 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey130_2__ctor_m1735843443(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey130_2_t2728082812 *, const MethodInfo*))U3CMapU3Ec__AnonStorey130_2__ctor_m1735843443_gshared)(__this, method)
// System.IDisposable StreamAPI/<Map>c__AnonStorey130`2<System.Object,System.Object>::<>m__1C3(System.Action`1<T2>,Priority)
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey130_2_U3CU3Em__1C3_m2514345428_gshared (U3CMapU3Ec__AnonStorey130_2_t2728082812 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey130_2_U3CU3Em__1C3_m2514345428(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CMapU3Ec__AnonStorey130_2_t2728082812 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CMapU3Ec__AnonStorey130_2_U3CU3Em__1C3_m2514345428_gshared)(__this, ___reaction0, ___p1, method)
