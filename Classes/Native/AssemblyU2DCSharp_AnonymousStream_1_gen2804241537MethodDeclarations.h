﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnonymousStream_1_gen94244231MethodDeclarations.h"

// System.Void AnonymousStream`1<UnityEngine.EventSystems.BaseEventData>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>)
#define AnonymousStream_1__ctor_m52982127(__this, ___subscribe0, method) ((  void (*) (AnonymousStream_1_t2804241537 *, Func_3_t3600102922 *, const MethodInfo*))AnonymousStream_1__ctor_m1486310211_gshared)(__this, ___subscribe0, method)
// System.IDisposable AnonymousStream`1<UnityEngine.EventSystems.BaseEventData>::Listen(System.Action`1<T>,Priority)
#define AnonymousStream_1_Listen_m3362792571(__this, ___observer0, ___p1, method) ((  Il2CppObject * (*) (AnonymousStream_1_t2804241537 *, Action_1_t3695556431 *, int32_t, const MethodInfo*))AnonymousStream_1_Listen_m191877793_gshared)(__this, ___observer0, ___p1, method)
// System.IDisposable AnonymousStream`1<UnityEngine.EventSystems.BaseEventData>::Listen(System.Action,Priority)
#define AnonymousStream_1_Listen_m2468658124(__this, ___observer0, ___p1, method) ((  Il2CppObject * (*) (AnonymousStream_1_t2804241537 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousStream_1_Listen_m3450752486_gshared)(__this, ___observer0, ___p1, method)
