﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Func`4<Zenject.DiContainer,System.Object,System.Object,System.Object>
struct Func_4_t2229882165;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryMethod`3<System.Object,System.Object,System.Object>
struct  FactoryMethod_3_t1233407108  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.FactoryMethod`3::_container
	DiContainer_t2383114449 * ____container_0;
	// System.Func`4<Zenject.DiContainer,TParam1,TParam2,TValue> Zenject.FactoryMethod`3::_method
	Func_4_t2229882165 * ____method_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(FactoryMethod_3_t1233407108, ____container_0)); }
	inline DiContainer_t2383114449 * get__container_0() const { return ____container_0; }
	inline DiContainer_t2383114449 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t2383114449 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier(&____container_0, value);
	}

	inline static int32_t get_offset_of__method_1() { return static_cast<int32_t>(offsetof(FactoryMethod_3_t1233407108, ____method_1)); }
	inline Func_4_t2229882165 * get__method_1() const { return ____method_1; }
	inline Func_4_t2229882165 ** get_address_of__method_1() { return &____method_1; }
	inline void set__method_1(Func_4_t2229882165 * value)
	{
		____method_1 = value;
		Il2CppCodeGenWriteBarrier(&____method_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
