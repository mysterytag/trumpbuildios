﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>
struct U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator14_3__ctor_m2497248440_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3__ctor_m2497248440(__this, method) ((  void (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3__ctor_m2497248440_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1089251649_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1089251649(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1089251649_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerator_get_Current_m3433108792_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerator_get_Current_m3433108792(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerator_get_Current_m3433108792_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerable_GetEnumerator_m3998682393_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerable_GetEnumerator_m3998682393(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerable_GetEnumerator_m3998682393_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1822530256_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1822530256(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1822530256_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::MoveNext()
extern "C"  bool U3CCreateSelectManyIteratorU3Ec__Iterator14_3_MoveNext_m4129660932_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_MoveNext_m4129660932(__this, method) ((  bool (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_MoveNext_m4129660932_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::Dispose()
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Dispose_m3162282741_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Dispose_m3162282741(__this, method) ((  void (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Dispose_m3162282741_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::Reset()
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Reset_m143681381_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Reset_m143681381(__this, method) ((  void (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t2465077431 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Reset_m143681381_gshared)(__this, method)
