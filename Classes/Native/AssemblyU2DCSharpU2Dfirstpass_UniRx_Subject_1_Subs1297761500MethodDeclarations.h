﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.Bounds>
struct Subscription_t1297761500;
// UniRx.Subject`1<UnityEngine.Bounds>
struct Subject_1_t1161582302;
// UniRx.IObserver`1<UnityEngine.Bounds>
struct IObserver_1_t1435546585;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.Bounds>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m440378791_gshared (Subscription_t1297761500 * __this, Subject_1_t1161582302 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m440378791(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t1297761500 *, Subject_1_t1161582302 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m440378791_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Bounds>::Dispose()
extern "C"  void Subscription_Dispose_m554366959_gshared (Subscription_t1297761500 * __this, const MethodInfo* method);
#define Subscription_Dispose_m554366959(__this, method) ((  void (*) (Subscription_t1297761500 *, const MethodInfo*))Subscription_Dispose_m554366959_gshared)(__this, method)
