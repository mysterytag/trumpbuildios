﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`1/ZipLatest/CombineLatestObserver<System.Object>
struct CombineLatestObserver_t301670550;
// UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>
struct ZipLatest_t2874059041;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest/CombineLatestObserver<System.Object>::.ctor(UniRx.Operators.ZipLatestObservable`1/ZipLatest<T>,System.Int32)
extern "C"  void CombineLatestObserver__ctor_m4281942305_gshared (CombineLatestObserver_t301670550 * __this, ZipLatest_t2874059041 * ___parent0, int32_t ___index1, const MethodInfo* method);
#define CombineLatestObserver__ctor_m4281942305(__this, ___parent0, ___index1, method) ((  void (*) (CombineLatestObserver_t301670550 *, ZipLatest_t2874059041 *, int32_t, const MethodInfo*))CombineLatestObserver__ctor_m4281942305_gshared)(__this, ___parent0, ___index1, method)
// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest/CombineLatestObserver<System.Object>::OnNext(T)
extern "C"  void CombineLatestObserver_OnNext_m4217154630_gshared (CombineLatestObserver_t301670550 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CombineLatestObserver_OnNext_m4217154630(__this, ___value0, method) ((  void (*) (CombineLatestObserver_t301670550 *, Il2CppObject *, const MethodInfo*))CombineLatestObserver_OnNext_m4217154630_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest/CombineLatestObserver<System.Object>::OnError(System.Exception)
extern "C"  void CombineLatestObserver_OnError_m3728440149_gshared (CombineLatestObserver_t301670550 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method);
#define CombineLatestObserver_OnError_m3728440149(__this, ___ex0, method) ((  void (*) (CombineLatestObserver_t301670550 *, Exception_t1967233988 *, const MethodInfo*))CombineLatestObserver_OnError_m3728440149_gshared)(__this, ___ex0, method)
// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest/CombineLatestObserver<System.Object>::OnCompleted()
extern "C"  void CombineLatestObserver_OnCompleted_m3188770344_gshared (CombineLatestObserver_t301670550 * __this, const MethodInfo* method);
#define CombineLatestObserver_OnCompleted_m3188770344(__this, method) ((  void (*) (CombineLatestObserver_t301670550 *, const MethodInfo*))CombineLatestObserver_OnCompleted_m3188770344_gshared)(__this, method)
