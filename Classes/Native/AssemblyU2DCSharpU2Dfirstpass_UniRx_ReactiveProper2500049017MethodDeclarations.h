﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<UnityEngine.Quaternion>
struct ReactiveProperty_1_t2500049017;
// UniRx.IObservable`1<UnityEngine.Quaternion>
struct IObservable_1_t1650514343;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion>
struct IEqualityComparer_1_t4215982630;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Quaternion>
struct IObserver_1_t4103714882;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m79599915_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m79599915(__this, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, const MethodInfo*))ReactiveProperty_1__ctor_m79599915_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m2467598739_gshared (ReactiveProperty_1_t2500049017 * __this, Quaternion_t1891715979  ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2467598739(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, Quaternion_t1891715979 , const MethodInfo*))ReactiveProperty_1__ctor_m2467598739_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m3449126090_gshared (ReactiveProperty_1_t2500049017 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3449126090(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m3449126090_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m3190392802_gshared (ReactiveProperty_1_t2500049017 * __this, Il2CppObject* ___source0, Quaternion_t1891715979  ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3190392802(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, Il2CppObject*, Quaternion_t1891715979 , const MethodInfo*))ReactiveProperty_1__ctor_m3190392802_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m1985501154_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m1985501154(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m1985501154_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m1655527528_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m1655527528(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t2500049017 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m1655527528_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::get_Value()
extern "C"  Quaternion_t1891715979  ReactiveProperty_1_get_Value_m3947833714_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m3947833714(__this, method) ((  Quaternion_t1891715979  (*) (ReactiveProperty_1_t2500049017 *, const MethodInfo*))ReactiveProperty_1_get_Value_m3947833714_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m2094249057_gshared (ReactiveProperty_1_t2500049017 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m2094249057(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, Quaternion_t1891715979 , const MethodInfo*))ReactiveProperty_1_set_Value_m2094249057_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m568885558_gshared (ReactiveProperty_1_t2500049017 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m568885558(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, Quaternion_t1891715979 , const MethodInfo*))ReactiveProperty_1_SetValue_m568885558_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m679489657_gshared (ReactiveProperty_1_t2500049017 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m679489657(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, Quaternion_t1891715979 , const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m679489657_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m228578026_gshared (ReactiveProperty_1_t2500049017 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m228578026(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t2500049017 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m228578026_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m3379357352_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m3379357352(__this, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, const MethodInfo*))ReactiveProperty_1_Dispose_m3379357352_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m4165391903_gshared (ReactiveProperty_1_t2500049017 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m4165391903(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t2500049017 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m4165391903_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m1564948168_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m1564948168(__this, method) ((  String_t* (*) (ReactiveProperty_1_t2500049017 *, const MethodInfo*))ReactiveProperty_1_ToString_m1564948168_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1735105336_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1735105336(__this, method) ((  bool (*) (ReactiveProperty_1_t2500049017 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1735105336_gshared)(__this, method)
