﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkSteamAccountRequest
struct LinkSteamAccountRequest_t3142953640;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LinkSteamAccountRequest::.ctor()
extern "C"  void LinkSteamAccountRequest__ctor_m1930957825 (LinkSteamAccountRequest_t3142953640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkSteamAccountRequest::get_SteamTicket()
extern "C"  String_t* LinkSteamAccountRequest_get_SteamTicket_m2742057327 (LinkSteamAccountRequest_t3142953640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkSteamAccountRequest::set_SteamTicket(System.String)
extern "C"  void LinkSteamAccountRequest_set_SteamTicket_m4261820356 (LinkSteamAccountRequest_t3142953640 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
