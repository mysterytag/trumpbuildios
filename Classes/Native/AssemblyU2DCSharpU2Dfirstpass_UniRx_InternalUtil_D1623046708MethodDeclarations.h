﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct DisposedObserver_1_t1623046708;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4024706262_gshared (DisposedObserver_1_t1623046708 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m4024706262(__this, method) ((  void (*) (DisposedObserver_1_t1623046708 *, const MethodInfo*))DisposedObserver_1__ctor_m4024706262_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m4024713623_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m4024713623(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m4024713623_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m2124013536_gshared (DisposedObserver_1_t1623046708 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m2124013536(__this, method) ((  void (*) (DisposedObserver_1_t1623046708 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m2124013536_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1393803533_gshared (DisposedObserver_1_t1623046708 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1393803533(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1623046708 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1393803533_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m807887358_gshared (DisposedObserver_1_t1623046708 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m807887358(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1623046708 *, CollectionMoveEvent_1_t2448589246 , const MethodInfo*))DisposedObserver_1_OnNext_m807887358_gshared)(__this, ___value0, method)
