﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils2/<FormatEach>c__AnonStoreyEF`1<System.Object>
struct U3CFormatEachU3Ec__AnonStoreyEF_1_t2861012304;
// System.Object
struct Il2CppObject;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils2/<FormatEach>c__AnonStoreyEF`1<System.Object>::.ctor()
extern "C"  void U3CFormatEachU3Ec__AnonStoreyEF_1__ctor_m3984047472_gshared (U3CFormatEachU3Ec__AnonStoreyEF_1_t2861012304 * __this, const MethodInfo* method);
#define U3CFormatEachU3Ec__AnonStoreyEF_1__ctor_m3984047472(__this, method) ((  void (*) (U3CFormatEachU3Ec__AnonStoreyEF_1_t2861012304 *, const MethodInfo*))U3CFormatEachU3Ec__AnonStoreyEF_1__ctor_m3984047472_gshared)(__this, method)
// System.Object Utils2/<FormatEach>c__AnonStoreyEF`1<System.Object>::<>m__155(System.Func`2<T,System.Object>)
extern "C"  Il2CppObject * U3CFormatEachU3Ec__AnonStoreyEF_1_U3CU3Em__155_m2580837254_gshared (U3CFormatEachU3Ec__AnonStoreyEF_1_t2861012304 * __this, Func_2_t2135783352 * ___func0, const MethodInfo* method);
#define U3CFormatEachU3Ec__AnonStoreyEF_1_U3CU3Em__155_m2580837254(__this, ___func0, method) ((  Il2CppObject * (*) (U3CFormatEachU3Ec__AnonStoreyEF_1_t2861012304 *, Func_2_t2135783352 *, const MethodInfo*))U3CFormatEachU3Ec__AnonStoreyEF_1_U3CU3Em__155_m2580837254_gshared)(__this, ___func0, method)
