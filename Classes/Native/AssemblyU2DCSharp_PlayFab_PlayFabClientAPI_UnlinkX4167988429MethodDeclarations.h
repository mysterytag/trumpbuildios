﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkXboxAccountRequestCallback
struct UnlinkXboxAccountRequestCallback_t4167988429;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkXboxAccountRequest
struct UnlinkXboxAccountRequest_t1448666232;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkXboxA1448666232.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkXboxAccountRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkXboxAccountRequestCallback__ctor_m706892604 (UnlinkXboxAccountRequestCallback_t4167988429 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkXboxAccountRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkXboxAccountRequest,System.Object)
extern "C"  void UnlinkXboxAccountRequestCallback_Invoke_m832935487 (UnlinkXboxAccountRequestCallback_t4167988429 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkXboxAccountRequest_t1448666232 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkXboxAccountRequestCallback_t4167988429(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkXboxAccountRequest_t1448666232 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkXboxAccountRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkXboxAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkXboxAccountRequestCallback_BeginInvoke_m4121467968 (UnlinkXboxAccountRequestCallback_t4167988429 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkXboxAccountRequest_t1448666232 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkXboxAccountRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkXboxAccountRequestCallback_EndInvoke_m339264332 (UnlinkXboxAccountRequestCallback_t4167988429 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
