﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe`1<System.Boolean>
struct Subscribe_1_t3319793125;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe`1<System.Boolean>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m1760256423_gshared (Subscribe_1_t3319793125 * __this, Action_1_t359458046 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define Subscribe_1__ctor_m1760256423(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (Subscribe_1_t3319793125 *, Action_1_t359458046 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe_1__ctor_m1760256423_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
// System.Void UniRx.Observer/Subscribe`1<System.Boolean>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m724910212_gshared (Subscribe_1_t3319793125 * __this, bool ___value0, const MethodInfo* method);
#define Subscribe_1_OnNext_m724910212(__this, ___value0, method) ((  void (*) (Subscribe_1_t3319793125 *, bool, const MethodInfo*))Subscribe_1_OnNext_m724910212_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Boolean>::OnError(System.Exception)
extern "C"  void Subscribe_1_OnError_m462410643_gshared (Subscribe_1_t3319793125 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe_1_OnError_m462410643(__this, ___error0, method) ((  void (*) (Subscribe_1_t3319793125 *, Exception_t1967233988 *, const MethodInfo*))Subscribe_1_OnError_m462410643_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Boolean>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m1793657702_gshared (Subscribe_1_t3319793125 * __this, const MethodInfo* method);
#define Subscribe_1_OnCompleted_m1793657702(__this, method) ((  void (*) (Subscribe_1_t3319793125 *, const MethodInfo*))Subscribe_1_OnCompleted_m1793657702_gshared)(__this, method)
