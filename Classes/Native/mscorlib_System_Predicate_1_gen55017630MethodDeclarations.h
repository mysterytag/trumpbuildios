﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"

// System.Void System.Predicate`1<SubstanceBase`2/Intrusion<System.Double,System.Double>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m3277368832(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t55017630 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<SubstanceBase`2/Intrusion<System.Double,System.Double>>::Invoke(T)
#define Predicate_1_Invoke_m704904322(__this, ___obj0, method) ((  bool (*) (Predicate_1_t55017630 *, Intrusion_t3779021028 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<SubstanceBase`2/Intrusion<System.Double,System.Double>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1705937169(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t55017630 *, Intrusion_t3779021028 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<SubstanceBase`2/Intrusion<System.Double,System.Double>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m2018413714(__this, ___result0, method) ((  bool (*) (Predicate_1_t55017630 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
