﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,UniRx.Unit>
struct SelectManyObserverWithIndex_t3833952010;
// UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>
struct SelectManyObservable_2_t2284173584;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyObserverWithIndex__ctor_m3250801526_gshared (SelectManyObserverWithIndex_t3833952010 * __this, SelectManyObservable_2_t2284173584 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyObserverWithIndex__ctor_m3250801526(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyObserverWithIndex_t3833952010 *, SelectManyObservable_2_t2284173584 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex__ctor_m3250801526_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,UniRx.Unit>::Run()
extern "C"  Il2CppObject * SelectManyObserverWithIndex_Run_m4031715250_gshared (SelectManyObserverWithIndex_t3833952010 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_Run_m4031715250(__this, method) ((  Il2CppObject * (*) (SelectManyObserverWithIndex_t3833952010 *, const MethodInfo*))SelectManyObserverWithIndex_Run_m4031715250_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,UniRx.Unit>::OnNext(TSource)
extern "C"  void SelectManyObserverWithIndex_OnNext_m4222897713_gshared (SelectManyObserverWithIndex_t3833952010 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnNext_m4222897713(__this, ___value0, method) ((  void (*) (SelectManyObserverWithIndex_t3833952010 *, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex_OnNext_m4222897713_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectManyObserverWithIndex_OnError_m900318299_gshared (SelectManyObserverWithIndex_t3833952010 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnError_m900318299(__this, ___error0, method) ((  void (*) (SelectManyObserverWithIndex_t3833952010 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserverWithIndex_OnError_m900318299_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<System.Object,UniRx.Unit>::OnCompleted()
extern "C"  void SelectManyObserverWithIndex_OnCompleted_m504358958_gshared (SelectManyObserverWithIndex_t3833952010 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnCompleted_m504358958(__this, method) ((  void (*) (SelectManyObserverWithIndex_t3833952010 *, const MethodInfo*))SelectManyObserverWithIndex_OnCompleted_m504358958_gshared)(__this, method)
