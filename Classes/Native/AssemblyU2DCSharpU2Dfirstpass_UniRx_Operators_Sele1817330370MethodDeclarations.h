﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,System.Object>
struct SelectManyEnumerableObserver_t1817330370;
// UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>
struct SelectManyObservable_2_t562993966;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserver__ctor_m99749780_gshared (SelectManyEnumerableObserver_t1817330370 * __this, SelectManyObservable_2_t562993966 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserver__ctor_m99749780(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserver_t1817330370 *, SelectManyObservable_2_t562993966 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver__ctor_m99749780_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserver_Run_m457881300_gshared (SelectManyEnumerableObserver_t1817330370 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_Run_m457881300(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserver_t1817330370 *, const MethodInfo*))SelectManyEnumerableObserver_Run_m457881300_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,System.Object>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserver_OnNext_m3463169363_gshared (SelectManyEnumerableObserver_t1817330370 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnNext_m3463169363(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserver_t1817330370 *, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver_OnNext_m3463169363_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserver_OnError_m1696086269_gshared (SelectManyEnumerableObserver_t1817330370 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnError_m1696086269(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserver_t1817330370 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserver_OnError_m1696086269_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,System.Object>::OnCompleted()
extern "C"  void SelectManyEnumerableObserver_OnCompleted_m2032058320_gshared (SelectManyEnumerableObserver_t1817330370 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnCompleted_m2032058320(__this, method) ((  void (*) (SelectManyEnumerableObserver_t1817330370 *, const MethodInfo*))SelectManyEnumerableObserver_OnCompleted_m2032058320_gshared)(__this, method)
