﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<System.Double>
struct EmptyObserver_1_t4238836900;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2372598506_gshared (EmptyObserver_1_t4238836900 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2372598506(__this, method) ((  void (*) (EmptyObserver_1_t4238836900 *, const MethodInfo*))EmptyObserver_1__ctor_m2372598506_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m54013443_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m54013443(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m54013443_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m4146386676_gshared (EmptyObserver_1_t4238836900 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m4146386676(__this, method) ((  void (*) (EmptyObserver_1_t4238836900 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m4146386676_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1512094241_gshared (EmptyObserver_1_t4238836900 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1512094241(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t4238836900 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1512094241_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Double>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2270233362_gshared (EmptyObserver_1_t4238836900 * __this, double ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2270233362(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t4238836900 *, double, const MethodInfo*))EmptyObserver_1_OnNext_m2270233362_gshared)(__this, ___value0, method)
