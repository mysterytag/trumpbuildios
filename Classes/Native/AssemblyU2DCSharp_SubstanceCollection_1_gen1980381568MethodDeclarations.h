﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceCollection`1<System.Object>
struct SubstanceCollection_1_t1980381568;
// UniRx.InternalUtil.ImmutableList`1<System.Object>
struct ImmutableList_1_t1897310919;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceCollection`1<System.Object>::.ctor()
extern "C"  void SubstanceCollection_1__ctor_m2479562270_gshared (SubstanceCollection_1_t1980381568 * __this, const MethodInfo* method);
#define SubstanceCollection_1__ctor_m2479562270(__this, method) ((  void (*) (SubstanceCollection_1_t1980381568 *, const MethodInfo*))SubstanceCollection_1__ctor_m2479562270_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> SubstanceCollection`1<System.Object>::Result()
extern "C"  ImmutableList_1_t1897310919 * SubstanceCollection_1_Result_m1905662954_gshared (SubstanceCollection_1_t1980381568 * __this, const MethodInfo* method);
#define SubstanceCollection_1_Result_m1905662954(__this, method) ((  ImmutableList_1_t1897310919 * (*) (SubstanceCollection_1_t1980381568 *, const MethodInfo*))SubstanceCollection_1_Result_m1905662954_gshared)(__this, method)
