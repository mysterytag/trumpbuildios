﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DonateCell
struct DonateCell_t2798474385;

#include "codegen/il2cpp-codegen.h"

// System.Void DonateCell::.ctor()
extern "C"  void DonateCell__ctor_m3260405546 (DonateCell_t2798474385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DonateCell::Awake()
extern "C"  void DonateCell_Awake_m3498010765 (DonateCell_t2798474385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DonateCell::<Awake>m__6()
extern "C"  void DonateCell_U3CAwakeU3Em__6_m695014566 (DonateCell_t2798474385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
