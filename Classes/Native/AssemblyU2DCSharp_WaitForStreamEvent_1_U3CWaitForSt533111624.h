﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// WaitForStreamEvent`1<System.Object>
struct WaitForStreamEvent_1_t1294890264;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145<System.Object>
struct  U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624  : public Il2CppObject
{
public:
	// System.Action`1<T> WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145::act
	Action_1_t985559125 * ___act_0;
	// WaitForStreamEvent`1<T> WaitForStreamEvent`1/<WaitForStreamEvent>c__AnonStorey145::<>f__this
	WaitForStreamEvent_1_t1294890264 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_act_0() { return static_cast<int32_t>(offsetof(U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624, ___act_0)); }
	inline Action_1_t985559125 * get_act_0() const { return ___act_0; }
	inline Action_1_t985559125 ** get_address_of_act_0() { return &___act_0; }
	inline void set_act_0(Action_1_t985559125 * value)
	{
		___act_0 = value;
		Il2CppCodeGenWriteBarrier(&___act_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CWaitForStreamEventU3Ec__AnonStorey145_t533111624, ___U3CU3Ef__this_1)); }
	inline WaitForStreamEvent_1_t1294890264 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline WaitForStreamEvent_1_t1294890264 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(WaitForStreamEvent_1_t1294890264 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
