﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ConcatObservable`1<System.Object>
struct ConcatObservable_1_t1571046694;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ConcatObservable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  void ConcatObservable_1__ctor_m3950127814_gshared (ConcatObservable_1_t1571046694 * __this, Il2CppObject* ___sources0, const MethodInfo* method);
#define ConcatObservable_1__ctor_m3950127814(__this, ___sources0, method) ((  void (*) (ConcatObservable_1_t1571046694 *, Il2CppObject*, const MethodInfo*))ConcatObservable_1__ctor_m3950127814_gshared)(__this, ___sources0, method)
// UniRx.IObservable`1<T> UniRx.Operators.ConcatObservable`1<System.Object>::Combine(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  Il2CppObject* ConcatObservable_1_Combine_m1191673255_gshared (ConcatObservable_1_t1571046694 * __this, Il2CppObject* ___combineSources0, const MethodInfo* method);
#define ConcatObservable_1_Combine_m1191673255(__this, ___combineSources0, method) ((  Il2CppObject* (*) (ConcatObservable_1_t1571046694 *, Il2CppObject*, const MethodInfo*))ConcatObservable_1_Combine_m1191673255_gshared)(__this, ___combineSources0, method)
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>> UniRx.Operators.ConcatObservable`1<System.Object>::CombineSources(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>,System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  Il2CppObject* ConcatObservable_1_CombineSources_m1435020884_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___first0, Il2CppObject* ___second1, const MethodInfo* method);
#define ConcatObservable_1_CombineSources_m1435020884(__this /* static, unused */, ___first0, ___second1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))ConcatObservable_1_CombineSources_m1435020884_gshared)(__this /* static, unused */, ___first0, ___second1, method)
// System.IDisposable UniRx.Operators.ConcatObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ConcatObservable_1_SubscribeCore_m2004381934_gshared (ConcatObservable_1_t1571046694 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ConcatObservable_1_SubscribeCore_m2004381934(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ConcatObservable_1_t1571046694 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ConcatObservable_1_SubscribeCore_m2004381934_gshared)(__this, ___observer0, ___cancel1, method)
