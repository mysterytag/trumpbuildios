﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.HttpWebRequest
struct HttpWebRequest_t171953869;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey35
struct  U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859  : public Il2CppObject
{
public:
	// System.Net.HttpWebRequest UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey35::request
	HttpWebRequest_t171953869 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859, ___request_0)); }
	inline HttpWebRequest_t171953869 * get_request_0() const { return ___request_0; }
	inline HttpWebRequest_t171953869 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HttpWebRequest_t171953869 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier(&___request_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
