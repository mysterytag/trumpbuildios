﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.StartObservable`1<System.Object>
struct StartObservable_1_t2362529796;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.StartObservable`1/StartObserver<System.Object>
struct  StartObserver_t822136817  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.StartObservable`1<T> UniRx.Operators.StartObservable`1/StartObserver::parent
	StartObservable_1_t2362529796 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(StartObserver_t822136817, ___parent_2)); }
	inline StartObservable_1_t2362529796 * get_parent_2() const { return ___parent_2; }
	inline StartObservable_1_t2362529796 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(StartObservable_1_t2362529796 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
