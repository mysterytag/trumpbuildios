﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Object
struct Il2CppObject;
// AnalyticsController
struct AnalyticsController_t2265609122;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsController/<SendRevenue>c__Iterator1F
struct  U3CSendRevenueU3Ec__Iterator1F_t965039904  : public Il2CppObject
{
public:
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::revenue
	String_t* ___revenue_0;
	// System.Single AnalyticsController/<SendRevenue>c__Iterator1F::<usdRevenue>__0
	float ___U3CusdRevenueU3E__0_1;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::currency
	String_t* ___currency_2;
	// System.Boolean AnalyticsController/<SendRevenue>c__Iterator1F::storeDebuff
	bool ___storeDebuff_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> AnalyticsController/<SendRevenue>c__Iterator1F::<param>__1
	Dictionary_2_t2606186806 * ___U3CparamU3E__1_4;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::offer
	String_t* ___offer_5;
	// System.Int32 AnalyticsController/<SendRevenue>c__Iterator1F::<a>__2
	int32_t ___U3CaU3E__2_6;
	// System.Single AnalyticsController/<SendRevenue>c__Iterator1F::<b>__3
	float ___U3CbU3E__3_7;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::curCristal
	String_t* ___curCristal_8;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::origin
	String_t* ___origin_9;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::bonusType
	String_t* ___bonusType_10;
	// System.Int32 AnalyticsController/<SendRevenue>c__Iterator1F::$PC
	int32_t ___U24PC_11;
	// System.Object AnalyticsController/<SendRevenue>c__Iterator1F::$current
	Il2CppObject * ___U24current_12;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::<$>revenue
	String_t* ___U3CU24U3Erevenue_13;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::<$>currency
	String_t* ___U3CU24U3Ecurrency_14;
	// System.Boolean AnalyticsController/<SendRevenue>c__Iterator1F::<$>storeDebuff
	bool ___U3CU24U3EstoreDebuff_15;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::<$>offer
	String_t* ___U3CU24U3Eoffer_16;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::<$>curCristal
	String_t* ___U3CU24U3EcurCristal_17;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::<$>origin
	String_t* ___U3CU24U3Eorigin_18;
	// System.String AnalyticsController/<SendRevenue>c__Iterator1F::<$>bonusType
	String_t* ___U3CU24U3EbonusType_19;
	// AnalyticsController AnalyticsController/<SendRevenue>c__Iterator1F::<>f__this
	AnalyticsController_t2265609122 * ___U3CU3Ef__this_20;

public:
	inline static int32_t get_offset_of_revenue_0() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___revenue_0)); }
	inline String_t* get_revenue_0() const { return ___revenue_0; }
	inline String_t** get_address_of_revenue_0() { return &___revenue_0; }
	inline void set_revenue_0(String_t* value)
	{
		___revenue_0 = value;
		Il2CppCodeGenWriteBarrier(&___revenue_0, value);
	}

	inline static int32_t get_offset_of_U3CusdRevenueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CusdRevenueU3E__0_1)); }
	inline float get_U3CusdRevenueU3E__0_1() const { return ___U3CusdRevenueU3E__0_1; }
	inline float* get_address_of_U3CusdRevenueU3E__0_1() { return &___U3CusdRevenueU3E__0_1; }
	inline void set_U3CusdRevenueU3E__0_1(float value)
	{
		___U3CusdRevenueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_currency_2() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___currency_2)); }
	inline String_t* get_currency_2() const { return ___currency_2; }
	inline String_t** get_address_of_currency_2() { return &___currency_2; }
	inline void set_currency_2(String_t* value)
	{
		___currency_2 = value;
		Il2CppCodeGenWriteBarrier(&___currency_2, value);
	}

	inline static int32_t get_offset_of_storeDebuff_3() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___storeDebuff_3)); }
	inline bool get_storeDebuff_3() const { return ___storeDebuff_3; }
	inline bool* get_address_of_storeDebuff_3() { return &___storeDebuff_3; }
	inline void set_storeDebuff_3(bool value)
	{
		___storeDebuff_3 = value;
	}

	inline static int32_t get_offset_of_U3CparamU3E__1_4() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CparamU3E__1_4)); }
	inline Dictionary_2_t2606186806 * get_U3CparamU3E__1_4() const { return ___U3CparamU3E__1_4; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CparamU3E__1_4() { return &___U3CparamU3E__1_4; }
	inline void set_U3CparamU3E__1_4(Dictionary_2_t2606186806 * value)
	{
		___U3CparamU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CparamU3E__1_4, value);
	}

	inline static int32_t get_offset_of_offer_5() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___offer_5)); }
	inline String_t* get_offer_5() const { return ___offer_5; }
	inline String_t** get_address_of_offer_5() { return &___offer_5; }
	inline void set_offer_5(String_t* value)
	{
		___offer_5 = value;
		Il2CppCodeGenWriteBarrier(&___offer_5, value);
	}

	inline static int32_t get_offset_of_U3CaU3E__2_6() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CaU3E__2_6)); }
	inline int32_t get_U3CaU3E__2_6() const { return ___U3CaU3E__2_6; }
	inline int32_t* get_address_of_U3CaU3E__2_6() { return &___U3CaU3E__2_6; }
	inline void set_U3CaU3E__2_6(int32_t value)
	{
		___U3CaU3E__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__3_7() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CbU3E__3_7)); }
	inline float get_U3CbU3E__3_7() const { return ___U3CbU3E__3_7; }
	inline float* get_address_of_U3CbU3E__3_7() { return &___U3CbU3E__3_7; }
	inline void set_U3CbU3E__3_7(float value)
	{
		___U3CbU3E__3_7 = value;
	}

	inline static int32_t get_offset_of_curCristal_8() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___curCristal_8)); }
	inline String_t* get_curCristal_8() const { return ___curCristal_8; }
	inline String_t** get_address_of_curCristal_8() { return &___curCristal_8; }
	inline void set_curCristal_8(String_t* value)
	{
		___curCristal_8 = value;
		Il2CppCodeGenWriteBarrier(&___curCristal_8, value);
	}

	inline static int32_t get_offset_of_origin_9() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___origin_9)); }
	inline String_t* get_origin_9() const { return ___origin_9; }
	inline String_t** get_address_of_origin_9() { return &___origin_9; }
	inline void set_origin_9(String_t* value)
	{
		___origin_9 = value;
		Il2CppCodeGenWriteBarrier(&___origin_9, value);
	}

	inline static int32_t get_offset_of_bonusType_10() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___bonusType_10)); }
	inline String_t* get_bonusType_10() const { return ___bonusType_10; }
	inline String_t** get_address_of_bonusType_10() { return &___bonusType_10; }
	inline void set_bonusType_10(String_t* value)
	{
		___bonusType_10 = value;
		Il2CppCodeGenWriteBarrier(&___bonusType_10, value);
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Erevenue_13() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CU24U3Erevenue_13)); }
	inline String_t* get_U3CU24U3Erevenue_13() const { return ___U3CU24U3Erevenue_13; }
	inline String_t** get_address_of_U3CU24U3Erevenue_13() { return &___U3CU24U3Erevenue_13; }
	inline void set_U3CU24U3Erevenue_13(String_t* value)
	{
		___U3CU24U3Erevenue_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Erevenue_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecurrency_14() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CU24U3Ecurrency_14)); }
	inline String_t* get_U3CU24U3Ecurrency_14() const { return ___U3CU24U3Ecurrency_14; }
	inline String_t** get_address_of_U3CU24U3Ecurrency_14() { return &___U3CU24U3Ecurrency_14; }
	inline void set_U3CU24U3Ecurrency_14(String_t* value)
	{
		___U3CU24U3Ecurrency_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecurrency_14, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EstoreDebuff_15() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CU24U3EstoreDebuff_15)); }
	inline bool get_U3CU24U3EstoreDebuff_15() const { return ___U3CU24U3EstoreDebuff_15; }
	inline bool* get_address_of_U3CU24U3EstoreDebuff_15() { return &___U3CU24U3EstoreDebuff_15; }
	inline void set_U3CU24U3EstoreDebuff_15(bool value)
	{
		___U3CU24U3EstoreDebuff_15 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eoffer_16() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CU24U3Eoffer_16)); }
	inline String_t* get_U3CU24U3Eoffer_16() const { return ___U3CU24U3Eoffer_16; }
	inline String_t** get_address_of_U3CU24U3Eoffer_16() { return &___U3CU24U3Eoffer_16; }
	inline void set_U3CU24U3Eoffer_16(String_t* value)
	{
		___U3CU24U3Eoffer_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eoffer_16, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcurCristal_17() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CU24U3EcurCristal_17)); }
	inline String_t* get_U3CU24U3EcurCristal_17() const { return ___U3CU24U3EcurCristal_17; }
	inline String_t** get_address_of_U3CU24U3EcurCristal_17() { return &___U3CU24U3EcurCristal_17; }
	inline void set_U3CU24U3EcurCristal_17(String_t* value)
	{
		___U3CU24U3EcurCristal_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EcurCristal_17, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eorigin_18() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CU24U3Eorigin_18)); }
	inline String_t* get_U3CU24U3Eorigin_18() const { return ___U3CU24U3Eorigin_18; }
	inline String_t** get_address_of_U3CU24U3Eorigin_18() { return &___U3CU24U3Eorigin_18; }
	inline void set_U3CU24U3Eorigin_18(String_t* value)
	{
		___U3CU24U3Eorigin_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eorigin_18, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EbonusType_19() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CU24U3EbonusType_19)); }
	inline String_t* get_U3CU24U3EbonusType_19() const { return ___U3CU24U3EbonusType_19; }
	inline String_t** get_address_of_U3CU24U3EbonusType_19() { return &___U3CU24U3EbonusType_19; }
	inline void set_U3CU24U3EbonusType_19(String_t* value)
	{
		___U3CU24U3EbonusType_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EbonusType_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_20() { return static_cast<int32_t>(offsetof(U3CSendRevenueU3Ec__Iterator1F_t965039904, ___U3CU3Ef__this_20)); }
	inline AnalyticsController_t2265609122 * get_U3CU3Ef__this_20() const { return ___U3CU3Ef__this_20; }
	inline AnalyticsController_t2265609122 ** get_address_of_U3CU3Ef__this_20() { return &___U3CU3Ef__this_20; }
	inline void set_U3CU3Ef__this_20(AnalyticsController_t2265609122 * value)
	{
		___U3CU3Ef__this_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
