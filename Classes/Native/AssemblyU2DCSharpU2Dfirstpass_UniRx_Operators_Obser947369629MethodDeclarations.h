﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>
struct ObserveOn__t947369629;
// UniRx.Operators.ObserveOnObservable`1<System.Object>
struct ObserveOnObservable_1_t1522207957;
// UniRx.ISchedulerQueueing
struct ISchedulerQueueing_t1271699701;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::.ctor(UniRx.Operators.ObserveOnObservable`1<T>,UniRx.ISchedulerQueueing,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void ObserveOn___ctor_m1548936999_gshared (ObserveOn__t947369629 * __this, ObserveOnObservable_1_t1522207957 * ___parent0, Il2CppObject * ___scheduler1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method);
#define ObserveOn___ctor_m1548936999(__this, ___parent0, ___scheduler1, ___observer2, ___cancel3, method) ((  void (*) (ObserveOn__t947369629 *, ObserveOnObservable_1_t1522207957 *, Il2CppObject *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ObserveOn___ctor_m1548936999_gshared)(__this, ___parent0, ___scheduler1, ___observer2, ___cancel3, method)
// System.IDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::Run()
extern "C"  Il2CppObject * ObserveOn__Run_m3768363898_gshared (ObserveOn__t947369629 * __this, const MethodInfo* method);
#define ObserveOn__Run_m3768363898(__this, method) ((  Il2CppObject * (*) (ObserveOn__t947369629 *, const MethodInfo*))ObserveOn__Run_m3768363898_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnNext_(T)
extern "C"  void ObserveOn__OnNext__m2423936093_gshared (ObserveOn__t947369629 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ObserveOn__OnNext__m2423936093(__this, ___value0, method) ((  void (*) (ObserveOn__t947369629 *, Il2CppObject *, const MethodInfo*))ObserveOn__OnNext__m2423936093_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnError_(System.Exception)
extern "C"  void ObserveOn__OnError__m1521757812_gshared (ObserveOn__t947369629 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ObserveOn__OnError__m1521757812(__this, ___error0, method) ((  void (*) (ObserveOn__t947369629 *, Exception_t1967233988 *, const MethodInfo*))ObserveOn__OnError__m1521757812_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnCompleted_(UniRx.Unit)
extern "C"  void ObserveOn__OnCompleted__m1706344645_gshared (ObserveOn__t947369629 * __this, Unit_t2558286038  ____0, const MethodInfo* method);
#define ObserveOn__OnCompleted__m1706344645(__this, ____0, method) ((  void (*) (ObserveOn__t947369629 *, Unit_t2558286038 , const MethodInfo*))ObserveOn__OnCompleted__m1706344645_gshared)(__this, ____0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnNext(T)
extern "C"  void ObserveOn__OnNext_m2710539262_gshared (ObserveOn__t947369629 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ObserveOn__OnNext_m2710539262(__this, ___value0, method) ((  void (*) (ObserveOn__t947369629 *, Il2CppObject *, const MethodInfo*))ObserveOn__OnNext_m2710539262_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnError(System.Exception)
extern "C"  void ObserveOn__OnError_m3538414349_gshared (ObserveOn__t947369629 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ObserveOn__OnError_m3538414349(__this, ___error0, method) ((  void (*) (ObserveOn__t947369629 *, Exception_t1967233988 *, const MethodInfo*))ObserveOn__OnError_m3538414349_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnCompleted()
extern "C"  void ObserveOn__OnCompleted_m1272777184_gshared (ObserveOn__t947369629 * __this, const MethodInfo* method);
#define ObserveOn__OnCompleted_m1272777184(__this, method) ((  void (*) (ObserveOn__t947369629 *, const MethodInfo*))ObserveOn__OnCompleted_m1272777184_gshared)(__this, method)
