﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// System.Func`2<System.Collections.Generic.IList`1<System.Int64>,System.Boolean>
struct Func_2_t2964252473;
// System.Action`1<System.Collections.Generic.IList`1<System.Int64>>
struct Action_1_t867392605;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample08_DetectDoubleClick
struct  Sample08_DetectDoubleClick_t1711438338  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct Sample08_DetectDoubleClick_t1711438338_StaticFields
{
public:
	// System.Func`2<System.Int64,System.Boolean> UniRx.Examples.Sample08_DetectDoubleClick::<>f__am$cache0
	Func_2_t2251336571 * ___U3CU3Ef__amU24cache0_2;
	// System.Func`2<System.Collections.Generic.IList`1<System.Int64>,System.Boolean> UniRx.Examples.Sample08_DetectDoubleClick::<>f__am$cache1
	Func_2_t2964252473 * ___U3CU3Ef__amU24cache1_3;
	// System.Action`1<System.Collections.Generic.IList`1<System.Int64>> UniRx.Examples.Sample08_DetectDoubleClick::<>f__am$cache2
	Action_1_t867392605 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Sample08_DetectDoubleClick_t1711438338_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t2251336571 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t2251336571 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t2251336571 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Sample08_DetectDoubleClick_t1711438338_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Func_2_t2964252473 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Func_2_t2964252473 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Func_2_t2964252473 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Sample08_DetectDoubleClick_t1711438338_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_1_t867392605 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_1_t867392605 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_1_t867392605 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
