﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableEndDragTrigger
struct ObservableEndDragTrigger_t4099037453;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservableEndDragTrigger::.ctor()
extern "C"  void ObservableEndDragTrigger__ctor_m2945160144 (ObservableEndDragTrigger_t4099037453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEndDragTrigger::UnityEngine.EventSystems.IEndDragHandler.OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEndDragTrigger_UnityEngine_EventSystems_IEndDragHandler_OnEndDrag_m1053766801 (ObservableEndDragTrigger_t4099037453 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEndDragTrigger::OnEndDragAsObservable()
extern "C"  Il2CppObject* ObservableEndDragTrigger_OnEndDragAsObservable_m3420795799 (ObservableEndDragTrigger_t4099037453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEndDragTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableEndDragTrigger_RaiseOnCompletedOnDestroy_m2875220745 (ObservableEndDragTrigger_t4099037453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
