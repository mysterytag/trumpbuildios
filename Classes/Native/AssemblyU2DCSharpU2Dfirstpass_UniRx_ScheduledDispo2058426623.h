﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile)
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ScheduledDisposable
struct  ScheduledDisposable_t2058426623  : public Il2CppObject
{
public:
	// UniRx.IScheduler UniRx.ScheduledDisposable::scheduler
	Il2CppObject * ___scheduler_0;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.ScheduledDisposable::disposable
	Il2CppObject * ___disposable_1;
	// System.Int32 UniRx.ScheduledDisposable::isDisposed
	int32_t ___isDisposed_2;

public:
	inline static int32_t get_offset_of_scheduler_0() { return static_cast<int32_t>(offsetof(ScheduledDisposable_t2058426623, ___scheduler_0)); }
	inline Il2CppObject * get_scheduler_0() const { return ___scheduler_0; }
	inline Il2CppObject ** get_address_of_scheduler_0() { return &___scheduler_0; }
	inline void set_scheduler_0(Il2CppObject * value)
	{
		___scheduler_0 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_0, value);
	}

	inline static int32_t get_offset_of_disposable_1() { return static_cast<int32_t>(offsetof(ScheduledDisposable_t2058426623, ___disposable_1)); }
	inline Il2CppObject * get_disposable_1() const { return ___disposable_1; }
	inline Il2CppObject ** get_address_of_disposable_1() { return &___disposable_1; }
	inline void set_disposable_1(Il2CppObject * value)
	{
		___disposable_1 = value;
		Il2CppCodeGenWriteBarrier(&___disposable_1, value);
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ScheduledDisposable_t2058426623, ___isDisposed_2)); }
	inline int32_t get_isDisposed_2() const { return ___isDisposed_2; }
	inline int32_t* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(int32_t value)
	{
		___isDisposed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
