﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<System.Single>
struct OperatorObservableBase_1_t22353992;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<System.Single>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m4000287643_gshared (OperatorObservableBase_1_t22353992 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m4000287643(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t22353992 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m4000287643_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Single>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2802103591_gshared (OperatorObservableBase_1_t22353992 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2802103591(__this, method) ((  bool (*) (OperatorObservableBase_1_t22353992 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2802103591_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Single>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1800231929_gshared (OperatorObservableBase_1_t22353992 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m1800231929(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t22353992 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m1800231929_gshared)(__this, ___observer0, method)
