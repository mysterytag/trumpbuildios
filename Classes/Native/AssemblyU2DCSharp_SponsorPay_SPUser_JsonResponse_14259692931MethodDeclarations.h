﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>
struct JsonResponse_1_t4259692931;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen2936825364.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m146280347_gshared (JsonResponse_1_t4259692931 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m146280347(__this, method) ((  void (*) (JsonResponse_1_t4259692931 *, const MethodInfo*))JsonResponse_1__ctor_m146280347_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m3520725836_gshared (JsonResponse_1_t4259692931 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m3520725836(__this, method) ((  String_t* (*) (JsonResponse_1_t4259692931 *, const MethodInfo*))JsonResponse_1_get_key_m3520725836_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m1936855943_gshared (JsonResponse_1_t4259692931 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m1936855943(__this, ___value0, method) ((  void (*) (JsonResponse_1_t4259692931 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m1936855943_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::get_value()
extern "C"  Nullable_1_t2936825364  JsonResponse_1_get_value_m3308583682_gshared (JsonResponse_1_t4259692931 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m3308583682(__this, method) ((  Nullable_1_t2936825364  (*) (JsonResponse_1_t4259692931 *, const MethodInfo*))JsonResponse_1_get_value_m3308583682_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m597188561_gshared (JsonResponse_1_t4259692931 * __this, Nullable_1_t2936825364  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m597188561(__this, ___value0, method) ((  void (*) (JsonResponse_1_t4259692931 *, Nullable_1_t2936825364 , const MethodInfo*))JsonResponse_1_set_value_m597188561_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m2616112565_gshared (JsonResponse_1_t4259692931 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m2616112565(__this, method) ((  String_t* (*) (JsonResponse_1_t4259692931 *, const MethodInfo*))JsonResponse_1_get_error_m2616112565_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserConnection>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m4253582206_gshared (JsonResponse_1_t4259692931 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m4253582206(__this, ___value0, method) ((  void (*) (JsonResponse_1_t4259692931 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m4253582206_gshared)(__this, ___value0, method)
