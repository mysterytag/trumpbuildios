﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>
struct MergeConcurrentObserver_t3534593302;
// UniRx.Operators.MergeObservable`1<System.Object>
struct MergeObservable_1_t3637992042;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::.ctor(UniRx.Operators.MergeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void MergeConcurrentObserver__ctor_m1744247409_gshared (MergeConcurrentObserver_t3534593302 * __this, MergeObservable_1_t3637992042 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define MergeConcurrentObserver__ctor_m1744247409(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (MergeConcurrentObserver_t3534593302 *, MergeObservable_1_t3637992042 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))MergeConcurrentObserver__ctor_m1744247409_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::Run()
extern "C"  Il2CppObject * MergeConcurrentObserver_Run_m2796918060_gshared (MergeConcurrentObserver_t3534593302 * __this, const MethodInfo* method);
#define MergeConcurrentObserver_Run_m2796918060(__this, method) ((  Il2CppObject * (*) (MergeConcurrentObserver_t3534593302 *, const MethodInfo*))MergeConcurrentObserver_Run_m2796918060_gshared)(__this, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::OnNext(UniRx.IObservable`1<T>)
extern "C"  void MergeConcurrentObserver_OnNext_m3276267255_gshared (MergeConcurrentObserver_t3534593302 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define MergeConcurrentObserver_OnNext_m3276267255(__this, ___value0, method) ((  void (*) (MergeConcurrentObserver_t3534593302 *, Il2CppObject*, const MethodInfo*))MergeConcurrentObserver_OnNext_m3276267255_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::OnError(System.Exception)
extern "C"  void MergeConcurrentObserver_OnError_m58416213_gshared (MergeConcurrentObserver_t3534593302 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define MergeConcurrentObserver_OnError_m58416213(__this, ___error0, method) ((  void (*) (MergeConcurrentObserver_t3534593302 *, Exception_t1967233988 *, const MethodInfo*))MergeConcurrentObserver_OnError_m58416213_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::OnCompleted()
extern "C"  void MergeConcurrentObserver_OnCompleted_m3814336296_gshared (MergeConcurrentObserver_t3534593302 * __this, const MethodInfo* method);
#define MergeConcurrentObserver_OnCompleted_m3814336296(__this, method) ((  void (*) (MergeConcurrentObserver_t3534593302 *, const MethodInfo*))MergeConcurrentObserver_OnCompleted_m3814336296_gshared)(__this, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>::Subscribe(UniRx.IObservable`1<T>)
extern "C"  void MergeConcurrentObserver_Subscribe_m3366134245_gshared (MergeConcurrentObserver_t3534593302 * __this, Il2CppObject* ___innerSource0, const MethodInfo* method);
#define MergeConcurrentObserver_Subscribe_m3366134245(__this, ___innerSource0, method) ((  void (*) (MergeConcurrentObserver_t3534593302 *, Il2CppObject*, const MethodInfo*))MergeConcurrentObserver_Subscribe_m3366134245_gshared)(__this, ___innerSource0, method)
