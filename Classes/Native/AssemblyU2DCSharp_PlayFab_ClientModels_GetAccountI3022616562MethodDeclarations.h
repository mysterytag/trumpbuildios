﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetAccountInfoResult
struct GetAccountInfoResult_t3022616562;
// PlayFab.ClientModels.UserAccountInfo
struct UserAccountInfo_t960776096;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserAccountI960776096.h"

// System.Void PlayFab.ClientModels.GetAccountInfoResult::.ctor()
extern "C"  void GetAccountInfoResult__ctor_m1390764939 (GetAccountInfoResult_t3022616562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserAccountInfo PlayFab.ClientModels.GetAccountInfoResult::get_AccountInfo()
extern "C"  UserAccountInfo_t960776096 * GetAccountInfoResult_get_AccountInfo_m1818471270 (GetAccountInfoResult_t3022616562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetAccountInfoResult::set_AccountInfo(PlayFab.ClientModels.UserAccountInfo)
extern "C"  void GetAccountInfoResult_set_AccountInfo_m4040180661 (GetAccountInfoResult_t3022616562 * __this, UserAccountInfo_t960776096 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
