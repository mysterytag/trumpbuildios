﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<WindowBase>
struct List_1_t357456890;
// UnityEngine.UI.Image
struct Image_t3354615620;
// WindowManager
struct WindowManager_t3316821373;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// System.Func`2<WindowBase,System.Boolean>
struct Func_2_t3684413824;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowManager
struct  WindowManager_t3316821373  : public MonoBehaviour_t3012272455
{
public:
	// System.Collections.Generic.List`1<WindowBase> WindowManager::windows
	List_1_t357456890 * ___windows_2;
	// System.Boolean WindowManager::recentlyPoppedUp
	bool ___recentlyPoppedUp_3;
	// UnityEngine.UI.Image WindowManager::_darkImg
	Image_t3354615620 * ____darkImg_4;
	// UniRx.ReactiveProperty`1<System.Boolean> WindowManager::showDark
	ReactiveProperty_1_t819338379 * ___showDark_6;
	// System.Int32 WindowManager::highestSiblingIndex
	int32_t ___highestSiblingIndex_7;

public:
	inline static int32_t get_offset_of_windows_2() { return static_cast<int32_t>(offsetof(WindowManager_t3316821373, ___windows_2)); }
	inline List_1_t357456890 * get_windows_2() const { return ___windows_2; }
	inline List_1_t357456890 ** get_address_of_windows_2() { return &___windows_2; }
	inline void set_windows_2(List_1_t357456890 * value)
	{
		___windows_2 = value;
		Il2CppCodeGenWriteBarrier(&___windows_2, value);
	}

	inline static int32_t get_offset_of_recentlyPoppedUp_3() { return static_cast<int32_t>(offsetof(WindowManager_t3316821373, ___recentlyPoppedUp_3)); }
	inline bool get_recentlyPoppedUp_3() const { return ___recentlyPoppedUp_3; }
	inline bool* get_address_of_recentlyPoppedUp_3() { return &___recentlyPoppedUp_3; }
	inline void set_recentlyPoppedUp_3(bool value)
	{
		___recentlyPoppedUp_3 = value;
	}

	inline static int32_t get_offset_of__darkImg_4() { return static_cast<int32_t>(offsetof(WindowManager_t3316821373, ____darkImg_4)); }
	inline Image_t3354615620 * get__darkImg_4() const { return ____darkImg_4; }
	inline Image_t3354615620 ** get_address_of__darkImg_4() { return &____darkImg_4; }
	inline void set__darkImg_4(Image_t3354615620 * value)
	{
		____darkImg_4 = value;
		Il2CppCodeGenWriteBarrier(&____darkImg_4, value);
	}

	inline static int32_t get_offset_of_showDark_6() { return static_cast<int32_t>(offsetof(WindowManager_t3316821373, ___showDark_6)); }
	inline ReactiveProperty_1_t819338379 * get_showDark_6() const { return ___showDark_6; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_showDark_6() { return &___showDark_6; }
	inline void set_showDark_6(ReactiveProperty_1_t819338379 * value)
	{
		___showDark_6 = value;
		Il2CppCodeGenWriteBarrier(&___showDark_6, value);
	}

	inline static int32_t get_offset_of_highestSiblingIndex_7() { return static_cast<int32_t>(offsetof(WindowManager_t3316821373, ___highestSiblingIndex_7)); }
	inline int32_t get_highestSiblingIndex_7() const { return ___highestSiblingIndex_7; }
	inline int32_t* get_address_of_highestSiblingIndex_7() { return &___highestSiblingIndex_7; }
	inline void set_highestSiblingIndex_7(int32_t value)
	{
		___highestSiblingIndex_7 = value;
	}
};

struct WindowManager_t3316821373_StaticFields
{
public:
	// WindowManager WindowManager::instance
	WindowManager_t3316821373 * ___instance_5;
	// System.Func`2<WindowBase,System.Boolean> WindowManager::<>f__am$cache6
	Func_2_t3684413824 * ___U3CU3Ef__amU24cache6_8;
	// System.Func`2<WindowBase,System.Boolean> WindowManager::<>f__am$cache7
	Func_2_t3684413824 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(WindowManager_t3316821373_StaticFields, ___instance_5)); }
	inline WindowManager_t3316821373 * get_instance_5() const { return ___instance_5; }
	inline WindowManager_t3316821373 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(WindowManager_t3316821373 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(WindowManager_t3316821373_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Func_2_t3684413824 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Func_2_t3684413824 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Func_2_t3684413824 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(WindowManager_t3316821373_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Func_2_t3684413824 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Func_2_t3684413824 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Func_2_t3684413824 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
