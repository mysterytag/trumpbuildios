﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"

// System.Void System.Comparison`1<PlayFab.ClientModels.StatisticValue>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m652608530(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t1602363437 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<PlayFab.ClientModels.StatisticValue>::Invoke(T,T)
#define Comparison_1_Invoke_m904468078(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1602363437 *, StatisticValue_t3193655857 *, StatisticValue_t3193655857 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<PlayFab.ClientModels.StatisticValue>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m2057996263(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t1602363437 *, StatisticValue_t3193655857 *, StatisticValue_t3193655857 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<PlayFab.ClientModels.StatisticValue>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m2811524926(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t1602363437 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
