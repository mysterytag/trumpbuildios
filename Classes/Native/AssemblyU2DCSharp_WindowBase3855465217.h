﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WindowManager
struct WindowManager_t3316821373;
// UnityEngine.Animator
struct Animator_t792326996;
// System.Action
struct Action_t437523947;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowBase
struct  WindowBase_t3855465217  : public MonoBehaviour_t3012272455
{
public:
	// WindowManager WindowBase::managerInstance
	WindowManager_t3316821373 * ___managerInstance_2;
	// UnityEngine.Animator WindowBase::animator
	Animator_t792326996 * ___animator_3;
	// System.Action WindowBase::closeCallback
	Action_t437523947 * ___closeCallback_4;
	// System.Boolean WindowBase::isShown
	bool ___isShown_5;

public:
	inline static int32_t get_offset_of_managerInstance_2() { return static_cast<int32_t>(offsetof(WindowBase_t3855465217, ___managerInstance_2)); }
	inline WindowManager_t3316821373 * get_managerInstance_2() const { return ___managerInstance_2; }
	inline WindowManager_t3316821373 ** get_address_of_managerInstance_2() { return &___managerInstance_2; }
	inline void set_managerInstance_2(WindowManager_t3316821373 * value)
	{
		___managerInstance_2 = value;
		Il2CppCodeGenWriteBarrier(&___managerInstance_2, value);
	}

	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(WindowBase_t3855465217, ___animator_3)); }
	inline Animator_t792326996 * get_animator_3() const { return ___animator_3; }
	inline Animator_t792326996 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(Animator_t792326996 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier(&___animator_3, value);
	}

	inline static int32_t get_offset_of_closeCallback_4() { return static_cast<int32_t>(offsetof(WindowBase_t3855465217, ___closeCallback_4)); }
	inline Action_t437523947 * get_closeCallback_4() const { return ___closeCallback_4; }
	inline Action_t437523947 ** get_address_of_closeCallback_4() { return &___closeCallback_4; }
	inline void set_closeCallback_4(Action_t437523947 * value)
	{
		___closeCallback_4 = value;
		Il2CppCodeGenWriteBarrier(&___closeCallback_4, value);
	}

	inline static int32_t get_offset_of_isShown_5() { return static_cast<int32_t>(offsetof(WindowBase_t3855465217, ___isShown_5)); }
	inline bool get_isShown_5() const { return ___isShown_5; }
	inline bool* get_address_of_isShown_5() { return &___isShown_5; }
	inline void set_isShown_5(bool value)
	{
		___isShown_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
