﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkPSNAccountRequestCallback
struct UnlinkPSNAccountRequestCallback_t1178714047;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkPSNAccountRequest
struct UnlinkPSNAccountRequest_t26689898;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkPSNAcco26689898.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkPSNAccountRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkPSNAccountRequestCallback__ctor_m119707854 (UnlinkPSNAccountRequestCallback_t1178714047 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkPSNAccountRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkPSNAccountRequest,System.Object)
extern "C"  void UnlinkPSNAccountRequestCallback_Invoke_m4283090779 (UnlinkPSNAccountRequestCallback_t1178714047 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkPSNAccountRequest_t26689898 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkPSNAccountRequestCallback_t1178714047(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkPSNAccountRequest_t26689898 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkPSNAccountRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkPSNAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkPSNAccountRequestCallback_BeginInvoke_m880259902 (UnlinkPSNAccountRequestCallback_t1178714047 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkPSNAccountRequest_t26689898 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkPSNAccountRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkPSNAccountRequestCallback_EndInvoke_m1129226718 (UnlinkPSNAccountRequestCallback_t1178714047 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
