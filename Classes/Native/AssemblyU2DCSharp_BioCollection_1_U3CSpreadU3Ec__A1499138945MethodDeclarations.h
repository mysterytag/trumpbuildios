﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioCollection`1/<Spread>c__AnonStoreyF6<System.Object>
struct U3CSpreadU3Ec__AnonStoreyF6_t1499138945;

#include "codegen/il2cpp-codegen.h"

// System.Void BioCollection`1/<Spread>c__AnonStoreyF6<System.Object>::.ctor()
extern "C"  void U3CSpreadU3Ec__AnonStoreyF6__ctor_m872089071_gshared (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * __this, const MethodInfo* method);
#define U3CSpreadU3Ec__AnonStoreyF6__ctor_m872089071(__this, method) ((  void (*) (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 *, const MethodInfo*))U3CSpreadU3Ec__AnonStoreyF6__ctor_m872089071_gshared)(__this, method)
// System.Void BioCollection`1/<Spread>c__AnonStoreyF6<System.Object>::<>m__16E()
extern "C"  void U3CSpreadU3Ec__AnonStoreyF6_U3CU3Em__16E_m2978309802_gshared (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * __this, const MethodInfo* method);
#define U3CSpreadU3Ec__AnonStoreyF6_U3CU3Em__16E_m2978309802(__this, method) ((  void (*) (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 *, const MethodInfo*))U3CSpreadU3Ec__AnonStoreyF6_U3CU3Em__16E_m2978309802_gshared)(__this, method)
