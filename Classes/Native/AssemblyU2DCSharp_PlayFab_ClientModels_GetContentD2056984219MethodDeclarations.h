﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetContentDownloadUrlRequest
struct GetContentDownloadUrlRequest_t2056984219;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.GetContentDownloadUrlRequest::.ctor()
extern "C"  void GetContentDownloadUrlRequest__ctor_m1019930882 (GetContentDownloadUrlRequest_t2056984219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetContentDownloadUrlRequest::get_Key()
extern "C"  String_t* GetContentDownloadUrlRequest_get_Key_m476396153 (GetContentDownloadUrlRequest_t2056984219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetContentDownloadUrlRequest::set_Key(System.String)
extern "C"  void GetContentDownloadUrlRequest_set_Key_m765860704 (GetContentDownloadUrlRequest_t2056984219 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetContentDownloadUrlRequest::get_HttpMethod()
extern "C"  String_t* GetContentDownloadUrlRequest_get_HttpMethod_m202677585 (GetContentDownloadUrlRequest_t2056984219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetContentDownloadUrlRequest::set_HttpMethod(System.String)
extern "C"  void GetContentDownloadUrlRequest_set_HttpMethod_m3715573562 (GetContentDownloadUrlRequest_t2056984219 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetContentDownloadUrlRequest::get_ThruCDN()
extern "C"  Nullable_1_t3097043249  GetContentDownloadUrlRequest_get_ThruCDN_m3779461878 (GetContentDownloadUrlRequest_t2056984219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetContentDownloadUrlRequest::set_ThruCDN(System.Nullable`1<System.Boolean>)
extern "C"  void GetContentDownloadUrlRequest_set_ThruCDN_m675764367 (GetContentDownloadUrlRequest_t2056984219 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
