﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithPSNResponseCallback
struct LoginWithPSNResponseCallback_t1749901410;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithPSNRequest
struct LoginWithPSNRequest_t4060832355;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithPS4060832355.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithPSNResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithPSNResponseCallback__ctor_m1651044433 (LoginWithPSNResponseCallback_t1749901410 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithPSNResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithPSNRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LoginWithPSNResponseCallback_Invoke_m2345178935 (LoginWithPSNResponseCallback_t1749901410 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithPSNRequest_t4060832355 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithPSNResponseCallback_t1749901410(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithPSNRequest_t4060832355 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithPSNResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithPSNRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithPSNResponseCallback_BeginInvoke_m3444415620 (LoginWithPSNResponseCallback_t1749901410 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithPSNRequest_t4060832355 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithPSNResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithPSNResponseCallback_EndInvoke_m1763390177 (LoginWithPSNResponseCallback_t1749901410 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
