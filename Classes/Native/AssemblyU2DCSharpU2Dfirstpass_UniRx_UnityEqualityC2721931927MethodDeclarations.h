﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEqualityComparer/Vector3EqualityComparer
struct Vector3EqualityComparer_t2721931927;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void UniRx.UnityEqualityComparer/Vector3EqualityComparer::.ctor()
extern "C"  void Vector3EqualityComparer__ctor_m4072807467 (Vector3EqualityComparer_t2721931927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.UnityEqualityComparer/Vector3EqualityComparer::Equals(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3EqualityComparer_Equals_m2837926352 (Vector3EqualityComparer_t2721931927 * __this, Vector3_t3525329789  ___self0, Vector3_t3525329789  ___vector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.UnityEqualityComparer/Vector3EqualityComparer::GetHashCode(UnityEngine.Vector3)
extern "C"  int32_t Vector3EqualityComparer_GetHashCode_m701892809 (Vector3EqualityComparer_t2721931927 * __this, Vector3_t3525329789  ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
