﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E
struct U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6E__ctor_m990179820 (U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
