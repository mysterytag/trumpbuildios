﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.GA_SpecialEvents
struct  GA_SpecialEvents_t4239990159  : public MonoBehaviour_t3012272455
{
public:
	// System.Int32 GameAnalyticsSDK.GA_SpecialEvents::_frameCountCrit
	int32_t ____frameCountCrit_4;
	// System.Single GameAnalyticsSDK.GA_SpecialEvents::_lastUpdateCrit
	float ____lastUpdateCrit_5;

public:
	inline static int32_t get_offset_of__frameCountCrit_4() { return static_cast<int32_t>(offsetof(GA_SpecialEvents_t4239990159, ____frameCountCrit_4)); }
	inline int32_t get__frameCountCrit_4() const { return ____frameCountCrit_4; }
	inline int32_t* get_address_of__frameCountCrit_4() { return &____frameCountCrit_4; }
	inline void set__frameCountCrit_4(int32_t value)
	{
		____frameCountCrit_4 = value;
	}

	inline static int32_t get_offset_of__lastUpdateCrit_5() { return static_cast<int32_t>(offsetof(GA_SpecialEvents_t4239990159, ____lastUpdateCrit_5)); }
	inline float get__lastUpdateCrit_5() const { return ____lastUpdateCrit_5; }
	inline float* get_address_of__lastUpdateCrit_5() { return &____lastUpdateCrit_5; }
	inline void set__lastUpdateCrit_5(float value)
	{
		____lastUpdateCrit_5 = value;
	}
};

struct GA_SpecialEvents_t4239990159_StaticFields
{
public:
	// System.Int32 GameAnalyticsSDK.GA_SpecialEvents::_frameCountAvg
	int32_t ____frameCountAvg_2;
	// System.Single GameAnalyticsSDK.GA_SpecialEvents::_lastUpdateAvg
	float ____lastUpdateAvg_3;
	// System.Int32 GameAnalyticsSDK.GA_SpecialEvents::_criticalFpsCount
	int32_t ____criticalFpsCount_6;

public:
	inline static int32_t get_offset_of__frameCountAvg_2() { return static_cast<int32_t>(offsetof(GA_SpecialEvents_t4239990159_StaticFields, ____frameCountAvg_2)); }
	inline int32_t get__frameCountAvg_2() const { return ____frameCountAvg_2; }
	inline int32_t* get_address_of__frameCountAvg_2() { return &____frameCountAvg_2; }
	inline void set__frameCountAvg_2(int32_t value)
	{
		____frameCountAvg_2 = value;
	}

	inline static int32_t get_offset_of__lastUpdateAvg_3() { return static_cast<int32_t>(offsetof(GA_SpecialEvents_t4239990159_StaticFields, ____lastUpdateAvg_3)); }
	inline float get__lastUpdateAvg_3() const { return ____lastUpdateAvg_3; }
	inline float* get_address_of__lastUpdateAvg_3() { return &____lastUpdateAvg_3; }
	inline void set__lastUpdateAvg_3(float value)
	{
		____lastUpdateAvg_3 = value;
	}

	inline static int32_t get_offset_of__criticalFpsCount_6() { return static_cast<int32_t>(offsetof(GA_SpecialEvents_t4239990159_StaticFields, ____criticalFpsCount_6)); }
	inline int32_t get__criticalFpsCount_6() const { return ____criticalFpsCount_6; }
	inline int32_t* get_address_of__criticalFpsCount_6() { return &____criticalFpsCount_6; }
	inline void set__criticalFpsCount_6(int32_t value)
	{
		____criticalFpsCount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
