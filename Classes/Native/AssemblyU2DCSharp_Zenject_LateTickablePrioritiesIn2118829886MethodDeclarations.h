﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.LateTickablePrioritiesInstaller
struct LateTickablePrioritiesInstaller_t2118829886;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.LateTickablePrioritiesInstaller::.ctor(System.Collections.Generic.List`1<System.Type>)
extern "C"  void LateTickablePrioritiesInstaller__ctor_m3666721342 (LateTickablePrioritiesInstaller_t2118829886 * __this, List_1_t3576188904 * ___tickables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.LateTickablePrioritiesInstaller::InstallBindings()
extern "C"  void LateTickablePrioritiesInstaller_InstallBindings_m4126418824 (LateTickablePrioritiesInstaller_t2118829886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.LateTickablePrioritiesInstaller::BindPriority(Zenject.DiContainer,System.Type,System.Int32)
extern "C"  void LateTickablePrioritiesInstaller_BindPriority_m2076762739 (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Type_t * ___tickableType1, int32_t ___priorityCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
