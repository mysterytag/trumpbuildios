﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.TitleNewsItem
struct TitleNewsItem_t2523316974;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.TitleNewsItem::.ctor()
extern "C"  void TitleNewsItem__ctor_m3941641723 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayFab.ClientModels.TitleNewsItem::get_Timestamp()
extern "C"  DateTime_t339033936  TitleNewsItem_get_Timestamp_m4192668109 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TitleNewsItem::set_Timestamp(System.DateTime)
extern "C"  void TitleNewsItem_set_Timestamp_m1497876902 (TitleNewsItem_t2523316974 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.TitleNewsItem::get_NewsId()
extern "C"  String_t* TitleNewsItem_get_NewsId_m2988503203 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TitleNewsItem::set_NewsId(System.String)
extern "C"  void TitleNewsItem_set_NewsId_m2547245710 (TitleNewsItem_t2523316974 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.TitleNewsItem::get_Title()
extern "C"  String_t* TitleNewsItem_get_Title_m3593325317 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TitleNewsItem::set_Title(System.String)
extern "C"  void TitleNewsItem_set_Title_m412461806 (TitleNewsItem_t2523316974 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.TitleNewsItem::get_Body()
extern "C"  String_t* TitleNewsItem_get_Body_m1129684727 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.TitleNewsItem::set_Body(System.String)
extern "C"  void TitleNewsItem_set_Body_m3046810746 (TitleNewsItem_t2523316974 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
