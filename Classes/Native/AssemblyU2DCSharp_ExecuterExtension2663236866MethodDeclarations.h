﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IProcessExecution
struct IProcessExecution_t2362207186;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3012272455;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// IProcessExecution ExecuterExtension::Execute(UnityEngine.MonoBehaviour,System.Collections.IEnumerator)
extern "C"  Il2CppObject * ExecuterExtension_Execute_m3617475999 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t3012272455 * ___obj0, Il2CppObject * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
