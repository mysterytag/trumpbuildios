﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ResourceProvider
struct ResourceProvider_t2556536756;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.ResourceProvider::.ctor(System.Type,System.String)
extern "C"  void ResourceProvider__ctor_m916117144 (ResourceProvider_t2556536756 * __this, Type_t * ___concreteType0, String_t* ___resourcePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.ResourceProvider::GetInstanceType()
extern "C"  Type_t * ResourceProvider_GetInstanceType_m670444518 (ResourceProvider_t2556536756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.ResourceProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * ResourceProvider_GetInstance_m286090146 (ResourceProvider_t2556536756 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.ResourceProvider::GetTypeToInstantiate(System.Type)
extern "C"  Type_t * ResourceProvider_GetTypeToInstantiate_m4132456153 (ResourceProvider_t2556536756 * __this, Type_t * ___contractType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.ResourceProvider::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* ResourceProvider_ValidateBinding_m2282149832 (ResourceProvider_t2556536756 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
