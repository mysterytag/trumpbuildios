﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>
struct ObserveOnObservable_1_t3243387575;
// System.Collections.Generic.LinkedList`1<System.IDisposable>
struct LinkedList_1_t3369050920;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>
struct  ObserveOn_t679350430  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.ObserveOnObservable`1<T> UniRx.Operators.ObserveOnObservable`1/ObserveOn::parent
	ObserveOnObservable_1_t3243387575 * ___parent_2;
	// System.Collections.Generic.LinkedList`1<System.IDisposable> UniRx.Operators.ObserveOnObservable`1/ObserveOn::scheduleDisposables
	LinkedList_1_t3369050920 * ___scheduleDisposables_3;
	// System.Boolean UniRx.Operators.ObserveOnObservable`1/ObserveOn::isDisposed
	bool ___isDisposed_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ObserveOn_t679350430, ___parent_2)); }
	inline ObserveOnObservable_1_t3243387575 * get_parent_2() const { return ___parent_2; }
	inline ObserveOnObservable_1_t3243387575 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ObserveOnObservable_1_t3243387575 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_scheduleDisposables_3() { return static_cast<int32_t>(offsetof(ObserveOn_t679350430, ___scheduleDisposables_3)); }
	inline LinkedList_1_t3369050920 * get_scheduleDisposables_3() const { return ___scheduleDisposables_3; }
	inline LinkedList_1_t3369050920 ** get_address_of_scheduleDisposables_3() { return &___scheduleDisposables_3; }
	inline void set_scheduleDisposables_3(LinkedList_1_t3369050920 * value)
	{
		___scheduleDisposables_3 = value;
		Il2CppCodeGenWriteBarrier(&___scheduleDisposables_3, value);
	}

	inline static int32_t get_offset_of_isDisposed_4() { return static_cast<int32_t>(offsetof(ObserveOn_t679350430, ___isDisposed_4)); }
	inline bool get_isDisposed_4() const { return ___isDisposed_4; }
	inline bool* get_address_of_isDisposed_4() { return &___isDisposed_4; }
	inline void set_isDisposed_4(bool value)
	{
		___isDisposed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
