﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoMark
struct GoMark_t2137571317;

#include "codegen/il2cpp-codegen.h"

// System.Void GoMark::.ctor()
extern "C"  void GoMark__ctor_m932104262 (GoMark_t2137571317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoMark::Start()
extern "C"  void GoMark_Start_m4174209350 (GoMark_t2137571317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoMark::Update()
extern "C"  void GoMark_Update_m557323143 (GoMark_t2137571317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
