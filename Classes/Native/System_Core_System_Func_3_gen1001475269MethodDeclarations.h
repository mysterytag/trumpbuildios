﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen2236225891MethodDeclarations.h"

// System.Void System.Func`3<System.DateTime,System.Int64,<>__AnonType5`2<System.DateTime,System.Int64>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m3094572922(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t1001475269 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m235171373_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.DateTime,System.Int64,<>__AnonType5`2<System.DateTime,System.Int64>>::Invoke(T1,T2)
#define Func_3_Invoke_m1772425507(__this, ___arg10, ___arg21, method) ((  U3CU3E__AnonType5_2_t3897323094 * (*) (Func_3_t1001475269 *, DateTime_t339033936 , int64_t, const MethodInfo*))Func_3_Invoke_m2850141056_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.DateTime,System.Int64,<>__AnonType5`2<System.DateTime,System.Int64>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m306697512(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t1001475269 *, DateTime_t339033936 , int64_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3110739589_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.DateTime,System.Int64,<>__AnonType5`2<System.DateTime,System.Int64>>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m1032753176(__this, ___result0, method) ((  U3CU3E__AnonType5_2_t3897323094 * (*) (Func_3_t1001475269 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m3068744539_gshared)(__this, ___result0, method)
