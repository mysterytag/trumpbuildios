﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestCoro
struct TestCoro_t3212325755;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;

#include "codegen/il2cpp-codegen.h"

// System.Void TestCoro::.ctor()
extern "C"  void TestCoro__ctor_m819676544 (TestCoro_t3212325755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TestCoro::Test()
extern "C"  Il2CppObject * TestCoro_Test_m580259358 (TestCoro_t3212325755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro::Start()
extern "C"  void TestCoro_Start_m4061781632 (TestCoro_t3212325755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Int32> TestCoro::Test2()
extern "C"  Il2CppObject* TestCoro_Test2_m66563747 (TestCoro_t3212325755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TestCoro::Bearutine()
extern "C"  Il2CppObject * TestCoro_Bearutine_m3703818563 (TestCoro_t3212325755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TestCoro::Bearutine2()
extern "C"  Il2CppObject * TestCoro_Bearutine2_m3149235377 (TestCoro_t3212325755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro::Update()
extern "C"  void TestCoro_Update_m1367031181 (TestCoro_t3212325755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
