﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableRectTransformTrigger
struct  ObservableRectTransformTrigger_t3969084276  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableRectTransformTrigger::onRectTransformDimensionsChange
	Subject_1_t201353362 * ___onRectTransformDimensionsChange_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableRectTransformTrigger::onRectTransformRemoved
	Subject_1_t201353362 * ___onRectTransformRemoved_9;

public:
	inline static int32_t get_offset_of_onRectTransformDimensionsChange_8() { return static_cast<int32_t>(offsetof(ObservableRectTransformTrigger_t3969084276, ___onRectTransformDimensionsChange_8)); }
	inline Subject_1_t201353362 * get_onRectTransformDimensionsChange_8() const { return ___onRectTransformDimensionsChange_8; }
	inline Subject_1_t201353362 ** get_address_of_onRectTransformDimensionsChange_8() { return &___onRectTransformDimensionsChange_8; }
	inline void set_onRectTransformDimensionsChange_8(Subject_1_t201353362 * value)
	{
		___onRectTransformDimensionsChange_8 = value;
		Il2CppCodeGenWriteBarrier(&___onRectTransformDimensionsChange_8, value);
	}

	inline static int32_t get_offset_of_onRectTransformRemoved_9() { return static_cast<int32_t>(offsetof(ObservableRectTransformTrigger_t3969084276, ___onRectTransformRemoved_9)); }
	inline Subject_1_t201353362 * get_onRectTransformRemoved_9() const { return ___onRectTransformRemoved_9; }
	inline Subject_1_t201353362 ** get_address_of_onRectTransformRemoved_9() { return &___onRectTransformRemoved_9; }
	inline void set_onRectTransformRemoved_9(Subject_1_t201353362 * value)
	{
		___onRectTransformRemoved_9 = value;
		Il2CppCodeGenWriteBarrier(&___onRectTransformRemoved_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
