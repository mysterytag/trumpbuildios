﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType7`2<System.Object,System.Object>
struct U3CU3E__AnonType7_2_t1682020602;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType7`2<System.Object,System.Object>::.ctor(<reward>__T,<loggedIn>__T)
extern "C"  void U3CU3E__AnonType7_2__ctor_m4130492530_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, Il2CppObject * ___reward0, Il2CppObject * ___loggedIn1, const MethodInfo* method);
#define U3CU3E__AnonType7_2__ctor_m4130492530(__this, ___reward0, ___loggedIn1, method) ((  void (*) (U3CU3E__AnonType7_2_t1682020602 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType7_2__ctor_m4130492530_gshared)(__this, ___reward0, ___loggedIn1, method)
// <reward>__T <>__AnonType7`2<System.Object,System.Object>::get_reward()
extern "C"  Il2CppObject * U3CU3E__AnonType7_2_get_reward_m3189197276_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, const MethodInfo* method);
#define U3CU3E__AnonType7_2_get_reward_m3189197276(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType7_2_t1682020602 *, const MethodInfo*))U3CU3E__AnonType7_2_get_reward_m3189197276_gshared)(__this, method)
// <loggedIn>__T <>__AnonType7`2<System.Object,System.Object>::get_loggedIn()
extern "C"  Il2CppObject * U3CU3E__AnonType7_2_get_loggedIn_m824266780_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, const MethodInfo* method);
#define U3CU3E__AnonType7_2_get_loggedIn_m824266780(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType7_2_t1682020602 *, const MethodInfo*))U3CU3E__AnonType7_2_get_loggedIn_m824266780_gshared)(__this, method)
// System.Boolean <>__AnonType7`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType7_2_Equals_m408721039_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType7_2_Equals_m408721039(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType7_2_t1682020602 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType7_2_Equals_m408721039_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType7`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType7_2_GetHashCode_m1877289907_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, const MethodInfo* method);
#define U3CU3E__AnonType7_2_GetHashCode_m1877289907(__this, method) ((  int32_t (*) (U3CU3E__AnonType7_2_t1682020602 *, const MethodInfo*))U3CU3E__AnonType7_2_GetHashCode_m1877289907_gshared)(__this, method)
// System.String <>__AnonType7`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType7_2_ToString_m594560961_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, const MethodInfo* method);
#define U3CU3E__AnonType7_2_ToString_m594560961(__this, method) ((  String_t* (*) (U3CU3E__AnonType7_2_t1682020602 *, const MethodInfo*))U3CU3E__AnonType7_2_ToString_m594560961_gshared)(__this, method)
