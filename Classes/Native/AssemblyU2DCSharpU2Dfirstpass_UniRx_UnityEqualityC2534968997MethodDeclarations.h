﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEqualityComparer/QuaternionEqualityComparer
struct QuaternionEqualityComparer_t2534968997;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void UniRx.UnityEqualityComparer/QuaternionEqualityComparer::.ctor()
extern "C"  void QuaternionEqualityComparer__ctor_m3981203135 (QuaternionEqualityComparer_t2534968997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.UnityEqualityComparer/QuaternionEqualityComparer::Equals(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool QuaternionEqualityComparer_Equals_m948492390 (QuaternionEqualityComparer_t2534968997 * __this, Quaternion_t1891715979  ___self0, Quaternion_t1891715979  ___vector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.UnityEqualityComparer/QuaternionEqualityComparer::GetHashCode(UnityEngine.Quaternion)
extern "C"  int32_t QuaternionEqualityComparer_GetHashCode_m2348678595 (QuaternionEqualityComparer_t2534968997 * __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
