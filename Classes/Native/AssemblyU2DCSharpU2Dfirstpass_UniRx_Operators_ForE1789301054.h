﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ForEachAsyncObservable`1<System.Object>
struct ForEachAsyncObservable_1_t3176723348;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>
struct  ForEachAsync__t1789301054  : public OperatorObserverBase_2_t2908947767
{
public:
	// UniRx.Operators.ForEachAsyncObservable`1<T> UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_::parent
	ForEachAsyncObservable_1_t3176723348 * ___parent_2;
	// System.Int32 UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ForEachAsync__t1789301054, ___parent_2)); }
	inline ForEachAsyncObservable_1_t3176723348 * get_parent_2() const { return ___parent_2; }
	inline ForEachAsyncObservable_1_t3176723348 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ForEachAsyncObservable_1_t3176723348 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(ForEachAsync__t1789301054, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
