﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t792326996;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleWaitAnimation
struct  BattleWaitAnimation_t3210678167  : public Il2CppObject
{
public:
	// UnityEngine.Animator BattleWaitAnimation::_animator
	Animator_t792326996 * ____animator_0;
	// System.Boolean BattleWaitAnimation::waitAnimation
	bool ___waitAnimation_1;
	// System.Single BattleWaitAnimation::time
	float ___time_2;
	// System.Int32 BattleWaitAnimation::hash
	int32_t ___hash_3;

public:
	inline static int32_t get_offset_of__animator_0() { return static_cast<int32_t>(offsetof(BattleWaitAnimation_t3210678167, ____animator_0)); }
	inline Animator_t792326996 * get__animator_0() const { return ____animator_0; }
	inline Animator_t792326996 ** get_address_of__animator_0() { return &____animator_0; }
	inline void set__animator_0(Animator_t792326996 * value)
	{
		____animator_0 = value;
		Il2CppCodeGenWriteBarrier(&____animator_0, value);
	}

	inline static int32_t get_offset_of_waitAnimation_1() { return static_cast<int32_t>(offsetof(BattleWaitAnimation_t3210678167, ___waitAnimation_1)); }
	inline bool get_waitAnimation_1() const { return ___waitAnimation_1; }
	inline bool* get_address_of_waitAnimation_1() { return &___waitAnimation_1; }
	inline void set_waitAnimation_1(bool value)
	{
		___waitAnimation_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(BattleWaitAnimation_t3210678167, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(BattleWaitAnimation_t3210678167, ___hash_3)); }
	inline int32_t get_hash_3() const { return ___hash_3; }
	inline int32_t* get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(int32_t value)
	{
		___hash_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
