﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.TypeDescriptor/DefaultTypeDescriptionProvider
struct DefaultTypeDescriptionProvider_t1713221906;
// System.ComponentModel.ICustomTypeDescriptor
struct ICustomTypeDescriptor_t1558278316;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void System.ComponentModel.TypeDescriptor/DefaultTypeDescriptionProvider::.ctor()
extern "C"  void DefaultTypeDescriptionProvider__ctor_m3636893909 (DefaultTypeDescriptionProvider_t1713221906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.ICustomTypeDescriptor System.ComponentModel.TypeDescriptor/DefaultTypeDescriptionProvider::GetExtendedTypeDescriptor(System.Object)
extern "C"  Il2CppObject * DefaultTypeDescriptionProvider_GetExtendedTypeDescriptor_m1321153302 (DefaultTypeDescriptionProvider_t1713221906 * __this, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.ICustomTypeDescriptor System.ComponentModel.TypeDescriptor/DefaultTypeDescriptionProvider::GetTypeDescriptor(System.Type,System.Object)
extern "C"  Il2CppObject * DefaultTypeDescriptionProvider_GetTypeDescriptor_m1600532290 (DefaultTypeDescriptionProvider_t1713221906 * __this, Type_t * ___objectType0, Il2CppObject * ___instance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
