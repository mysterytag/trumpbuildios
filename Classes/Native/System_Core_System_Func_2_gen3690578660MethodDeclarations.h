﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3392211982MethodDeclarations.h"

// System.Void System.Func`2<UniRx.Unit,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3853806675(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3690578660 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2719813104_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UniRx.Unit,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>::Invoke(T)
#define Func_2_Invoke_m2262191407(__this, ___arg10, method) ((  Il2CppObject* (*) (Func_2_t3690578660 *, Unit_t2558286038 , const MethodInfo*))Func_2_Invoke_m2321944950_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UniRx.Unit,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m748761950(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3690578660 *, Unit_t2558286038 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3501888937_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UniRx.Unit,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1701665285(__this, ___result0, method) ((  Il2CppObject* (*) (Func_2_t3690578660 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m859232286_gshared)(__this, ___result0, method)
