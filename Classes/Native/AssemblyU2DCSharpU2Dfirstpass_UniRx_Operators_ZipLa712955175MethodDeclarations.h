﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_5_t712955175;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipLatestFunc_5__ctor_m1315956637_gshared (ZipLatestFunc_5_t712955175 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ZipLatestFunc_5__ctor_m1315956637(__this, ___object0, ___method1, method) ((  void (*) (ZipLatestFunc_5_t712955175 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ZipLatestFunc_5__ctor_m1315956637_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * ZipLatestFunc_5_Invoke_m2240407974_gshared (ZipLatestFunc_5_t712955175 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
#define ZipLatestFunc_5_Invoke_m2240407974(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  Il2CppObject * (*) (ZipLatestFunc_5_t712955175 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_5_Invoke_m2240407974_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipLatestFunc_5_BeginInvoke_m3689622318_gshared (ZipLatestFunc_5_t712955175 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method);
#define ZipLatestFunc_5_BeginInvoke_m3689622318(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (ZipLatestFunc_5_t712955175 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_5_BeginInvoke_m3689622318_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TR UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ZipLatestFunc_5_EndInvoke_m3457834388_gshared (ZipLatestFunc_5_t712955175 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ZipLatestFunc_5_EndInvoke_m3457834388(__this, ___result0, method) ((  Il2CppObject * (*) (ZipLatestFunc_5_t712955175 *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_5_EndInvoke_m3457834388_gshared)(__this, ___result0, method)
