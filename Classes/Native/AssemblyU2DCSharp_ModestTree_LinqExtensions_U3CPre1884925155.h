﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.LinqExtensions/<Prepend>c__Iterator37`1<System.Object>
struct  U3CPrependU3Ec__Iterator37_1_t1884925155  : public Il2CppObject
{
public:
	// T ModestTree.LinqExtensions/<Prepend>c__Iterator37`1::item
	Il2CppObject * ___item_0;
	// System.Collections.Generic.IEnumerable`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator37`1::first
	Il2CppObject* ___first_1;
	// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator37`1::<$s_235>__0
	Il2CppObject* ___U3CU24s_235U3E__0_2;
	// T ModestTree.LinqExtensions/<Prepend>c__Iterator37`1::<t>__1
	Il2CppObject * ___U3CtU3E__1_3;
	// System.Int32 ModestTree.LinqExtensions/<Prepend>c__Iterator37`1::$PC
	int32_t ___U24PC_4;
	// T ModestTree.LinqExtensions/<Prepend>c__Iterator37`1::$current
	Il2CppObject * ___U24current_5;
	// T ModestTree.LinqExtensions/<Prepend>c__Iterator37`1::<$>item
	Il2CppObject * ___U3CU24U3Eitem_6;
	// System.Collections.Generic.IEnumerable`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator37`1::<$>first
	Il2CppObject* ___U3CU24U3Efirst_7;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator37_1_t1884925155, ___item_0)); }
	inline Il2CppObject * get_item_0() const { return ___item_0; }
	inline Il2CppObject ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Il2CppObject * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier(&___item_0, value);
	}

	inline static int32_t get_offset_of_first_1() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator37_1_t1884925155, ___first_1)); }
	inline Il2CppObject* get_first_1() const { return ___first_1; }
	inline Il2CppObject** get_address_of_first_1() { return &___first_1; }
	inline void set_first_1(Il2CppObject* value)
	{
		___first_1 = value;
		Il2CppCodeGenWriteBarrier(&___first_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_235U3E__0_2() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator37_1_t1884925155, ___U3CU24s_235U3E__0_2)); }
	inline Il2CppObject* get_U3CU24s_235U3E__0_2() const { return ___U3CU24s_235U3E__0_2; }
	inline Il2CppObject** get_address_of_U3CU24s_235U3E__0_2() { return &___U3CU24s_235U3E__0_2; }
	inline void set_U3CU24s_235U3E__0_2(Il2CppObject* value)
	{
		___U3CU24s_235U3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_235U3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CtU3E__1_3() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator37_1_t1884925155, ___U3CtU3E__1_3)); }
	inline Il2CppObject * get_U3CtU3E__1_3() const { return ___U3CtU3E__1_3; }
	inline Il2CppObject ** get_address_of_U3CtU3E__1_3() { return &___U3CtU3E__1_3; }
	inline void set_U3CtU3E__1_3(Il2CppObject * value)
	{
		___U3CtU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator37_1_t1884925155, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator37_1_t1884925155, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eitem_6() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator37_1_t1884925155, ___U3CU24U3Eitem_6)); }
	inline Il2CppObject * get_U3CU24U3Eitem_6() const { return ___U3CU24U3Eitem_6; }
	inline Il2CppObject ** get_address_of_U3CU24U3Eitem_6() { return &___U3CU24U3Eitem_6; }
	inline void set_U3CU24U3Eitem_6(Il2CppObject * value)
	{
		___U3CU24U3Eitem_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eitem_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efirst_7() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator37_1_t1884925155, ___U3CU24U3Efirst_7)); }
	inline Il2CppObject* get_U3CU24U3Efirst_7() const { return ___U3CU24U3Efirst_7; }
	inline Il2CppObject** get_address_of_U3CU24U3Efirst_7() { return &___U3CU24U3Efirst_7; }
	inline void set_U3CU24U3Efirst_7(Il2CppObject* value)
	{
		___U3CU24U3Efirst_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efirst_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
