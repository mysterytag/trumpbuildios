﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String DoubleExtensions::ToFortranDouble(System.Double)
extern "C"  String_t* DoubleExtensions_ToFortranDouble_m2882598471 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DoubleExtensions::ToFortranDouble(System.Double,System.Int32)
extern "C"  String_t* DoubleExtensions_ToFortranDouble_m3113538032 (Il2CppObject * __this /* static, unused */, double ___value0, int32_t ___precision1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
