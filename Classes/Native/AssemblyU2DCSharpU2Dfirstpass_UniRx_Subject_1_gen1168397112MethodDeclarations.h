﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.Vector2>
struct Subject_1_t1168397112;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.Subject`1<UnityEngine.Vector2>::.ctor()
extern "C"  void Subject_1__ctor_m3680169138_gshared (Subject_1_t1168397112 * __this, const MethodInfo* method);
#define Subject_1__ctor_m3680169138(__this, method) ((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))Subject_1__ctor_m3680169138_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Vector2>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2397862042_gshared (Subject_1_t1168397112 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m2397862042(__this, method) ((  bool (*) (Subject_1_t1168397112 *, const MethodInfo*))Subject_1_get_HasObservers_m2397862042_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector2>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3883553468_gshared (Subject_1_t1168397112 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3883553468(__this, method) ((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))Subject_1_OnCompleted_m3883553468_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m3178416617_gshared (Subject_1_t1168397112 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m3178416617(__this, ___error0, method) ((  void (*) (Subject_1_t1168397112 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m3178416617_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector2>::OnNext(T)
extern "C"  void Subject_1_OnNext_m420192986_gshared (Subject_1_t1168397112 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m420192986(__this, ___value0, method) ((  void (*) (Subject_1_t1168397112 *, Vector2_t3525329788 , const MethodInfo*))Subject_1_OnNext_m420192986_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Vector2>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m442634481_gshared (Subject_1_t1168397112 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m442634481(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1168397112 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m442634481_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector2>::Dispose()
extern "C"  void Subject_1_Dispose_m1782740079_gshared (Subject_1_t1168397112 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1782740079(__this, method) ((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))Subject_1_Dispose_m1782740079_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector2>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1666727672_gshared (Subject_1_t1168397112 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1666727672(__this, method) ((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1666727672_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Vector2>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m2536092433_gshared (Subject_1_t1168397112 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2536092433(__this, method) ((  bool (*) (Subject_1_t1168397112 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m2536092433_gshared)(__this, method)
