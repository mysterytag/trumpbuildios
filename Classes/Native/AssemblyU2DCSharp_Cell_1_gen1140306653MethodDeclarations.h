﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1<System.Single>
struct Cell_1_t1140306653;
// ICell`1<System.Single>
struct ICell_1_t2509839998;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// IStream`1<System.Single>
struct IStream_1_t1510900012;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"

// System.Void Cell`1<System.Single>::.ctor()
extern "C"  void Cell_1__ctor_m1359945515_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method);
#define Cell_1__ctor_m1359945515(__this, method) ((  void (*) (Cell_1_t1140306653 *, const MethodInfo*))Cell_1__ctor_m1359945515_gshared)(__this, method)
// System.Void Cell`1<System.Single>::.ctor(T)
extern "C"  void Cell_1__ctor_m3503606675_gshared (Cell_1_t1140306653 * __this, float ___initial0, const MethodInfo* method);
#define Cell_1__ctor_m3503606675(__this, ___initial0, method) ((  void (*) (Cell_1_t1140306653 *, float, const MethodInfo*))Cell_1__ctor_m3503606675_gshared)(__this, ___initial0, method)
// System.Void Cell`1<System.Single>::.ctor(ICell`1<T>)
extern "C"  void Cell_1__ctor_m334321185_gshared (Cell_1_t1140306653 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define Cell_1__ctor_m334321185(__this, ___other0, method) ((  void (*) (Cell_1_t1140306653 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m334321185_gshared)(__this, ___other0, method)
// System.Void Cell`1<System.Single>::SetInputCell(ICell`1<T>)
extern "C"  void Cell_1_SetInputCell_m1221539129_gshared (Cell_1_t1140306653 * __this, Il2CppObject* ___cell0, const MethodInfo* method);
#define Cell_1_SetInputCell_m1221539129(__this, ___cell0, method) ((  void (*) (Cell_1_t1140306653 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m1221539129_gshared)(__this, ___cell0, method)
// T Cell`1<System.Single>::get_value()
extern "C"  float Cell_1_get_value_m3534945648_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method);
#define Cell_1_get_value_m3534945648(__this, method) ((  float (*) (Cell_1_t1140306653 *, const MethodInfo*))Cell_1_get_value_m3534945648_gshared)(__this, method)
// System.Void Cell`1<System.Single>::set_value(T)
extern "C"  void Cell_1_set_value_m135039553_gshared (Cell_1_t1140306653 * __this, float ___value0, const MethodInfo* method);
#define Cell_1_set_value_m135039553(__this, ___value0, method) ((  void (*) (Cell_1_t1140306653 *, float, const MethodInfo*))Cell_1_set_value_m135039553_gshared)(__this, ___value0, method)
// System.Void Cell`1<System.Single>::Set(T)
extern "C"  void Cell_1_Set_m2351851667_gshared (Cell_1_t1140306653 * __this, float ___val0, const MethodInfo* method);
#define Cell_1_Set_m2351851667(__this, ___val0, method) ((  void (*) (Cell_1_t1140306653 *, float, const MethodInfo*))Cell_1_Set_m2351851667_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<System.Single>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m183215894_gshared (Cell_1_t1140306653 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_OnChanged_m183215894(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1140306653 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m183215894_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<System.Single>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m3338773765_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method);
#define Cell_1_get_valueObject_m3338773765(__this, method) ((  Il2CppObject * (*) (Cell_1_t1140306653 *, const MethodInfo*))Cell_1_get_valueObject_m3338773765_gshared)(__this, method)
// System.IDisposable Cell`1<System.Single>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m987445891_gshared (Cell_1_t1140306653 * __this, Action_1_t1106661726 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_ListenUpdates_m987445891(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1140306653 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m987445891_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<System.Single>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m170818459_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method);
#define Cell_1_get_updates_m170818459(__this, method) ((  Il2CppObject* (*) (Cell_1_t1140306653 *, const MethodInfo*))Cell_1_get_updates_m170818459_gshared)(__this, method)
// System.IDisposable Cell`1<System.Single>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m3909280863_gshared (Cell_1_t1140306653 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_Bind_m3909280863(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1140306653 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))Cell_1_Bind_m3909280863_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<System.Single>::SetInputStream(IStream`1<T>)
extern "C"  void Cell_1_SetInputStream_m3007441333_gshared (Cell_1_t1140306653 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Cell_1_SetInputStream_m3007441333(__this, ___stream0, method) ((  void (*) (Cell_1_t1140306653 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m3007441333_gshared)(__this, ___stream0, method)
// System.Void Cell`1<System.Single>::ResetInputStream()
extern "C"  void Cell_1_ResetInputStream_m1809700116_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method);
#define Cell_1_ResetInputStream_m1809700116(__this, method) ((  void (*) (Cell_1_t1140306653 *, const MethodInfo*))Cell_1_ResetInputStream_m1809700116_gshared)(__this, method)
// System.Void Cell`1<System.Single>::TransactionIterationFinished()
extern "C"  void Cell_1_TransactionIterationFinished_m1930590122_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method);
#define Cell_1_TransactionIterationFinished_m1930590122(__this, method) ((  void (*) (Cell_1_t1140306653 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m1930590122_gshared)(__this, method)
// System.Void Cell`1<System.Single>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m1139872572_gshared (Cell_1_t1140306653 * __this, int32_t ___p0, const MethodInfo* method);
#define Cell_1_Unpack_m1139872572(__this, ___p0, method) ((  void (*) (Cell_1_t1140306653 *, int32_t, const MethodInfo*))Cell_1_Unpack_m1139872572_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<System.Single>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m713162653_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t1140306653 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_LessThanOrEqual_m713162653(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t1140306653 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m713162653_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<System.Single>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m3002547658_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t1140306653 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_GreaterThanOrEqual_m3002547658(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t1140306653 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m3002547658_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
