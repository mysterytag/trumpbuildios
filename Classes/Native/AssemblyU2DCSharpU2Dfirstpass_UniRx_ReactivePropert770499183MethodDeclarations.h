﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Double>
struct ReactivePropertyObserver_t770499183;
// UniRx.ReactiveProperty`1<System.Double>
struct ReactiveProperty_1_t1142849652;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Double>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m3251298178_gshared (ReactivePropertyObserver_t770499183 * __this, ReactiveProperty_1_t1142849652 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m3251298178(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t770499183 *, ReactiveProperty_1_t1142849652 *, const MethodInfo*))ReactivePropertyObserver__ctor_m3251298178_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Double>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m438504425_gshared (ReactivePropertyObserver_t770499183 * __this, double ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m438504425(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t770499183 *, double, const MethodInfo*))ReactivePropertyObserver_OnNext_m438504425_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Double>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m378287352_gshared (ReactivePropertyObserver_t770499183 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m378287352(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t770499183 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m378287352_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Double>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m1300798539_gshared (ReactivePropertyObserver_t770499183 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m1300798539(__this, method) ((  void (*) (ReactivePropertyObserver_t770499183 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m1300798539_gshared)(__this, method)
