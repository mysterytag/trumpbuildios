﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OligarchParameters
struct OligarchParameters_t821144443;
// TopPanel
struct TopPanel_t3379186127;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GrapRotation
struct  GrapRotation_t3746846744  : public MonoBehaviour_t3012272455
{
public:
	// OligarchParameters GrapRotation::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_2;
	// TopPanel GrapRotation::TopPanel
	TopPanel_t3379186127 * ___TopPanel_3;

public:
	inline static int32_t get_offset_of_OligarchParameters_2() { return static_cast<int32_t>(offsetof(GrapRotation_t3746846744, ___OligarchParameters_2)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_2() const { return ___OligarchParameters_2; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_2() { return &___OligarchParameters_2; }
	inline void set_OligarchParameters_2(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_2 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_2, value);
	}

	inline static int32_t get_offset_of_TopPanel_3() { return static_cast<int32_t>(offsetof(GrapRotation_t3746846744, ___TopPanel_3)); }
	inline TopPanel_t3379186127 * get_TopPanel_3() const { return ___TopPanel_3; }
	inline TopPanel_t3379186127 ** get_address_of_TopPanel_3() { return &___TopPanel_3; }
	inline void set_TopPanel_3(TopPanel_t3379186127 * value)
	{
		___TopPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___TopPanel_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
