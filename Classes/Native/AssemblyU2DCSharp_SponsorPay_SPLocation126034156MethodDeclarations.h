﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPLocation
struct SPLocation_t126034156;

#include "codegen/il2cpp-codegen.h"

// System.Void SponsorPay.SPLocation::.ctor()
extern "C"  void SPLocation__ctor_m2801161689 (SPLocation_t126034156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double SponsorPay.SPLocation::get_Long()
extern "C"  double SPLocation_get_Long_m4279061965 (SPLocation_t126034156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPLocation::set_Long(System.Double)
extern "C"  void SPLocation_set_Long_m3152744574 (SPLocation_t126034156 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double SponsorPay.SPLocation::get_Lat()
extern "C"  double SPLocation_get_Lat_m553262992 (SPLocation_t126034156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SPLocation::set_Lat(System.Double)
extern "C"  void SPLocation_set_Lat_m3144218281 (SPLocation_t126034156 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
