﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.IDisposable>
struct List_1_t2425880343;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellUtils.ListDisposable
struct  ListDisposable_t2393830995  : public Il2CppObject
{
public:
	// System.Boolean CellUtils.ListDisposable::_disposed
	bool ____disposed_0;
	// System.Collections.Generic.List`1<System.IDisposable> CellUtils.ListDisposable::_disposables
	List_1_t2425880343 * ____disposables_1;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(ListDisposable_t2393830995, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__disposables_1() { return static_cast<int32_t>(offsetof(ListDisposable_t2393830995, ____disposables_1)); }
	inline List_1_t2425880343 * get__disposables_1() const { return ____disposables_1; }
	inline List_1_t2425880343 ** get_address_of__disposables_1() { return &____disposables_1; }
	inline void set__disposables_1(List_1_t2425880343 * value)
	{
		____disposables_1 = value;
		Il2CppCodeGenWriteBarrier(&____disposables_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
