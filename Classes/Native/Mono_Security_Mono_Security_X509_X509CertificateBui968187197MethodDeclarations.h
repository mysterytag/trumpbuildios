﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509CertificateBuilder
struct X509CertificateBuilder_t968187197;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String
struct String_t;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t4236534322;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t3528692651;
// Mono.Security.ASN1
struct ASN1_t1254135647;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAl4236534322.h"

// System.Void Mono.Security.X509.X509CertificateBuilder::.ctor()
extern "C"  void X509CertificateBuilder__ctor_m3387775039 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::.ctor(System.Byte)
extern "C"  void X509CertificateBuilder__ctor_m150520268 (X509CertificateBuilder_t968187197 * __this, uint8_t ___version0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.X509.X509CertificateBuilder::get_Version()
extern "C"  uint8_t X509CertificateBuilder_get_Version_m1273651352 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_Version(System.Byte)
extern "C"  void X509CertificateBuilder_set_Version_m1275548883 (X509CertificateBuilder_t968187197 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509CertificateBuilder::get_SerialNumber()
extern "C"  ByteU5BU5D_t58506160* X509CertificateBuilder_get_SerialNumber_m4288039009 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_SerialNumber(System.Byte[])
extern "C"  void X509CertificateBuilder_set_SerialNumber_m2505934442 (X509CertificateBuilder_t968187197 * __this, ByteU5BU5D_t58506160* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509CertificateBuilder::get_IssuerName()
extern "C"  String_t* X509CertificateBuilder_get_IssuerName_m1125195535 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_IssuerName(System.String)
extern "C"  void X509CertificateBuilder_set_IssuerName_m1568472060 (X509CertificateBuilder_t968187197 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509CertificateBuilder::get_NotBefore()
extern "C"  DateTime_t339033936  X509CertificateBuilder_get_NotBefore_m2833907647 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_NotBefore(System.DateTime)
extern "C"  void X509CertificateBuilder_set_NotBefore_m1453189958 (X509CertificateBuilder_t968187197 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509CertificateBuilder::get_NotAfter()
extern "C"  DateTime_t339033936  X509CertificateBuilder_get_NotAfter_m3124496638 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_NotAfter(System.DateTime)
extern "C"  void X509CertificateBuilder_set_NotAfter_m1953283373 (X509CertificateBuilder_t968187197 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509CertificateBuilder::get_SubjectName()
extern "C"  String_t* X509CertificateBuilder_get_SubjectName_m3223787566 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_SubjectName(System.String)
extern "C"  void X509CertificateBuilder_set_SubjectName_m2745297675 (X509CertificateBuilder_t968187197 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.X509.X509CertificateBuilder::get_SubjectPublicKey()
extern "C"  AsymmetricAlgorithm_t4236534322 * X509CertificateBuilder_get_SubjectPublicKey_m1787286503 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_SubjectPublicKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C"  void X509CertificateBuilder_set_SubjectPublicKey_m3787725510 (X509CertificateBuilder_t968187197 * __this, AsymmetricAlgorithm_t4236534322 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509CertificateBuilder::get_IssuerUniqueId()
extern "C"  ByteU5BU5D_t58506160* X509CertificateBuilder_get_IssuerUniqueId_m1851377129 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_IssuerUniqueId(System.Byte[])
extern "C"  void X509CertificateBuilder_set_IssuerUniqueId_m3280696674 (X509CertificateBuilder_t968187197 * __this, ByteU5BU5D_t58506160* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509CertificateBuilder::get_SubjectUniqueId()
extern "C"  ByteU5BU5D_t58506160* X509CertificateBuilder_get_SubjectUniqueId_m1126768118 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateBuilder::set_SubjectUniqueId(System.Byte[])
extern "C"  void X509CertificateBuilder_set_SubjectUniqueId_m2155103473 (X509CertificateBuilder_t968187197 * __this, ByteU5BU5D_t58506160* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509CertificateBuilder::get_Extensions()
extern "C"  X509ExtensionCollection_t3528692651 * X509CertificateBuilder_get_Extensions_m3435660317 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X509CertificateBuilder::SubjectPublicKeyInfo()
extern "C"  ASN1_t1254135647 * X509CertificateBuilder_SubjectPublicKeyInfo_m2718429982 (X509CertificateBuilder_t968187197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509CertificateBuilder::UniqueIdentifier(System.Byte[])
extern "C"  ByteU5BU5D_t58506160* X509CertificateBuilder_UniqueIdentifier_m165584852 (X509CertificateBuilder_t968187197 * __this, ByteU5BU5D_t58506160* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X509CertificateBuilder::ToBeSigned(System.String)
extern "C"  ASN1_t1254135647 * X509CertificateBuilder_ToBeSigned_m1539530882 (X509CertificateBuilder_t968187197 * __this, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
