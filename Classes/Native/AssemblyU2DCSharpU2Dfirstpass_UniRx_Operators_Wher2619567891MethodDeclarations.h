﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where_<UniRx.Unit>
struct Where__t2619567891;
// UniRx.Operators.WhereObservable`1<UniRx.Unit>
struct WhereObservable_1_t3756845987;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where_<UniRx.Unit>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where___ctor_m4230100072_gshared (Where__t2619567891 * __this, WhereObservable_1_t3756845987 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where___ctor_m4230100072(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where__t2619567891 *, WhereObservable_1_t3756845987 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where___ctor_m4230100072_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<UniRx.Unit>::OnNext(T)
extern "C"  void Where__OnNext_m225337742_gshared (Where__t2619567891 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Where__OnNext_m225337742(__this, ___value0, method) ((  void (*) (Where__t2619567891 *, Unit_t2558286038 , const MethodInfo*))Where__OnNext_m225337742_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Where__OnError_m2013896861_gshared (Where__t2619567891 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where__OnError_m2013896861(__this, ___error0, method) ((  void (*) (Where__t2619567891 *, Exception_t1967233988 *, const MethodInfo*))Where__OnError_m2013896861_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<UniRx.Unit>::OnCompleted()
extern "C"  void Where__OnCompleted_m1513527152_gshared (Where__t2619567891 * __this, const MethodInfo* method);
#define Where__OnCompleted_m1513527152(__this, method) ((  void (*) (Where__t2619567891 *, const MethodInfo*))Where__OnCompleted_m1513527152_gshared)(__this, method)
