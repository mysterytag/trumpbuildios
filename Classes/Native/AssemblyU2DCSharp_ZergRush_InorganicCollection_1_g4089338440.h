﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Stream`1<System.Int32>
struct Stream_1_t1538553809;
// Stream`1<UniRx.Unit>
struct Stream_1_t1249425060;
// Stream`1<UniRx.CollectionAddEvent`1<UpgradeButton>>
struct Stream_1_t1746021931;
// Stream`1<CollectionMoveEvent`1<UpgradeButton>>
struct Stream_1_t2901555325;
// Stream`1<CollectionRemoveEvent`1<UpgradeButton>>
struct Stream_1_t227759202;
// Stream`1<CollectionReplaceEvent`1<UpgradeButton>>
struct Stream_1_t1826872014;
// Stream`1<System.Collections.Generic.ICollection`1<UpgradeButton>>
struct Stream_1_t632437750;

#include "mscorlib_System_Collections_ObjectModel_Collection3444455072.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.InorganicCollection`1<UpgradeButton>
struct  InorganicCollection_1_t4089338440  : public Collection_1_t3444455072
{
public:
	// Stream`1<System.Int32> ZergRush.InorganicCollection`1::countChanged
	Stream_1_t1538553809 * ___countChanged_2;
	// Stream`1<UniRx.Unit> ZergRush.InorganicCollection`1::collectionReset
	Stream_1_t1249425060 * ___collectionReset_3;
	// Stream`1<UniRx.CollectionAddEvent`1<T>> ZergRush.InorganicCollection`1::collectionAdd
	Stream_1_t1746021931 * ___collectionAdd_4;
	// Stream`1<CollectionMoveEvent`1<T>> ZergRush.InorganicCollection`1::collectionMove
	Stream_1_t2901555325 * ___collectionMove_5;
	// Stream`1<CollectionRemoveEvent`1<T>> ZergRush.InorganicCollection`1::collectionRemove
	Stream_1_t227759202 * ___collectionRemove_6;
	// Stream`1<CollectionReplaceEvent`1<T>> ZergRush.InorganicCollection`1::collectionReplace
	Stream_1_t1826872014 * ___collectionReplace_7;
	// Stream`1<System.Collections.Generic.ICollection`1<T>> ZergRush.InorganicCollection`1::updates
	Stream_1_t632437750 * ___updates_8;

public:
	inline static int32_t get_offset_of_countChanged_2() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t4089338440, ___countChanged_2)); }
	inline Stream_1_t1538553809 * get_countChanged_2() const { return ___countChanged_2; }
	inline Stream_1_t1538553809 ** get_address_of_countChanged_2() { return &___countChanged_2; }
	inline void set_countChanged_2(Stream_1_t1538553809 * value)
	{
		___countChanged_2 = value;
		Il2CppCodeGenWriteBarrier(&___countChanged_2, value);
	}

	inline static int32_t get_offset_of_collectionReset_3() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t4089338440, ___collectionReset_3)); }
	inline Stream_1_t1249425060 * get_collectionReset_3() const { return ___collectionReset_3; }
	inline Stream_1_t1249425060 ** get_address_of_collectionReset_3() { return &___collectionReset_3; }
	inline void set_collectionReset_3(Stream_1_t1249425060 * value)
	{
		___collectionReset_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReset_3, value);
	}

	inline static int32_t get_offset_of_collectionAdd_4() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t4089338440, ___collectionAdd_4)); }
	inline Stream_1_t1746021931 * get_collectionAdd_4() const { return ___collectionAdd_4; }
	inline Stream_1_t1746021931 ** get_address_of_collectionAdd_4() { return &___collectionAdd_4; }
	inline void set_collectionAdd_4(Stream_1_t1746021931 * value)
	{
		___collectionAdd_4 = value;
		Il2CppCodeGenWriteBarrier(&___collectionAdd_4, value);
	}

	inline static int32_t get_offset_of_collectionMove_5() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t4089338440, ___collectionMove_5)); }
	inline Stream_1_t2901555325 * get_collectionMove_5() const { return ___collectionMove_5; }
	inline Stream_1_t2901555325 ** get_address_of_collectionMove_5() { return &___collectionMove_5; }
	inline void set_collectionMove_5(Stream_1_t2901555325 * value)
	{
		___collectionMove_5 = value;
		Il2CppCodeGenWriteBarrier(&___collectionMove_5, value);
	}

	inline static int32_t get_offset_of_collectionRemove_6() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t4089338440, ___collectionRemove_6)); }
	inline Stream_1_t227759202 * get_collectionRemove_6() const { return ___collectionRemove_6; }
	inline Stream_1_t227759202 ** get_address_of_collectionRemove_6() { return &___collectionRemove_6; }
	inline void set_collectionRemove_6(Stream_1_t227759202 * value)
	{
		___collectionRemove_6 = value;
		Il2CppCodeGenWriteBarrier(&___collectionRemove_6, value);
	}

	inline static int32_t get_offset_of_collectionReplace_7() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t4089338440, ___collectionReplace_7)); }
	inline Stream_1_t1826872014 * get_collectionReplace_7() const { return ___collectionReplace_7; }
	inline Stream_1_t1826872014 ** get_address_of_collectionReplace_7() { return &___collectionReplace_7; }
	inline void set_collectionReplace_7(Stream_1_t1826872014 * value)
	{
		___collectionReplace_7 = value;
		Il2CppCodeGenWriteBarrier(&___collectionReplace_7, value);
	}

	inline static int32_t get_offset_of_updates_8() { return static_cast<int32_t>(offsetof(InorganicCollection_1_t4089338440, ___updates_8)); }
	inline Stream_1_t632437750 * get_updates_8() const { return ___updates_8; }
	inline Stream_1_t632437750 ** get_address_of_updates_8() { return &___updates_8; }
	inline void set_updates_8(Stream_1_t632437750 * value)
	{
		___updates_8 = value;
		Il2CppCodeGenWriteBarrier(&___updates_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
