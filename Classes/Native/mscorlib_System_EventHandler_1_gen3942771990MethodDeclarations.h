﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_EventHandler_1_gen1679684063MethodDeclarations.h"

// System.Void System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m2372365329(__this, ___object0, ___method1, method) ((  void (*) (EventHandler_1_t3942771990 *, Il2CppObject *, IntPtr_t, const MethodInfo*))EventHandler_1__ctor_m1337593804_gshared)(__this, ___object0, ___method1, method)
// System.Void System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>::Invoke(System.Object,TEventArgs)
#define EventHandler_1_Invoke_m2154207886(__this, ___sender0, ___e1, method) ((  void (*) (EventHandler_1_t3942771990 *, Il2CppObject *, MyEventArgs_t3100194347 *, const MethodInfo*))EventHandler_1_Invoke_m2623239957_gshared)(__this, ___sender0, ___e1, method)
// System.IAsyncResult System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>::BeginInvoke(System.Object,TEventArgs,System.AsyncCallback,System.Object)
#define EventHandler_1_BeginInvoke_m1737601812(__this, ___sender0, ___e1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (EventHandler_1_t3942771990 *, Il2CppObject *, MyEventArgs_t3100194347 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))EventHandler_1_BeginInvoke_m996893970_gshared)(__this, ___sender0, ___e1, ___callback2, ___object3, method)
// System.Void System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>::EndInvoke(System.IAsyncResult)
#define EventHandler_1_EndInvoke_m4047915614(__this, ___result0, method) ((  void (*) (EventHandler_1_t3942771990 *, Il2CppObject *, const MethodInfo*))EventHandler_1_EndInvoke_m2479179740_gshared)(__this, ___result0, method)
