﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadDataCompletedEventArgs
struct UploadDataCompletedEventArgs_t737981633;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.UploadDataCompletedEventArgs::.ctor(System.Byte[],System.Exception,System.Boolean,System.Object)
extern "C"  void UploadDataCompletedEventArgs__ctor_m1892791291 (UploadDataCompletedEventArgs_t737981633 * __this, ByteU5BU5D_t58506160* ___result0, Exception_t1967233988 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.UploadDataCompletedEventArgs::get_Result()
extern "C"  ByteU5BU5D_t58506160* UploadDataCompletedEventArgs_get_Result_m2402603869 (UploadDataCompletedEventArgs_t737981633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
