﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P4248560733MethodDeclarations.h"

// System.Int32 UniRx.InternalUtil.PriorityQueue`1/IndexedItem<UniRx.InternalUtil.ScheduledItem>::CompareTo(UniRx.InternalUtil.PriorityQueue`1/IndexedItem<T>)
#define IndexedItem_CompareTo_m4288522182(__this, ___other0, method) ((  int32_t (*) (IndexedItem_t1029390262 *, IndexedItem_t1029390262 , const MethodInfo*))IndexedItem_CompareTo_m3674265725_gshared)(__this, ___other0, method)
