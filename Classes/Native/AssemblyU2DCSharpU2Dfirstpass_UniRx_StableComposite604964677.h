﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile)
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/Trinary
struct  Trinary_t604964677  : public StableCompositeDisposable_t3433635614
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/Trinary::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Trinary::_disposable1
	Il2CppObject * ____disposable1_1;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Trinary::_disposable2
	Il2CppObject * ____disposable2_2;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Trinary::_disposable3
	Il2CppObject * ____disposable3_3;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(Trinary_t604964677, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposable1_1() { return static_cast<int32_t>(offsetof(Trinary_t604964677, ____disposable1_1)); }
	inline Il2CppObject * get__disposable1_1() const { return ____disposable1_1; }
	inline Il2CppObject ** get_address_of__disposable1_1() { return &____disposable1_1; }
	inline void set__disposable1_1(Il2CppObject * value)
	{
		____disposable1_1 = value;
		Il2CppCodeGenWriteBarrier(&____disposable1_1, value);
	}

	inline static int32_t get_offset_of__disposable2_2() { return static_cast<int32_t>(offsetof(Trinary_t604964677, ____disposable2_2)); }
	inline Il2CppObject * get__disposable2_2() const { return ____disposable2_2; }
	inline Il2CppObject ** get_address_of__disposable2_2() { return &____disposable2_2; }
	inline void set__disposable2_2(Il2CppObject * value)
	{
		____disposable2_2 = value;
		Il2CppCodeGenWriteBarrier(&____disposable2_2, value);
	}

	inline static int32_t get_offset_of__disposable3_3() { return static_cast<int32_t>(offsetof(Trinary_t604964677, ____disposable3_3)); }
	inline Il2CppObject * get__disposable3_3() const { return ____disposable3_3; }
	inline Il2CppObject ** get_address_of__disposable3_3() { return &____disposable3_3; }
	inline void set__disposable3_3(Il2CppObject * value)
	{
		____disposable3_3 = value;
		Il2CppCodeGenWriteBarrier(&____disposable3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
