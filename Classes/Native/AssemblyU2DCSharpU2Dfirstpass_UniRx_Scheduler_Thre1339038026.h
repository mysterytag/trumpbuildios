﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.HashSet`1<System.Threading.Timer>
struct HashSet_1_t1949832359;
// System.Action
struct Action_t437523947;
// System.Threading.Timer
struct Timer_t3546110984;
// UniRx.InternalUtil.AsyncLock
struct AsyncLock_t4023816884;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer
struct  PeriodicTimer_t1339038026  : public Il2CppObject
{
public:
	// System.Action UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::_action
	Action_t437523947 * ____action_1;
	// System.Threading.Timer UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::_timer
	Timer_t3546110984 * ____timer_2;
	// UniRx.InternalUtil.AsyncLock UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::_gate
	AsyncLock_t4023816884 * ____gate_3;

public:
	inline static int32_t get_offset_of__action_1() { return static_cast<int32_t>(offsetof(PeriodicTimer_t1339038026, ____action_1)); }
	inline Action_t437523947 * get__action_1() const { return ____action_1; }
	inline Action_t437523947 ** get_address_of__action_1() { return &____action_1; }
	inline void set__action_1(Action_t437523947 * value)
	{
		____action_1 = value;
		Il2CppCodeGenWriteBarrier(&____action_1, value);
	}

	inline static int32_t get_offset_of__timer_2() { return static_cast<int32_t>(offsetof(PeriodicTimer_t1339038026, ____timer_2)); }
	inline Timer_t3546110984 * get__timer_2() const { return ____timer_2; }
	inline Timer_t3546110984 ** get_address_of__timer_2() { return &____timer_2; }
	inline void set__timer_2(Timer_t3546110984 * value)
	{
		____timer_2 = value;
		Il2CppCodeGenWriteBarrier(&____timer_2, value);
	}

	inline static int32_t get_offset_of__gate_3() { return static_cast<int32_t>(offsetof(PeriodicTimer_t1339038026, ____gate_3)); }
	inline AsyncLock_t4023816884 * get__gate_3() const { return ____gate_3; }
	inline AsyncLock_t4023816884 ** get_address_of__gate_3() { return &____gate_3; }
	inline void set__gate_3(AsyncLock_t4023816884 * value)
	{
		____gate_3 = value;
		Il2CppCodeGenWriteBarrier(&____gate_3, value);
	}
};

struct PeriodicTimer_t1339038026_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Threading.Timer> UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::s_timers
	HashSet_1_t1949832359 * ___s_timers_0;

public:
	inline static int32_t get_offset_of_s_timers_0() { return static_cast<int32_t>(offsetof(PeriodicTimer_t1339038026_StaticFields, ___s_timers_0)); }
	inline HashSet_1_t1949832359 * get_s_timers_0() const { return ___s_timers_0; }
	inline HashSet_1_t1949832359 ** get_address_of_s_timers_0() { return &___s_timers_0; }
	inline void set_s_timers_0(HashSet_1_t1949832359 * value)
	{
		___s_timers_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_timers_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
