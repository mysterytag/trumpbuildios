﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IConnectableObservable`1<System.Object>
struct IConnectableObservable_1_t2600005682;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RefCountObservable`1<System.Object>
struct  RefCountObservable_1_t1275253774  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IConnectableObservable`1<T> UniRx.Operators.RefCountObservable`1::source
	Il2CppObject* ___source_1;
	// System.Object UniRx.Operators.RefCountObservable`1::gate
	Il2CppObject * ___gate_2;
	// System.Int32 UniRx.Operators.RefCountObservable`1::refCount
	int32_t ___refCount_3;
	// System.IDisposable UniRx.Operators.RefCountObservable`1::connection
	Il2CppObject * ___connection_4;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(RefCountObservable_1_t1275253774, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(RefCountObservable_1_t1275253774, ___gate_2)); }
	inline Il2CppObject * get_gate_2() const { return ___gate_2; }
	inline Il2CppObject ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(Il2CppObject * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier(&___gate_2, value);
	}

	inline static int32_t get_offset_of_refCount_3() { return static_cast<int32_t>(offsetof(RefCountObservable_1_t1275253774, ___refCount_3)); }
	inline int32_t get_refCount_3() const { return ___refCount_3; }
	inline int32_t* get_address_of_refCount_3() { return &___refCount_3; }
	inline void set_refCount_3(int32_t value)
	{
		___refCount_3 = value;
	}

	inline static int32_t get_offset_of_connection_4() { return static_cast<int32_t>(offsetof(RefCountObservable_1_t1275253774, ___connection_4)); }
	inline Il2CppObject * get_connection_4() const { return ___connection_4; }
	inline Il2CppObject ** get_address_of_connection_4() { return &___connection_4; }
	inline void set_connection_4(Il2CppObject * value)
	{
		___connection_4 = value;
		Il2CppCodeGenWriteBarrier(&___connection_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
