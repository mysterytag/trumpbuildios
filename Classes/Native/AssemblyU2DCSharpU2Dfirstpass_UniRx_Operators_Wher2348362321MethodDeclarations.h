﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where<System.Boolean>
struct Where_t2348362321;
// UniRx.Operators.WhereObservable`1<System.Boolean>
struct WhereObservable_1_t1409565290;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where<System.Boolean>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where__ctor_m924409508_gshared (Where_t2348362321 * __this, WhereObservable_1_t1409565290 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where__ctor_m924409508(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where_t2348362321 *, WhereObservable_1_t1409565290 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where__ctor_m924409508_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Boolean>::OnNext(T)
extern "C"  void Where_OnNext_m1232455370_gshared (Where_t2348362321 * __this, bool ___value0, const MethodInfo* method);
#define Where_OnNext_m1232455370(__this, ___value0, method) ((  void (*) (Where_t2348362321 *, bool, const MethodInfo*))Where_OnNext_m1232455370_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Boolean>::OnError(System.Exception)
extern "C"  void Where_OnError_m2994146777_gshared (Where_t2348362321 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where_OnError_m2994146777(__this, ___error0, method) ((  void (*) (Where_t2348362321 *, Exception_t1967233988 *, const MethodInfo*))Where_OnError_m2994146777_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Boolean>::OnCompleted()
extern "C"  void Where_OnCompleted_m3444637356_gshared (Where_t2348362321 * __this, const MethodInfo* method);
#define Where_OnCompleted_m3444637356(__this, method) ((  void (*) (Where_t2348362321 *, const MethodInfo*))Where_OnCompleted_m3444637356_gshared)(__this, method)
