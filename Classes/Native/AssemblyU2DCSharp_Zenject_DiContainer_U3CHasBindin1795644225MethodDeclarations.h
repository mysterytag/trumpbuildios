﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<HasBinding>c__AnonStorey188
struct U3CHasBindingU3Ec__AnonStorey188_t1795644225;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.DiContainer/<HasBinding>c__AnonStorey188::.ctor()
extern "C"  void U3CHasBindingU3Ec__AnonStorey188__ctor_m856271304 (U3CHasBindingU3Ec__AnonStorey188_t1795644225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<HasBinding>c__AnonStorey188::<>m__2C3(Zenject.ProviderBase)
extern "C"  bool U3CHasBindingU3Ec__AnonStorey188_U3CU3Em__2C3_m1784399526 (U3CHasBindingU3Ec__AnonStorey188_t1795644225 * __this, ProviderBase_t1627494391 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
