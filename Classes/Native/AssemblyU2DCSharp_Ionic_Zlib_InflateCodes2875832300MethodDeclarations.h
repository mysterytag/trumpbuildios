﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.InflateCodes
struct InflateCodes_t2875832300;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// Ionic.Zlib.InflateBlocks
struct InflateBlocks_t3788948410;
// Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3910383704;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_InflateBlocks3788948410.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_ZlibCodec3910383704.h"

// System.Void Ionic.Zlib.InflateCodes::.ctor()
extern "C"  void InflateCodes__ctor_m3718369397 (InflateCodes_t2875832300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.InflateCodes::Init(System.Int32,System.Int32,System.Int32[],System.Int32,System.Int32[],System.Int32)
extern "C"  void InflateCodes_Init_m2463143499 (InflateCodes_t2875832300 * __this, int32_t ___bl0, int32_t ___bd1, Int32U5BU5D_t1809983122* ___tl2, int32_t ___tl_index3, Int32U5BU5D_t1809983122* ___td4, int32_t ___td_index5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateCodes::Process(Ionic.Zlib.InflateBlocks,System.Int32)
extern "C"  int32_t InflateCodes_Process_m117644029 (InflateCodes_t2875832300 * __this, InflateBlocks_t3788948410 * ___blocks0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.InflateCodes::InflateFast(System.Int32,System.Int32,System.Int32[],System.Int32,System.Int32[],System.Int32,Ionic.Zlib.InflateBlocks,Ionic.Zlib.ZlibCodec)
extern "C"  int32_t InflateCodes_InflateFast_m1368675048 (InflateCodes_t2875832300 * __this, int32_t ___bl0, int32_t ___bd1, Int32U5BU5D_t1809983122* ___tl2, int32_t ___tl_index3, Int32U5BU5D_t1809983122* ___td4, int32_t ___td_index5, InflateBlocks_t3788948410 * ___s6, ZlibCodec_t3910383704 * ___z7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
