﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Single,System.Object,System.Object>
struct U3CMergeU3Ec__AnonStorey113_3_t3131141555;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Single,System.Object,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m612980494_gshared (U3CMergeU3Ec__AnonStorey113_3_t3131141555 * __this, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3__ctor_m612980494(__this, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3131141555 *, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3__ctor_m612980494_gshared)(__this, method)
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Single,System.Object,System.Object>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m4026700442_gshared (U3CMergeU3Ec__AnonStorey113_3_t3131141555 * __this, float ___val0, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m4026700442(__this, ___val0, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3131141555 *, float, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m4026700442_gshared)(__this, ___val0, method)
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Single,System.Object,System.Object>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m274585959_gshared (U3CMergeU3Ec__AnonStorey113_3_t3131141555 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m274585959(__this, ___val0, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3131141555 *, Il2CppObject *, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m274585959_gshared)(__this, ___val0, method)
