﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D
struct U3CPressStreamU3Ec__AnonStorey14D_t2542632689;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"

// System.Void ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D::.ctor()
extern "C"  void U3CPressStreamU3Ec__AnonStorey14D__ctor_m1849442253 (U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D::<>m__1FE(UnityEngine.EventSystems.BaseEventData)
extern "C"  void U3CPressStreamU3Ec__AnonStorey14D_U3CU3Em__1FE_m3056454486 (U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * __this, BaseEventData_t3547103726 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
