﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162
struct U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993;
// TableButtons/<RegisterDonateButtons>c__AnonStorey169
struct U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162/<RegisterDonateButtons>c__AnonStorey163
struct  U3CRegisterDonateButtonsU3Ec__AnonStorey163_t2690654994  : public Il2CppObject
{
public:
	// System.Boolean TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162/<RegisterDonateButtons>c__AnonStorey163::b
	bool ___b_0;
	// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162 TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162/<RegisterDonateButtons>c__AnonStorey163::<>f__ref$354
	U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993 * ___U3CU3Ef__refU24354_1;
	// TableButtons/<RegisterDonateButtons>c__AnonStorey169 TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162/<RegisterDonateButtons>c__AnonStorey163::<>f__ref$361
	U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * ___U3CU3Ef__refU24361_2;

public:
	inline static int32_t get_offset_of_b_0() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey163_t2690654994, ___b_0)); }
	inline bool get_b_0() const { return ___b_0; }
	inline bool* get_address_of_b_0() { return &___b_0; }
	inline void set_b_0(bool value)
	{
		___b_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24354_1() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey163_t2690654994, ___U3CU3Ef__refU24354_1)); }
	inline U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993 * get_U3CU3Ef__refU24354_1() const { return ___U3CU3Ef__refU24354_1; }
	inline U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993 ** get_address_of_U3CU3Ef__refU24354_1() { return &___U3CU3Ef__refU24354_1; }
	inline void set_U3CU3Ef__refU24354_1(U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993 * value)
	{
		___U3CU3Ef__refU24354_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24354_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24361_2() { return static_cast<int32_t>(offsetof(U3CRegisterDonateButtonsU3Ec__AnonStorey163_t2690654994, ___U3CU3Ef__refU24361_2)); }
	inline U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * get_U3CU3Ef__refU24361_2() const { return ___U3CU3Ef__refU24361_2; }
	inline U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 ** get_address_of_U3CU3Ef__refU24361_2() { return &___U3CU3Ef__refU24361_2; }
	inline void set_U3CU3Ef__refU24361_2(U3CRegisterDonateButtonsU3Ec__AnonStorey169_t2690655000 * value)
	{
		___U3CU3Ef__refU24361_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24361_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
