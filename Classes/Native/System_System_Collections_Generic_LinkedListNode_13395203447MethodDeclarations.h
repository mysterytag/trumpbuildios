﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_1_539097040MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m1308241451(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t3395203447 *, LinkedList_1_t1138375077 *, TaskInfo_t3693212827 *, const MethodInfo*))LinkedListNode_1__ctor_m648136130_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m654093259(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t3395203447 *, LinkedList_1_t1138375077 *, TaskInfo_t3693212827 *, LinkedListNode_1_t3395203447 *, LinkedListNode_1_t3395203447 *, const MethodInfo*))LinkedListNode_1__ctor_m448391458_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::Detach()
#define LinkedListNode_1_Detach_m2676928149(__this, method) ((  void (*) (LinkedListNode_1_t3395203447 *, const MethodInfo*))LinkedListNode_1_Detach_m3406254942_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::get_List()
#define LinkedListNode_1_get_List_m4055311129(__this, method) ((  LinkedList_1_t1138375077 * (*) (LinkedListNode_1_t3395203447 *, const MethodInfo*))LinkedListNode_1_get_List_m3467110818_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::get_Next()
#define LinkedListNode_1_get_Next_m3808406352(__this, method) ((  LinkedListNode_1_t3395203447 * (*) (LinkedListNode_1_t3395203447 *, const MethodInfo*))LinkedListNode_1_get_Next_m1427618777_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::get_Previous()
#define LinkedListNode_1_get_Previous_m1354267732(__this, method) ((  LinkedListNode_1_t3395203447 * (*) (LinkedListNode_1_t3395203447 *, const MethodInfo*))LinkedListNode_1_get_Previous_m3755155549_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>>::get_Value()
#define LinkedListNode_1_get_Value_m2427119561(__this, method) ((  TaskInfo_t3693212827 * (*) (LinkedListNode_1_t3395203447 *, const MethodInfo*))LinkedListNode_1_get_Value_m702633824_gshared)(__this, method)
