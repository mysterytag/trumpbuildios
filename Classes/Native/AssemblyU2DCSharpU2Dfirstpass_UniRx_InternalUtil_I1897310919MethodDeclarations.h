﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ImmutableList`1<System.Object>
struct ImmutableList_1_t1897310919;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.InternalUtil.ImmutableList`1<System.Object>::.ctor()
extern "C"  void ImmutableList_1__ctor_m2467204245_gshared (ImmutableList_1_t1897310919 * __this, const MethodInfo* method);
#define ImmutableList_1__ctor_m2467204245(__this, method) ((  void (*) (ImmutableList_1_t1897310919 *, const MethodInfo*))ImmutableList_1__ctor_m2467204245_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<System.Object>::.ctor(T[])
extern "C"  void ImmutableList_1__ctor_m707697735_gshared (ImmutableList_1_t1897310919 * __this, ObjectU5BU5D_t11523773* ___data0, const MethodInfo* method);
#define ImmutableList_1__ctor_m707697735(__this, ___data0, method) ((  void (*) (ImmutableList_1_t1897310919 *, ObjectU5BU5D_t11523773*, const MethodInfo*))ImmutableList_1__ctor_m707697735_gshared)(__this, ___data0, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<System.Object>::.cctor()
extern "C"  void ImmutableList_1__cctor_m2986791352_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ImmutableList_1__cctor_m2986791352(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableList_1__cctor_m2986791352_gshared)(__this /* static, unused */, method)
// T[] UniRx.InternalUtil.ImmutableList`1<System.Object>::get_Data()
extern "C"  ObjectU5BU5D_t11523773* ImmutableList_1_get_Data_m1676800965_gshared (ImmutableList_1_t1897310919 * __this, const MethodInfo* method);
#define ImmutableList_1_get_Data_m1676800965(__this, method) ((  ObjectU5BU5D_t11523773* (*) (ImmutableList_1_t1897310919 *, const MethodInfo*))ImmutableList_1_get_Data_m1676800965_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<System.Object>::Add(T)
extern "C"  ImmutableList_1_t1897310919 * ImmutableList_1_Add_m948892931_gshared (ImmutableList_1_t1897310919 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ImmutableList_1_Add_m948892931(__this, ___value0, method) ((  ImmutableList_1_t1897310919 * (*) (ImmutableList_1_t1897310919 *, Il2CppObject *, const MethodInfo*))ImmutableList_1_Add_m948892931_gshared)(__this, ___value0, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<System.Object>::Remove(T)
extern "C"  ImmutableList_1_t1897310919 * ImmutableList_1_Remove_m1538917202_gshared (ImmutableList_1_t1897310919 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ImmutableList_1_Remove_m1538917202(__this, ___value0, method) ((  ImmutableList_1_t1897310919 * (*) (ImmutableList_1_t1897310919 *, Il2CppObject *, const MethodInfo*))ImmutableList_1_Remove_m1538917202_gshared)(__this, ___value0, method)
// System.Int32 UniRx.InternalUtil.ImmutableList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ImmutableList_1_IndexOf_m3149323628_gshared (ImmutableList_1_t1897310919 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ImmutableList_1_IndexOf_m3149323628(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t1897310919 *, Il2CppObject *, const MethodInfo*))ImmutableList_1_IndexOf_m3149323628_gshared)(__this, ___value0, method)
