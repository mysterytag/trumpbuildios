﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19
struct  U3CDelayActionU3Ec__Iterator19_t942204024  : public Il2CppObject
{
public:
	// System.TimeSpan UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::dueTime
	TimeSpan_t763862892  ___dueTime_0;
	// UniRx.ICancelable UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::cancellation
	Il2CppObject * ___cancellation_1;
	// System.Action UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::action
	Action_t437523947 * ___action_2;
	// System.Int32 UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::$PC
	int32_t ___U24PC_3;
	// System.Object UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::$current
	Il2CppObject * ___U24current_4;
	// System.TimeSpan UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::<$>dueTime
	TimeSpan_t763862892  ___U3CU24U3EdueTime_5;
	// UniRx.ICancelable UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::<$>cancellation
	Il2CppObject * ___U3CU24U3Ecancellation_6;
	// System.Action UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::<$>action
	Action_t437523947 * ___U3CU24U3Eaction_7;

public:
	inline static int32_t get_offset_of_dueTime_0() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator19_t942204024, ___dueTime_0)); }
	inline TimeSpan_t763862892  get_dueTime_0() const { return ___dueTime_0; }
	inline TimeSpan_t763862892 * get_address_of_dueTime_0() { return &___dueTime_0; }
	inline void set_dueTime_0(TimeSpan_t763862892  value)
	{
		___dueTime_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator19_t942204024, ___cancellation_1)); }
	inline Il2CppObject * get_cancellation_1() const { return ___cancellation_1; }
	inline Il2CppObject ** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(Il2CppObject * value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier(&___cancellation_1, value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator19_t942204024, ___action_2)); }
	inline Action_t437523947 * get_action_2() const { return ___action_2; }
	inline Action_t437523947 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t437523947 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier(&___action_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator19_t942204024, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator19_t942204024, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EdueTime_5() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator19_t942204024, ___U3CU24U3EdueTime_5)); }
	inline TimeSpan_t763862892  get_U3CU24U3EdueTime_5() const { return ___U3CU24U3EdueTime_5; }
	inline TimeSpan_t763862892 * get_address_of_U3CU24U3EdueTime_5() { return &___U3CU24U3EdueTime_5; }
	inline void set_U3CU24U3EdueTime_5(TimeSpan_t763862892  value)
	{
		___U3CU24U3EdueTime_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ecancellation_6() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator19_t942204024, ___U3CU24U3Ecancellation_6)); }
	inline Il2CppObject * get_U3CU24U3Ecancellation_6() const { return ___U3CU24U3Ecancellation_6; }
	inline Il2CppObject ** get_address_of_U3CU24U3Ecancellation_6() { return &___U3CU24U3Ecancellation_6; }
	inline void set_U3CU24U3Ecancellation_6(Il2CppObject * value)
	{
		___U3CU24U3Ecancellation_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecancellation_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eaction_7() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator19_t942204024, ___U3CU24U3Eaction_7)); }
	inline Action_t437523947 * get_U3CU24U3Eaction_7() const { return ___U3CU24U3Eaction_7; }
	inline Action_t437523947 ** get_address_of_U3CU24U3Eaction_7() { return &___U3CU24U3Eaction_7; }
	inline void set_U3CU24U3Eaction_7(Action_t437523947 * value)
	{
		___U3CU24U3Eaction_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eaction_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
