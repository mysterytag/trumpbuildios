﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<StartPlay>c__Iterator21
struct U3CStartPlayU3Ec__Iterator21_t2971317228;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TutorialPlayer/<StartPlay>c__Iterator21::.ctor()
extern "C"  void U3CStartPlayU3Ec__Iterator21__ctor_m1768933103 (U3CStartPlayU3Ec__Iterator21_t2971317228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<StartPlay>c__Iterator21::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartPlayU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m371624131 (U3CStartPlayU3Ec__Iterator21_t2971317228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<StartPlay>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartPlayU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m2156950103 (U3CStartPlayU3Ec__Iterator21_t2971317228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<StartPlay>c__Iterator21::MoveNext()
extern "C"  bool U3CStartPlayU3Ec__Iterator21_MoveNext_m3222253733 (U3CStartPlayU3Ec__Iterator21_t2971317228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<StartPlay>c__Iterator21::Dispose()
extern "C"  void U3CStartPlayU3Ec__Iterator21_Dispose_m3330913132 (U3CStartPlayU3Ec__Iterator21_t2971317228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<StartPlay>c__Iterator21::Reset()
extern "C"  void U3CStartPlayU3Ec__Iterator21_Reset_m3710333340 (U3CStartPlayU3Ec__Iterator21_t2971317228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<StartPlay>c__Iterator21::<>m__14D(UniRx.Unit)
extern "C"  void U3CStartPlayU3Ec__Iterator21_U3CU3Em__14D_m3699782863 (U3CStartPlayU3Ec__Iterator21_t2971317228 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
