﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.GlobalCompositionRoot
struct GlobalCompositionRoot_t3505596126;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t514686775;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
struct IEnumerable_1_t2589882162;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
struct IEnumerator_1_t1200834254;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object
struct Il2CppObject;
// Zenject.SceneDecoratorCompositionRoot
struct SceneDecoratorCompositionRoot_t3441797106;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50
struct  U3CInitU3Ec__Iterator50_t1096843866  : public Il2CppObject
{
public:
	// Zenject.GlobalCompositionRoot Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::<globalRoot>__0
	GlobalCompositionRoot_t3505596126 * ___U3CglobalRootU3E__0_0;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::<rootObjectsBeforeLoad>__1
	List_1_t514686775 * ___U3CrootObjectsBeforeLoadU3E__1_1;
	// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject> Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::<newlyAddedObjects>__2
	Il2CppObject* ___U3CnewlyAddedObjectsU3E__2_2;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::<$s_325>__3
	Il2CppObject* ___U3CU24s_325U3E__3_3;
	// UnityEngine.GameObject Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::<obj>__4
	GameObject_t4012695102 * ___U3CobjU3E__4_4;
	// System.Int32 Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::$PC
	int32_t ___U24PC_5;
	// System.Object Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::$current
	Il2CppObject * ___U24current_6;
	// Zenject.SceneDecoratorCompositionRoot Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::<>f__this
	SceneDecoratorCompositionRoot_t3441797106 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CglobalRootU3E__0_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__Iterator50_t1096843866, ___U3CglobalRootU3E__0_0)); }
	inline GlobalCompositionRoot_t3505596126 * get_U3CglobalRootU3E__0_0() const { return ___U3CglobalRootU3E__0_0; }
	inline GlobalCompositionRoot_t3505596126 ** get_address_of_U3CglobalRootU3E__0_0() { return &___U3CglobalRootU3E__0_0; }
	inline void set_U3CglobalRootU3E__0_0(GlobalCompositionRoot_t3505596126 * value)
	{
		___U3CglobalRootU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CglobalRootU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CrootObjectsBeforeLoadU3E__1_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__Iterator50_t1096843866, ___U3CrootObjectsBeforeLoadU3E__1_1)); }
	inline List_1_t514686775 * get_U3CrootObjectsBeforeLoadU3E__1_1() const { return ___U3CrootObjectsBeforeLoadU3E__1_1; }
	inline List_1_t514686775 ** get_address_of_U3CrootObjectsBeforeLoadU3E__1_1() { return &___U3CrootObjectsBeforeLoadU3E__1_1; }
	inline void set_U3CrootObjectsBeforeLoadU3E__1_1(List_1_t514686775 * value)
	{
		___U3CrootObjectsBeforeLoadU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrootObjectsBeforeLoadU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CnewlyAddedObjectsU3E__2_2() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__Iterator50_t1096843866, ___U3CnewlyAddedObjectsU3E__2_2)); }
	inline Il2CppObject* get_U3CnewlyAddedObjectsU3E__2_2() const { return ___U3CnewlyAddedObjectsU3E__2_2; }
	inline Il2CppObject** get_address_of_U3CnewlyAddedObjectsU3E__2_2() { return &___U3CnewlyAddedObjectsU3E__2_2; }
	inline void set_U3CnewlyAddedObjectsU3E__2_2(Il2CppObject* value)
	{
		___U3CnewlyAddedObjectsU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnewlyAddedObjectsU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_325U3E__3_3() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__Iterator50_t1096843866, ___U3CU24s_325U3E__3_3)); }
	inline Il2CppObject* get_U3CU24s_325U3E__3_3() const { return ___U3CU24s_325U3E__3_3; }
	inline Il2CppObject** get_address_of_U3CU24s_325U3E__3_3() { return &___U3CU24s_325U3E__3_3; }
	inline void set_U3CU24s_325U3E__3_3(Il2CppObject* value)
	{
		___U3CU24s_325U3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_325U3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__4_4() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__Iterator50_t1096843866, ___U3CobjU3E__4_4)); }
	inline GameObject_t4012695102 * get_U3CobjU3E__4_4() const { return ___U3CobjU3E__4_4; }
	inline GameObject_t4012695102 ** get_address_of_U3CobjU3E__4_4() { return &___U3CobjU3E__4_4; }
	inline void set_U3CobjU3E__4_4(GameObject_t4012695102 * value)
	{
		___U3CobjU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__Iterator50_t1096843866, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__Iterator50_t1096843866, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__Iterator50_t1096843866, ___U3CU3Ef__this_7)); }
	inline SceneDecoratorCompositionRoot_t3441797106 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline SceneDecoratorCompositionRoot_t3441797106 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(SceneDecoratorCompositionRoot_t3441797106 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
