﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.ChunkStream
struct ChunkStream_t795922327;
// System.Net.HttpListenerContext
struct HttpListenerContext_t260537341;

#include "System_System_Net_RequestStream1720256057.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkedInputStream
struct  ChunkedInputStream_t2723915784  : public RequestStream_t1720256057
{
public:
	// System.Boolean System.Net.ChunkedInputStream::disposed
	bool ___disposed_7;
	// System.Net.ChunkStream System.Net.ChunkedInputStream::decoder
	ChunkStream_t795922327 * ___decoder_8;
	// System.Net.HttpListenerContext System.Net.ChunkedInputStream::context
	HttpListenerContext_t260537341 * ___context_9;
	// System.Boolean System.Net.ChunkedInputStream::no_more_data
	bool ___no_more_data_10;

public:
	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t2723915784, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}

	inline static int32_t get_offset_of_decoder_8() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t2723915784, ___decoder_8)); }
	inline ChunkStream_t795922327 * get_decoder_8() const { return ___decoder_8; }
	inline ChunkStream_t795922327 ** get_address_of_decoder_8() { return &___decoder_8; }
	inline void set_decoder_8(ChunkStream_t795922327 * value)
	{
		___decoder_8 = value;
		Il2CppCodeGenWriteBarrier(&___decoder_8, value);
	}

	inline static int32_t get_offset_of_context_9() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t2723915784, ___context_9)); }
	inline HttpListenerContext_t260537341 * get_context_9() const { return ___context_9; }
	inline HttpListenerContext_t260537341 ** get_address_of_context_9() { return &___context_9; }
	inline void set_context_9(HttpListenerContext_t260537341 * value)
	{
		___context_9 = value;
		Il2CppCodeGenWriteBarrier(&___context_9, value);
	}

	inline static int32_t get_offset_of_no_more_data_10() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t2723915784, ___no_more_data_10)); }
	inline bool get_no_more_data_10() const { return ___no_more_data_10; }
	inline bool* get_address_of_no_more_data_10() { return &___no_more_data_10; }
	inline void set_no_more_data_10(bool value)
	{
		___no_more_data_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
