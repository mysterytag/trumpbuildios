﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1<UniRx.Unit>
struct MergeObservable_1_t1064204364;
// UniRx.IObservable`1<UniRx.IObservable`1<UniRx.Unit>>
struct IObservable_1_t2075882766;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.MergeObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>,System.Boolean)
extern "C"  void MergeObservable_1__ctor_m1101124109_gshared (MergeObservable_1_t1064204364 * __this, Il2CppObject* ___sources0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define MergeObservable_1__ctor_m1101124109(__this, ___sources0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (MergeObservable_1_t1064204364 *, Il2CppObject*, bool, const MethodInfo*))MergeObservable_1__ctor_m1101124109_gshared)(__this, ___sources0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.Void UniRx.Operators.MergeObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>,System.Int32,System.Boolean)
extern "C"  void MergeObservable_1__ctor_m482326838_gshared (MergeObservable_1_t1064204364 * __this, Il2CppObject* ___sources0, int32_t ___maxConcurrent1, bool ___isRequiredSubscribeOnCurrentThread2, const MethodInfo* method);
#define MergeObservable_1__ctor_m482326838(__this, ___sources0, ___maxConcurrent1, ___isRequiredSubscribeOnCurrentThread2, method) ((  void (*) (MergeObservable_1_t1064204364 *, Il2CppObject*, int32_t, bool, const MethodInfo*))MergeObservable_1__ctor_m482326838_gshared)(__this, ___sources0, ___maxConcurrent1, ___isRequiredSubscribeOnCurrentThread2, method)
// System.IDisposable UniRx.Operators.MergeObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * MergeObservable_1_SubscribeCore_m2966227564_gshared (MergeObservable_1_t1064204364 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define MergeObservable_1_SubscribeCore_m2966227564(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (MergeObservable_1_t1064204364 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))MergeObservable_1_SubscribeCore_m2966227564_gshared)(__this, ___observer0, ___cancel1, method)
