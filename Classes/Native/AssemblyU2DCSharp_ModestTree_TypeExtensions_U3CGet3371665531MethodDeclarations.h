﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41
struct U3CGetAllMethodsU3Ec__Iterator41_t3371665531;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>
struct IEnumerator_1_t649360429;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::.ctor()
extern "C"  void U3CGetAllMethodsU3Ec__Iterator41__ctor_m473758091 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::System.Collections.Generic.IEnumerator<System.Reflection.MethodInfo>.get_Current()
extern "C"  MethodInfo_t * U3CGetAllMethodsU3Ec__Iterator41_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MethodInfoU3E_get_Current_m281929138 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAllMethodsU3Ec__Iterator41_System_Collections_IEnumerator_get_Current_m38826245 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetAllMethodsU3Ec__Iterator41_System_Collections_IEnumerable_GetEnumerator_m611982086 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo> ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::System.Collections.Generic.IEnumerable<System.Reflection.MethodInfo>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetAllMethodsU3Ec__Iterator41_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m1670105365 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::MoveNext()
extern "C"  bool U3CGetAllMethodsU3Ec__Iterator41_MoveNext_m3912088945 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::Dispose()
extern "C"  void U3CGetAllMethodsU3Ec__Iterator41_Dispose_m4208242440 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.TypeExtensions/<GetAllMethods>c__Iterator41::Reset()
extern "C"  void U3CGetAllMethodsU3Ec__Iterator41_Reset_m2415158328 (U3CGetAllMethodsU3Ec__Iterator41_t3371665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
