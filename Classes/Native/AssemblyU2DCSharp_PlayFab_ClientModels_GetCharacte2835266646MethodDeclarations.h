﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCharacterInventoryRequest
struct GetCharacterInventoryRequest_t2835266646;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetCharacterInventoryRequest::.ctor()
extern "C"  void GetCharacterInventoryRequest__ctor_m1431733671 (GetCharacterInventoryRequest_t2835266646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterInventoryRequest::get_PlayFabId()
extern "C"  String_t* GetCharacterInventoryRequest_get_PlayFabId_m2141173997 (GetCharacterInventoryRequest_t2835266646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterInventoryRequest::set_PlayFabId(System.String)
extern "C"  void GetCharacterInventoryRequest_set_PlayFabId_m475460652 (GetCharacterInventoryRequest_t2835266646 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterInventoryRequest::get_CharacterId()
extern "C"  String_t* GetCharacterInventoryRequest_get_CharacterId_m2297185891 (GetCharacterInventoryRequest_t2835266646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterInventoryRequest::set_CharacterId(System.String)
extern "C"  void GetCharacterInventoryRequest_set_CharacterId_m2932675766 (GetCharacterInventoryRequest_t2835266646 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterInventoryRequest::get_CatalogVersion()
extern "C"  String_t* GetCharacterInventoryRequest_get_CatalogVersion_m3736152770 (GetCharacterInventoryRequest_t2835266646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterInventoryRequest::set_CatalogVersion(System.String)
extern "C"  void GetCharacterInventoryRequest_set_CatalogVersion_m3629948137 (GetCharacterInventoryRequest_t2835266646 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
