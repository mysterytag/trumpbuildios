﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_ThreadSa1163962996MethodDeclarations.h"

// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::.ctor(PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
#define ThreadSafeDictionary_2__ctor_m2147777950(__this, ___valueFactory0, method) ((  void (*) (ThreadSafeDictionary_2_t4048072176 *, ThreadSafeDictionaryValueFactory_2_t360228411 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m145995086_gshared)(__this, ___valueFactory0, method)
// System.Collections.IEnumerator PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m4069837469(__this, method) ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t4048072176 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m647792317_gshared)(__this, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m3907015005(__this, ___key0, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t4048072176 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m3585103853_gshared)(__this, ___key0, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m1784070997(__this, ___key0, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t4048072176 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m844388037_gshared)(__this, ___key0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m260956513(__this, ___key0, ___value1, method) ((  void (*) (ThreadSafeDictionary_2_t4048072176 *, Type_t *, Il2CppObject*, const MethodInfo*))ThreadSafeDictionary_2_Add_m3884150033_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::ContainsKey(TKey)
#define ThreadSafeDictionary_2_ContainsKey_m2318764339(__this, ___key0, method) ((  bool (*) (ThreadSafeDictionary_2_t4048072176 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_ContainsKey_m2111272811_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m1207993642(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t4048072176 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m79078016_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::Remove(TKey)
#define ThreadSafeDictionary_2_Remove_m242252765(__this, ___key0, method) ((  bool (*) (ThreadSafeDictionary_2_t4048072176 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m3287321253_gshared)(__this, ___key0, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m3184197260(__this, ___key0, ___value1, method) ((  bool (*) (ThreadSafeDictionary_2_t4048072176 *, Type_t *, Il2CppObject**, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m2392274116_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m872080938(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t4048072176 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m2528035356_gshared)(__this, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m2068463457(__this, ___key0, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t4048072176 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m1128780497_gshared)(__this, ___key0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m658150548(__this, ___key0, ___value1, method) ((  void (*) (ThreadSafeDictionary_2_t4048072176 *, Type_t *, Il2CppObject*, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m1582880484_gshared)(__this, ___key0, ___value1, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m1624168036(__this, ___item0, method) ((  void (*) (ThreadSafeDictionary_2_t4048072176 *, KeyValuePair_2_t1902098332 , const MethodInfo*))ThreadSafeDictionary_2_Add_m1635310772_gshared)(__this, ___item0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::Clear()
#define ThreadSafeDictionary_2_Clear_m38067711(__this, method) ((  void (*) (ThreadSafeDictionary_2_t4048072176 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m3600968783_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m2208567380(__this, ___item0, method) ((  bool (*) (ThreadSafeDictionary_2_t4048072176 *, KeyValuePair_2_t1902098332 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m78891804_gshared)(__this, ___item0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m2595255624(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ThreadSafeDictionary_2_t4048072176 *, KeyValuePair_2U5BU5D_t3983198837*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m3361965976_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m1384330506(__this, method) ((  int32_t (*) (ThreadSafeDictionary_2_t4048072176 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m187477558_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m425952729(__this, method) ((  bool (*) (ThreadSafeDictionary_2_t4048072176 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m3744395425_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m1004682361(__this, ___item0, method) ((  bool (*) (ThreadSafeDictionary_2_t4048072176 *, KeyValuePair_2_t1902098332 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m4054976833_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m307770769(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t4048072176 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m2011494711_gshared)(__this, method)
