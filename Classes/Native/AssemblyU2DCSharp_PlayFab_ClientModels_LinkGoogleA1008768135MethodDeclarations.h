﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkGoogleAccountResult
struct LinkGoogleAccountResult_t1008768135;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkGoogleAccountResult::.ctor()
extern "C"  void LinkGoogleAccountResult__ctor_m3648836866 (LinkGoogleAccountResult_t1008768135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
