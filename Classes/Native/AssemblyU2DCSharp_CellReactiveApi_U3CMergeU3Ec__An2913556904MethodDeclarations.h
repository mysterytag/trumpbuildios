﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>
struct U3CMergeU3Ec__AnonStorey113_3_t2913556904;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m1123404121_gshared (U3CMergeU3Ec__AnonStorey113_3_t2913556904 * __this, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3__ctor_m1123404121(__this, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t2913556904 *, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3__ctor_m1123404121_gshared)(__this, method)
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1124525925_gshared (U3CMergeU3Ec__AnonStorey113_3_t2913556904 * __this, DateTime_t339033936  ___val0, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1124525925(__this, ___val0, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t2913556904 *, DateTime_t339033936 , const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1124525925_gshared)(__this, ___val0, method)
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m501489148_gshared (U3CMergeU3Ec__AnonStorey113_3_t2913556904 * __this, int64_t ___val0, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m501489148(__this, ___val0, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t2913556904 *, int64_t, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m501489148_gshared)(__this, ___val0, method)
