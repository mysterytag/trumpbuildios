﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Stubs`1<System.Boolean>::.cctor()
extern "C"  void Stubs_1__cctor_m1340283405_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Stubs_1__cctor_m1340283405(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m1340283405_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<System.Boolean>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m1046516233_gshared (Il2CppObject * __this /* static, unused */, bool ___t0, const MethodInfo* method);
#define Stubs_1_U3CIgnoreU3Em__71_m1046516233(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, bool, const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m1046516233_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<System.Boolean>::<Identity>m__72(T)
extern "C"  bool Stubs_1_U3CIdentityU3Em__72_m3768037021_gshared (Il2CppObject * __this /* static, unused */, bool ___t0, const MethodInfo* method);
#define Stubs_1_U3CIdentityU3Em__72_m3768037021(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, bool, const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m3768037021_gshared)(__this /* static, unused */, ___t0, method)
