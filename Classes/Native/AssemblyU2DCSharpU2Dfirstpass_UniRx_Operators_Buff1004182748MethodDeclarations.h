﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>
struct Buffer__t1004182748;
// UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>
struct Buffer_t1984651551;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>::.ctor(UniRx.Operators.BufferObservable`2/Buffer<TSource,TWindowBoundary>)
extern "C"  void Buffer___ctor_m3639263566_gshared (Buffer__t1004182748 * __this, Buffer_t1984651551 * ___parent0, const MethodInfo* method);
#define Buffer___ctor_m3639263566(__this, ___parent0, method) ((  void (*) (Buffer__t1004182748 *, Buffer_t1984651551 *, const MethodInfo*))Buffer___ctor_m3639263566_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>::OnNext(TWindowBoundary)
extern "C"  void Buffer__OnNext_m2846136290_gshared (Buffer__t1004182748 * __this, int64_t ___value0, const MethodInfo* method);
#define Buffer__OnNext_m2846136290(__this, ___value0, method) ((  void (*) (Buffer__t1004182748 *, int64_t, const MethodInfo*))Buffer__OnNext_m2846136290_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>::OnError(System.Exception)
extern "C"  void Buffer__OnError_m3669021003_gshared (Buffer__t1004182748 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Buffer__OnError_m3669021003(__this, ___error0, method) ((  void (*) (Buffer__t1004182748 *, Exception_t1967233988 *, const MethodInfo*))Buffer__OnError_m3669021003_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Int64,System.Int64>::OnCompleted()
extern "C"  void Buffer__OnCompleted_m3540966686_gshared (Buffer__t1004182748 * __this, const MethodInfo* method);
#define Buffer__OnCompleted_m3540966686(__this, method) ((  void (*) (Buffer__t1004182748 *, const MethodInfo*))Buffer__OnCompleted_m3540966686_gshared)(__this, method)
