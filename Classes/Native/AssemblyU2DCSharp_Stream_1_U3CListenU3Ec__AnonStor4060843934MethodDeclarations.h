﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<System.Double>
struct U3CListenU3Ec__AnonStorey128_t4060843934;

#include "codegen/il2cpp-codegen.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<System.Double>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m1234771834_gshared (U3CListenU3Ec__AnonStorey128_t4060843934 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m1234771834(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t4060843934 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m1234771834_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Double>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m4267406743_gshared (U3CListenU3Ec__AnonStorey128_t4060843934 * __this, double ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m4267406743(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t4060843934 *, double, const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m4267406743_gshared)(__this, ____0, method)
