﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_EqualityComparer32304522.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct  DefaultComparer_t4230834899  : public EqualityComparer_1_t32304522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
