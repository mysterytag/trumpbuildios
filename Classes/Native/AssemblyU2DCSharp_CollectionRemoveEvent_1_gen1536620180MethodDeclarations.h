﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258MethodDeclarations.h"

// System.Void CollectionRemoveEvent`1<UpgradeButton>::.ctor(System.Int32,T)
#define CollectionRemoveEvent_1__ctor_m2428204929(__this, ___index0, ___value1, method) ((  void (*) (CollectionRemoveEvent_1_t1536620180 *, int32_t, UpgradeButton_t1475467342 *, const MethodInfo*))CollectionRemoveEvent_1__ctor_m1174003473_gshared)(__this, ___index0, ___value1, method)
// System.Int32 CollectionRemoveEvent`1<UpgradeButton>::get_Index()
#define CollectionRemoveEvent_1_get_Index_m242234641(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t1536620180 *, const MethodInfo*))CollectionRemoveEvent_1_get_Index_m2777665697_gshared)(__this, method)
// System.Void CollectionRemoveEvent`1<UpgradeButton>::set_Index(System.Int32)
#define CollectionRemoveEvent_1_set_Index_m4142805948(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t1536620180 *, int32_t, const MethodInfo*))CollectionRemoveEvent_1_set_Index_m1446049612_gshared)(__this, ___value0, method)
// T CollectionRemoveEvent`1<UpgradeButton>::get_Value()
#define CollectionRemoveEvent_1_get_Value_m2205510879(__this, method) ((  UpgradeButton_t1475467342 * (*) (CollectionRemoveEvent_1_t1536620180 *, const MethodInfo*))CollectionRemoveEvent_1_get_Value_m445974639_gshared)(__this, method)
// System.Void CollectionRemoveEvent`1<UpgradeButton>::set_Value(T)
#define CollectionRemoveEvent_1_set_Value_m3353098964(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t1536620180 *, UpgradeButton_t1475467342 *, const MethodInfo*))CollectionRemoveEvent_1_set_Value_m347083076_gshared)(__this, ___value0, method)
// System.String CollectionRemoveEvent`1<UpgradeButton>::ToString()
#define CollectionRemoveEvent_1_ToString_m686907579(__this, method) ((  String_t* (*) (CollectionRemoveEvent_1_t1536620180 *, const MethodInfo*))CollectionRemoveEvent_1_ToString_m2292716331_gshared)(__this, method)
