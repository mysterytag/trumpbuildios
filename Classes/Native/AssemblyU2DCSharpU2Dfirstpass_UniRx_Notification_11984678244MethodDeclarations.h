﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Notification`1/<ToObservable>c__AnonStorey55<System.Object>
struct U3CToObservableU3Ec__AnonStorey55_t1984678244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Notification`1/<ToObservable>c__AnonStorey55<System.Object>::.ctor()
extern "C"  void U3CToObservableU3Ec__AnonStorey55__ctor_m1172590403_gshared (U3CToObservableU3Ec__AnonStorey55_t1984678244 * __this, const MethodInfo* method);
#define U3CToObservableU3Ec__AnonStorey55__ctor_m1172590403(__this, method) ((  void (*) (U3CToObservableU3Ec__AnonStorey55_t1984678244 *, const MethodInfo*))U3CToObservableU3Ec__AnonStorey55__ctor_m1172590403_gshared)(__this, method)
// System.IDisposable UniRx.Notification`1/<ToObservable>c__AnonStorey55<System.Object>::<>m__69(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * U3CToObservableU3Ec__AnonStorey55_U3CU3Em__69_m3771438140_gshared (U3CToObservableU3Ec__AnonStorey55_t1984678244 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define U3CToObservableU3Ec__AnonStorey55_U3CU3Em__69_m3771438140(__this, ___observer0, method) ((  Il2CppObject * (*) (U3CToObservableU3Ec__AnonStorey55_t1984678244 *, Il2CppObject*, const MethodInfo*))U3CToObservableU3Ec__AnonStorey55_U3CU3Em__69_m3771438140_gshared)(__this, ___observer0, method)
