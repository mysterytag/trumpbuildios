﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t1751284303;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0__ctor_m3288738114 (U3CU3Ec__DisplayClass4_0_t1751284303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__0()
extern "C"  Color_t1588175760  U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m4123970660 (U3CU3Ec__DisplayClass4_0_t1751284303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m1298142362 (U3CU3Ec__DisplayClass4_0_t1751284303 * __this, Color_t1588175760  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
