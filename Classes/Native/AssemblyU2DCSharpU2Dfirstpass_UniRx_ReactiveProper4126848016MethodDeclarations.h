﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<UnityEngine.Bounds>
struct ReactiveProperty_1_t4126848016;
// UniRx.IObservable`1<UnityEngine.Bounds>
struct IObservable_1_t3277313342;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds>
struct IEqualityComparer_1_t1547814333;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Bounds>
struct IObserver_1_t1435546585;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m3808573442_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3808573442(__this, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, const MethodInfo*))ReactiveProperty_1__ctor_m3808573442_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m2101661084_gshared (ReactiveProperty_1_t4126848016 * __this, Bounds_t3518514978  ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2101661084(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, Bounds_t3518514978 , const MethodInfo*))ReactiveProperty_1__ctor_m2101661084_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m869683297_gshared (ReactiveProperty_1_t4126848016 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m869683297(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m869683297_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m2541998521_gshared (ReactiveProperty_1_t4126848016 * __this, Il2CppObject* ___source0, Bounds_t3518514978  ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2541998521(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, Il2CppObject*, Bounds_t3518514978 , const MethodInfo*))ReactiveProperty_1__ctor_m2541998521_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m1619563499_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m1619563499(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m1619563499_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Bounds>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m3430053937_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m3430053937(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t4126848016 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m3430053937_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.Bounds>::get_Value()
extern "C"  Bounds_t3518514978  ReactiveProperty_1_get_Value_m513908041_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m513908041(__this, method) ((  Bounds_t3518514978  (*) (ReactiveProperty_1_t4126848016 *, const MethodInfo*))ReactiveProperty_1_get_Value_m513908041_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m486851562_gshared (ReactiveProperty_1_t4126848016 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m486851562(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, Bounds_t3518514978 , const MethodInfo*))ReactiveProperty_1_set_Value_m486851562_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m3842169997_gshared (ReactiveProperty_1_t4126848016 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m3842169997(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, Bounds_t3518514978 , const MethodInfo*))ReactiveProperty_1_SetValue_m3842169997_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m4268779408_gshared (ReactiveProperty_1_t4126848016 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m4268779408(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, Bounds_t3518514978 , const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m4268779408_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Bounds>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m3642423105_gshared (ReactiveProperty_1_t4126848016 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m3642423105(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t4126848016 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m3642423105_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m625224639_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m625224639(__this, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, const MethodInfo*))ReactiveProperty_1_Dispose_m625224639_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m2251810550_gshared (ReactiveProperty_1_t4126848016 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m2251810550(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t4126848016 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m2251810550_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.Bounds>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m125066769_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m125066769(__this, method) ((  String_t* (*) (ReactiveProperty_1_t4126848016 *, const MethodInfo*))ReactiveProperty_1_ToString_m125066769_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Bounds>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m564760001_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m564760001(__this, method) ((  bool (*) (ReactiveProperty_1_t4126848016 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m564760001_gshared)(__this, method)
