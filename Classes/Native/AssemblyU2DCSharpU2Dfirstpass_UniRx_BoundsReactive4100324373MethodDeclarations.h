﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.BoundsReactiveProperty
struct BoundsReactiveProperty_t4100324373;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds>
struct IEqualityComparer_1_t1547814333;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"

// System.Void UniRx.BoundsReactiveProperty::.ctor()
extern "C"  void BoundsReactiveProperty__ctor_m1171234580 (BoundsReactiveProperty_t4100324373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BoundsReactiveProperty::.ctor(UnityEngine.Bounds)
extern "C"  void BoundsReactiveProperty__ctor_m2555393320 (BoundsReactiveProperty_t4100324373 * __this, Bounds_t3518514978  ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds> UniRx.BoundsReactiveProperty::get_EqualityComparer()
extern "C"  Il2CppObject* BoundsReactiveProperty_get_EqualityComparer_m3459484445 (BoundsReactiveProperty_t4100324373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
