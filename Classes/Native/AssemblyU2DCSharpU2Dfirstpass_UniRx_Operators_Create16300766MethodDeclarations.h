﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1/Create<System.Boolean>
struct Create_t16300766;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CreateObservable`1/Create<System.Boolean>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m2173222544_gshared (Create_t16300766 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Create__ctor_m2173222544(__this, ___observer0, ___cancel1, method) ((  void (*) (Create_t16300766 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Create__ctor_m2173222544_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Boolean>::OnNext(T)
extern "C"  void Create_OnNext_m1872647938_gshared (Create_t16300766 * __this, bool ___value0, const MethodInfo* method);
#define Create_OnNext_m1872647938(__this, ___value0, method) ((  void (*) (Create_t16300766 *, bool, const MethodInfo*))Create_OnNext_m1872647938_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Boolean>::OnError(System.Exception)
extern "C"  void Create_OnError_m3131584017_gshared (Create_t16300766 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Create_OnError_m3131584017(__this, ___error0, method) ((  void (*) (Create_t16300766 *, Exception_t1967233988 *, const MethodInfo*))Create_OnError_m3131584017_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Boolean>::OnCompleted()
extern "C"  void Create_OnCompleted_m2412163812_gshared (Create_t16300766 * __this, const MethodInfo* method);
#define Create_OnCompleted_m2412163812(__this, method) ((  void (*) (Create_t16300766 *, const MethodInfo*))Create_OnCompleted_m2412163812_gshared)(__this, method)
