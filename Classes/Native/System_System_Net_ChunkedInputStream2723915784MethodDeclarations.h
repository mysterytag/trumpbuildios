﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.ChunkedInputStream
struct ChunkedInputStream_t2723915784;
// System.Net.HttpListenerContext
struct HttpListenerContext_t260537341;
// System.IO.Stream
struct Stream_t219029575;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Net.ChunkStream
struct ChunkStream_t795922327;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_HttpListenerContext260537341.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "System_System_Net_ChunkStream795922327.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.ChunkedInputStream::.ctor(System.Net.HttpListenerContext,System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern "C"  void ChunkedInputStream__ctor_m974228007 (ChunkedInputStream_t2723915784 * __this, HttpListenerContext_t260537341 * ___context0, Stream_t219029575 * ___stream1, ByteU5BU5D_t58506160* ___buffer2, int32_t ___offset3, int32_t ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ChunkStream System.Net.ChunkedInputStream::get_Decoder()
extern "C"  ChunkStream_t795922327 * ChunkedInputStream_get_Decoder_m2470115191 (ChunkedInputStream_t2723915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ChunkedInputStream::set_Decoder(System.Net.ChunkStream)
extern "C"  void ChunkedInputStream_set_Decoder_m2480266660 (ChunkedInputStream_t2723915784 * __this, ChunkStream_t795922327 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ChunkedInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ChunkedInputStream_Read_m1501527579 (ChunkedInputStream_t2723915784 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.ChunkedInputStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ChunkedInputStream_BeginRead_m4122821062 (ChunkedInputStream_t2723915784 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t1363551830 * ___cback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ChunkedInputStream::OnRead(System.IAsyncResult)
extern "C"  void ChunkedInputStream_OnRead_m1738191238 (ChunkedInputStream_t2723915784 * __this, Il2CppObject * ___base_ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ChunkedInputStream::EndRead(System.IAsyncResult)
extern "C"  int32_t ChunkedInputStream_EndRead_m617776264 (ChunkedInputStream_t2723915784 * __this, Il2CppObject * ___ares0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ChunkedInputStream::Close()
extern "C"  void ChunkedInputStream_Close_m1384011442 (ChunkedInputStream_t2723915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
