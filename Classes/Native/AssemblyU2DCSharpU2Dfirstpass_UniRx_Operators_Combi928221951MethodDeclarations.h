﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatest_t928221951;
// UniRx.Operators.CombineLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_5_t1731983148;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.CombineLatestObservable`5<T1,T2,T3,T4,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void CombineLatest__ctor_m1293158819_gshared (CombineLatest_t928221951 * __this, int32_t ___length0, CombineLatestObservable_5_t1731983148 * ___parent1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method);
#define CombineLatest__ctor_m1293158819(__this, ___length0, ___parent1, ___observer2, ___cancel3, method) ((  void (*) (CombineLatest_t928221951 *, int32_t, CombineLatestObservable_5_t1731983148 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatest__ctor_m1293158819_gshared)(__this, ___length0, ___parent1, ___observer2, ___cancel3, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m1833850631_gshared (CombineLatest_t928221951 * __this, const MethodInfo* method);
#define CombineLatest_Run_m1833850631(__this, method) ((  Il2CppObject * (*) (CombineLatest_t928221951 *, const MethodInfo*))CombineLatest_Run_m1833850631_gshared)(__this, method)
// TR UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * CombineLatest_GetResult_m3403243107_gshared (CombineLatest_t928221951 * __this, const MethodInfo* method);
#define CombineLatest_GetResult_m3403243107(__this, method) ((  Il2CppObject * (*) (CombineLatest_t928221951 *, const MethodInfo*))CombineLatest_GetResult_m3403243107_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void CombineLatest_OnNext_m759908831_gshared (CombineLatest_t928221951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CombineLatest_OnNext_m759908831(__this, ___value0, method) ((  void (*) (CombineLatest_t928221951 *, Il2CppObject *, const MethodInfo*))CombineLatest_OnNext_m759908831_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m693952624_gshared (CombineLatest_t928221951 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatest_OnError_m693952624(__this, ___error0, method) ((  void (*) (CombineLatest_t928221951 *, Exception_t1967233988 *, const MethodInfo*))CombineLatest_OnError_m693952624_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`5/CombineLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m1059371971_gshared (CombineLatest_t928221951 * __this, const MethodInfo* method);
#define CombineLatest_OnCompleted_m1059371971(__this, method) ((  void (*) (CombineLatest_t928221951 *, const MethodInfo*))CombineLatest_OnCompleted_m1059371971_gshared)(__this, method)
