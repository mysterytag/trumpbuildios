﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC
struct U3CTestAsyncU3Ec__IteratorC_t488520482;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::.ctor()
extern "C"  void U3CTestAsyncU3Ec__IteratorC__ctor_m186727593 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestAsyncU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4032043859 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestAsyncU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3872728295 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::MoveNext()
extern "C"  bool U3CTestAsyncU3Ec__IteratorC_MoveNext_m3384603003 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::Dispose()
extern "C"  void U3CTestAsyncU3Ec__IteratorC_Dispose_m3249840806 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__IteratorC::Reset()
extern "C"  void U3CTestAsyncU3Ec__IteratorC_Reset_m2128127830 (U3CTestAsyncU3Ec__IteratorC_t488520482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
