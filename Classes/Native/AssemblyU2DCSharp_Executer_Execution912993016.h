﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IProcess
struct IProcess_t4026237414;
// Executer
struct Executer_t2107661245;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Executer/Execution
struct  Execution_t912993016  : public Il2CppObject
{
public:
	// IProcess Executer/Execution::coro
	Il2CppObject * ___coro_0;
	// Executer Executer/Execution::parent
	Executer_t2107661245 * ___parent_1;

public:
	inline static int32_t get_offset_of_coro_0() { return static_cast<int32_t>(offsetof(Execution_t912993016, ___coro_0)); }
	inline Il2CppObject * get_coro_0() const { return ___coro_0; }
	inline Il2CppObject ** get_address_of_coro_0() { return &___coro_0; }
	inline void set_coro_0(Il2CppObject * value)
	{
		___coro_0 = value;
		Il2CppCodeGenWriteBarrier(&___coro_0, value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(Execution_t912993016, ___parent_1)); }
	inline Executer_t2107661245 * get_parent_1() const { return ___parent_1; }
	inline Executer_t2107661245 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(Executer_t2107661245 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
