﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey143<System.Object,System.Object>
struct U3CAffectU3Ec__AnonStorey143_t1517854361;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Object,System.Object>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m113918323_gshared (U3CAffectU3Ec__AnonStorey143_t1517854361 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143__ctor_m113918323(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t1517854361 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey143__ctor_m113918323_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Object,System.Object>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m4009603321_gshared (U3CAffectU3Ec__AnonStorey143_t1517854361 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m4009603321(__this, ___val0, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t1517854361 *, Il2CppObject *, const MethodInfo*))U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m4009603321_gshared)(__this, ___val0, method)
