﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<System.Object>
struct ThrottleFrame_t3629975492;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick<System.Object>
struct  ThrottleFrameTick_t1733702649  : public Il2CppObject
{
public:
	// UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame<T> UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick::parent
	ThrottleFrame_t3629975492 * ___parent_0;
	// System.UInt64 UniRx.Operators.ThrottleFrameObservable`1/ThrottleFrame/ThrottleFrameTick::currentid
	uint64_t ___currentid_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(ThrottleFrameTick_t1733702649, ___parent_0)); }
	inline ThrottleFrame_t3629975492 * get_parent_0() const { return ___parent_0; }
	inline ThrottleFrame_t3629975492 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ThrottleFrame_t3629975492 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_currentid_1() { return static_cast<int32_t>(offsetof(ThrottleFrameTick_t1733702649, ___currentid_1)); }
	inline uint64_t get_currentid_1() const { return ___currentid_1; }
	inline uint64_t* get_address_of_currentid_1() { return &___currentid_1; }
	inline void set_currentid_1(uint64_t value)
	{
		___currentid_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
