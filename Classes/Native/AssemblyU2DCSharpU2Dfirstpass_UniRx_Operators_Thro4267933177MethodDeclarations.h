﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Int64>
struct U3COnNextU3Ec__AnonStorey6A_t4267933177;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Int64>::.ctor()
extern "C"  void U3COnNextU3Ec__AnonStorey6A__ctor_m155556972_gshared (U3COnNextU3Ec__AnonStorey6A_t4267933177 * __this, const MethodInfo* method);
#define U3COnNextU3Ec__AnonStorey6A__ctor_m155556972(__this, method) ((  void (*) (U3COnNextU3Ec__AnonStorey6A_t4267933177 *, const MethodInfo*))U3COnNextU3Ec__AnonStorey6A__ctor_m155556972_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Int64>::<>m__88()
extern "C"  void U3COnNextU3Ec__AnonStorey6A_U3CU3Em__88_m1474650933_gshared (U3COnNextU3Ec__AnonStorey6A_t4267933177 * __this, const MethodInfo* method);
#define U3COnNextU3Ec__AnonStorey6A_U3CU3Em__88_m1474650933(__this, method) ((  void (*) (U3COnNextU3Ec__AnonStorey6A_t4267933177 *, const MethodInfo*))U3COnNextU3Ec__AnonStorey6A_U3CU3Em__88_m1474650933_gshared)(__this, method)
