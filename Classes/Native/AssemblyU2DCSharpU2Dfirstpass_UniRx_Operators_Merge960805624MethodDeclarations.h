﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>
struct MergeConcurrentObserver_t960805624;
// UniRx.Operators.MergeObservable`1<UniRx.Unit>
struct MergeObservable_1_t1064204364;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::.ctor(UniRx.Operators.MergeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void MergeConcurrentObserver__ctor_m2772962521_gshared (MergeConcurrentObserver_t960805624 * __this, MergeObservable_1_t1064204364 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define MergeConcurrentObserver__ctor_m2772962521(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (MergeConcurrentObserver_t960805624 *, MergeObservable_1_t1064204364 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))MergeConcurrentObserver__ctor_m2772962521_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::Run()
extern "C"  Il2CppObject * MergeConcurrentObserver_Run_m2388209898_gshared (MergeConcurrentObserver_t960805624 * __this, const MethodInfo* method);
#define MergeConcurrentObserver_Run_m2388209898(__this, method) ((  Il2CppObject * (*) (MergeConcurrentObserver_t960805624 *, const MethodInfo*))MergeConcurrentObserver_Run_m2388209898_gshared)(__this, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::OnNext(UniRx.IObservable`1<T>)
extern "C"  void MergeConcurrentObserver_OnNext_m3567754127_gshared (MergeConcurrentObserver_t960805624 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define MergeConcurrentObserver_OnNext_m3567754127(__this, ___value0, method) ((  void (*) (MergeConcurrentObserver_t960805624 *, Il2CppObject*, const MethodInfo*))MergeConcurrentObserver_OnNext_m3567754127_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::OnError(System.Exception)
extern "C"  void MergeConcurrentObserver_OnError_m2293113021_gshared (MergeConcurrentObserver_t960805624 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define MergeConcurrentObserver_OnError_m2293113021(__this, ___error0, method) ((  void (*) (MergeConcurrentObserver_t960805624 *, Exception_t1967233988 *, const MethodInfo*))MergeConcurrentObserver_OnError_m2293113021_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::OnCompleted()
extern "C"  void MergeConcurrentObserver_OnCompleted_m2659338128_gshared (MergeConcurrentObserver_t960805624 * __this, const MethodInfo* method);
#define MergeConcurrentObserver_OnCompleted_m2659338128(__this, method) ((  void (*) (MergeConcurrentObserver_t960805624 *, const MethodInfo*))MergeConcurrentObserver_OnCompleted_m2659338128_gshared)(__this, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>::Subscribe(UniRx.IObservable`1<T>)
extern "C"  void MergeConcurrentObserver_Subscribe_m2627665485_gshared (MergeConcurrentObserver_t960805624 * __this, Il2CppObject* ___innerSource0, const MethodInfo* method);
#define MergeConcurrentObserver_Subscribe_m2627665485(__this, ___innerSource0, method) ((  void (*) (MergeConcurrentObserver_t960805624 *, Il2CppObject*, const MethodInfo*))MergeConcurrentObserver_Subscribe_m2627665485_gshared)(__this, ___innerSource0, method)
