﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.JsonArray
struct JsonArray_t1741147506;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.JsonArray::.ctor()
extern "C"  void JsonArray__ctor_m73549077 (JsonArray_t1741147506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.JsonArray::.ctor(System.Int32)
extern "C"  void JsonArray__ctor_m2037383910 (JsonArray_t1741147506 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.JsonArray::ToString()
extern "C"  String_t* JsonArray_ToString_m3433455288 (JsonArray_t1741147506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
