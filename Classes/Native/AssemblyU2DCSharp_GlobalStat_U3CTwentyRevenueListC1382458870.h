﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// GlobalStat
struct GlobalStat_t1134678199;
// System.Predicate`1<System.Int64>
struct Predicate_1_t3418378780;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156843.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalStat/<TwentyRevenueListCheck>c__Iterator19
struct  U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870  : public Il2CppObject
{
public:
	// System.Boolean GlobalStat/<TwentyRevenueListCheck>c__Iterator19::<red_blinker>__0
	bool ___U3Cred_blinkerU3E__0_0;
	// System.Int64 GlobalStat/<TwentyRevenueListCheck>c__Iterator19::<max_t>__1
	int64_t ___U3Cmax_tU3E__1_1;
	// System.Collections.Generic.List`1/Enumerator<System.Int64> GlobalStat/<TwentyRevenueListCheck>c__Iterator19::<$s_92>__2
	Enumerator_t1730156843  ___U3CU24s_92U3E__2_2;
	// System.Int64 GlobalStat/<TwentyRevenueListCheck>c__Iterator19::<t>__3
	int64_t ___U3CtU3E__3_3;
	// System.Int64 GlobalStat/<TwentyRevenueListCheck>c__Iterator19::<counter>__4
	int64_t ___U3CcounterU3E__4_4;
	// System.Int32 GlobalStat/<TwentyRevenueListCheck>c__Iterator19::$PC
	int32_t ___U24PC_5;
	// System.Object GlobalStat/<TwentyRevenueListCheck>c__Iterator19::$current
	Il2CppObject * ___U24current_6;
	// GlobalStat GlobalStat/<TwentyRevenueListCheck>c__Iterator19::<>f__this
	GlobalStat_t1134678199 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3Cred_blinkerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870, ___U3Cred_blinkerU3E__0_0)); }
	inline bool get_U3Cred_blinkerU3E__0_0() const { return ___U3Cred_blinkerU3E__0_0; }
	inline bool* get_address_of_U3Cred_blinkerU3E__0_0() { return &___U3Cred_blinkerU3E__0_0; }
	inline void set_U3Cred_blinkerU3E__0_0(bool value)
	{
		___U3Cred_blinkerU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cmax_tU3E__1_1() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870, ___U3Cmax_tU3E__1_1)); }
	inline int64_t get_U3Cmax_tU3E__1_1() const { return ___U3Cmax_tU3E__1_1; }
	inline int64_t* get_address_of_U3Cmax_tU3E__1_1() { return &___U3Cmax_tU3E__1_1; }
	inline void set_U3Cmax_tU3E__1_1(int64_t value)
	{
		___U3Cmax_tU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_92U3E__2_2() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870, ___U3CU24s_92U3E__2_2)); }
	inline Enumerator_t1730156843  get_U3CU24s_92U3E__2_2() const { return ___U3CU24s_92U3E__2_2; }
	inline Enumerator_t1730156843 * get_address_of_U3CU24s_92U3E__2_2() { return &___U3CU24s_92U3E__2_2; }
	inline void set_U3CU24s_92U3E__2_2(Enumerator_t1730156843  value)
	{
		___U3CU24s_92U3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__3_3() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870, ___U3CtU3E__3_3)); }
	inline int64_t get_U3CtU3E__3_3() const { return ___U3CtU3E__3_3; }
	inline int64_t* get_address_of_U3CtU3E__3_3() { return &___U3CtU3E__3_3; }
	inline void set_U3CtU3E__3_3(int64_t value)
	{
		___U3CtU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__4_4() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870, ___U3CcounterU3E__4_4)); }
	inline int64_t get_U3CcounterU3E__4_4() const { return ___U3CcounterU3E__4_4; }
	inline int64_t* get_address_of_U3CcounterU3E__4_4() { return &___U3CcounterU3E__4_4; }
	inline void set_U3CcounterU3E__4_4(int64_t value)
	{
		___U3CcounterU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870, ___U3CU3Ef__this_7)); }
	inline GlobalStat_t1134678199 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline GlobalStat_t1134678199 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(GlobalStat_t1134678199 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

struct U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870_StaticFields
{
public:
	// System.Predicate`1<System.Int64> GlobalStat/<TwentyRevenueListCheck>c__Iterator19::<>f__am$cache8
	Predicate_1_t3418378780 * ___U3CU3Ef__amU24cache8_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline Predicate_1_t3418378780 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline Predicate_1_t3418378780 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(Predicate_1_t3418378780 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
