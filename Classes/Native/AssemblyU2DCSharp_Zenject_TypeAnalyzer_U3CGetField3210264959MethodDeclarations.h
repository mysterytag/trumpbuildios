﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48
struct U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo>
struct IEnumerator_1_t2630816222;
// System.Reflection.FieldInfo
struct FieldInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"

// System.Void Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::.ctor()
extern "C"  void U3CGetFieldInjectablesU3Ec__Iterator48__ctor_m2018515068 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::System.Collections.Generic.IEnumerator<Zenject.InjectableInfo>.get_Current()
extern "C"  InjectableInfo_t1147709774 * U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_Generic_IEnumeratorU3CZenject_InjectableInfoU3E_get_Current_m2006423853 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_IEnumerator_get_Current_m2709704426 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_IEnumerable_GetEnumerator_m2027911013 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::System.Collections.Generic.IEnumerable<Zenject.InjectableInfo>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m1311321116 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::MoveNext()
extern "C"  bool U3CGetFieldInjectablesU3Ec__Iterator48_MoveNext_m1315356664 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::Dispose()
extern "C"  void U3CGetFieldInjectablesU3Ec__Iterator48_Dispose_m2661012921 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::Reset()
extern "C"  void U3CGetFieldInjectablesU3Ec__Iterator48_Reset_m3959915305 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::<>m__2B8(System.Reflection.FieldInfo)
extern "C"  bool U3CGetFieldInjectablesU3Ec__Iterator48_U3CU3Em__2B8_m2039708125 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
