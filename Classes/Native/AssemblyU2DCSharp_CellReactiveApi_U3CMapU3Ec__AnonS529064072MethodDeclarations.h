﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Int32>
struct U3CMapU3Ec__AnonStorey10E_2_t529064072;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Int32>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10E_2__ctor_m3164597324_gshared (U3CMapU3Ec__AnonStorey10E_2_t529064072 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2__ctor_m3164597324(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey10E_2_t529064072 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2__ctor_m3164597324_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Int32>::<>m__180(System.Action`1<T2>,Priority)
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2920807427_gshared (U3CMapU3Ec__AnonStorey10E_2_t529064072 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2920807427(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CMapU3Ec__AnonStorey10E_2_t529064072 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2920807427_gshared)(__this, ___reaction0, ___p1, method)
// T2 CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Int32>::<>m__181()
extern "C"  int32_t U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1454256246_gshared (U3CMapU3Ec__AnonStorey10E_2_t529064072 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1454256246(__this, method) ((  int32_t (*) (U3CMapU3Ec__AnonStorey10E_2_t529064072 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1454256246_gshared)(__this, method)
