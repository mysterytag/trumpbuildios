﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.XmlSerializableMapping
struct XmlSerializableMapping_t3919399670;
// System.String
struct String_t;
// System.Xml.Serialization.TypeData
struct TypeData_t3837952962;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_Xml_System_Xml_Serialization_TypeData3837952962.h"

// System.Void System.Xml.Serialization.XmlSerializableMapping::.ctor(System.String,System.String,System.Xml.Serialization.TypeData,System.String,System.String)
extern "C"  void XmlSerializableMapping__ctor_m1941425557 (XmlSerializableMapping_t3919399670 * __this, String_t* ___elementName0, String_t* ___ns1, TypeData_t3837952962 * ___typeData2, String_t* ___xmlType3, String_t* ___xmlTypeNamespace4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
