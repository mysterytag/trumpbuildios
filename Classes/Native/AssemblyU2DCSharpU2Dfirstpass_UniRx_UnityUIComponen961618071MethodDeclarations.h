﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<SubscribeToInteractable>c__AnonStoreyA6
struct U3CSubscribeToInteractableU3Ec__AnonStoreyA6_t961618071;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToInteractable>c__AnonStoreyA6::.ctor()
extern "C"  void U3CSubscribeToInteractableU3Ec__AnonStoreyA6__ctor_m2562781553 (U3CSubscribeToInteractableU3Ec__AnonStoreyA6_t961618071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToInteractable>c__AnonStoreyA6::<>m__E4(System.Boolean)
extern "C"  void U3CSubscribeToInteractableU3Ec__AnonStoreyA6_U3CU3Em__E4_m2253620864 (U3CSubscribeToInteractableU3Ec__AnonStoreyA6_t961618071 * __this, bool ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
