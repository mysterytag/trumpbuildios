﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LogEvent>c__AnonStoreyBA
struct U3CLogEventU3Ec__AnonStoreyBA_t398092826;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LogEvent>c__AnonStoreyBA::.ctor()
extern "C"  void U3CLogEventU3Ec__AnonStoreyBA__ctor_m3031879385 (U3CLogEventU3Ec__AnonStoreyBA_t398092826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LogEvent>c__AnonStoreyBA::<>m__A7(PlayFab.CallRequestContainer)
extern "C"  void U3CLogEventU3Ec__AnonStoreyBA_U3CU3Em__A7_m1143626125 (U3CLogEventU3Ec__AnonStoreyBA_t398092826 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
