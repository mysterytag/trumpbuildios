﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AdToApp.AdToAppSDKDelegate
struct AdToAppSDKDelegate_t2853395885;

#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp2854275270.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdToApp.AdToAppSDKDelegate/AndroidBannerListener
struct  AndroidBannerListener_t3507331119  : public ATABannerAdListener_t2854275270
{
public:
	// AdToApp.AdToAppSDKDelegate AdToApp.AdToAppSDKDelegate/AndroidBannerListener::_sdkDelegate
	AdToAppSDKDelegate_t2853395885 * ____sdkDelegate_0;

public:
	inline static int32_t get_offset_of__sdkDelegate_0() { return static_cast<int32_t>(offsetof(AndroidBannerListener_t3507331119, ____sdkDelegate_0)); }
	inline AdToAppSDKDelegate_t2853395885 * get__sdkDelegate_0() const { return ____sdkDelegate_0; }
	inline AdToAppSDKDelegate_t2853395885 ** get_address_of__sdkDelegate_0() { return &____sdkDelegate_0; }
	inline void set__sdkDelegate_0(AdToAppSDKDelegate_t2853395885 * value)
	{
		____sdkDelegate_0 = value;
		Il2CppCodeGenWriteBarrier(&____sdkDelegate_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
