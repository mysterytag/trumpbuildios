﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`3<System.Object,System.Object,System.Object>
struct FactoryNested_3_t3116483514;
// Zenject.IFactory`2<System.Object,System.Object>
struct IFactory_2_t972313871;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`3<System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`2<TParam1,TConcrete>)
extern "C"  void FactoryNested_3__ctor_m1830744183_gshared (FactoryNested_3_t3116483514 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_3__ctor_m1830744183(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_3_t3116483514 *, Il2CppObject*, const MethodInfo*))FactoryNested_3__ctor_m1830744183_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`3<System.Object,System.Object,System.Object>::Create(TParam1)
extern "C"  Il2CppObject * FactoryNested_3_Create_m1558019269_gshared (FactoryNested_3_t3116483514 * __this, Il2CppObject * ___param0, const MethodInfo* method);
#define FactoryNested_3_Create_m1558019269(__this, ___param0, method) ((  Il2CppObject * (*) (FactoryNested_3_t3116483514 *, Il2CppObject *, const MethodInfo*))FactoryNested_3_Create_m1558019269_gshared)(__this, ___param0, method)
