﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.FileWebRequest
struct FileWebRequest_t1669470913;
// System.Uri
struct Uri_t2776692961;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.String
struct String_t;
// System.Net.ICredentials
struct ICredentials_t2307785309;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1099177929;
// System.Net.IWebProxy
struct IWebProxy_t49946189;
// System.Exception
struct Exception_t1967233988;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;
// System.IO.Stream
struct Stream_t219029575;
// System.Net.WebResponse
struct WebResponse_t2411292415;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.FileWebRequest::.ctor(System.Uri)
extern "C"  void FileWebRequest__ctor_m1190066806 (FileWebRequest_t1669470913 * __this, Uri_t2776692961 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void FileWebRequest__ctor_m2417420612 (FileWebRequest_t1669470913 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m3587475368 (FileWebRequest_t1669470913 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.FileWebRequest::get_ConnectionGroupName()
extern "C"  String_t* FileWebRequest_get_ConnectionGroupName_m918981575 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_ConnectionGroupName(System.String)
extern "C"  void FileWebRequest_set_ConnectionGroupName_m804724754 (FileWebRequest_t1669470913 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.FileWebRequest::get_ContentLength()
extern "C"  int64_t FileWebRequest_get_ContentLength_m3959517096 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_ContentLength(System.Int64)
extern "C"  void FileWebRequest_set_ContentLength_m3507692309 (FileWebRequest_t1669470913 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.FileWebRequest::get_ContentType()
extern "C"  String_t* FileWebRequest_get_ContentType_m4002263790 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_ContentType(System.String)
extern "C"  void FileWebRequest_set_ContentType_m2123986763 (FileWebRequest_t1669470913 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.FileWebRequest::get_Credentials()
extern "C"  Il2CppObject * FileWebRequest_get_Credentials_m3151607268 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_Credentials(System.Net.ICredentials)
extern "C"  void FileWebRequest_set_Credentials_m2271052495 (FileWebRequest_t1669470913 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.FileWebRequest::get_Headers()
extern "C"  WebHeaderCollection_t1099177929 * FileWebRequest_get_Headers_m3065734382 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.FileWebRequest::get_Method()
extern "C"  String_t* FileWebRequest_get_Method_m2005929480 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_Method(System.String)
extern "C"  void FileWebRequest_set_Method_m339613475 (FileWebRequest_t1669470913 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.FileWebRequest::get_PreAuthenticate()
extern "C"  bool FileWebRequest_get_PreAuthenticate_m3086692006 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_PreAuthenticate(System.Boolean)
extern "C"  void FileWebRequest_set_PreAuthenticate_m3393511571 (FileWebRequest_t1669470913 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.FileWebRequest::get_Proxy()
extern "C"  Il2CppObject * FileWebRequest_get_Proxy_m469077010 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_Proxy(System.Net.IWebProxy)
extern "C"  void FileWebRequest_set_Proxy_m1837681983 (FileWebRequest_t1669470913 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.FileWebRequest::get_RequestUri()
extern "C"  Uri_t2776692961 * FileWebRequest_get_RequestUri_m3313865781 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.FileWebRequest::get_Timeout()
extern "C"  int32_t FileWebRequest_get_Timeout_m3552536393 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_Timeout(System.Int32)
extern "C"  void FileWebRequest_set_Timeout_m1454600950 (FileWebRequest_t1669470913 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.FileWebRequest::get_UseDefaultCredentials()
extern "C"  bool FileWebRequest_get_UseDefaultCredentials_m1021146416 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::set_UseDefaultCredentials(System.Boolean)
extern "C"  void FileWebRequest_set_UseDefaultCredentials_m3824180509 (FileWebRequest_t1669470913 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Net.FileWebRequest::GetMustImplement()
extern "C"  Exception_t1967233988 * FileWebRequest_GetMustImplement_m391298838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::Abort()
extern "C"  void FileWebRequest_Abort_m303714001 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.FileWebRequest::BeginGetRequestStream(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FileWebRequest_BeginGetRequestStream_m2530068115 (FileWebRequest_t1669470913 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FileWebRequest::EndGetRequestStream(System.IAsyncResult)
extern "C"  Stream_t219029575 * FileWebRequest_EndGetRequestStream_m5977400 (FileWebRequest_t1669470913 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FileWebRequest::GetRequestStream()
extern "C"  Stream_t219029575 * FileWebRequest_GetRequestStream_m3303490852 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FileWebRequest::GetRequestStreamInternal()
extern "C"  Stream_t219029575 * FileWebRequest_GetRequestStreamInternal_m1727879617 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.FileWebRequest::BeginGetResponse(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FileWebRequest_BeginGetResponse_m1046890263 (FileWebRequest_t1669470913 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.FileWebRequest::EndGetResponse(System.IAsyncResult)
extern "C"  WebResponse_t2411292415 * FileWebRequest_EndGetResponse_m1769167488 (FileWebRequest_t1669470913 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.FileWebRequest::GetResponse()
extern "C"  WebResponse_t2411292415 * FileWebRequest_GetResponse_m3466591250 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.FileWebRequest::GetResponseInternal()
extern "C"  WebResponse_t2411292415 * FileWebRequest_GetResponseInternal_m3074376367 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void FileWebRequest_GetObjectData_m1166847137 (FileWebRequest_t1669470913 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::Close()
extern "C"  void FileWebRequest_Close_m2365028249 (FileWebRequest_t1669470913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
