﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En333243017MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m349723013(__this, ___dictionary0, method) ((  void (*) (Enumerator_t464625499 *, Dictionary_2_t697597558 *, const MethodInfo*))Enumerator__ctor_m2536120371_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3469205436(__this, method) ((  Il2CppObject * (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1912820430_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2269556240(__this, method) ((  void (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1284658850_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4064135321(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1428556203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2445762456(__this, method) ((  Il2CppObject * (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3247488298_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2079045674(__this, method) ((  Il2CppObject * (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3738433852_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::MoveNext()
#define Enumerator_MoveNext_m2867760435(__this, method) ((  bool (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_MoveNext_m3400303246_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::get_Current()
#define Enumerator_get_Current_m565450124(__this, method) ((  KeyValuePair_2_t186128856  (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_get_Current_m3991428322_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2227337673(__this, method) ((  double (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_get_CurrentKey_m2296830939_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1487491117(__this, method) ((  String_t* (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_get_CurrentValue_m3846010303_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::Reset()
#define Enumerator_Reset_m689208407(__this, method) ((  void (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_Reset_m3277856261_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::VerifyState()
#define Enumerator_VerifyState_m3138973472(__this, method) ((  void (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_VerifyState_m4041479758_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4231336584(__this, method) ((  void (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_VerifyCurrent_m3956483638_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.String>::Dispose()
#define Enumerator_Dispose_m3427744615(__this, method) ((  void (*) (Enumerator_t464625499 *, const MethodInfo*))Enumerator_Dispose_m37300629_gshared)(__this, method)
