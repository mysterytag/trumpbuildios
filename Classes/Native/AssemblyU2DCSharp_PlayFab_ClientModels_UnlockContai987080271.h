﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UnlockContainerItemRequest
struct  UnlockContainerItemRequest_t987080271  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.UnlockContainerItemRequest::<ContainerItemId>k__BackingField
	String_t* ___U3CContainerItemIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.UnlockContainerItemRequest::<CatalogVersion>k__BackingField
	String_t* ___U3CCatalogVersionU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.UnlockContainerItemRequest::<CharacterId>k__BackingField
	String_t* ___U3CCharacterIdU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CContainerItemIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnlockContainerItemRequest_t987080271, ___U3CContainerItemIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CContainerItemIdU3Ek__BackingField_0() const { return ___U3CContainerItemIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CContainerItemIdU3Ek__BackingField_0() { return &___U3CContainerItemIdU3Ek__BackingField_0; }
	inline void set_U3CContainerItemIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CContainerItemIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContainerItemIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CCatalogVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnlockContainerItemRequest_t987080271, ___U3CCatalogVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CCatalogVersionU3Ek__BackingField_1() const { return ___U3CCatalogVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CCatalogVersionU3Ek__BackingField_1() { return &___U3CCatalogVersionU3Ek__BackingField_1; }
	inline void set_U3CCatalogVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CCatalogVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCatalogVersionU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CCharacterIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnlockContainerItemRequest_t987080271, ___U3CCharacterIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CCharacterIdU3Ek__BackingField_2() const { return ___U3CCharacterIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCharacterIdU3Ek__BackingField_2() { return &___U3CCharacterIdU3Ek__BackingField_2; }
	inline void set_U3CCharacterIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CCharacterIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterIdU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
