﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Func`2<System.String,System.String>
struct Func_2_t917545008;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t160061447;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Params
struct  Params_t2383406502  : public Il2CppObject
{
public:

public:
};

struct Params_t2383406502_StaticFields
{
public:
	// System.Boolean Params::initialized
	bool ___initialized_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Params::_parameters
	Dictionary_2_t2606186806 * ____parameters_1;
	// System.Func`2<System.String,System.String> Params::<>f__am$cache2
	Func_2_t917545008 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<System.String,System.Boolean> Params::<>f__am$cache3
	Func_2_t160061447 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<System.String,System.Boolean> Params::<>f__am$cache4
	Func_2_t160061447 * ___U3CU3Ef__amU24cache4_4;

public:
	inline static int32_t get_offset_of_initialized_0() { return static_cast<int32_t>(offsetof(Params_t2383406502_StaticFields, ___initialized_0)); }
	inline bool get_initialized_0() const { return ___initialized_0; }
	inline bool* get_address_of_initialized_0() { return &___initialized_0; }
	inline void set_initialized_0(bool value)
	{
		___initialized_0 = value;
	}

	inline static int32_t get_offset_of__parameters_1() { return static_cast<int32_t>(offsetof(Params_t2383406502_StaticFields, ____parameters_1)); }
	inline Dictionary_2_t2606186806 * get__parameters_1() const { return ____parameters_1; }
	inline Dictionary_2_t2606186806 ** get_address_of__parameters_1() { return &____parameters_1; }
	inline void set__parameters_1(Dictionary_2_t2606186806 * value)
	{
		____parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&____parameters_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Params_t2383406502_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t917545008 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t917545008 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t917545008 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Params_t2383406502_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t160061447 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t160061447 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t160061447 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(Params_t2383406502_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t160061447 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t160061447 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t160061447 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
