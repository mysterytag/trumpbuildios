﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VKProfileInfo
struct VKProfileInfo_t3473649666;

#include "codegen/il2cpp-codegen.h"

// System.Void VKProfileInfo::.ctor()
extern "C"  void VKProfileInfo__ctor_m979866537 (VKProfileInfo_t3473649666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
