﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<System.Byte>
struct Subject_1_t421761145;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Byte>
struct IObserver_1_t695725428;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Subject`1<System.Byte>::.ctor()
extern "C"  void Subject_1__ctor_m2689976865_gshared (Subject_1_t421761145 * __this, const MethodInfo* method);
#define Subject_1__ctor_m2689976865(__this, method) ((  void (*) (Subject_1_t421761145 *, const MethodInfo*))Subject_1__ctor_m2689976865_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Byte>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1412271051_gshared (Subject_1_t421761145 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m1412271051(__this, method) ((  bool (*) (Subject_1_t421761145 *, const MethodInfo*))Subject_1_get_HasObservers_m1412271051_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Byte>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m971972843_gshared (Subject_1_t421761145 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m971972843(__this, method) ((  void (*) (Subject_1_t421761145 *, const MethodInfo*))Subject_1_OnCompleted_m971972843_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Byte>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m993053080_gshared (Subject_1_t421761145 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m993053080(__this, ___error0, method) ((  void (*) (Subject_1_t421761145 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m993053080_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<System.Byte>::OnNext(T)
extern "C"  void Subject_1_OnNext_m2328158345_gshared (Subject_1_t421761145 * __this, uint8_t ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m2328158345(__this, ___value0, method) ((  void (*) (Subject_1_t421761145 *, uint8_t, const MethodInfo*))Subject_1_OnNext_m2328158345_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<System.Byte>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m1101121120_gshared (Subject_1_t421761145 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m1101121120(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t421761145 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m1101121120_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<System.Byte>::Dispose()
extern "C"  void Subject_1_Dispose_m3690705438_gshared (Subject_1_t421761145 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m3690705438(__this, method) ((  void (*) (Subject_1_t421761145 *, const MethodInfo*))Subject_1_Dispose_m3690705438_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Byte>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m3041680807_gshared (Subject_1_t421761145 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m3041680807(__this, method) ((  void (*) (Subject_1_t421761145 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3041680807_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Byte>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m4043881474_gshared (Subject_1_t421761145 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m4043881474(__this, method) ((  bool (*) (Subject_1_t421761145 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m4043881474_gshared)(__this, method)
