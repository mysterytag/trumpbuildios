﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeWhileObservable`1<System.Object>
struct TakeWhileObservable_1_t3525448012;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Func`3<System.Object,System.Int32,System.Boolean>
struct Func_3_t3064567499;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TakeWhileObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void TakeWhileObservable_1__ctor_m3525215727_gshared (TakeWhileObservable_1_t3525448012 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, const MethodInfo* method);
#define TakeWhileObservable_1__ctor_m3525215727(__this, ___source0, ___predicate1, method) ((  void (*) (TakeWhileObservable_1_t3525448012 *, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))TakeWhileObservable_1__ctor_m3525215727_gshared)(__this, ___source0, ___predicate1, method)
// System.Void UniRx.Operators.TakeWhileObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void TakeWhileObservable_1__ctor_m2999732631_gshared (TakeWhileObservable_1_t3525448012 * __this, Il2CppObject* ___source0, Func_3_t3064567499 * ___predicateWithIndex1, const MethodInfo* method);
#define TakeWhileObservable_1__ctor_m2999732631(__this, ___source0, ___predicateWithIndex1, method) ((  void (*) (TakeWhileObservable_1_t3525448012 *, Il2CppObject*, Func_3_t3064567499 *, const MethodInfo*))TakeWhileObservable_1__ctor_m2999732631_gshared)(__this, ___source0, ___predicateWithIndex1, method)
// System.IDisposable UniRx.Operators.TakeWhileObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeWhileObservable_1_SubscribeCore_m420399844_gshared (TakeWhileObservable_1_t3525448012 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TakeWhileObservable_1_SubscribeCore_m420399844(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TakeWhileObservable_1_t3525448012 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeWhileObservable_1_SubscribeCore_m420399844_gshared)(__this, ___observer0, ___cancel1, method)
