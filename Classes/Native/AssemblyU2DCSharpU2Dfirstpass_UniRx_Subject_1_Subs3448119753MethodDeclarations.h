﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subscription_t3448119753;
// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t3311940555;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t3585904838;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1051003500_gshared (Subscription_t3448119753 * __this, Subject_1_t3311940555 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1051003500(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t3448119753 *, Subject_1_t3311940555 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1051003500_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subscription_Dispose_m3809482762_gshared (Subscription_t3448119753 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3809482762(__this, method) ((  void (*) (Subscription_t3448119753 *, const MethodInfo*))Subscription_Dispose_m3809482762_gshared)(__this, method)
