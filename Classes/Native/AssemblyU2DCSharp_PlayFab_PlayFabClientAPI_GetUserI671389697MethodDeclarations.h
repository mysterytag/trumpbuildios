﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetUserInventoryResponseCallback
struct GetUserInventoryResponseCallback_t671389697;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetUserInventoryRequest
struct GetUserInventoryRequest_t2337033188;
// PlayFab.ClientModels.GetUserInventoryResult
struct GetUserInventoryResult_t1496009288;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserInve2337033188.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserInve1496009288.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetUserInventoryResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetUserInventoryResponseCallback__ctor_m3065638000 (GetUserInventoryResponseCallback_t671389697 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserInventoryResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetUserInventoryRequest,PlayFab.ClientModels.GetUserInventoryResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetUserInventoryResponseCallback_Invoke_m1232418653 (GetUserInventoryResponseCallback_t671389697 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserInventoryRequest_t2337033188 * ___request2, GetUserInventoryResult_t1496009288 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetUserInventoryResponseCallback_t671389697(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetUserInventoryRequest_t2337033188 * ___request2, GetUserInventoryResult_t1496009288 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetUserInventoryResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetUserInventoryRequest,PlayFab.ClientModels.GetUserInventoryResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetUserInventoryResponseCallback_BeginInvoke_m2499501386 (GetUserInventoryResponseCallback_t671389697 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserInventoryRequest_t2337033188 * ___request2, GetUserInventoryResult_t1496009288 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserInventoryResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetUserInventoryResponseCallback_EndInvoke_m2052396672 (GetUserInventoryResponseCallback_t671389697 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
