﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectSingletonProvider
struct  GameObjectSingletonProvider_t177180832  : public ProviderBase_t1627494391
{
public:
	// System.String Zenject.GameObjectSingletonProvider::_name
	String_t* ____name_2;
	// Zenject.DiContainer Zenject.GameObjectSingletonProvider::_container
	DiContainer_t2383114449 * ____container_3;
	// System.Type Zenject.GameObjectSingletonProvider::_componentType
	Type_t * ____componentType_4;
	// System.Object Zenject.GameObjectSingletonProvider::_instance
	Il2CppObject * ____instance_5;

public:
	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(GameObjectSingletonProvider_t177180832, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier(&____name_2, value);
	}

	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(GameObjectSingletonProvider_t177180832, ____container_3)); }
	inline DiContainer_t2383114449 * get__container_3() const { return ____container_3; }
	inline DiContainer_t2383114449 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t2383114449 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier(&____container_3, value);
	}

	inline static int32_t get_offset_of__componentType_4() { return static_cast<int32_t>(offsetof(GameObjectSingletonProvider_t177180832, ____componentType_4)); }
	inline Type_t * get__componentType_4() const { return ____componentType_4; }
	inline Type_t ** get_address_of__componentType_4() { return &____componentType_4; }
	inline void set__componentType_4(Type_t * value)
	{
		____componentType_4 = value;
		Il2CppCodeGenWriteBarrier(&____componentType_4, value);
	}

	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(GameObjectSingletonProvider_t177180832, ____instance_5)); }
	inline Il2CppObject * get__instance_5() const { return ____instance_5; }
	inline Il2CppObject ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(Il2CppObject * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier(&____instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
