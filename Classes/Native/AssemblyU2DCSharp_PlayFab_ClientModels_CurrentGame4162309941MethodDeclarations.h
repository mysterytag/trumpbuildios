﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CurrentGamesResult
struct CurrentGamesResult_t4162309941;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GameInfo>
struct List_1_t2564353257;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.CurrentGamesResult::.ctor()
extern "C"  void CurrentGamesResult__ctor_m2319428136 (CurrentGamesResult_t4162309941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GameInfo> PlayFab.ClientModels.CurrentGamesResult::get_Games()
extern "C"  List_1_t2564353257 * CurrentGamesResult_get_Games_m725776509 (CurrentGamesResult_t4162309941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CurrentGamesResult::set_Games(System.Collections.Generic.List`1<PlayFab.ClientModels.GameInfo>)
extern "C"  void CurrentGamesResult_set_Games_m2923608396 (CurrentGamesResult_t4162309941 * __this, List_1_t2564353257 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.CurrentGamesResult::get_PlayerCount()
extern "C"  int32_t CurrentGamesResult_get_PlayerCount_m1586154393 (CurrentGamesResult_t4162309941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CurrentGamesResult::set_PlayerCount(System.Int32)
extern "C"  void CurrentGamesResult_set_PlayerCount_m2757241320 (CurrentGamesResult_t4162309941 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.CurrentGamesResult::get_GameCount()
extern "C"  int32_t CurrentGamesResult_get_GameCount_m1354024936 (CurrentGamesResult_t4162309941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CurrentGamesResult::set_GameCount(System.Int32)
extern "C"  void CurrentGamesResult_set_GameCount_m2480058167 (CurrentGamesResult_t4162309941 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
