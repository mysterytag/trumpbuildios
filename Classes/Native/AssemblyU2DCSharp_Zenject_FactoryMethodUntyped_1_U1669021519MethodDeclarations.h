﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>
struct U3CValidateU3Ec__Iterator45_t1669021519;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::.ctor()
extern "C"  void U3CValidateU3Ec__Iterator45__ctor_m3732071676_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator45__ctor_m3732071676(__this, method) ((  void (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))U3CValidateU3Ec__Iterator45__ctor_m3732071676_gshared)(__this, method)
// Zenject.ZenjectResolveException Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateU3Ec__Iterator45_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m2958685317_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator45_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m2958685317(__this, method) ((  ZenjectResolveException_t1201052999 * (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))U3CValidateU3Ec__Iterator45_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m2958685317_gshared)(__this, method)
// System.Object Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateU3Ec__Iterator45_System_Collections_IEnumerator_get_Current_m1332034346_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator45_System_Collections_IEnumerator_get_Current_m1332034346(__this, method) ((  Il2CppObject * (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))U3CValidateU3Ec__Iterator45_System_Collections_IEnumerator_get_Current_m1332034346_gshared)(__this, method)
// System.Collections.IEnumerator Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateU3Ec__Iterator45_System_Collections_IEnumerable_GetEnumerator_m831256805_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator45_System_Collections_IEnumerable_GetEnumerator_m831256805(__this, method) ((  Il2CppObject * (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))U3CValidateU3Ec__Iterator45_System_Collections_IEnumerable_GetEnumerator_m831256805_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateU3Ec__Iterator45_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m2937777190_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator45_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m2937777190(__this, method) ((  Il2CppObject* (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))U3CValidateU3Ec__Iterator45_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m2937777190_gshared)(__this, method)
// System.Boolean Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::MoveNext()
extern "C"  bool U3CValidateU3Ec__Iterator45_MoveNext_m2737589880_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator45_MoveNext_m2737589880(__this, method) ((  bool (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))U3CValidateU3Ec__Iterator45_MoveNext_m2737589880_gshared)(__this, method)
// System.Void Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::Dispose()
extern "C"  void U3CValidateU3Ec__Iterator45_Dispose_m121471545_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator45_Dispose_m121471545(__this, method) ((  void (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))U3CValidateU3Ec__Iterator45_Dispose_m121471545_gshared)(__this, method)
// System.Void Zenject.FactoryMethodUntyped`1/<Validate>c__Iterator45<System.Object>::Reset()
extern "C"  void U3CValidateU3Ec__Iterator45_Reset_m1378504617_gshared (U3CValidateU3Ec__Iterator45_t1669021519 * __this, const MethodInfo* method);
#define U3CValidateU3Ec__Iterator45_Reset_m1378504617(__this, method) ((  void (*) (U3CValidateU3Ec__Iterator45_t1669021519 *, const MethodInfo*))U3CValidateU3Ec__Iterator45_Reset_m1378504617_gshared)(__this, method)
