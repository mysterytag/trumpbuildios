﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<SetTable>c__AnonStorey15A
struct U3CSetTableU3Ec__AnonStorey15A_t1809656844;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void TableButtons/<SetTable>c__AnonStorey15A::.ctor()
extern "C"  void U3CSetTableU3Ec__AnonStorey15A__ctor_m1225324739 (U3CSetTableU3Ec__AnonStorey15A_t1809656844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TableButtons/<SetTable>c__AnonStorey15A::<>m__22D()
extern "C"  bool U3CSetTableU3Ec__AnonStorey15A_U3CU3Em__22D_m3225858662 (U3CSetTableU3Ec__AnonStorey15A_t1809656844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<SetTable>c__AnonStorey15A::<>m__22E()
extern "C"  void U3CSetTableU3Ec__AnonStorey15A_U3CU3Em__22E_m3553024347 (U3CSetTableU3Ec__AnonStorey15A_t1809656844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<SetTable>c__AnonStorey15A::<>m__22F(System.String)
extern "C"  void U3CSetTableU3Ec__AnonStorey15A_U3CU3Em__22F_m4289458022 (U3CSetTableU3Ec__AnonStorey15A_t1809656844 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<SetTable>c__AnonStorey15A::<>m__230(System.Boolean)
extern "C"  void U3CSetTableU3Ec__AnonStorey15A_U3CU3Em__230_m3630120860 (U3CSetTableU3Ec__AnonStorey15A_t1809656844 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<SetTable>c__AnonStorey15A::<>m__231()
extern "C"  void U3CSetTableU3Ec__AnonStorey15A_U3CU3Em__231_m3553034918 (U3CSetTableU3Ec__AnonStorey15A_t1809656844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<SetTable>c__AnonStorey15A::<>m__232(System.String)
extern "C"  void U3CSetTableU3Ec__AnonStorey15A_U3CU3Em__232_m2968549371 (U3CSetTableU3Ec__AnonStorey15A_t1809656844 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
