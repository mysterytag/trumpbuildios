﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171
struct U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171/<WhenInjectedInto>c__AnonStorey172
struct  U3CWhenInjectedIntoU3Ec__AnonStorey172_t2299533245  : public Il2CppObject
{
public:
	// Zenject.InjectContext Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171/<WhenInjectedInto>c__AnonStorey172::r
	InjectContext_t3456483891 * ___r_0;
	// Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171 Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171/<WhenInjectedInto>c__AnonStorey172::<>f__ref$369
	U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244 * ___U3CU3Ef__refU24369_1;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(U3CWhenInjectedIntoU3Ec__AnonStorey172_t2299533245, ___r_0)); }
	inline InjectContext_t3456483891 * get_r_0() const { return ___r_0; }
	inline InjectContext_t3456483891 ** get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(InjectContext_t3456483891 * value)
	{
		___r_0 = value;
		Il2CppCodeGenWriteBarrier(&___r_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24369_1() { return static_cast<int32_t>(offsetof(U3CWhenInjectedIntoU3Ec__AnonStorey172_t2299533245, ___U3CU3Ef__refU24369_1)); }
	inline U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244 * get_U3CU3Ef__refU24369_1() const { return ___U3CU3Ef__refU24369_1; }
	inline U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244 ** get_address_of_U3CU3Ef__refU24369_1() { return &___U3CU3Ef__refU24369_1; }
	inline void set_U3CU3Ef__refU24369_1(U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244 * value)
	{
		___U3CU3Ef__refU24369_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24369_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
