﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithGameCenter>c__AnonStorey63
struct U3CLoginWithGameCenterU3Ec__AnonStorey63_t2410549440;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithGameCenter>c__AnonStorey63::.ctor()
extern "C"  void U3CLoginWithGameCenterU3Ec__AnonStorey63__ctor_m3184688787 (U3CLoginWithGameCenterU3Ec__AnonStorey63_t2410549440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithGameCenter>c__AnonStorey63::<>m__50(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithGameCenterU3Ec__AnonStorey63_U3CU3Em__50_m157994892 (U3CLoginWithGameCenterU3Ec__AnonStorey63_t2410549440 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
