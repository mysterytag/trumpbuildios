﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t1539766316;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1306794257.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m391077881_gshared (Enumerator_t1306794258 * __this, Dictionary_2_t1539766316 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m391077881(__this, ___host0, method) ((  void (*) (Enumerator_t1306794258 *, Dictionary_2_t1539766316 *, const MethodInfo*))Enumerator__ctor_m391077881_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3672404562_gshared (Enumerator_t1306794258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3672404562(__this, method) ((  Il2CppObject * (*) (Enumerator_t1306794258 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3672404562_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3763734300_gshared (Enumerator_t1306794258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3763734300(__this, method) ((  void (*) (Enumerator_t1306794258 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3763734300_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m3951396571_gshared (Enumerator_t1306794258 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3951396571(__this, method) ((  void (*) (Enumerator_t1306794258 *, const MethodInfo*))Enumerator_Dispose_m3951396571_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m280957772_gshared (Enumerator_t1306794258 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m280957772(__this, method) ((  bool (*) (Enumerator_t1306794258 *, const MethodInfo*))Enumerator_MoveNext_m280957772_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2620786764_gshared (Enumerator_t1306794258 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2620786764(__this, method) ((  Il2CppObject * (*) (Enumerator_t1306794258 *, const MethodInfo*))Enumerator_get_Current_m2620786764_gshared)(__this, method)
