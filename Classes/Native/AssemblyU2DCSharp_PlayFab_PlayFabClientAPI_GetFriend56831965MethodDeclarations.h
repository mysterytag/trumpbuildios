﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerRequestCallback
struct GetFriendLeaderboardAroundPlayerRequestCallback_t56831965;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest
struct GetFriendLeaderboardAroundPlayerRequest_t16887688;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLead16887688.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFriendLeaderboardAroundPlayerRequestCallback__ctor_m3478598124 (GetFriendLeaderboardAroundPlayerRequestCallback_t56831965 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest,System.Object)
extern "C"  void GetFriendLeaderboardAroundPlayerRequestCallback_Invoke_m992638367 (GetFriendLeaderboardAroundPlayerRequestCallback_t56831965 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundPlayerRequest_t16887688 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardAroundPlayerRequestCallback_t56831965(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundPlayerRequest_t16887688 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFriendLeaderboardAroundPlayerRequestCallback_BeginInvoke_m1973404158 (GetFriendLeaderboardAroundPlayerRequestCallback_t56831965 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundPlayerRequest_t16887688 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundPlayerRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetFriendLeaderboardAroundPlayerRequestCallback_EndInvoke_m1787329020 (GetFriendLeaderboardAroundPlayerRequestCallback_t56831965 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
