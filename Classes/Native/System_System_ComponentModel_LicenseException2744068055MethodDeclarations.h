﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.LicenseException
struct LicenseException_t2744068055;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.ComponentModel.LicenseException::.ctor(System.Type)
extern "C"  void LicenseException__ctor_m1956356750 (LicenseException_t2744068055 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.LicenseException::.ctor(System.Type,System.Object)
extern "C"  void LicenseException__ctor_m2550357212 (LicenseException_t2744068055 * __this, Type_t * ___type0, Il2CppObject * ___instance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.LicenseException::.ctor(System.Type,System.Object,System.String)
extern "C"  void LicenseException__ctor_m4193965080 (LicenseException_t2744068055 * __this, Type_t * ___type0, Il2CppObject * ___instance1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.LicenseException::.ctor(System.Type,System.Object,System.String,System.Exception)
extern "C"  void LicenseException__ctor_m1223205918 (LicenseException_t2744068055 * __this, Type_t * ___type0, Il2CppObject * ___instance1, String_t* ___message2, Exception_t1967233988 * ___innerException3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.LicenseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LicenseException__ctor_m905917132 (LicenseException_t2744068055 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.LicenseException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LicenseException_GetObjectData_m3413569065 (LicenseException_t2744068055 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.ComponentModel.LicenseException::get_LicensedType()
extern "C"  Type_t * LicenseException_get_LicensedType_m770128217 (LicenseException_t2744068055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
