﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper
struct AdToAppAndroidWrapper_t877850908;
// System.String
struct String_t;
// AdToApp.AndroidWrapper.ATAInterstitialAdListener
struct ATAInterstitialAdListener_t382012998;
// AdToApp.AndroidWrapper.ATABannerAdListener
struct ATABannerAdListener_t2854275270;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrappe382012998.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp2854275270.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::.ctor()
extern "C"  void AdToAppAndroidWrapper__ctor_m3587631719 (AdToAppAndroidWrapper_t877850908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper::get_Version()
extern "C"  String_t* AdToAppAndroidWrapper_get_Version_m2578489521 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::InitializeSDK(System.String,System.String)
extern "C"  void AdToAppAndroidWrapper_InitializeSDK_m2745794255 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, String_t* ___sdkKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::SetLogging(System.Boolean)
extern "C"  void AdToAppAndroidWrapper_SetLogging_m285204049 (Il2CppObject * __this /* static, unused */, bool ___logging0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowInterstitial(System.String)
extern "C"  void AdToAppAndroidWrapper_ShowInterstitial_m2345890876 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::SetInterstitialAdListeners(AdToApp.AndroidWrapper.ATAInterstitialAdListener)
extern "C"  void AdToAppAndroidWrapper_SetInterstitialAdListeners_m2032796921 (Il2CppObject * __this /* static, unused */, ATAInterstitialAdListener_t382012998 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowBanner(System.Int32,System.Int32)
extern "C"  void AdToAppAndroidWrapper_ShowBanner_m218655392 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowBanner(System.Int32,System.Int32,System.String)
extern "C"  void AdToAppAndroidWrapper_ShowBanner_m2996992732 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, String_t* ___bannerSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowBannerAtPositon(System.String)
extern "C"  void AdToAppAndroidWrapper_ShowBannerAtPositon_m795426811 (Il2CppObject * __this /* static, unused */, String_t* ___bannerPositon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowBannerAtPositon(System.String,System.String)
extern "C"  void AdToAppAndroidWrapper_ShowBannerAtPositon_m2909361399 (Il2CppObject * __this /* static, unused */, String_t* ___bannerPositon0, String_t* ___bannerSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::RemoveAllBanners()
extern "C"  void AdToAppAndroidWrapper_RemoveAllBanners_m3826459399 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::SetBannerAdListeners(AdToApp.AndroidWrapper.ATABannerAdListener)
extern "C"  void AdToAppAndroidWrapper_SetBannerAdListeners_m2865466041 (Il2CppObject * __this /* static, unused */, ATABannerAdListener_t2854275270 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::onPause(System.Boolean)
extern "C"  void AdToAppAndroidWrapper_onPause_m3724000435 (Il2CppObject * __this /* static, unused */, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AndroidWrapper.AdToAppAndroidWrapper::HasInterstitial(System.String)
extern "C"  bool AdToAppAndroidWrapper_HasInterstitial_m3242219667 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::LoadNextBanner()
extern "C"  void AdToAppAndroidWrapper_LoadNextBanner_m4103270210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::SetBannerRefreshInterval(System.Double)
extern "C"  void AdToAppAndroidWrapper_SetBannerRefreshInterval_m1756139379 (Il2CppObject * __this /* static, unused */, double ___refreshInterval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::CallWrapperMethod(System.String,System.Object[])
extern "C"  void AdToAppAndroidWrapper_CallWrapperMethod_m2056165811 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::CallWrapperMethodWithContext(System.Action`1<System.Object>)
extern "C"  void AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762 (Il2CppObject * __this /* static, unused */, Action_1_t985559125 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable AdToApp.AndroidWrapper.AdToAppAndroidWrapper::GetAndroidContext()
extern "C"  Il2CppObject * AdToAppAndroidWrapper_GetAndroidContext_m4026730848 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper::NormalizeAdContentType(System.String)
extern "C"  String_t* AdToAppAndroidWrapper_NormalizeAdContentType_m4118584223 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::setTestMode(System.Boolean)
extern "C"  void AdToAppAndroidWrapper_setTestMode_m3263678259 (Il2CppObject * __this /* static, unused */, bool ___isEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper::getRewardedCurrency()
extern "C"  String_t* AdToAppAndroidWrapper_getRewardedCurrency_m2026025751 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper::getRewardedValue()
extern "C"  String_t* AdToAppAndroidWrapper_getRewardedValue_m3410800141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::setTargetingParam(System.String,System.String)
extern "C"  void AdToAppAndroidWrapper_setTargetingParam_m2947927131 (Il2CppObject * __this /* static, unused */, String_t* ___parameterName0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<RemoveAllBanners>m__5(System.Object)
extern "C"  void AdToAppAndroidWrapper_U3CRemoveAllBannersU3Em__5_m1653727735 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___wrapperContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<onPause>m__6(System.Object)
extern "C"  void AdToAppAndroidWrapper_U3ConPauseU3Em__6_m562301251 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___wrapperContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<onPause>m__7(System.Object)
extern "C"  void AdToAppAndroidWrapper_U3ConPauseU3Em__7_m51767074 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___wrapperContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
