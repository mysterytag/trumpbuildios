﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<IProcess>::.ctor()
#define LinkedList_1__ctor_m346782520(__this, method) ((  void (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m2079627800(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1471399664 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4099443464(__this, ___value0, method) ((  void (*) (LinkedList_1_t1471399664 *, Il2CppObject *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m885796813(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1471399664 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<IProcess>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1738245281(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<IProcess>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2044091528(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<IProcess>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m40308972(__this, method) ((  bool (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<IProcess>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m3018234065(__this, method) ((  bool (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<IProcess>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3010730415(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m1330812980(__this, ___node0, method) ((  void (*) (LinkedList_1_t1471399664 *, LinkedListNode_1_t3728228034 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<IProcess>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
#define LinkedList_1_AddBefore_m1585013842(__this, ___node0, ___value1, method) ((  LinkedListNode_1_t3728228034 * (*) (LinkedList_1_t1471399664 *, LinkedListNode_1_t3728228034 *, Il2CppObject *, const MethodInfo*))LinkedList_1_AddBefore_m1086189582_gshared)(__this, ___node0, ___value1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<IProcess>::AddLast(T)
#define LinkedList_1_AddLast_m4013726181(__this, ___value0, method) ((  LinkedListNode_1_t3728228034 * (*) (LinkedList_1_t1471399664 *, Il2CppObject *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::Clear()
#define LinkedList_1_Clear_m1399104898(__this, method) ((  void (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<IProcess>::Contains(T)
#define LinkedList_1_Contains_m917723016(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1471399664 *, Il2CppObject *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m2521200184(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1471399664 *, IProcessU5BU5D_t1995942851*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<IProcess>::Find(T)
#define LinkedList_1_Find_m2937851690(__this, ___value0, method) ((  LinkedListNode_1_t3728228034 * (*) (LinkedList_1_t1471399664 *, Il2CppObject *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<IProcess>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m881768088(__this, method) ((  Enumerator_t2908979376  (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m3647359349(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1471399664 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m2288474127(__this, ___sender0, method) ((  void (*) (LinkedList_1_t1471399664 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<IProcess>::Remove(T)
#define LinkedList_1_Remove_m241719683(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1471399664 *, Il2CppObject *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m1877524676(__this, ___node0, method) ((  void (*) (LinkedList_1_t1471399664 *, LinkedListNode_1_t3728228034 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m166859009(__this, method) ((  void (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_RemoveFirst_m1652892321_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<IProcess>::RemoveLast()
#define LinkedList_1_RemoveLast_m862534343(__this, method) ((  void (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<IProcess>::get_Count()
#define LinkedList_1_get_Count_m4220539403(__this, method) ((  int32_t (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<IProcess>::get_First()
#define LinkedList_1_get_First_m1378107812(__this, method) ((  LinkedListNode_1_t3728228034 * (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<IProcess>::get_Last()
#define LinkedList_1_get_Last_m2210359898(__this, method) ((  LinkedListNode_1_t3728228034 * (*) (LinkedList_1_t1471399664 *, const MethodInfo*))LinkedList_1_get_Last_m270176030_gshared)(__this, method)
