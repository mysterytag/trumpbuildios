﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171
struct U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171::.ctor()
extern "C"  void U3CWhenInjectedIntoU3Ec__AnonStorey171__ctor_m2203715796 (U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171::<>m__28F(Zenject.InjectContext)
extern "C"  bool U3CWhenInjectedIntoU3Ec__AnonStorey171_U3CU3Em__28F_m4193738434 (U3CWhenInjectedIntoU3Ec__AnonStorey171_t2299533244 * __this, InjectContext_t3456483891 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
