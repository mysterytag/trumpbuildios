﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<UpdateCityLevel>c__IteratorE
struct U3CUpdateCityLevelU3Ec__IteratorE_t308347614;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<UpdateCityLevel>c__IteratorE::.ctor()
extern "C"  void U3CUpdateCityLevelU3Ec__IteratorE__ctor_m3864002910 (U3CUpdateCityLevelU3Ec__IteratorE_t308347614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<UpdateCityLevel>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateCityLevelU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m551612350 (U3CUpdateCityLevelU3Ec__IteratorE_t308347614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<UpdateCityLevel>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateCityLevelU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m2679517010 (U3CUpdateCityLevelU3Ec__IteratorE_t308347614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<UpdateCityLevel>c__IteratorE::MoveNext()
extern "C"  bool U3CUpdateCityLevelU3Ec__IteratorE_MoveNext_m2523271614 (U3CUpdateCityLevelU3Ec__IteratorE_t308347614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<UpdateCityLevel>c__IteratorE::Dispose()
extern "C"  void U3CUpdateCityLevelU3Ec__IteratorE_Dispose_m2353335835 (U3CUpdateCityLevelU3Ec__IteratorE_t308347614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<UpdateCityLevel>c__IteratorE::Reset()
extern "C"  void U3CUpdateCityLevelU3Ec__IteratorE_Reset_m1510435851 (U3CUpdateCityLevelU3Ec__IteratorE_t308347614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
