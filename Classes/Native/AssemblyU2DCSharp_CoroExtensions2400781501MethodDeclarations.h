﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IProcess
struct IProcess_t4026237414;
// IProcessExecution
struct IProcessExecution_t2362207186;

#include "codegen/il2cpp-codegen.h"

// IProcess CoroExtensions::Wait(IProcessExecution)
extern "C"  Il2CppObject * CoroExtensions_Wait_m2317910094 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___exec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
