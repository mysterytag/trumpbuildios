﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameData
struct GameData_t2589969116;

#include "UnityEngine_UnityEngine_ScriptableObject184905905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDataDescriptor
struct  GameDataDescriptor_t594253675  : public ScriptableObject_t184905905
{
public:
	// GameData GameDataDescriptor::data
	GameData_t2589969116 * ___data_2;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(GameDataDescriptor_t594253675, ___data_2)); }
	inline GameData_t2589969116 * get_data_2() const { return ___data_2; }
	inline GameData_t2589969116 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(GameData_t2589969116 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier(&___data_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
