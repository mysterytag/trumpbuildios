﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PaymentOption>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m145033744(__this, ___l0, method) ((  void (*) (Enumerator_t172016852 *, List_1_t2086233860 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PaymentOption>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2645557314(__this, method) ((  void (*) (Enumerator_t172016852 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PaymentOption>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1036738168(__this, method) ((  Il2CppObject * (*) (Enumerator_t172016852 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PaymentOption>::Dispose()
#define Enumerator_Dispose_m679025653(__this, method) ((  void (*) (Enumerator_t172016852 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PaymentOption>::VerifyState()
#define Enumerator_VerifyState_m925096110(__this, method) ((  void (*) (Enumerator_t172016852 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PaymentOption>::MoveNext()
#define Enumerator_MoveNext_m2388399218(__this, method) ((  bool (*) (Enumerator_t172016852 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.PaymentOption>::get_Current()
#define Enumerator_get_Current_m1674987015(__this, method) ((  PaymentOption_t1289274891 * (*) (Enumerator_t172016852 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
