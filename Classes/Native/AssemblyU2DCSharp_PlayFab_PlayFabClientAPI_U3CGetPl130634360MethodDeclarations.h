﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromGoogleIDs>c__AnonStorey70
struct U3CGetPlayFabIDsFromGoogleIDsU3Ec__AnonStorey70_t130634360;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromGoogleIDs>c__AnonStorey70::.ctor()
extern "C"  void U3CGetPlayFabIDsFromGoogleIDsU3Ec__AnonStorey70__ctor_m1952449595 (U3CGetPlayFabIDsFromGoogleIDsU3Ec__AnonStorey70_t130634360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromGoogleIDs>c__AnonStorey70::<>m__5D(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPlayFabIDsFromGoogleIDsU3Ec__AnonStorey70_U3CU3Em__5D_m3593774152 (U3CGetPlayFabIDsFromGoogleIDsU3Ec__AnonStorey70_t130634360 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
