﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DoObservable`1<System.Object>
struct DoObservable_1_t911152645;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DoObservable`1/Do<System.Object>
struct  Do_t3185263597  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DoObservable`1<T> UniRx.Operators.DoObservable`1/Do::parent
	DoObservable_1_t911152645 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Do_t3185263597, ___parent_2)); }
	inline DoObservable_1_t911152645 * get_parent_2() const { return ___parent_2; }
	inline DoObservable_1_t911152645 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DoObservable_1_t911152645 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
