﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_t2750076620;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass14_0__ctor_m1067789419 (U3CU3Ec__DisplayClass14_0_t2750076620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern "C"  Vector2_t3525329788  U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m3972882208 (U3CU3Ec__DisplayClass14_0_t2750076620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m543544308 (U3CU3Ec__DisplayClass14_0_t2750076620 * __this, Vector2_t3525329788  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
