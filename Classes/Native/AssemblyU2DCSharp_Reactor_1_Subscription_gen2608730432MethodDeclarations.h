﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<System.Double>
struct Subscription_t2608730433;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<System.Double>::.ctor()
extern "C"  void Subscription__ctor_m1162570650_gshared (Subscription_t2608730433 * __this, const MethodInfo* method);
#define Subscription__ctor_m1162570650(__this, method) ((  void (*) (Subscription_t2608730433 *, const MethodInfo*))Subscription__ctor_m1162570650_gshared)(__this, method)
// System.Void Reactor`1/Subscription<System.Double>::Dispose()
extern "C"  void Subscription_Dispose_m437180759_gshared (Subscription_t2608730433 * __this, const MethodInfo* method);
#define Subscription_Dispose_m437180759(__this, method) ((  void (*) (Subscription_t2608730433 *, const MethodInfo*))Subscription_Dispose_m437180759_gshared)(__this, method)
