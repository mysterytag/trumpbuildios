﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"

// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m2336549733_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m2336549733(__this, method) ((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m2336549733_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  KeyValuePair_2_t3312956448  U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m690243694_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m690243694(__this, method) ((  KeyValuePair_2_t3312956448  (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m690243694_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m270576619_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m270576619(__this, method) ((  Il2CppObject * (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m270576619_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m1517105100_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m1517105100(__this, method) ((  Il2CppObject * (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m1517105100_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1072048833_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1072048833(__this, method) ((  Il2CppObject* (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1072048833_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1198816055_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1198816055(__this, method) ((  bool (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1198816055_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m3349647970_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m3349647970(__this, method) ((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m3349647970_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m4277949970_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m4277949970(__this, method) ((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m4277949970_gshared)(__this, method)
