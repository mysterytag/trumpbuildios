﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70
struct U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey70__ctor_m3320012066 (U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey70::<>m__8D(System.Action`1<System.TimeSpan>)
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey70_U3CU3Em__8D_m809751865 (U3CSubscribeCoreU3Ec__AnonStorey70_t1995955663 * __this, Action_1_t912315597 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
