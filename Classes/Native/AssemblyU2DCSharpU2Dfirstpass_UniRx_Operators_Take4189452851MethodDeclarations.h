﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>
struct Take__t4189452851;
// UniRx.Operators.TakeObservable`1<UniRx.Unit>
struct TakeObservable_1_t505031811;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take___ctor_m3614515988_gshared (Take__t4189452851 * __this, TakeObservable_1_t505031811 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Take___ctor_m3614515988(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Take__t4189452851 *, TakeObservable_1_t505031811 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Take___ctor_m3614515988_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::Run()
extern "C"  Il2CppObject * Take__Run_m1446796188_gshared (Take__t4189452851 * __this, const MethodInfo* method);
#define Take__Run_m1446796188(__this, method) ((  Il2CppObject * (*) (Take__t4189452851 *, const MethodInfo*))Take__Run_m1446796188_gshared)(__this, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::Tick()
extern "C"  void Take__Tick_m922957587_gshared (Take__t4189452851 * __this, const MethodInfo* method);
#define Take__Tick_m922957587(__this, method) ((  void (*) (Take__t4189452851 *, const MethodInfo*))Take__Tick_m922957587_gshared)(__this, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::OnNext(T)
extern "C"  void Take__OnNext_m2761941494_gshared (Take__t4189452851 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Take__OnNext_m2761941494(__this, ___value0, method) ((  void (*) (Take__t4189452851 *, Unit_t2558286038 , const MethodInfo*))Take__OnNext_m2761941494_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Take__OnError_m1094537477_gshared (Take__t4189452851 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Take__OnError_m1094537477(__this, ___error0, method) ((  void (*) (Take__t4189452851 *, Exception_t1967233988 *, const MethodInfo*))Take__OnError_m1094537477_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>::OnCompleted()
extern "C"  void Take__OnCompleted_m39953368_gshared (Take__t4189452851 * __this, const MethodInfo* method);
#define Take__OnCompleted_m39953368(__this, method) ((  void (*) (Take__t4189452851 *, const MethodInfo*))Take__OnCompleted_m39953368_gshared)(__this, method)
