﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DisposableManager
struct DisposableManager_t2094948738;
// System.Collections.Generic.List`1<System.IDisposable>
struct List_1_t2425880343;
// System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>>
struct List_1_t221540724;
// Zenject.SingletonInstanceHelper
struct SingletonInstanceHelper_t833281251;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;
// Zenject.DisposableManager/DisposableInfo
struct DisposableInfo_t1284166222;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_SingletonInstanceHelper833281251.h"
#include "AssemblyU2DCSharp_Zenject_DisposableManager_Dispos1284166222.h"

// System.Void Zenject.DisposableManager::.ctor(System.Collections.Generic.List`1<System.IDisposable>,System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>>,Zenject.SingletonInstanceHelper)
extern "C"  void DisposableManager__ctor_m60091618 (DisposableManager_t2094948738 * __this, List_1_t2425880343 * ___disposables0, List_1_t221540724 * ___priorities1, SingletonInstanceHelper_t833281251 * ___singletonInstanceHelper2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DisposableManager::Dispose()
extern "C"  void DisposableManager_Dispose_m2601842778 (DisposableManager_t2094948738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DisposableManager::WarnForMissingBindings()
extern "C"  void DisposableManager_WarnForMissingBindings_m565898168 (DisposableManager_t2094948738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.DisposableManager::<DisposableManager>m__2D6(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  int32_t DisposableManager_U3CDisposableManagerU3Em__2D6_m86928299 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.DisposableManager::<Dispose>m__2D7(Zenject.DisposableManager/DisposableInfo)
extern "C"  int32_t DisposableManager_U3CDisposeU3Em__2D7_m2354468013 (Il2CppObject * __this /* static, unused */, DisposableInfo_t1284166222 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable Zenject.DisposableManager::<Dispose>m__2D8(Zenject.DisposableManager/DisposableInfo)
extern "C"  Il2CppObject * DisposableManager_U3CDisposeU3Em__2D8_m874795827 (Il2CppObject * __this /* static, unused */, DisposableInfo_t1284166222 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.DisposableManager::<WarnForMissingBindings>m__2D9(Zenject.DisposableManager/DisposableInfo)
extern "C"  Type_t * DisposableManager_U3CWarnForMissingBindingsU3Em__2D9_m3814230345 (Il2CppObject * __this /* static, unused */, DisposableInfo_t1284166222 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
