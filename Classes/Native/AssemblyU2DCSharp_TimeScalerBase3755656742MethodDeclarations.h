﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeScalerBase
struct TimeScalerBase_t3755656742;
// FiniteTimeProc
struct FiniteTimeProc_t2933053874;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FiniteTimeProc2933053874.h"

// System.Void TimeScalerBase::.ctor(FiniteTimeProc)
extern "C"  void TimeScalerBase__ctor_m752888003 (TimeScalerBase_t3755656742 * __this, FiniteTimeProc_t2933053874 * ___inProc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeScalerBase::Dispose()
extern "C"  void TimeScalerBase_Dispose_m1341195314 (TimeScalerBase_t3755656742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
