﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InjectableInfo>
struct ReadOnlyCollection_1_t15887826;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PostInjectableInfo
struct  PostInjectableInfo_t3080283662  : public Il2CppObject
{
public:
	// System.Reflection.MethodInfo Zenject.PostInjectableInfo::MethodInfo
	MethodInfo_t * ___MethodInfo_0;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.InjectableInfo> Zenject.PostInjectableInfo::InjectableInfo
	ReadOnlyCollection_1_t15887826 * ___InjectableInfo_1;

public:
	inline static int32_t get_offset_of_MethodInfo_0() { return static_cast<int32_t>(offsetof(PostInjectableInfo_t3080283662, ___MethodInfo_0)); }
	inline MethodInfo_t * get_MethodInfo_0() const { return ___MethodInfo_0; }
	inline MethodInfo_t ** get_address_of_MethodInfo_0() { return &___MethodInfo_0; }
	inline void set_MethodInfo_0(MethodInfo_t * value)
	{
		___MethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___MethodInfo_0, value);
	}

	inline static int32_t get_offset_of_InjectableInfo_1() { return static_cast<int32_t>(offsetof(PostInjectableInfo_t3080283662, ___InjectableInfo_1)); }
	inline ReadOnlyCollection_1_t15887826 * get_InjectableInfo_1() const { return ___InjectableInfo_1; }
	inline ReadOnlyCollection_1_t15887826 ** get_address_of_InjectableInfo_1() { return &___InjectableInfo_1; }
	inline void set_InjectableInfo_1(ReadOnlyCollection_1_t15887826 * value)
	{
		___InjectableInfo_1 = value;
		Il2CppCodeGenWriteBarrier(&___InjectableInfo_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
