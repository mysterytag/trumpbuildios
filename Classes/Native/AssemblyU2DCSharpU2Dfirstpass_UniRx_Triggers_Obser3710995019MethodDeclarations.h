﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservablePointerClickTrigger
struct ObservablePointerClickTrigger_t3710995019;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservablePointerClickTrigger::.ctor()
extern "C"  void ObservablePointerClickTrigger__ctor_m1215451072 (ObservablePointerClickTrigger_t3710995019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerClickTrigger::UnityEngine.EventSystems.IPointerClickHandler.OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservablePointerClickTrigger_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_m4121190821 (ObservablePointerClickTrigger_t3710995019 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerClickTrigger::OnPointerClickAsObservable()
extern "C"  Il2CppObject* ObservablePointerClickTrigger_OnPointerClickAsObservable_m4279055975 (ObservablePointerClickTrigger_t3710995019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerClickTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservablePointerClickTrigger_RaiseOnCompletedOnDestroy_m1610729209 (ObservablePointerClickTrigger_t3710995019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
