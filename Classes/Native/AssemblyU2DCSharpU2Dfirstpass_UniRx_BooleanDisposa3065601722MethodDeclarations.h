﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.BooleanDisposable
struct BooleanDisposable_t3065601722;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.BooleanDisposable::.ctor()
extern "C"  void BooleanDisposable__ctor_m2534881639 (BooleanDisposable_t3065601722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BooleanDisposable::.ctor(System.Boolean)
extern "C"  void BooleanDisposable__ctor_m3878547102 (BooleanDisposable_t3065601722 * __this, bool ___isDisposed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.BooleanDisposable::get_IsDisposed()
extern "C"  bool BooleanDisposable_get_IsDisposed_m2624310745 (BooleanDisposable_t3065601722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BooleanDisposable::set_IsDisposed(System.Boolean)
extern "C"  void BooleanDisposable_set_IsDisposed_m4066294816 (BooleanDisposable_t3065601722 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BooleanDisposable::Dispose()
extern "C"  void BooleanDisposable_Dispose_m673081316 (BooleanDisposable_t3065601722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
