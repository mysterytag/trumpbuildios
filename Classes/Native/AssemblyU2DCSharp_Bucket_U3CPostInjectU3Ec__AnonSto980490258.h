﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Bucket
struct Bucket_t2000631306;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bucket/<PostInject>c__AnonStorey51
struct  U3CPostInjectU3Ec__AnonStorey51_t980490258  : public Il2CppObject
{
public:
	// UnityEngine.Vector2 Bucket/<PostInject>c__AnonStorey51::size
	Vector2_t3525329788  ___size_0;
	// Bucket Bucket/<PostInject>c__AnonStorey51::<>f__this
	Bucket_t2000631306 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStorey51_t980490258, ___size_0)); }
	inline Vector2_t3525329788  get_size_0() const { return ___size_0; }
	inline Vector2_t3525329788 * get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(Vector2_t3525329788  value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStorey51_t980490258, ___U3CU3Ef__this_1)); }
	inline Bucket_t2000631306 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline Bucket_t2000631306 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(Bucket_t2000631306 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
