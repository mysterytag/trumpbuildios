﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuEntry
struct MenuEntry_t453027379;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"
#include "mscorlib_System_String968488902.h"

// System.Void MenuEntry::.ctor()
extern "C"  void MenuEntry__ctor_m202815640 (MenuEntry_t453027379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuEntry::SetIcon(UnityEngine.Sprite)
extern "C"  void MenuEntry_SetIcon_m445946997 (MenuEntry_t453027379 * __this, Sprite_t4006040370 * ___icon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuEntry::SetEntryName(System.String)
extern "C"  void MenuEntry_SetEntryName_m3889069723 (MenuEntry_t453027379 * __this, String_t* ___entryName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuEntry::SetEntryStatus(System.String)
extern "C"  void MenuEntry_SetEntryStatus_m1051994388 (MenuEntry_t453027379 * __this, String_t* ___entryStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuEntry::SetButtonPrice(System.String)
extern "C"  void MenuEntry_SetButtonPrice_m522451585 (MenuEntry_t453027379 * __this, String_t* ___buttonPrice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
