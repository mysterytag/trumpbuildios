﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetCharacterLeaderboardRequest
struct  GetCharacterLeaderboardRequest_t2906494549  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.GetCharacterLeaderboardRequest::<CharacterType>k__BackingField
	String_t* ___U3CCharacterTypeU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.GetCharacterLeaderboardRequest::<StatisticName>k__BackingField
	String_t* ___U3CStatisticNameU3Ek__BackingField_1;
	// System.Int32 PlayFab.ClientModels.GetCharacterLeaderboardRequest::<StartPosition>k__BackingField
	int32_t ___U3CStartPositionU3Ek__BackingField_2;
	// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetCharacterLeaderboardRequest::<MaxResultsCount>k__BackingField
	Nullable_1_t1438485399  ___U3CMaxResultsCountU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCharacterTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetCharacterLeaderboardRequest_t2906494549, ___U3CCharacterTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CCharacterTypeU3Ek__BackingField_0() const { return ___U3CCharacterTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCharacterTypeU3Ek__BackingField_0() { return &___U3CCharacterTypeU3Ek__BackingField_0; }
	inline void set_U3CCharacterTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CCharacterTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterTypeU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CStatisticNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetCharacterLeaderboardRequest_t2906494549, ___U3CStatisticNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CStatisticNameU3Ek__BackingField_1() const { return ___U3CStatisticNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CStatisticNameU3Ek__BackingField_1() { return &___U3CStatisticNameU3Ek__BackingField_1; }
	inline void set_U3CStatisticNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CStatisticNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStatisticNameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CStartPositionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetCharacterLeaderboardRequest_t2906494549, ___U3CStartPositionU3Ek__BackingField_2)); }
	inline int32_t get_U3CStartPositionU3Ek__BackingField_2() const { return ___U3CStartPositionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStartPositionU3Ek__BackingField_2() { return &___U3CStartPositionU3Ek__BackingField_2; }
	inline void set_U3CStartPositionU3Ek__BackingField_2(int32_t value)
	{
		___U3CStartPositionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMaxResultsCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetCharacterLeaderboardRequest_t2906494549, ___U3CMaxResultsCountU3Ek__BackingField_3)); }
	inline Nullable_1_t1438485399  get_U3CMaxResultsCountU3Ek__BackingField_3() const { return ___U3CMaxResultsCountU3Ek__BackingField_3; }
	inline Nullable_1_t1438485399 * get_address_of_U3CMaxResultsCountU3Ek__BackingField_3() { return &___U3CMaxResultsCountU3Ek__BackingField_3; }
	inline void set_U3CMaxResultsCountU3Ek__BackingField_3(Nullable_1_t1438485399  value)
	{
		___U3CMaxResultsCountU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
