﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ScanObservable`2/Scan<UniRx.Unit,System.Int64>
struct Scan_t1598733234;
// UniRx.Operators.ScanObservable`2<UniRx.Unit,System.Int64>
struct ScanObservable_2_t850793272;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ScanObservable`2/Scan<UniRx.Unit,System.Int64>::.ctor(UniRx.Operators.ScanObservable`2<TSource,TAccumulate>,UniRx.IObserver`1<TAccumulate>,System.IDisposable)
extern "C"  void Scan__ctor_m1022232742_gshared (Scan_t1598733234 * __this, ScanObservable_2_t850793272 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Scan__ctor_m1022232742(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Scan_t1598733234 *, ScanObservable_2_t850793272 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Scan__ctor_m1022232742_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.ScanObservable`2/Scan<UniRx.Unit,System.Int64>::OnNext(TSource)
extern "C"  void Scan_OnNext_m3291596771_gshared (Scan_t1598733234 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Scan_OnNext_m3291596771(__this, ___value0, method) ((  void (*) (Scan_t1598733234 *, Unit_t2558286038 , const MethodInfo*))Scan_OnNext_m3291596771_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ScanObservable`2/Scan<UniRx.Unit,System.Int64>::OnError(System.Exception)
extern "C"  void Scan_OnError_m3848816525_gshared (Scan_t1598733234 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Scan_OnError_m3848816525(__this, ___error0, method) ((  void (*) (Scan_t1598733234 *, Exception_t1967233988 *, const MethodInfo*))Scan_OnError_m3848816525_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ScanObservable`2/Scan<UniRx.Unit,System.Int64>::OnCompleted()
extern "C"  void Scan_OnCompleted_m3131319904_gshared (Scan_t1598733234 * __this, const MethodInfo* method);
#define Scan_OnCompleted_m3131319904(__this, method) ((  void (*) (Scan_t1598733234 *, const MethodInfo*))Scan_OnCompleted_m3131319904_gshared)(__this, method)
