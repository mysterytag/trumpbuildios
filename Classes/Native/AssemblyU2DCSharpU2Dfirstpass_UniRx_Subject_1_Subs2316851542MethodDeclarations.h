﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Subscription_t2316851542;
// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Subject_1_t2180672344;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1285516328_gshared (Subscription_t2316851542 * __this, Subject_1_t2180672344 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1285516328(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t2316851542 *, Subject_1_t2180672344 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1285516328_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m2278203598_gshared (Subscription_t2316851542 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2278203598(__this, method) ((  void (*) (Subscription_t2316851542 *, const MethodInfo*))Subscription_Dispose_m2278203598_gshared)(__this, method)
