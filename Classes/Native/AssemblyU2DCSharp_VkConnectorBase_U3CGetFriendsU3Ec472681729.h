﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VkConnectorBase/<GetFriends>c__AnonStoreyDB
struct U3CGetFriendsU3Ec__AnonStoreyDB_t472681730;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VkConnectorBase/<GetFriends>c__AnonStoreyDB/<GetFriends>c__AnonStoreyDA
struct  U3CGetFriendsU3Ec__AnonStoreyDA_t472681729  : public Il2CppObject
{
public:
	// System.String VkConnectorBase/<GetFriends>c__AnonStoreyDB/<GetFriends>c__AnonStoreyDA::userId
	String_t* ___userId_0;
	// VkConnectorBase/<GetFriends>c__AnonStoreyDB VkConnectorBase/<GetFriends>c__AnonStoreyDB/<GetFriends>c__AnonStoreyDA::<>f__ref$219
	U3CGetFriendsU3Ec__AnonStoreyDB_t472681730 * ___U3CU3Ef__refU24219_1;

public:
	inline static int32_t get_offset_of_userId_0() { return static_cast<int32_t>(offsetof(U3CGetFriendsU3Ec__AnonStoreyDA_t472681729, ___userId_0)); }
	inline String_t* get_userId_0() const { return ___userId_0; }
	inline String_t** get_address_of_userId_0() { return &___userId_0; }
	inline void set_userId_0(String_t* value)
	{
		___userId_0 = value;
		Il2CppCodeGenWriteBarrier(&___userId_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24219_1() { return static_cast<int32_t>(offsetof(U3CGetFriendsU3Ec__AnonStoreyDA_t472681729, ___U3CU3Ef__refU24219_1)); }
	inline U3CGetFriendsU3Ec__AnonStoreyDB_t472681730 * get_U3CU3Ef__refU24219_1() const { return ___U3CU3Ef__refU24219_1; }
	inline U3CGetFriendsU3Ec__AnonStoreyDB_t472681730 ** get_address_of_U3CU3Ef__refU24219_1() { return &___U3CU3Ef__refU24219_1; }
	inline void set_U3CU3Ef__refU24219_1(U3CGetFriendsU3Ec__AnonStoreyDB_t472681730 * value)
	{
		___U3CU3Ef__refU24219_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24219_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
