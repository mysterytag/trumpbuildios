﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1<System.Int64>
struct Cell_1_t3029512514;
// ICell`1<System.Int64>
struct ICell_1_t104078563;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// IStream`1<System.Int64>
struct IStream_1_t3400105873;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"

// System.Void Cell`1<System.Int64>::.ctor()
extern "C"  void Cell_1__ctor_m403607060_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method);
#define Cell_1__ctor_m403607060(__this, method) ((  void (*) (Cell_1_t3029512514 *, const MethodInfo*))Cell_1__ctor_m403607060_gshared)(__this, method)
// System.Void Cell`1<System.Int64>::.ctor(T)
extern "C"  void Cell_1__ctor_m3921885642_gshared (Cell_1_t3029512514 * __this, int64_t ___initial0, const MethodInfo* method);
#define Cell_1__ctor_m3921885642(__this, ___initial0, method) ((  void (*) (Cell_1_t3029512514 *, int64_t, const MethodInfo*))Cell_1__ctor_m3921885642_gshared)(__this, ___initial0, method)
// System.Void Cell`1<System.Int64>::.ctor(ICell`1<T>)
extern "C"  void Cell_1__ctor_m824309194_gshared (Cell_1_t3029512514 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define Cell_1__ctor_m824309194(__this, ___other0, method) ((  void (*) (Cell_1_t3029512514 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m824309194_gshared)(__this, ___other0, method)
// System.Void Cell`1<System.Int64>::SetInputCell(ICell`1<T>)
extern "C"  void Cell_1_SetInputCell_m396383600_gshared (Cell_1_t3029512514 * __this, Il2CppObject* ___cell0, const MethodInfo* method);
#define Cell_1_SetInputCell_m396383600(__this, ___cell0, method) ((  void (*) (Cell_1_t3029512514 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m396383600_gshared)(__this, ___cell0, method)
// T Cell`1<System.Int64>::get_value()
extern "C"  int64_t Cell_1_get_value_m804504699_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method);
#define Cell_1_get_value_m804504699(__this, method) ((  int64_t (*) (Cell_1_t3029512514 *, const MethodInfo*))Cell_1_get_value_m804504699_gshared)(__this, method)
// System.Void Cell`1<System.Int64>::set_value(T)
extern "C"  void Cell_1_set_value_m186320120_gshared (Cell_1_t3029512514 * __this, int64_t ___value0, const MethodInfo* method);
#define Cell_1_set_value_m186320120(__this, ___value0, method) ((  void (*) (Cell_1_t3029512514 *, int64_t, const MethodInfo*))Cell_1_set_value_m186320120_gshared)(__this, ___value0, method)
// System.Void Cell`1<System.Int64>::Set(T)
extern "C"  void Cell_1_Set_m796981386_gshared (Cell_1_t3029512514 * __this, int64_t ___val0, const MethodInfo* method);
#define Cell_1_Set_m796981386(__this, ___val0, method) ((  void (*) (Cell_1_t3029512514 *, int64_t, const MethodInfo*))Cell_1_Set_m796981386_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<System.Int64>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m3072437177_gshared (Cell_1_t3029512514 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_OnChanged_m3072437177(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t3029512514 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m3072437177_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<System.Int64>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m2966024868_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method);
#define Cell_1_get_valueObject_m2966024868(__this, method) ((  Il2CppObject * (*) (Cell_1_t3029512514 *, const MethodInfo*))Cell_1_get_valueObject_m2966024868_gshared)(__this, method)
// System.IDisposable Cell`1<System.Int64>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m2615983424_gshared (Cell_1_t3029512514 * __this, Action_1_t2995867587 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_ListenUpdates_m2615983424(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t3029512514 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m2615983424_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<System.Int64>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m843735170_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method);
#define Cell_1_get_updates_m843735170(__this, method) ((  Il2CppObject* (*) (Cell_1_t3029512514 *, const MethodInfo*))Cell_1_get_updates_m843735170_gshared)(__this, method)
// System.IDisposable Cell`1<System.Int64>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m2503534850_gshared (Cell_1_t3029512514 * __this, Action_1_t2995867587 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_Bind_m2503534850(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t3029512514 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))Cell_1_Bind_m2503534850_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<System.Int64>::SetInputStream(IStream`1<T>)
extern "C"  void Cell_1_SetInputStream_m2005538412_gshared (Cell_1_t3029512514 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Cell_1_SetInputStream_m2005538412(__this, ___stream0, method) ((  void (*) (Cell_1_t3029512514 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m2005538412_gshared)(__this, ___stream0, method)
// System.Void Cell`1<System.Int64>::ResetInputStream()
extern "C"  void Cell_1_ResetInputStream_m4114426507_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method);
#define Cell_1_ResetInputStream_m4114426507(__this, method) ((  void (*) (Cell_1_t3029512514 *, const MethodInfo*))Cell_1_ResetInputStream_m4114426507_gshared)(__this, method)
// System.Void Cell`1<System.Int64>::TransactionIterationFinished()
extern "C"  void Cell_1_TransactionIterationFinished_m1174557345_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method);
#define Cell_1_TransactionIterationFinished_m1174557345(__this, method) ((  void (*) (Cell_1_t3029512514 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m1174557345_gshared)(__this, method)
// System.Void Cell`1<System.Int64>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m4073776883_gshared (Cell_1_t3029512514 * __this, int32_t ___p0, const MethodInfo* method);
#define Cell_1_Unpack_m4073776883(__this, ___p0, method) ((  void (*) (Cell_1_t3029512514 *, int32_t, const MethodInfo*))Cell_1_Unpack_m4073776883_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<System.Int64>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m1961942878_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t3029512514 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_LessThanOrEqual_m1961942878(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t3029512514 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m1961942878_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<System.Int64>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m2407512681_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t3029512514 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_GreaterThanOrEqual_m2407512681(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t3029512514 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m2407512681_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
