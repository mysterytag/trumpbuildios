﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AsUnitObservableObservable`1<System.Object>
struct AsUnitObservableObservable_1_t2313647363;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.AsUnitObservableObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void AsUnitObservableObservable_1__ctor_m2657289694_gshared (AsUnitObservableObservable_1_t2313647363 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define AsUnitObservableObservable_1__ctor_m2657289694(__this, ___source0, method) ((  void (*) (AsUnitObservableObservable_1_t2313647363 *, Il2CppObject*, const MethodInfo*))AsUnitObservableObservable_1__ctor_m2657289694_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.AsUnitObservableObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  Il2CppObject * AsUnitObservableObservable_1_SubscribeCore_m2239287369_gshared (AsUnitObservableObservable_1_t2313647363 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AsUnitObservableObservable_1_SubscribeCore_m2239287369(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (AsUnitObservableObservable_1_t2313647363 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AsUnitObservableObservable_1_SubscribeCore_m2239287369_gshared)(__this, ___observer0, ___cancel1, method)
