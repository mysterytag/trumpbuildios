﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// System.Func`2<System.Int64,UnityEngine.Vector2>
struct Func_2_t1270693722;
// System.Func`2<UnityEngine.Touch,System.Boolean>
struct Func_2_t1874381577;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ObservableExtension
struct  ObservableExtension_t8126852  : public Il2CppObject
{
public:

public:
};

struct ObservableExtension_t8126852_StaticFields
{
public:
	// System.Func`2<System.Int64,System.Boolean> ZergRush.ObservableExtension::<>f__am$cache0
	Func_2_t2251336571 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Int64,UnityEngine.Vector2> ZergRush.ObservableExtension::<>f__am$cache1
	Func_2_t1270693722 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.Touch,System.Boolean> ZergRush.ObservableExtension::<>f__am$cache2
	Func_2_t1874381577 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.Touch,System.Boolean> ZergRush.ObservableExtension::<>f__am$cache3
	Func_2_t1874381577 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ObservableExtension_t8126852_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t2251336571 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t2251336571 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t2251336571 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ObservableExtension_t8126852_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1270693722 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1270693722 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1270693722 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(ObservableExtension_t8126852_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t1874381577 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t1874381577 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t1874381577 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(ObservableExtension_t8126852_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t1874381577 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t1874381577 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t1874381577 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
