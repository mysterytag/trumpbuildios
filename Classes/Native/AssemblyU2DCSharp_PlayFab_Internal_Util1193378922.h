﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t2956870243;
// PlayFab.Internal.Util/MyJsonSerializerStrategy
struct MyJsonSerializerStrategy_t989142809;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Globalization_DateTimeStyles3832294611.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.Util
struct  Util_t1193378922  : public Il2CppObject
{
public:

public:
};

struct Util_t1193378922_StaticFields
{
public:
	// System.String[] PlayFab.Internal.Util::_defaultDateTimeFormats
	StringU5BU5D_t2956870243* ____defaultDateTimeFormats_2;
	// System.Globalization.DateTimeStyles PlayFab.Internal.Util::_dateTimeStyles
	int32_t ____dateTimeStyles_3;
	// PlayFab.Internal.Util/MyJsonSerializerStrategy PlayFab.Internal.Util::ApiSerializerStrategy
	MyJsonSerializerStrategy_t989142809 * ___ApiSerializerStrategy_4;

public:
	inline static int32_t get_offset_of__defaultDateTimeFormats_2() { return static_cast<int32_t>(offsetof(Util_t1193378922_StaticFields, ____defaultDateTimeFormats_2)); }
	inline StringU5BU5D_t2956870243* get__defaultDateTimeFormats_2() const { return ____defaultDateTimeFormats_2; }
	inline StringU5BU5D_t2956870243** get_address_of__defaultDateTimeFormats_2() { return &____defaultDateTimeFormats_2; }
	inline void set__defaultDateTimeFormats_2(StringU5BU5D_t2956870243* value)
	{
		____defaultDateTimeFormats_2 = value;
		Il2CppCodeGenWriteBarrier(&____defaultDateTimeFormats_2, value);
	}

	inline static int32_t get_offset_of__dateTimeStyles_3() { return static_cast<int32_t>(offsetof(Util_t1193378922_StaticFields, ____dateTimeStyles_3)); }
	inline int32_t get__dateTimeStyles_3() const { return ____dateTimeStyles_3; }
	inline int32_t* get_address_of__dateTimeStyles_3() { return &____dateTimeStyles_3; }
	inline void set__dateTimeStyles_3(int32_t value)
	{
		____dateTimeStyles_3 = value;
	}

	inline static int32_t get_offset_of_ApiSerializerStrategy_4() { return static_cast<int32_t>(offsetof(Util_t1193378922_StaticFields, ___ApiSerializerStrategy_4)); }
	inline MyJsonSerializerStrategy_t989142809 * get_ApiSerializerStrategy_4() const { return ___ApiSerializerStrategy_4; }
	inline MyJsonSerializerStrategy_t989142809 ** get_address_of_ApiSerializerStrategy_4() { return &___ApiSerializerStrategy_4; }
	inline void set_ApiSerializerStrategy_4(MyJsonSerializerStrategy_t989142809 * value)
	{
		___ApiSerializerStrategy_4 = value;
		Il2CppCodeGenWriteBarrier(&___ApiSerializerStrategy_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
