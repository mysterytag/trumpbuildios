﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Boolean[]
struct BooleanU5BU5D_t3804927312;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>
struct  NthCombineLatestObserverBase_1_t2199482330  : public OperatorObserverBase_2_t1187768149
{
public:
	// System.Int32 UniRx.Operators.NthCombineLatestObserverBase`1::length
	int32_t ___length_2;
	// System.Boolean UniRx.Operators.NthCombineLatestObserverBase`1::isAllValueStarted
	bool ___isAllValueStarted_3;
	// System.Boolean[] UniRx.Operators.NthCombineLatestObserverBase`1::isStarted
	BooleanU5BU5D_t3804927312* ___isStarted_4;
	// System.Boolean[] UniRx.Operators.NthCombineLatestObserverBase`1::isCompleted
	BooleanU5BU5D_t3804927312* ___isCompleted_5;

public:
	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(NthCombineLatestObserverBase_1_t2199482330, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}

	inline static int32_t get_offset_of_isAllValueStarted_3() { return static_cast<int32_t>(offsetof(NthCombineLatestObserverBase_1_t2199482330, ___isAllValueStarted_3)); }
	inline bool get_isAllValueStarted_3() const { return ___isAllValueStarted_3; }
	inline bool* get_address_of_isAllValueStarted_3() { return &___isAllValueStarted_3; }
	inline void set_isAllValueStarted_3(bool value)
	{
		___isAllValueStarted_3 = value;
	}

	inline static int32_t get_offset_of_isStarted_4() { return static_cast<int32_t>(offsetof(NthCombineLatestObserverBase_1_t2199482330, ___isStarted_4)); }
	inline BooleanU5BU5D_t3804927312* get_isStarted_4() const { return ___isStarted_4; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isStarted_4() { return &___isStarted_4; }
	inline void set_isStarted_4(BooleanU5BU5D_t3804927312* value)
	{
		___isStarted_4 = value;
		Il2CppCodeGenWriteBarrier(&___isStarted_4, value);
	}

	inline static int32_t get_offset_of_isCompleted_5() { return static_cast<int32_t>(offsetof(NthCombineLatestObserverBase_1_t2199482330, ___isCompleted_5)); }
	inline BooleanU5BU5D_t3804927312* get_isCompleted_5() const { return ___isCompleted_5; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isCompleted_5() { return &___isCompleted_5; }
	inline void set_isCompleted_5(BooleanU5BU5D_t3804927312* value)
	{
		___isCompleted_5 = value;
		Il2CppCodeGenWriteBarrier(&___isCompleted_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
