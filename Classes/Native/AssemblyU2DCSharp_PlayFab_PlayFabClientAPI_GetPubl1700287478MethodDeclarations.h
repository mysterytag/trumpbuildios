﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPublisherDataResponseCallback
struct GetPublisherDataResponseCallback_t1700287478;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPublisherDataRequest
struct GetPublisherDataRequest_t1266178127;
// PlayFab.ClientModels.GetPublisherDataResult
struct GetPublisherDataResult_t2154202237;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPublishe1266178127.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPublishe2154202237.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPublisherDataResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPublisherDataResponseCallback__ctor_m1839829477 (GetPublisherDataResponseCallback_t1700287478 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPublisherDataResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPublisherDataRequest,PlayFab.ClientModels.GetPublisherDataResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPublisherDataResponseCallback_Invoke_m2905575794 (GetPublisherDataResponseCallback_t1700287478 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPublisherDataRequest_t1266178127 * ___request2, GetPublisherDataResult_t2154202237 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPublisherDataResponseCallback_t1700287478(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPublisherDataRequest_t1266178127 * ___request2, GetPublisherDataResult_t2154202237 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPublisherDataResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPublisherDataRequest,PlayFab.ClientModels.GetPublisherDataResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPublisherDataResponseCallback_BeginInvoke_m1582540319 (GetPublisherDataResponseCallback_t1700287478 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPublisherDataRequest_t1266178127 * ___request2, GetPublisherDataResult_t2154202237 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPublisherDataResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPublisherDataResponseCallback_EndInvoke_m2977152629 (GetPublisherDataResponseCallback_t1700287478 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
