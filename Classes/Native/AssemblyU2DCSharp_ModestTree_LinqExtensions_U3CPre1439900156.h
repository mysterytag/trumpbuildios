﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>
struct  U3CPrependU3Ec__Iterator38_1_t1439900156  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerable`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::second
	Il2CppObject* ___second_0;
	// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::<$s_236>__0
	Il2CppObject* ___U3CU24s_236U3E__0_1;
	// T ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::<t>__1
	Il2CppObject * ___U3CtU3E__1_2;
	// System.Collections.Generic.IEnumerable`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::first
	Il2CppObject* ___first_3;
	// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::<$s_237>__2
	Il2CppObject* ___U3CU24s_237U3E__2_4;
	// T ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::<t>__3
	Il2CppObject * ___U3CtU3E__3_5;
	// System.Int32 ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::$PC
	int32_t ___U24PC_6;
	// T ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::$current
	Il2CppObject * ___U24current_7;
	// System.Collections.Generic.IEnumerable`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::<$>second
	Il2CppObject* ___U3CU24U3Esecond_8;
	// System.Collections.Generic.IEnumerable`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator38`1::<$>first
	Il2CppObject* ___U3CU24U3Efirst_9;

public:
	inline static int32_t get_offset_of_second_0() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___second_0)); }
	inline Il2CppObject* get_second_0() const { return ___second_0; }
	inline Il2CppObject** get_address_of_second_0() { return &___second_0; }
	inline void set_second_0(Il2CppObject* value)
	{
		___second_0 = value;
		Il2CppCodeGenWriteBarrier(&___second_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_236U3E__0_1() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___U3CU24s_236U3E__0_1)); }
	inline Il2CppObject* get_U3CU24s_236U3E__0_1() const { return ___U3CU24s_236U3E__0_1; }
	inline Il2CppObject** get_address_of_U3CU24s_236U3E__0_1() { return &___U3CU24s_236U3E__0_1; }
	inline void set_U3CU24s_236U3E__0_1(Il2CppObject* value)
	{
		___U3CU24s_236U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_236U3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CtU3E__1_2() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___U3CtU3E__1_2)); }
	inline Il2CppObject * get_U3CtU3E__1_2() const { return ___U3CtU3E__1_2; }
	inline Il2CppObject ** get_address_of_U3CtU3E__1_2() { return &___U3CtU3E__1_2; }
	inline void set_U3CtU3E__1_2(Il2CppObject * value)
	{
		___U3CtU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtU3E__1_2, value);
	}

	inline static int32_t get_offset_of_first_3() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___first_3)); }
	inline Il2CppObject* get_first_3() const { return ___first_3; }
	inline Il2CppObject** get_address_of_first_3() { return &___first_3; }
	inline void set_first_3(Il2CppObject* value)
	{
		___first_3 = value;
		Il2CppCodeGenWriteBarrier(&___first_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_237U3E__2_4() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___U3CU24s_237U3E__2_4)); }
	inline Il2CppObject* get_U3CU24s_237U3E__2_4() const { return ___U3CU24s_237U3E__2_4; }
	inline Il2CppObject** get_address_of_U3CU24s_237U3E__2_4() { return &___U3CU24s_237U3E__2_4; }
	inline void set_U3CU24s_237U3E__2_4(Il2CppObject* value)
	{
		___U3CU24s_237U3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_237U3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CtU3E__3_5() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___U3CtU3E__3_5)); }
	inline Il2CppObject * get_U3CtU3E__3_5() const { return ___U3CtU3E__3_5; }
	inline Il2CppObject ** get_address_of_U3CtU3E__3_5() { return &___U3CtU3E__3_5; }
	inline void set_U3CtU3E__3_5(Il2CppObject * value)
	{
		___U3CtU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtU3E__3_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esecond_8() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___U3CU24U3Esecond_8)); }
	inline Il2CppObject* get_U3CU24U3Esecond_8() const { return ___U3CU24U3Esecond_8; }
	inline Il2CppObject** get_address_of_U3CU24U3Esecond_8() { return &___U3CU24U3Esecond_8; }
	inline void set_U3CU24U3Esecond_8(Il2CppObject* value)
	{
		___U3CU24U3Esecond_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esecond_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efirst_9() { return static_cast<int32_t>(offsetof(U3CPrependU3Ec__Iterator38_1_t1439900156, ___U3CU24U3Efirst_9)); }
	inline Il2CppObject* get_U3CU24U3Efirst_9() const { return ___U3CU24U3Efirst_9; }
	inline Il2CppObject** get_address_of_U3CU24U3Efirst_9() { return &___U3CU24U3Efirst_9; }
	inline void set_U3CU24U3Efirst_9(Il2CppObject* value)
	{
		___U3CU24U3Efirst_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efirst_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
