﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.Collider2D>
struct Subject_1_t3828072815;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTrigger2DTrigger
struct  ObservableTrigger2DTrigger_t885825170  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::onTriggerEnter2D
	Subject_1_t3828072815 * ___onTriggerEnter2D_8;
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::onTriggerExit2D
	Subject_1_t3828072815 * ___onTriggerExit2D_9;
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::onTriggerStay2D
	Subject_1_t3828072815 * ___onTriggerStay2D_10;

public:
	inline static int32_t get_offset_of_onTriggerEnter2D_8() { return static_cast<int32_t>(offsetof(ObservableTrigger2DTrigger_t885825170, ___onTriggerEnter2D_8)); }
	inline Subject_1_t3828072815 * get_onTriggerEnter2D_8() const { return ___onTriggerEnter2D_8; }
	inline Subject_1_t3828072815 ** get_address_of_onTriggerEnter2D_8() { return &___onTriggerEnter2D_8; }
	inline void set_onTriggerEnter2D_8(Subject_1_t3828072815 * value)
	{
		___onTriggerEnter2D_8 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerEnter2D_8, value);
	}

	inline static int32_t get_offset_of_onTriggerExit2D_9() { return static_cast<int32_t>(offsetof(ObservableTrigger2DTrigger_t885825170, ___onTriggerExit2D_9)); }
	inline Subject_1_t3828072815 * get_onTriggerExit2D_9() const { return ___onTriggerExit2D_9; }
	inline Subject_1_t3828072815 ** get_address_of_onTriggerExit2D_9() { return &___onTriggerExit2D_9; }
	inline void set_onTriggerExit2D_9(Subject_1_t3828072815 * value)
	{
		___onTriggerExit2D_9 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerExit2D_9, value);
	}

	inline static int32_t get_offset_of_onTriggerStay2D_10() { return static_cast<int32_t>(offsetof(ObservableTrigger2DTrigger_t885825170, ___onTriggerStay2D_10)); }
	inline Subject_1_t3828072815 * get_onTriggerStay2D_10() const { return ___onTriggerStay2D_10; }
	inline Subject_1_t3828072815 ** get_address_of_onTriggerStay2D_10() { return &___onTriggerStay2D_10; }
	inline void set_onTriggerStay2D_10(Subject_1_t3828072815 * value)
	{
		___onTriggerStay2D_10 = value;
		Il2CppCodeGenWriteBarrier(&___onTriggerStay2D_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
