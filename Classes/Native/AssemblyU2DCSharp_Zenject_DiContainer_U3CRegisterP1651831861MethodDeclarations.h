﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<RegisterProvider>c__AnonStorey185
struct U3CRegisterProviderU3Ec__AnonStorey185_t1651831861;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.DiContainer/<RegisterProvider>c__AnonStorey185::.ctor()
extern "C"  void U3CRegisterProviderU3Ec__AnonStorey185__ctor_m2089565908 (U3CRegisterProviderU3Ec__AnonStorey185_t1651831861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<RegisterProvider>c__AnonStorey185::<>m__2BE(Zenject.ProviderBase)
extern "C"  bool U3CRegisterProviderU3Ec__AnonStorey185_U3CU3Em__2BE_m1598858445 (U3CRegisterProviderU3Ec__AnonStorey185_t1651831861 * __this, ProviderBase_t1627494391 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
