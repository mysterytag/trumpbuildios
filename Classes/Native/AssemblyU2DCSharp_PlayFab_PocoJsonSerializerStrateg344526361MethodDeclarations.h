﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t344526361;
// System.String
struct String_t;
// PlayFab.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t4072949631;
// System.Type
struct Type_t;
// System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>
struct IDictionary_2_t3083487430;
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>
struct IDictionary_2_t301019319;
// System.Object
struct Il2CppObject;
// System.Enum
struct Enum_t2778772662;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Enum2778772662.h"

// System.Void PlayFab.PocoJsonSerializerStrategy::.ctor()
extern "C"  void PocoJsonSerializerStrategy__ctor_m3606999912 (PocoJsonSerializerStrategy_t344526361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PocoJsonSerializerStrategy::.cctor()
extern "C"  void PocoJsonSerializerStrategy__cctor_m3960718661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String)
extern "C"  String_t* PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m360522415 (PocoJsonSerializerStrategy_t344526361 * __this, String_t* ___clrPropertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/ConstructorDelegate PlayFab.PocoJsonSerializerStrategy::ContructorDelegateFactory(System.Type)
extern "C"  ConstructorDelegate_t4072949631 * PocoJsonSerializerStrategy_ContructorDelegateFactory_m264094831 (PocoJsonSerializerStrategy_t344526361 * __this, Type_t * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate> PlayFab.PocoJsonSerializerStrategy::GetterValueFactory(System.Type)
extern "C"  Il2CppObject* PocoJsonSerializerStrategy_GetterValueFactory_m2840755235 (PocoJsonSerializerStrategy_t344526361 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>> PlayFab.PocoJsonSerializerStrategy::SetterValueFactory(System.Type)
extern "C"  Il2CppObject* PocoJsonSerializerStrategy_SetterValueFactory_m1943165971 (PocoJsonSerializerStrategy_t344526361 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.PocoJsonSerializerStrategy::TrySerializeNonPrimitiveObject(System.Object,System.Object&)
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m3473895608 (PocoJsonSerializerStrategy_t344526361 * __this, Il2CppObject * ___input0, Il2CppObject ** ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.PocoJsonSerializerStrategy::DeserializeObject(System.Object,System.Type)
extern "C"  Il2CppObject * PocoJsonSerializerStrategy_DeserializeObject_m1775711448 (PocoJsonSerializerStrategy_t344526361 * __this, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.PocoJsonSerializerStrategy::SerializeEnum(System.Enum)
extern "C"  Il2CppObject * PocoJsonSerializerStrategy_SerializeEnum_m71763680 (PocoJsonSerializerStrategy_t344526361 * __this, Enum_t2778772662 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&)
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeKnownTypes_m1946596981 (PocoJsonSerializerStrategy_t344526361 * __this, Il2CppObject * ___input0, Il2CppObject ** ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&)
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m450122510 (PocoJsonSerializerStrategy_t344526361 * __this, Il2CppObject * ___input0, Il2CppObject ** ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
