﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen517714524MethodDeclarations.h"

// System.Void System.Action`1<UniRx.Tuple`2<System.String,System.String>>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2662748795(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t3594443476 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2013857454_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<UniRx.Tuple`2<System.String,System.String>>::Invoke(T)
#define Action_1_Invoke_m35806770(__this, ___obj0, method) ((  void (*) (Action_1_t3594443476 *, Tuple_2_t3445990771 , const MethodInfo*))Action_1_Invoke_m3370673622_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<UniRx.Tuple`2<System.String,System.String>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m2788655751(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t3594443476 *, Tuple_2_t3445990771 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m650516011_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<UniRx.Tuple`2<System.String,System.String>>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m2794920162(__this, ___result0, method) ((  void (*) (Action_1_t3594443476 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3445030846_gshared)(__this, ___result0, method)
