﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkKongregateAccountResult
struct LinkKongregateAccountResult_t1077060901;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkKongregateAccountResult::.ctor()
extern "C"  void LinkKongregateAccountResult__ctor_m1057954596 (LinkKongregateAccountResult_t1077060901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
