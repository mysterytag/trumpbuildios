﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Type
struct Type_t;
// PlayFab.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t3082203415;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;
// System.Char[]
struct CharU5BU5D_t3416858730;
// PlayFab.JsonArray
struct JsonArray_t1741147506;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.Collections.IEnumerable
struct IEnumerable_t287189635;
// PlayFab.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t344526361;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_PlayFab_SimpleJson_TokenType2145466547.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"

// System.Void PlayFab.SimpleJson::.cctor()
extern "C"  void SimpleJson__cctor_m1117589447 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.SimpleJson::DeserializeObject(System.String)
extern "C"  Il2CppObject * SimpleJson_DeserializeObject_m1774289173 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.SimpleJson::TryDeserializeObject(System.String,System.Object&)
extern "C"  bool SimpleJson_TryDeserializeObject_m1840888817 (Il2CppObject * __this /* static, unused */, String_t* ___json0, Il2CppObject ** ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.SimpleJson::DeserializeObject(System.String,System.Type,PlayFab.IJsonSerializerStrategy)
extern "C"  Il2CppObject * SimpleJson_DeserializeObject_m2340256857 (Il2CppObject * __this /* static, unused */, String_t* ___json0, Type_t * ___type1, Il2CppObject * ___jsonSerializerStrategy2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.SimpleJson::DeserializeObject(System.String,System.Type)
extern "C"  Il2CppObject * SimpleJson_DeserializeObject_m3535214792 (Il2CppObject * __this /* static, unused */, String_t* ___json0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.SimpleJson::SerializeObject(System.Object,PlayFab.IJsonSerializerStrategy)
extern "C"  String_t* SimpleJson_SerializeObject_m1491424485 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, Il2CppObject * ___jsonSerializerStrategy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.SimpleJson::EscapeToJavascriptString(System.String)
extern "C"  String_t* SimpleJson_EscapeToJavascriptString_m1821004359 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Object> PlayFab.SimpleJson::ParseObject(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Il2CppObject* SimpleJson_ParseObject_m3521866726 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.JsonArray PlayFab.SimpleJson::ParseArray(System.Char[],System.Int32&,System.Boolean&)
extern "C"  JsonArray_t1741147506 * SimpleJson_ParseArray_m161445278 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.SimpleJson::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Il2CppObject * SimpleJson_ParseValue_m721865422 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.SimpleJson::ParseString(System.Char[],System.Int32&,System.Boolean&)
extern "C"  String_t* SimpleJson_ParseString_m4093027002 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.SimpleJson::ConvertFromUtf32(System.Int32)
extern "C"  String_t* SimpleJson_ConvertFromUtf32_m3539626139 (Il2CppObject * __this /* static, unused */, int32_t ___utf320, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.SimpleJson::ParseNumber(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Il2CppObject * SimpleJson_ParseNumber_m1825851924 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.SimpleJson::GetLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t SimpleJson_GetLastIndexOfNumber_m1399304110 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.SimpleJson::EatWhitespace(System.Char[],System.Int32&)
extern "C"  void SimpleJson_EatWhitespace_m2939548727 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.SimpleJson/TokenType PlayFab.SimpleJson::LookAhead(System.Char[],System.Int32)
extern "C"  uint8_t SimpleJson_LookAhead_m1886741630 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.SimpleJson/TokenType PlayFab.SimpleJson::NextToken(System.Char[],System.Int32&)
extern "C"  uint8_t SimpleJson_NextToken_m3588940074 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.SimpleJson::SerializeValue(PlayFab.IJsonSerializerStrategy,System.Object,System.Text.StringBuilder)
extern "C"  bool SimpleJson_SerializeValue_m1839413606 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy0, Il2CppObject * ___value1, StringBuilder_t3822575854 * ___builder2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.SimpleJson::SerializeObject(PlayFab.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Collections.IEnumerable,System.Text.StringBuilder)
extern "C"  bool SimpleJson_SerializeObject_m214672684 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy0, Il2CppObject * ___keys1, Il2CppObject * ___values2, StringBuilder_t3822575854 * ___builder3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.SimpleJson::SerializeArray(PlayFab.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Text.StringBuilder)
extern "C"  bool SimpleJson_SerializeArray_m2801008357 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy0, Il2CppObject * ___anArray1, StringBuilder_t3822575854 * ___builder2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.SimpleJson::SerializeString(System.String,System.Text.StringBuilder)
extern "C"  bool SimpleJson_SerializeString_m480651493 (Il2CppObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.SimpleJson::SerializeNumber(System.Object,System.Text.StringBuilder)
extern "C"  bool SimpleJson_SerializeNumber_m2996755647 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___number0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.SimpleJson::IsNumeric(System.Object)
extern "C"  bool SimpleJson_IsNumeric_m287984545 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.IJsonSerializerStrategy PlayFab.SimpleJson::get_CurrentJsonSerializerStrategy()
extern "C"  Il2CppObject * SimpleJson_get_CurrentJsonSerializerStrategy_m4269179241 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.SimpleJson::set_CurrentJsonSerializerStrategy(PlayFab.IJsonSerializerStrategy)
extern "C"  void SimpleJson_set_CurrentJsonSerializerStrategy_m3239879242 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.PocoJsonSerializerStrategy PlayFab.SimpleJson::get_PocoJsonSerializerStrategy()
extern "C"  PocoJsonSerializerStrategy_t344526361 * SimpleJson_get_PocoJsonSerializerStrategy_m767722985 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
