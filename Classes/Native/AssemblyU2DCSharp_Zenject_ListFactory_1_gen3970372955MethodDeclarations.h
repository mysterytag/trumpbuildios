﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ListFactory`1<System.Object>
struct ListFactory_1_t3970372955;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"

// System.Void Zenject.ListFactory`1<System.Object>::.ctor(System.Collections.Generic.List`1<System.Type>,Zenject.DiContainer)
extern "C"  void ListFactory_1__ctor_m3964182025_gshared (ListFactory_1_t3970372955 * __this, List_1_t3576188904 * ___implTypes0, DiContainer_t2383114449 * ___container1, const MethodInfo* method);
#define ListFactory_1__ctor_m3964182025(__this, ___implTypes0, ___container1, method) ((  void (*) (ListFactory_1_t3970372955 *, List_1_t3576188904 *, DiContainer_t2383114449 *, const MethodInfo*))ListFactory_1__ctor_m3964182025_gshared)(__this, ___implTypes0, ___container1, method)
// System.Void Zenject.ListFactory`1<System.Object>::Bind()
extern "C"  void ListFactory_1_Bind_m219167332_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListFactory_1_Bind_m219167332(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListFactory_1_Bind_m219167332_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Zenject.ListFactory`1<System.Object>::Create(System.Object[])
extern "C"  List_1_t1634065389 * ListFactory_1_Create_m2666892410_gshared (ListFactory_1_t3970372955 * __this, ObjectU5BU5D_t11523773* ___constructorArgs0, const MethodInfo* method);
#define ListFactory_1_Create_m2666892410(__this, ___constructorArgs0, method) ((  List_1_t1634065389 * (*) (ListFactory_1_t3970372955 *, ObjectU5BU5D_t11523773*, const MethodInfo*))ListFactory_1_Create_m2666892410_gshared)(__this, ___constructorArgs0, method)
