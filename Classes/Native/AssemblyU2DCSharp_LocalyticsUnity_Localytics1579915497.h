﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LocalyticsUnity.Localytics/LocalyticsDidTagEvent
struct LocalyticsDidTagEvent_t1639420300;
// LocalyticsUnity.Localytics/LocalyticsSessionDidOpen
struct LocalyticsSessionDidOpen_t115241990;
// LocalyticsUnity.Localytics/LocalyticsSessionWillClose
struct LocalyticsSessionWillClose_t2690468515;
// LocalyticsUnity.Localytics/LocalyticsSessionWillOpen
struct LocalyticsSessionWillOpen_t2581002303;
// LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage
struct LocalyticsDidDismissInAppMessage_t1872466793;
// LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage
struct LocalyticsDidDisplayInAppMessage_t3613222305;
// LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage
struct LocalyticsWillDismissInAppMessage_t2580912496;
// LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage
struct LocalyticsWillDisplayInAppMessage_t26700712;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalyticsUnity.Localytics
struct  Localytics_t1579915497  : public Il2CppObject
{
public:

public:
};

struct Localytics_t1579915497_StaticFields
{
public:
	// LocalyticsUnity.Localytics/LocalyticsDidTagEvent LocalyticsUnity.Localytics::OnLocalyticsDidTagEvent
	LocalyticsDidTagEvent_t1639420300 * ___OnLocalyticsDidTagEvent_0;
	// LocalyticsUnity.Localytics/LocalyticsSessionDidOpen LocalyticsUnity.Localytics::OnLocalyticsSessionDidOpen
	LocalyticsSessionDidOpen_t115241990 * ___OnLocalyticsSessionDidOpen_1;
	// LocalyticsUnity.Localytics/LocalyticsSessionWillClose LocalyticsUnity.Localytics::OnLocalyticsSessionWillClose
	LocalyticsSessionWillClose_t2690468515 * ___OnLocalyticsSessionWillClose_2;
	// LocalyticsUnity.Localytics/LocalyticsSessionWillOpen LocalyticsUnity.Localytics::OnLocalyticsSessionWillOpen
	LocalyticsSessionWillOpen_t2581002303 * ___OnLocalyticsSessionWillOpen_3;
	// LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage LocalyticsUnity.Localytics::OnLocalyticsDidDismissInAppMessage
	LocalyticsDidDismissInAppMessage_t1872466793 * ___OnLocalyticsDidDismissInAppMessage_4;
	// LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage LocalyticsUnity.Localytics::OnLocalyticsDidDisplayInAppMessage
	LocalyticsDidDisplayInAppMessage_t3613222305 * ___OnLocalyticsDidDisplayInAppMessage_5;
	// LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage LocalyticsUnity.Localytics::OnLocalyticsWillDismissInAppMessage
	LocalyticsWillDismissInAppMessage_t2580912496 * ___OnLocalyticsWillDismissInAppMessage_6;
	// LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage LocalyticsUnity.Localytics::OnLocalyticsWillDisplayInAppMessage
	LocalyticsWillDisplayInAppMessage_t26700712 * ___OnLocalyticsWillDisplayInAppMessage_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> LocalyticsUnity.Localytics::<>f__switch$map1
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map1_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> LocalyticsUnity.Localytics::<>f__switch$map2
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map2_9;

public:
	inline static int32_t get_offset_of_OnLocalyticsDidTagEvent_0() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___OnLocalyticsDidTagEvent_0)); }
	inline LocalyticsDidTagEvent_t1639420300 * get_OnLocalyticsDidTagEvent_0() const { return ___OnLocalyticsDidTagEvent_0; }
	inline LocalyticsDidTagEvent_t1639420300 ** get_address_of_OnLocalyticsDidTagEvent_0() { return &___OnLocalyticsDidTagEvent_0; }
	inline void set_OnLocalyticsDidTagEvent_0(LocalyticsDidTagEvent_t1639420300 * value)
	{
		___OnLocalyticsDidTagEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocalyticsDidTagEvent_0, value);
	}

	inline static int32_t get_offset_of_OnLocalyticsSessionDidOpen_1() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___OnLocalyticsSessionDidOpen_1)); }
	inline LocalyticsSessionDidOpen_t115241990 * get_OnLocalyticsSessionDidOpen_1() const { return ___OnLocalyticsSessionDidOpen_1; }
	inline LocalyticsSessionDidOpen_t115241990 ** get_address_of_OnLocalyticsSessionDidOpen_1() { return &___OnLocalyticsSessionDidOpen_1; }
	inline void set_OnLocalyticsSessionDidOpen_1(LocalyticsSessionDidOpen_t115241990 * value)
	{
		___OnLocalyticsSessionDidOpen_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocalyticsSessionDidOpen_1, value);
	}

	inline static int32_t get_offset_of_OnLocalyticsSessionWillClose_2() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___OnLocalyticsSessionWillClose_2)); }
	inline LocalyticsSessionWillClose_t2690468515 * get_OnLocalyticsSessionWillClose_2() const { return ___OnLocalyticsSessionWillClose_2; }
	inline LocalyticsSessionWillClose_t2690468515 ** get_address_of_OnLocalyticsSessionWillClose_2() { return &___OnLocalyticsSessionWillClose_2; }
	inline void set_OnLocalyticsSessionWillClose_2(LocalyticsSessionWillClose_t2690468515 * value)
	{
		___OnLocalyticsSessionWillClose_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocalyticsSessionWillClose_2, value);
	}

	inline static int32_t get_offset_of_OnLocalyticsSessionWillOpen_3() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___OnLocalyticsSessionWillOpen_3)); }
	inline LocalyticsSessionWillOpen_t2581002303 * get_OnLocalyticsSessionWillOpen_3() const { return ___OnLocalyticsSessionWillOpen_3; }
	inline LocalyticsSessionWillOpen_t2581002303 ** get_address_of_OnLocalyticsSessionWillOpen_3() { return &___OnLocalyticsSessionWillOpen_3; }
	inline void set_OnLocalyticsSessionWillOpen_3(LocalyticsSessionWillOpen_t2581002303 * value)
	{
		___OnLocalyticsSessionWillOpen_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocalyticsSessionWillOpen_3, value);
	}

	inline static int32_t get_offset_of_OnLocalyticsDidDismissInAppMessage_4() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___OnLocalyticsDidDismissInAppMessage_4)); }
	inline LocalyticsDidDismissInAppMessage_t1872466793 * get_OnLocalyticsDidDismissInAppMessage_4() const { return ___OnLocalyticsDidDismissInAppMessage_4; }
	inline LocalyticsDidDismissInAppMessage_t1872466793 ** get_address_of_OnLocalyticsDidDismissInAppMessage_4() { return &___OnLocalyticsDidDismissInAppMessage_4; }
	inline void set_OnLocalyticsDidDismissInAppMessage_4(LocalyticsDidDismissInAppMessage_t1872466793 * value)
	{
		___OnLocalyticsDidDismissInAppMessage_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocalyticsDidDismissInAppMessage_4, value);
	}

	inline static int32_t get_offset_of_OnLocalyticsDidDisplayInAppMessage_5() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___OnLocalyticsDidDisplayInAppMessage_5)); }
	inline LocalyticsDidDisplayInAppMessage_t3613222305 * get_OnLocalyticsDidDisplayInAppMessage_5() const { return ___OnLocalyticsDidDisplayInAppMessage_5; }
	inline LocalyticsDidDisplayInAppMessage_t3613222305 ** get_address_of_OnLocalyticsDidDisplayInAppMessage_5() { return &___OnLocalyticsDidDisplayInAppMessage_5; }
	inline void set_OnLocalyticsDidDisplayInAppMessage_5(LocalyticsDidDisplayInAppMessage_t3613222305 * value)
	{
		___OnLocalyticsDidDisplayInAppMessage_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocalyticsDidDisplayInAppMessage_5, value);
	}

	inline static int32_t get_offset_of_OnLocalyticsWillDismissInAppMessage_6() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___OnLocalyticsWillDismissInAppMessage_6)); }
	inline LocalyticsWillDismissInAppMessage_t2580912496 * get_OnLocalyticsWillDismissInAppMessage_6() const { return ___OnLocalyticsWillDismissInAppMessage_6; }
	inline LocalyticsWillDismissInAppMessage_t2580912496 ** get_address_of_OnLocalyticsWillDismissInAppMessage_6() { return &___OnLocalyticsWillDismissInAppMessage_6; }
	inline void set_OnLocalyticsWillDismissInAppMessage_6(LocalyticsWillDismissInAppMessage_t2580912496 * value)
	{
		___OnLocalyticsWillDismissInAppMessage_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocalyticsWillDismissInAppMessage_6, value);
	}

	inline static int32_t get_offset_of_OnLocalyticsWillDisplayInAppMessage_7() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___OnLocalyticsWillDisplayInAppMessage_7)); }
	inline LocalyticsWillDisplayInAppMessage_t26700712 * get_OnLocalyticsWillDisplayInAppMessage_7() const { return ___OnLocalyticsWillDisplayInAppMessage_7; }
	inline LocalyticsWillDisplayInAppMessage_t26700712 ** get_address_of_OnLocalyticsWillDisplayInAppMessage_7() { return &___OnLocalyticsWillDisplayInAppMessage_7; }
	inline void set_OnLocalyticsWillDisplayInAppMessage_7(LocalyticsWillDisplayInAppMessage_t26700712 * value)
	{
		___OnLocalyticsWillDisplayInAppMessage_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnLocalyticsWillDisplayInAppMessage_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_8() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___U3CU3Ef__switchU24map1_8)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map1_8() const { return ___U3CU3Ef__switchU24map1_8; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map1_8() { return &___U3CU3Ef__switchU24map1_8; }
	inline void set_U3CU3Ef__switchU24map1_8(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map1_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_9() { return static_cast<int32_t>(offsetof(Localytics_t1579915497_StaticFields, ___U3CU3Ef__switchU24map2_9)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map2_9() const { return ___U3CU3Ef__switchU24map2_9; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map2_9() { return &___U3CU3Ef__switchU24map2_9; }
	inline void set_U3CU3Ef__switchU24map2_9(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map2_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
