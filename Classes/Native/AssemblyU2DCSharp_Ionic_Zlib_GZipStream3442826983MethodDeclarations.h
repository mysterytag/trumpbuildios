﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.GZipStream
struct GZipStream_t3442826983;
// System.IO.Stream
struct Stream_t219029575;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionMode1632830838.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_FlushType3984958891.h"
#include "mscorlib_System_IO_SeekOrigin3506139269.h"

// System.Void Ionic.Zlib.GZipStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode)
extern "C"  void GZipStream__ctor_m2177275843 (GZipStream_t3442826983 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,Ionic.Zlib.CompressionLevel)
extern "C"  void GZipStream__ctor_m3348473338 (GZipStream_t3442826983 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, int32_t ___level2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,System.Boolean)
extern "C"  void GZipStream__ctor_m3769949530 (GZipStream_t3442826983 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, bool ___leaveOpen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,Ionic.Zlib.CompressionLevel,System.Boolean)
extern "C"  void GZipStream__ctor_m3548297795 (GZipStream_t3442826983 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, int32_t ___level2, bool ___leaveOpen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::.cctor()
extern "C"  void GZipStream__cctor_m2615593683 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ionic.Zlib.GZipStream::get_Comment()
extern "C"  String_t* GZipStream_get_Comment_m1442177425 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::set_Comment(System.String)
extern "C"  void GZipStream_set_Comment_m367082056 (GZipStream_t3442826983 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ionic.Zlib.GZipStream::get_FileName()
extern "C"  String_t* GZipStream_get_FileName_m2499547543 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::set_FileName(System.String)
extern "C"  void GZipStream_set_FileName_m3702495348 (GZipStream_t3442826983 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.GZipStream::get_Crc32()
extern "C"  int32_t GZipStream_get_Crc32_m3311789776 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.FlushType Ionic.Zlib.GZipStream::get_FlushMode()
extern "C"  int32_t GZipStream_get_FlushMode_m2705837836 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::set_FlushMode(Ionic.Zlib.FlushType)
extern "C"  void GZipStream_set_FlushMode_m4124057307 (GZipStream_t3442826983 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.GZipStream::get_BufferSize()
extern "C"  int32_t GZipStream_get_BufferSize_m383382342 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::set_BufferSize(System.Int32)
extern "C"  void GZipStream_set_BufferSize_m3678222553 (GZipStream_t3442826983 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.GZipStream::get_TotalIn()
extern "C"  int64_t GZipStream_get_TotalIn_m2535563687 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.GZipStream::get_TotalOut()
extern "C"  int64_t GZipStream_get_TotalOut_m1298885678 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::Dispose(System.Boolean)
extern "C"  void GZipStream_Dispose_m2484601614 (GZipStream_t3442826983 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.GZipStream::get_CanRead()
extern "C"  bool GZipStream_get_CanRead_m2203298569 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.GZipStream::get_CanSeek()
extern "C"  bool GZipStream_get_CanSeek_m2232053611 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.GZipStream::get_CanWrite()
extern "C"  bool GZipStream_get_CanWrite_m105432430 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::Flush()
extern "C"  void GZipStream_Flush_m1846440764 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.GZipStream::get_Length()
extern "C"  int64_t GZipStream_get_Length_m1066993514 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.GZipStream::get_Position()
extern "C"  int64_t GZipStream_get_Position_m3400051757 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::set_Position(System.Int64)
extern "C"  void GZipStream_set_Position_m3463985314 (GZipStream_t3442826983 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.GZipStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t GZipStream_Read_m14052599 (GZipStream_t3442826983 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.GZipStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t GZipStream_Seek_m695136826 (GZipStream_t3442826983 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::SetLength(System.Int64)
extern "C"  void GZipStream_SetLength_m3031039602 (GZipStream_t3442826983 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.GZipStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void GZipStream_Write_m3558716082 (GZipStream_t3442826983 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.GZipStream::EmitHeader()
extern "C"  int32_t GZipStream_EmitHeader_m2322580124 (GZipStream_t3442826983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.GZipStream::CompressString(System.String)
extern "C"  ByteU5BU5D_t58506160* GZipStream_CompressString_m2054864911 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.GZipStream::CompressBuffer(System.Byte[])
extern "C"  ByteU5BU5D_t58506160* GZipStream_CompressBuffer_m4209387719 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ionic.Zlib.GZipStream::UncompressString(System.Byte[])
extern "C"  String_t* GZipStream_UncompressString_m4041902870 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___compressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.GZipStream::UncompressBuffer(System.Byte[])
extern "C"  ByteU5BU5D_t58506160* GZipStream_UncompressBuffer_m3218880654 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___compressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
