﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<ContinueWith>c__AnonStorey48`2<System.Object,System.Object>
struct U3CContinueWithU3Ec__AnonStorey48_2_t215346063;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<ContinueWith>c__AnonStorey48`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey48_2__ctor_m1053906337_gshared (U3CContinueWithU3Ec__AnonStorey48_2_t215346063 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey48_2__ctor_m1053906337(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey48_2_t215346063 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey48_2__ctor_m1053906337_gshared)(__this, method)
// UniRx.IObservable`1<TR> UniRx.Observable/<ContinueWith>c__AnonStorey48`2<System.Object,System.Object>::<>m__46(T)
extern "C"  Il2CppObject* U3CContinueWithU3Ec__AnonStorey48_2_U3CU3Em__46_m3330506120_gshared (U3CContinueWithU3Ec__AnonStorey48_2_t215346063 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey48_2_U3CU3Em__46_m3330506120(__this, ____0, method) ((  Il2CppObject* (*) (U3CContinueWithU3Ec__AnonStorey48_2_t215346063 *, Il2CppObject *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey48_2_U3CU3Em__46_m3330506120_gshared)(__this, ____0, method)
