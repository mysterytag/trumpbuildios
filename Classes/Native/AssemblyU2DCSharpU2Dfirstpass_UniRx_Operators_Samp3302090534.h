﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SampleFrameObservable`1<UniRx.Unit>
struct SampleFrameObservable_1_t3056629055;
// System.Object
struct Il2CppObject;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SampleFrameObservable`1/SampleFrame<UniRx.Unit>
struct  SampleFrame_t3302090534  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.SampleFrameObservable`1<T> UniRx.Operators.SampleFrameObservable`1/SampleFrame::parent
	SampleFrameObservable_1_t3056629055 * ___parent_2;
	// System.Object UniRx.Operators.SampleFrameObservable`1/SampleFrame::gate
	Il2CppObject * ___gate_3;
	// T UniRx.Operators.SampleFrameObservable`1/SampleFrame::latestValue
	Unit_t2558286038  ___latestValue_4;
	// System.Boolean UniRx.Operators.SampleFrameObservable`1/SampleFrame::isUpdated
	bool ___isUpdated_5;
	// System.Boolean UniRx.Operators.SampleFrameObservable`1/SampleFrame::isCompleted
	bool ___isCompleted_6;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.SampleFrameObservable`1/SampleFrame::sourceSubscription
	SingleAssignmentDisposable_t2336378823 * ___sourceSubscription_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SampleFrame_t3302090534, ___parent_2)); }
	inline SampleFrameObservable_1_t3056629055 * get_parent_2() const { return ___parent_2; }
	inline SampleFrameObservable_1_t3056629055 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SampleFrameObservable_1_t3056629055 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(SampleFrame_t3302090534, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_latestValue_4() { return static_cast<int32_t>(offsetof(SampleFrame_t3302090534, ___latestValue_4)); }
	inline Unit_t2558286038  get_latestValue_4() const { return ___latestValue_4; }
	inline Unit_t2558286038 * get_address_of_latestValue_4() { return &___latestValue_4; }
	inline void set_latestValue_4(Unit_t2558286038  value)
	{
		___latestValue_4 = value;
	}

	inline static int32_t get_offset_of_isUpdated_5() { return static_cast<int32_t>(offsetof(SampleFrame_t3302090534, ___isUpdated_5)); }
	inline bool get_isUpdated_5() const { return ___isUpdated_5; }
	inline bool* get_address_of_isUpdated_5() { return &___isUpdated_5; }
	inline void set_isUpdated_5(bool value)
	{
		___isUpdated_5 = value;
	}

	inline static int32_t get_offset_of_isCompleted_6() { return static_cast<int32_t>(offsetof(SampleFrame_t3302090534, ___isCompleted_6)); }
	inline bool get_isCompleted_6() const { return ___isCompleted_6; }
	inline bool* get_address_of_isCompleted_6() { return &___isCompleted_6; }
	inline void set_isCompleted_6(bool value)
	{
		___isCompleted_6 = value;
	}

	inline static int32_t get_offset_of_sourceSubscription_7() { return static_cast<int32_t>(offsetof(SampleFrame_t3302090534, ___sourceSubscription_7)); }
	inline SingleAssignmentDisposable_t2336378823 * get_sourceSubscription_7() const { return ___sourceSubscription_7; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_sourceSubscription_7() { return &___sourceSubscription_7; }
	inline void set_sourceSubscription_7(SingleAssignmentDisposable_t2336378823 * value)
	{
		___sourceSubscription_7 = value;
		Il2CppCodeGenWriteBarrier(&___sourceSubscription_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
