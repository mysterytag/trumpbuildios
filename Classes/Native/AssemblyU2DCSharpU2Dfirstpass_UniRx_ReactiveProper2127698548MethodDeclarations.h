﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>
struct ReactivePropertyObserver_t2127698548;
// UniRx.ReactiveProperty`1<UnityEngine.Quaternion>
struct ReactiveProperty_1_t2500049017;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m240166677_gshared (ReactivePropertyObserver_t2127698548 * __this, ReactiveProperty_1_t2500049017 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m240166677(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t2127698548 *, ReactiveProperty_1_t2500049017 *, const MethodInfo*))ReactivePropertyObserver__ctor_m240166677_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m414948598_gshared (ReactivePropertyObserver_t2127698548 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m414948598(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t2127698548 *, Quaternion_t1891715979 , const MethodInfo*))ReactivePropertyObserver_OnNext_m414948598_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m881003525_gshared (ReactivePropertyObserver_t2127698548 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m881003525(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t2127698548 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m881003525_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m1009245912_gshared (ReactivePropertyObserver_t2127698548 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m1009245912(__this, method) ((  void (*) (ReactivePropertyObserver_t2127698548 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m1009245912_gshared)(__this, method)
