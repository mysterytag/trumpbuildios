﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Counter>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1481245507(__this, ___l0, method) ((  void (*) (Enumerator_t1505225893 *, List_1_t3419442901 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Counter>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3765097263(__this, method) ((  void (*) (Enumerator_t1505225893 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Counter>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2195829083(__this, method) ((  Il2CppObject * (*) (Enumerator_t1505225893 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Counter>::Dispose()
#define Enumerator_Dispose_m1168986600(__this, method) ((  void (*) (Enumerator_t1505225893 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Counter>::VerifyState()
#define Enumerator_VerifyState_m2459295009(__this, method) ((  void (*) (Enumerator_t1505225893 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Counter>::MoveNext()
#define Enumerator_MoveNext_m2602472987(__this, method) ((  bool (*) (Enumerator_t1505225893 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Counter>::get_Current()
#define Enumerator_get_Current_m3615891544(__this, method) ((  Counter_t2622483932 * (*) (Enumerator_t1505225893 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
