﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1<System.Object>
struct U3CFilterU3Ec__AnonStorey11E_1_t4290970890;

#include "codegen/il2cpp-codegen.h"

// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1<System.Object>::.ctor()
extern "C"  void U3CFilterU3Ec__AnonStorey11E_1__ctor_m671944929_gshared (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * __this, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey11E_1__ctor_m671944929(__this, method) ((  void (*) (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 *, const MethodInfo*))U3CFilterU3Ec__AnonStorey11E_1__ctor_m671944929_gshared)(__this, method)
// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1<System.Object>::<>m__1AB(System.Boolean)
extern "C"  void U3CFilterU3Ec__AnonStorey11E_1_U3CU3Em__1AB_m3060285249_gshared (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 * __this, bool ___b0, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey11E_1_U3CU3Em__1AB_m3060285249(__this, ___b0, method) ((  void (*) (U3CFilterU3Ec__AnonStorey11E_1_t4290970890 *, bool, const MethodInfo*))U3CFilterU3Ec__AnonStorey11E_1_U3CU3Em__1AB_m3060285249_gshared)(__this, ___b0, method)
