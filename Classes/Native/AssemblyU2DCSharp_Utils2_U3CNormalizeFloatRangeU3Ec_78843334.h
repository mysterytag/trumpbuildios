﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils2/<NormalizeFloatRange>c__AnonStoreyF1
struct  U3CNormalizeFloatRangeU3Ec__AnonStoreyF1_t78843334  : public Il2CppObject
{
public:
	// System.Single Utils2/<NormalizeFloatRange>c__AnonStoreyF1::magnitude
	float ___magnitude_0;

public:
	inline static int32_t get_offset_of_magnitude_0() { return static_cast<int32_t>(offsetof(U3CNormalizeFloatRangeU3Ec__AnonStoreyF1_t78843334, ___magnitude_0)); }
	inline float get_magnitude_0() const { return ___magnitude_0; }
	inline float* get_address_of_magnitude_0() { return &___magnitude_0; }
	inline void set_magnitude_0(float value)
	{
		___magnitude_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
