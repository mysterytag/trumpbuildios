﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>
struct Throttle_t1826605473;
// UniRx.Operators.ThrottleObservable`1<System.Int64>
struct ThrottleObservable_1_t3227609402;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::.ctor(UniRx.Operators.ThrottleObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Throttle__ctor_m2529190470_gshared (Throttle_t1826605473 * __this, ThrottleObservable_1_t3227609402 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Throttle__ctor_m2529190470(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Throttle_t1826605473 *, ThrottleObservable_1_t3227609402 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Throttle__ctor_m2529190470_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::Run()
extern "C"  Il2CppObject * Throttle_Run_m2383935619_gshared (Throttle_t1826605473 * __this, const MethodInfo* method);
#define Throttle_Run_m2383935619(__this, method) ((  Il2CppObject * (*) (Throttle_t1826605473 *, const MethodInfo*))Throttle_Run_m2383935619_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::OnNext(System.UInt64)
extern "C"  void Throttle_OnNext_m1768585380_gshared (Throttle_t1826605473 * __this, uint64_t ___currentid0, const MethodInfo* method);
#define Throttle_OnNext_m1768585380(__this, ___currentid0, method) ((  void (*) (Throttle_t1826605473 *, uint64_t, const MethodInfo*))Throttle_OnNext_m1768585380_gshared)(__this, ___currentid0, method)
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::OnNext(T)
extern "C"  void Throttle_OnNext_m781399815_gshared (Throttle_t1826605473 * __this, int64_t ___value0, const MethodInfo* method);
#define Throttle_OnNext_m781399815(__this, ___value0, method) ((  void (*) (Throttle_t1826605473 *, int64_t, const MethodInfo*))Throttle_OnNext_m781399815_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::OnError(System.Exception)
extern "C"  void Throttle_OnError_m1194222614_gshared (Throttle_t1826605473 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Throttle_OnError_m1194222614(__this, ___error0, method) ((  void (*) (Throttle_t1826605473 *, Exception_t1967233988 *, const MethodInfo*))Throttle_OnError_m1194222614_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle<System.Int64>::OnCompleted()
extern "C"  void Throttle_OnCompleted_m160565353_gshared (Throttle_t1826605473 * __this, const MethodInfo* method);
#define Throttle_OnCompleted_m160565353(__this, method) ((  void (*) (Throttle_t1826605473 *, const MethodInfo*))Throttle_OnCompleted_m160565353_gshared)(__this, method)
