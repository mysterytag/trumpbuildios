﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Diagnostics.ObservableLogger
struct ObservableLogger_t586897455;
// System.Action`1<UniRx.Diagnostics.LogEntry>
struct Action_1_t2039608427;
// UniRx.Diagnostics.Logger
struct Logger_t2118475020;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Diagnostics.LogEntry>
struct IObserver_1_t4103154625;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo2118475020.h"

// System.Void UniRx.Diagnostics.ObservableLogger::.ctor()
extern "C"  void ObservableLogger__ctor_m2728320066 (ObservableLogger_t586897455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.ObservableLogger::.cctor()
extern "C"  void ObservableLogger__cctor_m2491447211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<UniRx.Diagnostics.LogEntry> UniRx.Diagnostics.ObservableLogger::RegisterLogger(UniRx.Diagnostics.Logger)
extern "C"  Action_1_t2039608427 * ObservableLogger_RegisterLogger_m4245567678 (Il2CppObject * __this /* static, unused */, Logger_t2118475020 * ___logger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Diagnostics.ObservableLogger::Subscribe(UniRx.IObserver`1<UniRx.Diagnostics.LogEntry>)
extern "C"  Il2CppObject * ObservableLogger_Subscribe_m1405270705 (ObservableLogger_t586897455 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
