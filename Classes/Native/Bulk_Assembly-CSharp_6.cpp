﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// PlayFab.ClientModels.LinkPSNAccountRequest
struct LinkPSNAccountRequest_t4270489187;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkPSNAccountResult
struct LinkPSNAccountResult_t2251115497;
// PlayFab.ClientModels.LinkSteamAccountRequest
struct LinkSteamAccountRequest_t3142953640;
// PlayFab.ClientModels.LinkSteamAccountResult
struct LinkSteamAccountResult_t3738764036;
// PlayFab.ClientModels.LinkXboxAccountRequest
struct LinkXboxAccountRequest_t4157425311;
// PlayFab.ClientModels.LinkXboxAccountResult
struct LinkXboxAccountResult_t4187130925;
// PlayFab.ClientModels.ListUsersCharactersRequest
struct ListUsersCharactersRequest_t3884929547;
// PlayFab.ClientModels.ListUsersCharactersResult
struct ListUsersCharactersResult_t1961583425;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterResult>
struct List_1_t1071583631;
// PlayFab.ClientModels.LogEventRequest
struct LogEventRequest_t3812150249;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// PlayFab.ClientModels.LogEventResult
struct LogEventResult_t3344709027;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.ClientModels.UserSettings
struct UserSettings_t8286526;
// PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest
struct LoginWithAndroidDeviceIDRequest_t522798990;
// PlayFab.ClientModels.LoginWithCustomIDRequest
struct LoginWithCustomIDRequest_t3362330884;
// PlayFab.ClientModels.LoginWithEmailAddressRequest
struct LoginWithEmailAddressRequest_t3586495128;
// PlayFab.ClientModels.LoginWithFacebookRequest
struct LoginWithFacebookRequest_t58367306;
// PlayFab.ClientModels.LoginWithGameCenterRequest
struct LoginWithGameCenterRequest_t1605888457;
// PlayFab.ClientModels.LoginWithGoogleAccountRequest
struct LoginWithGoogleAccountRequest_t692379802;
// PlayFab.ClientModels.LoginWithIOSDeviceIDRequest
struct LoginWithIOSDeviceIDRequest_t318822832;
// PlayFab.ClientModels.LoginWithKongregateRequest
struct LoginWithKongregateRequest_t366847061;
// PlayFab.ClientModels.LoginWithPlayFabRequest
struct LoginWithPlayFabRequest_t402940283;
// PlayFab.ClientModels.LoginWithPSNRequest
struct LoginWithPSNRequest_t4060832355;
// PlayFab.ClientModels.LoginWithSteamRequest
struct LoginWithSteamRequest_t700401790;
// PlayFab.ClientModels.LoginWithXboxRequest
struct LoginWithXboxRequest_t2356925853;
// PlayFab.ClientModels.MatchmakeRequest
struct MatchmakeRequest_t4152079884;
// PlayFab.ClientModels.MatchmakeResult
struct MatchmakeResult_t30538528;
// PlayFab.ClientModels.ModifyUserVirtualCurrencyResult
struct ModifyUserVirtualCurrencyResult_t448487620;
// PlayFab.ClientModels.OpenTradeRequest
struct OpenTradeRequest_t1121233957;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// PlayFab.ClientModels.OpenTradeResponse
struct OpenTradeResponse_t3658687371;
// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;
// PlayFab.ClientModels.PayForPurchaseRequest
struct PayForPurchaseRequest_t2884624637;
// PlayFab.ClientModels.PayForPurchaseResult
struct PayForPurchaseResult_t128200207;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// PlayFab.ClientModels.PaymentOption
struct PaymentOption_t1289274891;
// PlayFab.ClientModels.PlayerLeaderboardEntry
struct PlayerLeaderboardEntry_t2241891366;
// PlayFab.ClientModels.PSNAccountPlayFabIdPair
struct PSNAccountPlayFabIdPair_t680553942;
// PlayFab.ClientModels.PurchaseItemRequest
struct PurchaseItemRequest_t3673358379;
// PlayFab.ClientModels.PurchaseItemResult
struct PurchaseItemResult_t2370400545;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// PlayFab.ClientModels.RedeemCouponRequest
struct RedeemCouponRequest_t2632721213;
// PlayFab.ClientModels.RedeemCouponResult
struct RedeemCouponResult_t1089905615;
// PlayFab.ClientModels.RefreshPSNAuthTokenRequest
struct RefreshPSNAuthTokenRequest_t4115941982;
// PlayFab.ClientModels.RegionInfo
struct RegionInfo_t3474869746;
// PlayFab.ClientModels.RegisterForIOSPushNotificationRequest
struct RegisterForIOSPushNotificationRequest_t2944199315;
// PlayFab.ClientModels.RegisterForIOSPushNotificationResult
struct RegisterForIOSPushNotificationResult_t2485426617;
// PlayFab.ClientModels.RegisterPlayFabUserRequest
struct RegisterPlayFabUserRequest_t2314572612;
// PlayFab.ClientModels.RegisterPlayFabUserResult
struct RegisterPlayFabUserResult_t109811432;
// PlayFab.ClientModels.RemoveFriendRequest
struct RemoveFriendRequest_t4041798621;
// PlayFab.ClientModels.RemoveFriendResult
struct RemoveFriendResult_t3352117039;
// PlayFab.ClientModels.RemoveSharedGroupMembersRequest
struct RemoveSharedGroupMembersRequest_t2614915772;
// PlayFab.ClientModels.RemoveSharedGroupMembersResult
struct RemoveSharedGroupMembersResult_t3306088560;
// PlayFab.ClientModels.ReportPlayerClientRequest
struct ReportPlayerClientRequest_t3784712447;
// PlayFab.ClientModels.ReportPlayerClientResult
struct ReportPlayerClientResult_t3759465933;
// PlayFab.ClientModels.RestoreIOSPurchasesRequest
struct RestoreIOSPurchasesRequest_t2671348620;
// PlayFab.ClientModels.RestoreIOSPurchasesResult
struct RestoreIOSPurchasesResult_t675509664;
// PlayFab.ClientModels.RunCloudScriptRequest
struct RunCloudScriptRequest_t180913386;
// System.Object
struct Il2CppObject;
// PlayFab.ClientModels.RunCloudScriptResult
struct RunCloudScriptResult_t3227572354;
// PlayFab.ClientModels.SendAccountRecoveryEmailRequest
struct SendAccountRecoveryEmailRequest_t558031005;
// PlayFab.ClientModels.SendAccountRecoveryEmailResult
struct SendAccountRecoveryEmailResult_t3793926767;
// PlayFab.ClientModels.SetFriendTagsRequest
struct SetFriendTagsRequest_t1987017382;
// PlayFab.ClientModels.SetFriendTagsResult
struct SetFriendTagsResult_t4117117766;
// PlayFab.ClientModels.SharedGroupDataRecord
struct SharedGroupDataRecord_t3101610309;
// PlayFab.ClientModels.StartGameRequest
struct StartGameRequest_t2370915499;
// PlayFab.ClientModels.StartGameResult
struct StartGameResult_t665818273;
// PlayFab.ClientModels.StartPurchaseRequest
struct StartPurchaseRequest_t2105128380;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemPurchaseRequest>
struct List_1_t1528421284;
// PlayFab.ClientModels.StartPurchaseResult
struct StartPurchaseResult_t1765623152;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>
struct List_1_t45083516;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PaymentOption>
struct List_1_t2086233860;
// PlayFab.ClientModels.StatisticUpdate
struct StatisticUpdate_t3411289641;
// PlayFab.ClientModels.StatisticValue
struct StatisticValue_t3193655857;
// PlayFab.ClientModels.SteamPlayFabIdPair
struct SteamPlayFabIdPair_t2684316776;
// PlayFab.ClientModels.StoreItem
struct StoreItem_t2872884100;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;
// PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest
struct SubtractUserVirtualCurrencyRequest_t3004138178;
// PlayFab.ClientModels.TitleNewsItem
struct TitleNewsItem_t2523316974;
// PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest
struct UnlinkAndroidDeviceIDRequest_t1293232914;
// PlayFab.ClientModels.UnlinkAndroidDeviceIDResult
struct UnlinkAndroidDeviceIDResult_t4233284954;
// PlayFab.ClientModels.UnlinkCustomIDRequest
struct UnlinkCustomIDRequest_t1348755968;
// PlayFab.ClientModels.UnlinkCustomIDResult
struct UnlinkCustomIDResult_t3957981356;
// PlayFab.ClientModels.UnlinkFacebookAccountRequest
struct UnlinkFacebookAccountRequest_t3175165483;
// PlayFab.ClientModels.UnlinkFacebookAccountResult
struct UnlinkFacebookAccountResult_t830309153;
// PlayFab.ClientModels.UnlinkGameCenterAccountRequest
struct UnlinkGameCenterAccountRequest_t687920076;
// PlayFab.ClientModels.UnlinkGameCenterAccountResult
struct UnlinkGameCenterAccountResult_t2689738080;
// PlayFab.ClientModels.UnlinkGoogleAccountRequest
struct UnlinkGoogleAccountRequest_t4045133086;
// PlayFab.ClientModels.UnlinkGoogleAccountResult
struct UnlinkGoogleAccountResult_t2936582606;
// PlayFab.ClientModels.UnlinkIOSDeviceIDRequest
struct UnlinkIOSDeviceIDRequest_t1716723508;
// PlayFab.ClientModels.UnlinkIOSDeviceIDResult
struct UnlinkIOSDeviceIDResult_t3277114616;
// PlayFab.ClientModels.UnlinkKongregateAccountRequest
struct UnlinkKongregateAccountRequest_t2566546368;
// PlayFab.ClientModels.UnlinkKongregateAccountResult
struct UnlinkKongregateAccountResult_t2611791596;
// PlayFab.ClientModels.UnlinkPSNAccountRequest
struct UnlinkPSNAccountRequest_t26689898;
// PlayFab.ClientModels.UnlinkPSNAccountResult
struct UnlinkPSNAccountResult_t2391313410;
// PlayFab.ClientModels.UnlinkSteamAccountRequest
struct UnlinkSteamAccountRequest_t775800815;
// PlayFab.ClientModels.UnlinkSteamAccountResult
struct UnlinkSteamAccountResult_t1030004957;
// PlayFab.ClientModels.UnlinkXboxAccountRequest
struct UnlinkXboxAccountRequest_t1448666232;
// PlayFab.ClientModels.UnlinkXboxAccountResult
struct UnlinkXboxAccountResult_t4238298932;
// PlayFab.ClientModels.UnlockContainerInstanceRequest
struct UnlockContainerInstanceRequest_t1476418829;
// PlayFab.ClientModels.UnlockContainerItemRequest
struct UnlockContainerItemRequest_t987080271;
// PlayFab.ClientModels.UnlockContainerItemResult
struct UnlockContainerItemResult_t2837935741;
// PlayFab.ClientModels.UpdateCharacterDataRequest
struct UpdateCharacterDataRequest_t3002197813;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// PlayFab.ClientModels.UpdateCharacterDataResult
struct UpdateCharacterDataResult_t1101824215;
// PlayFab.ClientModels.UpdateCharacterStatisticsRequest
struct UpdateCharacterStatisticsRequest_t1560413084;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkPSNAcco4270489187.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkPSNAcco4270489187MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkPSNAcco2251115497.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkPSNAcco2251115497MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkSteamAc3142953640.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkSteamAc3142953640MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkSteamAc3738764036.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkSteamAc3738764036MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkXboxAcc4157425311.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkXboxAcc4157425311MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkXboxAcc4187130925.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkXboxAcc4187130925MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ListUsersCh3884929547.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ListUsersCh3884929547MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ListUsersCh1961583425.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ListUsersCh1961583425MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1071583631.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LogEventReq3812150249.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LogEventReq3812150249MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LogEventRes3344709027.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LogEventRes3344709027MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserSettings8286526.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithAnd522798990.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithAnd522798990MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithCu3362330884.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithCu3362330884MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithEm3586495128.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithEm3586495128MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithFace58367306.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithFace58367306MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithGa1605888457.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithGa1605888457MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithGoo692379802.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithGoo692379802MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithIOS318822832.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithIOS318822832MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithKon366847061.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithKon366847061MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithPla402940283.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithPla402940283MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithPS4060832355.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithPS4060832355MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithSte700401790.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithSte700401790MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithXb2356925853.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithXb2356925853MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeRe4152079884.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeRe4152079884MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeResu30538528.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeResu30538528MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2958521481.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeStat72483573.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeStat72483573MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ModifyUserVi448487620.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ModifyUserVi448487620MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_OpenTradeRe1121233957.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_OpenTradeRe1121233957MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_OpenTradeRe3658687371.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_OpenTradeRe3658687371MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PayForPurch2884624637.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PayForPurch2884624637MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PayForPurcha128200207.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PayForPurcha128200207MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen4258606580.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "mscorlib_System_UInt32985925326.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PaymentOpti1289274891.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PaymentOpti1289274891MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PlayerLeade2241891366.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PlayerLeade2241891366MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PSNAccountPl680553942.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PSNAccountPl680553942MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PurchaseIte3673358379.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PurchaseIte3673358379MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PurchaseIte2370400545.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PurchaseIte2370400545MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1322103281.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RedeemCoupo2632721213.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RedeemCoupo2632721213MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RedeemCoupo1089905615.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RedeemCoupo1089905615MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RefreshPSNA4115941982.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RefreshPSNA4115941982MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Region1621303076.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Region1621303076MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegionInfo3474869746.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegionInfo3474869746MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterFor2944199315.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterFor2944199315MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterFor2485426617.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterFor2485426617MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterPla2314572612.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterPla2314572612MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterPlay109811432.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegisterPlay109811432MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveFrien4041798621.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveFrien4041798621MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveFrien3352117039.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveFrien3352117039MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveShare2614915772.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveShare2614915772MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveShare3306088560.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveShare3306088560MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ReportPlaye3784712447.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ReportPlaye3784712447MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ReportPlaye3759465933.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ReportPlaye3759465933MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RestoreIOSP2671348620.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RestoreIOSP2671348620MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RestoreIOSPu675509664.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RestoreIOSPu675509664MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RunCloudScri180913386.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RunCloudScri180913386MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RunCloudScr3227572354.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RunCloudScr3227572354MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SendAccountR558031005.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SendAccountR558031005MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SendAccount3793926767.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SendAccount3793926767MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SetFriendTa1987017382.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SetFriendTa1987017382MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SetFriendTa4117117766.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SetFriendTa4117117766MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SharedGroup3101610309.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SharedGroup3101610309MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartGameRe2370915499.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartGameRe2370915499MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartGameRes665818273.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartGameRes665818273MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartPurcha2105128380.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartPurcha2105128380MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1528421284.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartPurcha1765623152.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartPurcha1765623152MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen45083516.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2086233860.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StatisticUp3411289641.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StatisticUp3411289641MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StatisticVa3193655857.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StatisticVa3193655857MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SteamPlayFa2684316776.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SteamPlayFa2684316776MethodDeclarations.h"
#include "mscorlib_System_UInt64985925421.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StoreItem2872884100.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StoreItem2872884100MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2623623230.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SubtractUse3004138178.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SubtractUse3004138178MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TitleActiva3893470992.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TitleActiva3893470992MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TitleNewsIt2523316974.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TitleNewsIt2523316974MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1403402234.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeStatus2812331622.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeStatus2812331622MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Transaction1372568672.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Transaction1372568672MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkAndro1293232914.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkAndro1293232914MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkAndro4233284954.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkAndro4233284954MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkCusto1348755968.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkCusto1348755968MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkCusto3957981356.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkCusto3957981356MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkFaceb3175165483.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkFaceb3175165483MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkFacebo830309153.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkFacebo830309153MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGameCe687920076.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGameCe687920076MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGameC2689738080.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGameC2689738080MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGoogl4045133086.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGoogl4045133086MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGoogl2936582606.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGoogl2936582606MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkIOSDe1716723508.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkIOSDe1716723508MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkIOSDe3277114616.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkIOSDe3277114616MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkKongr2566546368.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkKongr2566546368MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkKongr2611791596.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkKongr2611791596MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkPSNAcco26689898.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkPSNAcco26689898MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkPSNAc2391313410.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkPSNAc2391313410MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkSteamA775800815.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkSteamA775800815MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkSteam1030004957.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkSteam1030004957MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkXboxA1448666232.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkXboxA1448666232MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkXboxA4238298932.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkXboxA4238298932MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockConta1476418829.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockConta1476418829MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockContai987080271.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockContai987080271MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockConta2837935741.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockConta2837935741MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara3002197813.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara3002197813MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara1101824215.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara1101824215MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara1560413084.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara1560413084MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayFab.ClientModels.LinkPSNAccountRequest::.ctor()
extern "C"  void LinkPSNAccountRequest__ctor_m878120998 (LinkPSNAccountRequest_t4270489187 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkPSNAccountRequest::get_AuthCode()
extern "C"  String_t* LinkPSNAccountRequest_get_AuthCode_m3422249663 (LinkPSNAccountRequest_t4270489187 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAuthCodeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkPSNAccountRequest::set_AuthCode(System.String)
extern "C"  void LinkPSNAccountRequest_set_AuthCode_m1905259314 (LinkPSNAccountRequest_t4270489187 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAuthCodeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkPSNAccountRequest::get_RedirectUri()
extern "C"  String_t* LinkPSNAccountRequest_get_RedirectUri_m2914223368 (LinkPSNAccountRequest_t4270489187 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CRedirectUriU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkPSNAccountRequest::set_RedirectUri(System.String)
extern "C"  void LinkPSNAccountRequest_set_RedirectUri_m1960804363 (LinkPSNAccountRequest_t4270489187 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CRedirectUriU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.LinkPSNAccountRequest::get_IssuerId()
extern "C"  Nullable_1_t1438485399  LinkPSNAccountRequest_get_IssuerId_m833707210 (LinkPSNAccountRequest_t4270489187 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CIssuerIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkPSNAccountRequest::set_IssuerId(System.Nullable`1<System.Int32>)
extern "C"  void LinkPSNAccountRequest_set_IssuerId_m1280060511 (LinkPSNAccountRequest_t4270489187 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CIssuerIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkPSNAccountResult::.ctor()
extern "C"  void LinkPSNAccountResult__ctor_m1331369332 (LinkPSNAccountResult_t2251115497 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkSteamAccountRequest::.ctor()
extern "C"  void LinkSteamAccountRequest__ctor_m1930957825 (LinkSteamAccountRequest_t3142953640 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkSteamAccountRequest::get_SteamTicket()
extern "C"  String_t* LinkSteamAccountRequest_get_SteamTicket_m2742057327 (LinkSteamAccountRequest_t3142953640 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSteamTicketU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkSteamAccountRequest::set_SteamTicket(System.String)
extern "C"  void LinkSteamAccountRequest_set_SteamTicket_m4261820356 (LinkSteamAccountRequest_t3142953640 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSteamTicketU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkSteamAccountResult::.ctor()
extern "C"  void LinkSteamAccountResult__ctor_m4274825785 (LinkSteamAccountResult_t3738764036 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkXboxAccountRequest::.ctor()
extern "C"  void LinkXboxAccountRequest__ctor_m3647685886 (LinkXboxAccountRequest_t4157425311 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LinkXboxAccountRequest::get_XboxToken()
extern "C"  String_t* LinkXboxAccountRequest_get_XboxToken_m2550567228 (LinkXboxAccountRequest_t4157425311 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CXboxTokenU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LinkXboxAccountRequest::set_XboxToken(System.String)
extern "C"  void LinkXboxAccountRequest_set_XboxToken_m3906515581 (LinkXboxAccountRequest_t4157425311 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CXboxTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LinkXboxAccountResult::.ctor()
extern "C"  void LinkXboxAccountResult__ctor_m2667636124 (LinkXboxAccountResult_t4187130925 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.ListUsersCharactersRequest::.ctor()
extern "C"  void ListUsersCharactersRequest__ctor_m1969106450 (ListUsersCharactersRequest_t3884929547 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ListUsersCharactersRequest::get_PlayFabId()
extern "C"  String_t* ListUsersCharactersRequest_get_PlayFabId_m3394880280 (ListUsersCharactersRequest_t3884929547 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ListUsersCharactersRequest::set_PlayFabId(System.String)
extern "C"  void ListUsersCharactersRequest_set_PlayFabId_m319638689 (ListUsersCharactersRequest_t3884929547 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ListUsersCharactersResult::.ctor()
extern "C"  void ListUsersCharactersResult__ctor_m2336393736 (ListUsersCharactersResult_t1961583425 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterResult> PlayFab.ClientModels.ListUsersCharactersResult::get_Characters()
extern "C"  List_1_t1071583631 * ListUsersCharactersResult_get_Characters_m2847187146 (ListUsersCharactersResult_t1961583425 * __this, const MethodInfo* method)
{
	{
		List_1_t1071583631 * L_0 = __this->get_U3CCharactersU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ListUsersCharactersResult::set_Characters(System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterResult>)
extern "C"  void ListUsersCharactersResult_set_Characters_m2919384823 (ListUsersCharactersResult_t1961583425 * __this, List_1_t1071583631 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1071583631 * L_0 = ___value0;
		__this->set_U3CCharactersU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LogEventRequest::.ctor()
extern "C"  void LogEventRequest__ctor_m3950199264 (LogEventRequest_t3812150249 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.LogEventRequest::get_Timestamp()
extern "C"  Nullable_1_t3225071844  LogEventRequest_get_Timestamp_m1030575069 (LogEventRequest_t3812150249 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = __this->get_U3CTimestampU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LogEventRequest::set_Timestamp(System.Nullable`1<System.DateTime>)
extern "C"  void LogEventRequest_set_Timestamp_m3131855944 (LogEventRequest_t3812150249 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = ___value0;
		__this->set_U3CTimestampU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LogEventRequest::get_EventName()
extern "C"  String_t* LogEventRequest_get_EventName_m2885370711 (LogEventRequest_t3812150249 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEventNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LogEventRequest::set_EventName(System.String)
extern "C"  void LogEventRequest_set_EventName_m1245967388 (LogEventRequest_t3812150249 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CEventNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> PlayFab.ClientModels.LogEventRequest::get_Body()
extern "C"  Dictionary_2_t2474804324 * LogEventRequest_get_Body_m1640965547 (LogEventRequest_t3812150249 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_U3CBodyU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LogEventRequest::set_Body(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void LogEventRequest_set_Body_m1260650210 (LogEventRequest_t3812150249 * __this, Dictionary_2_t2474804324 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = ___value0;
		__this->set_U3CBodyU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.LogEventRequest::get_ProfileSetEvent()
extern "C"  bool LogEventRequest_get_ProfileSetEvent_m3098647618 (LogEventRequest_t3812150249 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CProfileSetEventU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LogEventRequest::set_ProfileSetEvent(System.Boolean)
extern "C"  void LogEventRequest_set_ProfileSetEvent_m811120505 (LogEventRequest_t3812150249 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CProfileSetEventU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LogEventResult::.ctor()
extern "C"  void LogEventResult__ctor_m460637306 (LogEventResult_t3344709027 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginResult::.ctor()
extern "C"  void LoginResult__ctor_m712990355 (LoginResult_t2117559510 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginResult::get_SessionTicket()
extern "C"  String_t* LoginResult_get_SessionTicket_m2264351559 (LoginResult_t2117559510 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSessionTicketU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginResult::set_SessionTicket(System.String)
extern "C"  void LoginResult_set_SessionTicket_m593625132 (LoginResult_t2117559510 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSessionTicketU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginResult::get_PlayFabId()
extern "C"  String_t* LoginResult_get_PlayFabId_m88276147 (LoginResult_t2117559510 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginResult::set_PlayFabId(System.String)
extern "C"  void LoginResult_set_PlayFabId_m1760372416 (LoginResult_t2117559510 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.LoginResult::get_NewlyCreated()
extern "C"  bool LoginResult_get_NewlyCreated_m4259775817 (LoginResult_t2117559510 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CNewlyCreatedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginResult::set_NewlyCreated(System.Boolean)
extern "C"  void LoginResult_set_NewlyCreated_m2859193280 (LoginResult_t2117559510 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CNewlyCreatedU3Ek__BackingField_4(L_0);
		return;
	}
}
// PlayFab.ClientModels.UserSettings PlayFab.ClientModels.LoginResult::get_SettingsForUser()
extern "C"  UserSettings_t8286526 * LoginResult_get_SettingsForUser_m3876956114 (LoginResult_t2117559510 * __this, const MethodInfo* method)
{
	{
		UserSettings_t8286526 * L_0 = __this->get_U3CSettingsForUserU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginResult::set_SettingsForUser(PlayFab.ClientModels.UserSettings)
extern "C"  void LoginResult_set_SettingsForUser_m3923847361 (LoginResult_t2117559510 * __this, UserSettings_t8286526 * ___value0, const MethodInfo* method)
{
	{
		UserSettings_t8286526 * L_0 = ___value0;
		__this->set_U3CSettingsForUserU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.LoginResult::get_LastLoginTime()
extern "C"  Nullable_1_t3225071844  LoginResult_get_LastLoginTime_m1397509722 (LoginResult_t2117559510 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = __this->get_U3CLastLoginTimeU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginResult::set_LastLoginTime(System.Nullable`1<System.DateTime>)
extern "C"  void LoginResult_set_LastLoginTime_m1617922437 (LoginResult_t2117559510 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = ___value0;
		__this->set_U3CLastLoginTimeU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::.ctor()
extern "C"  void LoginWithAndroidDeviceIDRequest__ctor_m3166839771 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_TitleId()
extern "C"  String_t* LoginWithAndroidDeviceIDRequest_get_TitleId_m4166179104 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_TitleId(System.String)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_TitleId_m741868339 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_AndroidDeviceId()
extern "C"  String_t* LoginWithAndroidDeviceIDRequest_get_AndroidDeviceId_m737258989 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAndroidDeviceIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_AndroidDeviceId(System.String)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_AndroidDeviceId_m2535080582 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAndroidDeviceIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_OS()
extern "C"  String_t* LoginWithAndroidDeviceIDRequest_get_OS_m1934383865 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COSU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_OS(System.String)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_OS_m2418993336 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COSU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_AndroidDevice()
extern "C"  String_t* LoginWithAndroidDeviceIDRequest_get_AndroidDevice_m2816405426 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAndroidDeviceU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_AndroidDevice(System.String)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_AndroidDevice_m3655510305 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAndroidDeviceU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithAndroidDeviceIDRequest_get_CreateAccount_m260860728 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_CreateAccount_m3489099099 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithCustomIDRequest::.ctor()
extern "C"  void LoginWithCustomIDRequest__ctor_m3572265401 (LoginWithCustomIDRequest_t3362330884 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithCustomIDRequest::get_TitleId()
extern "C"  String_t* LoginWithCustomIDRequest_get_TitleId_m2735533348 (LoginWithCustomIDRequest_t3362330884 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithCustomIDRequest::set_TitleId(System.String)
extern "C"  void LoginWithCustomIDRequest_set_TitleId_m3416292501 (LoginWithCustomIDRequest_t3362330884 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithCustomIDRequest::get_CustomId()
extern "C"  String_t* LoginWithCustomIDRequest_get_CustomId_m3409343773 (LoginWithCustomIDRequest_t3362330884 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithCustomIDRequest::set_CustomId(System.String)
extern "C"  void LoginWithCustomIDRequest_set_CustomId_m4136163246 (LoginWithCustomIDRequest_t3362330884 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithCustomIDRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithCustomIDRequest_get_CreateAccount_m650424392 (LoginWithCustomIDRequest_t3362330884 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithCustomIDRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithCustomIDRequest_set_CreateAccount_m3209891901 (LoginWithCustomIDRequest_t3362330884 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithEmailAddressRequest::.ctor()
extern "C"  void LoginWithEmailAddressRequest__ctor_m2261752229 (LoginWithEmailAddressRequest_t3586495128 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithEmailAddressRequest::get_TitleId()
extern "C"  String_t* LoginWithEmailAddressRequest_get_TitleId_m2367744656 (LoginWithEmailAddressRequest_t3586495128 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithEmailAddressRequest::set_TitleId(System.String)
extern "C"  void LoginWithEmailAddressRequest_set_TitleId_m3250033961 (LoginWithEmailAddressRequest_t3586495128 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithEmailAddressRequest::get_Email()
extern "C"  String_t* LoginWithEmailAddressRequest_get_Email_m3358843225 (LoginWithEmailAddressRequest_t3586495128 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithEmailAddressRequest::set_Email(System.String)
extern "C"  void LoginWithEmailAddressRequest_set_Email_m2923687616 (LoginWithEmailAddressRequest_t3586495128 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithEmailAddressRequest::get_Password()
extern "C"  String_t* LoginWithEmailAddressRequest_get_Password_m3472069536 (LoginWithEmailAddressRequest_t3586495128 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPasswordU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithEmailAddressRequest::set_Password(System.String)
extern "C"  void LoginWithEmailAddressRequest_set_Password_m4285566859 (LoginWithEmailAddressRequest_t3586495128 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPasswordU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithFacebookRequest::.ctor()
extern "C"  void LoginWithFacebookRequest__ctor_m660888627 (LoginWithFacebookRequest_t58367306 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithFacebookRequest::get_TitleId()
extern "C"  String_t* LoginWithFacebookRequest_get_TitleId_m2821035038 (LoginWithFacebookRequest_t58367306 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithFacebookRequest::set_TitleId(System.String)
extern "C"  void LoginWithFacebookRequest_set_TitleId_m819755995 (LoginWithFacebookRequest_t58367306 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithFacebookRequest::get_AccessToken()
extern "C"  String_t* LoginWithFacebookRequest_get_AccessToken_m387083264 (LoginWithFacebookRequest_t58367306 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAccessTokenU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithFacebookRequest::set_AccessToken(System.String)
extern "C"  void LoginWithFacebookRequest_set_AccessToken_m753930169 (LoginWithFacebookRequest_t58367306 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAccessTokenU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithFacebookRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithFacebookRequest_get_CreateAccount_m3978212290 (LoginWithFacebookRequest_t58367306 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithFacebookRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithFacebookRequest_set_CreateAccount_m749265923 (LoginWithFacebookRequest_t58367306 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGameCenterRequest::.ctor()
extern "C"  void LoginWithGameCenterRequest__ctor_m1248947220 (LoginWithGameCenterRequest_t1605888457 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithGameCenterRequest::get_TitleId()
extern "C"  String_t* LoginWithGameCenterRequest_get_TitleId_m1711872639 (LoginWithGameCenterRequest_t1605888457 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGameCenterRequest::set_TitleId(System.String)
extern "C"  void LoginWithGameCenterRequest_set_TitleId_m561804890 (LoginWithGameCenterRequest_t1605888457 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithGameCenterRequest::get_PlayerId()
extern "C"  String_t* LoginWithGameCenterRequest_get_PlayerId_m1123207826 (LoginWithGameCenterRequest_t1605888457 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayerIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGameCenterRequest::set_PlayerId(System.String)
extern "C"  void LoginWithGameCenterRequest_set_PlayerId_m3616986393 (LoginWithGameCenterRequest_t1605888457 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayerIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithGameCenterRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithGameCenterRequest_get_CreateAccount_m2982747107 (LoginWithGameCenterRequest_t1605888457 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGameCenterRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithGameCenterRequest_set_CreateAccount_m505130434 (LoginWithGameCenterRequest_t1605888457 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::.ctor()
extern "C"  void LoginWithGoogleAccountRequest__ctor_m3359589839 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::get_TitleId()
extern "C"  String_t* LoginWithGoogleAccountRequest_get_TitleId_m2464370516 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::set_TitleId(System.String)
extern "C"  void LoginWithGoogleAccountRequest_set_TitleId_m2424149183 (LoginWithGoogleAccountRequest_t692379802 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::get_AccessToken()
extern "C"  String_t* LoginWithGoogleAccountRequest_get_AccessToken_m2842926134 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAccessTokenU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::set_AccessToken(System.String)
extern "C"  void LoginWithGoogleAccountRequest_set_AccessToken_m852629149 (LoginWithGoogleAccountRequest_t692379802 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAccessTokenU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithGoogleAccountRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithGoogleAccountRequest_get_CreateAccount_m3278260460 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithGoogleAccountRequest_set_CreateAccount_m4207863783 (LoginWithGoogleAccountRequest_t692379802 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::get_PublisherId()
extern "C"  String_t* LoginWithGoogleAccountRequest_get_PublisherId_m2370936056 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPublisherIdU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::set_PublisherId(System.String)
extern "C"  void LoginWithGoogleAccountRequest_set_PublisherId_m835117339 (LoginWithGoogleAccountRequest_t692379802 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPublisherIdU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::.ctor()
extern "C"  void LoginWithIOSDeviceIDRequest__ctor_m1121702649 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_TitleId()
extern "C"  String_t* LoginWithIOSDeviceIDRequest_get_TitleId_m2455060542 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_TitleId(System.String)
extern "C"  void LoginWithIOSDeviceIDRequest_set_TitleId_m2617316181 (LoginWithIOSDeviceIDRequest_t318822832 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_DeviceId()
extern "C"  String_t* LoginWithIOSDeviceIDRequest_get_DeviceId_m1372528040 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_DeviceId(System.String)
extern "C"  void LoginWithIOSDeviceIDRequest_set_DeviceId_m2103184553 (LoginWithIOSDeviceIDRequest_t318822832 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDeviceIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_OS()
extern "C"  String_t* LoginWithIOSDeviceIDRequest_get_OS_m1339128091 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COSU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_OS(System.String)
extern "C"  void LoginWithIOSDeviceIDRequest_set_OS_m2078329686 (LoginWithIOSDeviceIDRequest_t318822832 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COSU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_DeviceModel()
extern "C"  String_t* LoginWithIOSDeviceIDRequest_get_DeviceModel_m521453598 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceModelU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_DeviceModel(System.String)
extern "C"  void LoginWithIOSDeviceIDRequest_set_DeviceModel_m372379381 (LoginWithIOSDeviceIDRequest_t318822832 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDeviceModelU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithIOSDeviceIDRequest_get_CreateAccount_m3694256342 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithIOSDeviceIDRequest_set_CreateAccount_m1414575869 (LoginWithIOSDeviceIDRequest_t318822832 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::.ctor()
extern "C"  void LoginWithKongregateRequest__ctor_m2722549512 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithKongregateRequest::get_TitleId()
extern "C"  String_t* LoginWithKongregateRequest_get_TitleId_m1480423027 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::set_TitleId(System.String)
extern "C"  void LoginWithKongregateRequest_set_TitleId_m2178426598 (LoginWithKongregateRequest_t366847061 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithKongregateRequest::get_KongregateId()
extern "C"  String_t* LoginWithKongregateRequest_get_KongregateId_m426331800 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CKongregateIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::set_KongregateId(System.String)
extern "C"  void LoginWithKongregateRequest_set_KongregateId_m548908627 (LoginWithKongregateRequest_t366847061 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CKongregateIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithKongregateRequest::get_AuthTicket()
extern "C"  String_t* LoginWithKongregateRequest_get_AuthTicket_m1759390166 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAuthTicketU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::set_AuthTicket(System.String)
extern "C"  void LoginWithKongregateRequest_set_AuthTicket_m715402773 (LoginWithKongregateRequest_t366847061 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAuthTicketU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithKongregateRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithKongregateRequest_get_CreateAccount_m1870508247 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithKongregateRequest_set_CreateAccount_m2257618766 (LoginWithKongregateRequest_t366847061 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPlayFabRequest::.ctor()
extern "C"  void LoginWithPlayFabRequest__ctor_m152984206 (LoginWithPlayFabRequest_t402940283 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithPlayFabRequest::get_TitleId()
extern "C"  String_t* LoginWithPlayFabRequest_get_TitleId_m2685094547 (LoginWithPlayFabRequest_t402940283 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPlayFabRequest::set_TitleId(System.String)
extern "C"  void LoginWithPlayFabRequest_set_TitleId_m2210197920 (LoginWithPlayFabRequest_t402940283 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithPlayFabRequest::get_Username()
extern "C"  String_t* LoginWithPlayFabRequest_get_Username_m1480220536 (LoginWithPlayFabRequest_t402940283 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPlayFabRequest::set_Username(System.String)
extern "C"  void LoginWithPlayFabRequest_set_Username_m566187609 (LoginWithPlayFabRequest_t402940283 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithPlayFabRequest::get_Password()
extern "C"  String_t* LoginWithPlayFabRequest_get_Password_m425014269 (LoginWithPlayFabRequest_t402940283 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPasswordU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPlayFabRequest::set_Password(System.String)
extern "C"  void LoginWithPlayFabRequest_set_Password_m2115420660 (LoginWithPlayFabRequest_t402940283 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPasswordU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::.ctor()
extern "C"  void LoginWithPSNRequest__ctor_m3934050726 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithPSNRequest::get_TitleId()
extern "C"  String_t* LoginWithPSNRequest_get_TitleId_m4223639083 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_TitleId(System.String)
extern "C"  void LoginWithPSNRequest_set_TitleId_m3122932616 (LoginWithPSNRequest_t4060832355 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithPSNRequest::get_AuthCode()
extern "C"  String_t* LoginWithPSNRequest_get_AuthCode_m1114678783 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAuthCodeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_AuthCode(System.String)
extern "C"  void LoginWithPSNRequest_set_AuthCode_m141687986 (LoginWithPSNRequest_t4060832355 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAuthCodeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithPSNRequest::get_RedirectUri()
extern "C"  String_t* LoginWithPSNRequest_get_RedirectUri_m3316677064 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CRedirectUriU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_RedirectUri(System.String)
extern "C"  void LoginWithPSNRequest_set_RedirectUri_m3742303883 (LoginWithPSNRequest_t4060832355 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CRedirectUriU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.LoginWithPSNRequest::get_IssuerId()
extern "C"  Nullable_1_t1438485399  LoginWithPSNRequest_get_IssuerId_m2689213194 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CIssuerIdU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_IssuerId(System.Nullable`1<System.Int32>)
extern "C"  void LoginWithPSNRequest_set_IssuerId_m2169427423 (LoginWithPSNRequest_t4060832355 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CIssuerIdU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithPSNRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithPSNRequest_get_CreateAccount_m3964506883 (LoginWithPSNRequest_t4060832355 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithPSNRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithPSNRequest_set_CreateAccount_m1097606512 (LoginWithPSNRequest_t4060832355 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithSteamRequest::.ctor()
extern "C"  void LoginWithSteamRequest__ctor_m446985323 (LoginWithSteamRequest_t700401790 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithSteamRequest::get_TitleId()
extern "C"  String_t* LoginWithSteamRequest_get_TitleId_m3578439152 (LoginWithSteamRequest_t700401790 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithSteamRequest::set_TitleId(System.String)
extern "C"  void LoginWithSteamRequest_set_TitleId_m3441182883 (LoginWithSteamRequest_t700401790 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithSteamRequest::get_SteamTicket()
extern "C"  String_t* LoginWithSteamRequest_get_SteamTicket_m4194703513 (LoginWithSteamRequest_t700401790 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSteamTicketU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithSteamRequest::set_SteamTicket(System.String)
extern "C"  void LoginWithSteamRequest_set_SteamTicket_m2268266522 (LoginWithSteamRequest_t700401790 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSteamTicketU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithSteamRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithSteamRequest_get_CreateAccount_m3919639688 (LoginWithSteamRequest_t700401790 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithSteamRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithSteamRequest_set_CreateAccount_m1222035147 (LoginWithSteamRequest_t700401790 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.LoginWithXboxRequest::.ctor()
extern "C"  void LoginWithXboxRequest__ctor_m2449094208 (LoginWithXboxRequest_t2356925853 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithXboxRequest::get_TitleId()
extern "C"  String_t* LoginWithXboxRequest_get_TitleId_m4239800811 (LoginWithXboxRequest_t2356925853 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithXboxRequest::set_TitleId(System.String)
extern "C"  void LoginWithXboxRequest_set_TitleId_m3756803246 (LoginWithXboxRequest_t2356925853 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.LoginWithXboxRequest::get_XboxToken()
extern "C"  String_t* LoginWithXboxRequest_get_XboxToken_m82160958 (LoginWithXboxRequest_t2356925853 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CXboxTokenU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithXboxRequest::set_XboxToken(System.String)
extern "C"  void LoginWithXboxRequest_set_XboxToken_m1211399803 (LoginWithXboxRequest_t2356925853 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CXboxTokenU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithXboxRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithXboxRequest_get_CreateAccount_m218779343 (LoginWithXboxRequest_t2356925853 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CCreateAccountU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.LoginWithXboxRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithXboxRequest_set_CreateAccount_m2849995542 (LoginWithXboxRequest_t2356925853 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CCreateAccountU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeRequest::.ctor()
extern "C"  void MatchmakeRequest__ctor_m2291222449 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeRequest::get_BuildVersion()
extern "C"  String_t* MatchmakeRequest_get_BuildVersion_m3851768771 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CBuildVersionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_BuildVersion(System.String)
extern "C"  void MatchmakeRequest_set_BuildVersion_m4107888456 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBuildVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.MatchmakeRequest::get_Region()
extern "C"  Nullable_1_t212373688  MatchmakeRequest_get_Region_m676310474 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t212373688  L_0 = __this->get_U3CRegionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_Region(System.Nullable`1<PlayFab.ClientModels.Region>)
extern "C"  void MatchmakeRequest_set_Region_m3181670925 (MatchmakeRequest_t4152079884 * __this, Nullable_1_t212373688  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t212373688  L_0 = ___value0;
		__this->set_U3CRegionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeRequest::get_GameMode()
extern "C"  String_t* MatchmakeRequest_get_GameMode_m1373031214 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CGameModeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_GameMode(System.String)
extern "C"  void MatchmakeRequest_set_GameMode_m1289785917 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CGameModeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeRequest::get_LobbyId()
extern "C"  String_t* MatchmakeRequest_get_LobbyId_m55543450 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CLobbyIdU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_LobbyId(System.String)
extern "C"  void MatchmakeRequest_set_LobbyId_m3127240159 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLobbyIdU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeRequest::get_StatisticName()
extern "C"  String_t* MatchmakeRequest_get_StatisticName_m3437508228 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_StatisticName(System.String)
extern "C"  void MatchmakeRequest_set_StatisticName_m2029542645 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeRequest::get_CharacterId()
extern "C"  String_t* MatchmakeRequest_get_CharacterId_m2766741613 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_CharacterId(System.String)
extern "C"  void MatchmakeRequest_set_CharacterId_m2158471020 (MatchmakeRequest_t4152079884 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.MatchmakeRequest::get_EnableQueue()
extern "C"  Nullable_1_t3097043249  MatchmakeRequest_get_EnableQueue_m3725861693 (MatchmakeRequest_t4152079884 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CEnableQueueU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeRequest::set_EnableQueue(System.Nullable`1<System.Boolean>)
extern "C"  void MatchmakeRequest_set_EnableQueue_m2386426920 (MatchmakeRequest_t4152079884 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CEnableQueueU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeResult::.ctor()
extern "C"  void MatchmakeResult__ctor_m545669257 (MatchmakeResult_t30538528 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeResult::get_LobbyID()
extern "C"  String_t* MatchmakeResult_get_LobbyID_m1869227948 (MatchmakeResult_t30538528 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CLobbyIDU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeResult::set_LobbyID(System.String)
extern "C"  void MatchmakeResult_set_LobbyID_m468652583 (MatchmakeResult_t30538528 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLobbyIDU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeResult::get_ServerHostname()
extern "C"  String_t* MatchmakeResult_get_ServerHostname_m3062055773 (MatchmakeResult_t30538528 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CServerHostnameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeResult::set_ServerHostname(System.String)
extern "C"  void MatchmakeResult_set_ServerHostname_m3355871828 (MatchmakeResult_t30538528 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CServerHostnameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.MatchmakeResult::get_ServerPort()
extern "C"  Nullable_1_t1438485399  MatchmakeResult_get_ServerPort_m676658807 (MatchmakeResult_t30538528 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CServerPortU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeResult::set_ServerPort(System.Nullable`1<System.Int32>)
extern "C"  void MatchmakeResult_set_ServerPort_m3987972498 (MatchmakeResult_t30538528 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CServerPortU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeResult::get_Ticket()
extern "C"  String_t* MatchmakeResult_get_Ticket_m2895155795 (MatchmakeResult_t30538528 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTicketU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeResult::set_Ticket(System.String)
extern "C"  void MatchmakeResult_set_Ticket_m1381743134 (MatchmakeResult_t30538528 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTicketU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.MatchmakeResult::get_Expires()
extern "C"  String_t* MatchmakeResult_get_Expires_m4171866799 (MatchmakeResult_t30538528 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CExpiresU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeResult::set_Expires(System.String)
extern "C"  void MatchmakeResult_set_Expires_m109630468 (MatchmakeResult_t30538528 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CExpiresU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.MatchmakeResult::get_PollWaitTimeMS()
extern "C"  Nullable_1_t1438485399  MatchmakeResult_get_PollWaitTimeMS_m2588024442 (MatchmakeResult_t30538528 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CPollWaitTimeMSU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeResult::set_PollWaitTimeMS(System.Nullable`1<System.Int32>)
extern "C"  void MatchmakeResult_set_PollWaitTimeMS_m3188423599 (MatchmakeResult_t30538528 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CPollWaitTimeMSU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus> PlayFab.ClientModels.MatchmakeResult::get_Status()
extern "C"  Nullable_1_t2958521481  MatchmakeResult_get_Status_m2210551709 (MatchmakeResult_t30538528 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t2958521481  L_0 = __this->get_U3CStatusU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.MatchmakeResult::set_Status(System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>)
extern "C"  void MatchmakeResult_set_Status_m1088729692 (MatchmakeResult_t30538528 * __this, Nullable_1_t2958521481  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t2958521481  L_0 = ___value0;
		__this->set_U3CStatusU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::.ctor()
extern "C"  void ModifyUserVirtualCurrencyResult__ctor_m1492780901 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::get_PlayFabId()
extern "C"  String_t* ModifyUserVirtualCurrencyResult_get_PlayFabId_m3476370181 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::set_PlayFabId(System.String)
extern "C"  void ModifyUserVirtualCurrencyResult_set_PlayFabId_m115250478 (ModifyUserVirtualCurrencyResult_t448487620 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::get_VirtualCurrency()
extern "C"  String_t* ModifyUserVirtualCurrencyResult_get_VirtualCurrency_m275313555 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::set_VirtualCurrency(System.String)
extern "C"  void ModifyUserVirtualCurrencyResult_set_VirtualCurrency_m690739872 (ModifyUserVirtualCurrencyResult_t448487620 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::get_BalanceChange()
extern "C"  int32_t ModifyUserVirtualCurrencyResult_get_BalanceChange_m2905505528 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CBalanceChangeU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::set_BalanceChange(System.Int32)
extern "C"  void ModifyUserVirtualCurrencyResult_set_BalanceChange_m1413423267 (ModifyUserVirtualCurrencyResult_t448487620 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CBalanceChangeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::get_Balance()
extern "C"  int32_t ModifyUserVirtualCurrencyResult_get_Balance_m3705576552 (ModifyUserVirtualCurrencyResult_t448487620 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CBalanceU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ModifyUserVirtualCurrencyResult::set_Balance(System.Int32)
extern "C"  void ModifyUserVirtualCurrencyResult_set_Balance_m1030513043 (ModifyUserVirtualCurrencyResult_t448487620 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CBalanceU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.OpenTradeRequest::.ctor()
extern "C"  void OpenTradeRequest__ctor_m3994623416 (OpenTradeRequest_t1121233957 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::get_OfferedInventoryInstanceIds()
extern "C"  List_1_t1765447871 * OpenTradeRequest_get_OfferedInventoryInstanceIds_m4102934368 (OpenTradeRequest_t1121233957 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3COfferedInventoryInstanceIdsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.OpenTradeRequest::set_OfferedInventoryInstanceIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void OpenTradeRequest_set_OfferedInventoryInstanceIds_m188381359 (OpenTradeRequest_t1121233957 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3COfferedInventoryInstanceIdsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::get_RequestedCatalogItemIds()
extern "C"  List_1_t1765447871 * OpenTradeRequest_get_RequestedCatalogItemIds_m2931863480 (OpenTradeRequest_t1121233957 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CRequestedCatalogItemIdsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.OpenTradeRequest::set_RequestedCatalogItemIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void OpenTradeRequest_set_RequestedCatalogItemIds_m1638993543 (OpenTradeRequest_t1121233957 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CRequestedCatalogItemIdsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.OpenTradeRequest::get_AllowedPlayerIds()
extern "C"  List_1_t1765447871 * OpenTradeRequest_get_AllowedPlayerIds_m3849606483 (OpenTradeRequest_t1121233957 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CAllowedPlayerIdsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.OpenTradeRequest::set_AllowedPlayerIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void OpenTradeRequest_set_AllowedPlayerIds_m2852396364 (OpenTradeRequest_t1121233957 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CAllowedPlayerIdsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.OpenTradeResponse::.ctor()
extern "C"  void OpenTradeResponse__ctor_m2343269118 (OpenTradeResponse_t3658687371 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.OpenTradeResponse::get_Trade()
extern "C"  TradeInfo_t1933812770 * OpenTradeResponse_get_Trade_m2344114126 (OpenTradeResponse_t3658687371 * __this, const MethodInfo* method)
{
	{
		TradeInfo_t1933812770 * L_0 = __this->get_U3CTradeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.OpenTradeResponse::set_Trade(PlayFab.ClientModels.TradeInfo)
extern "C"  void OpenTradeResponse_set_Trade_m1175227471 (OpenTradeResponse_t3658687371 * __this, TradeInfo_t1933812770 * ___value0, const MethodInfo* method)
{
	{
		TradeInfo_t1933812770 * L_0 = ___value0;
		__this->set_U3CTradeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::.ctor()
extern "C"  void PayForPurchaseRequest__ctor_m3658349260 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.PayForPurchaseRequest::get_OrderId()
extern "C"  String_t* PayForPurchaseRequest_get_OrderId_m2552895879 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COrderIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::set_OrderId(System.String)
extern "C"  void PayForPurchaseRequest_set_OrderId_m3355550316 (PayForPurchaseRequest_t2884624637 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrderIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PayForPurchaseRequest::get_ProviderName()
extern "C"  String_t* PayForPurchaseRequest_get_ProviderName_m2668221056 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CProviderNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::set_ProviderName(System.String)
extern "C"  void PayForPurchaseRequest_set_ProviderName_m3086878097 (PayForPurchaseRequest_t2884624637 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CProviderNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PayForPurchaseRequest::get_Currency()
extern "C"  String_t* PayForPurchaseRequest_get_Currency_m5353141 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCurrencyU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::set_Currency(System.String)
extern "C"  void PayForPurchaseRequest_set_Currency_m2116765436 (PayForPurchaseRequest_t2884624637 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCurrencyU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PayForPurchaseRequest::get_ProviderTransactionId()
extern "C"  String_t* PayForPurchaseRequest_get_ProviderTransactionId_m3190669350 (PayForPurchaseRequest_t2884624637 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CProviderTransactionIdU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseRequest::set_ProviderTransactionId(System.String)
extern "C"  void PayForPurchaseRequest_set_ProviderTransactionId_m2192008301 (PayForPurchaseRequest_t2884624637 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CProviderTransactionIdU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::.ctor()
extern "C"  void PayForPurchaseResult__ctor_m728317454 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.PayForPurchaseResult::get_OrderId()
extern "C"  String_t* PayForPurchaseResult_get_OrderId_m3418204463 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COrderIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_OrderId(System.String)
extern "C"  void PayForPurchaseResult_set_OrderId_m496681962 (PayForPurchaseResult_t128200207 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrderIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.TransactionStatus> PlayFab.ClientModels.PayForPurchaseResult::get_Status()
extern "C"  Nullable_1_t4258606580  PayForPurchaseResult_get_Status_m4081449471 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t4258606580  L_0 = __this->get_U3CStatusU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_Status(System.Nullable`1<PlayFab.ClientModels.TransactionStatus>)
extern "C"  void PayForPurchaseResult_set_Status_m3509427404 (PayForPurchaseResult_t128200207 * __this, Nullable_1_t4258606580  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t4258606580  L_0 = ___value0;
		__this->set_U3CStatusU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.PayForPurchaseResult::get_VCAmount()
extern "C"  Dictionary_2_t190145395 * PayForPurchaseResult_get_VCAmount_m477096607 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CVCAmountU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_VCAmount(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void PayForPurchaseResult_set_VCAmount_m2755823852 (PayForPurchaseResult_t128200207 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CVCAmountU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PayForPurchaseResult::get_PurchaseCurrency()
extern "C"  String_t* PayForPurchaseResult_get_PurchaseCurrency_m1417936110 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPurchaseCurrencyU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_PurchaseCurrency(System.String)
extern "C"  void PayForPurchaseResult_set_PurchaseCurrency_m4018301181 (PayForPurchaseResult_t128200207 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPurchaseCurrencyU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.PayForPurchaseResult::get_PurchasePrice()
extern "C"  uint32_t PayForPurchaseResult_get_PurchasePrice_m4274210150 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CPurchasePriceU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_PurchasePrice(System.UInt32)
extern "C"  void PayForPurchaseResult_set_PurchasePrice_m328917699 (PayForPurchaseResult_t128200207 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CPurchasePriceU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.PayForPurchaseResult::get_CreditApplied()
extern "C"  uint32_t PayForPurchaseResult_get_CreditApplied_m2118280418 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CCreditAppliedU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_CreditApplied(System.UInt32)
extern "C"  void PayForPurchaseResult_set_CreditApplied_m1028902087 (PayForPurchaseResult_t128200207 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CCreditAppliedU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PayForPurchaseResult::get_ProviderData()
extern "C"  String_t* PayForPurchaseResult_get_ProviderData_m3254285623 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CProviderDataU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_ProviderData(System.String)
extern "C"  void PayForPurchaseResult_set_ProviderData_m3333024724 (PayForPurchaseResult_t128200207 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CProviderDataU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PayForPurchaseResult::get_PurchaseConfirmationPageURL()
extern "C"  String_t* PayForPurchaseResult_get_PurchaseConfirmationPageURL_m42263312 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPurchaseConfirmationPageURLU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_PurchaseConfirmationPageURL(System.String)
extern "C"  void PayForPurchaseResult_set_PurchaseConfirmationPageURL_m2662293033 (PayForPurchaseResult_t128200207 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPurchaseConfirmationPageURLU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.PayForPurchaseResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t190145395 * PayForPurchaseResult_get_VirtualCurrency_m2525469316 (PayForPurchaseResult_t128200207 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PayForPurchaseResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void PayForPurchaseResult_set_VirtualCurrency_m3225110009 (PayForPurchaseResult_t128200207 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.PaymentOption::.ctor()
extern "C"  void PaymentOption__ctor_m3714308990 (PaymentOption_t1289274891 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.PaymentOption::get_Currency()
extern "C"  String_t* PaymentOption_get_Currency_m439686339 (PaymentOption_t1289274891 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCurrencyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PaymentOption::set_Currency(System.String)
extern "C"  void PaymentOption_set_Currency_m506190510 (PaymentOption_t1289274891 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCurrencyU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PaymentOption::get_ProviderName()
extern "C"  String_t* PaymentOption_get_ProviderName_m2911863182 (PaymentOption_t1289274891 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CProviderNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PaymentOption::set_ProviderName(System.String)
extern "C"  void PaymentOption_set_ProviderName_m2034856003 (PaymentOption_t1289274891 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CProviderNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.PaymentOption::get_Price()
extern "C"  uint32_t PaymentOption_get_Price_m620295361 (PaymentOption_t1289274891 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CPriceU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PaymentOption::set_Price(System.UInt32)
extern "C"  void PaymentOption_set_Price_m2857973426 (PaymentOption_t1289274891 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CPriceU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.PaymentOption::get_StoreCredit()
extern "C"  uint32_t PaymentOption_get_StoreCredit_m1108183346 (PaymentOption_t1289274891 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CStoreCreditU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PaymentOption::set_StoreCredit(System.UInt32)
extern "C"  void PaymentOption_set_StoreCredit_m4028193569 (PaymentOption_t1289274891 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CStoreCreditU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::.ctor()
extern "C"  void PlayerLeaderboardEntry__ctor_m3341829975 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.PlayerLeaderboardEntry::get_PlayFabId()
extern "C"  String_t* PlayerLeaderboardEntry_get_PlayFabId_m944328925 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::set_PlayFabId(System.String)
extern "C"  void PlayerLeaderboardEntry_set_PlayFabId_m3384811644 (PlayerLeaderboardEntry_t2241891366 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PlayerLeaderboardEntry::get_DisplayName()
extern "C"  String_t* PlayerLeaderboardEntry_get_DisplayName_m3000298588 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::set_DisplayName(System.String)
extern "C"  void PlayerLeaderboardEntry_set_DisplayName_m915110301 (PlayerLeaderboardEntry_t2241891366 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.PlayerLeaderboardEntry::get_StatValue()
extern "C"  int32_t PlayerLeaderboardEntry_get_StatValue_m3273454167 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStatValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::set_StatValue(System.Int32)
extern "C"  void PlayerLeaderboardEntry_set_StatValue_m499900966 (PlayerLeaderboardEntry_t2241891366 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStatValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.PlayerLeaderboardEntry::get_Position()
extern "C"  int32_t PlayerLeaderboardEntry_get_Position_m1726384049 (PlayerLeaderboardEntry_t2241891366 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CPositionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PlayerLeaderboardEntry::set_Position(System.Int32)
extern "C"  void PlayerLeaderboardEntry_set_Position_m2700123844 (PlayerLeaderboardEntry_t2241891366 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPositionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.PSNAccountPlayFabIdPair::.ctor()
extern "C"  void PSNAccountPlayFabIdPair__ctor_m494601363 (PSNAccountPlayFabIdPair_t680553942 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.PSNAccountPlayFabIdPair::get_PSNAccountId()
extern "C"  String_t* PSNAccountPlayFabIdPair_get_PSNAccountId_m3183950554 (PSNAccountPlayFabIdPair_t680553942 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPSNAccountIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PSNAccountPlayFabIdPair::set_PSNAccountId(System.String)
extern "C"  void PSNAccountPlayFabIdPair_set_PSNAccountId_m3433630391 (PSNAccountPlayFabIdPair_t680553942 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPSNAccountIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PSNAccountPlayFabIdPair::get_PlayFabId()
extern "C"  String_t* PSNAccountPlayFabIdPair_get_PlayFabId_m2843160371 (PSNAccountPlayFabIdPair_t680553942 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PSNAccountPlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void PSNAccountPlayFabIdPair_set_PlayFabId_m1742028992 (PSNAccountPlayFabIdPair_t680553942 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemRequest::.ctor()
extern "C"  void PurchaseItemRequest__ctor_m4181081310 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_ItemId()
extern "C"  String_t* PurchaseItemRequest_get_ItemId_m2626691488 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_ItemId(System.String)
extern "C"  void PurchaseItemRequest_set_ItemId_m2705274481 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_VirtualCurrency()
extern "C"  String_t* PurchaseItemRequest_get_VirtualCurrency_m1506473164 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_VirtualCurrency(System.String)
extern "C"  void PurchaseItemRequest_set_VirtualCurrency_m1556283783 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.PurchaseItemRequest::get_Price()
extern "C"  int32_t PurchaseItemRequest_get_Price_m83012174 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CPriceU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_Price(System.Int32)
extern "C"  void PurchaseItemRequest_set_Price_m302381561 (PurchaseItemRequest_t3673358379 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPriceU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_CatalogVersion()
extern "C"  String_t* PurchaseItemRequest_get_CatalogVersion_m2560144209 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_CatalogVersion(System.String)
extern "C"  void PurchaseItemRequest_set_CatalogVersion_m2531274080 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_StoreId()
extern "C"  String_t* PurchaseItemRequest_get_StoreId_m3676709228 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStoreIdU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_StoreId(System.String)
extern "C"  void PurchaseItemRequest_set_StoreId_m1744484839 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStoreIdU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.PurchaseItemRequest::get_CharacterId()
extern "C"  String_t* PurchaseItemRequest_get_CharacterId_m1015619636 (PurchaseItemRequest_t3673358379 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemRequest::set_CharacterId(System.String)
extern "C"  void PurchaseItemRequest_set_CharacterId_m1636118431 (PurchaseItemRequest_t3673358379 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemResult::.ctor()
extern "C"  void PurchaseItemResult__ctor_m2684842428 (PurchaseItemResult_t2370400545 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.PurchaseItemResult::get_Items()
extern "C"  List_1_t1322103281 * PurchaseItemResult_get_Items_m3967626200 (PurchaseItemResult_t2370400545 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CItemsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.PurchaseItemResult::set_Items(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void PurchaseItemResult_set_Items_m2450336039 (PurchaseItemResult_t2370400545 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CItemsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RedeemCouponRequest::.ctor()
extern "C"  void RedeemCouponRequest__ctor_m2533256716 (RedeemCouponRequest_t2632721213 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RedeemCouponRequest::get_CouponCode()
extern "C"  String_t* RedeemCouponRequest_get_CouponCode_m2663500311 (RedeemCouponRequest_t2632721213 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCouponCodeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RedeemCouponRequest::set_CouponCode(System.String)
extern "C"  void RedeemCouponRequest_set_CouponCode_m4040697818 (RedeemCouponRequest_t2632721213 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCouponCodeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RedeemCouponRequest::get_CatalogVersion()
extern "C"  String_t* RedeemCouponRequest_get_CatalogVersion_m2839698659 (RedeemCouponRequest_t2632721213 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RedeemCouponRequest::set_CatalogVersion(System.String)
extern "C"  void RedeemCouponRequest_set_CatalogVersion_m3681427726 (RedeemCouponRequest_t2632721213 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RedeemCouponResult::.ctor()
extern "C"  void RedeemCouponResult__ctor_m553476814 (RedeemCouponResult_t1089905615 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.RedeemCouponResult::get_GrantedItems()
extern "C"  List_1_t1322103281 * RedeemCouponResult_get_GrantedItems_m1891020349 (RedeemCouponResult_t1089905615 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CGrantedItemsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RedeemCouponResult::set_GrantedItems(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void RedeemCouponResult_set_GrantedItems_m1860105122 (RedeemCouponResult_t1089905615 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CGrantedItemsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RefreshPSNAuthTokenRequest::.ctor()
extern "C"  void RefreshPSNAuthTokenRequest__ctor_m4253016863 (RefreshPSNAuthTokenRequest_t4115941982 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RefreshPSNAuthTokenRequest::get_AuthCode()
extern "C"  String_t* RefreshPSNAuthTokenRequest_get_AuthCode_m2769373888 (RefreshPSNAuthTokenRequest_t4115941982 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAuthCodeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RefreshPSNAuthTokenRequest::set_AuthCode(System.String)
extern "C"  void RefreshPSNAuthTokenRequest_set_AuthCode_m3504508843 (RefreshPSNAuthTokenRequest_t4115941982 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAuthCodeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RefreshPSNAuthTokenRequest::get_RedirectUri()
extern "C"  String_t* RefreshPSNAuthTokenRequest_get_RedirectUri_m703926631 (RefreshPSNAuthTokenRequest_t4115941982 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CRedirectUriU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RefreshPSNAuthTokenRequest::set_RedirectUri(System.String)
extern "C"  void RefreshPSNAuthTokenRequest_set_RedirectUri_m1131308274 (RefreshPSNAuthTokenRequest_t4115941982 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CRedirectUriU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.RefreshPSNAuthTokenRequest::get_IssuerId()
extern "C"  Nullable_1_t1438485399  RefreshPSNAuthTokenRequest_get_IssuerId_m431805619 (RefreshPSNAuthTokenRequest_t4115941982 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CIssuerIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RefreshPSNAuthTokenRequest::set_IssuerId(System.Nullable`1<System.Int32>)
extern "C"  void RefreshPSNAuthTokenRequest_set_IssuerId_m1144519832 (RefreshPSNAuthTokenRequest_t4115941982 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CIssuerIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RegionInfo::.ctor()
extern "C"  void RegionInfo__ctor_m6961675 (RegionInfo_t3474869746 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.RegionInfo::get_Region()
extern "C"  Nullable_1_t212373688  RegionInfo_get_Region_m3381460784 (RegionInfo_t3474869746 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t212373688  L_0 = __this->get_U3CRegionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegionInfo::set_Region(System.Nullable`1<PlayFab.ClientModels.Region>)
extern "C"  void RegionInfo_set_Region_m115377523 (RegionInfo_t3474869746 * __this, Nullable_1_t212373688  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t212373688  L_0 = ___value0;
		__this->set_U3CRegionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegionInfo::get_Name()
extern "C"  String_t* RegionInfo_get_Name_m2482601130 (RegionInfo_t3474869746 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegionInfo::set_Name(System.String)
extern "C"  void RegionInfo_set_Name_m3283075201 (RegionInfo_t3474869746 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.RegionInfo::get_Available()
extern "C"  bool RegionInfo_get_Available_m3049764605 (RegionInfo_t3474869746 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CAvailableU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegionInfo::set_Available(System.Boolean)
extern "C"  void RegionInfo_set_Available_m3779082252 (RegionInfo_t3474869746 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CAvailableU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegionInfo::get_PingUrl()
extern "C"  String_t* RegionInfo_get_PingUrl_m2559666112 (RegionInfo_t3474869746 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPingUrlU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegionInfo::set_PingUrl(System.String)
extern "C"  void RegionInfo_set_PingUrl_m818143417 (RegionInfo_t3474869746 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPingUrlU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::.ctor()
extern "C"  void RegisterForIOSPushNotificationRequest__ctor_m3037949430 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::get_DeviceToken()
extern "C"  String_t* RegisterForIOSPushNotificationRequest_get_DeviceToken_m3877369195 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceTokenU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::set_DeviceToken(System.String)
extern "C"  void RegisterForIOSPushNotificationRequest_set_DeviceToken_m2351246984 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDeviceTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::get_SendPushNotificationConfirmation()
extern "C"  Nullable_1_t3097043249  RegisterForIOSPushNotificationRequest_get_SendPushNotificationConfirmation_m701229890 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CSendPushNotificationConfirmationU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::set_SendPushNotificationConfirmation(System.Nullable`1<System.Boolean>)
extern "C"  void RegisterForIOSPushNotificationRequest_set_SendPushNotificationConfirmation_m4029145883 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CSendPushNotificationConfirmationU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::get_ConfirmationMessage()
extern "C"  String_t* RegisterForIOSPushNotificationRequest_get_ConfirmationMessage_m1632947066 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CConfirmationMessageU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::set_ConfirmationMessage(System.String)
extern "C"  void RegisterForIOSPushNotificationRequest_set_ConfirmationMessage_m2071152601 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CConfirmationMessageU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationResult::.ctor()
extern "C"  void RegisterForIOSPushNotificationResult__ctor_m431209892 (RegisterForIOSPushNotificationResult_t2485426617 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::.ctor()
extern "C"  void RegisterPlayFabUserRequest__ctor_m801848569 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_TitleId()
extern "C"  String_t* RegisterPlayFabUserRequest_get_TitleId_m2260331428 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_TitleId(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_TitleId_m1433758037 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_Username()
extern "C"  String_t* RegisterPlayFabUserRequest_get_Username_m1197465735 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_Username(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_Username_m2266355012 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_Email()
extern "C"  String_t* RegisterPlayFabUserRequest_get_Email_m2625771373 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_Email(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_Email_m494984684 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_Password()
extern "C"  String_t* RegisterPlayFabUserRequest_get_Password_m142259468 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPasswordU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_Password(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_Password_m3815588063 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPasswordU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.RegisterPlayFabUserRequest::get_RequireBothUsernameAndEmail()
extern "C"  Nullable_1_t3097043249  RegisterPlayFabUserRequest_get_RequireBothUsernameAndEmail_m1377696472 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = __this->get_U3CRequireBothUsernameAndEmailU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_RequireBothUsernameAndEmail(System.Nullable`1<System.Boolean>)
extern "C"  void RegisterPlayFabUserRequest_set_RequireBothUsernameAndEmail_m2608564909 (RegisterPlayFabUserRequest_t2314572612 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3097043249  L_0 = ___value0;
		__this->set_U3CRequireBothUsernameAndEmailU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_DisplayName()
extern "C"  String_t* RegisterPlayFabUserRequest_get_DisplayName_m887264766 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_DisplayName(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_DisplayName_m3077131707 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserRequest::get_Origination()
extern "C"  String_t* RegisterPlayFabUserRequest_get_Origination_m664081056 (RegisterPlayFabUserRequest_t2314572612 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COriginationU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserRequest::set_Origination(System.String)
extern "C"  void RegisterPlayFabUserRequest_set_Origination_m2449074265 (RegisterPlayFabUserRequest_t2314572612 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COriginationU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::.ctor()
extern "C"  void RegisterPlayFabUserResult__ctor_m3407118913 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::get_PlayFabId()
extern "C"  String_t* RegisterPlayFabUserResult_get_PlayFabId_m2591548321 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::set_PlayFabId(System.String)
extern "C"  void RegisterPlayFabUserResult_set_PlayFabId_m518044626 (RegisterPlayFabUserResult_t109811432 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::get_SessionTicket()
extern "C"  String_t* RegisterPlayFabUserResult_get_SessionTicket_m409142069 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSessionTicketU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::set_SessionTicket(System.String)
extern "C"  void RegisterPlayFabUserResult_set_SessionTicket_m3699424318 (RegisterPlayFabUserResult_t109811432 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSessionTicketU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RegisterPlayFabUserResult::get_Username()
extern "C"  String_t* RegisterPlayFabUserResult_get_Username_m1653627813 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUsernameU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::set_Username(System.String)
extern "C"  void RegisterPlayFabUserResult_set_Username_m1762633356 (RegisterPlayFabUserResult_t109811432 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUsernameU3Ek__BackingField_4(L_0);
		return;
	}
}
// PlayFab.ClientModels.UserSettings PlayFab.ClientModels.RegisterPlayFabUserResult::get_SettingsForUser()
extern "C"  UserSettings_t8286526 * RegisterPlayFabUserResult_get_SettingsForUser_m685996864 (RegisterPlayFabUserResult_t109811432 * __this, const MethodInfo* method)
{
	{
		UserSettings_t8286526 * L_0 = __this->get_U3CSettingsForUserU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RegisterPlayFabUserResult::set_SettingsForUser(PlayFab.ClientModels.UserSettings)
extern "C"  void RegisterPlayFabUserResult_set_SettingsForUser_m2089266003 (RegisterPlayFabUserResult_t109811432 * __this, UserSettings_t8286526 * ___value0, const MethodInfo* method)
{
	{
		UserSettings_t8286526 * L_0 = ___value0;
		__this->set_U3CSettingsForUserU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RemoveFriendRequest::.ctor()
extern "C"  void RemoveFriendRequest__ctor_m1481789804 (RemoveFriendRequest_t4041798621 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RemoveFriendRequest::get_FriendPlayFabId()
extern "C"  String_t* RemoveFriendRequest_get_FriendPlayFabId_m497708622 (RemoveFriendRequest_t4041798621 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFriendPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RemoveFriendRequest::set_FriendPlayFabId(System.String)
extern "C"  void RemoveFriendRequest_set_FriendPlayFabId_m2721090373 (RemoveFriendRequest_t4041798621 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFriendPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RemoveFriendResult::.ctor()
extern "C"  void RemoveFriendResult__ctor_m4260336494 (RemoveFriendResult_t3352117039 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.RemoveSharedGroupMembersRequest::.ctor()
extern "C"  void RemoveSharedGroupMembersRequest__ctor_m346139757 (RemoveSharedGroupMembersRequest_t2614915772 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RemoveSharedGroupMembersRequest::get_SharedGroupId()
extern "C"  String_t* RemoveSharedGroupMembersRequest_get_SharedGroupId_m1578462068 (RemoveSharedGroupMembersRequest_t2614915772 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSharedGroupIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RemoveSharedGroupMembersRequest::set_SharedGroupId(System.String)
extern "C"  void RemoveSharedGroupMembersRequest_set_SharedGroupId_m626972831 (RemoveSharedGroupMembersRequest_t2614915772 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSharedGroupIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.RemoveSharedGroupMembersRequest::get_PlayFabIds()
extern "C"  List_1_t1765447871 * RemoveSharedGroupMembersRequest_get_PlayFabIds_m356535414 (RemoveSharedGroupMembersRequest_t2614915772 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CPlayFabIdsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RemoveSharedGroupMembersRequest::set_PlayFabIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void RemoveSharedGroupMembersRequest_set_PlayFabIds_m847073261 (RemoveSharedGroupMembersRequest_t2614915772 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CPlayFabIdsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RemoveSharedGroupMembersResult::.ctor()
extern "C"  void RemoveSharedGroupMembersResult__ctor_m3669513293 (RemoveSharedGroupMembersResult_t3306088560 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.ReportPlayerClientRequest::.ctor()
extern "C"  void ReportPlayerClientRequest__ctor_m1155424778 (ReportPlayerClientRequest_t3784712447 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.ReportPlayerClientRequest::get_ReporteeId()
extern "C"  String_t* ReportPlayerClientRequest_get_ReporteeId_m1161488501 (ReportPlayerClientRequest_t3784712447 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CReporteeIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ReportPlayerClientRequest::set_ReporteeId(System.String)
extern "C"  void ReportPlayerClientRequest_set_ReporteeId_m3480544252 (ReportPlayerClientRequest_t3784712447 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CReporteeIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.ReportPlayerClientRequest::get_Comment()
extern "C"  String_t* ReportPlayerClientRequest_get_Comment_m331329435 (ReportPlayerClientRequest_t3784712447 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCommentU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ReportPlayerClientRequest::set_Comment(System.String)
extern "C"  void ReportPlayerClientRequest_set_Comment_m1708799576 (ReportPlayerClientRequest_t3784712447 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCommentU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.ReportPlayerClientResult::.ctor()
extern "C"  void ReportPlayerClientResult__ctor_m2171598608 (ReportPlayerClientResult_t3759465933 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayFab.ClientModels.ReportPlayerClientResult::get_Updated()
extern "C"  bool ReportPlayerClientResult_get_Updated_m2358293364 (ReportPlayerClientResult_t3759465933 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CUpdatedU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ReportPlayerClientResult::set_Updated(System.Boolean)
extern "C"  void ReportPlayerClientResult_set_Updated_m2295634243 (ReportPlayerClientResult_t3759465933 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CUpdatedU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.ReportPlayerClientResult::get_SubmissionsRemaining()
extern "C"  int32_t ReportPlayerClientResult_get_SubmissionsRemaining_m3549486270 (ReportPlayerClientResult_t3759465933 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CSubmissionsRemainingU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.ReportPlayerClientResult::set_SubmissionsRemaining(System.Int32)
extern "C"  void ReportPlayerClientResult_set_SubmissionsRemaining_m2672490961 (ReportPlayerClientResult_t3759465933 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CSubmissionsRemainingU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RestoreIOSPurchasesRequest::.ctor()
extern "C"  void RestoreIOSPurchasesRequest__ctor_m807802289 (RestoreIOSPurchasesRequest_t2671348620 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RestoreIOSPurchasesRequest::get_ReceiptData()
extern "C"  String_t* RestoreIOSPurchasesRequest_get_ReceiptData_m1723366155 (RestoreIOSPurchasesRequest_t2671348620 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CReceiptDataU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RestoreIOSPurchasesRequest::set_ReceiptData(System.String)
extern "C"  void RestoreIOSPurchasesRequest_set_ReceiptData_m3310536142 (RestoreIOSPurchasesRequest_t2671348620 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CReceiptDataU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RestoreIOSPurchasesResult::.ctor()
extern "C"  void RestoreIOSPurchasesResult__ctor_m3961500297 (RestoreIOSPurchasesResult_t675509664 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptRequest::.ctor()
extern "C"  void RunCloudScriptRequest__ctor_m2570744191 (RunCloudScriptRequest_t180913386 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RunCloudScriptRequest::get_ActionId()
extern "C"  String_t* RunCloudScriptRequest_get_ActionId_m873644674 (RunCloudScriptRequest_t180913386 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CActionIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptRequest::set_ActionId(System.String)
extern "C"  void RunCloudScriptRequest_set_ActionId_m2157676623 (RunCloudScriptRequest_t180913386 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CActionIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Object PlayFab.ClientModels.RunCloudScriptRequest::get_Params()
extern "C"  Il2CppObject * RunCloudScriptRequest_get_Params_m2338083177 (RunCloudScriptRequest_t180913386 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CParamsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptRequest::set_Params(System.Object)
extern "C"  void RunCloudScriptRequest_set_Params_m3136186988 (RunCloudScriptRequest_t180913386 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CParamsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RunCloudScriptRequest::get_ParamsEncoded()
extern "C"  String_t* RunCloudScriptRequest_get_ParamsEncoded_m3729537497 (RunCloudScriptRequest_t180913386 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CParamsEncodedU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptRequest::set_ParamsEncoded(System.String)
extern "C"  void RunCloudScriptRequest_set_ParamsEncoded_m3057837722 (RunCloudScriptRequest_t180913386 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CParamsEncodedU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptResult::.ctor()
extern "C"  void RunCloudScriptResult__ctor_m139044091 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.RunCloudScriptResult::get_ActionId()
extern "C"  String_t* RunCloudScriptResult_get_ActionId_m3102958176 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CActionIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_ActionId(System.String)
extern "C"  void RunCloudScriptResult_set_ActionId_m1697261003 (RunCloudScriptResult_t3227572354 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CActionIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.RunCloudScriptResult::get_Version()
extern "C"  int32_t RunCloudScriptResult_get_Version_m3398206806 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CVersionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_Version(System.Int32)
extern "C"  void RunCloudScriptResult_set_Version_m3183119013 (RunCloudScriptResult_t3227572354 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CVersionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.RunCloudScriptResult::get_Revision()
extern "C"  int32_t RunCloudScriptResult_get_Revision_m1428301887 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CRevisionU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_Revision(System.Int32)
extern "C"  void RunCloudScriptResult_set_Revision_m1596650962 (RunCloudScriptResult_t3227572354 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CRevisionU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Object PlayFab.ClientModels.RunCloudScriptResult::get_Results()
extern "C"  Il2CppObject * RunCloudScriptResult_get_Results_m706932379 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CResultsU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_Results(System.Object)
extern "C"  void RunCloudScriptResult_set_Results_m3553400802 (RunCloudScriptResult_t3227572354 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CResultsU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RunCloudScriptResult::get_ResultsEncoded()
extern "C"  String_t* RunCloudScriptResult_get_ResultsEncoded_m2537930855 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CResultsEncodedU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_ResultsEncoded(System.String)
extern "C"  void RunCloudScriptResult_set_ResultsEncoded_m4266838116 (RunCloudScriptResult_t3227572354 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CResultsEncodedU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.RunCloudScriptResult::get_ActionLog()
extern "C"  String_t* RunCloudScriptResult_get_ActionLog_m1705581761 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CActionLogU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_ActionLog(System.String)
extern "C"  void RunCloudScriptResult_set_ActionLog_m993762264 (RunCloudScriptResult_t3227572354 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CActionLogU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Double PlayFab.ClientModels.RunCloudScriptResult::get_ExecutionTime()
extern "C"  double RunCloudScriptResult_get_ExecutionTime_m1792762648 (RunCloudScriptResult_t3227572354 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_U3CExecutionTimeU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.RunCloudScriptResult::set_ExecutionTime(System.Double)
extern "C"  void RunCloudScriptResult_set_ExecutionTime_m1963501537 (RunCloudScriptResult_t3227572354 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CExecutionTimeU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailRequest::.ctor()
extern "C"  void SendAccountRecoveryEmailRequest__ctor_m1001540524 (SendAccountRecoveryEmailRequest_t558031005 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.SendAccountRecoveryEmailRequest::get_Email()
extern "C"  String_t* SendAccountRecoveryEmailRequest_get_Email_m959843130 (SendAccountRecoveryEmailRequest_t558031005 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailRequest::set_Email(System.String)
extern "C"  void SendAccountRecoveryEmailRequest_set_Email_m3906513817 (SendAccountRecoveryEmailRequest_t558031005 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.SendAccountRecoveryEmailRequest::get_TitleId()
extern "C"  String_t* SendAccountRecoveryEmailRequest_get_TitleId_m3326091313 (SendAccountRecoveryEmailRequest_t558031005 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailRequest::set_TitleId(System.String)
extern "C"  void SendAccountRecoveryEmailRequest_set_TitleId_m2853208002 (SendAccountRecoveryEmailRequest_t558031005 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.SendAccountRecoveryEmailRequest::get_PublisherId()
extern "C"  String_t* SendAccountRecoveryEmailRequest_get_PublisherId_m837859157 (SendAccountRecoveryEmailRequest_t558031005 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPublisherIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailRequest::set_PublisherId(System.String)
extern "C"  void SendAccountRecoveryEmailRequest_set_PublisherId_m571904670 (SendAccountRecoveryEmailRequest_t558031005 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPublisherIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailResult::.ctor()
extern "C"  void SendAccountRecoveryEmailResult__ctor_m226971950 (SendAccountRecoveryEmailResult_t3793926767 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.SetFriendTagsRequest::.ctor()
extern "C"  void SetFriendTagsRequest__ctor_m367340375 (SetFriendTagsRequest_t1987017382 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.SetFriendTagsRequest::get_FriendPlayFabId()
extern "C"  String_t* SetFriendTagsRequest_get_FriendPlayFabId_m1238148511 (SetFriendTagsRequest_t1987017382 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CFriendPlayFabIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SetFriendTagsRequest::set_FriendPlayFabId(System.String)
extern "C"  void SetFriendTagsRequest_set_FriendPlayFabId_m947902074 (SetFriendTagsRequest_t1987017382 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFriendPlayFabIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.SetFriendTagsRequest::get_Tags()
extern "C"  List_1_t1765447871 * SetFriendTagsRequest_get_Tags_m741977758 (SetFriendTagsRequest_t1987017382 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CTagsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SetFriendTagsRequest::set_Tags(System.Collections.Generic.List`1<System.String>)
extern "C"  void SetFriendTagsRequest_set_Tags_m1105724311 (SetFriendTagsRequest_t1987017382 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CTagsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.SetFriendTagsResult::.ctor()
extern "C"  void SetFriendTagsResult__ctor_m2700365859 (SetFriendTagsResult_t4117117766 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::.ctor()
extern "C"  void SharedGroupDataRecord__ctor_m127196036 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.SharedGroupDataRecord::get_Value()
extern "C"  String_t* SharedGroupDataRecord_get_Value_m2777304039 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CValueU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::set_Value(System.String)
extern "C"  void SharedGroupDataRecord_set_Value_m1117617228 (SharedGroupDataRecord_t3101610309 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.SharedGroupDataRecord::get_LastUpdatedBy()
extern "C"  String_t* SharedGroupDataRecord_get_LastUpdatedBy_m3856816946 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CLastUpdatedByU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::set_LastUpdatedBy(System.String)
extern "C"  void SharedGroupDataRecord_set_LastUpdatedBy_m1334148577 (SharedGroupDataRecord_t3101610309 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLastUpdatedByU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.DateTime PlayFab.ClientModels.SharedGroupDataRecord::get_LastUpdated()
extern "C"  DateTime_t339033936  SharedGroupDataRecord_get_LastUpdated_m2428103685 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = __this->get_U3CLastUpdatedU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::set_LastUpdated(System.DateTime)
extern "C"  void SharedGroupDataRecord_set_LastUpdated_m1405734574 (SharedGroupDataRecord_t3101610309 * __this, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ___value0;
		__this->set_U3CLastUpdatedU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.UserDataPermission> PlayFab.ClientModels.SharedGroupDataRecord::get_Permission()
extern "C"  Nullable_1_t2611574664  SharedGroupDataRecord_get_Permission_m1737884622 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t2611574664  L_0 = __this->get_U3CPermissionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::set_Permission(System.Nullable`1<PlayFab.ClientModels.UserDataPermission>)
extern "C"  void SharedGroupDataRecord_set_Permission_m2277968901 (SharedGroupDataRecord_t3101610309 * __this, Nullable_1_t2611574664  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t2611574664  L_0 = ___value0;
		__this->set_U3CPermissionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.StartGameRequest::.ctor()
extern "C"  void StartGameRequest__ctor_m1950462962 (StartGameRequest_t2370915499 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameRequest::get_BuildVersion()
extern "C"  String_t* StartGameRequest_get_BuildVersion_m2982438114 (StartGameRequest_t2370915499 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CBuildVersionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameRequest::set_BuildVersion(System.String)
extern "C"  void StartGameRequest_set_BuildVersion_m3242811529 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBuildVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// PlayFab.ClientModels.Region PlayFab.ClientModels.StartGameRequest::get_Region()
extern "C"  int32_t StartGameRequest_get_Region_m4228436782 (StartGameRequest_t2370915499 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CRegionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameRequest::set_Region(PlayFab.ClientModels.Region)
extern "C"  void StartGameRequest_set_Region_m2009805117 (StartGameRequest_t2370915499 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CRegionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameRequest::get_GameMode()
extern "C"  String_t* StartGameRequest_get_GameMode_m1061241805 (StartGameRequest_t2370915499 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CGameModeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameRequest::set_GameMode(System.String)
extern "C"  void StartGameRequest_set_GameMode_m989418494 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CGameModeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameRequest::get_StatisticName()
extern "C"  String_t* StartGameRequest_get_StatisticName_m2258061637 (StartGameRequest_t2370915499 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameRequest::set_StatisticName(System.String)
extern "C"  void StartGameRequest_set_StatisticName_m981961684 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameRequest::get_CharacterId()
extern "C"  String_t* StartGameRequest_get_CharacterId_m4262719342 (StartGameRequest_t2370915499 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameRequest::set_CharacterId(System.String)
extern "C"  void StartGameRequest_set_CharacterId_m329449995 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameRequest::get_CustomCommandLineData()
extern "C"  String_t* StartGameRequest_get_CustomCommandLineData_m87016322 (StartGameRequest_t2370915499 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomCommandLineDataU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameRequest::set_CustomCommandLineData(System.String)
extern "C"  void StartGameRequest_set_CustomCommandLineData_m207123895 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomCommandLineDataU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.StartGameResult::.ctor()
extern "C"  void StartGameResult__ctor_m1088866344 (StartGameResult_t665818273 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameResult::get_LobbyID()
extern "C"  String_t* StartGameResult_get_LobbyID_m201866251 (StartGameResult_t665818273 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CLobbyIDU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameResult::set_LobbyID(System.String)
extern "C"  void StartGameResult_set_LobbyID_m2501857320 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLobbyIDU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameResult::get_ServerHostname()
extern "C"  String_t* StartGameResult_get_ServerHostname_m1882609182 (StartGameResult_t665818273 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CServerHostnameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameResult::set_ServerHostname(System.String)
extern "C"  void StartGameResult_set_ServerHostname_m2308290867 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CServerHostnameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Nullable`1<System.Int32> PlayFab.ClientModels.StartGameResult::get_ServerPort()
extern "C"  Nullable_1_t1438485399  StartGameResult_get_ServerPort_m3896089016 (StartGameResult_t665818273 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = __this->get_U3CServerPortU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameResult::set_ServerPort(System.Nullable`1<System.Int32>)
extern "C"  void StartGameResult_set_ServerPort_m4231781937 (StartGameResult_t665818273 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1438485399  L_0 = ___value0;
		__this->set_U3CServerPortU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameResult::get_Ticket()
extern "C"  String_t* StartGameResult_get_Ticket_m901707284 (StartGameResult_t665818273 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTicketU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameResult::set_Ticket(System.String)
extern "C"  void StartGameResult_set_Ticket_m2971351037 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTicketU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameResult::get_Expires()
extern "C"  String_t* StartGameResult_get_Expires_m2504505102 (StartGameResult_t665818273 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CExpiresU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameResult::set_Expires(System.String)
extern "C"  void StartGameResult_set_Expires_m2142835205 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CExpiresU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartGameResult::get_Password()
extern "C"  String_t* StartGameResult_get_Password_m2456320035 (StartGameResult_t665818273 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPasswordU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartGameResult::set_Password(System.String)
extern "C"  void StartGameResult_set_Password_m3060424334 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPasswordU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseRequest::.ctor()
extern "C"  void StartPurchaseRequest__ctor_m1165822209 (StartPurchaseRequest_t2105128380 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.StartPurchaseRequest::get_CatalogVersion()
extern "C"  String_t* StartPurchaseRequest_get_CatalogVersion_m702756520 (StartPurchaseRequest_t2105128380 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseRequest::set_CatalogVersion(System.String)
extern "C"  void StartPurchaseRequest_set_CatalogVersion_m3876930499 (StartPurchaseRequest_t2105128380 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StartPurchaseRequest::get_StoreId()
extern "C"  String_t* StartPurchaseRequest_get_StoreId_m688693749 (StartPurchaseRequest_t2105128380 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStoreIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseRequest::set_StoreId(System.String)
extern "C"  void StartPurchaseRequest_set_StoreId_m3689463012 (StartPurchaseRequest_t2105128380 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStoreIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemPurchaseRequest> PlayFab.ClientModels.StartPurchaseRequest::get_Items()
extern "C"  List_1_t1528421284 * StartPurchaseRequest_get_Items_m4058324070 (StartPurchaseRequest_t2105128380 * __this, const MethodInfo* method)
{
	{
		List_1_t1528421284 * L_0 = __this->get_U3CItemsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseRequest::set_Items(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemPurchaseRequest>)
extern "C"  void StartPurchaseRequest_set_Items_m2441049229 (StartPurchaseRequest_t2105128380 * __this, List_1_t1528421284 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1528421284 * L_0 = ___value0;
		__this->set_U3CItemsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseResult::.ctor()
extern "C"  void StartPurchaseResult__ctor_m2171934009 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.StartPurchaseResult::get_OrderId()
extern "C"  String_t* StartPurchaseResult_get_OrderId_m1351194996 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COrderIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseResult::set_OrderId(System.String)
extern "C"  void StartPurchaseResult_set_OrderId_m283676383 (StartPurchaseResult_t1765623152 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrderIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem> PlayFab.ClientModels.StartPurchaseResult::get_Contents()
extern "C"  List_1_t45083516 * StartPurchaseResult_get_Contents_m1610386048 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method)
{
	{
		List_1_t45083516 * L_0 = __this->get_U3CContentsU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseResult::set_Contents(System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>)
extern "C"  void StartPurchaseResult_set_Contents_m269900343 (StartPurchaseResult_t1765623152 * __this, List_1_t45083516 * ___value0, const MethodInfo* method)
{
	{
		List_1_t45083516 * L_0 = ___value0;
		__this->set_U3CContentsU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.PaymentOption> PlayFab.ClientModels.StartPurchaseResult::get_PaymentOptions()
extern "C"  List_1_t2086233860 * StartPurchaseResult_get_PaymentOptions_m3124494844 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method)
{
	{
		List_1_t2086233860 * L_0 = __this->get_U3CPaymentOptionsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseResult::set_PaymentOptions(System.Collections.Generic.List`1<PlayFab.ClientModels.PaymentOption>)
extern "C"  void StartPurchaseResult_set_PaymentOptions_m2813423247 (StartPurchaseResult_t1765623152 * __this, List_1_t2086233860 * ___value0, const MethodInfo* method)
{
	{
		List_1_t2086233860 * L_0 = ___value0;
		__this->set_U3CPaymentOptionsU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.StartPurchaseResult::get_VirtualCurrencyBalances()
extern "C"  Dictionary_2_t190145395 * StartPurchaseResult_get_VirtualCurrencyBalances_m922486076 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CVirtualCurrencyBalancesU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StartPurchaseResult::set_VirtualCurrencyBalances(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void StartPurchaseResult_set_VirtualCurrencyBalances_m2111379543 (StartPurchaseResult_t1765623152 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyBalancesU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.StatisticUpdate::.ctor()
extern "C"  void StatisticUpdate__ctor_m3587796896 (StatisticUpdate_t3411289641 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.StatisticUpdate::get_StatisticName()
extern "C"  String_t* StatisticUpdate_get_StatisticName_m373974733 (StatisticUpdate_t3411289641 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StatisticUpdate::set_StatisticName(System.String)
extern "C"  void StatisticUpdate_set_StatisticName_m37662694 (StatisticUpdate_t3411289641 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Nullable`1<System.UInt32> PlayFab.ClientModels.StatisticUpdate::get_Version()
extern "C"  Nullable_1_t3871963234  StatisticUpdate_get_Version_m1558386529 (StatisticUpdate_t3411289641 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3871963234  L_0 = __this->get_U3CVersionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StatisticUpdate::set_Version(System.Nullable`1<System.UInt32>)
extern "C"  void StatisticUpdate_set_Version_m2574958024 (StatisticUpdate_t3411289641 * __this, Nullable_1_t3871963234  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3871963234  L_0 = ___value0;
		__this->set_U3CVersionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.StatisticUpdate::get_Value()
extern "C"  int32_t StatisticUpdate_get_Value_m3849669432 (StatisticUpdate_t3411289641 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StatisticUpdate::set_Value(System.Int32)
extern "C"  void StatisticUpdate_set_Value_m692904675 (StatisticUpdate_t3411289641 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.StatisticValue::.ctor()
extern "C"  void StatisticValue__ctor_m477226924 (StatisticValue_t3193655857 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.StatisticValue::get_StatisticName()
extern "C"  String_t* StatisticValue_get_StatisticName_m2142139071 (StatisticValue_t3193655857 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStatisticNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StatisticValue::set_StatisticName(System.String)
extern "C"  void StatisticValue_set_StatisticName_m3419217242 (StatisticValue_t3193655857 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStatisticNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.StatisticValue::get_Value()
extern "C"  int32_t StatisticValue_get_Value_m3624155040 (StatisticValue_t3193655857 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StatisticValue::set_Value(System.Int32)
extern "C"  void StatisticValue_set_Value_m2817245935 (StatisticValue_t3193655857 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.StatisticValue::get_Version()
extern "C"  String_t* StatisticValue_get_Version_m3742689948 (StatisticValue_t3193655857 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CVersionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StatisticValue::set_Version(System.String)
extern "C"  void StatisticValue_set_Version_m3755026909 (StatisticValue_t3193655857 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CVersionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.SteamPlayFabIdPair::.ctor()
extern "C"  void SteamPlayFabIdPair__ctor_m2335848021 (SteamPlayFabIdPair_t2684316776 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt64 PlayFab.ClientModels.SteamPlayFabIdPair::get_SteamId()
extern "C"  uint64_t SteamPlayFabIdPair_get_SteamId_m3335315121 (SteamPlayFabIdPair_t2684316776 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = __this->get_U3CSteamIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SteamPlayFabIdPair::set_SteamId(System.UInt64)
extern "C"  void SteamPlayFabIdPair_set_SteamId_m3203979258 (SteamPlayFabIdPair_t2684316776 * __this, uint64_t ___value0, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value0;
		__this->set_U3CSteamIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.SteamPlayFabIdPair::get_SteamStringId()
extern "C"  String_t* SteamPlayFabIdPair_get_SteamStringId_m1087141033 (SteamPlayFabIdPair_t2684316776 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSteamStringIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SteamPlayFabIdPair::set_SteamStringId(System.String)
extern "C"  void SteamPlayFabIdPair_set_SteamStringId_m1440820784 (SteamPlayFabIdPair_t2684316776 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSteamStringIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.SteamPlayFabIdPair::get_PlayFabId()
extern "C"  String_t* SteamPlayFabIdPair_get_PlayFabId_m3456150363 (SteamPlayFabIdPair_t2684316776 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPlayFabIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SteamPlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void SteamPlayFabIdPair_set_PlayFabId_m3783662654 (SteamPlayFabIdPair_t2684316776 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPlayFabIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.StoreItem::.ctor()
extern "C"  void StoreItem__ctor_m2646522149 (StoreItem_t2872884100 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.StoreItem::get_ItemId()
extern "C"  String_t* StoreItem_get_ItemId_m3392186745 (StoreItem_t2872884100 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StoreItem::set_ItemId(System.String)
extern "C"  void StoreItem_set_ItemId_m3311521528 (StoreItem_t2872884100 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.StoreItem::get_VirtualCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * StoreItem_get_VirtualCurrencyPrices_m2503535818 (StoreItem_t2872884100 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CVirtualCurrencyPricesU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StoreItem::set_VirtualCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void StoreItem_set_VirtualCurrencyPrices_m4140756165 (StoreItem_t2872884100 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyPricesU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.StoreItem::get_RealCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * StoreItem_get_RealCurrencyPrices_m3970713847 (StoreItem_t2872884100 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CRealCurrencyPricesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.StoreItem::set_RealCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void StoreItem_set_RealCurrencyPrices_m1937727534 (StoreItem_t2872884100 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CRealCurrencyPricesU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::.ctor()
extern "C"  void SubtractUserVirtualCurrencyRequest__ctor_m64819515 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::get_VirtualCurrency()
extern "C"  String_t* SubtractUserVirtualCurrencyRequest_get_VirtualCurrency_m3608477135 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::set_VirtualCurrency(System.String)
extern "C"  void SubtractUserVirtualCurrencyRequest_set_VirtualCurrency_m1885821706 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::get_Amount()
extern "C"  int32_t SubtractUserVirtualCurrencyRequest_get_Amount_m1785151100 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CAmountU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest::set_Amount(System.Int32)
extern "C"  void SubtractUserVirtualCurrencyRequest_set_Amount_m896065679 (SubtractUserVirtualCurrencyRequest_t3004138178 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CAmountU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.TitleNewsItem::.ctor()
extern "C"  void TitleNewsItem__ctor_m3941641723 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.DateTime PlayFab.ClientModels.TitleNewsItem::get_Timestamp()
extern "C"  DateTime_t339033936  TitleNewsItem_get_Timestamp_m4192668109 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = __this->get_U3CTimestampU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TitleNewsItem::set_Timestamp(System.DateTime)
extern "C"  void TitleNewsItem_set_Timestamp_m1497876902 (TitleNewsItem_t2523316974 * __this, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ___value0;
		__this->set_U3CTimestampU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.TitleNewsItem::get_NewsId()
extern "C"  String_t* TitleNewsItem_get_NewsId_m2988503203 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CNewsIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TitleNewsItem::set_NewsId(System.String)
extern "C"  void TitleNewsItem_set_NewsId_m2547245710 (TitleNewsItem_t2523316974 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNewsIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.TitleNewsItem::get_Title()
extern "C"  String_t* TitleNewsItem_get_Title_m3593325317 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TitleNewsItem::set_Title(System.String)
extern "C"  void TitleNewsItem_set_Title_m412461806 (TitleNewsItem_t2523316974 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.TitleNewsItem::get_Body()
extern "C"  String_t* TitleNewsItem_get_Body_m1129684727 (TitleNewsItem_t2523316974 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CBodyU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TitleNewsItem::set_Body(System.String)
extern "C"  void TitleNewsItem_set_Body_m3046810746 (TitleNewsItem_t2523316974 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBodyU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::.ctor()
extern "C"  void TradeInfo__ctor_m3494945351 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.TradeStatus> PlayFab.ClientModels.TradeInfo::get_Status()
extern "C"  Nullable_1_t1403402234  TradeInfo_get_Status_m3862206352 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1403402234  L_0 = __this->get_U3CStatusU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_Status(System.Nullable`1<PlayFab.ClientModels.TradeStatus>)
extern "C"  void TradeInfo_set_Status_m2628834123 (TradeInfo_t1933812770 * __this, Nullable_1_t1403402234  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t1403402234  L_0 = ___value0;
		__this->set_U3CStatusU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.TradeInfo::get_TradeId()
extern "C"  String_t* TradeInfo_get_TradeId_m4260404056 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTradeIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_TradeId(System.String)
extern "C"  void TradeInfo_set_TradeId_m1421729723 (TradeInfo_t1933812770 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTradeIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.TradeInfo::get_OfferingPlayerId()
extern "C"  String_t* TradeInfo_get_OfferingPlayerId_m1367589131 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COfferingPlayerIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_OfferingPlayerId(System.String)
extern "C"  void TradeInfo_set_OfferingPlayerId_m2533268966 (TradeInfo_t1933812770 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COfferingPlayerIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_OfferedInventoryInstanceIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_OfferedInventoryInstanceIds_m444301933 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3COfferedInventoryInstanceIdsU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_OfferedInventoryInstanceIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_OfferedInventoryInstanceIds_m1225186942 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3COfferedInventoryInstanceIdsU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_OfferedCatalogItemIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_OfferedCatalogItemIds_m3633738322 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3COfferedCatalogItemIdsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_OfferedCatalogItemIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_OfferedCatalogItemIds_m62028323 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3COfferedCatalogItemIdsU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_RequestedCatalogItemIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_RequestedCatalogItemIds_m1249112389 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CRequestedCatalogItemIdsU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_RequestedCatalogItemIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_RequestedCatalogItemIds_m3028202454 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CRequestedCatalogItemIdsU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_AllowedPlayerIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_AllowedPlayerIds_m2558003302 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CAllowedPlayerIdsU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_AllowedPlayerIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_AllowedPlayerIds_m4212998877 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CAllowedPlayerIdsU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.TradeInfo::get_AcceptedPlayerId()
extern "C"  String_t* TradeInfo_get_AcceptedPlayerId_m1926167404 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAcceptedPlayerIdU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_AcceptedPlayerId(System.String)
extern "C"  void TradeInfo_set_AcceptedPlayerId_m4232775973 (TradeInfo_t1933812770 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAcceptedPlayerIdU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::get_AcceptedInventoryInstanceIds()
extern "C"  List_1_t1765447871 * TradeInfo_get_AcceptedInventoryInstanceIds_m2428408197 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_AcceptedInventoryInstanceIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void TradeInfo_set_AcceptedInventoryInstanceIds_m1664951676 (TradeInfo_t1933812770 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::get_OpenedAt()
extern "C"  Nullable_1_t3225071844  TradeInfo_get_OpenedAt_m2038590640 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = __this->get_U3COpenedAtU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_OpenedAt(System.Nullable`1<System.DateTime>)
extern "C"  void TradeInfo_set_OpenedAt_m153537767 (TradeInfo_t1933812770 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = ___value0;
		__this->set_U3COpenedAtU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::get_FilledAt()
extern "C"  Nullable_1_t3225071844  TradeInfo_get_FilledAt_m1114254633 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = __this->get_U3CFilledAtU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_FilledAt(System.Nullable`1<System.DateTime>)
extern "C"  void TradeInfo_set_FilledAt_m76623648 (TradeInfo_t1933812770 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = ___value0;
		__this->set_U3CFilledAtU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::get_CancelledAt()
extern "C"  Nullable_1_t3225071844  TradeInfo_get_CancelledAt_m1467109458 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = __this->get_U3CCancelledAtU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_CancelledAt(System.Nullable`1<System.DateTime>)
extern "C"  void TradeInfo_set_CancelledAt_m3033380733 (TradeInfo_t1933812770 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = ___value0;
		__this->set_U3CCancelledAtU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::get_InvalidatedAt()
extern "C"  Nullable_1_t3225071844  TradeInfo_get_InvalidatedAt_m3136666186 (TradeInfo_t1933812770 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = __this->get_U3CInvalidatedAtU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.TradeInfo::set_InvalidatedAt(System.Nullable`1<System.DateTime>)
extern "C"  void TradeInfo_set_InvalidatedAt_m194925813 (TradeInfo_t1933812770 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t3225071844  L_0 = ___value0;
		__this->set_U3CInvalidatedAtU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest::.ctor()
extern "C"  void UnlinkAndroidDeviceIDRequest__ctor_m3733645931 (UnlinkAndroidDeviceIDRequest_t1293232914 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest::get_AndroidDeviceId()
extern "C"  String_t* UnlinkAndroidDeviceIDRequest_get_AndroidDeviceId_m1098042787 (UnlinkAndroidDeviceIDRequest_t1293232914 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAndroidDeviceIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest::set_AndroidDeviceId(System.String)
extern "C"  void UnlinkAndroidDeviceIDRequest_set_AndroidDeviceId_m3113869814 (UnlinkAndroidDeviceIDRequest_t1293232914 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAndroidDeviceIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkAndroidDeviceIDResult::.ctor()
extern "C"  void UnlinkAndroidDeviceIDResult__ctor_m869293711 (UnlinkAndroidDeviceIDResult_t4233284954 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkCustomIDRequest::.ctor()
extern "C"  void UnlinkCustomIDRequest__ctor_m3606506793 (UnlinkCustomIDRequest_t1348755968 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlinkCustomIDRequest::get_CustomId()
extern "C"  String_t* UnlinkCustomIDRequest_get_CustomId_m2856692563 (UnlinkCustomIDRequest_t1348755968 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlinkCustomIDRequest::set_CustomId(System.String)
extern "C"  void UnlinkCustomIDRequest_set_CustomId_m2423470878 (UnlinkCustomIDRequest_t1348755968 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkCustomIDResult::.ctor()
extern "C"  void UnlinkCustomIDResult__ctor_m1419381777 (UnlinkCustomIDResult_t3957981356 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkFacebookAccountRequest::.ctor()
extern "C"  void UnlinkFacebookAccountRequest__ctor_m2539541362 (UnlinkFacebookAccountRequest_t3175165483 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkFacebookAccountResult::.ctor()
extern "C"  void UnlinkFacebookAccountResult__ctor_m4294457512 (UnlinkFacebookAccountResult_t830309153 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkGameCenterAccountRequest::.ctor()
extern "C"  void UnlinkGameCenterAccountRequest__ctor_m2334355569 (UnlinkGameCenterAccountRequest_t687920076 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkGameCenterAccountResult::.ctor()
extern "C"  void UnlinkGameCenterAccountResult__ctor_m1655439305 (UnlinkGameCenterAccountResult_t2689738080 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkGoogleAccountRequest::.ctor()
extern "C"  void UnlinkGoogleAccountRequest__ctor_m531132511 (UnlinkGoogleAccountRequest_t4045133086 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkGoogleAccountResult::.ctor()
extern "C"  void UnlinkGoogleAccountResult__ctor_m1320176155 (UnlinkGoogleAccountResult_t2936582606 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkIOSDeviceIDRequest::.ctor()
extern "C"  void UnlinkIOSDeviceIDRequest__ctor_m3299762569 (UnlinkIOSDeviceIDRequest_t1716723508 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlinkIOSDeviceIDRequest::get_DeviceId()
extern "C"  String_t* UnlinkIOSDeviceIDRequest_get_DeviceId_m4244943794 (UnlinkIOSDeviceIDRequest_t1716723508 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDeviceIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlinkIOSDeviceIDRequest::set_DeviceId(System.String)
extern "C"  void UnlinkIOSDeviceIDRequest_set_DeviceId_m3496325945 (UnlinkIOSDeviceIDRequest_t1716723508 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDeviceIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkIOSDeviceIDResult::.ctor()
extern "C"  void UnlinkIOSDeviceIDResult__ctor_m301108145 (UnlinkIOSDeviceIDResult_t3277114616 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkKongregateAccountRequest::.ctor()
extern "C"  void UnlinkKongregateAccountRequest__ctor_m3749352189 (UnlinkKongregateAccountRequest_t2566546368 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkKongregateAccountResult::.ctor()
extern "C"  void UnlinkKongregateAccountResult__ctor_m1423989693 (UnlinkKongregateAccountResult_t2611791596 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkPSNAccountRequest::.ctor()
extern "C"  void UnlinkPSNAccountRequest__ctor_m3454465919 (UnlinkPSNAccountRequest_t26689898 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkPSNAccountResult::.ctor()
extern "C"  void UnlinkPSNAccountResult__ctor_m3908329211 (UnlinkPSNAccountResult_t2391313410 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkSteamAccountRequest::.ctor()
extern "C"  void UnlinkSteamAccountRequest__ctor_m3897264410 (UnlinkSteamAccountRequest_t775800815 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkSteamAccountResult::.ctor()
extern "C"  void UnlinkSteamAccountResult__ctor_m2537139712 (UnlinkSteamAccountResult_t1030004957 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkXboxAccountRequest::.ctor()
extern "C"  void UnlinkXboxAccountRequest__ctor_m1909999813 (UnlinkXboxAccountRequest_t1448666232 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlinkXboxAccountRequest::get_XboxToken()
extern "C"  String_t* UnlinkXboxAccountRequest_get_XboxToken_m1589018947 (UnlinkXboxAccountRequest_t1448666232 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CXboxTokenU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlinkXboxAccountRequest::set_XboxToken(System.String)
extern "C"  void UnlinkXboxAccountRequest_set_XboxToken_m3920601366 (UnlinkXboxAccountRequest_t1448666232 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CXboxTokenU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlinkXboxAccountResult::.ctor()
extern "C"  void UnlinkXboxAccountResult__ctor_m949013749 (UnlinkXboxAccountResult_t4238298932 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::.ctor()
extern "C"  void UnlockContainerInstanceRequest__ctor_m523485264 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::get_CharacterId()
extern "C"  String_t* UnlockContainerInstanceRequest_get_CharacterId_m2341430540 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::set_CharacterId(System.String)
extern "C"  void UnlockContainerInstanceRequest_set_CharacterId_m3551914989 (UnlockContainerInstanceRequest_t1476418829 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::get_ContainerItemInstanceId()
extern "C"  String_t* UnlockContainerInstanceRequest_get_ContainerItemInstanceId_m2253897548 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CContainerItemInstanceIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::set_ContainerItemInstanceId(System.String)
extern "C"  void UnlockContainerInstanceRequest_set_ContainerItemInstanceId_m2095922989 (UnlockContainerInstanceRequest_t1476418829 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CContainerItemInstanceIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::get_KeyItemInstanceId()
extern "C"  String_t* UnlockContainerInstanceRequest_get_KeyItemInstanceId_m1815916138 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CKeyItemInstanceIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::set_KeyItemInstanceId(System.String)
extern "C"  void UnlockContainerInstanceRequest_set_KeyItemInstanceId_m4270741007 (UnlockContainerInstanceRequest_t1476418829 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CKeyItemInstanceIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerInstanceRequest::get_CatalogVersion()
extern "C"  String_t* UnlockContainerInstanceRequest_get_CatalogVersion_m3273531257 (UnlockContainerInstanceRequest_t1476418829 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerInstanceRequest::set_CatalogVersion(System.String)
extern "C"  void UnlockContainerInstanceRequest_set_CatalogVersion_m206136914 (UnlockContainerInstanceRequest_t1476418829 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemRequest::.ctor()
extern "C"  void UnlockContainerItemRequest__ctor_m1836607054 (UnlockContainerItemRequest_t987080271 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerItemRequest::get_ContainerItemId()
extern "C"  String_t* UnlockContainerItemRequest_get_ContainerItemId_m2253319957 (UnlockContainerItemRequest_t987080271 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CContainerItemIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemRequest::set_ContainerItemId(System.String)
extern "C"  void UnlockContainerItemRequest_set_ContainerItemId_m3694648964 (UnlockContainerItemRequest_t987080271 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CContainerItemIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerItemRequest::get_CatalogVersion()
extern "C"  String_t* UnlockContainerItemRequest_get_CatalogVersion_m2286537915 (UnlockContainerItemRequest_t987080271 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCatalogVersionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemRequest::set_CatalogVersion(System.String)
extern "C"  void UnlockContainerItemRequest_set_CatalogVersion_m1808296656 (UnlockContainerItemRequest_t987080271 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCatalogVersionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerItemRequest::get_CharacterId()
extern "C"  String_t* UnlockContainerItemRequest_get_CharacterId_m429559562 (UnlockContainerItemRequest_t987080271 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemRequest::set_CharacterId(System.String)
extern "C"  void UnlockContainerItemRequest_set_CharacterId_m3281217583 (UnlockContainerItemRequest_t987080271 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::.ctor()
extern "C"  void UnlockContainerItemResult__ctor_m253909580 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerItemResult::get_UnlockedItemInstanceId()
extern "C"  String_t* UnlockContainerItemResult_get_UnlockedItemInstanceId_m2258041066 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUnlockedItemInstanceIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::set_UnlockedItemInstanceId(System.String)
extern "C"  void UnlockContainerItemResult_set_UnlockedItemInstanceId_m908884775 (UnlockContainerItemResult_t2837935741 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUnlockedItemInstanceIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String PlayFab.ClientModels.UnlockContainerItemResult::get_UnlockedWithItemInstanceId()
extern "C"  String_t* UnlockContainerItemResult_get_UnlockedWithItemInstanceId_m1922555600 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::set_UnlockedWithItemInstanceId(System.String)
extern "C"  void UnlockContainerItemResult_set_UnlockedWithItemInstanceId_m984341377 (UnlockContainerItemResult_t2837935741 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.UnlockContainerItemResult::get_GrantedItems()
extern "C"  List_1_t1322103281 * UnlockContainerItemResult_get_GrantedItems_m3369806061 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = __this->get_U3CGrantedItemsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::set_GrantedItems(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void UnlockContainerItemResult_set_GrantedItems_m693190756 (UnlockContainerItemResult_t2837935741 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1322103281 * L_0 = ___value0;
		__this->set_U3CGrantedItemsU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.UnlockContainerItemResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t2623623230 * UnlockContainerItemResult_get_VirtualCurrency_m1043778311 (UnlockContainerItemResult_t2837935741 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = __this->get_U3CVirtualCurrencyU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UnlockContainerItemResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void UnlockContainerItemResult_set_VirtualCurrency_m353700354 (UnlockContainerItemResult_t2837935741 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2623623230 * L_0 = ___value0;
		__this->set_U3CVirtualCurrencyU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::.ctor()
extern "C"  void UpdateCharacterDataRequest__ctor_m723503144 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UpdateCharacterDataRequest::get_CharacterId()
extern "C"  String_t* UpdateCharacterDataRequest_get_CharacterId_m2768037988 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::set_CharacterId(System.String)
extern "C"  void UpdateCharacterDataRequest_set_CharacterId_m3397554453 (UpdateCharacterDataRequest_t3002197813 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.UpdateCharacterDataRequest::get_Data()
extern "C"  Dictionary_2_t2606186806 * UpdateCharacterDataRequest_get_Data_m2830515745 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = __this->get_U3CDataU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::set_Data(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UpdateCharacterDataRequest_set_Data_m34486260 (UpdateCharacterDataRequest_t3002197813 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.UpdateCharacterDataRequest::get_KeysToRemove()
extern "C"  List_1_t1765447871 * UpdateCharacterDataRequest_get_KeysToRemove_m325457991 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_U3CKeysToRemoveU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::set_KeysToRemove(System.Collections.Generic.List`1<System.String>)
extern "C"  void UpdateCharacterDataRequest_set_KeysToRemove_m4177904000 (UpdateCharacterDataRequest_t3002197813 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = ___value0;
		__this->set_U3CKeysToRemoveU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Nullable`1<PlayFab.ClientModels.UserDataPermission> PlayFab.ClientModels.UpdateCharacterDataRequest::get_Permission()
extern "C"  Nullable_1_t2611574664  UpdateCharacterDataRequest_get_Permission_m185268094 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t2611574664  L_0 = __this->get_U3CPermissionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::set_Permission(System.Nullable`1<PlayFab.ClientModels.UserDataPermission>)
extern "C"  void UpdateCharacterDataRequest_set_Permission_m723143905 (UpdateCharacterDataRequest_t3002197813 * __this, Nullable_1_t2611574664  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t2611574664  L_0 = ___value0;
		__this->set_U3CPermissionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterDataResult::.ctor()
extern "C"  void UpdateCharacterDataResult__ctor_m4235875634 (UpdateCharacterDataResult_t1101824215 * __this, const MethodInfo* method)
{
	{
		PlayFabResultCommon__ctor_m4291338422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32 PlayFab.ClientModels.UpdateCharacterDataResult::get_DataVersion()
extern "C"  uint32_t UpdateCharacterDataResult_get_DataVersion_m25832442 (UpdateCharacterDataResult_t1101824215 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_U3CDataVersionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterDataResult::set_DataVersion(System.UInt32)
extern "C"  void UpdateCharacterDataResult_set_DataVersion_m3028528601 (UpdateCharacterDataResult_t1101824215 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_U3CDataVersionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterStatisticsRequest::.ctor()
extern "C"  void UpdateCharacterStatisticsRequest__ctor_m3916736545 (UpdateCharacterStatisticsRequest_t1560413084 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayFab.ClientModels.UpdateCharacterStatisticsRequest::get_CharacterId()
extern "C"  String_t* UpdateCharacterStatisticsRequest_get_CharacterId_m1305611997 (UpdateCharacterStatisticsRequest_t1560413084 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCharacterIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterStatisticsRequest::set_CharacterId(System.String)
extern "C"  void UpdateCharacterStatisticsRequest_set_CharacterId_m2795948284 (UpdateCharacterStatisticsRequest_t1560413084 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCharacterIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.UpdateCharacterStatisticsRequest::get_CharacterStatistics()
extern "C"  Dictionary_2_t190145395 * UpdateCharacterStatisticsRequest_get_CharacterStatistics_m197238727 (UpdateCharacterStatisticsRequest_t1560413084 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = __this->get_U3CCharacterStatisticsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void PlayFab.ClientModels.UpdateCharacterStatisticsRequest::set_CharacterStatistics(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void UpdateCharacterStatisticsRequest_set_CharacterStatistics_m2464082390 (UpdateCharacterStatisticsRequest_t1560413084 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t190145395 * L_0 = ___value0;
		__this->set_U3CCharacterStatisticsU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
