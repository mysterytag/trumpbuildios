﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils
struct ReflectionUtils_t266409575;
// System.Type
struct Type_t;
// System.Attribute
struct Attribute_t498693649;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo>
struct IEnumerable_1_t2119324394;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3542137334;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
struct IEnumerable_1_t67735429;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
struct IEnumerable_1_t4037084138;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// PlayFab.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t4072949631;
// PlayFab.ReflectionUtils/GetDelegate
struct GetDelegate_t270123739;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// PlayFab.ReflectionUtils/SetDelegate
struct SetDelegate_t181543911;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"
#include "mscorlib_System_Reflection_ConstructorInfo3542137334.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"

// System.Void PlayFab.ReflectionUtils::.ctor()
extern "C"  void ReflectionUtils__ctor_m327858560 (ReflectionUtils_t266409575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ReflectionUtils::.cctor()
extern "C"  void ReflectionUtils__cctor_m1091584557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type PlayFab.ReflectionUtils::GetTypeInfo(System.Type)
extern "C"  Type_t * ReflectionUtils_GetTypeInfo_m1705001027 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute PlayFab.ReflectionUtils::GetAttribute(System.Reflection.MemberInfo,System.Type)
extern "C"  Attribute_t498693649 * ReflectionUtils_GetAttribute_m3938741233 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___info0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type PlayFab.ReflectionUtils::GetGenericListElementType(System.Type)
extern "C"  Type_t * ReflectionUtils_GetGenericListElementType_m2190762154 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute PlayFab.ReflectionUtils::GetAttribute(System.Type,System.Type)
extern "C"  Attribute_t498693649 * ReflectionUtils_GetAttribute_m1901231234 (Il2CppObject * __this /* static, unused */, Type_t * ___objectType0, Type_t * ___attributeType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] PlayFab.ReflectionUtils::GetGenericTypeArguments(System.Type)
extern "C"  TypeU5BU5D_t3431720054* ReflectionUtils_GetGenericTypeArguments_m892654504 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ReflectionUtils::IsTypeGeneric(System.Type)
extern "C"  bool ReflectionUtils_IsTypeGeneric_m966573332 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ReflectionUtils::IsTypeGenericeCollectionInterface(System.Type)
extern "C"  bool ReflectionUtils_IsTypeGenericeCollectionInterface_m4084531006 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ReflectionUtils::IsAssignableFrom(System.Type,System.Type)
extern "C"  bool ReflectionUtils_IsAssignableFrom_m293192671 (Il2CppObject * __this /* static, unused */, Type_t * ___type10, Type_t * ___type21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ReflectionUtils::IsTypeDictionary(System.Type)
extern "C"  bool ReflectionUtils_IsTypeDictionary_m2664210543 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ReflectionUtils::IsNullableType(System.Type)
extern "C"  bool ReflectionUtils_IsNullableType_m4188866884 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ReflectionUtils::ToNullableType(System.Object,System.Type)
extern "C"  Il2CppObject * ReflectionUtils_ToNullableType_m599858338 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, Type_t * ___nullableType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ReflectionUtils::IsValueType(System.Type)
extern "C"  bool ReflectionUtils_IsValueType_m3852799750 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo> PlayFab.ReflectionUtils::GetConstructors(System.Type)
extern "C"  Il2CppObject* ReflectionUtils_GetConstructors_m140052412 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo PlayFab.ReflectionUtils::GetConstructorInfo(System.Type,System.Type[])
extern "C"  ConstructorInfo_t3542137334 * ReflectionUtils_GetConstructorInfo_m4227851779 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, TypeU5BU5D_t3431720054* ___argsType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> PlayFab.ReflectionUtils::GetProperties(System.Type)
extern "C"  Il2CppObject* ReflectionUtils_GetProperties_m2528293283 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> PlayFab.ReflectionUtils::GetFields(System.Type)
extern "C"  Il2CppObject* ReflectionUtils_GetFields_m1386743548 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo PlayFab.ReflectionUtils::GetGetterMethodInfo(System.Reflection.PropertyInfo)
extern "C"  MethodInfo_t * ReflectionUtils_GetGetterMethodInfo_m2538363601 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo PlayFab.ReflectionUtils::GetSetterMethodInfo(System.Reflection.PropertyInfo)
extern "C"  MethodInfo_t * ReflectionUtils_GetSetterMethodInfo_m1288433989 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/ConstructorDelegate PlayFab.ReflectionUtils::GetContructor(System.Reflection.ConstructorInfo)
extern "C"  ConstructorDelegate_t4072949631 * ReflectionUtils_GetContructor_m124695481 (Il2CppObject * __this /* static, unused */, ConstructorInfo_t3542137334 * ___constructorInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/ConstructorDelegate PlayFab.ReflectionUtils::GetContructor(System.Type,System.Type[])
extern "C"  ConstructorDelegate_t4072949631 * ReflectionUtils_GetContructor_m1521253205 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, TypeU5BU5D_t3431720054* ___argsType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/ConstructorDelegate PlayFab.ReflectionUtils::GetConstructorByReflection(System.Reflection.ConstructorInfo)
extern "C"  ConstructorDelegate_t4072949631 * ReflectionUtils_GetConstructorByReflection_m2006109788 (Il2CppObject * __this /* static, unused */, ConstructorInfo_t3542137334 * ___constructorInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/ConstructorDelegate PlayFab.ReflectionUtils::GetConstructorByReflection(System.Type,System.Type[])
extern "C"  ConstructorDelegate_t4072949631 * ReflectionUtils_GetConstructorByReflection_m1845160184 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, TypeU5BU5D_t3431720054* ___argsType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/GetDelegate PlayFab.ReflectionUtils::GetGetMethod(System.Reflection.PropertyInfo)
extern "C"  GetDelegate_t270123739 * ReflectionUtils_GetGetMethod_m581890114 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/GetDelegate PlayFab.ReflectionUtils::GetGetMethod(System.Reflection.FieldInfo)
extern "C"  GetDelegate_t270123739 * ReflectionUtils_GetGetMethod_m2795792901 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/GetDelegate PlayFab.ReflectionUtils::GetGetMethodByReflection(System.Reflection.PropertyInfo)
extern "C"  GetDelegate_t270123739 * ReflectionUtils_GetGetMethodByReflection_m3798950980 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/GetDelegate PlayFab.ReflectionUtils::GetGetMethodByReflection(System.Reflection.FieldInfo)
extern "C"  GetDelegate_t270123739 * ReflectionUtils_GetGetMethodByReflection_m2582529347 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/SetDelegate PlayFab.ReflectionUtils::GetSetMethod(System.Reflection.PropertyInfo)
extern "C"  SetDelegate_t181543911 * ReflectionUtils_GetSetMethod_m1071709274 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/SetDelegate PlayFab.ReflectionUtils::GetSetMethod(System.Reflection.FieldInfo)
extern "C"  SetDelegate_t181543911 * ReflectionUtils_GetSetMethod_m1653839085 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/SetDelegate PlayFab.ReflectionUtils::GetSetMethodByReflection(System.Reflection.PropertyInfo)
extern "C"  SetDelegate_t181543911 * ReflectionUtils_GetSetMethodByReflection_m1161301084 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ReflectionUtils/SetDelegate PlayFab.ReflectionUtils::GetSetMethodByReflection(System.Reflection.FieldInfo)
extern "C"  SetDelegate_t181543911 * ReflectionUtils_GetSetMethodByReflection_m2368492587 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
