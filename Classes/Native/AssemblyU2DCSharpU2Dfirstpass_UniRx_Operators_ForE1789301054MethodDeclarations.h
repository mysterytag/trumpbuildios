﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>
struct ForEachAsync__t1789301054;
// UniRx.Operators.ForEachAsyncObservable`1<System.Object>
struct ForEachAsyncObservable_1_t3176723348;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>::.ctor(UniRx.Operators.ForEachAsyncObservable`1<T>,UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  void ForEachAsync___ctor_m396767131_gshared (ForEachAsync__t1789301054 * __this, ForEachAsyncObservable_1_t3176723348 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ForEachAsync___ctor_m396767131(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ForEachAsync__t1789301054 *, ForEachAsyncObservable_1_t3176723348 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ForEachAsync___ctor_m396767131_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>::OnNext(T)
extern "C"  void ForEachAsync__OnNext_m449815454_gshared (ForEachAsync__t1789301054 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ForEachAsync__OnNext_m449815454(__this, ___value0, method) ((  void (*) (ForEachAsync__t1789301054 *, Il2CppObject *, const MethodInfo*))ForEachAsync__OnNext_m449815454_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>::OnError(System.Exception)
extern "C"  void ForEachAsync__OnError_m2034549421_gshared (ForEachAsync__t1789301054 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ForEachAsync__OnError_m2034549421(__this, ___error0, method) ((  void (*) (ForEachAsync__t1789301054 *, Exception_t1967233988 *, const MethodInfo*))ForEachAsync__OnError_m2034549421_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync_<System.Object>::OnCompleted()
extern "C"  void ForEachAsync__OnCompleted_m1913147776_gshared (ForEachAsync__t1789301054 * __this, const MethodInfo* method);
#define ForEachAsync__OnCompleted_m1913147776(__this, method) ((  void (*) (ForEachAsync__t1789301054 *, const MethodInfo*))ForEachAsync__OnCompleted_m1913147776_gshared)(__this, method)
