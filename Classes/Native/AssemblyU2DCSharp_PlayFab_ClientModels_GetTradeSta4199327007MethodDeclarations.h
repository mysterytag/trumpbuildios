﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetTradeStatusRequest
struct GetTradeStatusRequest_t4199327007;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetTradeStatusRequest::.ctor()
extern "C"  void GetTradeStatusRequest__ctor_m2676931818 (GetTradeStatusRequest_t4199327007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetTradeStatusRequest::get_OfferingPlayerId()
extern "C"  String_t* GetTradeStatusRequest_get_OfferingPlayerId_m173272968 (GetTradeStatusRequest_t4199327007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetTradeStatusRequest::set_OfferingPlayerId(System.String)
extern "C"  void GetTradeStatusRequest_set_OfferingPlayerId_m2823829001 (GetTradeStatusRequest_t4199327007 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetTradeStatusRequest::get_TradeId()
extern "C"  String_t* GetTradeStatusRequest_get_TradeId_m767802427 (GetTradeStatusRequest_t4199327007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetTradeStatusRequest::set_TradeId(System.String)
extern "C"  void GetTradeStatusRequest_set_TradeId_m4237362232 (GetTradeStatusRequest_t4199327007 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
