﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ScrollRect
struct ScrollRect_t1048578170;
// Cell`1<System.Single>
struct Cell_1_t1140306653;
// System.Collections.Generic.List`1<ITableBlock>
struct List_1_t3225501569;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// System.Func`2<ITableBlock,System.Single>
struct Func_2_t4022284381;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewTableView
struct  NewTableView_t2775249395  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.ScrollRect NewTableView::scrollRect
	ScrollRect_t1048578170 * ___scrollRect_2;
	// Cell`1<System.Single> NewTableView::scrollPosition
	Cell_1_t1140306653 * ___scrollPosition_3;
	// System.Collections.Generic.List`1<ITableBlock> NewTableView::blocks
	List_1_t3225501569 * ___blocks_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> NewTableView::pools
	Dictionary_2_t2474804324 * ___pools_5;
	// UnityEngine.RectTransform NewTableView::containerRect
	RectTransform_t3317474837 * ___containerRect_6;
	// System.Boolean NewTableView::needRebuild
	bool ___needRebuild_7;
	// UnityEngine.RectTransform NewTableView::scrollRectTransform
	RectTransform_t3317474837 * ___scrollRectTransform_8;
	// System.Single NewTableView::topOffset
	float ___topOffset_9;
	// System.Single NewTableView::bottomOffset
	float ___bottomOffset_10;

public:
	inline static int32_t get_offset_of_scrollRect_2() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___scrollRect_2)); }
	inline ScrollRect_t1048578170 * get_scrollRect_2() const { return ___scrollRect_2; }
	inline ScrollRect_t1048578170 ** get_address_of_scrollRect_2() { return &___scrollRect_2; }
	inline void set_scrollRect_2(ScrollRect_t1048578170 * value)
	{
		___scrollRect_2 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_2, value);
	}

	inline static int32_t get_offset_of_scrollPosition_3() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___scrollPosition_3)); }
	inline Cell_1_t1140306653 * get_scrollPosition_3() const { return ___scrollPosition_3; }
	inline Cell_1_t1140306653 ** get_address_of_scrollPosition_3() { return &___scrollPosition_3; }
	inline void set_scrollPosition_3(Cell_1_t1140306653 * value)
	{
		___scrollPosition_3 = value;
		Il2CppCodeGenWriteBarrier(&___scrollPosition_3, value);
	}

	inline static int32_t get_offset_of_blocks_4() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___blocks_4)); }
	inline List_1_t3225501569 * get_blocks_4() const { return ___blocks_4; }
	inline List_1_t3225501569 ** get_address_of_blocks_4() { return &___blocks_4; }
	inline void set_blocks_4(List_1_t3225501569 * value)
	{
		___blocks_4 = value;
		Il2CppCodeGenWriteBarrier(&___blocks_4, value);
	}

	inline static int32_t get_offset_of_pools_5() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___pools_5)); }
	inline Dictionary_2_t2474804324 * get_pools_5() const { return ___pools_5; }
	inline Dictionary_2_t2474804324 ** get_address_of_pools_5() { return &___pools_5; }
	inline void set_pools_5(Dictionary_2_t2474804324 * value)
	{
		___pools_5 = value;
		Il2CppCodeGenWriteBarrier(&___pools_5, value);
	}

	inline static int32_t get_offset_of_containerRect_6() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___containerRect_6)); }
	inline RectTransform_t3317474837 * get_containerRect_6() const { return ___containerRect_6; }
	inline RectTransform_t3317474837 ** get_address_of_containerRect_6() { return &___containerRect_6; }
	inline void set_containerRect_6(RectTransform_t3317474837 * value)
	{
		___containerRect_6 = value;
		Il2CppCodeGenWriteBarrier(&___containerRect_6, value);
	}

	inline static int32_t get_offset_of_needRebuild_7() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___needRebuild_7)); }
	inline bool get_needRebuild_7() const { return ___needRebuild_7; }
	inline bool* get_address_of_needRebuild_7() { return &___needRebuild_7; }
	inline void set_needRebuild_7(bool value)
	{
		___needRebuild_7 = value;
	}

	inline static int32_t get_offset_of_scrollRectTransform_8() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___scrollRectTransform_8)); }
	inline RectTransform_t3317474837 * get_scrollRectTransform_8() const { return ___scrollRectTransform_8; }
	inline RectTransform_t3317474837 ** get_address_of_scrollRectTransform_8() { return &___scrollRectTransform_8; }
	inline void set_scrollRectTransform_8(RectTransform_t3317474837 * value)
	{
		___scrollRectTransform_8 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRectTransform_8, value);
	}

	inline static int32_t get_offset_of_topOffset_9() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___topOffset_9)); }
	inline float get_topOffset_9() const { return ___topOffset_9; }
	inline float* get_address_of_topOffset_9() { return &___topOffset_9; }
	inline void set_topOffset_9(float value)
	{
		___topOffset_9 = value;
	}

	inline static int32_t get_offset_of_bottomOffset_10() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395, ___bottomOffset_10)); }
	inline float get_bottomOffset_10() const { return ___bottomOffset_10; }
	inline float* get_address_of_bottomOffset_10() { return &___bottomOffset_10; }
	inline void set_bottomOffset_10(float value)
	{
		___bottomOffset_10 = value;
	}
};

struct NewTableView_t2775249395_StaticFields
{
public:
	// System.Func`2<ITableBlock,System.Single> NewTableView::<>f__am$cache9
	Func_2_t4022284381 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(NewTableView_t2775249395_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Func_2_t4022284381 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Func_2_t4022284381 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Func_2_t4022284381 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
