﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.BehaviorSubject`1<System.Object>
struct  BehaviorSubject_1_t2662307150  : public Il2CppObject
{
public:
	// System.Object UniRx.BehaviorSubject`1::observerLock
	Il2CppObject * ___observerLock_0;
	// System.Boolean UniRx.BehaviorSubject`1::isStopped
	bool ___isStopped_1;
	// System.Boolean UniRx.BehaviorSubject`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.BehaviorSubject`1::lastValue
	Il2CppObject * ___lastValue_3;
	// System.Exception UniRx.BehaviorSubject`1::lastError
	Exception_t1967233988 * ___lastError_4;
	// UniRx.IObserver`1<T> UniRx.BehaviorSubject`1::outObserver
	Il2CppObject* ___outObserver_5;

public:
	inline static int32_t get_offset_of_observerLock_0() { return static_cast<int32_t>(offsetof(BehaviorSubject_1_t2662307150, ___observerLock_0)); }
	inline Il2CppObject * get_observerLock_0() const { return ___observerLock_0; }
	inline Il2CppObject ** get_address_of_observerLock_0() { return &___observerLock_0; }
	inline void set_observerLock_0(Il2CppObject * value)
	{
		___observerLock_0 = value;
		Il2CppCodeGenWriteBarrier(&___observerLock_0, value);
	}

	inline static int32_t get_offset_of_isStopped_1() { return static_cast<int32_t>(offsetof(BehaviorSubject_1_t2662307150, ___isStopped_1)); }
	inline bool get_isStopped_1() const { return ___isStopped_1; }
	inline bool* get_address_of_isStopped_1() { return &___isStopped_1; }
	inline void set_isStopped_1(bool value)
	{
		___isStopped_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(BehaviorSubject_1_t2662307150, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_lastValue_3() { return static_cast<int32_t>(offsetof(BehaviorSubject_1_t2662307150, ___lastValue_3)); }
	inline Il2CppObject * get_lastValue_3() const { return ___lastValue_3; }
	inline Il2CppObject ** get_address_of_lastValue_3() { return &___lastValue_3; }
	inline void set_lastValue_3(Il2CppObject * value)
	{
		___lastValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___lastValue_3, value);
	}

	inline static int32_t get_offset_of_lastError_4() { return static_cast<int32_t>(offsetof(BehaviorSubject_1_t2662307150, ___lastError_4)); }
	inline Exception_t1967233988 * get_lastError_4() const { return ___lastError_4; }
	inline Exception_t1967233988 ** get_address_of_lastError_4() { return &___lastError_4; }
	inline void set_lastError_4(Exception_t1967233988 * value)
	{
		___lastError_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastError_4, value);
	}

	inline static int32_t get_offset_of_outObserver_5() { return static_cast<int32_t>(offsetof(BehaviorSubject_1_t2662307150, ___outObserver_5)); }
	inline Il2CppObject* get_outObserver_5() const { return ___outObserver_5; }
	inline Il2CppObject** get_address_of_outObserver_5() { return &___outObserver_5; }
	inline void set_outObserver_5(Il2CppObject* value)
	{
		___outObserver_5 = value;
		Il2CppCodeGenWriteBarrier(&___outObserver_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
