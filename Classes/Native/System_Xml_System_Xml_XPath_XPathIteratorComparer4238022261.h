﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XPath.XPathIteratorComparer
struct XPathIteratorComparer_t4238022261;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathIteratorComparer
struct  XPathIteratorComparer_t4238022261  : public Il2CppObject
{
public:

public:
};

struct XPathIteratorComparer_t4238022261_StaticFields
{
public:
	// System.Xml.XPath.XPathIteratorComparer System.Xml.XPath.XPathIteratorComparer::Instance
	XPathIteratorComparer_t4238022261 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(XPathIteratorComparer_t4238022261_StaticFields, ___Instance_0)); }
	inline XPathIteratorComparer_t4238022261 * get_Instance_0() const { return ___Instance_0; }
	inline XPathIteratorComparer_t4238022261 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(XPathIteratorComparer_t4238022261 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
