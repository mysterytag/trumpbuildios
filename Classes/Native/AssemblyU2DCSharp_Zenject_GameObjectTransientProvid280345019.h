﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectTransientProviderFromPrefabResource
struct  GameObjectTransientProviderFromPrefabResource_t280345019  : public ProviderBase_t1627494391
{
public:
	// Zenject.DiContainer Zenject.GameObjectTransientProviderFromPrefabResource::_container
	DiContainer_t2383114449 * ____container_2;
	// System.String Zenject.GameObjectTransientProviderFromPrefabResource::_resourcePath
	String_t* ____resourcePath_3;
	// System.Type Zenject.GameObjectTransientProviderFromPrefabResource::_concreteType
	Type_t * ____concreteType_4;

public:
	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(GameObjectTransientProviderFromPrefabResource_t280345019, ____container_2)); }
	inline DiContainer_t2383114449 * get__container_2() const { return ____container_2; }
	inline DiContainer_t2383114449 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_t2383114449 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier(&____container_2, value);
	}

	inline static int32_t get_offset_of__resourcePath_3() { return static_cast<int32_t>(offsetof(GameObjectTransientProviderFromPrefabResource_t280345019, ____resourcePath_3)); }
	inline String_t* get__resourcePath_3() const { return ____resourcePath_3; }
	inline String_t** get_address_of__resourcePath_3() { return &____resourcePath_3; }
	inline void set__resourcePath_3(String_t* value)
	{
		____resourcePath_3 = value;
		Il2CppCodeGenWriteBarrier(&____resourcePath_3, value);
	}

	inline static int32_t get_offset_of__concreteType_4() { return static_cast<int32_t>(offsetof(GameObjectTransientProviderFromPrefabResource_t280345019, ____concreteType_4)); }
	inline Type_t * get__concreteType_4() const { return ____concreteType_4; }
	inline Type_t ** get_address_of__concreteType_4() { return &____concreteType_4; }
	inline void set__concreteType_4(Type_t * value)
	{
		____concreteType_4 = value;
		Il2CppCodeGenWriteBarrier(&____concreteType_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
