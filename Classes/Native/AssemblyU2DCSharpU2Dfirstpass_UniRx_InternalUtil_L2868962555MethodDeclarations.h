﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>
struct ListObserver_1_t2868962555;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>>
struct ImmutableList_1_t1551580990;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>
struct IObserver_1_t491376491;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1684225385_gshared (ListObserver_1_t2868962555 * __this, ImmutableList_1_t1551580990 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m1684225385(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2868962555 *, ImmutableList_1_t1551580990 *, const MethodInfo*))ListObserver_1__ctor_m1684225385_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m212763617_gshared (ListObserver_1_t2868962555 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m212763617(__this, method) ((  void (*) (ListObserver_1_t2868962555 *, const MethodInfo*))ListObserver_1_OnCompleted_m212763617_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1104505742_gshared (ListObserver_1_t2868962555 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m1104505742(__this, ___error0, method) ((  void (*) (ListObserver_1_t2868962555 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m1104505742_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2624073343_gshared (ListObserver_1_t2868962555 * __this, NetworkMessageInfo_t2574344884  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m2624073343(__this, ___value0, method) ((  void (*) (ListObserver_1_t2868962555 *, NetworkMessageInfo_t2574344884 , const MethodInfo*))ListObserver_1_OnNext_m2624073343_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1120274847_gshared (ListObserver_1_t2868962555 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1120274847(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2868962555 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1120274847_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkMessageInfo>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2645522764_gshared (ListObserver_1_t2868962555 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2645522764(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2868962555 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2645522764_gshared)(__this, ___observer0, method)
