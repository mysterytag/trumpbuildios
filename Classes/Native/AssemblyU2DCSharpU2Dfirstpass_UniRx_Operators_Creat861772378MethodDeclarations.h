﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>
struct CreateSafe_t861772378;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void CreateSafe__ctor_m903941043_gshared (CreateSafe_t861772378 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CreateSafe__ctor_m903941043(__this, ___observer0, ___cancel1, method) ((  void (*) (CreateSafe_t861772378 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CreateSafe__ctor_m903941043_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>::OnNext(T)
extern "C"  void CreateSafe_OnNext_m2277004287_gshared (CreateSafe_t861772378 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CreateSafe_OnNext_m2277004287(__this, ___value0, method) ((  void (*) (CreateSafe_t861772378 *, Il2CppObject *, const MethodInfo*))CreateSafe_OnNext_m2277004287_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>::OnError(System.Exception)
extern "C"  void CreateSafe_OnError_m1865060622_gshared (CreateSafe_t861772378 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CreateSafe_OnError_m1865060622(__this, ___error0, method) ((  void (*) (CreateSafe_t861772378 *, Exception_t1967233988 *, const MethodInfo*))CreateSafe_OnError_m1865060622_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CreateSafeObservable`1/CreateSafe<System.Object>::OnCompleted()
extern "C"  void CreateSafe_OnCompleted_m3765430625_gshared (CreateSafe_t861772378 * __this, const MethodInfo* method);
#define CreateSafe_OnCompleted_m3765430625(__this, method) ((  void (*) (CreateSafe_t861772378 *, const MethodInfo*))CreateSafe_OnCompleted_m3765430625_gshared)(__this, method)
