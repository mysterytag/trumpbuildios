﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Dns/ResolveCallback
struct ResolveCallback_t1117196849;
// System.Object
struct Il2CppObject;
// System.Net.IPHostEntry
struct IPHostEntry_t4205702573;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.Dns/ResolveCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ResolveCallback__ctor_m1168371937 (ResolveCallback_t1117196849 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/ResolveCallback::Invoke(System.String)
extern "C"  IPHostEntry_t4205702573 * ResolveCallback_Invoke_m4249394267 (ResolveCallback_t1117196849 * __this, String_t* ___hostName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" IPHostEntry_t4205702573 * pinvoke_delegate_wrapper_ResolveCallback_t1117196849(Il2CppObject* delegate, String_t* ___hostName0);
// System.IAsyncResult System.Net.Dns/ResolveCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResolveCallback_BeginInvoke_m1418236396 (ResolveCallback_t1117196849 * __this, String_t* ___hostName0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/ResolveCallback::EndInvoke(System.IAsyncResult)
extern "C"  IPHostEntry_t4205702573 * ResolveCallback_EndInvoke_m4274117629 (ResolveCallback_t1117196849 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
