﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>
struct EqualityComparer_1_t1850912637;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3573217627_gshared (EqualityComparer_1_t1850912637 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m3573217627(__this, method) ((  void (*) (EqualityComparer_1_t1850912637 *, const MethodInfo*))EqualityComparer_1__ctor_m3573217627_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2913467826_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2913467826(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2913467826_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3521257268_gshared (EqualityComparer_1_t1850912637 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3521257268(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t1850912637 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3521257268_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2616804182_gshared (EqualityComparer_1_t1850912637 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2616804182(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t1850912637 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2616804182_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UniRx.CollectionAddEvent`1<System.Object>>::get_Default()
extern "C"  EqualityComparer_1_t1850912637 * EqualityComparer_1_get_Default_m576764869_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m576764869(__this /* static, unused */, method) ((  EqualityComparer_1_t1850912637 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m576764869_gshared)(__this /* static, unused */, method)
