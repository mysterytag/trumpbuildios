﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateCharacterStatisticsRequest
struct UpdateCharacterStatisticsRequest_t1560413084;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UpdateCharacterStatisticsRequest::.ctor()
extern "C"  void UpdateCharacterStatisticsRequest__ctor_m3916736545 (UpdateCharacterStatisticsRequest_t1560413084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UpdateCharacterStatisticsRequest::get_CharacterId()
extern "C"  String_t* UpdateCharacterStatisticsRequest_get_CharacterId_m1305611997 (UpdateCharacterStatisticsRequest_t1560413084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateCharacterStatisticsRequest::set_CharacterId(System.String)
extern "C"  void UpdateCharacterStatisticsRequest_set_CharacterId_m2795948284 (UpdateCharacterStatisticsRequest_t1560413084 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.UpdateCharacterStatisticsRequest::get_CharacterStatistics()
extern "C"  Dictionary_2_t190145395 * UpdateCharacterStatisticsRequest_get_CharacterStatistics_m197238727 (UpdateCharacterStatisticsRequest_t1560413084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateCharacterStatisticsRequest::set_CharacterStatistics(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void UpdateCharacterStatisticsRequest_set_CharacterStatistics_m2464082390 (UpdateCharacterStatisticsRequest_t1560413084 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
