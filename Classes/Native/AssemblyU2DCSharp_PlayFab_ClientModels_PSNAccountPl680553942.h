﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.PSNAccountPlayFabIdPair
struct  PSNAccountPlayFabIdPair_t680553942  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.PSNAccountPlayFabIdPair::<PSNAccountId>k__BackingField
	String_t* ___U3CPSNAccountIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.PSNAccountPlayFabIdPair::<PlayFabId>k__BackingField
	String_t* ___U3CPlayFabIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPSNAccountIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PSNAccountPlayFabIdPair_t680553942, ___U3CPSNAccountIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CPSNAccountIdU3Ek__BackingField_0() const { return ___U3CPSNAccountIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPSNAccountIdU3Ek__BackingField_0() { return &___U3CPSNAccountIdU3Ek__BackingField_0; }
	inline void set_U3CPSNAccountIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CPSNAccountIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPSNAccountIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CPlayFabIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PSNAccountPlayFabIdPair_t680553942, ___U3CPlayFabIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CPlayFabIdU3Ek__BackingField_1() const { return ___U3CPlayFabIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CPlayFabIdU3Ek__BackingField_1() { return &___U3CPlayFabIdU3Ek__BackingField_1; }
	inline void set_U3CPlayFabIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CPlayFabIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayFabIdU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
