﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>
struct ThrowObserver_1_t3210247653;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::.ctor()
extern "C"  void ThrowObserver_1__ctor_m872765267_gshared (ThrowObserver_1_t3210247653 * __this, const MethodInfo* method);
#define ThrowObserver_1__ctor_m872765267(__this, method) ((  void (*) (ThrowObserver_1_t3210247653 *, const MethodInfo*))ThrowObserver_1__ctor_m872765267_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::.cctor()
extern "C"  void ThrowObserver_1__cctor_m803823290_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ThrowObserver_1__cctor_m803823290(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThrowObserver_1__cctor_m803823290_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::OnCompleted()
extern "C"  void ThrowObserver_1_OnCompleted_m838369437_gshared (ThrowObserver_1_t3210247653 * __this, const MethodInfo* method);
#define ThrowObserver_1_OnCompleted_m838369437(__this, method) ((  void (*) (ThrowObserver_1_t3210247653 *, const MethodInfo*))ThrowObserver_1_OnCompleted_m838369437_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void ThrowObserver_1_OnError_m3829280330_gshared (ThrowObserver_1_t3210247653 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ThrowObserver_1_OnError_m3829280330(__this, ___error0, method) ((  void (*) (ThrowObserver_1_t3210247653 *, Exception_t1967233988 *, const MethodInfo*))ThrowObserver_1_OnError_m3829280330_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ThrowObserver`1<UniRx.Unit>::OnNext(T)
extern "C"  void ThrowObserver_1_OnNext_m4039502139_gshared (ThrowObserver_1_t3210247653 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define ThrowObserver_1_OnNext_m4039502139(__this, ___value0, method) ((  void (*) (ThrowObserver_1_t3210247653 *, Unit_t2558286038 , const MethodInfo*))ThrowObserver_1_OnNext_m4039502139_gshared)(__this, ___value0, method)
