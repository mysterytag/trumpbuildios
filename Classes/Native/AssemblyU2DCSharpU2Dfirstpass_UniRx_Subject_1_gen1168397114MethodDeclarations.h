﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.Vector4>
struct Subject_1_t1168397114;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector4>
struct IObserver_1_t1442361397;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void UniRx.Subject`1<UnityEngine.Vector4>::.ctor()
extern "C"  void Subject_1__ctor_m86266420_gshared (Subject_1_t1168397114 * __this, const MethodInfo* method);
#define Subject_1__ctor_m86266420(__this, method) ((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))Subject_1__ctor_m86266420_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Vector4>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2616716760_gshared (Subject_1_t1168397114 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m2616716760(__this, method) ((  bool (*) (Subject_1_t1168397114 *, const MethodInfo*))Subject_1_get_HasObservers_m2616716760_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector4>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m2295205566_gshared (Subject_1_t1168397114 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m2295205566(__this, method) ((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))Subject_1_OnCompleted_m2295205566_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector4>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m269652715_gshared (Subject_1_t1168397114 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m269652715(__this, ___error0, method) ((  void (*) (Subject_1_t1168397114 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m269652715_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector4>::OnNext(T)
extern "C"  void Subject_1_OnNext_m4128354268_gshared (Subject_1_t1168397114 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m4128354268(__this, ___value0, method) ((  void (*) (Subject_1_t1168397114 *, Vector4_t3525329790 , const MethodInfo*))Subject_1_OnNext_m4128354268_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Vector4>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m3893596275_gshared (Subject_1_t1168397114 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m3893596275(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1168397114 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m3893596275_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector4>::Dispose()
extern "C"  void Subject_1_Dispose_m1195934065_gshared (Subject_1_t1168397114 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1195934065(__this, method) ((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))Subject_1_Dispose_m1195934065_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Vector4>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m2089429498_gshared (Subject_1_t1168397114 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m2089429498(__this, method) ((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m2089429498_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Vector4>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1164285135_gshared (Subject_1_t1168397114 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1164285135(__this, method) ((  bool (*) (Subject_1_t1168397114 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1164285135_gshared)(__this, method)
