﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Installer
struct Installer_t3915807453;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.Installer::.ctor()
extern "C"  void Installer__ctor_m4279559010 (Installer_t3915807453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.DiContainer Zenject.Installer::get_Container()
extern "C"  DiContainer_t2383114449 * Installer_get_Container_m3289643822 (Installer_t3915807453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.Installer::get_IsEnabled()
extern "C"  bool Installer_get_IsEnabled_m3825674434 (Installer_t3915807453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
