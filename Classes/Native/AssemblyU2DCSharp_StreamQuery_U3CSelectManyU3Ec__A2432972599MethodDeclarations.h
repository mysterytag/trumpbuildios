﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey13E_3__ctor_m2415025516_gshared (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey13E_3__ctor_m2415025516(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey13E_3__ctor_m2415025516_gshared)(__this, method)
// TR StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3<System.Object,System.Object,System.Object>::<>m__1D3(TC)
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey13E_3_U3CU3Em__1D3_m1289479197_gshared (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * __this, Il2CppObject * ___y0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey13E_3_U3CU3Em__1D3_m1289479197(__this, ___y0, method) ((  Il2CppObject * (*) (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey13E_3_U3CU3Em__1D3_m1289479197_gshared)(__this, ___y0, method)
