﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Boolean[]
struct BooleanU5BU5D_t3804927312;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.NthZipLatestObserverBase`1<System.Object>
struct  NthZipLatestObserverBase_1_t3084363628  : public OperatorObserverBase_2_t1187768149
{
public:
	// System.Int32 UniRx.Operators.NthZipLatestObserverBase`1::length
	int32_t ___length_2;
	// System.Boolean[] UniRx.Operators.NthZipLatestObserverBase`1::isStarted
	BooleanU5BU5D_t3804927312* ___isStarted_3;
	// System.Boolean[] UniRx.Operators.NthZipLatestObserverBase`1::isCompleted
	BooleanU5BU5D_t3804927312* ___isCompleted_4;

public:
	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(NthZipLatestObserverBase_1_t3084363628, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}

	inline static int32_t get_offset_of_isStarted_3() { return static_cast<int32_t>(offsetof(NthZipLatestObserverBase_1_t3084363628, ___isStarted_3)); }
	inline BooleanU5BU5D_t3804927312* get_isStarted_3() const { return ___isStarted_3; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isStarted_3() { return &___isStarted_3; }
	inline void set_isStarted_3(BooleanU5BU5D_t3804927312* value)
	{
		___isStarted_3 = value;
		Il2CppCodeGenWriteBarrier(&___isStarted_3, value);
	}

	inline static int32_t get_offset_of_isCompleted_4() { return static_cast<int32_t>(offsetof(NthZipLatestObserverBase_1_t3084363628, ___isCompleted_4)); }
	inline BooleanU5BU5D_t3804927312* get_isCompleted_4() const { return ___isCompleted_4; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isCompleted_4() { return &___isCompleted_4; }
	inline void set_isCompleted_4(BooleanU5BU5D_t3804927312* value)
	{
		___isCompleted_4 = value;
		Il2CppCodeGenWriteBarrier(&___isCompleted_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
