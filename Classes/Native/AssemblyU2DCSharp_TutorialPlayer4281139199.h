﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<TutorialTask>
struct List_1_t1570237212;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// TableButtons
struct TableButtons_t1868573683;
// Message
struct Message_t2619578343;
// OligarchParameters
struct OligarchParameters_t821144443;
// BottomButton
struct BottomButton_t1932555613;
// GuiController
struct GuiController_t1495593751;
// Tacticsoft.TableView
struct TableView_t692333993;
// GlobalStat
struct GlobalStat_t1134678199;
// GameController
struct GameController_t2782302542;
// TutorchikPanel
struct TutorchikPanel_t3935308711;
// System.Action
struct Action_t437523947;
// System.Func`2<BottomButton,UniRx.IObservable`1<UniRx.Unit>>
struct Func_2_t3563031945;
// System.Func`1<System.Boolean>
struct Func_1_t1353786588;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPlayer
struct  TutorialPlayer_t4281139199  : public MonoBehaviour_t3012272455
{
public:
	// System.Collections.Generic.List`1<TutorialTask> TutorialPlayer::tasks
	List_1_t1570237212 * ___tasks_2;
	// UniRx.ReactiveProperty`1<System.Boolean> TutorialPlayer::Playing
	ReactiveProperty_1_t819338379 * ___Playing_3;
	// System.Boolean TutorialPlayer::waitforcombo
	bool ___waitforcombo_4;
	// System.Boolean TutorialPlayer::blockRaycast
	bool ___blockRaycast_5;
	// TableButtons TutorialPlayer::TableButtons
	TableButtons_t1868573683 * ___TableButtons_6;
	// System.DateTime TutorialPlayer::lastStep
	DateTime_t339033936  ___lastStep_7;
	// UniRx.ReactiveProperty`1<System.Boolean> TutorialPlayer::showMenu
	ReactiveProperty_1_t819338379 * ___showMenu_8;
	// Message TutorialPlayer::Message
	Message_t2619578343 * ___Message_9;
	// OligarchParameters TutorialPlayer::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_10;
	// BottomButton TutorialPlayer::CloseTableButton
	BottomButton_t1932555613 * ___CloseTableButton_11;
	// GuiController TutorialPlayer::GuiController
	GuiController_t1495593751 * ___GuiController_12;
	// BottomButton TutorialPlayer::CaseButton
	BottomButton_t1932555613 * ___CaseButton_13;
	// BottomButton TutorialPlayer::AbacusButton
	BottomButton_t1932555613 * ___AbacusButton_14;
	// BottomButton TutorialPlayer::SafeButton
	BottomButton_t1932555613 * ___SafeButton_15;
	// BottomButton TutorialPlayer::SettingsButton
	BottomButton_t1932555613 * ___SettingsButton_16;
	// BottomButton TutorialPlayer::ToiletButton
	BottomButton_t1932555613 * ___ToiletButton_17;
	// Tacticsoft.TableView TutorialPlayer::TableView
	TableView_t692333993 * ___TableView_18;
	// GlobalStat TutorialPlayer::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_19;
	// System.Boolean TutorialPlayer::oneTimeAction
	bool ___oneTimeAction_20;
	// GameController TutorialPlayer::GameController
	GameController_t2782302542 * ___GameController_21;
	// TutorchikPanel TutorialPlayer::TutorchikPanel
	TutorchikPanel_t3935308711 * ___TutorchikPanel_22;

public:
	inline static int32_t get_offset_of_tasks_2() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___tasks_2)); }
	inline List_1_t1570237212 * get_tasks_2() const { return ___tasks_2; }
	inline List_1_t1570237212 ** get_address_of_tasks_2() { return &___tasks_2; }
	inline void set_tasks_2(List_1_t1570237212 * value)
	{
		___tasks_2 = value;
		Il2CppCodeGenWriteBarrier(&___tasks_2, value);
	}

	inline static int32_t get_offset_of_Playing_3() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___Playing_3)); }
	inline ReactiveProperty_1_t819338379 * get_Playing_3() const { return ___Playing_3; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_Playing_3() { return &___Playing_3; }
	inline void set_Playing_3(ReactiveProperty_1_t819338379 * value)
	{
		___Playing_3 = value;
		Il2CppCodeGenWriteBarrier(&___Playing_3, value);
	}

	inline static int32_t get_offset_of_waitforcombo_4() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___waitforcombo_4)); }
	inline bool get_waitforcombo_4() const { return ___waitforcombo_4; }
	inline bool* get_address_of_waitforcombo_4() { return &___waitforcombo_4; }
	inline void set_waitforcombo_4(bool value)
	{
		___waitforcombo_4 = value;
	}

	inline static int32_t get_offset_of_blockRaycast_5() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___blockRaycast_5)); }
	inline bool get_blockRaycast_5() const { return ___blockRaycast_5; }
	inline bool* get_address_of_blockRaycast_5() { return &___blockRaycast_5; }
	inline void set_blockRaycast_5(bool value)
	{
		___blockRaycast_5 = value;
	}

	inline static int32_t get_offset_of_TableButtons_6() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___TableButtons_6)); }
	inline TableButtons_t1868573683 * get_TableButtons_6() const { return ___TableButtons_6; }
	inline TableButtons_t1868573683 ** get_address_of_TableButtons_6() { return &___TableButtons_6; }
	inline void set_TableButtons_6(TableButtons_t1868573683 * value)
	{
		___TableButtons_6 = value;
		Il2CppCodeGenWriteBarrier(&___TableButtons_6, value);
	}

	inline static int32_t get_offset_of_lastStep_7() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___lastStep_7)); }
	inline DateTime_t339033936  get_lastStep_7() const { return ___lastStep_7; }
	inline DateTime_t339033936 * get_address_of_lastStep_7() { return &___lastStep_7; }
	inline void set_lastStep_7(DateTime_t339033936  value)
	{
		___lastStep_7 = value;
	}

	inline static int32_t get_offset_of_showMenu_8() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___showMenu_8)); }
	inline ReactiveProperty_1_t819338379 * get_showMenu_8() const { return ___showMenu_8; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_showMenu_8() { return &___showMenu_8; }
	inline void set_showMenu_8(ReactiveProperty_1_t819338379 * value)
	{
		___showMenu_8 = value;
		Il2CppCodeGenWriteBarrier(&___showMenu_8, value);
	}

	inline static int32_t get_offset_of_Message_9() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___Message_9)); }
	inline Message_t2619578343 * get_Message_9() const { return ___Message_9; }
	inline Message_t2619578343 ** get_address_of_Message_9() { return &___Message_9; }
	inline void set_Message_9(Message_t2619578343 * value)
	{
		___Message_9 = value;
		Il2CppCodeGenWriteBarrier(&___Message_9, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_10() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___OligarchParameters_10)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_10() const { return ___OligarchParameters_10; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_10() { return &___OligarchParameters_10; }
	inline void set_OligarchParameters_10(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_10 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_10, value);
	}

	inline static int32_t get_offset_of_CloseTableButton_11() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___CloseTableButton_11)); }
	inline BottomButton_t1932555613 * get_CloseTableButton_11() const { return ___CloseTableButton_11; }
	inline BottomButton_t1932555613 ** get_address_of_CloseTableButton_11() { return &___CloseTableButton_11; }
	inline void set_CloseTableButton_11(BottomButton_t1932555613 * value)
	{
		___CloseTableButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___CloseTableButton_11, value);
	}

	inline static int32_t get_offset_of_GuiController_12() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___GuiController_12)); }
	inline GuiController_t1495593751 * get_GuiController_12() const { return ___GuiController_12; }
	inline GuiController_t1495593751 ** get_address_of_GuiController_12() { return &___GuiController_12; }
	inline void set_GuiController_12(GuiController_t1495593751 * value)
	{
		___GuiController_12 = value;
		Il2CppCodeGenWriteBarrier(&___GuiController_12, value);
	}

	inline static int32_t get_offset_of_CaseButton_13() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___CaseButton_13)); }
	inline BottomButton_t1932555613 * get_CaseButton_13() const { return ___CaseButton_13; }
	inline BottomButton_t1932555613 ** get_address_of_CaseButton_13() { return &___CaseButton_13; }
	inline void set_CaseButton_13(BottomButton_t1932555613 * value)
	{
		___CaseButton_13 = value;
		Il2CppCodeGenWriteBarrier(&___CaseButton_13, value);
	}

	inline static int32_t get_offset_of_AbacusButton_14() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___AbacusButton_14)); }
	inline BottomButton_t1932555613 * get_AbacusButton_14() const { return ___AbacusButton_14; }
	inline BottomButton_t1932555613 ** get_address_of_AbacusButton_14() { return &___AbacusButton_14; }
	inline void set_AbacusButton_14(BottomButton_t1932555613 * value)
	{
		___AbacusButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___AbacusButton_14, value);
	}

	inline static int32_t get_offset_of_SafeButton_15() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___SafeButton_15)); }
	inline BottomButton_t1932555613 * get_SafeButton_15() const { return ___SafeButton_15; }
	inline BottomButton_t1932555613 ** get_address_of_SafeButton_15() { return &___SafeButton_15; }
	inline void set_SafeButton_15(BottomButton_t1932555613 * value)
	{
		___SafeButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___SafeButton_15, value);
	}

	inline static int32_t get_offset_of_SettingsButton_16() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___SettingsButton_16)); }
	inline BottomButton_t1932555613 * get_SettingsButton_16() const { return ___SettingsButton_16; }
	inline BottomButton_t1932555613 ** get_address_of_SettingsButton_16() { return &___SettingsButton_16; }
	inline void set_SettingsButton_16(BottomButton_t1932555613 * value)
	{
		___SettingsButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___SettingsButton_16, value);
	}

	inline static int32_t get_offset_of_ToiletButton_17() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___ToiletButton_17)); }
	inline BottomButton_t1932555613 * get_ToiletButton_17() const { return ___ToiletButton_17; }
	inline BottomButton_t1932555613 ** get_address_of_ToiletButton_17() { return &___ToiletButton_17; }
	inline void set_ToiletButton_17(BottomButton_t1932555613 * value)
	{
		___ToiletButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___ToiletButton_17, value);
	}

	inline static int32_t get_offset_of_TableView_18() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___TableView_18)); }
	inline TableView_t692333993 * get_TableView_18() const { return ___TableView_18; }
	inline TableView_t692333993 ** get_address_of_TableView_18() { return &___TableView_18; }
	inline void set_TableView_18(TableView_t692333993 * value)
	{
		___TableView_18 = value;
		Il2CppCodeGenWriteBarrier(&___TableView_18, value);
	}

	inline static int32_t get_offset_of_GlobalStat_19() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___GlobalStat_19)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_19() const { return ___GlobalStat_19; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_19() { return &___GlobalStat_19; }
	inline void set_GlobalStat_19(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_19 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_19, value);
	}

	inline static int32_t get_offset_of_oneTimeAction_20() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___oneTimeAction_20)); }
	inline bool get_oneTimeAction_20() const { return ___oneTimeAction_20; }
	inline bool* get_address_of_oneTimeAction_20() { return &___oneTimeAction_20; }
	inline void set_oneTimeAction_20(bool value)
	{
		___oneTimeAction_20 = value;
	}

	inline static int32_t get_offset_of_GameController_21() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___GameController_21)); }
	inline GameController_t2782302542 * get_GameController_21() const { return ___GameController_21; }
	inline GameController_t2782302542 ** get_address_of_GameController_21() { return &___GameController_21; }
	inline void set_GameController_21(GameController_t2782302542 * value)
	{
		___GameController_21 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_21, value);
	}

	inline static int32_t get_offset_of_TutorchikPanel_22() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199, ___TutorchikPanel_22)); }
	inline TutorchikPanel_t3935308711 * get_TutorchikPanel_22() const { return ___TutorchikPanel_22; }
	inline TutorchikPanel_t3935308711 ** get_address_of_TutorchikPanel_22() { return &___TutorchikPanel_22; }
	inline void set_TutorchikPanel_22(TutorchikPanel_t3935308711 * value)
	{
		___TutorchikPanel_22 = value;
		Il2CppCodeGenWriteBarrier(&___TutorchikPanel_22, value);
	}
};

struct TutorialPlayer_t4281139199_StaticFields
{
public:
	// System.Action TutorialPlayer::<>f__am$cache15
	Action_t437523947 * ___U3CU3Ef__amU24cache15_23;
	// System.Func`2<BottomButton,UniRx.IObservable`1<UniRx.Unit>> TutorialPlayer::<>f__am$cache16
	Func_2_t3563031945 * ___U3CU3Ef__amU24cache16_24;
	// System.Func`1<System.Boolean> TutorialPlayer::<>f__am$cache17
	Func_1_t1353786588 * ___U3CU3Ef__amU24cache17_25;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_23() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199_StaticFields, ___U3CU3Ef__amU24cache15_23)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache15_23() const { return ___U3CU3Ef__amU24cache15_23; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache15_23() { return &___U3CU3Ef__amU24cache15_23; }
	inline void set_U3CU3Ef__amU24cache15_23(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache15_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_24() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199_StaticFields, ___U3CU3Ef__amU24cache16_24)); }
	inline Func_2_t3563031945 * get_U3CU3Ef__amU24cache16_24() const { return ___U3CU3Ef__amU24cache16_24; }
	inline Func_2_t3563031945 ** get_address_of_U3CU3Ef__amU24cache16_24() { return &___U3CU3Ef__amU24cache16_24; }
	inline void set_U3CU3Ef__amU24cache16_24(Func_2_t3563031945 * value)
	{
		___U3CU3Ef__amU24cache16_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_25() { return static_cast<int32_t>(offsetof(TutorialPlayer_t4281139199_StaticFields, ___U3CU3Ef__amU24cache17_25)); }
	inline Func_1_t1353786588 * get_U3CU3Ef__amU24cache17_25() const { return ___U3CU3Ef__amU24cache17_25; }
	inline Func_1_t1353786588 ** get_address_of_U3CU3Ef__amU24cache17_25() { return &___U3CU3Ef__amU24cache17_25; }
	inline void set_U3CU3Ef__amU24cache17_25(Func_1_t1353786588 * value)
	{
		___U3CU3Ef__amU24cache17_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
