﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserTitleInfo
struct UserTitleInfo_t4099117963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen417810504.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.UserTitleInfo::.ctor()
extern "C"  void UserTitleInfo__ctor_m4084743678 (UserTitleInfo_t4099117963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserTitleInfo::get_DisplayName()
extern "C"  String_t* UserTitleInfo_get_DisplayName_m2660467997 (UserTitleInfo_t4099117963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserTitleInfo::set_DisplayName(System.String)
extern "C"  void UserTitleInfo_set_DisplayName_m246609686 (UserTitleInfo_t4099117963 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.UserOrigination> PlayFab.ClientModels.UserTitleInfo::get_Origination()
extern "C"  Nullable_1_t417810504  UserTitleInfo_get_Origination_m3577128636 (UserTitleInfo_t4099117963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserTitleInfo::set_Origination(System.Nullable`1<PlayFab.ClientModels.UserOrigination>)
extern "C"  void UserTitleInfo_set_Origination_m1753918615 (UserTitleInfo_t4099117963 * __this, Nullable_1_t417810504  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayFab.ClientModels.UserTitleInfo::get_Created()
extern "C"  DateTime_t339033936  UserTitleInfo_get_Created_m3907603042 (UserTitleInfo_t4099117963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserTitleInfo::set_Created(System.DateTime)
extern "C"  void UserTitleInfo_set_Created_m2931304241 (UserTitleInfo_t4099117963 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.UserTitleInfo::get_LastLogin()
extern "C"  Nullable_1_t3225071844  UserTitleInfo_get_LastLogin_m4247954584 (UserTitleInfo_t4099117963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserTitleInfo::set_LastLogin(System.Nullable`1<System.DateTime>)
extern "C"  void UserTitleInfo_set_LastLogin_m2413695427 (UserTitleInfo_t4099117963 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.UserTitleInfo::get_FirstLogin()
extern "C"  Nullable_1_t3225071844  UserTitleInfo_get_FirstLogin_m2798519286 (UserTitleInfo_t4099117963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserTitleInfo::set_FirstLogin(System.Nullable`1<System.DateTime>)
extern "C"  void UserTitleInfo_set_FirstLogin_m4221913133 (UserTitleInfo_t4099117963 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.UserTitleInfo::get_isBanned()
extern "C"  Nullable_1_t3097043249  UserTitleInfo_get_isBanned_m1036814304 (UserTitleInfo_t4099117963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserTitleInfo::set_isBanned(System.Nullable`1<System.Boolean>)
extern "C"  void UserTitleInfo_set_isBanned_m3652586557 (UserTitleInfo_t4099117963 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
