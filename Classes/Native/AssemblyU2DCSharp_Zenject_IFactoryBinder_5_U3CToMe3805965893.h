﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ModestTree.Util.Func`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_6_t3355333698;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IFactoryBinder`5/<ToMethod>c__AnonStorey17C<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  U3CToMethodU3Ec__AnonStorey17C_t3805965893  : public Il2CppObject
{
public:
	// ModestTree.Util.Func`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5/<ToMethod>c__AnonStorey17C::method
	Func_6_t3355333698 * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CToMethodU3Ec__AnonStorey17C_t3805965893, ___method_0)); }
	inline Func_6_t3355333698 * get_method_0() const { return ___method_0; }
	inline Func_6_t3355333698 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Func_6_t3355333698 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
