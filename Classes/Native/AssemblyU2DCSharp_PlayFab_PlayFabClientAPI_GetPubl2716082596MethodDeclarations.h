﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPublisherDataRequestCallback
struct GetPublisherDataRequestCallback_t2716082596;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPublisherDataRequest
struct GetPublisherDataRequest_t1266178127;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPublishe1266178127.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPublisherDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPublisherDataRequestCallback__ctor_m3859328051 (GetPublisherDataRequestCallback_t2716082596 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPublisherDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPublisherDataRequest,System.Object)
extern "C"  void GetPublisherDataRequestCallback_Invoke_m2831659857 (GetPublisherDataRequestCallback_t2716082596 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPublisherDataRequest_t1266178127 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPublisherDataRequestCallback_t2716082596(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPublisherDataRequest_t1266178127 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPublisherDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPublisherDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPublisherDataRequestCallback_BeginInvoke_m1027725662 (GetPublisherDataRequestCallback_t2716082596 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPublisherDataRequest_t1266178127 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPublisherDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPublisherDataRequestCallback_EndInvoke_m3878170563 (GetPublisherDataRequestCallback_t2716082596 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
