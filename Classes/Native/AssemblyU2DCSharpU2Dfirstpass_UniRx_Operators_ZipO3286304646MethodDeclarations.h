﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObserver`1<System.Object>
struct ZipObserver_1_t3286304646;
// System.Object
struct Il2CppObject;
// UniRx.Operators.IZipObservable
struct IZipObservable_t4005421640;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2545193960;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObserver`1<System.Object>::.ctor(System.Object,UniRx.Operators.IZipObservable,System.Int32,System.Collections.Generic.Queue`1<T>)
extern "C"  void ZipObserver_1__ctor_m1125050348_gshared (ZipObserver_1_t3286304646 * __this, Il2CppObject * ___gate0, Il2CppObject * ___parent1, int32_t ___index2, Queue_1_t2545193960 * ___queue3, const MethodInfo* method);
#define ZipObserver_1__ctor_m1125050348(__this, ___gate0, ___parent1, ___index2, ___queue3, method) ((  void (*) (ZipObserver_1_t3286304646 *, Il2CppObject *, Il2CppObject *, int32_t, Queue_1_t2545193960 *, const MethodInfo*))ZipObserver_1__ctor_m1125050348_gshared)(__this, ___gate0, ___parent1, ___index2, ___queue3, method)
// System.Void UniRx.Operators.ZipObserver`1<System.Object>::OnNext(T)
extern "C"  void ZipObserver_1_OnNext_m4288721644_gshared (ZipObserver_1_t3286304646 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ZipObserver_1_OnNext_m4288721644(__this, ___value0, method) ((  void (*) (ZipObserver_1_t3286304646 *, Il2CppObject *, const MethodInfo*))ZipObserver_1_OnNext_m4288721644_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void ZipObserver_1_OnError_m601454075_gshared (ZipObserver_1_t3286304646 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ZipObserver_1_OnError_m601454075(__this, ___error0, method) ((  void (*) (ZipObserver_1_t3286304646 *, Exception_t1967233988 *, const MethodInfo*))ZipObserver_1_OnError_m601454075_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipObserver`1<System.Object>::OnCompleted()
extern "C"  void ZipObserver_1_OnCompleted_m1577388494_gshared (ZipObserver_1_t3286304646 * __this, const MethodInfo* method);
#define ZipObserver_1_OnCompleted_m1577388494(__this, method) ((  void (*) (ZipObserver_1_t3286304646 *, const MethodInfo*))ZipObserver_1_OnCompleted_m1577388494_gshared)(__this, method)
