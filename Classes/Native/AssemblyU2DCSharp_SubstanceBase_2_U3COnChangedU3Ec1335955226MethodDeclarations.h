﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Boolean,System.Boolean>
struct U3COnChangedU3Ec__AnonStorey142_t1335955226;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m3102795806_gshared (U3COnChangedU3Ec__AnonStorey142_t1335955226 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142__ctor_m3102795806(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t1335955226 *, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142__ctor_m3102795806_gshared)(__this, method)
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Boolean,System.Boolean>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m4122085086_gshared (U3COnChangedU3Ec__AnonStorey142_t1335955226 * __this, bool ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m4122085086(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t1335955226 *, bool, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m4122085086_gshared)(__this, ____0, method)
