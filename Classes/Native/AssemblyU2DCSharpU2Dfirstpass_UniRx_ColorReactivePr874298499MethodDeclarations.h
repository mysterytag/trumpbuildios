﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ColorReactiveProperty
struct ColorReactiveProperty_t874298499;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color>
struct IEqualityComparer_1_t3912442411;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.ColorReactiveProperty::.ctor()
extern "C"  void ColorReactiveProperty__ctor_m3247093694 (ColorReactiveProperty_t874298499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ColorReactiveProperty::.ctor(UnityEngine.Color)
extern "C"  void ColorReactiveProperty__ctor_m1036944936 (ColorReactiveProperty_t874298499 * __this, Color_t1588175760  ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color> UniRx.ColorReactiveProperty::get_EqualityComparer()
extern "C"  Il2CppObject* ColorReactiveProperty_get_EqualityComparer_m2916775839 (ColorReactiveProperty_t874298499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
