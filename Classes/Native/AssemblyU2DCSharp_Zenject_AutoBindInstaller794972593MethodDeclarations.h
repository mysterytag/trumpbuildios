﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.AutoBindInstaller
struct AutoBindInstaller_t794972593;
// Zenject.CompositionRoot
struct CompositionRoot_t1939928321;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_CompositionRoot1939928321.h"

// System.Void Zenject.AutoBindInstaller::.ctor()
extern "C"  void AutoBindInstaller__ctor_m3366536206 (AutoBindInstaller_t794972593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.AutoBindInstaller::InstallBindings()
extern "C"  void AutoBindInstaller_InstallBindings_m3984745077 (AutoBindInstaller_t794972593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.AutoBindInstaller::Initialize(Zenject.CompositionRoot)
extern "C"  void AutoBindInstaller_Initialize_m1372391551 (AutoBindInstaller_t794972593 * __this, CompositionRoot_t1939928321 * ___compRoot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
