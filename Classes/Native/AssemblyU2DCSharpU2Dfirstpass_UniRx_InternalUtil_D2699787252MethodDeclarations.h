﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>
struct DisposedObserver_1_t2699787252;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m103793650_gshared (DisposedObserver_1_t2699787252 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m103793650(__this, method) ((  void (*) (DisposedObserver_1_t2699787252 *, const MethodInfo*))DisposedObserver_1__ctor_m103793650_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2735506939_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m2735506939(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m2735506939_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m950057468_gshared (DisposedObserver_1_t2699787252 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m950057468(__this, method) ((  void (*) (DisposedObserver_1_t2699787252 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m950057468_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m3724038441_gshared (DisposedObserver_1_t2699787252 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m3724038441(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2699787252 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m3724038441_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector4>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m3792153114_gshared (DisposedObserver_1_t2699787252 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m3792153114(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2699787252 *, Vector4_t3525329790 , const MethodInfo*))DisposedObserver_1_OnNext_m3792153114_gshared)(__this, ___value0, method)
