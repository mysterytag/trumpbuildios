﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Object,System.Object>
struct SelectManyObserverWithIndex_t843501612;
// UniRx.Operators.SelectManyObservable`3<System.Object,System.Object,System.Object>
struct SelectManyObservable_3_t3068991;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyObserverWithIndex__ctor_m2883735982_gshared (SelectManyObserverWithIndex_t843501612 * __this, SelectManyObservable_3_t3068991 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyObserverWithIndex__ctor_m2883735982(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyObserverWithIndex_t843501612 *, SelectManyObservable_3_t3068991 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex__ctor_m2883735982_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyObserverWithIndex_Run_m2634766935_gshared (SelectManyObserverWithIndex_t843501612 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_Run_m2634766935(__this, method) ((  Il2CppObject * (*) (SelectManyObserverWithIndex_t843501612 *, const MethodInfo*))SelectManyObserverWithIndex_Run_m2634766935_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Object,System.Object>::OnNext(TSource)
extern "C"  void SelectManyObserverWithIndex_OnNext_m3600890176_gshared (SelectManyObserverWithIndex_t843501612 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnNext_m3600890176(__this, ___value0, method) ((  void (*) (SelectManyObserverWithIndex_t843501612 *, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex_OnNext_m3600890176_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyObserverWithIndex_OnError_m51603626_gshared (SelectManyObserverWithIndex_t843501612 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnError_m51603626(__this, ___error0, method) ((  void (*) (SelectManyObserverWithIndex_t843501612 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserverWithIndex_OnError_m51603626_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void SelectManyObserverWithIndex_OnCompleted_m235555581_gshared (SelectManyObserverWithIndex_t843501612 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnCompleted_m235555581(__this, method) ((  void (*) (SelectManyObserverWithIndex_t843501612 *, const MethodInfo*))SelectManyObserverWithIndex_OnCompleted_m235555581_gshared)(__this, method)
