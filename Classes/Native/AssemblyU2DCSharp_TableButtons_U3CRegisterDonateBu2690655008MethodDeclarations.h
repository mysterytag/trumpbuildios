﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey16A
struct U3CRegisterDonateButtonsU3Ec__AnonStorey16A_t2690655008;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey16A::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey16A__ctor_m3506821166 (U3CRegisterDonateButtonsU3Ec__AnonStorey16A_t2690655008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey16A::<>m__275(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey16A_U3CU3Em__275_m1315948415 (U3CRegisterDonateButtonsU3Ec__AnonStorey16A_t2690655008 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey16A::<>m__276(System.Boolean)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey16A_U3CU3Em__276_m4212686867 (U3CRegisterDonateButtonsU3Ec__AnonStorey16A_t2690655008 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
