﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Component
struct Component_t2126946602;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoBehaviourSingletonProvider
struct  MonoBehaviourSingletonProvider_t3595575479  : public ProviderBase_t1627494391
{
public:
	// UnityEngine.Component Zenject.MonoBehaviourSingletonProvider::_instance
	Component_t2126946602 * ____instance_2;
	// Zenject.DiContainer Zenject.MonoBehaviourSingletonProvider::_container
	DiContainer_t2383114449 * ____container_3;
	// System.Type Zenject.MonoBehaviourSingletonProvider::_componentType
	Type_t * ____componentType_4;
	// UnityEngine.GameObject Zenject.MonoBehaviourSingletonProvider::_gameObject
	GameObject_t4012695102 * ____gameObject_5;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(MonoBehaviourSingletonProvider_t3595575479, ____instance_2)); }
	inline Component_t2126946602 * get__instance_2() const { return ____instance_2; }
	inline Component_t2126946602 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(Component_t2126946602 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}

	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(MonoBehaviourSingletonProvider_t3595575479, ____container_3)); }
	inline DiContainer_t2383114449 * get__container_3() const { return ____container_3; }
	inline DiContainer_t2383114449 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t2383114449 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier(&____container_3, value);
	}

	inline static int32_t get_offset_of__componentType_4() { return static_cast<int32_t>(offsetof(MonoBehaviourSingletonProvider_t3595575479, ____componentType_4)); }
	inline Type_t * get__componentType_4() const { return ____componentType_4; }
	inline Type_t ** get_address_of__componentType_4() { return &____componentType_4; }
	inline void set__componentType_4(Type_t * value)
	{
		____componentType_4 = value;
		Il2CppCodeGenWriteBarrier(&____componentType_4, value);
	}

	inline static int32_t get_offset_of__gameObject_5() { return static_cast<int32_t>(offsetof(MonoBehaviourSingletonProvider_t3595575479, ____gameObject_5)); }
	inline GameObject_t4012695102 * get__gameObject_5() const { return ____gameObject_5; }
	inline GameObject_t4012695102 ** get_address_of__gameObject_5() { return &____gameObject_5; }
	inline void set__gameObject_5(GameObject_t4012695102 * value)
	{
		____gameObject_5 = value;
		Il2CppCodeGenWriteBarrier(&____gameObject_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
