﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.Color>
struct Subject_1_t3526210380;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.Subject`1<UnityEngine.Color>::.ctor()
extern "C"  void Subject_1__ctor_m2358557510_gshared (Subject_1_t3526210380 * __this, const MethodInfo* method);
#define Subject_1__ctor_m2358557510(__this, method) ((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))Subject_1__ctor_m2358557510_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Color>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3725802630_gshared (Subject_1_t3526210380 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3725802630(__this, method) ((  bool (*) (Subject_1_t3526210380 *, const MethodInfo*))Subject_1_get_HasObservers_m3725802630_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Color>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3803963984_gshared (Subject_1_t3526210380 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3803963984(__this, method) ((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))Subject_1_OnCompleted_m3803963984_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m4086668157_gshared (Subject_1_t3526210380 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m4086668157(__this, ___error0, method) ((  void (*) (Subject_1_t3526210380 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m4086668157_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Color>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1661738094_gshared (Subject_1_t3526210380 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1661738094(__this, ___value0, method) ((  void (*) (Subject_1_t3526210380 *, Color_t1588175760 , const MethodInfo*))Subject_1_OnNext_m1661738094_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m905218245_gshared (Subject_1_t3526210380 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m905218245(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3526210380 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m905218245_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Color>::Dispose()
extern "C"  void Subject_1_Dispose_m3024285187_gshared (Subject_1_t3526210380 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m3024285187(__this, method) ((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))Subject_1_Dispose_m3024285187_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Color>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m3177178252_gshared (Subject_1_t3526210380 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m3177178252(__this, method) ((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3177178252_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Color>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m173528061_gshared (Subject_1_t3526210380 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m173528061(__this, method) ((  bool (*) (Subject_1_t3526210380 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m173528061_gshared)(__this, method)
