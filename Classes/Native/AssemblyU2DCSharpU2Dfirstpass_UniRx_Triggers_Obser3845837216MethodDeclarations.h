﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo
struct OnStateInfo_t3845837216;
// UnityEngine.Animator
struct Animator_t792326996;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo4162640357.h"

// System.Void UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::.ctor(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void OnStateInfo__ctor_m3587254640 (OnStateInfo_t3845837216 * __this, Animator_t792326996 * ___animator0, AnimatorStateInfo_t4162640357  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Animator UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::get_Animator()
extern "C"  Animator_t792326996 * OnStateInfo_get_Animator_m2094975873 (OnStateInfo_t3845837216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::set_Animator(UnityEngine.Animator)
extern "C"  void OnStateInfo_set_Animator_m4278465616 (OnStateInfo_t3845837216 * __this, Animator_t792326996 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::get_StateInfo()
extern "C"  AnimatorStateInfo_t4162640357  OnStateInfo_get_StateInfo_m2192565628 (OnStateInfo_t3845837216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::set_StateInfo(UnityEngine.AnimatorStateInfo)
extern "C"  void OnStateInfo_set_StateInfo_m1455701463 (OnStateInfo_t3845837216 * __this, AnimatorStateInfo_t4162640357  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::get_LayerIndex()
extern "C"  int32_t OnStateInfo_get_LayerIndex_m225814858 (OnStateInfo_t3845837216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::set_LayerIndex(System.Int32)
extern "C"  void OnStateInfo_set_LayerIndex_m4181489305 (OnStateInfo_t3845837216 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
