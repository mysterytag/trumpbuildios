﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3883920346MethodDeclarations.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2242860130(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t2662322769 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m252712190_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::Invoke(T)
#define Predicate_1_Invoke_m226116448(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2662322769 *, KeyValuePair_2_t2091358871 , const MethodInfo*))Predicate_1_Invoke_m684566468_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m83755375(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t2662322769 *, KeyValuePair_2_t2091358871 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2552303187_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m3013074036(__this, ___result0, method) ((  bool (*) (Predicate_1_t2662322769 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3276267152_gshared)(__this, ___result0, method)
