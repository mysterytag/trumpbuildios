﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19
struct U3CDelayActionU3Ec__Iterator19_t942204024;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::.ctor()
extern "C"  void U3CDelayActionU3Ec__Iterator19__ctor_m3066819796 (U3CDelayActionU3Ec__Iterator19_t942204024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayActionU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3908133832 (U3CDelayActionU3Ec__Iterator19_t942204024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayActionU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m632551260 (U3CDelayActionU3Ec__Iterator19_t942204024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::MoveNext()
extern "C"  bool U3CDelayActionU3Ec__Iterator19_MoveNext_m1393437296 (U3CDelayActionU3Ec__Iterator19_t942204024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::Dispose()
extern "C"  void U3CDelayActionU3Ec__Iterator19_Dispose_m764541969 (U3CDelayActionU3Ec__Iterator19_t942204024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator19::Reset()
extern "C"  void U3CDelayActionU3Ec__Iterator19_Reset_m713252737 (U3CDelayActionU3Ec__Iterator19_t942204024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
