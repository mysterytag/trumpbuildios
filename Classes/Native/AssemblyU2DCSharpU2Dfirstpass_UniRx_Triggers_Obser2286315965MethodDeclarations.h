﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservablePointerDownTrigger
struct ObservablePointerDownTrigger_t2286315965;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservablePointerDownTrigger::.ctor()
extern "C"  void ObservablePointerDownTrigger__ctor_m2114426016 (ObservablePointerDownTrigger_t2286315965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerDownTrigger::UnityEngine.EventSystems.IPointerDownHandler.OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservablePointerDownTrigger_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_m1291857249 (ObservablePointerDownTrigger_t2286315965 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerDownTrigger::OnPointerDownAsObservable()
extern "C"  Il2CppObject* ObservablePointerDownTrigger_OnPointerDownAsObservable_m3094311223 (ObservablePointerDownTrigger_t2286315965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservablePointerDownTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservablePointerDownTrigger_RaiseOnCompletedOnDestroy_m2686704601 (ObservablePointerDownTrigger_t2286315965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
