﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.BindingId
struct BindingId_t2965794261;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext
struct  InjectContext_t3456483891  : public Il2CppObject
{
public:
	// System.Type Zenject.InjectContext::ObjectType
	Type_t * ___ObjectType_0;
	// Zenject.InjectContext Zenject.InjectContext::ParentContext
	InjectContext_t3456483891 * ___ParentContext_1;
	// System.Object Zenject.InjectContext::ObjectInstance
	Il2CppObject * ___ObjectInstance_2;
	// System.String Zenject.InjectContext::Identifier
	String_t* ___Identifier_3;
	// System.String Zenject.InjectContext::ConcreteIdentifier
	String_t* ___ConcreteIdentifier_4;
	// System.String Zenject.InjectContext::MemberName
	String_t* ___MemberName_5;
	// System.Type Zenject.InjectContext::MemberType
	Type_t * ___MemberType_6;
	// System.Boolean Zenject.InjectContext::Optional
	bool ___Optional_7;
	// System.Object Zenject.InjectContext::FallBackValue
	Il2CppObject * ___FallBackValue_8;
	// Zenject.DiContainer Zenject.InjectContext::Container
	DiContainer_t2383114449 * ___Container_9;
	// Zenject.BindingId Zenject.InjectContext::BindingId
	BindingId_t2965794261 * ___BindingId_10;

public:
	inline static int32_t get_offset_of_ObjectType_0() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___ObjectType_0)); }
	inline Type_t * get_ObjectType_0() const { return ___ObjectType_0; }
	inline Type_t ** get_address_of_ObjectType_0() { return &___ObjectType_0; }
	inline void set_ObjectType_0(Type_t * value)
	{
		___ObjectType_0 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectType_0, value);
	}

	inline static int32_t get_offset_of_ParentContext_1() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___ParentContext_1)); }
	inline InjectContext_t3456483891 * get_ParentContext_1() const { return ___ParentContext_1; }
	inline InjectContext_t3456483891 ** get_address_of_ParentContext_1() { return &___ParentContext_1; }
	inline void set_ParentContext_1(InjectContext_t3456483891 * value)
	{
		___ParentContext_1 = value;
		Il2CppCodeGenWriteBarrier(&___ParentContext_1, value);
	}

	inline static int32_t get_offset_of_ObjectInstance_2() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___ObjectInstance_2)); }
	inline Il2CppObject * get_ObjectInstance_2() const { return ___ObjectInstance_2; }
	inline Il2CppObject ** get_address_of_ObjectInstance_2() { return &___ObjectInstance_2; }
	inline void set_ObjectInstance_2(Il2CppObject * value)
	{
		___ObjectInstance_2 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectInstance_2, value);
	}

	inline static int32_t get_offset_of_Identifier_3() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___Identifier_3)); }
	inline String_t* get_Identifier_3() const { return ___Identifier_3; }
	inline String_t** get_address_of_Identifier_3() { return &___Identifier_3; }
	inline void set_Identifier_3(String_t* value)
	{
		___Identifier_3 = value;
		Il2CppCodeGenWriteBarrier(&___Identifier_3, value);
	}

	inline static int32_t get_offset_of_ConcreteIdentifier_4() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___ConcreteIdentifier_4)); }
	inline String_t* get_ConcreteIdentifier_4() const { return ___ConcreteIdentifier_4; }
	inline String_t** get_address_of_ConcreteIdentifier_4() { return &___ConcreteIdentifier_4; }
	inline void set_ConcreteIdentifier_4(String_t* value)
	{
		___ConcreteIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier(&___ConcreteIdentifier_4, value);
	}

	inline static int32_t get_offset_of_MemberName_5() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___MemberName_5)); }
	inline String_t* get_MemberName_5() const { return ___MemberName_5; }
	inline String_t** get_address_of_MemberName_5() { return &___MemberName_5; }
	inline void set_MemberName_5(String_t* value)
	{
		___MemberName_5 = value;
		Il2CppCodeGenWriteBarrier(&___MemberName_5, value);
	}

	inline static int32_t get_offset_of_MemberType_6() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___MemberType_6)); }
	inline Type_t * get_MemberType_6() const { return ___MemberType_6; }
	inline Type_t ** get_address_of_MemberType_6() { return &___MemberType_6; }
	inline void set_MemberType_6(Type_t * value)
	{
		___MemberType_6 = value;
		Il2CppCodeGenWriteBarrier(&___MemberType_6, value);
	}

	inline static int32_t get_offset_of_Optional_7() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___Optional_7)); }
	inline bool get_Optional_7() const { return ___Optional_7; }
	inline bool* get_address_of_Optional_7() { return &___Optional_7; }
	inline void set_Optional_7(bool value)
	{
		___Optional_7 = value;
	}

	inline static int32_t get_offset_of_FallBackValue_8() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___FallBackValue_8)); }
	inline Il2CppObject * get_FallBackValue_8() const { return ___FallBackValue_8; }
	inline Il2CppObject ** get_address_of_FallBackValue_8() { return &___FallBackValue_8; }
	inline void set_FallBackValue_8(Il2CppObject * value)
	{
		___FallBackValue_8 = value;
		Il2CppCodeGenWriteBarrier(&___FallBackValue_8, value);
	}

	inline static int32_t get_offset_of_Container_9() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___Container_9)); }
	inline DiContainer_t2383114449 * get_Container_9() const { return ___Container_9; }
	inline DiContainer_t2383114449 ** get_address_of_Container_9() { return &___Container_9; }
	inline void set_Container_9(DiContainer_t2383114449 * value)
	{
		___Container_9 = value;
		Il2CppCodeGenWriteBarrier(&___Container_9, value);
	}

	inline static int32_t get_offset_of_BindingId_10() { return static_cast<int32_t>(offsetof(InjectContext_t3456483891, ___BindingId_10)); }
	inline BindingId_t2965794261 * get_BindingId_10() const { return ___BindingId_10; }
	inline BindingId_t2965794261 ** get_address_of_BindingId_10() { return &___BindingId_10; }
	inline void set_BindingId_10(BindingId_t2965794261 * value)
	{
		___BindingId_10 = value;
		Il2CppCodeGenWriteBarrier(&___BindingId_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
