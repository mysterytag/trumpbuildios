﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableDropTrigger
struct ObservableDropTrigger_t1797561639;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservableDropTrigger::.ctor()
extern "C"  void ObservableDropTrigger__ctor_m1587538788 (ObservableDropTrigger_t1797561639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableDropTrigger::UnityEngine.EventSystems.IDropHandler.OnDrop(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableDropTrigger_UnityEngine_EventSystems_IDropHandler_OnDrop_m2507327489 (ObservableDropTrigger_t1797561639 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableDropTrigger::OnDropAsObservable()
extern "C"  Il2CppObject* ObservableDropTrigger_OnDropAsObservable_m4223212007 (ObservableDropTrigger_t1797561639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableDropTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableDropTrigger_RaiseOnCompletedOnDestroy_m1280387229 (ObservableDropTrigger_t1797561639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
