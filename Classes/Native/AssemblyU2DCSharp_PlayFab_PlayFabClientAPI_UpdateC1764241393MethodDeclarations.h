﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsRequestCallback
struct UpdateCharacterStatisticsRequestCallback_t1764241393;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateCharacterStatisticsRequest
struct UpdateCharacterStatisticsRequest_t1560413084;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara1560413084.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateCharacterStatisticsRequestCallback__ctor_m2519866464 (UpdateCharacterStatisticsRequestCallback_t1764241393 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateCharacterStatisticsRequest,System.Object)
extern "C"  void UpdateCharacterStatisticsRequestCallback_Invoke_m4180005311 (UpdateCharacterStatisticsRequestCallback_t1764241393 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterStatisticsRequest_t1560413084 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateCharacterStatisticsRequestCallback_t1764241393(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterStatisticsRequest_t1560413084 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateCharacterStatisticsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateCharacterStatisticsRequestCallback_BeginInvoke_m2922177672 (UpdateCharacterStatisticsRequestCallback_t1764241393 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterStatisticsRequest_t1560413084 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateCharacterStatisticsRequestCallback_EndInvoke_m2579676784 (UpdateCharacterStatisticsRequestCallback_t1764241393 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
