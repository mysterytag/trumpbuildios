﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TransientProvider
struct TransientProvider_t1859995120;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.TransientProvider::.ctor(Zenject.DiContainer,System.Type)
extern "C"  void TransientProvider__ctor_m154705881 (TransientProvider_t1859995120 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___concreteType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.TransientProvider::GetInstanceType()
extern "C"  Type_t * TransientProvider_GetInstanceType_m3567265676 (TransientProvider_t1859995120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.TransientProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * TransientProvider_GetInstance_m503948774 (TransientProvider_t1859995120 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.TransientProvider::GetTypeToInstantiate(System.Type)
extern "C"  Type_t * TransientProvider_GetTypeToInstantiate_m197801599 (TransientProvider_t1859995120 * __this, Type_t * ___contractType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.TransientProvider::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* TransientProvider_ValidateBinding_m2413814776 (TransientProvider_t1859995120 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
