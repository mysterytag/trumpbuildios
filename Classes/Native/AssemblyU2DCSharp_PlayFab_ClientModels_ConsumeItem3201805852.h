﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.ConsumeItemResult
struct  ConsumeItemResult_t3201805852  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.ConsumeItemResult::<ItemInstanceId>k__BackingField
	String_t* ___U3CItemInstanceIdU3Ek__BackingField_2;
	// System.Int32 PlayFab.ClientModels.ConsumeItemResult::<RemainingUses>k__BackingField
	int32_t ___U3CRemainingUsesU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CItemInstanceIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConsumeItemResult_t3201805852, ___U3CItemInstanceIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CItemInstanceIdU3Ek__BackingField_2() const { return ___U3CItemInstanceIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CItemInstanceIdU3Ek__BackingField_2() { return &___U3CItemInstanceIdU3Ek__BackingField_2; }
	inline void set_U3CItemInstanceIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CItemInstanceIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemInstanceIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CRemainingUsesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConsumeItemResult_t3201805852, ___U3CRemainingUsesU3Ek__BackingField_3)); }
	inline int32_t get_U3CRemainingUsesU3Ek__BackingField_3() const { return ___U3CRemainingUsesU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CRemainingUsesU3Ek__BackingField_3() { return &___U3CRemainingUsesU3Ek__BackingField_3; }
	inline void set_U3CRemainingUsesU3Ek__BackingField_3(int32_t value)
	{
		___U3CRemainingUsesU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
