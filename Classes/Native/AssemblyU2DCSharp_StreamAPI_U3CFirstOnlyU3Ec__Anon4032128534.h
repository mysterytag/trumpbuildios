﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IEmptyStream
struct IEmptyStream_t3684082468;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<FirstOnly>c__AnonStorey13A
struct  U3CFirstOnlyU3Ec__AnonStorey13A_t4032128534  : public Il2CppObject
{
public:
	// IEmptyStream StreamAPI/<FirstOnly>c__AnonStorey13A::stream
	Il2CppObject * ___stream_0;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CFirstOnlyU3Ec__AnonStorey13A_t4032128534, ___stream_0)); }
	inline Il2CppObject * get_stream_0() const { return ___stream_0; }
	inline Il2CppObject ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
