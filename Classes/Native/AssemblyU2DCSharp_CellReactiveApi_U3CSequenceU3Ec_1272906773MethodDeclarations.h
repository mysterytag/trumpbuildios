﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1<System.Object>
struct U3CSequenceU3Ec__AnonStorey111_1_t1272906773;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1<System.Object>::.ctor()
extern "C"  void U3CSequenceU3Ec__AnonStorey111_1__ctor_m2577307519_gshared (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * __this, const MethodInfo* method);
#define U3CSequenceU3Ec__AnonStorey111_1__ctor_m2577307519(__this, method) ((  void (*) (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 *, const MethodInfo*))U3CSequenceU3Ec__AnonStorey111_1__ctor_m2577307519_gshared)(__this, method)
// System.Void CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1<System.Object>::<>m__199(T)
extern "C"  void U3CSequenceU3Ec__AnonStorey111_1_U3CU3Em__199_m1588367251_gshared (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CSequenceU3Ec__AnonStorey111_1_U3CU3Em__199_m1588367251(__this, ____0, method) ((  void (*) (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 *, Il2CppObject *, const MethodInfo*))U3CSequenceU3Ec__AnonStorey111_1_U3CU3Em__199_m1588367251_gshared)(__this, ____0, method)
