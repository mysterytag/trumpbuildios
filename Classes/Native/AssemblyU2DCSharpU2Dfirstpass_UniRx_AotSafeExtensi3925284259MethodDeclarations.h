﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>
struct U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::.ctor()
extern "C"  void U3CAsSafeEnumerableU3Ec__Iterator1D_1__ctor_m2146879469_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method);
#define U3CAsSafeEnumerableU3Ec__Iterator1D_1__ctor_m2146879469(__this, method) ((  void (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))U3CAsSafeEnumerableU3Ec__Iterator1D_1__ctor_m2146879469_gshared)(__this, method)
// T UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m900220374_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method);
#define U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m900220374(__this, method) ((  Il2CppObject * (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m900220374_gshared)(__this, method)
// System.Object UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3808416163_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method);
#define U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3808416163(__this, method) ((  Il2CppObject * (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3808416163_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m54988620_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method);
#define U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m54988620(__this, method) ((  Il2CppObject * (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m54988620_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m934906879_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method);
#define U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m934906879(__this, method) ((  Il2CppObject* (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m934906879_gshared)(__this, method)
// System.Boolean UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::MoveNext()
extern "C"  bool U3CAsSafeEnumerableU3Ec__Iterator1D_1_MoveNext_m1829913015_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method);
#define U3CAsSafeEnumerableU3Ec__Iterator1D_1_MoveNext_m1829913015(__this, method) ((  bool (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))U3CAsSafeEnumerableU3Ec__Iterator1D_1_MoveNext_m1829913015_gshared)(__this, method)
// System.Void UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::Dispose()
extern "C"  void U3CAsSafeEnumerableU3Ec__Iterator1D_1_Dispose_m1465150698_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method);
#define U3CAsSafeEnumerableU3Ec__Iterator1D_1_Dispose_m1465150698(__this, method) ((  void (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))U3CAsSafeEnumerableU3Ec__Iterator1D_1_Dispose_m1465150698_gshared)(__this, method)
// System.Void UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::Reset()
extern "C"  void U3CAsSafeEnumerableU3Ec__Iterator1D_1_Reset_m4088279706_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method);
#define U3CAsSafeEnumerableU3Ec__Iterator1D_1_Reset_m4088279706(__this, method) ((  void (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))U3CAsSafeEnumerableU3Ec__Iterator1D_1_Reset_m4088279706_gshared)(__this, method)
