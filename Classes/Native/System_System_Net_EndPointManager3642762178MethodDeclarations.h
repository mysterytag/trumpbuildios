﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.EndPointManager
struct EndPointManager_t3642762178;
// System.Net.HttpListener
struct HttpListener_t1747553510;
// System.String
struct String_t;
// System.Net.EndPointListener
struct EndPointListener_t4058801363;
// System.Net.IPAddress
struct IPAddress_t3220500535;
// System.Net.IPEndPoint
struct IPEndPoint_t1265996582;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_HttpListener1747553510.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_IPAddress3220500535.h"
#include "System_System_Net_EndPointListener4058801363.h"
#include "System_System_Net_IPEndPoint1265996582.h"

// System.Void System.Net.EndPointManager::.ctor()
extern "C"  void EndPointManager__ctor_m1278395028 (EndPointManager_t3642762178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointManager::.cctor()
extern "C"  void EndPointManager__cctor_m493443993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointManager::AddListener(System.Net.HttpListener)
extern "C"  void EndPointManager_AddListener_m2250819039 (Il2CppObject * __this /* static, unused */, HttpListener_t1747553510 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointManager::AddPrefix(System.String,System.Net.HttpListener)
extern "C"  void EndPointManager_AddPrefix_m2697711549 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, HttpListener_t1747553510 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointManager::AddPrefixInternal(System.String,System.Net.HttpListener)
extern "C"  void EndPointManager_AddPrefixInternal_m969349696 (Il2CppObject * __this /* static, unused */, String_t* ___p0, HttpListener_t1747553510 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.EndPointListener System.Net.EndPointManager::GetEPListener(System.Net.IPAddress,System.Int32,System.Net.HttpListener,System.Boolean)
extern "C"  EndPointListener_t4058801363 * EndPointManager_GetEPListener_m1820306518 (Il2CppObject * __this /* static, unused */, IPAddress_t3220500535 * ___addr0, int32_t ___port1, HttpListener_t1747553510 * ___listener2, bool ___secure3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointManager::RemoveEndPoint(System.Net.EndPointListener,System.Net.IPEndPoint)
extern "C"  void EndPointManager_RemoveEndPoint_m3716838736 (Il2CppObject * __this /* static, unused */, EndPointListener_t4058801363 * ___epl0, IPEndPoint_t1265996582 * ___ep1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointManager::RemoveListener(System.Net.HttpListener)
extern "C"  void EndPointManager_RemoveListener_m2998232606 (Il2CppObject * __this /* static, unused */, HttpListener_t1747553510 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointManager::RemovePrefix(System.String,System.Net.HttpListener)
extern "C"  void EndPointManager_RemovePrefix_m4054255996 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, HttpListener_t1747553510 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.EndPointManager::RemovePrefixInternal(System.String,System.Net.HttpListener)
extern "C"  void EndPointManager_RemovePrefixInternal_m1533551871 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, HttpListener_t1747553510 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
