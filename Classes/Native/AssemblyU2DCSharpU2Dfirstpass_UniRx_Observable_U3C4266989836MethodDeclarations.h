﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<System.Object,System.Object>
struct U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<System.Object,System.Object>::.ctor()
extern "C"  void U3COnErrorRetryU3Ec__AnonStorey3D_2__ctor_m3988424000_gshared (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * __this, const MethodInfo* method);
#define U3COnErrorRetryU3Ec__AnonStorey3D_2__ctor_m3988424000(__this, method) ((  void (*) (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 *, const MethodInfo*))U3COnErrorRetryU3Ec__AnonStorey3D_2__ctor_m3988424000_gshared)(__this, method)
// UniRx.IObservable`1<TSource> UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<System.Object,System.Object>::<>m__3F()
extern "C"  Il2CppObject* U3COnErrorRetryU3Ec__AnonStorey3D_2_U3CU3Em__3F_m2928952763_gshared (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * __this, const MethodInfo* method);
#define U3COnErrorRetryU3Ec__AnonStorey3D_2_U3CU3Em__3F_m2928952763(__this, method) ((  Il2CppObject* (*) (U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 *, const MethodInfo*))U3COnErrorRetryU3Ec__AnonStorey3D_2_U3CU3Em__3F_m2928952763_gshared)(__this, method)
