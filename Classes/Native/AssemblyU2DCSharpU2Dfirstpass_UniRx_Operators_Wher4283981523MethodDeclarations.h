﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<UniRx.Unit>
struct U3CCombinePredicateU3Ec__AnonStorey72_t4283981523;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<UniRx.Unit>::.ctor()
extern "C"  void U3CCombinePredicateU3Ec__AnonStorey72__ctor_m1231113092_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t4283981523 * __this, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72__ctor_m1231113092(__this, method) ((  void (*) (U3CCombinePredicateU3Ec__AnonStorey72_t4283981523 *, const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72__ctor_m1231113092_gshared)(__this, method)
// System.Boolean UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<UniRx.Unit>::<>m__8F(T)
extern "C"  bool U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m1800913095_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t4283981523 * __this, Unit_t2558286038  ___x0, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m1800913095(__this, ___x0, method) ((  bool (*) (U3CCombinePredicateU3Ec__AnonStorey72_t4283981523 *, Unit_t2558286038 , const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m1800913095_gshared)(__this, ___x0, method)
