﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>
struct U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2<System.Object,System.Object>
struct  U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393  : public Il2CppObject
{
public:
	// System.Action`1<T> CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2::reaction
	Action_1_t985559125 * ___reaction_0;
	// CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<T,T2> CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2::<>f__ref$251
	U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * ___U3CU3Ef__refU24251_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393, ___reaction_0)); }
	inline Action_1_t985559125 * get_reaction_0() const { return ___reaction_0; }
	inline Action_1_t985559125 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_1_t985559125 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24251_1() { return static_cast<int32_t>(offsetof(U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393, ___U3CU3Ef__refU24251_1)); }
	inline U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * get_U3CU3Ef__refU24251_1() const { return ___U3CU3Ef__refU24251_1; }
	inline U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 ** get_address_of_U3CU3Ef__refU24251_1() { return &___U3CU3Ef__refU24251_1; }
	inline void set_U3CU3Ef__refU24251_1(U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * value)
	{
		___U3CU3Ef__refU24251_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24251_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
