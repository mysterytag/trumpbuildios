﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator3A_1_t3575002786;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::.ctor()
extern "C"  void U3CToEnumerableU3Ec__Iterator3A_1__ctor_m2261611489_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3A_1__ctor_m2261611489(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3A_1__ctor_m2261611489_gshared)(__this, method)
// T ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2791918858_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2791918858(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2791918858_gshared)(__this, method)
// System.Object ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_IEnumerator_get_Current_m3608398447_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_IEnumerator_get_Current_m3608398447(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_IEnumerator_get_Current_m3608398447_gshared)(__this, method)
// System.Collections.IEnumerator ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_IEnumerable_GetEnumerator_m486128624_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_IEnumerable_GetEnumerator_m486128624(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_IEnumerable_GetEnumerator_m486128624_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m79047667_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m79047667(__this, method) ((  Il2CppObject* (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3A_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m79047667_gshared)(__this, method)
// System.Boolean ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::MoveNext()
extern "C"  bool U3CToEnumerableU3Ec__Iterator3A_1_MoveNext_m1822962523_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3A_1_MoveNext_m1822962523(__this, method) ((  bool (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3A_1_MoveNext_m1822962523_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::Dispose()
extern "C"  void U3CToEnumerableU3Ec__Iterator3A_1_Dispose_m53472222_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3A_1_Dispose_m53472222(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3A_1_Dispose_m53472222_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>::Reset()
extern "C"  void U3CToEnumerableU3Ec__Iterator3A_1_Reset_m4203011726_gshared (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator3A_1_Reset_m4203011726(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator3A_1_t3575002786 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator3A_1_Reset_m4203011726_gshared)(__this, method)
