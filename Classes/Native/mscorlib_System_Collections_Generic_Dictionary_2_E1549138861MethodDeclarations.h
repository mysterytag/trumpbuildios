﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>
struct Dictionary_2_t1782110920;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1549138861.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21270642218.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_Region3089759486.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2830618433_gshared (Enumerator_t1549138862 * __this, Dictionary_2_t1782110920 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2830618433(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1549138862 *, Dictionary_2_t1782110920 *, const MethodInfo*))Enumerator__ctor_m2830618433_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4162971594_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4162971594(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4162971594_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1989515988_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1989515988(__this, method) ((  void (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1989515988_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m321263499_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m321263499(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m321263499_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3859981606_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3859981606(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3859981606_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3933983288_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3933983288(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3933983288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3250803844_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3250803844(__this, method) ((  bool (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_MoveNext_m3250803844_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::get_Current()
extern "C"  KeyValuePair_2_t1270642218  Enumerator_get_Current_m2856640440_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2856640440(__this, method) ((  KeyValuePair_2_t1270642218  (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_get_Current_m2856640440_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m686828045_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m686828045(__this, method) ((  Il2CppObject * (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_get_CurrentKey_m686828045_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m738581261_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m738581261(__this, method) ((  int32_t (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_get_CurrentValue_m738581261_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::Reset()
extern "C"  void Enumerator_Reset_m1247877139_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1247877139(__this, method) ((  void (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_Reset_m1247877139_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4065906140_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4065906140(__this, method) ((  void (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_VerifyState_m4065906140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1660432964_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1660432964(__this, method) ((  void (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_VerifyCurrent_m1660432964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,PlayFab.UUnit.Region>::Dispose()
extern "C"  void Enumerator_Dispose_m3437484067_gshared (Enumerator_t1549138862 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3437484067(__this, method) ((  void (*) (Enumerator_t1549138862 *, const MethodInfo*))Enumerator_Dispose_m3437484067_gshared)(__this, method)
