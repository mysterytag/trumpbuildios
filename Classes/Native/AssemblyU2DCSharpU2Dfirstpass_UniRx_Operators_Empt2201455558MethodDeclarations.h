﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Empty_t2201455558;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m284600178_gshared (Empty_t2201455558 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m284600178(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t2201455558 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m284600178_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void Empty_OnNext_m2147868896_gshared (Empty_t2201455558 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method);
#define Empty_OnNext_m2147868896(__this, ___value0, method) ((  void (*) (Empty_t2201455558 *, CollectionRemoveEvent_1_t242637724 , const MethodInfo*))Empty_OnNext_m2147868896_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m4163648495_gshared (Empty_t2201455558 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m4163648495(__this, ___error0, method) ((  void (*) (Empty_t2201455558 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m4163648495_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m2876906946_gshared (Empty_t2201455558 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m2876906946(__this, method) ((  void (*) (Empty_t2201455558 *, const MethodInfo*))Empty_OnCompleted_m2876906946_gshared)(__this, method)
