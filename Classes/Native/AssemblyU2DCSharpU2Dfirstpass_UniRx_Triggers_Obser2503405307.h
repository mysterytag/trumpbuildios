﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>
struct Subject_1_t848168958;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservablePointerEnterTrigger
struct  ObservablePointerEnterTrigger_t2503405307  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerEnterTrigger::onPointerEnter
	Subject_1_t848168958 * ___onPointerEnter_8;

public:
	inline static int32_t get_offset_of_onPointerEnter_8() { return static_cast<int32_t>(offsetof(ObservablePointerEnterTrigger_t2503405307, ___onPointerEnter_8)); }
	inline Subject_1_t848168958 * get_onPointerEnter_8() const { return ___onPointerEnter_8; }
	inline Subject_1_t848168958 ** get_address_of_onPointerEnter_8() { return &___onPointerEnter_8; }
	inline void set_onPointerEnter_8(Subject_1_t848168958 * value)
	{
		___onPointerEnter_8 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerEnter_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
