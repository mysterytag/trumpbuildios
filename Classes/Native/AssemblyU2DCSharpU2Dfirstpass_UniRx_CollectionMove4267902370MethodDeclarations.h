﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246MethodDeclarations.h"

// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Int32,System.Int32,T)
#define CollectionMoveEvent_1__ctor_m2945145849(__this, ___oldIndex0, ___newIndex1, ___value2, method) ((  void (*) (CollectionMoveEvent_1_t4267902370 *, int32_t, int32_t, Tuple_2_t2188574943 , const MethodInfo*))CollectionMoveEvent_1__ctor_m1005665093_gshared)(__this, ___oldIndex0, ___newIndex1, ___value2, method)
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_OldIndex()
#define CollectionMoveEvent_1_get_OldIndex_m480033833(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t4267902370 *, const MethodInfo*))CollectionMoveEvent_1_get_OldIndex_m1808831945_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_OldIndex(System.Int32)
#define CollectionMoveEvent_1_set_OldIndex_m2384195960(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t4267902370 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_OldIndex_m3813399748_gshared)(__this, ___value0, method)
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_NewIndex()
#define CollectionMoveEvent_1_get_NewIndex_m3492451984(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t4267902370 *, const MethodInfo*))CollectionMoveEvent_1_get_NewIndex_m526282800_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_NewIndex(System.Int32)
#define CollectionMoveEvent_1_set_NewIndex_m3765070175(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t4267902370 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_NewIndex_m899306667_gshared)(__this, ___value0, method)
// T UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Value()
#define CollectionMoveEvent_1_get_Value_m3458745324(__this, method) ((  Tuple_2_t2188574943  (*) (CollectionMoveEvent_1_t4267902370 *, const MethodInfo*))CollectionMoveEvent_1_get_Value_m2394553534_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Value(T)
#define CollectionMoveEvent_1_set_Value_m1238478247(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t4267902370 *, Tuple_2_t2188574943 , const MethodInfo*))CollectionMoveEvent_1_set_Value_m1426460147_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ToString()
#define CollectionMoveEvent_1_ToString_m657540878(__this, method) ((  String_t* (*) (CollectionMoveEvent_1_t4267902370 *, const MethodInfo*))CollectionMoveEvent_1_ToString_m3719328820_gshared)(__this, method)
// System.Int32 UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::GetHashCode()
#define CollectionMoveEvent_1_GetHashCode_m155433310(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t4267902370 *, const MethodInfo*))CollectionMoveEvent_1_GetHashCode_m1029581758_gshared)(__this, method)
// System.Boolean UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Equals(UniRx.CollectionMoveEvent`1<T>)
#define CollectionMoveEvent_1_Equals_m4146041208(__this, ___other0, method) ((  bool (*) (CollectionMoveEvent_1_t4267902370 *, CollectionMoveEvent_1_t4267902370 , const MethodInfo*))CollectionMoveEvent_1_Equals_m2890172940_gshared)(__this, ___other0, method)
