﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<System.Int64>
struct ReactiveProperty_1_t3455747920;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Collections.Generic.IEqualityComparer`1<System.Int64>
struct IEqualityComparer_1_t876714237;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ReactiveProperty`1<System.Int64>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m2358756868_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2358756868(__this, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, const MethodInfo*))ReactiveProperty_1__ctor_m2358756868_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m107020250_gshared (ReactiveProperty_1_t3455747920 * __this, int64_t ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m107020250(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, int64_t, const MethodInfo*))ReactiveProperty_1__ctor_m107020250_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m1729138915_gshared (ReactiveProperty_1_t3455747920 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1729138915(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m1729138915_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m3845126587_gshared (ReactiveProperty_1_t3455747920 * __this, Il2CppObject* ___source0, int64_t ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3845126587(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, Il2CppObject*, int64_t, const MethodInfo*))ReactiveProperty_1__ctor_m3845126587_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m3919889961_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m3919889961(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m3919889961_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Int64>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m2529372463_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m2529372463(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t3455747920 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m2529372463_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<System.Int64>::get_Value()
extern "C"  int64_t ReactiveProperty_1_get_Value_m3168728843_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m3168728843(__this, method) ((  int64_t (*) (ReactiveProperty_1_t3455747920 *, const MethodInfo*))ReactiveProperty_1_get_Value_m3168728843_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m2082580264_gshared (ReactiveProperty_1_t3455747920 * __this, int64_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m2082580264(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, int64_t, const MethodInfo*))ReactiveProperty_1_set_Value_m2082580264_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2369624463_gshared (ReactiveProperty_1_t3455747920 * __this, int64_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m2369624463(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, int64_t, const MethodInfo*))ReactiveProperty_1_SetValue_m2369624463_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m1783838994_gshared (ReactiveProperty_1_t3455747920 * __this, int64_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m1783838994(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, int64_t, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m1783838994_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m702201603_gshared (ReactiveProperty_1_t3455747920 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m702201603(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t3455747920 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m702201603_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m3215868225_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m3215868225(__this, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, const MethodInfo*))ReactiveProperty_1_Dispose_m3215868225_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Int64>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m1078276344_gshared (ReactiveProperty_1_t3455747920 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m1078276344(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t3455747920 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m1078276344_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<System.Int64>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m483004175_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m483004175(__this, method) ((  String_t* (*) (ReactiveProperty_1_t3455747920 *, const MethodInfo*))ReactiveProperty_1_ToString_m483004175_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<System.Int64>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3942530047_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3942530047(__this, method) ((  bool (*) (ReactiveProperty_1_t3455747920 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3942530047_gshared)(__this, method)
