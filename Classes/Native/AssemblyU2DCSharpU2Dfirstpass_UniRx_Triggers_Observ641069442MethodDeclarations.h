﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableEventTrigger
struct ObservableEventTrigger_t641069442;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t552897310;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData>
struct IObservable_1_t3305902090;
// UniRx.IObservable`1<UnityEngine.EventSystems.AxisEventData>
struct IObservable_1_t311695674;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventDa552897310.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservableEventTrigger::.ctor()
extern "C"  void ObservableEventTrigger__ctor_m3745964347 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IDeselectHandler.OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IDeselectHandler_OnDeselect_m3604869468 (ObservableEventTrigger_t641069442 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IMoveHandler.OnMove(UnityEngine.EventSystems.AxisEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IMoveHandler_OnMove_m1945986388 (ObservableEventTrigger_t641069442 * __this, AxisEventData_t552897310 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IPointerDownHandler.OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_m4221264060 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IPointerEnterHandler.OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_m3661980800 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IPointerExitHandler.OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_m1831340860 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IPointerUpHandler.OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m2343958492 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.ISelectHandler.OnSelect(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_ISelectHandler_OnSelect_m3026622426 (ObservableEventTrigger_t641069442 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IPointerClickHandler.OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_m1307766560 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.ISubmitHandler.OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_ISubmitHandler_OnSubmit_m1444044306 (ObservableEventTrigger_t641069442 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IDragHandler.OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IDragHandler_OnDrag_m1374526926 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IBeginDragHandler.OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IBeginDragHandler_OnBeginDrag_m1527526396 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IEndDragHandler.OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IEndDragHandler_OnEndDrag_m2599352252 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IDropHandler.OnDrop(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IDropHandler_OnDrop_m2135881688 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IUpdateSelectedHandler.OnUpdateSelected(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IUpdateSelectedHandler_OnUpdateSelected_m1804393450 (ObservableEventTrigger_t641069442 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IInitializePotentialDragHandler.OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IInitializePotentialDragHandler_OnInitializePotentialDrag_m3169343836 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.ICancelHandler.OnCancel(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_ICancelHandler_OnCancel_m3248628438 (ObservableEventTrigger_t641069442 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::UnityEngine.EventSystems.IScrollHandler.OnScroll(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableEventTrigger_UnityEngine_EventSystems_IScrollHandler_OnScroll_m1401234716 (ObservableEventTrigger_t641069442 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::OnDeselectAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnDeselectAsObservable_m2305648904 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.AxisEventData> UniRx.Triggers.ObservableEventTrigger::OnMoveAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnMoveAsObservable_m233925740 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnPointerDownAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnPointerDownAsObservable_m3441941842 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnPointerEnterAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnPointerEnterAsObservable_m3040126420 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnPointerExitAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnPointerExitAsObservable_m752675022 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnPointerUpAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnPointerUpAsObservable_m3229574667 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::OnSelectAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnSelectAsObservable_m3673139271 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnPointerClickAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnPointerClickAsObservable_m3779850372 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::OnSubmitAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnSubmitAsObservable_m3600618083 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnDragAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnDragAsObservable_m3163672493 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnBeginDragAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnBeginDragAsObservable_m310892816 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnEndDragAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnEndDragAsObservable_m4006032386 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnDropAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnDropAsObservable_m994225704 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::OnUpdateSelectedAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnUpdateSelectedAsObservable_m3559044815 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnInitializePotentialDragAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnInitializePotentialDragAsObservable_m1862387925 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::OnCancelAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnCancelAsObservable_m104934725 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::OnScrollAsObservable()
extern "C"  Il2CppObject* ObservableEventTrigger_OnScrollAsObservable_m539210950 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableEventTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableEventTrigger_RaiseOnCompletedOnDestroy_m4116827380 (ObservableEventTrigger_t641069442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
