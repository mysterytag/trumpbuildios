﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConnectionCollector
struct ConnectionCollector_t444796719;
// System.Action`2<System.Object,ConnectionCollector>
struct Action_2_t3713150217;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.Object>
struct  U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981  : public Il2CppObject
{
public:
	// ConnectionCollector CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1::collector
	ConnectionCollector_t444796719 * ___collector_0;
	// System.Action`2<T,ConnectionCollector> CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1::action
	Action_2_t3713150217 * ___action_1;

public:
	inline static int32_t get_offset_of_collector_0() { return static_cast<int32_t>(offsetof(U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981, ___collector_0)); }
	inline ConnectionCollector_t444796719 * get_collector_0() const { return ___collector_0; }
	inline ConnectionCollector_t444796719 ** get_address_of_collector_0() { return &___collector_0; }
	inline void set_collector_0(ConnectionCollector_t444796719 * value)
	{
		___collector_0 = value;
		Il2CppCodeGenWriteBarrier(&___collector_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981, ___action_1)); }
	inline Action_2_t3713150217 * get_action_1() const { return ___action_1; }
	inline Action_2_t3713150217 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_2_t3713150217 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
