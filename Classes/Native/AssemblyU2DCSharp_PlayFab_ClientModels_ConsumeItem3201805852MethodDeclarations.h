﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ConsumeItemResult
struct ConsumeItemResult_t3201805852;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ConsumeItemResult::.ctor()
extern "C"  void ConsumeItemResult__ctor_m3086695309 (ConsumeItemResult_t3201805852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ConsumeItemResult::get_ItemInstanceId()
extern "C"  String_t* ConsumeItemResult_get_ItemInstanceId_m430327366 (ConsumeItemResult_t3201805852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConsumeItemResult::set_ItemInstanceId(System.String)
extern "C"  void ConsumeItemResult_set_ItemInstanceId_m2149676363 (ConsumeItemResult_t3201805852 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.ConsumeItemResult::get_RemainingUses()
extern "C"  int32_t ConsumeItemResult_get_RemainingUses_m1918470742 (ConsumeItemResult_t3201805852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConsumeItemResult::set_RemainingUses(System.Int32)
extern "C"  void ConsumeItemResult_set_RemainingUses_m3172814721 (ConsumeItemResult_t3201805852 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
