﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>
struct SelectManyObservable_2_t3540602214;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Func`2<UniRx.Unit,UniRx.IObservable`1<UniRx.Unit>>
struct Func_2_t577222668;
// System.Func`3<UniRx.Unit,System.Int32,UniRx.IObservable`1<UniRx.Unit>>
struct Func_3_t2361139122;
// System.Func`2<UniRx.Unit,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>
struct Func_2_t3690578660;
// System.Func`3<UniRx.Unit,System.Int32,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>
struct Func_3_t1179527818;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m206382432_gshared (SelectManyObservable_2_t3540602214 * __this, Il2CppObject* ___source0, Func_2_t577222668 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m206382432(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t3540602214 *, Il2CppObject*, Func_2_t577222668 *, const MethodInfo*))SelectManyObservable_2__ctor_m206382432_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m4062203036_gshared (SelectManyObservable_2_t3540602214 * __this, Il2CppObject* ___source0, Func_3_t2361139122 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m4062203036(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t3540602214 *, Il2CppObject*, Func_3_t2361139122 *, const MethodInfo*))SelectManyObservable_2__ctor_m4062203036_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m2961588656_gshared (SelectManyObservable_2_t3540602214 * __this, Il2CppObject* ___source0, Func_2_t3690578660 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m2961588656(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t3540602214 *, Il2CppObject*, Func_2_t3690578660 *, const MethodInfo*))SelectManyObservable_2__ctor_m2961588656_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m2740533236_gshared (SelectManyObservable_2_t3540602214 * __this, Il2CppObject* ___source0, Func_3_t1179527818 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m2740533236(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t3540602214 *, Il2CppObject*, Func_3_t1179527818 *, const MethodInfo*))SelectManyObservable_2__ctor_m2740533236_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * SelectManyObservable_2_SubscribeCore_m12378011_gshared (SelectManyObservable_2_t3540602214 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectManyObservable_2_SubscribeCore_m12378011(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectManyObservable_2_t3540602214 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObservable_2_SubscribeCore_m12378011_gshared)(__this, ___observer0, ___cancel1, method)
