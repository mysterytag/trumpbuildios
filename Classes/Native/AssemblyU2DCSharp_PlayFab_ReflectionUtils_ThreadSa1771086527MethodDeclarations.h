﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t1771086527;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ThreadSafeDictionaryValueFactory_2__ctor_m191619557_gshared (ThreadSafeDictionaryValueFactory_2_t1771086527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ThreadSafeDictionaryValueFactory_2__ctor_m191619557(__this, ___object0, ___method1, method) ((  void (*) (ThreadSafeDictionaryValueFactory_2_t1771086527 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ThreadSafeDictionaryValueFactory_2__ctor_m191619557_gshared)(__this, ___object0, ___method1, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::Invoke(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_Invoke_m808333140_gshared (ThreadSafeDictionaryValueFactory_2_t1771086527 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ThreadSafeDictionaryValueFactory_2_Invoke_m808333140(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t1771086527 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_Invoke_m808333140_gshared)(__this, ___key0, method)
// System.IAsyncResult PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::BeginInvoke(TKey,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3493335453_gshared (ThreadSafeDictionaryValueFactory_2_t1771086527 * __this, Il2CppObject * ___key0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3493335453(__this, ___key0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t1771086527 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_BeginInvoke_m3493335453_gshared)(__this, ___key0, ___callback1, ___object2, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_EndInvoke_m2172200533_gshared (ThreadSafeDictionaryValueFactory_2_t1771086527 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ThreadSafeDictionaryValueFactory_2_EndInvoke_m2172200533(__this, ___result0, method) ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t1771086527 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_EndInvoke_m2172200533_gshared)(__this, ___result0, method)
