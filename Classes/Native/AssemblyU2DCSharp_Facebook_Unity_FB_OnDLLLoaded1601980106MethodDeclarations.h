﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FB/OnDLLLoaded
struct OnDLLLoaded_t1601980106;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Facebook.Unity.FB/OnDLLLoaded::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDLLLoaded__ctor_m2262301583 (OnDLLLoaded_t1601980106 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/OnDLLLoaded::Invoke()
extern "C"  void OnDLLLoaded_Invoke_m3780148393 (OnDLLLoaded_t1601980106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnDLLLoaded_t1601980106(Il2CppObject* delegate);
// System.IAsyncResult Facebook.Unity.FB/OnDLLLoaded::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDLLLoaded_BeginInvoke_m1319254978 (OnDLLLoaded_t1601980106 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/OnDLLLoaded::EndInvoke(System.IAsyncResult)
extern "C"  void OnDLLLoaded_EndInvoke_m1188924703 (OnDLLLoaded_t1601980106 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
