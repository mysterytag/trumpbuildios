﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen91656570MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::.ctor()
#define Subject_1__ctor_m3440983834(__this, method) ((  void (*) (Subject_1_t1910969694 *, const MethodInfo*))Subject_1__ctor_m342849048_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::get_HasObservers()
#define Subject_1_get_HasObservers_m1680202106(__this, method) ((  bool (*) (Subject_1_t1910969694 *, const MethodInfo*))Subject_1_get_HasObservers_m501865844_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnCompleted()
#define Subject_1_OnCompleted_m1329280804(__this, method) ((  void (*) (Subject_1_t1910969694 *, const MethodInfo*))Subject_1_OnCompleted_m1258272674_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnError(System.Exception)
#define Subject_1_OnError_m473453137(__this, ___error0, method) ((  void (*) (Subject_1_t1910969694 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2042234319_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnNext(T)
#define Subject_1_OnNext_m2491349826(__this, ___value0, method) ((  void (*) (Subject_1_t1910969694 *, CollectionMoveEvent_1_t4267902370 , const MethodInfo*))Subject_1_OnNext_m1596156608_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m3749474223(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1910969694 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m497037271_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::Dispose()
#define Subject_1_Dispose_m3853896919(__this, method) ((  void (*) (Subject_1_t1910969694 *, const MethodInfo*))Subject_1_Dispose_m2958703701_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m2109779808(__this, method) ((  void (*) (Subject_1_t1910969694 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m171229406_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3386940401(__this, method) ((  bool (*) (Subject_1_t1910969694 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m4063057259_gshared)(__this, method)
