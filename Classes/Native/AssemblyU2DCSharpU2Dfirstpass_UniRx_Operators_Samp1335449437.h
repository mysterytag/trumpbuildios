﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SampleFrameObservable`1<System.Object>
struct  SampleFrameObservable_1_t1335449437  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.SampleFrameObservable`1::source
	Il2CppObject* ___source_1;
	// System.Int32 UniRx.Operators.SampleFrameObservable`1::frameCount
	int32_t ___frameCount_2;
	// UniRx.FrameCountType UniRx.Operators.SampleFrameObservable`1::frameCountType
	int32_t ___frameCountType_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(SampleFrameObservable_1_t1335449437, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(SampleFrameObservable_1_t1335449437, ___frameCount_2)); }
	inline int32_t get_frameCount_2() const { return ___frameCount_2; }
	inline int32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(int32_t value)
	{
		___frameCount_2 = value;
	}

	inline static int32_t get_offset_of_frameCountType_3() { return static_cast<int32_t>(offsetof(SampleFrameObservable_1_t1335449437, ___frameCountType_3)); }
	inline int32_t get_frameCountType_3() const { return ___frameCountType_3; }
	inline int32_t* get_address_of_frameCountType_3() { return &___frameCountType_3; }
	inline void set_frameCountType_3(int32_t value)
	{
		___frameCountType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
