﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ConsumeItemRequestCallback
struct ConsumeItemRequestCallback_t2404276965;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ConsumeItemRequest
struct ConsumeItemRequest_t3677119120;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumeItem3677119120.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ConsumeItemRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ConsumeItemRequestCallback__ctor_m1245458772 (ConsumeItemRequestCallback_t2404276965 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConsumeItemRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ConsumeItemRequest,System.Object)
extern "C"  void ConsumeItemRequestCallback_Invoke_m1154782527 (ConsumeItemRequestCallback_t2404276965 * __this, String_t* ___urlPath0, int32_t ___callId1, ConsumeItemRequest_t3677119120 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ConsumeItemRequestCallback_t2404276965(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ConsumeItemRequest_t3677119120 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ConsumeItemRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ConsumeItemRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConsumeItemRequestCallback_BeginInvoke_m1058686768 (ConsumeItemRequestCallback_t2404276965 * __this, String_t* ___urlPath0, int32_t ___callId1, ConsumeItemRequest_t3677119120 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConsumeItemRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ConsumeItemRequestCallback_EndInvoke_m1298492772 (ConsumeItemRequestCallback_t2404276965 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
