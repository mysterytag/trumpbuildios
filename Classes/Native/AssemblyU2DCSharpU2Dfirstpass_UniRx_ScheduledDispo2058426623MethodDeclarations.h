﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledDisposable
struct ScheduledDisposable_t2058426623;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ScheduledDisposable::.ctor(UniRx.IScheduler,System.IDisposable)
extern "C"  void ScheduledDisposable__ctor_m2368627524 (ScheduledDisposable_t2058426623 * __this, Il2CppObject * ___scheduler0, Il2CppObject * ___disposable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IScheduler UniRx.ScheduledDisposable::get_Scheduler()
extern "C"  Il2CppObject * ScheduledDisposable_get_Scheduler_m830293869 (ScheduledDisposable_t2058426623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.ScheduledDisposable::get_Disposable()
extern "C"  Il2CppObject * ScheduledDisposable_get_Disposable_m1845407632 (ScheduledDisposable_t2058426623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.ScheduledDisposable::get_IsDisposed()
extern "C"  bool ScheduledDisposable_get_IsDisposed_m362889950 (ScheduledDisposable_t2058426623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ScheduledDisposable::Dispose()
extern "C"  void ScheduledDisposable_Dispose_m4084831615 (ScheduledDisposable_t2058426623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ScheduledDisposable::DisposeInner()
extern "C"  void ScheduledDisposable_DisposeInner_m3097279481 (ScheduledDisposable_t2058426623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
