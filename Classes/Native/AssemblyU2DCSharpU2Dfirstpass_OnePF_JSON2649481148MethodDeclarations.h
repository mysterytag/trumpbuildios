﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.JSON
struct JSON_t2649481148;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON2649481148.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// System.Void OnePF.JSON::.ctor()
extern "C"  void JSON__ctor_m802668213 (JSON_t2649481148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON::.ctor(System.String)
extern "C"  void JSON__ctor_m2841521773 (JSON_t2649481148 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnePF.JSON::get_Item(System.String)
extern "C"  Il2CppObject * JSON_get_Item_m1796424386 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON::set_Item(System.String,System.Object)
extern "C"  void JSON_set_Item_m996622641 (JSON_t2649481148 * __this, String_t* ___fieldName0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.JSON::ToString(System.String)
extern "C"  String_t* JSON_ToString_m2727470212 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OnePF.JSON::ToInt(System.String)
extern "C"  int32_t JSON_ToInt_m961998353 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 OnePF.JSON::ToLong(System.String)
extern "C"  int64_t JSON_ToLong_m2611652581 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OnePF.JSON::ToFloat(System.String)
extern "C"  float JSON_ToFloat_m3878641306 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.JSON::ToBoolean(System.String)
extern "C"  bool JSON_ToBoolean_m2964217310 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.JSON::get_serialized()
extern "C"  String_t* JSON_get_serialized_m2868133311 (JSON_t2649481148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON::set_serialized(System.String)
extern "C"  void JSON_set_serialized_m962312562 (JSON_t2649481148 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON::ToJSON(System.String)
extern "C"  JSON_t2649481148 * JSON_ToJSON_m1451252037 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnePF.JSON::op_Implicit(OnePF.JSON)
extern "C"  Vector2_t3525329788  JSON_op_Implicit_m497151809 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Vector2)
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m2790371584 (Il2CppObject * __this /* static, unused */, Vector2_t3525329788  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 OnePF.JSON::op_Implicit(OnePF.JSON)
extern "C"  Vector3_t3525329789  JSON_op_Implicit_m826917570 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Vector3)
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m2790371615 (Il2CppObject * __this /* static, unused */, Vector3_t3525329789  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion OnePF.JSON::op_Implicit(OnePF.JSON)
extern "C"  Quaternion_t1891715979  JSON_op_Implicit_m1452766860 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Quaternion)
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m3800652997 (Il2CppObject * __this /* static, unused */, Quaternion_t1891715979  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color OnePF.JSON::op_Implicit(OnePF.JSON)
extern "C"  Color_t1588175760  JSON_op_Implicit_m3877481941 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Color)
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m3477382380 (Il2CppObject * __this /* static, unused */, Color_t1588175760  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 OnePF.JSON::op_Implicit(OnePF.JSON)
extern "C"  Color32_t4137084207  JSON_op_Implicit_m158659444 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Color32)
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m279922093 (Il2CppObject * __this /* static, unused */, Color32_t4137084207  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect OnePF.JSON::op_Implicit(OnePF.JSON)
extern "C"  Rect_t1525428817  JSON_op_Implicit_m757897426 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Rect)
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m1372645951 (Il2CppObject * __this /* static, unused */, Rect_t1525428817  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
