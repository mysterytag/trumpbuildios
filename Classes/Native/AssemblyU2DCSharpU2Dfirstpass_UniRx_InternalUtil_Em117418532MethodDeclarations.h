﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>
struct EmptyObserver_1_t117418532;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3507249563_gshared (EmptyObserver_1_t117418532 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m3507249563(__this, method) ((  void (*) (EmptyObserver_1_t117418532 *, const MethodInfo*))EmptyObserver_1__ctor_m3507249563_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m868457842_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m868457842(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m868457842_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2362488037_gshared (EmptyObserver_1_t117418532 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2362488037(__this, method) ((  void (*) (EmptyObserver_1_t117418532 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2362488037_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1833055890_gshared (EmptyObserver_1_t117418532 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1833055890(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t117418532 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1833055890_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.TimeInterval`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1748205955_gshared (EmptyObserver_1_t117418532 * __this, TimeInterval_1_t708065542  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m1748205955(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t117418532 *, TimeInterval_1_t708065542 , const MethodInfo*))EmptyObserver_1_OnNext_m1748205955_gshared)(__this, ___value0, method)
