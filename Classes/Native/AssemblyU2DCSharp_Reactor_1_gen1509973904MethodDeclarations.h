﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1676326883MethodDeclarations.h"

// System.Void Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::.ctor()
#define Reactor_1__ctor_m3623577431(__this, method) ((  void (*) (Reactor_1_t1509973904 *, const MethodInfo*))Reactor_1__ctor_m3042049264_gshared)(__this, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::React(T)
#define Reactor_1_React_m198445482(__this, ___t0, method) ((  void (*) (Reactor_1_t1509973904 *, CollectionAddEvent_1_t2250169008 , const MethodInfo*))Reactor_1_React_m3645908785_gshared)(__this, ___t0, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m1048196661(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t1509973904 *, CollectionAddEvent_1_t2250169008 , int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m2959551868_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::Empty()
#define Reactor_1_Empty_m597883702(__this, method) ((  bool (*) (Reactor_1_t1509973904 *, const MethodInfo*))Reactor_1_Empty_m1349981671_gshared)(__this, method)
// System.IDisposable Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m3746485456(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t1509973904 *, Action_1_t2398621713 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m243157725_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m1332659235(__this, method) ((  void (*) (Reactor_1_t1509973904 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m1822749610_gshared)(__this, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m2435799988(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1509973904 *, Action_1_t2398621713 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m625386573_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m170815144(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1509973904 *, Action_1_t2398621713 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m3790334913_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<DonateButton>>::Clear()
#define Reactor_1_Clear_m1029710722(__this, method) ((  void (*) (Reactor_1_t1509973904 *, const MethodInfo*))Reactor_1_Clear_m448182555_gshared)(__this, method)
