﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveDictionary`2<System.Object,System.Object>
struct ReactiveDictionary_2_t2215051344;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3824425150;
// System.Object
struct Il2CppObject;
// System.Collections.ICollection
struct ICollection_t3761522009;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t346249057;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t1852733134;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t1451594948;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IObservable`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObservable_1_t9779372;
// UniRx.IObservable`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObservable_1_t3081086655;
// UniRx.IObservable`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObservable_1_t3537725427;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void ReactiveDictionary_2__ctor_m1877793062_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2__ctor_m1877793062(__this, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2__ctor_m1877793062_gshared)(__this, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void ReactiveDictionary_2__ctor_m508283037_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define ReactiveDictionary_2__ctor_m508283037(__this, ___comparer0, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject*, const MethodInfo*))ReactiveDictionary_2__ctor_m508283037_gshared)(__this, ___comparer0, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ReactiveDictionary_2__ctor_m4266695425_gshared (ReactiveDictionary_2_t2215051344 * __this, Dictionary_2_t3824425150 * ___innerDictionary0, const MethodInfo* method);
#define ReactiveDictionary_2__ctor_m4266695425(__this, ___innerDictionary0, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Dictionary_2_t3824425150 *, const MethodInfo*))ReactiveDictionary_2__ctor_m4266695425_gshared)(__this, ___innerDictionary0, method)
// System.Object UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * ReactiveDictionary_2_System_Collections_IDictionary_get_Item_m3845755224_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_get_Item_m3845755224(__this, ___key0, method) ((  Il2CppObject * (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_get_Item_m3845755224_gshared)(__this, ___key0, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void ReactiveDictionary_2_System_Collections_IDictionary_set_Item_m4131795399_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_set_Item_m4131795399(__this, ___key0, ___value1, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_set_Item_m4131795399_gshared)(__this, ___key0, ___value1, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool ReactiveDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3341006429_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3341006429(__this, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3341006429_gshared)(__this, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool ReactiveDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2501445884_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2501445884(__this, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2501445884_gshared)(__this, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReactiveDictionary_2_System_Collections_ICollection_get_IsSynchronized_m4157348262_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_ICollection_get_IsSynchronized_m4157348262(__this, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_ICollection_get_IsSynchronized_m4157348262_gshared)(__this, method)
// System.Collections.ICollection UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * ReactiveDictionary_2_System_Collections_IDictionary_get_Keys_m2979180542_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_get_Keys_m2979180542(__this, method) ((  Il2CppObject * (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_get_Keys_m2979180542_gshared)(__this, method)
// System.Object UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReactiveDictionary_2_System_Collections_ICollection_get_SyncRoot_m1280414526_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_ICollection_get_SyncRoot_m1280414526(__this, method) ((  Il2CppObject * (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_ICollection_get_SyncRoot_m1280414526_gshared)(__this, method)
// System.Collections.ICollection UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * ReactiveDictionary_2_System_Collections_IDictionary_get_Values_m616869228_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_get_Values_m616869228(__this, method) ((  Il2CppObject * (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_get_Values_m616869228_gshared)(__this, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3197212266_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3197212266(__this, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3197212266_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TKey> UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* ReactiveDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3250057240_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3250057240(__this, method) ((  Il2CppObject* (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3250057240_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* ReactiveDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2550451892_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2550451892(__this, method) ((  Il2CppObject* (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2550451892_gshared)(__this, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void ReactiveDictionary_2_System_Collections_IDictionary_Add_m2252065834_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_Add_m2252065834(__this, ___key0, ___value1, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_Add_m2252065834_gshared)(__this, ___key0, ___value1, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool ReactiveDictionary_2_System_Collections_IDictionary_Contains_m2542443630_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_Contains_m2542443630(__this, ___key0, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_Contains_m2542443630_gshared)(__this, ___key0, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReactiveDictionary_2_System_Collections_ICollection_CopyTo_m1890308830_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_ICollection_CopyTo_m1890308830(__this, ___array0, ___index1, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Il2CppArray *, int32_t, const MethodInfo*))ReactiveDictionary_2_System_Collections_ICollection_CopyTo_m1890308830_gshared)(__this, ___array0, ___index1, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void ReactiveDictionary_2_System_Collections_IDictionary_Remove_m3993704965_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_Remove_m3993704965(__this, ___key0, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_Remove_m3993704965_gshared)(__this, ___key0, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1132085275_gshared (ReactiveDictionary_2_t2215051344 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1132085275(__this, ___item0, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, KeyValuePair_2_t3312956448 , const MethodInfo*))ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1132085275_gshared)(__this, ___item0, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m727866085_gshared (ReactiveDictionary_2_t2215051344 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m727866085(__this, ___item0, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, KeyValuePair_2_t3312956448 , const MethodInfo*))ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m727866085_gshared)(__this, ___item0, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m796884799_gshared (ReactiveDictionary_2_t2215051344 * __this, KeyValuePair_2U5BU5D_t346249057* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m796884799(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, KeyValuePair_2U5BU5D_t346249057*, int32_t, const MethodInfo*))ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m796884799_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* ReactiveDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2963428042_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2963428042(__this, method) ((  Il2CppObject* (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2963428042_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReactiveDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1508609939_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1508609939(__this, method) ((  Il2CppObject * (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1508609939_gshared)(__this, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m757331786_gshared (ReactiveDictionary_2_t2215051344 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m757331786(__this, ___item0, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, KeyValuePair_2_t3312956448 , const MethodInfo*))ReactiveDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m757331786_gshared)(__this, ___item0, method)
// System.Collections.IDictionaryEnumerator UniRx.ReactiveDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * ReactiveDictionary_2_System_Collections_IDictionary_GetEnumerator_m3871974807_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_System_Collections_IDictionary_GetEnumerator_m3871974807(__this, method) ((  Il2CppObject * (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_System_Collections_IDictionary_GetEnumerator_m3871974807_gshared)(__this, method)
// TValue UniRx.ReactiveDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ReactiveDictionary_2_get_Item_m4169563047_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ReactiveDictionary_2_get_Item_m4169563047(__this, ___key0, method) ((  Il2CppObject * (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_get_Item_m4169563047_gshared)(__this, ___key0, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void ReactiveDictionary_2_set_Item_m1451431014_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReactiveDictionary_2_set_Item_m1451431014(__this, ___key0, ___value1, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_set_Item_m1451431014_gshared)(__this, ___key0, ___value1, method)
// System.Int32 UniRx.ReactiveDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ReactiveDictionary_2_get_Count_m520999200_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_get_Count_m520999200(__this, method) ((  int32_t (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_get_Count_m520999200_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> UniRx.ReactiveDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  KeyCollection_t1852733134 * ReactiveDictionary_2_get_Keys_m232928941_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_get_Keys_m232928941(__this, method) ((  KeyCollection_t1852733134 * (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_get_Keys_m232928941_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> UniRx.ReactiveDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  ValueCollection_t1451594948 * ReactiveDictionary_2_get_Values_m1896117961_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_get_Values_m1896117961(__this, method) ((  ValueCollection_t1451594948 * (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_get_Values_m1896117961_gshared)(__this, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void ReactiveDictionary_2_Add_m2841986639_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReactiveDictionary_2_Add_m2841986639(__this, ___key0, ___value1, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_Add_m2841986639_gshared)(__this, ___key0, ___value1, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void ReactiveDictionary_2_Clear_m3578893649_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_Clear_m3578893649(__this, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_Clear_m3578893649_gshared)(__this, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool ReactiveDictionary_2_Remove_m698487547_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ReactiveDictionary_2_Remove_m698487547(__this, ___key0, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_Remove_m698487547_gshared)(__this, ___key0, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ReactiveDictionary_2_ContainsKey_m2761667797_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ReactiveDictionary_2_ContainsKey_m2761667797(__this, ___key0, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_ContainsKey_m2761667797_gshared)(__this, ___key0, method)
// System.Boolean UniRx.ReactiveDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ReactiveDictionary_2_TryGetValue_m1680107566_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define ReactiveDictionary_2_TryGetValue_m1680107566(__this, ___key0, ___value1, method) ((  bool (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))ReactiveDictionary_2_TryGetValue_m1680107566_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> UniRx.ReactiveDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3591453092  ReactiveDictionary_2_GetEnumerator_m2053127473_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_GetEnumerator_m2053127473(__this, method) ((  Enumerator_t3591453092  (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_GetEnumerator_m2053127473_gshared)(__this, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::Dispose()
extern "C"  void ReactiveDictionary_2_Dispose_m571151331_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_Dispose_m571151331(__this, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_Dispose_m571151331_gshared)(__this, method)
// UniRx.IObservable`1<System.Int32> UniRx.ReactiveDictionary`2<System.Object,System.Object>::ObserveCountChanged()
extern "C"  Il2CppObject* ReactiveDictionary_2_ObserveCountChanged_m1258558368_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_ObserveCountChanged_m1258558368(__this, method) ((  Il2CppObject* (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_ObserveCountChanged_m1258558368_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.ReactiveDictionary`2<System.Object,System.Object>::ObserveReset()
extern "C"  Il2CppObject* ReactiveDictionary_2_ObserveReset_m1953456745_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_ObserveReset_m1953456745(__this, method) ((  Il2CppObject* (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_ObserveReset_m1953456745_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.DictionaryAddEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2<System.Object,System.Object>::ObserveAdd()
extern "C"  Il2CppObject* ReactiveDictionary_2_ObserveAdd_m3896532688_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_ObserveAdd_m3896532688(__this, method) ((  Il2CppObject* (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_ObserveAdd_m3896532688_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.DictionaryRemoveEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2<System.Object,System.Object>::ObserveRemove()
extern "C"  Il2CppObject* ReactiveDictionary_2_ObserveRemove_m3435224978_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_ObserveRemove_m3435224978(__this, method) ((  Il2CppObject* (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_ObserveRemove_m3435224978_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.DictionaryReplaceEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2<System.Object,System.Object>::ObserveReplace()
extern "C"  Il2CppObject* ReactiveDictionary_2_ObserveReplace_m2100391600_gshared (ReactiveDictionary_2_t2215051344 * __this, const MethodInfo* method);
#define ReactiveDictionary_2_ObserveReplace_m2100391600(__this, method) ((  Il2CppObject* (*) (ReactiveDictionary_2_t2215051344 *, const MethodInfo*))ReactiveDictionary_2_ObserveReplace_m2100391600_gshared)(__this, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ReactiveDictionary_2_GetObjectData_m15844804_gshared (ReactiveDictionary_2_t2215051344 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define ReactiveDictionary_2_GetObjectData_m15844804(__this, ___info0, ___context1, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))ReactiveDictionary_2_GetObjectData_m15844804_gshared)(__this, ___info0, ___context1, method)
// System.Void UniRx.ReactiveDictionary`2<System.Object,System.Object>::OnDeserialization(System.Object)
extern "C"  void ReactiveDictionary_2_OnDeserialization_m726630176_gshared (ReactiveDictionary_2_t2215051344 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define ReactiveDictionary_2_OnDeserialization_m726630176(__this, ___sender0, method) ((  void (*) (ReactiveDictionary_2_t2215051344 *, Il2CppObject *, const MethodInfo*))ReactiveDictionary_2_OnDeserialization_m726630176_gshared)(__this, ___sender0, method)
