﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpeedUpWindow/<Init>c__AnonStorey151
struct U3CInitU3Ec__AnonStorey151_t1114672632;
// <>__AnonType5`2<System.DateTime,System.Int64>
struct U3CU3E__AnonType5_2_t3897323094;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void SpeedUpWindow/<Init>c__AnonStorey151::.ctor()
extern "C"  void U3CInitU3Ec__AnonStorey151__ctor_m1377229126 (U3CInitU3Ec__AnonStorey151_t1114672632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpWindow/<Init>c__AnonStorey151::<>m__203(<>__AnonType5`2<System.DateTime,System.Int64>)
extern "C"  void U3CInitU3Ec__AnonStorey151_U3CU3Em__203_m2788341161 (U3CInitU3Ec__AnonStorey151_t1114672632 * __this, U3CU3E__AnonType5_2_t3897323094 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpWindow/<Init>c__AnonStorey151::<>m__204(UniRx.Unit)
extern "C"  void U3CInitU3Ec__AnonStorey151_U3CU3Em__204_m3221209997 (U3CInitU3Ec__AnonStorey151_t1114672632 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
