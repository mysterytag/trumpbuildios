﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<BioProcessor`1/Instruction<System.Object>>
struct List_1_t2038067376;
// Stream`1<System.Object>
struct Stream_1_t3823212738;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BioProcessor`1<System.Object>
struct  BioProcessor_1_t2501661084  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<BioProcessor`1/Instruction<T>> BioProcessor`1::instructions
	List_1_t2038067376 * ___instructions_0;
	// Stream`1<T> BioProcessor`1::output_
	Stream_1_t3823212738 * ___output__1;

public:
	inline static int32_t get_offset_of_instructions_0() { return static_cast<int32_t>(offsetof(BioProcessor_1_t2501661084, ___instructions_0)); }
	inline List_1_t2038067376 * get_instructions_0() const { return ___instructions_0; }
	inline List_1_t2038067376 ** get_address_of_instructions_0() { return &___instructions_0; }
	inline void set_instructions_0(List_1_t2038067376 * value)
	{
		___instructions_0 = value;
		Il2CppCodeGenWriteBarrier(&___instructions_0, value);
	}

	inline static int32_t get_offset_of_output__1() { return static_cast<int32_t>(offsetof(BioProcessor_1_t2501661084, ___output__1)); }
	inline Stream_1_t3823212738 * get_output__1() const { return ___output__1; }
	inline Stream_1_t3823212738 ** get_address_of_output__1() { return &___output__1; }
	inline void set_output__1(Stream_1_t3823212738 * value)
	{
		___output__1 = value;
		Il2CppCodeGenWriteBarrier(&___output__1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
