﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct OperatorObservableBase_1_t91375252;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct IObserver_1_t3239229184;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m711874054_gshared (OperatorObservableBase_1_t91375252 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m711874054(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t91375252 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m711874054_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3166908436_gshared (OperatorObservableBase_1_t91375252 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3166908436(__this, method) ((  bool (*) (OperatorObservableBase_1_t91375252 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3166908436_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m2627411086_gshared (OperatorObservableBase_1_t91375252 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m2627411086(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t91375252 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m2627411086_gshared)(__this, ___observer0, method)
