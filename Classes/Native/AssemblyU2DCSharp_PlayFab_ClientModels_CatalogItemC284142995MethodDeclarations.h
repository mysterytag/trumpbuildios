﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CatalogItemContainerInfo
struct CatalogItemContainerInfo_t284142995;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::.ctor()
extern "C"  void CatalogItemContainerInfo__ctor_m2912062986 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItemContainerInfo::get_KeyItemId()
extern "C"  String_t* CatalogItemContainerInfo_get_KeyItemId_m975719215 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::set_KeyItemId(System.String)
extern "C"  void CatalogItemContainerInfo_set_KeyItemId_m1235375786 (CatalogItemContainerInfo_t284142995 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemContainerInfo::get_ItemContents()
extern "C"  List_1_t1765447871 * CatalogItemContainerInfo_get_ItemContents_m3624435903 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::set_ItemContents(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItemContainerInfo_set_ItemContents_m335383096 (CatalogItemContainerInfo_t284142995 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemContainerInfo::get_ResultTableContents()
extern "C"  List_1_t1765447871 * CatalogItemContainerInfo_get_ResultTableContents_m3221308763 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::set_ResultTableContents(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItemContainerInfo_set_ResultTableContents_m1121499306 (CatalogItemContainerInfo_t284142995 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItemContainerInfo::get_VirtualCurrencyContents()
extern "C"  Dictionary_2_t2623623230 * CatalogItemContainerInfo_get_VirtualCurrencyContents_m3342156843 (CatalogItemContainerInfo_t284142995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemContainerInfo::set_VirtualCurrencyContents(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CatalogItemContainerInfo_set_VirtualCurrencyContents_m2464960506 (CatalogItemContainerInfo_t284142995 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
