﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9A`3<System.Object,System.Object,System.Object>
struct U3CAsObservableU3Ec__AnonStorey9A_3_t1952899366;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t4036358287;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9A`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey9A_3__ctor_m1145347670_gshared (U3CAsObservableU3Ec__AnonStorey9A_3_t1952899366 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey9A_3__ctor_m1145347670(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey9A_3_t1952899366 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey9A_3__ctor_m1145347670_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9A`3<System.Object,System.Object,System.Object>::<>m__D0(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey9A_3_U3CU3Em__D0_m3774822950_gshared (U3CAsObservableU3Ec__AnonStorey9A_3_t1952899366 * __this, UnityAction_3_t4036358287 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey9A_3_U3CU3Em__D0_m3774822950(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey9A_3_t1952899366 *, UnityAction_3_t4036358287 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey9A_3_U3CU3Em__D0_m3774822950_gshared)(__this, ___h0, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9A`3<System.Object,System.Object,System.Object>::<>m__D1(UnityEngine.Events.UnityAction`3<T0,T1,T2>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey9A_3_U3CU3Em__D1_m2074082471_gshared (U3CAsObservableU3Ec__AnonStorey9A_3_t1952899366 * __this, UnityAction_3_t4036358287 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey9A_3_U3CU3Em__D1_m2074082471(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey9A_3_t1952899366 *, UnityAction_3_t4036358287 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey9A_3_U3CU3Em__D1_m2074082471_gshared)(__this, ___h0, method)
