﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<TableButtons/StatkaPoTableViev>
struct Subscription_t1287642339;
// UniRx.Subject`1<TableButtons/StatkaPoTableViev>
struct Subject_1_t1151463141;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>
struct IObserver_1_t1425427424;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<TableButtons/StatkaPoTableViev>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1508639718_gshared (Subscription_t1287642339 * __this, Subject_1_t1151463141 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1508639718(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t1287642339 *, Subject_1_t1151463141 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1508639718_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<TableButtons/StatkaPoTableViev>::Dispose()
extern "C"  void Subscription_Dispose_m189280848_gshared (Subscription_t1287642339 * __this, const MethodInfo* method);
#define Subscription_Dispose_m189280848(__this, method) ((  void (*) (Subscription_t1287642339 *, const MethodInfo*))Subscription_Dispose_m189280848_gshared)(__this, method)
