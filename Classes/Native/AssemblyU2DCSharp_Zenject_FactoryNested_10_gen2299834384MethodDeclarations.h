﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_10_t2299834384;
// Zenject.IFactory`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_9_t1437178096;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`9<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TConcrete>)
extern "C"  void FactoryNested_10__ctor_m1934166616_gshared (FactoryNested_10_t2299834384 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_10__ctor_m1934166616(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_10_t2299834384 *, Il2CppObject*, const MethodInfo*))FactoryNested_10__ctor_m1934166616_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8)
extern "C"  Il2CppObject * FactoryNested_10_Create_m1507100581_gshared (FactoryNested_10_t2299834384 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, const MethodInfo* method);
#define FactoryNested_10_Create_m1507100581(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, method) ((  Il2CppObject * (*) (FactoryNested_10_t2299834384 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryNested_10_Create_m1507100581_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, method)
