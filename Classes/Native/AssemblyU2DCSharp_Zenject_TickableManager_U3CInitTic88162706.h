﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.ITickable
struct ITickable_t789512021;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager/<InitTickables>c__AnonStorey193
struct  U3CInitTickablesU3Ec__AnonStorey193_t88162706  : public Il2CppObject
{
public:
	// Zenject.ITickable Zenject.TickableManager/<InitTickables>c__AnonStorey193::tickable
	Il2CppObject * ___tickable_0;

public:
	inline static int32_t get_offset_of_tickable_0() { return static_cast<int32_t>(offsetof(U3CInitTickablesU3Ec__AnonStorey193_t88162706, ___tickable_0)); }
	inline Il2CppObject * get_tickable_0() const { return ___tickable_0; }
	inline Il2CppObject ** get_address_of_tickable_0() { return &___tickable_0; }
	inline void set_tickable_0(Il2CppObject * value)
	{
		___tickable_0 = value;
		Il2CppCodeGenWriteBarrier(&___tickable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
