﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>
struct U3CJoinU3Ec__AnonStorey114_1_t4163512910;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey114_1__ctor_m3246169122_gshared (U3CJoinU3Ec__AnonStorey114_1_t4163512910 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey114_1__ctor_m3246169122(__this, method) ((  void (*) (U3CJoinU3Ec__AnonStorey114_1_t4163512910 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey114_1__ctor_m3246169122_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>::<>m__188(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m266671239_gshared (U3CJoinU3Ec__AnonStorey114_1_t4163512910 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m266671239(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CJoinU3Ec__AnonStorey114_1_t4163512910 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m266671239_gshared)(__this, ___reaction0, ___p1, method)
// T CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>::<>m__189()
extern "C"  int32_t U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__189_m3748842248_gshared (U3CJoinU3Ec__AnonStorey114_1_t4163512910 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__189_m3748842248(__this, method) ((  int32_t (*) (U3CJoinU3Ec__AnonStorey114_1_t4163512910 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__189_m3748842248_gshared)(__this, method)
