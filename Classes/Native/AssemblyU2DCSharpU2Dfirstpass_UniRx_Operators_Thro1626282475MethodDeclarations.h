﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6B__ctor_m161005097_gshared (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey6B__ctor_m161005097(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey6B__ctor_m161005097_gshared)(__this, method)
// System.Void UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<System.Object>::<>m__89()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6B_U3CU3Em__89_m2415332723_gshared (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey6B_U3CU3Em__89_m2415332723(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey6B_t1626282475 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey6B_U3CU3Em__89_m2415332723_gshared)(__this, method)
