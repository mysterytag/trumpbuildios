﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioProcessor`1/DisposableInstruction<System.Object>
struct DisposableInstruction_t393830967;

#include "codegen/il2cpp-codegen.h"

// System.Void BioProcessor`1/DisposableInstruction<System.Object>::.ctor()
extern "C"  void DisposableInstruction__ctor_m635928393_gshared (DisposableInstruction_t393830967 * __this, const MethodInfo* method);
#define DisposableInstruction__ctor_m635928393(__this, method) ((  void (*) (DisposableInstruction_t393830967 *, const MethodInfo*))DisposableInstruction__ctor_m635928393_gshared)(__this, method)
// System.Void BioProcessor`1/DisposableInstruction<System.Object>::Dispose()
extern "C"  void DisposableInstruction_Dispose_m1140112710_gshared (DisposableInstruction_t393830967 * __this, const MethodInfo* method);
#define DisposableInstruction_Dispose_m1140112710(__this, method) ((  void (*) (DisposableInstruction_t393830967 *, const MethodInfo*))DisposableInstruction_Dispose_m1140112710_gshared)(__this, method)
