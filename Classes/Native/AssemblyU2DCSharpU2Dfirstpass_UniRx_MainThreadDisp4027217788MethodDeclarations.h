﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.MainThreadDispatcher
struct MainThreadDispatcher_t4027217788;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t437523947;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.String
struct String_t;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp4027217788.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.MainThreadDispatcher::.ctor()
extern "C"  void MainThreadDispatcher__ctor_m453573773 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::.cctor()
extern "C"  void MainThreadDispatcher__cctor_m693788864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::Post(System.Action`1<System.Object>,System.Object)
extern "C"  void MainThreadDispatcher_Post_m2114450895 (Il2CppObject * __this /* static, unused */, Action_1_t985559125 * ___action0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::Send(System.Action`1<System.Object>,System.Object)
extern "C"  void MainThreadDispatcher_Send_m3742193335 (Il2CppObject * __this /* static, unused */, Action_1_t985559125 * ___action0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::UnsafeSend(System.Action)
extern "C"  void MainThreadDispatcher_UnsafeSend_m2650599480 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::UnsafeSend(System.Action`1<System.Object>,System.Object)
extern "C"  void MainThreadDispatcher_UnsafeSend_m3257912957 (Il2CppObject * __this /* static, unused */, Action_1_t985559125 * ___action0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::SendStartCoroutine(System.Collections.IEnumerator)
extern "C"  void MainThreadDispatcher_SendStartCoroutine_m2702036384 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UniRx.MainThreadDispatcher::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2246592261 * MainThreadDispatcher_StartCoroutine_m3648496618 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::RegisterUnhandledExceptionCallback(System.Action`1<System.Exception>)
extern "C"  void MainThreadDispatcher_RegisterUnhandledExceptionCallback_m1658193775 (Il2CppObject * __this /* static, unused */, Action_1_t2115686693 * ___exceptionCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.MainThreadDispatcher::get_InstanceName()
extern "C"  String_t* MainThreadDispatcher_get_InstanceName_m1664795843 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.MainThreadDispatcher::get_IsInitialized()
extern "C"  bool MainThreadDispatcher_get_IsInitialized_m500759408 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.MainThreadDispatcher UniRx.MainThreadDispatcher::get_Instance()
extern "C"  MainThreadDispatcher_t4027217788 * MainThreadDispatcher_get_Instance_m2309518296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::Initialize()
extern "C"  void MainThreadDispatcher_Initialize_m3023841831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::Awake()
extern "C"  void MainThreadDispatcher_Awake_m691178992 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::DestroyDispatcher(UniRx.MainThreadDispatcher)
extern "C"  void MainThreadDispatcher_DestroyDispatcher_m1615739690 (Il2CppObject * __this /* static, unused */, MainThreadDispatcher_t4027217788 * ___aDispatcher0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::CullAllExcessDispatchers()
extern "C"  void MainThreadDispatcher_CullAllExcessDispatchers_m3500057855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::OnDestroy()
extern "C"  void MainThreadDispatcher_OnDestroy_m3451896582 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::Update()
extern "C"  void MainThreadDispatcher_Update_m2902747168 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.MainThreadDispatcher::UpdateAsObservable()
extern "C"  Il2CppObject* MainThreadDispatcher_UpdateAsObservable_m46968669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::LateUpdate()
extern "C"  void MainThreadDispatcher_LateUpdate_m4171570662 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.MainThreadDispatcher::LateUpdateAsObservable()
extern "C"  Il2CppObject* MainThreadDispatcher_LateUpdateAsObservable_m4009347107 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::OnApplicationFocus(System.Boolean)
extern "C"  void MainThreadDispatcher_OnApplicationFocus_m1250469173 (MainThreadDispatcher_t4027217788 * __this, bool ___focus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Boolean> UniRx.MainThreadDispatcher::OnApplicationFocusAsObservable()
extern "C"  Il2CppObject* MainThreadDispatcher_OnApplicationFocusAsObservable_m251107528 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::OnApplicationPause(System.Boolean)
extern "C"  void MainThreadDispatcher_OnApplicationPause_m2704636499 (MainThreadDispatcher_t4027217788 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Boolean> UniRx.MainThreadDispatcher::OnApplicationPauseAsObservable()
extern "C"  Il2CppObject* MainThreadDispatcher_OnApplicationPauseAsObservable_m1450384742 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::OnApplicationQuit()
extern "C"  void MainThreadDispatcher_OnApplicationQuit_m346160779 (MainThreadDispatcher_t4027217788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.MainThreadDispatcher::OnApplicationQuitAsObservable()
extern "C"  Il2CppObject* MainThreadDispatcher_OnApplicationQuitAsObservable_m1999083704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher::<unhandledExceptionCallback>m__A8(System.Exception)
extern "C"  void MainThreadDispatcher_U3CunhandledExceptionCallbackU3Em__A8_m3817152526 (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
