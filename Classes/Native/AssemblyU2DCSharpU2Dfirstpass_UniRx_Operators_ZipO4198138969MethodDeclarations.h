﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_7_t4198138969;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_7_t396393786;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ZipObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.IObservable`1<T5>,UniRx.IObservable`1<T6>,UniRx.Operators.ZipFunc`7<T1,T2,T3,T4,T5,T6,TR>)
extern "C"  void ZipObservable_7__ctor_m2440111902_gshared (ZipObservable_7_t4198138969 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, Il2CppObject* ___source54, Il2CppObject* ___source65, ZipFunc_7_t396393786 * ___resultSelector6, const MethodInfo* method);
#define ZipObservable_7__ctor_m2440111902(__this, ___source10, ___source21, ___source32, ___source43, ___source54, ___source65, ___resultSelector6, method) ((  void (*) (ZipObservable_7_t4198138969 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, ZipFunc_7_t396393786 *, const MethodInfo*))ZipObservable_7__ctor_m2440111902_gshared)(__this, ___source10, ___source21, ___source32, ___source43, ___source54, ___source65, ___resultSelector6, method)
// System.IDisposable UniRx.Operators.ZipObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * ZipObservable_7_SubscribeCore_m123839477_gshared (ZipObservable_7_t4198138969 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ZipObservable_7_SubscribeCore_m123839477(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ZipObservable_7_t4198138969 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipObservable_7_SubscribeCore_m123839477_gshared)(__this, ___observer0, ___cancel1, method)
