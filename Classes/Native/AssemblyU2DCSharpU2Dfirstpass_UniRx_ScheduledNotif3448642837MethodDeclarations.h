﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Object>
struct U3CReportU3Ec__AnonStorey58_t3448642837;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Object>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey58__ctor_m2356660748_gshared (U3CReportU3Ec__AnonStorey58_t3448642837 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey58__ctor_m2356660748(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey58_t3448642837 *, const MethodInfo*))U3CReportU3Ec__AnonStorey58__ctor_m2356660748_gshared)(__this, method)
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Object>::<>m__6C()
extern "C"  void U3CReportU3Ec__AnonStorey58_U3CU3Em__6C_m3611421026_gshared (U3CReportU3Ec__AnonStorey58_t3448642837 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey58_U3CU3Em__6C_m3611421026(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey58_t3448642837 *, const MethodInfo*))U3CReportU3Ec__AnonStorey58_U3CU3Em__6C_m3611421026_gshared)(__this, method)
