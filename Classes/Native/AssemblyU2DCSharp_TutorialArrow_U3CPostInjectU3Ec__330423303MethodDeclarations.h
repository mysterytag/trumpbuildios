﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialArrow/<PostInject>c__AnonStorey16B
struct U3CPostInjectU3Ec__AnonStorey16B_t330423303;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void TutorialArrow/<PostInject>c__AnonStorey16B::.ctor()
extern "C"  void U3CPostInjectU3Ec__AnonStorey16B__ctor_m1283518608 (U3CPostInjectU3Ec__AnonStorey16B_t330423303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialArrow/<PostInject>c__AnonStorey16B::<>m__27E(UnityEngine.GameObject)
extern "C"  void U3CPostInjectU3Ec__AnonStorey16B_U3CU3Em__27E_m1605901057 (U3CPostInjectU3Ec__AnonStorey16B_t330423303 * __this, GameObject_t4012695102 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
