﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.RegionInfo
struct  RegionInfo_t3474869746  : public Il2CppObject
{
public:
	// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.RegionInfo::<Region>k__BackingField
	Nullable_1_t212373688  ___U3CRegionU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.RegionInfo::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Boolean PlayFab.ClientModels.RegionInfo::<Available>k__BackingField
	bool ___U3CAvailableU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.RegionInfo::<PingUrl>k__BackingField
	String_t* ___U3CPingUrlU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RegionInfo_t3474869746, ___U3CRegionU3Ek__BackingField_0)); }
	inline Nullable_1_t212373688  get_U3CRegionU3Ek__BackingField_0() const { return ___U3CRegionU3Ek__BackingField_0; }
	inline Nullable_1_t212373688 * get_address_of_U3CRegionU3Ek__BackingField_0() { return &___U3CRegionU3Ek__BackingField_0; }
	inline void set_U3CRegionU3Ek__BackingField_0(Nullable_1_t212373688  value)
	{
		___U3CRegionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegionInfo_t3474869746, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CAvailableU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RegionInfo_t3474869746, ___U3CAvailableU3Ek__BackingField_2)); }
	inline bool get_U3CAvailableU3Ek__BackingField_2() const { return ___U3CAvailableU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CAvailableU3Ek__BackingField_2() { return &___U3CAvailableU3Ek__BackingField_2; }
	inline void set_U3CAvailableU3Ek__BackingField_2(bool value)
	{
		___U3CAvailableU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPingUrlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RegionInfo_t3474869746, ___U3CPingUrlU3Ek__BackingField_3)); }
	inline String_t* get_U3CPingUrlU3Ek__BackingField_3() const { return ___U3CPingUrlU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CPingUrlU3Ek__BackingField_3() { return &___U3CPingUrlU3Ek__BackingField_3; }
	inline void set_U3CPingUrlU3Ek__BackingField_3(String_t* value)
	{
		___U3CPingUrlU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPingUrlU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
