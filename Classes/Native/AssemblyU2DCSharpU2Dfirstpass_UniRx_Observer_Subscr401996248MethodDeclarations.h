﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe`1<UnityEngine.Color>
struct Subscribe_1_t401996248;
// System.Action`1<UnityEngine.Color>
struct Action_1_t1736628465;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe`1<UnityEngine.Color>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m2162337070_gshared (Subscribe_1_t401996248 * __this, Action_1_t1736628465 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define Subscribe_1__ctor_m2162337070(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (Subscribe_1_t401996248 *, Action_1_t1736628465 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe_1__ctor_m2162337070_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
// System.Void UniRx.Observer/Subscribe`1<UnityEngine.Color>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m948714507_gshared (Subscribe_1_t401996248 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define Subscribe_1_OnNext_m948714507(__this, ___value0, method) ((  void (*) (Subscribe_1_t401996248 *, Color_t1588175760 , const MethodInfo*))Subscribe_1_OnNext_m948714507_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe`1<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void Subscribe_1_OnError_m3542409498_gshared (Subscribe_1_t401996248 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe_1_OnError_m3542409498(__this, ___error0, method) ((  void (*) (Subscribe_1_t401996248 *, Exception_t1967233988 *, const MethodInfo*))Subscribe_1_OnError_m3542409498_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe`1<UnityEngine.Color>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m3048794989_gshared (Subscribe_1_t401996248 * __this, const MethodInfo* method);
#define Subscribe_1_OnCompleted_m3048794989(__this, method) ((  void (*) (Subscribe_1_t401996248 *, const MethodInfo*))Subscribe_1_OnCompleted_m3048794989_gshared)(__this, method)
