﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ZipLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_5_t3111587102;
// System.Object
struct Il2CppObject;
// UniRx.Operators.ZipLatestObserver`1<System.Object>
struct ZipLatestObserver_1_t426653717;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthZ3084363628.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipLatestObservable`5/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatest_t4103764977  : public NthZipLatestObserverBase_1_t3084363628
{
public:
	// UniRx.Operators.ZipLatestObservable`5<T1,T2,T3,T4,TR> UniRx.Operators.ZipLatestObservable`5/ZipLatest::parent
	ZipLatestObservable_5_t3111587102 * ___parent_5;
	// System.Object UniRx.Operators.ZipLatestObservable`5/ZipLatest::gate
	Il2CppObject * ___gate_6;
	// UniRx.Operators.ZipLatestObserver`1<T1> UniRx.Operators.ZipLatestObservable`5/ZipLatest::c1
	ZipLatestObserver_1_t426653717 * ___c1_7;
	// UniRx.Operators.ZipLatestObserver`1<T2> UniRx.Operators.ZipLatestObservable`5/ZipLatest::c2
	ZipLatestObserver_1_t426653717 * ___c2_8;
	// UniRx.Operators.ZipLatestObserver`1<T3> UniRx.Operators.ZipLatestObservable`5/ZipLatest::c3
	ZipLatestObserver_1_t426653717 * ___c3_9;
	// UniRx.Operators.ZipLatestObserver`1<T4> UniRx.Operators.ZipLatestObservable`5/ZipLatest::c4
	ZipLatestObserver_1_t426653717 * ___c4_10;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(ZipLatest_t4103764977, ___parent_5)); }
	inline ZipLatestObservable_5_t3111587102 * get_parent_5() const { return ___parent_5; }
	inline ZipLatestObservable_5_t3111587102 ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipLatestObservable_5_t3111587102 * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier(&___parent_5, value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(ZipLatest_t4103764977, ___gate_6)); }
	inline Il2CppObject * get_gate_6() const { return ___gate_6; }
	inline Il2CppObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(Il2CppObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier(&___gate_6, value);
	}

	inline static int32_t get_offset_of_c1_7() { return static_cast<int32_t>(offsetof(ZipLatest_t4103764977, ___c1_7)); }
	inline ZipLatestObserver_1_t426653717 * get_c1_7() const { return ___c1_7; }
	inline ZipLatestObserver_1_t426653717 ** get_address_of_c1_7() { return &___c1_7; }
	inline void set_c1_7(ZipLatestObserver_1_t426653717 * value)
	{
		___c1_7 = value;
		Il2CppCodeGenWriteBarrier(&___c1_7, value);
	}

	inline static int32_t get_offset_of_c2_8() { return static_cast<int32_t>(offsetof(ZipLatest_t4103764977, ___c2_8)); }
	inline ZipLatestObserver_1_t426653717 * get_c2_8() const { return ___c2_8; }
	inline ZipLatestObserver_1_t426653717 ** get_address_of_c2_8() { return &___c2_8; }
	inline void set_c2_8(ZipLatestObserver_1_t426653717 * value)
	{
		___c2_8 = value;
		Il2CppCodeGenWriteBarrier(&___c2_8, value);
	}

	inline static int32_t get_offset_of_c3_9() { return static_cast<int32_t>(offsetof(ZipLatest_t4103764977, ___c3_9)); }
	inline ZipLatestObserver_1_t426653717 * get_c3_9() const { return ___c3_9; }
	inline ZipLatestObserver_1_t426653717 ** get_address_of_c3_9() { return &___c3_9; }
	inline void set_c3_9(ZipLatestObserver_1_t426653717 * value)
	{
		___c3_9 = value;
		Il2CppCodeGenWriteBarrier(&___c3_9, value);
	}

	inline static int32_t get_offset_of_c4_10() { return static_cast<int32_t>(offsetof(ZipLatest_t4103764977, ___c4_10)); }
	inline ZipLatestObserver_1_t426653717 * get_c4_10() const { return ___c4_10; }
	inline ZipLatestObserver_1_t426653717 ** get_address_of_c4_10() { return &___c4_10; }
	inline void set_c4_10(ZipLatestObserver_1_t426653717 * value)
	{
		___c4_10 = value;
		Il2CppCodeGenWriteBarrier(&___c4_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
