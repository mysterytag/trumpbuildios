﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>
struct Action_1_t3439808249;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m384607952_gshared (Action_1_t3439808249 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m384607952(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t3439808249 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m384607952_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>::Invoke(T)
extern "C"  void Action_1_Invoke_m2783359860_gshared (Action_1_t3439808249 * __this, int32_t ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m2783359860(__this, ___obj0, method) ((  void (*) (Action_1_t3439808249 *, int32_t, const MethodInfo*))Action_1_Invoke_m2783359860_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1758067521_gshared (Action_1_t3439808249 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m1758067521(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t3439808249 *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1758067521_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3670278368_gshared (Action_1_t3439808249 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m3670278368(__this, ___result0, method) ((  void (*) (Action_1_t3439808249 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3670278368_gshared)(__this, ___result0, method)
