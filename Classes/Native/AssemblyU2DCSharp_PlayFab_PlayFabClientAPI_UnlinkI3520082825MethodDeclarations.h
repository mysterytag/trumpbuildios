﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDRequestCallback
struct UnlinkIOSDeviceIDRequestCallback_t3520082825;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkIOSDeviceIDRequest
struct UnlinkIOSDeviceIDRequest_t1716723508;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkIOSDe1716723508.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkIOSDeviceIDRequestCallback__ctor_m2772198392 (UnlinkIOSDeviceIDRequestCallback_t3520082825 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkIOSDeviceIDRequest,System.Object)
extern "C"  void UnlinkIOSDeviceIDRequestCallback_Invoke_m844298943 (UnlinkIOSDeviceIDRequestCallback_t3520082825 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkIOSDeviceIDRequest_t1716723508 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkIOSDeviceIDRequestCallback_t3520082825(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkIOSDeviceIDRequest_t1716723508 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkIOSDeviceIDRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkIOSDeviceIDRequestCallback_BeginInvoke_m824685240 (UnlinkIOSDeviceIDRequestCallback_t3520082825 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkIOSDeviceIDRequest_t1716723508 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkIOSDeviceIDRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkIOSDeviceIDRequestCallback_EndInvoke_m2979041800 (UnlinkIOSDeviceIDRequestCallback_t3520082825 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
