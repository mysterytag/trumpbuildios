﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// StreamAPI/<ToEmpty>c__AnonStorey132`1<System.Object>
struct U3CToEmptyU3Ec__AnonStorey132_1_t3045407339;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1<System.Object>
struct  U3CToEmptyU3Ec__AnonStorey133_1_t2600382340  : public Il2CppObject
{
public:
	// System.Action StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1::reaction
	Action_t437523947 * ___reaction_0;
	// StreamAPI/<ToEmpty>c__AnonStorey132`1<T> StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1::<>f__ref$306
	U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 * ___U3CU3Ef__refU24306_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CToEmptyU3Ec__AnonStorey133_1_t2600382340, ___reaction_0)); }
	inline Action_t437523947 * get_reaction_0() const { return ___reaction_0; }
	inline Action_t437523947 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_t437523947 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24306_1() { return static_cast<int32_t>(offsetof(U3CToEmptyU3Ec__AnonStorey133_1_t2600382340, ___U3CU3Ef__refU24306_1)); }
	inline U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 * get_U3CU3Ef__refU24306_1() const { return ___U3CU3Ef__refU24306_1; }
	inline U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 ** get_address_of_U3CU3Ef__refU24306_1() { return &___U3CU3Ef__refU24306_1; }
	inline void set_U3CU3Ef__refU24306_1(U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 * value)
	{
		___U3CU3Ef__refU24306_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24306_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
