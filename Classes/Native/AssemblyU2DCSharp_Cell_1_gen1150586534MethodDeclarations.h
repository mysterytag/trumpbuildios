﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Cell_1_gen1019204052MethodDeclarations.h"

// System.Void Cell`1<System.String>::.ctor()
#define Cell_1__ctor_m3408303028(__this, method) ((  void (*) (Cell_1_t1150586534 *, const MethodInfo*))Cell_1__ctor_m1701983586_gshared)(__this, method)
// System.Void Cell`1<System.String>::.ctor(T)
#define Cell_1__ctor_m2578180138(__this, ___initial0, method) ((  void (*) (Cell_1_t1150586534 *, String_t*, const MethodInfo*))Cell_1__ctor_m1221884988_gshared)(__this, ___initial0, method)
// System.Void Cell`1<System.String>::.ctor(ICell`1<T>)
#define Cell_1__ctor_m1822913386(__this, ___other0, method) ((  void (*) (Cell_1_t1150586534 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m3977204632_gshared)(__this, ___other0, method)
// System.Void Cell`1<System.String>::SetInputCell(ICell`1<T>)
#define Cell_1_SetInputCell_m2887795664(__this, ___cell0, method) ((  void (*) (Cell_1_t1150586534 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m3952243682_gshared)(__this, ___cell0, method)
// T Cell`1<System.String>::get_value()
#define Cell_1_get_value_m3548054905(__this, method) ((  String_t* (*) (Cell_1_t1150586534 *, const MethodInfo*))Cell_1_get_value_m916594727_gshared)(__this, method)
// System.Void Cell`1<System.String>::set_value(T)
#define Cell_1_set_value_m541426520(__this, ___value0, method) ((  void (*) (Cell_1_t1150586534 *, String_t*, const MethodInfo*))Cell_1_set_value_m570539626_gshared)(__this, ___value0, method)
// System.Void Cell`1<System.String>::Set(T)
#define Cell_1_Set_m478265066(__this, ___val0, method) ((  void (*) (Cell_1_t1150586534 *, String_t*, const MethodInfo*))Cell_1_Set_m1115959164_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<System.String>::OnChanged(System.Action,Priority)
#define Cell_1_OnChanged_m3996914271(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1150586534 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m3781809805_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<System.String>::get_valueObject()
#define Cell_1_get_valueObject_m532398670(__this, method) ((  Il2CppObject * (*) (Cell_1_t1150586534 *, const MethodInfo*))Cell_1_get_valueObject_m2686689916_gshared)(__this, method)
// System.IDisposable Cell`1<System.String>::ListenUpdates(System.Action`1<T>,Priority)
#define Cell_1_ListenUpdates_m1824813402(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1150586534 *, Action_1_t1116941607 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m3691348204_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<System.String>::get_updates()
#define Cell_1_get_updates_m4178879844(__this, method) ((  Il2CppObject* (*) (Cell_1_t1150586534 *, const MethodInfo*))Cell_1_get_updates_m786418834_gshared)(__this, method)
// System.IDisposable Cell`1<System.String>::Bind(System.Action`1<T>,Priority)
#define Cell_1_Bind_m3428011944(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1150586534 *, Action_1_t1116941607 *, int32_t, const MethodInfo*))Cell_1_Bind_m3212907478_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<System.String>::SetInputStream(IStream`1<T>)
#define Cell_1_SetInputStream_m3551253708(__this, ___stream0, method) ((  void (*) (Cell_1_t1150586534 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m2944642014_gshared)(__this, ___stream0, method)
// System.Void Cell`1<System.String>::ResetInputStream()
#define Cell_1_ResetInputStream_m711418091(__this, method) ((  void (*) (Cell_1_t1150586534 *, const MethodInfo*))Cell_1_ResetInputStream_m3069937277_gshared)(__this, method)
// System.Void Cell`1<System.String>::TransactionIterationFinished()
#define Cell_1_TransactionIterationFinished_m548272385(__this, method) ((  void (*) (Cell_1_t1150586534 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m1709986707_gshared)(__this, method)
// System.Void Cell`1<System.String>::Unpack(System.Int32)
#define Cell_1_Unpack_m2252801363(__this, ___p0, method) ((  void (*) (Cell_1_t1150586534 *, int32_t, const MethodInfo*))Cell_1_Unpack_m1047006821_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<System.String>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
#define Cell_1_op_LessThanOrEqual_m1846327526(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t1150586534 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m4088946964_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<System.String>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
#define Cell_1_op_GreaterThanOrEqual_m2674332641(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t1150586534 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m38753523_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
