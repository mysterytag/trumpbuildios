﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2612498711(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3615068783 *, Il2CppObject *, Action_1_t985559125 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>::get_Key()
#define KeyValuePair_2_get_Key_m2260647025(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3615068783 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1470707378(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3615068783 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>::get_Value()
#define KeyValuePair_2_get_Value_m3775083349(__this, method) ((  Action_1_t985559125 * (*) (KeyValuePair_2_t3615068783 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1380311474(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3615068783 *, Action_1_t985559125 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>::ToString()
#define KeyValuePair_2_ToString_m4242888022(__this, method) ((  String_t* (*) (KeyValuePair_2_t3615068783 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
