﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Double,System.Object>
struct ShimEnumerator_t1996939764;
// System.Collections.Generic.Dictionary`2<System.Double,System.Object>
struct Dictionary_2_t566215076;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Double,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4045193612_gshared (ShimEnumerator_t1996939764 * __this, Dictionary_2_t566215076 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m4045193612(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1996939764 *, Dictionary_2_t566215076 *, const MethodInfo*))ShimEnumerator__ctor_m4045193612_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Double,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2058849557_gshared (ShimEnumerator_t1996939764 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2058849557(__this, method) ((  bool (*) (ShimEnumerator_t1996939764 *, const MethodInfo*))ShimEnumerator_MoveNext_m2058849557_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Double,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t130027246  ShimEnumerator_get_Entry_m4020605279_gshared (ShimEnumerator_t1996939764 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m4020605279(__this, method) ((  DictionaryEntry_t130027246  (*) (ShimEnumerator_t1996939764 *, const MethodInfo*))ShimEnumerator_get_Entry_m4020605279_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Double,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4089262330_gshared (ShimEnumerator_t1996939764 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m4089262330(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1996939764 *, const MethodInfo*))ShimEnumerator_get_Key_m4089262330_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Double,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m934459660_gshared (ShimEnumerator_t1996939764 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m934459660(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1996939764 *, const MethodInfo*))ShimEnumerator_get_Value_m934459660_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Double,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1792259220_gshared (ShimEnumerator_t1996939764 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1792259220(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1996939764 *, const MethodInfo*))ShimEnumerator_get_Current_m1792259220_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Double,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3498166750_gshared (ShimEnumerator_t1996939764 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3498166750(__this, method) ((  void (*) (ShimEnumerator_t1996939764 *, const MethodInfo*))ShimEnumerator_Reset_m3498166750_gshared)(__this, method)
