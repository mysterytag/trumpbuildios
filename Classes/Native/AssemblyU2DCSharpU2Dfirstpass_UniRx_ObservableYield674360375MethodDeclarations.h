﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableYieldInstruction`1<System.Int32>
struct ObservableYieldInstruction_1_t674360375;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObservableYieldInstruction`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void ObservableYieldInstruction_1__ctor_m297500463_gshared (ObservableYieldInstruction_1_t674360375 * __this, Il2CppObject* ___source0, bool ___reThrowOnError1, const MethodInfo* method);
#define ObservableYieldInstruction_1__ctor_m297500463(__this, ___source0, ___reThrowOnError1, method) ((  void (*) (ObservableYieldInstruction_1_t674360375 *, Il2CppObject*, bool, const MethodInfo*))ObservableYieldInstruction_1__ctor_m297500463_gshared)(__this, ___source0, ___reThrowOnError1, method)
// T UniRx.ObservableYieldInstruction`1<System.Int32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int32_t ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1920955096_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1920955096(__this, method) ((  int32_t (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1920955096_gshared)(__this, method)
// System.Object UniRx.ObservableYieldInstruction`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m2828006369_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m2828006369(__this, method) ((  Il2CppObject * (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m2828006369_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int32>::System.Collections.IEnumerator.MoveNext()
extern "C"  bool ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m994877134_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m994877134(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m994877134_gshared)(__this, method)
// System.Void UniRx.ObservableYieldInstruction`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m1030064427_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m1030064427(__this, method) ((  void (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m1030064427_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int32>::get_HasError()
extern "C"  bool ObservableYieldInstruction_1_get_HasError_m3158714280_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_HasError_m3158714280(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_get_HasError_m3158714280_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int32>::get_HasResult()
extern "C"  bool ObservableYieldInstruction_1_get_HasResult_m1699011359_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_HasResult_m1699011359(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_get_HasResult_m1699011359_gshared)(__this, method)
// T UniRx.ObservableYieldInstruction`1<System.Int32>::get_Result()
extern "C"  int32_t ObservableYieldInstruction_1_get_Result_m497384538_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_Result_m497384538(__this, method) ((  int32_t (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_get_Result_m497384538_gshared)(__this, method)
// System.Exception UniRx.ObservableYieldInstruction`1<System.Int32>::get_Error()
extern "C"  Exception_t1967233988 * ObservableYieldInstruction_1_get_Error_m2986063095_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_Error_m2986063095(__this, method) ((  Exception_t1967233988 * (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_get_Error_m2986063095_gshared)(__this, method)
// System.Void UniRx.ObservableYieldInstruction`1<System.Int32>::Dispose()
extern "C"  void ObservableYieldInstruction_1_Dispose_m4212136556_gshared (ObservableYieldInstruction_1_t674360375 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_Dispose_m4212136556(__this, method) ((  void (*) (ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ObservableYieldInstruction_1_Dispose_m4212136556_gshared)(__this, method)
