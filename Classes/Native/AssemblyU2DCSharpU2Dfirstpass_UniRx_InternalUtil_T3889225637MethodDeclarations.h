﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ThreadSafeQueueWorker
struct ThreadSafeQueueWorker_t3889225637;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.InternalUtil.ThreadSafeQueueWorker::.ctor()
extern "C"  void ThreadSafeQueueWorker__ctor_m3609458254 (ThreadSafeQueueWorker_t3889225637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InternalUtil.ThreadSafeQueueWorker::Enqueue(System.Action`1<System.Object>,System.Object)
extern "C"  void ThreadSafeQueueWorker_Enqueue_m1680239436 (ThreadSafeQueueWorker_t3889225637 * __this, Action_1_t985559125 * ___action0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.InternalUtil.ThreadSafeQueueWorker::ExecuteAll(System.Action`1<System.Exception>)
extern "C"  void ThreadSafeQueueWorker_ExecuteAll_m429924344 (ThreadSafeQueueWorker_t3889225637 * __this, Action_1_t2115686693 * ___unhandledExceptionCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
