﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>
struct ShimEnumerator_t2970491004;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t1539766316;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3389871762_gshared (ShimEnumerator_t2970491004 * __this, Dictionary_2_t1539766316 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3389871762(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2970491004 *, Dictionary_2_t1539766316 *, const MethodInfo*))ShimEnumerator__ctor_m3389871762_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2640423443_gshared (ShimEnumerator_t2970491004 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2640423443(__this, method) ((  bool (*) (ShimEnumerator_t2970491004 *, const MethodInfo*))ShimEnumerator_MoveNext_m2640423443_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Entry()
extern "C"  DictionaryEntry_t130027246  ShimEnumerator_get_Entry_m2555964791_gshared (ShimEnumerator_t2970491004 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2555964791(__this, method) ((  DictionaryEntry_t130027246  (*) (ShimEnumerator_t2970491004 *, const MethodInfo*))ShimEnumerator_get_Entry_m2555964791_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1258156406_gshared (ShimEnumerator_t2970491004 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1258156406(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2970491004 *, const MethodInfo*))ShimEnumerator_get_Key_m1258156406_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3250932360_gshared (ShimEnumerator_t2970491004 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3250932360(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2970491004 *, const MethodInfo*))ShimEnumerator_get_Value_m3250932360_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3129464592_gshared (ShimEnumerator_t2970491004 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3129464592(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2970491004 *, const MethodInfo*))ShimEnumerator_get_Current_m3129464592_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::Reset()
extern "C"  void ShimEnumerator_Reset_m1915762148_gshared (ShimEnumerator_t2970491004 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1915762148(__this, method) ((  void (*) (ShimEnumerator_t2970491004 *, const MethodInfo*))ShimEnumerator_Reset_m1915762148_gshared)(__this, method)
