﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>
struct IFactoryBinder_4_t2507049579;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.Func`5<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object>
struct Func_5_t936310804;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_4__ctor_m143918766_gshared (IFactoryBinder_4_t2507049579 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method);
#define IFactoryBinder_4__ctor_m143918766(__this, ___container0, ___identifier1, method) ((  void (*) (IFactoryBinder_4_t2507049579 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IFactoryBinder_4__ctor_m143918766_gshared)(__this, ___container0, ___identifier1, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToMethod(System.Func`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToMethod_m2184225648_gshared (IFactoryBinder_4_t2507049579 * __this, Func_5_t936310804 * ___method0, const MethodInfo* method);
#define IFactoryBinder_4_ToMethod_m2184225648(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, Func_5_t936310804 *, const MethodInfo*))IFactoryBinder_4_ToMethod_m2184225648_gshared)(__this, ___method0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToFactory()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToFactory_m2200572889_gshared (IFactoryBinder_4_t2507049579 * __this, const MethodInfo* method);
#define IFactoryBinder_4_ToFactory_m2200572889(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, const MethodInfo*))IFactoryBinder_4_ToFactory_m2200572889_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`4<System.Object,System.Object,System.Object,System.Object>::ToPrefab(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_4_ToPrefab_m3203569583_gshared (IFactoryBinder_4_t2507049579 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define IFactoryBinder_4_ToPrefab_m3203569583(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_4_t2507049579 *, GameObject_t4012695102 *, const MethodInfo*))IFactoryBinder_4_ToPrefab_m3203569583_gshared)(__this, ___prefab0, method)
