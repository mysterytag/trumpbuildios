﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayFab.UUnit.UUnitTestCase>
struct List_1_t811146334;
// PlayFab.UUnit.UUnitTestResult
struct UUnitTestResult_t4060832146;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.UUnitTestSuite
struct  UUnitTestSuite_t2558391287  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<PlayFab.UUnit.UUnitTestCase> PlayFab.UUnit.UUnitTestSuite::tests
	List_1_t811146334 * ___tests_0;
	// System.Int32 PlayFab.UUnit.UUnitTestSuite::lastTestIndex
	int32_t ___lastTestIndex_1;
	// PlayFab.UUnit.UUnitTestResult PlayFab.UUnit.UUnitTestSuite::testResult
	UUnitTestResult_t4060832146 * ___testResult_2;

public:
	inline static int32_t get_offset_of_tests_0() { return static_cast<int32_t>(offsetof(UUnitTestSuite_t2558391287, ___tests_0)); }
	inline List_1_t811146334 * get_tests_0() const { return ___tests_0; }
	inline List_1_t811146334 ** get_address_of_tests_0() { return &___tests_0; }
	inline void set_tests_0(List_1_t811146334 * value)
	{
		___tests_0 = value;
		Il2CppCodeGenWriteBarrier(&___tests_0, value);
	}

	inline static int32_t get_offset_of_lastTestIndex_1() { return static_cast<int32_t>(offsetof(UUnitTestSuite_t2558391287, ___lastTestIndex_1)); }
	inline int32_t get_lastTestIndex_1() const { return ___lastTestIndex_1; }
	inline int32_t* get_address_of_lastTestIndex_1() { return &___lastTestIndex_1; }
	inline void set_lastTestIndex_1(int32_t value)
	{
		___lastTestIndex_1 = value;
	}

	inline static int32_t get_offset_of_testResult_2() { return static_cast<int32_t>(offsetof(UUnitTestSuite_t2558391287, ___testResult_2)); }
	inline UUnitTestResult_t4060832146 * get_testResult_2() const { return ___testResult_2; }
	inline UUnitTestResult_t4060832146 ** get_address_of_testResult_2() { return &___testResult_2; }
	inline void set_testResult_2(UUnitTestResult_t4060832146 * value)
	{
		___testResult_2 = value;
		Il2CppCodeGenWriteBarrier(&___testResult_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
