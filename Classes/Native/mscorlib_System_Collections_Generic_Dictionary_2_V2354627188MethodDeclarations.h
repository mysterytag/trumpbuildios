﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3704248014MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m170263894(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2354627188 *, Dictionary_2_t432490094 *, const MethodInfo*))ValueCollection__ctor_m2981156712_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2784441372(__this, ___item0, method) ((  void (*) (ValueCollection_t2354627188 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3013662666_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m612601573(__this, method) ((  void (*) (ValueCollection_t2354627188 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m436091283_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3653729102(__this, ___item0, method) ((  bool (*) (ValueCollection_t2354627188 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1016525920_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3611496307(__this, ___item0, method) ((  bool (*) (ValueCollection_t2354627188 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3345065221_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1424588709(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2354627188 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1755159635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m4105475497(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2354627188 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3956555479_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3884466168(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2354627188 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1027045542_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1928904961(__this, method) ((  bool (*) (ValueCollection_t2354627188 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3586669075_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2548023393(__this, method) ((  bool (*) (ValueCollection_t2354627188 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m803128435_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2255242387(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2354627188 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3132898853_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2757600285(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2354627188 *, RegionU5BU5D_t4128254283*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3559326127_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3697854534(__this, method) ((  Enumerator_t199518037  (*) (ValueCollection_t2354627188 *, const MethodInfo*))ValueCollection_GetEnumerator_m3611913560_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.UUnit.Region>::get_Count()
#define ValueCollection_get_Count_m1650284187(__this, method) ((  int32_t (*) (ValueCollection_t2354627188 *, const MethodInfo*))ValueCollection_get_Count_m1366250669_gshared)(__this, method)
