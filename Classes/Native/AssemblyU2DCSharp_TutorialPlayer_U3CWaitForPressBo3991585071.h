﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPlayer/<WaitForPressBottomButton>c__AnonStoreyEC
struct  U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_t3991585071  : public Il2CppObject
{
public:
	// System.Boolean TutorialPlayer/<WaitForPressBottomButton>c__AnonStoreyEC::clicked
	bool ___clicked_0;

public:
	inline static int32_t get_offset_of_clicked_0() { return static_cast<int32_t>(offsetof(U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_t3991585071, ___clicked_0)); }
	inline bool get_clicked_0() const { return ___clicked_0; }
	inline bool* get_address_of_clicked_0() { return &___clicked_0; }
	inline void set_clicked_0(bool value)
	{
		___clicked_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
