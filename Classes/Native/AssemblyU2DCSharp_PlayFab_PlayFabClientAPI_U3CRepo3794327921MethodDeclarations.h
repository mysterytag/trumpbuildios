﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<ReportPlayer>c__AnonStoreyA8
struct U3CReportPlayerU3Ec__AnonStoreyA8_t3794327921;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<ReportPlayer>c__AnonStoreyA8::.ctor()
extern "C"  void U3CReportPlayerU3Ec__AnonStoreyA8__ctor_m2709363042 (U3CReportPlayerU3Ec__AnonStoreyA8_t3794327921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<ReportPlayer>c__AnonStoreyA8::<>m__95(PlayFab.CallRequestContainer)
extern "C"  void U3CReportPlayerU3Ec__AnonStoreyA8_U3CU3Em__95_m2243116764 (U3CReportPlayerU3Ec__AnonStoreyA8_t3794327921 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
