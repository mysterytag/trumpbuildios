﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Subscription_t2325194826;
// UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Subject_1_t2189015628;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m3220022820_gshared (Subscription_t2325194826 * __this, Subject_1_t2189015628 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m3220022820(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t2325194826 *, Subject_1_t2189015628 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m3220022820_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m3047827794_gshared (Subscription_t2325194826 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3047827794(__this, method) ((  void (*) (Subscription_t2325194826 *, const MethodInfo*))Subscription_Dispose_m3047827794_gshared)(__this, method)
