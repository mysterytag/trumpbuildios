﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/fbProfile
struct fbProfile_t675187021;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/fbProfile::.ctor()
extern "C"  void fbProfile__ctor_m2687457350 (fbProfile_t675187021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
