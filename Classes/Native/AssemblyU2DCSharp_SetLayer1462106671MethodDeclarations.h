﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SetLayer
struct SetLayer_t1462106671;

#include "codegen/il2cpp-codegen.h"

// System.Void SetLayer::.ctor()
extern "C"  void SetLayer__ctor_m2096089420 (SetLayer_t1462106671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetLayer::Start()
extern "C"  void SetLayer_Start_m1043227212 (SetLayer_t1462106671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
