﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.NetworkMessageInfo>
struct Subject_1_t217412208;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>
struct IObserver_1_t491376491;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"

// System.Void UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::.ctor()
extern "C"  void Subject_1__ctor_m1572181678_gshared (Subject_1_t217412208 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1572181678(__this, method) ((  void (*) (Subject_1_t217412208 *, const MethodInfo*))Subject_1__ctor_m1572181678_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1826860134_gshared (Subject_1_t217412208 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m1826860134(__this, method) ((  bool (*) (Subject_1_t217412208 *, const MethodInfo*))Subject_1_get_HasObservers_m1826860134_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3457454008_gshared (Subject_1_t217412208 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3457454008(__this, method) ((  void (*) (Subject_1_t217412208 *, const MethodInfo*))Subject_1_OnCompleted_m3457454008_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m193817829_gshared (Subject_1_t217412208 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m193817829(__this, ___error0, method) ((  void (*) (Subject_1_t217412208 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m193817829_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1868807638_gshared (Subject_1_t217412208 * __this, NetworkMessageInfo_t2574344884  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1868807638(__this, ___value0, method) ((  void (*) (Subject_1_t217412208 *, NetworkMessageInfo_t2574344884 , const MethodInfo*))Subject_1_OnNext_m1868807638_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2553690755_gshared (Subject_1_t217412208 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2553690755(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t217412208 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2553690755_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::Dispose()
extern "C"  void Subject_1_Dispose_m3231354731_gshared (Subject_1_t217412208 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m3231354731(__this, method) ((  void (*) (Subject_1_t217412208 *, const MethodInfo*))Subject_1_Dispose_m3231354731_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m3360923124_gshared (Subject_1_t217412208 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m3360923124(__this, method) ((  void (*) (Subject_1_t217412208 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3360923124_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.NetworkMessageInfo>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1520438237_gshared (Subject_1_t217412208 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1520438237(__this, method) ((  bool (*) (Subject_1_t217412208 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1520438237_gshared)(__this, method)
