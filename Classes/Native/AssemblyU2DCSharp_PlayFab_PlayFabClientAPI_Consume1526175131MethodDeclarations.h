﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsResponseCallback
struct ConsumePSNEntitlementsResponseCallback_t1526175131;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ConsumePSNEntitlementsRequest
struct ConsumePSNEntitlementsRequest_t3123362058;
// PlayFab.ClientModels.ConsumePSNEntitlementsResult
struct ConsumePSNEntitlementsResult_t4292321378;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumePSNE3123362058.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ConsumePSNE4292321378.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ConsumePSNEntitlementsResponseCallback__ctor_m3642327306 (ConsumePSNEntitlementsResponseCallback_t1526175131 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ConsumePSNEntitlementsRequest,PlayFab.ClientModels.ConsumePSNEntitlementsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void ConsumePSNEntitlementsResponseCallback_Invoke_m2029933879 (ConsumePSNEntitlementsResponseCallback_t1526175131 * __this, String_t* ___urlPath0, int32_t ___callId1, ConsumePSNEntitlementsRequest_t3123362058 * ___request2, ConsumePSNEntitlementsResult_t4292321378 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ConsumePSNEntitlementsResponseCallback_t1526175131(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ConsumePSNEntitlementsRequest_t3123362058 * ___request2, ConsumePSNEntitlementsResult_t4292321378 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ConsumePSNEntitlementsRequest,PlayFab.ClientModels.ConsumePSNEntitlementsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConsumePSNEntitlementsResponseCallback_BeginInvoke_m3065095716 (ConsumePSNEntitlementsResponseCallback_t1526175131 * __this, String_t* ___urlPath0, int32_t ___callId1, ConsumePSNEntitlementsRequest_t3123362058 * ___request2, ConsumePSNEntitlementsResult_t4292321378 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ConsumePSNEntitlementsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ConsumePSNEntitlementsResponseCallback_EndInvoke_m4204464154 (ConsumePSNEntitlementsResponseCallback_t1526175131 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
