﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetTitleNewsRequestCallback
struct GetTitleNewsRequestCallback_t2894451839;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetTitleNewsRequest
struct GetTitleNewsRequest_t3432781354;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleNew3432781354.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetTitleNewsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetTitleNewsRequestCallback__ctor_m3413456270 (GetTitleNewsRequestCallback_t2894451839 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTitleNewsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetTitleNewsRequest,System.Object)
extern "C"  void GetTitleNewsRequestCallback_Invoke_m2814768347 (GetTitleNewsRequestCallback_t2894451839 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTitleNewsRequest_t3432781354 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetTitleNewsRequestCallback_t2894451839(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetTitleNewsRequest_t3432781354 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetTitleNewsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetTitleNewsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetTitleNewsRequestCallback_BeginInvoke_m3159461310 (GetTitleNewsRequestCallback_t2894451839 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTitleNewsRequest_t3432781354 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTitleNewsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetTitleNewsRequestCallback_EndInvoke_m485334686 (GetTitleNewsRequestCallback_t2894451839 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
