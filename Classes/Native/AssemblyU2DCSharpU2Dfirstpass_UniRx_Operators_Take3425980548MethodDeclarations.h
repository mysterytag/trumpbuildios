﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>
struct TakeUntilOther_t3425980548;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,UniRx.Unit>
struct TakeUntil_t105161758;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>::.ctor(UniRx.Operators.TakeUntilObservable`2/TakeUntil<T,TOther>,System.IDisposable)
extern "C"  void TakeUntilOther__ctor_m3750508961_gshared (TakeUntilOther_t3425980548 * __this, TakeUntil_t105161758 * ___sourceObserver0, Il2CppObject * ___subscription1, const MethodInfo* method);
#define TakeUntilOther__ctor_m3750508961(__this, ___sourceObserver0, ___subscription1, method) ((  void (*) (TakeUntilOther_t3425980548 *, TakeUntil_t105161758 *, Il2CppObject *, const MethodInfo*))TakeUntilOther__ctor_m3750508961_gshared)(__this, ___sourceObserver0, ___subscription1, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>::OnNext(TOther)
extern "C"  void TakeUntilOther_OnNext_m1816174222_gshared (TakeUntilOther_t3425980548 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define TakeUntilOther_OnNext_m1816174222(__this, ___value0, method) ((  void (*) (TakeUntilOther_t3425980548 *, Unit_t2558286038 , const MethodInfo*))TakeUntilOther_OnNext_m1816174222_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>::OnError(System.Exception)
extern "C"  void TakeUntilOther_OnError_m14607043_gshared (TakeUntilOther_t3425980548 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeUntilOther_OnError_m14607043(__this, ___error0, method) ((  void (*) (TakeUntilOther_t3425980548 *, Exception_t1967233988 *, const MethodInfo*))TakeUntilOther_OnError_m14607043_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,UniRx.Unit>::OnCompleted()
extern "C"  void TakeUntilOther_OnCompleted_m477192854_gshared (TakeUntilOther_t3425980548 * __this, const MethodInfo* method);
#define TakeUntilOther_OnCompleted_m477192854(__this, method) ((  void (*) (TakeUntilOther_t3425980548 *, const MethodInfo*))TakeUntilOther_OnCompleted_m477192854_gshared)(__this, method)
