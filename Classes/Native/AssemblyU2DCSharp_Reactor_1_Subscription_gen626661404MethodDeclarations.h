﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<System.Int64>
struct Subscription_t626661405;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<System.Int64>::.ctor()
extern "C"  void Subscription__ctor_m3111847214_gshared (Subscription_t626661405 * __this, const MethodInfo* method);
#define Subscription__ctor_m3111847214(__this, method) ((  void (*) (Subscription_t626661405 *, const MethodInfo*))Subscription__ctor_m3111847214_gshared)(__this, method)
// System.Void Reactor`1/Subscription<System.Int64>::Dispose()
extern "C"  void Subscription_Dispose_m1086217707_gshared (Subscription_t626661405 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1086217707(__this, method) ((  void (*) (Subscription_t626661405 *, const MethodInfo*))Subscription_Dispose_m1086217707_gshared)(__this, method)
