﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t1429988286;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1<System.Object>
struct  U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616  : public Il2CppObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1::coroutine
	Func_1_t1429988286 * ___coroutine_0;
	// System.Boolean UniRx.Observable/<FromCoroutineValue>c__AnonStorey4B`1::nullAsNextUpdate
	bool ___nullAsNextUpdate_1;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616, ___coroutine_0)); }
	inline Func_1_t1429988286 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_1_t1429988286 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_1_t1429988286 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier(&___coroutine_0, value);
	}

	inline static int32_t get_offset_of_nullAsNextUpdate_1() { return static_cast<int32_t>(offsetof(U3CFromCoroutineValueU3Ec__AnonStorey4B_1_t3800936616, ___nullAsNextUpdate_1)); }
	inline bool get_nullAsNextUpdate_1() const { return ___nullAsNextUpdate_1; }
	inline bool* get_address_of_nullAsNextUpdate_1() { return &___nullAsNextUpdate_1; }
	inline void set_nullAsNextUpdate_1(bool value)
	{
		___nullAsNextUpdate_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
