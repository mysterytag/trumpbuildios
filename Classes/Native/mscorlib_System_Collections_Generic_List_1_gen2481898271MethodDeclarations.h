﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>
struct List_1_t2481898271;
// System.Collections.Generic.IEnumerable`1<PlayFab.Internal.GMFB_327/testRegion>
struct IEnumerable_1_t262126362;
// System.Collections.Generic.IEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>
struct IEnumerator_1_t3168045750;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<PlayFab.Internal.GMFB_327/testRegion>
struct ICollection_1_t2150770688;
// System.Collections.ObjectModel.ReadOnlyCollection`1<PlayFab.Internal.GMFB_327/testRegion>
struct ReadOnlyCollection_1_t553117354;
// PlayFab.Internal.GMFB_327/testRegion[]
struct testRegionU5BU5D_t485442179;
// System.Predicate`1<PlayFab.Internal.GMFB_327/testRegion>
struct Predicate_1_t2255903200;
// System.Action`1<PlayFab.Internal.GMFB_327/testRegion>
struct Action_1_t1833392007;
// System.Collections.Generic.IComparer`1<PlayFab.Internal.GMFB_327/testRegion>
struct IComparer_1_t89679415;
// System.Comparison`1<PlayFab.Internal.GMFB_327/testRegion>
struct Comparison_1_t93646882;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat567681263.h"

// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor()
extern "C"  void List_1__ctor_m2328426159_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1__ctor_m2328426159(__this, method) ((  void (*) (List_1_t2481898271 *, const MethodInfo*))List_1__ctor_m2328426159_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1312096583_gshared (List_1_t2481898271 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1312096583(__this, ___collection0, method) ((  void (*) (List_1_t2481898271 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1312096583_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m199235977_gshared (List_1_t2481898271 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m199235977(__this, ___capacity0, method) ((  void (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1__ctor_m199235977_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::.cctor()
extern "C"  void List_1__cctor_m3934640757_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3934640757(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3934640757_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1360270402_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1360270402(__this, method) ((  Il2CppObject* (*) (List_1_t2481898271 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1360270402_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2561946636_gshared (List_1_t2481898271 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2561946636(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2481898271 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2561946636_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m193116699_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m193116699(__this, method) ((  Il2CppObject * (*) (List_1_t2481898271 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m193116699_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1994925826_gshared (List_1_t2481898271 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1994925826(__this, ___item0, method) ((  int32_t (*) (List_1_t2481898271 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1994925826_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2854014_gshared (List_1_t2481898271 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2854014(__this, ___item0, method) ((  bool (*) (List_1_t2481898271 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2854014_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2547584090_gshared (List_1_t2481898271 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2547584090(__this, ___item0, method) ((  int32_t (*) (List_1_t2481898271 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2547584090_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2031974605_gshared (List_1_t2481898271 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2031974605(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2481898271 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2031974605_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2904614715_gshared (List_1_t2481898271 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2904614715(__this, ___item0, method) ((  void (*) (List_1_t2481898271 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2904614715_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m6846207_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m6846207(__this, method) ((  bool (*) (List_1_t2481898271 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m6846207_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3414528542_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3414528542(__this, method) ((  bool (*) (List_1_t2481898271 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3414528542_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1001127824_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1001127824(__this, method) ((  Il2CppObject * (*) (List_1_t2481898271 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1001127824_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3070095981_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3070095981(__this, method) ((  bool (*) (List_1_t2481898271 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3070095981_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1107233516_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1107233516(__this, method) ((  bool (*) (List_1_t2481898271 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1107233516_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1076776215_gshared (List_1_t2481898271 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1076776215(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1076776215_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3426395492_gshared (List_1_t2481898271 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3426395492(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2481898271 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3426395492_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Add(T)
extern "C"  void List_1_Add_m3248365895_gshared (List_1_t2481898271 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3248365895(__this, ___item0, method) ((  void (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_Add_m3248365895_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2825053698_gshared (List_1_t2481898271 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2825053698(__this, ___newCount0, method) ((  void (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2825053698_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2615850240_gshared (List_1_t2481898271 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2615850240(__this, ___collection0, method) ((  void (*) (List_1_t2481898271 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2615850240_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1876822464_gshared (List_1_t2481898271 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1876822464(__this, ___enumerable0, method) ((  void (*) (List_1_t2481898271 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1876822464_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4222864599_gshared (List_1_t2481898271 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4222864599(__this, ___collection0, method) ((  void (*) (List_1_t2481898271 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4222864599_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t553117354 * List_1_AsReadOnly_m3040277326_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3040277326(__this, method) ((  ReadOnlyCollection_1_t553117354 * (*) (List_1_t2481898271 *, const MethodInfo*))List_1_AsReadOnly_m3040277326_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Clear()
extern "C"  void List_1_Clear_m3921785955_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_Clear_m3921785955(__this, method) ((  void (*) (List_1_t2481898271 *, const MethodInfo*))List_1_Clear_m3921785955_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Contains(T)
extern "C"  bool List_1_Contains_m3564593621_gshared (List_1_t2481898271 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3564593621(__this, ___item0, method) ((  bool (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_Contains_m3564593621_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2753488247_gshared (List_1_t2481898271 * __this, testRegionU5BU5D_t485442179* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2753488247(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2481898271 *, testRegionU5BU5D_t485442179*, int32_t, const MethodInfo*))List_1_CopyTo_m2753488247_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2808880815_gshared (List_1_t2481898271 * __this, Predicate_1_t2255903200 * ___match0, const MethodInfo* method);
#define List_1_Find_m2808880815(__this, ___match0, method) ((  int32_t (*) (List_1_t2481898271 *, Predicate_1_t2255903200 *, const MethodInfo*))List_1_Find_m2808880815_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3426250572_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2255903200 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3426250572(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2255903200 *, const MethodInfo*))List_1_CheckMatch_m3426250572_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1270109481_gshared (List_1_t2481898271 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2255903200 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1270109481(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2481898271 *, int32_t, int32_t, Predicate_1_t2255903200 *, const MethodInfo*))List_1_GetIndex_m1270109481_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m461091776_gshared (List_1_t2481898271 * __this, Action_1_t1833392007 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m461091776(__this, ___action0, method) ((  void (*) (List_1_t2481898271 *, Action_1_t1833392007 *, const MethodInfo*))List_1_ForEach_m461091776_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::GetEnumerator()
extern "C"  Enumerator_t567681263  List_1_GetEnumerator_m1669804178_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1669804178(__this, method) ((  Enumerator_t567681263  (*) (List_1_t2481898271 *, const MethodInfo*))List_1_GetEnumerator_m1669804178_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3947580291_gshared (List_1_t2481898271 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3947580291(__this, ___item0, method) ((  int32_t (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_IndexOf_m3947580291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m898716878_gshared (List_1_t2481898271 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m898716878(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2481898271 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m898716878_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2499855431_gshared (List_1_t2481898271 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2499855431(__this, ___index0, method) ((  void (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2499855431_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1213590958_gshared (List_1_t2481898271 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1213590958(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2481898271 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1213590958_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1802880931_gshared (List_1_t2481898271 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1802880931(__this, ___collection0, method) ((  void (*) (List_1_t2481898271 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1802880931_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Remove(T)
extern "C"  bool List_1_Remove_m4132737808_gshared (List_1_t2481898271 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m4132737808(__this, ___item0, method) ((  bool (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_Remove_m4132737808_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1299005190_gshared (List_1_t2481898271 * __this, Predicate_1_t2255903200 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1299005190(__this, ___match0, method) ((  int32_t (*) (List_1_t2481898271 *, Predicate_1_t2255903200 *, const MethodInfo*))List_1_RemoveAll_m1299005190_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3382411124_gshared (List_1_t2481898271 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3382411124(__this, ___index0, method) ((  void (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3382411124_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Reverse()
extern "C"  void List_1_Reverse_m3812671096_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_Reverse_m3812671096(__this, method) ((  void (*) (List_1_t2481898271 *, const MethodInfo*))List_1_Reverse_m3812671096_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Sort()
extern "C"  void List_1_Sort_m3774338090_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_Sort_m3774338090(__this, method) ((  void (*) (List_1_t2481898271 *, const MethodInfo*))List_1_Sort_m3774338090_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4146505978_gshared (List_1_t2481898271 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4146505978(__this, ___comparer0, method) ((  void (*) (List_1_t2481898271 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4146505978_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2789868797_gshared (List_1_t2481898271 * __this, Comparison_1_t93646882 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2789868797(__this, ___comparison0, method) ((  void (*) (List_1_t2481898271 *, Comparison_1_t93646882 *, const MethodInfo*))List_1_Sort_m2789868797_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::ToArray()
extern "C"  testRegionU5BU5D_t485442179* List_1_ToArray_m3933777809_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_ToArray_m3933777809(__this, method) ((  testRegionU5BU5D_t485442179* (*) (List_1_t2481898271 *, const MethodInfo*))List_1_ToArray_m3933777809_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1253944387_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1253944387(__this, method) ((  void (*) (List_1_t2481898271 *, const MethodInfo*))List_1_TrimExcess_m1253944387_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1443088947_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1443088947(__this, method) ((  int32_t (*) (List_1_t2481898271 *, const MethodInfo*))List_1_get_Capacity_m1443088947_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m35242516_gshared (List_1_t2481898271 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m35242516(__this, ___value0, method) ((  void (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_set_Capacity_m35242516_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::get_Count()
extern "C"  int32_t List_1_get_Count_m1933212248_gshared (List_1_t2481898271 * __this, const MethodInfo* method);
#define List_1_get_Count_m1933212248(__this, method) ((  int32_t (*) (List_1_t2481898271 *, const MethodInfo*))List_1_get_Count_m1933212248_gshared)(__this, method)
// T System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m4101221018_gshared (List_1_t2481898271 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4101221018(__this, ___index0, method) ((  int32_t (*) (List_1_t2481898271 *, int32_t, const MethodInfo*))List_1_get_Item_m4101221018_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4134370053_gshared (List_1_t2481898271 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m4134370053(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2481898271 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m4134370053_gshared)(__this, ___index0, ___value1, method)
