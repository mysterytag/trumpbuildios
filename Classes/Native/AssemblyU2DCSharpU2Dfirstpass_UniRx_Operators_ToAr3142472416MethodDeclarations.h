﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ToArrayObservable`1<System.Object>
struct ToArrayObservable_1_t3142472416;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object[]>
struct IObserver_1_t2223522676;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ToArrayObservable`1<System.Object>::.ctor(UniRx.IObservable`1<TSource>)
extern "C"  void ToArrayObservable_1__ctor_m833600456_gshared (ToArrayObservable_1_t3142472416 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ToArrayObservable_1__ctor_m833600456(__this, ___source0, method) ((  void (*) (ToArrayObservable_1_t3142472416 *, Il2CppObject*, const MethodInfo*))ToArrayObservable_1__ctor_m833600456_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.ToArrayObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<TSource[]>,System.IDisposable)
extern "C"  Il2CppObject * ToArrayObservable_1_SubscribeCore_m659006067_gshared (ToArrayObservable_1_t3142472416 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ToArrayObservable_1_SubscribeCore_m659006067(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ToArrayObservable_1_t3142472416 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ToArrayObservable_1_SubscribeCore_m659006067_gshared)(__this, ___observer0, ___cancel1, method)
