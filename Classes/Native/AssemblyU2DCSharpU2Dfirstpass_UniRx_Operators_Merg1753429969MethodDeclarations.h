﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>
struct Merge_t1753429969;
// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<System.Object>
struct MergeConcurrentObserver_t3534593302;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>::.ctor(UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<T>,System.IDisposable)
extern "C"  void Merge__ctor_m508001328_gshared (Merge_t1753429969 * __this, MergeConcurrentObserver_t3534593302 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Merge__ctor_m508001328(__this, ___parent0, ___cancel1, method) ((  void (*) (Merge_t1753429969 *, MergeConcurrentObserver_t3534593302 *, Il2CppObject *, const MethodInfo*))Merge__ctor_m508001328_gshared)(__this, ___parent0, ___cancel1, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>::OnNext(T)
extern "C"  void Merge_OnNext_m2854594575_gshared (Merge_t1753429969 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Merge_OnNext_m2854594575(__this, ___value0, method) ((  void (*) (Merge_t1753429969 *, Il2CppObject *, const MethodInfo*))Merge_OnNext_m2854594575_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>::OnError(System.Exception)
extern "C"  void Merge_OnError_m3742221598_gshared (Merge_t1753429969 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Merge_OnError_m3742221598(__this, ___error0, method) ((  void (*) (Merge_t1753429969 *, Exception_t1967233988 *, const MethodInfo*))Merge_OnError_m3742221598_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<System.Object>::OnCompleted()
extern "C"  void Merge_OnCompleted_m2767500657_gshared (Merge_t1753429969 * __this, const MethodInfo* method);
#define Merge_OnCompleted_m2767500657(__this, method) ((  void (*) (Merge_t1753429969 *, const MethodInfo*))Merge_OnCompleted_m2767500657_gshared)(__this, method)
