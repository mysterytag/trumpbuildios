﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>
struct CombineLatestObserver_t301670549;
// UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Object>
struct CombineLatest_t868253967;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`1/CombineLatest<T>,System.Int32)
extern "C"  void CombineLatestObserver__ctor_m566411945_gshared (CombineLatestObserver_t301670549 * __this, CombineLatest_t868253967 * ___parent0, int32_t ___index1, const MethodInfo* method);
#define CombineLatestObserver__ctor_m566411945(__this, ___parent0, ___index1, method) ((  void (*) (CombineLatestObserver_t301670549 *, CombineLatest_t868253967 *, int32_t, const MethodInfo*))CombineLatestObserver__ctor_m566411945_gshared)(__this, ___parent0, ___index1, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>::OnNext(T)
extern "C"  void CombineLatestObserver_OnNext_m1370273410_gshared (CombineLatestObserver_t301670549 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CombineLatestObserver_OnNext_m1370273410(__this, ___value0, method) ((  void (*) (CombineLatestObserver_t301670549 *, Il2CppObject *, const MethodInfo*))CombineLatestObserver_OnNext_m1370273410_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>::OnError(System.Exception)
extern "C"  void CombineLatestObserver_OnError_m520588689_gshared (CombineLatestObserver_t301670549 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method);
#define CombineLatestObserver_OnError_m520588689(__this, ___ex0, method) ((  void (*) (CombineLatestObserver_t301670549 *, Exception_t1967233988 *, const MethodInfo*))CombineLatestObserver_OnError_m520588689_gshared)(__this, ___ex0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Object>::OnCompleted()
extern "C"  void CombineLatestObserver_OnCompleted_m4237906532_gshared (CombineLatestObserver_t301670549 * __this, const MethodInfo* method);
#define CombineLatestObserver_OnCompleted_m4237906532(__this, method) ((  void (*) (CombineLatestObserver_t301670549 *, const MethodInfo*))CombineLatestObserver_OnCompleted_m4237906532_gshared)(__this, method)
