﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableLateUpdateTrigger
struct ObservableLateUpdateTrigger_t2104302407;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableLateUpdateTrigger::.ctor()
extern "C"  void ObservableLateUpdateTrigger__ctor_m1722683396 (ObservableLateUpdateTrigger_t2104302407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableLateUpdateTrigger::LateUpdate()
extern "C"  void ObservableLateUpdateTrigger_LateUpdate_m1665440975 (ObservableLateUpdateTrigger_t2104302407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableLateUpdateTrigger::LateUpdateAsObservable()
extern "C"  Il2CppObject* ObservableLateUpdateTrigger_LateUpdateAsObservable_m2012520844 (ObservableLateUpdateTrigger_t2104302407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableLateUpdateTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableLateUpdateTrigger_RaiseOnCompletedOnDestroy_m595508541 (ObservableLateUpdateTrigger_t2104302407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
