﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ValidateAmazonReceiptResult
struct ValidateAmazonReceiptResult_t528948171;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.ValidateAmazonReceiptResult::.ctor()
extern "C"  void ValidateAmazonReceiptResult__ctor_m2684272446 (ValidateAmazonReceiptResult_t528948171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
