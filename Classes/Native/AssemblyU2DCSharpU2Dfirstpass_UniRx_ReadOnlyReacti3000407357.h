﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<System.Boolean>
struct Subject_1_t2149039961;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReadOnlyReactiveProperty`1<System.Boolean>
struct  ReadOnlyReactiveProperty_1_t3000407357  : public Il2CppObject
{
public:
	// System.Boolean UniRx.ReadOnlyReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_0;
	// System.Boolean UniRx.ReadOnlyReactiveProperty`1::isDisposed
	bool ___isDisposed_1;
	// T UniRx.ReadOnlyReactiveProperty`1::value
	bool ___value_2;
	// UniRx.Subject`1<T> UniRx.ReadOnlyReactiveProperty`1::publisher
	Subject_1_t2149039961 * ___publisher_3;
	// System.IDisposable UniRx.ReadOnlyReactiveProperty`1::sourceConnection
	Il2CppObject * ___sourceConnection_4;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_0() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3000407357, ___canPublishValueOnSubscribe_0)); }
	inline bool get_canPublishValueOnSubscribe_0() const { return ___canPublishValueOnSubscribe_0; }
	inline bool* get_address_of_canPublishValueOnSubscribe_0() { return &___canPublishValueOnSubscribe_0; }
	inline void set_canPublishValueOnSubscribe_0(bool value)
	{
		___canPublishValueOnSubscribe_0 = value;
	}

	inline static int32_t get_offset_of_isDisposed_1() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3000407357, ___isDisposed_1)); }
	inline bool get_isDisposed_1() const { return ___isDisposed_1; }
	inline bool* get_address_of_isDisposed_1() { return &___isDisposed_1; }
	inline void set_isDisposed_1(bool value)
	{
		___isDisposed_1 = value;
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3000407357, ___value_2)); }
	inline bool get_value_2() const { return ___value_2; }
	inline bool* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(bool value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_publisher_3() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3000407357, ___publisher_3)); }
	inline Subject_1_t2149039961 * get_publisher_3() const { return ___publisher_3; }
	inline Subject_1_t2149039961 ** get_address_of_publisher_3() { return &___publisher_3; }
	inline void set_publisher_3(Subject_1_t2149039961 * value)
	{
		___publisher_3 = value;
		Il2CppCodeGenWriteBarrier(&___publisher_3, value);
	}

	inline static int32_t get_offset_of_sourceConnection_4() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3000407357, ___sourceConnection_4)); }
	inline Il2CppObject * get_sourceConnection_4() const { return ___sourceConnection_4; }
	inline Il2CppObject ** get_address_of_sourceConnection_4() { return &___sourceConnection_4; }
	inline void set_sourceConnection_4(Il2CppObject * value)
	{
		___sourceConnection_4 = value;
		Il2CppCodeGenWriteBarrier(&___sourceConnection_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
