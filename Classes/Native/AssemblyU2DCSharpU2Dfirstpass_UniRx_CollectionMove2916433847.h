﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CollectionMoveEvent`1<System.Object>
struct  CollectionMoveEvent_1_t2916433847 
{
public:
	// System.Int32 UniRx.CollectionMoveEvent`1::<OldIndex>k__BackingField
	int32_t ___U3COldIndexU3Ek__BackingField_0;
	// System.Int32 UniRx.CollectionMoveEvent`1::<NewIndex>k__BackingField
	int32_t ___U3CNewIndexU3Ek__BackingField_1;
	// T UniRx.CollectionMoveEvent`1::<Value>k__BackingField
	Il2CppObject * ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COldIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionMoveEvent_1_t2916433847, ___U3COldIndexU3Ek__BackingField_0)); }
	inline int32_t get_U3COldIndexU3Ek__BackingField_0() const { return ___U3COldIndexU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3COldIndexU3Ek__BackingField_0() { return &___U3COldIndexU3Ek__BackingField_0; }
	inline void set_U3COldIndexU3Ek__BackingField_0(int32_t value)
	{
		___U3COldIndexU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CNewIndexU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionMoveEvent_1_t2916433847, ___U3CNewIndexU3Ek__BackingField_1)); }
	inline int32_t get_U3CNewIndexU3Ek__BackingField_1() const { return ___U3CNewIndexU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CNewIndexU3Ek__BackingField_1() { return &___U3CNewIndexU3Ek__BackingField_1; }
	inline void set_U3CNewIndexU3Ek__BackingField_1(int32_t value)
	{
		___U3CNewIndexU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CollectionMoveEvent_1_t2916433847, ___U3CValueU3Ek__BackingField_2)); }
	inline Il2CppObject * get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline Il2CppObject ** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(Il2CppObject * value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CValueU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
