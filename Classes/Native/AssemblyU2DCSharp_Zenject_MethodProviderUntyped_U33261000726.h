﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.MethodProviderUntyped
struct MethodProviderUntyped_t4072345204;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MethodProviderUntyped/<GetInstance>c__AnonStorey196
struct  U3CGetInstanceU3Ec__AnonStorey196_t3261000726  : public Il2CppObject
{
public:
	// Zenject.InjectContext Zenject.MethodProviderUntyped/<GetInstance>c__AnonStorey196::context
	InjectContext_t3456483891 * ___context_0;
	// Zenject.MethodProviderUntyped Zenject.MethodProviderUntyped/<GetInstance>c__AnonStorey196::<>f__this
	MethodProviderUntyped_t4072345204 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(U3CGetInstanceU3Ec__AnonStorey196_t3261000726, ___context_0)); }
	inline InjectContext_t3456483891 * get_context_0() const { return ___context_0; }
	inline InjectContext_t3456483891 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(InjectContext_t3456483891 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier(&___context_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CGetInstanceU3Ec__AnonStorey196_t3261000726, ___U3CU3Ef__this_1)); }
	inline MethodProviderUntyped_t4072345204 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline MethodProviderUntyped_t4072345204 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(MethodProviderUntyped_t4072345204 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
