﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t1712827743;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t1986792026;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419.h"

// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void Subject_1__ctor_m1370208901_gshared (Subject_1_t1712827743 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1370208901(__this, method) ((  void (*) (Subject_1_t1712827743 *, const MethodInfo*))Subject_1__ctor_m1370208901_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1297353703_gshared (Subject_1_t1712827743 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m1297353703(__this, method) ((  bool (*) (Subject_1_t1712827743 *, const MethodInfo*))Subject_1_get_HasObservers_m1297353703_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1493186127_gshared (Subject_1_t1712827743 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m1493186127(__this, method) ((  void (*) (Subject_1_t1712827743 *, const MethodInfo*))Subject_1_OnCompleted_m1493186127_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m863561980_gshared (Subject_1_t1712827743 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m863561980(__this, ___error0, method) ((  void (*) (Subject_1_t1712827743 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m863561980_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1046497261_gshared (Subject_1_t1712827743 * __this, CollectionRemoveEvent_1_t4069760419  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1046497261(__this, ___value0, method) ((  void (*) (Subject_1_t1712827743 *, CollectionRemoveEvent_1_t4069760419 , const MethodInfo*))Subject_1_OnNext_m1046497261_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m4152592388_gshared (Subject_1_t1712827743 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m4152592388(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1712827743 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m4152592388_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subject_1_Dispose_m2409044354_gshared (Subject_1_t1712827743 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2409044354(__this, method) ((  void (*) (Subject_1_t1712827743 *, const MethodInfo*))Subject_1_Dispose_m2409044354_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m290201867_gshared (Subject_1_t1712827743 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m290201867(__this, method) ((  void (*) (Subject_1_t1712827743 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m290201867_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3694259486_gshared (Subject_1_t1712827743 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3694259486(__this, method) ((  bool (*) (Subject_1_t1712827743 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3694259486_gshared)(__this, method)
