﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionSum
struct XPathFunctionSum_t1178274675;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionSum::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionSum__ctor_m1037722509 (XPathFunctionSum_t1178274675 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionSum::get_Peer()
extern "C"  bool XPathFunctionSum_get_Peer_m224515491 (XPathFunctionSum_t1178274675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionSum::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionSum_Evaluate_m2525900520 (XPathFunctionSum_t1178274675 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionSum::ToString()
extern "C"  String_t* XPathFunctionSum_ToString_m3653312617 (XPathFunctionSum_t1178274675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
