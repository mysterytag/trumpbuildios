﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.EventPattern`1<System.Object>
struct EventPattern_1_t2903003978;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.EventPattern`1<System.Object>::.ctor(System.Object,TEventArgs)
extern "C"  void EventPattern_1__ctor_m3160314633_gshared (EventPattern_1_t2903003978 * __this, Il2CppObject * ___sender0, Il2CppObject * ___e1, const MethodInfo* method);
#define EventPattern_1__ctor_m3160314633(__this, ___sender0, ___e1, method) ((  void (*) (EventPattern_1_t2903003978 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EventPattern_1__ctor_m3160314633_gshared)(__this, ___sender0, ___e1, method)
