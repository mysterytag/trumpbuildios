﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/AttributeTypeAndValue
struct AttributeTypeAndValue_t3850245072;
// System.String
struct String_t;
// Mono.Security.ASN1
struct ASN1_t1254135647;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::.ctor(System.String,System.Int32)
extern "C"  void AttributeTypeAndValue__ctor_m1154101250 (AttributeTypeAndValue_t3850245072 * __this, String_t* ___oid0, int32_t ___upperBound1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::.ctor(System.String,System.Int32,System.Byte)
extern "C"  void AttributeTypeAndValue__ctor_m1048196135 (AttributeTypeAndValue_t3850245072 * __this, String_t* ___oid0, int32_t ___upperBound1, uint8_t ___encoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X520/AttributeTypeAndValue::get_Value()
extern "C"  String_t* AttributeTypeAndValue_get_Value_m2127899190 (AttributeTypeAndValue_t3850245072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::set_Value(System.String)
extern "C"  void AttributeTypeAndValue_set_Value_m2494542275 (AttributeTypeAndValue_t3850245072 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::get_ASN1()
extern "C"  ASN1_t1254135647 * AttributeTypeAndValue_get_ASN1_m2779117110 (AttributeTypeAndValue_t3850245072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::GetASN1(System.Byte)
extern "C"  ASN1_t1254135647 * AttributeTypeAndValue_GetASN1_m336149366 (AttributeTypeAndValue_t3850245072 * __this, uint8_t ___encoding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::GetASN1()
extern "C"  ASN1_t1254135647 * AttributeTypeAndValue_GetASN1_m2636661973 (AttributeTypeAndValue_t3850245072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X520/AttributeTypeAndValue::GetBytes(System.Byte)
extern "C"  ByteU5BU5D_t58506160* AttributeTypeAndValue_GetBytes_m2645505065 (AttributeTypeAndValue_t3850245072 * __this, uint8_t ___encoding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X520/AttributeTypeAndValue::GetBytes()
extern "C"  ByteU5BU5D_t58506160* AttributeTypeAndValue_GetBytes_m4196843010 (AttributeTypeAndValue_t3850245072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.X509.X520/AttributeTypeAndValue::SelectBestEncoding()
extern "C"  uint8_t AttributeTypeAndValue_SelectBestEncoding_m750204702 (AttributeTypeAndValue_t3850245072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
