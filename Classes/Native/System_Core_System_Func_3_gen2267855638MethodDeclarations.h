﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen3690668578MethodDeclarations.h"

// System.Void System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<System.Object>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m4196385861(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t2267855638 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m2605817040_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<System.Object>>::Invoke(T1,T2)
#define Func_3_Invoke_m763229288(__this, ___arg10, ___arg21, method) ((  Il2CppObject* (*) (Func_3_t2267855638 *, Il2CppObject *, int32_t, const MethodInfo*))Func_3_Invoke_m2458764669_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<System.Object>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m3663875437(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t2267855638 *, Il2CppObject *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m171032578_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<System.Object>>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m4220125043(__this, ___result0, method) ((  Il2CppObject* (*) (Func_3_t2267855638 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m2951600894_gshared)(__this, ___result0, method)
