﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PostInjectAttribute
struct PostInjectAttribute_t2571569824;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.PostInjectAttribute::.ctor()
extern "C"  void PostInjectAttribute__ctor_m3763581503 (PostInjectAttribute_t2571569824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
