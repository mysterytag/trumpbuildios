﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/OpenTradeRequestCallback
struct OpenTradeRequestCallback_t2658287482;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.OpenTradeRequest
struct OpenTradeRequest_t1121233957;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_OpenTradeRe1121233957.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/OpenTradeRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OpenTradeRequestCallback__ctor_m4100275049 (OpenTradeRequestCallback_t2658287482 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/OpenTradeRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.OpenTradeRequest,System.Object)
extern "C"  void OpenTradeRequestCallback_Invoke_m1501653663 (OpenTradeRequestCallback_t2658287482 * __this, String_t* ___urlPath0, int32_t ___callId1, OpenTradeRequest_t1121233957 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OpenTradeRequestCallback_t2658287482(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, OpenTradeRequest_t1121233957 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/OpenTradeRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.OpenTradeRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OpenTradeRequestCallback_BeginInvoke_m534032986 (OpenTradeRequestCallback_t2658287482 * __this, String_t* ___urlPath0, int32_t ___callId1, OpenTradeRequest_t1121233957 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/OpenTradeRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OpenTradeRequestCallback_EndInvoke_m409520633 (OpenTradeRequestCallback_t2658287482 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
