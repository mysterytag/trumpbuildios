﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkCustomIDRequestCallback
struct UnlinkCustomIDRequestCallback_t3859278421;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkCustomIDRequest
struct UnlinkCustomIDRequest_t1348755968;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkCusto1348755968.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkCustomIDRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkCustomIDRequestCallback__ctor_m3208651876 (UnlinkCustomIDRequestCallback_t3859278421 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkCustomIDRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkCustomIDRequest,System.Object)
extern "C"  void UnlinkCustomIDRequestCallback_Invoke_m1880498351 (UnlinkCustomIDRequestCallback_t3859278421 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkCustomIDRequest_t1348755968 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkCustomIDRequestCallback_t3859278421(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkCustomIDRequest_t1348755968 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkCustomIDRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkCustomIDRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkCustomIDRequestCallback_BeginInvoke_m2612248254 (UnlinkCustomIDRequestCallback_t3859278421 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkCustomIDRequest_t1348755968 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkCustomIDRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkCustomIDRequestCallback_EndInvoke_m335374452 (UnlinkCustomIDRequestCallback_t3859278421 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
