﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3392211982MethodDeclarations.h"

// System.Void System.Func`2<UniRx.Unit,UniRx.IObservable`1<UniRx.Unit>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m109116419(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t577222668 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2719813104_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UniRx.Unit,UniRx.IObservable`1<UniRx.Unit>>::Invoke(T)
#define Func_2_Invoke_m4162250627(__this, ___arg10, method) ((  Il2CppObject* (*) (Func_2_t577222668 *, Unit_t2558286038 , const MethodInfo*))Func_2_Invoke_m2321944950_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UniRx.Unit,UniRx.IObservable`1<UniRx.Unit>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3910026806(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t577222668 *, Unit_t2558286038 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3501888937_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UniRx.Unit,UniRx.IObservable`1<UniRx.Unit>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3528604721(__this, ___result0, method) ((  Il2CppObject* (*) (Func_2_t577222668 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m859232286_gshared)(__this, ___result0, method)
