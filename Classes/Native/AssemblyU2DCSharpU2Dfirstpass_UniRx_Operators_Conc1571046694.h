﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ConcatObservable`1<System.Object>
struct  ConcatObservable_1_t1571046694  : public OperatorObservableBase_1_t4196218687
{
public:
	// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>> UniRx.Operators.ConcatObservable`1::sources
	Il2CppObject* ___sources_1;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(ConcatObservable_1_t1571046694, ___sources_1)); }
	inline Il2CppObject* get_sources_1() const { return ___sources_1; }
	inline Il2CppObject** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(Il2CppObject* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier(&___sources_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
