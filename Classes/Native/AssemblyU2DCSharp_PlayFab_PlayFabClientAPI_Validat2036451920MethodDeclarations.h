﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseRequestCallback
struct ValidateGooglePlayPurchaseRequestCallback_t2036451920;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest
struct ValidateGooglePlayPurchaseRequest_t4082068731;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateGoo4082068731.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ValidateGooglePlayPurchaseRequestCallback__ctor_m2366581471 (ValidateGooglePlayPurchaseRequestCallback_t2036451920 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest,System.Object)
extern "C"  void ValidateGooglePlayPurchaseRequestCallback_Invoke_m3428362937 (ValidateGooglePlayPurchaseRequestCallback_t2036451920 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateGooglePlayPurchaseRequest_t4082068731 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ValidateGooglePlayPurchaseRequestCallback_t2036451920(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ValidateGooglePlayPurchaseRequest_t4082068731 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ValidateGooglePlayPurchaseRequestCallback_BeginInvoke_m1960182750 (ValidateGooglePlayPurchaseRequestCallback_t2036451920 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateGooglePlayPurchaseRequest_t4082068731 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ValidateGooglePlayPurchaseRequestCallback_EndInvoke_m1613816943 (ValidateGooglePlayPurchaseRequestCallback_t2036451920 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
