﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<System.Int32>
struct Empty_t511265325;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int32>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2893778734_gshared (Empty_t511265325 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m2893778734(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t511265325 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m2893778734_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int32>::OnNext(T)
extern "C"  void Empty_OnNext_m1168253860_gshared (Empty_t511265325 * __this, int32_t ___value0, const MethodInfo* method);
#define Empty_OnNext_m1168253860(__this, ___value0, method) ((  void (*) (Empty_t511265325 *, int32_t, const MethodInfo*))Empty_OnNext_m1168253860_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int32>::OnError(System.Exception)
extern "C"  void Empty_OnError_m2467460787_gshared (Empty_t511265325 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m2467460787(__this, ___error0, method) ((  void (*) (Empty_t511265325 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m2467460787_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Int32>::OnCompleted()
extern "C"  void Empty_OnCompleted_m4025441926_gshared (Empty_t511265325 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m4025441926(__this, method) ((  void (*) (Empty_t511265325 *, const MethodInfo*))Empty_OnCompleted_m4025441926_gshared)(__this, method)
