﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SponsorPay_AbstractResponse2918001245.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.InterstitialAdStatus
struct  InterstitialAdStatus_t2696000795  : public AbstractResponse_t2918001245
{
public:
	// System.String SponsorPay.InterstitialAdStatus::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_1;
	// System.String SponsorPay.InterstitialAdStatus::<closeReason>k__BackingField
	String_t* ___U3CcloseReasonU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InterstitialAdStatus_t2696000795, ___U3CerrorU3Ek__BackingField_1)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_1() const { return ___U3CerrorU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_1() { return &___U3CerrorU3Ek__BackingField_1; }
	inline void set_U3CerrorU3Ek__BackingField_1(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CcloseReasonU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InterstitialAdStatus_t2696000795, ___U3CcloseReasonU3Ek__BackingField_2)); }
	inline String_t* get_U3CcloseReasonU3Ek__BackingField_2() const { return ___U3CcloseReasonU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcloseReasonU3Ek__BackingField_2() { return &___U3CcloseReasonU3Ek__BackingField_2; }
	inline void set_U3CcloseReasonU3Ek__BackingField_2(String_t* value)
	{
		___U3CcloseReasonU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcloseReasonU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
