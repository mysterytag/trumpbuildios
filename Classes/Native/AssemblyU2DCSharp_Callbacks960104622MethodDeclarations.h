﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Callbacks
struct Callbacks_t960104622;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Callbacks::.ctor()
extern "C"  void Callbacks__ctor_m1519710333 (Callbacks_t960104622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Callbacks::Event(System.String)
extern "C"  void Callbacks_Event_m2210087789 (Callbacks_t960104622 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
