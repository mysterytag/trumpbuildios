﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>
struct JsonResponse_1_t811794004;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3783893733.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m3628936110_gshared (JsonResponse_1_t811794004 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m3628936110(__this, method) ((  void (*) (JsonResponse_1_t811794004 *, const MethodInfo*))JsonResponse_1__ctor_m3628936110_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m1776202501_gshared (JsonResponse_1_t811794004 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m1776202501(__this, method) ((  String_t* (*) (JsonResponse_1_t811794004 *, const MethodInfo*))JsonResponse_1_get_key_m1776202501_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m2572480276_gshared (JsonResponse_1_t811794004 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m2572480276(__this, ___value0, method) ((  void (*) (JsonResponse_1_t811794004 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m2572480276_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::get_value()
extern "C"  Nullable_1_t3783893733  JsonResponse_1_get_value_m3536917427_gshared (JsonResponse_1_t811794004 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m3536917427(__this, method) ((  Nullable_1_t3783893733  (*) (JsonResponse_1_t811794004 *, const MethodInfo*))JsonResponse_1_get_value_m3536917427_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m2973816734_gshared (JsonResponse_1_t811794004 * __this, Nullable_1_t3783893733  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m2973816734(__this, ___value0, method) ((  void (*) (JsonResponse_1_t811794004 *, Nullable_1_t3783893733 , const MethodInfo*))JsonResponse_1_set_value_m2973816734_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m1166433070_gshared (JsonResponse_1_t811794004 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m1166433070(__this, method) ((  String_t* (*) (JsonResponse_1_t811794004 *, const MethodInfo*))JsonResponse_1_get_error_m1166433070_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserEthnicity>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m908242891_gshared (JsonResponse_1_t811794004 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m908242891(__this, ___value0, method) ((  void (*) (JsonResponse_1_t811794004 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m908242891_gshared)(__this, ___value0, method)
