﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RefCountObservable`1<System.Object>
struct RefCountObservable_1_t1275253774;
// UniRx.IConnectableObservable`1<System.Object>
struct IConnectableObservable_1_t2600005682;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.RefCountObservable`1<System.Object>::.ctor(UniRx.IConnectableObservable`1<T>)
extern "C"  void RefCountObservable_1__ctor_m3674820017_gshared (RefCountObservable_1_t1275253774 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define RefCountObservable_1__ctor_m3674820017(__this, ___source0, method) ((  void (*) (RefCountObservable_1_t1275253774 *, Il2CppObject*, const MethodInfo*))RefCountObservable_1__ctor_m3674820017_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.RefCountObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * RefCountObservable_1_SubscribeCore_m854383686_gshared (RefCountObservable_1_t1275253774 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define RefCountObservable_1_SubscribeCore_m854383686(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (RefCountObservable_1_t1275253774 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))RefCountObservable_1_SubscribeCore_m854383686_gshared)(__this, ___observer0, ___cancel1, method)
