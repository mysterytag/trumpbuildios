﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample01_ObservableWWW
struct Sample01_ObservableWWW_t1646932859;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// <>__AnonType0`2<System.String,System.String>
struct U3CU3E__AnonType0_2_t1370999997;
// System.String[]
struct StringU5BU5D_t2956870243;
// UniRx.WWWErrorException
struct WWWErrorException_t3758994352;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WWWErrorExcept3758994352.h"

// System.Void UniRx.Examples.Sample01_ObservableWWW::.ctor()
extern "C"  void Sample01_ObservableWWW__ctor_m3063018538 (Sample01_ObservableWWW_t1646932859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample01_ObservableWWW::Start()
extern "C"  void Sample01_ObservableWWW_Start_m2010156330 (Sample01_ObservableWWW_t1646932859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__9(System.String)
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__9_m1978097430 (Il2CppObject * __this /* static, unused */, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__A(System.Exception)
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__A_m348648996 (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.Examples.Sample01_ObservableWWW::<Start>m__B(System.String)
extern "C"  Il2CppObject* Sample01_ObservableWWW_U3CStartU3Em__B_m1918048653 (Il2CppObject * __this /* static, unused */, String_t* ___google0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType0`2<System.String,System.String> UniRx.Examples.Sample01_ObservableWWW::<Start>m__C(System.String,System.String)
extern "C"  U3CU3E__AnonType0_2_t1370999997 * Sample01_ObservableWWW_U3CStartU3Em__C_m713662703 (Il2CppObject * __this /* static, unused */, String_t* ___google0, String_t* ___bing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__D(<>__AnonType0`2<System.String,System.String>)
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__D_m183641227 (Il2CppObject * __this /* static, unused */, U3CU3E__AnonType0_2_t1370999997 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__E(System.String[])
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__E_m3496170024 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___xs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__F(System.Single)
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__F_m3612411410 (Il2CppObject * __this /* static, unused */, float ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__10(UniRx.WWWErrorException)
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__10_m2096553598 (Il2CppObject * __this /* static, unused */, WWWErrorException_t3758994352 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
