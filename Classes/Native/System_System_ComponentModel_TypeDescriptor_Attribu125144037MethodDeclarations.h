﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.TypeDescriptor/AttributeProvider/AttributeTypeDescriptor
struct AttributeTypeDescriptor_t125144037;
// System.ComponentModel.ICustomTypeDescriptor
struct ICustomTypeDescriptor_t1558278316;
// System.Attribute[]
struct AttributeU5BU5D_t4076389004;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_t3839011043;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ComponentModel.TypeDescriptor/AttributeProvider/AttributeTypeDescriptor::.ctor(System.ComponentModel.ICustomTypeDescriptor,System.Attribute[])
extern "C"  void AttributeTypeDescriptor__ctor_m1390219569 (AttributeTypeDescriptor_t125144037 * __this, Il2CppObject * ___parent0, AttributeU5BU5D_t4076389004* ___attributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.AttributeCollection System.ComponentModel.TypeDescriptor/AttributeProvider/AttributeTypeDescriptor::GetAttributes()
extern "C"  AttributeCollection_t3839011043 * AttributeTypeDescriptor_GetAttributes_m3322328875 (AttributeTypeDescriptor_t125144037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
