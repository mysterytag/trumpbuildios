﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subscription_t1849006941;
// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t1712827743;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t1986792026;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m952601866_gshared (Subscription_t1849006941 * __this, Subject_1_t1712827743 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m952601866(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t1849006941 *, Subject_1_t1712827743 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m952601866_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subscription_Dispose_m1259664044_gshared (Subscription_t1849006941 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1259664044(__this, method) ((  void (*) (Subscription_t1849006941 *, const MethodInfo*))Subscription_Dispose_m1259664044_gshared)(__this, method)
