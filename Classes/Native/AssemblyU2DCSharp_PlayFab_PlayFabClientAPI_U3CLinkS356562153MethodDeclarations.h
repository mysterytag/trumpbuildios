﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkSteamAccount>c__AnonStorey7D
struct U3CLinkSteamAccountU3Ec__AnonStorey7D_t356562153;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkSteamAccount>c__AnonStorey7D::.ctor()
extern "C"  void U3CLinkSteamAccountU3Ec__AnonStorey7D__ctor_m2302151914 (U3CLinkSteamAccountU3Ec__AnonStorey7D_t356562153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkSteamAccount>c__AnonStorey7D::<>m__6A(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkSteamAccountU3Ec__AnonStorey7D_U3CU3Em__6A_m3152377811 (U3CLinkSteamAccountU3Ec__AnonStorey7D_t356562153 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
