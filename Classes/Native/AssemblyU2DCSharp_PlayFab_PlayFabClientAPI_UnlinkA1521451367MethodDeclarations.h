﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDRequestCallback
struct UnlinkAndroidDeviceIDRequestCallback_t1521451367;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest
struct UnlinkAndroidDeviceIDRequest_t1293232914;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkAndro1293232914.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkAndroidDeviceIDRequestCallback__ctor_m1081836246 (UnlinkAndroidDeviceIDRequestCallback_t1521451367 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest,System.Object)
extern "C"  void UnlinkAndroidDeviceIDRequestCallback_Invoke_m883929087 (UnlinkAndroidDeviceIDRequestCallback_t1521451367 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkAndroidDeviceIDRequest_t1293232914 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkAndroidDeviceIDRequestCallback_t1521451367(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkAndroidDeviceIDRequest_t1293232914 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkAndroidDeviceIDRequestCallback_BeginInvoke_m1669890420 (UnlinkAndroidDeviceIDRequestCallback_t1521451367 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkAndroidDeviceIDRequest_t1293232914 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkAndroidDeviceIDRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkAndroidDeviceIDRequestCallback_EndInvoke_m2742931430 (UnlinkAndroidDeviceIDRequestCallback_t1521451367 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
