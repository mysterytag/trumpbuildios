﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromSteamIDs>c__AnonStorey73
struct U3CGetPlayFabIDsFromSteamIDsU3Ec__AnonStorey73_t3484288822;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromSteamIDs>c__AnonStorey73::.ctor()
extern "C"  void U3CGetPlayFabIDsFromSteamIDsU3Ec__AnonStorey73__ctor_m4088860893 (U3CGetPlayFabIDsFromSteamIDsU3Ec__AnonStorey73_t3484288822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromSteamIDs>c__AnonStorey73::<>m__60(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPlayFabIDsFromSteamIDsU3Ec__AnonStorey73_U3CU3Em__60_m771621941 (U3CGetPlayFabIDsFromSteamIDsU3Ec__AnonStorey73_t3484288822 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
