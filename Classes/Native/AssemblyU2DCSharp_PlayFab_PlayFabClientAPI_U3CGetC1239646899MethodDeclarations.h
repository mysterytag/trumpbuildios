﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetCatalogItems>c__AnonStorey9B
struct U3CGetCatalogItemsU3Ec__AnonStorey9B_t1239646899;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetCatalogItems>c__AnonStorey9B::.ctor()
extern "C"  void U3CGetCatalogItemsU3Ec__AnonStorey9B__ctor_m2917824000 (U3CGetCatalogItemsU3Ec__AnonStorey9B_t1239646899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetCatalogItems>c__AnonStorey9B::<>m__88(PlayFab.CallRequestContainer)
extern "C"  void U3CGetCatalogItemsU3Ec__AnonStorey9B_U3CU3Em__88_m3077360606 (U3CGetCatalogItemsU3Ec__AnonStorey9B_t1239646899 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
