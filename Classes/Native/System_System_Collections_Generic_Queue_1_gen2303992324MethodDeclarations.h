﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2545193960MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::.ctor()
#define Queue_1__ctor_m421446206(__this, method) ((  void (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m1423979718(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2303992324 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m433704140(__this, method) ((  bool (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2157272716(__this, method) ((  Il2CppObject * (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1444250896(__this, method) ((  Il2CppObject* (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m246315349(__this, method) ((  Il2CppObject * (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::Clear()
#define Queue_1_Clear_m2122546793(__this, method) ((  void (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_Clear_m448938124_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3243616305(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2303992324 *, IObservable_1U5BU5D_t2205425841*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::Dequeue()
#define Queue_1_Dequeue_m1773247853(__this, method) ((  Il2CppObject* (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::Peek()
#define Queue_1_Peek_m3674719712(__this, method) ((  Il2CppObject* (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::Enqueue(T)
#define Queue_1_Enqueue_m2279862586(__this, ___item0, method) ((  void (*) (Queue_1_t2303992324 *, Il2CppObject*, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m3635346601(__this, ___new_size0, method) ((  void (*) (Queue_1_t2303992324 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::get_Count()
#define Queue_1_get_Count_m2949223698(__this, method) ((  int32_t (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<UniRx.IObservable`1<System.Object>>::GetEnumerator()
#define Queue_1_GetEnumerator_m2773556541(__this, method) ((  Enumerator_t3773614042  (*) (Queue_1_t2303992324 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
