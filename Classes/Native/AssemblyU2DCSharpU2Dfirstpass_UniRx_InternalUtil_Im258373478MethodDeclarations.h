﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"

// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>::.ctor()
#define ImmutableList_1__ctor_m489013789(__this, method) ((  void (*) (ImmutableList_1_t258373478 *, const MethodInfo*))ImmutableList_1__ctor_m2467204245_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>::.ctor(T[])
#define ImmutableList_1__ctor_m3977058751(__this, ___data0, method) ((  void (*) (ImmutableList_1_t258373478 *, IObserver_1U5BU5D_t632717506*, const MethodInfo*))ImmutableList_1__ctor_m707697735_gshared)(__this, ___data0, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>::.cctor()
#define ImmutableList_1__cctor_m1792429360(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableList_1__cctor_m2986791352_gshared)(__this /* static, unused */, method)
// T[] UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>::get_Data()
#define ImmutableList_1_get_Data_m482130903(__this, method) ((  IObserver_1U5BU5D_t632717506* (*) (ImmutableList_1_t258373478 *, const MethodInfo*))ImmutableList_1_get_Data_m1676800965_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>::Add(T)
#define ImmutableList_1_Add_m590243817(__this, ___value0, method) ((  ImmutableList_1_t258373478 * (*) (ImmutableList_1_t258373478 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Add_m948892931_gshared)(__this, ___value0, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>::Remove(T)
#define ImmutableList_1_Remove_m2901794476(__this, ___value0, method) ((  ImmutableList_1_t258373478 * (*) (ImmutableList_1_t258373478 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Remove_m1538917202_gshared)(__this, ___value0, method)
// System.Int32 UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkPlayer>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m3069572624(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t258373478 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_IndexOf_m3149323628_gshared)(__this, ___value0, method)
