﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select<System.Int32,UnityEngine.Color>
struct Select_t1940965564;
// UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>
struct SelectObservable_2_t3125378242;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int32,UnityEngine.Color>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m968401887_gshared (Select_t1940965564 * __this, SelectObservable_2_t3125378242 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select__ctor_m968401887(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select_t1940965564 *, SelectObservable_2_t3125378242 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select__ctor_m968401887_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int32,UnityEngine.Color>::OnNext(T)
extern "C"  void Select_OnNext_m4159855241_gshared (Select_t1940965564 * __this, int32_t ___value0, const MethodInfo* method);
#define Select_OnNext_m4159855241(__this, ___value0, method) ((  void (*) (Select_t1940965564 *, int32_t, const MethodInfo*))Select_OnNext_m4159855241_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int32,UnityEngine.Color>::OnError(System.Exception)
extern "C"  void Select_OnError_m2720678808_gshared (Select_t1940965564 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select_OnError_m2720678808(__this, ___error0, method) ((  void (*) (Select_t1940965564 *, Exception_t1967233988 *, const MethodInfo*))Select_OnError_m2720678808_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int32,UnityEngine.Color>::OnCompleted()
extern "C"  void Select_OnCompleted_m4291795691_gshared (Select_t1940965564 * __this, const MethodInfo* method);
#define Select_OnCompleted_m4291795691(__this, method) ((  void (*) (Select_t1940965564 *, const MethodInfo*))Select_OnCompleted_m4291795691_gshared)(__this, method)
