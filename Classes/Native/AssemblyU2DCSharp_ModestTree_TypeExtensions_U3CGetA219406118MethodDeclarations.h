﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F
struct U3CGetAllFieldsU3Ec__Iterator3F_t219406118;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
struct IEnumerator_1_t2648036230;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::.ctor()
extern "C"  void U3CGetAllFieldsU3Ec__Iterator3F__ctor_m1148109978 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::System.Collections.Generic.IEnumerator<System.Reflection.FieldInfo>.get_Current()
extern "C"  FieldInfo_t * U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_FieldInfoU3E_get_Current_m891825729 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_IEnumerator_get_Current_m2874874956 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_IEnumerable_GetEnumerator_m1698845575 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo> ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::System.Collections.Generic.IEnumerable<System.Reflection.FieldInfo>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetAllFieldsU3Ec__Iterator3F_System_Collections_Generic_IEnumerableU3CSystem_Reflection_FieldInfoU3E_GetEnumerator_m2441111908 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::MoveNext()
extern "C"  bool U3CGetAllFieldsU3Ec__Iterator3F_MoveNext_m1871247898 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::Dispose()
extern "C"  void U3CGetAllFieldsU3Ec__Iterator3F_Dispose_m3720344151 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.TypeExtensions/<GetAllFields>c__Iterator3F::Reset()
extern "C"  void U3CGetAllFieldsU3Ec__Iterator3F_Reset_m3089510215 (U3CGetAllFieldsU3Ec__Iterator3F_t219406118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
