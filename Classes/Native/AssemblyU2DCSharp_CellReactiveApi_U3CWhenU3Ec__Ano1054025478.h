﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// CellReactiveApi/<When>c__AnonStorey10A`1<System.Object>
struct U3CWhenU3Ec__AnonStorey10A_1_t1499050477;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Object>
struct  U3CWhenU3Ec__AnonStorey10B_1_t1054025478  : public Il2CppObject
{
public:
	// System.Action CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1::reaction
	Action_t437523947 * ___reaction_0;
	// CellReactiveApi/<When>c__AnonStorey10A`1<T> CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1::<>f__ref$266
	U3CWhenU3Ec__AnonStorey10A_1_t1499050477 * ___U3CU3Ef__refU24266_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CWhenU3Ec__AnonStorey10B_1_t1054025478, ___reaction_0)); }
	inline Action_t437523947 * get_reaction_0() const { return ___reaction_0; }
	inline Action_t437523947 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_t437523947 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24266_1() { return static_cast<int32_t>(offsetof(U3CWhenU3Ec__AnonStorey10B_1_t1054025478, ___U3CU3Ef__refU24266_1)); }
	inline U3CWhenU3Ec__AnonStorey10A_1_t1499050477 * get_U3CU3Ef__refU24266_1() const { return ___U3CU3Ef__refU24266_1; }
	inline U3CWhenU3Ec__AnonStorey10A_1_t1499050477 ** get_address_of_U3CU3Ef__refU24266_1() { return &___U3CU3Ef__refU24266_1; }
	inline void set_U3CU3Ef__refU24266_1(U3CWhenU3Ec__AnonStorey10A_1_t1499050477 * value)
	{
		___U3CU3Ef__refU24266_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24266_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
