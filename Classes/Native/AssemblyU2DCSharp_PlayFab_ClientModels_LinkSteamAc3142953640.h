﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.LinkSteamAccountRequest
struct  LinkSteamAccountRequest_t3142953640  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.LinkSteamAccountRequest::<SteamTicket>k__BackingField
	String_t* ___U3CSteamTicketU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CSteamTicketU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LinkSteamAccountRequest_t3142953640, ___U3CSteamTicketU3Ek__BackingField_0)); }
	inline String_t* get_U3CSteamTicketU3Ek__BackingField_0() const { return ___U3CSteamTicketU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CSteamTicketU3Ek__BackingField_0() { return &___U3CSteamTicketU3Ek__BackingField_0; }
	inline void set_U3CSteamTicketU3Ek__BackingField_0(String_t* value)
	{
		___U3CSteamTicketU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSteamTicketU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
