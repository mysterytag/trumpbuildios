﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalStat/fbProfile
struct  fbProfile_t675187021  : public Il2CppObject
{
public:
	// System.String GlobalStat/fbProfile::firstName
	String_t* ___firstName_0;
	// System.String GlobalStat/fbProfile::lastName
	String_t* ___lastName_1;
	// System.String GlobalStat/fbProfile::age
	String_t* ___age_2;
	// System.String GlobalStat/fbProfile::gender
	String_t* ___gender_3;
	// System.String GlobalStat/fbProfile::locale
	String_t* ___locale_4;
	// System.String GlobalStat/fbProfile::Id
	String_t* ___Id_5;

public:
	inline static int32_t get_offset_of_firstName_0() { return static_cast<int32_t>(offsetof(fbProfile_t675187021, ___firstName_0)); }
	inline String_t* get_firstName_0() const { return ___firstName_0; }
	inline String_t** get_address_of_firstName_0() { return &___firstName_0; }
	inline void set_firstName_0(String_t* value)
	{
		___firstName_0 = value;
		Il2CppCodeGenWriteBarrier(&___firstName_0, value);
	}

	inline static int32_t get_offset_of_lastName_1() { return static_cast<int32_t>(offsetof(fbProfile_t675187021, ___lastName_1)); }
	inline String_t* get_lastName_1() const { return ___lastName_1; }
	inline String_t** get_address_of_lastName_1() { return &___lastName_1; }
	inline void set_lastName_1(String_t* value)
	{
		___lastName_1 = value;
		Il2CppCodeGenWriteBarrier(&___lastName_1, value);
	}

	inline static int32_t get_offset_of_age_2() { return static_cast<int32_t>(offsetof(fbProfile_t675187021, ___age_2)); }
	inline String_t* get_age_2() const { return ___age_2; }
	inline String_t** get_address_of_age_2() { return &___age_2; }
	inline void set_age_2(String_t* value)
	{
		___age_2 = value;
		Il2CppCodeGenWriteBarrier(&___age_2, value);
	}

	inline static int32_t get_offset_of_gender_3() { return static_cast<int32_t>(offsetof(fbProfile_t675187021, ___gender_3)); }
	inline String_t* get_gender_3() const { return ___gender_3; }
	inline String_t** get_address_of_gender_3() { return &___gender_3; }
	inline void set_gender_3(String_t* value)
	{
		___gender_3 = value;
		Il2CppCodeGenWriteBarrier(&___gender_3, value);
	}

	inline static int32_t get_offset_of_locale_4() { return static_cast<int32_t>(offsetof(fbProfile_t675187021, ___locale_4)); }
	inline String_t* get_locale_4() const { return ___locale_4; }
	inline String_t** get_address_of_locale_4() { return &___locale_4; }
	inline void set_locale_4(String_t* value)
	{
		___locale_4 = value;
		Il2CppCodeGenWriteBarrier(&___locale_4, value);
	}

	inline static int32_t get_offset_of_Id_5() { return static_cast<int32_t>(offsetof(fbProfile_t675187021, ___Id_5)); }
	inline String_t* get_Id_5() const { return ___Id_5; }
	inline String_t** get_address_of_Id_5() { return &___Id_5; }
	inline void set_Id_5(String_t* value)
	{
		___Id_5 = value;
		Il2CppCodeGenWriteBarrier(&___Id_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
