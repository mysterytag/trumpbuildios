﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3370636040.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhenAllObservable`1<System.Object>
struct  WhenAllObservable_1_t2782777313  : public OperatorObservableBase_1_t3370636040
{
public:
	// UniRx.IObservable`1<T>[] UniRx.Operators.WhenAllObservable`1::sources
	IObservable_1U5BU5D_t2205425841* ___sources_1;
	// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>> UniRx.Operators.WhenAllObservable`1::sourcesEnumerable
	Il2CppObject* ___sourcesEnumerable_2;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(WhenAllObservable_1_t2782777313, ___sources_1)); }
	inline IObservable_1U5BU5D_t2205425841* get_sources_1() const { return ___sources_1; }
	inline IObservable_1U5BU5D_t2205425841** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(IObservable_1U5BU5D_t2205425841* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier(&___sources_1, value);
	}

	inline static int32_t get_offset_of_sourcesEnumerable_2() { return static_cast<int32_t>(offsetof(WhenAllObservable_1_t2782777313, ___sourcesEnumerable_2)); }
	inline Il2CppObject* get_sourcesEnumerable_2() const { return ___sourcesEnumerable_2; }
	inline Il2CppObject** get_address_of_sourcesEnumerable_2() { return &___sourcesEnumerable_2; }
	inline void set_sourcesEnumerable_2(Il2CppObject* value)
	{
		___sourcesEnumerable_2 = value;
		Il2CppCodeGenWriteBarrier(&___sourcesEnumerable_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
