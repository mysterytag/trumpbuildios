﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_t2073511664;

#include "codegen/il2cpp-codegen.h"

// System.Void DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder::.ctor()
extern "C"  void ABSPathDecoder__ctor_m503157611 (ABSPathDecoder_t2073511664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
