﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewTableView
struct NewTableView_t2775249395;
// ITableBlock
struct ITableBlock_t2428542600;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void NewTableView::.ctor()
extern "C"  void NewTableView__ctor_m1590854152 (NewTableView_t2775249395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::Awake()
extern "C"  void NewTableView_Awake_m1828459371 (NewTableView_t2775249395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::OnEnable()
extern "C"  void NewTableView_OnEnable_m243509374 (NewTableView_t2775249395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::OnDisable()
extern "C"  void NewTableView_OnDisable_m3694760559 (NewTableView_t2775249395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::ScrollChanged(UnityEngine.Vector2)
extern "C"  void NewTableView_ScrollChanged_m2876431117 (NewTableView_t2775249395 * __this, Vector2_t3525329788  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::Clear()
extern "C"  void NewTableView_Clear_m3291954739 (NewTableView_t2775249395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::RemoveBlock(ITableBlock)
extern "C"  void NewTableView_RemoveBlock_m191244155 (NewTableView_t2775249395 * __this, Il2CppObject * ___tableBlock0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::Update()
extern "C"  void NewTableView_Update_m3798700549 (NewTableView_t2775249395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::Rebuild()
extern "C"  void NewTableView_Rebuild_m4014602049 (NewTableView_t2775249395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewTableView::NeedRebuild()
extern "C"  void NewTableView_NeedRebuild_m2127486155 (NewTableView_t2775249395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NewTableView::<Rebuild>m__132(ITableBlock)
extern "C"  float NewTableView_U3CRebuildU3Em__132_m245028846 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___block0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
