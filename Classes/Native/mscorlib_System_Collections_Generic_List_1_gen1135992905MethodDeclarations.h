﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.DateTime>
struct List_1_t1135992905;
// System.Collections.Generic.IEnumerable`1<System.DateTime>
struct IEnumerable_1_t3211188292;
// System.Collections.Generic.IEnumerator`1<System.DateTime>
struct IEnumerator_1_t1822140384;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.DateTime>
struct ICollection_1_t804865322;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>
struct ReadOnlyCollection_1_t3502179284;
// System.DateTime[]
struct DateTimeU5BU5D_t2411579761;
// System.Predicate`1<System.DateTime>
struct Predicate_1_t909997834;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// System.Collections.Generic.IComparer`1<System.DateTime>
struct IComparer_1_t3038741345;
// System.Comparison`1<System.DateTime>
struct Comparison_1_t3042708812;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3516743193.h"

// System.Void System.Collections.Generic.List`1<System.DateTime>::.ctor()
extern "C"  void List_1__ctor_m2882066544_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1__ctor_m2882066544(__this, method) ((  void (*) (List_1_t1135992905 *, const MethodInfo*))List_1__ctor_m2882066544_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1372859919_gshared (List_1_t1135992905 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1372859919(__this, ___collection0, method) ((  void (*) (List_1_t1135992905 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1372859919_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1025861057_gshared (List_1_t1135992905 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1025861057(__this, ___capacity0, method) ((  void (*) (List_1_t1135992905 *, int32_t, const MethodInfo*))List_1__ctor_m1025861057_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::.cctor()
extern "C"  void List_1__cctor_m2962620733_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2962620733(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2962620733_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.DateTime>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3176475842_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3176475842(__this, method) ((  Il2CppObject* (*) (List_1_t1135992905 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3176475842_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1136747220_gshared (List_1_t1135992905 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1136747220(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1135992905 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1136747220_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.DateTime>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1494398799_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1494398799(__this, method) ((  Il2CppObject * (*) (List_1_t1135992905 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1494398799_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4182304130_gshared (List_1_t1135992905 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4182304130(__this, ___item0, method) ((  int32_t (*) (List_1_t1135992905 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4182304130_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2446131722_gshared (List_1_t1135992905 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2446131722(__this, ___item0, method) ((  bool (*) (List_1_t1135992905 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2446131722_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1723239130_gshared (List_1_t1135992905 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1723239130(__this, ___item0, method) ((  int32_t (*) (List_1_t1135992905 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1723239130_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2435853573_gshared (List_1_t1135992905 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2435853573(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1135992905 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2435853573_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2493888515_gshared (List_1_t1135992905 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2493888515(__this, ___item0, method) ((  void (*) (List_1_t1135992905 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2493888515_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.DateTime>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2945281419_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2945281419(__this, method) ((  bool (*) (List_1_t1135992905 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2945281419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.DateTime>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m4289962258_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4289962258(__this, method) ((  bool (*) (List_1_t1135992905 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m4289962258_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.DateTime>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m954398014_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m954398014(__this, method) ((  Il2CppObject * (*) (List_1_t1135992905 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m954398014_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2786689273_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2786689273(__this, method) ((  bool (*) (List_1_t1135992905 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2786689273_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m959544032_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m959544032(__this, method) ((  bool (*) (List_1_t1135992905 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m959544032_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3414069061_gshared (List_1_t1135992905 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3414069061(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1135992905 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3414069061_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m712059804_gshared (List_1_t1135992905 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m712059804(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1135992905 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m712059804_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::Add(T)
extern "C"  void List_1_Add_m2576964111_gshared (List_1_t1135992905 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define List_1_Add_m2576964111(__this, ___item0, method) ((  void (*) (List_1_t1135992905 *, DateTime_t339033936 , const MethodInfo*))List_1_Add_m2576964111_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m34728650_gshared (List_1_t1135992905 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m34728650(__this, ___newCount0, method) ((  void (*) (List_1_t1135992905 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m34728650_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m689215944_gshared (List_1_t1135992905 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m689215944(__this, ___collection0, method) ((  void (*) (List_1_t1135992905 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m689215944_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m4245155464_gshared (List_1_t1135992905 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m4245155464(__this, ___enumerable0, method) ((  void (*) (List_1_t1135992905 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m4245155464_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1947208463_gshared (List_1_t1135992905 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1947208463(__this, ___collection0, method) ((  void (*) (List_1_t1135992905 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1947208463_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.DateTime>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3502179284 * List_1_AsReadOnly_m1238885818_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1238885818(__this, method) ((  ReadOnlyCollection_1_t3502179284 * (*) (List_1_t1135992905 *, const MethodInfo*))List_1_AsReadOnly_m1238885818_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::Clear()
extern "C"  void List_1_Clear_m288199835_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_Clear_m288199835(__this, method) ((  void (*) (List_1_t1135992905 *, const MethodInfo*))List_1_Clear_m288199835_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.DateTime>::Contains(T)
extern "C"  bool List_1_Contains_m1744205001_gshared (List_1_t1135992905 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define List_1_Contains_m1744205001(__this, ___item0, method) ((  bool (*) (List_1_t1135992905 *, DateTime_t339033936 , const MethodInfo*))List_1_Contains_m1744205001_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2245557311_gshared (List_1_t1135992905 * __this, DateTimeU5BU5D_t2411579761* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2245557311(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1135992905 *, DateTimeU5BU5D_t2411579761*, int32_t, const MethodInfo*))List_1_CopyTo_m2245557311_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.DateTime>::Find(System.Predicate`1<T>)
extern "C"  DateTime_t339033936  List_1_Find_m200637193_gshared (List_1_t1135992905 * __this, Predicate_1_t909997834 * ___match0, const MethodInfo* method);
#define List_1_Find_m200637193(__this, ___match0, method) ((  DateTime_t339033936  (*) (List_1_t1135992905 *, Predicate_1_t909997834 *, const MethodInfo*))List_1_Find_m200637193_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2890398084_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t909997834 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2890398084(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t909997834 *, const MethodInfo*))List_1_CheckMatch_m2890398084_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.DateTime>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3970282409_gshared (List_1_t1135992905 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t909997834 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3970282409(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1135992905 *, int32_t, int32_t, Predicate_1_t909997834 *, const MethodInfo*))List_1_GetIndex_m3970282409_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m4155328504_gshared (List_1_t1135992905 * __this, Action_1_t487486641 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m4155328504(__this, ___action0, method) ((  void (*) (List_1_t1135992905 *, Action_1_t487486641 *, const MethodInfo*))List_1_ForEach_m4155328504_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.DateTime>::GetEnumerator()
extern "C"  Enumerator_t3516743193  List_1_GetEnumerator_m794615174_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m794615174(__this, method) ((  Enumerator_t3516743193  (*) (List_1_t1135992905 *, const MethodInfo*))List_1_GetEnumerator_m794615174_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.DateTime>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3099507971_gshared (List_1_t1135992905 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3099507971(__this, ___item0, method) ((  int32_t (*) (List_1_t1135992905 *, DateTime_t339033936 , const MethodInfo*))List_1_IndexOf_m3099507971_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1019978646_gshared (List_1_t1135992905 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1019978646(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1135992905 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1019978646_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1991924495_gshared (List_1_t1135992905 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1991924495(__this, ___index0, method) ((  void (*) (List_1_t1135992905 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1991924495_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4153841270_gshared (List_1_t1135992905 * __this, int32_t ___index0, DateTime_t339033936  ___item1, const MethodInfo* method);
#define List_1_Insert_m4153841270(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1135992905 *, int32_t, DateTime_t339033936 , const MethodInfo*))List_1_Insert_m4153841270_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1438227051_gshared (List_1_t1135992905 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1438227051(__this, ___collection0, method) ((  void (*) (List_1_t1135992905 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1438227051_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.DateTime>::Remove(T)
extern "C"  bool List_1_Remove_m3768832772_gshared (List_1_t1135992905 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define List_1_Remove_m3768832772(__this, ___item0, method) ((  bool (*) (List_1_t1135992905 *, DateTime_t339033936 , const MethodInfo*))List_1_Remove_m3768832772_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.DateTime>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m806523014_gshared (List_1_t1135992905 * __this, Predicate_1_t909997834 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m806523014(__this, ___match0, method) ((  int32_t (*) (List_1_t1135992905 *, Predicate_1_t909997834 *, const MethodInfo*))List_1_RemoveAll_m806523014_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2027694140_gshared (List_1_t1135992905 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2027694140(__this, ___index0, method) ((  void (*) (List_1_t1135992905 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2027694140_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::Reverse()
extern "C"  void List_1_Reverse_m3744821424_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_Reverse_m3744821424(__this, method) ((  void (*) (List_1_t1135992905 *, const MethodInfo*))List_1_Reverse_m3744821424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::Sort()
extern "C"  void List_1_Sort_m3102936306_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_Sort_m3102936306(__this, method) ((  void (*) (List_1_t1135992905 *, const MethodInfo*))List_1_Sort_m3102936306_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3928370_gshared (List_1_t1135992905 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3928370(__this, ___comparer0, method) ((  void (*) (List_1_t1135992905 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3928370_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1347090373_gshared (List_1_t1135992905 * __this, Comparison_1_t3042708812 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1347090373(__this, ___comparison0, method) ((  void (*) (List_1_t1135992905 *, Comparison_1_t3042708812 *, const MethodInfo*))List_1_Sort_m1347090373_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.DateTime>::ToArray()
extern "C"  DateTimeU5BU5D_t2411579761* List_1_ToArray_m4104654063_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_ToArray_m4104654063(__this, method) ((  DateTimeU5BU5D_t2411579761* (*) (List_1_t1135992905 *, const MethodInfo*))List_1_ToArray_m4104654063_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2873962251_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2873962251(__this, method) ((  void (*) (List_1_t1135992905 *, const MethodInfo*))List_1_TrimExcess_m2873962251_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.DateTime>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1902279603_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1902279603(__this, method) ((  int32_t (*) (List_1_t1135992905 *, const MethodInfo*))List_1_get_Capacity_m1902279603_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1539884764_gshared (List_1_t1135992905 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1539884764(__this, ___value0, method) ((  void (*) (List_1_t1135992905 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1539884764_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.DateTime>::get_Count()
extern "C"  int32_t List_1_get_Count_m1412774104_gshared (List_1_t1135992905 * __this, const MethodInfo* method);
#define List_1_get_Count_m1412774104(__this, method) ((  int32_t (*) (List_1_t1135992905 *, const MethodInfo*))List_1_get_Count_m1412774104_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.DateTime>::get_Item(System.Int32)
extern "C"  DateTime_t339033936  List_1_get_Item_m377781760_gshared (List_1_t1135992905 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m377781760(__this, ___index0, method) ((  DateTime_t339033936  (*) (List_1_t1135992905 *, int32_t, const MethodInfo*))List_1_get_Item_m377781760_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.DateTime>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3626439117_gshared (List_1_t1135992905 * __this, int32_t ___index0, DateTime_t339033936  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3626439117(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1135992905 *, int32_t, DateTime_t339033936 , const MethodInfo*))List_1_set_Item_m3626439117_gshared)(__this, ___index0, ___value1, method)
