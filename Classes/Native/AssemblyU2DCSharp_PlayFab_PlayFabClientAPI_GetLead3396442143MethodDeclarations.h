﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerRequestCallback
struct GetLeaderboardAroundPlayerRequestCallback_t3396442143;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest
struct GetLeaderboardAroundPlayerRequest_t299203018;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderboa299203018.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardAroundPlayerRequestCallback__ctor_m196115758 (GetLeaderboardAroundPlayerRequestCallback_t3396442143 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest,System.Object)
extern "C"  void GetLeaderboardAroundPlayerRequestCallback_Invoke_m3197846811 (GetLeaderboardAroundPlayerRequestCallback_t3396442143 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundPlayerRequest_t299203018 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundPlayerRequestCallback_t3396442143(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundPlayerRequest_t299203018 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundPlayerRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardAroundPlayerRequestCallback_BeginInvoke_m4138055934 (GetLeaderboardAroundPlayerRequestCallback_t3396442143 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundPlayerRequest_t299203018 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundPlayerRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardAroundPlayerRequestCallback_EndInvoke_m943568446 (GetLeaderboardAroundPlayerRequestCallback_t3396442143 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
