﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_7_t932604128;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_7_t1858605825;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ZipLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.IObservable`1<T5>,UniRx.IObservable`1<T6>,UniRx.Operators.ZipLatestFunc`7<T1,T2,T3,T4,T5,T6,TR>)
extern "C"  void ZipLatestObservable_7__ctor_m2703913424_gshared (ZipLatestObservable_7_t932604128 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, Il2CppObject* ___source54, Il2CppObject* ___source65, ZipLatestFunc_7_t1858605825 * ___resultSelector6, const MethodInfo* method);
#define ZipLatestObservable_7__ctor_m2703913424(__this, ___source10, ___source21, ___source32, ___source43, ___source54, ___source65, ___resultSelector6, method) ((  void (*) (ZipLatestObservable_7_t932604128 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, ZipLatestFunc_7_t1858605825 *, const MethodInfo*))ZipLatestObservable_7__ctor_m2703913424_gshared)(__this, ___source10, ___source21, ___source32, ___source43, ___source54, ___source65, ___resultSelector6, method)
// System.IDisposable UniRx.Operators.ZipLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * ZipLatestObservable_7_SubscribeCore_m259703900_gshared (ZipLatestObservable_7_t932604128 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ZipLatestObservable_7_SubscribeCore_m259703900(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ZipLatestObservable_7_t932604128 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipLatestObservable_7_SubscribeCore_m259703900_gshared)(__this, ___observer0, ___cancel1, method)
