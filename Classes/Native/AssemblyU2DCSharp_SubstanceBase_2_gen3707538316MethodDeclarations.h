﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2<System.Boolean,System.Boolean>
struct SubstanceBase_2_t3707538316;
// ICell`1<System.Boolean>
struct ICell_1_t1762636318;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// IStream`1<System.Boolean>
struct IStream_1_t763696332;
// SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>
struct Intrusion_t1917188776;
// ILiving
struct ILiving_t2639664210;
// IEmptyStream
struct IEmptyStream_t3684082468;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m162542656_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method);
#define SubstanceBase_2__ctor_m162542656(__this, method) ((  void (*) (SubstanceBase_2_t3707538316 *, const MethodInfo*))SubstanceBase_2__ctor_m162542656_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m743856414_gshared (SubstanceBase_2_t3707538316 * __this, bool ___baseVal0, const MethodInfo* method);
#define SubstanceBase_2__ctor_m743856414(__this, ___baseVal0, method) ((  void (*) (SubstanceBase_2_t3707538316 *, bool, const MethodInfo*))SubstanceBase_2__ctor_m743856414_gshared)(__this, ___baseVal0, method)
// T SubstanceBase`2<System.Boolean,System.Boolean>::get_baseValue()
extern "C"  bool SubstanceBase_2_get_baseValue_m837721814_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_baseValue_m837721814(__this, method) ((  bool (*) (SubstanceBase_2_t3707538316 *, const MethodInfo*))SubstanceBase_2_get_baseValue_m837721814_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::set_baseValue(T)
extern "C"  void SubstanceBase_2_set_baseValue_m3914724413_gshared (SubstanceBase_2_t3707538316 * __this, bool ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValue_m3914724413(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t3707538316 *, bool, const MethodInfo*))SubstanceBase_2_set_baseValue_m3914724413_gshared)(__this, ___value0, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m822111824_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValueReactive_m822111824(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t3707538316 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_set_baseValueReactive_m822111824_gshared)(__this, ___value0, method)
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m276560366_gshared (SubstanceBase_2_t3707538316 * __this, Action_1_t359458046 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_Bind_m276560366(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, Action_1_t359458046 *, int32_t, const MethodInfo*))SubstanceBase_2_Bind_m276560366_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m845462693_gshared (SubstanceBase_2_t3707538316 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_OnChanged_m845462693(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, Action_t437523947 *, int32_t, const MethodInfo*))SubstanceBase_2_OnChanged_m845462693_gshared)(__this, ___action0, ___p1, method)
// System.Object SubstanceBase`2<System.Boolean,System.Boolean>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m2890777104_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_valueObject_m2890777104(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, const MethodInfo*))SubstanceBase_2_get_valueObject_m2890777104_gshared)(__this, method)
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m3872388564_gshared (SubstanceBase_2_t3707538316 * __this, Action_1_t359458046 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_ListenUpdates_m3872388564(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, Action_1_t359458046 *, int32_t, const MethodInfo*))SubstanceBase_2_ListenUpdates_m3872388564_gshared)(__this, ___action0, ___p1, method)
// IStream`1<T> SubstanceBase`2<System.Boolean,System.Boolean>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m2333122798_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_updates_m2333122798(__this, method) ((  Il2CppObject* (*) (SubstanceBase_2_t3707538316 *, const MethodInfo*))SubstanceBase_2_get_updates_m2333122798_gshared)(__this, method)
// T SubstanceBase`2<System.Boolean,System.Boolean>::get_value()
extern "C"  bool SubstanceBase_2_get_value_m3696610663_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_value_m3696610663(__this, method) ((  bool (*) (SubstanceBase_2_t3707538316 *, const MethodInfo*))SubstanceBase_2_get_value_m3696610663_gshared)(__this, method)
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Boolean,System.Boolean>::AffectInner(ILiving,InfT)
extern "C"  Intrusion_t1917188776 * SubstanceBase_2_AffectInner_m2503779489_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject * ___intruder0, bool ___influence1, const MethodInfo* method);
#define SubstanceBase_2_AffectInner_m2503779489(__this, ___intruder0, ___influence1, method) ((  Intrusion_t1917188776 * (*) (SubstanceBase_2_t3707538316 *, Il2CppObject *, bool, const MethodInfo*))SubstanceBase_2_AffectInner_m2503779489_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::AffectUnsafe(InfT)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m4234747039_gshared (SubstanceBase_2_t3707538316 * __this, bool ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m4234747039(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, bool, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m4234747039_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::AffectUnsafe(ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m3711033429_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject* ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m3711033429(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m3711033429_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Affect(ILiving,InfT)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m2462561983_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject * ___intruder0, bool ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m2462561983(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, Il2CppObject *, bool, const MethodInfo*))SubstanceBase_2_Affect_m2462561983_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Affect(ILiving,ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1204726325_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m1204726325(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_Affect_m1204726325_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Affect(ILiving,InfT,IEmptyStream)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3611334147_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject * ___intruder0, bool ___influence1, Il2CppObject * ___update2, const MethodInfo* method);
#define SubstanceBase_2_Affect_m3611334147(__this, ___intruder0, ___influence1, ___update2, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3707538316 *, Il2CppObject *, bool, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m3611334147_gshared)(__this, ___intruder0, ___influence1, ___update2, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m1962503088_gshared (SubstanceBase_2_t3707538316 * __this, Intrusion_t1917188776 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnAdd_m1962503088(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3707538316 *, Intrusion_t1917188776 *, const MethodInfo*))SubstanceBase_2_OnAdd_m1962503088_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m4132644265_gshared (SubstanceBase_2_t3707538316 * __this, Intrusion_t1917188776 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnRemove_m4132644265(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3707538316 *, Intrusion_t1917188776 *, const MethodInfo*))SubstanceBase_2_OnRemove_m4132644265_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m3483375428_gshared (SubstanceBase_2_t3707538316 * __this, Intrusion_t1917188776 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnUpdate_m3483375428(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3707538316 *, Intrusion_t1917188776 *, const MethodInfo*))SubstanceBase_2_OnUpdate_m3483375428_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m2304746742_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method);
#define SubstanceBase_2_UpdateAll_m2304746742(__this, method) ((  void (*) (SubstanceBase_2_t3707538316 *, const MethodInfo*))SubstanceBase_2_UpdateAll_m2304746742_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_ClearIntrusion_m1183670130_gshared (SubstanceBase_2_t3707538316 * __this, Intrusion_t1917188776 * ___intr0, const MethodInfo* method);
#define SubstanceBase_2_ClearIntrusion_m1183670130(__this, ___intr0, method) ((  void (*) (SubstanceBase_2_t3707538316 *, Intrusion_t1917188776 *, const MethodInfo*))SubstanceBase_2_ClearIntrusion_m1183670130_gshared)(__this, ___intr0, method)
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m797099883_gshared (SubstanceBase_2_t3707538316 * __this, bool ___val0, const MethodInfo* method);
#define SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m797099883(__this, ___val0, method) ((  void (*) (SubstanceBase_2_t3707538316 *, bool, const MethodInfo*))SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m797099883_gshared)(__this, ___val0, method)
