﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<Zenject.InjectContext,ModestTree.Util.Tuple`2<System.Type,System.Int32>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2095230383(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1208720932 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Zenject.InjectContext,ModestTree.Util.Tuple`2<System.Type,System.Int32>>::Invoke(T)
#define Func_2_Invoke_m4052164179(__this, ___arg10, method) ((  Tuple_2_t3719549051 * (*) (Func_2_t1208720932 *, InjectContext_t3456483891 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Zenject.InjectContext,ModestTree.Util.Tuple`2<System.Type,System.Int32>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2724319874(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1208720932 *, InjectContext_t3456483891 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Zenject.InjectContext,ModestTree.Util.Tuple`2<System.Type,System.Int32>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2699519841(__this, ___result0, method) ((  Tuple_2_t3719549051 * (*) (Func_2_t1208720932 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
