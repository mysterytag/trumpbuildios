﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPurchase>c__AnonStoreyA3
struct U3CGetPurchaseU3Ec__AnonStoreyA3_t1036006006;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPurchase>c__AnonStoreyA3::.ctor()
extern "C"  void U3CGetPurchaseU3Ec__AnonStoreyA3__ctor_m470792605 (U3CGetPurchaseU3Ec__AnonStoreyA3_t1036006006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPurchase>c__AnonStoreyA3::<>m__90(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPurchaseU3Ec__AnonStoreyA3_U3CU3Em__90_m921148562 (U3CGetPurchaseU3Ec__AnonStoreyA3_t1036006006 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
