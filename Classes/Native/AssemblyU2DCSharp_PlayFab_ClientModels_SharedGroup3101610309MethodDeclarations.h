﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.SharedGroupDataRecord
struct SharedGroupDataRecord_t3101610309;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"

// System.Void PlayFab.ClientModels.SharedGroupDataRecord::.ctor()
extern "C"  void SharedGroupDataRecord__ctor_m127196036 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SharedGroupDataRecord::get_Value()
extern "C"  String_t* SharedGroupDataRecord_get_Value_m2777304039 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::set_Value(System.String)
extern "C"  void SharedGroupDataRecord_set_Value_m1117617228 (SharedGroupDataRecord_t3101610309 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SharedGroupDataRecord::get_LastUpdatedBy()
extern "C"  String_t* SharedGroupDataRecord_get_LastUpdatedBy_m3856816946 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::set_LastUpdatedBy(System.String)
extern "C"  void SharedGroupDataRecord_set_LastUpdatedBy_m1334148577 (SharedGroupDataRecord_t3101610309 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayFab.ClientModels.SharedGroupDataRecord::get_LastUpdated()
extern "C"  DateTime_t339033936  SharedGroupDataRecord_get_LastUpdated_m2428103685 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::set_LastUpdated(System.DateTime)
extern "C"  void SharedGroupDataRecord_set_LastUpdated_m1405734574 (SharedGroupDataRecord_t3101610309 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.UserDataPermission> PlayFab.ClientModels.SharedGroupDataRecord::get_Permission()
extern "C"  Nullable_1_t2611574664  SharedGroupDataRecord_get_Permission_m1737884622 (SharedGroupDataRecord_t3101610309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SharedGroupDataRecord::set_Permission(System.Nullable`1<PlayFab.ClientModels.UserDataPermission>)
extern "C"  void SharedGroupDataRecord_set_Permission_m2277968901 (SharedGroupDataRecord_t3101610309 * __this, Nullable_1_t2611574664  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
