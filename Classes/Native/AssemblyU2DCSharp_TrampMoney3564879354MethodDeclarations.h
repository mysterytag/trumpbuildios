﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrampMoney
struct TrampMoney_t3564879354;

#include "codegen/il2cpp-codegen.h"

// System.Void TrampMoney::.ctor()
extern "C"  void TrampMoney__ctor_m1777136609 (TrampMoney_t3564879354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrampMoney::PostInject()
extern "C"  void TrampMoney_PostInject_m1292854740 (TrampMoney_t3564879354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrampMoney::<PostInject>m__279(System.Double)
extern "C"  void TrampMoney_U3CPostInjectU3Em__279_m865053607 (TrampMoney_t3564879354 * __this, double ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
