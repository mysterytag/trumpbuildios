﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>
struct TakeUntil_t1361590388;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>
struct  TakeUntilOther_t387441882  : public Il2CppObject
{
public:
	// UniRx.Operators.TakeUntilObservable`2/TakeUntil<T,TOther> UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther::sourceObserver
	TakeUntil_t1361590388 * ___sourceObserver_0;
	// System.IDisposable UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther::subscription
	Il2CppObject * ___subscription_1;

public:
	inline static int32_t get_offset_of_sourceObserver_0() { return static_cast<int32_t>(offsetof(TakeUntilOther_t387441882, ___sourceObserver_0)); }
	inline TakeUntil_t1361590388 * get_sourceObserver_0() const { return ___sourceObserver_0; }
	inline TakeUntil_t1361590388 ** get_address_of_sourceObserver_0() { return &___sourceObserver_0; }
	inline void set_sourceObserver_0(TakeUntil_t1361590388 * value)
	{
		___sourceObserver_0 = value;
		Il2CppCodeGenWriteBarrier(&___sourceObserver_0, value);
	}

	inline static int32_t get_offset_of_subscription_1() { return static_cast<int32_t>(offsetof(TakeUntilOther_t387441882, ___subscription_1)); }
	inline Il2CppObject * get_subscription_1() const { return ___subscription_1; }
	inline Il2CppObject ** get_address_of_subscription_1() { return &___subscription_1; }
	inline void set_subscription_1(Il2CppObject * value)
	{
		___subscription_1 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
