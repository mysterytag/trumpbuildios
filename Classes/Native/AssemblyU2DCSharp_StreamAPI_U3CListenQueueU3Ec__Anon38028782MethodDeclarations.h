﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<ListenQueue>c__AnonStorey12D`1<System.Object>
struct U3CListenQueueU3Ec__AnonStorey12D_1_t38028782;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamAPI/<ListenQueue>c__AnonStorey12D`1<System.Object>::.ctor()
extern "C"  void U3CListenQueueU3Ec__AnonStorey12D_1__ctor_m2689374087_gshared (U3CListenQueueU3Ec__AnonStorey12D_1_t38028782 * __this, const MethodInfo* method);
#define U3CListenQueueU3Ec__AnonStorey12D_1__ctor_m2689374087(__this, method) ((  void (*) (U3CListenQueueU3Ec__AnonStorey12D_1_t38028782 *, const MethodInfo*))U3CListenQueueU3Ec__AnonStorey12D_1__ctor_m2689374087_gshared)(__this, method)
// System.Void StreamAPI/<ListenQueue>c__AnonStorey12D`1<System.Object>::<>m__1C1(T)
extern "C"  void U3CListenQueueU3Ec__AnonStorey12D_1_U3CU3Em__1C1_m1599378349_gshared (U3CListenQueueU3Ec__AnonStorey12D_1_t38028782 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListenQueueU3Ec__AnonStorey12D_1_U3CU3Em__1C1_m1599378349(__this, ___obj0, method) ((  void (*) (U3CListenQueueU3Ec__AnonStorey12D_1_t38028782 *, Il2CppObject *, const MethodInfo*))U3CListenQueueU3Ec__AnonStorey12D_1_U3CU3Em__1C1_m1599378349_gshared)(__this, ___obj0, method)
