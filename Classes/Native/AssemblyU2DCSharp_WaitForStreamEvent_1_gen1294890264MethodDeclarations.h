﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WaitForStreamEvent`1<System.Object>
struct WaitForStreamEvent_1_t1294890264;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"

// System.Void WaitForStreamEvent`1<System.Object>::.ctor(IStream`1<T>,System.Action`1<T>)
extern "C"  void WaitForStreamEvent_1__ctor_m3706988780_gshared (WaitForStreamEvent_1_t1294890264 * __this, Il2CppObject* ___stream0, Action_1_t985559125 * ___act1, const MethodInfo* method);
#define WaitForStreamEvent_1__ctor_m3706988780(__this, ___stream0, ___act1, method) ((  void (*) (WaitForStreamEvent_1_t1294890264 *, Il2CppObject*, Action_1_t985559125 *, const MethodInfo*))WaitForStreamEvent_1__ctor_m3706988780_gshared)(__this, ___stream0, ___act1, method)
// System.Boolean WaitForStreamEvent`1<System.Object>::get_finished()
extern "C"  bool WaitForStreamEvent_1_get_finished_m303596645_gshared (WaitForStreamEvent_1_t1294890264 * __this, const MethodInfo* method);
#define WaitForStreamEvent_1_get_finished_m303596645(__this, method) ((  bool (*) (WaitForStreamEvent_1_t1294890264 *, const MethodInfo*))WaitForStreamEvent_1_get_finished_m303596645_gshared)(__this, method)
// System.Void WaitForStreamEvent`1<System.Object>::Tick(System.Single)
extern "C"  void WaitForStreamEvent_1_Tick_m3307853552_gshared (WaitForStreamEvent_1_t1294890264 * __this, float ___dt0, const MethodInfo* method);
#define WaitForStreamEvent_1_Tick_m3307853552(__this, ___dt0, method) ((  void (*) (WaitForStreamEvent_1_t1294890264 *, float, const MethodInfo*))WaitForStreamEvent_1_Tick_m3307853552_gshared)(__this, ___dt0, method)
// System.Void WaitForStreamEvent`1<System.Object>::Dispose()
extern "C"  void WaitForStreamEvent_1_Dispose_m3277807363_gshared (WaitForStreamEvent_1_t1294890264 * __this, const MethodInfo* method);
#define WaitForStreamEvent_1_Dispose_m3277807363(__this, method) ((  void (*) (WaitForStreamEvent_1_t1294890264 *, const MethodInfo*))WaitForStreamEvent_1_Dispose_m3277807363_gshared)(__this, method)
