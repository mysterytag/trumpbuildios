﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.MasterServerEvent>
struct Subject_1_t4215842110;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>
struct IObserver_1_t194839097;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"

// System.Void UniRx.Subject`1<UnityEngine.MasterServerEvent>::.ctor()
extern "C"  void Subject_1__ctor_m3947280824_gshared (Subject_1_t4215842110 * __this, const MethodInfo* method);
#define Subject_1__ctor_m3947280824(__this, method) ((  void (*) (Subject_1_t4215842110 *, const MethodInfo*))Subject_1__ctor_m3947280824_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.MasterServerEvent>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2103580884_gshared (Subject_1_t4215842110 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m2103580884(__this, method) ((  bool (*) (Subject_1_t4215842110 *, const MethodInfo*))Subject_1_get_HasObservers_m2103580884_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.MasterServerEvent>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3053306690_gshared (Subject_1_t4215842110 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3053306690(__this, method) ((  void (*) (Subject_1_t4215842110 *, const MethodInfo*))Subject_1_OnCompleted_m3053306690_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.MasterServerEvent>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m182690159_gshared (Subject_1_t4215842110 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m182690159(__this, ___error0, method) ((  void (*) (Subject_1_t4215842110 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m182690159_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.MasterServerEvent>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3711452768_gshared (Subject_1_t4215842110 * __this, int32_t ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m3711452768(__this, ___value0, method) ((  void (*) (Subject_1_t4215842110 *, int32_t, const MethodInfo*))Subject_1_OnNext_m3711452768_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.MasterServerEvent>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m3213533623_gshared (Subject_1_t4215842110 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m3213533623(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t4215842110 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m3213533623_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.MasterServerEvent>::Dispose()
extern "C"  void Subject_1_Dispose_m779032565_gshared (Subject_1_t4215842110 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m779032565(__this, method) ((  void (*) (Subject_1_t4215842110 *, const MethodInfo*))Subject_1_Dispose_m779032565_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.MasterServerEvent>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1778646142_gshared (Subject_1_t4215842110 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1778646142(__this, method) ((  void (*) (Subject_1_t4215842110 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1778646142_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.MasterServerEvent>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3356588235_gshared (Subject_1_t4215842110 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3356588235(__this, method) ((  bool (*) (Subject_1_t4215842110 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3356588235_gshared)(__this, method)
