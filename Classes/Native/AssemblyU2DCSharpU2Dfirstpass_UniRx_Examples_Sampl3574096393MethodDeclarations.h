﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample04_ConvertFromUnityCallback
struct Sample04_ConvertFromUnityCallback_t3574096393;
// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback
struct LogCallback_t3235662730;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3235662729.h"

// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback::.ctor()
extern "C"  void Sample04_ConvertFromUnityCallback__ctor_m2143655214 (Sample04_ConvertFromUnityCallback_t3574096393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback::Awake()
extern "C"  void Sample04_ConvertFromUnityCallback_Awake_m2381260433 (Sample04_ConvertFromUnityCallback_t3574096393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample04_ConvertFromUnityCallback::<Awake>m__13(UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback)
extern "C"  bool Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__13_m4234092497 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662730 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback::<Awake>m__14(UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback)
extern "C"  void Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__14_m2095151222 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662730 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample04_ConvertFromUnityCallback::<Awake>m__15(UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback)
extern "C"  bool Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__15_m3816695891 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662730 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback::<Awake>m__16(UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback)
extern "C"  void Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__16_m1677754616 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662730 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
