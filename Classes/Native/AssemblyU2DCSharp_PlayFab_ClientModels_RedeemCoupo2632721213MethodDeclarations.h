﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RedeemCouponRequest
struct RedeemCouponRequest_t2632721213;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.RedeemCouponRequest::.ctor()
extern "C"  void RedeemCouponRequest__ctor_m2533256716 (RedeemCouponRequest_t2632721213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RedeemCouponRequest::get_CouponCode()
extern "C"  String_t* RedeemCouponRequest_get_CouponCode_m2663500311 (RedeemCouponRequest_t2632721213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RedeemCouponRequest::set_CouponCode(System.String)
extern "C"  void RedeemCouponRequest_set_CouponCode_m4040697818 (RedeemCouponRequest_t2632721213 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RedeemCouponRequest::get_CatalogVersion()
extern "C"  String_t* RedeemCouponRequest_get_CatalogVersion_m2839698659 (RedeemCouponRequest_t2632721213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RedeemCouponRequest::set_CatalogVersion(System.String)
extern "C"  void RedeemCouponRequest_set_CatalogVersion_m3681427726 (RedeemCouponRequest_t2632721213 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
