﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionFalse
struct XPathFunctionFalse_t1362233227;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionFalse::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionFalse__ctor_m1496753845 (XPathFunctionFalse_t1362233227 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_HasStaticValue()
extern "C"  bool XPathFunctionFalse_get_HasStaticValue_m3224612098 (XPathFunctionFalse_t1362233227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_StaticValueAsBoolean()
extern "C"  bool XPathFunctionFalse_get_StaticValueAsBoolean_m891110156 (XPathFunctionFalse_t1362233227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_Peer()
extern "C"  bool XPathFunctionFalse_get_Peer_m1943435643 (XPathFunctionFalse_t1362233227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionFalse::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionFalse_Evaluate_m2923999952 (XPathFunctionFalse_t1362233227 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionFalse::ToString()
extern "C"  String_t* XPathFunctionFalse_ToString_m3804071681 (XPathFunctionFalse_t1362233227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
