﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MetaProcessBase/<Tick>c__AnonStorey146
struct U3CTickU3Ec__AnonStorey146_t2344266993;
// IProcess
struct IProcess_t4026237414;

#include "codegen/il2cpp-codegen.h"

// System.Void MetaProcessBase/<Tick>c__AnonStorey146::.ctor()
extern "C"  void U3CTickU3Ec__AnonStorey146__ctor_m1395127094 (U3CTickU3Ec__AnonStorey146_t2344266993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MetaProcessBase/<Tick>c__AnonStorey146::<>m__1E1(IProcess)
extern "C"  void U3CTickU3Ec__AnonStorey146_U3CU3Em__1E1_m3186399834 (U3CTickU3Ec__AnonStorey146_t2344266993 * __this, Il2CppObject * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
