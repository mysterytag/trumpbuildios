﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CU3E__AnonType0_2_2589238341MethodDeclarations.h"

// System.Void <>__AnonType0`2<System.String,System.String>::.ctor(<google>__T,<bing>__T)
#define U3CU3E__AnonType0_2__ctor_m100399262(__this, ___google0, ___bing1, method) ((  void (*) (U3CU3E__AnonType0_2_t1370999997 *, String_t*, String_t*, const MethodInfo*))U3CU3E__AnonType0_2__ctor_m1360322370_gshared)(__this, ___google0, ___bing1, method)
// <google>__T <>__AnonType0`2<System.String,System.String>::get_google()
#define U3CU3E__AnonType0_2_get_google_m1942857707(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_2_t1370999997 *, const MethodInfo*))U3CU3E__AnonType0_2_get_google_m2704299663_gshared)(__this, method)
// <bing>__T <>__AnonType0`2<System.String,System.String>::get_bing()
#define U3CU3E__AnonType0_2_get_bing_m4172723851(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_2_t1370999997 *, const MethodInfo*))U3CU3E__AnonType0_2_get_bing_m3212623407_gshared)(__this, method)
// System.Boolean <>__AnonType0`2<System.String,System.String>::Equals(System.Object)
#define U3CU3E__AnonType0_2_Equals_m291808696(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType0_2_t1370999997 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_2_Equals_m1009921940_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType0`2<System.String,System.String>::GetHashCode()
#define U3CU3E__AnonType0_2_GetHashCode_m3068884700(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_2_t1370999997 *, const MethodInfo*))U3CU3E__AnonType0_2_GetHashCode_m903781560_gshared)(__this, method)
// System.String <>__AnonType0`2<System.String,System.String>::ToString()
#define U3CU3E__AnonType0_2_ToString_m1741987408(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_2_t1370999997 *, const MethodInfo*))U3CU3E__AnonType0_2_ToString_m781886964_gshared)(__this, method)
