﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidVkConnector
struct AndroidVkConnector_t3950437193;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void AndroidVkConnector::.ctor()
extern "C"  void AndroidVkConnector__ctor_m967819122 (AndroidVkConnector_t3950437193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidVkConnector::CallUserProfile()
extern "C"  void AndroidVkConnector_CallUserProfile_m2793555952 (AndroidVkConnector_t3950437193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidVkConnector::CallPostAchievement(System.String,System.String)
extern "C"  void AndroidVkConnector_CallPostAchievement_m3811841245 (AndroidVkConnector_t3950437193 * __this, String_t* ___format0, String_t* ___s1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidVkConnector::IsLoggedIn()
extern "C"  bool AndroidVkConnector_IsLoggedIn_m2895286999 (AndroidVkConnector_t3950437193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidVkConnector::CallLogin()
extern "C"  void AndroidVkConnector_CallLogin_m1795020283 (AndroidVkConnector_t3950437193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidVkConnector::CallJoinGroup(System.String)
extern "C"  void AndroidVkConnector_CallJoinGroup_m1123294171 (AndroidVkConnector_t3950437193 * __this, String_t* ___groupId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidVkConnector::CallGetFriendList()
extern "C"  void AndroidVkConnector_CallGetFriendList_m3615315876 (AndroidVkConnector_t3950437193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidVkConnector::CallInviteFriend(System.String)
extern "C"  void AndroidVkConnector_CallInviteFriend_m3552984843 (AndroidVkConnector_t3950437193 * __this, String_t* ___friendId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidVkConnector::CallLogout()
extern "C"  void AndroidVkConnector_CallLogout_m4111843930 (AndroidVkConnector_t3950437193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
