﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsRequestCallback
struct GetPlayFabIDsFromFacebookIDsRequestCallback_t1008885767;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest
struct GetPlayFabIDsFromFacebookIDsRequest_t270576050;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID270576050.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromFacebookIDsRequestCallback__ctor_m1520499990 (GetPlayFabIDsFromFacebookIDsRequestCallback_t1008885767 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest,System.Object)
extern "C"  void GetPlayFabIDsFromFacebookIDsRequestCallback_Invoke_m2338558923 (GetPlayFabIDsFromFacebookIDsRequestCallback_t1008885767 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromFacebookIDsRequest_t270576050 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromFacebookIDsRequestCallback_t1008885767(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromFacebookIDsRequest_t270576050 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromFacebookIDsRequestCallback_BeginInvoke_m4066909374 (GetPlayFabIDsFromFacebookIDsRequestCallback_t1008885767 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromFacebookIDsRequest_t270576050 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromFacebookIDsRequestCallback_EndInvoke_m3724175910 (GetPlayFabIDsFromFacebookIDsRequestCallback_t1008885767 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
