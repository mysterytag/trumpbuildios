﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FinallyObservable`1/Finally<System.Object>
struct Finally_t1925751780;
// UniRx.Operators.FinallyObservable`1<System.Object>
struct FinallyObservable_1_t2652127741;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::.ctor(UniRx.Operators.FinallyObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Finally__ctor_m1268980357_gshared (Finally_t1925751780 * __this, FinallyObservable_1_t2652127741 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Finally__ctor_m1268980357(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Finally_t1925751780 *, FinallyObservable_1_t2652127741 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Finally__ctor_m1268980357_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.FinallyObservable`1/Finally<System.Object>::Run()
extern "C"  Il2CppObject * Finally_Run_m780958517_gshared (Finally_t1925751780 * __this, const MethodInfo* method);
#define Finally_Run_m780958517(__this, method) ((  Il2CppObject * (*) (Finally_t1925751780 *, const MethodInfo*))Finally_Run_m780958517_gshared)(__this, method)
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::OnNext(T)
extern "C"  void Finally_OnNext_m3205431823_gshared (Finally_t1925751780 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Finally_OnNext_m3205431823(__this, ___value0, method) ((  void (*) (Finally_t1925751780 *, Il2CppObject *, const MethodInfo*))Finally_OnNext_m3205431823_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::OnError(System.Exception)
extern "C"  void Finally_OnError_m3120766750_gshared (Finally_t1925751780 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Finally_OnError_m3120766750(__this, ___error0, method) ((  void (*) (Finally_t1925751780 *, Exception_t1967233988 *, const MethodInfo*))Finally_OnError_m3120766750_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::OnCompleted()
extern "C"  void Finally_OnCompleted_m295767921_gshared (Finally_t1925751780 * __this, const MethodInfo* method);
#define Finally_OnCompleted_m295767921(__this, method) ((  void (*) (Finally_t1925751780 *, const MethodInfo*))Finally_OnCompleted_m295767921_gshared)(__this, method)
// System.Void UniRx.Operators.FinallyObservable`1/Finally<System.Object>::<Run>m__7C()
extern "C"  void Finally_U3CRunU3Em__7C_m2836594759_gshared (Finally_t1925751780 * __this, const MethodInfo* method);
#define Finally_U3CRunU3Em__7C_m2836594759(__this, method) ((  void (*) (Finally_t1925751780 *, const MethodInfo*))Finally_U3CRunU3Em__7C_m2836594759_gshared)(__this, method)
