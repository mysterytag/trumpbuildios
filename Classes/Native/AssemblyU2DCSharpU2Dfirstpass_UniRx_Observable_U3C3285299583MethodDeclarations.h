﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey46`1<System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey46_1_t3285299583;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey46`1<System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey46_1__ctor_m2533392605_gshared (U3CFromAsyncPatternU3Ec__AnonStorey46_1_t3285299583 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey46_1__ctor_m2533392605(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey46_1_t3285299583 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey46_1__ctor_m2533392605_gshared)(__this, method)
// UniRx.Unit UniRx.Observable/<FromAsyncPattern>c__AnonStorey46`1<System.Object>::<>m__44(System.IAsyncResult)
extern "C"  Unit_t2558286038  U3CFromAsyncPatternU3Ec__AnonStorey46_1_U3CU3Em__44_m3491607508_gshared (U3CFromAsyncPatternU3Ec__AnonStorey46_1_t3285299583 * __this, Il2CppObject * ___iar0, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey46_1_U3CU3Em__44_m3491607508(__this, ___iar0, method) ((  Unit_t2558286038  (*) (U3CFromAsyncPatternU3Ec__AnonStorey46_1_t3285299583 *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey46_1_U3CU3Em__44_m3491607508_gshared)(__this, ___iar0, method)
