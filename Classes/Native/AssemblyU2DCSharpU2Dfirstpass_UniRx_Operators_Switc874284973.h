﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>
struct SwitchObserver_t321759539;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch<System.Object>
struct  Switch_t874284973  : public Il2CppObject
{
public:
	// UniRx.Operators.SwitchObservable`1/SwitchObserver<T> UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch::parent
	SwitchObserver_t321759539 * ___parent_0;
	// System.UInt64 UniRx.Operators.SwitchObservable`1/SwitchObserver/Switch::id
	uint64_t ___id_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Switch_t874284973, ___parent_0)); }
	inline SwitchObserver_t321759539 * get_parent_0() const { return ___parent_0; }
	inline SwitchObserver_t321759539 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(SwitchObserver_t321759539 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Switch_t874284973, ___id_1)); }
	inline uint64_t get_id_1() const { return ___id_1; }
	inline uint64_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(uint64_t value)
	{
		___id_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
