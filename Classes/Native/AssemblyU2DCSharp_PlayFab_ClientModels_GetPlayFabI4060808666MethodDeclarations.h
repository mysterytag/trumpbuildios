﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsResult
struct GetPlayFabIDsFromSteamIDsResult_t4060808666;
// System.Collections.Generic.List`1<PlayFab.ClientModels.SteamPlayFabIdPair>
struct List_1_t3481275745;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromSteamIDsResult__ctor_m2663627535 (GetPlayFabIDsFromSteamIDsResult_t4060808666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.SteamPlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsResult::get_Data()
extern "C"  List_1_t3481275745 * GetPlayFabIDsFromSteamIDsResult_get_Data_m2220093279 (GetPlayFabIDsFromSteamIDsResult_t4060808666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.SteamPlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromSteamIDsResult_set_Data_m1770862998 (GetPlayFabIDsFromSteamIDsResult_t4060808666 * __this, List_1_t3481275745 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
