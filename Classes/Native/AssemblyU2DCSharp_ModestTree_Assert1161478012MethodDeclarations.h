﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Func`1<System.String>
struct Func_1_t2111270149;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"

// System.Boolean ModestTree.Assert::get_IsEnabled()
extern "C"  bool Assert_get_IsEnabled_m759776857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::That(System.Boolean)
extern "C"  void Assert_That_m1320742921 (Il2CppObject * __this /* static, unused */, bool ___condition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsEqual(System.Object,System.Object)
extern "C"  void Assert_IsEqual_m3021801345 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsEqual(System.Object,System.Object,System.Func`1<System.String>)
extern "C"  void Assert_IsEqual_m3745255887 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, Func_1_t2111270149 * ___messageGenerator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsEqual(System.Object,System.Object,System.String)
extern "C"  void Assert_IsEqual_m357464573 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsNotEqual(System.Object,System.Object)
extern "C"  void Assert_IsNotEqual_m1677822028 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsNotEqual(System.Object,System.Object,System.Func`1<System.String>)
extern "C"  void Assert_IsNotEqual_m1217636580 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, Func_1_t2111270149 * ___messageGenerator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsNull(System.Object)
extern "C"  void Assert_IsNull_m3103527384 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsNotNull(System.Object)
extern "C"  void Assert_IsNotNull_m3452793965 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsNotNull(System.Object,System.String)
extern "C"  void Assert_IsNotNull_m3793213929 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsNull(System.Object,System.String,System.Object[])
extern "C"  void Assert_IsNull_m3959033472 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, String_t* ___message1, ObjectU5BU5D_t11523773* ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsNotNull(System.Object,System.String,System.Object[])
extern "C"  void Assert_IsNotNull_m3308289621 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, String_t* ___message1, ObjectU5BU5D_t11523773* ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::IsNotEqual(System.Object,System.Object,System.String)
extern "C"  void Assert_IsNotEqual_m475020168 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___left0, Il2CppObject * ___right1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::That(System.Boolean,System.Func`1<System.String>)
extern "C"  void Assert_That_m1170495047 (Il2CppObject * __this /* static, unused */, bool ___condition0, Func_1_t2111270149 * ___messageGenerator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::That(System.Boolean,System.String,System.Object[])
extern "C"  void Assert_That_m2934751473 (Il2CppObject * __this /* static, unused */, bool ___condition0, String_t* ___message1, ObjectU5BU5D_t11523773* ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::Throw(System.String)
extern "C"  void Assert_Throw_m197124005 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Assert::Throw(System.String,System.Object[])
extern "C"  void Assert_Throw_m753765393 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ModestTree.Assert::FormatString(System.String,System.Object[])
extern "C"  String_t* Assert_FormatString_m3297644574 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t11523773* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
