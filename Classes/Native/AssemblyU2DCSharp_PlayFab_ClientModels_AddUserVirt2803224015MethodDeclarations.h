﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AddUserVirtualCurrencyRequest
struct AddUserVirtualCurrencyRequest_t2803224015;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.AddUserVirtualCurrencyRequest::.ctor()
extern "C"  void AddUserVirtualCurrencyRequest__ctor_m1700619322 (AddUserVirtualCurrencyRequest_t2803224015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AddUserVirtualCurrencyRequest::get_VirtualCurrency()
extern "C"  String_t* AddUserVirtualCurrencyRequest_get_VirtualCurrency_m3931837160 (AddUserVirtualCurrencyRequest_t2803224015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddUserVirtualCurrencyRequest::set_VirtualCurrency(System.String)
extern "C"  void AddUserVirtualCurrencyRequest_set_VirtualCurrency_m4084867243 (AddUserVirtualCurrencyRequest_t2803224015 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.AddUserVirtualCurrencyRequest::get_Amount()
extern "C"  int32_t AddUserVirtualCurrencyRequest_get_Amount_m1692548665 (AddUserVirtualCurrencyRequest_t2803224015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AddUserVirtualCurrencyRequest::set_Amount(System.Int32)
extern "C"  void AddUserVirtualCurrencyRequest_set_Amount_m2342980464 (AddUserVirtualCurrencyRequest_t2803224015 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
