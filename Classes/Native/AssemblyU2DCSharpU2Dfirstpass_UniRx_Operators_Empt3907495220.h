﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2669133373.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct  Empty_t3907495220  : public OperatorObserverBase_2_t2669133373
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
