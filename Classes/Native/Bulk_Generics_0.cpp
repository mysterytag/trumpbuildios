﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// <>__AnonType0`2<System.Object,System.Object>
struct U3CU3E__AnonType0_2_t2589238341;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>
struct U3CU3E__AnonType0_4_t733143067;
// <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>
struct U3CU3E__AnonType0_4_t147831043;
// <>__AnonType1`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType1_2_t2559104484;
// <>__AnonType1`2<System.Object,System.Object>
struct U3CU3E__AnonType1_2_t3686769320;
// <>__AnonType2`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType2_2_t3656635463;
// <>__AnonType2`2<System.Object,System.Object>
struct U3CU3E__AnonType2_2_t489333003;
// <>__AnonType3`2<System.Boolean,System.Int32>
struct U3CU3E__AnonType3_2_t3095608592;
// <>__AnonType3`2<System.Object,System.Object>
struct U3CU3E__AnonType3_2_t1586863982;
// <>__AnonType4`2<System.Object,System.Object>
struct U3CU3E__AnonType4_2_t2684394961;
// <>__AnonType4`2<System.Single,System.Object>
struct U3CU3E__AnonType4_2_t3892302468;
// <>__AnonType5`2<System.DateTime,System.Int64>
struct U3CU3E__AnonType5_2_t3897323094;
// <>__AnonType5`2<System.Object,System.Object>
struct U3CU3E__AnonType5_2_t3781925940;
// <>__AnonType6`2<System.Double,System.Int32>
struct U3CU3E__AnonType6_2_t3631555212;
// <>__AnonType6`2<System.Object,System.Object>
struct U3CU3E__AnonType6_2_t584489623;
// <>__AnonType7`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType7_2_t554355766;
// <>__AnonType7`2<System.Object,System.Object>
struct U3CU3E__AnonType7_2_t1682020602;
// AbandonedStream`1<System.Object>
struct AbandonedStream_1_t2531752022;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action
struct Action_t437523947;
// AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Boolean>
struct U3CListenU3Ec__AnonStoreyF9_t1928065897;
// AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Double>
struct U3CListenU3Ec__AnonStoreyF9_t2251577170;
// AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Int32>
struct U3CListenU3Ec__AnonStoreyF9_t269508047;
// AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Object>
struct U3CListenU3Ec__AnonStoreyF9_t2554166976;
// AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Boolean>
struct U3COnChangedU3Ec__AnonStoreyFA_t1149388695;
// AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Double>
struct U3COnChangedU3Ec__AnonStoreyFA_t1472899968;
// AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Int32>
struct U3COnChangedU3Ec__AnonStoreyFA_t3785798141;
// AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Object>
struct U3COnChangedU3Ec__AnonStoreyFA_t1775489774;
// AnonymousCell`1<System.Boolean>
struct AnonymousCell_1_t3772459138;
// System.Func`3<System.Action`1<System.Boolean>,Priority,System.IDisposable>
struct Func_3_t3884450017;
// System.Func`1<System.Boolean>
struct Func_1_t1353786588;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// AnonymousCell`1<System.Double>
struct AnonymousCell_1_t4095970411;
// System.Func`3<System.Action`1<System.Double>,Priority,System.IDisposable>
struct Func_3_t892795874;
// System.Func`1<System.Double>
struct Func_1_t1677297861;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// AnonymousCell`1<System.Int32>
struct AnonymousCell_1_t2113901288;
// System.Func`3<System.Action`1<System.Int32>,Priority,System.IDisposable>
struct Func_3_t722536535;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// AnonymousCell`1<System.Object>
struct AnonymousCell_1_t103592921;
// System.Func`3<System.Action`1<System.Object>,Priority,System.IDisposable>
struct Func_3_t1585443616;
// System.Func`1<System.Object>
struct Func_1_t1979887667;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// AnonymousStream`1/<Listen>c__AnonStorey12C<System.Object>
struct U3CListenU3Ec__AnonStorey12C_t1220913119;
// AnonymousStream`1<System.Object>
struct AnonymousStream_1_t94244231;
// Astray`1<System.Object>
struct Astray_1_t3225947498;
// BioCell`1/<>c__AnonStoreyF4<System.Object>
struct U3CU3Ec__AnonStoreyF4_t4282333876;
// BioCell`1/<Spread>c__AnonStoreyF5<System.Object>
struct U3CSpreadU3Ec__AnonStoreyF5_t4127532072;
// BioCell`1<System.Object>
struct BioCell_1_t661504124;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// IOrganism
struct IOrganism_t2580843579;
// IRoot
struct IRoot_t69970123;
// IOnceEmptyStream
struct IOnceEmptyStream_t172682851;
// BioCollection`1/<Implant>c__AnonStoreyF8<System.Object>
struct U3CImplantU3Ec__AnonStoreyF8_t1491113711;
// BioCollection`1/<InsertItem>c__AnonStoreyF7<System.Object>
struct U3CInsertItemU3Ec__AnonStoreyF7_t871750507;
// BioCollection`1/<Spread>c__AnonStoreyF6<System.Object>
struct U3CSpreadU3Ec__AnonStoreyF6_t1499138945;
// BioCollection`1<System.Object>
struct BioCollection_1_t694114648;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// IProcess
struct IProcess_t4026237414;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// IStream`1<System.Int32>
struct IStream_1_t3400105778;
// IStream`1<UniRx.Unit>
struct IStream_1_t3110977029;
// IStream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IStream_1_t2969212978;
// IStream`1<CollectionMoveEvent`1<System.Object>>
struct IStream_1_t4124746372;
// IStream`1<CollectionRemoveEvent`1<System.Object>>
struct IStream_1_t1450950249;
// IStream`1<CollectionReplaceEvent`1<System.Object>>
struct IStream_1_t3050063061;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// ICell`1<System.Collections.Generic.ICollection`1<System.Object>>
struct ICell_1_t2854568783;
// System.Func`2<System.Collections.Generic.ICollection`1<System.Object>,System.Object>
struct Func_2_t3667291446;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Action`1<System.Collections.Generic.ICollection`1<System.Object>>
struct Action_1_t1451390511;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// BioProcessor`1/DisposableInstruction<System.Object>
struct DisposableInstruction_t393830967;
// BioProcessor`1/Instruction<System.Object>
struct Instruction_t1241108407;
// BioProcessor`1<System.Object>
struct BioProcessor_1_t2501661084;
// ILiving
struct ILiving_t2639664210;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// Blind`1<System.Object>
struct Blind_1_t3800599375;
// Cell`1/<OnChanged>c__AnonStoreyF3<System.Boolean>
struct U3COnChangedU3Ec__AnonStoreyF3_t3587154105;
// Cell`1/<OnChanged>c__AnonStoreyF3<System.DateTime>
struct U3COnChangedU3Ec__AnonStoreyF3_t3715182700;
// Cell`1/<OnChanged>c__AnonStoreyF3<System.Double>
struct U3COnChangedU3Ec__AnonStoreyF3_t3910665378;
// Cell`1/<OnChanged>c__AnonStoreyF3<System.Int32>
struct U3COnChangedU3Ec__AnonStoreyF3_t1928596255;
// Cell`1/<OnChanged>c__AnonStoreyF3<System.Int64>
struct U3COnChangedU3Ec__AnonStoreyF3_t1928596350;
// Cell`1/<OnChanged>c__AnonStoreyF3<System.Object>
struct U3COnChangedU3Ec__AnonStoreyF3_t4213255184;
// Cell`1/<OnChanged>c__AnonStoreyF3<System.Single>
struct U3COnChangedU3Ec__AnonStoreyF3_t39390489;
// Cell`1<System.Boolean>
struct Cell_1_t393102973;
// ICell`1<System.Boolean>
struct ICell_1_t1762636318;
// IStream`1<System.Boolean>
struct IStream_1_t763696332;
// UnityEngine.UI.Text
struct Text_t3286458198;
// Cell`1<System.DateTime>
struct Cell_1_t521131568;
// ICell`1<System.DateTime>
struct ICell_1_t1890664913;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// IStream`1<System.DateTime>
struct IStream_1_t891724927;
// Cell`1<System.Double>
struct Cell_1_t716614246;
// ICell`1<System.Double>
struct ICell_1_t2086147591;
// IStream`1<System.Double>
struct IStream_1_t1087207605;
// Cell`1<System.Int32>
struct Cell_1_t3029512419;
// ICell`1<System.Int32>
struct ICell_1_t104078468;
// Cell`1<System.Int64>
struct Cell_1_t3029512514;
// ICell`1<System.Int64>
struct ICell_1_t104078563;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// IStream`1<System.Int64>
struct IStream_1_t3400105873;
// Cell`1<System.Object>
struct Cell_1_t1019204052;
// Cell`1<System.Single>
struct Cell_1_t1140306653;
// ICell`1<System.Single>
struct ICell_1_t2509839998;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// IStream`1<System.Single>
struct IStream_1_t1510900012;
// CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2<System.Object,System.Object>
struct U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393;
// CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>
struct U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414;
// CellReactiveApi/<AsObservable>c__AnonStorey106`1<System.Object>
struct U3CAsObservableU3Ec__AnonStorey106_1_t706635991;
// CellReactiveApi/<AsStream>c__AnonStorey105`1<System.Object>
struct U3CAsStreamU3Ec__AnonStorey105_1_t3800865897;
// CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1<System.Object>
struct U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177;
// CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1<System.Object>
struct U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641;
// CellReactiveApi/<EachNotNull>c__AnonStorey103`1<System.Object>
struct U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640;
// CellReactiveApi/<FaceControl>c__AnonStorey101`1<System.Object>
struct U3CFaceControlU3Ec__AnonStorey101_1_t4266084863;
// CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1<System.Object>
struct U3CHoldU3Ec__AnonStorey100_1_t2662411751;
// CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>
struct U3CHoldU3Ec__AnonStoreyFF_1_t2863379238;
// CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Int32>
struct U3CJoinU3Ec__AnonStorey115_1_t3718487911;
// CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>
struct U3CJoinU3Ec__AnonStorey115_1_t1708179544;
// CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>
struct U3CJoinU3Ec__AnonStorey114_1_t4163512910;
// CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>
struct U3CJoinU3Ec__AnonStorey114_1_t2153204543;
// CellReactiveApi/<Join>c__AnonStorey117`1/<Join>c__AnonStorey116`1<System.Object>
struct U3CJoinU3Ec__AnonStorey116_1_t1263154545;
// CellReactiveApi/<Join>c__AnonStorey117`1<System.Object>
struct U3CJoinU3Ec__AnonStorey117_1_t818129546;
// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Boolean,System.Boolean>
struct U3CMapU3Ec__AnonStorey10F_2_t985129795;
// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Int32>
struct U3CMapU3Ec__AnonStorey10F_2_t1626595051;
// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Object>
struct U3CMapU3Ec__AnonStorey10F_2_t3911253980;
// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Object,System.Object>
struct U3CMapU3Ec__AnonStorey10F_2_t2112794631;
// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>
struct U3CMapU3Ec__AnonStorey10E_2_t4182566112;
// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Int32>
struct U3CMapU3Ec__AnonStorey10E_2_t529064072;
// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Object>
struct U3CMapU3Ec__AnonStorey10E_2_t2813723001;
// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Object,System.Object>
struct U3CMapU3Ec__AnonStorey10E_2_t1015263652;
// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>
struct U3CMergeU3Ec__AnonStorey113_3_t2913556904;
// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Double>
struct U3CMergeU3Ec__AnonStorey113_3_t3372762043;
// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Object>
struct U3CMergeU3Ec__AnonStorey113_3_t3675351849;
// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Object,System.Object,System.Object>
struct U3CMergeU3Ec__AnonStorey113_3_t2569540242;
// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Single,System.Object,System.Object>
struct U3CMergeU3Ec__AnonStorey113_3_t3131141555;
// CellReactiveApi/<Merge>c__AnonStorey112`3<System.DateTime,System.Int64,System.Object>
struct U3CMergeU3Ec__AnonStorey112_3_t3245937911;
// CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Double>
struct U3CMergeU3Ec__AnonStorey112_3_t3705143050;
// CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Object>
struct U3CMergeU3Ec__AnonStorey112_3_t4007732856;
// CellReactiveApi/<Merge>c__AnonStorey112`3<System.Object,System.Object,System.Object>
struct U3CMergeU3Ec__AnonStorey112_3_t2901921249;
// CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>
struct U3CMergeU3Ec__AnonStorey112_3_t3463522562;
// CellReactiveApi/<NotNull>c__AnonStorey102`1<System.Object>
struct U3CNotNullU3Ec__AnonStorey102_1_t291767342;
// CellReactiveApi/<SelectMany>c__AnonStorey118`2<System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey118_2_t927720085;
// CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Int32,System.Int32,System.Int32>
struct U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920;
// CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581;
// CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Int32,System.Int32,System.Int32>
struct U3CSelectManyU3Ec__AnonStorey119_3_t4205223976;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t1649583772;
// CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey119_3_t1259363341;
// CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1<System.Object>
struct U3CSequenceU3Ec__AnonStorey111_1_t1272906773;
// CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>
struct U3CSequenceU3Ec__AnonStorey110_1_t1717931772;
// System.Collections.Generic.IEnumerable`1<ICell`1<System.Object>>
struct IEnumerable_1_t965924457;
// System.Func`2<ICell`1<System.Object>,System.Object>
struct Func_2_t2767404563;
// System.Action`1<System.Collections.Generic.IEnumerable`1<System.Object>>
struct Action_1_t3857713481;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CU3E__AnonType0_2_2589238341.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CU3E__AnonType0_2_2589238341MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_4_gen733143067.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_4_gen733143067MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3940363287MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3940363287.h"
#include "mscorlib_System_Boolean211005341MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_4_gen147831043.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_4_gen147831043MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType1_2_gen2559104484.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType1_2_gen2559104484MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType1_2_gen3686769320.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType1_2_gen3686769320MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType2_2_gen3656635463.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType2_2_gen3656635463MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType2_2_gen489333003.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType2_2_gen489333003MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType3_2_gen3095608592.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType3_2_gen3095608592MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2281805437MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2281805437.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType3_2_gen1586863982.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType3_2_gen1586863982MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType4_2_gen2684394961.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType4_2_gen2684394961MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType4_2_gen3892302468.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType4_2_gen3892302468MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare392599671MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare392599671.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType5_2_gen3897323094.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType5_2_gen3897323094MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4068391882MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4068391882.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2281805532MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2281805532.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType5_2_gen3781925940.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType5_2_gen3781925940MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType6_2_gen3631555212.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType6_2_gen3631555212MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4263874560MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar4263874560.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType6_2_gen584489623.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType6_2_gen584489623MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType7_2_gen554355766.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType7_2_gen554355766MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType7_2_gen1682020602.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType7_2_gen1682020602MethodDeclarations.h"
#include "AssemblyU2DCSharp_AbandonedStream_1_gen2531752022.h"
#include "AssemblyU2DCSharp_AbandonedStream_1_gen2531752022MethodDeclarations.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "AssemblyU2DCSharp_CellUtils_EmptyDisposable1512564738MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_EmptyDisposable1512564738.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3CListenU3Ec__A1928065897.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3CListenU3Ec__A1928065897MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3CListenU3Ec__A2251577170.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3CListenU3Ec__A2251577170MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3CListenU3Ec__An269508047.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3CListenU3Ec__An269508047MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3CListenU3Ec__A2554166976.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3CListenU3Ec__A2554166976MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3COnChangedU3Ec1149388695.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3COnChangedU3Ec1149388695MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3COnChangedU3Ec1472899968.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3COnChangedU3Ec1472899968MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3COnChangedU3Ec3785798141.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3COnChangedU3Ec3785798141MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3COnChangedU3Ec1775489774.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_U3COnChangedU3Ec1775489774MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen3772459138.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen3772459138MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3884450017.h"
#include "System_Core_System_Func_1_gen1353786588.h"
#include "System_Core_System_Func_1_gen1353786588MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "System_Core_System_Func_3_gen3884450017MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen4095970411.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen4095970411MethodDeclarations.h"
#include "System_Core_System_Func_3_gen892795874.h"
#include "System_Core_System_Func_1_gen1677297861.h"
#include "System_Core_System_Func_1_gen1677297861MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen682969319.h"
#include "System_Core_System_Func_3_gen892795874MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen682969319MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen2113901288.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen2113901288MethodDeclarations.h"
#include "System_Core_System_Func_3_gen722536535.h"
#include "System_Core_System_Func_1_gen3990196034.h"
#include "System_Core_System_Func_1_gen3990196034MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "System_Core_System_Func_3_gen722536535MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen103592921.h"
#include "AssemblyU2DCSharp_AnonymousCell_1_gen103592921MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1585443616.h"
#include "System_Core_System_Func_1_gen1979887667.h"
#include "System_Core_System_Func_1_gen1979887667MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1585443616MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousStream_1_U3CListenU3Ec_1220913119.h"
#include "AssemblyU2DCSharp_AnonymousStream_1_U3CListenU3Ec_1220913119MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousStream_1_gen94244231.h"
#include "AssemblyU2DCSharp_AnonymousStream_1_gen94244231MethodDeclarations.h"
#include "AssemblyU2DCSharp_Astray_1_gen3225947498.h"
#include "AssemblyU2DCSharp_Astray_1_gen3225947498MethodDeclarations.h"
#include "AssemblyU2DCSharp_Organism_2_gen3070571648MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioCell_1_U3CU3Ec__AnonStoreyF4_4282333876.h"
#include "AssemblyU2DCSharp_BioCell_1_U3CU3Ec__AnonStoreyF4_4282333876MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioCell_1_gen661504124.h"
#include "AssemblyU2DCSharp_BioCell_1_gen661504124MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioCell_1_U3CSpreadU3Ec__AnonSto4127532072.h"
#include "AssemblyU2DCSharp_BioCell_1_U3CSpreadU3Ec__AnonSto4127532072MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_gen1019204052MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "AssemblyU2DCSharp_Cell_1_gen1019204052.h"
#include "AssemblyU2DCSharp_OnceEmptyStream3081939404MethodDeclarations.h"
#include "AssemblyU2DCSharp_OnceEmptyStream3081939404.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharp_EmptyStream2850978573MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioCollection_1_U3CImplantU3Ec__1491113711.h"
#include "AssemblyU2DCSharp_BioCollection_1_U3CImplantU3Ec__1491113711MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioCollection_1_gen694114648.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150.h"
#include "AssemblyU2DCSharp_BioCollection_1_U3CInsertItemU3Ec871750507.h"
#include "AssemblyU2DCSharp_BioCollection_1_U3CInsertItemU3Ec871750507MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioCollection_1_U3CSpreadU3Ec__A1499138945.h"
#include "AssemblyU2DCSharp_BioCollection_1_U3CSpreadU3Ec__A1499138945MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioCollection_1_gen694114648MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen4289044124.h"
#include "AssemblyU2DCSharp_Stream_1_gen4289044124MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Action_1_gen1777374079MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425880343MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425880343.h"
#include "mscorlib_System_Action_1_gen1777374079.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat511663335MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1249425060MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553809MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat511663335.h"
#include "AssemblyU2DCSharp_Stream_1_gen1249425060.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553809.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen2263194403.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen2263194403MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3884365576.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3884365576MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1188511092.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1188511092MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3667291446.h"
#include "System_Core_System_Func_2_gen3667291446MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi2643500415MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi2643500415.h"
#include "mscorlib_System_Action_1_gen1451390511.h"
#include "mscorlib_System_Action_1_gen1451390511MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioProcessor_1_DisposableInstruct393830967.h"
#include "AssemblyU2DCSharp_BioProcessor_1_DisposableInstruct393830967MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioProcessor_1_gen2501661084.h"
#include "AssemblyU2DCSharp_BioProcessor_1_Instruction_gen1241108407.h"
#include "AssemblyU2DCSharp_BioProcessor_1_gen2501661084MethodDeclarations.h"
#include "AssemblyU2DCSharp_BioProcessor_1_Instruction_gen1241108407MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2038067376.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2038067376MethodDeclarations.h"
#include "AssemblyU2DCSharp_TagCollector573861427MethodDeclarations.h"
#include "AssemblyU2DCSharp_TagCollector573861427.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat123850368.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat123850368MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"
#include "AssemblyU2DCSharp_Blind_1_gen3800599375.h"
#include "AssemblyU2DCSharp_Blind_1_gen3800599375MethodDeclarations.h"
#include "AssemblyU2DCSharp_Organism_2_gen181152922MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto3587154105.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto3587154105MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto3715182700.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto3715182700MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto3910665378.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto3910665378MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto1928596255.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto1928596255MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto1928596350.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto1928596350MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto4213255184.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonSto4213255184MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonStore39390489.h"
#include "AssemblyU2DCSharp_Cell_1_U3COnChangedU3Ec__AnonStore39390489MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_gen393102973.h"
#include "AssemblyU2DCSharp_Cell_1_gen393102973MethodDeclarations.h"
#include "AssemblyU2DCSharp_Transaction3809114814MethodDeclarations.h"
#include "AssemblyU2DCSharp_Transaction3809114814.h"
#include "AssemblyU2DCSharp_Stream_1_gen3197111659.h"
#include "AssemblyU2DCSharp_Stream_1_gen3197111659MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "AssemblyU2DCSharp_Cell_1_gen521131568.h"
#include "AssemblyU2DCSharp_Cell_1_gen521131568MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen487486641.h"
#include "mscorlib_System_Action_1_gen487486641MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3325140254.h"
#include "AssemblyU2DCSharp_Stream_1_gen3325140254MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_gen716614246.h"
#include "AssemblyU2DCSharp_Cell_1_gen716614246MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3520622932.h"
#include "AssemblyU2DCSharp_Stream_1_gen3520622932MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_gen3029512419.h"
#include "AssemblyU2DCSharp_Cell_1_gen3029512419MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_gen3029512514.h"
#include "AssemblyU2DCSharp_Cell_1_gen3029512514MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587.h"
#include "mscorlib_System_Action_1_gen2995867587MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553904.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553904MethodDeclarations.h"
#include "AssemblyU2DCSharp_Cell_1_gen1140306653.h"
#include "AssemblyU2DCSharp_Cell_1_gen1140306653MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1106661726.h"
#include "mscorlib_System_Action_1_gen1106661726MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3944315339.h"
#include "AssemblyU2DCSharp_Stream_1_gen3944315339MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CAccumulateU3E3105718393.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CAccumulateU3E3105718393MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CAccumulateU3E2008187414.h"
#include "System_Core_System_Func_3_gen1892209229.h"
#include "AssemblyU2DCSharp_CellReactiveApi_MapDisposable304581564.h"
#include "System_Core_System_Func_3_gen1892209229MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CAccumulateU3E2008187414MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_SingleAssignmentDispos1832432170MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CAsObservableU3706635991.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CAsObservableU3706635991MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CAsStreamU3Ec_3800865897.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CAsStreamU3Ec_3800865897MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CBindHistoricU2090475177.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CBindHistoricU2090475177MethodDeclarations.h"
#include "System_Core_System_Action_2_gen4105459918.h"
#include "AssemblyU2DCSharp_CellReactiveApi_Carrier_1_gen2723215674.h"
#include "System_Core_System_Action_2_gen4105459918MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CEachNotNullU31588511641.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CEachNotNullU31588511641MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CEachNotNullU32033536640.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CEachNotNullU32033536640MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CFaceControlU34266084863.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CFaceControlU34266084863MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CHoldU3Ec__Ano2662411751.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CHoldU3Ec__Ano2662411751MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CHoldU3Ec__Ano2863379238.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CHoldU3Ec__Ano2863379238MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano3718487911.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano3718487911MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_SingleAssignmentDispos1832432170.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g1451834474.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano1708179544.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano1708179544MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3736493403.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano4163512910.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano4163512910MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen252531173.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g1451834474MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_ListDisposable2393830995MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_ListDisposable2393830995.h"
#include "mscorlib_System_Action_1_gen252531173MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano2153204543.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano2153204543MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2537190102.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3736493403MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2537190102MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano1263154545.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Ano1263154545MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Anon818129546.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CJoinU3Ec__Anon818129546MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1538250116.h"
#include "mscorlib_System_Action_1_gen1538250116MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__AnonS985129795.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__AnonS985129795MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon4182566112.h"
#include "System_Core_System_Func_2_gen1008118516.h"
#include "System_Core_System_Func_2_gen1008118516MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon1626595051.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon1626595051MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__AnonS529064072.h"
#include "System_Core_System_Func_2_gen1649583772.h"
#include "System_Core_System_Func_2_gen1649583772MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon3911253980.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon3911253980MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon2813723001.h"
#include "System_Core_System_Func_2_gen3934242701.h"
#include "System_Core_System_Func_2_gen3934242701MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon2112794631.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon2112794631MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon1015263652.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon4182566112MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_MapDisposable304581564MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__AnonS529064072MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon2813723001MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMapU3Ec__Anon1015263652MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An2913556904.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An2913556904MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3245937911.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3372762043.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3372762043MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3705143050.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3433903597.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3675351849.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3675351849MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An4007732856.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An2569540242.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An2569540242MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An2901921249.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3131141555.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3131141555MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3463522562.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3245937911MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2236225891.h"
#include "System_Core_System_Func_3_gen2236225891MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3705143050MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2695431030.h"
#include "System_Core_System_Func_3_gen2695431030MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3433903597MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An4007732856MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2998020836.h"
#include "System_Core_System_Func_3_gen2998020836MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An2901921249MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CMergeU3Ec__An3463522562MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2453810542.h"
#include "System_Core_System_Func_3_gen2453810542MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CNotNullU3Ec__A291767342.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CNotNullU3Ec__A291767342MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3Ec927720085.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3Ec927720085MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3E1546175920.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3E1546175920MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3E4205223976.h"
#include "System_Core_System_Func_3_gen543102568.h"
#include "System_Core_System_Func_3_gen543102568MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3E2895282581.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3E2895282581MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3E1259363341.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3E4205223976MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3201214749.h"
#include "System_Core_System_Func_2_gen3201214749MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSelectManyU3E1259363341MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3687414329.h"
#include "System_Core_System_Func_2_gen3687414329MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSequenceU3Ec_1272906773.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSequenceU3Ec_1272906773MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3857713481.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSequenceU3Ec_1717931772.h"
#include "System_Core_System_Func_1_gen557074727.h"
#include "System_Core_System_Func_1_gen557074727MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3857713481MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CSequenceU3Ec_1717931772MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2767404563.h"
#include "System_Core_System_Func_2_gen2767404563MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"

// ICell`1<!!1> CellReactiveApi::Select<System.Object,System.Object>(ICell`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* CellReactiveApi_Select_TisIl2CppObject_TisIl2CppObject_m2914298492_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define CellReactiveApi_Select_TisIl2CppObject_TisIl2CppObject_m2914298492(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))CellReactiveApi_Select_TisIl2CppObject_TisIl2CppObject_m2914298492_gshared)(__this /* static, unused */, p0, p1, method)
// ICell`1<!!1> CellReactiveApi::Select<System.Collections.Generic.ICollection`1<System.Object>,System.Object>(ICell`1<!!0>,System.Func`2<!!0,!!1>)
#define CellReactiveApi_Select_TisICollection_1_t1302937806_TisIl2CppObject_m529762129(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3667291446 *, const MethodInfo*))CellReactiveApi_Select_TisIl2CppObject_TisIl2CppObject_m2914298492_gshared)(__this /* static, unused */, p0, p1, method)
// ICell`1<!!1> CellReactiveApi::Select<System.Int32,System.Int32>(ICell`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* CellReactiveApi_Select_TisInt32_t2847414787_TisInt32_t2847414787_m3271447024_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1649583772 * p1, const MethodInfo* method);
#define CellReactiveApi_Select_TisInt32_t2847414787_TisInt32_t2847414787_m3271447024(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1649583772 *, const MethodInfo*))CellReactiveApi_Select_TisInt32_t2847414787_TisInt32_t2847414787_m3271447024_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<ICell`1<System.Object>,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisICell_1_t2388737397_TisIl2CppObject_m180010276(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2767404563 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void <>__AnonType0`2<System.Object,System.Object>::.ctor(<google>__T,<bing>__T)
extern "C"  void U3CU3E__AnonType0_2__ctor_m1360322370_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, Il2CppObject * ___google0, Il2CppObject * ___bing1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___google0;
		__this->set_U3CgoogleU3E_0(L_0);
		Il2CppObject * L_1 = ___bing1;
		__this->set_U3CbingU3E_1(L_1);
		return;
	}
}
// <google>__T <>__AnonType0`2<System.Object,System.Object>::get_google()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_google_m2704299663_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CgoogleU3E_0();
		return L_0;
	}
}
// <bing>__T <>__AnonType0`2<System.Object,System.Object>::get_bing()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_bing_m3212623407_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CbingU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType0`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_2_Equals_m1009921940_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType0_2_t2589238341 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType0_2_t2589238341 *)((U3CU3E__AnonType0_2_t2589238341 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType0_2_t2589238341 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CgoogleU3E_0();
		U3CU3E__AnonType0_2_t2589238341 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CgoogleU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CbingU3E_1();
		U3CU3E__AnonType0_2_t2589238341 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CbingU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType0`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_2_GetHashCode_m903781560_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CgoogleU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CbingU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType0`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral3775950506;
extern Il2CppCodeGenString* _stringLiteral1903534959;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType0_2_ToString_m781886964_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType0_2_ToString_m781886964_gshared (U3CU3E__AnonType0_2_t2589238341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType0_2_ToString_m781886964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral3775950506);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3775950506);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CgoogleU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CgoogleU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1903534959);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1903534959);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CbingU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CbingU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::.ctor(<show>__T,<tutor>__T,<bottom>__T,<hidden>__T)
extern "C"  void U3CU3E__AnonType0_4__ctor_m352903163_gshared (U3CU3E__AnonType0_4_t733143067 * __this, bool ___show0, bool ___tutor1, bool ___bottom2, bool ___hidden3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___show0;
		__this->set_U3CshowU3E_0(L_0);
		bool L_1 = ___tutor1;
		__this->set_U3CtutorU3E_1(L_1);
		bool L_2 = ___bottom2;
		__this->set_U3CbottomU3E_2(L_2);
		bool L_3 = ___hidden3;
		__this->set_U3ChiddenU3E_3(L_3);
		return;
	}
}
// <show>__T <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::get_show()
extern "C"  bool U3CU3E__AnonType0_4_get_show_m1146603045_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CshowU3E_0();
		return L_0;
	}
}
// <tutor>__T <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::get_tutor()
extern "C"  bool U3CU3E__AnonType0_4_get_tutor_m1418476487_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CtutorU3E_1();
		return L_0;
	}
}
// <bottom>__T <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::get_bottom()
extern "C"  bool U3CU3E__AnonType0_4_get_bottom_m3328873317_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CbottomU3E_2();
		return L_0;
	}
}
// <hidden>__T <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::get_hidden()
extern "C"  bool U3CU3E__AnonType0_4_get_hidden_m1137801157_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3ChiddenU3E_3();
		return L_0;
	}
}
// System.Boolean <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_4_Equals_m2711421094_gshared (U3CU3E__AnonType0_4_t733143067 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType0_4_t733143067 * V_0 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType0_4_t733143067 *)((U3CU3E__AnonType0_4_t733143067 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType0_4_t733143067 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_2 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_3 = (bool)__this->get_U3CshowU3E_0();
		U3CU3E__AnonType0_4_t733143067 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (bool)L_4->get_U3CshowU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_2, (bool)L_3, (bool)L_5);
		if (!L_6)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t3940363287 * L_7 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_8 = (bool)__this->get_U3CtutorU3E_1();
		U3CU3E__AnonType0_4_t733143067 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = (bool)L_9->get_U3CtutorU3E_1();
		NullCheck((EqualityComparer_1_t3940363287 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_7, (bool)L_8, (bool)L_10);
		if (!L_11)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t3940363287 * L_12 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		bool L_13 = (bool)__this->get_U3CbottomU3E_2();
		U3CU3E__AnonType0_4_t733143067 * L_14 = V_0;
		NullCheck(L_14);
		bool L_15 = (bool)L_14->get_U3CbottomU3E_2();
		NullCheck((EqualityComparer_1_t3940363287 *)L_12);
		bool L_16 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_12, (bool)L_13, (bool)L_15);
		if (!L_16)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		EqualityComparer_1_t3940363287 * L_17 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		bool L_18 = (bool)__this->get_U3ChiddenU3E_3();
		U3CU3E__AnonType0_4_t733143067 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = (bool)L_19->get_U3ChiddenU3E_3();
		NullCheck((EqualityComparer_1_t3940363287 *)L_17);
		bool L_21 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_17, (bool)L_18, (bool)L_20);
		G_B6_0 = ((int32_t)(L_21));
		goto IL_0077;
	}

IL_0076:
	{
		G_B6_0 = 0;
	}

IL_0077:
	{
		G_B8_0 = G_B6_0;
		goto IL_007a;
	}

IL_0079:
	{
		G_B8_0 = 0;
	}

IL_007a:
	{
		return (bool)G_B8_0;
	}
}
// System.Int32 <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_4_GetHashCode_m1145406666_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_0 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_1 = (bool)__this->get_U3CshowU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_0, (bool)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t3940363287 * L_3 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_4 = (bool)__this->get_U3CtutorU3E_1();
		NullCheck((EqualityComparer_1_t3940363287 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_3, (bool)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t3940363287 * L_6 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		bool L_7 = (bool)__this->get_U3CbottomU3E_2();
		NullCheck((EqualityComparer_1_t3940363287 *)L_6);
		int32_t L_8 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_6, (bool)L_7);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		EqualityComparer_1_t3940363287 * L_9 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		bool L_10 = (bool)__this->get_U3ChiddenU3E_3();
		NullCheck((EqualityComparer_1_t3940363287 *)L_9);
		int32_t L_11 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_9, (bool)L_10);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)))^(int32_t)L_8))*(int32_t)((int32_t)16777619)))^(int32_t)L_11))*(int32_t)((int32_t)16777619)));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)((int32_t)((int32_t)L_13<<(int32_t)((int32_t)13)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14^(int32_t)((int32_t)((int32_t)L_15>>(int32_t)7))));
		int32_t L_16 = V_0;
		int32_t L_17 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)((int32_t)((int32_t)L_17<<(int32_t)3))));
		int32_t L_18 = V_0;
		int32_t L_19 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18^(int32_t)((int32_t)((int32_t)L_19>>(int32_t)((int32_t)17)))));
		int32_t L_20 = V_0;
		int32_t L_21 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)((int32_t)((int32_t)L_21<<(int32_t)5))));
		int32_t L_22 = V_0;
		return L_22;
	}
}
// System.String <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral2002584422;
extern Il2CppCodeGenString* _stringLiteral2427545121;
extern Il2CppCodeGenString* _stringLiteral2984578724;
extern Il2CppCodeGenString* _stringLiteral1375397573;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType0_4_ToString_m2444216842_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType0_4_ToString_m2444216842_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType0_4_ToString_m2444216842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	int32_t G_B8_0 = 0;
	StringU5BU5D_t2956870243* G_B8_1 = NULL;
	StringU5BU5D_t2956870243* G_B8_2 = NULL;
	int32_t G_B7_0 = 0;
	StringU5BU5D_t2956870243* G_B7_1 = NULL;
	StringU5BU5D_t2956870243* G_B7_2 = NULL;
	String_t* G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	StringU5BU5D_t2956870243* G_B9_2 = NULL;
	StringU5BU5D_t2956870243* G_B9_3 = NULL;
	int32_t G_B11_0 = 0;
	StringU5BU5D_t2956870243* G_B11_1 = NULL;
	StringU5BU5D_t2956870243* G_B11_2 = NULL;
	int32_t G_B10_0 = 0;
	StringU5BU5D_t2956870243* G_B10_1 = NULL;
	StringU5BU5D_t2956870243* G_B10_2 = NULL;
	String_t* G_B12_0 = NULL;
	int32_t G_B12_1 = 0;
	StringU5BU5D_t2956870243* G_B12_2 = NULL;
	StringU5BU5D_t2956870243* G_B12_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2002584422);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2002584422);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		bool L_3 = (bool)__this->get_U3CshowU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		bool L_4 = (bool)__this->get_U3CshowU3E_0();
		V_0 = (bool)L_4;
		String_t* L_5 = Boolean_ToString_m2512358154((bool*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0047;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0047:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral2427545121);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2427545121);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		bool L_9 = (bool)__this->get_U3CtutorU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		bool L_10 = (bool)__this->get_U3CtutorU3E_1();
		V_1 = (bool)L_10;
		String_t* L_11 = Boolean_ToString_m2512358154((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0080;
	}

IL_007b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0080:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral2984578724);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral2984578724);
		StringU5BU5D_t2956870243* L_14 = (StringU5BU5D_t2956870243*)L_13;
		bool L_15 = (bool)__this->get_U3CbottomU3E_2();
		G_B7_0 = 6;
		G_B7_1 = L_14;
		G_B7_2 = L_14;
	}
	{
		bool L_16 = (bool)__this->get_U3CbottomU3E_2();
		V_2 = (bool)L_16;
		String_t* L_17 = Boolean_ToString_m2512358154((bool*)(&V_2), /*hidden argument*/NULL);
		G_B9_0 = L_17;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_00b9;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B9_0 = L_18;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_00b9:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (String_t*)G_B9_0);
		StringU5BU5D_t2956870243* L_19 = (StringU5BU5D_t2956870243*)G_B9_3;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, _stringLiteral1375397573);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral1375397573);
		StringU5BU5D_t2956870243* L_20 = (StringU5BU5D_t2956870243*)L_19;
		bool L_21 = (bool)__this->get_U3ChiddenU3E_3();
		G_B10_0 = 8;
		G_B10_1 = L_20;
		G_B10_2 = L_20;
	}
	{
		bool L_22 = (bool)__this->get_U3ChiddenU3E_3();
		V_3 = (bool)L_22;
		String_t* L_23 = Boolean_ToString_m2512358154((bool*)(&V_3), /*hidden argument*/NULL);
		G_B12_0 = L_23;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_00f2;
	}

IL_00ed:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B12_0 = L_24;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_00f2:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		ArrayElementTypeCheck (G_B12_2, G_B12_0);
		(G_B12_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B12_1), (String_t*)G_B12_0);
		StringU5BU5D_t2956870243* L_25 = (StringU5BU5D_t2956870243*)G_B12_3;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)9));
		ArrayElementTypeCheck (L_25, _stringLiteral1117);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.Void <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::.ctor(<show>__T,<tutor>__T,<bottom>__T,<hidden>__T)
extern "C"  void U3CU3E__AnonType0_4__ctor_m3696565507_gshared (U3CU3E__AnonType0_4_t147831043 * __this, Il2CppObject * ___show0, Il2CppObject * ___tutor1, Il2CppObject * ___bottom2, Il2CppObject * ___hidden3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___show0;
		__this->set_U3CshowU3E_0(L_0);
		Il2CppObject * L_1 = ___tutor1;
		__this->set_U3CtutorU3E_1(L_1);
		Il2CppObject * L_2 = ___bottom2;
		__this->set_U3CbottomU3E_2(L_2);
		Il2CppObject * L_3 = ___hidden3;
		__this->set_U3ChiddenU3E_3(L_3);
		return;
	}
}
// <show>__T <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::get_show()
extern "C"  Il2CppObject * U3CU3E__AnonType0_4_get_show_m884195869_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		return L_0;
	}
}
// <tutor>__T <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::get_tutor()
extern "C"  Il2CppObject * U3CU3E__AnonType0_4_get_tutor_m2532450895_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CtutorU3E_1();
		return L_0;
	}
}
// <bottom>__T <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::get_bottom()
extern "C"  Il2CppObject * U3CU3E__AnonType0_4_get_bottom_m2040523357_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CbottomU3E_2();
		return L_0;
	}
}
// <hidden>__T <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::get_hidden()
extern "C"  Il2CppObject * U3CU3E__AnonType0_4_get_hidden_m4036773693_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3ChiddenU3E_3();
		return L_0;
	}
}
// System.Boolean <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_4_Equals_m4034822574_gshared (U3CU3E__AnonType0_4_t147831043 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType0_4_t147831043 * V_0 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType0_4_t147831043 *)((U3CU3E__AnonType0_4_t147831043 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType0_4_t147831043 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		U3CU3E__AnonType0_4_t147831043 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CshowU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CtutorU3E_1();
		U3CU3E__AnonType0_4_t147831043 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CtutorU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		if (!L_11)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t271497070 * L_12 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CbottomU3E_2();
		U3CU3E__AnonType0_4_t147831043 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject * L_15 = (Il2CppObject *)L_14->get_U3CbottomU3E_2();
		NullCheck((EqualityComparer_1_t271497070 *)L_12);
		bool L_16 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_12, (Il2CppObject *)L_13, (Il2CppObject *)L_15);
		if (!L_16)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		EqualityComparer_1_t271497070 * L_17 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Il2CppObject * L_18 = (Il2CppObject *)__this->get_U3ChiddenU3E_3();
		U3CU3E__AnonType0_4_t147831043 * L_19 = V_0;
		NullCheck(L_19);
		Il2CppObject * L_20 = (Il2CppObject *)L_19->get_U3ChiddenU3E_3();
		NullCheck((EqualityComparer_1_t271497070 *)L_17);
		bool L_21 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_17, (Il2CppObject *)L_18, (Il2CppObject *)L_20);
		G_B6_0 = ((int32_t)(L_21));
		goto IL_0077;
	}

IL_0076:
	{
		G_B6_0 = 0;
	}

IL_0077:
	{
		G_B8_0 = G_B6_0;
		goto IL_007a;
	}

IL_0079:
	{
		G_B8_0 = 0;
	}

IL_007a:
	{
		return (bool)G_B8_0;
	}
}
// System.Int32 <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_4_GetHashCode_m2266149074_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CtutorU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		EqualityComparer_1_t271497070 * L_6 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CbottomU3E_2();
		NullCheck((EqualityComparer_1_t271497070 *)L_6);
		int32_t L_8 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_6, (Il2CppObject *)L_7);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		EqualityComparer_1_t271497070 * L_9 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3ChiddenU3E_3();
		NullCheck((EqualityComparer_1_t271497070 *)L_9);
		int32_t L_11 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_9, (Il2CppObject *)L_10);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)))^(int32_t)L_8))*(int32_t)((int32_t)16777619)))^(int32_t)L_11))*(int32_t)((int32_t)16777619)));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)((int32_t)((int32_t)L_13<<(int32_t)((int32_t)13)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14^(int32_t)((int32_t)((int32_t)L_15>>(int32_t)7))));
		int32_t L_16 = V_0;
		int32_t L_17 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)((int32_t)((int32_t)L_17<<(int32_t)3))));
		int32_t L_18 = V_0;
		int32_t L_19 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_18^(int32_t)((int32_t)((int32_t)L_19>>(int32_t)((int32_t)17)))));
		int32_t L_20 = V_0;
		int32_t L_21 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)((int32_t)((int32_t)L_21<<(int32_t)5))));
		int32_t L_22 = V_0;
		return L_22;
	}
}
// System.String <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral2002584422;
extern Il2CppCodeGenString* _stringLiteral2427545121;
extern Il2CppCodeGenString* _stringLiteral2984578724;
extern Il2CppCodeGenString* _stringLiteral1375397573;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType0_4_ToString_m133639554_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType0_4_ToString_m133639554_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType0_4_ToString_m133639554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	int32_t G_B8_0 = 0;
	StringU5BU5D_t2956870243* G_B8_1 = NULL;
	StringU5BU5D_t2956870243* G_B8_2 = NULL;
	int32_t G_B7_0 = 0;
	StringU5BU5D_t2956870243* G_B7_1 = NULL;
	StringU5BU5D_t2956870243* G_B7_2 = NULL;
	String_t* G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	StringU5BU5D_t2956870243* G_B9_2 = NULL;
	StringU5BU5D_t2956870243* G_B9_3 = NULL;
	int32_t G_B11_0 = 0;
	StringU5BU5D_t2956870243* G_B11_1 = NULL;
	StringU5BU5D_t2956870243* G_B11_2 = NULL;
	int32_t G_B10_0 = 0;
	StringU5BU5D_t2956870243* G_B10_1 = NULL;
	StringU5BU5D_t2956870243* G_B10_2 = NULL;
	String_t* G_B12_0 = NULL;
	int32_t G_B12_1 = 0;
	StringU5BU5D_t2956870243* G_B12_2 = NULL;
	StringU5BU5D_t2956870243* G_B12_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2002584422);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2002584422);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0042;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0047;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0047:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral2427545121);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2427545121);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CtutorU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007b;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CtutorU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0080;
	}

IL_007b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0080:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral2984578724);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral2984578724);
		StringU5BU5D_t2956870243* L_14 = (StringU5BU5D_t2956870243*)L_13;
		Il2CppObject * L_15 = (Il2CppObject *)__this->get_U3CbottomU3E_2();
		G_B7_0 = 6;
		G_B7_1 = L_14;
		G_B7_2 = L_14;
		if (!L_15)
		{
			G_B8_0 = 6;
			G_B8_1 = L_14;
			G_B8_2 = L_14;
			goto IL_00b4;
		}
	}
	{
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_U3CbottomU3E_2();
		V_2 = (Il2CppObject *)L_16;
		NullCheck((Il2CppObject *)(*(&V_2)));
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_2)));
		G_B9_0 = L_17;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_00b9;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B9_0 = L_18;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_00b9:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (String_t*)G_B9_0);
		StringU5BU5D_t2956870243* L_19 = (StringU5BU5D_t2956870243*)G_B9_3;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, _stringLiteral1375397573);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral1375397573);
		StringU5BU5D_t2956870243* L_20 = (StringU5BU5D_t2956870243*)L_19;
		Il2CppObject * L_21 = (Il2CppObject *)__this->get_U3ChiddenU3E_3();
		G_B10_0 = 8;
		G_B10_1 = L_20;
		G_B10_2 = L_20;
		if (!L_21)
		{
			G_B11_0 = 8;
			G_B11_1 = L_20;
			G_B11_2 = L_20;
			goto IL_00ed;
		}
	}
	{
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_U3ChiddenU3E_3();
		V_3 = (Il2CppObject *)L_22;
		NullCheck((Il2CppObject *)(*(&V_3)));
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_3)));
		G_B12_0 = L_23;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_00f2;
	}

IL_00ed:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B12_0 = L_24;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_00f2:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		ArrayElementTypeCheck (G_B12_2, G_B12_0);
		(G_B12_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B12_1), (String_t*)G_B12_0);
		StringU5BU5D_t2956870243* L_25 = (StringU5BU5D_t2956870243*)G_B12_3;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)9));
		ArrayElementTypeCheck (L_25, _stringLiteral1117);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.Void <>__AnonType1`2<System.Boolean,System.Boolean>::.ctor(<show>__T,<hidden>__T)
extern "C"  void U3CU3E__AnonType1_2__ctor_m2869477419_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, bool ___show0, bool ___hidden1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___show0;
		__this->set_U3CshowU3E_0(L_0);
		bool L_1 = ___hidden1;
		__this->set_U3ChiddenU3E_1(L_1);
		return;
	}
}
// <show>__T <>__AnonType1`2<System.Boolean,System.Boolean>::get_show()
extern "C"  bool U3CU3E__AnonType1_2_get_show_m1770788966_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CshowU3E_0();
		return L_0;
	}
}
// <hidden>__T <>__AnonType1`2<System.Boolean,System.Boolean>::get_hidden()
extern "C"  bool U3CU3E__AnonType1_2_get_hidden_m1310890246_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3ChiddenU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType1`2<System.Boolean,System.Boolean>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType1_2_Equals_m2125273029_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType1_2_t2559104484 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType1_2_t2559104484 *)((U3CU3E__AnonType1_2_t2559104484 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType1_2_t2559104484 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_2 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_3 = (bool)__this->get_U3CshowU3E_0();
		U3CU3E__AnonType1_2_t2559104484 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (bool)L_4->get_U3CshowU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_2, (bool)L_3, (bool)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t3940363287 * L_7 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_8 = (bool)__this->get_U3ChiddenU3E_1();
		U3CU3E__AnonType1_2_t2559104484 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = (bool)L_9->get_U3ChiddenU3E_1();
		NullCheck((EqualityComparer_1_t3940363287 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_7, (bool)L_8, (bool)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType1`2<System.Boolean,System.Boolean>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType1_2_GetHashCode_m2036580713_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_0 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_1 = (bool)__this->get_U3CshowU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_0, (bool)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t3940363287 * L_3 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_4 = (bool)__this->get_U3ChiddenU3E_1();
		NullCheck((EqualityComparer_1_t3940363287 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_3, (bool)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType1`2<System.Boolean,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral2002584422;
extern Il2CppCodeGenString* _stringLiteral1375397573;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType1_2_ToString_m851545227_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType1_2_ToString_m851545227_gshared (U3CU3E__AnonType1_2_t2559104484 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType1_2_ToString_m851545227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2002584422);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2002584422);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		bool L_3 = (bool)__this->get_U3CshowU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		bool L_4 = (bool)__this->get_U3CshowU3E_0();
		V_0 = (bool)L_4;
		String_t* L_5 = Boolean_ToString_m2512358154((bool*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1375397573);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1375397573);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		bool L_9 = (bool)__this->get_U3ChiddenU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		bool L_10 = (bool)__this->get_U3ChiddenU3E_1();
		V_1 = (bool)L_10;
		String_t* L_11 = Boolean_ToString_m2512358154((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType1`2<System.Object,System.Object>::.ctor(<show>__T,<hidden>__T)
extern "C"  void U3CU3E__AnonType1_2__ctor_m1629335399_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, Il2CppObject * ___show0, Il2CppObject * ___hidden1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___show0;
		__this->set_U3CshowU3E_0(L_0);
		Il2CppObject * L_1 = ___hidden1;
		__this->set_U3ChiddenU3E_1(L_1);
		return;
	}
}
// <show>__T <>__AnonType1`2<System.Object,System.Object>::get_show()
extern "C"  Il2CppObject * U3CU3E__AnonType1_2_get_show_m3552153506_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		return L_0;
	}
}
// <hidden>__T <>__AnonType1`2<System.Object,System.Object>::get_hidden()
extern "C"  Il2CppObject * U3CU3E__AnonType1_2_get_hidden_m4204487170_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3ChiddenU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType1`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType1_2_Equals_m543332105_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType1_2_t3686769320 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType1_2_t3686769320 *)((U3CU3E__AnonType1_2_t3686769320 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType1_2_t3686769320 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		U3CU3E__AnonType1_2_t3686769320 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CshowU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3ChiddenU3E_1();
		U3CU3E__AnonType1_2_t3686769320 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3ChiddenU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType1`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType1_2_GetHashCode_m2936555053_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3ChiddenU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType1`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral2002584422;
extern Il2CppCodeGenString* _stringLiteral1375397573;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType1_2_ToString_m2447757191_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType1_2_ToString_m2447757191_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType1_2_ToString_m2447757191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2002584422);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2002584422);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CshowU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1375397573);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1375397573);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3ChiddenU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3ChiddenU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType2`2<System.Boolean,System.Boolean>::.ctor(<hidden>__T,<showTable>__T)
extern "C"  void U3CU3E__AnonType2_2__ctor_m1934779086_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, bool ___hidden0, bool ___showTable1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___hidden0;
		__this->set_U3ChiddenU3E_0(L_0);
		bool L_1 = ___showTable1;
		__this->set_U3CshowTableU3E_1(L_1);
		return;
	}
}
// <hidden>__T <>__AnonType2`2<System.Boolean,System.Boolean>::get_hidden()
extern "C"  bool U3CU3E__AnonType2_2_get_hidden_m2280471269_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3ChiddenU3E_0();
		return L_0;
	}
}
// <showTable>__T <>__AnonType2`2<System.Boolean,System.Boolean>::get_showTable()
extern "C"  bool U3CU3E__AnonType2_2_get_showTable_m1407594909_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CshowTableU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType2`2<System.Boolean,System.Boolean>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType2_2_Equals_m2755731654_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType2_2_t3656635463 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType2_2_t3656635463 *)((U3CU3E__AnonType2_2_t3656635463 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType2_2_t3656635463 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_2 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_3 = (bool)__this->get_U3ChiddenU3E_0();
		U3CU3E__AnonType2_2_t3656635463 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (bool)L_4->get_U3ChiddenU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_2, (bool)L_3, (bool)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t3940363287 * L_7 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_8 = (bool)__this->get_U3CshowTableU3E_1();
		U3CU3E__AnonType2_2_t3656635463 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = (bool)L_9->get_U3CshowTableU3E_1();
		NullCheck((EqualityComparer_1_t3940363287 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_7, (bool)L_8, (bool)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType2`2<System.Boolean,System.Boolean>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType2_2_GetHashCode_m2028821354_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_0 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_1 = (bool)__this->get_U3ChiddenU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_0, (bool)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t3940363287 * L_3 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_4 = (bool)__this->get_U3CshowTableU3E_1();
		NullCheck((EqualityComparer_1_t3940363287 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_3, (bool)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType2`2<System.Boolean,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral3131846041;
extern Il2CppCodeGenString* _stringLiteral978809510;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType2_2_ToString_m3963165226_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType2_2_ToString_m3963165226_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType2_2_ToString_m3963165226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral3131846041);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3131846041);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		bool L_3 = (bool)__this->get_U3ChiddenU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		bool L_4 = (bool)__this->get_U3ChiddenU3E_0();
		V_0 = (bool)L_4;
		String_t* L_5 = Boolean_ToString_m2512358154((bool*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral978809510);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral978809510);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		bool L_9 = (bool)__this->get_U3CshowTableU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		bool L_10 = (bool)__this->get_U3CshowTableU3E_1();
		V_1 = (bool)L_10;
		String_t* L_11 = Boolean_ToString_m2512358154((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType2`2<System.Object,System.Object>::.ctor(<hidden>__T,<showTable>__T)
extern "C"  void U3CU3E__AnonType2_2__ctor_m2429388114_gshared (U3CU3E__AnonType2_2_t489333003 * __this, Il2CppObject * ___hidden0, Il2CppObject * ___showTable1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___hidden0;
		__this->set_U3ChiddenU3E_0(L_0);
		Il2CppObject * L_1 = ___showTable1;
		__this->set_U3CshowTableU3E_1(L_1);
		return;
	}
}
// <hidden>__T <>__AnonType2`2<System.Object,System.Object>::get_hidden()
extern "C"  Il2CppObject * U3CU3E__AnonType2_2_get_hidden_m3021139873_gshared (U3CU3E__AnonType2_2_t489333003 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3ChiddenU3E_0();
		return L_0;
	}
}
// <showTable>__T <>__AnonType2`2<System.Object,System.Object>::get_showTable()
extern "C"  Il2CppObject * U3CU3E__AnonType2_2_get_showTable_m922775713_gshared (U3CU3E__AnonType2_2_t489333003 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CshowTableU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType2`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType2_2_Equals_m1236724810_gshared (U3CU3E__AnonType2_2_t489333003 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType2_2_t489333003 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType2_2_t489333003 *)((U3CU3E__AnonType2_2_t489333003 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType2_2_t489333003 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3ChiddenU3E_0();
		U3CU3E__AnonType2_2_t489333003 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3ChiddenU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CshowTableU3E_1();
		U3CU3E__AnonType2_2_t489333003 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CshowTableU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType2`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType2_2_GetHashCode_m612527214_gshared (U3CU3E__AnonType2_2_t489333003 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3ChiddenU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CshowTableU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType2`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral3131846041;
extern Il2CppCodeGenString* _stringLiteral978809510;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType2_2_ToString_m1423063270_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType2_2_ToString_m1423063270_gshared (U3CU3E__AnonType2_2_t489333003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType2_2_ToString_m1423063270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral3131846041);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3131846041);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3ChiddenU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3ChiddenU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral978809510);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral978809510);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CshowTableU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CshowTableU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType3`2<System.Boolean,System.Int32>::.ctor(<b>__T,<i>__T)
extern "C"  void U3CU3E__AnonType3_2__ctor_m4134975535_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, bool ___b0, int32_t ___i1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___b0;
		__this->set_U3CbU3E_0(L_0);
		int32_t L_1 = ___i1;
		__this->set_U3CiU3E_1(L_1);
		return;
	}
}
// <b>__T <>__AnonType3`2<System.Boolean,System.Int32>::get_b()
extern "C"  bool U3CU3E__AnonType3_2_get_b_m3615758246_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CbU3E_0();
		return L_0;
	}
}
// <i>__T <>__AnonType3`2<System.Boolean,System.Int32>::get_i()
extern "C"  int32_t U3CU3E__AnonType3_2_get_i_m1283246900_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CiU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType3`2<System.Boolean,System.Int32>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType3_2_Equals_m3411759213_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType3_2_t3095608592 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType3_2_t3095608592 *)((U3CU3E__AnonType3_2_t3095608592 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType3_2_t3095608592 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_2 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_3 = (bool)__this->get_U3CbU3E_0();
		U3CU3E__AnonType3_2_t3095608592 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (bool)L_4->get_U3CbU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_2, (bool)L_3, (bool)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t2281805437 * L_7 = ((  EqualityComparer_1_t2281805437 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_8 = (int32_t)__this->get_U3CiU3E_1();
		U3CU3E__AnonType3_2_t3095608592 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = (int32_t)L_9->get_U3CiU3E_1();
		NullCheck((EqualityComparer_1_t2281805437 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(!0,!0) */, (EqualityComparer_1_t2281805437 *)L_7, (int32_t)L_8, (int32_t)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType3`2<System.Boolean,System.Int32>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType3_2_GetHashCode_m485663633_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_0 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_1 = (bool)__this->get_U3CbU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_0, (bool)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t2281805437 * L_3 = ((  EqualityComparer_1_t2281805437 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_4 = (int32_t)__this->get_U3CiU3E_1();
		NullCheck((EqualityComparer_1_t2281805437 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(!0) */, (EqualityComparer_1_t2281805437 *)L_3, (int32_t)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType3`2<System.Boolean,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral32504865;
extern Il2CppCodeGenString* _stringLiteral1292396046;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType3_2_ToString_m715328675_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType3_2_ToString_m715328675_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType3_2_ToString_m715328675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral32504865);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral32504865);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		bool L_3 = (bool)__this->get_U3CbU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		bool L_4 = (bool)__this->get_U3CbU3E_0();
		V_0 = (bool)L_4;
		String_t* L_5 = Boolean_ToString_m2512358154((bool*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1292396046);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1292396046);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		int32_t L_9 = (int32_t)__this->get_U3CiU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		int32_t L_10 = (int32_t)__this->get_U3CiU3E_1();
		V_1 = (int32_t)L_10;
		String_t* L_11 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType3`2<System.Object,System.Object>::.ctor(<b>__T,<i>__T)
extern "C"  void U3CU3E__AnonType3_2__ctor_m3532991313_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, Il2CppObject * ___b0, Il2CppObject * ___i1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___b0;
		__this->set_U3CbU3E_0(L_0);
		Il2CppObject * L_1 = ___i1;
		__this->set_U3CiU3E_1(L_1);
		return;
	}
}
// <b>__T <>__AnonType3`2<System.Object,System.Object>::get_b()
extern "C"  Il2CppObject * U3CU3E__AnonType3_2_get_b_m2717857092_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CbU3E_0();
		return L_0;
	}
}
// <i>__T <>__AnonType3`2<System.Object,System.Object>::get_i()
extern "C"  Il2CppObject * U3CU3E__AnonType3_2_get_i_m385345746_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CiU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType3`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType3_2_Equals_m1930117515_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType3_2_t1586863982 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType3_2_t1586863982 *)((U3CU3E__AnonType3_2_t1586863982 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType3_2_t1586863982 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CbU3E_0();
		U3CU3E__AnonType3_2_t1586863982 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CbU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CiU3E_1();
		U3CU3E__AnonType3_2_t1586863982 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CiU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType3`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType3_2_GetHashCode_m2583466671_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CbU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CiU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType3`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral32504865;
extern Il2CppCodeGenString* _stringLiteral1292396046;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType3_2_ToString_m398369349_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType3_2_ToString_m398369349_gshared (U3CU3E__AnonType3_2_t1586863982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType3_2_ToString_m398369349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral32504865);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral32504865);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CbU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CbU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1292396046);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1292396046);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CiU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CiU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType4`2<System.Object,System.Object>::.ctor(<f>__T,<valuta>__T)
extern "C"  void U3CU3E__AnonType4_2__ctor_m4203460648_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, Il2CppObject * ___f0, Il2CppObject * ___valuta1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___f0;
		__this->set_U3CfU3E_0(L_0);
		Il2CppObject * L_1 = ___valuta1;
		__this->set_U3CvalutaU3E_1(L_1);
		return;
	}
}
// <f>__T <>__AnonType4`2<System.Object,System.Object>::get_f()
extern "C"  Il2CppObject * U3CU3E__AnonType4_2_get_f_m1903600205_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CfU3E_0();
		return L_0;
	}
}
// <valuta>__T <>__AnonType4`2<System.Object,System.Object>::get_valuta()
extern "C"  Il2CppObject * U3CU3E__AnonType4_2_get_valuta_m2588036671_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType4`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType4_2_Equals_m2623510220_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType4_2_t2684394961 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType4_2_t2684394961 *)((U3CU3E__AnonType4_2_t2684394961 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType4_2_t2684394961 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CfU3E_0();
		U3CU3E__AnonType4_2_t2684394961 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CfU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		U3CU3E__AnonType4_2_t2684394961 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CvalutaU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType4`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType4_2_GetHashCode_m259438832_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CfU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType4`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral32624029;
extern Il2CppCodeGenString* _stringLiteral4088481326;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType4_2_ToString_m3668642724_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType4_2_ToString_m3668642724_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType4_2_ToString_m3668642724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral32624029);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral32624029);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CfU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CfU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral4088481326);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral4088481326);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType4`2<System.Single,System.Object>::.ctor(<f>__T,<valuta>__T)
extern "C"  void U3CU3E__AnonType4_2__ctor_m2661760497_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, float ___f0, Il2CppObject * ___valuta1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		float L_0 = ___f0;
		__this->set_U3CfU3E_0(L_0);
		Il2CppObject * L_1 = ___valuta1;
		__this->set_U3CvalutaU3E_1(L_1);
		return;
	}
}
// <f>__T <>__AnonType4`2<System.Single,System.Object>::get_f()
extern "C"  float U3CU3E__AnonType4_2_get_f_m487471190_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_U3CfU3E_0();
		return L_0;
	}
}
// <valuta>__T <>__AnonType4`2<System.Single,System.Object>::get_valuta()
extern "C"  Il2CppObject * U3CU3E__AnonType4_2_get_valuta_m2605979094_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType4`2<System.Single,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType4_2_Equals_m1823435029_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType4_2_t3892302468 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType4_2_t3892302468 *)((U3CU3E__AnonType4_2_t3892302468 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType4_2_t3892302468 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t392599671 * L_2 = ((  EqualityComparer_1_t392599671 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		float L_3 = (float)__this->get_U3CfU3E_0();
		U3CU3E__AnonType4_2_t3892302468 * L_4 = V_0;
		NullCheck(L_4);
		float L_5 = (float)L_4->get_U3CfU3E_0();
		NullCheck((EqualityComparer_1_t392599671 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, float, float >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(!0,!0) */, (EqualityComparer_1_t392599671 *)L_2, (float)L_3, (float)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		U3CU3E__AnonType4_2_t3892302468 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CvalutaU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType4`2<System.Single,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType4_2_GetHashCode_m815653945_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t392599671 * L_0 = ((  EqualityComparer_1_t392599671 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		float L_1 = (float)__this->get_U3CfU3E_0();
		NullCheck((EqualityComparer_1_t392599671 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, float >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::GetHashCode(!0) */, (EqualityComparer_1_t392599671 *)L_0, (float)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType4`2<System.Single,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral32624029;
extern Il2CppCodeGenString* _stringLiteral4088481326;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType4_2_ToString_m937938171_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType4_2_ToString_m937938171_gshared (U3CU3E__AnonType4_2_t3892302468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType4_2_ToString_m937938171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral32624029);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral32624029);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		float L_3 = (float)__this->get_U3CfU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		float L_4 = (float)__this->get_U3CfU3E_0();
		V_0 = (float)L_4;
		String_t* L_5 = Single_ToString_m5736032((float*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral4088481326);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral4088481326);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CvalutaU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType5`2<System.DateTime,System.Int64>::.ctor(<time>__T,<_>__T)
extern "C"  void U3CU3E__AnonType5_2__ctor_m3089998308_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, DateTime_t339033936  ___time0, int64_t ____1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DateTime_t339033936  L_0 = ___time0;
		__this->set_U3CtimeU3E_0(L_0);
		int64_t L_1 = ____1;
		__this->set_U3C_U3E_1(L_1);
		return;
	}
}
// <time>__T <>__AnonType5`2<System.DateTime,System.Int64>::get_time()
extern "C"  DateTime_t339033936  U3CU3E__AnonType5_2_get_time_m37887292_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = (DateTime_t339033936 )__this->get_U3CtimeU3E_0();
		return L_0;
	}
}
// <_>__T <>__AnonType5`2<System.DateTime,System.Int64>::get__()
extern "C"  int64_t U3CU3E__AnonType5_2_get___m3540666626_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_U3C_U3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType5`2<System.DateTime,System.Int64>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType5_2_Equals_m2860831791_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType5_2_t3897323094 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType5_2_t3897323094 *)((U3CU3E__AnonType5_2_t3897323094 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType5_2_t3897323094 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t4068391882 * L_2 = ((  EqualityComparer_1_t4068391882 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		DateTime_t339033936  L_3 = (DateTime_t339033936 )__this->get_U3CtimeU3E_0();
		U3CU3E__AnonType5_2_t3897323094 * L_4 = V_0;
		NullCheck(L_4);
		DateTime_t339033936  L_5 = (DateTime_t339033936 )L_4->get_U3CtimeU3E_0();
		NullCheck((EqualityComparer_1_t4068391882 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, DateTime_t339033936 , DateTime_t339033936  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.DateTime>::Equals(!0,!0) */, (EqualityComparer_1_t4068391882 *)L_2, (DateTime_t339033936 )L_3, (DateTime_t339033936 )L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t2281805532 * L_7 = ((  EqualityComparer_1_t2281805532 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int64_t L_8 = (int64_t)__this->get_U3C_U3E_1();
		U3CU3E__AnonType5_2_t3897323094 * L_9 = V_0;
		NullCheck(L_9);
		int64_t L_10 = (int64_t)L_9->get_U3C_U3E_1();
		NullCheck((EqualityComparer_1_t2281805532 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, int64_t, int64_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int64>::Equals(!0,!0) */, (EqualityComparer_1_t2281805532 *)L_7, (int64_t)L_8, (int64_t)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType5`2<System.DateTime,System.Int64>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType5_2_GetHashCode_m4155620679_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t4068391882 * L_0 = ((  EqualityComparer_1_t4068391882 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		DateTime_t339033936  L_1 = (DateTime_t339033936 )__this->get_U3CtimeU3E_0();
		NullCheck((EqualityComparer_1_t4068391882 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, DateTime_t339033936  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.DateTime>::GetHashCode(!0) */, (EqualityComparer_1_t4068391882 *)L_0, (DateTime_t339033936 )L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t2281805532 * L_3 = ((  EqualityComparer_1_t2281805532 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int64_t L_4 = (int64_t)__this->get_U3C_U3E_1();
		NullCheck((EqualityComparer_1_t2281805532 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, int64_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int64>::GetHashCode(!0) */, (EqualityComparer_1_t2281805532 *)L_3, (int64_t)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType5`2<System.DateTime,System.Int64>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral2916333974;
extern Il2CppCodeGenString* _stringLiteral1292098136;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType5_2_ToString_m1408186147_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType5_2_ToString_m1408186147_gshared (U3CU3E__AnonType5_2_t3897323094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType5_2_ToString_m1408186147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int64_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2916333974);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2916333974);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		DateTime_t339033936  L_3 = (DateTime_t339033936 )__this->get_U3CtimeU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		DateTime_t339033936  L_4 = (DateTime_t339033936 )__this->get_U3CtimeU3E_0();
		V_0 = (DateTime_t339033936 )L_4;
		String_t* L_5 = DateTime_ToString_m3221907059((DateTime_t339033936 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1292098136);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1292098136);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		int64_t L_9 = (int64_t)__this->get_U3C_U3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		int64_t L_10 = (int64_t)__this->get_U3C_U3E_1();
		V_1 = (int64_t)L_10;
		String_t* L_11 = Int64_ToString_m3478011791((int64_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType5`2<System.Object,System.Object>::.ctor(<time>__T,<_>__T)
extern "C"  void U3CU3E__AnonType5_2__ctor_m2680221738_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, Il2CppObject * ___time0, Il2CppObject * ____1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___time0;
		__this->set_U3CtimeU3E_0(L_0);
		Il2CppObject * L_1 = ____1;
		__this->set_U3C_U3E_1(L_1);
		return;
	}
}
// <time>__T <>__AnonType5`2<System.Object,System.Object>::get_time()
extern "C"  Il2CppObject * U3CU3E__AnonType5_2_get_time_m1506448158_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CtimeU3E_0();
		return L_0;
	}
}
// <_>__T <>__AnonType5`2<System.Object,System.Object>::get__()
extern "C"  Il2CppObject * U3CU3E__AnonType5_2_get___m2300451264_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3C_U3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType5`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType5_2_Equals_m3316902925_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType5_2_t3781925940 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType5_2_t3781925940 *)((U3CU3E__AnonType5_2_t3781925940 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType5_2_t3781925940 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CtimeU3E_0();
		U3CU3E__AnonType5_2_t3781925940 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CtimeU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3C_U3E_1();
		U3CU3E__AnonType5_2_t3781925940 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3C_U3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType5`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType5_2_GetHashCode_m2230378289_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CtimeU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3C_U3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType5`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral2916333974;
extern Il2CppCodeGenString* _stringLiteral1292098136;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType5_2_ToString_m2643948803_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType5_2_ToString_m2643948803_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType5_2_ToString_m2643948803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2916333974);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2916333974);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CtimeU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CtimeU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1292098136);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1292098136);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3C_U3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3C_U3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType6`2<System.Double,System.Int32>::.ctor(<money>__T,<level>__T)
extern "C"  void U3CU3E__AnonType6_2__ctor_m870492846_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, double ___money0, int32_t ___level1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		double L_0 = ___money0;
		__this->set_U3CmoneyU3E_0(L_0);
		int32_t L_1 = ___level1;
		__this->set_U3ClevelU3E_1(L_1);
		return;
	}
}
// <money>__T <>__AnonType6`2<System.Double,System.Int32>::get_money()
extern "C"  double U3CU3E__AnonType6_2_get_money_m3007538860_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, const MethodInfo* method)
{
	{
		double L_0 = (double)__this->get_U3CmoneyU3E_0();
		return L_0;
	}
}
// <level>__T <>__AnonType6`2<System.Double,System.Int32>::get_level()
extern "C"  int32_t U3CU3E__AnonType6_2_get_level_m1710193708_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3ClevelU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType6`2<System.Double,System.Int32>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType6_2_Equals_m3329063801_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType6_2_t3631555212 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType6_2_t3631555212 *)((U3CU3E__AnonType6_2_t3631555212 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType6_2_t3631555212 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t4263874560 * L_2 = ((  EqualityComparer_1_t4263874560 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		double L_3 = (double)__this->get_U3CmoneyU3E_0();
		U3CU3E__AnonType6_2_t3631555212 * L_4 = V_0;
		NullCheck(L_4);
		double L_5 = (double)L_4->get_U3CmoneyU3E_0();
		NullCheck((EqualityComparer_1_t4263874560 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, double, double >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Double>::Equals(!0,!0) */, (EqualityComparer_1_t4263874560 *)L_2, (double)L_3, (double)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t2281805437 * L_7 = ((  EqualityComparer_1_t2281805437 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_8 = (int32_t)__this->get_U3ClevelU3E_1();
		U3CU3E__AnonType6_2_t3631555212 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = (int32_t)L_9->get_U3ClevelU3E_1();
		NullCheck((EqualityComparer_1_t2281805437 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(!0,!0) */, (EqualityComparer_1_t2281805437 *)L_7, (int32_t)L_8, (int32_t)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType6`2<System.Double,System.Int32>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType6_2_GetHashCode_m1637063953_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t4263874560 * L_0 = ((  EqualityComparer_1_t4263874560 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		double L_1 = (double)__this->get_U3CmoneyU3E_0();
		NullCheck((EqualityComparer_1_t4263874560 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, double >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Double>::GetHashCode(!0) */, (EqualityComparer_1_t4263874560 *)L_0, (double)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t2281805437 * L_3 = ((  EqualityComparer_1_t2281805437 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_4 = (int32_t)__this->get_U3ClevelU3E_1();
		NullCheck((EqualityComparer_1_t2281805437 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(!0) */, (EqualityComparer_1_t2281805437 *)L_3, (int32_t)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType6`2<System.Double,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral1958577923;
extern Il2CppCodeGenString* _stringLiteral102651667;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType6_2_ToString_m2164810329_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType6_2_ToString_m2164810329_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType6_2_ToString_m2164810329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral1958577923);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1958577923);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		double L_3 = (double)__this->get_U3CmoneyU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		double L_4 = (double)__this->get_U3CmoneyU3E_0();
		V_0 = (double)L_4;
		String_t* L_5 = Double_ToString_m3380246633((double*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral102651667);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral102651667);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		int32_t L_9 = (int32_t)__this->get_U3ClevelU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		int32_t L_10 = (int32_t)__this->get_U3ClevelU3E_1();
		V_1 = (int32_t)L_10;
		String_t* L_11 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType6`2<System.Object,System.Object>::.ctor(<money>__T,<level>__T)
extern "C"  void U3CU3E__AnonType6_2__ctor_m3585939793_gshared (U3CU3E__AnonType6_2_t584489623 * __this, Il2CppObject * ___money0, Il2CppObject * ___level1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___money0;
		__this->set_U3CmoneyU3E_0(L_0);
		Il2CppObject * L_1 = ___level1;
		__this->set_U3ClevelU3E_1(L_1);
		return;
	}
}
// <money>__T <>__AnonType6`2<System.Object,System.Object>::get_money()
extern "C"  Il2CppObject * U3CU3E__AnonType6_2_get_money_m3568823683_gshared (U3CU3E__AnonType6_2_t584489623 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CmoneyU3E_0();
		return L_0;
	}
}
// <level>__T <>__AnonType6`2<System.Object,System.Object>::get_level()
extern "C"  Il2CppObject * U3CU3E__AnonType6_2_get_level_m2638676747_gshared (U3CU3E__AnonType6_2_t584489623 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3ClevelU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType6`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType6_2_Equals_m4010295630_gshared (U3CU3E__AnonType6_2_t584489623 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType6_2_t584489623 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType6_2_t584489623 *)((U3CU3E__AnonType6_2_t584489623 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType6_2_t584489623 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CmoneyU3E_0();
		U3CU3E__AnonType6_2_t584489623 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CmoneyU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3ClevelU3E_1();
		U3CU3E__AnonType6_2_t584489623 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3ClevelU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType6`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType6_2_GetHashCode_m4201317746_gshared (U3CU3E__AnonType6_2_t584489623 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CmoneyU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3ClevelU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType6`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral1958577923;
extern Il2CppCodeGenString* _stringLiteral102651667;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType6_2_ToString_m1619254882_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType6_2_ToString_m1619254882_gshared (U3CU3E__AnonType6_2_t584489623 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType6_2_ToString_m1619254882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral1958577923);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1958577923);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CmoneyU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CmoneyU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral102651667);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral102651667);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3ClevelU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3ClevelU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType7`2<System.Boolean,System.Boolean>::.ctor(<reward>__T,<loggedIn>__T)
extern "C"  void U3CU3E__AnonType7_2__ctor_m1817581750_gshared (U3CU3E__AnonType7_2_t554355766 * __this, bool ___reward0, bool ___loggedIn1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___reward0;
		__this->set_U3CrewardU3E_0(L_0);
		bool L_1 = ___loggedIn1;
		__this->set_U3CloggedInU3E_1(L_1);
		return;
	}
}
// <reward>__T <>__AnonType7`2<System.Boolean,System.Boolean>::get_reward()
extern "C"  bool U3CU3E__AnonType7_2_get_reward_m390852512_gshared (U3CU3E__AnonType7_2_t554355766 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CrewardU3E_0();
		return L_0;
	}
}
// <loggedIn>__T <>__AnonType7`2<System.Boolean,System.Boolean>::get_loggedIn()
extern "C"  bool U3CU3E__AnonType7_2_get_loggedIn_m3813944032_gshared (U3CU3E__AnonType7_2_t554355766 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CloggedInU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType7`2<System.Boolean,System.Boolean>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType7_2_Equals_m1613057483_gshared (U3CU3E__AnonType7_2_t554355766 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType7_2_t554355766 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType7_2_t554355766 *)((U3CU3E__AnonType7_2_t554355766 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType7_2_t554355766 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_2 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_3 = (bool)__this->get_U3CrewardU3E_0();
		U3CU3E__AnonType7_2_t554355766 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (bool)L_4->get_U3CrewardU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_2, (bool)L_3, (bool)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t3940363287 * L_7 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_8 = (bool)__this->get_U3CloggedInU3E_1();
		U3CU3E__AnonType7_2_t554355766 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = (bool)L_9->get_U3CloggedInU3E_1();
		NullCheck((EqualityComparer_1_t3940363287 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, bool, bool >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::Equals(!0,!0) */, (EqualityComparer_1_t3940363287 *)L_7, (bool)L_8, (bool)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType7`2<System.Boolean,System.Boolean>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType7_2_GetHashCode_m1990024559_gshared (U3CU3E__AnonType7_2_t554355766 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t3940363287 * L_0 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_1 = (bool)__this->get_U3CrewardU3E_0();
		NullCheck((EqualityComparer_1_t3940363287 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_0, (bool)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t3940363287 * L_3 = ((  EqualityComparer_1_t3940363287 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		bool L_4 = (bool)__this->get_U3CloggedInU3E_1();
		NullCheck((EqualityComparer_1_t3940363287 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, bool >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::GetHashCode(!0) */, (EqualityComparer_1_t3940363287 *)L_3, (bool)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType7`2<System.Boolean,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral3464385012;
extern Il2CppCodeGenString* _stringLiteral2984258056;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType7_2_ToString_m2341396037_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType7_2_ToString_m2341396037_gshared (U3CU3E__AnonType7_2_t554355766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType7_2_ToString_m2341396037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral3464385012);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3464385012);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		bool L_3 = (bool)__this->get_U3CrewardU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		bool L_4 = (bool)__this->get_U3CrewardU3E_0();
		V_0 = (bool)L_4;
		String_t* L_5 = Boolean_ToString_m2512358154((bool*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral2984258056);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2984258056);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		bool L_9 = (bool)__this->get_U3CloggedInU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		bool L_10 = (bool)__this->get_U3CloggedInU3E_1();
		V_1 = (bool)L_10;
		String_t* L_11 = Boolean_ToString_m2512358154((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType7`2<System.Object,System.Object>::.ctor(<reward>__T,<loggedIn>__T)
extern "C"  void U3CU3E__AnonType7_2__ctor_m4130492530_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, Il2CppObject * ___reward0, Il2CppObject * ___loggedIn1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___reward0;
		__this->set_U3CrewardU3E_0(L_0);
		Il2CppObject * L_1 = ___loggedIn1;
		__this->set_U3CloggedInU3E_1(L_1);
		return;
	}
}
// <reward>__T <>__AnonType7`2<System.Object,System.Object>::get_reward()
extern "C"  Il2CppObject * U3CU3E__AnonType7_2_get_reward_m3189197276_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CrewardU3E_0();
		return L_0;
	}
}
// <loggedIn>__T <>__AnonType7`2<System.Object,System.Object>::get_loggedIn()
extern "C"  Il2CppObject * U3CU3E__AnonType7_2_get_loggedIn_m824266780_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CloggedInU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType7`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType7_2_Equals_m408721039_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType7_2_t1682020602 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType7_2_t1682020602 *)((U3CU3E__AnonType7_2_t1682020602 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		U3CU3E__AnonType7_2_t1682020602 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_2 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CrewardU3E_0();
		U3CU3E__AnonType7_2_t1682020602 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CrewardU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_7 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CloggedInU3E_1();
		U3CU3E__AnonType7_2_t1682020602 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CloggedInU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType7`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType7_2_GetHashCode_m1877289907_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		EqualityComparer_1_t271497070 * L_0 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CrewardU3E_0();
		NullCheck((EqualityComparer_1_t271497070 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		EqualityComparer_1_t271497070 * L_3 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CloggedInU3E_1();
		NullCheck((EqualityComparer_1_t271497070 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t271497070 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType7`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral3464385012;
extern Il2CppCodeGenString* _stringLiteral2984258056;
extern Il2CppCodeGenString* _stringLiteral1117;
extern const uint32_t U3CU3E__AnonType7_2_ToString_m594560961_MetadataUsageId;
extern "C"  String_t* U3CU3E__AnonType7_2_ToString_m594560961_gshared (U3CU3E__AnonType7_2_t1682020602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType7_2_ToString_m594560961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral123);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral123);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral3464385012);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3464385012);
		StringU5BU5D_t2956870243* L_2 = (StringU5BU5D_t2956870243*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CrewardU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CrewardU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral2984258056);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2984258056);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CloggedInU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CloggedInU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, _stringLiteral1117);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void AbandonedStream`1<System.Object>::.ctor()
extern "C"  void AbandonedStream_1__ctor_m2472560868_gshared (AbandonedStream_1_t2531752022 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable AbandonedStream`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern Il2CppClass* EmptyDisposable_t1512564738_il2cpp_TypeInfo_var;
extern const uint32_t AbandonedStream_1_Listen_m694518664_MetadataUsageId;
extern "C"  Il2CppObject * AbandonedStream_1_Listen_m694518664_gshared (AbandonedStream_1_t2531752022 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbandonedStream_1_Listen_m694518664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EmptyDisposable_t1512564738 * L_0 = (EmptyDisposable_t1512564738 *)il2cpp_codegen_object_new(EmptyDisposable_t1512564738_il2cpp_TypeInfo_var);
		EmptyDisposable__ctor_m791146077(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.IDisposable AbandonedStream`1<System.Object>::Listen(System.Action,Priority)
extern Il2CppClass* EmptyDisposable_t1512564738_il2cpp_TypeInfo_var;
extern const uint32_t AbandonedStream_1_Listen_m4064897183_MetadataUsageId;
extern "C"  Il2CppObject * AbandonedStream_1_Listen_m4064897183_gshared (AbandonedStream_1_t2531752022 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbandonedStream_1_Listen_m4064897183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EmptyDisposable_t1512564738 * L_0 = (EmptyDisposable_t1512564738 *)il2cpp_codegen_object_new(EmptyDisposable_t1512564738_il2cpp_TypeInfo_var);
		EmptyDisposable__ctor_m791146077(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Boolean>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStoreyF9__ctor_m2910158178_gshared (U3CListenU3Ec__AnonStoreyF9_t1928065897 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Boolean>::<>m__16F(T)
extern "C"  void U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m1055092294_gshared (U3CListenU3Ec__AnonStoreyF9_t1928065897 * __this, bool ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Double>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStoreyF9__ctor_m2451335361_gshared (U3CListenU3Ec__AnonStoreyF9_t2251577170 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Double>::<>m__16F(T)
extern "C"  void U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m1431802405_gshared (U3CListenU3Ec__AnonStoreyF9_t2251577170 * __this, double ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Int32>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStoreyF9__ctor_m4004770184_gshared (U3CListenU3Ec__AnonStoreyF9_t269508047 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Int32>::<>m__16F(T)
extern "C"  void U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m366960492_gshared (U3CListenU3Ec__AnonStoreyF9_t269508047 * __this, int32_t ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStoreyF9__ctor_m1046847151_gshared (U3CListenU3Ec__AnonStoreyF9_t2554166976 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Object>::<>m__16F(T)
extern "C"  void U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m2904039699_gshared (U3CListenU3Ec__AnonStoreyF9_t2554166976 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Boolean>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA__ctor_m1298588278_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Boolean>::<>m__170(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m246956209_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 * __this, bool ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Double>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA__ctor_m44044589_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Double>::<>m__170(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m297614312_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 * __this, double ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Int32>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA__ctor_m740527004_gshared (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Int32>::<>m__170(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m2131748567_gshared (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 * __this, int32_t ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Object>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA__ctor_m2934523675_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Object>::<>m__170(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m1769851606_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1<System.Boolean>::.ctor()
extern "C"  void AnonymousCell_1__ctor_m3473318386_gshared (AnonymousCell_1_t3772459138 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1<System.Boolean>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
extern "C"  void AnonymousCell_1__ctor_m2842727590_gshared (AnonymousCell_1_t3772459138 * __this, Func_3_t3884450017 * ___subscribe0, Func_1_t1353786588 * ___current1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_3_t3884450017 * L_0 = ___subscribe0;
		__this->set_listen_0(L_0);
		Func_1_t1353786588 * L_1 = ___current1;
		__this->set_current_1(L_1);
		return;
	}
}
// T AnonymousCell`1<System.Boolean>::get_value()
extern "C"  bool AnonymousCell_1_get_value_m729637367_gshared (AnonymousCell_1_t3772459138 * __this, const MethodInfo* method)
{
	{
		Func_1_t1353786588 * L_0 = (Func_1_t1353786588 *)__this->get_current_1();
		NullCheck((Func_1_t1353786588 *)L_0);
		bool L_1 = ((  bool (*) (Func_1_t1353786588 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1353786588 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_1;
	}
}
// System.IDisposable AnonymousCell`1<System.Boolean>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_ListenUpdates_m3927268892_gshared (AnonymousCell_1_t3772459138 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Func_3_t3884450017 * L_0 = (Func_3_t3884450017 *)__this->get_listen_0();
		Action_1_t359458046 * L_1 = ___reaction0;
		int32_t L_2 = ___p1;
		NullCheck((Func_3_t3884450017 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_3_t3884450017 *, Action_1_t359458046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t3884450017 *)L_0, (Action_1_t359458046 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_3;
	}
}
// System.IDisposable AnonymousCell`1<System.Boolean>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Bind_m1394253990_gshared (AnonymousCell_1_t3772459138 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t359458046 * L_0 = ___reaction0;
		Func_1_t1353786588 * L_1 = (Func_1_t1353786588 *)__this->get_current_1();
		NullCheck((Func_1_t1353786588 *)L_1);
		bool L_2 = ((  bool (*) (Func_1_t1353786588 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1353786588 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((Action_1_t359458046 *)L_0);
		((  void (*) (Action_1_t359458046 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t359458046 *)L_0, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t359458046 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((AnonymousCell_1_t3772459138 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(8 /* System.IDisposable AnonymousCell`1<System.Boolean>::ListenUpdates(System.Action`1<T>,Priority) */, (AnonymousCell_1_t3772459138 *)__this, (Action_1_t359458046 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// System.IDisposable AnonymousCell`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AnonymousCell_1_Subscribe_m1674443487_gshared (AnonymousCell_1_t3772459138 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject* L_1 = (Il2CppObject*)L_0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_1, 2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_1_t359458046 * L_3 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_1, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((AnonymousCell_1_t3772459138 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(8 /* System.IDisposable AnonymousCell`1<System.Boolean>::ListenUpdates(System.Action`1<T>,Priority) */, (AnonymousCell_1_t3772459138 *)__this, (Action_1_t359458046 *)L_3, (int32_t)1);
		return L_4;
	}
}
// System.IDisposable AnonymousCell`1<System.Boolean>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m736209628_gshared (AnonymousCell_1_t3772459138 * __this, Action_1_t359458046 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Action_1_t359458046 * L_0 = ___reaction0;
		int32_t L_1 = ___priority1;
		NullCheck((AnonymousCell_1_t3772459138 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(7 /* System.IDisposable AnonymousCell`1<System.Boolean>::Bind(System.Action`1<T>,Priority) */, (AnonymousCell_1_t3772459138 *)__this, (Action_1_t359458046 *)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.IDisposable AnonymousCell`1<System.Boolean>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m3644783819_gshared (AnonymousCell_1_t3772459138 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStoreyF9_t1928065897 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStoreyF9_t1928065897 * L_0 = (U3CListenU3Ec__AnonStoreyF9_t1928065897 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CListenU3Ec__AnonStoreyF9_t1928065897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CListenU3Ec__AnonStoreyF9_t1928065897 *)L_0;
		U3CListenU3Ec__AnonStoreyF9_t1928065897 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3CListenU3Ec__AnonStoreyF9_t1928065897 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_1_t359458046 * L_5 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_6 = ___priority1;
		NullCheck((AnonymousCell_1_t3772459138 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(7 /* System.IDisposable AnonymousCell`1<System.Boolean>::Bind(System.Action`1<T>,Priority) */, (AnonymousCell_1_t3772459138 *)__this, (Action_1_t359458046 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable AnonymousCell`1<System.Boolean>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_OnChanged_m1963156317_gshared (AnonymousCell_1_t3772459138 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyFA_t1149388695 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyFA_t1149388695 * L_0 = (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		V_0 = (U3COnChangedU3Ec__AnonStoreyFA_t1149388695 *)L_0;
		U3COnChangedU3Ec__AnonStoreyFA_t1149388695 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Func_3_t3884450017 * L_3 = (Func_3_t3884450017 *)__this->get_listen_0();
		U3COnChangedU3Ec__AnonStoreyFA_t1149388695 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		Action_1_t359458046 * L_6 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_7 = ___p1;
		NullCheck((Func_3_t3884450017 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t3884450017 *, Action_1_t359458046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t3884450017 *)L_3, (Action_1_t359458046 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_8;
	}
}
// System.Object AnonymousCell`1<System.Boolean>::get_valueObject()
extern "C"  Il2CppObject * AnonymousCell_1_get_valueObject_m3942661324_gshared (AnonymousCell_1_t3772459138 * __this, const MethodInfo* method)
{
	{
		NullCheck((AnonymousCell_1_t3772459138 *)__this);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(9 /* T AnonymousCell`1<System.Boolean>::get_value() */, (AnonymousCell_1_t3772459138 *)__this);
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), &L_1);
		return L_2;
	}
}
// System.Void AnonymousCell`1<System.Double>::.ctor()
extern "C"  void AnonymousCell_1__ctor_m4270617137_gshared (AnonymousCell_1_t4095970411 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1<System.Double>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
extern "C"  void AnonymousCell_1__ctor_m521802405_gshared (AnonymousCell_1_t4095970411 * __this, Func_3_t892795874 * ___subscribe0, Func_1_t1677297861 * ___current1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_3_t892795874 * L_0 = ___subscribe0;
		__this->set_listen_0(L_0);
		Func_1_t1677297861 * L_1 = ___current1;
		__this->set_current_1(L_1);
		return;
	}
}
// T AnonymousCell`1<System.Double>::get_value()
extern "C"  double AnonymousCell_1_get_value_m1409289432_gshared (AnonymousCell_1_t4095970411 * __this, const MethodInfo* method)
{
	{
		Func_1_t1677297861 * L_0 = (Func_1_t1677297861 *)__this->get_current_1();
		NullCheck((Func_1_t1677297861 *)L_0);
		double L_1 = ((  double (*) (Func_1_t1677297861 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1677297861 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_1;
	}
}
// System.IDisposable AnonymousCell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_ListenUpdates_m1966092355_gshared (AnonymousCell_1_t4095970411 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Func_3_t892795874 * L_0 = (Func_3_t892795874 *)__this->get_listen_0();
		Action_1_t682969319 * L_1 = ___reaction0;
		int32_t L_2 = ___p1;
		NullCheck((Func_3_t892795874 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_3_t892795874 *, Action_1_t682969319 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t892795874 *)L_0, (Action_1_t682969319 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_3;
	}
}
// System.IDisposable AnonymousCell`1<System.Double>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Bind_m3152967327_gshared (AnonymousCell_1_t4095970411 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t682969319 * L_0 = ___reaction0;
		Func_1_t1677297861 * L_1 = (Func_1_t1677297861 *)__this->get_current_1();
		NullCheck((Func_1_t1677297861 *)L_1);
		double L_2 = ((  double (*) (Func_1_t1677297861 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1677297861 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((Action_1_t682969319 *)L_0);
		((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t682969319 *)L_0, (double)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t682969319 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((AnonymousCell_1_t4095970411 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(8 /* System.IDisposable AnonymousCell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority) */, (AnonymousCell_1_t4095970411 *)__this, (Action_1_t682969319 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// System.IDisposable AnonymousCell`1<System.Double>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AnonymousCell_1_Subscribe_m3848338200_gshared (AnonymousCell_1_t4095970411 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject* L_1 = (Il2CppObject*)L_0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_1, 2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_1_t682969319 * L_3 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_1, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((AnonymousCell_1_t4095970411 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(8 /* System.IDisposable AnonymousCell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority) */, (AnonymousCell_1_t4095970411 *)__this, (Action_1_t682969319 *)L_3, (int32_t)1);
		return L_4;
	}
}
// System.IDisposable AnonymousCell`1<System.Double>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m2937579157_gshared (AnonymousCell_1_t4095970411 * __this, Action_1_t682969319 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Action_1_t682969319 * L_0 = ___reaction0;
		int32_t L_1 = ___priority1;
		NullCheck((AnonymousCell_1_t4095970411 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(7 /* System.IDisposable AnonymousCell`1<System.Double>::Bind(System.Action`1<T>,Priority) */, (AnonymousCell_1_t4095970411 *)__this, (Action_1_t682969319 *)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.IDisposable AnonymousCell`1<System.Double>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m2745078130_gshared (AnonymousCell_1_t4095970411 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStoreyF9_t2251577170 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStoreyF9_t2251577170 * L_0 = (U3CListenU3Ec__AnonStoreyF9_t2251577170 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CListenU3Ec__AnonStoreyF9_t2251577170 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CListenU3Ec__AnonStoreyF9_t2251577170 *)L_0;
		U3CListenU3Ec__AnonStoreyF9_t2251577170 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3CListenU3Ec__AnonStoreyF9_t2251577170 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_1_t682969319 * L_5 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_6 = ___priority1;
		NullCheck((AnonymousCell_1_t4095970411 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(7 /* System.IDisposable AnonymousCell`1<System.Double>::Bind(System.Action`1<T>,Priority) */, (AnonymousCell_1_t4095970411 *)__this, (Action_1_t682969319 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable AnonymousCell`1<System.Double>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_OnChanged_m3721869654_gshared (AnonymousCell_1_t4095970411 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyFA_t1472899968 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyFA_t1472899968 * L_0 = (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		V_0 = (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 *)L_0;
		U3COnChangedU3Ec__AnonStoreyFA_t1472899968 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Func_3_t892795874 * L_3 = (Func_3_t892795874 *)__this->get_listen_0();
		U3COnChangedU3Ec__AnonStoreyFA_t1472899968 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		Action_1_t682969319 * L_6 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_7 = ___p1;
		NullCheck((Func_3_t892795874 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t892795874 *, Action_1_t682969319 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t892795874 *)L_3, (Action_1_t682969319 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_8;
	}
}
// System.Object AnonymousCell`1<System.Double>::get_valueObject()
extern "C"  Il2CppObject * AnonymousCell_1_get_valueObject_m3207412929_gshared (AnonymousCell_1_t4095970411 * __this, const MethodInfo* method)
{
	{
		NullCheck((AnonymousCell_1_t4095970411 *)__this);
		double L_0 = VirtFuncInvoker0< double >::Invoke(9 /* T AnonymousCell`1<System.Double>::get_value() */, (AnonymousCell_1_t4095970411 *)__this);
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), &L_1);
		return L_2;
	}
}
// System.Void AnonymousCell`1<System.Int32>::.ctor()
extern "C"  void AnonymousCell_1__ctor_m3093625368_gshared (AnonymousCell_1_t2113901288 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1<System.Int32>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
extern "C"  void AnonymousCell_1__ctor_m69632588_gshared (AnonymousCell_1_t2113901288 * __this, Func_3_t722536535 * ___subscribe0, Func_1_t3990196034 * ___current1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_3_t722536535 * L_0 = ___subscribe0;
		__this->set_listen_0(L_0);
		Func_1_t3990196034 * L_1 = ___current1;
		__this->set_current_1(L_1);
		return;
	}
}
// T AnonymousCell`1<System.Int32>::get_value()
extern "C"  int32_t AnonymousCell_1_get_value_m1324051165_gshared (AnonymousCell_1_t2113901288 * __this, const MethodInfo* method)
{
	{
		Func_1_t3990196034 * L_0 = (Func_1_t3990196034 *)__this->get_current_1();
		NullCheck((Func_1_t3990196034 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Func_1_t3990196034 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t3990196034 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_1;
	}
}
// System.IDisposable AnonymousCell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_ListenUpdates_m2071196534_gshared (AnonymousCell_1_t2113901288 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Func_3_t722536535 * L_0 = (Func_3_t722536535 *)__this->get_listen_0();
		Action_1_t2995867492 * L_1 = ___reaction0;
		int32_t L_2 = ___p1;
		NullCheck((Func_3_t722536535 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_3_t722536535 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t722536535 *)L_0, (Action_1_t2995867492 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_3;
	}
}
// System.IDisposable AnonymousCell`1<System.Int32>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Bind_m2557459980_gshared (AnonymousCell_1_t2113901288 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t2995867492 * L_0 = ___reaction0;
		Func_1_t3990196034 * L_1 = (Func_1_t3990196034 *)__this->get_current_1();
		NullCheck((Func_1_t3990196034 *)L_1);
		int32_t L_2 = ((  int32_t (*) (Func_1_t3990196034 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t3990196034 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((Action_1_t2995867492 *)L_0);
		((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t2995867492 *)L_0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t2995867492 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((AnonymousCell_1_t2113901288 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(8 /* System.IDisposable AnonymousCell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority) */, (AnonymousCell_1_t2113901288 *)__this, (Action_1_t2995867492 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// System.IDisposable AnonymousCell`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AnonymousCell_1_Subscribe_m2927049157_gshared (AnonymousCell_1_t2113901288 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject* L_1 = (Il2CppObject*)L_0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_1, 2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_1_t2995867492 * L_3 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_1, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((AnonymousCell_1_t2113901288 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(8 /* System.IDisposable AnonymousCell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority) */, (AnonymousCell_1_t2113901288 *)__this, (Action_1_t2995867492 *)L_3, (int32_t)1);
		return L_4;
	}
}
// System.IDisposable AnonymousCell`1<System.Int32>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m1885669058_gshared (AnonymousCell_1_t2113901288 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Action_1_t2995867492 * L_0 = ___reaction0;
		int32_t L_1 = ___priority1;
		NullCheck((AnonymousCell_1_t2113901288 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(7 /* System.IDisposable AnonymousCell`1<System.Int32>::Bind(System.Action`1<T>,Priority) */, (AnonymousCell_1_t2113901288 *)__this, (Action_1_t2995867492 *)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.IDisposable AnonymousCell`1<System.Int32>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m3131001125_gshared (AnonymousCell_1_t2113901288 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStoreyF9_t269508047 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStoreyF9_t269508047 * L_0 = (U3CListenU3Ec__AnonStoreyF9_t269508047 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CListenU3Ec__AnonStoreyF9_t269508047 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CListenU3Ec__AnonStoreyF9_t269508047 *)L_0;
		U3CListenU3Ec__AnonStoreyF9_t269508047 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3CListenU3Ec__AnonStoreyF9_t269508047 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_1_t2995867492 * L_5 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_6 = ___priority1;
		NullCheck((AnonymousCell_1_t2113901288 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(7 /* System.IDisposable AnonymousCell`1<System.Int32>::Bind(System.Action`1<T>,Priority) */, (AnonymousCell_1_t2113901288 *)__this, (Action_1_t2995867492 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable AnonymousCell`1<System.Int32>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_OnChanged_m3126362307_gshared (AnonymousCell_1_t2113901288 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyFA_t3785798141 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyFA_t3785798141 * L_0 = (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		V_0 = (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 *)L_0;
		U3COnChangedU3Ec__AnonStoreyFA_t3785798141 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Func_3_t722536535 * L_3 = (Func_3_t722536535 *)__this->get_listen_0();
		U3COnChangedU3Ec__AnonStoreyFA_t3785798141 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		Action_1_t2995867492 * L_6 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_7 = ___p1;
		NullCheck((Func_3_t722536535 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t722536535 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t722536535 *)L_3, (Action_1_t2995867492 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_8;
	}
}
// System.Object AnonymousCell`1<System.Int32>::get_valueObject()
extern "C"  Il2CppObject * AnonymousCell_1_get_valueObject_m142079666_gshared (AnonymousCell_1_t2113901288 * __this, const MethodInfo* method)
{
	{
		NullCheck((AnonymousCell_1_t2113901288 *)__this);
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(9 /* T AnonymousCell`1<System.Int32>::get_value() */, (AnonymousCell_1_t2113901288 *)__this);
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), &L_1);
		return L_2;
	}
}
// System.Void AnonymousCell`1<System.Object>::.ctor()
extern "C"  void AnonymousCell_1__ctor_m2866128927_gshared (AnonymousCell_1_t103592921 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousCell`1<System.Object>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
extern "C"  void AnonymousCell_1__ctor_m1853677587_gshared (AnonymousCell_1_t103592921 * __this, Func_3_t1585443616 * ___subscribe0, Func_1_t1979887667 * ___current1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_3_t1585443616 * L_0 = ___subscribe0;
		__this->set_listen_0(L_0);
		Func_1_t1979887667 * L_1 = ___current1;
		__this->set_current_1(L_1);
		return;
	}
}
// T AnonymousCell`1<System.Object>::get_value()
extern "C"  Il2CppObject * AnonymousCell_1_get_value_m2881526726_gshared (AnonymousCell_1_t103592921 * __this, const MethodInfo* method)
{
	{
		Func_1_t1979887667 * L_0 = (Func_1_t1979887667 *)__this->get_current_1();
		NullCheck((Func_1_t1979887667 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_1;
	}
}
// System.IDisposable AnonymousCell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_ListenUpdates_m2243511189_gshared (AnonymousCell_1_t103592921 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Func_3_t1585443616 * L_0 = (Func_3_t1585443616 *)__this->get_listen_0();
		Action_1_t985559125 * L_1 = ___reaction0;
		int32_t L_2 = ___p1;
		NullCheck((Func_3_t1585443616 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_3_t1585443616 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t1585443616 *)L_0, (Action_1_t985559125 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_3;
	}
}
// System.IDisposable AnonymousCell`1<System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Bind_m1087176461_gshared (AnonymousCell_1_t103592921 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = ___reaction0;
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)__this->get_current_1();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((AnonymousCell_1_t103592921 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(8 /* System.IDisposable AnonymousCell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, (AnonymousCell_1_t103592921 *)__this, (Action_1_t985559125 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// System.IDisposable AnonymousCell`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AnonymousCell_1_Subscribe_m1803732742_gshared (AnonymousCell_1_t103592921 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject* L_1 = (Il2CppObject*)L_0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetInterfaceMethodInfo(L_1, 2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
		Action_1_t985559125 * L_3 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_3, (Il2CppObject *)L_1, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((AnonymousCell_1_t103592921 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(8 /* System.IDisposable AnonymousCell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, (AnonymousCell_1_t103592921 *)__this, (Action_1_t985559125 *)L_3, (int32_t)1);
		return L_4;
	}
}
// System.IDisposable AnonymousCell`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m1987447683_gshared (AnonymousCell_1_t103592921 * __this, Action_1_t985559125 * ___reaction0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = ___reaction0;
		int32_t L_1 = ___priority1;
		NullCheck((AnonymousCell_1_t103592921 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(7 /* System.IDisposable AnonymousCell`1<System.Object>::Bind(System.Action`1<T>,Priority) */, (AnonymousCell_1_t103592921 *)__this, (Action_1_t985559125 *)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.IDisposable AnonymousCell`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m3510407108_gshared (AnonymousCell_1_t103592921 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStoreyF9_t2554166976 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStoreyF9_t2554166976 * L_0 = (U3CListenU3Ec__AnonStoreyF9_t2554166976 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CListenU3Ec__AnonStoreyF9_t2554166976 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CListenU3Ec__AnonStoreyF9_t2554166976 *)L_0;
		U3CListenU3Ec__AnonStoreyF9_t2554166976 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3CListenU3Ec__AnonStoreyF9_t2554166976 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_1_t985559125 * L_5 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_6 = ___priority1;
		NullCheck((AnonymousCell_1_t103592921 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(7 /* System.IDisposable AnonymousCell`1<System.Object>::Bind(System.Action`1<T>,Priority) */, (AnonymousCell_1_t103592921 *)__this, (Action_1_t985559125 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable AnonymousCell`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_OnChanged_m1656078788_gshared (AnonymousCell_1_t103592921 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyFA_t1775489774 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyFA_t1775489774 * L_0 = (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		V_0 = (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 *)L_0;
		U3COnChangedU3Ec__AnonStoreyFA_t1775489774 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Func_3_t1585443616 * L_3 = (Func_3_t1585443616 *)__this->get_listen_0();
		U3COnChangedU3Ec__AnonStoreyFA_t1775489774 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_7 = ___p1;
		NullCheck((Func_3_t1585443616 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t1585443616 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t1585443616 *)L_3, (Action_1_t985559125 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_8;
	}
}
// System.Object AnonymousCell`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * AnonymousCell_1_get_valueObject_m701915439_gshared (AnonymousCell_1_t103592921 * __this, const MethodInfo* method)
{
	{
		NullCheck((AnonymousCell_1_t103592921 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* T AnonymousCell`1<System.Object>::get_value() */, (AnonymousCell_1_t103592921 *)__this);
		return L_0;
	}
}
// System.Void AnonymousStream`1/<Listen>c__AnonStorey12C<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey12C__ctor_m4165821334_gshared (U3CListenU3Ec__AnonStorey12C_t1220913119 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousStream`1/<Listen>c__AnonStorey12C<System.Object>::<>m__1C0(T)
extern "C"  void U3CListenU3Ec__AnonStorey12C_U3CU3Em__1C0_m4074916829_gshared (U3CListenU3Ec__AnonStorey12C_t1220913119 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_observer_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnonymousStream`1<System.Object>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>)
extern "C"  void AnonymousStream_1__ctor_m1486310211_gshared (AnonymousStream_1_t94244231 * __this, Func_3_t1585443616 * ___subscribe0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_3_t1585443616 * L_0 = ___subscribe0;
		__this->set_listen_0(L_0);
		return;
	}
}
// System.IDisposable AnonymousStream`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousStream_1_Listen_m191877793_gshared (AnonymousStream_1_t94244231 * __this, Action_1_t985559125 * ___observer0, int32_t ___p1, const MethodInfo* method)
{
	{
		Func_3_t1585443616 * L_0 = (Func_3_t1585443616 *)__this->get_listen_0();
		Action_1_t985559125 * L_1 = ___observer0;
		int32_t L_2 = ___p1;
		NullCheck((Func_3_t1585443616 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_3_t1585443616 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_3_t1585443616 *)L_0, (Action_1_t985559125 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_3;
	}
}
// System.IDisposable AnonymousStream`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousStream_1_Listen_m3450752486_gshared (AnonymousStream_1_t94244231 * __this, Action_t437523947 * ___observer0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey12C_t1220913119 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey12C_t1220913119 * L_0 = (U3CListenU3Ec__AnonStorey12C_t1220913119 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (U3CListenU3Ec__AnonStorey12C_t1220913119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (U3CListenU3Ec__AnonStorey12C_t1220913119 *)L_0;
		U3CListenU3Ec__AnonStorey12C_t1220913119 * L_1 = V_0;
		Action_t437523947 * L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		Func_3_t1585443616 * L_3 = (Func_3_t1585443616 *)__this->get_listen_0();
		U3CListenU3Ec__AnonStorey12C_t1220913119 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_7 = ___p1;
		NullCheck((Func_3_t1585443616 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_3_t1585443616 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_3_t1585443616 *)L_3, (Action_1_t985559125 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_8;
	}
}
// System.Void Astray`1<System.Object>::.ctor()
extern "C"  void Astray_1__ctor_m2603205864_gshared (Astray_1_t3225947498 * __this, const MethodInfo* method)
{
	{
		NullCheck((Organism_2_t3070571648 *)__this);
		((  void (*) (Organism_2_t3070571648 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Organism_2_t3070571648 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void BioCell`1/<>c__AnonStoreyF4<System.Object>::.ctor()
extern "C"  void U3CU3Ec__AnonStoreyF4__ctor_m3819804446_gshared (U3CU3Ec__AnonStoreyF4_t4282333876 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BioCell`1/<>c__AnonStoreyF4<System.Object>::<>m__163()
extern "C"  void U3CU3Ec__AnonStoreyF4_U3CU3Em__163_m3465695113_gshared (U3CU3Ec__AnonStoreyF4_t4282333876 * __this, const MethodInfo* method)
{
	{
		BioCell_1_t661504124 * L_0 = (BioCell_1_t661504124 *)__this->get_U3CU3Ef__this_1();
		NullCheck((BioCell_1_t661504124 *)L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)L_0);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_temp_0();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0031;
		}
	}
	{
		BioCell_1_t661504124 * L_3 = (BioCell_1_t661504124 *)__this->get_U3CU3Ef__this_1();
		NullCheck((BioCell_1_t661504124 *)L_3);
		VirtActionInvoker1< Il2CppObject * >::Invoke(12 /* System.Void BioCell`1<System.Object>::set_value(T) */, (BioCell_1_t661504124 *)L_3, (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
	}

IL_0031:
	{
		return;
	}
}
// System.Void BioCell`1/<Spread>c__AnonStoreyF5<System.Object>::.ctor()
extern "C"  void U3CSpreadU3Ec__AnonStoreyF5__ctor_m1461531314_gshared (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BioCell`1/<Spread>c__AnonStoreyF5<System.Object>::<>m__164()
extern "C"  void U3CSpreadU3Ec__AnonStoreyF5_U3CU3Em__164_m930881334_gshared (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * __this, const MethodInfo* method)
{
	{
		BioCell_1_t661504124 * L_0 = (BioCell_1_t661504124 *)__this->get_U3CU3Ef__this_1();
		NullCheck((BioCell_1_t661504124 *)L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)L_0);
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_temp_0();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0031;
		}
	}
	{
		BioCell_1_t661504124 * L_3 = (BioCell_1_t661504124 *)__this->get_U3CU3Ef__this_1();
		NullCheck((BioCell_1_t661504124 *)L_3);
		VirtActionInvoker1< Il2CppObject * >::Invoke(12 /* System.Void BioCell`1<System.Object>::set_value(T) */, (BioCell_1_t661504124 *)L_3, (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
	}

IL_0031:
	{
		return;
	}
}
// System.Void BioCell`1<System.Object>::.ctor()
extern "C"  void BioCell_1__ctor_m27543930_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t1019204052 *)__this);
		((  void (*) (Cell_1_t1019204052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t1019204052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void BioCell`1<System.Object>::.ctor(T)
extern "C"  void BioCell_1__ctor_m853863204_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t1019204052 *)__this);
		((  void (*) (Cell_1_t1019204052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t1019204052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ___val0;
		NullCheck((BioCell_1_t661504124 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(12 /* System.Void BioCell`1<System.Object>::set_value(T) */, (BioCell_1_t661504124 *)__this, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void BioCell`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral118;
extern const uint32_t BioCell_1__ctor_m4206272443_MetadataUsageId;
extern "C"  void BioCell_1__ctor_m4206272443_gshared (BioCell_1_t661504124 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1__ctor_m4206272443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Cell_1_t1019204052 *)__this);
		((  void (*) (Cell_1_t1019204052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t1019204052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SerializationInfo_t2995724695 * L_0 = ___info0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2995724695 *)L_0);
		Il2CppObject * L_2 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2995724695 *)L_0, (String_t*)_stringLiteral118, (Type_t *)L_1, /*hidden argument*/NULL);
		NullCheck((BioCell_1_t661504124 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(12 /* System.Void BioCell`1<System.Object>::set_value(T) */, (BioCell_1_t661504124 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))));
		return;
	}
}
// System.Void BioCell`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral118;
extern const uint32_t BioCell_1_GetObjectData_m870710296_MetadataUsageId;
extern "C"  void BioCell_1_GetObjectData_m870710296_gshared (BioCell_1_t661504124 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_GetObjectData_m870710296_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		NullCheck((Cell_1_t1019204052 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Cell_1_t1019204052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Cell_1_t1019204052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2995724695 *)L_0);
		SerializationInfo_AddValue_m3341936982((SerializationInfo_t2995724695 *)L_0, (String_t*)_stringLiteral118, (Il2CppObject *)L_1, (Type_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// IOrganism BioCell`1<System.Object>::get_carrier()
extern "C"  Il2CppObject * BioCell_1_get_carrier_m192118863_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_carrier__6();
		return L_0;
	}
}
// System.Void BioCell`1<System.Object>::set_carrier(IOrganism)
extern "C"  void BioCell_1_set_carrier_m1896678372_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_carrier__6(L_0);
		return;
	}
}
// IRoot BioCell`1<System.Object>::get_root()
extern "C"  Il2CppObject * BioCell_1_get_root_m2120150461_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_root__7();
		return L_0;
	}
}
// System.Void BioCell`1<System.Object>::set_root(IRoot)
extern "C"  void BioCell_1_set_root_m3977040414_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_root__7(L_0);
		return;
	}
}
// IOnceEmptyStream BioCell`1<System.Object>::get_fellAsleep()
extern Il2CppClass* OnceEmptyStream_t3081939404_il2cpp_TypeInfo_var;
extern const uint32_t BioCell_1_get_fellAsleep_m3267003874_MetadataUsageId;
extern "C"  Il2CppObject * BioCell_1_get_fellAsleep_m3267003874_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_get_fellAsleep_m3267003874_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OnceEmptyStream_t3081939404 * V_0 = NULL;
	OnceEmptyStream_t3081939404 * G_B2_0 = NULL;
	OnceEmptyStream_t3081939404 * G_B1_0 = NULL;
	{
		OnceEmptyStream_t3081939404 * L_0 = (OnceEmptyStream_t3081939404 *)__this->get_fellAsleep__9();
		OnceEmptyStream_t3081939404 * L_1 = (OnceEmptyStream_t3081939404 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		OnceEmptyStream_t3081939404 * L_2 = (OnceEmptyStream_t3081939404 *)il2cpp_codegen_object_new(OnceEmptyStream_t3081939404_il2cpp_TypeInfo_var);
		OnceEmptyStream__ctor_m3370738463(L_2, /*hidden argument*/NULL);
		OnceEmptyStream_t3081939404 * L_3 = (OnceEmptyStream_t3081939404 *)L_2;
		V_0 = (OnceEmptyStream_t3081939404 *)L_3;
		__this->set_fellAsleep__9(L_3);
		OnceEmptyStream_t3081939404 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void BioCell`1<System.Object>::DisposeWhenAsleep(System.IDisposable)
extern "C"  void BioCell_1_DisposeWhenAsleep_m2399898397_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___disposable0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___disposable0;
		NullCheck((BioCell_1_t661504124 *)__this);
		((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((BioCell_1_t661504124 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Boolean BioCell`1<System.Object>::get_isAlive()
extern "C"  bool BioCell_1_get_isAlive_m2614499230_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_alive_8();
		return L_0;
	}
}
// System.Void BioCell`1<System.Object>::Setup(IOrganism,IRoot)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t BioCell_1_Setup_m3936296995_MetadataUsageId;
extern "C"  void BioCell_1_Setup_m3936296995_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___c0, Il2CppObject * ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_Setup_m3936296995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___c0;
		NullCheck((BioCell_1_t661504124 *)__this);
		((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((BioCell_1_t661504124 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject * L_1 = ___r1;
		NullCheck((BioCell_1_t661504124 *)__this);
		((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((BioCell_1_t661504124 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)__this);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)__this);
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = ___c0;
		Il2CppObject * L_5 = ___r1;
		NullCheck((Il2CppObject *)(*(&V_0)));
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void IOrganism::Setup(IOrganism,IRoot) */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_0)), (Il2CppObject *)L_4, (Il2CppObject *)L_5);
	}

IL_0034:
	{
		return;
	}
}
// System.Void BioCell`1<System.Object>::WakeUp()
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t BioCell_1_WakeUp_m1546909161_MetadataUsageId;
extern "C"  void BioCell_1_WakeUp_m1546909161_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_WakeUp_m1546909161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)__this);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)__this);
		V_0 = (Il2CppObject *)L_1;
		NullCheck((Il2CppObject *)(*(&V_0)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void IOrganism::WakeUp() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_0)));
	}

IL_0024:
	{
		__this->set_wokeUp_5((bool)1);
		return;
	}
}
// System.Void BioCell`1<System.Object>::Spread()
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern const uint32_t BioCell_1_Spread_m3794005757_MetadataUsageId;
extern "C"  void BioCell_1_Spread_m3794005757_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_Spread_m3794005757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)__this);
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * L_1 = (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CSpreadU3Ec__AnonStoreyF5_t4127532072 *)L_1;
		U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_U3CU3Ef__this_1(__this);
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)__this);
		V_1 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_1)));
		InterfaceActionInvoker0::Invoke(2 /* System.Void IOrganism::Spread() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_1)));
		U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * L_4 = V_0;
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T BioCell`1<System.Object>::get_value() */, (BioCell_1_t661504124 *)__this);
		NullCheck(L_4);
		L_4->set_temp_0(L_5);
		U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject ** L_7 = (Il2CppObject **)L_6->get_address_of_temp_0();
		NullCheck((Il2CppObject *)(*L_7));
		Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* IOnceEmptyStream ILiving::get_fellAsleep() */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_7));
		U3CSpreadU3Ec__AnonStoreyF5_t4127532072 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		Il2CppObject * L_12 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11, (int32_t)1);
		NullCheck((BioCell_1_t661504124 *)__this);
		((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((BioCell_1_t661504124 *)__this, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
	}

IL_0066:
	{
		__this->set_alive_8((bool)1);
		return;
	}
}
// System.IDisposable BioCell`1<System.Object>::get_prevToSleepConnection()
extern "C"  Il2CppObject * BioCell_1_get_prevToSleepConnection_m477401155_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_prevToSleepConnection__10();
		return L_0;
	}
}
// System.Void BioCell`1<System.Object>::set_prevToSleepConnection(System.IDisposable)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t BioCell_1_set_prevToSleepConnection_m3653585240_MetadataUsageId;
extern "C"  void BioCell_1_set_prevToSleepConnection_m3653585240_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_set_prevToSleepConnection_m3653585240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_prevToSleepConnection__10();
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral96784904, /*hidden argument*/NULL);
	}

IL_001b:
	{
		Il2CppObject * L_2 = ___value0;
		__this->set_prevToSleepConnection__10(L_2);
		return;
	}
}
// T BioCell`1<System.Object>::get_value()
extern "C"  Il2CppObject * BioCell_1_get_value_m266880097_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t1019204052 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Cell_1_t1019204052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Cell_1_t1019204052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_0;
	}
}
// System.Void BioCell`1<System.Object>::set_value(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern const uint32_t BioCell_1_set_value_m2666559826_MetadataUsageId;
extern "C"  void BioCell_1_set_value_m2666559826_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_set_value_m2666559826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CU3Ec__AnonStoreyF4_t4282333876 * V_1 = NULL;
	{
		NullCheck((Cell_1_t1019204052 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Cell_1_t1019204052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Cell_1_t1019204052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Il2CppObject * L_2 = ___value0;
		bool L_3 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		Il2CppObject * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0053;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((BioCell_1_t661504124 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		if (!L_5)
		{
			goto IL_0046;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((BioCell_1_t661504124 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject *)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
		NullCheck((BioCell_1_t661504124 *)__this);
		((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((BioCell_1_t661504124 *)__this, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
	}

IL_0046:
	{
		NullCheck((Il2CppObject *)(*(&V_0)));
		InterfaceActionInvoker0::Invoke(3 /* System.Void IOrganism::ToSleep() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_0)));
	}

IL_0053:
	{
		Il2CppObject * L_7 = ___value0;
		if (!L_7)
		{
			goto IL_00ef;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(21 /* IOrganism BioCell`1<System.Object>::get_carrier() */, (BioCell_1_t661504124 *)__this);
		if (!L_8)
		{
			goto IL_0082;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_9 = VirtFuncInvoker0< Il2CppObject * >::Invoke(21 /* IOrganism BioCell`1<System.Object>::get_carrier() */, (BioCell_1_t661504124 *)__this);
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_10 = VirtFuncInvoker0< Il2CppObject * >::Invoke(22 /* IRoot BioCell`1<System.Object>::get_root() */, (BioCell_1_t661504124 *)__this);
		NullCheck((Il2CppObject *)(*(&___value0)));
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void IOrganism::Setup(IOrganism,IRoot) */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_9, (Il2CppObject *)L_10);
	}

IL_0082:
	{
		bool L_11 = (bool)__this->get_wokeUp_5();
		if (!L_11)
		{
			goto IL_009a;
		}
	}
	{
		NullCheck((Il2CppObject *)(*(&___value0)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void IOrganism::WakeUp() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___value0)));
	}

IL_009a:
	{
		bool L_12 = (bool)__this->get_alive_8();
		if (!L_12)
		{
			goto IL_00ef;
		}
	}
	{
		U3CU3Ec__AnonStoreyF4_t4282333876 * L_13 = (U3CU3Ec__AnonStoreyF4_t4282333876 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((  void (*) (U3CU3Ec__AnonStoreyF4_t4282333876 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		V_1 = (U3CU3Ec__AnonStoreyF4_t4282333876 *)L_13;
		U3CU3Ec__AnonStoreyF4_t4282333876 * L_14 = V_1;
		NullCheck(L_14);
		L_14->set_U3CU3Ef__this_1(__this);
		NullCheck((Il2CppObject *)(*(&___value0)));
		InterfaceActionInvoker0::Invoke(2 /* System.Void IOrganism::Spread() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___value0)));
		U3CU3Ec__AnonStoreyF4_t4282333876 * L_15 = V_1;
		Il2CppObject * L_16 = ___value0;
		NullCheck(L_15);
		L_15->set_temp_0(L_16);
		U3CU3Ec__AnonStoreyF4_t4282333876 * L_17 = V_1;
		NullCheck(L_17);
		Il2CppObject ** L_18 = (Il2CppObject **)L_17->get_address_of_temp_0();
		NullCheck((Il2CppObject *)(*L_18));
		Il2CppObject * L_19 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* IOnceEmptyStream ILiving::get_fellAsleep() */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_18));
		U3CU3Ec__AnonStoreyF4_t4282333876 * L_20 = V_1;
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		Action_t437523947 * L_22 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_22, (Il2CppObject *)L_20, (IntPtr_t)L_21, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_19);
		Il2CppObject * L_23 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_19, (Action_t437523947 *)L_22, (int32_t)1);
		NullCheck((BioCell_1_t661504124 *)__this);
		((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((BioCell_1_t661504124 *)__this, (Il2CppObject *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
	}

IL_00ef:
	{
		Il2CppObject * L_24 = ___value0;
		NullCheck((Cell_1_t1019204052 *)__this);
		((  void (*) (Cell_1_t1019204052 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((Cell_1_t1019204052 *)__this, (Il2CppObject *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		return;
	}
}
// T BioCell`1<System.Object>::get_innerVal()
extern "C"  Il2CppObject * BioCell_1_get_innerVal_m3760006813_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t1019204052 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Cell_1_t1019204052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Cell_1_t1019204052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_0;
	}
}
// System.Void BioCell`1<System.Object>::Collect(System.IDisposable)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t BioCell_1_Collect_m1958937368_MetadataUsageId;
extern "C"  void BioCell_1_Collect_m1958937368_gshared (BioCell_1_t661504124 * __this, Il2CppObject * ___tail0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_Collect_m1958937368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((BioCell_1_t661504124 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((BioCell_1_t661504124 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = ___tail0;
		NullCheck((Il2CppObject *)(*(&V_0)));
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_0)), (Il2CppObject *)L_2);
		goto IL_0030;
	}

IL_002a:
	{
		Il2CppObject * L_3 = ___tail0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
	}

IL_0030:
	{
		return;
	}
}
// System.Void BioCell`1<System.Object>::ToSleep()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t BioCell_1_ToSleep_m856150772_MetadataUsageId;
extern "C"  void BioCell_1_ToSleep_m856150772_gshared (BioCell_1_t661504124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCell_1_ToSleep_m856150772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((BioCell_1_t661504124 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((BioCell_1_t661504124 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		NullCheck((BioCell_1_t661504124 *)__this);
		((  void (*) (BioCell_1_t661504124 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((BioCell_1_t661504124 *)__this, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
	}

IL_001d:
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((BioCell_1_t661504124 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		NullCheck((BioCell_1_t661504124 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (BioCell_1_t661504124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((BioCell_1_t661504124 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		InterfaceActionInvoker0::Invoke(3 /* System.Void IOrganism::ToSleep() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_0)));
	}

IL_0041:
	{
		__this->set_alive_8((bool)0);
		__this->set_wokeUp_5((bool)0);
		OnceEmptyStream_t3081939404 * L_4 = (OnceEmptyStream_t3081939404 *)__this->get_fellAsleep__9();
		if (!L_4)
		{
			goto IL_0065;
		}
	}
	{
		OnceEmptyStream_t3081939404 * L_5 = (OnceEmptyStream_t3081939404 *)__this->get_fellAsleep__9();
		NullCheck((EmptyStream_t2850978573 *)L_5);
		EmptyStream_Send_m2813379598((EmptyStream_t2850978573 *)L_5, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void BioCollection`1/<Implant>c__AnonStoreyF8<System.Object>::.ctor()
extern "C"  void U3CImplantU3Ec__AnonStoreyF8__ctor_m2349960973_gshared (U3CImplantU3Ec__AnonStoreyF8_t1491113711 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BioCollection`1/<Implant>c__AnonStoreyF8<System.Object>::<>m__16C()
extern "C"  void U3CImplantU3Ec__AnonStoreyF8_U3CU3Em__16C_m2550389066_gshared (U3CImplantU3Ec__AnonStoreyF8_t1491113711 * __this, const MethodInfo* method)
{
	{
		BioCollection_1_t694114648 * L_0 = (BioCollection_1_t694114648 *)__this->get_U3CU3Ef__this_1();
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_orphan_0();
		NullCheck((Collection_1_t2806094150 *)L_0);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(!0) */, (Collection_1_t2806094150 *)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void BioCollection`1/<InsertItem>c__AnonStoreyF7<System.Object>::.ctor()
extern "C"  void U3CInsertItemU3Ec__AnonStoreyF7__ctor_m1955124201_gshared (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BioCollection`1/<InsertItem>c__AnonStoreyF7<System.Object>::<>m__16B()
extern "C"  void U3CInsertItemU3Ec__AnonStoreyF7_U3CU3Em__16B_m3883537197_gshared (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * __this, const MethodInfo* method)
{
	{
		BioCollection_1_t694114648 * L_0 = (BioCollection_1_t694114648 *)__this->get_U3CU3Ef__this_1();
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_item_0();
		NullCheck((Collection_1_t2806094150 *)L_0);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(!0) */, (Collection_1_t2806094150 *)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void BioCollection`1/<Spread>c__AnonStoreyF6<System.Object>::.ctor()
extern "C"  void U3CSpreadU3Ec__AnonStoreyF6__ctor_m872089071_gshared (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BioCollection`1/<Spread>c__AnonStoreyF6<System.Object>::<>m__16E()
extern "C"  void U3CSpreadU3Ec__AnonStoreyF6_U3CU3Em__16E_m2978309802_gshared (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * __this, const MethodInfo* method)
{
	{
		BioCollection_1_t694114648 * L_0 = (BioCollection_1_t694114648 *)__this->get_U3CU3Ef__this_1();
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_child_0();
		NullCheck((Collection_1_t2806094150 *)L_0);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(!0) */, (Collection_1_t2806094150 *)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::.ctor()
extern "C"  void BioCollection_1__ctor_m2236466102_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stream_1_t4289044124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_updates_15(L_0);
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Collection_1_t2806094150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t BioCollection_1__ctor_m2133799689_MetadataUsageId;
extern "C"  void BioCollection_1__ctor_m2133799689_gshared (BioCollection_1_t694114648 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1__ctor_m2133799689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stream_1_t4289044124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_updates_15(L_0);
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Collection_1_t2806094150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_1 = ___collection0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Il2CppObject* L_3 = ___collection0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3);
		V_1 = (Il2CppObject*)L_4;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003c;
		}

IL_002e:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5);
			V_0 = (Il2CppObject *)L_6;
			Il2CppObject * L_7 = V_0;
			NullCheck((Collection_1_t2806094150 *)__this);
			VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(!0) */, (Collection_1_t2806094150 *)__this, (Il2CppObject *)L_7);
		}

IL_003c:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_002e;
			}
		}

IL_0047:
		{
			IL2CPP_LEAVE(0x57, FINALLY_004c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004c;
	}

FINALLY_004c:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			if (L_10)
			{
				goto IL_0050;
			}
		}

IL_004f:
		{
			IL2CPP_END_FINALLY(76)
		}

IL_0050:
		{
			Il2CppObject* L_11 = V_1;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(76)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(76)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0057:
	{
		return;
	}
}
// System.Void BioCollection`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void BioCollection_1__ctor_m2006345998_gshared (BioCollection_1_t694114648 * __this, List_1_t1634065389 * ___list0, const MethodInfo* method)
{
	BioCollection_1_t694114648 * G_B2_0 = NULL;
	BioCollection_1_t694114648 * G_B1_0 = NULL;
	List_1_t1634065389 * G_B3_0 = NULL;
	BioCollection_1_t694114648 * G_B3_1 = NULL;
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stream_1_t4289044124 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_updates_15(L_0);
		List_1_t1634065389 * L_1 = ___list0;
		G_B1_0 = ((BioCollection_1_t694114648 *)(__this));
		if (!L_1)
		{
			G_B2_0 = ((BioCollection_1_t694114648 *)(__this));
			goto IL_001d;
		}
	}
	{
		List_1_t1634065389 * L_2 = ___list0;
		List_1_t1634065389 * L_3 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		G_B3_0 = L_3;
		G_B3_1 = ((BioCollection_1_t694114648 *)(G_B1_0));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = ((List_1_t1634065389 *)(NULL));
		G_B3_1 = ((BioCollection_1_t694114648 *)(G_B2_0));
	}

IL_001e:
	{
		NullCheck((Collection_1_t2806094150 *)G_B3_1);
		((  void (*) (Collection_1_t2806094150 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Collection_1_t2806094150 *)G_B3_1, (Il2CppObject*)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// IProcess BioCollection`1<System.Object>::Start(System.Collections.IEnumerator)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3507897986;
extern const uint32_t BioCollection_1_Start_m277942794_MetadataUsageId;
extern "C"  Il2CppObject * BioCollection_1_Start_m277942794_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___bearutine0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_Start_m277942794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, (String_t*)_stringLiteral3507897986, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// IOnceEmptyStream BioCollection`1<System.Object>::get_fellAsleep()
extern Il2CppClass* OnceEmptyStream_t3081939404_il2cpp_TypeInfo_var;
extern const uint32_t BioCollection_1_get_fellAsleep_m825523110_MetadataUsageId;
extern "C"  Il2CppObject * BioCollection_1_get_fellAsleep_m825523110_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_get_fellAsleep_m825523110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OnceEmptyStream_t3081939404 * V_0 = NULL;
	OnceEmptyStream_t3081939404 * G_B2_0 = NULL;
	OnceEmptyStream_t3081939404 * G_B1_0 = NULL;
	{
		OnceEmptyStream_t3081939404 * L_0 = (OnceEmptyStream_t3081939404 *)__this->get_fellAsleep__5();
		OnceEmptyStream_t3081939404 * L_1 = (OnceEmptyStream_t3081939404 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		OnceEmptyStream_t3081939404 * L_2 = (OnceEmptyStream_t3081939404 *)il2cpp_codegen_object_new(OnceEmptyStream_t3081939404_il2cpp_TypeInfo_var);
		OnceEmptyStream__ctor_m3370738463(L_2, /*hidden argument*/NULL);
		OnceEmptyStream_t3081939404 * L_3 = (OnceEmptyStream_t3081939404 *)L_2;
		V_0 = (OnceEmptyStream_t3081939404 *)L_3;
		__this->set_fellAsleep__5(L_3);
		OnceEmptyStream_t3081939404 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void BioCollection`1<System.Object>::DisposeWhenAsleep(System.IDisposable)
extern "C"  void BioCollection_1_DisposeWhenAsleep_m3673322585_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___disposable0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___disposable0;
		NullCheck((BioCollection_1_t694114648 *)__this);
		((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((BioCollection_1_t694114648 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Boolean BioCollection`1<System.Object>::get_isAlive()
extern "C"  bool BioCollection_1_get_isAlive_m846113754_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_alive_4();
		return L_0;
	}
}
// IOrganism BioCollection`1<System.Object>::get_carrier()
extern "C"  Il2CppObject * BioCollection_1_get_carrier_m256481163_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_carrier__6();
		return L_0;
	}
}
// System.Void BioCollection`1<System.Object>::set_carrier(IOrganism)
extern "C"  void BioCollection_1_set_carrier_m1747995176_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_carrier__6(L_0);
		return;
	}
}
// IRoot BioCollection`1<System.Object>::get_root()
extern "C"  Il2CppObject * BioCollection_1_get_root_m4145318913_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_root__7();
		return L_0;
	}
}
// System.Void BioCollection`1<System.Object>::set_root(IRoot)
extern "C"  void BioCollection_1_set_root_m346443866_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_root__7(L_0);
		return;
	}
}
// IOrganism BioCollection`1<System.Object>::Carrier()
extern "C"  Il2CppObject * BioCollection_1_Carrier_m738768532_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	{
		NullCheck((BioCollection_1_t694114648 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(46 /* IOrganism BioCollection`1<System.Object>::get_carrier() */, (BioCollection_1_t694114648 *)__this);
		return L_0;
	}
}
// System.Void BioCollection`1<System.Object>::Setup(IOrganism,IRoot)
extern "C"  void BioCollection_1_Setup_m3787613799_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___c0, Il2CppObject * ___r1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___c0;
		NullCheck((BioCollection_1_t694114648 *)__this);
		((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((BioCollection_1_t694114648 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Il2CppObject * L_1 = ___r1;
		NullCheck((BioCollection_1_t694114648 *)__this);
		((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((BioCollection_1_t694114648 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		Action_1_t985559125 * L_3 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_3, (Il2CppObject *)__this, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck((BioCollection_1_t694114648 *)__this);
		((  void (*) (BioCollection_1_t694114648 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((BioCollection_1_t694114648 *)__this, (Action_1_t985559125 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::WakeUp()
extern "C"  void BioCollection_1_WakeUp_m1304019757_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	BioCollection_1_t694114648 * G_B2_0 = NULL;
	BioCollection_1_t694114648 * G_B1_0 = NULL;
	{
		Action_1_t985559125 * L_0 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cacheE_16();
		G_B1_0 = ((BioCollection_1_t694114648 *)(__this));
		if (L_0)
		{
			G_B2_0 = ((BioCollection_1_t694114648 *)(__this));
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->set_U3CU3Ef__amU24cacheE_16(L_2);
		G_B2_0 = ((BioCollection_1_t694114648 *)(G_B1_0));
	}

IL_0019:
	{
		Action_1_t985559125 * L_3 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cacheE_16();
		NullCheck((BioCollection_1_t694114648 *)G_B2_0);
		((  void (*) (BioCollection_1_t694114648 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((BioCollection_1_t694114648 *)G_B2_0, (Action_1_t985559125 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_wokeUp_3((bool)1);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::Spread()
extern "C"  void BioCollection_1_Spread_m3551116353_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_1, (Il2CppObject *)__this, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		NullCheck((BioCollection_1_t694114648 *)__this);
		((  void (*) (BioCollection_1_t694114648 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((BioCollection_1_t694114648 *)__this, (Action_1_t985559125 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_alive_4((bool)1);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::ToSleep()
extern Il2CppClass* Action_1_t1777374079_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1327751148_MethodInfo_var;
extern const MethodInfo* List_1_ForEach_m3025640294_MethodInfo_var;
extern const uint32_t BioCollection_1_ToSleep_m1916513840_MetadataUsageId;
extern "C"  void BioCollection_1_ToSleep_m1916513840_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_ToSleep_m1916513840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2425880343 * G_B3_0 = NULL;
	List_1_t2425880343 * G_B2_0 = NULL;
	BioCollection_1_t694114648 * G_B6_0 = NULL;
	BioCollection_1_t694114648 * G_B5_0 = NULL;
	{
		List_1_t2425880343 * L_0 = (List_1_t2425880343 *)__this->get_tails_2();
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		List_1_t2425880343 * L_1 = (List_1_t2425880343 *)__this->get_tails_2();
		Action_1_t1777374079 * L_2 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cacheF_17();
		G_B2_0 = L_1;
		if (L_2)
		{
			G_B3_0 = L_1;
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		Action_1_t1777374079 * L_4 = (Action_1_t1777374079 *)il2cpp_codegen_object_new(Action_1_t1777374079_il2cpp_TypeInfo_var);
		Action_1__ctor_m1327751148(L_4, (Il2CppObject *)NULL, (IntPtr_t)L_3, /*hidden argument*/Action_1__ctor_m1327751148_MethodInfo_var);
		((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->set_U3CU3Ef__amU24cacheF_17(L_4);
		G_B3_0 = G_B2_0;
	}

IL_0029:
	{
		Action_1_t1777374079 * L_5 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cacheF_17();
		NullCheck((List_1_t2425880343 *)G_B3_0);
		List_1_ForEach_m3025640294((List_1_t2425880343 *)G_B3_0, (Action_1_t1777374079 *)L_5, /*hidden argument*/List_1_ForEach_m3025640294_MethodInfo_var);
		List_1_t2425880343 * L_6 = (List_1_t2425880343 *)__this->get_tails_2();
		NullCheck((List_1_t2425880343 *)L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Clear() */, (List_1_t2425880343 *)L_6);
	}

IL_003e:
	{
		Action_1_t985559125 * L_7 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cache10_18();
		G_B5_0 = ((BioCollection_1_t694114648 *)(__this));
		if (L_7)
		{
			G_B6_0 = ((BioCollection_1_t694114648 *)(__this));
			goto IL_0057;
		}
	}
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_9, (Il2CppObject *)NULL, (IntPtr_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->set_U3CU3Ef__amU24cache10_18(L_9);
		G_B6_0 = ((BioCollection_1_t694114648 *)(G_B5_0));
	}

IL_0057:
	{
		Action_1_t985559125 * L_10 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cache10_18();
		NullCheck((BioCollection_1_t694114648 *)G_B6_0);
		((  void (*) (BioCollection_1_t694114648 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((BioCollection_1_t694114648 *)G_B6_0, (Action_1_t985559125 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_wokeUp_3((bool)0);
		__this->set_alive_4((bool)0);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::ForEach(System.Action`1<T>)
extern "C"  void BioCollection_1_ForEach_m3261191230_gshared (BioCollection_1_t694114648 * __this, Action_1_t985559125 * ___action0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0018;
	}

IL_0007:
	{
		Action_1_t985559125 * L_0 = ___action0;
		int32_t L_1 = V_0;
		NullCheck((Collection_1_t2806094150 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_1);
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		int32_t L_3 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0018:
	{
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count() */, (Collection_1_t2806094150 *)__this);
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void BioCollection`1<System.Object>::Collect(System.IDisposable)
extern Il2CppClass* List_1_t2425880343_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m215590625_MethodInfo_var;
extern const uint32_t BioCollection_1_Collect_m1657079636_MetadataUsageId;
extern "C"  void BioCollection_1_Collect_m1657079636_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___tail0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_Collect_m1657079636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2425880343 * L_0 = (List_1_t2425880343 *)__this->get_tails_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t2425880343 * L_1 = (List_1_t2425880343 *)il2cpp_codegen_object_new(List_1_t2425880343_il2cpp_TypeInfo_var);
		List_1__ctor_m215590625(L_1, /*hidden argument*/List_1__ctor_m215590625_MethodInfo_var);
		__this->set_tails_2(L_1);
	}

IL_0016:
	{
		List_1_t2425880343 * L_2 = (List_1_t2425880343 *)__this->get_tails_2();
		Il2CppObject * L_3 = ___tail0;
		NullCheck((List_1_t2425880343 *)L_2);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Add(!0) */, (List_1_t2425880343 *)L_2, (Il2CppObject *)L_3);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::ClearItems()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t511663335_il2cpp_TypeInfo_var;
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1613394326_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m443741914_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1919929410_MethodInfo_var;
extern const MethodInfo* Stream_1_Send_m1361871350_MethodInfo_var;
extern const MethodInfo* Stream_1_Send_m1041823273_MethodInfo_var;
extern const uint32_t BioCollection_1_ClearItems_m2204296641_MetadataUsageId;
extern "C"  void BioCollection_1_ClearItems_m2204296641_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_ClearItems_m2204296641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Enumerator_t511663335  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	BioCollection_1_t694114648 * G_B10_0 = NULL;
	BioCollection_1_t694114648 * G_B9_0 = NULL;
	{
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count() */, (Collection_1_t2806094150 *)__this);
		V_0 = (int32_t)L_0;
		List_1_t2425880343 * L_1 = (List_1_t2425880343 *)__this->get_tails_2();
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		List_1_t2425880343 * L_2 = (List_1_t2425880343 *)__this->get_tails_2();
		NullCheck((List_1_t2425880343 *)L_2);
		Enumerator_t511663335  L_3 = List_1_GetEnumerator_m1613394326((List_1_t2425880343 *)L_2, /*hidden argument*/List_1_GetEnumerator_m1613394326_MethodInfo_var);
		V_2 = (Enumerator_t511663335 )L_3;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_0023:
		{
			Il2CppObject * L_4 = Enumerator_get_Current_m443741914((Enumerator_t511663335 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m443741914_MethodInfo_var);
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
		}

IL_0031:
		{
			bool L_6 = Enumerator_MoveNext_m1919929410((Enumerator_t511663335 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m1919929410_MethodInfo_var);
			if (L_6)
			{
				goto IL_0023;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		Enumerator_t511663335  L_7 = V_2;
		Enumerator_t511663335  L_8 = L_7;
		Il2CppObject * L_9 = Box(Enumerator_t511663335_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(66)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004e:
	{
		List_1_t2425880343 * L_10 = (List_1_t2425880343 *)__this->get_tails_2();
		NullCheck((List_1_t2425880343 *)L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Clear() */, (List_1_t2425880343 *)L_10);
	}

IL_0059:
	{
		Action_1_t985559125 * L_11 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cache11_19();
		G_B9_0 = ((BioCollection_1_t694114648 *)(__this));
		if (L_11)
		{
			G_B10_0 = ((BioCollection_1_t694114648 *)(__this));
			goto IL_0072;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		Action_1_t985559125 * L_13 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_13, (Il2CppObject *)NULL, (IntPtr_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->set_U3CU3Ef__amU24cache11_19(L_13);
		G_B10_0 = ((BioCollection_1_t694114648 *)(G_B9_0));
	}

IL_0072:
	{
		Action_1_t985559125 * L_14 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cache11_19();
		NullCheck((BioCollection_1_t694114648 *)G_B10_0);
		((  void (*) (BioCollection_1_t694114648 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((BioCollection_1_t694114648 *)G_B10_0, (Action_1_t985559125 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)((Collection_1_t2806094150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		Stream_1_t1249425060 * L_15 = (Stream_1_t1249425060 *)__this->get_collectionReset_10();
		if (!L_15)
		{
			goto IL_009d;
		}
	}
	{
		Stream_1_t1249425060 * L_16 = (Stream_1_t1249425060 *)__this->get_collectionReset_10();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_17 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Stream_1_t1249425060 *)L_16);
		Stream_1_Send_m1361871350((Stream_1_t1249425060 *)L_16, (Unit_t2558286038 )L_17, /*hidden argument*/Stream_1_Send_m1361871350_MethodInfo_var);
	}

IL_009d:
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00bb;
		}
	}
	{
		Stream_1_t1538553809 * L_19 = (Stream_1_t1538553809 *)__this->get_countChanged_9();
		if (!L_19)
		{
			goto IL_00bb;
		}
	}
	{
		Stream_1_t1538553809 * L_20 = (Stream_1_t1538553809 *)__this->get_countChanged_9();
		NullCheck((Stream_1_t1538553809 *)L_20);
		Stream_1_Send_m1041823273((Stream_1_t1538553809 *)L_20, (int32_t)0, /*hidden argument*/Stream_1_Send_m1041823273_MethodInfo_var);
	}

IL_00bb:
	{
		Stream_1_t4289044124 * L_21 = (Stream_1_t4289044124 *)__this->get_updates_15();
		NullCheck((Stream_1_t4289044124 *)L_21);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((Stream_1_t4289044124 *)L_21, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::InsertItem(System.Int32,T)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern const MethodInfo* Stream_1_Send_m1041823273_MethodInfo_var;
extern const uint32_t BioCollection_1_InsertItem_m3700911267_MetadataUsageId;
extern "C"  void BioCollection_1_InsertItem_m3700911267_gshared (BioCollection_1_t694114648 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_InsertItem_m3700911267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * V_0 = NULL;
	{
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_0 = (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		((  void (*) (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		V_0 = (U3CInsertItemU3Ec__AnonStoreyF7_t871750507 *)L_0;
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_1 = V_0;
		Il2CppObject * L_2 = ___item1;
		NullCheck(L_1);
		L_1->set_item_0(L_2);
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		int32_t L_4 = ___index0;
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_item_0();
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_4, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_transplantTarget_8();
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_8 = V_0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_item_0();
		bool L_10 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_7, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0042:
	{
		NullCheck((BioCollection_1_t694114648 *)__this);
		Il2CppObject * L_11 = VirtFuncInvoker0< Il2CppObject * >::Invoke(46 /* IOrganism BioCollection`1<System.Object>::get_carrier() */, (BioCollection_1_t694114648 *)__this);
		if (!L_11)
		{
			goto IL_006a;
		}
	}
	{
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_12 = V_0;
		NullCheck(L_12);
		Il2CppObject ** L_13 = (Il2CppObject **)L_12->get_address_of_item_0();
		NullCheck((BioCollection_1_t694114648 *)__this);
		Il2CppObject * L_14 = VirtFuncInvoker0< Il2CppObject * >::Invoke(46 /* IOrganism BioCollection`1<System.Object>::get_carrier() */, (BioCollection_1_t694114648 *)__this);
		NullCheck((BioCollection_1_t694114648 *)__this);
		Il2CppObject * L_15 = VirtFuncInvoker0< Il2CppObject * >::Invoke(47 /* IRoot BioCollection`1<System.Object>::get_root() */, (BioCollection_1_t694114648 *)__this);
		NullCheck((Il2CppObject *)(*L_13));
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void IOrganism::Setup(IOrganism,IRoot) */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_13), (Il2CppObject *)L_14, (Il2CppObject *)L_15);
	}

IL_006a:
	{
		bool L_16 = (bool)__this->get_wokeUp_3();
		if (!L_16)
		{
			goto IL_0086;
		}
	}
	{
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_17 = V_0;
		NullCheck(L_17);
		Il2CppObject ** L_18 = (Il2CppObject **)L_17->get_address_of_item_0();
		NullCheck((Il2CppObject *)(*L_18));
		InterfaceActionInvoker0::Invoke(1 /* System.Void IOrganism::WakeUp() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_18));
	}

IL_0086:
	{
		bool L_19 = (bool)__this->get_alive_4();
		if (!L_19)
		{
			goto IL_00cb;
		}
	}
	{
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_20 = V_0;
		NullCheck(L_20);
		Il2CppObject ** L_21 = (Il2CppObject **)L_20->get_address_of_item_0();
		NullCheck((Il2CppObject *)(*L_21));
		InterfaceActionInvoker0::Invoke(2 /* System.Void IOrganism::Spread() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_21));
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_22 = V_0;
		NullCheck(L_22);
		Il2CppObject ** L_23 = (Il2CppObject **)L_22->get_address_of_item_0();
		NullCheck((Il2CppObject *)(*L_23));
		Il2CppObject * L_24 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* IOnceEmptyStream ILiving::get_fellAsleep() */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_23));
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_25 = V_0;
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		Action_t437523947 * L_27 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_27, (Il2CppObject *)L_25, (IntPtr_t)L_26, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_24);
		Il2CppObject * L_28 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_24, (Action_t437523947 *)L_27, (int32_t)1);
		NullCheck((BioCollection_1_t694114648 *)__this);
		((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((BioCollection_1_t694114648 *)__this, (Il2CppObject *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_00cb:
	{
		Stream_1_t1107661009 * L_29 = (Stream_1_t1107661009 *)__this->get_collectionAdd_11();
		if (!L_29)
		{
			goto IL_00ed;
		}
	}
	{
		Stream_1_t1107661009 * L_30 = (Stream_1_t1107661009 *)__this->get_collectionAdd_11();
		int32_t L_31 = ___index0;
		U3CInsertItemU3Ec__AnonStoreyF7_t871750507 * L_32 = V_0;
		NullCheck(L_32);
		Il2CppObject * L_33 = (Il2CppObject *)L_32->get_item_0();
		CollectionAddEvent_1_t2416521987  L_34;
		memset(&L_34, 0, sizeof(L_34));
		((  void (*) (CollectionAddEvent_1_t2416521987 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)(&L_34, (int32_t)L_31, (Il2CppObject *)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		NullCheck((Stream_1_t1107661009 *)L_30);
		((  void (*) (Stream_1_t1107661009 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37)->method)((Stream_1_t1107661009 *)L_30, (CollectionAddEvent_1_t2416521987 )L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37));
	}

IL_00ed:
	{
		Stream_1_t1538553809 * L_35 = (Stream_1_t1538553809 *)__this->get_countChanged_9();
		if (!L_35)
		{
			goto IL_0109;
		}
	}
	{
		Stream_1_t1538553809 * L_36 = (Stream_1_t1538553809 *)__this->get_countChanged_9();
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_37 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count() */, (Collection_1_t2806094150 *)__this);
		NullCheck((Stream_1_t1538553809 *)L_36);
		Stream_1_Send_m1041823273((Stream_1_t1538553809 *)L_36, (int32_t)L_37, /*hidden argument*/Stream_1_Send_m1041823273_MethodInfo_var);
	}

IL_0109:
	{
		Stream_1_t4289044124 * L_38 = (Stream_1_t4289044124 *)__this->get_updates_15();
		NullCheck((Stream_1_t4289044124 *)L_38);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((Stream_1_t4289044124 *)L_38, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::Move(System.Int32,System.Int32)
extern "C"  void BioCollection_1_Move_m522700199_gshared (BioCollection_1_t694114648 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___oldIndex0;
		int32_t L_1 = ___newIndex1;
		NullCheck((BioCollection_1_t694114648 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(60 /* System.Void BioCollection`1<System.Object>::MoveItem(System.Int32,System.Int32) */, (BioCollection_1_t694114648 *)__this, (int32_t)L_0, (int32_t)L_1);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::Implant(IOrganism,T)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern const uint32_t BioCollection_1_Implant_m2746653748_MetadataUsageId;
extern "C"  void BioCollection_1_Implant_m2746653748_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___orphan1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_Implant_m2746653748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CImplantU3Ec__AnonStoreyF8_t1491113711 * V_0 = NULL;
	{
		U3CImplantU3Ec__AnonStoreyF8_t1491113711 * L_0 = (U3CImplantU3Ec__AnonStoreyF8_t1491113711 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		((  void (*) (U3CImplantU3Ec__AnonStoreyF8_t1491113711 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40));
		V_0 = (U3CImplantU3Ec__AnonStoreyF8_t1491113711 *)L_0;
		U3CImplantU3Ec__AnonStoreyF8_t1491113711 * L_1 = V_0;
		Il2CppObject * L_2 = ___orphan1;
		NullCheck(L_1);
		L_1->set_orphan_0(L_2);
		U3CImplantU3Ec__AnonStoreyF8_t1491113711 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		U3CImplantU3Ec__AnonStoreyF8_t1491113711 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_orphan_0();
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(!0) */, (Collection_1_t2806094150 *)__this, (Il2CppObject *)L_5);
		Il2CppObject * L_6 = ___intruder0;
		NullCheck((Il2CppObject *)L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* IOnceEmptyStream ILiving::get_fellAsleep() */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
		U3CImplantU3Ec__AnonStoreyF8_t1491113711 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		Action_t437523947 * L_10 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_10, (Il2CppObject *)L_8, (IntPtr_t)L_9, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_7);
		InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_7, (Action_t437523947 *)L_10, (int32_t)1);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::TransplantOrganTo(T,BioCollection`1<T>)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2052012615;
extern Il2CppCodeGenString* _stringLiteral3099601904;
extern const uint32_t BioCollection_1_TransplantOrganTo_m1526006390_MetadataUsageId;
extern "C"  void BioCollection_1_TransplantOrganTo_m1526006390_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___organ0, BioCollection_1_t694114648 * ___anotherBody1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_TransplantOrganTo_m1526006390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BioCollection_1_t694114648 * L_0 = ___anotherBody1;
		NullCheck((BioCollection_1_t694114648 *)L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(46 /* IOrganism BioCollection`1<System.Object>::get_carrier() */, (BioCollection_1_t694114648 *)L_0);
		NullCheck((BioCollection_1_t694114648 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(46 /* IOrganism BioCollection`1<System.Object>::get_carrier() */, (BioCollection_1_t694114648 *)__this);
		if ((((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2052012615, /*hidden argument*/NULL);
	}

IL_001b:
	{
		Il2CppObject * L_3 = ___organ0;
		__this->set_transplantTarget_8(L_3);
		Il2CppObject * L_4 = ___organ0;
		NullCheck((Collection_1_t2806094150 *)__this);
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(!0) */, (Collection_1_t2806094150 *)__this, (Il2CppObject *)L_4);
		if (L_5)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral3099601904, /*hidden argument*/NULL);
		__this->set_transplantTarget_8(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32))));
		return;
	}

IL_0045:
	{
		BioCollection_1_t694114648 * L_6 = ___anotherBody1;
		Il2CppObject * L_7 = ___organ0;
		NullCheck(L_6);
		L_6->set_transplantTarget_8(L_7);
		BioCollection_1_t694114648 * L_8 = ___anotherBody1;
		Il2CppObject * L_9 = ___organ0;
		NullCheck((Collection_1_t2806094150 *)L_8);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(!0) */, (Collection_1_t2806094150 *)L_8, (Il2CppObject *)L_9);
		BioCollection_1_t694114648 * L_10 = ___anotherBody1;
		NullCheck(L_10);
		L_10->set_transplantTarget_8(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32))));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::MoveItem(System.Int32,System.Int32)
extern "C"  void BioCollection_1_MoveItem_m869199252_gshared (BioCollection_1_t694114648 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___oldIndex0;
		NullCheck((Collection_1_t2806094150 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0);
		V_0 = (Il2CppObject *)L_1;
		int32_t L_2 = ___oldIndex0;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43));
		int32_t L_3 = ___newIndex1;
		Il2CppObject * L_4 = V_0;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		Stream_1_t2263194403 * L_5 = (Stream_1_t2263194403 *)__this->get_collectionMove_12();
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Stream_1_t2263194403 * L_6 = (Stream_1_t2263194403 *)__this->get_collectionMove_12();
		int32_t L_7 = ___oldIndex0;
		int32_t L_8 = ___newIndex1;
		Il2CppObject * L_9 = V_0;
		CollectionMoveEvent_1_t3572055381 * L_10 = (CollectionMoveEvent_1_t3572055381 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44));
		((  void (*) (CollectionMoveEvent_1_t3572055381 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)(L_10, (int32_t)L_7, (int32_t)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
		NullCheck((Stream_1_t2263194403 *)L_6);
		((  void (*) (Stream_1_t2263194403 *, CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46)->method)((Stream_1_t2263194403 *)L_6, (CollectionMoveEvent_1_t3572055381 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46));
	}

IL_0035:
	{
		Stream_1_t4289044124 * L_11 = (Stream_1_t4289044124 *)__this->get_updates_15();
		NullCheck((Stream_1_t4289044124 *)L_11);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((Stream_1_t4289044124 *)L_11, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::RemoveItem(System.Int32)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const MethodInfo* Stream_1_Send_m1041823273_MethodInfo_var;
extern const uint32_t BioCollection_1_RemoveItem_m3904509654_MetadataUsageId;
extern "C"  void BioCollection_1_RemoveItem_m3904509654_gshared (BioCollection_1_t694114648 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_RemoveItem_m3904509654_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2806094150 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0);
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_transplantTarget_8();
		Il2CppObject * L_3 = V_0;
		bool L_4 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0030;
		}
	}
	{
		NullCheck((Il2CppObject *)(*(&V_0)));
		InterfaceActionInvoker0::Invoke(3 /* System.Void IOrganism::ToSleep() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_0)));
	}

IL_0030:
	{
		int32_t L_5 = ___index0;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43));
		Stream_1_t3884365576 * L_6 = (Stream_1_t3884365576 *)__this->get_collectionRemove_13();
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		Stream_1_t3884365576 * L_7 = (Stream_1_t3884365576 *)__this->get_collectionRemove_13();
		int32_t L_8 = ___index0;
		Il2CppObject * L_9 = V_0;
		CollectionRemoveEvent_1_t898259258 * L_10 = (CollectionRemoveEvent_1_t898259258 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		((  void (*) (CollectionRemoveEvent_1_t898259258 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48)->method)(L_10, (int32_t)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48));
		NullCheck((Stream_1_t3884365576 *)L_7);
		((  void (*) (Stream_1_t3884365576 *, CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49)->method)((Stream_1_t3884365576 *)L_7, (CollectionRemoveEvent_1_t898259258 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49));
	}

IL_0054:
	{
		Stream_1_t1538553809 * L_11 = (Stream_1_t1538553809 *)__this->get_countChanged_9();
		if (!L_11)
		{
			goto IL_0070;
		}
	}
	{
		Stream_1_t1538553809 * L_12 = (Stream_1_t1538553809 *)__this->get_countChanged_9();
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count() */, (Collection_1_t2806094150 *)__this);
		NullCheck((Stream_1_t1538553809 *)L_12);
		Stream_1_Send_m1041823273((Stream_1_t1538553809 *)L_12, (int32_t)L_13, /*hidden argument*/Stream_1_Send_m1041823273_MethodInfo_var);
	}

IL_0070:
	{
		Stream_1_t4289044124 * L_14 = (Stream_1_t4289044124 *)__this->get_updates_15();
		NullCheck((Stream_1_t4289044124 *)L_14);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((Stream_1_t4289044124 *)L_14, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void BioCollection_1_SetItem_m2557962322_gshared (BioCollection_1_t694114648 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2806094150 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0);
		V_0 = (Il2CppObject *)L_1;
		int32_t L_2 = ___index0;
		Il2CppObject * L_3 = ___item1;
		NullCheck((Collection_1_t2806094150 *)__this);
		((  void (*) (Collection_1_t2806094150 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50)->method)((Collection_1_t2806094150 *)__this, (int32_t)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50));
		Stream_1_t1188511092 * L_4 = (Stream_1_t1188511092 *)__this->get_collectionReplace_14();
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		Stream_1_t1188511092 * L_5 = (Stream_1_t1188511092 *)__this->get_collectionReplace_14();
		int32_t L_6 = ___index0;
		Il2CppObject * L_7 = V_0;
		Il2CppObject * L_8 = ___item1;
		CollectionReplaceEvent_1_t2497372070 * L_9 = (CollectionReplaceEvent_1_t2497372070 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 51));
		((  void (*) (CollectionReplaceEvent_1_t2497372070 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52)->method)(L_9, (int32_t)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52));
		NullCheck((Stream_1_t1188511092 *)L_5);
		((  void (*) (Stream_1_t1188511092 *, CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 53)->method)((Stream_1_t1188511092 *)L_5, (CollectionReplaceEvent_1_t2497372070 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 53));
	}

IL_002e:
	{
		Stream_1_t4289044124 * L_10 = (Stream_1_t4289044124 *)__this->get_updates_15();
		NullCheck((Stream_1_t4289044124 *)L_10);
		((  void (*) (Stream_1_t4289044124 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)((Stream_1_t4289044124 *)L_10, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		return;
	}
}
// IStream`1<System.Int32> BioCollection`1<System.Object>::ObserveCountChanged()
extern Il2CppClass* Stream_1_t1538553809_il2cpp_TypeInfo_var;
extern const MethodInfo* Stream_1__ctor_m2512605719_MethodInfo_var;
extern const uint32_t BioCollection_1_ObserveCountChanged_m3434133199_MetadataUsageId;
extern "C"  Il2CppObject* BioCollection_1_ObserveCountChanged_m3434133199_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_ObserveCountChanged_m3434133199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Stream_1_t1538553809 * V_0 = NULL;
	Stream_1_t1538553809 * G_B2_0 = NULL;
	Stream_1_t1538553809 * G_B1_0 = NULL;
	{
		Stream_1_t1538553809 * L_0 = (Stream_1_t1538553809 *)__this->get_countChanged_9();
		Stream_1_t1538553809 * L_1 = (Stream_1_t1538553809 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1538553809 * L_2 = (Stream_1_t1538553809 *)il2cpp_codegen_object_new(Stream_1_t1538553809_il2cpp_TypeInfo_var);
		Stream_1__ctor_m2512605719(L_2, /*hidden argument*/Stream_1__ctor_m2512605719_MethodInfo_var);
		Stream_1_t1538553809 * L_3 = (Stream_1_t1538553809 *)L_2;
		V_0 = (Stream_1_t1538553809 *)L_3;
		__this->set_countChanged_9(L_3);
		Stream_1_t1538553809 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<UniRx.Unit> BioCollection`1<System.Object>::ObserveReset()
extern Il2CppClass* Stream_1_t1249425060_il2cpp_TypeInfo_var;
extern const MethodInfo* Stream_1__ctor_m2832653796_MethodInfo_var;
extern const uint32_t BioCollection_1_ObserveReset_m1437768404_MetadataUsageId;
extern "C"  Il2CppObject* BioCollection_1_ObserveReset_m1437768404_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_ObserveReset_m1437768404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Stream_1_t1249425060 * V_0 = NULL;
	Stream_1_t1249425060 * G_B2_0 = NULL;
	Stream_1_t1249425060 * G_B1_0 = NULL;
	{
		Stream_1_t1249425060 * L_0 = (Stream_1_t1249425060 *)__this->get_collectionReset_10();
		Stream_1_t1249425060 * L_1 = (Stream_1_t1249425060 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1249425060 * L_2 = (Stream_1_t1249425060 *)il2cpp_codegen_object_new(Stream_1_t1249425060_il2cpp_TypeInfo_var);
		Stream_1__ctor_m2832653796(L_2, /*hidden argument*/Stream_1__ctor_m2832653796_MethodInfo_var);
		Stream_1_t1249425060 * L_3 = (Stream_1_t1249425060 *)L_2;
		V_0 = (Stream_1_t1249425060 *)L_3;
		__this->set_collectionReset_10(L_3);
		Stream_1_t1249425060 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<UniRx.CollectionAddEvent`1<T>> BioCollection`1<System.Object>::ObserveAdd()
extern "C"  Il2CppObject* BioCollection_1_ObserveAdd_m3776200054_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	Stream_1_t1107661009 * V_0 = NULL;
	Stream_1_t1107661009 * G_B2_0 = NULL;
	Stream_1_t1107661009 * G_B1_0 = NULL;
	{
		Stream_1_t1107661009 * L_0 = (Stream_1_t1107661009 *)__this->get_collectionAdd_11();
		Stream_1_t1107661009 * L_1 = (Stream_1_t1107661009 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1107661009 * L_2 = (Stream_1_t1107661009 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 54));
		((  void (*) (Stream_1_t1107661009 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 55)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 55));
		Stream_1_t1107661009 * L_3 = (Stream_1_t1107661009 *)L_2;
		V_0 = (Stream_1_t1107661009 *)L_3;
		__this->set_collectionAdd_11(L_3);
		Stream_1_t1107661009 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<CollectionMoveEvent`1<T>> BioCollection`1<System.Object>::ObserveMove()
extern "C"  Il2CppObject* BioCollection_1_ObserveMove_m2430852416_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	Stream_1_t2263194403 * V_0 = NULL;
	Stream_1_t2263194403 * G_B2_0 = NULL;
	Stream_1_t2263194403 * G_B1_0 = NULL;
	{
		Stream_1_t2263194403 * L_0 = (Stream_1_t2263194403 *)__this->get_collectionMove_12();
		Stream_1_t2263194403 * L_1 = (Stream_1_t2263194403 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t2263194403 * L_2 = (Stream_1_t2263194403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 56));
		((  void (*) (Stream_1_t2263194403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 57)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 57));
		Stream_1_t2263194403 * L_3 = (Stream_1_t2263194403 *)L_2;
		V_0 = (Stream_1_t2263194403 *)L_3;
		__this->set_collectionMove_12(L_3);
		Stream_1_t2263194403 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<CollectionRemoveEvent`1<T>> BioCollection`1<System.Object>::ObserveRemove()
extern "C"  Il2CppObject* BioCollection_1_ObserveRemove_m3770763232_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	Stream_1_t3884365576 * V_0 = NULL;
	Stream_1_t3884365576 * G_B2_0 = NULL;
	Stream_1_t3884365576 * G_B1_0 = NULL;
	{
		Stream_1_t3884365576 * L_0 = (Stream_1_t3884365576 *)__this->get_collectionRemove_13();
		Stream_1_t3884365576 * L_1 = (Stream_1_t3884365576 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3884365576 * L_2 = (Stream_1_t3884365576 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 58));
		((  void (*) (Stream_1_t3884365576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 59)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 59));
		Stream_1_t3884365576 * L_3 = (Stream_1_t3884365576 *)L_2;
		V_0 = (Stream_1_t3884365576 *)L_3;
		__this->set_collectionRemove_13(L_3);
		Stream_1_t3884365576 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// IStream`1<CollectionReplaceEvent`1<T>> BioCollection`1<System.Object>::ObserveReplace()
extern "C"  Il2CppObject* BioCollection_1_ObserveReplace_m612405460_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	Stream_1_t1188511092 * V_0 = NULL;
	Stream_1_t1188511092 * G_B2_0 = NULL;
	Stream_1_t1188511092 * G_B1_0 = NULL;
	{
		Stream_1_t1188511092 * L_0 = (Stream_1_t1188511092 *)__this->get_collectionReplace_14();
		Stream_1_t1188511092 * L_1 = (Stream_1_t1188511092 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1188511092 * L_2 = (Stream_1_t1188511092 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 60));
		((  void (*) (Stream_1_t1188511092 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 61)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 61));
		Stream_1_t1188511092 * L_3 = (Stream_1_t1188511092 *)L_2;
		V_0 = (Stream_1_t1188511092 *)L_3;
		__this->set_collectionReplace_14(L_3);
		Stream_1_t1188511092 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable BioCollection`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * BioCollection_1_OnChanged_m4072865435_gshared (BioCollection_1_t694114648 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)__this->get_updates_15();
		Action_t437523947 * L_1 = ___action0;
		int32_t L_2 = ___p1;
		NullCheck((Stream_1_t4289044124 *)L_0);
		Il2CppObject * L_3 = VirtFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(5 /* System.IDisposable Stream`1<System.Collections.Generic.ICollection`1<System.Object>>::Listen(System.Action,Priority) */, (Stream_1_t4289044124 *)L_0, (Action_t437523947 *)L_1, (int32_t)L_2);
		return L_3;
	}
}
// ICell`1<System.Object> BioCollection`1<System.Object>::AsCellObject()
extern "C"  Il2CppObject* BioCollection_1_AsCellObject_m190317022_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	BioCollection_1_t694114648 * G_B2_0 = NULL;
	BioCollection_1_t694114648 * G_B1_0 = NULL;
	{
		Func_2_t3667291446 * L_0 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cache12_20();
		G_B1_0 = ((BioCollection_1_t694114648 *)(__this));
		if (L_0)
		{
			G_B2_0 = ((BioCollection_1_t694114648 *)(__this));
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 63));
		Func_2_t3667291446 * L_2 = (Func_2_t3667291446 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 64));
		((  void (*) (Func_2_t3667291446 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 65)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 65));
		((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->set_U3CU3Ef__amU24cache12_20(L_2);
		G_B2_0 = ((BioCollection_1_t694114648 *)(G_B1_0));
	}

IL_0019:
	{
		Func_2_t3667291446 * L_3 = ((BioCollection_1_t694114648_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->static_fields)->get_U3CU3Ef__amU24cache12_20();
		Il2CppObject* L_4 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3667291446 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 66)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B2_0, (Func_2_t3667291446 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 66));
		return L_4;
	}
}
// System.IDisposable BioCollection`1<System.Object>::Bind(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
extern "C"  Il2CppObject * BioCollection_1_Bind_m75966107_gshared (BioCollection_1_t694114648 * __this, Action_1_t1451390511 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t1451390511 * L_0 = ___action0;
		NullCheck((Action_1_t1451390511 *)L_0);
		((  void (*) (Action_1_t1451390511 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 67)->method)((Action_1_t1451390511 *)L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 67));
		Stream_1_t4289044124 * L_1 = (Stream_1_t4289044124 *)__this->get_updates_15();
		Action_1_t1451390511 * L_2 = ___action0;
		int32_t L_3 = ___p1;
		NullCheck((Stream_1_t4289044124 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1451390511 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Collections.Generic.ICollection`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t4289044124 *)L_1, (Action_1_t1451390511 *)L_2, (int32_t)L_3);
		return L_4;
	}
}
// System.IDisposable BioCollection`1<System.Object>::ListenUpdates(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
extern "C"  Il2CppObject * BioCollection_1_ListenUpdates_m181791445_gshared (BioCollection_1_t694114648 * __this, Action_1_t1451390511 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t4289044124 * L_0 = (Stream_1_t4289044124 *)__this->get_updates_15();
		Action_1_t1451390511 * L_1 = ___reaction0;
		int32_t L_2 = ___p1;
		NullCheck((Stream_1_t4289044124 *)L_0);
		Il2CppObject * L_3 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1451390511 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Collections.Generic.ICollection`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t4289044124 *)L_0, (Action_1_t1451390511 *)L_1, (int32_t)L_2);
		return L_3;
	}
}
// System.Collections.Generic.ICollection`1<T> BioCollection`1<System.Object>::get_value()
extern "C"  Il2CppObject* BioCollection_1_get_value_m3891915910_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object BioCollection`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * BioCollection_1_get_valueObject_m1074670598_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void BioCollection`1<System.Object>::<Setup>m__165(T)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t BioCollection_1_U3CSetupU3Em__165_m2328322154_MetadataUsageId;
extern "C"  void BioCollection_1_U3CSetupU3Em__165_m2328322154_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_U3CSetupU3Em__165_m2328322154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((BioCollection_1_t694114648 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(46 /* IOrganism BioCollection`1<System.Object>::get_carrier() */, (BioCollection_1_t694114648 *)__this);
		NullCheck((BioCollection_1_t694114648 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(47 /* IRoot BioCollection`1<System.Object>::get_root() */, (BioCollection_1_t694114648 *)__this);
		NullCheck((Il2CppObject *)(*(&___child0)));
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void IOrganism::Setup(IOrganism,IRoot) */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___child0)), (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::<WakeUp>m__166(T)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t BioCollection_1_U3CWakeUpU3Em__166_m337445417_MetadataUsageId;
extern "C"  void BioCollection_1_U3CWakeUpU3Em__166_m337445417_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_U3CWakeUpU3Em__166_m337445417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)(*(&___child0)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void IOrganism::WakeUp() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___child0)));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::<Spread>m__167(T)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern const uint32_t BioCollection_1_U3CSpreadU3Em__167_m1616714652_MetadataUsageId;
extern "C"  void BioCollection_1_U3CSpreadU3Em__167_m1616714652_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_U3CSpreadU3Em__167_m1616714652_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * V_0 = NULL;
	{
		U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * L_0 = (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 69));
		((  void (*) (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 70)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 70));
		V_0 = (U3CSpreadU3Ec__AnonStoreyF6_t1499138945 *)L_0;
		U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * L_1 = V_0;
		Il2CppObject * L_2 = ___child0;
		NullCheck(L_1);
		L_1->set_child_0(L_2);
		U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject ** L_5 = (Il2CppObject **)L_4->get_address_of_child_0();
		NullCheck((Il2CppObject *)(*L_5));
		InterfaceActionInvoker0::Invoke(2 /* System.Void IOrganism::Spread() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_5));
		U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject ** L_7 = (Il2CppObject **)L_6->get_address_of_child_0();
		NullCheck((Il2CppObject *)(*L_7));
		Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* IOnceEmptyStream ILiving::get_fellAsleep() */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)(*L_7));
		U3CSpreadU3Ec__AnonStoreyF6_t1499138945 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 71));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		Il2CppObject * L_12 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11, (int32_t)1);
		NullCheck((BioCollection_1_t694114648 *)__this);
		((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((BioCollection_1_t694114648 *)__this, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::<ToSleep>m__168(System.IDisposable)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t BioCollection_1_U3CToSleepU3Em__168_m1604672846_MetadataUsageId;
extern "C"  void BioCollection_1_U3CToSleepU3Em__168_m1604672846_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___d0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_U3CToSleepU3Em__168_m1604672846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___d0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void BioCollection`1<System.Object>::<ToSleep>m__169(T)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t BioCollection_1_U3CToSleepU3Em__169_m4149698949_MetadataUsageId;
extern "C"  void BioCollection_1_U3CToSleepU3Em__169_m4149698949_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_U3CToSleepU3Em__169_m4149698949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)(*(&___child0)));
		InterfaceActionInvoker0::Invoke(3 /* System.Void IOrganism::ToSleep() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___child0)));
		return;
	}
}
// System.Void BioCollection`1<System.Object>::<ClearItems>m__16A(T)
extern Il2CppClass* IOrganism_t2580843579_il2cpp_TypeInfo_var;
extern const uint32_t BioCollection_1_U3CClearItemsU3Em__16A_m3742891986_MetadataUsageId;
extern "C"  void BioCollection_1_U3CClearItemsU3Em__16A_m3742891986_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioCollection_1_U3CClearItemsU3Em__16A_m3742891986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)(*(&___child0)));
		InterfaceActionInvoker0::Invoke(3 /* System.Void IOrganism::ToSleep() */, IOrganism_t2580843579_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&___child0)));
		return;
	}
}
// System.Object BioCollection`1<System.Object>::<AsCellObject>m__16D(System.Collections.Generic.ICollection`1<T>)
extern "C"  Il2CppObject * BioCollection_1_U3CAsCellObjectU3Em__16D_m3797917425_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___collection0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___collection0;
		return L_0;
	}
}
// System.Void BioProcessor`1/DisposableInstruction<System.Object>::.ctor()
extern "C"  void DisposableInstruction__ctor_m635928393_gshared (DisposableInstruction_t393830967 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BioProcessor`1/DisposableInstruction<System.Object>::Dispose()
extern "C"  void DisposableInstruction_Dispose_m1140112710_gshared (DisposableInstruction_t393830967 * __this, const MethodInfo* method)
{
	{
		BioProcessor_1_t2501661084 * L_0 = (BioProcessor_1_t2501661084 *)__this->get_substance_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		BioProcessor_1_t2501661084 * L_1 = (BioProcessor_1_t2501661084 *)__this->get_substance_1();
		Instruction_t1241108407 * L_2 = (Instruction_t1241108407 *)__this->get_instruction_0();
		NullCheck((BioProcessor_1_t2501661084 *)L_1);
		((  void (*) (BioProcessor_1_t2501661084 *, Instruction_t1241108407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((BioProcessor_1_t2501661084 *)L_1, (Instruction_t1241108407 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_001c:
	{
		__this->set_substance_1((BioProcessor_1_t2501661084 *)NULL);
		__this->set_instruction_0((Instruction_t1241108407 *)NULL);
		return;
	}
}
// System.Void BioProcessor`1/Instruction<System.Object>::.ctor()
extern "C"  void Instruction__ctor_m154630601_gshared (Instruction_t1241108407 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BioProcessor`1<System.Object>::.ctor()
extern "C"  void BioProcessor_1__ctor_m3935699562_gshared (BioProcessor_1_t2501661084 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable BioProcessor`1<System.Object>::Affect(ILiving,System.Func`2<T,T>)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t BioProcessor_1_Affect_m3514211993_MetadataUsageId;
extern "C"  Il2CppObject * BioProcessor_1_Affect_m3514211993_gshared (BioProcessor_1_t2501661084 * __this, Il2CppObject * ___intruder0, Func_2_t2135783352 * ___processor1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioProcessor_1_Affect_m3514211993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Instruction_t1241108407 * V_0 = NULL;
	DisposableInstruction_t393830967 * V_1 = NULL;
	Instruction_t1241108407 * V_2 = NULL;
	DisposableInstruction_t393830967 * V_3 = NULL;
	{
		List_1_t2038067376 * L_0 = (List_1_t2038067376 *)__this->get_instructions_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t2038067376 * L_1 = (List_1_t2038067376 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t2038067376 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_instructions_0(L_1);
	}

IL_0016:
	{
		Instruction_t1241108407 * L_2 = (Instruction_t1241108407 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Instruction_t1241108407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_2 = (Instruction_t1241108407 *)L_2;
		Instruction_t1241108407 * L_3 = V_2;
		Il2CppObject * L_4 = ___intruder0;
		NullCheck(L_3);
		L_3->set_intruder_0(L_4);
		Instruction_t1241108407 * L_5 = V_2;
		Func_2_t2135783352 * L_6 = ___processor1;
		NullCheck(L_5);
		L_5->set_instruction_1(L_6);
		Instruction_t1241108407 * L_7 = V_2;
		V_0 = (Instruction_t1241108407 *)L_7;
		List_1_t2038067376 * L_8 = (List_1_t2038067376 *)__this->get_instructions_0();
		Instruction_t1241108407 * L_9 = V_0;
		NullCheck((List_1_t2038067376 *)L_8);
		VirtActionInvoker1< Instruction_t1241108407 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<BioProcessor`1/Instruction<System.Object>>::Add(!0) */, (List_1_t2038067376 *)L_8, (Instruction_t1241108407 *)L_9);
		DisposableInstruction_t393830967 * L_10 = (DisposableInstruction_t393830967 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (DisposableInstruction_t393830967 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_3 = (DisposableInstruction_t393830967 *)L_10;
		DisposableInstruction_t393830967 * L_11 = V_3;
		Instruction_t1241108407 * L_12 = V_0;
		NullCheck(L_11);
		L_11->set_instruction_0(L_12);
		DisposableInstruction_t393830967 * L_13 = V_3;
		NullCheck(L_13);
		L_13->set_substance_1(__this);
		DisposableInstruction_t393830967 * L_14 = V_3;
		V_1 = (DisposableInstruction_t393830967 *)L_14;
		Il2CppObject * L_15 = ___intruder0;
		DisposableInstruction_t393830967 * L_16 = V_1;
		NullCheck((Il2CppObject *)L_15);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_15, (Il2CppObject *)L_16);
		DisposableInstruction_t393830967 * L_17 = V_1;
		return L_17;
	}
}
// System.IDisposable BioProcessor`1<System.Object>::AffectUnsafe(System.Func`2<T,T>)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t BioProcessor_1_AffectUnsafe_m2299920505_MetadataUsageId;
extern "C"  Il2CppObject * BioProcessor_1_AffectUnsafe_m2299920505_gshared (BioProcessor_1_t2501661084 * __this, Func_2_t2135783352 * ___processor0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioProcessor_1_AffectUnsafe_m2299920505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		Func_2_t2135783352 * L_1 = ___processor0;
		NullCheck((BioProcessor_1_t2501661084 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (BioProcessor_1_t2501661084 *, Il2CppObject *, Func_2_t2135783352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((BioProcessor_1_t2501661084 *)__this, (Il2CppObject *)L_0, (Func_2_t2135783352 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_2;
	}
}
// System.Void BioProcessor`1<System.Object>::ClearInstruction(BioProcessor`1/Instruction<T>)
extern "C"  void BioProcessor_1_ClearInstruction_m2200475659_gshared (BioProcessor_1_t2501661084 * __this, Instruction_t1241108407 * ___intr0, const MethodInfo* method)
{
	{
		List_1_t2038067376 * L_0 = (List_1_t2038067376 *)__this->get_instructions_0();
		Instruction_t1241108407 * L_1 = ___intr0;
		NullCheck((List_1_t2038067376 *)L_0);
		VirtFuncInvoker1< bool, Instruction_t1241108407 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<BioProcessor`1/Instruction<System.Object>>::Remove(!0) */, (List_1_t2038067376 *)L_0, (Instruction_t1241108407 *)L_1);
		return;
	}
}
// T BioProcessor`1<System.Object>::Process(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t BioProcessor_1_Process_m2053800744_MetadataUsageId;
extern "C"  Il2CppObject * BioProcessor_1_Process_m2053800744_gshared (BioProcessor_1_t2501661084 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BioProcessor_1_Process_m2053800744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Instruction_t1241108407 * V_0 = NULL;
	Enumerator_t123850368  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2038067376 * L_0 = (List_1_t2038067376 *)__this->get_instructions_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		return L_1;
	}

IL_000d:
	{
		List_1_t2038067376 * L_2 = (List_1_t2038067376 *)__this->get_instructions_0();
		NullCheck((List_1_t2038067376 *)L_2);
		Enumerator_t123850368  L_3 = ((  Enumerator_t123850368  (*) (List_1_t2038067376 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((List_1_t2038067376 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_1 = (Enumerator_t123850368 )L_3;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0034;
		}

IL_001e:
		{
			Instruction_t1241108407 * L_4 = ((  Instruction_t1241108407 * (*) (Enumerator_t123850368 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Enumerator_t123850368 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			V_0 = (Instruction_t1241108407 *)L_4;
			Instruction_t1241108407 * L_5 = V_0;
			NullCheck(L_5);
			Func_2_t2135783352 * L_6 = (Func_2_t2135783352 *)L_5->get_instruction_1();
			Il2CppObject * L_7 = ___value0;
			NullCheck((Func_2_t2135783352 *)L_6);
			Il2CppObject * L_8 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Func_2_t2135783352 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			___value0 = (Il2CppObject *)L_8;
		}

IL_0034:
		{
			bool L_9 = ((  bool (*) (Enumerator_t123850368 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Enumerator_t123850368 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			if (L_9)
			{
				goto IL_001e;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x51, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Enumerator_t123850368  L_10 = V_1;
		Enumerator_t123850368  L_11 = L_10;
		Il2CppObject * L_12 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0051:
	{
		Stream_1_t3823212738 * L_13 = (Stream_1_t3823212738 *)__this->get_output__1();
		if (!L_13)
		{
			goto IL_0068;
		}
	}
	{
		Stream_1_t3823212738 * L_14 = (Stream_1_t3823212738 *)__this->get_output__1();
		Il2CppObject * L_15 = ___value0;
		NullCheck((Stream_1_t3823212738 *)L_14);
		((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Stream_1_t3823212738 *)L_14, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
	}

IL_0068:
	{
		Il2CppObject * L_16 = ___value0;
		return L_16;
	}
}
// IStream`1<T> BioProcessor`1<System.Object>::get_output()
extern "C"  Il2CppObject* BioProcessor_1_get_output_m3847914643_gshared (BioProcessor_1_t2501661084 * __this, const MethodInfo* method)
{
	Stream_1_t3823212738 * V_0 = NULL;
	Stream_1_t3823212738 * G_B2_0 = NULL;
	Stream_1_t3823212738 * G_B1_0 = NULL;
	{
		Stream_1_t3823212738 * L_0 = (Stream_1_t3823212738 *)__this->get_output__1();
		Stream_1_t3823212738 * L_1 = (Stream_1_t3823212738 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3823212738 * L_2 = (Stream_1_t3823212738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Stream_1_t3823212738 * L_3 = (Stream_1_t3823212738 *)L_2;
		V_0 = (Stream_1_t3823212738 *)L_3;
		__this->set_output__1(L_3);
		Stream_1_t3823212738 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.Void Blind`1<System.Object>::.ctor()
extern "C"  void Blind_1__ctor_m4245399685_gshared (Blind_1_t3800599375 * __this, const MethodInfo* method)
{
	{
		NullCheck((Organism_2_t181152922 *)__this);
		((  void (*) (Organism_2_t181152922 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Organism_2_t181152922 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Boolean>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m4242813319_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Boolean>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m299397247_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 * __this, bool ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.DateTime>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m1831197126_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.DateTime>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m3162366270_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 * __this, DateTime_t339033936  ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Double>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m1940134908_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Double>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m4177795188_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 * __this, double ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Int32>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m2048617197_gshared (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Int32>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m1147700965_gshared (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 * __this, int32_t ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Int64>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m3136929932_gshared (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Int64>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m2631202052_gshared (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 * __this, int64_t ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Object>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m535646698_gshared (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Object>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m1355065186_gshared (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Single>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m193608627_gshared (U3COnChangedU3Ec__AnonStoreyF3_t39390489 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Single>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m3973416107_gshared (U3COnChangedU3Ec__AnonStoreyF3_t39390489 * __this, float ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1<System.Boolean>::.ctor()
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m1744551183_MetadataUsageId;
extern "C"  void Cell_1__ctor_m1744551183_gshared (Cell_1_t393102973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m1744551183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1<System.Boolean>::.ctor(T)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m2541480495_MetadataUsageId;
extern "C"  void Cell_1__ctor_m2541480495_gshared (Cell_1_t393102973 * __this, bool ___initial0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m2541480495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_1 = ___initial0;
		__this->set_Value_2(L_1);
		return;
	}
}
// System.Void Cell`1<System.Boolean>::.ctor(ICell`1<T>)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m1361635077_MetadataUsageId;
extern "C"  void Cell_1__ctor_m1361635077_gshared (Cell_1_t393102973 * __this, Il2CppObject* ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m1361635077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___other0;
		NullCheck((Cell_1_t393102973 *)__this);
		((  void (*) (Cell_1_t393102973 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t393102973 *)__this, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void Cell`1<System.Boolean>::SetInputCell(ICell`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputCell_m4043152341_MetadataUsageId;
extern "C"  void Cell_1_SetInputCell_m4043152341_gshared (Cell_1_t393102973 * __this, Il2CppObject* ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputCell_m4043152341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___cell0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t359458046 * L_4 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Boolean>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Action_1_t359458046 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// T Cell`1<System.Boolean>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_get_value_m4241254838_MetadataUsageId;
extern "C"  bool Cell_1_get_value_m4241254838_gshared (Cell_1_t393102973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_get_value_m4241254838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		bool L_1 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		bool L_2 = (bool)__this->get_holdedValue_4();
		return L_2;
	}

IL_0022:
	{
		bool L_3 = (bool)__this->get_Value_2();
		return L_3;
	}
}
// System.Void Cell`1<System.Boolean>::set_value(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4175183221;
extern const uint32_t Cell_1_set_value_m3532323549_MetadataUsageId;
extern "C"  void Cell_1_set_value_m3532323549_gshared (Cell_1_t393102973 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_set_value_m3532323549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4175183221, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_0031:
	{
		bool L_3 = ___value0;
		NullCheck((Cell_1_t393102973 *)__this);
		((  void (*) (Cell_1_t393102973 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Cell_1_t393102973 *)__this, (bool)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void Cell`1<System.Boolean>::Set(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_Set_m4026826287_MetadataUsageId;
extern "C"  void Cell_1_Set_m4026826287_gshared (Cell_1_t393102973 * __this, bool ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_Set_m4026826287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_1 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_1)
		{
			goto IL_00b3;
		}
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		bool L_3 = (bool)__this->get_holdedValue_4();
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = ___val0;
		bool L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_7);
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		bool L_10 = (bool)__this->get_holdedValueIsCurrent_3();
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		bool L_11 = (bool)__this->get_Value_2();
		bool L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_12);
		bool L_14 = ___val0;
		bool L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_15);
		bool L_17 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_13, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0062;
		}
	}
	{
		return;
	}

IL_0062:
	{
		bool L_18 = ___val0;
		__this->set_holdedValue_4(L_18);
		__this->set_holdedValueIsCurrent_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_19 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_19)
		{
			goto IL_00ae;
		}
	}
	{
		Stream_1_t3197111659 * L_20 = (Stream_1_t3197111659 *)__this->get_update_0();
		if (!L_20)
		{
			goto IL_00ae;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_00a3;
	}

IL_0092:
	{
		Stream_1_t3197111659 * L_21 = (Stream_1_t3197111659 *)__this->get_update_0();
		bool L_22 = ___val0;
		int32_t L_23 = V_0;
		NullCheck((Stream_1_t3197111659 *)L_21);
		((  void (*) (Stream_1_t3197111659 *, bool, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3197111659 *)L_21, (bool)L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		int32_t L_26 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_currentPriorityUnpack_6();
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0092;
		}
	}

IL_00ae:
	{
		goto IL_00f2;
	}

IL_00b3:
	{
		bool L_27 = (bool)__this->get_Value_2();
		bool L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_28);
		bool L_30 = ___val0;
		bool L_31 = L_30;
		Il2CppObject * L_32 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_31);
		bool L_33 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_29, (Il2CppObject *)L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00cf;
		}
	}
	{
		return;
	}

IL_00cf:
	{
		bool L_34 = ___val0;
		__this->set_Value_2(L_34);
		Stream_1_t3197111659 * L_35 = (Stream_1_t3197111659 *)__this->get_update_0();
		if (!L_35)
		{
			goto IL_00f2;
		}
	}
	{
		Stream_1_t3197111659 * L_36 = (Stream_1_t3197111659 *)__this->get_update_0();
		bool L_37 = (bool)__this->get_Value_2();
		NullCheck((Stream_1_t3197111659 *)L_36);
		((  void (*) (Stream_1_t3197111659 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Stream_1_t3197111659 *)L_36, (bool)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00f2:
	{
		return;
	}
}
// System.IDisposable Cell`1<System.Boolean>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m3436308404_gshared (Cell_1_t393102973 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyF3_t3587154105 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyF3_t3587154105 * L_0 = (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3COnChangedU3Ec__AnonStoreyF3_t3587154105 *)L_0;
		U3COnChangedU3Ec__AnonStoreyF3_t3587154105 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Stream_1_t3197111659 * L_3 = (Stream_1_t3197111659 *)__this->get_update_0();
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		Stream_1_t3197111659 * L_4 = (Stream_1_t3197111659 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_4);
	}

IL_0023:
	{
		Stream_1_t3197111659 * L_5 = (Stream_1_t3197111659 *)__this->get_update_0();
		U3COnChangedU3Ec__AnonStoreyF3_t3587154105 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t359458046 * L_8 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_9 = ___p1;
		NullCheck((Stream_1_t3197111659 *)L_5);
		Il2CppObject * L_10 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Boolean>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3197111659 *)L_5, (Action_1_t359458046 *)L_8, (int32_t)L_9);
		return L_10;
	}
}
// System.Object Cell`1<System.Boolean>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m1046160671_gshared (Cell_1_t393102973 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t393102973 *)__this);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(11 /* T Cell`1<System.Boolean>::get_value() */, (Cell_1_t393102973 *)__this);
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return L_2;
	}
}
// System.IDisposable Cell`1<System.Boolean>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m1565576101_gshared (Cell_1_t393102973 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3197111659 * L_0 = (Stream_1_t3197111659 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3197111659 * L_1 = (Stream_1_t3197111659 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_1);
	}

IL_0016:
	{
		Stream_1_t3197111659 * L_2 = (Stream_1_t3197111659 *)__this->get_update_0();
		Action_1_t359458046 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3197111659 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Boolean>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3197111659 *)L_2, (Action_1_t359458046 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> Cell`1<System.Boolean>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m762843005_gshared (Cell_1_t393102973 * __this, const MethodInfo* method)
{
	Stream_1_t3197111659 * V_0 = NULL;
	Stream_1_t3197111659 * G_B2_0 = NULL;
	Stream_1_t3197111659 * G_B1_0 = NULL;
	{
		Stream_1_t3197111659 * L_0 = (Stream_1_t3197111659 *)__this->get_update_0();
		Stream_1_t3197111659 * L_1 = (Stream_1_t3197111659 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3197111659 * L_2 = (Stream_1_t3197111659 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Stream_1_t3197111659 * L_3 = (Stream_1_t3197111659 *)L_2;
		V_0 = (Stream_1_t3197111659 *)L_3;
		__this->set_update_0(L_3);
		Stream_1_t3197111659 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable Cell`1<System.Boolean>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m2867406077_gshared (Cell_1_t393102973 * __this, Action_1_t359458046 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t359458046 * L_0 = ___action0;
		bool L_1 = (bool)__this->get_Value_2();
		NullCheck((Action_1_t359458046 *)L_0);
		((  void (*) (Action_1_t359458046 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Action_1_t359458046 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Stream_1_t3197111659 * L_2 = (Stream_1_t3197111659 *)__this->get_update_0();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Stream_1_t3197111659 * L_3 = (Stream_1_t3197111659 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_3);
	}

IL_0022:
	{
		Stream_1_t3197111659 * L_4 = (Stream_1_t3197111659 *)__this->get_update_0();
		Action_1_t359458046 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3197111659 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Boolean>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3197111659 *)L_4, (Action_1_t359458046 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Void Cell`1<System.Boolean>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputStream_m979608145_MetadataUsageId;
extern "C"  void Cell_1_SetInputStream_m979608145_gshared (Cell_1_t393102973 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputStream_m979608145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t359458046 * L_4 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Boolean>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t359458046 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Cell`1<System.Boolean>::ResetInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_ResetInputStream_m3591659696_MetadataUsageId;
extern "C"  void Cell_1_ResetInputStream_m3591659696_gshared (Cell_1_t393102973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_ResetInputStream_m3591659696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Cell`1<System.Boolean>::TransactionIterationFinished()
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_TransactionIterationFinished_m3098048838_MetadataUsageId;
extern "C"  void Cell_1_TransactionIterationFinished_m3098048838_gshared (Cell_1_t393102973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_TransactionIterationFinished_m3098048838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		bool L_0 = (bool)__this->get_holdedValue_4();
		__this->set_Value_2(L_0);
		__this->set_holdedValueIsCurrent_3((bool)0);
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_1 = V_0;
		__this->set_holdedValue_4(L_1);
		return;
	}
}
// System.Void Cell`1<System.Boolean>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m4206045144_gshared (Cell_1_t393102973 * __this, int32_t ___p0, const MethodInfo* method)
{
	{
		Stream_1_t3197111659 * L_0 = (Stream_1_t3197111659 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = (bool)__this->get_holdedValue_4();
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_2);
		bool L_4 = (bool)__this->get_Value_2();
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		Stream_1_t3197111659 * L_8 = (Stream_1_t3197111659 *)__this->get_update_0();
		bool L_9 = (bool)__this->get_holdedValue_4();
		int32_t L_10 = ___p0;
		NullCheck((Stream_1_t3197111659 *)L_8);
		((  void (*) (Stream_1_t3197111659 *, bool, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3197111659 *)L_8, (bool)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean Cell`1<System.Boolean>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m2487722393_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t393102973 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Cell`1<System.Boolean>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m2159315534_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t393102973 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Cell`1<System.DateTime>::.ctor()
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m1694482238_MetadataUsageId;
extern "C"  void Cell_1__ctor_m1694482238_gshared (Cell_1_t521131568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m1694482238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (DateTime_t339033936_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t339033936  L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1<System.DateTime>::.ctor(T)
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m989343200_MetadataUsageId;
extern "C"  void Cell_1__ctor_m989343200_gshared (Cell_1_t521131568 * __this, DateTime_t339033936  ___initial0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m989343200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (DateTime_t339033936_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t339033936  L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		DateTime_t339033936  L_1 = ___initial0;
		__this->set_Value_2(L_1);
		return;
	}
}
// System.Void Cell`1<System.DateTime>::.ctor(ICell`1<T>)
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m4170136180_MetadataUsageId;
extern "C"  void Cell_1__ctor_m4170136180_gshared (Cell_1_t521131568 * __this, Il2CppObject* ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m4170136180_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (DateTime_t339033936_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t339033936  L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___other0;
		NullCheck((Cell_1_t521131568 *)__this);
		((  void (*) (Cell_1_t521131568 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t521131568 *)__this, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void Cell`1<System.DateTime>::SetInputCell(ICell`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputCell_m1460034950_MetadataUsageId;
extern "C"  void Cell_1_SetInputCell_m1460034950_gshared (Cell_1_t521131568 * __this, Il2CppObject* ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputCell_m1460034950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___cell0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t487486641 * L_4 = (Action_1_t487486641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t487486641 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t487486641 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.DateTime>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Action_1_t487486641 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// T Cell`1<System.DateTime>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_get_value_m1160472643_MetadataUsageId;
extern "C"  DateTime_t339033936  Cell_1_get_value_m1160472643_gshared (Cell_1_t521131568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_get_value_m1160472643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		bool L_1 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		DateTime_t339033936  L_2 = (DateTime_t339033936 )__this->get_holdedValue_4();
		return L_2;
	}

IL_0022:
	{
		DateTime_t339033936  L_3 = (DateTime_t339033936 )__this->get_Value_2();
		return L_3;
	}
}
// System.Void Cell`1<System.DateTime>::set_value(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4175183221;
extern const uint32_t Cell_1_set_value_m300678670_MetadataUsageId;
extern "C"  void Cell_1_set_value_m300678670_gshared (Cell_1_t521131568 * __this, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_set_value_m300678670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4175183221, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_0031:
	{
		DateTime_t339033936  L_3 = ___value0;
		NullCheck((Cell_1_t521131568 *)__this);
		((  void (*) (Cell_1_t521131568 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Cell_1_t521131568 *)__this, (DateTime_t339033936 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void Cell`1<System.DateTime>::Set(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_Set_m7338528_MetadataUsageId;
extern "C"  void Cell_1_Set_m7338528_gshared (Cell_1_t521131568 * __this, DateTime_t339033936  ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_Set_m7338528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_1 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_1)
		{
			goto IL_00b3;
		}
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		DateTime_t339033936  L_3 = (DateTime_t339033936 )__this->get_holdedValue_4();
		DateTime_t339033936  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		DateTime_t339033936  L_6 = ___val0;
		DateTime_t339033936  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_7);
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		bool L_10 = (bool)__this->get_holdedValueIsCurrent_3();
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		DateTime_t339033936  L_11 = (DateTime_t339033936 )__this->get_Value_2();
		DateTime_t339033936  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_12);
		DateTime_t339033936  L_14 = ___val0;
		DateTime_t339033936  L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_15);
		bool L_17 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_13, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0062;
		}
	}
	{
		return;
	}

IL_0062:
	{
		DateTime_t339033936  L_18 = ___val0;
		__this->set_holdedValue_4(L_18);
		__this->set_holdedValueIsCurrent_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_19 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_19)
		{
			goto IL_00ae;
		}
	}
	{
		Stream_1_t3325140254 * L_20 = (Stream_1_t3325140254 *)__this->get_update_0();
		if (!L_20)
		{
			goto IL_00ae;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_00a3;
	}

IL_0092:
	{
		Stream_1_t3325140254 * L_21 = (Stream_1_t3325140254 *)__this->get_update_0();
		DateTime_t339033936  L_22 = ___val0;
		int32_t L_23 = V_0;
		NullCheck((Stream_1_t3325140254 *)L_21);
		((  void (*) (Stream_1_t3325140254 *, DateTime_t339033936 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3325140254 *)L_21, (DateTime_t339033936 )L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		int32_t L_26 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_currentPriorityUnpack_6();
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0092;
		}
	}

IL_00ae:
	{
		goto IL_00f2;
	}

IL_00b3:
	{
		DateTime_t339033936  L_27 = (DateTime_t339033936 )__this->get_Value_2();
		DateTime_t339033936  L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_28);
		DateTime_t339033936  L_30 = ___val0;
		DateTime_t339033936  L_31 = L_30;
		Il2CppObject * L_32 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_31);
		bool L_33 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_29, (Il2CppObject *)L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00cf;
		}
	}
	{
		return;
	}

IL_00cf:
	{
		DateTime_t339033936  L_34 = ___val0;
		__this->set_Value_2(L_34);
		Stream_1_t3325140254 * L_35 = (Stream_1_t3325140254 *)__this->get_update_0();
		if (!L_35)
		{
			goto IL_00f2;
		}
	}
	{
		Stream_1_t3325140254 * L_36 = (Stream_1_t3325140254 *)__this->get_update_0();
		DateTime_t339033936  L_37 = (DateTime_t339033936 )__this->get_Value_2();
		NullCheck((Stream_1_t3325140254 *)L_36);
		((  void (*) (Stream_1_t3325140254 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Stream_1_t3325140254 *)L_36, (DateTime_t339033936 )L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00f2:
	{
		return;
	}
}
// System.IDisposable Cell`1<System.DateTime>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m3463937193_gshared (Cell_1_t521131568 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyF3_t3715182700 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyF3_t3715182700 * L_0 = (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3COnChangedU3Ec__AnonStoreyF3_t3715182700 *)L_0;
		U3COnChangedU3Ec__AnonStoreyF3_t3715182700 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Stream_1_t3325140254 * L_3 = (Stream_1_t3325140254 *)__this->get_update_0();
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		Stream_1_t3325140254 * L_4 = (Stream_1_t3325140254 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3325140254 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_4);
	}

IL_0023:
	{
		Stream_1_t3325140254 * L_5 = (Stream_1_t3325140254 *)__this->get_update_0();
		U3COnChangedU3Ec__AnonStoreyF3_t3715182700 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t487486641 * L_8 = (Action_1_t487486641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t487486641 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_9 = ___p1;
		NullCheck((Stream_1_t3325140254 *)L_5);
		Il2CppObject * L_10 = VirtFuncInvoker2< Il2CppObject *, Action_1_t487486641 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.DateTime>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3325140254 *)L_5, (Action_1_t487486641 *)L_8, (int32_t)L_9);
		return L_10;
	}
}
// System.Object Cell`1<System.DateTime>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m4016140312_gshared (Cell_1_t521131568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t521131568 *)__this);
		DateTime_t339033936  L_0 = VirtFuncInvoker0< DateTime_t339033936  >::Invoke(11 /* T Cell`1<System.DateTime>::get_value() */, (Cell_1_t521131568 *)__this);
		DateTime_t339033936  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return L_2;
	}
}
// System.IDisposable Cell`1<System.DateTime>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m2767911504_gshared (Cell_1_t521131568 * __this, Action_1_t487486641 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3325140254 * L_0 = (Stream_1_t3325140254 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3325140254 * L_1 = (Stream_1_t3325140254 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3325140254 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_1);
	}

IL_0016:
	{
		Stream_1_t3325140254 * L_2 = (Stream_1_t3325140254 *)__this->get_update_0();
		Action_1_t487486641 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3325140254 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t487486641 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.DateTime>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3325140254 *)L_2, (Action_1_t487486641 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> Cell`1<System.DateTime>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m4219183406_gshared (Cell_1_t521131568 * __this, const MethodInfo* method)
{
	Stream_1_t3325140254 * V_0 = NULL;
	Stream_1_t3325140254 * G_B2_0 = NULL;
	Stream_1_t3325140254 * G_B1_0 = NULL;
	{
		Stream_1_t3325140254 * L_0 = (Stream_1_t3325140254 *)__this->get_update_0();
		Stream_1_t3325140254 * L_1 = (Stream_1_t3325140254 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3325140254 * L_2 = (Stream_1_t3325140254 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3325140254 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Stream_1_t3325140254 * L_3 = (Stream_1_t3325140254 *)L_2;
		V_0 = (Stream_1_t3325140254 *)L_3;
		__this->set_update_0(L_3);
		Stream_1_t3325140254 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable Cell`1<System.DateTime>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m2895034866_gshared (Cell_1_t521131568 * __this, Action_1_t487486641 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t487486641 * L_0 = ___action0;
		DateTime_t339033936  L_1 = (DateTime_t339033936 )__this->get_Value_2();
		NullCheck((Action_1_t487486641 *)L_0);
		((  void (*) (Action_1_t487486641 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Action_1_t487486641 *)L_0, (DateTime_t339033936 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Stream_1_t3325140254 * L_2 = (Stream_1_t3325140254 *)__this->get_update_0();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Stream_1_t3325140254 * L_3 = (Stream_1_t3325140254 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3325140254 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_3);
	}

IL_0022:
	{
		Stream_1_t3325140254 * L_4 = (Stream_1_t3325140254 *)__this->get_update_0();
		Action_1_t487486641 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3325140254 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t487486641 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.DateTime>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3325140254 *)L_4, (Action_1_t487486641 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Void Cell`1<System.DateTime>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputStream_m98706306_MetadataUsageId;
extern "C"  void Cell_1_SetInputStream_m98706306_gshared (Cell_1_t521131568 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputStream_m98706306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t487486641 * L_4 = (Action_1_t487486641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t487486641 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t487486641 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.DateTime>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t487486641 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Cell`1<System.DateTime>::ResetInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_ResetInputStream_m460880673_MetadataUsageId;
extern "C"  void Cell_1_ResetInputStream_m460880673_gshared (Cell_1_t521131568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_ResetInputStream_m460880673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Cell`1<System.DateTime>::TransactionIterationFinished()
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_TransactionIterationFinished_m2659938871_MetadataUsageId;
extern "C"  void Cell_1_TransactionIterationFinished_m2659938871_gshared (Cell_1_t521131568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_TransactionIterationFinished_m2659938871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t339033936  L_0 = (DateTime_t339033936 )__this->get_holdedValue_4();
		__this->set_Value_2(L_0);
		__this->set_holdedValueIsCurrent_3((bool)0);
		Initobj (DateTime_t339033936_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t339033936  L_1 = V_0;
		__this->set_holdedValue_4(L_1);
		return;
	}
}
// System.Void Cell`1<System.DateTime>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m2004511241_gshared (Cell_1_t521131568 * __this, int32_t ___p0, const MethodInfo* method)
{
	{
		Stream_1_t3325140254 * L_0 = (Stream_1_t3325140254 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		DateTime_t339033936  L_1 = (DateTime_t339033936 )__this->get_holdedValue_4();
		DateTime_t339033936  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_2);
		DateTime_t339033936  L_4 = (DateTime_t339033936 )__this->get_Value_2();
		DateTime_t339033936  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		Stream_1_t3325140254 * L_8 = (Stream_1_t3325140254 *)__this->get_update_0();
		DateTime_t339033936  L_9 = (DateTime_t339033936 )__this->get_holdedValue_4();
		int32_t L_10 = ___p0;
		NullCheck((Stream_1_t3325140254 *)L_8);
		((  void (*) (Stream_1_t3325140254 *, DateTime_t339033936 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3325140254 *)L_8, (DateTime_t339033936 )L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean Cell`1<System.DateTime>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m2421816048_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t521131568 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Cell`1<System.DateTime>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m1543445911_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t521131568 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Cell`1<System.Double>::.ctor()
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m3106471796_MetadataUsageId;
extern "C"  void Cell_1__ctor_m3106471796_gshared (Cell_1_t716614246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m3106471796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1<System.Double>::.ctor(T)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m1811346538_MetadataUsageId;
extern "C"  void Cell_1__ctor_m1811346538_gshared (Cell_1_t716614246 * __this, double ___initial0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m1811346538_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		double L_1 = ___initial0;
		__this->set_Value_2(L_1);
		return;
	}
}
// System.Void Cell`1<System.Double>::.ctor(ICell`1<T>)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m2187734826_MetadataUsageId;
extern "C"  void Cell_1__ctor_m2187734826_gshared (Cell_1_t716614246 * __this, Il2CppObject* ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m2187734826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___other0;
		NullCheck((Cell_1_t716614246 *)__this);
		((  void (*) (Cell_1_t716614246 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t716614246 *)__this, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void Cell`1<System.Double>::SetInputCell(ICell`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputCell_m2840252432_MetadataUsageId;
extern "C"  void Cell_1_SetInputCell_m2840252432_gshared (Cell_1_t716614246 * __this, Il2CppObject* ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputCell_m2840252432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___cell0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t682969319 * L_4 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Double>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Action_1_t682969319 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// T Cell`1<System.Double>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_get_value_m3739324729_MetadataUsageId;
extern "C"  double Cell_1_get_value_m3739324729_gshared (Cell_1_t716614246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_get_value_m3739324729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		bool L_1 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		double L_2 = (double)__this->get_holdedValue_4();
		return L_2;
	}

IL_0022:
	{
		double L_3 = (double)__this->get_Value_2();
		return L_3;
	}
}
// System.Void Cell`1<System.Double>::set_value(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4175183221;
extern const uint32_t Cell_1_set_value_m2175823768_MetadataUsageId;
extern "C"  void Cell_1_set_value_m2175823768_gshared (Cell_1_t716614246 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_set_value_m2175823768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4175183221, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_0031:
	{
		double L_3 = ___value0;
		NullCheck((Cell_1_t716614246 *)__this);
		((  void (*) (Cell_1_t716614246 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Cell_1_t716614246 *)__this, (double)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void Cell`1<System.Double>::Set(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_Set_m4070759210_MetadataUsageId;
extern "C"  void Cell_1_Set_m4070759210_gshared (Cell_1_t716614246 * __this, double ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_Set_m4070759210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_1 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_1)
		{
			goto IL_00b3;
		}
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		double L_3 = (double)__this->get_holdedValue_4();
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		double L_6 = ___val0;
		double L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_7);
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		bool L_10 = (bool)__this->get_holdedValueIsCurrent_3();
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		double L_11 = (double)__this->get_Value_2();
		double L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_12);
		double L_14 = ___val0;
		double L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_15);
		bool L_17 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_13, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0062;
		}
	}
	{
		return;
	}

IL_0062:
	{
		double L_18 = ___val0;
		__this->set_holdedValue_4(L_18);
		__this->set_holdedValueIsCurrent_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_19 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_19)
		{
			goto IL_00ae;
		}
	}
	{
		Stream_1_t3520622932 * L_20 = (Stream_1_t3520622932 *)__this->get_update_0();
		if (!L_20)
		{
			goto IL_00ae;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_00a3;
	}

IL_0092:
	{
		Stream_1_t3520622932 * L_21 = (Stream_1_t3520622932 *)__this->get_update_0();
		double L_22 = ___val0;
		int32_t L_23 = V_0;
		NullCheck((Stream_1_t3520622932 *)L_21);
		((  void (*) (Stream_1_t3520622932 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3520622932 *)L_21, (double)L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		int32_t L_26 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_currentPriorityUnpack_6();
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0092;
		}
	}

IL_00ae:
	{
		goto IL_00f2;
	}

IL_00b3:
	{
		double L_27 = (double)__this->get_Value_2();
		double L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_28);
		double L_30 = ___val0;
		double L_31 = L_30;
		Il2CppObject * L_32 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_31);
		bool L_33 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_29, (Il2CppObject *)L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00cf;
		}
	}
	{
		return;
	}

IL_00cf:
	{
		double L_34 = ___val0;
		__this->set_Value_2(L_34);
		Stream_1_t3520622932 * L_35 = (Stream_1_t3520622932 *)__this->get_update_0();
		if (!L_35)
		{
			goto IL_00f2;
		}
	}
	{
		Stream_1_t3520622932 * L_36 = (Stream_1_t3520622932 *)__this->get_update_0();
		double L_37 = (double)__this->get_Value_2();
		NullCheck((Stream_1_t3520622932 *)L_36);
		((  void (*) (Stream_1_t3520622932 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Stream_1_t3520622932 *)L_36, (double)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00f2:
	{
		return;
	}
}
// System.IDisposable Cell`1<System.Double>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m1552633375_gshared (Cell_1_t716614246 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyF3_t3910665378 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyF3_t3910665378 * L_0 = (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 *)L_0;
		U3COnChangedU3Ec__AnonStoreyF3_t3910665378 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Stream_1_t3520622932 * L_3 = (Stream_1_t3520622932 *)__this->get_update_0();
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		Stream_1_t3520622932 * L_4 = (Stream_1_t3520622932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_4);
	}

IL_0023:
	{
		Stream_1_t3520622932 * L_5 = (Stream_1_t3520622932 *)__this->get_update_0();
		U3COnChangedU3Ec__AnonStoreyF3_t3910665378 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t682969319 * L_8 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_9 = ___p1;
		NullCheck((Stream_1_t3520622932 *)L_5);
		Il2CppObject * L_10 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Double>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3520622932 *)L_5, (Action_1_t682969319 *)L_8, (int32_t)L_9);
		return L_10;
	}
}
// System.Object Cell`1<System.Double>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m897220110_gshared (Cell_1_t716614246 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t716614246 *)__this);
		double L_0 = VirtFuncInvoker0< double >::Invoke(11 /* T Cell`1<System.Double>::get_value() */, (Cell_1_t716614246 *)__this);
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return L_2;
	}
}
// System.IDisposable Cell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m3413929370_gshared (Cell_1_t716614246 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3520622932 * L_0 = (Stream_1_t3520622932 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3520622932 * L_1 = (Stream_1_t3520622932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_1);
	}

IL_0016:
	{
		Stream_1_t3520622932 * L_2 = (Stream_1_t3520622932 *)__this->get_update_0();
		Action_1_t682969319 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3520622932 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Double>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3520622932 *)L_2, (Action_1_t682969319 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> Cell`1<System.Double>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m3305586980_gshared (Cell_1_t716614246 * __this, const MethodInfo* method)
{
	Stream_1_t3520622932 * V_0 = NULL;
	Stream_1_t3520622932 * G_B2_0 = NULL;
	Stream_1_t3520622932 * G_B1_0 = NULL;
	{
		Stream_1_t3520622932 * L_0 = (Stream_1_t3520622932 *)__this->get_update_0();
		Stream_1_t3520622932 * L_1 = (Stream_1_t3520622932 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3520622932 * L_2 = (Stream_1_t3520622932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Stream_1_t3520622932 * L_3 = (Stream_1_t3520622932 *)L_2;
		V_0 = (Stream_1_t3520622932 *)L_3;
		__this->set_update_0(L_3);
		Stream_1_t3520622932 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable Cell`1<System.Double>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m983731048_gshared (Cell_1_t716614246 * __this, Action_1_t682969319 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t682969319 * L_0 = ___action0;
		double L_1 = (double)__this->get_Value_2();
		NullCheck((Action_1_t682969319 *)L_0);
		((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Action_1_t682969319 *)L_0, (double)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Stream_1_t3520622932 * L_2 = (Stream_1_t3520622932 *)__this->get_update_0();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Stream_1_t3520622932 * L_3 = (Stream_1_t3520622932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_3);
	}

IL_0022:
	{
		Stream_1_t3520622932 * L_4 = (Stream_1_t3520622932 *)__this->get_update_0();
		Action_1_t682969319 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3520622932 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Double>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3520622932 *)L_4, (Action_1_t682969319 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Void Cell`1<System.Double>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputStream_m3828760844_MetadataUsageId;
extern "C"  void Cell_1_SetInputStream_m3828760844_gshared (Cell_1_t716614246 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputStream_m3828760844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t682969319 * L_4 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Double>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t682969319 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Cell`1<System.Double>::ResetInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_ResetInputStream_m3430948139_MetadataUsageId;
extern "C"  void Cell_1_ResetInputStream_m3430948139_gshared (Cell_1_t716614246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_ResetInputStream_m3430948139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Cell`1<System.Double>::TransactionIterationFinished()
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_TransactionIterationFinished_m944657729_MetadataUsageId;
extern "C"  void Cell_1_TransactionIterationFinished_m944657729_gshared (Cell_1_t716614246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_TransactionIterationFinished_m944657729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		double L_0 = (double)__this->get_holdedValue_4();
		__this->set_Value_2(L_0);
		__this->set_holdedValueIsCurrent_3((bool)0);
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_1 = V_0;
		__this->set_holdedValue_4(L_1);
		return;
	}
}
// System.Void Cell`1<System.Double>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m86094227_gshared (Cell_1_t716614246 * __this, int32_t ___p0, const MethodInfo* method)
{
	{
		Stream_1_t3520622932 * L_0 = (Stream_1_t3520622932 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		double L_1 = (double)__this->get_holdedValue_4();
		double L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_2);
		double L_4 = (double)__this->get_Value_2();
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		Stream_1_t3520622932 * L_8 = (Stream_1_t3520622932 *)__this->get_update_0();
		double L_9 = (double)__this->get_holdedValue_4();
		int32_t L_10 = ___p0;
		NullCheck((Stream_1_t3520622932 *)L_8);
		((  void (*) (Stream_1_t3520622932 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3520622932 *)L_8, (double)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean Cell`1<System.Double>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m65828006_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t716614246 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Cell`1<System.Double>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m2659237921_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t716614246 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Cell`1<System.Int32>::.ctor()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m3610261621_MetadataUsageId;
extern "C"  void Cell_1__ctor_m3610261621_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m3610261621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1<System.Int32>::.ctor(T)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m248961929_MetadataUsageId;
extern "C"  void Cell_1__ctor_m248961929_gshared (Cell_1_t3029512419 * __this, int32_t ___initial0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m248961929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_1 = ___initial0;
		__this->set_Value_2(L_1);
		return;
	}
}
// System.Void Cell`1<System.Int32>::.ctor(ICell`1<T>)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m2220808939_MetadataUsageId;
extern "C"  void Cell_1__ctor_m2220808939_gshared (Cell_1_t3029512419 * __this, Il2CppObject* ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m2220808939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___other0;
		NullCheck((Cell_1_t3029512419 *)__this);
		((  void (*) (Cell_1_t3029512419 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t3029512419 *)__this, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void Cell`1<System.Int32>::SetInputCell(ICell`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputCell_m3121518895_MetadataUsageId;
extern "C"  void Cell_1_SetInputCell_m3121518895_gshared (Cell_1_t3029512419 * __this, Il2CppObject* ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputCell_m3121518895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___cell0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t2995867492 * L_4 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Int32>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Action_1_t2995867492 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// T Cell`1<System.Int32>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_get_value_m3615970908_MetadataUsageId;
extern "C"  int32_t Cell_1_get_value_m3615970908_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_get_value_m3615970908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		bool L_1 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_holdedValue_4();
		return L_2;
	}

IL_0022:
	{
		int32_t L_3 = (int32_t)__this->get_Value_2();
		return L_3;
	}
}
// System.Void Cell`1<System.Int32>::set_value(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4175183221;
extern const uint32_t Cell_1_set_value_m1442426679_MetadataUsageId;
extern "C"  void Cell_1_set_value_m1442426679_gshared (Cell_1_t3029512419 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_set_value_m1442426679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4175183221, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_0031:
	{
		int32_t L_3 = ___value0;
		NullCheck((Cell_1_t3029512419 *)__this);
		((  void (*) (Cell_1_t3029512419 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Cell_1_t3029512419 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void Cell`1<System.Int32>::Set(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_Set_m2285895177_MetadataUsageId;
extern "C"  void Cell_1_Set_m2285895177_gshared (Cell_1_t3029512419 * __this, int32_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_Set_m2285895177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_1 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_1)
		{
			goto IL_00b3;
		}
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_holdedValue_4();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		int32_t L_6 = ___val0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_7);
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		bool L_10 = (bool)__this->get_holdedValueIsCurrent_3();
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_11 = (int32_t)__this->get_Value_2();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_12);
		int32_t L_14 = ___val0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_15);
		bool L_17 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_13, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0062;
		}
	}
	{
		return;
	}

IL_0062:
	{
		int32_t L_18 = ___val0;
		__this->set_holdedValue_4(L_18);
		__this->set_holdedValueIsCurrent_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_19 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_19)
		{
			goto IL_00ae;
		}
	}
	{
		Stream_1_t1538553809 * L_20 = (Stream_1_t1538553809 *)__this->get_update_0();
		if (!L_20)
		{
			goto IL_00ae;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_00a3;
	}

IL_0092:
	{
		Stream_1_t1538553809 * L_21 = (Stream_1_t1538553809 *)__this->get_update_0();
		int32_t L_22 = ___val0;
		int32_t L_23 = V_0;
		NullCheck((Stream_1_t1538553809 *)L_21);
		((  void (*) (Stream_1_t1538553809 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t1538553809 *)L_21, (int32_t)L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		int32_t L_26 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_currentPriorityUnpack_6();
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0092;
		}
	}

IL_00ae:
	{
		goto IL_00f2;
	}

IL_00b3:
	{
		int32_t L_27 = (int32_t)__this->get_Value_2();
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_28);
		int32_t L_30 = ___val0;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_31);
		bool L_33 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_29, (Il2CppObject *)L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00cf;
		}
	}
	{
		return;
	}

IL_00cf:
	{
		int32_t L_34 = ___val0;
		__this->set_Value_2(L_34);
		Stream_1_t1538553809 * L_35 = (Stream_1_t1538553809 *)__this->get_update_0();
		if (!L_35)
		{
			goto IL_00f2;
		}
	}
	{
		Stream_1_t1538553809 * L_36 = (Stream_1_t1538553809 *)__this->get_update_0();
		int32_t L_37 = (int32_t)__this->get_Value_2();
		NullCheck((Stream_1_t1538553809 *)L_36);
		((  void (*) (Stream_1_t1538553809 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Stream_1_t1538553809 *)L_36, (int32_t)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00f2:
	{
		return;
	}
}
// System.IDisposable Cell`1<System.Int32>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m1809460954_gshared (Cell_1_t3029512419 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyF3_t1928596255 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyF3_t1928596255 * L_0 = (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 *)L_0;
		U3COnChangedU3Ec__AnonStoreyF3_t1928596255 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Stream_1_t1538553809 * L_3 = (Stream_1_t1538553809 *)__this->get_update_0();
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		Stream_1_t1538553809 * L_4 = (Stream_1_t1538553809 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t1538553809 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_4);
	}

IL_0023:
	{
		Stream_1_t1538553809 * L_5 = (Stream_1_t1538553809 *)__this->get_update_0();
		U3COnChangedU3Ec__AnonStoreyF3_t1928596255 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t2995867492 * L_8 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_9 = ___p1;
		NullCheck((Stream_1_t1538553809 *)L_5);
		Il2CppObject * L_10 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Int32>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t1538553809 *)L_5, (Action_1_t2995867492 *)L_8, (int32_t)L_9);
		return L_10;
	}
}
// System.Object Cell`1<System.Int32>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m67557317_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t3029512419 *)__this);
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(11 /* T Cell`1<System.Int32>::get_value() */, (Cell_1_t3029512419 *)__this);
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return L_2;
	}
}
// System.IDisposable Cell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m3364826943_gshared (Cell_1_t3029512419 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t1538553809 * L_0 = (Stream_1_t1538553809 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t1538553809 * L_1 = (Stream_1_t1538553809 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t1538553809 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_1);
	}

IL_0016:
	{
		Stream_1_t1538553809 * L_2 = (Stream_1_t1538553809 *)__this->get_update_0();
		Action_1_t2995867492 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t1538553809 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Int32>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t1538553809 *)L_2, (Action_1_t2995867492 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> Cell`1<System.Int32>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m1128332835_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method)
{
	Stream_1_t1538553809 * V_0 = NULL;
	Stream_1_t1538553809 * G_B2_0 = NULL;
	Stream_1_t1538553809 * G_B1_0 = NULL;
	{
		Stream_1_t1538553809 * L_0 = (Stream_1_t1538553809 *)__this->get_update_0();
		Stream_1_t1538553809 * L_1 = (Stream_1_t1538553809 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1538553809 * L_2 = (Stream_1_t1538553809 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t1538553809 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Stream_1_t1538553809 * L_3 = (Stream_1_t1538553809 *)L_2;
		V_0 = (Stream_1_t1538553809 *)L_3;
		__this->set_update_0(L_3);
		Stream_1_t1538553809 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable Cell`1<System.Int32>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m1240558627_gshared (Cell_1_t3029512419 * __this, Action_1_t2995867492 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t2995867492 * L_0 = ___action0;
		int32_t L_1 = (int32_t)__this->get_Value_2();
		NullCheck((Action_1_t2995867492 *)L_0);
		((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Action_1_t2995867492 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Stream_1_t1538553809 * L_2 = (Stream_1_t1538553809 *)__this->get_update_0();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Stream_1_t1538553809 * L_3 = (Stream_1_t1538553809 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t1538553809 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_3);
	}

IL_0022:
	{
		Stream_1_t1538553809 * L_4 = (Stream_1_t1538553809 *)__this->get_update_0();
		Action_1_t2995867492 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t1538553809 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Int32>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t1538553809 *)L_4, (Action_1_t2995867492 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Void Cell`1<System.Int32>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputStream_m3986842283_MetadataUsageId;
extern "C"  void Cell_1_SetInputStream_m3986842283_gshared (Cell_1_t3029512419 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputStream_m3986842283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t2995867492 * L_4 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Int32>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t2995867492 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Cell`1<System.Int32>::ResetInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_ResetInputStream_m161278346_MetadataUsageId;
extern "C"  void Cell_1_ResetInputStream_m161278346_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_ResetInputStream_m161278346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Cell`1<System.Int32>::TransactionIterationFinished()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_TransactionIterationFinished_m2537065248_MetadataUsageId;
extern "C"  void Cell_1_TransactionIterationFinished_m2537065248_gshared (Cell_1_t3029512419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_TransactionIterationFinished_m2537065248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_holdedValue_4();
		__this->set_Value_2(L_0);
		__this->set_holdedValueIsCurrent_3((bool)0);
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_1 = V_0;
		__this->set_holdedValue_4(L_1);
		return;
	}
}
// System.Void Cell`1<System.Int32>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m1849483826_gshared (Cell_1_t3029512419 * __this, int32_t ___p0, const MethodInfo* method)
{
	{
		Stream_1_t1538553809 * L_0 = (Stream_1_t1538553809 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_1 = (int32_t)__this->get_holdedValue_4();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_2);
		int32_t L_4 = (int32_t)__this->get_Value_2();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		Stream_1_t1538553809 * L_8 = (Stream_1_t1538553809 *)__this->get_update_0();
		int32_t L_9 = (int32_t)__this->get_holdedValue_4();
		int32_t L_10 = ___p0;
		NullCheck((Stream_1_t1538553809 *)L_8);
		((  void (*) (Stream_1_t1538553809 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t1538553809 *)L_8, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean Cell`1<System.Int32>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m204013695_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t3029512419 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Cell`1<System.Int32>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m475462056_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t3029512419 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Cell`1<System.Int64>::.ctor()
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m403607060_MetadataUsageId;
extern "C"  void Cell_1__ctor_m403607060_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m403607060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1<System.Int64>::.ctor(T)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m3921885642_MetadataUsageId;
extern "C"  void Cell_1__ctor_m3921885642_gshared (Cell_1_t3029512514 * __this, int64_t ___initial0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m3921885642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int64_t L_1 = ___initial0;
		__this->set_Value_2(L_1);
		return;
	}
}
// System.Void Cell`1<System.Int64>::.ctor(ICell`1<T>)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m824309194_MetadataUsageId;
extern "C"  void Cell_1__ctor_m824309194_gshared (Cell_1_t3029512514 * __this, Il2CppObject* ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m824309194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___other0;
		NullCheck((Cell_1_t3029512514 *)__this);
		((  void (*) (Cell_1_t3029512514 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t3029512514 *)__this, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void Cell`1<System.Int64>::SetInputCell(ICell`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputCell_m396383600_MetadataUsageId;
extern "C"  void Cell_1_SetInputCell_m396383600_gshared (Cell_1_t3029512514 * __this, Il2CppObject* ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputCell_m396383600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___cell0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t2995867587 * L_4 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t2995867587 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867587 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Int64>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Action_1_t2995867587 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// T Cell`1<System.Int64>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_get_value_m804504699_MetadataUsageId;
extern "C"  int64_t Cell_1_get_value_m804504699_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_get_value_m804504699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		bool L_1 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		int64_t L_2 = (int64_t)__this->get_holdedValue_4();
		return L_2;
	}

IL_0022:
	{
		int64_t L_3 = (int64_t)__this->get_Value_2();
		return L_3;
	}
}
// System.Void Cell`1<System.Int64>::set_value(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4175183221;
extern const uint32_t Cell_1_set_value_m186320120_MetadataUsageId;
extern "C"  void Cell_1_set_value_m186320120_gshared (Cell_1_t3029512514 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_set_value_m186320120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4175183221, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_0031:
	{
		int64_t L_3 = ___value0;
		NullCheck((Cell_1_t3029512514 *)__this);
		((  void (*) (Cell_1_t3029512514 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Cell_1_t3029512514 *)__this, (int64_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void Cell`1<System.Int64>::Set(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_Set_m796981386_MetadataUsageId;
extern "C"  void Cell_1_Set_m796981386_gshared (Cell_1_t3029512514 * __this, int64_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_Set_m796981386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_1 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_1)
		{
			goto IL_00b3;
		}
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		int64_t L_3 = (int64_t)__this->get_holdedValue_4();
		int64_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		int64_t L_6 = ___val0;
		int64_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_7);
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		bool L_10 = (bool)__this->get_holdedValueIsCurrent_3();
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		int64_t L_11 = (int64_t)__this->get_Value_2();
		int64_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_12);
		int64_t L_14 = ___val0;
		int64_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_15);
		bool L_17 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_13, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0062;
		}
	}
	{
		return;
	}

IL_0062:
	{
		int64_t L_18 = ___val0;
		__this->set_holdedValue_4(L_18);
		__this->set_holdedValueIsCurrent_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_19 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_19)
		{
			goto IL_00ae;
		}
	}
	{
		Stream_1_t1538553904 * L_20 = (Stream_1_t1538553904 *)__this->get_update_0();
		if (!L_20)
		{
			goto IL_00ae;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_00a3;
	}

IL_0092:
	{
		Stream_1_t1538553904 * L_21 = (Stream_1_t1538553904 *)__this->get_update_0();
		int64_t L_22 = ___val0;
		int32_t L_23 = V_0;
		NullCheck((Stream_1_t1538553904 *)L_21);
		((  void (*) (Stream_1_t1538553904 *, int64_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t1538553904 *)L_21, (int64_t)L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		int32_t L_26 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_currentPriorityUnpack_6();
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0092;
		}
	}

IL_00ae:
	{
		goto IL_00f2;
	}

IL_00b3:
	{
		int64_t L_27 = (int64_t)__this->get_Value_2();
		int64_t L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_28);
		int64_t L_30 = ___val0;
		int64_t L_31 = L_30;
		Il2CppObject * L_32 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_31);
		bool L_33 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_29, (Il2CppObject *)L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00cf;
		}
	}
	{
		return;
	}

IL_00cf:
	{
		int64_t L_34 = ___val0;
		__this->set_Value_2(L_34);
		Stream_1_t1538553904 * L_35 = (Stream_1_t1538553904 *)__this->get_update_0();
		if (!L_35)
		{
			goto IL_00f2;
		}
	}
	{
		Stream_1_t1538553904 * L_36 = (Stream_1_t1538553904 *)__this->get_update_0();
		int64_t L_37 = (int64_t)__this->get_Value_2();
		NullCheck((Stream_1_t1538553904 *)L_36);
		((  void (*) (Stream_1_t1538553904 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Stream_1_t1538553904 *)L_36, (int64_t)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00f2:
	{
		return;
	}
}
// System.IDisposable Cell`1<System.Int64>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m3072437177_gshared (Cell_1_t3029512514 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyF3_t1928596350 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyF3_t1928596350 * L_0 = (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3COnChangedU3Ec__AnonStoreyF3_t1928596350 *)L_0;
		U3COnChangedU3Ec__AnonStoreyF3_t1928596350 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Stream_1_t1538553904 * L_3 = (Stream_1_t1538553904 *)__this->get_update_0();
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		Stream_1_t1538553904 * L_4 = (Stream_1_t1538553904 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t1538553904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_4);
	}

IL_0023:
	{
		Stream_1_t1538553904 * L_5 = (Stream_1_t1538553904 *)__this->get_update_0();
		U3COnChangedU3Ec__AnonStoreyF3_t1928596350 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t2995867587 * L_8 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t2995867587 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_9 = ___p1;
		NullCheck((Stream_1_t1538553904 *)L_5);
		Il2CppObject * L_10 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867587 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Int64>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t1538553904 *)L_5, (Action_1_t2995867587 *)L_8, (int32_t)L_9);
		return L_10;
	}
}
// System.Object Cell`1<System.Int64>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m2966024868_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t3029512514 *)__this);
		int64_t L_0 = VirtFuncInvoker0< int64_t >::Invoke(11 /* T Cell`1<System.Int64>::get_value() */, (Cell_1_t3029512514 *)__this);
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return L_2;
	}
}
// System.IDisposable Cell`1<System.Int64>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m2615983424_gshared (Cell_1_t3029512514 * __this, Action_1_t2995867587 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t1538553904 * L_0 = (Stream_1_t1538553904 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t1538553904 * L_1 = (Stream_1_t1538553904 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t1538553904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_1);
	}

IL_0016:
	{
		Stream_1_t1538553904 * L_2 = (Stream_1_t1538553904 *)__this->get_update_0();
		Action_1_t2995867587 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t1538553904 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867587 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Int64>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t1538553904 *)L_2, (Action_1_t2995867587 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> Cell`1<System.Int64>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m843735170_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method)
{
	Stream_1_t1538553904 * V_0 = NULL;
	Stream_1_t1538553904 * G_B2_0 = NULL;
	Stream_1_t1538553904 * G_B1_0 = NULL;
	{
		Stream_1_t1538553904 * L_0 = (Stream_1_t1538553904 *)__this->get_update_0();
		Stream_1_t1538553904 * L_1 = (Stream_1_t1538553904 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t1538553904 * L_2 = (Stream_1_t1538553904 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t1538553904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Stream_1_t1538553904 * L_3 = (Stream_1_t1538553904 *)L_2;
		V_0 = (Stream_1_t1538553904 *)L_3;
		__this->set_update_0(L_3);
		Stream_1_t1538553904 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable Cell`1<System.Int64>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m2503534850_gshared (Cell_1_t3029512514 * __this, Action_1_t2995867587 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t2995867587 * L_0 = ___action0;
		int64_t L_1 = (int64_t)__this->get_Value_2();
		NullCheck((Action_1_t2995867587 *)L_0);
		((  void (*) (Action_1_t2995867587 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Action_1_t2995867587 *)L_0, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Stream_1_t1538553904 * L_2 = (Stream_1_t1538553904 *)__this->get_update_0();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Stream_1_t1538553904 * L_3 = (Stream_1_t1538553904 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t1538553904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_3);
	}

IL_0022:
	{
		Stream_1_t1538553904 * L_4 = (Stream_1_t1538553904 *)__this->get_update_0();
		Action_1_t2995867587 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t1538553904 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t2995867587 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Int64>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t1538553904 *)L_4, (Action_1_t2995867587 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Void Cell`1<System.Int64>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputStream_m2005538412_MetadataUsageId;
extern "C"  void Cell_1_SetInputStream_m2005538412_gshared (Cell_1_t3029512514 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputStream_m2005538412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t2995867587 * L_4 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t2995867587 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867587 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Int64>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t2995867587 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Cell`1<System.Int64>::ResetInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_ResetInputStream_m4114426507_MetadataUsageId;
extern "C"  void Cell_1_ResetInputStream_m4114426507_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_ResetInputStream_m4114426507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Cell`1<System.Int64>::TransactionIterationFinished()
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_TransactionIterationFinished_m1174557345_MetadataUsageId;
extern "C"  void Cell_1_TransactionIterationFinished_m1174557345_gshared (Cell_1_t3029512514 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_TransactionIterationFinished_m1174557345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		int64_t L_0 = (int64_t)__this->get_holdedValue_4();
		__this->set_Value_2(L_0);
		__this->set_holdedValueIsCurrent_3((bool)0);
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_1 = V_0;
		__this->set_holdedValue_4(L_1);
		return;
	}
}
// System.Void Cell`1<System.Int64>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m4073776883_gshared (Cell_1_t3029512514 * __this, int32_t ___p0, const MethodInfo* method)
{
	{
		Stream_1_t1538553904 * L_0 = (Stream_1_t1538553904 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int64_t L_1 = (int64_t)__this->get_holdedValue_4();
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_2);
		int64_t L_4 = (int64_t)__this->get_Value_2();
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		Stream_1_t1538553904 * L_8 = (Stream_1_t1538553904 *)__this->get_update_0();
		int64_t L_9 = (int64_t)__this->get_holdedValue_4();
		int32_t L_10 = ___p0;
		NullCheck((Stream_1_t1538553904 *)L_8);
		((  void (*) (Stream_1_t1538553904 *, int64_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t1538553904 *)L_8, (int64_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean Cell`1<System.Int64>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m1961942878_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t3029512514 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Cell`1<System.Int64>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m2407512681_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t3029512514 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Cell`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m1701983586_MetadataUsageId;
extern "C"  void Cell_1__ctor_m1701983586_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m1701983586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1<System.Object>::.ctor(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m1221884988_MetadataUsageId;
extern "C"  void Cell_1__ctor_m1221884988_gshared (Cell_1_t1019204052 * __this, Il2CppObject * ___initial0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m1221884988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___initial0;
		__this->set_Value_2(L_1);
		return;
	}
}
// System.Void Cell`1<System.Object>::.ctor(ICell`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m3977204632_MetadataUsageId;
extern "C"  void Cell_1__ctor_m3977204632_gshared (Cell_1_t1019204052 * __this, Il2CppObject* ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m3977204632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___other0;
		NullCheck((Cell_1_t1019204052 *)__this);
		((  void (*) (Cell_1_t1019204052 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t1019204052 *)__this, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void Cell`1<System.Object>::SetInputCell(ICell`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputCell_m3952243682_MetadataUsageId;
extern "C"  void Cell_1_SetInputCell_m3952243682_gshared (Cell_1_t1019204052 * __this, Il2CppObject* ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputCell_m3952243682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___cell0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t985559125 * L_4 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Object>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Action_1_t985559125 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// T Cell`1<System.Object>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_get_value_m916594727_MetadataUsageId;
extern "C"  Il2CppObject * Cell_1_get_value_m916594727_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_get_value_m916594727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		bool L_1 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_holdedValue_4();
		return L_2;
	}

IL_0022:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_Value_2();
		return L_3;
	}
}
// System.Void Cell`1<System.Object>::set_value(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4175183221;
extern const uint32_t Cell_1_set_value_m570539626_MetadataUsageId;
extern "C"  void Cell_1_set_value_m570539626_gshared (Cell_1_t1019204052 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_set_value_m570539626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4175183221, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_0031:
	{
		Il2CppObject * L_3 = ___value0;
		NullCheck((Cell_1_t1019204052 *)__this);
		((  void (*) (Cell_1_t1019204052 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Cell_1_t1019204052 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void Cell`1<System.Object>::Set(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_Set_m1115959164_MetadataUsageId;
extern "C"  void Cell_1_Set_m1115959164_gshared (Cell_1_t1019204052 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_Set_m1115959164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_1 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_1)
		{
			goto IL_00b3;
		}
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_holdedValue_4();
		Il2CppObject * L_4 = ___val0;
		bool L_5 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		bool L_6 = (bool)__this->get_holdedValueIsCurrent_3();
		if (L_6)
		{
			goto IL_0062;
		}
	}
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_Value_2();
		Il2CppObject * L_8 = ___val0;
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0062;
		}
	}
	{
		return;
	}

IL_0062:
	{
		Il2CppObject * L_10 = ___val0;
		__this->set_holdedValue_4(L_10);
		__this->set_holdedValueIsCurrent_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_11 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_11)
		{
			goto IL_00ae;
		}
	}
	{
		Stream_1_t3823212738 * L_12 = (Stream_1_t3823212738 *)__this->get_update_0();
		if (!L_12)
		{
			goto IL_00ae;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_00a3;
	}

IL_0092:
	{
		Stream_1_t3823212738 * L_13 = (Stream_1_t3823212738 *)__this->get_update_0();
		Il2CppObject * L_14 = ___val0;
		int32_t L_15 = V_0;
		NullCheck((Stream_1_t3823212738 *)L_13);
		((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3823212738 *)L_13, (Il2CppObject *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		int32_t L_18 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_currentPriorityUnpack_6();
		if ((((int32_t)L_17) <= ((int32_t)L_18)))
		{
			goto IL_0092;
		}
	}

IL_00ae:
	{
		goto IL_00f2;
	}

IL_00b3:
	{
		Il2CppObject * L_19 = (Il2CppObject *)__this->get_Value_2();
		Il2CppObject * L_20 = ___val0;
		bool L_21 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_19, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00cf;
		}
	}
	{
		return;
	}

IL_00cf:
	{
		Il2CppObject * L_22 = ___val0;
		__this->set_Value_2(L_22);
		Stream_1_t3823212738 * L_23 = (Stream_1_t3823212738 *)__this->get_update_0();
		if (!L_23)
		{
			goto IL_00f2;
		}
	}
	{
		Stream_1_t3823212738 * L_24 = (Stream_1_t3823212738 *)__this->get_update_0();
		Il2CppObject * L_25 = (Il2CppObject *)__this->get_Value_2();
		NullCheck((Stream_1_t3823212738 *)L_24);
		((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Stream_1_t3823212738 *)L_24, (Il2CppObject *)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00f2:
	{
		return;
	}
}
// System.IDisposable Cell`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m3781809805_gshared (Cell_1_t1019204052 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyF3_t4213255184 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyF3_t4213255184 * L_0 = (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3COnChangedU3Ec__AnonStoreyF3_t4213255184 *)L_0;
		U3COnChangedU3Ec__AnonStoreyF3_t4213255184 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Stream_1_t3823212738 * L_3 = (Stream_1_t3823212738 *)__this->get_update_0();
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		Stream_1_t3823212738 * L_4 = (Stream_1_t3823212738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_4);
	}

IL_0023:
	{
		Stream_1_t3823212738 * L_5 = (Stream_1_t3823212738 *)__this->get_update_0();
		U3COnChangedU3Ec__AnonStoreyF3_t4213255184 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t985559125 * L_8 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_9 = ___p1;
		NullCheck((Stream_1_t3823212738 *)L_5);
		Il2CppObject * L_10 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3823212738 *)L_5, (Action_1_t985559125 *)L_8, (int32_t)L_9);
		return L_10;
	}
}
// System.Object Cell`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m2686689916_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t1019204052 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* T Cell`1<System.Object>::get_value() */, (Cell_1_t1019204052 *)__this);
		return L_0;
	}
}
// System.IDisposable Cell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m3691348204_gshared (Cell_1_t1019204052 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3823212738 * L_0 = (Stream_1_t3823212738 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3823212738 * L_1 = (Stream_1_t3823212738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_1);
	}

IL_0016:
	{
		Stream_1_t3823212738 * L_2 = (Stream_1_t3823212738 *)__this->get_update_0();
		Action_1_t985559125 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3823212738 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3823212738 *)L_2, (Action_1_t985559125 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> Cell`1<System.Object>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m786418834_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method)
{
	Stream_1_t3823212738 * V_0 = NULL;
	Stream_1_t3823212738 * G_B2_0 = NULL;
	Stream_1_t3823212738 * G_B1_0 = NULL;
	{
		Stream_1_t3823212738 * L_0 = (Stream_1_t3823212738 *)__this->get_update_0();
		Stream_1_t3823212738 * L_1 = (Stream_1_t3823212738 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3823212738 * L_2 = (Stream_1_t3823212738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Stream_1_t3823212738 * L_3 = (Stream_1_t3823212738 *)L_2;
		V_0 = (Stream_1_t3823212738 *)L_3;
		__this->set_update_0(L_3);
		Stream_1_t3823212738 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable Cell`1<System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m3212907478_gshared (Cell_1_t1019204052 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = ___action0;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_Value_2();
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Stream_1_t3823212738 * L_2 = (Stream_1_t3823212738 *)__this->get_update_0();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Stream_1_t3823212738 * L_3 = (Stream_1_t3823212738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_3);
	}

IL_0022:
	{
		Stream_1_t3823212738 * L_4 = (Stream_1_t3823212738 *)__this->get_update_0();
		Action_1_t985559125 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3823212738 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3823212738 *)L_4, (Action_1_t985559125 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Void Cell`1<System.Object>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputStream_m2944642014_MetadataUsageId;
extern "C"  void Cell_1_SetInputStream_m2944642014_gshared (Cell_1_t1019204052 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputStream_m2944642014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t985559125 * L_4 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t985559125 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Cell`1<System.Object>::ResetInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_ResetInputStream_m3069937277_MetadataUsageId;
extern "C"  void Cell_1_ResetInputStream_m3069937277_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_ResetInputStream_m3069937277_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Cell`1<System.Object>::TransactionIterationFinished()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_TransactionIterationFinished_m1709986707_MetadataUsageId;
extern "C"  void Cell_1_TransactionIterationFinished_m1709986707_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_TransactionIterationFinished_m1709986707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_holdedValue_4();
		__this->set_Value_2(L_0);
		__this->set_holdedValueIsCurrent_3((bool)0);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		__this->set_holdedValue_4(L_1);
		return;
	}
}
// System.Void Cell`1<System.Object>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m1047006821_gshared (Cell_1_t1019204052 * __this, int32_t ___p0, const MethodInfo* method)
{
	{
		Stream_1_t3823212738 * L_0 = (Stream_1_t3823212738 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_holdedValue_4();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_Value_2();
		bool L_3 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003e;
		}
	}
	{
		Stream_1_t3823212738 * L_4 = (Stream_1_t3823212738 *)__this->get_update_0();
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_holdedValue_4();
		int32_t L_6 = ___p0;
		NullCheck((Stream_1_t3823212738 *)L_4);
		((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3823212738 *)L_4, (Il2CppObject *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean Cell`1<System.Object>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m4088946964_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t1019204052 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Cell`1<System.Object>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m38753523_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t1019204052 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Cell`1<System.Single>::.ctor()
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m1359945515_MetadataUsageId;
extern "C"  void Cell_1__ctor_m1359945515_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m1359945515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cell`1<System.Single>::.ctor(T)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m3503606675_MetadataUsageId;
extern "C"  void Cell_1__ctor_m3503606675_gshared (Cell_1_t1140306653 * __this, float ___initial0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m3503606675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		float L_1 = ___initial0;
		__this->set_Value_2(L_1);
		return;
	}
}
// System.Void Cell`1<System.Single>::.ctor(ICell`1<T>)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1__ctor_m334321185_MetadataUsageId;
extern "C"  void Cell_1__ctor_m334321185_gshared (Cell_1_t1140306653 * __this, Il2CppObject* ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1__ctor_m334321185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		__this->set_Value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___other0;
		NullCheck((Cell_1_t1140306653 *)__this);
		((  void (*) (Cell_1_t1140306653 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Cell_1_t1140306653 *)__this, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void Cell`1<System.Single>::SetInputCell(ICell`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputCell_m1221539129_MetadataUsageId;
extern "C"  void Cell_1_SetInputCell_m1221539129_gshared (Cell_1_t1140306653 * __this, Il2CppObject* ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputCell_m1221539129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___cell0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t1106661726 * L_4 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Single>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Action_1_t1106661726 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// T Cell`1<System.Single>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_get_value_m3534945648_MetadataUsageId;
extern "C"  float Cell_1_get_value_m3534945648_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_get_value_m3534945648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		bool L_1 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		float L_2 = (float)__this->get_holdedValue_4();
		return L_2;
	}

IL_0022:
	{
		float L_3 = (float)__this->get_Value_2();
		return L_3;
	}
}
// System.Void Cell`1<System.Single>::set_value(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4175183221;
extern const uint32_t Cell_1_set_value_m135039553_MetadataUsageId;
extern "C"  void Cell_1_set_value_m135039553_gshared (Cell_1_t1140306653 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_set_value_m135039553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4175183221, /*hidden argument*/NULL);
	}

IL_0014:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_0031:
	{
		float L_3 = ___value0;
		NullCheck((Cell_1_t1140306653 *)__this);
		((  void (*) (Cell_1_t1140306653 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Cell_1_t1140306653 *)__this, (float)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void Cell`1<System.Single>::Set(T)
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_Set_m2351851667_MetadataUsageId;
extern "C"  void Cell_1_Set_m2351851667_gshared (Cell_1_t1140306653 * __this, float ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_Set_m2351851667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_1 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_1)
		{
			goto IL_00b3;
		}
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_holdedValueIsCurrent_3();
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		float L_3 = (float)__this->get_holdedValue_4();
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		float L_6 = ___val0;
		float L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_7);
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		bool L_10 = (bool)__this->get_holdedValueIsCurrent_3();
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		float L_11 = (float)__this->get_Value_2();
		float L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_12);
		float L_14 = ___val0;
		float L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_15);
		bool L_17 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_13, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0062;
		}
	}
	{
		return;
	}

IL_0062:
	{
		float L_18 = ___val0;
		__this->set_holdedValue_4(L_18);
		__this->set_holdedValueIsCurrent_3((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_19 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_19)
		{
			goto IL_00ae;
		}
	}
	{
		Stream_1_t3944315339 * L_20 = (Stream_1_t3944315339 *)__this->get_update_0();
		if (!L_20)
		{
			goto IL_00ae;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_00a3;
	}

IL_0092:
	{
		Stream_1_t3944315339 * L_21 = (Stream_1_t3944315339 *)__this->get_update_0();
		float L_22 = ___val0;
		int32_t L_23 = V_0;
		NullCheck((Stream_1_t3944315339 *)L_21);
		((  void (*) (Stream_1_t3944315339 *, float, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3944315339 *)L_21, (float)L_22, (int32_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_24 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		int32_t L_26 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_currentPriorityUnpack_6();
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_0092;
		}
	}

IL_00ae:
	{
		goto IL_00f2;
	}

IL_00b3:
	{
		float L_27 = (float)__this->get_Value_2();
		float L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_28);
		float L_30 = ___val0;
		float L_31 = L_30;
		Il2CppObject * L_32 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_31);
		bool L_33 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_29, (Il2CppObject *)L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00cf;
		}
	}
	{
		return;
	}

IL_00cf:
	{
		float L_34 = ___val0;
		__this->set_Value_2(L_34);
		Stream_1_t3944315339 * L_35 = (Stream_1_t3944315339 *)__this->get_update_0();
		if (!L_35)
		{
			goto IL_00f2;
		}
	}
	{
		Stream_1_t3944315339 * L_36 = (Stream_1_t3944315339 *)__this->get_update_0();
		float L_37 = (float)__this->get_Value_2();
		NullCheck((Stream_1_t3944315339 *)L_36);
		((  void (*) (Stream_1_t3944315339 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Stream_1_t3944315339 *)L_36, (float)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00f2:
	{
		return;
	}
}
// System.IDisposable Cell`1<System.Single>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m183215894_gshared (Cell_1_t1140306653 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStoreyF3_t39390489 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStoreyF3_t39390489 * L_0 = (U3COnChangedU3Ec__AnonStoreyF3_t39390489 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t39390489 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3COnChangedU3Ec__AnonStoreyF3_t39390489 *)L_0;
		U3COnChangedU3Ec__AnonStoreyF3_t39390489 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Stream_1_t3944315339 * L_3 = (Stream_1_t3944315339 *)__this->get_update_0();
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		Stream_1_t3944315339 * L_4 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_4);
	}

IL_0023:
	{
		Stream_1_t3944315339 * L_5 = (Stream_1_t3944315339 *)__this->get_update_0();
		U3COnChangedU3Ec__AnonStoreyF3_t39390489 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t1106661726 * L_8 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_9 = ___p1;
		NullCheck((Stream_1_t3944315339 *)L_5);
		Il2CppObject * L_10 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3944315339 *)L_5, (Action_1_t1106661726 *)L_8, (int32_t)L_9);
		return L_10;
	}
}
// System.Object Cell`1<System.Single>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m3338773765_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method)
{
	{
		NullCheck((Cell_1_t1140306653 *)__this);
		float L_0 = VirtFuncInvoker0< float >::Invoke(11 /* T Cell`1<System.Single>::get_value() */, (Cell_1_t1140306653 *)__this);
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return L_2;
	}
}
// System.IDisposable Cell`1<System.Single>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m987445891_gshared (Cell_1_t1140306653 * __this, Action_1_t1106661726 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3944315339 * L_1 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_1);
	}

IL_0016:
	{
		Stream_1_t3944315339 * L_2 = (Stream_1_t3944315339 *)__this->get_update_0();
		Action_1_t1106661726 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3944315339 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3944315339 *)L_2, (Action_1_t1106661726 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> Cell`1<System.Single>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m170818459_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method)
{
	Stream_1_t3944315339 * V_0 = NULL;
	Stream_1_t3944315339 * G_B2_0 = NULL;
	Stream_1_t3944315339 * G_B1_0 = NULL;
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_0();
		Stream_1_t3944315339 * L_1 = (Stream_1_t3944315339 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3944315339 * L_2 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Stream_1_t3944315339 * L_3 = (Stream_1_t3944315339 *)L_2;
		V_0 = (Stream_1_t3944315339 *)L_3;
		__this->set_update_0(L_3);
		Stream_1_t3944315339 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// System.IDisposable Cell`1<System.Single>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m3909280863_gshared (Cell_1_t1140306653 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Action_1_t1106661726 * L_0 = ___action0;
		float L_1 = (float)__this->get_Value_2();
		NullCheck((Action_1_t1106661726 *)L_0);
		((  void (*) (Action_1_t1106661726 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Action_1_t1106661726 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Stream_1_t3944315339 * L_2 = (Stream_1_t3944315339 *)__this->get_update_0();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Stream_1_t3944315339 * L_3 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		__this->set_update_0(L_3);
	}

IL_0022:
	{
		Stream_1_t3944315339 * L_4 = (Stream_1_t3944315339 *)__this->get_update_0();
		Action_1_t1106661726 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3944315339 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3944315339 *)L_4, (Action_1_t1106661726 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Void Cell`1<System.Single>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_SetInputStream_m3007441333_MetadataUsageId;
extern "C"  void Cell_1_SetInputStream_m3007441333_gshared (Cell_1_t1140306653 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_SetInputStream_m3007441333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t1106661726 * L_4 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t1106661726 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Cell`1<System.Single>::ResetInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_ResetInputStream_m1809700116_MetadataUsageId;
extern "C"  void Cell_1_ResetInputStream_m1809700116_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_ResetInputStream_m1809700116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Cell`1<System.Single>::TransactionIterationFinished()
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Cell_1_TransactionIterationFinished_m1930590122_MetadataUsageId;
extern "C"  void Cell_1_TransactionIterationFinished_m1930590122_gshared (Cell_1_t1140306653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Cell_1_TransactionIterationFinished_m1930590122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = (float)__this->get_holdedValue_4();
		__this->set_Value_2(L_0);
		__this->set_holdedValueIsCurrent_3((bool)0);
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_1 = V_0;
		__this->set_holdedValue_4(L_1);
		return;
	}
}
// System.Void Cell`1<System.Single>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m1139872572_gshared (Cell_1_t1140306653 * __this, int32_t ___p0, const MethodInfo* method)
{
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_0();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_1 = (float)__this->get_holdedValue_4();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_2);
		float L_4 = (float)__this->get_Value_2();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		Stream_1_t3944315339 * L_8 = (Stream_1_t3944315339 *)__this->get_update_0();
		float L_9 = (float)__this->get_holdedValue_4();
		int32_t L_10 = ___p0;
		NullCheck((Stream_1_t3944315339 *)L_8);
		((  void (*) (Stream_1_t3944315339 *, float, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Stream_1_t3944315339 *)L_8, (float)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean Cell`1<System.Single>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m713162653_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t1140306653 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Cell`1<System.Single>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m3002547658_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t1140306653 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAccumulateU3Ec__AnonStoreyFC_2__ctor_m987777597_gshared (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2<System.Object,System.Object>::<>m__191(T2)
extern "C"  void U3CAccumulateU3Ec__AnonStoreyFC_2_U3CU3Em__191_m2024471559_gshared (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * L_0 = (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 *)__this->get_U3CU3Ef__refU24251_1();
		NullCheck(L_0);
		Func_3_t1892209229 * L_1 = (Func_3_t1892209229 *)L_0->get_accomulator_1();
		U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * L_2 = (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 *)__this->get_U3CU3Ef__refU24251_1();
		NullCheck(L_2);
		MapDisposable_t304581564 * L_3 = (MapDisposable_t304581564 *)L_2->get_disp_2();
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_last_1();
		Il2CppObject * L_5 = ___val0;
		NullCheck((Func_3_t1892209229 *)L_1);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_3_t1892209229 *)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))), (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (Il2CppObject *)L_6;
		Il2CppObject * L_7 = V_0;
		U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * L_8 = (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 *)__this->get_U3CU3Ef__refU24251_1();
		NullCheck(L_8);
		MapDisposable_t304581564 * L_9 = (MapDisposable_t304581564 *)L_8->get_disp_2();
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_last_1();
		bool L_11 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_7, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0048;
		}
	}
	{
		return;
	}

IL_0048:
	{
		U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * L_12 = (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 *)__this->get_U3CU3Ef__refU24251_1();
		NullCheck(L_12);
		MapDisposable_t304581564 * L_13 = (MapDisposable_t304581564 *)L_12->get_disp_2();
		Il2CppObject * L_14 = V_0;
		NullCheck(L_13);
		L_13->set_last_1(L_14);
		Action_1_t985559125 * L_15 = (Action_1_t985559125 *)__this->get_reaction_0();
		Il2CppObject * L_16 = V_0;
		NullCheck((Action_1_t985559125 *)L_15);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_15, (Il2CppObject *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAccumulateU3Ec__AnonStoreyFB_2__ctor_m357651446_gshared (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>::<>m__171(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CAccumulateU3Ec__AnonStoreyFB_2_U3CU3Em__171_m1385176897_gshared (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * V_0 = NULL;
	{
		U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * L_0 = (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 *)L_0;
		U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24251_1(__this);
		U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		MapDisposable_t304581564 * L_4 = (MapDisposable_t304581564 *)__this->get_disp_2();
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_stream_0();
		U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_8 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_9 = ___p1;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_10 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5, (Action_1_t985559125 *)L_8, (int32_t)L_9);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_4);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_4, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		MapDisposable_t304581564 * L_11 = (MapDisposable_t304581564 *)__this->get_disp_2();
		return L_11;
	}
}
// T CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>::<>m__172()
extern "C"  Il2CppObject * U3CAccumulateU3Ec__AnonStoreyFB_2_U3CU3Em__172_m1414540942_gshared (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * __this, const MethodInfo* method)
{
	{
		MapDisposable_t304581564 * L_0 = (MapDisposable_t304581564 *)__this->get_disp_2();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_last_1();
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)));
	}
}
// System.Void CellReactiveApi/<AsObservable>c__AnonStorey106`1<System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey106_1__ctor_m591083181_gshared (U3CAsObservableU3Ec__AnonStorey106_1_t706635991 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T CellReactiveApi/<AsObservable>c__AnonStorey106`1<System.Object>::<>m__17B()
extern "C"  Il2CppObject * U3CAsObservableU3Ec__AnonStorey106_1_U3CU3Em__17B_m28013929_gshared (U3CAsObservableU3Ec__AnonStorey106_1_t706635991 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_cell_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void CellReactiveApi/<AsStream>c__AnonStorey105`1<System.Object>::.ctor()
extern "C"  void U3CAsStreamU3Ec__AnonStorey105_1__ctor_m1617263759_gshared (U3CAsStreamU3Ec__AnonStorey105_1_t3800865897 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T CellReactiveApi/<AsStream>c__AnonStorey105`1<System.Object>::<>m__17A()
extern "C"  Il2CppObject * U3CAsStreamU3Ec__AnonStorey105_1_U3CU3Em__17A_m3302523142_gshared (U3CAsStreamU3Ec__AnonStorey105_1_t3800865897 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_cell_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1<System.Object>::.ctor()
extern "C"  void U3CBindHistoricU3Ec__AnonStoreyFD_1__ctor_m766171871_gshared (U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1<System.Object>::<>m__173(T)
extern "C"  void U3CBindHistoricU3Ec__AnonStoreyFD_1_U3CU3Em__173_m3255384503_gshared (U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Action_2_t4105459918 * L_0 = (Action_2_t4105459918 *)__this->get_reaction_0();
		Carrier_1_t2723215674 * L_1 = (Carrier_1_t2723215674 *)__this->get_disp_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)L_1->get_val_0();
		Il2CppObject * L_3 = ___val0;
		NullCheck((Action_2_t4105459918 *)L_0);
		((  void (*) (Action_2_t4105459918 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_2_t4105459918 *)L_0, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Carrier_1_t2723215674 * L_4 = (Carrier_1_t2723215674 *)__this->get_disp_1();
		Il2CppObject * L_5 = ___val0;
		NullCheck(L_4);
		L_4->set_val_0(L_5);
		return;
	}
}
// System.Void CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1<System.Object>::.ctor()
extern "C"  void U3CEachNotNullU3Ec__AnonStorey104_1__ctor_m3035201695_gshared (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1<System.Object>::<>m__193(T)
extern "C"  void U3CEachNotNullU3Ec__AnonStorey104_1_U3CU3Em__193_m2585472633_gshared (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___val0;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)__this->get_act_0();
		Il2CppObject * L_2 = ___val0;
		NullCheck((Action_1_t985559125 *)L_1);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t985559125 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0017:
	{
		return;
	}
}
// System.Void CellReactiveApi/<EachNotNull>c__AnonStorey103`1<System.Object>::.ctor()
extern "C"  void U3CEachNotNullU3Ec__AnonStorey103_1__ctor_m2392627342_gshared (U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<EachNotNull>c__AnonStorey103`1<System.Object>::<>m__179(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CEachNotNullU3Ec__AnonStorey103_1_U3CU3Em__179_m811569745_gshared (U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 * __this, Action_1_t985559125 * ___act0, int32_t ___p1, const MethodInfo* method)
{
	U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * V_0 = NULL;
	{
		U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * L_0 = (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 *)L_0;
		U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24259_1(__this);
		U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___act0;
		NullCheck(L_2);
		L_2->set_act_0(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_cell_0();
		U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_8 = ___p1;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Object>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_4, (Action_1_t985559125 *)L_7, (int32_t)L_8);
		return L_9;
	}
}
// System.Void CellReactiveApi/<FaceControl>c__AnonStorey101`1<System.Object>::.ctor()
extern "C"  void U3CFaceControlU3Ec__AnonStorey101_1__ctor_m3255302693_gshared (U3CFaceControlU3Ec__AnonStorey101_1_t4266084863 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T CellReactiveApi/<FaceControl>c__AnonStorey101`1<System.Object>::<>m__177(T)
extern "C"  Il2CppObject * U3CFaceControlU3Ec__AnonStorey101_1_U3CU3Em__177_m165187226_gshared (U3CFaceControlU3Ec__AnonStorey101_1_t4266084863 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	Il2CppObject * G_B3_0 = NULL;
	{
		Func_2_t1509682273 * L_0 = (Func_2_t1509682273 *)__this->get_predicate_0();
		Il2CppObject * L_1 = ___val0;
		NullCheck((Func_2_t1509682273 *)L_0);
		bool L_2 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1509682273 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_3 = ___val0;
		G_B3_0 = L_3;
		goto IL_001d;
	}

IL_0017:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_orphan_1();
		G_B3_0 = L_4;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// System.Void CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1<System.Object>::.ctor()
extern "C"  void U3CHoldU3Ec__AnonStorey100_1__ctor_m3705864343_gshared (U3CHoldU3Ec__AnonStorey100_1_t2662411751 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1<System.Object>::<>m__192(T)
extern "C"  void U3CHoldU3Ec__AnonStorey100_1_U3CU3Em__192_m685997586_gshared (U3CHoldU3Ec__AnonStorey100_1_t2662411751 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___val0;
		U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * L_1 = (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 *)__this->get_U3CU3Ef__refU24255_1();
		NullCheck(L_1);
		MapDisposable_t304581564 * L_2 = (MapDisposable_t304581564 *)L_1->get_disp_1();
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_last_1();
		bool L_4 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * L_5 = (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 *)__this->get_U3CU3Ef__refU24255_1();
		NullCheck(L_5);
		MapDisposable_t304581564 * L_6 = (MapDisposable_t304581564 *)L_5->get_disp_1();
		Il2CppObject * L_7 = ___val0;
		NullCheck(L_6);
		L_6->set_last_1(L_7);
		Action_1_t985559125 * L_8 = (Action_1_t985559125 *)__this->get_reaction_0();
		Il2CppObject * L_9 = ___val0;
		NullCheck((Action_1_t985559125 *)L_8);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t985559125 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0042:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>::.ctor()
extern "C"  void U3CHoldU3Ec__AnonStoreyFF_1__ctor_m2711189924_gshared (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>::<>m__175(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CHoldU3Ec__AnonStoreyFF_1_U3CU3Em__175_m1395136363_gshared (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CHoldU3Ec__AnonStorey100_1_t2662411751 * V_0 = NULL;
	{
		U3CHoldU3Ec__AnonStorey100_1_t2662411751 * L_0 = (U3CHoldU3Ec__AnonStorey100_1_t2662411751 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CHoldU3Ec__AnonStorey100_1_t2662411751 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CHoldU3Ec__AnonStorey100_1_t2662411751 *)L_0;
		U3CHoldU3Ec__AnonStorey100_1_t2662411751 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24255_1(__this);
		U3CHoldU3Ec__AnonStorey100_1_t2662411751 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		MapDisposable_t304581564 * L_4 = (MapDisposable_t304581564 *)__this->get_disp_1();
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_stream_0();
		U3CHoldU3Ec__AnonStorey100_1_t2662411751 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_8 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_9 = ___p1;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_10 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5, (Action_1_t985559125 *)L_8, (int32_t)L_9);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_4);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_4, (Il2CppObject *)L_10, /*hidden argument*/NULL);
		MapDisposable_t304581564 * L_11 = (MapDisposable_t304581564 *)__this->get_disp_1();
		return L_11;
	}
}
// T CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>::<>m__176()
extern "C"  Il2CppObject * U3CHoldU3Ec__AnonStoreyFF_1_U3CU3Em__176_m512190116_gshared (U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * __this, const MethodInfo* method)
{
	{
		MapDisposable_t304581564 * L_0 = (MapDisposable_t304581564 *)__this->get_disp_1();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_last_1();
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)));
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Int32>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey115_1__ctor_m3137183832_gshared (U3CJoinU3Ec__AnonStorey115_1_t3718487911 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Int32>::<>m__19C(ICell`1<T>)
extern "C"  void U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19C_m1744712434_gshared (U3CJoinU3Ec__AnonStorey115_1_t3718487911 * __this, Il2CppObject* ___innerCell0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleAssignmentDisposable_t1832432170 * L_0 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_0();
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_0);
		bool L_1 = SingleAssignmentDisposable_get_IsDisposed_m2877825417((SingleAssignmentDisposable_t1832432170 *)L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_005a;
		}
	}
	{
		Il2CppObject* L_2 = ___innerCell0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2);
		V_0 = (int32_t)L_3;
		CellJoinDisposable_1_t1451834474 * L_4 = (CellJoinDisposable_1_t1451834474 *)__this->get_group_1();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		int32_t L_6 = V_0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_7);
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_004f;
		}
	}
	{
		Action_1_t2995867492 * L_10 = (Action_1_t2995867492 *)__this->get_reaction_2();
		int32_t L_11 = V_0;
		NullCheck((Action_1_t2995867492 *)L_10);
		((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t2995867492 *)L_10, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		CellJoinDisposable_1_t1451834474 * L_12 = (CellJoinDisposable_1_t1451834474 *)__this->get_group_1();
		int32_t L_13 = V_0;
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_14);
		NullCheck(L_12);
		L_12->set_lastValue_2(L_15);
	}

IL_004f:
	{
		SingleAssignmentDisposable_t1832432170 * L_16 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_0();
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_16);
		VirtActionInvoker0::Invoke(4 /* System.Void CellUtils.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t1832432170 *)L_16);
	}

IL_005a:
	{
		SingleAssignmentDisposable_t1832432170 * L_17 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_0();
		Il2CppObject* L_18 = ___innerCell0;
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t2995867492 * L_20 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_20, (Il2CppObject *)__this, (IntPtr_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_21 = (int32_t)__this->get_p_3();
		NullCheck((Il2CppObject*)L_18);
		Il2CppObject * L_22 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_18, (Action_1_t2995867492 *)L_20, (int32_t)L_21);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_17);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_17, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Int32>::<>m__19D(T)
extern "C"  void U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19D_m893289473_gshared (U3CJoinU3Ec__AnonStorey115_1_t3718487911 * __this, int32_t ___val0, const MethodInfo* method)
{
	{
		Action_1_t2995867492 * L_0 = (Action_1_t2995867492 *)__this->get_reaction_2();
		int32_t L_1 = ___val0;
		NullCheck((Action_1_t2995867492 *)L_0);
		((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t2995867492 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		CellJoinDisposable_1_t1451834474 * L_2 = (CellJoinDisposable_1_t1451834474 *)__this->get_group_1();
		int32_t L_3 = ___val0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_4);
		NullCheck(L_2);
		L_2->set_lastValue_2(L_5);
		return;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey115_1__ctor_m4216441311_gshared (U3CJoinU3Ec__AnonStorey115_1_t1708179544 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>::<>m__19C(ICell`1<T>)
extern "C"  void U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19C_m4050255051_gshared (U3CJoinU3Ec__AnonStorey115_1_t1708179544 * __this, Il2CppObject* ___innerCell0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		SingleAssignmentDisposable_t1832432170 * L_0 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_0();
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_0);
		bool L_1 = SingleAssignmentDisposable_get_IsDisposed_m2877825417((SingleAssignmentDisposable_t1832432170 *)L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_005a;
		}
	}
	{
		Il2CppObject* L_2 = ___innerCell0;
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2);
		V_0 = (Il2CppObject *)L_3;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_group_1();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		Il2CppObject * L_6 = V_0;
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004f;
		}
	}
	{
		Action_1_t985559125 * L_8 = (Action_1_t985559125 *)__this->get_reaction_2();
		Il2CppObject * L_9 = V_0;
		NullCheck((Action_1_t985559125 *)L_8);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		CellJoinDisposable_1_t3736493403 * L_10 = (CellJoinDisposable_1_t3736493403 *)__this->get_group_1();
		Il2CppObject * L_11 = V_0;
		NullCheck(L_10);
		L_10->set_lastValue_2(L_11);
	}

IL_004f:
	{
		SingleAssignmentDisposable_t1832432170 * L_12 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_0();
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_12);
		VirtActionInvoker0::Invoke(4 /* System.Void CellUtils.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t1832432170 *)L_12);
	}

IL_005a:
	{
		SingleAssignmentDisposable_t1832432170 * L_13 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_0();
		Il2CppObject* L_14 = ___innerCell0;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_1_t985559125 * L_16 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_16, (Il2CppObject *)__this, (IntPtr_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_17 = (int32_t)__this->get_p_3();
		NullCheck((Il2CppObject*)L_14);
		Il2CppObject * L_18 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_14, (Action_1_t985559125 *)L_16, (int32_t)L_17);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>::<>m__19D(T)
extern "C"  void U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19D_m1959039496_gshared (U3CJoinU3Ec__AnonStorey115_1_t1708179544 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_reaction_2();
		Il2CppObject * L_1 = ___val0;
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		CellJoinDisposable_1_t3736493403 * L_2 = (CellJoinDisposable_1_t3736493403 *)__this->get_group_1();
		Il2CppObject * L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_lastValue_2(L_3);
		return;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey114_1__ctor_m3246169122_gshared (U3CJoinU3Ec__AnonStorey114_1_t4163512910 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>::<>m__188(System.Action`1<T>,Priority)
extern Il2CppClass* SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var;
extern const uint32_t U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m266671239_MetadataUsageId;
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m266671239_gshared (U3CJoinU3Ec__AnonStorey114_1_t4163512910 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m266671239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t1832432170 * V_0 = NULL;
	Action_1_t252531173 * V_1 = NULL;
	U3CJoinU3Ec__AnonStorey115_1_t3718487911 * V_2 = NULL;
	CellJoinDisposable_1_t1451834474 * V_3 = NULL;
	{
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_0 = (U3CJoinU3Ec__AnonStorey115_1_t3718487911 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CJoinU3Ec__AnonStorey115_1_t3718487911 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_2 = (U3CJoinU3Ec__AnonStorey115_1_t3718487911 *)L_0;
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_1 = V_2;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24276_4(__this);
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_2 = V_2;
		Action_1_t2995867492 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_2(L_3);
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_4 = V_2;
		int32_t L_5 = ___p1;
		NullCheck(L_4);
		L_4->set_p_3(L_5);
		SingleAssignmentDisposable_t1832432170 * L_6 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_6, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t1832432170 *)L_6;
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_7 = V_2;
		SingleAssignmentDisposable_t1832432170 * L_8 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_inner_0(L_8);
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_9 = V_2;
		CellJoinDisposable_1_t1451834474 * L_10 = (CellJoinDisposable_1_t1451834474 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (CellJoinDisposable_1_t1451834474 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_3 = (CellJoinDisposable_1_t1451834474 *)L_10;
		CellJoinDisposable_1_t1451834474 * L_11 = V_3;
		SingleAssignmentDisposable_t1832432170 * L_12 = V_0;
		NullCheck((ListDisposable_t2393830995 *)L_11);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_11, (Il2CppObject *)L_12);
		CellJoinDisposable_1_t1451834474 * L_13 = V_3;
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_14 = V_2;
		NullCheck(L_14);
		SingleAssignmentDisposable_t1832432170 * L_15 = (SingleAssignmentDisposable_t1832432170 *)L_14->get_inner_0();
		NullCheck((ListDisposable_t2393830995 *)L_13);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_13, (Il2CppObject *)L_15);
		CellJoinDisposable_1_t1451834474 * L_16 = V_3;
		NullCheck(L_9);
		L_9->set_group_1(L_16);
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_17 = V_2;
		NullCheck(L_17);
		CellJoinDisposable_1_t1451834474 * L_18 = (CellJoinDisposable_1_t1451834474 *)L_17->get_group_1();
		Il2CppObject* L_19 = (Il2CppObject*)__this->get_cell_0();
		NullCheck((Il2CppObject*)L_19);
		Il2CppObject* L_20 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(2 /* T ICell`1<ICell`1<System.Int32>>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_19);
		NullCheck((Il2CppObject*)L_20);
		int32_t L_21 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_20);
		int32_t L_22 = L_21;
		Il2CppObject * L_23 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_22);
		NullCheck(L_18);
		L_18->set_lastValue_2(L_23);
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_24 = V_2;
		IntPtr_t L_25;
		L_25.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Action_1_t252531173 * L_26 = (Action_1_t252531173 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (Action_1_t252531173 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_26, (Il2CppObject *)L_24, (IntPtr_t)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_1 = (Action_1_t252531173 *)L_26;
		Action_1_t252531173 * L_27 = V_1;
		Il2CppObject* L_28 = (Il2CppObject*)__this->get_cell_0();
		NullCheck((Il2CppObject*)L_28);
		Il2CppObject* L_29 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(2 /* T ICell`1<ICell`1<System.Int32>>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_28);
		NullCheck((Action_1_t252531173 *)L_27);
		((  void (*) (Action_1_t252531173 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Action_1_t252531173 *)L_27, (Il2CppObject*)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		SingleAssignmentDisposable_t1832432170 * L_30 = V_0;
		Il2CppObject* L_31 = (Il2CppObject*)__this->get_cell_0();
		Action_1_t252531173 * L_32 = V_1;
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_33 = V_2;
		NullCheck(L_33);
		int32_t L_34 = (int32_t)L_33->get_p_3();
		NullCheck((Il2CppObject*)L_31);
		Il2CppObject * L_35 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t252531173 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<ICell`1<System.Int32>>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_31, (Action_1_t252531173 *)L_32, (int32_t)L_34);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_30);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_30, (Il2CppObject *)L_35, /*hidden argument*/NULL);
		U3CJoinU3Ec__AnonStorey115_1_t3718487911 * L_36 = V_2;
		NullCheck(L_36);
		CellJoinDisposable_1_t1451834474 * L_37 = (CellJoinDisposable_1_t1451834474 *)L_36->get_group_1();
		return L_37;
	}
}
// T CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>::<>m__189()
extern "C"  int32_t U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__189_m3748842248_gshared (U3CJoinU3Ec__AnonStorey114_1_t4163512910 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_cell_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(2 /* T ICell`1<ICell`1<System.Int32>>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey114_1__ctor_m3300018005_gshared (U3CJoinU3Ec__AnonStorey114_1_t2153204543 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>::<>m__188(System.Action`1<T>,Priority)
extern Il2CppClass* SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var;
extern const uint32_t U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m1965070592_MetadataUsageId;
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m1965070592_gshared (U3CJoinU3Ec__AnonStorey114_1_t2153204543 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m1965070592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t1832432170 * V_0 = NULL;
	Action_1_t2537190102 * V_1 = NULL;
	U3CJoinU3Ec__AnonStorey115_1_t1708179544 * V_2 = NULL;
	CellJoinDisposable_1_t3736493403 * V_3 = NULL;
	{
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_0 = (U3CJoinU3Ec__AnonStorey115_1_t1708179544 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CJoinU3Ec__AnonStorey115_1_t1708179544 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_2 = (U3CJoinU3Ec__AnonStorey115_1_t1708179544 *)L_0;
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_1 = V_2;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24276_4(__this);
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_2 = V_2;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_2(L_3);
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_4 = V_2;
		int32_t L_5 = ___p1;
		NullCheck(L_4);
		L_4->set_p_3(L_5);
		SingleAssignmentDisposable_t1832432170 * L_6 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_6, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t1832432170 *)L_6;
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_7 = V_2;
		SingleAssignmentDisposable_t1832432170 * L_8 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_inner_0(L_8);
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_9 = V_2;
		CellJoinDisposable_1_t3736493403 * L_10 = (CellJoinDisposable_1_t3736493403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_3 = (CellJoinDisposable_1_t3736493403 *)L_10;
		CellJoinDisposable_1_t3736493403 * L_11 = V_3;
		SingleAssignmentDisposable_t1832432170 * L_12 = V_0;
		NullCheck((ListDisposable_t2393830995 *)L_11);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_11, (Il2CppObject *)L_12);
		CellJoinDisposable_1_t3736493403 * L_13 = V_3;
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_14 = V_2;
		NullCheck(L_14);
		SingleAssignmentDisposable_t1832432170 * L_15 = (SingleAssignmentDisposable_t1832432170 *)L_14->get_inner_0();
		NullCheck((ListDisposable_t2393830995 *)L_13);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_13, (Il2CppObject *)L_15);
		CellJoinDisposable_1_t3736493403 * L_16 = V_3;
		NullCheck(L_9);
		L_9->set_group_1(L_16);
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_17 = V_2;
		NullCheck(L_17);
		CellJoinDisposable_1_t3736493403 * L_18 = (CellJoinDisposable_1_t3736493403 *)L_17->get_group_1();
		Il2CppObject* L_19 = (Il2CppObject*)__this->get_cell_0();
		NullCheck((Il2CppObject*)L_19);
		Il2CppObject* L_20 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(2 /* T ICell`1<ICell`1<System.Object>>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_19);
		NullCheck((Il2CppObject*)L_20);
		Il2CppObject * L_21 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_20);
		NullCheck(L_18);
		L_18->set_lastValue_2(L_21);
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_22 = V_2;
		IntPtr_t L_23;
		L_23.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Action_1_t2537190102 * L_24 = (Action_1_t2537190102 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (Action_1_t2537190102 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_24, (Il2CppObject *)L_22, (IntPtr_t)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_1 = (Action_1_t2537190102 *)L_24;
		Action_1_t2537190102 * L_25 = V_1;
		Il2CppObject* L_26 = (Il2CppObject*)__this->get_cell_0();
		NullCheck((Il2CppObject*)L_26);
		Il2CppObject* L_27 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(2 /* T ICell`1<ICell`1<System.Object>>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_26);
		NullCheck((Action_1_t2537190102 *)L_25);
		((  void (*) (Action_1_t2537190102 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Action_1_t2537190102 *)L_25, (Il2CppObject*)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		SingleAssignmentDisposable_t1832432170 * L_28 = V_0;
		Il2CppObject* L_29 = (Il2CppObject*)__this->get_cell_0();
		Action_1_t2537190102 * L_30 = V_1;
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_31 = V_2;
		NullCheck(L_31);
		int32_t L_32 = (int32_t)L_31->get_p_3();
		NullCheck((Il2CppObject*)L_29);
		Il2CppObject * L_33 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2537190102 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<ICell`1<System.Object>>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_29, (Action_1_t2537190102 *)L_30, (int32_t)L_32);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_28);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_28, (Il2CppObject *)L_33, /*hidden argument*/NULL);
		U3CJoinU3Ec__AnonStorey115_1_t1708179544 * L_34 = V_2;
		NullCheck(L_34);
		CellJoinDisposable_1_t3736493403 * L_35 = (CellJoinDisposable_1_t3736493403 *)L_34->get_group_1();
		return L_35;
	}
}
// T CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>::<>m__189()
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__189_m3577530199_gshared (U3CJoinU3Ec__AnonStorey114_1_t2153204543 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_cell_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(2 /* T ICell`1<ICell`1<System.Object>>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey117`1/<Join>c__AnonStorey116`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey116_1__ctor_m3190598077_gshared (U3CJoinU3Ec__AnonStorey116_1_t1263154545 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey117`1/<Join>c__AnonStorey116`1<System.Object>::<>m__19E(IStream`1<T>)
extern "C"  void U3CJoinU3Ec__AnonStorey116_1_U3CU3Em__19E_m1995501581_gshared (U3CJoinU3Ec__AnonStorey116_1_t1263154545 * __this, Il2CppObject* ___innerStream0, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t1832432170 * L_0 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_0();
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void CellUtils.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t1832432170 *)L_0);
		Il2CppObject* L_1 = ___innerStream0;
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		SingleAssignmentDisposable_t1832432170 * L_2 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_0();
		Il2CppObject* L_3 = ___innerStream0;
		Action_1_t985559125 * L_4 = (Action_1_t985559125 *)__this->get_reaction_1();
		int32_t L_5 = (int32_t)__this->get_p_2();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_6 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_3, (Action_1_t985559125 *)L_4, (int32_t)L_5);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_2);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_2, (Il2CppObject *)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Join>c__AnonStorey117`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey117_1__ctor_m1889892760_gshared (U3CJoinU3Ec__AnonStorey117_1_t818129546 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Join>c__AnonStorey117`1<System.Object>::<>m__18A(System.Action`1<T>,Priority)
extern Il2CppClass* SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var;
extern const uint32_t U3CJoinU3Ec__AnonStorey117_1_U3CU3Em__18A_m3958291610_MetadataUsageId;
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey117_1_U3CU3Em__18A_m3958291610_gshared (U3CJoinU3Ec__AnonStorey117_1_t818129546 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CJoinU3Ec__AnonStorey117_1_U3CU3Em__18A_m3958291610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t1832432170 * V_0 = NULL;
	CellJoinDisposable_1_t3736493403 * V_1 = NULL;
	Action_1_t1538250116 * V_2 = NULL;
	U3CJoinU3Ec__AnonStorey116_1_t1263154545 * V_3 = NULL;
	CellJoinDisposable_1_t3736493403 * V_4 = NULL;
	{
		U3CJoinU3Ec__AnonStorey116_1_t1263154545 * L_0 = (U3CJoinU3Ec__AnonStorey116_1_t1263154545 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CJoinU3Ec__AnonStorey116_1_t1263154545 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_3 = (U3CJoinU3Ec__AnonStorey116_1_t1263154545 *)L_0;
		U3CJoinU3Ec__AnonStorey116_1_t1263154545 * L_1 = V_3;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24279_3(__this);
		U3CJoinU3Ec__AnonStorey116_1_t1263154545 * L_2 = V_3;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CJoinU3Ec__AnonStorey116_1_t1263154545 * L_4 = V_3;
		int32_t L_5 = ___p1;
		NullCheck(L_4);
		L_4->set_p_2(L_5);
		SingleAssignmentDisposable_t1832432170 * L_6 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_6, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t1832432170 *)L_6;
		U3CJoinU3Ec__AnonStorey116_1_t1263154545 * L_7 = V_3;
		SingleAssignmentDisposable_t1832432170 * L_8 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_inner_0(L_8);
		CellJoinDisposable_1_t3736493403 * L_9 = (CellJoinDisposable_1_t3736493403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_4 = (CellJoinDisposable_1_t3736493403 *)L_9;
		CellJoinDisposable_1_t3736493403 * L_10 = V_4;
		SingleAssignmentDisposable_t1832432170 * L_11 = V_0;
		NullCheck((ListDisposable_t2393830995 *)L_10);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_10, (Il2CppObject *)L_11);
		CellJoinDisposable_1_t3736493403 * L_12 = V_4;
		U3CJoinU3Ec__AnonStorey116_1_t1263154545 * L_13 = V_3;
		NullCheck(L_13);
		SingleAssignmentDisposable_t1832432170 * L_14 = (SingleAssignmentDisposable_t1832432170 *)L_13->get_inner_0();
		NullCheck((ListDisposable_t2393830995 *)L_12);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_12, (Il2CppObject *)L_14);
		CellJoinDisposable_1_t3736493403 * L_15 = V_4;
		V_1 = (CellJoinDisposable_1_t3736493403 *)L_15;
		U3CJoinU3Ec__AnonStorey116_1_t1263154545 * L_16 = V_3;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_1_t1538250116 * L_18 = (Action_1_t1538250116 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t1538250116 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_18, (Il2CppObject *)L_16, (IntPtr_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_2 = (Action_1_t1538250116 *)L_18;
		SingleAssignmentDisposable_t1832432170 * L_19 = V_0;
		Il2CppObject* L_20 = (Il2CppObject*)__this->get_cell_0();
		Action_1_t1538250116 * L_21 = V_2;
		U3CJoinU3Ec__AnonStorey116_1_t1263154545 * L_22 = V_3;
		NullCheck(L_22);
		int32_t L_23 = (int32_t)L_22->get_p_2();
		NullCheck((Il2CppObject*)L_20);
		Il2CppObject * L_24 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1538250116 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<IStream`1<System.Object>>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), (Il2CppObject*)L_20, (Action_1_t1538250116 *)L_21, (int32_t)L_23);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_19);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_19, (Il2CppObject *)L_24, /*hidden argument*/NULL);
		CellJoinDisposable_1_t3736493403 * L_25 = V_1;
		return L_25;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10F_2__ctor_m1454680429_gshared (U3CMapU3Ec__AnonStorey10F_2_t985129795 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Boolean,System.Boolean>::<>m__197(T)
extern "C"  void U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m2641039811_gshared (U3CMapU3Ec__AnonStorey10F_2_t985129795 * __this, bool ___val0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		U3CMapU3Ec__AnonStorey10E_2_t4182566112 * L_0 = (U3CMapU3Ec__AnonStorey10E_2_t4182566112 *)__this->get_U3CU3Ef__refU24270_2();
		NullCheck(L_0);
		Func_2_t1008118516 * L_1 = (Func_2_t1008118516 *)L_0->get_map_0();
		bool L_2 = ___val0;
		NullCheck((Func_2_t1008118516 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1008118516 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1008118516 *)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (bool)L_3;
		bool L_4 = V_0;
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_5);
		MapDisposable_t304581564 * L_7 = (MapDisposable_t304581564 *)__this->get_disp_0();
		NullCheck(L_7);
		Il2CppObject * L_8 = (Il2CppObject *)L_7->get_last_1();
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_6, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_004a;
		}
	}
	{
		MapDisposable_t304581564 * L_10 = (MapDisposable_t304581564 *)__this->get_disp_0();
		bool L_11 = V_0;
		bool L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_12);
		NullCheck(L_10);
		L_10->set_last_1(L_13);
		Action_1_t359458046 * L_14 = (Action_1_t359458046 *)__this->get_reaction_1();
		bool L_15 = V_0;
		NullCheck((Action_1_t359458046 *)L_14);
		((  void (*) (Action_1_t359458046 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t359458046 *)L_14, (bool)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_004a:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Int32>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10F_2__ctor_m4226048493_gshared (U3CMapU3Ec__AnonStorey10F_2_t1626595051 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Int32>::<>m__197(T)
extern "C"  void U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m990546499_gshared (U3CMapU3Ec__AnonStorey10F_2_t1626595051 * __this, int32_t ___val0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		U3CMapU3Ec__AnonStorey10E_2_t529064072 * L_0 = (U3CMapU3Ec__AnonStorey10E_2_t529064072 *)__this->get_U3CU3Ef__refU24270_2();
		NullCheck(L_0);
		Func_2_t1649583772 * L_1 = (Func_2_t1649583772 *)L_0->get_map_0();
		int32_t L_2 = ___val0;
		NullCheck((Func_2_t1649583772 *)L_1);
		int32_t L_3 = ((  int32_t (*) (Func_2_t1649583772 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1649583772 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_5);
		MapDisposable_t304581564 * L_7 = (MapDisposable_t304581564 *)__this->get_disp_0();
		NullCheck(L_7);
		Il2CppObject * L_8 = (Il2CppObject *)L_7->get_last_1();
		bool L_9 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_6, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_004a;
		}
	}
	{
		MapDisposable_t304581564 * L_10 = (MapDisposable_t304581564 *)__this->get_disp_0();
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_12);
		NullCheck(L_10);
		L_10->set_last_1(L_13);
		Action_1_t2995867492 * L_14 = (Action_1_t2995867492 *)__this->get_reaction_1();
		int32_t L_15 = V_0;
		NullCheck((Action_1_t2995867492 *)L_14);
		((  void (*) (Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t2995867492 *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_004a:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10F_2__ctor_m3611507434_gshared (U3CMapU3Ec__AnonStorey10F_2_t3911253980 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Object>::<>m__197(T)
extern "C"  void U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m690658496_gshared (U3CMapU3Ec__AnonStorey10F_2_t3911253980 * __this, int32_t ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMapU3Ec__AnonStorey10E_2_t2813723001 * L_0 = (U3CMapU3Ec__AnonStorey10E_2_t2813723001 *)__this->get_U3CU3Ef__refU24270_2();
		NullCheck(L_0);
		Func_2_t3934242701 * L_1 = (Func_2_t3934242701 *)L_0->get_map_0();
		int32_t L_2 = ___val0;
		NullCheck((Func_2_t3934242701 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t3934242701 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t3934242701 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		MapDisposable_t304581564 * L_5 = (MapDisposable_t304581564 *)__this->get_disp_0();
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_last_1();
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_4, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		MapDisposable_t304581564 * L_8 = (MapDisposable_t304581564 *)__this->get_disp_0();
		Il2CppObject * L_9 = V_0;
		NullCheck(L_8);
		L_8->set_last_1(L_9);
		Action_1_t985559125 * L_10 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_11 = V_0;
		NullCheck((Action_1_t985559125 *)L_10);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_004a:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10F_2__ctor_m2278539761_gshared (U3CMapU3Ec__AnonStorey10F_2_t2112794631 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Object,System.Object>::<>m__197(T)
extern "C"  void U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m578701383_gshared (U3CMapU3Ec__AnonStorey10F_2_t2112794631 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMapU3Ec__AnonStorey10E_2_t1015263652 * L_0 = (U3CMapU3Ec__AnonStorey10E_2_t1015263652 *)__this->get_U3CU3Ef__refU24270_2();
		NullCheck(L_0);
		Func_2_t2135783352 * L_1 = (Func_2_t2135783352 *)L_0->get_map_0();
		Il2CppObject * L_2 = ___val0;
		NullCheck((Func_2_t2135783352 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t2135783352 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_0;
		MapDisposable_t304581564 * L_5 = (MapDisposable_t304581564 *)__this->get_disp_0();
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_last_1();
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_4, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		MapDisposable_t304581564 * L_8 = (MapDisposable_t304581564 *)__this->get_disp_0();
		Il2CppObject * L_9 = V_0;
		NullCheck(L_8);
		L_8->set_last_1(L_9);
		Action_1_t985559125 * L_10 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_11 = V_0;
		NullCheck((Action_1_t985559125 *)L_10);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_004a:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10E_2__ctor_m3755338828_gshared (U3CMapU3Ec__AnonStorey10E_2_t4182566112 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>::<>m__180(System.Action`1<T2>,Priority)
extern Il2CppClass* MapDisposable_t304581564_il2cpp_TypeInfo_var;
extern const uint32_t U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2588572291_MetadataUsageId;
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2588572291_gshared (U3CMapU3Ec__AnonStorey10E_2_t4182566112 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2588572291_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMapU3Ec__AnonStorey10F_2_t985129795 * V_0 = NULL;
	{
		U3CMapU3Ec__AnonStorey10F_2_t985129795 * L_0 = (U3CMapU3Ec__AnonStorey10F_2_t985129795 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t985129795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CMapU3Ec__AnonStorey10F_2_t985129795 *)L_0;
		U3CMapU3Ec__AnonStorey10F_2_t985129795 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24270_2(__this);
		U3CMapU3Ec__AnonStorey10F_2_t985129795 * L_2 = V_0;
		Action_1_t359458046 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMapU3Ec__AnonStorey10F_2_t985129795 * L_4 = V_0;
		MapDisposable_t304581564 * L_5 = (MapDisposable_t304581564 *)il2cpp_codegen_object_new(MapDisposable_t304581564_il2cpp_TypeInfo_var);
		MapDisposable__ctor_m1056915391(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMapU3Ec__AnonStorey10F_2_t985129795 * L_6 = V_0;
		NullCheck(L_6);
		MapDisposable_t304581564 * L_7 = (MapDisposable_t304581564 *)L_6->get_disp_0();
		Func_2_t1008118516 * L_8 = (Func_2_t1008118516 *)__this->get_map_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(2 /* T ICell`1<System.Boolean>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		NullCheck((Func_2_t1008118516 *)L_8);
		bool L_11 = ((  bool (*) (Func_2_t1008118516 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t1008118516 *)L_8, (bool)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		bool L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_12);
		NullCheck(L_7);
		L_7->set_last_1(L_13);
		U3CMapU3Ec__AnonStorey10F_2_t985129795 * L_14 = V_0;
		NullCheck(L_14);
		MapDisposable_t304581564 * L_15 = (MapDisposable_t304581564 *)L_14->get_disp_0();
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_cell_1();
		U3CMapU3Ec__AnonStorey10F_2_t985129795 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Action_1_t359458046 * L_19 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_20 = ___p1;
		NullCheck((Il2CppObject*)L_16);
		Il2CppObject * L_21 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Boolean>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_16, (Action_1_t359458046 *)L_19, (int32_t)L_20);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_15);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_15, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		U3CMapU3Ec__AnonStorey10F_2_t985129795 * L_22 = V_0;
		NullCheck(L_22);
		MapDisposable_t304581564 * L_23 = (MapDisposable_t304581564 *)L_22->get_disp_0();
		return L_23;
	}
}
// T2 CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>::<>m__181()
extern "C"  bool U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m2772125942_gshared (U3CMapU3Ec__AnonStorey10E_2_t4182566112 * __this, const MethodInfo* method)
{
	{
		Func_2_t1008118516 * L_0 = (Func_2_t1008118516 *)__this->get_map_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(2 /* T ICell`1<System.Boolean>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1);
		NullCheck((Func_2_t1008118516 *)L_0);
		bool L_3 = ((  bool (*) (Func_2_t1008118516 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t1008118516 *)L_0, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_3;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Int32>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10E_2__ctor_m3164597324_gshared (U3CMapU3Ec__AnonStorey10E_2_t529064072 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Int32>::<>m__180(System.Action`1<T2>,Priority)
extern Il2CppClass* MapDisposable_t304581564_il2cpp_TypeInfo_var;
extern const uint32_t U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2920807427_MetadataUsageId;
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2920807427_gshared (U3CMapU3Ec__AnonStorey10E_2_t529064072 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2920807427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMapU3Ec__AnonStorey10F_2_t1626595051 * V_0 = NULL;
	{
		U3CMapU3Ec__AnonStorey10F_2_t1626595051 * L_0 = (U3CMapU3Ec__AnonStorey10F_2_t1626595051 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t1626595051 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CMapU3Ec__AnonStorey10F_2_t1626595051 *)L_0;
		U3CMapU3Ec__AnonStorey10F_2_t1626595051 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24270_2(__this);
		U3CMapU3Ec__AnonStorey10F_2_t1626595051 * L_2 = V_0;
		Action_1_t2995867492 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMapU3Ec__AnonStorey10F_2_t1626595051 * L_4 = V_0;
		MapDisposable_t304581564 * L_5 = (MapDisposable_t304581564 *)il2cpp_codegen_object_new(MapDisposable_t304581564_il2cpp_TypeInfo_var);
		MapDisposable__ctor_m1056915391(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMapU3Ec__AnonStorey10F_2_t1626595051 * L_6 = V_0;
		NullCheck(L_6);
		MapDisposable_t304581564 * L_7 = (MapDisposable_t304581564 *)L_6->get_disp_0();
		Func_2_t1649583772 * L_8 = (Func_2_t1649583772 *)__this->get_map_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		NullCheck((Func_2_t1649583772 *)L_8);
		int32_t L_11 = ((  int32_t (*) (Func_2_t1649583772 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t1649583772 *)L_8, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_12);
		NullCheck(L_7);
		L_7->set_last_1(L_13);
		U3CMapU3Ec__AnonStorey10F_2_t1626595051 * L_14 = V_0;
		NullCheck(L_14);
		MapDisposable_t304581564 * L_15 = (MapDisposable_t304581564 *)L_14->get_disp_0();
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_cell_1();
		U3CMapU3Ec__AnonStorey10F_2_t1626595051 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Action_1_t2995867492 * L_19 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_20 = ___p1;
		NullCheck((Il2CppObject*)L_16);
		Il2CppObject * L_21 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_16, (Action_1_t2995867492 *)L_19, (int32_t)L_20);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_15);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_15, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		U3CMapU3Ec__AnonStorey10F_2_t1626595051 * L_22 = V_0;
		NullCheck(L_22);
		MapDisposable_t304581564 * L_23 = (MapDisposable_t304581564 *)L_22->get_disp_0();
		return L_23;
	}
}
// T2 CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Int32>::<>m__181()
extern "C"  int32_t U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1454256246_gshared (U3CMapU3Ec__AnonStorey10E_2_t529064072 * __this, const MethodInfo* method)
{
	{
		Func_2_t1649583772 * L_0 = (Func_2_t1649583772 *)__this->get_map_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1);
		NullCheck((Func_2_t1649583772 *)L_0);
		int32_t L_3 = ((  int32_t (*) (Func_2_t1649583772 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t1649583772 *)L_0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_3;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10E_2__ctor_m771292267_gshared (U3CMapU3Ec__AnonStorey10E_2_t2813723001 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Object>::<>m__180(System.Action`1<T2>,Priority)
extern Il2CppClass* MapDisposable_t304581564_il2cpp_TypeInfo_var;
extern const uint32_t U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m1992129854_MetadataUsageId;
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m1992129854_gshared (U3CMapU3Ec__AnonStorey10E_2_t2813723001 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m1992129854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMapU3Ec__AnonStorey10F_2_t3911253980 * V_0 = NULL;
	{
		U3CMapU3Ec__AnonStorey10F_2_t3911253980 * L_0 = (U3CMapU3Ec__AnonStorey10F_2_t3911253980 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t3911253980 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CMapU3Ec__AnonStorey10F_2_t3911253980 *)L_0;
		U3CMapU3Ec__AnonStorey10F_2_t3911253980 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24270_2(__this);
		U3CMapU3Ec__AnonStorey10F_2_t3911253980 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMapU3Ec__AnonStorey10F_2_t3911253980 * L_4 = V_0;
		MapDisposable_t304581564 * L_5 = (MapDisposable_t304581564 *)il2cpp_codegen_object_new(MapDisposable_t304581564_il2cpp_TypeInfo_var);
		MapDisposable__ctor_m1056915391(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMapU3Ec__AnonStorey10F_2_t3911253980 * L_6 = V_0;
		NullCheck(L_6);
		MapDisposable_t304581564 * L_7 = (MapDisposable_t304581564 *)L_6->get_disp_0();
		Func_2_t3934242701 * L_8 = (Func_2_t3934242701 *)__this->get_map_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		NullCheck((Func_2_t3934242701 *)L_8);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_2_t3934242701 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t3934242701 *)L_8, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_7);
		L_7->set_last_1(L_11);
		U3CMapU3Ec__AnonStorey10F_2_t3911253980 * L_12 = V_0;
		NullCheck(L_12);
		MapDisposable_t304581564 * L_13 = (MapDisposable_t304581564 *)L_12->get_disp_0();
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_cell_1();
		U3CMapU3Ec__AnonStorey10F_2_t3911253980 * L_15 = V_0;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Action_1_t2995867492 * L_17 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_17, (Il2CppObject *)L_15, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_18 = ___p1;
		NullCheck((Il2CppObject*)L_14);
		Il2CppObject * L_19 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_14, (Action_1_t2995867492 *)L_17, (int32_t)L_18);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_13, (Il2CppObject *)L_19, /*hidden argument*/NULL);
		U3CMapU3Ec__AnonStorey10F_2_t3911253980 * L_20 = V_0;
		NullCheck(L_20);
		MapDisposable_t304581564 * L_21 = (MapDisposable_t304581564 *)L_20->get_disp_0();
		return L_21;
	}
}
// T2 CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Object>::<>m__181()
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1165071513_gshared (U3CMapU3Ec__AnonStorey10E_2_t2813723001 * __this, const MethodInfo* method)
{
	{
		Func_2_t3934242701 * L_0 = (Func_2_t3934242701 *)__this->get_map_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1);
		NullCheck((Func_2_t3934242701 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t3934242701 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t3934242701 *)L_0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_3;
	}
}
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10E_2__ctor_m131215504_gshared (U3CMapU3Ec__AnonStorey10E_2_t1015263652 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<Map>c__AnonStorey10E`2<System.Object,System.Object>::<>m__180(System.Action`1<T2>,Priority)
extern Il2CppClass* MapDisposable_t304581564_il2cpp_TypeInfo_var;
extern const uint32_t U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m3817395839_MetadataUsageId;
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m3817395839_gshared (U3CMapU3Ec__AnonStorey10E_2_t1015263652 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m3817395839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMapU3Ec__AnonStorey10F_2_t2112794631 * V_0 = NULL;
	{
		U3CMapU3Ec__AnonStorey10F_2_t2112794631 * L_0 = (U3CMapU3Ec__AnonStorey10F_2_t2112794631 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t2112794631 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CMapU3Ec__AnonStorey10F_2_t2112794631 *)L_0;
		U3CMapU3Ec__AnonStorey10F_2_t2112794631 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24270_2(__this);
		U3CMapU3Ec__AnonStorey10F_2_t2112794631 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMapU3Ec__AnonStorey10F_2_t2112794631 * L_4 = V_0;
		MapDisposable_t304581564 * L_5 = (MapDisposable_t304581564 *)il2cpp_codegen_object_new(MapDisposable_t304581564_il2cpp_TypeInfo_var);
		MapDisposable__ctor_m1056915391(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMapU3Ec__AnonStorey10F_2_t2112794631 * L_6 = V_0;
		NullCheck(L_6);
		MapDisposable_t304581564 * L_7 = (MapDisposable_t304581564 *)L_6->get_disp_0();
		Func_2_t2135783352 * L_8 = (Func_2_t2135783352 *)__this->get_map_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		NullCheck((Func_2_t2135783352 *)L_8);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t2135783352 *)L_8, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_7);
		L_7->set_last_1(L_11);
		U3CMapU3Ec__AnonStorey10F_2_t2112794631 * L_12 = V_0;
		NullCheck(L_12);
		MapDisposable_t304581564 * L_13 = (MapDisposable_t304581564 *)L_12->get_disp_0();
		Il2CppObject* L_14 = (Il2CppObject*)__this->get_cell_1();
		U3CMapU3Ec__AnonStorey10F_2_t2112794631 * L_15 = V_0;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Action_1_t985559125 * L_17 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_17, (Il2CppObject *)L_15, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_18 = ___p1;
		NullCheck((Il2CppObject*)L_14);
		Il2CppObject * L_19 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_14, (Action_1_t985559125 *)L_17, (int32_t)L_18);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_13, (Il2CppObject *)L_19, /*hidden argument*/NULL);
		U3CMapU3Ec__AnonStorey10F_2_t2112794631 * L_20 = V_0;
		NullCheck(L_20);
		MapDisposable_t304581564 * L_21 = (MapDisposable_t304581564 *)L_20->get_disp_0();
		return L_21;
	}
}
// T2 CellReactiveApi/<Map>c__AnonStorey10E`2<System.Object,System.Object>::<>m__181()
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1917839858_gshared (U3CMapU3Ec__AnonStorey10E_2_t1015263652 * __this, const MethodInfo* method)
{
	{
		Func_2_t2135783352 * L_0 = (Func_2_t2135783352 *)__this->get_map_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1);
		NullCheck((Func_2_t2135783352 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t2135783352 *)L_0, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_3;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m1123404121_gshared (U3CMergeU3Ec__AnonStorey113_3_t2913556904 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1124525925_gshared (U3CMergeU3Ec__AnonStorey113_3_t2913556904 * __this, DateTime_t339033936  ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey112_3_t3245937911 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t3245937911 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_curr_3();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		Il2CppObject * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_lastValue_2(L_8);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((Action_1_t985559125 *)L_9);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m501489148_gshared (U3CMergeU3Ec__AnonStorey113_3_t2913556904 * __this, int64_t ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey112_3_t3245937911 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t3245937911 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_curr_3();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		Il2CppObject * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_lastValue_2(L_8);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((Action_1_t985559125 *)L_9);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Double>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m1555042902_gshared (U3CMergeU3Ec__AnonStorey113_3_t3372762043 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Double>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m4098517474_gshared (U3CMergeU3Ec__AnonStorey113_3_t3372762043 * __this, double ___val0, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		U3CMergeU3Ec__AnonStorey112_3_t3705143050 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t3705143050 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1677297861 * L_1 = (Func_1_t1677297861 *)L_0->get_curr_3();
		NullCheck((Func_1_t1677297861 *)L_1);
		double L_2 = ((  double (*) (Func_1_t1677297861 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1677297861 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (double)L_2;
		double L_3 = V_0;
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_4);
		CellJoinDisposable_1_t3433903597 * L_6 = (CellJoinDisposable_1_t3433903597 *)__this->get_disp_0();
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_lastValue_2();
		bool L_8 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3433903597 * L_9 = (CellJoinDisposable_1_t3433903597 *)__this->get_disp_0();
		double L_10 = V_0;
		double L_11 = L_10;
		Il2CppObject * L_12 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_11);
		NullCheck(L_9);
		L_9->set_lastValue_2(L_12);
		Action_1_t682969319 * L_13 = (Action_1_t682969319 *)__this->get_reaction_1();
		double L_14 = V_0;
		NullCheck((Action_1_t682969319 *)L_13);
		((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t682969319 *)L_13, (double)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Double>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m2500913951_gshared (U3CMergeU3Ec__AnonStorey113_3_t3372762043 * __this, int32_t ___val0, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		U3CMergeU3Ec__AnonStorey112_3_t3705143050 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t3705143050 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1677297861 * L_1 = (Func_1_t1677297861 *)L_0->get_curr_3();
		NullCheck((Func_1_t1677297861 *)L_1);
		double L_2 = ((  double (*) (Func_1_t1677297861 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1677297861 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (double)L_2;
		double L_3 = V_0;
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_4);
		CellJoinDisposable_1_t3433903597 * L_6 = (CellJoinDisposable_1_t3433903597 *)__this->get_disp_0();
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_lastValue_2();
		bool L_8 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_5, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3433903597 * L_9 = (CellJoinDisposable_1_t3433903597 *)__this->get_disp_0();
		double L_10 = V_0;
		double L_11 = L_10;
		Il2CppObject * L_12 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_11);
		NullCheck(L_9);
		L_9->set_lastValue_2(L_12);
		Action_1_t682969319 * L_13 = (Action_1_t682969319 *)__this->get_reaction_1();
		double L_14 = V_0;
		NullCheck((Action_1_t682969319 *)L_13);
		((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t682969319 *)L_13, (double)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m150554692_gshared (U3CMergeU3Ec__AnonStorey113_3_t3675351849 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Object>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1275787472_gshared (U3CMergeU3Ec__AnonStorey113_3_t3675351849 * __this, double ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey112_3_t4007732856 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t4007732856 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_curr_3();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		Il2CppObject * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_lastValue_2(L_8);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((Action_1_t985559125 *)L_9);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Object>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m895629809_gshared (U3CMergeU3Ec__AnonStorey113_3_t3675351849 * __this, int32_t ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey112_3_t4007732856 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t4007732856 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_curr_3();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		Il2CppObject * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_lastValue_2(L_8);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((Action_1_t985559125 *)L_9);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m1413055685_gshared (U3CMergeU3Ec__AnonStorey113_3_t2569540242 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Object,System.Object,System.Object>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1273433297_gshared (U3CMergeU3Ec__AnonStorey113_3_t2569540242 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey112_3_t2901921249 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t2901921249 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_curr_3();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		Il2CppObject * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_lastValue_2(L_8);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((Action_1_t985559125 *)L_9);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Object,System.Object,System.Object>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m822650384_gshared (U3CMergeU3Ec__AnonStorey113_3_t2569540242 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey112_3_t2901921249 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t2901921249 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_curr_3();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		Il2CppObject * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_lastValue_2(L_8);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((Action_1_t985559125 *)L_9);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Single,System.Object,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m612980494_gshared (U3CMergeU3Ec__AnonStorey113_3_t3131141555 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Single,System.Object,System.Object>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m4026700442_gshared (U3CMergeU3Ec__AnonStorey113_3_t3131141555 * __this, float ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey112_3_t3463522562 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t3463522562 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_curr_3();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		Il2CppObject * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_lastValue_2(L_8);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((Action_1_t985559125 *)L_9);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Single,System.Object,System.Object>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m274585959_gshared (U3CMergeU3Ec__AnonStorey113_3_t3131141555 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey112_3_t3463522562 * L_0 = (U3CMergeU3Ec__AnonStorey112_3_t3463522562 *)__this->get_U3CU3Ef__refU24274_2();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_curr_3();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_lastValue_2();
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)__this->get_disp_0();
		Il2CppObject * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_lastValue_2(L_8);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((Action_1_t985559125 *)L_9);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0049:
	{
		return;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3<System.DateTime,System.Int64,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey112_3__ctor_m3886235819_gshared (U3CMergeU3Ec__AnonStorey112_3_t3245937911 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T3 CellReactiveApi/<Merge>c__AnonStorey112`3<System.DateTime,System.Int64,System.Object>::<>m__186()
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m3407938719_gshared (U3CMergeU3Ec__AnonStorey112_3_t3245937911 * __this, const MethodInfo* method)
{
	{
		Func_3_t2236225891 * L_0 = (Func_3_t2236225891 *)__this->get_func_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		DateTime_t339033936  L_2 = InterfaceFuncInvoker0< DateTime_t339033936  >::Invoke(2 /* T ICell`1<System.DateTime>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_1);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_3);
		int64_t L_4 = InterfaceFuncInvoker0< int64_t >::Invoke(2 /* T ICell`1<System.Int64>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_3);
		NullCheck((Func_3_t2236225891 *)L_0);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_3_t2236225891 *, DateTime_t339033936 , int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t2236225891 *)L_0, (DateTime_t339033936 )L_2, (int64_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
// System.IDisposable CellReactiveApi/<Merge>c__AnonStorey112`3<System.DateTime,System.Int64,System.Object>::<>m__187(System.Action`1<T3>,Priority)
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m4010497764_gshared (U3CMergeU3Ec__AnonStorey112_3_t3245937911 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CMergeU3Ec__AnonStorey113_3_t2913556904 * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_0 = (U3CMergeU3Ec__AnonStorey113_3_t2913556904 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t2913556904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3CMergeU3Ec__AnonStorey113_3_t2913556904 *)L_0;
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24274_2(__this);
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_4 = V_0;
		CellJoinDisposable_1_t3736493403 * L_5 = (CellJoinDisposable_1_t3736493403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_6 = V_0;
		NullCheck(L_6);
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)L_6->get_disp_0();
		Func_3_t2236225891 * L_8 = (Func_3_t2236225891 *)__this->get_func_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		DateTime_t339033936  L_10 = InterfaceFuncInvoker0< DateTime_t339033936  >::Invoke(2 /* T ICell`1<System.DateTime>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_9);
		Il2CppObject* L_11 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_11);
		int64_t L_12 = InterfaceFuncInvoker0< int64_t >::Invoke(2 /* T ICell`1<System.Int64>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_11);
		NullCheck((Func_3_t2236225891 *)L_8);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (Func_3_t2236225891 *, DateTime_t339033936 , int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t2236225891 *)L_8, (DateTime_t339033936 )L_10, (int64_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck(L_7);
		L_7->set_lastValue_2(L_13);
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_14 = V_0;
		NullCheck(L_14);
		CellJoinDisposable_1_t3736493403 * L_15 = (CellJoinDisposable_1_t3736493403 *)L_14->get_disp_0();
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_cell_1();
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_1_t487486641 * L_19 = (Action_1_t487486641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Action_1_t487486641 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		int32_t L_20 = ___p1;
		NullCheck((Il2CppObject*)L_16);
		Il2CppObject * L_21 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t487486641 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.DateTime>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_16, (Action_1_t487486641 *)L_19, (int32_t)L_20);
		NullCheck((ListDisposable_t2393830995 *)L_15);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_15, (Il2CppObject *)L_21);
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_22 = V_0;
		NullCheck(L_22);
		CellJoinDisposable_1_t3736493403 * L_23 = (CellJoinDisposable_1_t3736493403 *)L_22->get_disp_0();
		Il2CppObject* L_24 = (Il2CppObject*)__this->get_cell2_2();
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_25 = V_0;
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t2995867587 * L_27 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((  void (*) (Action_1_t2995867587 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(L_27, (Il2CppObject *)L_25, (IntPtr_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		int32_t L_28 = ___p1;
		NullCheck((Il2CppObject*)L_24);
		Il2CppObject * L_29 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867587 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Int64>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_24, (Action_1_t2995867587 *)L_27, (int32_t)L_28);
		NullCheck((ListDisposable_t2393830995 *)L_23);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_23, (Il2CppObject *)L_29);
		U3CMergeU3Ec__AnonStorey113_3_t2913556904 * L_30 = V_0;
		NullCheck(L_30);
		CellJoinDisposable_1_t3736493403 * L_31 = (CellJoinDisposable_1_t3736493403 *)L_30->get_disp_0();
		return L_31;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Double>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey112_3__ctor_m3734451752_gshared (U3CMergeU3Ec__AnonStorey112_3_t3705143050 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T3 CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Double>::<>m__186()
extern "C"  double U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m2791000706_gshared (U3CMergeU3Ec__AnonStorey112_3_t3705143050 * __this, const MethodInfo* method)
{
	{
		Func_3_t2695431030 * L_0 = (Func_3_t2695431030 *)__this->get_func_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		double L_2 = InterfaceFuncInvoker0< double >::Invoke(2 /* T ICell`1<System.Double>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_1);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_3);
		NullCheck((Func_3_t2695431030 *)L_0);
		double L_5 = ((  double (*) (Func_3_t2695431030 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t2695431030 *)L_0, (double)L_2, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
// System.IDisposable CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Double>::<>m__187(System.Action`1<T3>,Priority)
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m2303144199_gshared (U3CMergeU3Ec__AnonStorey112_3_t3705143050 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CMergeU3Ec__AnonStorey113_3_t3372762043 * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_0 = (U3CMergeU3Ec__AnonStorey113_3_t3372762043 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3372762043 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3CMergeU3Ec__AnonStorey113_3_t3372762043 *)L_0;
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24274_2(__this);
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_2 = V_0;
		Action_1_t682969319 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_4 = V_0;
		CellJoinDisposable_1_t3433903597 * L_5 = (CellJoinDisposable_1_t3433903597 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CellJoinDisposable_1_t3433903597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_6 = V_0;
		NullCheck(L_6);
		CellJoinDisposable_1_t3433903597 * L_7 = (CellJoinDisposable_1_t3433903597 *)L_6->get_disp_0();
		Func_3_t2695431030 * L_8 = (Func_3_t2695431030 *)__this->get_func_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		double L_10 = InterfaceFuncInvoker0< double >::Invoke(2 /* T ICell`1<System.Double>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_9);
		Il2CppObject* L_11 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_11);
		int32_t L_12 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_11);
		NullCheck((Func_3_t2695431030 *)L_8);
		double L_13 = ((  double (*) (Func_3_t2695431030 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t2695431030 *)L_8, (double)L_10, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		double L_14 = L_13;
		Il2CppObject * L_15 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_14);
		NullCheck(L_7);
		L_7->set_lastValue_2(L_15);
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_16 = V_0;
		NullCheck(L_16);
		CellJoinDisposable_1_t3433903597 * L_17 = (CellJoinDisposable_1_t3433903597 *)L_16->get_disp_0();
		Il2CppObject* L_18 = (Il2CppObject*)__this->get_cell_1();
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_19 = V_0;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_1_t682969319 * L_21 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_21, (Il2CppObject *)L_19, (IntPtr_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		int32_t L_22 = ___p1;
		NullCheck((Il2CppObject*)L_18);
		Il2CppObject * L_23 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_18, (Action_1_t682969319 *)L_21, (int32_t)L_22);
		NullCheck((ListDisposable_t2393830995 *)L_17);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_17, (Il2CppObject *)L_23);
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_24 = V_0;
		NullCheck(L_24);
		CellJoinDisposable_1_t3433903597 * L_25 = (CellJoinDisposable_1_t3433903597 *)L_24->get_disp_0();
		Il2CppObject* L_26 = (Il2CppObject*)__this->get_cell2_2();
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_27 = V_0;
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t2995867492 * L_29 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(L_29, (Il2CppObject *)L_27, (IntPtr_t)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		int32_t L_30 = ___p1;
		NullCheck((Il2CppObject*)L_26);
		Il2CppObject * L_31 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_26, (Action_1_t2995867492 *)L_29, (int32_t)L_30);
		NullCheck((ListDisposable_t2393830995 *)L_25);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_25, (Il2CppObject *)L_31);
		U3CMergeU3Ec__AnonStorey113_3_t3372762043 * L_32 = V_0;
		NullCheck(L_32);
		CellJoinDisposable_1_t3433903597 * L_33 = (CellJoinDisposable_1_t3433903597 *)L_32->get_disp_0();
		return L_33;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey112_3__ctor_m2329963542_gshared (U3CMergeU3Ec__AnonStorey112_3_t4007732856 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T3 CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Object>::<>m__186()
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m3254134228_gshared (U3CMergeU3Ec__AnonStorey112_3_t4007732856 * __this, const MethodInfo* method)
{
	{
		Func_3_t2998020836 * L_0 = (Func_3_t2998020836 *)__this->get_func_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		double L_2 = InterfaceFuncInvoker0< double >::Invoke(2 /* T ICell`1<System.Double>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_1);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_3);
		NullCheck((Func_3_t2998020836 *)L_0);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_3_t2998020836 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t2998020836 *)L_0, (double)L_2, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
// System.IDisposable CellReactiveApi/<Merge>c__AnonStorey112`3<System.Double,System.Int32,System.Object>::<>m__187(System.Action`1<T3>,Priority)
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m770882905_gshared (U3CMergeU3Ec__AnonStorey112_3_t4007732856 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CMergeU3Ec__AnonStorey113_3_t3675351849 * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_0 = (U3CMergeU3Ec__AnonStorey113_3_t3675351849 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3675351849 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3CMergeU3Ec__AnonStorey113_3_t3675351849 *)L_0;
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24274_2(__this);
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_4 = V_0;
		CellJoinDisposable_1_t3736493403 * L_5 = (CellJoinDisposable_1_t3736493403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_6 = V_0;
		NullCheck(L_6);
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)L_6->get_disp_0();
		Func_3_t2998020836 * L_8 = (Func_3_t2998020836 *)__this->get_func_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		double L_10 = InterfaceFuncInvoker0< double >::Invoke(2 /* T ICell`1<System.Double>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_9);
		Il2CppObject* L_11 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_11);
		int32_t L_12 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* T ICell`1<System.Int32>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_11);
		NullCheck((Func_3_t2998020836 *)L_8);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (Func_3_t2998020836 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t2998020836 *)L_8, (double)L_10, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck(L_7);
		L_7->set_lastValue_2(L_13);
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_14 = V_0;
		NullCheck(L_14);
		CellJoinDisposable_1_t3736493403 * L_15 = (CellJoinDisposable_1_t3736493403 *)L_14->get_disp_0();
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_cell_1();
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_1_t682969319 * L_19 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		int32_t L_20 = ___p1;
		NullCheck((Il2CppObject*)L_16);
		Il2CppObject * L_21 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_16, (Action_1_t682969319 *)L_19, (int32_t)L_20);
		NullCheck((ListDisposable_t2393830995 *)L_15);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_15, (Il2CppObject *)L_21);
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_22 = V_0;
		NullCheck(L_22);
		CellJoinDisposable_1_t3736493403 * L_23 = (CellJoinDisposable_1_t3736493403 *)L_22->get_disp_0();
		Il2CppObject* L_24 = (Il2CppObject*)__this->get_cell2_2();
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_25 = V_0;
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t2995867492 * L_27 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(L_27, (Il2CppObject *)L_25, (IntPtr_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		int32_t L_28 = ___p1;
		NullCheck((Il2CppObject*)L_24);
		Il2CppObject * L_29 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_24, (Action_1_t2995867492 *)L_27, (int32_t)L_28);
		NullCheck((ListDisposable_t2393830995 *)L_23);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_23, (Il2CppObject *)L_29);
		U3CMergeU3Ec__AnonStorey113_3_t3675351849 * L_30 = V_0;
		NullCheck(L_30);
		CellJoinDisposable_1_t3736493403 * L_31 = (CellJoinDisposable_1_t3736493403 *)L_30->get_disp_0();
		return L_31;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey112_3__ctor_m255253299_gshared (U3CMergeU3Ec__AnonStorey112_3_t2901921249 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T3 CellReactiveApi/<Merge>c__AnonStorey112`3<System.Object,System.Object,System.Object>::<>m__186()
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m721869139_gshared (U3CMergeU3Ec__AnonStorey112_3_t2901921249 * __this, const MethodInfo* method)
{
	{
		Func_3_t1892209229 * L_0 = (Func_3_t1892209229 *)__this->get_func_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_1);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_3);
		NullCheck((Func_3_t1892209229 *)L_0);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t1892209229 *)L_0, (Il2CppObject *)L_2, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
// System.IDisposable CellReactiveApi/<Merge>c__AnonStorey112`3<System.Object,System.Object,System.Object>::<>m__187(System.Action`1<T3>,Priority)
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m938389666_gshared (U3CMergeU3Ec__AnonStorey112_3_t2901921249 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CMergeU3Ec__AnonStorey113_3_t2569540242 * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_0 = (U3CMergeU3Ec__AnonStorey113_3_t2569540242 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t2569540242 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3CMergeU3Ec__AnonStorey113_3_t2569540242 *)L_0;
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24274_2(__this);
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_4 = V_0;
		CellJoinDisposable_1_t3736493403 * L_5 = (CellJoinDisposable_1_t3736493403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_6 = V_0;
		NullCheck(L_6);
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)L_6->get_disp_0();
		Func_3_t1892209229 * L_8 = (Func_3_t1892209229 *)__this->get_func_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_9);
		Il2CppObject* L_11 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_11);
		NullCheck((Func_3_t1892209229 *)L_8);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t1892209229 *)L_8, (Il2CppObject *)L_10, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck(L_7);
		L_7->set_lastValue_2(L_13);
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_14 = V_0;
		NullCheck(L_14);
		CellJoinDisposable_1_t3736493403 * L_15 = (CellJoinDisposable_1_t3736493403 *)L_14->get_disp_0();
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_cell_1();
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_1_t985559125 * L_19 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		int32_t L_20 = ___p1;
		NullCheck((Il2CppObject*)L_16);
		Il2CppObject * L_21 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_16, (Action_1_t985559125 *)L_19, (int32_t)L_20);
		NullCheck((ListDisposable_t2393830995 *)L_15);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_15, (Il2CppObject *)L_21);
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_22 = V_0;
		NullCheck(L_22);
		CellJoinDisposable_1_t3736493403 * L_23 = (CellJoinDisposable_1_t3736493403 *)L_22->get_disp_0();
		Il2CppObject* L_24 = (Il2CppObject*)__this->get_cell2_2();
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_25 = V_0;
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t985559125 * L_27 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(L_27, (Il2CppObject *)L_25, (IntPtr_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		int32_t L_28 = ___p1;
		NullCheck((Il2CppObject*)L_24);
		Il2CppObject * L_29 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_24, (Action_1_t985559125 *)L_27, (int32_t)L_28);
		NullCheck((ListDisposable_t2393830995 *)L_23);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_23, (Il2CppObject *)L_29);
		U3CMergeU3Ec__AnonStorey113_3_t2569540242 * L_30 = V_0;
		NullCheck(L_30);
		CellJoinDisposable_1_t3736493403 * L_31 = (CellJoinDisposable_1_t3736493403 *)L_30->get_disp_0();
		return L_31;
	}
}
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey112_3__ctor_m3750145404_gshared (U3CMergeU3Ec__AnonStorey112_3_t3463522562 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T3 CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>::<>m__186()
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__186_m2750346858_gshared (U3CMergeU3Ec__AnonStorey112_3_t3463522562 * __this, const MethodInfo* method)
{
	{
		Func_3_t2453810542 * L_0 = (Func_3_t2453810542 *)__this->get_func_0();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_1);
		float L_2 = InterfaceFuncInvoker0< float >::Invoke(2 /* T ICell`1<System.Single>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_1);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_3);
		NullCheck((Func_3_t2453810542 *)L_0);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_3_t2453810542 *, float, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t2453810542 *)L_0, (float)L_2, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
// System.IDisposable CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>::<>m__187(System.Action`1<T3>,Priority)
extern "C"  Il2CppObject * U3CMergeU3Ec__AnonStorey112_3_U3CU3Em__187_m2280052025_gshared (U3CMergeU3Ec__AnonStorey112_3_t3463522562 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CMergeU3Ec__AnonStorey113_3_t3131141555 * V_0 = NULL;
	{
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_0 = (U3CMergeU3Ec__AnonStorey113_3_t3131141555 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3131141555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (U3CMergeU3Ec__AnonStorey113_3_t3131141555 *)L_0;
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24274_2(__this);
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_4 = V_0;
		CellJoinDisposable_1_t3736493403 * L_5 = (CellJoinDisposable_1_t3736493403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_6 = V_0;
		NullCheck(L_6);
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)L_6->get_disp_0();
		Func_3_t2453810542 * L_8 = (Func_3_t2453810542 *)__this->get_func_0();
		Il2CppObject* L_9 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_9);
		float L_10 = InterfaceFuncInvoker0< float >::Invoke(2 /* T ICell`1<System.Single>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_9);
		Il2CppObject* L_11 = (Il2CppObject*)__this->get_cell2_2();
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_11);
		NullCheck((Func_3_t2453810542 *)L_8);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (Func_3_t2453810542 *, float, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_3_t2453810542 *)L_8, (float)L_10, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck(L_7);
		L_7->set_lastValue_2(L_13);
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_14 = V_0;
		NullCheck(L_14);
		CellJoinDisposable_1_t3736493403 * L_15 = (CellJoinDisposable_1_t3736493403 *)L_14->get_disp_0();
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_cell_1();
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Action_1_t1106661726 * L_19 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		int32_t L_20 = ___p1;
		NullCheck((Il2CppObject*)L_16);
		Il2CppObject * L_21 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Single>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_16, (Action_1_t1106661726 *)L_19, (int32_t)L_20);
		NullCheck((ListDisposable_t2393830995 *)L_15);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_15, (Il2CppObject *)L_21);
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_22 = V_0;
		NullCheck(L_22);
		CellJoinDisposable_1_t3736493403 * L_23 = (CellJoinDisposable_1_t3736493403 *)L_22->get_disp_0();
		Il2CppObject* L_24 = (Il2CppObject*)__this->get_cell2_2();
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_25 = V_0;
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t985559125 * L_27 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(L_27, (Il2CppObject *)L_25, (IntPtr_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		int32_t L_28 = ___p1;
		NullCheck((Il2CppObject*)L_24);
		Il2CppObject * L_29 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_24, (Action_1_t985559125 *)L_27, (int32_t)L_28);
		NullCheck((ListDisposable_t2393830995 *)L_23);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_23, (Il2CppObject *)L_29);
		U3CMergeU3Ec__AnonStorey113_3_t3131141555 * L_30 = V_0;
		NullCheck(L_30);
		CellJoinDisposable_1_t3736493403 * L_31 = (CellJoinDisposable_1_t3736493403 *)L_30->get_disp_0();
		return L_31;
	}
}
// System.Void CellReactiveApi/<NotNull>c__AnonStorey102`1<System.Object>::.ctor()
extern "C"  void U3CNotNullU3Ec__AnonStorey102_1__ctor_m2607898092_gshared (U3CNotNullU3Ec__AnonStorey102_1_t291767342 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T CellReactiveApi/<NotNull>c__AnonStorey102`1<System.Object>::<>m__178(T)
extern "C"  Il2CppObject * U3CNotNullU3Ec__AnonStorey102_1_U3CU3Em__178_m1839195072_gshared (U3CNotNullU3Ec__AnonStorey102_1_t291767342 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = ___val0;
		Il2CppObject * L_1 = (Il2CppObject *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000e;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_orphan_0();
		G_B2_0 = L_2;
	}

IL_000e:
	{
		return G_B2_0;
	}
}
// System.Void CellReactiveApi/<SelectMany>c__AnonStorey118`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey118_2__ctor_m1010833179_gshared (U3CSelectManyU3Ec__AnonStorey118_2_t927720085 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// ICell`1<TR> CellReactiveApi/<SelectMany>c__AnonStorey118`2<System.Object,System.Object>::<>m__18D(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey118_2_U3CU3Em__18D_m3944559670_gshared (U3CSelectManyU3Ec__AnonStorey118_2_t927720085 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_other_0();
		return L_0;
	}
}
// System.Void CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Int32,System.Int32,System.Int32>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey11A_3__ctor_m462548113_gshared (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TR CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Int32,System.Int32,System.Int32>::<>m__19F(TC)
extern "C"  int32_t U3CSelectManyU3Ec__AnonStorey11A_3_U3CU3Em__19F_m1015973816_gshared (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * __this, int32_t ___y0, const MethodInfo* method)
{
	{
		U3CSelectManyU3Ec__AnonStorey119_3_t4205223976 * L_0 = (U3CSelectManyU3Ec__AnonStorey119_3_t4205223976 *)__this->get_U3CU3Ef__refU24281_1();
		NullCheck(L_0);
		Func_3_t543102568 * L_1 = (Func_3_t543102568 *)L_0->get_resultSelector_1();
		int32_t L_2 = (int32_t)__this->get_x_0();
		int32_t L_3 = ___y0;
		NullCheck((Func_3_t543102568 *)L_1);
		int32_t L_4 = ((  int32_t (*) (Func_3_t543102568 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_3_t543102568 *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_4;
	}
}
// System.Void CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey11A_3__ctor_m90349578_gshared (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TR CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Object,System.Object,System.Object>::<>m__19F(TC)
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey11A_3_U3CU3Em__19F_m205515901_gshared (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * __this, Il2CppObject * ___y0, const MethodInfo* method)
{
	{
		U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 * L_0 = (U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 *)__this->get_U3CU3Ef__refU24281_1();
		NullCheck(L_0);
		Func_3_t1892209229 * L_1 = (Func_3_t1892209229 *)L_0->get_resultSelector_1();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_x_0();
		Il2CppObject * L_3 = ___y0;
		NullCheck((Func_3_t1892209229 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_3_t1892209229 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_4;
	}
}
// System.Void CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Int32,System.Int32,System.Int32>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey119_3__ctor_m3417166508_gshared (U3CSelectManyU3Ec__AnonStorey119_3_t4205223976 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// ICell`1<TR> CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Int32,System.Int32,System.Int32>::<>m__18E(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey119_3_U3CU3Em__18E_m3232065760_gshared (U3CSelectManyU3Ec__AnonStorey119_3_t4205223976 * __this, int32_t ___x0, const MethodInfo* method)
{
	U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * V_0 = NULL;
	{
		U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * L_0 = (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 *)L_0;
		U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24281_1(__this);
		U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * L_2 = V_0;
		int32_t L_3 = ___x0;
		NullCheck(L_2);
		L_2->set_x_0(L_3);
		Func_2_t3201214749 * L_4 = (Func_2_t3201214749 *)__this->get_collectionSelector_0();
		U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get_x_0();
		NullCheck((Func_2_t3201214749 *)L_4);
		Il2CppObject* L_7 = ((  Il2CppObject* (*) (Func_2_t3201214749 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t3201214749 *)L_4, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		U3CSelectManyU3Ec__AnonStorey11A_3_t1546175920 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Func_2_t1649583772 * L_10 = (Func_2_t1649583772 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t1649583772 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_10, (Il2CppObject *)L_8, (IntPtr_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject* L_11 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1649583772 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject*)L_7, (Func_2_t1649583772 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_11;
	}
}
// System.Void CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey119_3__ctor_m67190799_gshared (U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// ICell`1<TR> CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Object,System.Object,System.Object>::<>m__18E(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey119_3_U3CU3Em__18E_m3276304969_gshared (U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * V_0 = NULL;
	{
		U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * L_0 = (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 *)L_0;
		U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24281_1(__this);
		U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * L_2 = V_0;
		Il2CppObject * L_3 = ___x0;
		NullCheck(L_2);
		L_2->set_x_0(L_3);
		Func_2_t3687414329 * L_4 = (Func_2_t3687414329 *)__this->get_collectionSelector_0();
		U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_x_0();
		NullCheck((Func_2_t3687414329 *)L_4);
		Il2CppObject* L_7 = ((  Il2CppObject* (*) (Func_2_t3687414329 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t3687414329 *)L_4, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Func_2_t2135783352 * L_10 = (Func_2_t2135783352 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2135783352 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_10, (Il2CppObject *)L_8, (IntPtr_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject* L_11 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject*)L_7, (Func_2_t2135783352 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_11;
	}
}
// System.Void CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1<System.Object>::.ctor()
extern "C"  void U3CSequenceU3Ec__AnonStorey111_1__ctor_m2577307519_gshared (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<Sequence>c__AnonStorey110`1/<Sequence>c__AnonStorey111`1<System.Object>::<>m__199(T)
extern "C"  void U3CSequenceU3Ec__AnonStorey111_1_U3CU3Em__199_m1588367251_gshared (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_1_t3857713481 * L_0 = (Action_1_t3857713481 *)__this->get_reaction_0();
		U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * L_1 = (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 *)__this->get_U3CU3Ef__refU24272_1();
		NullCheck(L_1);
		Func_1_t557074727 * L_2 = (Func_1_t557074727 *)L_1->get_values_1();
		NullCheck((Func_1_t557074727 *)L_2);
		Il2CppObject* L_3 = ((  Il2CppObject* (*) (Func_1_t557074727 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t557074727 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((Action_1_t3857713481 *)L_0);
		((  void (*) (Action_1_t3857713481 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t3857713481 *)L_0, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>::.ctor()
extern "C"  void U3CSequenceU3Ec__AnonStorey110_1__ctor_m2464883546_gshared (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<T> CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>::<>m__184()
extern "C"  Il2CppObject* U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__184_m2423780908_gshared (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * __this, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_cells_0();
		Func_2_t2767404563 * L_1 = ((U3CSequenceU3Ec__AnonStorey110_1_t1717931772_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Func_2_t2767404563 * L_3 = (Func_2_t2767404563 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Func_2_t2767404563 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, (Il2CppObject *)NULL, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((U3CSequenceU3Ec__AnonStorey110_1_t1717931772_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t2767404563 * L_4 = ((U3CSequenceU3Ec__AnonStorey110_1_t1717931772_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		Il2CppObject* L_5 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2767404563 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B2_0, (Func_2_t2767404563 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_5;
	}
}
// System.IDisposable CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>::<>m__185(System.Action`1<System.Collections.Generic.IEnumerable`1<T>>,Priority)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__185_m3963883721_MetadataUsageId;
extern "C"  Il2CppObject * U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__185_m3963883721_gshared (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * __this, Action_1_t3857713481 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__185_m3963883721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CellJoinDisposable_1_t3736493403 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * L_0 = (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_3 = (U3CSequenceU3Ec__AnonStorey111_1_t1272906773 *)L_0;
		U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * L_1 = V_3;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24272_1(__this);
		U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * L_2 = V_3;
		Action_1_t3857713481 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		CellJoinDisposable_1_t3736493403 * L_4 = (CellJoinDisposable_1_t3736493403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (CellJoinDisposable_1_t3736493403 *)L_4;
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_cells_0();
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<ICell`1<System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_5);
		V_2 = (Il2CppObject*)L_6;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004b;
		}

IL_002b:
		{
			Il2CppObject* L_7 = V_2;
			NullCheck((Il2CppObject*)L_7);
			Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<ICell`1<System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_7);
			V_1 = (Il2CppObject*)L_8;
			CellJoinDisposable_1_t3736493403 * L_9 = V_0;
			Il2CppObject* L_10 = V_1;
			U3CSequenceU3Ec__AnonStorey111_1_t1272906773 * L_11 = V_3;
			IntPtr_t L_12;
			L_12.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			Action_1_t985559125 * L_13 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(L_13, (Il2CppObject *)L_11, (IntPtr_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			int32_t L_14 = ___p1;
			NullCheck((Il2CppObject*)L_10);
			Il2CppObject * L_15 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (Il2CppObject*)L_10, (Action_1_t985559125 *)L_13, (int32_t)L_14);
			NullCheck((ListDisposable_t2393830995 *)L_9);
			VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_9, (Il2CppObject *)L_15);
		}

IL_004b:
		{
			Il2CppObject* L_16 = V_2;
			NullCheck((Il2CppObject *)L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
			if (L_17)
			{
				goto IL_002b;
			}
		}

IL_0056:
		{
			IL2CPP_LEAVE(0x66, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_18 = V_2;
			if (L_18)
			{
				goto IL_005f;
			}
		}

IL_005e:
		{
			IL2CPP_END_FINALLY(91)
		}

IL_005f:
		{
			Il2CppObject* L_19 = V_2;
			NullCheck((Il2CppObject *)L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
			IL2CPP_END_FINALLY(91)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0066:
	{
		CellJoinDisposable_1_t3736493403 * L_20 = V_0;
		return L_20;
	}
}
// T CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>::<>m__198(ICell`1<T>)
extern "C"  Il2CppObject * U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__198_m1110291974_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___cell0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___cell0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (Il2CppObject*)L_0);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
