﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Action_1_t517714524;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey97`2<System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey97_2_t564295989  : public Il2CppObject
{
public:
	// System.Action`1<UniRx.Tuple`2<T0,T1>> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey97`2::h
	Action_1_t517714524 * ___h_0;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey97_2_t564295989, ___h_0)); }
	inline Action_1_t517714524 * get_h_0() const { return ___h_0; }
	inline Action_1_t517714524 ** get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(Action_1_t517714524 * value)
	{
		___h_0 = value;
		Il2CppCodeGenWriteBarrier(&___h_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
