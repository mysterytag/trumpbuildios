﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>
struct ValueCollection_t3704248014;
// System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>
struct Dictionary_2_t1782110920;
// System.Collections.Generic.IEnumerator`1<PlayFab.UUnit.Region>
struct IEnumerator_1_t277898638;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// PlayFab.UUnit.Region[]
struct RegionU5BU5D_t4128254283;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_Region3089759486.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1549138861.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2981156712_gshared (ValueCollection_t3704248014 * __this, Dictionary_2_t1782110920 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2981156712(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3704248014 *, Dictionary_2_t1782110920 *, const MethodInfo*))ValueCollection__ctor_m2981156712_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3013662666_gshared (ValueCollection_t3704248014 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3013662666(__this, ___item0, method) ((  void (*) (ValueCollection_t3704248014 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3013662666_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m436091283_gshared (ValueCollection_t3704248014 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m436091283(__this, method) ((  void (*) (ValueCollection_t3704248014 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m436091283_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1016525920_gshared (ValueCollection_t3704248014 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1016525920(__this, ___item0, method) ((  bool (*) (ValueCollection_t3704248014 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1016525920_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3345065221_gshared (ValueCollection_t3704248014 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3345065221(__this, ___item0, method) ((  bool (*) (ValueCollection_t3704248014 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3345065221_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1755159635_gshared (ValueCollection_t3704248014 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1755159635(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3704248014 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1755159635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3956555479_gshared (ValueCollection_t3704248014 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3956555479(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3704248014 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3956555479_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1027045542_gshared (ValueCollection_t3704248014 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1027045542(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3704248014 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1027045542_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3586669075_gshared (ValueCollection_t3704248014 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3586669075(__this, method) ((  bool (*) (ValueCollection_t3704248014 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3586669075_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m803128435_gshared (ValueCollection_t3704248014 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m803128435(__this, method) ((  bool (*) (ValueCollection_t3704248014 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m803128435_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3132898853_gshared (ValueCollection_t3704248014 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3132898853(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3704248014 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3132898853_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3559326127_gshared (ValueCollection_t3704248014 * __this, RegionU5BU5D_t4128254283* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3559326127(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3704248014 *, RegionU5BU5D_t4128254283*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3559326127_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::GetEnumerator()
extern "C"  Enumerator_t1549138863  ValueCollection_GetEnumerator_m3611913560_gshared (ValueCollection_t3704248014 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3611913560(__this, method) ((  Enumerator_t1549138863  (*) (ValueCollection_t3704248014 *, const MethodInfo*))ValueCollection_GetEnumerator_m3611913560_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1366250669_gshared (ValueCollection_t3704248014 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1366250669(__this, method) ((  int32_t (*) (ValueCollection_t3704248014 *, const MethodInfo*))ValueCollection_get_Count_m1366250669_gshared)(__this, method)
