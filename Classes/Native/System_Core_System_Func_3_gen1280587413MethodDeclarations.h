﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen2554989993MethodDeclarations.h"

// System.Void System.Func`3<UniRx.IObserver`1<System.Double>,UniRx.CancellationToken,System.Collections.IEnumerator>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m1230059902(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t1280587413 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m2183027641_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<UniRx.IObserver`1<System.Double>,UniRx.CancellationToken,System.Collections.IEnumerator>::Invoke(T1,T2)
#define Func_3_Invoke_m3985215371(__this, ___arg10, ___arg21, method) ((  Il2CppObject * (*) (Func_3_t1280587413 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))Func_3_Invoke_m1848481136_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<UniRx.IObserver`1<System.Double>,UniRx.CancellationToken,System.Collections.IEnumerator>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m2374278924(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t1280587413 *, Il2CppObject*, CancellationToken_t1439151560 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m2647552753_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<UniRx.IObserver`1<System.Double>,UniRx.CancellationToken,System.Collections.IEnumerator>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m649703088(__this, ___result0, method) ((  Il2CppObject * (*) (Func_3_t1280587413 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m847220587_gshared)(__this, ___result0, method)
