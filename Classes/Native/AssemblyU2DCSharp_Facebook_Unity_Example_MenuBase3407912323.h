﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Facebook_Unity_Example_ConsoleBa3696948539.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode698979849.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MenuBase
struct  MenuBase_t3407912323  : public ConsoleBase_t3696948539
{
public:

public:
};

struct MenuBase_t3407912323_StaticFields
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Example.MenuBase::shareDialogMode
	int32_t ___shareDialogMode_13;

public:
	inline static int32_t get_offset_of_shareDialogMode_13() { return static_cast<int32_t>(offsetof(MenuBase_t3407912323_StaticFields, ___shareDialogMode_13)); }
	inline int32_t get_shareDialogMode_13() const { return ___shareDialogMode_13; }
	inline int32_t* get_address_of_shareDialogMode_13() { return &___shareDialogMode_13; }
	inline void set_shareDialogMode_13(int32_t value)
	{
		___shareDialogMode_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
