﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.UUnitTestCase
struct UUnitTestCase_t14187365;
// System.String
struct String_t;
// PlayFab.UUnit.UUnitTestResult
struct UUnitTestResult_t4060832146;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_UUnitTestResult4060832146.h"

// System.Void PlayFab.UUnit.UUnitTestCase::.ctor()
extern "C"  void UUnitTestCase__ctor_m3465081696 (UUnitTestCase_t14187365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestCase::SetTest(System.String)
extern "C"  void UUnitTestCase_SetTest_m4288317424 (UUnitTestCase_t14187365 * __this, String_t* ___testMethodName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestCase::Run(PlayFab.UUnit.UUnitTestResult)
extern "C"  void UUnitTestCase_Run_m2279198001 (UUnitTestCase_t14187365 * __this, UUnitTestResult_t4060832146 * ___testResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestCase::SetUp()
extern "C"  void UUnitTestCase_SetUp_m1999461339 (UUnitTestCase_t14187365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestCase::TearDown()
extern "C"  void UUnitTestCase_TearDown_m3172238728 (UUnitTestCase_t14187365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
