﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalNotificationManager
struct LocalNotificationManager_t980457207;
// INotificationManager
struct INotificationManager_t3349952825;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void LocalNotificationManager::.ctor()
extern "C"  void LocalNotificationManager__ctor_m3220561284 (LocalNotificationManager_t980457207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// INotificationManager LocalNotificationManager::get_manager()
extern "C"  Il2CppObject * LocalNotificationManager_get_manager_m1297565986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalNotificationManager::SetNotification(System.String,System.Int32,System.String)
extern "C"  void LocalNotificationManager_SetNotification_m388117024 (Il2CppObject * __this /* static, unused */, String_t* ___message0, int32_t ___delay1, String_t* ___title2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalNotificationManager::CancelAllNotification()
extern "C"  void LocalNotificationManager_CancelAllNotification_m2926561140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
