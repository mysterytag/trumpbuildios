﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
struct BaseReferenceHolder_t634391631;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef3541566221.h"

// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::.ctor(System.IntPtr)
extern "C"  void BaseReferenceHolder__ctor_m2809683036 (BaseReferenceHolder_t634391631 * __this, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.BaseReferenceHolder::IsDisposed()
extern "C"  bool BaseReferenceHolder_IsDisposed_m4043367279 (BaseReferenceHolder_t634391631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.BaseReferenceHolder::SelfPtr()
extern "C"  HandleRef_t3541566221  BaseReferenceHolder_SelfPtr_m1052503806 (BaseReferenceHolder_t634391631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Finalize()
extern "C"  void BaseReferenceHolder_Finalize_m1761231466 (BaseReferenceHolder_t634391631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose()
extern "C"  void BaseReferenceHolder_Dispose_m2168547637 (BaseReferenceHolder_t634391631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.PInvoke.BaseReferenceHolder::AsPointer()
extern "C"  IntPtr_t BaseReferenceHolder_AsPointer_m1925325420 (BaseReferenceHolder_t634391631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose(System.Boolean)
extern "C"  void BaseReferenceHolder_Dispose_m1458134508 (BaseReferenceHolder_t634391631 * __this, bool ___fromFinalizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
