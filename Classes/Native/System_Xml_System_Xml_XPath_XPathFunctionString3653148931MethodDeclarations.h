﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionString
struct XPathFunctionString_t3653148931;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionString::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionString__ctor_m3584382127 (XPathFunctionString_t3653148931 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionString::get_ReturnType()
extern "C"  int32_t XPathFunctionString_get_ReturnType_m463862549 (XPathFunctionString_t3653148931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionString::get_Peer()
extern "C"  bool XPathFunctionString_get_Peer_m723679185 (XPathFunctionString_t3653148931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionString::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionString_Evaluate_m2019360384 (XPathFunctionString_t3653148931 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionString::ToString()
extern "C"  String_t* XPathFunctionString_ToString_m2064029165 (XPathFunctionString_t3653148931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
