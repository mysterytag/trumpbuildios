﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// StreamAPI/<Filter>c__AnonStorey134`1<System.Object>
struct U3CFilterU3Ec__AnonStorey134_1_t1444715119;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1<System.Object>
struct  U3CFilterU3Ec__AnonStorey135_1_t999690120  : public Il2CppObject
{
public:
	// System.Action`1<T> StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1::reaction
	Action_1_t985559125 * ___reaction_0;
	// StreamAPI/<Filter>c__AnonStorey134`1<T> StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1::<>f__ref$308
	U3CFilterU3Ec__AnonStorey134_1_t1444715119 * ___U3CU3Ef__refU24308_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey135_1_t999690120, ___reaction_0)); }
	inline Action_1_t985559125 * get_reaction_0() const { return ___reaction_0; }
	inline Action_1_t985559125 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_1_t985559125 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24308_1() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey135_1_t999690120, ___U3CU3Ef__refU24308_1)); }
	inline U3CFilterU3Ec__AnonStorey134_1_t1444715119 * get_U3CU3Ef__refU24308_1() const { return ___U3CU3Ef__refU24308_1; }
	inline U3CFilterU3Ec__AnonStorey134_1_t1444715119 ** get_address_of_U3CU3Ef__refU24308_1() { return &___U3CU3Ef__refU24308_1; }
	inline void set_U3CU3Ef__refU24308_1(U3CFilterU3Ec__AnonStorey134_1_t1444715119 * value)
	{
		___U3CU3Ef__refU24308_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24308_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
