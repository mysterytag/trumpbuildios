﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C<System.Object>
struct U3CRunTimerU3Ec__AnonStorey6C_t4277735173;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C<System.Object>::.ctor()
extern "C"  void U3CRunTimerU3Ec__AnonStorey6C__ctor_m576555264_gshared (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * __this, const MethodInfo* method);
#define U3CRunTimerU3Ec__AnonStorey6C__ctor_m576555264(__this, method) ((  void (*) (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 *, const MethodInfo*))U3CRunTimerU3Ec__AnonStorey6C__ctor_m576555264_gshared)(__this, method)
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout/<RunTimer>c__AnonStorey6C<System.Object>::<>m__8A()
extern "C"  void U3CRunTimerU3Ec__AnonStorey6C_U3CU3Em__8A_m2327092370_gshared (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 * __this, const MethodInfo* method);
#define U3CRunTimerU3Ec__AnonStorey6C_U3CU3Em__8A_m2327092370(__this, method) ((  void (*) (U3CRunTimerU3Ec__AnonStorey6C_t4277735173 *, const MethodInfo*))U3CRunTimerU3Ec__AnonStorey6C_U3CU3Em__8A_m2327092370_gshared)(__this, method)
