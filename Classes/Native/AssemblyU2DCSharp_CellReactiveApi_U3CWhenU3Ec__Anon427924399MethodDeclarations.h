﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Boolean>
struct U3CWhenU3Ec__AnonStorey10B_1_t427924399;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Boolean>::.ctor()
extern "C"  void U3CWhenU3Ec__AnonStorey10B_1__ctor_m2201070194_gshared (U3CWhenU3Ec__AnonStorey10B_1_t427924399 * __this, const MethodInfo* method);
#define U3CWhenU3Ec__AnonStorey10B_1__ctor_m2201070194(__this, method) ((  void (*) (U3CWhenU3Ec__AnonStorey10B_1_t427924399 *, const MethodInfo*))U3CWhenU3Ec__AnonStorey10B_1__ctor_m2201070194_gshared)(__this, method)
// System.Void CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Boolean>::<>m__195(T)
extern "C"  void U3CWhenU3Ec__AnonStorey10B_1_U3CU3Em__195_m1371873162_gshared (U3CWhenU3Ec__AnonStorey10B_1_t427924399 * __this, bool ___val0, const MethodInfo* method);
#define U3CWhenU3Ec__AnonStorey10B_1_U3CU3Em__195_m1371873162(__this, ___val0, method) ((  void (*) (U3CWhenU3Ec__AnonStorey10B_1_t427924399 *, bool, const MethodInfo*))U3CWhenU3Ec__AnonStorey10B_1_U3CU3Em__195_m1371873162_gshared)(__this, ___val0, method)
