﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetTradeStatusResponseCallback
struct GetTradeStatusResponseCallback_t1850044710;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetTradeStatusRequest
struct GetTradeStatusRequest_t4199327007;
// PlayFab.ClientModels.GetTradeStatusResponse
struct GetTradeStatusResponse_t295324113;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTradeSta4199327007.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTradeStat295324113.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetTradeStatusResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetTradeStatusResponseCallback__ctor_m3389252373 (GetTradeStatusResponseCallback_t1850044710 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTradeStatusResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetTradeStatusRequest,PlayFab.ClientModels.GetTradeStatusResponse,PlayFab.PlayFabError,System.Object)
extern "C"  void GetTradeStatusResponseCallback_Invoke_m850882822 (GetTradeStatusResponseCallback_t1850044710 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTradeStatusRequest_t4199327007 * ___request2, GetTradeStatusResponse_t295324113 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetTradeStatusResponseCallback_t1850044710(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetTradeStatusRequest_t4199327007 * ___request2, GetTradeStatusResponse_t295324113 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetTradeStatusResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetTradeStatusRequest,PlayFab.ClientModels.GetTradeStatusResponse,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetTradeStatusResponseCallback_BeginInvoke_m1969508651 (GetTradeStatusResponseCallback_t1850044710 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTradeStatusRequest_t4199327007 * ___request2, GetTradeStatusResponse_t295324113 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTradeStatusResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetTradeStatusResponseCallback_EndInvoke_m478554021 (GetTradeStatusResponseCallback_t1850044710 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
