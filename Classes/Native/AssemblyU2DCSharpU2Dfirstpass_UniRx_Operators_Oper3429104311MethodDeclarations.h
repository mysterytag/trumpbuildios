﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Pair`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t3429104311;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Pair`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m227286753_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m227286753(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m227286753_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Pair`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1687459142_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1687459142(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1687459142_gshared)(__this, method)
