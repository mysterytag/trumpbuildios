﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::.ctor()
#define Stream_1__ctor_m527636568(__this, method) ((  void (*) (Stream_1_t3962515817 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::Send(T)
#define Stream_1_Send_m3351821418(__this, ___value0, method) ((  void (*) (Stream_1_t3962515817 *, CollectionRemoveEvent_1_t976409499 *, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m864903040(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t3962515817 *, CollectionRemoveEvent_1_t976409499 *, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<CollectionRemoveEvent`1<SettingsButton>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m2082454914(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t3962515817 *, Action_1_t1124862204 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<CollectionRemoveEvent`1<SettingsButton>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m2436717157(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t3962515817 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::Dispose()
#define Stream_1_Dispose_m150883989(__this, method) ((  void (*) (Stream_1_t3962515817 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m2294636200(__this, ___stream0, method) ((  void (*) (Stream_1_t3962515817 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::ClearInputStream()
#define Stream_1_ClearInputStream_m614711689(__this, method) ((  void (*) (Stream_1_t3962515817 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m4119624669(__this, method) ((  void (*) (Stream_1_t3962515817 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::Unpack(System.Int32)
#define Stream_1_Unpack_m2609261359(__this, ___p0, method) ((  void (*) (Stream_1_t3962515817 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<SettingsButton>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m511455477(__this, ___val0, method) ((  void (*) (Stream_1_t3962515817 *, CollectionRemoveEvent_1_t976409499 *, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
