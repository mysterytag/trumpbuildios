﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<UnityEngine.Quaternion>
struct Predicate_1_t2462679877;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Predicate`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3166806662_gshared (Predicate_1_t2462679877 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m3166806662(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t2462679877 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m3166806662_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2591548732_gshared (Predicate_1_t2462679877 * __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m2591548732(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2462679877 *, Quaternion_t1891715979 , const MethodInfo*))Predicate_1_Invoke_m2591548732_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2803378123_gshared (Predicate_1_t2462679877 * __this, Quaternion_t1891715979  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m2803378123(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t2462679877 *, Quaternion_t1891715979 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2803378123_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3422895128_gshared (Predicate_1_t2462679877 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m3422895128(__this, ___result0, method) ((  bool (*) (Predicate_1_t2462679877 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3422895128_gshared)(__this, ___result0, method)
