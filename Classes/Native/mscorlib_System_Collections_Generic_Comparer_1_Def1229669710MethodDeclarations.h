﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Quaternion>
struct DefaultComparer_t1229669711;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Quaternion>::.ctor()
extern "C"  void DefaultComparer__ctor_m2912696155_gshared (DefaultComparer_t1229669711 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2912696155(__this, method) ((  void (*) (DefaultComparer_t1229669711 *, const MethodInfo*))DefaultComparer__ctor_m2912696155_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Quaternion>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m737442556_gshared (DefaultComparer_t1229669711 * __this, Quaternion_t1891715979  ___x0, Quaternion_t1891715979  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m737442556(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1229669711 *, Quaternion_t1891715979 , Quaternion_t1891715979 , const MethodInfo*))DefaultComparer_Compare_m737442556_gshared)(__this, ___x0, ___y1, method)
