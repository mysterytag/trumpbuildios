﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Double,System.Object>
struct Dictionary_2_t566215076;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va333243017.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Double,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2987011443_gshared (Enumerator_t333243019 * __this, Dictionary_2_t566215076 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2987011443(__this, ___host0, method) ((  void (*) (Enumerator_t333243019 *, Dictionary_2_t566215076 *, const MethodInfo*))Enumerator__ctor_m2987011443_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Double,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1768929166_gshared (Enumerator_t333243019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1768929166(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243019 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1768929166_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Double,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2027716962_gshared (Enumerator_t333243019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2027716962(__this, method) ((  void (*) (Enumerator_t333243019 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2027716962_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Double,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1038288597_gshared (Enumerator_t333243019 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1038288597(__this, method) ((  void (*) (Enumerator_t333243019 *, const MethodInfo*))Enumerator_Dispose_m1038288597_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Double,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2444431694_gshared (Enumerator_t333243019 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2444431694(__this, method) ((  bool (*) (Enumerator_t333243019 *, const MethodInfo*))Enumerator_MoveNext_m2444431694_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Double,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m592899700_gshared (Enumerator_t333243019 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m592899700(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243019 *, const MethodInfo*))Enumerator_get_Current_m592899700_gshared)(__this, method)
