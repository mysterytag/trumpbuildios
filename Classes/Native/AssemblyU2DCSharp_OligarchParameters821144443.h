﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t1886596500;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_OligarchType2745121163.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OligarchParameters
struct  OligarchParameters_t821144443  : public Il2CppObject
{
public:
	// System.Boolean OligarchParameters::ActivateTopColorChangingBySpeed
	bool ___ActivateTopColorChangingBySpeed_0;
	// System.Boolean OligarchParameters::BucketIsActive
	bool ___BucketIsActive_1;
	// System.Boolean OligarchParameters::AchivIsActive
	bool ___AchivIsActive_2;
	// System.Boolean OligarchParameters::NeedDonateButtonsInTable
	bool ___NeedDonateButtonsInTable_3;
	// System.Boolean OligarchParameters::UseMoneyParent
	bool ___UseMoneyParent_4;
	// UnityEngine.Material OligarchParameters::UiGrayMaterial
	Material_t1886596500 * ___UiGrayMaterial_5;
	// OligarchType OligarchParameters::OligarchType
	int32_t ___OligarchType_6;
	// System.Single OligarchParameters::OffsetForTableWithPremium
	float ___OffsetForTableWithPremium_7;
	// System.Single OligarchParameters::OffsetForTableWithoutPremium
	float ___OffsetForTableWithoutPremium_8;
	// System.String OligarchParameters::MoneySymbol
	String_t* ___MoneySymbol_9;
	// UnityEngine.Color OligarchParameters::TopPanelFromColor
	Color_t1588175760  ___TopPanelFromColor_10;
	// UnityEngine.Color OligarchParameters::TopPanelToColor
	Color_t1588175760  ___TopPanelToColor_11;

public:
	inline static int32_t get_offset_of_ActivateTopColorChangingBySpeed_0() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___ActivateTopColorChangingBySpeed_0)); }
	inline bool get_ActivateTopColorChangingBySpeed_0() const { return ___ActivateTopColorChangingBySpeed_0; }
	inline bool* get_address_of_ActivateTopColorChangingBySpeed_0() { return &___ActivateTopColorChangingBySpeed_0; }
	inline void set_ActivateTopColorChangingBySpeed_0(bool value)
	{
		___ActivateTopColorChangingBySpeed_0 = value;
	}

	inline static int32_t get_offset_of_BucketIsActive_1() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___BucketIsActive_1)); }
	inline bool get_BucketIsActive_1() const { return ___BucketIsActive_1; }
	inline bool* get_address_of_BucketIsActive_1() { return &___BucketIsActive_1; }
	inline void set_BucketIsActive_1(bool value)
	{
		___BucketIsActive_1 = value;
	}

	inline static int32_t get_offset_of_AchivIsActive_2() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___AchivIsActive_2)); }
	inline bool get_AchivIsActive_2() const { return ___AchivIsActive_2; }
	inline bool* get_address_of_AchivIsActive_2() { return &___AchivIsActive_2; }
	inline void set_AchivIsActive_2(bool value)
	{
		___AchivIsActive_2 = value;
	}

	inline static int32_t get_offset_of_NeedDonateButtonsInTable_3() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___NeedDonateButtonsInTable_3)); }
	inline bool get_NeedDonateButtonsInTable_3() const { return ___NeedDonateButtonsInTable_3; }
	inline bool* get_address_of_NeedDonateButtonsInTable_3() { return &___NeedDonateButtonsInTable_3; }
	inline void set_NeedDonateButtonsInTable_3(bool value)
	{
		___NeedDonateButtonsInTable_3 = value;
	}

	inline static int32_t get_offset_of_UseMoneyParent_4() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___UseMoneyParent_4)); }
	inline bool get_UseMoneyParent_4() const { return ___UseMoneyParent_4; }
	inline bool* get_address_of_UseMoneyParent_4() { return &___UseMoneyParent_4; }
	inline void set_UseMoneyParent_4(bool value)
	{
		___UseMoneyParent_4 = value;
	}

	inline static int32_t get_offset_of_UiGrayMaterial_5() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___UiGrayMaterial_5)); }
	inline Material_t1886596500 * get_UiGrayMaterial_5() const { return ___UiGrayMaterial_5; }
	inline Material_t1886596500 ** get_address_of_UiGrayMaterial_5() { return &___UiGrayMaterial_5; }
	inline void set_UiGrayMaterial_5(Material_t1886596500 * value)
	{
		___UiGrayMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___UiGrayMaterial_5, value);
	}

	inline static int32_t get_offset_of_OligarchType_6() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___OligarchType_6)); }
	inline int32_t get_OligarchType_6() const { return ___OligarchType_6; }
	inline int32_t* get_address_of_OligarchType_6() { return &___OligarchType_6; }
	inline void set_OligarchType_6(int32_t value)
	{
		___OligarchType_6 = value;
	}

	inline static int32_t get_offset_of_OffsetForTableWithPremium_7() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___OffsetForTableWithPremium_7)); }
	inline float get_OffsetForTableWithPremium_7() const { return ___OffsetForTableWithPremium_7; }
	inline float* get_address_of_OffsetForTableWithPremium_7() { return &___OffsetForTableWithPremium_7; }
	inline void set_OffsetForTableWithPremium_7(float value)
	{
		___OffsetForTableWithPremium_7 = value;
	}

	inline static int32_t get_offset_of_OffsetForTableWithoutPremium_8() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___OffsetForTableWithoutPremium_8)); }
	inline float get_OffsetForTableWithoutPremium_8() const { return ___OffsetForTableWithoutPremium_8; }
	inline float* get_address_of_OffsetForTableWithoutPremium_8() { return &___OffsetForTableWithoutPremium_8; }
	inline void set_OffsetForTableWithoutPremium_8(float value)
	{
		___OffsetForTableWithoutPremium_8 = value;
	}

	inline static int32_t get_offset_of_MoneySymbol_9() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___MoneySymbol_9)); }
	inline String_t* get_MoneySymbol_9() const { return ___MoneySymbol_9; }
	inline String_t** get_address_of_MoneySymbol_9() { return &___MoneySymbol_9; }
	inline void set_MoneySymbol_9(String_t* value)
	{
		___MoneySymbol_9 = value;
		Il2CppCodeGenWriteBarrier(&___MoneySymbol_9, value);
	}

	inline static int32_t get_offset_of_TopPanelFromColor_10() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___TopPanelFromColor_10)); }
	inline Color_t1588175760  get_TopPanelFromColor_10() const { return ___TopPanelFromColor_10; }
	inline Color_t1588175760 * get_address_of_TopPanelFromColor_10() { return &___TopPanelFromColor_10; }
	inline void set_TopPanelFromColor_10(Color_t1588175760  value)
	{
		___TopPanelFromColor_10 = value;
	}

	inline static int32_t get_offset_of_TopPanelToColor_11() { return static_cast<int32_t>(offsetof(OligarchParameters_t821144443, ___TopPanelToColor_11)); }
	inline Color_t1588175760  get_TopPanelToColor_11() const { return ___TopPanelToColor_11; }
	inline Color_t1588175760 * get_address_of_TopPanelToColor_11() { return &___TopPanelToColor_11; }
	inline void set_TopPanelToColor_11(Color_t1588175760  value)
	{
		___TopPanelToColor_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
