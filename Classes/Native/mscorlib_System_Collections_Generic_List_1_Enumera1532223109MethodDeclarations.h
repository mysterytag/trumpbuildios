﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.JSON>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1611560045(__this, ___l0, method) ((  void (*) (Enumerator_t1532223109 *, List_1_t3446440117 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.JSON>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3342520261(__this, method) ((  void (*) (Enumerator_t1532223109 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<OnePF.JSON>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m770363067(__this, method) ((  Il2CppObject * (*) (Enumerator_t1532223109 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.JSON>::Dispose()
#define Enumerator_Dispose_m4164496786(__this, method) ((  void (*) (Enumerator_t1532223109 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.JSON>::VerifyState()
#define Enumerator_VerifyState_m521655243(__this, method) ((  void (*) (Enumerator_t1532223109 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<OnePF.JSON>::MoveNext()
#define Enumerator_MoveNext_m2799148149(__this, method) ((  bool (*) (Enumerator_t1532223109 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<OnePF.JSON>::get_Current()
#define Enumerator_get_Current_m2476545060(__this, method) ((  JSON_t2649481148 * (*) (Enumerator_t1532223109 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
