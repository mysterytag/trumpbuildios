﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.NullIterator
struct NullIterator_t979652658;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1624538935;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3696234203;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1624538935.h"
#include "System_Xml_System_Xml_XPath_NullIterator979652658.h"

// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void NullIterator__ctor_m2854644327 (NullIterator_t979652658 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void NullIterator__ctor_m4158990173 (NullIterator_t979652658 * __this, XPathNavigator_t1624538935 * ___nav0, Il2CppObject * ___nsm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.NullIterator)
extern "C"  void NullIterator__ctor_m233625713 (NullIterator_t979652658 * __this, NullIterator_t979652658 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.NullIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * NullIterator_Clone_m2491346443 (NullIterator_t979652658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.NullIterator::MoveNextCore()
extern "C"  bool NullIterator_MoveNextCore_m161861338 (NullIterator_t979652658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XPath.NullIterator::get_CurrentPosition()
extern "C"  int32_t NullIterator_get_CurrentPosition_m3673814846 (NullIterator_t979652658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator System.Xml.XPath.NullIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * NullIterator_get_Current_m3786274181 (NullIterator_t979652658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
