﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergEngineTools/<PressStream>c__AnonStorey14E
struct U3CPressStreamU3Ec__AnonStorey14E_t2542632690;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<UnityEngine.EventSystems.BaseEventData>
struct Action_1_t3695556431;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void ZergEngineTools/<PressStream>c__AnonStorey14E::.ctor()
extern "C"  void U3CPressStreamU3Ec__AnonStorey14E__ctor_m630648751 (U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable ZergEngineTools/<PressStream>c__AnonStorey14E::<>m__1FB(System.Action`1<UnityEngine.EventSystems.BaseEventData>,Priority)
extern "C"  Il2CppObject * U3CPressStreamU3Ec__AnonStorey14E_U3CU3Em__1FB_m2964617654 (U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * __this, Action_1_t3695556431 * ___reaction0, int32_t ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
