﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_TaskInfo_g1064508404MethodDeclarations.h"

// System.Void Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>::.ctor(TTask,System.Int32)
#define TaskInfo__ctor_m2840699106(__this, ___task0, ___priority1, method) ((  void (*) (TaskInfo_t3693212827 *, Il2CppObject *, int32_t, const MethodInfo*))TaskInfo__ctor_m1167730649_gshared)(__this, ___task0, ___priority1, method)
