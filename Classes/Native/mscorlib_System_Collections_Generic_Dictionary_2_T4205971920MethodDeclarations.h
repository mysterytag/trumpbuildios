﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Object>
struct Transform_1_t4205971920;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1299617905_gshared (Transform_1_t4205971920 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1299617905(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t4205971920 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1299617905_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m132017899_gshared (Transform_1_t4205971920 * __this, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m132017899(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t4205971920 *, double, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m132017899_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m756347978_gshared (Transform_1_t4205971920 * __this, double ___key0, Il2CppObject * ___value1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m756347978(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t4205971920 *, double, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m756347978_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1898856639_gshared (Transform_1_t4205971920 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1898856639(__this, ___result0, method) ((  Il2CppObject * (*) (Transform_1_t4205971920 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1898856639_gshared)(__this, ___result0, method)
