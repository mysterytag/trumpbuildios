﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070MethodDeclarations.h"

// System.Void CollectionReplaceEvent`1<DonateButton>::.ctor(System.Int32,T,T)
#define CollectionReplaceEvent_1__ctor_m4289862182(__this, ___index0, ___oldValue1, ___newValue2, method) ((  void (*) (CollectionReplaceEvent_1_t2331019091 *, int32_t, DonateButton_t670753441 *, DonateButton_t670753441 *, const MethodInfo*))CollectionReplaceEvent_1__ctor_m1290910597_gshared)(__this, ___index0, ___oldValue1, ___newValue2, method)
// System.Int32 CollectionReplaceEvent`1<DonateButton>::get_Index()
#define CollectionReplaceEvent_1_get_Index_m4129843678(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t2331019091 *, const MethodInfo*))CollectionReplaceEvent_1_get_Index_m2241178009_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<DonateButton>::set_Index(System.Int32)
#define CollectionReplaceEvent_1_set_Index_m2800000265(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t2331019091 *, int32_t, const MethodInfo*))CollectionReplaceEvent_1_set_Index_m4096015976_gshared)(__this, ___value0, method)
// T CollectionReplaceEvent`1<DonateButton>::get_OldValue()
#define CollectionReplaceEvent_1_get_OldValue_m2051272785(__this, method) ((  DonateButton_t670753441 * (*) (CollectionReplaceEvent_1_t2331019091 *, const MethodInfo*))CollectionReplaceEvent_1_get_OldValue_m3651064692_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<DonateButton>::set_OldValue(T)
#define CollectionReplaceEvent_1_set_OldValue_m33917080(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t2331019091 *, DonateButton_t670753441 *, const MethodInfo*))CollectionReplaceEvent_1_set_OldValue_m1129314551_gshared)(__this, ___value0, method)
// T CollectionReplaceEvent`1<DonateButton>::get_NewValue()
#define CollectionReplaceEvent_1_get_NewValue_m768723640(__this, method) ((  DonateButton_t670753441 * (*) (CollectionReplaceEvent_1_t2331019091 *, const MethodInfo*))CollectionReplaceEvent_1_get_NewValue_m2368515547_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<DonateButton>::set_NewValue(T)
#define CollectionReplaceEvent_1_set_NewValue_m3224566545(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t2331019091 *, DonateButton_t670753441 *, const MethodInfo*))CollectionReplaceEvent_1_set_NewValue_m24996720_gshared)(__this, ___value0, method)
// System.String CollectionReplaceEvent`1<DonateButton>::ToString()
#define CollectionReplaceEvent_1_ToString_m673766990(__this, method) ((  String_t* (*) (CollectionReplaceEvent_1_t2331019091 *, const MethodInfo*))CollectionReplaceEvent_1_ToString_m2524726313_gshared)(__this, method)
