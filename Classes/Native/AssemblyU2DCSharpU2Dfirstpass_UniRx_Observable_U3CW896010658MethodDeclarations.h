﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>
struct U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::.ctor()
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1__ctor_m3165280184_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method);
#define U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1__ctor_m3165280184(__this, method) ((  void (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1__ctor_m3165280184_gshared)(__this, method)
// System.Object UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1595304922_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method);
#define U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1595304922(__this, method) ((  Il2CppObject * (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1595304922_gshared)(__this, method)
// System.Object UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_System_Collections_IEnumerator_get_Current_m3158270830_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method);
#define U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_System_Collections_IEnumerator_get_Current_m3158270830(__this, method) ((  Il2CppObject * (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_System_Collections_IEnumerator_get_Current_m3158270830_gshared)(__this, method)
// System.Boolean UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::MoveNext()
extern "C"  bool U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_MoveNext_m1303354964_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method);
#define U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_MoveNext_m1303354964(__this, method) ((  bool (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_MoveNext_m1303354964_gshared)(__this, method)
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::Dispose()
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Dispose_m895694325_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method);
#define U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Dispose_m895694325(__this, method) ((  void (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Dispose_m895694325_gshared)(__this, method)
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::Reset()
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Reset_m811713125_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method);
#define U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Reset_m811713125(__this, method) ((  void (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_Reset_m811713125_gshared)(__this, method)
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::<>__Finally0()
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally0_m597996475_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method);
#define U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally0_m597996475(__this, method) ((  void (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally0_m597996475_gshared)(__this, method)
// System.Void UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>::<>__Finally1()
extern "C"  void U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally1_m597997436_gshared (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 * __this, const MethodInfo* method);
#define U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally1_m597997436(__this, method) ((  void (*) (U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658 *, const MethodInfo*))U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_U3CU3E__Finally1_m597997436_gshared)(__this, method)
