﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext>
struct IEnumerator_1_t644623043;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext/<>c__Iterator43
struct  U3CU3Ec__Iterator43_t705133326  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext/<>c__Iterator43::<$s_261>__0
	Il2CppObject* ___U3CU24s_261U3E__0_0;
	// Zenject.InjectContext Zenject.InjectContext/<>c__Iterator43::<context>__1
	InjectContext_t3456483891 * ___U3CcontextU3E__1_1;
	// System.Int32 Zenject.InjectContext/<>c__Iterator43::$PC
	int32_t ___U24PC_2;
	// Zenject.InjectContext Zenject.InjectContext/<>c__Iterator43::$current
	InjectContext_t3456483891 * ___U24current_3;
	// Zenject.InjectContext Zenject.InjectContext/<>c__Iterator43::<>f__this
	InjectContext_t3456483891 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_261U3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator43_t705133326, ___U3CU24s_261U3E__0_0)); }
	inline Il2CppObject* get_U3CU24s_261U3E__0_0() const { return ___U3CU24s_261U3E__0_0; }
	inline Il2CppObject** get_address_of_U3CU24s_261U3E__0_0() { return &___U3CU24s_261U3E__0_0; }
	inline void set_U3CU24s_261U3E__0_0(Il2CppObject* value)
	{
		___U3CU24s_261U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_261U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CcontextU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator43_t705133326, ___U3CcontextU3E__1_1)); }
	inline InjectContext_t3456483891 * get_U3CcontextU3E__1_1() const { return ___U3CcontextU3E__1_1; }
	inline InjectContext_t3456483891 ** get_address_of_U3CcontextU3E__1_1() { return &___U3CcontextU3E__1_1; }
	inline void set_U3CcontextU3E__1_1(InjectContext_t3456483891 * value)
	{
		___U3CcontextU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcontextU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator43_t705133326, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator43_t705133326, ___U24current_3)); }
	inline InjectContext_t3456483891 * get_U24current_3() const { return ___U24current_3; }
	inline InjectContext_t3456483891 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(InjectContext_t3456483891 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator43_t705133326, ___U3CU3Ef__this_4)); }
	inline InjectContext_t3456483891 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline InjectContext_t3456483891 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(InjectContext_t3456483891 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
