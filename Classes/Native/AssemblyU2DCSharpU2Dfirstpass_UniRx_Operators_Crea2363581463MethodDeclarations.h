﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>
struct Create_t2363581463;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m3239614205_gshared (Create_t2363581463 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Create__ctor_m3239614205(__this, ___observer0, ___cancel1, method) ((  void (*) (Create_t2363581463 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Create__ctor_m3239614205_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>::OnNext(T)
extern "C"  void Create_OnNext_m3447503477_gshared (Create_t2363581463 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Create_OnNext_m3447503477(__this, ___value0, method) ((  void (*) (Create_t2363581463 *, Unit_t2558286038 , const MethodInfo*))Create_OnNext_m3447503477_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Create_OnError_m2309966724_gshared (Create_t2363581463 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Create_OnError_m2309966724(__this, ___error0, method) ((  void (*) (Create_t2363581463 *, Exception_t1967233988 *, const MethodInfo*))Create_OnError_m2309966724_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<UniRx.Unit>::OnCompleted()
extern "C"  void Create_OnCompleted_m1209017559_gshared (Create_t2363581463 * __this, const MethodInfo* method);
#define Create_OnCompleted_m1209017559(__this, method) ((  void (*) (Create_t2363581463 *, const MethodInfo*))Create_OnCompleted_m1209017559_gshared)(__this, method)
