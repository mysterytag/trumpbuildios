﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPlayerStatistics>c__AnonStorey91
struct U3CGetPlayerStatisticsU3Ec__AnonStorey91_t1243254623;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPlayerStatistics>c__AnonStorey91::.ctor()
extern "C"  void U3CGetPlayerStatisticsU3Ec__AnonStorey91__ctor_m3694199508 (U3CGetPlayerStatisticsU3Ec__AnonStorey91_t1243254623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPlayerStatistics>c__AnonStorey91::<>m__7E(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPlayerStatisticsU3Ec__AnonStorey91_U3CU3Em__7E_m1293818144 (U3CGetPlayerStatisticsU3Ec__AnonStorey91_t1243254623 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
