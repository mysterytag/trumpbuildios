﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// CellReactiveApi/<Hold>c__AnonStoreyFF`1<System.Object>
struct U3CHoldU3Ec__AnonStoreyFF_1_t2863379238;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1<System.Object>
struct  U3CHoldU3Ec__AnonStorey100_1_t2662411751  : public Il2CppObject
{
public:
	// System.Action`1<T> CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1::reaction
	Action_1_t985559125 * ___reaction_0;
	// CellReactiveApi/<Hold>c__AnonStoreyFF`1<T> CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1::<>f__ref$255
	U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * ___U3CU3Ef__refU24255_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CHoldU3Ec__AnonStorey100_1_t2662411751, ___reaction_0)); }
	inline Action_1_t985559125 * get_reaction_0() const { return ___reaction_0; }
	inline Action_1_t985559125 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_1_t985559125 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24255_1() { return static_cast<int32_t>(offsetof(U3CHoldU3Ec__AnonStorey100_1_t2662411751, ___U3CU3Ef__refU24255_1)); }
	inline U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * get_U3CU3Ef__refU24255_1() const { return ___U3CU3Ef__refU24255_1; }
	inline U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 ** get_address_of_U3CU3Ef__refU24255_1() { return &___U3CU3Ef__refU24255_1; }
	inline void set_U3CU3Ef__refU24255_1(U3CHoldU3Ec__AnonStoreyFF_1_t2863379238 * value)
	{
		___U3CU3Ef__refU24255_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24255_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
