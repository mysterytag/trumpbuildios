﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/<Schedule>c__AnonStorey75/<Schedule>c__AnonStorey76
struct U3CScheduleU3Ec__AnonStorey76_t3913535387;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey75/<Schedule>c__AnonStorey76::.ctor()
extern "C"  void U3CScheduleU3Ec__AnonStorey76__ctor_m2487110717 (U3CScheduleU3Ec__AnonStorey76_t3913535387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey75/<Schedule>c__AnonStorey76::<>m__9D()
extern "C"  void U3CScheduleU3Ec__AnonStorey76_U3CU3Em__9D_m124912689 (U3CScheduleU3Ec__AnonStorey76_t3913535387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
