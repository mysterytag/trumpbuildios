﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactiveUI/<SetContent>c__AnonStorey121`1<System.Object>
struct  U3CSetContentU3Ec__AnonStorey121_1_t1463683798  : public Il2CppObject
{
public:
	// System.String ReactiveUI/<SetContent>c__AnonStorey121`1::format
	String_t* ___format_0;
	// UnityEngine.UI.Text ReactiveUI/<SetContent>c__AnonStorey121`1::text
	Text_t3286458198 * ___text_1;

public:
	inline static int32_t get_offset_of_format_0() { return static_cast<int32_t>(offsetof(U3CSetContentU3Ec__AnonStorey121_1_t1463683798, ___format_0)); }
	inline String_t* get_format_0() const { return ___format_0; }
	inline String_t** get_address_of_format_0() { return &___format_0; }
	inline void set_format_0(String_t* value)
	{
		___format_0 = value;
		Il2CppCodeGenWriteBarrier(&___format_0, value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(U3CSetContentU3Ec__AnonStorey121_1_t1463683798, ___text_1)); }
	inline Text_t3286458198 * get_text_1() const { return ___text_1; }
	inline Text_t3286458198 ** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(Text_t3286458198 * value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier(&___text_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
