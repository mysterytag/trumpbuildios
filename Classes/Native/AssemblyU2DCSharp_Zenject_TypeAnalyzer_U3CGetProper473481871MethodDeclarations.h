﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47
struct U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo>
struct IEnumerator_1_t2630816222;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"

// System.Void Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::.ctor()
extern "C"  void U3CGetPropertyInjectablesU3Ec__Iterator47__ctor_m1864367420 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::System.Collections.Generic.IEnumerator<Zenject.InjectableInfo>.get_Current()
extern "C"  InjectableInfo_t1147709774 * U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_Generic_IEnumeratorU3CZenject_InjectableInfoU3E_get_Current_m1881937623 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_IEnumerator_get_Current_m526218612 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_IEnumerable_GetEnumerator_m567270965 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::System.Collections.Generic.IEnumerable<Zenject.InjectableInfo>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m4015091492 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::MoveNext()
extern "C"  bool U3CGetPropertyInjectablesU3Ec__Iterator47_MoveNext_m1591760416 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::Dispose()
extern "C"  void U3CGetPropertyInjectablesU3Ec__Iterator47_Dispose_m554011257 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::Reset()
extern "C"  void U3CGetPropertyInjectablesU3Ec__Iterator47_Reset_m3805767657 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::<>m__2B7(System.Reflection.PropertyInfo)
extern "C"  bool U3CGetPropertyInjectablesU3Ec__Iterator47_U3CU3Em__2B7_m2670351761 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
