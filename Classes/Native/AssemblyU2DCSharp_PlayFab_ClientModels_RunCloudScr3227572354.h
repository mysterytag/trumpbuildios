﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.RunCloudScriptResult
struct  RunCloudScriptResult_t3227572354  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.RunCloudScriptResult::<ActionId>k__BackingField
	String_t* ___U3CActionIdU3Ek__BackingField_2;
	// System.Int32 PlayFab.ClientModels.RunCloudScriptResult::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_3;
	// System.Int32 PlayFab.ClientModels.RunCloudScriptResult::<Revision>k__BackingField
	int32_t ___U3CRevisionU3Ek__BackingField_4;
	// System.Object PlayFab.ClientModels.RunCloudScriptResult::<Results>k__BackingField
	Il2CppObject * ___U3CResultsU3Ek__BackingField_5;
	// System.String PlayFab.ClientModels.RunCloudScriptResult::<ResultsEncoded>k__BackingField
	String_t* ___U3CResultsEncodedU3Ek__BackingField_6;
	// System.String PlayFab.ClientModels.RunCloudScriptResult::<ActionLog>k__BackingField
	String_t* ___U3CActionLogU3Ek__BackingField_7;
	// System.Double PlayFab.ClientModels.RunCloudScriptResult::<ExecutionTime>k__BackingField
	double ___U3CExecutionTimeU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CActionIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RunCloudScriptResult_t3227572354, ___U3CActionIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CActionIdU3Ek__BackingField_2() const { return ___U3CActionIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CActionIdU3Ek__BackingField_2() { return &___U3CActionIdU3Ek__BackingField_2; }
	inline void set_U3CActionIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CActionIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CActionIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RunCloudScriptResult_t3227572354, ___U3CVersionU3Ek__BackingField_3)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_3() const { return ___U3CVersionU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_3() { return &___U3CVersionU3Ek__BackingField_3; }
	inline void set_U3CVersionU3Ek__BackingField_3(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CRevisionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RunCloudScriptResult_t3227572354, ___U3CRevisionU3Ek__BackingField_4)); }
	inline int32_t get_U3CRevisionU3Ek__BackingField_4() const { return ___U3CRevisionU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CRevisionU3Ek__BackingField_4() { return &___U3CRevisionU3Ek__BackingField_4; }
	inline void set_U3CRevisionU3Ek__BackingField_4(int32_t value)
	{
		___U3CRevisionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CResultsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RunCloudScriptResult_t3227572354, ___U3CResultsU3Ek__BackingField_5)); }
	inline Il2CppObject * get_U3CResultsU3Ek__BackingField_5() const { return ___U3CResultsU3Ek__BackingField_5; }
	inline Il2CppObject ** get_address_of_U3CResultsU3Ek__BackingField_5() { return &___U3CResultsU3Ek__BackingField_5; }
	inline void set_U3CResultsU3Ek__BackingField_5(Il2CppObject * value)
	{
		___U3CResultsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResultsU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CResultsEncodedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RunCloudScriptResult_t3227572354, ___U3CResultsEncodedU3Ek__BackingField_6)); }
	inline String_t* get_U3CResultsEncodedU3Ek__BackingField_6() const { return ___U3CResultsEncodedU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CResultsEncodedU3Ek__BackingField_6() { return &___U3CResultsEncodedU3Ek__BackingField_6; }
	inline void set_U3CResultsEncodedU3Ek__BackingField_6(String_t* value)
	{
		___U3CResultsEncodedU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResultsEncodedU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CActionLogU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RunCloudScriptResult_t3227572354, ___U3CActionLogU3Ek__BackingField_7)); }
	inline String_t* get_U3CActionLogU3Ek__BackingField_7() const { return ___U3CActionLogU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CActionLogU3Ek__BackingField_7() { return &___U3CActionLogU3Ek__BackingField_7; }
	inline void set_U3CActionLogU3Ek__BackingField_7(String_t* value)
	{
		___U3CActionLogU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CActionLogU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CExecutionTimeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RunCloudScriptResult_t3227572354, ___U3CExecutionTimeU3Ek__BackingField_8)); }
	inline double get_U3CExecutionTimeU3Ek__BackingField_8() const { return ___U3CExecutionTimeU3Ek__BackingField_8; }
	inline double* get_address_of_U3CExecutionTimeU3Ek__BackingField_8() { return &___U3CExecutionTimeU3Ek__BackingField_8; }
	inline void set_U3CExecutionTimeU3Ek__BackingField_8(double value)
	{
		___U3CExecutionTimeU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
