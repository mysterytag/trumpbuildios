﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest
struct GetPlayFabIDsFromKongregateIDsRequest_t4157912391;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromKongregateIDsRequest__ctor_m2508352962 (GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest::get_KongregateIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromKongregateIDsRequest_get_KongregateIDs_m1622940451 (GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsRequest::set_KongregateIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromKongregateIDsRequest_set_KongregateIDs_m627357300 (GetPlayFabIDsFromKongregateIDsRequest_t4157912391 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
