﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.WeakReference
struct WeakReference_t2193916456;
// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Double>
struct U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Double>
struct  U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983  : public Il2CppObject
{
public:
	// System.WeakReference UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2::reference
	WeakReference_t2193916456 * ___reference_0;
	// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<TSource,TProperty> UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2::<>f__ref$145
	U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 * ___U3CU3Ef__refU24145_1;

public:
	inline static int32_t get_offset_of_reference_0() { return static_cast<int32_t>(offsetof(U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983, ___reference_0)); }
	inline WeakReference_t2193916456 * get_reference_0() const { return ___reference_0; }
	inline WeakReference_t2193916456 ** get_address_of_reference_0() { return &___reference_0; }
	inline void set_reference_0(WeakReference_t2193916456 * value)
	{
		___reference_0 = value;
		Il2CppCodeGenWriteBarrier(&___reference_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24145_1() { return static_cast<int32_t>(offsetof(U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2257977983, ___U3CU3Ef__refU24145_1)); }
	inline U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 * get_U3CU3Ef__refU24145_1() const { return ___U3CU3Ef__refU24145_1; }
	inline U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 ** get_address_of_U3CU3Ef__refU24145_1() { return &___U3CU3Ef__refU24145_1; }
	inline void set_U3CU3Ef__refU24145_1(U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1160447004 * value)
	{
		___U3CU3Ef__refU24145_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24145_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
