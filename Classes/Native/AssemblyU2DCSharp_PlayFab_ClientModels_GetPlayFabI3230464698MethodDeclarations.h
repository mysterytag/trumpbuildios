﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult
struct GetPlayFabIDsFromFacebookIDsResult_t3230464698;
// System.Collections.Generic.List`1<PlayFab.ClientModels.FacebookPlayFabIdPair>
struct List_1_t2865784683;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromFacebookIDsResult__ctor_m2646282307 (GetPlayFabIDsFromFacebookIDsResult_t3230464698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.FacebookPlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult::get_Data()
extern "C"  List_1_t2865784683 * GetPlayFabIDsFromFacebookIDsResult_get_Data_m326376125 (GetPlayFabIDsFromFacebookIDsResult_t3230464698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.FacebookPlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromFacebookIDsResult_set_Data_m705063502 (GetPlayFabIDsFromFacebookIDsResult_t3230464698 * __this, List_1_t2865784683 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
