﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4105459918;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.InjectableInfo::.ctor(System.Boolean,System.String,System.String,System.Type,System.Type,System.Action`2<System.Object,System.Object>,System.Object)
extern "C"  void InjectableInfo__ctor_m1847028633 (InjectableInfo_t1147709774 * __this, bool ___optional0, String_t* ___identifier1, String_t* ___memberName2, Type_t * ___memberType3, Type_t * ___objectType4, Action_2_t4105459918 * ___setter5, Il2CppObject * ___defaultValue6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectContext Zenject.InjectableInfo::CreateInjectContext(Zenject.DiContainer,Zenject.InjectContext,System.Object,System.String)
extern "C"  InjectContext_t3456483891 * InjectableInfo_CreateInjectContext_m2694381853 (InjectableInfo_t1147709774 * __this, DiContainer_t2383114449 * ___container0, InjectContext_t3456483891 * ___currentContext1, Il2CppObject * ___targetInstance2, String_t* ___concreteIdentifier3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
