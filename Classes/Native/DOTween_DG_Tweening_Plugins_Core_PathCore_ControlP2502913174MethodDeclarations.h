﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ControlP2502913174.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void DG.Tweening.Plugins.Core.PathCore.ControlPoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void ControlPoint__ctor_m583813511 (ControlPoint_t2502913174 * __this, Vector3_t3525329789  ___a0, Vector3_t3525329789  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Plugins.Core.PathCore.ControlPoint DG.Tweening.Plugins.Core.PathCore.ControlPoint::op_Addition(DG.Tweening.Plugins.Core.PathCore.ControlPoint,UnityEngine.Vector3)
extern "C"  ControlPoint_t2502913174  ControlPoint_op_Addition_m412975311 (Il2CppObject * __this /* static, unused */, ControlPoint_t2502913174  ___cp0, Vector3_t3525329789  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ControlPoint_t2502913174;
struct ControlPoint_t2502913174_marshaled_pinvoke;

extern "C" void ControlPoint_t2502913174_marshal_pinvoke(const ControlPoint_t2502913174& unmarshaled, ControlPoint_t2502913174_marshaled_pinvoke& marshaled);
extern "C" void ControlPoint_t2502913174_marshal_pinvoke_back(const ControlPoint_t2502913174_marshaled_pinvoke& marshaled, ControlPoint_t2502913174& unmarshaled);
extern "C" void ControlPoint_t2502913174_marshal_pinvoke_cleanup(ControlPoint_t2502913174_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ControlPoint_t2502913174;
struct ControlPoint_t2502913174_marshaled_com;

extern "C" void ControlPoint_t2502913174_marshal_com(const ControlPoint_t2502913174& unmarshaled, ControlPoint_t2502913174_marshaled_com& marshaled);
extern "C" void ControlPoint_t2502913174_marshal_com_back(const ControlPoint_t2502913174_marshaled_com& marshaled, ControlPoint_t2502913174& unmarshaled);
extern "C" void ControlPoint_t2502913174_marshal_com_cleanup(ControlPoint_t2502913174_marshaled_com& marshaled);
