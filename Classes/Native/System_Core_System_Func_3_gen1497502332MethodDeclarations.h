﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen2998020836MethodDeclarations.h"

// System.Void System.Func`3<System.Double,System.Int32,<>__AnonType6`2<System.Double,System.Int32>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m3848603193(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t1497502332 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m3183142242_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Double,System.Int32,<>__AnonType6`2<System.Double,System.Int32>>::Invoke(T1,T2)
#define Func_3_Invoke_m2747271684(__this, ___arg10, ___arg21, method) ((  U3CU3E__AnonType6_2_t3631555212 * (*) (Func_3_t1497502332 *, double, int32_t, const MethodInfo*))Func_3_Invoke_m1788085675_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Double,System.Int32,<>__AnonType6`2<System.Double,System.Int32>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m952293897(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t1497502332 *, double, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m868203056_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Double,System.Int32,<>__AnonType6`2<System.Double,System.Int32>>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m1375360343(__this, ___result0, method) ((  U3CU3E__AnonType6_2_t3631555212 * (*) (Func_3_t1497502332 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m4016454800_gshared)(__this, ___result0, method)
