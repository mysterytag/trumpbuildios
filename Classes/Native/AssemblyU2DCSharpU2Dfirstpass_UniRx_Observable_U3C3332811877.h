﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.RefCountDisposable
struct RefCountDisposable_t3288429070;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<AddRef>c__AnonStorey37`1<System.Object>
struct  U3CAddRefU3Ec__AnonStorey37_1_t3332811877  : public Il2CppObject
{
public:
	// UniRx.RefCountDisposable UniRx.Observable/<AddRef>c__AnonStorey37`1::r
	RefCountDisposable_t3288429070 * ___r_0;
	// UniRx.IObservable`1<T> UniRx.Observable/<AddRef>c__AnonStorey37`1::xs
	Il2CppObject* ___xs_1;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(U3CAddRefU3Ec__AnonStorey37_1_t3332811877, ___r_0)); }
	inline RefCountDisposable_t3288429070 * get_r_0() const { return ___r_0; }
	inline RefCountDisposable_t3288429070 ** get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(RefCountDisposable_t3288429070 * value)
	{
		___r_0 = value;
		Il2CppCodeGenWriteBarrier(&___r_0, value);
	}

	inline static int32_t get_offset_of_xs_1() { return static_cast<int32_t>(offsetof(U3CAddRefU3Ec__AnonStorey37_1_t3332811877, ___xs_1)); }
	inline Il2CppObject* get_xs_1() const { return ___xs_1; }
	inline Il2CppObject** get_address_of_xs_1() { return &___xs_1; }
	inline void set_xs_1(Il2CppObject* value)
	{
		___xs_1 = value;
		Il2CppCodeGenWriteBarrier(&___xs_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
