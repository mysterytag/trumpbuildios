﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadDataCompletedEventHandler
struct UploadDataCompletedEventHandler_t1421445114;
// System.Object
struct Il2CppObject;
// System.Net.UploadDataCompletedEventArgs
struct UploadDataCompletedEventArgs_t737981633;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_UploadDataCompletedEventArgs737981633.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.UploadDataCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadDataCompletedEventHandler__ctor_m3690739526 (UploadDataCompletedEventHandler_t1421445114 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadDataCompletedEventHandler::Invoke(System.Object,System.Net.UploadDataCompletedEventArgs)
extern "C"  void UploadDataCompletedEventHandler_Invoke_m3794616473 (UploadDataCompletedEventHandler_t1421445114 * __this, Il2CppObject * ___sender0, UploadDataCompletedEventArgs_t737981633 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UploadDataCompletedEventHandler_t1421445114(Il2CppObject* delegate, Il2CppObject * ___sender0, UploadDataCompletedEventArgs_t737981633 * ___e1);
// System.IAsyncResult System.Net.UploadDataCompletedEventHandler::BeginInvoke(System.Object,System.Net.UploadDataCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UploadDataCompletedEventHandler_BeginInvoke_m2280310608 (UploadDataCompletedEventHandler_t1421445114 * __this, Il2CppObject * ___sender0, UploadDataCompletedEventArgs_t737981633 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadDataCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UploadDataCompletedEventHandler_EndInvoke_m3706338390 (UploadDataCompletedEventHandler_t1421445114 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
