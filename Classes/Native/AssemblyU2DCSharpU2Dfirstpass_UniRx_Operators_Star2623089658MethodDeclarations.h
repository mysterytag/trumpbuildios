﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.StartWithObservable`1<System.Object>
struct StartWithObservable_1_t2623089658;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Func`1<System.Object>
struct Func_1_t1979887667;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.StartWithObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void StartWithObservable_1__ctor_m3233996175_gshared (StartWithObservable_1_t2623089658 * __this, Il2CppObject* ___source0, Il2CppObject * ___value1, const MethodInfo* method);
#define StartWithObservable_1__ctor_m3233996175(__this, ___source0, ___value1, method) ((  void (*) (StartWithObservable_1_t2623089658 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartWithObservable_1__ctor_m3233996175_gshared)(__this, ___source0, ___value1, method)
// System.Void UniRx.Operators.StartWithObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`1<T>)
extern "C"  void StartWithObservable_1__ctor_m2721135357_gshared (StartWithObservable_1_t2623089658 * __this, Il2CppObject* ___source0, Func_1_t1979887667 * ___valueFactory1, const MethodInfo* method);
#define StartWithObservable_1__ctor_m2721135357(__this, ___source0, ___valueFactory1, method) ((  void (*) (StartWithObservable_1_t2623089658 *, Il2CppObject*, Func_1_t1979887667 *, const MethodInfo*))StartWithObservable_1__ctor_m2721135357_gshared)(__this, ___source0, ___valueFactory1, method)
// System.IDisposable UniRx.Operators.StartWithObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * StartWithObservable_1_SubscribeCore_m3653749958_gshared (StartWithObservable_1_t2623089658 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define StartWithObservable_1_SubscribeCore_m3653749958(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (StartWithObservable_1_t2623089658 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartWithObservable_1_SubscribeCore_m3653749958_gshared)(__this, ___observer0, ___cancel1, method)
