﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<ConsumePSNEntitlements>c__AnonStoreyC1
struct U3CConsumePSNEntitlementsU3Ec__AnonStoreyC1_t469932328;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<ConsumePSNEntitlements>c__AnonStoreyC1::.ctor()
extern "C"  void U3CConsumePSNEntitlementsU3Ec__AnonStoreyC1__ctor_m1886804875 (U3CConsumePSNEntitlementsU3Ec__AnonStoreyC1_t469932328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<ConsumePSNEntitlements>c__AnonStoreyC1::<>m__AE(PlayFab.CallRequestContainer)
extern "C"  void U3CConsumePSNEntitlementsU3Ec__AnonStoreyC1_U3CU3Em__AE_m599987789 (U3CConsumePSNEntitlementsU3Ec__AnonStoreyC1_t469932328 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
