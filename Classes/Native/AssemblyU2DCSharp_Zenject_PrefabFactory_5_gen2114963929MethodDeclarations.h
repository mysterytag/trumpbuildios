﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct PrefabFactory_5_t2114963929;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"

// System.Void Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void PrefabFactory_5__ctor_m300534179_gshared (PrefabFactory_5_t2114963929 * __this, const MethodInfo* method);
#define PrefabFactory_5__ctor_m300534179(__this, method) ((  void (*) (PrefabFactory_5_t2114963929 *, const MethodInfo*))PrefabFactory_5__ctor_m300534179_gshared)(__this, method)
// T Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2,P3,P4)
extern "C"  Il2CppObject * PrefabFactory_5_Create_m2507381298_gshared (PrefabFactory_5_t2114963929 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject * ___param1, Il2CppObject * ___param22, Il2CppObject * ___param33, Il2CppObject * ___param44, const MethodInfo* method);
#define PrefabFactory_5_Create_m2507381298(__this, ___prefab0, ___param1, ___param22, ___param33, ___param44, method) ((  Il2CppObject * (*) (PrefabFactory_5_t2114963929 *, GameObject_t4012695102 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))PrefabFactory_5_Create_m2507381298_gshared)(__this, ___prefab0, ___param1, ___param22, ___param33, ___param44, method)
// T Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(System.String,P1,P2,P3,P4)
extern "C"  Il2CppObject * PrefabFactory_5_Create_m209914308_gshared (PrefabFactory_5_t2114963929 * __this, String_t* ___prefabResourceName0, Il2CppObject * ___param1, Il2CppObject * ___param22, Il2CppObject * ___param33, Il2CppObject * ___param44, const MethodInfo* method);
#define PrefabFactory_5_Create_m209914308(__this, ___prefabResourceName0, ___param1, ___param22, ___param33, ___param44, method) ((  Il2CppObject * (*) (PrefabFactory_5_t2114963929 *, String_t*, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))PrefabFactory_5_Create_m209914308_gshared)(__this, ___prefabResourceName0, ___param1, ___param22, ___param33, ___param44, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* PrefabFactory_5_Validate_m2773008758_gshared (PrefabFactory_5_t2114963929 * __this, const MethodInfo* method);
#define PrefabFactory_5_Validate_m2773008758(__this, method) ((  Il2CppObject* (*) (PrefabFactory_5_t2114963929 *, const MethodInfo*))PrefabFactory_5_Validate_m2773008758_gshared)(__this, method)
