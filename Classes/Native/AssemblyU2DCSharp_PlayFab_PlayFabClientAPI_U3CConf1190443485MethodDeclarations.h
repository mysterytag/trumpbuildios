﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<ConfirmPurchase>c__AnonStoreyA0
struct U3CConfirmPurchaseU3Ec__AnonStoreyA0_t1190443485;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<ConfirmPurchase>c__AnonStoreyA0::.ctor()
extern "C"  void U3CConfirmPurchaseU3Ec__AnonStoreyA0__ctor_m2941496854 (U3CConfirmPurchaseU3Ec__AnonStoreyA0_t1190443485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<ConfirmPurchase>c__AnonStoreyA0::<>m__8D(PlayFab.CallRequestContainer)
extern "C"  void U3CConfirmPurchaseU3Ec__AnonStoreyA0_U3CU3Em__8D_m3452869760 (U3CConfirmPurchaseU3Ec__AnonStoreyA0_t1190443485 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
