﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastVerticalLayoutWithContentSizeFitter
struct  FastVerticalLayoutWithContentSizeFitter_t573856392  : public MonoBehaviour_t3012272455
{
public:
	// System.Single FastVerticalLayoutWithContentSizeFitter::Height
	float ___Height_2;
	// System.Boolean FastVerticalLayoutWithContentSizeFitter::_needRebuild
	bool ____needRebuild_3;

public:
	inline static int32_t get_offset_of_Height_2() { return static_cast<int32_t>(offsetof(FastVerticalLayoutWithContentSizeFitter_t573856392, ___Height_2)); }
	inline float get_Height_2() const { return ___Height_2; }
	inline float* get_address_of_Height_2() { return &___Height_2; }
	inline void set_Height_2(float value)
	{
		___Height_2 = value;
	}

	inline static int32_t get_offset_of__needRebuild_3() { return static_cast<int32_t>(offsetof(FastVerticalLayoutWithContentSizeFitter_t573856392, ____needRebuild_3)); }
	inline bool get__needRebuild_3() const { return ____needRebuild_3; }
	inline bool* get_address_of__needRebuild_3() { return &____needRebuild_3; }
	inline void set__needRebuild_3(bool value)
	{
		____needRebuild_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
