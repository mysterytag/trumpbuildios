﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ThrottleFirstObservable`1<System.Object>
struct ThrottleFirstObservable_1_t2123238056;
// System.Object
struct Il2CppObject;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst<System.Object>
struct  ThrottleFirst_t4236367119  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.ThrottleFirstObservable`1<T> UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst::parent
	ThrottleFirstObservable_1_t2123238056 * ___parent_2;
	// System.Object UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst::gate
	Il2CppObject * ___gate_3;
	// System.Boolean UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst::open
	bool ___open_4;
	// UniRx.SerialDisposable UniRx.Operators.ThrottleFirstObservable`1/ThrottleFirst::cancelable
	SerialDisposable_t2547852742 * ___cancelable_5;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ThrottleFirst_t4236367119, ___parent_2)); }
	inline ThrottleFirstObservable_1_t2123238056 * get_parent_2() const { return ___parent_2; }
	inline ThrottleFirstObservable_1_t2123238056 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ThrottleFirstObservable_1_t2123238056 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(ThrottleFirst_t4236367119, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_open_4() { return static_cast<int32_t>(offsetof(ThrottleFirst_t4236367119, ___open_4)); }
	inline bool get_open_4() const { return ___open_4; }
	inline bool* get_address_of_open_4() { return &___open_4; }
	inline void set_open_4(bool value)
	{
		___open_4 = value;
	}

	inline static int32_t get_offset_of_cancelable_5() { return static_cast<int32_t>(offsetof(ThrottleFirst_t4236367119, ___cancelable_5)); }
	inline SerialDisposable_t2547852742 * get_cancelable_5() const { return ___cancelable_5; }
	inline SerialDisposable_t2547852742 ** get_address_of_cancelable_5() { return &___cancelable_5; }
	inline void set_cancelable_5(SerialDisposable_t2547852742 * value)
	{
		___cancelable_5 = value;
		Il2CppCodeGenWriteBarrier(&___cancelable_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
