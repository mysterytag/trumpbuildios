﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RefCountObservable`1/RefCount/<Run>c__AnonStorey65<System.Object>
struct U3CRunU3Ec__AnonStorey65_t1610859672;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.RefCountObservable`1/RefCount/<Run>c__AnonStorey65<System.Object>::.ctor()
extern "C"  void U3CRunU3Ec__AnonStorey65__ctor_m1735275837_gshared (U3CRunU3Ec__AnonStorey65_t1610859672 * __this, const MethodInfo* method);
#define U3CRunU3Ec__AnonStorey65__ctor_m1735275837(__this, method) ((  void (*) (U3CRunU3Ec__AnonStorey65_t1610859672 *, const MethodInfo*))U3CRunU3Ec__AnonStorey65__ctor_m1735275837_gshared)(__this, method)
// System.Void UniRx.Operators.RefCountObservable`1/RefCount/<Run>c__AnonStorey65<System.Object>::<>m__82()
extern "C"  void U3CRunU3Ec__AnonStorey65_U3CU3Em__82_m3461018944_gshared (U3CRunU3Ec__AnonStorey65_t1610859672 * __this, const MethodInfo* method);
#define U3CRunU3Ec__AnonStorey65_U3CU3Em__82_m3461018944(__this, method) ((  void (*) (U3CRunU3Ec__AnonStorey65_t1610859672 *, const MethodInfo*))U3CRunU3Ec__AnonStorey65_U3CU3Em__82_m3461018944_gshared)(__this, method)
