﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"

// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.BindingValidator::ValidateContract(Zenject.DiContainer,Zenject.InjectContext)
extern "C"  Il2CppObject* BindingValidator_ValidateContract_m159488582 (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, InjectContext_t3456483891 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.BindingValidator::ValidateObjectGraph(Zenject.DiContainer,System.Type,Zenject.InjectContext,System.String,System.Type[])
extern "C"  Il2CppObject* BindingValidator_ValidateObjectGraph_m65278683 (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Type_t * ___concreteType1, InjectContext_t3456483891 * ___currentContext2, String_t* ___concreteIdentifier3, TypeU5BU5D_t3431720054* ___extras4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingValidator::TryTakingFromExtras(System.Type,System.Collections.Generic.List`1<System.Type>)
extern "C"  bool BindingValidator_TryTakingFromExtras_m2644641519 (Il2CppObject * __this /* static, unused */, Type_t * ___contractType0, List_1_t3576188904 * ___extrasList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
