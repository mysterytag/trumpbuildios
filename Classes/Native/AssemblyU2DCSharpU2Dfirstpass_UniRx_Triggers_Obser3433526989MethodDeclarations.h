﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableUpdateTrigger
struct ObservableUpdateTrigger_t3433526989;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableUpdateTrigger::.ctor()
extern "C"  void ObservableUpdateTrigger__ctor_m1672677054 (ObservableUpdateTrigger_t3433526989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableUpdateTrigger::Update()
extern "C"  void ObservableUpdateTrigger_Update_m2040243215 (ObservableUpdateTrigger_t3433526989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableUpdateTrigger::UpdateAsObservable()
extern "C"  Il2CppObject* ObservableUpdateTrigger_UpdateAsObservable_m3223523532 (ObservableUpdateTrigger_t3433526989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableUpdateTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableUpdateTrigger_RaiseOnCompletedOnDestroy_m2576290551 (ObservableUpdateTrigger_t3433526989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
