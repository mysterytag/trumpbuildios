﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<Post>c__AnonStorey82
struct  U3CPostU3Ec__AnonStorey82_t213725631  : public Il2CppObject
{
public:
	// System.String UniRx.ObservableWWW/<Post>c__AnonStorey82::url
	String_t* ___url_0;
	// System.Byte[] UniRx.ObservableWWW/<Post>c__AnonStorey82::postData
	ByteU5BU5D_t58506160* ___postData_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<Post>c__AnonStorey82::headers
	Dictionary_2_t2606186806 * ___headers_2;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<Post>c__AnonStorey82::progress
	Il2CppObject* ___progress_3;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey82_t213725631, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_postData_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey82_t213725631, ___postData_1)); }
	inline ByteU5BU5D_t58506160* get_postData_1() const { return ___postData_1; }
	inline ByteU5BU5D_t58506160** get_address_of_postData_1() { return &___postData_1; }
	inline void set_postData_1(ByteU5BU5D_t58506160* value)
	{
		___postData_1 = value;
		Il2CppCodeGenWriteBarrier(&___postData_1, value);
	}

	inline static int32_t get_offset_of_headers_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey82_t213725631, ___headers_2)); }
	inline Dictionary_2_t2606186806 * get_headers_2() const { return ___headers_2; }
	inline Dictionary_2_t2606186806 ** get_address_of_headers_2() { return &___headers_2; }
	inline void set_headers_2(Dictionary_2_t2606186806 * value)
	{
		___headers_2 = value;
		Il2CppCodeGenWriteBarrier(&___headers_2, value);
	}

	inline static int32_t get_offset_of_progress_3() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey82_t213725631, ___progress_3)); }
	inline Il2CppObject* get_progress_3() const { return ___progress_3; }
	inline Il2CppObject** get_address_of_progress_3() { return &___progress_3; }
	inline void set_progress_3(Il2CppObject* value)
	{
		___progress_3 = value;
		Il2CppCodeGenWriteBarrier(&___progress_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
