﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReplaySubject`1<System.Object>
struct ReplaySubject_1_t1910820897;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor()
extern "C"  void ReplaySubject_1__ctor_m2541553777_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method);
#define ReplaySubject_1__ctor_m2541553777(__this, method) ((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))ReplaySubject_1__ctor_m2541553777_gshared)(__this, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(UniRx.IScheduler)
extern "C"  void ReplaySubject_1__ctor_m3619462535_gshared (ReplaySubject_1_t1910820897 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method);
#define ReplaySubject_1__ctor_m3619462535(__this, ___scheduler0, method) ((  void (*) (ReplaySubject_1_t1910820897 *, Il2CppObject *, const MethodInfo*))ReplaySubject_1__ctor_m3619462535_gshared)(__this, ___scheduler0, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.Int32)
extern "C"  void ReplaySubject_1__ctor_m3130301506_gshared (ReplaySubject_1_t1910820897 * __this, int32_t ___bufferSize0, const MethodInfo* method);
#define ReplaySubject_1__ctor_m3130301506(__this, ___bufferSize0, method) ((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, const MethodInfo*))ReplaySubject_1__ctor_m3130301506_gshared)(__this, ___bufferSize0, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.Int32,UniRx.IScheduler)
extern "C"  void ReplaySubject_1__ctor_m4237425562_gshared (ReplaySubject_1_t1910820897 * __this, int32_t ___bufferSize0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ReplaySubject_1__ctor_m4237425562(__this, ___bufferSize0, ___scheduler1, method) ((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, Il2CppObject *, const MethodInfo*))ReplaySubject_1__ctor_m4237425562_gshared)(__this, ___bufferSize0, ___scheduler1, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.TimeSpan)
extern "C"  void ReplaySubject_1__ctor_m3497541899_gshared (ReplaySubject_1_t1910820897 * __this, TimeSpan_t763862892  ___window0, const MethodInfo* method);
#define ReplaySubject_1__ctor_m3497541899(__this, ___window0, method) ((  void (*) (ReplaySubject_1_t1910820897 *, TimeSpan_t763862892 , const MethodInfo*))ReplaySubject_1__ctor_m3497541899_gshared)(__this, ___window0, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.TimeSpan,UniRx.IScheduler)
extern "C"  void ReplaySubject_1__ctor_m3564462577_gshared (ReplaySubject_1_t1910820897 * __this, TimeSpan_t763862892  ___window0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ReplaySubject_1__ctor_m3564462577(__this, ___window0, ___scheduler1, method) ((  void (*) (ReplaySubject_1_t1910820897 *, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))ReplaySubject_1__ctor_m3564462577_gshared)(__this, ___window0, ___scheduler1, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.Int32,System.TimeSpan,UniRx.IScheduler)
extern "C"  void ReplaySubject_1__ctor_m191511044_gshared (ReplaySubject_1_t1910820897 * __this, int32_t ___bufferSize0, TimeSpan_t763862892  ___window1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define ReplaySubject_1__ctor_m191511044(__this, ___bufferSize0, ___window1, ___scheduler2, method) ((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))ReplaySubject_1__ctor_m191511044_gshared)(__this, ___bufferSize0, ___window1, ___scheduler2, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::Trim()
extern "C"  void ReplaySubject_1_Trim_m767772245_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method);
#define ReplaySubject_1_Trim_m767772245(__this, method) ((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))ReplaySubject_1_Trim_m767772245_gshared)(__this, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::OnCompleted()
extern "C"  void ReplaySubject_1_OnCompleted_m1109549883_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method);
#define ReplaySubject_1_OnCompleted_m1109549883(__this, method) ((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))ReplaySubject_1_OnCompleted_m1109549883_gshared)(__this, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::OnError(System.Exception)
extern "C"  void ReplaySubject_1_OnError_m3749909480_gshared (ReplaySubject_1_t1910820897 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReplaySubject_1_OnError_m3749909480(__this, ___error0, method) ((  void (*) (ReplaySubject_1_t1910820897 *, Exception_t1967233988 *, const MethodInfo*))ReplaySubject_1_OnError_m3749909480_gshared)(__this, ___error0, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::OnNext(T)
extern "C"  void ReplaySubject_1_OnNext_m1427491545_gshared (ReplaySubject_1_t1910820897 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReplaySubject_1_OnNext_m1427491545(__this, ___value0, method) ((  void (*) (ReplaySubject_1_t1910820897 *, Il2CppObject *, const MethodInfo*))ReplaySubject_1_OnNext_m1427491545_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReplaySubject`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReplaySubject_1_Subscribe_m3366756272_gshared (ReplaySubject_1_t1910820897 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReplaySubject_1_Subscribe_m3366756272(__this, ___observer0, method) ((  Il2CppObject * (*) (ReplaySubject_1_t1910820897 *, Il2CppObject*, const MethodInfo*))ReplaySubject_1_Subscribe_m3366756272_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::Dispose()
extern "C"  void ReplaySubject_1_Dispose_m2790038638_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method);
#define ReplaySubject_1_Dispose_m2790038638(__this, method) ((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))ReplaySubject_1_Dispose_m2790038638_gshared)(__this, method)
// System.Void UniRx.ReplaySubject`1<System.Object>::ThrowIfDisposed()
extern "C"  void ReplaySubject_1_ThrowIfDisposed_m309721079_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method);
#define ReplaySubject_1_ThrowIfDisposed_m309721079(__this, method) ((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))ReplaySubject_1_ThrowIfDisposed_m309721079_gshared)(__this, method)
// System.Boolean UniRx.ReplaySubject`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReplaySubject_1_IsRequiredSubscribeOnCurrentThread_m3711271858_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method);
#define ReplaySubject_1_IsRequiredSubscribeOnCurrentThread_m3711271858(__this, method) ((  bool (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))ReplaySubject_1_IsRequiredSubscribeOnCurrentThread_m3711271858_gshared)(__this, method)
