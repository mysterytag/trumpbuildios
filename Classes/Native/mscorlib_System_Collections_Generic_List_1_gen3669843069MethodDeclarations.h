﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::.ctor()
#define List_1__ctor_m134031402(__this, method) ((  void (*) (List_1_t3669843069 *, const MethodInfo*))List_1__ctor_m348833073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1902710293(__this, ___collection0, method) ((  void (*) (List_1_t3669843069 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::.ctor(System.Int32)
#define List_1__ctor_m2777542267(__this, ___capacity0, method) ((  void (*) (List_1_t3669843069 *, int32_t, const MethodInfo*))List_1__ctor_m2892763214_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::.cctor()
#define List_1__cctor_m3672877251(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1245702324(__this, method) ((  Il2CppObject* (*) (List_1_t3669843069 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2826092634(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3669843069 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3367511401(__this, method) ((  Il2CppObject * (*) (List_1_t3669843069 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1299221236(__this, ___item0, method) ((  int32_t (*) (List_1_t3669843069 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1578122060(__this, ___item0, method) ((  bool (*) (List_1_t3669843069 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1791633228(__this, ___item0, method) ((  int32_t (*) (List_1_t3669843069 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3033527231(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3669843069 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m789023497(__this, ___item0, method) ((  void (*) (List_1_t3669843069 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2463333453(__this, method) ((  bool (*) (List_1_t3669843069 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1117243536(__this, method) ((  bool (*) (List_1_t3669843069 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2017695170(__this, method) ((  Il2CppObject * (*) (List_1_t3669843069 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3465106363(__this, method) ((  bool (*) (List_1_t3669843069 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m704333790(__this, method) ((  bool (*) (List_1_t3669843069 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3040419529(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3669843069 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3845794774(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3669843069 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Add(T)
#define List_1_Add_m3458149141(__this, ___item0, method) ((  void (*) (List_1_t3669843069 *, StoreItem_t2872884100 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3994746064(__this, ___newCount0, method) ((  void (*) (List_1_t3669843069 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3479442382(__this, ___collection0, method) ((  void (*) (List_1_t3669843069 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2740414606(__this, ___enumerable0, method) ((  void (*) (List_1_t3669843069 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2714887497(__this, ___collection0, method) ((  void (*) (List_1_t3669843069 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::AsReadOnly()
#define List_1_AsReadOnly_m3314343196(__this, method) ((  ReadOnlyCollection_1_t1741062152 * (*) (List_1_t3669843069 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Clear()
#define List_1_Clear_m1835131989(__this, method) ((  void (*) (List_1_t3669843069 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Contains(T)
#define List_1_Contains_m612790855(__this, ___item0, method) ((  bool (*) (List_1_t3669843069 *, StoreItem_t2872884100 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1860851653(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3669843069 *, StoreItemU5BU5D_t2055598573*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Find(System.Predicate`1<T>)
#define List_1_Find_m442039521(__this, ___match0, method) ((  StoreItem_t2872884100 * (*) (List_1_t3669843069 *, Predicate_1_t3443847998 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m411940030(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3443847998 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3753765275(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3669843069 *, int32_t, int32_t, Predicate_1_t3443847998 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m2361816754(__this, ___action0, method) ((  void (*) (List_1_t3669843069 *, Action_1_t3021336805 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::GetEnumerator()
#define List_1_GetEnumerator_m1282488324(__this, method) ((  Enumerator_t1755626061  (*) (List_1_t3669843069 *, const MethodInfo*))List_1_GetEnumerator_m1820263692_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::IndexOf(T)
#define List_1_IndexOf_m3370670417(__this, ___item0, method) ((  int32_t (*) (List_1_t3669843069 *, StoreItem_t2872884100 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2879691036(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3669843069 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1607218837(__this, ___index0, method) ((  void (*) (List_1_t3669843069 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Insert(System.Int32,T)
#define List_1_Insert_m341154684(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3669843069 *, int32_t, StoreItem_t2872884100 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2786241265(__this, ___collection0, method) ((  void (*) (List_1_t3669843069 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Remove(T)
#define List_1_Remove_m460396546(__this, ___item0, method) ((  bool (*) (List_1_t3669843069 *, StoreItem_t2872884100 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2684083796(__this, ___match0, method) ((  int32_t (*) (List_1_t3669843069 *, Predicate_1_t3443847998 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2509974850(__this, ___index0, method) ((  void (*) (List_1_t3669843069 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Reverse()
#define List_1_Reverse_m4287937002(__this, method) ((  void (*) (List_1_t3669843069 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Sort()
#define List_1_Sort_m3984121336(__this, method) ((  void (*) (List_1_t3669843069 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2987687660(__this, ___comparer0, method) ((  void (*) (List_1_t3669843069 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1582800971(__this, ___comparison0, method) ((  void (*) (List_1_t3669843069 *, Comparison_1_t1281591680 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::ToArray()
#define List_1_ToArray_m1247474883(__this, method) ((  StoreItemU5BU5D_t2055598573* (*) (List_1_t3669843069 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::TrimExcess()
#define List_1_TrimExcess_m3688342417(__this, method) ((  void (*) (List_1_t3669843069 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::get_Capacity()
#define List_1_get_Capacity_m3752411393(__this, method) ((  int32_t (*) (List_1_t3669843069 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1204934882(__this, ___value0, method) ((  void (*) (List_1_t3669843069 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::get_Count()
#define List_1_get_Count_m1228875338(__this, method) ((  int32_t (*) (List_1_t3669843069 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::get_Item(System.Int32)
#define List_1_get_Item_m2789029160(__this, ___index0, method) ((  StoreItem_t2872884100 * (*) (List_1_t3669843069 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.StoreItem>::set_Item(System.Int32,T)
#define List_1_set_Item_m3241733459(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3669843069 *, int32_t, StoreItem_t2872884100 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
