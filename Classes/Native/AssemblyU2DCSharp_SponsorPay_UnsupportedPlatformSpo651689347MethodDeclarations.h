﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.UnsupportedPlatformSponsorPayPlugin
struct UnsupportedPlatformSponsorPayPlugin_t651689347;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"

// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::.ctor()
extern "C"  void UnsupportedPlatformSponsorPayPlugin__ctor_m3448039314 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.UnsupportedPlatformSponsorPayPlugin::StartSDK(System.String,System.String,System.String)
extern "C"  String_t* UnsupportedPlatformSponsorPayPlugin_StartSDK_m2762071117 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, String_t* ___appId0, String_t* ___userId1, String_t* ___securityToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::ReportActionCompletion(System.String,System.String)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_ReportActionCompletion_m3984809222 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, String_t* ___credentialsToken0, String_t* ___actionId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::LaunchOfferWall(System.String,System.String,System.String)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_LaunchOfferWall_m1686651351 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, String_t* ___placementId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::RequestNewCoins(System.String,System.String,System.String)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_RequestNewCoins_m3096706297 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, String_t* ___currencyId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::ShowVCSNotifications(System.Boolean)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_ShowVCSNotifications_m2749574568 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, bool ___showNotification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::RequestBrandEngageOffers(System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_RequestBrandEngageOffers_m3234879769 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, bool ___queryVCS2, String_t* ___currencyId3, String_t* ___placementId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::StartBrandEngage()
extern "C"  void UnsupportedPlatformSponsorPayPlugin_StartBrandEngage_m120472696 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::ShowBrandEngageRewardNotification(System.Boolean)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_ShowBrandEngageRewardNotification_m375255884 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, bool ___showNotification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::RequestInterstitialAds(System.String,System.String)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_RequestInterstitialAds_m1080207383 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, String_t* ___credentialsToken0, String_t* ___placementId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::ShowInterstitialAd()
extern "C"  void UnsupportedPlatformSponsorPayPlugin_ShowInterstitialAd_m1612802942 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::VideoDownloadPause(System.Boolean)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_VideoDownloadPause_m4154933052 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::EnableLogging(System.Boolean)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_EnableLogging_m1964673923 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, bool ___shouldEnableLogging0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::SetLogLevel(SponsorPay.SPLogLevel)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_SetLogLevel_m1384731331 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::AddParameters(System.String)
extern "C"  void UnsupportedPlatformSponsorPayPlugin_AddParameters_m3651160711 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.UnsupportedPlatformSponsorPayPlugin::RemoveAllParameters()
extern "C"  void UnsupportedPlatformSponsorPayPlugin_RemoveAllParameters_m229779703 (UnsupportedPlatformSponsorPayPlugin_t651689347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
