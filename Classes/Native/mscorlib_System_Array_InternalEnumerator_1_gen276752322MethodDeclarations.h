﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen276752322.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void System.Array/InternalEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3183451641_gshared (InternalEnumerator_1_t276752322 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3183451641(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t276752322 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3183451641_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3440442119_gshared (InternalEnumerator_1_t276752322 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3440442119(__this, method) ((  void (*) (InternalEnumerator_1_t276752322 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3440442119_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1030342195_gshared (InternalEnumerator_1_t276752322 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1030342195(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t276752322 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1030342195_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m118659856_gshared (InternalEnumerator_1_t276752322 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m118659856(__this, method) ((  void (*) (InternalEnumerator_1_t276752322 *, const MethodInfo*))InternalEnumerator_1_Dispose_m118659856_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m196220915_gshared (InternalEnumerator_1_t276752322 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m196220915(__this, method) ((  bool (*) (InternalEnumerator_1_t276752322 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m196220915_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Current()
extern "C"  Tuple_2_t369261819  InternalEnumerator_1_get_Current_m1325172864_gshared (InternalEnumerator_1_t276752322 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1325172864(__this, method) ((  Tuple_2_t369261819  (*) (InternalEnumerator_1_t276752322 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1325172864_gshared)(__this, method)
