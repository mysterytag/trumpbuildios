﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.CollectionAddEvent`1<System.Object>::.ctor(System.Int32,T)
extern "C"  void CollectionAddEvent_1__ctor_m4121910756_gshared (CollectionAddEvent_1_t2416521987 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define CollectionAddEvent_1__ctor_m4121910756(__this, ___index0, ___value1, method) ((  void (*) (CollectionAddEvent_1_t2416521987 *, int32_t, Il2CppObject *, const MethodInfo*))CollectionAddEvent_1__ctor_m4121910756_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionAddEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionAddEvent_1_get_Index_m1649808760_gshared (CollectionAddEvent_1_t2416521987 * __this, const MethodInfo* method);
#define CollectionAddEvent_1_get_Index_m1649808760(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))CollectionAddEvent_1_get_Index_m1649808760_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionAddEvent_1_set_Index_m4001500511_gshared (CollectionAddEvent_1_t2416521987 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionAddEvent_1_set_Index_m4001500511(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t2416521987 *, int32_t, const MethodInfo*))CollectionAddEvent_1_set_Index_m4001500511_gshared)(__this, ___value0, method)
// T UniRx.CollectionAddEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionAddEvent_1_get_Value_m1882365472_gshared (CollectionAddEvent_1_t2416521987 * __this, const MethodInfo* method);
#define CollectionAddEvent_1_get_Value_m1882365472(__this, method) ((  Il2CppObject * (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))CollectionAddEvent_1_get_Value_m1882365472_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionAddEvent_1_set_Value_m1389365521_gshared (CollectionAddEvent_1_t2416521987 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionAddEvent_1_set_Value_m1389365521(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t2416521987 *, Il2CppObject *, const MethodInfo*))CollectionAddEvent_1_set_Value_m1389365521_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionAddEvent`1<System.Object>::ToString()
extern "C"  String_t* CollectionAddEvent_1_ToString_m3034630034_gshared (CollectionAddEvent_1_t2416521987 * __this, const MethodInfo* method);
#define CollectionAddEvent_1_ToString_m3034630034(__this, method) ((  String_t* (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))CollectionAddEvent_1_ToString_m3034630034_gshared)(__this, method)
// System.Int32 UniRx.CollectionAddEvent`1<System.Object>::GetHashCode()
extern "C"  int32_t CollectionAddEvent_1_GetHashCode_m3912132320_gshared (CollectionAddEvent_1_t2416521987 * __this, const MethodInfo* method);
#define CollectionAddEvent_1_GetHashCode_m3912132320(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))CollectionAddEvent_1_GetHashCode_m3912132320_gshared)(__this, method)
// System.Boolean UniRx.CollectionAddEvent`1<System.Object>::Equals(UniRx.CollectionAddEvent`1<T>)
extern "C"  bool CollectionAddEvent_1_Equals_m2095755040_gshared (CollectionAddEvent_1_t2416521987 * __this, CollectionAddEvent_1_t2416521987  ___other0, const MethodInfo* method);
#define CollectionAddEvent_1_Equals_m2095755040(__this, ___other0, method) ((  bool (*) (CollectionAddEvent_1_t2416521987 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))CollectionAddEvent_1_Equals_m2095755040_gshared)(__this, ___other0, method)
