﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopMult
struct PopMult_t1269530369;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void PopMult::.ctor()
extern "C"  void PopMult__ctor_m3125150090 (PopMult_t1269530369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopMult::Init(System.Single)
extern "C"  void PopMult_Init_m596631137 (PopMult_t1269530369 * __this, float ___scale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PopMult::ShowCoroutine()
extern "C"  Il2CppObject * PopMult_ShowCoroutine_m3554172811 (PopMult_t1269530369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
