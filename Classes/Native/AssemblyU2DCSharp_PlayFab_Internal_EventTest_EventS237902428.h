﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.PlayFabSettings/RequestCallback`1<System.Object>
struct RequestCallback_1_t923153878;
// PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,PlayFab.Internal.PlayFabResultCommon>
struct ResponseCallback_2_t2783168302;
// PlayFab.PlayFabClientAPI/LoginWithCustomIDRequestCallback
struct LoginWithCustomIDRequestCallback_t1450663257;
// PlayFab.PlayFabClientAPI/LoginWithCustomIDResponseCallback
struct LoginWithCustomIDResponseCallback_t1126993633;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.EventTest/EventStaticListener
struct  EventStaticListener_t237902428  : public Il2CppObject
{
public:

public:
};

struct EventStaticListener_t237902428_StaticFields
{
public:
	// PlayFab.PlayFabSettings/RequestCallback`1<System.Object> PlayFab.Internal.EventTest/EventStaticListener::_onRequestGl
	RequestCallback_1_t923153878 * ____onRequestGl_0;
	// PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,PlayFab.Internal.PlayFabResultCommon> PlayFab.Internal.EventTest/EventStaticListener::_onResponseGl
	ResponseCallback_2_t2783168302 * ____onResponseGl_1;
	// PlayFab.PlayFabClientAPI/LoginWithCustomIDRequestCallback PlayFab.Internal.EventTest/EventStaticListener::_onRequestLogin
	LoginWithCustomIDRequestCallback_t1450663257 * ____onRequestLogin_2;
	// PlayFab.PlayFabClientAPI/LoginWithCustomIDResponseCallback PlayFab.Internal.EventTest/EventStaticListener::_onResponseLogin
	LoginWithCustomIDResponseCallback_t1126993633 * ____onResponseLogin_3;

public:
	inline static int32_t get_offset_of__onRequestGl_0() { return static_cast<int32_t>(offsetof(EventStaticListener_t237902428_StaticFields, ____onRequestGl_0)); }
	inline RequestCallback_1_t923153878 * get__onRequestGl_0() const { return ____onRequestGl_0; }
	inline RequestCallback_1_t923153878 ** get_address_of__onRequestGl_0() { return &____onRequestGl_0; }
	inline void set__onRequestGl_0(RequestCallback_1_t923153878 * value)
	{
		____onRequestGl_0 = value;
		Il2CppCodeGenWriteBarrier(&____onRequestGl_0, value);
	}

	inline static int32_t get_offset_of__onResponseGl_1() { return static_cast<int32_t>(offsetof(EventStaticListener_t237902428_StaticFields, ____onResponseGl_1)); }
	inline ResponseCallback_2_t2783168302 * get__onResponseGl_1() const { return ____onResponseGl_1; }
	inline ResponseCallback_2_t2783168302 ** get_address_of__onResponseGl_1() { return &____onResponseGl_1; }
	inline void set__onResponseGl_1(ResponseCallback_2_t2783168302 * value)
	{
		____onResponseGl_1 = value;
		Il2CppCodeGenWriteBarrier(&____onResponseGl_1, value);
	}

	inline static int32_t get_offset_of__onRequestLogin_2() { return static_cast<int32_t>(offsetof(EventStaticListener_t237902428_StaticFields, ____onRequestLogin_2)); }
	inline LoginWithCustomIDRequestCallback_t1450663257 * get__onRequestLogin_2() const { return ____onRequestLogin_2; }
	inline LoginWithCustomIDRequestCallback_t1450663257 ** get_address_of__onRequestLogin_2() { return &____onRequestLogin_2; }
	inline void set__onRequestLogin_2(LoginWithCustomIDRequestCallback_t1450663257 * value)
	{
		____onRequestLogin_2 = value;
		Il2CppCodeGenWriteBarrier(&____onRequestLogin_2, value);
	}

	inline static int32_t get_offset_of__onResponseLogin_3() { return static_cast<int32_t>(offsetof(EventStaticListener_t237902428_StaticFields, ____onResponseLogin_3)); }
	inline LoginWithCustomIDResponseCallback_t1126993633 * get__onResponseLogin_3() const { return ____onResponseLogin_3; }
	inline LoginWithCustomIDResponseCallback_t1126993633 ** get_address_of__onResponseLogin_3() { return &____onResponseLogin_3; }
	inline void set__onResponseLogin_3(LoginWithCustomIDResponseCallback_t1126993633 * value)
	{
		____onResponseLogin_3 = value;
		Il2CppCodeGenWriteBarrier(&____onResponseLogin_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
