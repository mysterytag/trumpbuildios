﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>
struct RepeatSafe_t3020782369;
// UniRx.Operators.RepeatSafeObservable`1<System.Object>
struct RepeatSafeObservable_1_t4177995194;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>::.ctor(UniRx.Operators.RepeatSafeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void RepeatSafe__ctor_m1237643148_gshared (RepeatSafe_t3020782369 * __this, RepeatSafeObservable_1_t4177995194 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define RepeatSafe__ctor_m1237643148(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (RepeatSafe_t3020782369 *, RepeatSafeObservable_1_t4177995194 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))RepeatSafe__ctor_m1237643148_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>::Run()
extern "C"  Il2CppObject * RepeatSafe_Run_m147843717_gshared (RepeatSafe_t3020782369 * __this, const MethodInfo* method);
#define RepeatSafe_Run_m147843717(__this, method) ((  Il2CppObject * (*) (RepeatSafe_t3020782369 *, const MethodInfo*))RepeatSafe_Run_m147843717_gshared)(__this, method)
// System.Void UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>::RecursiveRun(System.Action)
extern "C"  void RepeatSafe_RecursiveRun_m3530997335_gshared (RepeatSafe_t3020782369 * __this, Action_t437523947 * ___self0, const MethodInfo* method);
#define RepeatSafe_RecursiveRun_m3530997335(__this, ___self0, method) ((  void (*) (RepeatSafe_t3020782369 *, Action_t437523947 *, const MethodInfo*))RepeatSafe_RecursiveRun_m3530997335_gshared)(__this, ___self0, method)
// System.Void UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>::OnNext(T)
extern "C"  void RepeatSafe_OnNext_m3210926367_gshared (RepeatSafe_t3020782369 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define RepeatSafe_OnNext_m3210926367(__this, ___value0, method) ((  void (*) (RepeatSafe_t3020782369 *, Il2CppObject *, const MethodInfo*))RepeatSafe_OnNext_m3210926367_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>::OnError(System.Exception)
extern "C"  void RepeatSafe_OnError_m1751569966_gshared (RepeatSafe_t3020782369 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RepeatSafe_OnError_m1751569966(__this, ___error0, method) ((  void (*) (RepeatSafe_t3020782369 *, Exception_t1967233988 *, const MethodInfo*))RepeatSafe_OnError_m1751569966_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>::OnCompleted()
extern "C"  void RepeatSafe_OnCompleted_m2266160769_gshared (RepeatSafe_t3020782369 * __this, const MethodInfo* method);
#define RepeatSafe_OnCompleted_m2266160769(__this, method) ((  void (*) (RepeatSafe_t3020782369 *, const MethodInfo*))RepeatSafe_OnCompleted_m2266160769_gshared)(__this, method)
// System.Void UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>::<Run>m__85()
extern "C"  void RepeatSafe_U3CRunU3Em__85_m3315814152_gshared (RepeatSafe_t3020782369 * __this, const MethodInfo* method);
#define RepeatSafe_U3CRunU3Em__85_m3315814152(__this, method) ((  void (*) (RepeatSafe_t3020782369 *, const MethodInfo*))RepeatSafe_U3CRunU3Em__85_m3315814152_gshared)(__this, method)
