﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ReturnObservable`1/Return<UniRx.Unit>
struct Return_t334929451;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ReturnObservable`1/Return<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Return__ctor_m2102420093_gshared (Return_t334929451 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Return__ctor_m2102420093(__this, ___observer0, ___cancel1, method) ((  void (*) (Return_t334929451 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Return__ctor_m2102420093_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<UniRx.Unit>::OnNext(T)
extern "C"  void Return_OnNext_m983940853_gshared (Return_t334929451 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Return_OnNext_m983940853(__this, ___value0, method) ((  void (*) (Return_t334929451 *, Unit_t2558286038 , const MethodInfo*))Return_OnNext_m983940853_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Return_OnError_m2168410116_gshared (Return_t334929451 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Return_OnError_m2168410116(__this, ___error0, method) ((  void (*) (Return_t334929451 *, Exception_t1967233988 *, const MethodInfo*))Return_OnError_m2168410116_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<UniRx.Unit>::OnCompleted()
extern "C"  void Return_OnCompleted_m941812055_gshared (Return_t334929451 * __this, const MethodInfo* method);
#define Return_OnCompleted_m941812055(__this, method) ((  void (*) (Return_t334929451 *, const MethodInfo*))Return_OnCompleted_m941812055_gshared)(__this, method)
