﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetTitleDataResult
struct GetTitleDataResult_t1299334873;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetTitleDataResult::.ctor()
extern "C"  void GetTitleDataResult__ctor_m1019710212 (GetTitleDataResult_t1299334873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.GetTitleDataResult::get_Data()
extern "C"  Dictionary_2_t2606186806 * GetTitleDataResult_get_Data_m4083068869 (GetTitleDataResult_t1299334873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetTitleDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetTitleDataResult_set_Data_m1674290584 (GetTitleDataResult_t1299334873 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
