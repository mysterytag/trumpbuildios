﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<UniRx.IObserver`1<System.Object>,System.Collections.IEnumerator>
struct Func_2_t1278048720;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromCoroutine>c__AnonStorey4C`1<System.Object>
struct  U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468  : public Il2CppObject
{
public:
	// System.Func`2<UniRx.IObserver`1<T>,System.Collections.IEnumerator> UniRx.Observable/<FromCoroutine>c__AnonStorey4C`1::coroutine
	Func_2_t1278048720 * ___coroutine_0;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CFromCoroutineU3Ec__AnonStorey4C_1_t2050741468, ___coroutine_0)); }
	inline Func_2_t1278048720 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_2_t1278048720 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_2_t1278048720 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier(&___coroutine_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
