﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Graphic
struct Graphic_t933884113;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0
struct  U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263  : public Il2CppObject
{
public:
	// UnityEngine.UI.Graphic UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStoreyA0::graphic
	Graphic_t933884113 * ___graphic_0;

public:
	inline static int32_t get_offset_of_graphic_0() { return static_cast<int32_t>(offsetof(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStoreyA0_t1880697263, ___graphic_0)); }
	inline Graphic_t933884113 * get_graphic_0() const { return ___graphic_0; }
	inline Graphic_t933884113 ** get_address_of_graphic_0() { return &___graphic_0; }
	inline void set_graphic_0(Graphic_t933884113 * value)
	{
		___graphic_0 = value;
		Il2CppCodeGenWriteBarrier(&___graphic_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
