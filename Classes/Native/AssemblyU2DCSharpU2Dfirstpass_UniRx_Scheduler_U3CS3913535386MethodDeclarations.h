﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/<Schedule>c__AnonStorey75
struct U3CScheduleU3Ec__AnonStorey75_t3913535386;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey75::.ctor()
extern "C"  void U3CScheduleU3Ec__AnonStorey75__ctor_m1867195529 (U3CScheduleU3Ec__AnonStorey75_t3913535386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey75::<>m__98()
extern "C"  void U3CScheduleU3Ec__AnonStorey75_U3CU3Em__98_m1386859633 (U3CScheduleU3Ec__AnonStorey75_t3913535386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey75::<>m__9C(System.TimeSpan)
extern "C"  void U3CScheduleU3Ec__AnonStorey75_U3CU3Em__9C_m4159681632 (U3CScheduleU3Ec__AnonStorey75_t3913535386 * __this, TimeSpan_t763862892  ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
