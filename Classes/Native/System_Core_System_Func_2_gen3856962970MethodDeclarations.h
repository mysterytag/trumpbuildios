﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Object,UniRx.Unit>
struct Func_2_t3856962970;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`2<System.Object,UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m298322708_gshared (Func_2_t3856962970 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m298322708(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3856962970 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m298322708_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Object,UniRx.Unit>::Invoke(T)
extern "C"  Unit_t2558286038  Func_2_Invoke_m4026479570_gshared (Func_2_t3856962970 * __this, Il2CppObject * ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m4026479570(__this, ___arg10, method) ((  Unit_t2558286038  (*) (Func_2_t3856962970 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m4026479570_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Object,UniRx.Unit>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3097046021_gshared (Func_2_t3856962970 * __this, Il2CppObject * ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m3097046021(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3856962970 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3097046021_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Object,UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  Unit_t2558286038  Func_2_EndInvoke_m2450917186_gshared (Func_2_t3856962970 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m2450917186(__this, ___result0, method) ((  Unit_t2558286038  (*) (Func_2_t3856962970 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2450917186_gshared)(__this, ___result0, method)
