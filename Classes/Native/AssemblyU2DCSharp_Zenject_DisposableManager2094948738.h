﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.SingletonInstanceHelper
struct SingletonInstanceHelper_t833281251;
// System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>
struct List_1_t2081125191;
// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32>
struct Func_2_t1968156484;
// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.Int32>
struct Func_2_t846447813;
// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.IDisposable>
struct Func_2_t3922921696;
// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.Type>
struct Func_2_t778262961;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager
struct  DisposableManager_t2094948738  : public Il2CppObject
{
public:
	// Zenject.SingletonInstanceHelper Zenject.DisposableManager::_singletonInstanceHelper
	SingletonInstanceHelper_t833281251 * ____singletonInstanceHelper_0;
	// System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo> Zenject.DisposableManager::_disposables
	List_1_t2081125191 * ____disposables_1;
	// System.Boolean Zenject.DisposableManager::_disposed
	bool ____disposed_2;

public:
	inline static int32_t get_offset_of__singletonInstanceHelper_0() { return static_cast<int32_t>(offsetof(DisposableManager_t2094948738, ____singletonInstanceHelper_0)); }
	inline SingletonInstanceHelper_t833281251 * get__singletonInstanceHelper_0() const { return ____singletonInstanceHelper_0; }
	inline SingletonInstanceHelper_t833281251 ** get_address_of__singletonInstanceHelper_0() { return &____singletonInstanceHelper_0; }
	inline void set__singletonInstanceHelper_0(SingletonInstanceHelper_t833281251 * value)
	{
		____singletonInstanceHelper_0 = value;
		Il2CppCodeGenWriteBarrier(&____singletonInstanceHelper_0, value);
	}

	inline static int32_t get_offset_of__disposables_1() { return static_cast<int32_t>(offsetof(DisposableManager_t2094948738, ____disposables_1)); }
	inline List_1_t2081125191 * get__disposables_1() const { return ____disposables_1; }
	inline List_1_t2081125191 ** get_address_of__disposables_1() { return &____disposables_1; }
	inline void set__disposables_1(List_1_t2081125191 * value)
	{
		____disposables_1 = value;
		Il2CppCodeGenWriteBarrier(&____disposables_1, value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(DisposableManager_t2094948738, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}
};

struct DisposableManager_t2094948738_StaticFields
{
public:
	// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32> Zenject.DisposableManager::<>f__am$cache3
	Func_2_t1968156484 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.Int32> Zenject.DisposableManager::<>f__am$cache4
	Func_2_t846447813 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.IDisposable> Zenject.DisposableManager::<>f__am$cache5
	Func_2_t3922921696 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<Zenject.DisposableManager/DisposableInfo,System.Type> Zenject.DisposableManager::<>f__am$cache6
	Func_2_t778262961 * ___U3CU3Ef__amU24cache6_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(DisposableManager_t2094948738_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t1968156484 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t1968156484 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t1968156484 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(DisposableManager_t2094948738_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t846447813 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t846447813 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t846447813 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(DisposableManager_t2094948738_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t3922921696 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t3922921696 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t3922921696 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(DisposableManager_t2094948738_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t778262961 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t778262961 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t778262961 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
