﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.DeltaOfCoinsResponseReceivedHandler
struct DeltaOfCoinsResponseReceivedHandler_t2871420975;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.DeltaOfCoinsResponseReceivedHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DeltaOfCoinsResponseReceivedHandler__ctor_m115183100 (DeltaOfCoinsResponseReceivedHandler_t2871420975 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.DeltaOfCoinsResponseReceivedHandler::Invoke(System.Double,System.String)
extern "C"  void DeltaOfCoinsResponseReceivedHandler_Invoke_m2351359208 (DeltaOfCoinsResponseReceivedHandler_t2871420975 * __this, double ___deltaOfCoins0, String_t* ___transactionId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_DeltaOfCoinsResponseReceivedHandler_t2871420975(Il2CppObject* delegate, double ___deltaOfCoins0, String_t* ___transactionId1);
// System.IAsyncResult SponsorPay.DeltaOfCoinsResponseReceivedHandler::BeginInvoke(System.Double,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DeltaOfCoinsResponseReceivedHandler_BeginInvoke_m3291255989 (DeltaOfCoinsResponseReceivedHandler_t2871420975 * __this, double ___deltaOfCoins0, String_t* ___transactionId1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.DeltaOfCoinsResponseReceivedHandler::EndInvoke(System.IAsyncResult)
extern "C"  void DeltaOfCoinsResponseReceivedHandler_EndInvoke_m1597988364 (DeltaOfCoinsResponseReceivedHandler_t2871420975 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
