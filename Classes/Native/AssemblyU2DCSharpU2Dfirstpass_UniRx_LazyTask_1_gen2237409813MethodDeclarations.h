﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.LazyTask`1<System.Object>
struct LazyTask_1_t2237409813;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.LazyTask`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void LazyTask_1__ctor_m3859110140_gshared (LazyTask_1_t2237409813 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define LazyTask_1__ctor_m3859110140(__this, ___source0, method) ((  void (*) (LazyTask_1_t2237409813 *, Il2CppObject*, const MethodInfo*))LazyTask_1__ctor_m3859110140_gshared)(__this, ___source0, method)
// T UniRx.LazyTask`1<System.Object>::get_Result()
extern "C"  Il2CppObject * LazyTask_1_get_Result_m2602473934_gshared (LazyTask_1_t2237409813 * __this, const MethodInfo* method);
#define LazyTask_1_get_Result_m2602473934(__this, method) ((  Il2CppObject * (*) (LazyTask_1_t2237409813 *, const MethodInfo*))LazyTask_1_get_Result_m2602473934_gshared)(__this, method)
// System.Exception UniRx.LazyTask`1<System.Object>::get_Exception()
extern "C"  Exception_t1967233988 * LazyTask_1_get_Exception_m41140598_gshared (LazyTask_1_t2237409813 * __this, const MethodInfo* method);
#define LazyTask_1_get_Exception_m41140598(__this, method) ((  Exception_t1967233988 * (*) (LazyTask_1_t2237409813 *, const MethodInfo*))LazyTask_1_get_Exception_m41140598_gshared)(__this, method)
// System.Void UniRx.LazyTask`1<System.Object>::set_Exception(System.Exception)
extern "C"  void LazyTask_1_set_Exception_m1329432797_gshared (LazyTask_1_t2237409813 * __this, Exception_t1967233988 * ___value0, const MethodInfo* method);
#define LazyTask_1_set_Exception_m1329432797(__this, ___value0, method) ((  void (*) (LazyTask_1_t2237409813 *, Exception_t1967233988 *, const MethodInfo*))LazyTask_1_set_Exception_m1329432797_gshared)(__this, ___value0, method)
// UnityEngine.Coroutine UniRx.LazyTask`1<System.Object>::Start()
extern "C"  Coroutine_t2246592261 * LazyTask_1_Start_m3620087119_gshared (LazyTask_1_t2237409813 * __this, const MethodInfo* method);
#define LazyTask_1_Start_m3620087119(__this, method) ((  Coroutine_t2246592261 * (*) (LazyTask_1_t2237409813 *, const MethodInfo*))LazyTask_1_Start_m3620087119_gshared)(__this, method)
// System.String UniRx.LazyTask`1<System.Object>::ToString()
extern "C"  String_t* LazyTask_1_ToString_m598023216_gshared (LazyTask_1_t2237409813 * __this, const MethodInfo* method);
#define LazyTask_1_ToString_m598023216(__this, method) ((  String_t* (*) (LazyTask_1_t2237409813 *, const MethodInfo*))LazyTask_1_ToString_m598023216_gshared)(__this, method)
// UniRx.LazyTask`1<T> UniRx.LazyTask`1<System.Object>::FromResult(T)
extern "C"  LazyTask_1_t2237409813 * LazyTask_1_FromResult_m690895647_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method);
#define LazyTask_1_FromResult_m690895647(__this /* static, unused */, ___value0, method) ((  LazyTask_1_t2237409813 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))LazyTask_1_FromResult_m690895647_gshared)(__this /* static, unused */, ___value0, method)
// System.Void UniRx.LazyTask`1<System.Object>::<Start>m__A6(T)
extern "C"  void LazyTask_1_U3CStartU3Em__A6_m3962041325_gshared (LazyTask_1_t2237409813 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define LazyTask_1_U3CStartU3Em__A6_m3962041325(__this, ___x0, method) ((  void (*) (LazyTask_1_t2237409813 *, Il2CppObject *, const MethodInfo*))LazyTask_1_U3CStartU3Em__A6_m3962041325_gshared)(__this, ___x0, method)
// System.Void UniRx.LazyTask`1<System.Object>::<Start>m__A7(System.Exception)
extern "C"  void LazyTask_1_U3CStartU3Em__A7_m2771203074_gshared (LazyTask_1_t2237409813 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method);
#define LazyTask_1_U3CStartU3Em__A7_m2771203074(__this, ___ex0, method) ((  void (*) (LazyTask_1_t2237409813 *, Exception_t1967233988 *, const MethodInfo*))LazyTask_1_U3CStartU3Em__A7_m2771203074_gshared)(__this, ___ex0, method)
