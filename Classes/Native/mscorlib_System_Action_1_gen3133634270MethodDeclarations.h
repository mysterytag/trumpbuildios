﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"

// System.Void System.Action`1<PlayFab.ClientModels.KongregatePlayFabIdPair>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1766676615(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t3133634270 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1498188969_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<PlayFab.ClientModels.KongregatePlayFabIdPair>::Invoke(T)
#define Action_1_Invoke_m1512036829(__this, ___obj0, method) ((  void (*) (Action_1_t3133634270 *, KongregatePlayFabIdPair_t2985181565 *, const MethodInfo*))Action_1_Invoke_m2789055860_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<PlayFab.ClientModels.KongregatePlayFabIdPair>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m4035241522(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t3133634270 *, KongregatePlayFabIdPair_t2985181565 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m917692971_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<PlayFab.ClientModels.KongregatePlayFabIdPair>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m985146903(__this, ___result0, method) ((  void (*) (Action_1_t3133634270 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3562128182_gshared)(__this, ___result0, method)
