﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Xml.XPath.ExprFilter
struct ExprFilter_t3320087274;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.ExprFunctionCall
struct ExprFunctionCall_t3745907208;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t176365656;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Xml.Xsl.IStaticXsltContext
struct IStaticXsltContext_t2347050862;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t1864207532;
// System.Xml.XPath.ExprGE
struct ExprGE_t1813726896;
// System.Xml.XPath.ExprGT
struct ExprGT_t1813726911;
// System.Xml.XPath.ExprLE
struct ExprLE_t1813727051;
// System.Xml.XPath.ExprLiteral
struct ExprLiteral_t569227735;
// System.Xml.XPath.ExprLT
struct ExprLT_t1813727066;
// System.Xml.XPath.ExprMINUS
struct ExprMINUS_t2200832088;
// System.Xml.XPath.ExprMOD
struct ExprMOD_t79694250;
// System.Xml.XPath.ExprMULT
struct ExprMULT_t2159257026;
// System.Xml.XPath.ExprNE
struct ExprNE_t1813727113;
// System.Xml.XPath.ExprNEG
struct ExprNEG_t79694904;
// System.Xml.XPath.ExprNumber
struct ExprNumber_t3560215227;
// System.Xml.XPath.ExprNumeric
struct ExprNumeric_t2681320501;
// System.Xml.XPath.ExprOR
struct ExprOR_t1813727157;
// System.Xml.XPath.ExprParens
struct ExprParens_t3599155227;
// System.Xml.XPath.ExprPLUS
struct ExprPLUS_t2159338028;
// System.Xml.XPath.ExprRoot
struct ExprRoot_t2159432084;
// System.Xml.XPath.ExprSLASH
struct ExprSLASH_t2206450021;
// System.Xml.XPath.NodeSet
struct NodeSet_t3503134685;
// System.Xml.XPath.ExprSLASH2
struct ExprSLASH2_t3664170439;
// System.Xml.XPath.ExprUNION
struct ExprUNION_t2208364215;
// System.Xml.XPath.ExprVariable
struct ExprVariable_t1838989486;
// System.Xml.XPath.FollowingIterator
struct FollowingIterator_t4160732924;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;
// System.Xml.XPath.FollowingSiblingIterator
struct FollowingSiblingIterator_t1992378268;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Xml.XPath.ListIterator
struct ListIterator_t2154705993;
// System.Collections.IList
struct IList_t1612618265;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3696234203;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1624538935;
// System.Xml.XPath.NamespaceIterator
struct NamespaceIterator_t2306455494;
// System.Xml.XPath.NodeNameTest
struct NodeNameTest_t873888508;
// System.Xml.XPath.NodeTest
struct NodeTest_t911751889;
// System.Xml.XPath.AxisSpecifier
struct AxisSpecifier_t4216515578;
// System.Xml.XPath.NodeTypeTest
struct NodeTypeTest_t2651955243;
// System.Xml.XPath.NullIterator
struct NullIterator_t979652658;
// System.Xml.XPath.ParensIterator
struct ParensIterator_t3967469908;
// System.Xml.XPath.ParentIterator
struct ParentIterator_t2160015445;
// System.Xml.XPath.PrecedingIterator
struct PrecedingIterator_t2050916142;
// System.Xml.XPath.PrecedingSiblingIterator
struct PrecedingSiblingIterator_t3194309418;
// System.Xml.XPath.PredicateIterator
struct PredicateIterator_t4169199650;
// System.Xml.XPath.RelationalExpr
struct RelationalExpr_t4268017369;
// System.Xml.XPath.SelfIterator
struct SelfIterator_t2566309111;
// System.Xml.XPath.SimpleIterator
struct SimpleIterator_t1094113629;
// System.Xml.XPath.SimpleSlashIterator
struct SimpleSlashIterator_t2349635926;
// System.Xml.XPath.SlashIterator
struct SlashIterator_t818098664;
// System.Xml.XPath.SortedIterator
struct SortedIterator_t269320040;
// System.Xml.XPath.UnionIterator
struct UnionIterator_t1824978234;
// System.Xml.XPath.WrapperIterator
struct WrapperIterator_t2702681854;
// System.Xml.XPath.XPathBooleanFunction
struct XPathBooleanFunction_t2634824384;
// System.Xml.XPath.XPathException
struct XPathException_t2353341743;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Exception
struct Exception_t1967233988;
// System.Xml.XPath.XPathExpression
struct XPathExpression_t3441638418;
// System.Xml.XPath.XPathFunction
struct XPathFunction_t33626258;
// System.Xml.XPath.XPathFunctionBoolean
struct XPathFunctionBoolean_t3213667280;
// System.Xml.XPath.XPathFunctionCeil
struct XPathFunctionCeil_t1855013687;
// System.Xml.XPath.XPathFunctionConcat
struct XPathFunctionConcat_t3190339590;
// System.Xml.XPath.XPathFunctionContains
struct XPathFunctionContains_t2249625041;
// System.Xml.XPath.XPathFunctionCount
struct XPathFunctionCount_t1359888247;
// System.Xml.XPath.XPathFunctionFalse
struct XPathFunctionFalse_t1362233227;
// System.Xml.XPath.XPathFunctionFloor
struct XPathFunctionFloor_t1362563700;
// System.Xml.XPath.XPathFunctionId
struct XPathFunctionId_t879333517;
// System.Xml.XPath.XPathFunctionLang
struct XPathFunctionLang_t1855278112;
// System.Xml.XPath.XPathFunctionLast
struct XPathFunctionLast_t1855278280;
// System.Xml.XPath.XPathFunctionLocalName
struct XPathFunctionLocalName_t4208157054;
// System.Xml.XPath.XPathFunctionName
struct XPathFunctionName_t1855337661;
// System.Xml.XPath.XPathFunctionNamespaceUri
struct XPathFunctionNamespaceUri_t2303716163;
// System.Xml.XPath.XPathFunctionNormalizeSpace
struct XPathFunctionNormalizeSpace_t2104917483;
// System.Xml.XPath.XPathFunctionNot
struct XPathFunctionNot_t1178269691;
// System.Xml.XPath.XPathFunctionNumber
struct XPathFunctionNumber_t3510770747;
// System.Xml.XPath.XPathFunctionPosition
struct XPathFunctionPosition_t3564875995;
// System.Xml.XPath.XPathFunctionRound
struct XPathFunctionRound_t1373741046;
// System.Xml.XPath.XPathFunctionStartsWith
struct XPathFunctionStartsWith_t2744556329;
// System.Xml.XPath.XPathFunctionString
struct XPathFunctionString_t3653148931;
// System.Xml.XPath.XPathFunctionStringLength
struct XPathFunctionStringLength_t3423546249;
// System.Xml.XPath.XPathFunctionSubstring
struct XPathFunctionSubstring_t1649127225;
// System.Xml.XPath.XPathFunctionSubstringAfter
struct XPathFunctionSubstringAfter_t2731175421;
// System.Xml.XPath.XPathFunctionSubstringBefore
struct XPathFunctionSubstringBefore_t2778086872;
// System.Xml.XPath.XPathFunctionSum
struct XPathFunctionSum_t1178274675;
// System.Xml.XPath.XPathFunctionTranslate
struct XPathFunctionTranslate_t2171417142;
// System.Xml.XPath.XPathFunctionTrue
struct XPathFunctionTrue_t1855532992;
// System.Xml.XPath.XPathItem
struct XPathItem_t880576077;
// System.Xml.XPath.XPathIteratorComparer
struct XPathIteratorComparer_t4238022261;
// System.Collections.IEnumerable
struct IEnumerable_t287189635;
// System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0
struct U3CEnumerateChildrenU3Ec__Iterator0_t3706982454;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Xml.XPath.XPathNavigator/EnumerableIterator
struct EnumerableIterator_t325940342;
// System.Xml.XPath.XPathNavigatorComparer
struct XPathNavigatorComparer_t877816196;
// System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2
struct U3CGetEnumeratorU3Ec__Iterator2_t490656865;
// System.Xml.XPath.XPathNumericFunction
struct XPathNumericFunction_t3734910117;
// System.Xml.XPath.XPathSortElement
struct XPathSortElement_t2708585374;
// System.Xml.XPath.XPathSorter
struct XPathSorter_t3331359429;
// System.Xml.XPath.XPathSorters
struct XPathSorters_t4176623784;
// System.Xml.Xsl.XsltContext
struct XsltContext_t4010601445;
// System.Xml.Xsl.IXsltContextVariable
struct IXsltContextVariable_t2835953560;
// System.Xml.Xsl.IXsltContextFunction
struct IXsltContextFunction_t1171511540;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Xml_System_Xml_XPath_ExprFilter3320087274.h"
#include "System_Xml_System_Xml_XPath_ExprFilter3320087274MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"
#include "mscorlib_System_Void2779279689.h"
#include "System_Xml_System_Xml_XPath_NodeSet3503134685MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_PredicateIterator4169199650MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_PredicateIterator4169199650.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_Xml_System_Xml_XPath_NodeSet3503134685.h"
#include "System_Xml_System_Xml_XPath_ExprFunctionCall3745907208.h"
#include "System_Xml_System_Xml_XPath_ExprFunctionCall3745907208MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName176365656.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "System_Xml_System_Xml_XmlQualifiedName176365656MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLast1855278280MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionPosition3564875995MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCount1359888247MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionId879333517MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLocalName4208157054MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNamespace2303716163MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionName1855337661MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionString3653148931MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionConcat3190339590MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStartsWit2744556329MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionContains2249625041MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2778086872MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2731175421MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1649127225MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStringLen3423546249MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNormalize2104917483MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate2171417142MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionBoolean3213667280MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNot1178269691MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue1855532992MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFalse1362233227MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLang1855278112MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNumber3510770747MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSum1178274675MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFloor1362563700MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCeil1855013687MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionRound1373741046MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLast1855278280.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionPosition3564875995.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCount1359888247.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionId879333517.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLocalName4208157054.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNamespace2303716163.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionName1855337661.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionString3653148931.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionConcat3190339590.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStartsWit2744556329.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionContains2249625041.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2778086872.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2731175421.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1649127225.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStringLen3423546249.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNormalize2104917483.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate2171417142.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionBoolean3213667280.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNot1178269691.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue1855532992.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFalse1362233227.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLang1855278112.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNumber3510770747.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSum1178274675.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFloor1362563700.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCeil1855013687.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionRound1373741046.h"
#include "mscorlib_System_Char2778706699.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System.Xml_ArrayTypes.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathException2353341743MethodDeclarations.h"
#include "System_Xml_System_Xml_Xsl_XsltContext4010601445.h"
#include "System_Xml_System_Xml_Xsl_XsltContext4010601445MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathException2353341743.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator2394191562MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator2394191562.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1624538935.h"
#include "System_Xml_System_Xml_XPath_ExprGE1813726896.h"
#include "System_Xml_System_Xml_XPath_ExprGE1813726896MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_RelationalExpr4268017369MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "System_Xml_System_Xml_XPath_ExprGT1813726911.h"
#include "System_Xml_System_Xml_XPath_ExprGT1813726911MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLE1813727051.h"
#include "System_Xml_System_Xml_XPath_ExprLE1813727051MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral569227735.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral569227735MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLT1813727066.h"
#include "System_Xml_System_Xml_XPath_ExprLT1813727066MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS2200832088.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS2200832088MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNumeric2681320501MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprBinary3205612403MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprBinary3205612403.h"
#include "System_Xml_System_Xml_XPath_ExprMOD79694250.h"
#include "System_Xml_System_Xml_XPath_ExprMOD79694250MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprMULT2159257026.h"
#include "System_Xml_System_Xml_XPath_ExprMULT2159257026MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNE1813727113.h"
#include "System_Xml_System_Xml_XPath_ExprNE1813727113MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_EqualityExpr3775540012MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNEG79694904.h"
#include "System_Xml_System_Xml_XPath_ExprNEG79694904MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNumber3560215227MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNumber3560215227.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctions731143291MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNumeric2681320501.h"
#include "System_Xml_System_Xml_XPath_ExprOR1813727157.h"
#include "System_Xml_System_Xml_XPath_ExprOR1813727157MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprBoolean451478864MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprParens3599155227.h"
#include "System_Xml_System_Xml_XPath_ExprParens3599155227MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator2702681854MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ParensIterator3967469908MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator2702681854.h"
#include "System_Xml_System_Xml_XPath_ParensIterator3967469908.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS2159338028.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS2159338028MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprRoot2159432084.h"
#include "System_Xml_System_Xml_XPath_ExprRoot2159432084MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SelfIterator2566309111MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1624538935MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SelfIterator2566309111.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH2206450021.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH2206450021MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SimpleSlashIterator2349635926MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SlashIterator818098664MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SortedIterator269320040MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SimpleSlashIterator2349635926.h"
#include "System_Xml_System_Xml_XPath_SlashIterator818098664.h"
#include "System_Xml_System_Xml_XPath_SortedIterator269320040.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH23664170439.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH23664170439MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest2651955243MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest2651955243.h"
#include "System_Xml_System_Xml_XPath_Axes4021066818.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType4070737174.h"
#include "System_Xml_System_Xml_XPath_NodeTest911751889.h"
#include "System_Xml_System_Xml_XPath_NodeTest911751889MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_AxisSpecifier4216515578MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest873888508MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest873888508.h"
#include "System_Xml_System_Xml_XPath_AxisSpecifier4216515578.h"
#include "System_Xml_System_Xml_XPath_ExprUNION2208364215.h"
#include "System_Xml_System_Xml_XPath_ExprUNION2208364215MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_UnionIterator1824978234MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_UnionIterator1824978234.h"
#include "System_Xml_System_Xml_XPath_ExprVariable1838989486.h"
#include "System_Xml_System_Xml_XPath_ExprVariable1838989486MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "System_Xml_System_Xml_XPath_FollowingIterator4160732924.h"
#include "System_Xml_System_Xml_XPath_FollowingIterator4160732924MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SimpleIterator1094113629MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SimpleIterator1094113629.h"
#include "System_Xml_System_Xml_XPath_FollowingSiblingIterat1992378268.h"
#include "System_Xml_System_Xml_XPath_FollowingSiblingIterat1992378268MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ListIterator2154705993.h"
#include "System_Xml_System_Xml_XPath_ListIterator2154705993MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NamespaceIterator2306455494.h"
#include "System_Xml_System_Xml_XPath_NamespaceIterator2306455494MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_AxisIterator3322918988MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_AxisIterator3322918988.h"
#include "mscorlib_System_Enum2778772662MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662.h"
#include "System_Xml_System_Xml_XPath_NullIterator979652658.h"
#include "System_Xml_System_Xml_XPath_NullIterator979652658MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ParentIterator2160015445.h"
#include "System_Xml_System_Xml_XPath_ParentIterator2160015445MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_PrecedingIterator2050916142.h"
#include "System_Xml_System_Xml_XPath_PrecedingIterator2050916142MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNodeOrder2460046845.h"
#include "System_Xml_System_Xml_XPath_PrecedingSiblingIterat3194309418.h"
#include "System_Xml_System_Xml_XPath_PrecedingSiblingIterat3194309418MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_RelationalExpr4268017369.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathItem880576077MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathItem880576077.h"
#include "mscorlib_System_Collections_SortedList3322490541.h"
#include "mscorlib_System_Collections_SortedList3322490541MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer4238022261.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer4238022261MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer877816196.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer877816196MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "System_Xml_System_Xml_XPath_XmlDataType483140632.h"
#include "System_Xml_System_Xml_XPath_XmlDataType483140632MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathBooleanFunction2634824384.h"
#include "System_Xml_System_Xml_XPath_XPathBooleanFunction2634824384MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunction33626258MethodDeclarations.h"
#include "mscorlib_System_SystemException3155420757MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathExpression3441638418.h"
#include "System_Xml_System_Xml_XPath_XPathExpression3441638418MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser618877717MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression938776358MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser618877717.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression938776358.h"
#include "System_Xml_System_Xml_XPath_XPathFunction33626258.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction3734910117MethodDeclarations.h"
#include "mscorlib_System_Math2778998461MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1695827251MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2777878083.h"
#include "mscorlib_System_RuntimeFieldHandle3184214143.h"
#include "System_Xml_System_Xml_XPath_XPathFunctions731143291.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "mscorlib_System_Globalization_NumberFormatInfo3411951076MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo3411951076.h"
#include "mscorlib_System_OverflowException3216083426.h"
#include "mscorlib_System_FormatException2404802957.h"
#include "System_Xml_System_Xml_XmlChar3591879093.h"
#include "System_Xml_System_Xml_XmlChar3591879093MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberStyles3988678145.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope2865249459.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope2865249459MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar214874486.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum3706982454MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum3706982454.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumerab325940342MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumerab325940342.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGe490656865MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGe490656865.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType4070737174MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction3734910117.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement2708585374.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement2708585374MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathSorter3331359429.h"
#include "System_Xml_System_Xml_XPath_XPathSorter3331359429MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathSorters4176623784.h"
#include "System_Xml_System_Xml_XPath_XPathSorters4176623784MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1861067185MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1861067185.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XPath.ExprFilter::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprFilter__ctor_m1987528211 (ExprFilter_t3320087274 * __this, Expression_t4217024437 * ___expr0, Expression_t4217024437 * ___pred1, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		Expression_t4217024437 * L_0 = ___expr0;
		__this->set_expr_0(L_0);
		Expression_t4217024437 * L_1 = ___pred1;
		__this->set_pred_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprFilter::Optimize()
extern "C"  Expression_t4217024437 * ExprFilter_Optimize_m4005115936 (ExprFilter_t3320087274 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_expr_0();
		NullCheck(L_0);
		Expression_t4217024437 * L_1 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set_expr_0(L_1);
		Expression_t4217024437 * L_2 = __this->get_pred_1();
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set_pred_1(L_3);
		return __this;
	}
}
// System.String System.Xml.XPath.ExprFilter::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral1362;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t ExprFilter_ToString_m1575612896_MetadataUsageId;
extern "C"  String_t* ExprFilter_ToString_m1575612896 (ExprFilter_t3320087274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFilter_ToString_m1575612896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral40);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral40);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_expr_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral1362);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1362);
		StringU5BU5D_t2956870243* L_5 = L_4;
		Expression_t4217024437 * L_6 = __this->get_pred_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t2956870243* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral93);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Object System.Xml.XPath.ExprFilter::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* PredicateIterator_t4169199650_il2cpp_TypeInfo_var;
extern const uint32_t ExprFilter_Evaluate_m1209514577_MetadataUsageId;
extern "C"  Il2CppObject * ExprFilter_Evaluate_m1209514577 (ExprFilter_t3320087274 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFilter_Evaluate_m1209514577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t3696600956 * V_0 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_expr_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t3696600956 * L_2 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		BaseIterator_t3696600956 * L_3 = V_0;
		Expression_t4217024437 * L_4 = __this->get_pred_1();
		PredicateIterator_t4169199650 * L_5 = (PredicateIterator_t4169199650 *)il2cpp_codegen_object_new(PredicateIterator_t4169199650_il2cpp_TypeInfo_var);
		PredicateIterator__ctor_m3004206002(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.Xml.XPath.ExprFilter::get_Peer()
extern "C"  bool ExprFilter_get_Peer_m1513885530 (ExprFilter_t3320087274 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get_pred_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprFilter::get_Subtree()
extern Il2CppClass* NodeSet_t3503134685_il2cpp_TypeInfo_var;
extern const uint32_t ExprFilter_get_Subtree_m1459955336_MetadataUsageId;
extern "C"  bool ExprFilter_get_Subtree_m1459955336 (ExprFilter_t3320087274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFilter_get_Subtree_m1459955336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t3503134685 * V_0 = NULL;
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_expr_0();
		V_0 = ((NodeSet_t3503134685 *)IsInstClass(L_0, NodeSet_t3503134685_il2cpp_TypeInfo_var));
		NodeSet_t3503134685 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		NodeSet_t3503134685 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
	}

IL_001b:
	{
		return (bool)G_B3_0;
	}
}
// System.Void System.Xml.XPath.ExprFunctionCall::.ctor(System.Xml.XmlQualifiedName,System.Xml.XPath.FunctionArguments,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var;
extern const uint32_t ExprFunctionCall__ctor_m3125265142_MetadataUsageId;
extern "C"  void ExprFunctionCall__ctor_m3125265142 (ExprFunctionCall_t3745907208 * __this, XmlQualifiedName_t176365656 * ___name0, FunctionArguments_t3402201403 * ___args1, Il2CppObject * ___ctx2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall__ctor_m3125265142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set__args_2(L_0);
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___ctx2;
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_2 = ___ctx2;
		XmlQualifiedName_t176365656 * L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_3);
		NullCheck(L_2);
		XmlQualifiedName_t176365656 * L_5 = InterfaceFuncInvoker1< XmlQualifiedName_t176365656 *, String_t* >::Invoke(2 /* System.Xml.XmlQualifiedName System.Xml.Xsl.IStaticXsltContext::LookupQName(System.String) */, IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var, L_2, L_4);
		___name0 = L_5;
		__this->set_resolvedName_1((bool)1);
	}

IL_002c:
	{
		XmlQualifiedName_t176365656 * L_6 = ___name0;
		__this->set__name_0(L_6);
		FunctionArguments_t3402201403 * L_7 = ___args1;
		if (!L_7)
		{
			goto IL_0045;
		}
	}
	{
		FunctionArguments_t3402201403 * L_8 = ___args1;
		ArrayList_t2121638921 * L_9 = __this->get__args_2();
		NullCheck(L_8);
		FunctionArguments_ToArrayList_m227079145(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprFunctionCall::Factory(System.Xml.XmlQualifiedName,System.Xml.XPath.FunctionArguments,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprFunctionCall_t3745907208_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionLast_t1855278280_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionPosition_t3564875995_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionCount_t1359888247_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionId_t879333517_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionLocalName_t4208157054_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionNamespaceUri_t2303716163_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionName_t1855337661_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionString_t3653148931_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionConcat_t3190339590_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionStartsWith_t2744556329_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionContains_t2249625041_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionSubstringBefore_t2778086872_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionSubstringAfter_t2731175421_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionSubstring_t1649127225_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionStringLength_t3423546249_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionNormalizeSpace_t2104917483_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionTranslate_t2171417142_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionBoolean_t3213667280_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionNot_t1178269691_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionTrue_t1855532992_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionFalse_t1362233227_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionLang_t1855278112_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionNumber_t3510770747_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionSum_t1178274675_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionFloor_t1362563700_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionCeil_t1855013687_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionRound_t1373741046_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3314326;
extern Il2CppCodeGenString* _stringLiteral747804969;
extern Il2CppCodeGenString* _stringLiteral94851343;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral1257351085;
extern Il2CppCodeGenString* _stringLiteral4094257658;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3402981393;
extern Il2CppCodeGenString* _stringLiteral2940172052;
extern Il2CppCodeGenString* _stringLiteral3279525058;
extern Il2CppCodeGenString* _stringLiteral3727521311;
extern Il2CppCodeGenString* _stringLiteral2021305979;
extern Il2CppCodeGenString* _stringLiteral2835269472;
extern Il2CppCodeGenString* _stringLiteral530542161;
extern Il2CppCodeGenString* _stringLiteral2899918882;
extern Il2CppCodeGenString* _stringLiteral94053958;
extern Il2CppCodeGenString* _stringLiteral1052832078;
extern Il2CppCodeGenString* _stringLiteral64711720;
extern Il2CppCodeGenString* _stringLiteral109267;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3314158;
extern Il2CppCodeGenString* _stringLiteral3260603209;
extern Il2CppCodeGenString* _stringLiteral114251;
extern Il2CppCodeGenString* _stringLiteral97526796;
extern Il2CppCodeGenString* _stringLiteral660387005;
extern Il2CppCodeGenString* _stringLiteral108704142;
extern const uint32_t ExprFunctionCall_Factory_m1613910802_MetadataUsageId;
extern "C"  Expression_t4217024437 * ExprFunctionCall_Factory_m1613910802 (Il2CppObject * __this /* static, unused */, XmlQualifiedName_t176365656 * ___name0, FunctionArguments_t3402201403 * ___args1, Il2CppObject * ___ctx2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall_Factory_m1613910802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t190145395 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		XmlQualifiedName_t176365656 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Namespace_m2987642414(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = XmlQualifiedName_get_Namespace_m2987642414(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_5 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_6 = ___name0;
		FunctionArguments_t3402201403 * L_7 = ___args1;
		Il2CppObject * L_8 = ___ctx2;
		ExprFunctionCall_t3745907208 * L_9 = (ExprFunctionCall_t3745907208 *)il2cpp_codegen_object_new(ExprFunctionCall_t3745907208_il2cpp_TypeInfo_var);
		ExprFunctionCall__ctor_m3125265142(L_9, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0029:
	{
		XmlQualifiedName_t176365656 * L_10 = ___name0;
		NullCheck(L_10);
		String_t* L_11 = XmlQualifiedName_get_Name_m607016698(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		String_t* L_12 = V_0;
		if (!L_12)
		{
			goto IL_02ea;
		}
	}
	{
		Dictionary_2_t190145395 * L_13 = ((ExprFunctionCall_t3745907208_StaticFields*)ExprFunctionCall_t3745907208_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map41_3();
		if (L_13)
		{
			goto IL_01a4;
		}
	}
	{
		Dictionary_2_t190145395 * L_14 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_14, ((int32_t)27), /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_14;
		Dictionary_2_t190145395 * L_15 = V_1;
		NullCheck(L_15);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_15, _stringLiteral3314326, 0);
		Dictionary_2_t190145395 * L_16 = V_1;
		NullCheck(L_16);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_16, _stringLiteral747804969, 1);
		Dictionary_2_t190145395 * L_17 = V_1;
		NullCheck(L_17);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_17, _stringLiteral94851343, 2);
		Dictionary_2_t190145395 * L_18 = V_1;
		NullCheck(L_18);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_18, _stringLiteral3355, 3);
		Dictionary_2_t190145395 * L_19 = V_1;
		NullCheck(L_19);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_19, _stringLiteral1257351085, 4);
		Dictionary_2_t190145395 * L_20 = V_1;
		NullCheck(L_20);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_20, _stringLiteral4094257658, 5);
		Dictionary_2_t190145395 * L_21 = V_1;
		NullCheck(L_21);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_21, _stringLiteral3373707, 6);
		Dictionary_2_t190145395 * L_22 = V_1;
		NullCheck(L_22);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_22, _stringLiteral3402981393, 7);
		Dictionary_2_t190145395 * L_23 = V_1;
		NullCheck(L_23);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_23, _stringLiteral2940172052, 8);
		Dictionary_2_t190145395 * L_24 = V_1;
		NullCheck(L_24);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_24, _stringLiteral3279525058, ((int32_t)9));
		Dictionary_2_t190145395 * L_25 = V_1;
		NullCheck(L_25);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_25, _stringLiteral3727521311, ((int32_t)10));
		Dictionary_2_t190145395 * L_26 = V_1;
		NullCheck(L_26);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_26, _stringLiteral2021305979, ((int32_t)11));
		Dictionary_2_t190145395 * L_27 = V_1;
		NullCheck(L_27);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_27, _stringLiteral2835269472, ((int32_t)12));
		Dictionary_2_t190145395 * L_28 = V_1;
		NullCheck(L_28);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_28, _stringLiteral530542161, ((int32_t)13));
		Dictionary_2_t190145395 * L_29 = V_1;
		NullCheck(L_29);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_29, _stringLiteral2899918882, ((int32_t)14));
		Dictionary_2_t190145395 * L_30 = V_1;
		NullCheck(L_30);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_30, _stringLiteral94053958, ((int32_t)15));
		Dictionary_2_t190145395 * L_31 = V_1;
		NullCheck(L_31);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_31, _stringLiteral1052832078, ((int32_t)16));
		Dictionary_2_t190145395 * L_32 = V_1;
		NullCheck(L_32);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_32, _stringLiteral64711720, ((int32_t)17));
		Dictionary_2_t190145395 * L_33 = V_1;
		NullCheck(L_33);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_33, _stringLiteral109267, ((int32_t)18));
		Dictionary_2_t190145395 * L_34 = V_1;
		NullCheck(L_34);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_34, _stringLiteral3569038, ((int32_t)19));
		Dictionary_2_t190145395 * L_35 = V_1;
		NullCheck(L_35);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_35, _stringLiteral97196323, ((int32_t)20));
		Dictionary_2_t190145395 * L_36 = V_1;
		NullCheck(L_36);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_36, _stringLiteral3314158, ((int32_t)21));
		Dictionary_2_t190145395 * L_37 = V_1;
		NullCheck(L_37);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_37, _stringLiteral3260603209, ((int32_t)22));
		Dictionary_2_t190145395 * L_38 = V_1;
		NullCheck(L_38);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_38, _stringLiteral114251, ((int32_t)23));
		Dictionary_2_t190145395 * L_39 = V_1;
		NullCheck(L_39);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_39, _stringLiteral97526796, ((int32_t)24));
		Dictionary_2_t190145395 * L_40 = V_1;
		NullCheck(L_40);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_40, _stringLiteral660387005, ((int32_t)25));
		Dictionary_2_t190145395 * L_41 = V_1;
		NullCheck(L_41);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_41, _stringLiteral108704142, ((int32_t)26));
		Dictionary_2_t190145395 * L_42 = V_1;
		((ExprFunctionCall_t3745907208_StaticFields*)ExprFunctionCall_t3745907208_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map41_3(L_42);
	}

IL_01a4:
	{
		Dictionary_2_t190145395 * L_43 = ((ExprFunctionCall_t3745907208_StaticFields*)ExprFunctionCall_t3745907208_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map41_3();
		String_t* L_44 = V_0;
		NullCheck(L_43);
		bool L_45 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_43, L_44, (&V_2));
		if (!L_45)
		{
			goto IL_02ea;
		}
	}
	{
		int32_t L_46 = V_2;
		if (L_46 == 0)
		{
			goto IL_022d;
		}
		if (L_46 == 1)
		{
			goto IL_0234;
		}
		if (L_46 == 2)
		{
			goto IL_023b;
		}
		if (L_46 == 3)
		{
			goto IL_0242;
		}
		if (L_46 == 4)
		{
			goto IL_0249;
		}
		if (L_46 == 5)
		{
			goto IL_0250;
		}
		if (L_46 == 6)
		{
			goto IL_0257;
		}
		if (L_46 == 7)
		{
			goto IL_025e;
		}
		if (L_46 == 8)
		{
			goto IL_0265;
		}
		if (L_46 == 9)
		{
			goto IL_026c;
		}
		if (L_46 == 10)
		{
			goto IL_0273;
		}
		if (L_46 == 11)
		{
			goto IL_027a;
		}
		if (L_46 == 12)
		{
			goto IL_0281;
		}
		if (L_46 == 13)
		{
			goto IL_0288;
		}
		if (L_46 == 14)
		{
			goto IL_028f;
		}
		if (L_46 == 15)
		{
			goto IL_0296;
		}
		if (L_46 == 16)
		{
			goto IL_029d;
		}
		if (L_46 == 17)
		{
			goto IL_02a4;
		}
		if (L_46 == 18)
		{
			goto IL_02ab;
		}
		if (L_46 == 19)
		{
			goto IL_02b2;
		}
		if (L_46 == 20)
		{
			goto IL_02b9;
		}
		if (L_46 == 21)
		{
			goto IL_02c0;
		}
		if (L_46 == 22)
		{
			goto IL_02c7;
		}
		if (L_46 == 23)
		{
			goto IL_02ce;
		}
		if (L_46 == 24)
		{
			goto IL_02d5;
		}
		if (L_46 == 25)
		{
			goto IL_02dc;
		}
		if (L_46 == 26)
		{
			goto IL_02e3;
		}
	}
	{
		goto IL_02ea;
	}

IL_022d:
	{
		FunctionArguments_t3402201403 * L_47 = ___args1;
		XPathFunctionLast_t1855278280 * L_48 = (XPathFunctionLast_t1855278280 *)il2cpp_codegen_object_new(XPathFunctionLast_t1855278280_il2cpp_TypeInfo_var);
		XPathFunctionLast__ctor_m565310986(L_48, L_47, /*hidden argument*/NULL);
		return L_48;
	}

IL_0234:
	{
		FunctionArguments_t3402201403 * L_49 = ___args1;
		XPathFunctionPosition_t3564875995 * L_50 = (XPathFunctionPosition_t3564875995 *)il2cpp_codegen_object_new(XPathFunctionPosition_t3564875995_il2cpp_TypeInfo_var);
		XPathFunctionPosition__ctor_m545736599(L_50, L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_023b:
	{
		FunctionArguments_t3402201403 * L_51 = ___args1;
		XPathFunctionCount_t1359888247 * L_52 = (XPathFunctionCount_t1359888247 *)il2cpp_codegen_object_new(XPathFunctionCount_t1359888247_il2cpp_TypeInfo_var);
		XPathFunctionCount__ctor_m74396489(L_52, L_51, /*hidden argument*/NULL);
		return L_52;
	}

IL_0242:
	{
		FunctionArguments_t3402201403 * L_53 = ___args1;
		XPathFunctionId_t879333517 * L_54 = (XPathFunctionId_t879333517 *)il2cpp_codegen_object_new(XPathFunctionId_t879333517_il2cpp_TypeInfo_var);
		XPathFunctionId__ctor_m3649212261(L_54, L_53, /*hidden argument*/NULL);
		return L_54;
	}

IL_0249:
	{
		FunctionArguments_t3402201403 * L_55 = ___args1;
		XPathFunctionLocalName_t4208157054 * L_56 = (XPathFunctionLocalName_t4208157054 *)il2cpp_codegen_object_new(XPathFunctionLocalName_t4208157054_il2cpp_TypeInfo_var);
		XPathFunctionLocalName__ctor_m2097528674(L_56, L_55, /*hidden argument*/NULL);
		return L_56;
	}

IL_0250:
	{
		FunctionArguments_t3402201403 * L_57 = ___args1;
		XPathFunctionNamespaceUri_t2303716163 * L_58 = (XPathFunctionNamespaceUri_t2303716163 *)il2cpp_codegen_object_new(XPathFunctionNamespaceUri_t2303716163_il2cpp_TypeInfo_var);
		XPathFunctionNamespaceUri__ctor_m503725999(L_58, L_57, /*hidden argument*/NULL);
		return L_58;
	}

IL_0257:
	{
		FunctionArguments_t3402201403 * L_59 = ___args1;
		XPathFunctionName_t1855337661 * L_60 = (XPathFunctionName_t1855337661 *)il2cpp_codegen_object_new(XPathFunctionName_t1855337661_il2cpp_TypeInfo_var);
		XPathFunctionName__ctor_m17270517(L_60, L_59, /*hidden argument*/NULL);
		return L_60;
	}

IL_025e:
	{
		FunctionArguments_t3402201403 * L_61 = ___args1;
		XPathFunctionString_t3653148931 * L_62 = (XPathFunctionString_t3653148931 *)il2cpp_codegen_object_new(XPathFunctionString_t3653148931_il2cpp_TypeInfo_var);
		XPathFunctionString__ctor_m3584382127(L_62, L_61, /*hidden argument*/NULL);
		return L_62;
	}

IL_0265:
	{
		FunctionArguments_t3402201403 * L_63 = ___args1;
		XPathFunctionConcat_t3190339590 * L_64 = (XPathFunctionConcat_t3190339590 *)il2cpp_codegen_object_new(XPathFunctionConcat_t3190339590_il2cpp_TypeInfo_var);
		XPathFunctionConcat__ctor_m2570590668(L_64, L_63, /*hidden argument*/NULL);
		return L_64;
	}

IL_026c:
	{
		FunctionArguments_t3402201403 * L_65 = ___args1;
		XPathFunctionStartsWith_t2744556329 * L_66 = (XPathFunctionStartsWith_t2744556329 *)il2cpp_codegen_object_new(XPathFunctionStartsWith_t2744556329_il2cpp_TypeInfo_var);
		XPathFunctionStartsWith__ctor_m3911729737(L_66, L_65, /*hidden argument*/NULL);
		return L_66;
	}

IL_0273:
	{
		FunctionArguments_t3402201403 * L_67 = ___args1;
		XPathFunctionContains_t2249625041 * L_68 = (XPathFunctionContains_t2249625041 *)il2cpp_codegen_object_new(XPathFunctionContains_t2249625041_il2cpp_TypeInfo_var);
		XPathFunctionContains__ctor_m2892508385(L_68, L_67, /*hidden argument*/NULL);
		return L_68;
	}

IL_027a:
	{
		FunctionArguments_t3402201403 * L_69 = ___args1;
		XPathFunctionSubstringBefore_t2778086872 * L_70 = (XPathFunctionSubstringBefore_t2778086872 *)il2cpp_codegen_object_new(XPathFunctionSubstringBefore_t2778086872_il2cpp_TypeInfo_var);
		XPathFunctionSubstringBefore__ctor_m498616712(L_70, L_69, /*hidden argument*/NULL);
		return L_70;
	}

IL_0281:
	{
		FunctionArguments_t3402201403 * L_71 = ___args1;
		XPathFunctionSubstringAfter_t2731175421 * L_72 = (XPathFunctionSubstringAfter_t2731175421 *)il2cpp_codegen_object_new(XPathFunctionSubstringAfter_t2731175421_il2cpp_TypeInfo_var);
		XPathFunctionSubstringAfter__ctor_m4187633269(L_72, L_71, /*hidden argument*/NULL);
		return L_72;
	}

IL_0288:
	{
		FunctionArguments_t3402201403 * L_73 = ___args1;
		XPathFunctionSubstring_t1649127225 * L_74 = (XPathFunctionSubstring_t1649127225 *)il2cpp_codegen_object_new(XPathFunctionSubstring_t1649127225_il2cpp_TypeInfo_var);
		XPathFunctionSubstring__ctor_m2565884615(L_74, L_73, /*hidden argument*/NULL);
		return L_74;
	}

IL_028f:
	{
		FunctionArguments_t3402201403 * L_75 = ___args1;
		XPathFunctionStringLength_t3423546249 * L_76 = (XPathFunctionStringLength_t3423546249 *)il2cpp_codegen_object_new(XPathFunctionStringLength_t3423546249_il2cpp_TypeInfo_var);
		XPathFunctionStringLength__ctor_m2699284393(L_76, L_75, /*hidden argument*/NULL);
		return L_76;
	}

IL_0296:
	{
		FunctionArguments_t3402201403 * L_77 = ___args1;
		XPathFunctionNormalizeSpace_t2104917483 * L_78 = (XPathFunctionNormalizeSpace_t2104917483 *)il2cpp_codegen_object_new(XPathFunctionNormalizeSpace_t2104917483_il2cpp_TypeInfo_var);
		XPathFunctionNormalizeSpace__ctor_m3177496007(L_78, L_77, /*hidden argument*/NULL);
		return L_78;
	}

IL_029d:
	{
		FunctionArguments_t3402201403 * L_79 = ___args1;
		XPathFunctionTranslate_t2171417142 * L_80 = (XPathFunctionTranslate_t2171417142 *)il2cpp_codegen_object_new(XPathFunctionTranslate_t2171417142_il2cpp_TypeInfo_var);
		XPathFunctionTranslate__ctor_m1616227242(L_80, L_79, /*hidden argument*/NULL);
		return L_80;
	}

IL_02a4:
	{
		FunctionArguments_t3402201403 * L_81 = ___args1;
		XPathFunctionBoolean_t3213667280 * L_82 = (XPathFunctionBoolean_t3213667280 *)il2cpp_codegen_object_new(XPathFunctionBoolean_t3213667280_il2cpp_TypeInfo_var);
		XPathFunctionBoolean__ctor_m2311468944(L_82, L_81, /*hidden argument*/NULL);
		return L_82;
	}

IL_02ab:
	{
		FunctionArguments_t3402201403 * L_83 = ___args1;
		XPathFunctionNot_t1178269691 * L_84 = (XPathFunctionNot_t1178269691 *)il2cpp_codegen_object_new(XPathFunctionNot_t1178269691_il2cpp_TypeInfo_var);
		XPathFunctionNot__ctor_m1396109829(L_84, L_83, /*hidden argument*/NULL);
		return L_84;
	}

IL_02b2:
	{
		FunctionArguments_t3402201403 * L_85 = ___args1;
		XPathFunctionTrue_t1855532992 * L_86 = (XPathFunctionTrue_t1855532992 *)il2cpp_codegen_object_new(XPathFunctionTrue_t1855532992_il2cpp_TypeInfo_var);
		XPathFunctionTrue__ctor_m3889883154(L_86, L_85, /*hidden argument*/NULL);
		return L_86;
	}

IL_02b9:
	{
		FunctionArguments_t3402201403 * L_87 = ___args1;
		XPathFunctionFalse_t1362233227 * L_88 = (XPathFunctionFalse_t1362233227 *)il2cpp_codegen_object_new(XPathFunctionFalse_t1362233227_il2cpp_TypeInfo_var);
		XPathFunctionFalse__ctor_m1496753845(L_88, L_87, /*hidden argument*/NULL);
		return L_88;
	}

IL_02c0:
	{
		FunctionArguments_t3402201403 * L_89 = ___args1;
		XPathFunctionLang_t1855278112 * L_90 = (XPathFunctionLang_t1855278112 *)il2cpp_codegen_object_new(XPathFunctionLang_t1855278112_il2cpp_TypeInfo_var);
		XPathFunctionLang__ctor_m915197874(L_90, L_89, /*hidden argument*/NULL);
		return L_90;
	}

IL_02c7:
	{
		FunctionArguments_t3402201403 * L_91 = ___args1;
		XPathFunctionNumber_t3510770747 * L_92 = (XPathFunctionNumber_t3510770747 *)il2cpp_codegen_object_new(XPathFunctionNumber_t3510770747_il2cpp_TypeInfo_var);
		XPathFunctionNumber__ctor_m2761557623(L_92, L_91, /*hidden argument*/NULL);
		return L_92;
	}

IL_02ce:
	{
		FunctionArguments_t3402201403 * L_93 = ___args1;
		XPathFunctionSum_t1178274675 * L_94 = (XPathFunctionSum_t1178274675 *)il2cpp_codegen_object_new(XPathFunctionSum_t1178274675_il2cpp_TypeInfo_var);
		XPathFunctionSum__ctor_m1037722509(L_94, L_93, /*hidden argument*/NULL);
		return L_94;
	}

IL_02d5:
	{
		FunctionArguments_t3402201403 * L_95 = ___args1;
		XPathFunctionFloor_t1362563700 * L_96 = (XPathFunctionFloor_t1362563700 *)il2cpp_codegen_object_new(XPathFunctionFloor_t1362563700_il2cpp_TypeInfo_var);
		XPathFunctionFloor__ctor_m224085036(L_96, L_95, /*hidden argument*/NULL);
		return L_96;
	}

IL_02dc:
	{
		FunctionArguments_t3402201403 * L_97 = ___args1;
		XPathFunctionCeil_t1855013687 * L_98 = (XPathFunctionCeil_t1855013687 *)il2cpp_codegen_object_new(XPathFunctionCeil_t1855013687_il2cpp_TypeInfo_var);
		XPathFunctionCeil__ctor_m3298422843(L_98, L_97, /*hidden argument*/NULL);
		return L_98;
	}

IL_02e3:
	{
		FunctionArguments_t3402201403 * L_99 = ___args1;
		XPathFunctionRound_t1373741046 * L_100 = (XPathFunctionRound_t1373741046 *)il2cpp_codegen_object_new(XPathFunctionRound_t1373741046_il2cpp_TypeInfo_var);
		XPathFunctionRound__ctor_m948443498(L_100, L_99, /*hidden argument*/NULL);
		return L_100;
	}

IL_02ea:
	{
		XmlQualifiedName_t176365656 * L_101 = ___name0;
		FunctionArguments_t3402201403 * L_102 = ___args1;
		Il2CppObject * L_103 = ___ctx2;
		ExprFunctionCall_t3745907208 * L_104 = (ExprFunctionCall_t3745907208 *)il2cpp_codegen_object_new(ExprFunctionCall_t3745907208_il2cpp_TypeInfo_var);
		ExprFunctionCall__ctor_m3125265142(L_104, L_101, L_102, L_103, /*hidden argument*/NULL);
		return L_104;
	}
}
// System.String System.Xml.XPath.ExprFunctionCall::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t4217024437_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t ExprFunctionCall_ToString_m3998080894_MetadataUsageId;
extern "C"  String_t* ExprFunctionCall_ToString_m3998080894 (ExprFunctionCall_t3745907208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall_ToString_m3998080894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	Expression_t4217024437 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = 0;
		goto IL_004c;
	}

IL_000d:
	{
		ArrayList_t2121638921 * L_1 = __this->get__args_2();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_1, L_2);
		V_2 = ((Expression_t4217024437 *)CastclassClass(L_3, Expression_t4217024437_il2cpp_TypeInfo_var));
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_6 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, L_7, _stringLiteral1396, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_003b:
	{
		String_t* L_9 = V_0;
		Expression_t4217024437 * L_10 = V_2;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_14 = V_1;
		ArrayList_t2121638921 * L_15 = __this->get__args_2();
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_15);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_000d;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_17 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		XmlQualifiedName_t176365656 * L_18 = __this->get__name_0();
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_18);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_19);
		ObjectU5BU5D_t11523773* L_20 = L_17;
		uint16_t L_21 = ((uint16_t)((int32_t)40));
		Il2CppObject * L_22 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = L_20;
		String_t* L_24 = V_0;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_24);
		ObjectU5BU5D_t11523773* L_25 = L_23;
		uint16_t L_26 = ((uint16_t)((int32_t)41));
		Il2CppObject * L_27 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 3);
		ArrayElementTypeCheck (L_25, L_27);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m3016520001(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		return L_28;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprFunctionCall::get_ReturnType()
extern "C"  int32_t ExprFunctionCall_get_ReturnType_m3858789124 (ExprFunctionCall_t3745907208 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprFunctionCall::GetReturnType(System.Xml.XPath.BaseIterator)
extern "C"  int32_t ExprFunctionCall_GetReturnType_m2079911483 (ExprFunctionCall_t3745907208 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Xml.XPath.XPathResultType[] System.Xml.XPath.ExprFunctionCall::GetArgTypes(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XPathResultTypeU5BU5D_t1864207532_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t4217024437_il2cpp_TypeInfo_var;
extern const uint32_t ExprFunctionCall_GetArgTypes_m669661892_MetadataUsageId;
extern "C"  XPathResultTypeU5BU5D_t1864207532* ExprFunctionCall_GetArgTypes_m669661892 (ExprFunctionCall_t3745907208 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall_GetArgTypes_m669661892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathResultTypeU5BU5D_t1864207532* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ArrayList_t2121638921 * L_0 = __this->get__args_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		V_0 = ((XPathResultTypeU5BU5D_t1864207532*)SZArrayNew(XPathResultTypeU5BU5D_t1864207532_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_1 = 0;
		goto IL_0036;
	}

IL_0018:
	{
		XPathResultTypeU5BU5D_t1864207532* L_2 = V_0;
		int32_t L_3 = V_1;
		ArrayList_t2121638921 * L_4 = __this->get__args_2();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, L_5);
		BaseIterator_t3696600956 * L_7 = ___iter0;
		NullCheck(((Expression_t4217024437 *)CastclassClass(L_6, Expression_t4217024437_il2cpp_TypeInfo_var)));
		int32_t L_8 = VirtFuncInvoker1< int32_t, BaseIterator_t3696600956 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, ((Expression_t4217024437 *)CastclassClass(L_6, Expression_t4217024437_il2cpp_TypeInfo_var)), L_7);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (int32_t)L_8);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_1;
		ArrayList_t2121638921 * L_11 = __this->get__args_2();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0018;
		}
	}
	{
		XPathResultTypeU5BU5D_t1864207532* L_13 = V_0;
		return L_13;
	}
}
// System.Object System.Xml.XPath.ExprFunctionCall::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XsltContext_t4010601445_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* IXsltContextFunction_t1171511540_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t4217024437_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4154394440;
extern Il2CppCodeGenString* _stringLiteral2113392597;
extern const uint32_t ExprFunctionCall_Evaluate_m3216499699_MetadataUsageId;
extern "C"  Il2CppObject * ExprFunctionCall_Evaluate_m3216499699 (ExprFunctionCall_t3745907208 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall_Evaluate_m3216499699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathResultTypeU5BU5D_t1864207532* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	XsltContext_t4010601445 * V_2 = NULL;
	ObjectU5BU5D_t11523773* V_3 = NULL;
	XPathResultTypeU5BU5D_t1864207532* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Expression_t4217024437 * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		XPathResultTypeU5BU5D_t1864207532* L_1 = ExprFunctionCall_GetArgTypes_m669661892(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = (Il2CppObject *)NULL;
		BaseIterator_t3696600956 * L_2 = ___iter0;
		NullCheck(L_2);
		Il2CppObject * L_3 = BaseIterator_get_NamespaceManager_m3653702256(L_2, /*hidden argument*/NULL);
		V_2 = ((XsltContext_t4010601445 *)IsInstClass(L_3, XsltContext_t4010601445_il2cpp_TypeInfo_var));
		XsltContext_t4010601445 * L_4 = V_2;
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		bool L_5 = __this->get_resolvedName_1();
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		XsltContext_t4010601445 * L_6 = V_2;
		XmlQualifiedName_t176365656 * L_7 = __this->get__name_0();
		XPathResultTypeU5BU5D_t1864207532* L_8 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_9 = VirtFuncInvoker2< Il2CppObject *, XmlQualifiedName_t176365656 *, XPathResultTypeU5BU5D_t1864207532* >::Invoke(18 /* System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.Xml.XmlQualifiedName,System.Xml.XPath.XPathResultType[]) */, L_6, L_7, L_8);
		V_1 = L_9;
		goto IL_0058;
	}

IL_003a:
	{
		XsltContext_t4010601445 * L_10 = V_2;
		XmlQualifiedName_t176365656 * L_11 = __this->get__name_0();
		NullCheck(L_11);
		String_t* L_12 = XmlQualifiedName_get_Namespace_m2987642414(L_11, /*hidden argument*/NULL);
		XmlQualifiedName_t176365656 * L_13 = __this->get__name_0();
		NullCheck(L_13);
		String_t* L_14 = XmlQualifiedName_get_Name_m607016698(L_13, /*hidden argument*/NULL);
		XPathResultTypeU5BU5D_t1864207532* L_15 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_16 = VirtFuncInvoker3< Il2CppObject *, String_t*, String_t*, XPathResultTypeU5BU5D_t1864207532* >::Invoke(15 /* System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.String,System.String,System.Xml.XPath.XPathResultType[]) */, L_10, L_12, L_14, L_15);
		V_1 = L_16;
	}

IL_0058:
	{
		Il2CppObject * L_17 = V_1;
		if (L_17)
		{
			goto IL_007e;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_18 = __this->get__name_0();
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral4154394440, L_19, _stringLiteral2113392597, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_21 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_007e:
	{
		ArrayList_t2121638921 * L_22 = __this->get__args_2();
		NullCheck(L_22);
		int32_t L_23 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_22);
		V_3 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_23));
		Il2CppObject * L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Xml.Xsl.IXsltContextFunction::get_Maxargs() */, IXsltContextFunction_t1171511540_il2cpp_TypeInfo_var, L_24);
		if (!L_25)
		{
			goto IL_0119;
		}
	}
	{
		Il2CppObject * L_26 = V_1;
		NullCheck(L_26);
		XPathResultTypeU5BU5D_t1864207532* L_27 = InterfaceFuncInvoker0< XPathResultTypeU5BU5D_t1864207532* >::Invoke(0 /* System.Xml.XPath.XPathResultType[] System.Xml.Xsl.IXsltContextFunction::get_ArgTypes() */, IXsltContextFunction_t1171511540_il2cpp_TypeInfo_var, L_26);
		V_4 = L_27;
		V_5 = 0;
		goto IL_0107;
	}

IL_00aa:
	{
		XPathResultTypeU5BU5D_t1864207532* L_28 = V_4;
		if (L_28)
		{
			goto IL_00b9;
		}
	}
	{
		V_6 = 5;
		goto IL_00db;
	}

IL_00b9:
	{
		int32_t L_29 = V_5;
		XPathResultTypeU5BU5D_t1864207532* L_30 = V_4;
		NullCheck(L_30);
		if ((((int32_t)L_29) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_00d0;
		}
	}
	{
		XPathResultTypeU5BU5D_t1864207532* L_31 = V_4;
		int32_t L_32 = V_5;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		V_6 = ((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33)));
		goto IL_00db;
	}

IL_00d0:
	{
		XPathResultTypeU5BU5D_t1864207532* L_34 = V_4;
		XPathResultTypeU5BU5D_t1864207532* L_35 = V_4;
		NullCheck(L_35);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length))))-(int32_t)1)));
		int32_t L_36 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length))))-(int32_t)1));
		V_6 = ((L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36)));
	}

IL_00db:
	{
		ArrayList_t2121638921 * L_37 = __this->get__args_2();
		int32_t L_38 = V_5;
		NullCheck(L_37);
		Il2CppObject * L_39 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_37, L_38);
		V_7 = ((Expression_t4217024437 *)CastclassClass(L_39, Expression_t4217024437_il2cpp_TypeInfo_var));
		Expression_t4217024437 * L_40 = V_7;
		BaseIterator_t3696600956 * L_41 = ___iter0;
		int32_t L_42 = V_6;
		NullCheck(L_40);
		Il2CppObject * L_43 = Expression_EvaluateAs_m2173512581(L_40, L_41, L_42, /*hidden argument*/NULL);
		V_8 = L_43;
		ObjectU5BU5D_t11523773* L_44 = V_3;
		int32_t L_45 = V_5;
		Il2CppObject * L_46 = V_8;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		ArrayElementTypeCheck (L_44, L_46);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(L_45), (Il2CppObject *)L_46);
		int32_t L_47 = V_5;
		V_5 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0107:
	{
		int32_t L_48 = V_5;
		ArrayList_t2121638921 * L_49 = __this->get__args_2();
		NullCheck(L_49);
		int32_t L_50 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_49);
		if ((((int32_t)L_48) < ((int32_t)L_50)))
		{
			goto IL_00aa;
		}
	}

IL_0119:
	{
		Il2CppObject * L_51 = V_1;
		XsltContext_t4010601445 * L_52 = V_2;
		ObjectU5BU5D_t11523773* L_53 = V_3;
		BaseIterator_t3696600956 * L_54 = ___iter0;
		NullCheck(L_54);
		XPathNavigator_t1624538935 * L_55 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_54);
		NullCheck(L_51);
		Il2CppObject * L_56 = InterfaceFuncInvoker3< Il2CppObject *, XsltContext_t4010601445 *, ObjectU5BU5D_t11523773*, XPathNavigator_t1624538935 * >::Invoke(2 /* System.Object System.Xml.Xsl.IXsltContextFunction::Invoke(System.Xml.Xsl.XsltContext,System.Object[],System.Xml.XPath.XPathNavigator) */, IXsltContextFunction_t1171511540_il2cpp_TypeInfo_var, L_51, L_52, L_53, L_55);
		return L_56;
	}
}
// System.Boolean System.Xml.XPath.ExprFunctionCall::get_Peer()
extern "C"  bool ExprFunctionCall_get_Peer_m569283768 (ExprFunctionCall_t3745907208 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.ExprGE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprGE__ctor_m4218213977 (ExprGE_t1813726896 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		RelationalExpr__ctor_m506647938(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprGE::get_Operator()
extern Il2CppCodeGenString* _stringLiteral1983;
extern const uint32_t ExprGE_get_Operator_m3778930663_MetadataUsageId;
extern "C"  String_t* ExprGE_get_Operator_m3778930663 (ExprGE_t1813726896 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprGE_get_Operator_m3778930663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1983;
	}
}
// System.Boolean System.Xml.XPath.ExprGE::Compare(System.Double,System.Double)
extern "C"  bool ExprGE_Compare_m601870988 (ExprGE_t1813726896 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	{
		double L_0 = ___arg10;
		double L_1 = ___arg21;
		return (bool)((((int32_t)((!(((double)L_0) >= ((double)L_1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprGT::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprGT__ctor_m2309283432 (ExprGT_t1813726911 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		RelationalExpr__ctor_m506647938(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprGT::get_Operator()
extern Il2CppCodeGenString* _stringLiteral62;
extern const uint32_t ExprGT_get_Operator_m2603739638_MetadataUsageId;
extern "C"  String_t* ExprGT_get_Operator_m2603739638 (ExprGT_t1813726911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprGT_get_Operator_m2603739638_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral62;
	}
}
// System.Boolean System.Xml.XPath.ExprGT::Compare(System.Double,System.Double)
extern "C"  bool ExprGT_Compare_m3941486427 (ExprGT_t1813726911 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	{
		double L_0 = ___arg10;
		double L_1 = ___arg21;
		return (bool)((((double)L_0) > ((double)L_1))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprLE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprLE__ctor_m240811764 (ExprLE_t1813727051 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		RelationalExpr__ctor_m506647938(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprLE::get_Operator()
extern Il2CppCodeGenString* _stringLiteral1922;
extern const uint32_t ExprLE_get_Operator_m3088536194_MetadataUsageId;
extern "C"  String_t* ExprLE_get_Operator_m3088536194 (ExprLE_t1813727051 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprLE_get_Operator_m3088536194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1922;
	}
}
// System.Boolean System.Xml.XPath.ExprLE::Compare(System.Double,System.Double)
extern "C"  bool ExprLE_Compare_m3614803687 (ExprLE_t1813727051 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	{
		double L_0 = ___arg10;
		double L_1 = ___arg21;
		return (bool)((((int32_t)((!(((double)L_0) <= ((double)L_1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprLiteral::.ctor(System.String)
extern "C"  void ExprLiteral__ctor_m1598703472 (ExprLiteral_t569227735 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set__value_0(L_0);
		return;
	}
}
// System.String System.Xml.XPath.ExprLiteral::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t ExprLiteral_ToString_m242797249_MetadataUsageId;
extern "C"  String_t* ExprLiteral_ToString_m242797249 (ExprLiteral_t569227735 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprLiteral_ToString_m242797249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get__value_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral39, L_0, _stringLiteral39, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprLiteral::get_ReturnType()
extern "C"  int32_t ExprLiteral_get_ReturnType_m2223182313 (ExprLiteral_t569227735 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.ExprLiteral::get_Peer()
extern "C"  bool ExprLiteral_get_Peer_m3129008549 (ExprLiteral_t569227735 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.ExprLiteral::get_HasStaticValue()
extern "C"  bool ExprLiteral_get_HasStaticValue_m2424400044 (ExprLiteral_t569227735 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.String System.Xml.XPath.ExprLiteral::get_StaticValueAsString()
extern "C"  String_t* ExprLiteral_get_StaticValueAsString_m495911466 (ExprLiteral_t569227735 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__value_0();
		return L_0;
	}
}
// System.Object System.Xml.XPath.ExprLiteral::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * ExprLiteral_Evaluate_m876214316 (ExprLiteral_t569227735 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__value_0();
		return L_0;
	}
}
// System.String System.Xml.XPath.ExprLiteral::EvaluateString(System.Xml.XPath.BaseIterator)
extern "C"  String_t* ExprLiteral_EvaluateString_m2275560173 (ExprLiteral_t569227735 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__value_0();
		return L_0;
	}
}
// System.Void System.Xml.XPath.ExprLT::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprLT__ctor_m2626848515 (ExprLT_t1813727066 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		RelationalExpr__ctor_m506647938(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprLT::get_Operator()
extern Il2CppCodeGenString* _stringLiteral60;
extern const uint32_t ExprLT_get_Operator_m1913345169_MetadataUsageId;
extern "C"  String_t* ExprLT_get_Operator_m1913345169 (ExprLT_t1813727066 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprLT_get_Operator_m1913345169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral60;
	}
}
// System.Boolean System.Xml.XPath.ExprLT::Compare(System.Double,System.Double)
extern "C"  bool ExprLT_Compare_m2659451830 (ExprLT_t1813727066 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	{
		double L_0 = ___arg10;
		double L_1 = ___arg21;
		return (bool)((((double)L_0) < ((double)L_1))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprMINUS::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprMINUS__ctor_m1636365327 (ExprMINUS_t2200832088 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprMINUS::get_Operator()
extern Il2CppCodeGenString* _stringLiteral45;
extern const uint32_t ExprMINUS_get_Operator_m3066304643_MetadataUsageId;
extern "C"  String_t* ExprMINUS_get_Operator_m3066304643 (ExprMINUS_t2200832088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprMINUS_get_Operator_m3066304643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral45;
	}
}
// System.Double System.Xml.XPath.ExprMINUS::get_StaticValueAsNumber()
extern "C"  double ExprMINUS_get_StaticValueAsNumber_m3648238977 (ExprMINUS_t2200832088 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t4217024437 * L_1 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = ((double)((double)L_2-(double)L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprMINUS::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprMINUS_EvaluateNumber_m1137133908 (ExprMINUS_t2200832088 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return ((double)((double)L_2-(double)L_5));
	}
}
// System.Void System.Xml.XPath.ExprMOD::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprMOD__ctor_m1750329121 (ExprMOD_t79694250 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprMOD::get_Operator()
extern Il2CppCodeGenString* _stringLiteral37;
extern const uint32_t ExprMOD_get_Operator_m3646624853_MetadataUsageId;
extern "C"  String_t* ExprMOD_get_Operator_m3646624853 (ExprMOD_t79694250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprMOD_get_Operator_m3646624853_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral37;
	}
}
// System.Double System.Xml.XPath.ExprMOD::get_StaticValueAsNumber()
extern "C"  double ExprMOD_get_StaticValueAsNumber_m3539694191 (ExprMOD_t79694250 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t4217024437 * L_1 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = (fmod(L_2, L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprMOD::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprMOD_EvaluateNumber_m1045250882 (ExprMOD_t79694250 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return (fmod(L_2, L_5));
	}
}
// System.Void System.Xml.XPath.ExprMULT::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprMULT__ctor_m2682740523 (ExprMULT_t2159257026 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprMULT::get_Operator()
extern Il2CppCodeGenString* _stringLiteral42;
extern const uint32_t ExprMULT_get_Operator_m1717408121_MetadataUsageId;
extern "C"  String_t* ExprMULT_get_Operator_m1717408121 (ExprMULT_t2159257026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprMULT_get_Operator_m1717408121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral42;
	}
}
// System.Double System.Xml.XPath.ExprMULT::get_StaticValueAsNumber()
extern "C"  double ExprMULT_get_StaticValueAsNumber_m3551270731 (ExprMULT_t2159257026 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t4217024437 * L_1 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = ((double)((double)L_2*(double)L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprMULT::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprMULT_EvaluateNumber_m3619086878 (ExprMULT_t2159257026 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return ((double)((double)L_2*(double)L_5));
	}
}
// System.Void System.Xml.XPath.ExprNE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprNE__ctor_m3803811634 (ExprNE_t1813727113 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		EqualityExpr__ctor_m2210778824(__this, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprNE::get_Operator()
extern Il2CppCodeGenString* _stringLiteral1084;
extern const uint32_t ExprNE_get_Operator_m1094391488_MetadataUsageId;
extern "C"  String_t* ExprNE_get_Operator_m1094391488 (ExprNE_t1813727113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNE_get_Operator_m1094391488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1084;
	}
}
// System.Void System.Xml.XPath.ExprNEG::.ctor(System.Xml.XPath.Expression)
extern "C"  void ExprNEG__ctor_m3726946082 (ExprNEG_t79694904 * __this, Expression_t4217024437 * ___expr0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Expression_t4217024437 * L_0 = ___expr0;
		__this->set__expr_0(L_0);
		return;
	}
}
// System.String System.Xml.XPath.ExprNEG::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1427;
extern const uint32_t ExprNEG_ToString_m1964650658_MetadataUsageId;
extern "C"  String_t* ExprNEG_ToString_m1964650658 (ExprNEG_t79694904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNEG_ToString_m1964650658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1427, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprNEG::get_ReturnType()
extern "C"  int32_t ExprNEG_get_ReturnType_m3037696906 (ExprNEG_t79694904 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprNEG::Optimize()
extern Il2CppClass* ExprNumber_t3560215227_il2cpp_TypeInfo_var;
extern const uint32_t ExprNEG_Optimize_m3524037924_MetadataUsageId;
extern "C"  Expression_t4217024437 * ExprNEG_Optimize_m3524037924 (ExprNEG_t79694904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNEG_Optimize_m3524037924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ExprNEG_t79694904 * G_B3_0 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		Expression_t4217024437 * L_1 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set__expr_0(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprNEG::get_HasStaticValue() */, __this);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		G_B3_0 = __this;
		goto IL_002d;
	}

IL_0022:
	{
		double L_3 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.ExprNEG::get_StaticValueAsNumber() */, __this);
		ExprNumber_t3560215227 * L_4 = (ExprNumber_t3560215227 *)il2cpp_codegen_object_new(ExprNumber_t3560215227_il2cpp_TypeInfo_var);
		ExprNumber__ctor_m2500538694(L_4, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((ExprNEG_t79694904 *)(L_4));
	}

IL_002d:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprNEG::get_Peer()
extern "C"  bool ExprNEG_get_Peer_m269107718 (ExprNEG_t79694904 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Xml.XPath.ExprNEG::get_HasStaticValue()
extern "C"  bool ExprNEG_get_HasStaticValue_m2088403917 (ExprNEG_t79694904 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.ExprNEG::get_StaticValueAsNumber()
extern "C"  double ExprNEG_get_StaticValueAsNumber_m150751009 (ExprNEG_t79694904 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get__expr_0();
		NullCheck(L_2);
		double L_3 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_2);
		G_B3_0 = ((double)((double)(-1.0)*(double)L_3));
		goto IL_0033;
	}

IL_002a:
	{
		G_B3_0 = (0.0);
	}

IL_0033:
	{
		return G_B3_0;
	}
}
// System.Object System.Xml.XPath.ExprNEG::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t ExprNEG_Evaluate_m3230994667_MetadataUsageId;
extern "C"  Il2CppObject * ExprNEG_Evaluate_m3230994667 (ExprNEG_t79694904 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNEG_Evaluate_m3230994667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = ((-L_2));
		Il2CppObject * L_4 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.Double System.Xml.XPath.ExprNEG::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprNEG_EvaluateNumber_m3791047412 (ExprNEG_t79694904 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		return ((-L_2));
	}
}
// System.Void System.Xml.XPath.ExprNumber::.ctor(System.Double)
extern "C"  void ExprNumber__ctor_m2500538694 (ExprNumber_t3560215227 * __this, double ___value0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		double L_0 = ___value0;
		__this->set__value_0(L_0);
		return;
	}
}
// System.String System.Xml.XPath.ExprNumber::ToString()
extern "C"  String_t* ExprNumber_ToString_m3667120177 (ExprNumber_t3560215227 * __this, const MethodInfo* method)
{
	{
		double* L_0 = __this->get_address_of__value_0();
		String_t* L_1 = Double_ToString_m3380246633(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprNumber::get_ReturnType()
extern "C"  int32_t ExprNumber_get_ReturnType_m3482223799 (ExprNumber_t3560215227 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.ExprNumber::get_Peer()
extern "C"  bool ExprNumber_get_Peer_m3605392811 (ExprNumber_t3560215227 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.ExprNumber::get_HasStaticValue()
extern "C"  bool ExprNumber_get_HasStaticValue_m1751360818 (ExprNumber_t3560215227 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Double System.Xml.XPath.ExprNumber::get_StaticValueAsNumber()
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t ExprNumber_get_StaticValueAsNumber_m3840256050_MetadataUsageId;
extern "C"  double ExprNumber_get_StaticValueAsNumber_m3840256050 (ExprNumber_t3560215227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNumber_get_StaticValueAsNumber_m3840256050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = __this->get__value_0();
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_1);
		double L_3 = XPathFunctions_ToNumber_m1220011083(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Object System.Xml.XPath.ExprNumber::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t ExprNumber_Evaluate_m2340743584_MetadataUsageId;
extern "C"  Il2CppObject * ExprNumber_Evaluate_m2340743584 (ExprNumber_t3560215227 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNumber_Evaluate_m2340743584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = __this->get__value_0();
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Double System.Xml.XPath.ExprNumber::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprNumber_EvaluateNumber_m2056124805 (ExprNumber_t3560215227 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		double L_0 = __this->get__value_0();
		return L_0;
	}
}
// System.Void System.Xml.XPath.ExprNumeric::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprNumeric__ctor_m1682712108 (ExprNumeric_t2681320501 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		ExprBinary__ctor_m18595484(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprNumeric::get_ReturnType()
extern "C"  int32_t ExprNumeric_get_ReturnType_m1470918855 (ExprNumeric_t2681320501 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprNumeric::Optimize()
extern Il2CppClass* ExprNumber_t3560215227_il2cpp_TypeInfo_var;
extern const uint32_t ExprNumeric_Optimize_m465564705_MetadataUsageId;
extern "C"  Expression_t4217024437 * ExprNumeric_Optimize_m465564705 (ExprNumeric_t2681320501 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNumeric_Optimize_m465564705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ExprNumeric_t2681320501 * G_B3_0 = NULL;
	{
		ExprBinary_Optimize_m4062507817(__this, /*hidden argument*/NULL);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		G_B3_0 = __this;
		goto IL_0023;
	}

IL_0018:
	{
		double L_1 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, __this);
		ExprNumber_t3560215227 * L_2 = (ExprNumber_t3560215227 *)il2cpp_codegen_object_new(ExprNumber_t3560215227_il2cpp_TypeInfo_var);
		ExprNumber__ctor_m2500538694(L_2, L_1, /*hidden argument*/NULL);
		G_B3_0 = ((ExprNumeric_t2681320501 *)(L_2));
	}

IL_0023:
	{
		return G_B3_0;
	}
}
// System.Object System.Xml.XPath.ExprNumeric::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t ExprNumeric_Evaluate_m4274044814_MetadataUsageId;
extern "C"  Il2CppObject * ExprNumeric_Evaluate_m4274044814 (ExprNumeric_t2681320501 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNumeric_Evaluate_m4274044814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		double L_1 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, __this, L_0);
		double L_2 = L_1;
		Il2CppObject * L_3 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Void System.Xml.XPath.ExprOR::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprOR__ctor_m1067593566 (ExprOR_t1813727157 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		ExprBoolean__ctor_m1871519559(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprOR::get_Operator()
extern Il2CppCodeGenString* _stringLiteral3555;
extern const uint32_t ExprOR_get_Operator_m510476012_MetadataUsageId;
extern "C"  String_t* ExprOR_get_Operator_m510476012 (ExprOR_t1813727157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprOR_get_Operator_m510476012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3555;
	}
}
// System.Boolean System.Xml.XPath.ExprOR::get_StaticValueAsBoolean()
extern "C"  bool ExprOR_get_StaticValueAsBoolean_m1871030710 (ExprOR_t1813727157 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Expression_t4217024437 * L_1 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_1);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_3);
		G_B4_0 = ((int32_t)(L_4));
		goto IL_0029;
	}

IL_0028:
	{
		G_B4_0 = 1;
	}

IL_0029:
	{
		G_B6_0 = G_B4_0;
		goto IL_002f;
	}

IL_002e:
	{
		G_B6_0 = 0;
	}

IL_002f:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean System.Xml.XPath.ExprOR::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool ExprOR_EvaluateBoolean_m2492968597 (ExprOR_t1813727157 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (bool)1;
	}

IL_0013:
	{
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return L_5;
	}
}
// System.Void System.Xml.XPath.ExprParens::.ctor(System.Xml.XPath.Expression)
extern "C"  void ExprParens__ctor_m2196271671 (ExprParens_t3599155227 * __this, Expression_t4217024437 * ___expr0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Expression_t4217024437 * L_0 = ___expr0;
		__this->set__expr_0(L_0);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprParens::Optimize()
extern "C"  Expression_t4217024437 * ExprParens_Optimize_m4227609553 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		return __this;
	}
}
// System.Boolean System.Xml.XPath.ExprParens::get_HasStaticValue()
extern "C"  bool ExprParens_get_HasStaticValue_m2344585874 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.ExprParens::get_StaticValue()
extern "C"  Il2CppObject * ExprParens_get_StaticValue_m15807529 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* System.Object System.Xml.XPath.Expression::get_StaticValue() */, L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.ExprParens::get_StaticValueAsString()
extern "C"  String_t* ExprParens_get_StaticValueAsString_m3898008922 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.Expression::get_StaticValueAsString() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.ExprParens::get_StaticValueAsNumber()
extern "C"  double ExprParens_get_StaticValueAsNumber_m201465554 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		double L_1 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Xml.XPath.ExprParens::get_StaticValueAsBoolean()
extern "C"  bool ExprParens_get_StaticValueAsBoolean_m1863365788 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.ExprParens::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t ExprParens_ToString_m1798106513_MetadataUsageId;
extern "C"  String_t* ExprParens_ToString_m1798106513 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprParens_ToString_m1798106513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral40, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprParens::get_ReturnType()
extern "C"  int32_t ExprParens_get_ReturnType_m3027507735 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.ExprParens::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperIterator_t2702681854_il2cpp_TypeInfo_var;
extern Il2CppClass* ParensIterator_t3967469908_il2cpp_TypeInfo_var;
extern const uint32_t ExprParens_Evaluate_m1572685888_MetadataUsageId;
extern "C"  Il2CppObject * ExprParens_Evaluate_m1572685888 (ExprParens_t3599155227 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprParens_Evaluate_m1572685888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	XPathNodeIterator_t2394191562 * V_1 = NULL;
	BaseIterator_t3696600956 * V_2 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t3696600956 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		V_1 = ((XPathNodeIterator_t2394191562 *)IsInstClass(L_3, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var));
		XPathNodeIterator_t2394191562 * L_4 = V_1;
		V_2 = ((BaseIterator_t3696600956 *)IsInstClass(L_4, BaseIterator_t3696600956_il2cpp_TypeInfo_var));
		BaseIterator_t3696600956 * L_5 = V_2;
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		XPathNodeIterator_t2394191562 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		XPathNodeIterator_t2394191562 * L_7 = V_1;
		BaseIterator_t3696600956 * L_8 = ___iter0;
		NullCheck(L_8);
		Il2CppObject * L_9 = BaseIterator_get_NamespaceManager_m3653702256(L_8, /*hidden argument*/NULL);
		WrapperIterator_t2702681854 * L_10 = (WrapperIterator_t2702681854 *)il2cpp_codegen_object_new(WrapperIterator_t2702681854_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m802118820(L_10, L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
	}

IL_0034:
	{
		BaseIterator_t3696600956 * L_11 = V_2;
		if (!L_11)
		{
			goto IL_0041;
		}
	}
	{
		BaseIterator_t3696600956 * L_12 = V_2;
		ParensIterator_t3967469908 * L_13 = (ParensIterator_t3967469908 *)il2cpp_codegen_object_new(ParensIterator_t3967469908_il2cpp_TypeInfo_var);
		ParensIterator__ctor_m938283977(L_13, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0041:
	{
		Il2CppObject * L_14 = V_0;
		return L_14;
	}
}
// System.Boolean System.Xml.XPath.ExprParens::get_Peer()
extern "C"  bool ExprParens_get_Peer_m1736379147 (ExprParens_t3599155227 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.ExprPLUS::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprPLUS__ctor_m693128085 (ExprPLUS_t2159338028 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprPLUS::get_Operator()
extern Il2CppCodeGenString* _stringLiteral43;
extern const uint32_t ExprPLUS_get_Operator_m2059188707_MetadataUsageId;
extern "C"  String_t* ExprPLUS_get_Operator_m2059188707 (ExprPLUS_t2159338028 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprPLUS_get_Operator_m2059188707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral43;
	}
}
// System.Double System.Xml.XPath.ExprPLUS::get_StaticValueAsNumber()
extern "C"  double ExprPLUS_get_StaticValueAsNumber_m2404542625 (ExprPLUS_t2159338028 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t4217024437 * L_1 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = ((double)((double)L_2+(double)L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprPLUS::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprPLUS_EvaluateNumber_m3849137268 (ExprPLUS_t2159338028 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return ((double)((double)L_2+(double)L_5));
	}
}
// System.Void System.Xml.XPath.ExprRoot::.ctor()
extern "C"  void ExprRoot__ctor_m3406596739 (ExprRoot_t2159432084 * __this, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprRoot::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ExprRoot_ToString_m808174346_MetadataUsageId;
extern "C"  String_t* ExprRoot_ToString_m808174346 (ExprRoot_t2159432084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprRoot_ToString_m808174346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// System.Object System.Xml.XPath.ExprRoot::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern Il2CppClass* SelfIterator_t2566309111_il2cpp_TypeInfo_var;
extern const uint32_t ExprRoot_Evaluate_m2052910567_MetadataUsageId;
extern "C"  Il2CppObject * ExprRoot_Evaluate_m2052910567 (ExprRoot_t2159432084 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprRoot_Evaluate_m2052910567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1624538935 * V_0 = NULL;
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_0);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		BaseIterator_t3696600956 * L_2 = ___iter0;
		NullCheck(L_2);
		XPathNodeIterator_t2394191562 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		___iter0 = ((BaseIterator_t3696600956 *)CastclassClass(L_3, BaseIterator_t3696600956_il2cpp_TypeInfo_var));
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_4);
	}

IL_001f:
	{
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		XPathNavigator_t1624538935 * L_7 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_6);
		V_0 = L_7;
		XPathNavigator_t1624538935 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_8);
		XPathNavigator_t1624538935 * L_9 = V_0;
		BaseIterator_t3696600956 * L_10 = ___iter0;
		NullCheck(L_10);
		Il2CppObject * L_11 = BaseIterator_get_NamespaceManager_m3653702256(L_10, /*hidden argument*/NULL);
		SelfIterator_t2566309111 * L_12 = (SelfIterator_t2566309111 *)il2cpp_codegen_object_new(SelfIterator_t2566309111_il2cpp_TypeInfo_var);
		SelfIterator__ctor_m4150585400(L_12, L_9, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean System.Xml.XPath.ExprRoot::get_Peer()
extern "C"  bool ExprRoot_get_Peer_m290743108 (ExprRoot_t2159432084 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.ExprRoot::get_Subtree()
extern "C"  bool ExprRoot_get_Subtree_m1326600798 (ExprRoot_t2159432084 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.ExprSLASH::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.NodeSet)
extern "C"  void ExprSLASH__ctor_m1380016254 (ExprSLASH_t2206450021 * __this, Expression_t4217024437 * ___left0, NodeSet_t3503134685 * ___right1, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		Expression_t4217024437 * L_0 = ___left0;
		__this->set_left_0(L_0);
		NodeSet_t3503134685 * L_1 = ___right1;
		__this->set_right_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprSLASH::Optimize()
extern Il2CppClass* NodeSet_t3503134685_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH_Optimize_m3034839057_MetadataUsageId;
extern "C"  Expression_t4217024437 * ExprSLASH_Optimize_m3034839057 (ExprSLASH_t2206450021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH_Optimize_m3034839057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		Expression_t4217024437 * L_1 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set_left_0(L_1);
		NodeSet_t3503134685 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set_right_1(((NodeSet_t3503134685 *)CastclassClass(L_3, NodeSet_t3503134685_il2cpp_TypeInfo_var)));
		return __this;
	}
}
// System.String System.Xml.XPath.ExprSLASH::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t ExprSLASH_ToString_m2156751823_MetadataUsageId;
extern "C"  String_t* ExprSLASH_ToString_m2156751823 (ExprSLASH_t2206450021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH_ToString_m2156751823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		NodeSet_t3503134685 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral47, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object System.Xml.XPath.ExprSLASH::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* SimpleSlashIterator_t2349635926_il2cpp_TypeInfo_var;
extern Il2CppClass* SlashIterator_t818098664_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedIterator_t269320040_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH_Evaluate_m2117643614_MetadataUsageId;
extern "C"  Il2CppObject * ExprSLASH_Evaluate_m2117643614 (ExprSLASH_t2206450021 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH_Evaluate_m2117643614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t3696600956 * V_0 = NULL;
	BaseIterator_t3696600956 * V_1 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t3696600956 * L_2 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t4217024437 * L_3 = __this->get_left_0();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_3);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		NodeSet_t3503134685 * L_5 = __this->get_right_1();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_5);
		if (!L_6)
		{
			goto IL_003a;
		}
	}
	{
		BaseIterator_t3696600956 * L_7 = V_0;
		NodeSet_t3503134685 * L_8 = __this->get_right_1();
		SimpleSlashIterator_t2349635926 * L_9 = (SimpleSlashIterator_t2349635926 *)il2cpp_codegen_object_new(SimpleSlashIterator_t2349635926_il2cpp_TypeInfo_var);
		SimpleSlashIterator__ctor_m3208158132(L_9, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_003a:
	{
		BaseIterator_t3696600956 * L_10 = V_0;
		NodeSet_t3503134685 * L_11 = __this->get_right_1();
		SlashIterator_t818098664 * L_12 = (SlashIterator_t818098664 *)il2cpp_codegen_object_new(SlashIterator_t818098664_il2cpp_TypeInfo_var);
		SlashIterator__ctor_m2348632994(L_12, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		BaseIterator_t3696600956 * L_13 = V_1;
		SortedIterator_t269320040 * L_14 = (SortedIterator_t269320040 *)il2cpp_codegen_object_new(SortedIterator_t269320040_il2cpp_TypeInfo_var);
		SortedIterator__ctor_m2178587357(L_14, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH::get_RequireSorting()
extern "C"  bool ExprSLASH_get_RequireSorting_m2957526960 (ExprSLASH_t2206450021 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_0);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		NodeSet_t3503134685 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH::get_Peer()
extern "C"  bool ExprSLASH_get_Peer_m3801044979 (ExprSLASH_t2206450021 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		NodeSet_t3503134685 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH::get_Subtree()
extern Il2CppClass* NodeSet_t3503134685_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH_get_Subtree_m2865916751_MetadataUsageId;
extern "C"  bool ExprSLASH_get_Subtree_m2865916751 (ExprSLASH_t2206450021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH_get_Subtree_m2865916751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t3503134685 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		V_0 = ((NodeSet_t3503134685 *)IsInstClass(L_0, NodeSet_t3503134685_il2cpp_TypeInfo_var));
		NodeSet_t3503134685 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		NodeSet_t3503134685 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_2);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		NodeSet_t3503134685 * L_4 = __this->get_right_1();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_4);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 0;
	}

IL_002b:
	{
		return (bool)G_B4_0;
	}
}
// System.Void System.Xml.XPath.ExprSLASH2::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.NodeSet)
extern "C"  void ExprSLASH2__ctor_m1370991914 (ExprSLASH2_t3664170439 * __this, Expression_t4217024437 * ___left0, NodeSet_t3503134685 * ___right1, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		Expression_t4217024437 * L_0 = ___left0;
		__this->set_left_0(L_0);
		NodeSet_t3503134685 * L_1 = ___right1;
		__this->set_right_1(L_1);
		return;
	}
}
// System.Void System.Xml.XPath.ExprSLASH2::.cctor()
extern Il2CppClass* NodeTypeTest_t2651955243_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprSLASH2_t3664170439_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH2__cctor_m3450258365_MetadataUsageId;
extern "C"  void ExprSLASH2__cctor_m3450258365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2__cctor_m3450258365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NodeTypeTest_t2651955243 * L_0 = (NodeTypeTest_t2651955243 *)il2cpp_codegen_object_new(NodeTypeTest_t2651955243_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m2014120890(L_0, 5, ((int32_t)9), /*hidden argument*/NULL);
		((ExprSLASH2_t3664170439_StaticFields*)ExprSLASH2_t3664170439_il2cpp_TypeInfo_var->static_fields)->set_DescendantOrSelfStar_2(L_0);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprSLASH2::Optimize()
extern Il2CppClass* NodeSet_t3503134685_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeTest_t911751889_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeNameTest_t873888508_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprSLASH_t2206450021_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeTypeTest_t2651955243_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH2_Optimize_m389619581_MetadataUsageId;
extern "C"  Expression_t4217024437 * ExprSLASH2_Optimize_m389619581 (ExprSLASH2_t3664170439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2_Optimize_m389619581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeTest_t911751889 * V_0 = NULL;
	NodeNameTest_t873888508 * V_1 = NULL;
	NodeTypeTest_t2651955243 * V_2 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		Expression_t4217024437 * L_1 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set_left_0(L_1);
		NodeSet_t3503134685 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set_right_1(((NodeSet_t3503134685 *)CastclassClass(L_3, NodeSet_t3503134685_il2cpp_TypeInfo_var)));
		NodeSet_t3503134685 * L_4 = __this->get_right_1();
		V_0 = ((NodeTest_t911751889 *)IsInstClass(L_4, NodeTest_t911751889_il2cpp_TypeInfo_var));
		NodeTest_t911751889 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_008a;
		}
	}
	{
		NodeTest_t911751889 * L_6 = V_0;
		NullCheck(L_6);
		AxisSpecifier_t4216515578 * L_7 = NodeTest_get_Axis_m243839149(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = AxisSpecifier_get_Axis_m269195078(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)3))))
		{
			goto IL_008a;
		}
	}
	{
		NodeTest_t911751889 * L_9 = V_0;
		V_1 = ((NodeNameTest_t873888508 *)IsInstClass(L_9, NodeNameTest_t873888508_il2cpp_TypeInfo_var));
		NodeNameTest_t873888508 * L_10 = V_1;
		if (!L_10)
		{
			goto IL_006a;
		}
	}
	{
		Expression_t4217024437 * L_11 = __this->get_left_0();
		NodeNameTest_t873888508 * L_12 = V_1;
		NodeNameTest_t873888508 * L_13 = (NodeNameTest_t873888508 *)il2cpp_codegen_object_new(NodeNameTest_t873888508_il2cpp_TypeInfo_var);
		NodeNameTest__ctor_m3377740561(L_13, L_12, 4, /*hidden argument*/NULL);
		ExprSLASH_t2206450021 * L_14 = (ExprSLASH_t2206450021 *)il2cpp_codegen_object_new(ExprSLASH_t2206450021_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_14, L_11, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_006a:
	{
		NodeTest_t911751889 * L_15 = V_0;
		V_2 = ((NodeTypeTest_t2651955243 *)IsInstClass(L_15, NodeTypeTest_t2651955243_il2cpp_TypeInfo_var));
		NodeTypeTest_t2651955243 * L_16 = V_2;
		if (!L_16)
		{
			goto IL_008a;
		}
	}
	{
		Expression_t4217024437 * L_17 = __this->get_left_0();
		NodeTypeTest_t2651955243 * L_18 = V_2;
		NodeTypeTest_t2651955243 * L_19 = (NodeTypeTest_t2651955243 *)il2cpp_codegen_object_new(NodeTypeTest_t2651955243_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m1445907889(L_19, L_18, 4, /*hidden argument*/NULL);
		ExprSLASH_t2206450021 * L_20 = (ExprSLASH_t2206450021 *)il2cpp_codegen_object_new(ExprSLASH_t2206450021_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_20, L_17, L_19, /*hidden argument*/NULL);
		return L_20;
	}

IL_008a:
	{
		return __this;
	}
}
// System.String System.Xml.XPath.ExprSLASH2::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1504;
extern const uint32_t ExprSLASH2_ToString_m2255083837_MetadataUsageId;
extern "C"  String_t* ExprSLASH2_ToString_m2255083837 (ExprSLASH2_t3664170439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2_ToString_m2255083837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		NodeSet_t3503134685 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral1504, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object System.Xml.XPath.ExprSLASH2::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* ExprSLASH2_t3664170439_il2cpp_TypeInfo_var;
extern Il2CppClass* SimpleSlashIterator_t2349635926_il2cpp_TypeInfo_var;
extern Il2CppClass* SlashIterator_t818098664_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedIterator_t269320040_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH2_Evaluate_m1368362004_MetadataUsageId;
extern "C"  Il2CppObject * ExprSLASH2_Evaluate_m1368362004 (ExprSLASH2_t3664170439 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2_Evaluate_m1368362004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t3696600956 * V_0 = NULL;
	BaseIterator_t3696600956 * V_1 = NULL;
	SlashIterator_t818098664 * V_2 = NULL;
	SortedIterator_t269320040 * G_B6_0 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t3696600956 * L_2 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t4217024437 * L_3 = __this->get_left_0();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_3);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Expression_t4217024437 * L_5 = __this->get_left_0();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_5);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		BaseIterator_t3696600956 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExprSLASH2_t3664170439_il2cpp_TypeInfo_var);
		NodeTest_t911751889 * L_8 = ((ExprSLASH2_t3664170439_StaticFields*)ExprSLASH2_t3664170439_il2cpp_TypeInfo_var->static_fields)->get_DescendantOrSelfStar_2();
		SimpleSlashIterator_t2349635926 * L_9 = (SimpleSlashIterator_t2349635926 *)il2cpp_codegen_object_new(SimpleSlashIterator_t2349635926_il2cpp_TypeInfo_var);
		SimpleSlashIterator__ctor_m3208158132(L_9, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0067;
	}

IL_003e:
	{
		BaseIterator_t3696600956 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExprSLASH2_t3664170439_il2cpp_TypeInfo_var);
		NodeTest_t911751889 * L_11 = ((ExprSLASH2_t3664170439_StaticFields*)ExprSLASH2_t3664170439_il2cpp_TypeInfo_var->static_fields)->get_DescendantOrSelfStar_2();
		SlashIterator_t818098664 * L_12 = (SlashIterator_t818098664 *)il2cpp_codegen_object_new(SlashIterator_t818098664_il2cpp_TypeInfo_var);
		SlashIterator__ctor_m2348632994(L_12, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Expression_t4217024437 * L_13 = __this->get_left_0();
		NullCheck(L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_13);
		if (!L_14)
		{
			goto IL_0065;
		}
	}
	{
		BaseIterator_t3696600956 * L_15 = V_1;
		SortedIterator_t269320040 * L_16 = (SortedIterator_t269320040 *)il2cpp_codegen_object_new(SortedIterator_t269320040_il2cpp_TypeInfo_var);
		SortedIterator__ctor_m2178587357(L_16, L_15, /*hidden argument*/NULL);
		G_B6_0 = L_16;
		goto IL_0066;
	}

IL_0065:
	{
		BaseIterator_t3696600956 * L_17 = V_1;
		G_B6_0 = ((SortedIterator_t269320040 *)(L_17));
	}

IL_0066:
	{
		V_0 = G_B6_0;
	}

IL_0067:
	{
		BaseIterator_t3696600956 * L_18 = V_0;
		NodeSet_t3503134685 * L_19 = __this->get_right_1();
		SlashIterator_t818098664 * L_20 = (SlashIterator_t818098664 *)il2cpp_codegen_object_new(SlashIterator_t818098664_il2cpp_TypeInfo_var);
		SlashIterator__ctor_m2348632994(L_20, L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		SlashIterator_t818098664 * L_21 = V_2;
		SortedIterator_t269320040 * L_22 = (SortedIterator_t269320040 *)il2cpp_codegen_object_new(SortedIterator_t269320040_il2cpp_TypeInfo_var);
		SortedIterator__ctor_m2178587357(L_22, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH2::get_RequireSorting()
extern "C"  bool ExprSLASH2_get_RequireSorting_m786448756 (ExprSLASH2_t3664170439 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_0);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		NodeSet_t3503134685 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH2::get_Peer()
extern "C"  bool ExprSLASH2_get_Peer_m2193356471 (ExprSLASH2_t3664170439 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH2::get_Subtree()
extern Il2CppClass* NodeSet_t3503134685_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH2_get_Subtree_m1397892619_MetadataUsageId;
extern "C"  bool ExprSLASH2_get_Subtree_m1397892619 (ExprSLASH2_t3664170439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2_get_Subtree_m1397892619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t3503134685 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		V_0 = ((NodeSet_t3503134685 *)IsInstClass(L_0, NodeSet_t3503134685_il2cpp_TypeInfo_var));
		NodeSet_t3503134685 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		NodeSet_t3503134685 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_2);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		NodeSet_t3503134685 * L_4 = __this->get_right_1();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_4);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 0;
	}

IL_002b:
	{
		return (bool)G_B4_0;
	}
}
// System.Void System.Xml.XPath.ExprUNION::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprUNION__ctor_m2912512622 (ExprUNION_t2208364215 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		Expression_t4217024437 * L_0 = ___left0;
		__this->set_left_0(L_0);
		Expression_t4217024437 * L_1 = ___right1;
		__this->set_right_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprUNION::Optimize()
extern "C"  Expression_t4217024437 * ExprUNION_Optimize_m4157719139 (ExprUNION_t2208364215 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		Expression_t4217024437 * L_1 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set_left_0(L_1);
		Expression_t4217024437 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set_right_1(L_3);
		return __this;
	}
}
// System.String System.Xml.XPath.ExprUNION::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34628;
extern const uint32_t ExprUNION_ToString_m3279631905_MetadataUsageId;
extern "C"  String_t* ExprUNION_ToString_m3279631905 (ExprUNION_t2208364215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprUNION_ToString_m3279631905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Expression_t4217024437 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral34628, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object System.Xml.XPath.ExprUNION::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* UnionIterator_t1824978234_il2cpp_TypeInfo_var;
extern const uint32_t ExprUNION_Evaluate_m3576044620_MetadataUsageId;
extern "C"  Il2CppObject * ExprUNION_Evaluate_m3576044620 (ExprUNION_t2208364215 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprUNION_Evaluate_m3576044620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t3696600956 * V_0 = NULL;
	BaseIterator_t3696600956 * V_1 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t3696600956 * L_2 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t4217024437 * L_3 = __this->get_right_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		BaseIterator_t3696600956 * L_5 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		BaseIterator_t3696600956 * L_6 = ___iter0;
		BaseIterator_t3696600956 * L_7 = V_0;
		BaseIterator_t3696600956 * L_8 = V_1;
		UnionIterator_t1824978234 * L_9 = (UnionIterator_t1824978234 *)il2cpp_codegen_object_new(UnionIterator_t1824978234_il2cpp_TypeInfo_var);
		UnionIterator__ctor_m4002392457(L_9, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean System.Xml.XPath.ExprUNION::get_Peer()
extern "C"  bool ExprUNION_get_Peer_m628957765 (ExprUNION_t2208364215 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprUNION::get_Subtree()
extern Il2CppClass* NodeSet_t3503134685_il2cpp_TypeInfo_var;
extern const uint32_t ExprUNION_get_Subtree_m1086171069_MetadataUsageId;
extern "C"  bool ExprUNION_get_Subtree_m1086171069 (ExprUNION_t2208364215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprUNION_get_Subtree_m1086171069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t3503134685 * V_0 = NULL;
	NodeSet_t3503134685 * V_1 = NULL;
	int32_t G_B5_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_left_0();
		V_0 = ((NodeSet_t3503134685 *)IsInstClass(L_0, NodeSet_t3503134685_il2cpp_TypeInfo_var));
		Expression_t4217024437 * L_1 = __this->get_right_1();
		V_1 = ((NodeSet_t3503134685 *)IsInstClass(L_1, NodeSet_t3503134685_il2cpp_TypeInfo_var));
		NodeSet_t3503134685 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		NodeSet_t3503134685 * L_3 = V_1;
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		NodeSet_t3503134685 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_4);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		NodeSet_t3503134685 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_6);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0038;
	}

IL_0037:
	{
		G_B5_0 = 0;
	}

IL_0038:
	{
		return (bool)G_B5_0;
	}
}
// System.Void System.Xml.XPath.ExprVariable::.ctor(System.Xml.XmlQualifiedName,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var;
extern const uint32_t ExprVariable__ctor_m2228197543_MetadataUsageId;
extern "C"  void ExprVariable__ctor_m2228197543 (ExprVariable_t1838989486 * __this, XmlQualifiedName_t176365656 * ___name0, Il2CppObject * ___ctx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprVariable__ctor_m2228197543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___ctx1;
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Il2CppObject * L_1 = ___ctx1;
		XmlQualifiedName_t176365656 * L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_2);
		NullCheck(L_1);
		XmlQualifiedName_t176365656 * L_4 = InterfaceFuncInvoker1< XmlQualifiedName_t176365656 *, String_t* >::Invoke(2 /* System.Xml.XmlQualifiedName System.Xml.Xsl.IStaticXsltContext::LookupQName(System.String) */, IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var, L_1, L_3);
		___name0 = L_4;
		__this->set_resolvedName_1((bool)1);
	}

IL_0021:
	{
		XmlQualifiedName_t176365656 * L_5 = ___name0;
		__this->set__name_0(L_5);
		return;
	}
}
// System.String System.Xml.XPath.ExprVariable::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral36;
extern const uint32_t ExprVariable_ToString_m4055062564_MetadataUsageId;
extern "C"  String_t* ExprVariable_ToString_m4055062564 (ExprVariable_t1838989486 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprVariable_ToString_m4055062564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlQualifiedName_t176365656 * L_0 = __this->get__name_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral36, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprVariable::get_ReturnType()
extern "C"  int32_t ExprVariable_get_ReturnType_m158260906 (ExprVariable_t1838989486 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprVariable::GetReturnType(System.Xml.XPath.BaseIterator)
extern "C"  int32_t ExprVariable_GetReturnType_m2092369121 (ExprVariable_t1838989486 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Object System.Xml.XPath.ExprVariable::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XsltContext_t4010601445_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppClass* IXsltContextVariable_t2835953560_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperIterator_t2702681854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2209688223;
extern Il2CppCodeGenString* _stringLiteral4212489508;
extern Il2CppCodeGenString* _stringLiteral2113392597;
extern const uint32_t ExprVariable_Evaluate_m2065010189_MetadataUsageId;
extern "C"  Il2CppObject * ExprVariable_Evaluate_m2065010189 (ExprVariable_t1838989486 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprVariable_Evaluate_m2065010189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	XsltContext_t4010601445 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	XPathNodeIterator_t2394191562 * V_3 = NULL;
	Type_t * G_B7_0 = NULL;
	String_t* G_B7_1 = NULL;
	Type_t * G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	Type_t * G_B8_0 = NULL;
	Type_t * G_B8_1 = NULL;
	String_t* G_B8_2 = NULL;
	XPathNodeIterator_t2394191562 * G_B15_0 = NULL;
	{
		V_0 = (Il2CppObject *)NULL;
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		V_1 = ((XsltContext_t4010601445 *)IsInstClass(L_1, XsltContext_t4010601445_il2cpp_TypeInfo_var));
		XsltContext_t4010601445 * L_2 = V_1;
		if (!L_2)
		{
			goto IL_0058;
		}
	}
	{
		bool L_3 = __this->get_resolvedName_1();
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		XsltContext_t4010601445 * L_4 = V_1;
		XmlQualifiedName_t176365656 * L_5 = __this->get__name_0();
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, XmlQualifiedName_t176365656 * >::Invoke(17 /* System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.Xml.XmlQualifiedName) */, L_4, L_5);
		V_0 = L_6;
		goto IL_0053;
	}

IL_0031:
	{
		XsltContext_t4010601445 * L_7 = V_1;
		XmlQualifiedName_t176365656 * L_8 = __this->get__name_0();
		NullCheck(L_8);
		String_t* L_9 = XmlQualifiedName_get_Name_m607016698(L_8, /*hidden argument*/NULL);
		XmlQualifiedName_t176365656 * L_10 = __this->get__name_0();
		NullCheck(L_10);
		String_t* L_11 = XmlQualifiedName_get_Namespace_m2987642414(L_10, /*hidden argument*/NULL);
		XmlQualifiedName_t176365656 * L_12 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_12, L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		Il2CppObject * L_13 = VirtFuncInvoker1< Il2CppObject *, XmlQualifiedName_t176365656 * >::Invoke(17 /* System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.Xml.XmlQualifiedName) */, L_7, L_12);
		V_0 = L_13;
	}

IL_0053:
	{
		goto IL_008a;
	}

IL_0058:
	{
		BaseIterator_t3696600956 * L_14 = ___iter0;
		NullCheck(L_14);
		Type_t * L_15 = Object_GetType_m2022236990(L_14, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_16 = ___iter0;
		NullCheck(L_16);
		Il2CppObject * L_17 = BaseIterator_get_NamespaceManager_m3653702256(L_16, /*hidden argument*/NULL);
		G_B6_0 = L_15;
		G_B6_1 = _stringLiteral2209688223;
		if (!L_17)
		{
			G_B7_0 = L_15;
			G_B7_1 = _stringLiteral2209688223;
			goto IL_007e;
		}
	}
	{
		BaseIterator_t3696600956 * L_18 = ___iter0;
		NullCheck(L_18);
		Il2CppObject * L_19 = BaseIterator_get_NamespaceManager_m3653702256(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Type_t * L_20 = Object_GetType_m2022236990(L_19, /*hidden argument*/NULL);
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_007f;
	}

IL_007e:
	{
		G_B8_0 = ((Type_t *)(NULL));
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Format_m2398979370(NULL /*static, unused*/, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_22 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_22, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_008a:
	{
		Il2CppObject * L_23 = V_0;
		if (L_23)
		{
			goto IL_00b0;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_24 = __this->get__name_0();
		NullCheck(L_24);
		String_t* L_25 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral4212489508, L_25, _stringLiteral2113392597, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_27 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_27, L_26, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_00b0:
	{
		Il2CppObject * L_28 = V_0;
		XsltContext_t4010601445 * L_29 = V_1;
		NullCheck(L_28);
		Il2CppObject * L_30 = InterfaceFuncInvoker1< Il2CppObject *, XsltContext_t4010601445 * >::Invoke(0 /* System.Object System.Xml.Xsl.IXsltContextVariable::Evaluate(System.Xml.Xsl.XsltContext) */, IXsltContextVariable_t2835953560_il2cpp_TypeInfo_var, L_28, L_29);
		V_2 = L_30;
		Il2CppObject * L_31 = V_2;
		V_3 = ((XPathNodeIterator_t2394191562 *)IsInstClass(L_31, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var));
		XPathNodeIterator_t2394191562 * L_32 = V_3;
		if (!L_32)
		{
			goto IL_00e3;
		}
	}
	{
		XPathNodeIterator_t2394191562 * L_33 = V_3;
		if (!((BaseIterator_t3696600956 *)IsInstClass(L_33, BaseIterator_t3696600956_il2cpp_TypeInfo_var)))
		{
			goto IL_00d6;
		}
	}
	{
		XPathNodeIterator_t2394191562 * L_34 = V_3;
		G_B15_0 = L_34;
		goto IL_00e2;
	}

IL_00d6:
	{
		XPathNodeIterator_t2394191562 * L_35 = V_3;
		BaseIterator_t3696600956 * L_36 = ___iter0;
		NullCheck(L_36);
		Il2CppObject * L_37 = BaseIterator_get_NamespaceManager_m3653702256(L_36, /*hidden argument*/NULL);
		WrapperIterator_t2702681854 * L_38 = (WrapperIterator_t2702681854 *)il2cpp_codegen_object_new(WrapperIterator_t2702681854_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m802118820(L_38, L_35, L_37, /*hidden argument*/NULL);
		G_B15_0 = ((XPathNodeIterator_t2394191562 *)(L_38));
	}

IL_00e2:
	{
		return G_B15_0;
	}

IL_00e3:
	{
		Il2CppObject * L_39 = V_2;
		return L_39;
	}
}
// System.Boolean System.Xml.XPath.ExprVariable::get_Peer()
extern "C"  bool ExprVariable_get_Peer_m3838091998 (ExprVariable_t1838989486 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.FollowingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void FollowingIterator__ctor_m4068454143 (FollowingIterator_t4160732924 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.FollowingIterator::.ctor(System.Xml.XPath.FollowingIterator)
extern "C"  void FollowingIterator__ctor_m4069268917 (FollowingIterator_t4160732924 * __this, FollowingIterator_t4160732924 * ___other0, const MethodInfo* method)
{
	{
		FollowingIterator_t4160732924 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.FollowingIterator::Clone()
extern Il2CppClass* FollowingIterator_t4160732924_il2cpp_TypeInfo_var;
extern const uint32_t FollowingIterator_Clone_m1372703741_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * FollowingIterator_Clone_m1372703741 (FollowingIterator_t4160732924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FollowingIterator_Clone_m1372703741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FollowingIterator_t4160732924 * L_0 = (FollowingIterator_t4160732924 *)il2cpp_codegen_object_new(FollowingIterator_t4160732924_il2cpp_TypeInfo_var);
		FollowingIterator__ctor_m4069268917(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.FollowingIterator::MoveNextCore()
extern "C"  bool FollowingIterator_MoveNextCore_m1477945986 (FollowingIterator_t4160732924 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get__finished_6();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (bool)1;
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_1)
		{
			goto IL_008c;
		}
	}
	{
		V_0 = (bool)0;
		XPathNavigator_t1624538935 * L_2 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_2);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)3)))
		{
			goto IL_003b;
		}
	}
	{
		goto IL_004e;
	}

IL_003b:
	{
		XPathNavigator_t1624538935 * L_6 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_6);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_6);
		V_0 = (bool)1;
		goto IL_008c;
	}

IL_004e:
	{
		XPathNavigator_t1624538935 * L_7 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_7);
		if (!L_8)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)1;
	}

IL_0060:
	{
		goto IL_0077;
	}

IL_0065:
	{
		XPathNavigator_t1624538935 * L_9 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_9);
		if (!L_10)
		{
			goto IL_0077;
		}
	}
	{
		return (bool)1;
	}

IL_0077:
	{
		XPathNavigator_t1624538935 * L_11 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_11);
		if (L_12)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_008c;
	}

IL_008c:
	{
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_00c6;
		}
	}
	{
		XPathNavigator_t1624538935 * L_14 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_14);
		if (!L_15)
		{
			goto IL_00a4;
		}
	}
	{
		return (bool)1;
	}

IL_00a4:
	{
		XPathNavigator_t1624538935 * L_16 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_16);
		if (!L_17)
		{
			goto IL_00b6;
		}
	}
	{
		return (bool)1;
	}

IL_00b6:
	{
		XPathNavigator_t1624538935 * L_18 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_18);
		if (L_19)
		{
			goto IL_00a4;
		}
	}

IL_00c6:
	{
		__this->set__finished_6((bool)1);
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.FollowingSiblingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void FollowingSiblingIterator__ctor_m3707604177 (FollowingSiblingIterator_t1992378268 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.FollowingSiblingIterator::.ctor(System.Xml.XPath.FollowingSiblingIterator)
extern "C"  void FollowingSiblingIterator__ctor_m1388330545 (FollowingSiblingIterator_t1992378268 * __this, FollowingSiblingIterator_t1992378268 * ___other0, const MethodInfo* method)
{
	{
		FollowingSiblingIterator_t1992378268 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.FollowingSiblingIterator::Clone()
extern Il2CppClass* FollowingSiblingIterator_t1992378268_il2cpp_TypeInfo_var;
extern const uint32_t FollowingSiblingIterator_Clone_m83290849_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * FollowingSiblingIterator_Clone_m83290849 (FollowingSiblingIterator_t1992378268 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FollowingSiblingIterator_Clone_m83290849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FollowingSiblingIterator_t1992378268 * L_0 = (FollowingSiblingIterator_t1992378268 *)il2cpp_codegen_object_new(FollowingSiblingIterator_t1992378268_il2cpp_TypeInfo_var);
		FollowingSiblingIterator__ctor_m1388330545(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.FollowingSiblingIterator::MoveNextCore()
extern "C"  bool FollowingSiblingIterator_MoveNextCore_m1382672580 (FollowingSiblingIterator_t1992378268 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		XPathNavigator_t1624538935 * L_0 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0021;
	}

IL_001f:
	{
		return (bool)0;
	}

IL_0021:
	{
		XPathNavigator_t1624538935 * L_4 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_4);
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		return (bool)1;
	}

IL_0033:
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.FunctionArguments::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.FunctionArguments)
extern "C"  void FunctionArguments__ctor_m3611561738 (FunctionArguments_t3402201403 * __this, Expression_t4217024437 * ___arg0, FunctionArguments_t3402201403 * ___tail1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Expression_t4217024437 * L_0 = ___arg0;
		__this->set__arg_0(L_0);
		FunctionArguments_t3402201403 * L_1 = ___tail1;
		__this->set__tail_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.FunctionArguments::get_Arg()
extern "C"  Expression_t4217024437 * FunctionArguments_get_Arg_m969707333 (FunctionArguments_t3402201403 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get__arg_0();
		return L_0;
	}
}
// System.Xml.XPath.FunctionArguments System.Xml.XPath.FunctionArguments::get_Tail()
extern "C"  FunctionArguments_t3402201403 * FunctionArguments_get_Tail_m52932169 (FunctionArguments_t3402201403 * __this, const MethodInfo* method)
{
	{
		FunctionArguments_t3402201403 * L_0 = __this->get__tail_1();
		return L_0;
	}
}
// System.Void System.Xml.XPath.FunctionArguments::ToArrayList(System.Collections.ArrayList)
extern "C"  void FunctionArguments_ToArrayList_m227079145 (FunctionArguments_t3402201403 * __this, ArrayList_t2121638921 * ___a0, const MethodInfo* method)
{
	FunctionArguments_t3402201403 * V_0 = NULL;
	{
		V_0 = __this;
	}

IL_0002:
	{
		ArrayList_t2121638921 * L_0 = ___a0;
		FunctionArguments_t3402201403 * L_1 = V_0;
		NullCheck(L_1);
		Expression_t4217024437 * L_2 = L_1->get__arg_0();
		NullCheck(L_0);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_2);
		FunctionArguments_t3402201403 * L_3 = V_0;
		NullCheck(L_3);
		FunctionArguments_t3402201403 * L_4 = L_3->get__tail_1();
		V_0 = L_4;
		FunctionArguments_t3402201403 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XPath.ListIterator::.ctor(System.Xml.XPath.BaseIterator,System.Collections.IList)
extern "C"  void ListIterator__ctor_m222741241 (ListIterator_t2154705993 * __this, BaseIterator_t3696600956 * ___iter0, Il2CppObject * ___list1, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___list1;
		__this->set__list_3(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.ListIterator::.ctor(System.Collections.IList,System.Xml.IXmlNamespaceResolver)
extern "C"  void ListIterator__ctor_m320855722 (ListIterator_t2154705993 * __this, Il2CppObject * ___list0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___nsm1;
		BaseIterator__ctor_m3826808382(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___list0;
		__this->set__list_3(L_1);
		return;
	}
}
// System.Void System.Xml.XPath.ListIterator::.ctor(System.Xml.XPath.ListIterator)
extern "C"  void ListIterator__ctor_m3954479633 (ListIterator_t2154705993 * __this, ListIterator_t2154705993 * ___other0, const MethodInfo* method)
{
	{
		ListIterator_t2154705993 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		ListIterator_t2154705993 * L_1 = ___other0;
		NullCheck(L_1);
		Il2CppObject * L_2 = L_1->get__list_3();
		__this->set__list_3(L_2);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.ListIterator::Clone()
extern Il2CppClass* ListIterator_t2154705993_il2cpp_TypeInfo_var;
extern const uint32_t ListIterator_Clone_m1676587988_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * ListIterator_Clone_m1676587988 (ListIterator_t2154705993 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListIterator_Clone_m1676587988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ListIterator_t2154705993 * L_0 = (ListIterator_t2154705993 *)il2cpp_codegen_object_new(ListIterator_t2154705993_il2cpp_TypeInfo_var);
		ListIterator__ctor_m3954479633(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.ListIterator::MoveNextCore()
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t ListIterator_MoveNextCore_m2796201201_MetadataUsageId;
extern "C"  bool ListIterator_MoveNextCore_m2796201201 (ListIterator_t2154705993 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListIterator_MoveNextCore_m2796201201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		Il2CppObject * L_1 = __this->get__list_3();
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3761522009_il2cpp_TypeInfo_var, L_1);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		return (bool)1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.ListIterator::get_Current()
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1612618265_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern const uint32_t ListIterator_get_Current_m3594158222_MetadataUsageId;
extern "C"  XPathNavigator_t1624538935 * ListIterator_get_Current_m3594158222 (ListIterator_t2154705993 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListIterator_get_Current_m3594158222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__list_3();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3761522009_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_2)
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return (XPathNavigator_t1624538935 *)NULL;
	}

IL_001d:
	{
		Il2CppObject * L_3 = __this->get__list_3();
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		NullCheck(L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1612618265_il2cpp_TypeInfo_var, L_3, ((int32_t)((int32_t)L_4-(int32_t)1)));
		return ((XPathNavigator_t1624538935 *)CastclassClass(L_5, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
	}
}
// System.Int32 System.Xml.XPath.ListIterator::get_Count()
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t ListIterator_get_Count_m773660244_MetadataUsageId;
extern "C"  int32_t ListIterator_get_Count_m773660244 (ListIterator_t2154705993 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListIterator_get_Count_m773660244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__list_3();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3761522009_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.NamespaceIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void NamespaceIterator__ctor_m729989193 (NamespaceIterator_t2306455494 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.NamespaceIterator::.ctor(System.Xml.XPath.NamespaceIterator)
extern "C"  void NamespaceIterator__ctor_m30894881 (NamespaceIterator_t2306455494 * __this, NamespaceIterator_t2306455494 * ___other0, const MethodInfo* method)
{
	{
		NamespaceIterator_t2306455494 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.NamespaceIterator::Clone()
extern Il2CppClass* NamespaceIterator_t2306455494_il2cpp_TypeInfo_var;
extern const uint32_t NamespaceIterator_Clone_m1073878643_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * NamespaceIterator_Clone_m1073878643 (NamespaceIterator_t2306455494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NamespaceIterator_Clone_m1073878643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NamespaceIterator_t2306455494 * L_0 = (NamespaceIterator_t2306455494 *)il2cpp_codegen_object_new(NamespaceIterator_t2306455494_il2cpp_TypeInfo_var);
		NamespaceIterator__ctor_m30894881(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.NamespaceIterator::MoveNextCore()
extern "C"  bool NamespaceIterator_MoveNextCore_m212520780 (NamespaceIterator_t2306455494 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		XPathNavigator_t1624538935 * L_1 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_1);
		bool L_2 = XPathNavigator_MoveToFirstNamespace_m3738691923(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		return (bool)1;
	}

IL_001d:
	{
		goto IL_0034;
	}

IL_0022:
	{
		XPathNavigator_t1624538935 * L_3 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_3);
		bool L_4 = XPathNavigator_MoveToNextNamespace_m197928770(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		return (bool)1;
	}

IL_0034:
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.NodeNameTest::.ctor(System.Xml.XPath.Axes,System.Xml.XmlQualifiedName,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var;
extern const uint32_t NodeNameTest__ctor_m98601429_MetadataUsageId;
extern "C"  void NodeNameTest__ctor_m98601429 (NodeNameTest_t873888508 * __this, int32_t ___axis0, XmlQualifiedName_t176365656 * ___name1, Il2CppObject * ___ctx2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeNameTest__ctor_m98601429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___axis0;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___ctx2;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject * L_2 = ___ctx2;
		XmlQualifiedName_t176365656 * L_3 = ___name1;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_3);
		NullCheck(L_2);
		XmlQualifiedName_t176365656 * L_5 = InterfaceFuncInvoker1< XmlQualifiedName_t176365656 *, String_t* >::Invoke(2 /* System.Xml.XmlQualifiedName System.Xml.Xsl.IStaticXsltContext::LookupQName(System.String) */, IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var, L_2, L_4);
		___name1 = L_5;
		__this->set_resolvedName_2((bool)1);
	}

IL_0022:
	{
		XmlQualifiedName_t176365656 * L_6 = ___name1;
		__this->set__name_1(L_6);
		return;
	}
}
// System.Void System.Xml.XPath.NodeNameTest::.ctor(System.Xml.XPath.NodeNameTest,System.Xml.XPath.Axes)
extern "C"  void NodeNameTest__ctor_m3377740561 (NodeNameTest_t873888508 * __this, NodeNameTest_t873888508 * ___source0, int32_t ___axis1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis1;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		NodeNameTest_t873888508 * L_1 = ___source0;
		NullCheck(L_1);
		XmlQualifiedName_t176365656 * L_2 = L_1->get__name_1();
		__this->set__name_1(L_2);
		NodeNameTest_t873888508 * L_3 = ___source0;
		NullCheck(L_3);
		bool L_4 = L_3->get_resolvedName_2();
		__this->set_resolvedName_2(L_4);
		return;
	}
}
// System.String System.Xml.XPath.NodeNameTest::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1856;
extern const uint32_t NodeNameTest_ToString_m2840726898_MetadataUsageId;
extern "C"  String_t* NodeNameTest_ToString_m2840726898 (NodeNameTest_t873888508 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeNameTest_ToString_m2840726898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AxisSpecifier_t4216515578 * L_0 = ((NodeTest_t911751889 *)__this)->get__axis_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XPath.AxisSpecifier::ToString() */, L_0);
		XmlQualifiedName_t176365656 * L_2 = __this->get__name_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral1856, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean System.Xml.XPath.NodeNameTest::Match(System.Xml.IXmlNamespaceResolver,System.Xml.XPath.XPathNavigator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IXmlNamespaceResolver_t3696234203_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral638572358;
extern const uint32_t NodeNameTest_Match_m3615173812_MetadataUsageId;
extern "C"  bool NodeNameTest_Match_m3615173812 (NodeNameTest_t873888508 * __this, Il2CppObject * ___nsm0, XPathNavigator_t1624538935 * ___nav1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeNameTest_Match_m3615173812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		XPathNavigator_t1624538935 * L_0 = ___nav1;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_0);
		AxisSpecifier_t4216515578 * L_2 = ((NodeTest_t911751889 *)__this)->get__axis_0();
		NullCheck(L_2);
		int32_t L_3 = AxisSpecifier_get_NodeType_m3352025689(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_3)))
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		XmlQualifiedName_t176365656 * L_4 = __this->get__name_1();
		NullCheck(L_4);
		String_t* L_5 = XmlQualifiedName_get_Name_m607016698(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_7 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004f;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_8 = __this->get__name_1();
		NullCheck(L_8);
		String_t* L_9 = XmlQualifiedName_get_Name_m607016698(L_8, /*hidden argument*/NULL);
		XPathNavigator_t1624538935 * L_10 = ___nav1;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004f;
		}
	}
	{
		return (bool)0;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_13;
		XmlQualifiedName_t176365656 * L_14 = __this->get__name_1();
		NullCheck(L_14);
		String_t* L_15 = XmlQualifiedName_get_Namespace_m2987642414(L_14, /*hidden argument*/NULL);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_17 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00c4;
		}
	}
	{
		bool L_18 = __this->get_resolvedName_2();
		if (!L_18)
		{
			goto IL_008b;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_19 = __this->get__name_1();
		NullCheck(L_19);
		String_t* L_20 = XmlQualifiedName_get_Namespace_m2987642414(L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		goto IL_00a3;
	}

IL_008b:
	{
		Il2CppObject * L_21 = ___nsm0;
		if (!L_21)
		{
			goto IL_00a3;
		}
	}
	{
		Il2CppObject * L_22 = ___nsm0;
		XmlQualifiedName_t176365656 * L_23 = __this->get__name_1();
		NullCheck(L_23);
		String_t* L_24 = XmlQualifiedName_get_Namespace_m2987642414(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_25 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(0 /* System.String System.Xml.IXmlNamespaceResolver::LookupNamespace(System.String) */, IXmlNamespaceResolver_t3696234203_il2cpp_TypeInfo_var, L_22, L_24);
		V_0 = L_25;
	}

IL_00a3:
	{
		String_t* L_26 = V_0;
		if (L_26)
		{
			goto IL_00c4;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_27 = __this->get__name_1();
		NullCheck(L_27);
		String_t* L_28 = XmlQualifiedName_get_Namespace_m2987642414(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral638572358, L_28, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_30 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_30, L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00c4:
	{
		String_t* L_31 = V_0;
		XPathNavigator_t1624538935 * L_32 = ___nav1;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		return L_34;
	}
}
// System.Void System.Xml.XPath.NodeSet::.ctor()
extern "C"  void NodeSet__ctor_m3658293772 (NodeSet_t3503134685 * __this, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.NodeSet::get_ReturnType()
extern "C"  int32_t NodeSet_get_ReturnType_m2311151599 (NodeSet_t3503134685 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(3);
	}
}
// System.Void System.Xml.XPath.NodeTest::.ctor(System.Xml.XPath.Axes)
extern Il2CppClass* AxisSpecifier_t4216515578_il2cpp_TypeInfo_var;
extern const uint32_t NodeTest__ctor_m3652977216_MetadataUsageId;
extern "C"  void NodeTest__ctor_m3652977216 (NodeTest_t911751889 * __this, int32_t ___axis0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTest__ctor_m3652977216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___axis0;
		AxisSpecifier_t4216515578 * L_1 = (AxisSpecifier_t4216515578 *)il2cpp_codegen_object_new(AxisSpecifier_t4216515578_il2cpp_TypeInfo_var);
		AxisSpecifier__ctor_m223823863(L_1, L_0, /*hidden argument*/NULL);
		__this->set__axis_0(L_1);
		return;
	}
}
// System.Xml.XPath.AxisSpecifier System.Xml.XPath.NodeTest::get_Axis()
extern "C"  AxisSpecifier_t4216515578 * NodeTest_get_Axis_m243839149 (NodeTest_t911751889 * __this, const MethodInfo* method)
{
	{
		AxisSpecifier_t4216515578 * L_0 = __this->get__axis_0();
		return L_0;
	}
}
// System.Object System.Xml.XPath.NodeTest::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* AxisIterator_t3322918988_il2cpp_TypeInfo_var;
extern const uint32_t NodeTest_Evaluate_m2924662090_MetadataUsageId;
extern "C"  Il2CppObject * NodeTest_Evaluate_m2924662090 (NodeTest_t911751889 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTest_Evaluate_m2924662090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t3696600956 * V_0 = NULL;
	{
		AxisSpecifier_t4216515578 * L_0 = __this->get__axis_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t3696600956 * L_2 = AxisSpecifier_Evaluate_m3457862801(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		BaseIterator_t3696600956 * L_3 = V_0;
		AxisIterator_t3322918988 * L_4 = (AxisIterator_t3322918988 *)il2cpp_codegen_object_new(AxisIterator_t3322918988_il2cpp_TypeInfo_var);
		AxisIterator__ctor_m2307956914(L_4, L_3, __this, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean System.Xml.XPath.NodeTest::get_RequireSorting()
extern "C"  bool NodeTest_get_RequireSorting_m101109566 (NodeTest_t911751889 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		AxisSpecifier_t4216515578 * L_0 = __this->get__axis_0();
		NullCheck(L_0);
		int32_t L_1 = AxisSpecifier_get_Axis_m269195078(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0047;
		}
		if (L_2 == 1)
		{
			goto IL_0047;
		}
		if (L_2 == 2)
		{
			goto IL_0047;
		}
		if (L_2 == 3)
		{
			goto IL_0049;
		}
		if (L_2 == 4)
		{
			goto IL_0049;
		}
		if (L_2 == 5)
		{
			goto IL_0049;
		}
		if (L_2 == 6)
		{
			goto IL_0049;
		}
		if (L_2 == 7)
		{
			goto IL_0049;
		}
		if (L_2 == 8)
		{
			goto IL_0047;
		}
		if (L_2 == 9)
		{
			goto IL_0049;
		}
		if (L_2 == 10)
		{
			goto IL_0047;
		}
		if (L_2 == 11)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0049;
	}

IL_0047:
	{
		return (bool)1;
	}

IL_0049:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.NodeTest::get_Peer()
extern "C"  bool NodeTest_get_Peer_m1939429633 (NodeTest_t911751889 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		AxisSpecifier_t4216515578 * L_0 = __this->get__axis_0();
		NullCheck(L_0);
		int32_t L_1 = AxisSpecifier_get_Axis_m269195078(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0043;
		}
		if (L_2 == 1)
		{
			goto IL_0043;
		}
		if (L_2 == 2)
		{
			goto IL_0045;
		}
		if (L_2 == 3)
		{
			goto IL_0045;
		}
		if (L_2 == 4)
		{
			goto IL_0043;
		}
		if (L_2 == 5)
		{
			goto IL_0043;
		}
		if (L_2 == 6)
		{
			goto IL_0043;
		}
		if (L_2 == 7)
		{
			goto IL_0045;
		}
		if (L_2 == 8)
		{
			goto IL_0045;
		}
		if (L_2 == 9)
		{
			goto IL_0045;
		}
		if (L_2 == 10)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0045;
	}

IL_0043:
	{
		return (bool)0;
	}

IL_0045:
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.NodeTest::get_Subtree()
extern "C"  bool NodeTest_get_Subtree_m100870017 (NodeTest_t911751889 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		AxisSpecifier_t4216515578 * L_0 = __this->get__axis_0();
		NullCheck(L_0);
		int32_t L_1 = AxisSpecifier_get_Axis_m269195078(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0047;
		}
		if (L_2 == 1)
		{
			goto IL_0047;
		}
		if (L_2 == 2)
		{
			goto IL_0049;
		}
		if (L_2 == 3)
		{
			goto IL_0049;
		}
		if (L_2 == 4)
		{
			goto IL_0049;
		}
		if (L_2 == 5)
		{
			goto IL_0049;
		}
		if (L_2 == 6)
		{
			goto IL_0047;
		}
		if (L_2 == 7)
		{
			goto IL_0047;
		}
		if (L_2 == 8)
		{
			goto IL_0049;
		}
		if (L_2 == 9)
		{
			goto IL_0047;
		}
		if (L_2 == 10)
		{
			goto IL_0047;
		}
		if (L_2 == 11)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0049;
	}

IL_0047:
	{
		return (bool)0;
	}

IL_0049:
	{
		return (bool)1;
	}
}
// System.Void System.Xml.XPath.NodeTypeTest::.ctor(System.Xml.XPath.Axes)
extern "C"  void NodeTypeTest__ctor_m423301530 (NodeTypeTest_t2651955243 * __this, int32_t ___axis0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis0;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		AxisSpecifier_t4216515578 * L_1 = ((NodeTest_t911751889 *)__this)->get__axis_0();
		NullCheck(L_1);
		int32_t L_2 = AxisSpecifier_get_NodeType_m3352025689(L_1, /*hidden argument*/NULL);
		__this->set_type_1(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.NodeTypeTest::.ctor(System.Xml.XPath.Axes,System.Xml.XPath.XPathNodeType)
extern "C"  void NodeTypeTest__ctor_m2014120890 (NodeTypeTest_t2651955243 * __this, int32_t ___axis0, int32_t ___type1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis0;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___type1;
		__this->set_type_1(L_1);
		return;
	}
}
// System.Void System.Xml.XPath.NodeTypeTest::.ctor(System.Xml.XPath.Axes,System.Xml.XPath.XPathNodeType,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1267081075;
extern Il2CppCodeGenString* _stringLiteral2347313233;
extern const uint32_t NodeTypeTest__ctor_m2764187254_MetadataUsageId;
extern "C"  void NodeTypeTest__ctor_m2764187254 (NodeTypeTest_t2651955243 * __this, int32_t ___axis0, int32_t ___type1, String_t* ___param2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTypeTest__ctor_m2764187254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___axis0;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___type1;
		__this->set_type_1(L_1);
		String_t* L_2 = ___param2;
		__this->set__param_2(L_2);
		String_t* L_3 = ___param2;
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = ___type1;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_5 = ___type1;
		String_t* L_6 = NodeTypeTest_ToString_m1634841327(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1267081075, L_6, _stringLiteral2347313233, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_8 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Xml.XPath.NodeTypeTest::.ctor(System.Xml.XPath.NodeTypeTest,System.Xml.XPath.Axes)
extern "C"  void NodeTypeTest__ctor_m1445907889 (NodeTypeTest_t2651955243 * __this, NodeTypeTest_t2651955243 * ___other0, int32_t ___axis1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis1;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		NodeTypeTest_t2651955243 * L_1 = ___other0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_type_1();
		__this->set_type_1(L_2);
		NodeTypeTest_t2651955243 * L_3 = ___other0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get__param_2();
		__this->set__param_2(L_4);
		return;
	}
}
// System.String System.Xml.XPath.NodeTypeTest::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1279;
extern Il2CppCodeGenString* _stringLiteral1250;
extern Il2CppCodeGenString* _stringLiteral1281;
extern Il2CppCodeGenString* _stringLiteral1856;
extern const uint32_t NodeTypeTest_ToString_m2899043361_MetadataUsageId;
extern "C"  String_t* NodeTypeTest_ToString_m2899043361 (NodeTypeTest_t2651955243 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTypeTest_ToString_m2899043361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_type_1();
		String_t* L_1 = NodeTypeTest_ToString_m1634841327(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_type_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)7))))
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_3 = __this->get__param_2();
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_4 = V_0;
		String_t* L_5 = __this->get__param_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2933632197(NULL /*static, unused*/, L_4, _stringLiteral1279, L_5, _stringLiteral1250, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_004b;
	}

IL_003f:
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, L_7, _stringLiteral1281, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_004b:
	{
		AxisSpecifier_t4216515578 * L_9 = ((NodeTest_t911751889 *)__this)->get__axis_0();
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XPath.AxisSpecifier::ToString() */, L_9);
		String_t* L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1825781833(NULL /*static, unused*/, L_10, _stringLiteral1856, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String System.Xml.XPath.NodeTypeTest::ToString(System.Xml.XPath.XPathNodeType)
extern Il2CppClass* XPathNodeType_t4070737174_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral950398559;
extern Il2CppCodeGenString* _stringLiteral3556653;
extern Il2CppCodeGenString* _stringLiteral3628607700;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern Il2CppCodeGenString* _stringLiteral3858170656;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t NodeTypeTest_ToString_m1634841327_MetadataUsageId;
extern "C"  String_t* NodeTypeTest_ToString_m1634841327 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTypeTest_ToString_m1634841327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___type0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0039;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_003f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0033;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_004b;
	}

IL_0033:
	{
		return _stringLiteral950398559;
	}

IL_0039:
	{
		return _stringLiteral3556653;
	}

IL_003f:
	{
		return _stringLiteral3628607700;
	}

IL_0045:
	{
		return _stringLiteral3386882;
	}

IL_004b:
	{
		int32_t L_2 = ___type0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(XPathNodeType_t4070737174_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Enum_t2778772662 *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2778772662 *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3858170656, L_5, _stringLiteral93, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean System.Xml.XPath.NodeTypeTest::Match(System.Xml.IXmlNamespaceResolver,System.Xml.XPath.XPathNavigator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t NodeTypeTest_Match_m4271858789_MetadataUsageId;
extern "C"  bool NodeTypeTest_Match_m4271858789 (NodeTypeTest_t2651955243 * __this, Il2CppObject * ___nsm0, XPathNavigator_t1624538935 * ___nav1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTypeTest_Match_m4271858789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		XPathNavigator_t1624538935 * L_0 = ___nav1;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_0);
		V_0 = L_1;
		int32_t L_2 = __this->get_type_1();
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 0)
		{
			goto IL_0063;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 1)
		{
			goto IL_0082;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 2)
		{
			goto IL_0082;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 3)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 4)
		{
			goto IL_0082;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 5)
		{
			goto IL_0033;
		}
	}
	{
		goto IL_0082;
	}

IL_0033:
	{
		return (bool)1;
	}

IL_0035:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_003e;
		}
	}
	{
		return (bool)0;
	}

IL_003e:
	{
		String_t* L_5 = __this->get__param_2();
		if (!L_5)
		{
			goto IL_0061;
		}
	}
	{
		XPathNavigator_t1624538935 * L_6 = ___nav1;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_6);
		String_t* L_8 = __this->get__param_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0061;
		}
	}
	{
		return (bool)0;
	}

IL_0061:
	{
		return (bool)1;
	}

IL_0063:
	{
		int32_t L_10 = V_0;
		V_2 = L_10;
		int32_t L_11 = V_2;
		if (((int32_t)((int32_t)L_11-(int32_t)4)) == 0)
		{
			goto IL_007e;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)4)) == 1)
		{
			goto IL_007e;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)4)) == 2)
		{
			goto IL_007e;
		}
	}
	{
		goto IL_0080;
	}

IL_007e:
	{
		return (bool)1;
	}

IL_0080:
	{
		return (bool)0;
	}

IL_0082:
	{
		int32_t L_12 = __this->get_type_1();
		int32_t L_13 = V_0;
		return (bool)((((int32_t)L_12) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void NullIterator__ctor_m2854644327 (NullIterator_t979652658 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		SelfIterator__ctor_m2835358572(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void NullIterator__ctor_m4158990173 (NullIterator_t979652658 * __this, XPathNavigator_t1624538935 * ___nav0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = ___nav0;
		Il2CppObject * L_1 = ___nsm1;
		SelfIterator__ctor_m4150585400(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.NullIterator)
extern "C"  void NullIterator__ctor_m233625713 (NullIterator_t979652658 * __this, NullIterator_t979652658 * ___other0, const MethodInfo* method)
{
	{
		NullIterator_t979652658 * L_0 = ___other0;
		SelfIterator__ctor_m3078894092(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.NullIterator::Clone()
extern Il2CppClass* NullIterator_t979652658_il2cpp_TypeInfo_var;
extern const uint32_t NullIterator_Clone_m2491346443_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * NullIterator_Clone_m2491346443 (NullIterator_t979652658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NullIterator_Clone_m2491346443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullIterator_t979652658 * L_0 = (NullIterator_t979652658 *)il2cpp_codegen_object_new(NullIterator_t979652658_il2cpp_TypeInfo_var);
		NullIterator__ctor_m233625713(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.NullIterator::MoveNextCore()
extern "C"  bool NullIterator_MoveNextCore_m161861338 (NullIterator_t979652658 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int32 System.Xml.XPath.NullIterator::get_CurrentPosition()
extern "C"  int32_t NullIterator_get_CurrentPosition_m3673814846 (NullIterator_t979652658 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.NullIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * NullIterator_get_Current_m3786274181 (NullIterator_t979652658 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		return L_0;
	}
}
// System.Void System.Xml.XPath.ParensIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void ParensIterator__ctor_m938283977 (ParensIterator_t3967469908 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_2 = ___iter0;
		__this->set__iter_3(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.ParensIterator::.ctor(System.Xml.XPath.ParensIterator)
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern const uint32_t ParensIterator__ctor_m3840579889_MetadataUsageId;
extern "C"  void ParensIterator__ctor_m3840579889 (ParensIterator_t3967469908 * __this, ParensIterator_t3967469908 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParensIterator__ctor_m3840579889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParensIterator_t3967469908 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		ParensIterator_t3967469908 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t3696600956 * L_2 = L_1->get__iter_3();
		NullCheck(L_2);
		XPathNodeIterator_t2394191562 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__iter_3(((BaseIterator_t3696600956 *)CastclassClass(L_3, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.ParensIterator::Clone()
extern Il2CppClass* ParensIterator_t3967469908_il2cpp_TypeInfo_var;
extern const uint32_t ParensIterator_Clone_m2933303977_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * ParensIterator_Clone_m2933303977 (ParensIterator_t3967469908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParensIterator_Clone_m2933303977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParensIterator_t3967469908 * L_0 = (ParensIterator_t3967469908 *)il2cpp_codegen_object_new(ParensIterator_t3967469908_il2cpp_TypeInfo_var);
		ParensIterator__ctor_m3840579889(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.ParensIterator::MoveNextCore()
extern "C"  bool ParensIterator_MoveNextCore_m2517159868 (ParensIterator_t3967469908 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_0);
		return L_1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.ParensIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * ParensIterator_get_Current_m1307635171 (ParensIterator_t3967469908 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		XPathNavigator_t1624538935 * L_1 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_0);
		return L_1;
	}
}
// System.Int32 System.Xml.XPath.ParensIterator::get_Count()
extern "C"  int32_t ParensIterator_get_Count_m3610177769 (ParensIterator_t3967469908 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.ParentIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void ParentIterator__ctor_m2909223434 (ParentIterator_t2160015445 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		XPathNavigator_t1624538935 * L_1 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_1);
		__this->set_canMove_6(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.ParentIterator::.ctor(System.Xml.XPath.ParentIterator,System.Boolean)
extern "C"  void ParentIterator__ctor_m3526189836 (ParentIterator_t2160015445 * __this, ParentIterator_t2160015445 * ___other0, bool ___dummy1, const MethodInfo* method)
{
	{
		ParentIterator_t2160015445 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		ParentIterator_t2160015445 * L_1 = ___other0;
		NullCheck(L_1);
		bool L_2 = L_1->get_canMove_6();
		__this->set_canMove_6(L_2);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.ParentIterator::Clone()
extern Il2CppClass* ParentIterator_t2160015445_il2cpp_TypeInfo_var;
extern const uint32_t ParentIterator_Clone_m1936231624_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * ParentIterator_Clone_m1936231624 (ParentIterator_t2160015445 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParentIterator_Clone_m1936231624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParentIterator_t2160015445 * L_0 = (ParentIterator_t2160015445 *)il2cpp_codegen_object_new(ParentIterator_t2160015445_il2cpp_TypeInfo_var);
		ParentIterator__ctor_m3526189836(L_0, __this, (bool)1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.ParentIterator::MoveNextCore()
extern "C"  bool ParentIterator_MoveNextCore_m439950525 (ParentIterator_t2160015445 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_canMove_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		__this->set_canMove_6((bool)0);
		return (bool)1;
	}
}
// System.Void System.Xml.XPath.PrecedingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void PrecedingIterator__ctor_m3521034673 (PrecedingIterator_t2050916142 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		XPathNavigator_t1624538935 * L_3 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_2);
		__this->set_startPosition_8(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.PrecedingIterator::.ctor(System.Xml.XPath.PrecedingIterator)
extern "C"  void PrecedingIterator__ctor_m3696965713 (PrecedingIterator_t2050916142 * __this, PrecedingIterator_t2050916142 * ___other0, const MethodInfo* method)
{
	{
		PrecedingIterator_t2050916142 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		PrecedingIterator_t2050916142 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = L_1->get_startPosition_8();
		__this->set_startPosition_8(L_2);
		PrecedingIterator_t2050916142 * L_3 = ___other0;
		NullCheck(L_3);
		bool L_4 = L_3->get_started_7();
		__this->set_started_7(L_4);
		PrecedingIterator_t2050916142 * L_5 = ___other0;
		NullCheck(L_5);
		bool L_6 = L_5->get_finished_6();
		__this->set_finished_6(L_6);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.PrecedingIterator::Clone()
extern Il2CppClass* PrecedingIterator_t2050916142_il2cpp_TypeInfo_var;
extern const uint32_t PrecedingIterator_Clone_m1082336267_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * PrecedingIterator_Clone_m1082336267 (PrecedingIterator_t2050916142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrecedingIterator_Clone_m1082336267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PrecedingIterator_t2050916142 * L_0 = (PrecedingIterator_t2050916142 *)il2cpp_codegen_object_new(PrecedingIterator_t2050916142_il2cpp_TypeInfo_var);
		PrecedingIterator__ctor_m3696965713(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.PrecedingIterator::MoveNextCore()
extern "C"  bool PrecedingIterator_MoveNextCore_m3322136244 (PrecedingIterator_t2050916142 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_finished_6();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_1 = __this->get_started_7();
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		__this->set_started_7((bool)1);
		XPathNavigator_t1624538935 * L_2 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_2);
	}

IL_002a:
	{
		V_0 = (bool)1;
		goto IL_009b;
	}

IL_0031:
	{
		goto IL_0069;
	}

IL_0036:
	{
		goto IL_0054;
	}

IL_003b:
	{
		XPathNavigator_t1624538935 * L_3 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_3);
		if (L_4)
		{
			goto IL_0054;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_0054:
	{
		XPathNavigator_t1624538935 * L_5 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_5);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		goto IL_0079;
	}

IL_0069:
	{
		XPathNavigator_t1624538935 * L_7 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_7);
		if (!L_8)
		{
			goto IL_0036;
		}
	}

IL_0079:
	{
		XPathNavigator_t1624538935 * L_9 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		XPathNavigator_t1624538935 * L_10 = __this->get_startPosition_8();
		NullCheck(L_9);
		bool L_11 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, L_9, L_10);
		if (!L_11)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_009b;
	}

IL_0094:
	{
		V_0 = (bool)0;
		goto IL_00a1;
	}

IL_009b:
	{
		bool L_12 = V_0;
		if (L_12)
		{
			goto IL_0031;
		}
	}

IL_00a1:
	{
		XPathNavigator_t1624538935 * L_13 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		XPathNavigator_t1624538935 * L_14 = __this->get_startPosition_8();
		NullCheck(L_13);
		int32_t L_15 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_13, L_14);
		if (!L_15)
		{
			goto IL_00c0;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.PrecedingIterator::get_ReverseAxis()
extern "C"  bool PrecedingIterator_get_ReverseAxis_m3280303211 (PrecedingIterator_t2050916142 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Xml.XPath.PrecedingSiblingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void PrecedingSiblingIterator__ctor_m3406853599 (PrecedingSiblingIterator_t3194309418 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		XPathNavigator_t1624538935 * L_3 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_2);
		__this->set_startPosition_8(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.PrecedingSiblingIterator::.ctor(System.Xml.XPath.PrecedingSiblingIterator)
extern "C"  void PrecedingSiblingIterator__ctor_m2932109425 (PrecedingSiblingIterator_t3194309418 * __this, PrecedingSiblingIterator_t3194309418 * ___other0, const MethodInfo* method)
{
	{
		PrecedingSiblingIterator_t3194309418 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		PrecedingSiblingIterator_t3194309418 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = L_1->get_startPosition_8();
		__this->set_startPosition_8(L_2);
		PrecedingSiblingIterator_t3194309418 * L_3 = ___other0;
		NullCheck(L_3);
		bool L_4 = L_3->get_started_7();
		__this->set_started_7(L_4);
		PrecedingSiblingIterator_t3194309418 * L_5 = ___other0;
		NullCheck(L_5);
		bool L_6 = L_5->get_finished_6();
		__this->set_finished_6(L_6);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.PrecedingSiblingIterator::Clone()
extern Il2CppClass* PrecedingSiblingIterator_t3194309418_il2cpp_TypeInfo_var;
extern const uint32_t PrecedingSiblingIterator_Clone_m1927481107_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * PrecedingSiblingIterator_Clone_m1927481107 (PrecedingSiblingIterator_t3194309418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrecedingSiblingIterator_Clone_m1927481107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PrecedingSiblingIterator_t3194309418 * L_0 = (PrecedingSiblingIterator_t3194309418 *)il2cpp_codegen_object_new(PrecedingSiblingIterator_t3194309418_il2cpp_TypeInfo_var);
		PrecedingSiblingIterator__ctor_m2932109425(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.PrecedingSiblingIterator::MoveNextCore()
extern "C"  bool PrecedingSiblingIterator_MoveNextCore_m2380337234 (PrecedingSiblingIterator_t3194309418 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_finished_6();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_1 = __this->get_started_7();
		if (L_1)
		{
			goto IL_0070;
		}
	}
	{
		__this->set_started_7((bool)1);
		XPathNavigator_t1624538935 * L_2 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)3)))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0047;
	}

IL_003e:
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_0047:
	{
		XPathNavigator_t1624538935 * L_6 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_6);
		VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirst() */, L_6);
		XPathNavigator_t1624538935 * L_7 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		XPathNavigator_t1624538935 * L_8 = __this->get_startPosition_8();
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_7, L_8);
		if (L_9)
		{
			goto IL_006b;
		}
	}
	{
		return (bool)1;
	}

IL_006b:
	{
		goto IL_0089;
	}

IL_0070:
	{
		XPathNavigator_t1624538935 * L_10 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_10);
		if (L_11)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_0089:
	{
		XPathNavigator_t1624538935 * L_12 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		XPathNavigator_t1624538935 * L_13 = __this->get_startPosition_8();
		NullCheck(L_12);
		int32_t L_14 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_12, L_13);
		if (!L_14)
		{
			goto IL_00a8;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_00a8:
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.PrecedingSiblingIterator::get_ReverseAxis()
extern "C"  bool PrecedingSiblingIterator_get_ReverseAxis_m872373773 (PrecedingSiblingIterator_t3194309418 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Xml.XPath.PredicateIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.Expression)
extern "C"  void PredicateIterator__ctor_m3004206002 (PredicateIterator_t4169199650 * __this, BaseIterator_t3696600956 * ___iter0, Expression_t4217024437 * ___pred1, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_2 = ___iter0;
		__this->set__iter_3(L_2);
		Expression_t4217024437 * L_3 = ___pred1;
		__this->set__pred_4(L_3);
		Expression_t4217024437 * L_4 = ___pred1;
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, BaseIterator_t3696600956 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		__this->set_resType_5(L_6);
		return;
	}
}
// System.Void System.Xml.XPath.PredicateIterator::.ctor(System.Xml.XPath.PredicateIterator)
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern const uint32_t PredicateIterator__ctor_m1246946409_MetadataUsageId;
extern "C"  void PredicateIterator__ctor_m1246946409 (PredicateIterator_t4169199650 * __this, PredicateIterator_t4169199650 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PredicateIterator__ctor_m1246946409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PredicateIterator_t4169199650 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		PredicateIterator_t4169199650 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t3696600956 * L_2 = L_1->get__iter_3();
		NullCheck(L_2);
		XPathNodeIterator_t2394191562 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__iter_3(((BaseIterator_t3696600956 *)CastclassClass(L_3, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
		PredicateIterator_t4169199650 * L_4 = ___other0;
		NullCheck(L_4);
		Expression_t4217024437 * L_5 = L_4->get__pred_4();
		__this->set__pred_4(L_5);
		PredicateIterator_t4169199650 * L_6 = ___other0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_resType_5();
		__this->set_resType_5(L_7);
		PredicateIterator_t4169199650 * L_8 = ___other0;
		NullCheck(L_8);
		bool L_9 = L_8->get_finished_6();
		__this->set_finished_6(L_9);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.PredicateIterator::Clone()
extern Il2CppClass* PredicateIterator_t4169199650_il2cpp_TypeInfo_var;
extern const uint32_t PredicateIterator_Clone_m2751366551_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * PredicateIterator_Clone_m2751366551 (PredicateIterator_t4169199650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PredicateIterator_Clone_m2751366551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PredicateIterator_t4169199650 * L_0 = (PredicateIterator_t4169199650 *)il2cpp_codegen_object_new(PredicateIterator_t4169199650_il2cpp_TypeInfo_var);
		PredicateIterator__ctor_m1246946409(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.PredicateIterator::MoveNextCore()
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t PredicateIterator_MoveNextCore_m3008221096_MetadataUsageId;
extern "C"  bool PredicateIterator_MoveNextCore_m3008221096 (PredicateIterator_t4169199650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PredicateIterator_MoveNextCore_m3008221096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_finished_6();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		goto IL_00cc;
	}

IL_0012:
	{
		int32_t L_1 = __this->get_resType_5();
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)5)))
		{
			goto IL_0057;
		}
	}
	{
		goto IL_00aa;
	}

IL_002b:
	{
		Expression_t4217024437 * L_4 = __this->get__pred_4();
		BaseIterator_t3696600956 * L_5 = __this->get__iter_3();
		NullCheck(L_4);
		double L_6 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		BaseIterator_t3696600956 * L_7 = __this->get__iter_3();
		NullCheck(L_7);
		int32_t L_8 = BaseIterator_get_ComparablePosition_m1201108051(L_7, /*hidden argument*/NULL);
		if ((((double)L_6) == ((double)(((double)((double)L_8))))))
		{
			goto IL_0052;
		}
	}
	{
		goto IL_00cc;
	}

IL_0052:
	{
		goto IL_00ca;
	}

IL_0057:
	{
		Expression_t4217024437 * L_9 = __this->get__pred_4();
		BaseIterator_t3696600956 * L_10 = __this->get__iter_3();
		NullCheck(L_9);
		Il2CppObject * L_11 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t3696600956 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_9, L_10);
		V_0 = L_11;
		Il2CppObject * L_12 = V_0;
		if (!((Il2CppObject *)IsInstSealed(L_12, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_0095;
		}
	}
	{
		Il2CppObject * L_13 = V_0;
		BaseIterator_t3696600956 * L_14 = __this->get__iter_3();
		NullCheck(L_14);
		int32_t L_15 = BaseIterator_get_ComparablePosition_m1201108051(L_14, /*hidden argument*/NULL);
		if ((((double)((*(double*)((double*)UnBox (L_13, Double_t534516614_il2cpp_TypeInfo_var))))) == ((double)(((double)((double)L_15))))))
		{
			goto IL_0090;
		}
	}
	{
		goto IL_00cc;
	}

IL_0090:
	{
		goto IL_00a5;
	}

IL_0095:
	{
		Il2CppObject * L_16 = V_0;
		bool L_17 = XPathFunctions_ToBoolean_m2654560037(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00cc;
	}

IL_00a5:
	{
		goto IL_00ca;
	}

IL_00aa:
	{
		Expression_t4217024437 * L_18 = __this->get__pred_4();
		BaseIterator_t3696600956 * L_19 = __this->get__iter_3();
		NullCheck(L_18);
		bool L_20 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_18, L_19);
		if (L_20)
		{
			goto IL_00c5;
		}
	}
	{
		goto IL_00cc;
	}

IL_00c5:
	{
		goto IL_00ca;
	}

IL_00ca:
	{
		return (bool)1;
	}

IL_00cc:
	{
		BaseIterator_t3696600956 * L_21 = __this->get__iter_3();
		NullCheck(L_21);
		bool L_22 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_21);
		if (L_22)
		{
			goto IL_0012;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.PredicateIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * PredicateIterator_get_Current_m508558563 (PredicateIterator_t4169199650 * __this, const MethodInfo* method)
{
	XPathNavigator_t1624538935 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1624538935 *)(NULL));
		goto IL_001c;
	}

IL_0011:
	{
		BaseIterator_t3696600956 * L_1 = __this->get__iter_3();
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.PredicateIterator::get_ReverseAxis()
extern "C"  bool PredicateIterator_get_ReverseAxis_m1577932535 (PredicateIterator_t4169199650 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XPath.BaseIterator::get_ReverseAxis() */, L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.PredicateIterator::ToString()
extern "C"  String_t* PredicateIterator_ToString_m891628812 (PredicateIterator_t4169199650 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.RelationalExpr::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void RelationalExpr__ctor_m506647938 (RelationalExpr_t4268017369 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = ___left0;
		Expression_t4217024437 * L_1 = ___right1;
		ExprBoolean__ctor_m1871519559(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Xml.XPath.RelationalExpr::get_StaticValueAsBoolean()
extern "C"  bool RelationalExpr_get_StaticValueAsBoolean_m1605638618 (RelationalExpr_t4268017369 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		Expression_t4217024437 * L_1 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		bool L_5 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, L_2, L_4);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002d;
	}

IL_002c:
	{
		G_B3_0 = 0;
	}

IL_002d:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.RelationalExpr::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t RelationalExpr_EvaluateBoolean_m1387308217_MetadataUsageId;
extern "C"  bool RelationalExpr_EvaluateBoolean_m1387308217 (RelationalExpr_t4268017369 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RelationalExpr_EvaluateBoolean_m1387308217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	Expression_t4217024437 * V_3 = NULL;
	Expression_t4217024437 * V_4 = NULL;
	int32_t V_5 = 0;
	bool V_6 = false;
	bool V_7 = false;
	BaseIterator_t3696600956 * V_8 = NULL;
	double V_9 = 0.0;
	BaseIterator_t3696600956 * V_10 = NULL;
	ArrayList_t2121638921 * V_11 = NULL;
	double V_12 = 0.0;
	int32_t V_13 = 0;
	{
		Expression_t4217024437 * L_0 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, BaseIterator_t3696600956 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t4217024437 * L_3 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, BaseIterator_t3696600956 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)5))))
		{
			goto IL_0033;
		}
	}
	{
		Expression_t4217024437 * L_7 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		BaseIterator_t3696600956 * L_8 = ___iter0;
		NullCheck(L_7);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t3696600956 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_7, L_8);
		int32_t L_10 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_0033:
	{
		int32_t L_11 = V_1;
		if ((!(((uint32_t)L_11) == ((uint32_t)5))))
		{
			goto IL_004c;
		}
	}
	{
		Expression_t4217024437 * L_12 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		BaseIterator_t3696600956 * L_13 = ___iter0;
		NullCheck(L_12);
		Il2CppObject * L_14 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t3696600956 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_12, L_13);
		int32_t L_15 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
	}

IL_004c:
	{
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_16) == ((uint32_t)4))))
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 1;
	}

IL_0055:
	{
		int32_t L_17 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)4))))
		{
			goto IL_005e;
		}
	}
	{
		V_1 = 1;
	}

IL_005e:
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)3)))
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_19 = V_1;
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_01cf;
		}
	}

IL_006c:
	{
		V_2 = (bool)0;
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)3)))
		{
			goto IL_0093;
		}
	}
	{
		V_2 = (bool)1;
		Expression_t4217024437 * L_21 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		V_3 = L_21;
		Expression_t4217024437 * L_22 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		V_4 = L_22;
		int32_t L_23 = V_0;
		V_5 = L_23;
		int32_t L_24 = V_1;
		V_0 = L_24;
		int32_t L_25 = V_5;
		V_1 = L_25;
		goto IL_00a2;
	}

IL_0093:
	{
		Expression_t4217024437 * L_26 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		V_3 = L_26;
		Expression_t4217024437 * L_27 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		V_4 = L_27;
	}

IL_00a2:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)2))))
		{
			goto IL_00d2;
		}
	}
	{
		Expression_t4217024437 * L_29 = V_3;
		BaseIterator_t3696600956 * L_30 = ___iter0;
		NullCheck(L_29);
		bool L_31 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_29, L_30);
		V_6 = L_31;
		Expression_t4217024437 * L_32 = V_4;
		BaseIterator_t3696600956 * L_33 = ___iter0;
		NullCheck(L_32);
		bool L_34 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_32, L_33);
		V_7 = L_34;
		bool L_35 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		double L_36 = Convert_ToDouble_m1966172364(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		bool L_37 = V_7;
		double L_38 = Convert_ToDouble_m1966172364(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		bool L_39 = V_2;
		bool L_40 = RelationalExpr_Compare_m3318673384(__this, L_36, L_38, L_39, /*hidden argument*/NULL);
		return L_40;
	}

IL_00d2:
	{
		Expression_t4217024437 * L_41 = V_3;
		BaseIterator_t3696600956 * L_42 = ___iter0;
		NullCheck(L_41);
		BaseIterator_t3696600956 * L_43 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_41, L_42);
		V_8 = L_43;
		int32_t L_44 = V_1;
		if (!L_44)
		{
			goto IL_00e8;
		}
	}
	{
		int32_t L_45 = V_1;
		if ((!(((uint32_t)L_45) == ((uint32_t)1))))
		{
			goto IL_0129;
		}
	}

IL_00e8:
	{
		Expression_t4217024437 * L_46 = V_4;
		BaseIterator_t3696600956 * L_47 = ___iter0;
		NullCheck(L_46);
		double L_48 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_46, L_47);
		V_9 = L_48;
		goto IL_0118;
	}

IL_00f7:
	{
		BaseIterator_t3696600956 * L_49 = V_8;
		NullCheck(L_49);
		XPathNavigator_t1624538935 * L_50 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_49);
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_50);
		double L_52 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		double L_53 = V_9;
		bool L_54 = V_2;
		bool L_55 = RelationalExpr_Compare_m3318673384(__this, L_52, L_53, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0118;
		}
	}
	{
		return (bool)1;
	}

IL_0118:
	{
		BaseIterator_t3696600956 * L_56 = V_8;
		NullCheck(L_56);
		bool L_57 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_56);
		if (L_57)
		{
			goto IL_00f7;
		}
	}
	{
		goto IL_01cd;
	}

IL_0129:
	{
		int32_t L_58 = V_1;
		if ((!(((uint32_t)L_58) == ((uint32_t)3))))
		{
			goto IL_01cd;
		}
	}
	{
		Expression_t4217024437 * L_59 = V_4;
		BaseIterator_t3696600956 * L_60 = ___iter0;
		NullCheck(L_59);
		BaseIterator_t3696600956 * L_61 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_59, L_60);
		V_10 = L_61;
		ArrayList_t2121638921 * L_62 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_62, /*hidden argument*/NULL);
		V_11 = L_62;
		goto IL_0164;
	}

IL_0146:
	{
		ArrayList_t2121638921 * L_63 = V_11;
		BaseIterator_t3696600956 * L_64 = V_8;
		NullCheck(L_64);
		XPathNavigator_t1624538935 * L_65 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_64);
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_65);
		double L_67 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		double L_68 = L_67;
		Il2CppObject * L_69 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_68);
		NullCheck(L_63);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_63, L_69);
	}

IL_0164:
	{
		BaseIterator_t3696600956 * L_70 = V_8;
		NullCheck(L_70);
		bool L_71 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_70);
		if (L_71)
		{
			goto IL_0146;
		}
	}
	{
		goto IL_01c1;
	}

IL_0175:
	{
		BaseIterator_t3696600956 * L_72 = V_10;
		NullCheck(L_72);
		XPathNavigator_t1624538935 * L_73 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_72);
		NullCheck(L_73);
		String_t* L_74 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_73);
		double L_75 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		V_12 = L_75;
		V_13 = 0;
		goto IL_01b3;
	}

IL_0190:
	{
		ArrayList_t2121638921 * L_76 = V_11;
		int32_t L_77 = V_13;
		NullCheck(L_76);
		Il2CppObject * L_78 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_76, L_77);
		double L_79 = V_12;
		bool L_80 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, ((*(double*)((double*)UnBox (L_78, Double_t534516614_il2cpp_TypeInfo_var)))), L_79);
		if (!L_80)
		{
			goto IL_01ad;
		}
	}
	{
		return (bool)1;
	}

IL_01ad:
	{
		int32_t L_81 = V_13;
		V_13 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_01b3:
	{
		int32_t L_82 = V_13;
		ArrayList_t2121638921 * L_83 = V_11;
		NullCheck(L_83);
		int32_t L_84 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_83);
		if ((((int32_t)L_82) < ((int32_t)L_84)))
		{
			goto IL_0190;
		}
	}

IL_01c1:
	{
		BaseIterator_t3696600956 * L_85 = V_10;
		NullCheck(L_85);
		bool L_86 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_85);
		if (L_86)
		{
			goto IL_0175;
		}
	}

IL_01cd:
	{
		return (bool)0;
	}

IL_01cf:
	{
		Expression_t4217024437 * L_87 = ((ExprBinary_t3205612403 *)__this)->get__left_0();
		BaseIterator_t3696600956 * L_88 = ___iter0;
		NullCheck(L_87);
		double L_89 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_87, L_88);
		Expression_t4217024437 * L_90 = ((ExprBinary_t3205612403 *)__this)->get__right_1();
		BaseIterator_t3696600956 * L_91 = ___iter0;
		NullCheck(L_90);
		double L_92 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_90, L_91);
		bool L_93 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, L_89, L_92);
		return L_93;
	}
}
// System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double,System.Boolean)
extern "C"  bool RelationalExpr_Compare_m3318673384 (RelationalExpr_t4268017369 * __this, double ___arg10, double ___arg21, bool ___fReverse2, const MethodInfo* method)
{
	{
		bool L_0 = ___fReverse2;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		double L_1 = ___arg21;
		double L_2 = ___arg10;
		bool L_3 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, L_1, L_2);
		return L_3;
	}

IL_000f:
	{
		double L_4 = ___arg10;
		double L_5 = ___arg21;
		bool L_6 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, L_4, L_5);
		return L_6;
	}
}
// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void SelfIterator__ctor_m2835358572 (SelfIterator_t2566309111 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void SelfIterator__ctor_m4150585400 (SelfIterator_t2566309111 * __this, XPathNavigator_t1624538935 * ___nav0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = ___nav0;
		Il2CppObject * L_1 = ___nsm1;
		SimpleIterator__ctor_m1024992466(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.SelfIterator,System.Boolean)
extern "C"  void SelfIterator__ctor_m3078894092 (SelfIterator_t2566309111 * __this, SelfIterator_t2566309111 * ___other0, bool ___clone1, const MethodInfo* method)
{
	{
		SelfIterator_t2566309111 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SelfIterator::Clone()
extern Il2CppClass* SelfIterator_t2566309111_il2cpp_TypeInfo_var;
extern const uint32_t SelfIterator_Clone_m3186820838_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * SelfIterator_Clone_m3186820838 (SelfIterator_t2566309111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SelfIterator_Clone_m3186820838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SelfIterator_t2566309111 * L_0 = (SelfIterator_t2566309111 *)il2cpp_codegen_object_new(SelfIterator_t2566309111_il2cpp_TypeInfo_var);
		SelfIterator__ctor_m3078894092(L_0, __this, (bool)1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.SelfIterator::MoveNextCore()
extern "C"  bool SelfIterator_MoveNextCore_m260577695 (SelfIterator_t2566309111 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)1;
	}

IL_000d:
	{
		return (bool)0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SelfIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * SelfIterator_get_Current_m4205100576 (SelfIterator_t2566309111 * __this, const MethodInfo* method)
{
	XPathNavigator_t1624538935 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1624538935 *)(NULL));
		goto IL_0017;
	}

IL_0011:
	{
		XPathNavigator_t1624538935 * L_1 = ((SimpleIterator_t1094113629 *)__this)->get__nav_3();
		G_B3_0 = L_1;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.SimpleIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void SimpleIterator__ctor_m1076221202 (SimpleIterator_t1094113629 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_2 = ___iter0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_2);
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		__this->set_skipfirst_5((bool)1);
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_4);
	}

IL_0025:
	{
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_5);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0042;
		}
	}
	{
		BaseIterator_t3696600956 * L_7 = ___iter0;
		NullCheck(L_7);
		XPathNavigator_t1624538935 * L_8 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_7);
		NullCheck(L_8);
		XPathNavigator_t1624538935 * L_9 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_8);
		__this->set__nav_3(L_9);
	}

IL_0042:
	{
		return;
	}
}
// System.Void System.Xml.XPath.SimpleIterator::.ctor(System.Xml.XPath.SimpleIterator,System.Boolean)
extern "C"  void SimpleIterator__ctor_m403425804 (SimpleIterator_t1094113629 * __this, SimpleIterator_t1094113629 * ___other0, bool ___clone1, const MethodInfo* method)
{
	SimpleIterator_t1094113629 * G_B3_0 = NULL;
	SimpleIterator_t1094113629 * G_B2_0 = NULL;
	XPathNavigator_t1624538935 * G_B4_0 = NULL;
	SimpleIterator_t1094113629 * G_B4_1 = NULL;
	{
		SimpleIterator_t1094113629 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		SimpleIterator_t1094113629 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = L_1->get__nav_3();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		bool L_3 = ___clone1;
		G_B2_0 = __this;
		if (!L_3)
		{
			G_B3_0 = __this;
			goto IL_0029;
		}
	}
	{
		SimpleIterator_t1094113629 * L_4 = ___other0;
		NullCheck(L_4);
		XPathNavigator_t1624538935 * L_5 = L_4->get__nav_3();
		NullCheck(L_5);
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_5);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		goto IL_002f;
	}

IL_0029:
	{
		SimpleIterator_t1094113629 * L_7 = ___other0;
		NullCheck(L_7);
		XPathNavigator_t1624538935 * L_8 = L_7->get__nav_3();
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_002f:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__nav_3(G_B4_0);
	}

IL_0034:
	{
		SimpleIterator_t1094113629 * L_9 = ___other0;
		NullCheck(L_9);
		bool L_10 = L_9->get_skipfirst_5();
		__this->set_skipfirst_5(L_10);
		return;
	}
}
// System.Void System.Xml.XPath.SimpleIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void SimpleIterator__ctor_m1024992466 (SimpleIterator_t1094113629 * __this, XPathNavigator_t1624538935 * ___nav0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___nsm1;
		BaseIterator__ctor_m3826808382(__this, L_0, /*hidden argument*/NULL);
		XPathNavigator_t1624538935 * L_1 = ___nav0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_1);
		__this->set__nav_3(L_2);
		return;
	}
}
// System.Boolean System.Xml.XPath.SimpleIterator::MoveNext()
extern "C"  bool SimpleIterator_MoveNext_m2872354886 (SimpleIterator_t1094113629 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_skipfirst_5();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		XPathNavigator_t1624538935 * L_1 = __this->get__nav_3();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_skipfirst_5((bool)0);
		BaseIterator_SetPosition_m2844760437(__this, 1, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0028:
	{
		bool L_2 = BaseIterator_MoveNext_m2848019493(__this, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * SimpleIterator_get_Current_m1974708922 (SimpleIterator_t1094113629 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (XPathNavigator_t1624538935 *)NULL;
	}

IL_000d:
	{
		XPathNavigator_t1624538935 * L_1 = __this->get__nav_3();
		__this->set__current_4(L_1);
		XPathNavigator_t1624538935 * L_2 = __this->get__current_4();
		return L_2;
	}
}
// System.Void System.Xml.XPath.SimpleSlashIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.NodeSet)
extern "C"  void SimpleSlashIterator__ctor_m3208158132 (SimpleSlashIterator_t2349635926 * __this, BaseIterator_t3696600956 * ___left0, NodeSet_t3503134685 * ___expr1, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___left0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_2 = ___left0;
		__this->set__left_4(L_2);
		NodeSet_t3503134685 * L_3 = ___expr1;
		__this->set__expr_3(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.SimpleSlashIterator::.ctor(System.Xml.XPath.SimpleSlashIterator)
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern const uint32_t SimpleSlashIterator__ctor_m1778448769_MetadataUsageId;
extern "C"  void SimpleSlashIterator__ctor_m1778448769 (SimpleSlashIterator_t2349635926 * __this, SimpleSlashIterator_t2349635926 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleSlashIterator__ctor_m1778448769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SimpleSlashIterator_t2349635926 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		SimpleSlashIterator_t2349635926 * L_1 = ___other0;
		NullCheck(L_1);
		NodeSet_t3503134685 * L_2 = L_1->get__expr_3();
		__this->set__expr_3(L_2);
		SimpleSlashIterator_t2349635926 * L_3 = ___other0;
		NullCheck(L_3);
		BaseIterator_t3696600956 * L_4 = L_3->get__left_4();
		NullCheck(L_4);
		XPathNodeIterator_t2394191562 * L_5 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_4);
		__this->set__left_4(((BaseIterator_t3696600956 *)CastclassClass(L_5, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
		SimpleSlashIterator_t2349635926 * L_6 = ___other0;
		NullCheck(L_6);
		BaseIterator_t3696600956 * L_7 = L_6->get__right_5();
		if (!L_7)
		{
			goto IL_004a;
		}
	}
	{
		SimpleSlashIterator_t2349635926 * L_8 = ___other0;
		NullCheck(L_8);
		BaseIterator_t3696600956 * L_9 = L_8->get__right_5();
		NullCheck(L_9);
		XPathNodeIterator_t2394191562 * L_10 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_9);
		__this->set__right_5(((BaseIterator_t3696600956 *)CastclassClass(L_10, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
	}

IL_004a:
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SimpleSlashIterator::Clone()
extern Il2CppClass* SimpleSlashIterator_t2349635926_il2cpp_TypeInfo_var;
extern const uint32_t SimpleSlashIterator_Clone_m2687801187_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * SimpleSlashIterator_Clone_m2687801187 (SimpleSlashIterator_t2349635926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleSlashIterator_Clone_m2687801187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SimpleSlashIterator_t2349635926 * L_0 = (SimpleSlashIterator_t2349635926 *)il2cpp_codegen_object_new(SimpleSlashIterator_t2349635926_il2cpp_TypeInfo_var);
		SimpleSlashIterator__ctor_m1778448769(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.SimpleSlashIterator::MoveNextCore()
extern "C"  bool SimpleSlashIterator_MoveNextCore_m3882540316 (SimpleSlashIterator_t2349635926 * __this, const MethodInfo* method)
{
	{
		goto IL_002e;
	}

IL_0005:
	{
		BaseIterator_t3696600956 * L_0 = __this->get__left_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_0);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		NodeSet_t3503134685 * L_2 = __this->get__expr_3();
		BaseIterator_t3696600956 * L_3 = __this->get__left_4();
		NullCheck(L_2);
		BaseIterator_t3696600956 * L_4 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_2, L_3);
		__this->set__right_5(L_4);
	}

IL_002e:
	{
		BaseIterator_t3696600956 * L_5 = __this->get__right_5();
		if (!L_5)
		{
			goto IL_0005;
		}
	}
	{
		BaseIterator_t3696600956 * L_6 = __this->get__right_5();
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_6);
		if (!L_7)
		{
			goto IL_0005;
		}
	}
	{
		XPathNavigator_t1624538935 * L_8 = __this->get__current_6();
		if (L_8)
		{
			goto IL_006f;
		}
	}
	{
		BaseIterator_t3696600956 * L_9 = __this->get__right_5();
		NullCheck(L_9);
		XPathNavigator_t1624538935 * L_10 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_9);
		NullCheck(L_10);
		XPathNavigator_t1624538935 * L_11 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_10);
		__this->set__current_6(L_11);
		goto IL_00a0;
	}

IL_006f:
	{
		XPathNavigator_t1624538935 * L_12 = __this->get__current_6();
		BaseIterator_t3696600956 * L_13 = __this->get__right_5();
		NullCheck(L_13);
		XPathNavigator_t1624538935 * L_14 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_13);
		NullCheck(L_12);
		bool L_15 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_12, L_14);
		if (L_15)
		{
			goto IL_00a0;
		}
	}
	{
		BaseIterator_t3696600956 * L_16 = __this->get__right_5();
		NullCheck(L_16);
		XPathNavigator_t1624538935 * L_17 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_16);
		NullCheck(L_17);
		XPathNavigator_t1624538935 * L_18 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_17);
		__this->set__current_6(L_18);
	}

IL_00a0:
	{
		return (bool)1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleSlashIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * SimpleSlashIterator_get_Current_m3159180143 (SimpleSlashIterator_t2349635926 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get__current_6();
		return L_0;
	}
}
// System.Void System.Xml.XPath.SlashIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.NodeSet)
extern "C"  void SlashIterator__ctor_m2348632994 (SlashIterator_t818098664 * __this, BaseIterator_t3696600956 * ___iter0, NodeSet_t3503134685 * ___expr1, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_2 = ___iter0;
		__this->set__iterLeft_3(L_2);
		NodeSet_t3503134685 * L_3 = ___expr1;
		__this->set__expr_5(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.SlashIterator::.ctor(System.Xml.XPath.SlashIterator)
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedList_t3322490541_il2cpp_TypeInfo_var;
extern const uint32_t SlashIterator__ctor_m1699614173_MetadataUsageId;
extern "C"  void SlashIterator__ctor_m1699614173 (SlashIterator_t818098664 * __this, SlashIterator_t818098664 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SlashIterator__ctor_m1699614173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlashIterator_t818098664 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		SlashIterator_t818098664 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t3696600956 * L_2 = L_1->get__iterLeft_3();
		NullCheck(L_2);
		XPathNodeIterator_t2394191562 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__iterLeft_3(((BaseIterator_t3696600956 *)CastclassClass(L_3, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
		SlashIterator_t818098664 * L_4 = ___other0;
		NullCheck(L_4);
		BaseIterator_t3696600956 * L_5 = L_4->get__iterRight_4();
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		SlashIterator_t818098664 * L_6 = ___other0;
		NullCheck(L_6);
		BaseIterator_t3696600956 * L_7 = L_6->get__iterRight_4();
		NullCheck(L_7);
		XPathNodeIterator_t2394191562 * L_8 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_7);
		__this->set__iterRight_4(((BaseIterator_t3696600956 *)CastclassClass(L_8, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
	}

IL_003e:
	{
		SlashIterator_t818098664 * L_9 = ___other0;
		NullCheck(L_9);
		NodeSet_t3503134685 * L_10 = L_9->get__expr_5();
		__this->set__expr_5(L_10);
		SlashIterator_t818098664 * L_11 = ___other0;
		NullCheck(L_11);
		SortedList_t3322490541 * L_12 = L_11->get__iterList_6();
		if (!L_12)
		{
			goto IL_006b;
		}
	}
	{
		SlashIterator_t818098664 * L_13 = ___other0;
		NullCheck(L_13);
		SortedList_t3322490541 * L_14 = L_13->get__iterList_6();
		NullCheck(L_14);
		Il2CppObject * L_15 = VirtFuncInvoker0< Il2CppObject * >::Invoke(38 /* System.Object System.Collections.SortedList::Clone() */, L_14);
		__this->set__iterList_6(((SortedList_t3322490541 *)CastclassClass(L_15, SortedList_t3322490541_il2cpp_TypeInfo_var)));
	}

IL_006b:
	{
		SlashIterator_t818098664 * L_16 = ___other0;
		NullCheck(L_16);
		bool L_17 = L_16->get__finished_7();
		__this->set__finished_7(L_17);
		SlashIterator_t818098664 * L_18 = ___other0;
		NullCheck(L_18);
		BaseIterator_t3696600956 * L_19 = L_18->get__nextIterRight_8();
		if (!L_19)
		{
			goto IL_0098;
		}
	}
	{
		SlashIterator_t818098664 * L_20 = ___other0;
		NullCheck(L_20);
		BaseIterator_t3696600956 * L_21 = L_20->get__nextIterRight_8();
		NullCheck(L_21);
		XPathNodeIterator_t2394191562 * L_22 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_21);
		__this->set__nextIterRight_8(((BaseIterator_t3696600956 *)CastclassClass(L_22, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
	}

IL_0098:
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SlashIterator::Clone()
extern Il2CppClass* SlashIterator_t818098664_il2cpp_TypeInfo_var;
extern const uint32_t SlashIterator_Clone_m3185525137_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * SlashIterator_Clone_m3185525137 (SlashIterator_t818098664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SlashIterator_Clone_m3185525137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlashIterator_t818098664 * L_0 = (SlashIterator_t818098664 *)il2cpp_codegen_object_new(SlashIterator_t818098664_il2cpp_TypeInfo_var);
		SlashIterator__ctor_m1699614173(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.SlashIterator::MoveNextCore()
extern Il2CppClass* XPathIteratorComparer_t4238022261_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedList_t3322490541_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern const uint32_t SlashIterator_MoveNextCore_m2132081902_MetadataUsageId;
extern "C"  bool SlashIterator_MoveNextCore_m2132081902 (SlashIterator_t818098664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SlashIterator_MoveNextCore_m2132081902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		bool L_0 = __this->get__finished_7();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		BaseIterator_t3696600956 * L_1 = __this->get__iterRight_4();
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		BaseIterator_t3696600956 * L_2 = __this->get__iterLeft_3();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_2);
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		NodeSet_t3503134685 * L_4 = __this->get__expr_5();
		BaseIterator_t3696600956 * L_5 = __this->get__iterLeft_3();
		NullCheck(L_4);
		BaseIterator_t3696600956 * L_6 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		__this->set__iterRight_4(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(XPathIteratorComparer_t4238022261_il2cpp_TypeInfo_var);
		XPathIteratorComparer_t4238022261 * L_7 = ((XPathIteratorComparer_t4238022261_StaticFields*)XPathIteratorComparer_t4238022261_il2cpp_TypeInfo_var->static_fields)->get_Instance_0();
		SortedList_t3322490541 * L_8 = (SortedList_t3322490541 *)il2cpp_codegen_object_new(SortedList_t3322490541_il2cpp_TypeInfo_var);
		SortedList__ctor_m3446713763(L_8, L_7, /*hidden argument*/NULL);
		__this->set__iterList_6(L_8);
	}

IL_0051:
	{
		goto IL_00f0;
	}

IL_0056:
	{
		SortedList_t3322490541 * L_9 = __this->get__iterList_6();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.SortedList::get_Count() */, L_9);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_009d;
		}
	}
	{
		SortedList_t3322490541 * L_11 = __this->get__iterList_6();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.SortedList::get_Count() */, L_11);
		V_0 = ((int32_t)((int32_t)L_12-(int32_t)1));
		SortedList_t3322490541 * L_13 = __this->get__iterList_6();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(44 /* System.Object System.Collections.SortedList::GetByIndex(System.Int32) */, L_13, L_14);
		__this->set__iterRight_4(((BaseIterator_t3696600956 *)CastclassClass(L_15, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
		SortedList_t3322490541 * L_16 = __this->get__iterList_6();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.SortedList::RemoveAt(System.Int32) */, L_16, L_17);
		goto IL_0100;
	}

IL_009d:
	{
		BaseIterator_t3696600956 * L_18 = __this->get__nextIterRight_8();
		if (!L_18)
		{
			goto IL_00c0;
		}
	}
	{
		BaseIterator_t3696600956 * L_19 = __this->get__nextIterRight_8();
		__this->set__iterRight_4(L_19);
		__this->set__nextIterRight_8((BaseIterator_t3696600956 *)NULL);
		goto IL_0100;
	}

IL_00c0:
	{
		BaseIterator_t3696600956 * L_20 = __this->get__iterLeft_3();
		NullCheck(L_20);
		bool L_21 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_20);
		if (L_21)
		{
			goto IL_00d9;
		}
	}
	{
		__this->set__finished_7((bool)1);
		return (bool)0;
	}

IL_00d9:
	{
		NodeSet_t3503134685 * L_22 = __this->get__expr_5();
		BaseIterator_t3696600956 * L_23 = __this->get__iterLeft_3();
		NullCheck(L_22);
		BaseIterator_t3696600956 * L_24 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_22, L_23);
		__this->set__iterRight_4(L_24);
	}

IL_00f0:
	{
		BaseIterator_t3696600956 * L_25 = __this->get__iterRight_4();
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_25);
		if (!L_26)
		{
			goto IL_0056;
		}
	}

IL_0100:
	{
		V_1 = (bool)1;
		goto IL_025e;
	}

IL_0107:
	{
		V_1 = (bool)0;
		BaseIterator_t3696600956 * L_27 = __this->get__nextIterRight_8();
		if (L_27)
		{
			goto IL_0176;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_014e;
	}

IL_011b:
	{
		BaseIterator_t3696600956 * L_28 = __this->get__iterLeft_3();
		NullCheck(L_28);
		bool L_29 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_28);
		if (!L_29)
		{
			goto IL_0147;
		}
	}
	{
		NodeSet_t3503134685 * L_30 = __this->get__expr_5();
		BaseIterator_t3696600956 * L_31 = __this->get__iterLeft_3();
		NullCheck(L_30);
		BaseIterator_t3696600956 * L_32 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_30, L_31);
		__this->set__nextIterRight_8(L_32);
		goto IL_014e;
	}

IL_0147:
	{
		V_2 = (bool)1;
		goto IL_0169;
	}

IL_014e:
	{
		BaseIterator_t3696600956 * L_33 = __this->get__nextIterRight_8();
		if (!L_33)
		{
			goto IL_011b;
		}
	}
	{
		BaseIterator_t3696600956 * L_34 = __this->get__nextIterRight_8();
		NullCheck(L_34);
		bool L_35 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_34);
		if (!L_35)
		{
			goto IL_011b;
		}
	}

IL_0169:
	{
		bool L_36 = V_2;
		if (!L_36)
		{
			goto IL_0176;
		}
	}
	{
		__this->set__nextIterRight_8((BaseIterator_t3696600956 *)NULL);
	}

IL_0176:
	{
		BaseIterator_t3696600956 * L_37 = __this->get__nextIterRight_8();
		if (!L_37)
		{
			goto IL_025e;
		}
	}
	{
		BaseIterator_t3696600956 * L_38 = __this->get__iterRight_4();
		NullCheck(L_38);
		XPathNavigator_t1624538935 * L_39 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_38);
		BaseIterator_t3696600956 * L_40 = __this->get__nextIterRight_8();
		NullCheck(L_40);
		XPathNavigator_t1624538935 * L_41 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_40);
		NullCheck(L_39);
		int32_t L_42 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_39, L_41);
		V_4 = L_42;
		int32_t L_43 = V_4;
		if ((((int32_t)L_43) == ((int32_t)1)))
		{
			goto IL_01b3;
		}
	}
	{
		int32_t L_44 = V_4;
		if ((((int32_t)L_44) == ((int32_t)2)))
		{
			goto IL_01e4;
		}
	}
	{
		goto IL_025e;
	}

IL_01b3:
	{
		SortedList_t3322490541 * L_45 = __this->get__iterList_6();
		BaseIterator_t3696600956 * L_46 = __this->get__iterRight_4();
		BaseIterator_t3696600956 * L_47 = __this->get__iterRight_4();
		NullCheck(L_45);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.SortedList::set_Item(System.Object,System.Object) */, L_45, L_46, L_47);
		BaseIterator_t3696600956 * L_48 = __this->get__nextIterRight_8();
		__this->set__iterRight_4(L_48);
		__this->set__nextIterRight_8((BaseIterator_t3696600956 *)NULL);
		V_1 = (bool)1;
		goto IL_025e;
	}

IL_01e4:
	{
		BaseIterator_t3696600956 * L_49 = __this->get__nextIterRight_8();
		NullCheck(L_49);
		bool L_50 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_49);
		if (L_50)
		{
			goto IL_0200;
		}
	}
	{
		__this->set__nextIterRight_8((BaseIterator_t3696600956 *)NULL);
		goto IL_0257;
	}

IL_0200:
	{
		SortedList_t3322490541 * L_51 = __this->get__iterList_6();
		NullCheck(L_51);
		int32_t L_52 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.SortedList::get_Count() */, L_51);
		V_3 = L_52;
		SortedList_t3322490541 * L_53 = __this->get__iterList_6();
		BaseIterator_t3696600956 * L_54 = __this->get__nextIterRight_8();
		BaseIterator_t3696600956 * L_55 = __this->get__nextIterRight_8();
		NullCheck(L_53);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.SortedList::set_Item(System.Object,System.Object) */, L_53, L_54, L_55);
		int32_t L_56 = V_3;
		SortedList_t3322490541 * L_57 = __this->get__iterList_6();
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.SortedList::get_Count() */, L_57);
		if ((((int32_t)L_56) == ((int32_t)L_58)))
		{
			goto IL_0257;
		}
	}
	{
		SortedList_t3322490541 * L_59 = __this->get__iterList_6();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		Il2CppObject * L_61 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(44 /* System.Object System.Collections.SortedList::GetByIndex(System.Int32) */, L_59, L_60);
		__this->set__nextIterRight_8(((BaseIterator_t3696600956 *)CastclassClass(L_61, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
		SortedList_t3322490541 * L_62 = __this->get__iterList_6();
		int32_t L_63 = V_3;
		NullCheck(L_62);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.SortedList::RemoveAt(System.Int32) */, L_62, L_63);
	}

IL_0257:
	{
		V_1 = (bool)1;
		goto IL_025e;
	}

IL_025e:
	{
		bool L_64 = V_1;
		if (L_64)
		{
			goto IL_0107;
		}
	}
	{
		return (bool)1;
	}
	// Dead block : IL_0266: br IL_0051
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SlashIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * SlashIterator_get_Current_m812878301 (SlashIterator_t818098664 * __this, const MethodInfo* method)
{
	XPathNavigator_t1624538935 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1624538935 *)(NULL));
		goto IL_001c;
	}

IL_0011:
	{
		BaseIterator_t3696600956 * L_1 = __this->get__iterRight_4();
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.SortedIterator::.ctor(System.Xml.XPath.BaseIterator)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var;
extern const uint32_t SortedIterator__ctor_m2178587357_MetadataUsageId;
extern "C"  void SortedIterator__ctor_m2178587357 (SortedIterator_t269320040 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SortedIterator__ctor_m2178587357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1624538935 * V_0 = NULL;
	int32_t V_1 = 0;
	XPathNavigator_t1624538935 * V_2 = NULL;
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		ArrayList_t2121638921 * L_2 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_2, /*hidden argument*/NULL);
		__this->set_list_3(L_2);
		goto IL_0033;
	}

IL_001c:
	{
		ArrayList_t2121638921 * L_3 = __this->get_list_3();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t1624538935 * L_5 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_5);
		NullCheck(L_3);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_3, L_6);
	}

IL_0033:
	{
		BaseIterator_t3696600956 * L_7 = ___iter0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_7);
		if (L_8)
		{
			goto IL_001c;
		}
	}
	{
		ArrayList_t2121638921 * L_9 = __this->get_list_3();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_9);
		if (L_10)
		{
			goto IL_004f;
		}
	}
	{
		return;
	}

IL_004f:
	{
		ArrayList_t2121638921 * L_11 = __this->get_list_3();
		NullCheck(L_11);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_11, 0);
		V_0 = ((XPathNavigator_t1624538935 *)CastclassClass(L_12, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
		ArrayList_t2121638921 * L_13 = __this->get_list_3();
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var);
		XPathNavigatorComparer_t877816196 * L_14 = ((XPathNavigatorComparer_t877816196_StaticFields*)XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var->static_fields)->get_Instance_0();
		NullCheck(L_13);
		VirtActionInvoker1< Il2CppObject * >::Invoke(46 /* System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer) */, L_13, L_14);
		V_1 = 1;
		goto IL_00b1;
	}

IL_0078:
	{
		ArrayList_t2121638921 * L_15 = __this->get_list_3();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		Il2CppObject * L_17 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_15, L_16);
		V_2 = ((XPathNavigator_t1624538935 *)CastclassClass(L_17, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
		XPathNavigator_t1624538935 * L_18 = V_0;
		XPathNavigator_t1624538935 * L_19 = V_2;
		NullCheck(L_18);
		bool L_20 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_18, L_19);
		if (!L_20)
		{
			goto IL_00ab;
		}
	}
	{
		ArrayList_t2121638921 * L_21 = __this->get_list_3();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_21, L_22);
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23-(int32_t)1));
		goto IL_00ad;
	}

IL_00ab:
	{
		XPathNavigator_t1624538935 * L_24 = V_2;
		V_0 = L_24;
	}

IL_00ad:
	{
		int32_t L_25 = V_1;
		V_1 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_26 = V_1;
		ArrayList_t2121638921 * L_27 = __this->get_list_3();
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_27);
		if ((((int32_t)L_26) < ((int32_t)L_28)))
		{
			goto IL_0078;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XPath.SortedIterator::.ctor(System.Xml.XPath.SortedIterator)
extern "C"  void SortedIterator__ctor_m3092690865 (SortedIterator_t269320040 * __this, SortedIterator_t269320040 * ___other0, const MethodInfo* method)
{
	{
		SortedIterator_t269320040 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		SortedIterator_t269320040 * L_1 = ___other0;
		NullCheck(L_1);
		ArrayList_t2121638921 * L_2 = L_1->get_list_3();
		__this->set_list_3(L_2);
		SortedIterator_t269320040 * L_3 = ___other0;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_3);
		BaseIterator_SetPosition_m2844760437(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SortedIterator::Clone()
extern Il2CppClass* SortedIterator_t269320040_il2cpp_TypeInfo_var;
extern const uint32_t SortedIterator_Clone_m3148712725_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * SortedIterator_Clone_m3148712725 (SortedIterator_t269320040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SortedIterator_Clone_m3148712725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SortedIterator_t269320040 * L_0 = (SortedIterator_t269320040 *)il2cpp_codegen_object_new(SortedIterator_t269320040_il2cpp_TypeInfo_var);
		SortedIterator__ctor_m3092690865(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.SortedIterator::MoveNextCore()
extern "C"  bool SortedIterator_MoveNextCore_m1723129296 (SortedIterator_t269320040 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		ArrayList_t2121638921 * L_1 = __this->get_list_3();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		return (bool)((((int32_t)L_0) < ((int32_t)L_2))? 1 : 0);
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SortedIterator::get_Current()
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern const uint32_t SortedIterator_get_Current_m2806041935_MetadataUsageId;
extern "C"  XPathNavigator_t1624538935 * SortedIterator_get_Current_m2806041935 (SortedIterator_t269320040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SortedIterator_get_Current_m2806041935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1624538935 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1624538935 *)(NULL));
		goto IL_0029;
	}

IL_0011:
	{
		ArrayList_t2121638921 * L_1 = __this->get_list_3();
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		NullCheck(L_1);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)));
		G_B3_0 = ((XPathNavigator_t1624538935 *)CastclassClass(L_3, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Int32 System.Xml.XPath.SortedIterator::get_Count()
extern "C"  int32_t SortedIterator_get_Count_m3817323349 (SortedIterator_t269320040 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_list_3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.UnionIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.BaseIterator,System.Xml.XPath.BaseIterator)
extern "C"  void UnionIterator__ctor_m4002392457 (UnionIterator_t1824978234 * __this, BaseIterator_t3696600956 * ___iter0, BaseIterator_t3696600956 * ___left1, BaseIterator_t3696600956 * ___right2, const MethodInfo* method)
{
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_2 = ___left1;
		__this->set__left_3(L_2);
		BaseIterator_t3696600956 * L_3 = ___right2;
		__this->set__right_4(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.UnionIterator::.ctor(System.Xml.XPath.UnionIterator)
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern const uint32_t UnionIterator__ctor_m1987098937_MetadataUsageId;
extern "C"  void UnionIterator__ctor_m1987098937 (UnionIterator_t1824978234 * __this, UnionIterator_t1824978234 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator__ctor_m1987098937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnionIterator_t1824978234 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		UnionIterator_t1824978234 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t3696600956 * L_2 = L_1->get__left_3();
		NullCheck(L_2);
		XPathNodeIterator_t2394191562 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__left_3(((BaseIterator_t3696600956 *)CastclassClass(L_3, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
		UnionIterator_t1824978234 * L_4 = ___other0;
		NullCheck(L_4);
		BaseIterator_t3696600956 * L_5 = L_4->get__right_4();
		NullCheck(L_5);
		XPathNodeIterator_t2394191562 * L_6 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_5);
		__this->set__right_4(((BaseIterator_t3696600956 *)CastclassClass(L_6, BaseIterator_t3696600956_il2cpp_TypeInfo_var)));
		UnionIterator_t1824978234 * L_7 = ___other0;
		NullCheck(L_7);
		bool L_8 = L_7->get_keepLeft_5();
		__this->set_keepLeft_5(L_8);
		UnionIterator_t1824978234 * L_9 = ___other0;
		NullCheck(L_9);
		bool L_10 = L_9->get_keepRight_6();
		__this->set_keepRight_6(L_10);
		UnionIterator_t1824978234 * L_11 = ___other0;
		NullCheck(L_11);
		XPathNavigator_t1624538935 * L_12 = L_11->get__current_7();
		if (!L_12)
		{
			goto IL_0067;
		}
	}
	{
		UnionIterator_t1824978234 * L_13 = ___other0;
		NullCheck(L_13);
		XPathNavigator_t1624538935 * L_14 = L_13->get__current_7();
		NullCheck(L_14);
		XPathNavigator_t1624538935 * L_15 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_14);
		__this->set__current_7(L_15);
	}

IL_0067:
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.UnionIterator::Clone()
extern Il2CppClass* UnionIterator_t1824978234_il2cpp_TypeInfo_var;
extern const uint32_t UnionIterator_Clone_m2246908543_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * UnionIterator_Clone_m2246908543 (UnionIterator_t1824978234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_Clone_m2246908543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnionIterator_t1824978234 * L_0 = (UnionIterator_t1824978234 *)il2cpp_codegen_object_new(UnionIterator_t1824978234_il2cpp_TypeInfo_var);
		UnionIterator__ctor_m1987098937(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.UnionIterator::MoveNextCore()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91958834;
extern const uint32_t UnionIterator_MoveNextCore_m2409356352_MetadataUsageId;
extern "C"  bool UnionIterator_MoveNextCore_m2409356352 (UnionIterator_t1824978234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_MoveNextCore_m2409356352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		bool L_0 = __this->get_keepLeft_5();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		BaseIterator_t3696600956 * L_1 = __this->get__left_3();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_1);
		__this->set_keepLeft_5(L_2);
	}

IL_001c:
	{
		bool L_3 = __this->get_keepRight_6();
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		BaseIterator_t3696600956 * L_4 = __this->get__right_4();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_4);
		__this->set_keepRight_6(L_5);
	}

IL_0038:
	{
		bool L_6 = __this->get_keepLeft_5();
		if (L_6)
		{
			goto IL_0050;
		}
	}
	{
		bool L_7 = __this->get_keepRight_6();
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		return (bool)0;
	}

IL_0050:
	{
		bool L_8 = __this->get_keepRight_6();
		if (L_8)
		{
			goto IL_0070;
		}
	}
	{
		__this->set_keepLeft_5((bool)0);
		BaseIterator_t3696600956 * L_9 = __this->get__left_3();
		UnionIterator_SetCurrent_m1765329958(__this, L_9, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0070:
	{
		bool L_10 = __this->get_keepLeft_5();
		if (L_10)
		{
			goto IL_0090;
		}
	}
	{
		__this->set_keepRight_6((bool)0);
		BaseIterator_t3696600956 * L_11 = __this->get__right_4();
		UnionIterator_SetCurrent_m1765329958(__this, L_11, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0090:
	{
		BaseIterator_t3696600956 * L_12 = __this->get__left_3();
		NullCheck(L_12);
		XPathNavigator_t1624538935 * L_13 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_12);
		BaseIterator_t3696600956 * L_14 = __this->get__right_4();
		NullCheck(L_14);
		XPathNavigator_t1624538935 * L_15 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_14);
		NullCheck(L_13);
		int32_t L_16 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_13, L_15);
		V_0 = L_16;
		int32_t L_17 = V_0;
		if (L_17 == 0)
		{
			goto IL_00e5;
		}
		if (L_17 == 1)
		{
			goto IL_00fa;
		}
		if (L_17 == 2)
		{
			goto IL_00c7;
		}
		if (L_17 == 3)
		{
			goto IL_00e5;
		}
	}
	{
		goto IL_010f;
	}

IL_00c7:
	{
		int32_t L_18 = 0;
		V_1 = (bool)L_18;
		__this->set_keepRight_6((bool)L_18);
		bool L_19 = V_1;
		__this->set_keepLeft_5(L_19);
		BaseIterator_t3696600956 * L_20 = __this->get__right_4();
		UnionIterator_SetCurrent_m1765329958(__this, L_20, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_00e5:
	{
		__this->set_keepLeft_5((bool)0);
		BaseIterator_t3696600956 * L_21 = __this->get__left_3();
		UnionIterator_SetCurrent_m1765329958(__this, L_21, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_00fa:
	{
		__this->set_keepRight_6((bool)0);
		BaseIterator_t3696600956 * L_22 = __this->get__right_4();
		UnionIterator_SetCurrent_m1765329958(__this, L_22, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_010f:
	{
		InvalidOperationException_t2420574324 * L_23 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_23, _stringLiteral91958834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23);
	}
}
// System.Void System.Xml.XPath.UnionIterator::SetCurrent(System.Xml.XPath.XPathNodeIterator)
extern "C"  void UnionIterator_SetCurrent_m1765329958 (UnionIterator_t1824978234 * __this, XPathNodeIterator_t2394191562 * ___iter0, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get__current_7();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		XPathNodeIterator_t2394191562 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		XPathNavigator_t1624538935 * L_3 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_2);
		__this->set__current_7(L_3);
		goto IL_0048;
	}

IL_0021:
	{
		XPathNavigator_t1624538935 * L_4 = __this->get__current_7();
		XPathNodeIterator_t2394191562 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_4);
		bool L_7 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_4, L_6);
		if (L_7)
		{
			goto IL_0048;
		}
	}
	{
		XPathNodeIterator_t2394191562 * L_8 = ___iter0;
		NullCheck(L_8);
		XPathNavigator_t1624538935 * L_9 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_8);
		NullCheck(L_9);
		XPathNavigator_t1624538935 * L_10 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_9);
		__this->set__current_7(L_10);
	}

IL_0048:
	{
		return;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.UnionIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * UnionIterator_get_Current_m1791653963 (UnionIterator_t1824978234 * __this, const MethodInfo* method)
{
	XPathNavigator_t1624538935 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		XPathNavigator_t1624538935 * L_1 = __this->get__current_7();
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = ((XPathNavigator_t1624538935 *)(NULL));
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.WrapperIterator::.ctor(System.Xml.XPath.XPathNodeIterator,System.Xml.IXmlNamespaceResolver)
extern "C"  void WrapperIterator__ctor_m802118820 (WrapperIterator_t2702681854 * __this, XPathNodeIterator_t2394191562 * ___iter0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___nsm1;
		BaseIterator__ctor_m3826808382(__this, L_0, /*hidden argument*/NULL);
		XPathNodeIterator_t2394191562 * L_1 = ___iter0;
		__this->set_iter_3(L_1);
		return;
	}
}
// System.Void System.Xml.XPath.WrapperIterator::.ctor(System.Xml.XPath.WrapperIterator)
extern "C"  void WrapperIterator__ctor_m2127158577 (WrapperIterator_t2702681854 * __this, WrapperIterator_t2702681854 * ___other0, const MethodInfo* method)
{
	{
		WrapperIterator_t2702681854 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		WrapperIterator_t2702681854 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNodeIterator_t2394191562 * L_2 = L_1->get_iter_3();
		NullCheck(L_2);
		XPathNodeIterator_t2394191562 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set_iter_3(L_3);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.WrapperIterator::Clone()
extern Il2CppClass* WrapperIterator_t2702681854_il2cpp_TypeInfo_var;
extern const uint32_t WrapperIterator_Clone_m3191539131_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * WrapperIterator_Clone_m3191539131 (WrapperIterator_t2702681854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WrapperIterator_Clone_m3191539131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WrapperIterator_t2702681854 * L_0 = (WrapperIterator_t2702681854 *)il2cpp_codegen_object_new(WrapperIterator_t2702681854_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m2127158577(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.WrapperIterator::MoveNextCore()
extern "C"  bool WrapperIterator_MoveNextCore_m1284174404 (WrapperIterator_t2702681854 * __this, const MethodInfo* method)
{
	{
		XPathNodeIterator_t2394191562 * L_0 = __this->get_iter_3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_0);
		return L_1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.WrapperIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * WrapperIterator_get_Current_m4028081223 (WrapperIterator_t2702681854 * __this, const MethodInfo* method)
{
	{
		XPathNodeIterator_t2394191562 * L_0 = __this->get_iter_3();
		NullCheck(L_0);
		XPathNavigator_t1624538935 * L_1 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.XPathBooleanFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathBooleanFunction__ctor_m3002905248 (XPathBooleanFunction_t2634824384 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathBooleanFunction::get_ReturnType()
extern "C"  int32_t XPathBooleanFunction_get_ReturnType_m3799540540 (XPathBooleanFunction_t2634824384 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Object System.Xml.XPath.XPathBooleanFunction::get_StaticValue()
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t XPathBooleanFunction_get_StaticValue_m4271685220_MetadataUsageId;
extern "C"  Il2CppObject * XPathBooleanFunction_get_StaticValue_m4271685220 (XPathBooleanFunction_t2634824384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathBooleanFunction_get_StaticValue_m4271685220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, __this);
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathException::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathException__ctor_m866447368_MetadataUsageId;
extern "C"  void XPathException__ctor_m866447368 (XPathException_t2353341743 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathException__ctor_m866447368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		SystemException__ctor_m3697314481(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void XPathException__ctor_m2948568393 (XPathException_t2353341743 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		StreamingContext_t986364934  L_1 = ___context1;
		SystemException__ctor_m2083527090(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathException::.ctor(System.String,System.Exception)
extern "C"  void XPathException__ctor_m4004445308 (XPathException_t2353341743 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1967233988 * L_1 = ___innerException1;
		SystemException__ctor_m253088421(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathException::.ctor(System.String)
extern "C"  void XPathException__ctor_m2838559738 (XPathException_t2353341743 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SystemException__ctor_m253088421(__this, L_0, (Exception_t1967233988 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.XPathException::get_Message()
extern "C"  String_t* XPathException_get_Message_m3664156007 (XPathException_t2353341743 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = Exception_get_Message_m1013139483(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XPath.XPathException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void XPathException_GetObjectData_m488604582 (XPathException_t2353341743 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		StreamingContext_t986364934  L_1 = ___context1;
		Exception_GetObjectData_m1945031808(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathExpression::.ctor()
extern "C"  void XPathExpression__ctor_m883028087 (XPathExpression_t3441638418 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathExpression::Compile(System.String)
extern "C"  XPathExpression_t3441638418 * XPathExpression_Compile_m1263926045 (Il2CppObject * __this /* static, unused */, String_t* ___xpath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___xpath0;
		XPathExpression_t3441638418 * L_1 = XPathExpression_Compile_m3855669648(NULL /*static, unused*/, L_0, (Il2CppObject *)NULL, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathExpression::Compile(System.String,System.Xml.IXmlNamespaceResolver,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* XPathParser_t618877717_il2cpp_TypeInfo_var;
extern Il2CppClass* CompiledExpression_t938776358_il2cpp_TypeInfo_var;
extern const uint32_t XPathExpression_Compile_m3855669648_MetadataUsageId;
extern "C"  XPathExpression_t3441638418 * XPathExpression_Compile_m3855669648 (Il2CppObject * __this /* static, unused */, String_t* ___xpath0, Il2CppObject * ___nsmgr1, Il2CppObject * ___ctx2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathExpression_Compile_m3855669648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathParser_t618877717 * V_0 = NULL;
	CompiledExpression_t938776358 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___ctx2;
		XPathParser_t618877717 * L_1 = (XPathParser_t618877717 *)il2cpp_codegen_object_new(XPathParser_t618877717_il2cpp_TypeInfo_var);
		XPathParser__ctor_m2662166670(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___xpath0;
		XPathParser_t618877717 * L_3 = V_0;
		String_t* L_4 = ___xpath0;
		NullCheck(L_3);
		Expression_t4217024437 * L_5 = XPathParser_Compile_m3447021097(L_3, L_4, /*hidden argument*/NULL);
		CompiledExpression_t938776358 * L_6 = (CompiledExpression_t938776358 *)il2cpp_codegen_object_new(CompiledExpression_t938776358_il2cpp_TypeInfo_var);
		CompiledExpression__ctor_m2683771966(L_6, L_2, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		CompiledExpression_t938776358 * L_7 = V_1;
		Il2CppObject * L_8 = ___nsmgr1;
		NullCheck(L_7);
		VirtActionInvoker1< Il2CppObject * >::Invoke(5 /* System.Void System.Xml.XPath.CompiledExpression::SetContext(System.Xml.IXmlNamespaceResolver) */, L_7, L_8);
		CompiledExpression_t938776358 * L_9 = V_1;
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunction__ctor_m2315013248 (XPathFunction_t33626258 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathFunctionBoolean::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral533703603;
extern const uint32_t XPathFunctionBoolean__ctor_m2311468944_MetadataUsageId;
extern "C"  void XPathFunctionBoolean__ctor_m2311468944 (XPathFunctionBoolean_t3213667280 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionBoolean__ctor_m2311468944_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m3002905248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t2353341743 * L_6 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral533703603, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionBoolean::get_Peer()
extern "C"  bool XPathFunctionBoolean_get_Peer_m4075223552 (XPathFunctionBoolean_t3213667280 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionBoolean::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionBoolean_Evaluate_m2675549483_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionBoolean_Evaluate_m2675549483 (XPathFunctionBoolean_t3213667280 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionBoolean_Evaluate_m2675549483_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_2);
		bool L_4 = XPathFunctions_ToBoolean_m2432449683(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0021:
	{
		Expression_t4217024437 * L_7 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_8 = ___iter0;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_7, L_8);
		bool L_10 = L_9;
		Il2CppObject * L_11 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}
}
// System.String System.Xml.XPath.XPathFunctionBoolean::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2006063360;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionBoolean_ToString_m2171607622_MetadataUsageId;
extern "C"  String_t* XPathFunctionBoolean_ToString_m2171607622 (XPathFunctionBoolean_t3213667280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionBoolean_ToString_m2171607622_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2006063360);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2006063360);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionCeil::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1658293869;
extern const uint32_t XPathFunctionCeil__ctor_m3298422843_MetadataUsageId;
extern "C"  void XPathFunctionCeil__ctor_m3298422843 (XPathFunctionCeil_t1855013687 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCeil__ctor_m3298422843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral1658293869, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t4217024437 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionCeil::get_HasStaticValue()
extern "C"  bool XPathFunctionCeil_get_HasStaticValue_m2317770444 (XPathFunctionCeil_t1855013687 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.XPathFunctionCeil::get_StaticValueAsNumber()
extern "C"  double XPathFunctionCeil_get_StaticValueAsNumber_m3864092994 (XPathFunctionCeil_t1855013687 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathFunctionCeil::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		double L_3 = ceil(L_2);
		G_B3_0 = L_3;
		goto IL_0029;
	}

IL_0020:
	{
		G_B3_0 = (0.0);
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionCeil::get_Peer()
extern "C"  bool XPathFunctionCeil_get_Peer_m2107441093 (XPathFunctionCeil_t1855013687 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionCeil::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionCeil_Evaluate_m2197801932_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionCeil_Evaluate_m2197801932 (XPathFunctionCeil_t1855013687 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCeil_Evaluate_m2197801932_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = ceil(L_2);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.String System.Xml.XPath.XPathFunctionCeil::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral94541763;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionCeil_ToString_m521177249_MetadataUsageId;
extern "C"  String_t* XPathFunctionCeil_ToString_m521177249 (XPathFunctionCeil_t1855013687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCeil_ToString_m521177249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral94541763);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral94541763);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionConcat::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3483196761;
extern const uint32_t XPathFunctionConcat__ctor_m2570590668_MetadataUsageId;
extern "C"  void XPathFunctionConcat__ctor_m2570590668 (XPathFunctionConcat_t3190339590 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionConcat__ctor_m2570590668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3483196761, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		ArrayList_t2121638921 * L_6 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_6, /*hidden argument*/NULL);
		ArrayList_t2121638921 * L_7 = L_6;
		V_0 = L_7;
		__this->set_rgs_0(L_7);
		ArrayList_t2121638921 * L_8 = V_0;
		NullCheck(L_5);
		FunctionArguments_ToArrayList_m227079145(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionConcat::get_ReturnType()
extern "C"  int32_t XPathFunctionConcat_get_ReturnType_m827815768 (XPathFunctionConcat_t3190339590 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionConcat::get_Peer()
extern Il2CppClass* Expression_t4217024437_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionConcat_get_Peer_m1370169940_MetadataUsageId;
extern "C"  bool XPathFunctionConcat_get_Peer_m1370169940 (XPathFunctionConcat_t3190339590 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionConcat_get_Peer_m1370169940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0028;
	}

IL_0007:
	{
		ArrayList_t2121638921 * L_0 = __this->get_rgs_0();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(((Expression_t4217024437 *)CastclassClass(L_2, Expression_t4217024437_il2cpp_TypeInfo_var)));
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, ((Expression_t4217024437 *)CastclassClass(L_2, Expression_t4217024437_il2cpp_TypeInfo_var)));
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		return (bool)0;
	}

IL_0024:
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_5 = V_0;
		ArrayList_t2121638921 * L_6 = __this->get_rgs_0();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_6);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionConcat::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t4217024437_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionConcat_Evaluate_m1093166813_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionConcat_Evaluate_m1093166813 (XPathFunctionConcat_t3190339590 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionConcat_Evaluate_m1093166813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ArrayList_t2121638921 * L_1 = __this->get_rgs_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_1 = L_2;
		V_2 = 0;
		goto IL_003b;
	}

IL_0019:
	{
		StringBuilder_t3822575854 * L_3 = V_0;
		ArrayList_t2121638921 * L_4 = __this->get_rgs_0();
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, L_5);
		BaseIterator_t3696600956 * L_7 = ___iter0;
		NullCheck(((Expression_t4217024437 *)CastclassClass(L_6, Expression_t4217024437_il2cpp_TypeInfo_var)));
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, ((Expression_t4217024437 *)CastclassClass(L_6, Expression_t4217024437_il2cpp_TypeInfo_var)), L_7);
		NullCheck(L_3);
		StringBuilder_Append_m3898090075(L_3, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_10 = V_2;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0019;
		}
	}
	{
		StringBuilder_t3822575854 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = StringBuilder_ToString_m350379841(L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionConcat::ToString()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral951020436;
extern Il2CppCodeGenString* _stringLiteral119816;
extern const uint32_t XPathFunctionConcat_ToString_m2710519920_MetadataUsageId;
extern "C"  String_t* XPathFunctionConcat_ToString_m2710519920 (XPathFunctionConcat_t3190339590 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionConcat_ToString_m2710519920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t3822575854 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, _stringLiteral951020436, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_0051;
	}

IL_0019:
	{
		StringBuilder_t3822575854 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		ArrayList_t2121638921 * L_5 = __this->get_rgs_0();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_5, L_6);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_8);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		NullCheck(L_2);
		StringBuilder_AppendFormat_m259793396(L_2, L_3, _stringLiteral119816, L_4, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_9 = V_0;
		NullCheck(L_9);
		StringBuilder_Append_m2143093878(L_9, ((int32_t)44), /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		ArrayList_t2121638921 * L_12 = __this->get_rgs_0();
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)L_13-(int32_t)1)))))
		{
			goto IL_0019;
		}
	}
	{
		StringBuilder_t3822575854 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_15 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_16 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		ArrayList_t2121638921 * L_17 = __this->get_rgs_0();
		ArrayList_t2121638921 * L_18 = __this->get_rgs_0();
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_18);
		NullCheck(L_17);
		Il2CppObject * L_20 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_17, ((int32_t)((int32_t)L_19-(int32_t)1)));
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		ArrayElementTypeCheck (L_16, L_21);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_21);
		NullCheck(L_14);
		StringBuilder_AppendFormat_m259793396(L_14, L_15, _stringLiteral119816, L_16, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_22 = V_0;
		NullCheck(L_22);
		StringBuilder_Append_m2143093878(L_22, ((int32_t)41), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = StringBuilder_ToString_m350379841(L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void System.Xml.XPath.XPathFunctionContains::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3609780800;
extern const uint32_t XPathFunctionContains__ctor_m2892508385_MetadataUsageId;
extern "C"  void XPathFunctionContains__ctor_m2892508385 (XPathFunctionContains_t2249625041 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionContains__ctor_m2892508385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t3402201403 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t2353341743 * L_7 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, _stringLiteral3609780800, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t3402201403 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t4217024437 * L_9 = FunctionArguments_get_Arg_m969707333(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t3402201403 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t3402201403 * L_11 = FunctionArguments_get_Tail_m52932169(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t4217024437 * L_12 = FunctionArguments_get_Arg_m969707333(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionContains::get_ReturnType()
extern "C"  int32_t XPathFunctionContains_get_ReturnType_m3276932515 (XPathFunctionContains_t2249625041 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionContains::get_Peer()
extern "C"  bool XPathFunctionContains_get_Peer_m4254071775 (XPathFunctionContains_t2249625041 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionContains::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionContains_Evaluate_m1003236466_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionContains_Evaluate_m1003236466 (XPathFunctionContains_t2249625041 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionContains_Evaluate_m1003236466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t4217024437 * L_3 = __this->get_arg1_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		NullCheck(L_2);
		int32_t L_6 = String_IndexOf_m1476794331(L_2, L_5, /*hidden argument*/NULL);
		bool L_7 = ((bool)((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		Il2CppObject * L_8 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}
}
// System.String System.Xml.XPath.XPathFunctionContains::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3884010985;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionContains_ToString_m571760443_MetadataUsageId;
extern "C"  String_t* XPathFunctionContains_ToString_m571760443 (XPathFunctionContains_t2249625041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionContains_ToString_m571760443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3884010985);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3884010985);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_5 = L_4;
		Expression_t4217024437 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t2956870243* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral41);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionCount::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral549981186;
extern const uint32_t XPathFunctionCount__ctor_m74396489_MetadataUsageId;
extern "C"  void XPathFunctionCount__ctor_m74396489 (XPathFunctionCount_t1359888247 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCount__ctor_m74396489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral549981186, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t4217024437 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionCount::get_ReturnType()
extern "C"  int32_t XPathFunctionCount_get_ReturnType_m4130170227 (XPathFunctionCount_t1359888247 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionCount::get_Peer()
extern "C"  bool XPathFunctionCount_get_Peer_m2430742375 (XPathFunctionCount_t1359888247 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionCount::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionCount_Evaluate_m3315815524_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionCount_Evaluate_m3315815524 (XPathFunctionCount_t1359888247 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCount_Evaluate_m3315815524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t3696600956 * L_2 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count() */, L_2);
		double L_4 = (((double)((double)L_3)));
		Il2CppObject * L_5 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionCount::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool XPathFunctionCount_EvaluateBoolean_m3382145239 (XPathFunctionCount_t1359888247 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, BaseIterator_t3696600956 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_001f;
		}
	}
	{
		Expression_t4217024437 * L_3 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return L_5;
	}

IL_001f:
	{
		Expression_t4217024437 * L_6 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_7 = ___iter0;
		NullCheck(L_6);
		BaseIterator_t3696600956 * L_8 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		return L_9;
	}
}
// System.String System.Xml.XPath.XPathFunctionCount::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2940391673;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionCount_ToString_m4291378413_MetadataUsageId;
extern "C"  String_t* XPathFunctionCount_ToString_m4291378413 (XPathFunctionCount_t1359888247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCount_ToString_m4291378413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2940391673, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionFalse::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1914279038;
extern const uint32_t XPathFunctionFalse__ctor_m1496753845_MetadataUsageId;
extern "C"  void XPathFunctionFalse__ctor_m1496753845 (XPathFunctionFalse_t1362233227 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFalse__ctor_m1496753845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m3002905248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t2353341743 * L_2 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_2, _stringLiteral1914279038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_HasStaticValue()
extern "C"  bool XPathFunctionFalse_get_HasStaticValue_m3224612098 (XPathFunctionFalse_t1362233227 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_StaticValueAsBoolean()
extern "C"  bool XPathFunctionFalse_get_StaticValueAsBoolean_m891110156 (XPathFunctionFalse_t1362233227 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_Peer()
extern "C"  bool XPathFunctionFalse_get_Peer_m1943435643 (XPathFunctionFalse_t1362233227 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionFalse::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionFalse_Evaluate_m2923999952_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionFalse_Evaluate_m2923999952 (XPathFunctionFalse_t1362233227 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFalse_Evaluate_m2923999952_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((bool)0);
		Il2CppObject * L_1 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.XPathFunctionFalse::ToString()
extern Il2CppCodeGenString* _stringLiteral3211354468;
extern const uint32_t XPathFunctionFalse_ToString_m3804071681_MetadataUsageId;
extern "C"  String_t* XPathFunctionFalse_ToString_m3804071681 (XPathFunctionFalse_t1362233227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFalse_ToString_m3804071681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3211354468;
	}
}
// System.Void System.Xml.XPath.XPathFunctionFloor::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3373125940;
extern const uint32_t XPathFunctionFloor__ctor_m224085036_MetadataUsageId;
extern "C"  void XPathFunctionFloor__ctor_m224085036 (XPathFunctionFloor_t1362563700 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFloor__ctor_m224085036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3373125940, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t4217024437 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFloor::get_HasStaticValue()
extern "C"  bool XPathFunctionFloor_get_HasStaticValue_m899265323 (XPathFunctionFloor_t1362563700 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.XPathFunctionFloor::get_StaticValueAsNumber()
extern "C"  double XPathFunctionFloor_get_StaticValueAsNumber_m3220284121 (XPathFunctionFloor_t1362563700 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathFunctionFloor::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		double L_3 = floor(L_2);
		G_B3_0 = L_3;
		goto IL_0029;
	}

IL_0020:
	{
		G_B3_0 = (0.0);
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFloor::get_Peer()
extern "C"  bool XPathFunctionFloor_get_Peer_m3353177828 (XPathFunctionFloor_t1362563700 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionFloor::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionFloor_Evaluate_m1729380871_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionFloor_Evaluate_m1729380871 (XPathFunctionFloor_t1362563700 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFloor_Evaluate_m1729380871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = floor(L_2);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.String System.Xml.XPath.XPathFunctionFloor::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3023330716;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionFloor_ToString_m918846570_MetadataUsageId;
extern "C"  String_t* XPathFunctionFloor_ToString_m918846570 (XPathFunctionFloor_t1362563700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFloor_ToString_m918846570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3023330716);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3023330716);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionId::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3188397582;
extern const uint32_t XPathFunctionId__ctor_m3649212261_MetadataUsageId;
extern "C"  void XPathFunctionId__ctor_m3649212261 (XPathFunctionId_t879333517 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionId__ctor_m3649212261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3188397582, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t4217024437 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Void System.Xml.XPath.XPathFunctionId::.cctor()
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionId_t879333517_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D25_11_FieldInfo_var;
extern const uint32_t XPathFunctionId__cctor_m1596924113_MetadataUsageId;
extern "C"  void XPathFunctionId__cctor_m1596924113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionId__cctor_m1596924113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t3416858730* L_0 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D25_11_FieldInfo_var), /*hidden argument*/NULL);
		((XPathFunctionId_t879333517_StaticFields*)XPathFunctionId_t879333517_il2cpp_TypeInfo_var->static_fields)->set_rgchWhitespace_1(L_0);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionId::get_ReturnType()
extern "C"  int32_t XPathFunctionId_get_ReturnType_m2668233119 (XPathFunctionId_t879333517 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(3);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionId::get_Peer()
extern "C"  bool XPathFunctionId_get_Peer_m1177386971 (XPathFunctionId_t879333517 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionId::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionId_t879333517_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var;
extern Il2CppClass* ListIterator_t2154705993_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t XPathFunctionId_Evaluate_m2633997366_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionId_Evaluate_m2633997366 (XPathFunctionId_t879333517 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionId_Evaluate_m2633997366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	XPathNodeIterator_t2394191562 * V_2 = NULL;
	XPathNavigator_t1624538935 * V_3 = NULL;
	ArrayList_t2121638921 * V_4 = NULL;
	StringU5BU5D_t2956870243* V_5 = NULL;
	int32_t V_6 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t3696600956 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_1 = L_2;
		Il2CppObject * L_3 = V_1;
		V_2 = ((XPathNodeIterator_t2394191562 *)IsInstClass(L_3, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var));
		XPathNodeIterator_t2394191562 * L_4 = V_2;
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_5;
		goto IL_003c;
	}

IL_0025:
	{
		String_t* L_6 = V_0;
		XPathNodeIterator_t2394191562 * L_7 = V_2;
		NullCheck(L_7);
		XPathNavigator_t1624538935 * L_8 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_7);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1825781833(NULL /*static, unused*/, L_6, L_9, _stringLiteral32, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_003c:
	{
		XPathNodeIterator_t2394191562 * L_11 = V_2;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_11);
		if (L_12)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0053;
	}

IL_004c:
	{
		Il2CppObject * L_13 = V_1;
		String_t* L_14 = XPathFunctions_ToString_m2310576707(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0053:
	{
		BaseIterator_t3696600956 * L_15 = ___iter0;
		NullCheck(L_15);
		XPathNavigator_t1624538935 * L_16 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_15);
		NullCheck(L_16);
		XPathNavigator_t1624538935 * L_17 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_16);
		V_3 = L_17;
		ArrayList_t2121638921 * L_18 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_18, /*hidden argument*/NULL);
		V_4 = L_18;
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XPathFunctionId_t879333517_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_20 = ((XPathFunctionId_t879333517_StaticFields*)XPathFunctionId_t879333517_il2cpp_TypeInfo_var->static_fields)->get_rgchWhitespace_1();
		NullCheck(L_19);
		StringU5BU5D_t2956870243* L_21 = String_Split_m290179486(L_19, L_20, /*hidden argument*/NULL);
		V_5 = L_21;
		V_6 = 0;
		goto IL_009f;
	}

IL_007b:
	{
		XPathNavigator_t1624538935 * L_22 = V_3;
		StringU5BU5D_t2956870243* L_23 = V_5;
		int32_t L_24 = V_6;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck(L_22);
		bool L_26 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToId(System.String) */, L_22, ((L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25))));
		if (!L_26)
		{
			goto IL_0099;
		}
	}
	{
		ArrayList_t2121638921 * L_27 = V_4;
		XPathNavigator_t1624538935 * L_28 = V_3;
		NullCheck(L_28);
		XPathNavigator_t1624538935 * L_29 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_28);
		NullCheck(L_27);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_27, L_29);
	}

IL_0099:
	{
		int32_t L_30 = V_6;
		V_6 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_31 = V_6;
		StringU5BU5D_t2956870243* L_32 = V_5;
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length)))))))
		{
			goto IL_007b;
		}
	}
	{
		ArrayList_t2121638921 * L_33 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var);
		XPathNavigatorComparer_t877816196 * L_34 = ((XPathNavigatorComparer_t877816196_StaticFields*)XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var->static_fields)->get_Instance_0();
		NullCheck(L_33);
		VirtActionInvoker1< Il2CppObject * >::Invoke(46 /* System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer) */, L_33, L_34);
		BaseIterator_t3696600956 * L_35 = ___iter0;
		ArrayList_t2121638921 * L_36 = V_4;
		ListIterator_t2154705993 * L_37 = (ListIterator_t2154705993 *)il2cpp_codegen_object_new(ListIterator_t2154705993_il2cpp_TypeInfo_var);
		ListIterator__ctor_m222741241(L_37, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}
}
// System.String System.Xml.XPath.XPathFunctionId::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104045;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionId_ToString_m2274108279_MetadataUsageId;
extern "C"  String_t* XPathFunctionId_ToString_m2274108279 (XPathFunctionId_t879333517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionId_ToString_m2274108279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral104045, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionLang::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2950777750;
extern const uint32_t XPathFunctionLang__ctor_m915197874_MetadataUsageId;
extern "C"  void XPathFunctionLang__ctor_m915197874 (XPathFunctionLang_t1855278112 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang__ctor_m915197874_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral2950777750, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t4217024437 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLang::get_ReturnType()
extern "C"  int32_t XPathFunctionLang_get_ReturnType_m4120654770 (XPathFunctionLang_t1855278112 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLang::get_Peer()
extern "C"  bool XPathFunctionLang_get_Peer_m3306550062 (XPathFunctionLang_t1855278112 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLang::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLang_Evaluate_m1010965763_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionLang_Evaluate_m1010965763 (XPathFunctionLang_t1855278112 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_Evaluate_m1010965763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		bool L_1 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathFunctionLang::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, __this, L_0);
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLang::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLang_EvaluateBoolean_m1037770014_MetadataUsageId;
extern "C"  bool XPathFunctionLang_EvaluateBoolean_m1037770014 (XPathFunctionLang_t1855278112 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_EvaluateBoolean_m1037770014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_4 = String_ToLower_m2140020155(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Xml.XPath.XPathNavigator::get_XmlLang() */, L_6);
		CultureInfo_t3603717042 * L_8 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_9 = String_ToLower_m2140020155(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_0;
		String_t* L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_13 = V_0;
		String_t* L_14 = V_1;
		CharU5BU5D_t3416858730* L_15 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)45));
		NullCheck(L_14);
		StringU5BU5D_t2956870243* L_16 = String_Split_m290179486(L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		int32_t L_17 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_13, ((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17))), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_18));
		goto IL_0055;
	}

IL_0054:
	{
		G_B3_0 = 1;
	}

IL_0055:
	{
		return (bool)G_B3_0;
	}
}
// System.String System.Xml.XPath.XPathFunctionLang::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral102738938;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionLang_ToString_m1720286218_MetadataUsageId;
extern "C"  String_t* XPathFunctionLang_ToString_m1720286218 (XPathFunctionLang_t1855278112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_ToString_m1720286218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral102738938);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral102738938);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionLast::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral570445291;
extern const uint32_t XPathFunctionLast__ctor_m565310986_MetadataUsageId;
extern "C"  void XPathFunctionLast__ctor_m565310986 (XPathFunctionLast_t1855278280 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast__ctor_m565310986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t2353341743 * L_2 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_2, _stringLiteral570445291, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLast::get_ReturnType()
extern "C"  int32_t XPathFunctionLast_get_ReturnType_m3983301210 (XPathFunctionLast_t1855278280 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLast::get_Peer()
extern "C"  bool XPathFunctionLast_get_Peer_m1259485142 (XPathFunctionLast_t1855278280 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLast::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLast_Evaluate_m380434779_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionLast_Evaluate_m380434779 (XPathFunctionLast_t1855278280 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast_Evaluate_m380434779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count() */, L_0);
		double L_2 = (((double)((double)L_1)));
		Il2CppObject * L_3 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathFunctionLast::ToString()
extern Il2CppCodeGenString* _stringLiteral3185068567;
extern const uint32_t XPathFunctionLast_ToString_m3968188594_MetadataUsageId;
extern "C"  String_t* XPathFunctionLast_ToString_m3968188594 (XPathFunctionLast_t1855278280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast_ToString_m3968188594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3185068567;
	}
}
// System.Void System.Xml.XPath.XPathFunctionLocalName::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4160111950;
extern const uint32_t XPathFunctionLocalName__ctor_m2097528674_MetadataUsageId;
extern "C"  void XPathFunctionLocalName__ctor_m2097528674 (XPathFunctionLocalName_t4208157054 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName__ctor_m2097528674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t2353341743 * L_6 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral4160111950, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLocalName::get_ReturnType()
extern "C"  int32_t XPathFunctionLocalName_get_ReturnType_m4113090874 (XPathFunctionLocalName_t4208157054 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLocalName::get_Peer()
extern "C"  bool XPathFunctionLocalName_get_Peer_m565692014 (XPathFunctionLocalName_t4208157054 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLocalName::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLocalName_Evaluate_m1852518461_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionLocalName_Evaluate_m1852518461 (XPathFunctionLocalName_t4208157054 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName_Evaluate_m1852518461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t3696600956 * V_0 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t4217024437 * L_4 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t3696600956 * L_6 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t3696600956 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t3696600956 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t3696600956 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t1624538935 * L_12 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionLocalName::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral323178011;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionLocalName_ToString_m1873365364_MetadataUsageId;
extern "C"  String_t* XPathFunctionLocalName_ToString_m1873365364 (XPathFunctionLocalName_t4208157054 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName_ToString_m1873365364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral323178011, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionName::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral543394096;
extern const uint32_t XPathFunctionName__ctor_m17270517_MetadataUsageId;
extern "C"  void XPathFunctionName__ctor_m17270517 (XPathFunctionName_t1855337661 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName__ctor_m17270517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t2353341743 * L_6 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral543394096, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionName::get_ReturnType()
extern "C"  int32_t XPathFunctionName_get_ReturnType_m2142310159 (XPathFunctionName_t1855337661 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionName::get_Peer()
extern "C"  bool XPathFunctionName_get_Peer_m3377846347 (XPathFunctionName_t1855337661 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionName::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionName_Evaluate_m2105002246_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionName_Evaluate_m2105002246 (XPathFunctionName_t1855337661 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName_Evaluate_m2105002246_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t3696600956 * V_0 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t4217024437 * L_4 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t3696600956 * L_6 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t3696600956 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t3696600956 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t3696600956 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t1624538935 * L_12 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionName::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104584957;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionName_ToString_m1791582503_MetadataUsageId;
extern "C"  String_t* XPathFunctionName_ToString_m1791582503 (XPathFunctionName_t1855337661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName_ToString_m1791582503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		G_B1_0 = _stringLiteral104584957;
		if (!L_0)
		{
			G_B2_0 = _stringLiteral104584957;
			goto IL_0020;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0025;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, G_B3_1, G_B3_0, _stringLiteral41, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNamespaceUri::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2162484513;
extern const uint32_t XPathFunctionNamespaceUri__ctor_m503725999_MetadataUsageId;
extern "C"  void XPathFunctionNamespaceUri__ctor_m503725999 (XPathFunctionNamespaceUri_t2303716163 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri__ctor_m503725999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t2353341743 * L_6 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral2162484513, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNamespaceUri::get_Peer()
extern "C"  bool XPathFunctionNamespaceUri_get_Peer_m3433121489 (XPathFunctionNamespaceUri_t2303716163 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionNamespaceUri::get_ReturnType()
extern "C"  int32_t XPathFunctionNamespaceUri_get_ReturnType_m3395396885 (XPathFunctionNamespaceUri_t2303716163 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Object System.Xml.XPath.XPathFunctionNamespaceUri::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNamespaceUri_Evaluate_m1130007872_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionNamespaceUri_Evaluate_m1130007872 (XPathFunctionNamespaceUri_t2303716163 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri_Evaluate_m1130007872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t3696600956 * V_0 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t4217024437 * L_4 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t3696600956 * L_6 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t3696600956 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t3696600956 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t3696600956 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t1624538935 * L_12 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionNamespaceUri::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2367935854;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionNamespaceUri_ToString_m1933919405_MetadataUsageId;
extern "C"  String_t* XPathFunctionNamespaceUri_ToString_m1933919405 (XPathFunctionNamespaceUri_t2303716163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri_ToString_m1933919405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2367935854, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNormalizeSpace::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3904114773;
extern const uint32_t XPathFunctionNormalizeSpace__ctor_m3177496007_MetadataUsageId;
extern "C"  void XPathFunctionNormalizeSpace__ctor_m3177496007 (XPathFunctionNormalizeSpace_t2104917483 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace__ctor_m3177496007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t2353341743 * L_6 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral3904114773, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionNormalizeSpace::get_ReturnType()
extern "C"  int32_t XPathFunctionNormalizeSpace_get_ReturnType_m873947901 (XPathFunctionNormalizeSpace_t2104917483 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNormalizeSpace::get_Peer()
extern "C"  bool XPathFunctionNormalizeSpace_get_Peer_m2843429305 (XPathFunctionNormalizeSpace_t2104917483 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNormalizeSpace::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNormalizeSpace_Evaluate_m3453465240_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionNormalizeSpace_Evaluate_m3453465240 (XPathFunctionNormalizeSpace_t2104917483 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace_Evaluate_m3453465240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringBuilder_t3822575854 * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	uint16_t V_4 = 0x0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_2 = ___iter0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_001d:
	{
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t1624538935 * L_5 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		V_0 = L_6;
	}

IL_0029:
	{
		StringBuilder_t3822575854 * L_7 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = (bool)0;
		V_3 = 0;
		goto IL_0096;
	}

IL_0038:
	{
		String_t* L_8 = V_0;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		uint16_t L_10 = String_get_Chars_m3015341861(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		uint16_t L_11 = V_4;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)32))))
		{
			goto IL_0065;
		}
	}
	{
		uint16_t L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)9))))
		{
			goto IL_0065;
		}
	}
	{
		uint16_t L_13 = V_4;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)13))))
		{
			goto IL_0065;
		}
	}
	{
		uint16_t L_14 = V_4;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_006c;
		}
	}

IL_0065:
	{
		V_2 = (bool)1;
		goto IL_0092;
	}

IL_006c:
	{
		bool L_15 = V_2;
		if (!L_15)
		{
			goto IL_0089;
		}
	}
	{
		V_2 = (bool)0;
		StringBuilder_t3822575854 * L_16 = V_1;
		NullCheck(L_16);
		int32_t L_17 = StringBuilder_get_Length_m2443133099(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		StringBuilder_t3822575854 * L_18 = V_1;
		NullCheck(L_18);
		StringBuilder_Append_m2143093878(L_18, ((int32_t)32), /*hidden argument*/NULL);
	}

IL_0089:
	{
		StringBuilder_t3822575854 * L_19 = V_1;
		uint16_t L_20 = V_4;
		NullCheck(L_19);
		StringBuilder_Append_m2143093878(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0092:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_22 = V_3;
		String_t* L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m2979997331(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0038;
		}
	}
	{
		StringBuilder_t3822575854 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = StringBuilder_ToString_m350379841(L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.String System.Xml.XPath.XPathFunctionNormalizeSpace::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2915672738;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionNormalizeSpace_ToString_m1950784725_MetadataUsageId;
extern "C"  String_t* XPathFunctionNormalizeSpace_ToString_m1950784725 (XPathFunctionNormalizeSpace_t2104917483 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace_ToString_m1950784725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2915672738);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2915672738);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_002b;
		}
	}
	{
		Expression_t4217024437 * L_3 = __this->get_arg0_0();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0030;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0030:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_6 = G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral41);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m21867311(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNot::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1286369211;
extern const uint32_t XPathFunctionNot__ctor_m1396109829_MetadataUsageId;
extern "C"  void XPathFunctionNot__ctor_m1396109829 (XPathFunctionNot_t1178269691 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot__ctor_m1396109829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m3002905248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral1286369211, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t4217024437 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNot::get_Peer()
extern "C"  bool XPathFunctionNot_get_Peer_m2256221739 (XPathFunctionNot_t1178269691 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNot::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNot_Evaluate_m2620128096_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionNot_Evaluate_m2620128096 (XPathFunctionNot_t1178269691 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot_Evaluate_m2620128096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, BaseIterator_t3696600956 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		bool L_3 = ((bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0));
		Il2CppObject * L_4 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.String System.Xml.XPath.XPathFunctionNot::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3387317;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionNot_ToString_m1390051569_MetadataUsageId;
extern "C"  String_t* XPathFunctionNot_ToString_m1390051569 (XPathFunctionNot_t1178269691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot_ToString_m1390051569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3387317);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3387317);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNumber::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2493164338;
extern const uint32_t XPathFunctionNumber__ctor_m2761557623_MetadataUsageId;
extern "C"  void XPathFunctionNumber__ctor_m2761557623 (XPathFunctionNumber_t3510770747 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber__ctor_m2761557623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t2353341743 * L_6 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral2493164338, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNumber::Optimize()
extern Il2CppClass* ExprNumber_t3560215227_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNumber_Optimize_m2286708775_MetadataUsageId;
extern "C"  Expression_t4217024437 * XPathFunctionNumber_Optimize_m2286708775 (XPathFunctionNumber_t3510770747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_Optimize_m2286708775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathFunctionNumber_t3510770747 * G_B5_0 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		Expression_t4217024437 * L_2 = VirtFuncInvoker0< Expression_t4217024437 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_1);
		__this->set_arg0_0(L_2);
		Expression_t4217024437 * L_3 = __this->get_arg0_0();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_3);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		G_B5_0 = __this;
		goto IL_003f;
	}

IL_0034:
	{
		double L_5 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.XPathFunctionNumber::get_StaticValueAsNumber() */, __this);
		ExprNumber_t3560215227 * L_6 = (ExprNumber_t3560215227 *)il2cpp_codegen_object_new(ExprNumber_t3560215227_il2cpp_TypeInfo_var);
		ExprNumber__ctor_m2500538694(L_6, L_5, /*hidden argument*/NULL);
		G_B5_0 = ((XPathFunctionNumber_t3510770747 *)(L_6));
	}

IL_003f:
	{
		return G_B5_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNumber::get_HasStaticValue()
extern "C"  bool XPathFunctionNumber_get_HasStaticValue_m917305616 (XPathFunctionNumber_t3510770747 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.Double System.Xml.XPath.XPathFunctionNumber::get_StaticValueAsNumber()
extern "C"  double XPathFunctionNumber_get_StaticValueAsNumber_m3694847038 (XPathFunctionNumber_t3510770747 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		G_B3_0 = L_2;
		goto IL_0024;
	}

IL_001b:
	{
		G_B3_0 = (0.0);
	}

IL_0024:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNumber::get_Peer()
extern "C"  bool XPathFunctionNumber_get_Peer_m384753417 (XPathFunctionNumber_t3510770747 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNumber::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNumber_Evaluate_m3471016520_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionNumber_Evaluate_m3471016520 (XPathFunctionNumber_t3510770747 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_Evaluate_m3471016520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_2);
		double L_4 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0021:
	{
		Expression_t4217024437 * L_7 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_8 = ___iter0;
		NullCheck(L_7);
		double L_9 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_7, L_8);
		double L_10 = L_9;
		Il2CppObject * L_11 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}
}
// System.String System.Xml.XPath.XPathFunctionNumber::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2294451711;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionNumber_ToString_m1725103397_MetadataUsageId;
extern "C"  String_t* XPathFunctionNumber_ToString_m1725103397 (XPathFunctionNumber_t3510770747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_ToString_m1725103397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2294451711);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2294451711);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionPosition::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4139044408;
extern const uint32_t XPathFunctionPosition__ctor_m545736599_MetadataUsageId;
extern "C"  void XPathFunctionPosition__ctor_m545736599 (XPathFunctionPosition_t3564875995 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition__ctor_m545736599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t2353341743 * L_2 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_2, _stringLiteral4139044408, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionPosition::get_ReturnType()
extern "C"  int32_t XPathFunctionPosition_get_ReturnType_m1987852845 (XPathFunctionPosition_t3564875995 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionPosition::get_Peer()
extern "C"  bool XPathFunctionPosition_get_Peer_m393666537 (XPathFunctionPosition_t3564875995 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionPosition::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionPosition_Evaluate_m3275652264_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionPosition_Evaluate_m3275652264 (XPathFunctionPosition_t3564875995 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition_Evaluate_m3275652264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_0);
		double L_2 = (((double)((double)L_1)));
		Il2CppObject * L_3 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathFunctionPosition::ToString()
extern Il2CppCodeGenString* _stringLiteral1381038058;
extern const uint32_t XPathFunctionPosition_ToString_m1006322501_MetadataUsageId;
extern "C"  String_t* XPathFunctionPosition_ToString_m1006322501 (XPathFunctionPosition_t3564875995 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition_ToString_m1006322501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1381038058;
	}
}
// System.Void System.Xml.XPath.XPathFunctionRound::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3542717238;
extern const uint32_t XPathFunctionRound__ctor_m948443498_MetadataUsageId;
extern "C"  void XPathFunctionRound__ctor_m948443498 (XPathFunctionRound_t1373741046 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound__ctor_m948443498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3542717238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t4217024437 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionRound::get_HasStaticValue()
extern "C"  bool XPathFunctionRound_get_HasStaticValue_m4183577389 (XPathFunctionRound_t1373741046 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.XPathFunctionRound::get_StaticValueAsNumber()
extern "C"  double XPathFunctionRound_get_StaticValueAsNumber_m272413719 (XPathFunctionRound_t1373741046 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathFunctionRound::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		double L_3 = XPathFunctionRound_Round_m561292306(__this, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002a;
	}

IL_0021:
	{
		G_B3_0 = (0.0);
	}

IL_002a:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionRound::get_Peer()
extern "C"  bool XPathFunctionRound_get_Peer_m1655032166 (XPathFunctionRound_t1373741046 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionRound::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionRound_Evaluate_m760303301_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionRound_Evaluate_m760303301 (XPathFunctionRound_t1373741046 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound_Evaluate_m760303301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = XPathFunctionRound_Round_m561292306(__this, L_2, /*hidden argument*/NULL);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.Double System.Xml.XPath.XPathFunctionRound::Round(System.Double)
extern "C"  double XPathFunctionRound_Round_m561292306 (XPathFunctionRound_t1373741046 * __this, double ___arg0, const MethodInfo* method)
{
	{
		double L_0 = ___arg0;
		if ((((double)L_0) < ((double)(-0.5))))
		{
			goto IL_001e;
		}
	}
	{
		double L_1 = ___arg0;
		if ((!(((double)L_1) > ((double)(0.0)))))
		{
			goto IL_002f;
		}
	}

IL_001e:
	{
		double L_2 = ___arg0;
		double L_3 = floor(((double)((double)L_2+(double)(0.5))));
		return L_3;
	}

IL_002f:
	{
		double L_4 = ___arg0;
		double L_5 = bankers_round(L_4);
		return L_5;
	}
}
// System.String System.Xml.XPath.XPathFunctionRound::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3369828442;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionRound_ToString_m3515668204_MetadataUsageId;
extern "C"  String_t* XPathFunctionRound_ToString_m3515668204 (XPathFunctionRound_t1373741046 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound_ToString_m3515668204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3369828442);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3369828442);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctions::ToBoolean(System.Object)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToBoolean_m2654560037_MetadataUsageId;
extern "C"  bool XPathFunctions_ToBoolean_m2654560037 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToBoolean_m2654560037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	XPathNodeIterator_t2394191562 * V_1 = NULL;
	int32_t G_B8_0 = 0;
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		Il2CppObject * L_2 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_2, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_3 = ___arg0;
		return ((*(bool*)((bool*)UnBox (L_3, Boolean_t211005341_il2cpp_TypeInfo_var))));
	}

IL_001e:
	{
		Il2CppObject * L_4 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_004c;
		}
	}
	{
		Il2CppObject * L_5 = ___arg0;
		V_0 = ((*(double*)((double*)UnBox (L_5, Double_t534516614_il2cpp_TypeInfo_var))));
		double L_6 = V_0;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		double L_7 = V_0;
		bool L_8 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		goto IL_004b;
	}

IL_004a:
	{
		G_B8_0 = 0;
	}

IL_004b:
	{
		return (bool)G_B8_0;
	}

IL_004c:
	{
		Il2CppObject * L_9 = ___arg0;
		if (!((String_t*)IsInstSealed(L_9, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0069;
		}
	}
	{
		Il2CppObject * L_10 = ___arg0;
		NullCheck(((String_t*)CastclassSealed(L_10, String_t_il2cpp_TypeInfo_var)));
		int32_t L_11 = String_get_Length_m2979997331(((String_t*)CastclassSealed(L_10, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_11) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0069:
	{
		Il2CppObject * L_12 = ___arg0;
		if (!((XPathNodeIterator_t2394191562 *)IsInstClass(L_12, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		Il2CppObject * L_13 = ___arg0;
		V_1 = ((XPathNodeIterator_t2394191562 *)CastclassClass(L_13, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var));
		XPathNodeIterator_t2394191562 * L_14 = V_1;
		NullCheck(L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_14);
		return L_15;
	}

IL_0082:
	{
		Il2CppObject * L_16 = ___arg0;
		if (!((XPathNavigator_t1624538935 *)IsInstClass(L_16, XPathNavigator_t1624538935_il2cpp_TypeInfo_var)))
		{
			goto IL_00a0;
		}
	}
	{
		Il2CppObject * L_17 = ___arg0;
		NullCheck(((XPathNavigator_t1624538935 *)CastclassClass(L_17, XPathNavigator_t1624538935_il2cpp_TypeInfo_var)));
		XPathNodeIterator_t2394191562 * L_18 = VirtFuncInvoker1< XPathNodeIterator_t2394191562 *, int32_t >::Invoke(33 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::SelectChildren(System.Xml.XPath.XPathNodeType) */, ((XPathNavigator_t1624538935 *)CastclassClass(L_17, XPathNavigator_t1624538935_il2cpp_TypeInfo_var)), ((int32_t)9));
		bool L_19 = XPathFunctions_ToBoolean_m2654560037(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_00a0:
	{
		ArgumentException_t124305799 * L_20 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctions::ToBoolean(System.String)
extern "C"  bool XPathFunctions_ToBoolean_m2432449683 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___s0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2979997331(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		return (bool)G_B3_0;
	}
}
// System.String System.Xml.XPath.XPathFunctions::ToString(System.Object)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern const uint32_t XPathFunctions_ToString_m2310576707_MetadataUsageId;
extern "C"  String_t* XPathFunctions_ToString_m2310576707 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToString_m2310576707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNodeIterator_t2394191562 * V_0 = NULL;
	String_t* G_B8_0 = NULL;
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		Il2CppObject * L_2 = ___arg0;
		if (!((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_3 = ___arg0;
		return ((String_t*)CastclassSealed(L_3, String_t_il2cpp_TypeInfo_var));
	}

IL_001e:
	{
		Il2CppObject * L_4 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_0044;
		}
	}
	{
		Il2CppObject * L_5 = ___arg0;
		if (!((*(bool*)((bool*)UnBox (L_5, Boolean_t211005341_il2cpp_TypeInfo_var)))))
		{
			goto IL_003e;
		}
	}
	{
		G_B8_0 = _stringLiteral3569038;
		goto IL_0043;
	}

IL_003e:
	{
		G_B8_0 = _stringLiteral97196323;
	}

IL_0043:
	{
		return G_B8_0;
	}

IL_0044:
	{
		Il2CppObject * L_6 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_6, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		Il2CppObject * L_7 = ___arg0;
		String_t* L_8 = XPathFunctions_ToString_m1520227313(NULL /*static, unused*/, ((*(double*)((double*)UnBox (L_7, Double_t534516614_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_8;
	}

IL_005b:
	{
		Il2CppObject * L_9 = ___arg0;
		if (!((XPathNodeIterator_t2394191562 *)IsInstClass(L_9, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_10 = ___arg0;
		V_0 = ((XPathNodeIterator_t2394191562 *)CastclassClass(L_10, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var));
		XPathNodeIterator_t2394191562 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_11);
		if (L_12)
		{
			goto IL_007e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_13;
	}

IL_007e:
	{
		XPathNodeIterator_t2394191562 * L_14 = V_0;
		NullCheck(L_14);
		XPathNavigator_t1624538935 * L_15 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_14);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_15);
		return L_16;
	}

IL_008a:
	{
		Il2CppObject * L_17 = ___arg0;
		if (!((XPathNavigator_t1624538935 *)IsInstClass(L_17, XPathNavigator_t1624538935_il2cpp_TypeInfo_var)))
		{
			goto IL_00a1;
		}
	}
	{
		Il2CppObject * L_18 = ___arg0;
		NullCheck(((XPathNavigator_t1624538935 *)CastclassClass(L_18, XPathNavigator_t1624538935_il2cpp_TypeInfo_var)));
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, ((XPathNavigator_t1624538935 *)CastclassClass(L_18, XPathNavigator_t1624538935_il2cpp_TypeInfo_var)));
		return L_19;
	}

IL_00a1:
	{
		ArgumentException_t124305799 * L_20 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}
}
// System.String System.Xml.XPath.XPathFunctions::ToString(System.Double)
extern Il2CppClass* NumberFormatInfo_t3411951076_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral506745205;
extern Il2CppCodeGenString* _stringLiteral237817416;
extern Il2CppCodeGenString* _stringLiteral82;
extern const uint32_t XPathFunctions_ToString_m1520227313_MetadataUsageId;
extern "C"  String_t* XPathFunctions_ToString_m1520227313 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToString_m1520227313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___d0;
		if ((!(((double)L_0) == ((double)(-std::numeric_limits<double>::infinity())))))
		{
			goto IL_0015;
		}
	}
	{
		return _stringLiteral506745205;
	}

IL_0015:
	{
		double L_1 = ___d0;
		if ((!(((double)L_1) == ((double)(std::numeric_limits<double>::infinity())))))
		{
			goto IL_002a;
		}
	}
	{
		return _stringLiteral237817416;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t3411951076_il2cpp_TypeInfo_var);
		NumberFormatInfo_t3411951076 * L_2 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = Double_ToString_m2573497243((&___d0), _stringLiteral82, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Double System.Xml.XPath.XPathFunctions::ToNumber(System.Object)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t3696600956_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToNumber_m1220011083_MetadataUsageId;
extern "C"  double XPathFunctions_ToNumber_m1220011083 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToNumber_m1220011083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		Il2CppObject * L_2 = ___arg0;
		if (((BaseIterator_t3696600956 *)IsInstClass(L_2, BaseIterator_t3696600956_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject * L_3 = ___arg0;
		if (!((XPathNavigator_t1624538935 *)IsInstClass(L_3, XPathNavigator_t1624538935_il2cpp_TypeInfo_var)))
		{
			goto IL_002a;
		}
	}

IL_0022:
	{
		Il2CppObject * L_4 = ___arg0;
		String_t* L_5 = XPathFunctions_ToString_m2310576707(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		___arg0 = L_5;
	}

IL_002a:
	{
		Il2CppObject * L_6 = ___arg0;
		if (!((String_t*)IsInstSealed(L_6, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0043;
		}
	}
	{
		Il2CppObject * L_7 = ___arg0;
		V_0 = ((String_t*)IsInstSealed(L_7, String_t_il2cpp_TypeInfo_var));
		String_t* L_8 = V_0;
		double L_9 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0043:
	{
		Il2CppObject * L_10 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_10, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_0055;
		}
	}
	{
		Il2CppObject * L_11 = ___arg0;
		return ((*(double*)((double*)UnBox (L_11, Double_t534516614_il2cpp_TypeInfo_var))));
	}

IL_0055:
	{
		Il2CppObject * L_12 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_12, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_006c;
		}
	}
	{
		Il2CppObject * L_13 = ___arg0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		double L_14 = Convert_ToDouble_m1966172364(NULL /*static, unused*/, ((*(bool*)((bool*)UnBox (L_13, Boolean_t211005341_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_14;
	}

IL_006c:
	{
		ArgumentException_t124305799 * L_15 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}
}
// System.Double System.Xml.XPath.XPathFunctions::ToNumber(System.String)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t3411951076_il2cpp_TypeInfo_var;
extern Il2CppClass* OverflowException_t3216083426_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t2404802957_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToNumber_m997900729_MetadataUsageId;
extern "C"  double XPathFunctions_ToNumber_m997900729 (Il2CppObject * __this /* static, unused */, String_t* ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToNumber_m997900729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	double V_1 = 0.0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		String_t* L_2 = ___arg0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_3 = ((XmlChar_t3591879093_StaticFields*)XmlChar_t3591879093_il2cpp_TypeInfo_var->static_fields)->get_WhitespaceChars_0();
		NullCheck(L_2);
		String_t* L_4 = String_Trim_m1469603388(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002d;
		}
	}
	{
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_7 = V_0;
			NullCheck(L_7);
			uint16_t L_8 = String_get_Chars_m3015341861(L_7, 0, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)46)))))
			{
				goto IL_0049;
			}
		}

IL_003b:
		{
			uint16_t L_9 = ((uint16_t)((int32_t)46));
			Il2CppObject * L_10 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_9);
			String_t* L_11 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m389863537(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
			V_0 = L_12;
		}

IL_0049:
		{
			String_t* L_13 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t3411951076_il2cpp_TypeInfo_var);
			NumberFormatInfo_t3411951076 * L_14 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
			double L_15 = Double_Parse_m3249074997(NULL /*static, unused*/, L_13, ((int32_t)39), L_14, /*hidden argument*/NULL);
			V_1 = L_15;
			goto IL_008b;
		}

IL_005c:
		{
			; // IL_005c: leave IL_008b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t3216083426_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0061;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2404802957_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0076;
		throw e;
	}

CATCH_0061:
	{ // begin catch(System.OverflowException)
		{
			V_1 = (std::numeric_limits<double>::quiet_NaN());
			goto IL_008b;
		}

IL_0071:
		{
			; // IL_0071: leave IL_008b
		}
	} // end catch (depth: 1)

CATCH_0076:
	{ // begin catch(System.FormatException)
		{
			V_1 = (std::numeric_limits<double>::quiet_NaN());
			goto IL_008b;
		}

IL_0086:
		{
			; // IL_0086: leave IL_008b
		}
	} // end catch (depth: 1)

IL_008b:
	{
		double L_16 = V_1;
		return L_16;
	}
}
// System.Void System.Xml.XPath.XPathFunctionStartsWith::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3140805757;
extern const uint32_t XPathFunctionStartsWith__ctor_m3911729737_MetadataUsageId;
extern "C"  void XPathFunctionStartsWith__ctor_m3911729737 (XPathFunctionStartsWith_t2744556329 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith__ctor_m3911729737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t3402201403 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t2353341743 * L_7 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, _stringLiteral3140805757, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t3402201403 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t4217024437 * L_9 = FunctionArguments_get_Arg_m969707333(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t3402201403 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t3402201403 * L_11 = FunctionArguments_get_Tail_m52932169(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t4217024437 * L_12 = FunctionArguments_get_Arg_m969707333(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionStartsWith::get_ReturnType()
extern "C"  int32_t XPathFunctionStartsWith_get_ReturnType_m116298299 (XPathFunctionStartsWith_t2744556329 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionStartsWith::get_Peer()
extern "C"  bool XPathFunctionStartsWith_get_Peer_m267456887 (XPathFunctionStartsWith_t2744556329 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionStartsWith::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionStartsWith_Evaluate_m846929178_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionStartsWith_Evaluate_m846929178 (XPathFunctionStartsWith_t2744556329 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith_Evaluate_m846929178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t4217024437 * L_3 = __this->get_arg1_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		NullCheck(L_2);
		bool L_6 = String_StartsWith_m1500793453(L_2, L_5, /*hidden argument*/NULL);
		bool L_7 = L_6;
		Il2CppObject * L_8 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}
}
// System.String System.Xml.XPath.XPathFunctionStartsWith::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2881029030;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionStartsWith_ToString_m1645832723_MetadataUsageId;
extern "C"  String_t* XPathFunctionStartsWith_ToString_m1645832723 (XPathFunctionStartsWith_t2744556329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith_ToString_m1645832723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2881029030);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2881029030);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_5 = L_4;
		Expression_t4217024437 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t2956870243* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral41);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionString::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4266816362;
extern const uint32_t XPathFunctionString__ctor_m3584382127_MetadataUsageId;
extern "C"  void XPathFunctionString__ctor_m3584382127 (XPathFunctionString_t3653148931 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionString__ctor_m3584382127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t2353341743 * L_6 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral4266816362, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionString::get_ReturnType()
extern "C"  int32_t XPathFunctionString_get_ReturnType_m463862549 (XPathFunctionString_t3653148931 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionString::get_Peer()
extern "C"  bool XPathFunctionString_get_Peer_m723679185 (XPathFunctionString_t3653148931 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionString::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionString_Evaluate_m2019360384 (XPathFunctionString_t3653148931 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t4217024437 * L_4 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		return L_6;
	}
}
// System.String System.Xml.XPath.XPathFunctionString::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2413208119;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionString_ToString_m2064029165_MetadataUsageId;
extern "C"  String_t* XPathFunctionString_ToString_m2064029165 (XPathFunctionString_t3653148931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionString_ToString_m2064029165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2413208119, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionStringLength::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2119956473;
extern const uint32_t XPathFunctionStringLength__ctor_m2699284393_MetadataUsageId;
extern "C"  void XPathFunctionStringLength__ctor_m2699284393 (XPathFunctionStringLength_t3423546249 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength__ctor_m2699284393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t4217024437 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t2353341743 * L_6 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral2119956473, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionStringLength::get_ReturnType()
extern "C"  int32_t XPathFunctionStringLength_get_ReturnType_m1348968411 (XPathFunctionStringLength_t3423546249 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionStringLength::get_Peer()
extern "C"  bool XPathFunctionStringLength_get_Peer_m3797175831 (XPathFunctionStringLength_t3423546249 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionStringLength::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionStringLength_Evaluate_m913298362_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionStringLength_Evaluate_m913298362 (XPathFunctionStringLength_t3423546249 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength_Evaluate_m913298362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_2 = ___iter0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_001d:
	{
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t1624538935 * L_5 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		V_0 = L_6;
	}

IL_0029:
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m2979997331(L_7, /*hidden argument*/NULL);
		double L_9 = (((double)((double)L_8)));
		Il2CppObject * L_10 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_9);
		return L_10;
	}
}
// System.String System.Xml.XPath.XPathFunctionStringLength::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3998139462;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionStringLength_ToString_m2297973747_MetadataUsageId;
extern "C"  String_t* XPathFunctionStringLength_ToString_m2297973747 (XPathFunctionStringLength_t3423546249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength_ToString_m2297973747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3998139462);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3998139462);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstring::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3692286390;
extern const uint32_t XPathFunctionSubstring__ctor_m2565884615_MetadataUsageId;
extern "C"  void XPathFunctionSubstring__ctor_m2565884615 (XPathFunctionSubstring_t1649127225 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring__ctor_m2565884615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t3402201403 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		FunctionArguments_t3402201403 * L_7 = ___args0;
		NullCheck(L_7);
		FunctionArguments_t3402201403 * L_8 = FunctionArguments_get_Tail_m52932169(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		FunctionArguments_t3402201403 * L_9 = FunctionArguments_get_Tail_m52932169(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		FunctionArguments_t3402201403 * L_10 = FunctionArguments_get_Tail_m52932169(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}

IL_003d:
	{
		XPathException_t2353341743 * L_11 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_11, _stringLiteral3692286390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0048:
	{
		FunctionArguments_t3402201403 * L_12 = ___args0;
		NullCheck(L_12);
		Expression_t4217024437 * L_13 = FunctionArguments_get_Arg_m969707333(L_12, /*hidden argument*/NULL);
		__this->set_arg0_0(L_13);
		FunctionArguments_t3402201403 * L_14 = ___args0;
		NullCheck(L_14);
		FunctionArguments_t3402201403 * L_15 = FunctionArguments_get_Tail_m52932169(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Expression_t4217024437 * L_16 = FunctionArguments_get_Arg_m969707333(L_15, /*hidden argument*/NULL);
		__this->set_arg1_1(L_16);
		FunctionArguments_t3402201403 * L_17 = ___args0;
		NullCheck(L_17);
		FunctionArguments_t3402201403 * L_18 = FunctionArguments_get_Tail_m52932169(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		FunctionArguments_t3402201403 * L_19 = FunctionArguments_get_Tail_m52932169(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_008b;
		}
	}
	{
		FunctionArguments_t3402201403 * L_20 = ___args0;
		NullCheck(L_20);
		FunctionArguments_t3402201403 * L_21 = FunctionArguments_get_Tail_m52932169(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		FunctionArguments_t3402201403 * L_22 = FunctionArguments_get_Tail_m52932169(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Expression_t4217024437 * L_23 = FunctionArguments_get_Arg_m969707333(L_22, /*hidden argument*/NULL);
		__this->set_arg2_2(L_23);
	}

IL_008b:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstring::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstring_get_ReturnType_m3917299253 (XPathFunctionSubstring_t1649127225 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstring::get_Peer()
extern "C"  bool XPathFunctionSubstring_get_Peer_m3084538793 (XPathFunctionSubstring_t1649127225 * __this, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		Expression_t4217024437 * L_4 = __this->get_arg2_2();
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		Expression_t4217024437 * L_5 = __this->get_arg2_2();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_5);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_003c;
	}

IL_003b:
	{
		G_B5_0 = 1;
	}

IL_003c:
	{
		G_B7_0 = G_B5_0;
		goto IL_003f;
	}

IL_003e:
	{
		G_B7_0 = 0;
	}

IL_003f:
	{
		return (bool)G_B7_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstring::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionSubstring_Evaluate_m2782613730_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionSubstring_Evaluate_m2782613730 (XPathFunctionSubstring_t1649127225 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring_Evaluate_m2782613730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t4217024437 * L_3 = __this->get_arg1_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		double L_6 = bankers_round(L_5);
		V_1 = ((double)((double)L_6-(double)(1.0)));
		double L_7 = V_1;
		bool L_8 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004c;
		}
	}
	{
		double L_9 = V_1;
		bool L_10 = Double_IsNegativeInfinity_m553043933(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_004c;
		}
	}
	{
		double L_11 = V_1;
		String_t* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m2979997331(L_12, /*hidden argument*/NULL);
		if ((!(((double)L_11) >= ((double)(((double)((double)L_13)))))))
		{
			goto IL_0052;
		}
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_14;
	}

IL_0052:
	{
		Expression_t4217024437 * L_15 = __this->get_arg2_2();
		if (L_15)
		{
			goto IL_007f;
		}
	}
	{
		double L_16 = V_1;
		if ((!(((double)L_16) < ((double)(0.0)))))
		{
			goto IL_0076;
		}
	}
	{
		V_1 = (0.0);
	}

IL_0076:
	{
		String_t* L_17 = V_0;
		double L_18 = V_1;
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m2809233063(L_17, (((int32_t)((int32_t)L_18))), /*hidden argument*/NULL);
		return L_19;
	}

IL_007f:
	{
		Expression_t4217024437 * L_20 = __this->get_arg2_2();
		BaseIterator_t3696600956 * L_21 = ___iter0;
		NullCheck(L_20);
		double L_22 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_20, L_21);
		double L_23 = bankers_round(L_22);
		V_2 = L_23;
		double L_24 = V_2;
		bool L_25 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_26;
	}

IL_00a2:
	{
		double L_27 = V_1;
		if ((((double)L_27) < ((double)(0.0))))
		{
			goto IL_00c0;
		}
	}
	{
		double L_28 = V_2;
		if ((!(((double)L_28) < ((double)(0.0)))))
		{
			goto IL_00e3;
		}
	}

IL_00c0:
	{
		double L_29 = V_1;
		double L_30 = V_2;
		V_2 = ((double)((double)L_29+(double)L_30));
		double L_31 = V_2;
		if ((!(((double)L_31) <= ((double)(0.0)))))
		{
			goto IL_00d9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_32;
	}

IL_00d9:
	{
		V_1 = (0.0);
	}

IL_00e3:
	{
		String_t* L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m2979997331(L_33, /*hidden argument*/NULL);
		double L_35 = V_1;
		V_3 = ((double)((double)(((double)((double)L_34)))-(double)L_35));
		double L_36 = V_2;
		double L_37 = V_3;
		if ((!(((double)L_36) > ((double)L_37))))
		{
			goto IL_00f6;
		}
	}
	{
		double L_38 = V_3;
		V_2 = L_38;
	}

IL_00f6:
	{
		String_t* L_39 = V_0;
		double L_40 = V_1;
		double L_41 = V_2;
		NullCheck(L_39);
		String_t* L_42 = String_Substring_m675079568(L_39, (((int32_t)((int32_t)L_40))), (((int32_t)((int32_t)L_41))), /*hidden argument*/NULL);
		return L_42;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstring::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3561905143;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionSubstring_ToString_m97244847_MetadataUsageId;
extern "C"  String_t* XPathFunctionSubstring_ToString_m97244847 (XPathFunctionSubstring_t1649127225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring_ToString_m97244847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3561905143);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3561905143);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_5 = L_4;
		Expression_t4217024437 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t2956870243* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral44);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_9 = L_8;
		Expression_t4217024437 * L_10 = __this->get_arg2_2();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 5);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = L_9;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, _stringLiteral41);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstringAfter::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1846764191;
extern const uint32_t XPathFunctionSubstringAfter__ctor_m4187633269_MetadataUsageId;
extern "C"  void XPathFunctionSubstringAfter__ctor_m4187633269 (XPathFunctionSubstringAfter_t2731175421 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter__ctor_m4187633269_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t3402201403 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t2353341743 * L_7 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, _stringLiteral1846764191, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t3402201403 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t4217024437 * L_9 = FunctionArguments_get_Arg_m969707333(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t3402201403 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t3402201403 * L_11 = FunctionArguments_get_Tail_m52932169(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t4217024437 * L_12 = FunctionArguments_get_Arg_m969707333(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstringAfter::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstringAfter_get_ReturnType_m4232721039 (XPathFunctionSubstringAfter_t2731175421 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstringAfter::get_Peer()
extern "C"  bool XPathFunctionSubstringAfter_get_Peer_m94219979 (XPathFunctionSubstringAfter_t2731175421 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstringAfter::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionSubstringAfter_Evaluate_m3905912518_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionSubstringAfter_Evaluate_m3905912518 (XPathFunctionSubstringAfter_t2731175421 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter_Evaluate_m3905912518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t4217024437 * L_3 = __this->get_arg1_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		String_t* L_6 = V_0;
		String_t* L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = String_IndexOf_m1476794331(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_002f:
	{
		String_t* L_11 = V_0;
		int32_t L_12 = V_2;
		String_t* L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m2979997331(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_15 = String_Substring_m2809233063(L_11, ((int32_t)((int32_t)L_12+(int32_t)L_14)), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstringAfter::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1994007752;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionSubstringAfter_ToString_m3496542695_MetadataUsageId;
extern "C"  String_t* XPathFunctionSubstringAfter_ToString_m3496542695 (XPathFunctionSubstringAfter_t2731175421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter_ToString_m3496542695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1994007752);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1994007752);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_5 = L_4;
		Expression_t4217024437 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t2956870243* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral41);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstringBefore::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3519749988;
extern const uint32_t XPathFunctionSubstringBefore__ctor_m498616712_MetadataUsageId;
extern "C"  void XPathFunctionSubstringBefore__ctor_m498616712 (XPathFunctionSubstringBefore_t2778086872 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore__ctor_m498616712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t3402201403 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t2353341743 * L_7 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, _stringLiteral3519749988, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t3402201403 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t4217024437 * L_9 = FunctionArguments_get_Arg_m969707333(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t3402201403 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t3402201403 * L_11 = FunctionArguments_get_Tail_m52932169(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t4217024437 * L_12 = FunctionArguments_get_Arg_m969707333(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstringBefore::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstringBefore_get_ReturnType_m529496916 (XPathFunctionSubstringBefore_t2778086872 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstringBefore::get_Peer()
extern "C"  bool XPathFunctionSubstringBefore_get_Peer_m3859992328 (XPathFunctionSubstringBefore_t2778086872 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstringBefore::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionSubstringBefore_Evaluate_m2540736547_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionSubstringBefore_Evaluate_m2540736547 (XPathFunctionSubstringBefore_t2778086872 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore_Evaluate_m2540736547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t4217024437 * L_3 = __this->get_arg1_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		String_t* L_6 = V_0;
		String_t* L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = String_IndexOf_m1476794331(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_002f:
	{
		String_t* L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		String_t* L_13 = String_Substring_m675079568(L_11, 0, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstringBefore::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2530943245;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionSubstringBefore_ToString_m1453021774_MetadataUsageId;
extern "C"  String_t* XPathFunctionSubstringBefore_ToString_m1453021774 (XPathFunctionSubstringBefore_t2778086872 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore_ToString_m1453021774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2530943245);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2530943245);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_5 = L_4;
		Expression_t4217024437 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t2956870243* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral41);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSum::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3026784563;
extern const uint32_t XPathFunctionSum__ctor_m1037722509_MetadataUsageId;
extern "C"  void XPathFunctionSum__ctor_m1037722509 (XPathFunctionSum_t1178274675 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum__ctor_m1037722509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t2353341743 * L_4 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3026784563, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t3402201403 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t4217024437 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSum::get_Peer()
extern "C"  bool XPathFunctionSum_get_Peer_m224515491 (XPathFunctionSum_t1178274675 * __this, const MethodInfo* method)
{
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSum::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionSum_Evaluate_m2525900520_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionSum_Evaluate_m2525900520 (XPathFunctionSum_t1178274675 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum_Evaluate_m2525900520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNodeIterator_t2394191562 * V_0 = NULL;
	double V_1 = 0.0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t3696600956 * L_2 = VirtFuncInvoker1< BaseIterator_t3696600956 *, BaseIterator_t3696600956 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		V_1 = (0.0);
		goto IL_002f;
	}

IL_001c:
	{
		double L_3 = V_1;
		XPathNodeIterator_t2394191562 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t1624538935 * L_5 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		double L_7 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = ((double)((double)L_3+(double)L_7));
	}

IL_002f:
	{
		XPathNodeIterator_t2394191562 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_001c;
		}
	}
	{
		double L_10 = V_1;
		double L_11 = L_10;
		Il2CppObject * L_12 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_11);
		return L_12;
	}
}
// System.String System.Xml.XPath.XPathFunctionSum::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3541821;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionSum_ToString_m3653312617_MetadataUsageId;
extern "C"  String_t* XPathFunctionSum_ToString_m3653312617 (XPathFunctionSum_t1178274675 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum_ToString_m3653312617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3541821);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3541821);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionTranslate::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1187128336;
extern const uint32_t XPathFunctionTranslate__ctor_m1616227242_MetadataUsageId;
extern "C"  void XPathFunctionTranslate__ctor_m1616227242 (XPathFunctionTranslate_t2171417142 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate__ctor_m1616227242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t3402201403 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t3402201403 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t3402201403 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t3402201403 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t3402201403 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t3402201403 * L_7 = ___args0;
		NullCheck(L_7);
		FunctionArguments_t3402201403 * L_8 = FunctionArguments_get_Tail_m52932169(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		FunctionArguments_t3402201403 * L_9 = FunctionArguments_get_Tail_m52932169(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		FunctionArguments_t3402201403 * L_10 = FunctionArguments_get_Tail_m52932169(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}

IL_003d:
	{
		XPathException_t2353341743 * L_11 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_11, _stringLiteral1187128336, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0048:
	{
		FunctionArguments_t3402201403 * L_12 = ___args0;
		NullCheck(L_12);
		Expression_t4217024437 * L_13 = FunctionArguments_get_Arg_m969707333(L_12, /*hidden argument*/NULL);
		__this->set_arg0_0(L_13);
		FunctionArguments_t3402201403 * L_14 = ___args0;
		NullCheck(L_14);
		FunctionArguments_t3402201403 * L_15 = FunctionArguments_get_Tail_m52932169(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Expression_t4217024437 * L_16 = FunctionArguments_get_Arg_m969707333(L_15, /*hidden argument*/NULL);
		__this->set_arg1_1(L_16);
		FunctionArguments_t3402201403 * L_17 = ___args0;
		NullCheck(L_17);
		FunctionArguments_t3402201403 * L_18 = FunctionArguments_get_Tail_m52932169(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		FunctionArguments_t3402201403 * L_19 = FunctionArguments_get_Tail_m52932169(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Expression_t4217024437 * L_20 = FunctionArguments_get_Arg_m969707333(L_19, /*hidden argument*/NULL);
		__this->set_arg2_2(L_20);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionTranslate::get_ReturnType()
extern "C"  int32_t XPathFunctionTranslate_get_ReturnType_m2841959410 (XPathFunctionTranslate_t2171417142 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTranslate::get_Peer()
extern "C"  bool XPathFunctionTranslate_get_Peer_m1850098470 (XPathFunctionTranslate_t2171417142 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Expression_t4217024437 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		Expression_t4217024437 * L_4 = __this->get_arg2_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_4);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B4_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B4_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionTranslate::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionTranslate_Evaluate_m4091121285_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionTranslate_Evaluate_m4091121285 (XPathFunctionTranslate_t2171417142 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate_Evaluate_m4091121285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	StringBuilder_t3822575854 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		Expression_t4217024437 * L_0 = __this->get_arg0_0();
		BaseIterator_t3696600956 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t4217024437 * L_3 = __this->get_arg1_1();
		BaseIterator_t3696600956 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		Expression_t4217024437 * L_6 = __this->get_arg2_2();
		BaseIterator_t3696600956 * L_7 = ___iter0;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		V_2 = L_8;
		String_t* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m2979997331(L_9, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_11 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_11, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		V_4 = 0;
		String_t* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m2979997331(L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m2979997331(L_14, /*hidden argument*/NULL);
		V_6 = L_15;
		goto IL_0095;
	}

IL_004b:
	{
		String_t* L_16 = V_1;
		String_t* L_17 = V_0;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		uint16_t L_19 = String_get_Chars_m3015341861(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_20 = String_IndexOf_m2775210486(L_16, L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		int32_t L_21 = V_7;
		if ((((int32_t)L_21) == ((int32_t)(-1))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_22 = V_7;
		int32_t L_23 = V_6;
		if ((((int32_t)L_22) >= ((int32_t)L_23)))
		{
			goto IL_007b;
		}
	}
	{
		StringBuilder_t3822575854 * L_24 = V_3;
		String_t* L_25 = V_2;
		int32_t L_26 = V_7;
		NullCheck(L_25);
		uint16_t L_27 = String_get_Chars_m3015341861(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m2143093878(L_24, L_27, /*hidden argument*/NULL);
	}

IL_007b:
	{
		goto IL_008f;
	}

IL_0080:
	{
		StringBuilder_t3822575854 * L_28 = V_3;
		String_t* L_29 = V_0;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		uint16_t L_31 = String_get_Chars_m3015341861(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		StringBuilder_Append_m2143093878(L_28, L_31, /*hidden argument*/NULL);
	}

IL_008f:
	{
		int32_t L_32 = V_4;
		V_4 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0095:
	{
		int32_t L_33 = V_4;
		int32_t L_34 = V_5;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_004b;
		}
	}
	{
		StringBuilder_t3822575854 * L_35 = V_3;
		NullCheck(L_35);
		String_t* L_36 = StringBuilder_ToString_m350379841(L_35, /*hidden argument*/NULL);
		return L_36;
	}
}
// System.String System.Xml.XPath.XPathFunctionTranslate::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3998139462;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionTranslate_ToString_m3157771820_MetadataUsageId;
extern "C"  String_t* XPathFunctionTranslate_ToString_m3157771820 (XPathFunctionTranslate_t2171417142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate_ToString_m3157771820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3998139462);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3998139462);
		StringU5BU5D_t2956870243* L_1 = L_0;
		Expression_t4217024437 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t2956870243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_5 = L_4;
		Expression_t4217024437 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t2956870243* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral44);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_9 = L_8;
		Expression_t4217024437 * L_10 = __this->get_arg2_2();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 5);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = L_9;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, _stringLiteral41);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Xml.XPath.XPathFunctionTrue::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral431422963;
extern const uint32_t XPathFunctionTrue__ctor_m3889883154_MetadataUsageId;
extern "C"  void XPathFunctionTrue__ctor_m3889883154 (XPathFunctionTrue_t1855532992 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue__ctor_m3889883154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m3002905248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t2353341743 * L_2 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_2, _stringLiteral431422963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_HasStaticValue()
extern "C"  bool XPathFunctionTrue_get_HasStaticValue_m683691669 (XPathFunctionTrue_t1855532992 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_StaticValueAsBoolean()
extern "C"  bool XPathFunctionTrue_get_StaticValueAsBoolean_m807479903 (XPathFunctionTrue_t1855532992 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_Peer()
extern "C"  bool XPathFunctionTrue_get_Peer_m423716558 (XPathFunctionTrue_t1855532992 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionTrue::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionTrue_Evaluate_m955960675_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionTrue_Evaluate_m955960675 (XPathFunctionTrue_t1855532992 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue_Evaluate_m955960675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((bool)1);
		Il2CppObject * L_1 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.XPathFunctionTrue::ToString()
extern Il2CppCodeGenString* _stringLiteral3429846799;
extern const uint32_t XPathFunctionTrue_ToString_m3132420010_MetadataUsageId;
extern "C"  String_t* XPathFunctionTrue_ToString_m3132420010 (XPathFunctionTrue_t1855532992 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue_ToString_m3132420010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3429846799;
	}
}
// System.Void System.Xml.XPath.XPathItem::.ctor()
extern "C"  void XPathItem__ctor_m4134533980 (XPathItem_t880576077 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathIteratorComparer::.ctor()
extern "C"  void XPathIteratorComparer__ctor_m2155618996 (XPathIteratorComparer_t4238022261 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathIteratorComparer::.cctor()
extern Il2CppClass* XPathIteratorComparer_t4238022261_il2cpp_TypeInfo_var;
extern const uint32_t XPathIteratorComparer__cctor_m1917583225_MetadataUsageId;
extern "C"  void XPathIteratorComparer__cctor_m1917583225 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathIteratorComparer__cctor_m1917583225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XPathIteratorComparer_t4238022261 * L_0 = (XPathIteratorComparer_t4238022261 *)il2cpp_codegen_object_new(XPathIteratorComparer_t4238022261_il2cpp_TypeInfo_var);
		XPathIteratorComparer__ctor_m2155618996(L_0, /*hidden argument*/NULL);
		((XPathIteratorComparer_t4238022261_StaticFields*)XPathIteratorComparer_t4238022261_il2cpp_TypeInfo_var->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Int32 System.Xml.XPath.XPathIteratorComparer::Compare(System.Object,System.Object)
extern Il2CppClass* XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var;
extern const uint32_t XPathIteratorComparer_Compare_m764851373_MetadataUsageId;
extern "C"  int32_t XPathIteratorComparer_Compare_m764851373 (XPathIteratorComparer_t4238022261 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathIteratorComparer_Compare_m764851373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNodeIterator_t2394191562 * V_0 = NULL;
	XPathNodeIterator_t2394191562 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNodeIterator_t2394191562 *)IsInstClass(L_0, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNodeIterator_t2394191562 *)IsInstClass(L_1, XPathNodeIterator_t2394191562_il2cpp_TypeInfo_var));
		XPathNodeIterator_t2394191562 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		XPathNodeIterator_t2394191562 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return 1;
	}

IL_001e:
	{
		XPathNodeIterator_t2394191562 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t1624538935 * L_5 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		XPathNodeIterator_t2394191562 * L_6 = V_1;
		NullCheck(L_6);
		XPathNavigator_t1624538935 * L_7 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_6);
		NullCheck(L_5);
		int32_t L_8 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_5, L_7);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0047;
	}

IL_0043:
	{
		return 0;
	}

IL_0045:
	{
		return (-1);
	}

IL_0047:
	{
		return 1;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::.ctor()
extern "C"  void XPathNavigator__ctor_m2215010048 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		XPathItem__ctor_m4134533980(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::.cctor()
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D18_9_FieldInfo_var;
extern const uint32_t XPathNavigator__cctor_m3758705837_MetadataUsageId;
extern "C"  void XPathNavigator__cctor_m3758705837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator__cctor_m3758705837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t3416858730* L_0 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)38));
		CharU5BU5D_t3416858730* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint16_t)((int32_t)60));
		CharU5BU5D_t3416858730* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint16_t)((int32_t)62));
		((XPathNavigator_t1624538935_StaticFields*)XPathNavigator_t1624538935_il2cpp_TypeInfo_var->static_fields)->set_escape_text_chars_0(L_2);
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)6));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D18_9_FieldInfo_var), /*hidden argument*/NULL);
		((XPathNavigator_t1624538935_StaticFields*)XPathNavigator_t1624538935_il2cpp_TypeInfo_var->static_fields)->set_escape_attr_chars_1(L_3);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNavigator::System.ICloneable.Clone()
extern "C"  Il2CppObject * XPathNavigator_System_ICloneable_Clone_m1822899695 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_HasAttributes()
extern "C"  bool XPathNavigator_get_HasAttributes_m238100238 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_HasChildren()
extern "C"  bool XPathNavigator_get_HasChildren_m3581374262 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		return (bool)1;
	}
}
// System.String System.Xml.XPath.XPathNavigator::get_XmlLang()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3314158;
extern Il2CppCodeGenString* _stringLiteral1952986079;
extern const uint32_t XPathNavigator_get_XmlLang_m3014372733_MetadataUsageId;
extern "C"  String_t* XPathNavigator_get_XmlLang_m3014372733 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_get_XmlLang_m3014372733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1624538935 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		XPathNavigator_t1624538935 * L_0 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t1624538935 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_0021;
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	{
		XPathNavigator_t1624538935 * L_5 = V_0;
		NullCheck(L_5);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_5);
		goto IL_002d;
	}

IL_002d:
	{
		XPathNavigator_t1624538935 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(20 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String) */, L_6, _stringLiteral3314158, _stringLiteral1952986079);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		XPathNavigator_t1624538935 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_8);
		return L_9;
	}

IL_0049:
	{
		XPathNavigator_t1624538935 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_10);
		if (L_11)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_12;
	}
}
// System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator)
extern "C"  int32_t XPathNavigator_ComparePosition_m3713421981 (XPathNavigator_t1624538935 * __this, XPathNavigator_t1624538935 * ___nav0, const MethodInfo* method)
{
	XPathNavigator_t1624538935 * V_0 = NULL;
	XPathNavigator_t1624538935 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		XPathNavigator_t1624538935 * L_0 = ___nav0;
		bool L_1 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, __this, L_0);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (int32_t)(2);
	}

IL_000e:
	{
		XPathNavigator_t1624538935 * L_2 = ___nav0;
		bool L_3 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, __this, L_2);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (int32_t)(0);
	}

IL_001c:
	{
		XPathNavigator_t1624538935 * L_4 = ___nav0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, L_4, __this);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		return (int32_t)(1);
	}

IL_002a:
	{
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_6;
		XPathNavigator_t1624538935 * L_7 = ___nav0;
		NullCheck(L_7);
		XPathNavigator_t1624538935 * L_8 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_7);
		V_1 = L_8;
		XPathNavigator_t1624538935 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_9);
		XPathNavigator_t1624538935 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_10);
		XPathNavigator_t1624538935 * L_11 = V_0;
		XPathNavigator_t1624538935 * L_12 = V_1;
		NullCheck(L_11);
		bool L_13 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_11, L_12);
		if (L_13)
		{
			goto IL_0052;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0052:
	{
		XPathNavigator_t1624538935 * L_14 = V_0;
		NullCheck(L_14);
		VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_14, __this);
		XPathNavigator_t1624538935 * L_15 = V_1;
		XPathNavigator_t1624538935 * L_16 = ___nav0;
		NullCheck(L_15);
		VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_15, L_16);
		V_2 = 0;
		goto IL_006d;
	}

IL_0069:
	{
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006d:
	{
		XPathNavigator_t1624538935 * L_18 = V_0;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_18);
		if (L_19)
		{
			goto IL_0069;
		}
	}
	{
		XPathNavigator_t1624538935 * L_20 = V_0;
		NullCheck(L_20);
		VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_20, __this);
		V_3 = 0;
		goto IL_008b;
	}

IL_0087:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_008b:
	{
		XPathNavigator_t1624538935 * L_22 = V_1;
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_22);
		if (L_23)
		{
			goto IL_0087;
		}
	}
	{
		XPathNavigator_t1624538935 * L_24 = V_1;
		XPathNavigator_t1624538935 * L_25 = ___nav0;
		NullCheck(L_24);
		VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_24, L_25);
		int32_t L_26 = V_2;
		V_4 = L_26;
		goto IL_00b3;
	}

IL_00a6:
	{
		XPathNavigator_t1624538935 * L_27 = V_0;
		NullCheck(L_27);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_27);
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_00b3:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = V_3;
		if ((((int32_t)L_29) > ((int32_t)L_30)))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_31 = V_3;
		V_5 = L_31;
		goto IL_00d0;
	}

IL_00c3:
	{
		XPathNavigator_t1624538935 * L_32 = V_1;
		NullCheck(L_32);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_32);
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33-(int32_t)1));
	}

IL_00d0:
	{
		int32_t L_34 = V_5;
		int32_t L_35 = V_4;
		if ((((int32_t)L_34) > ((int32_t)L_35)))
		{
			goto IL_00c3;
		}
	}
	{
		goto IL_00f2;
	}

IL_00de:
	{
		XPathNavigator_t1624538935 * L_36 = V_0;
		NullCheck(L_36);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_36);
		XPathNavigator_t1624538935 * L_37 = V_1;
		NullCheck(L_37);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_37);
		int32_t L_38 = V_4;
		V_4 = ((int32_t)((int32_t)L_38-(int32_t)1));
	}

IL_00f2:
	{
		XPathNavigator_t1624538935 * L_39 = V_0;
		XPathNavigator_t1624538935 * L_40 = V_1;
		NullCheck(L_39);
		bool L_41 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_39, L_40);
		if (!L_41)
		{
			goto IL_00de;
		}
	}
	{
		XPathNavigator_t1624538935 * L_42 = V_0;
		NullCheck(L_42);
		VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_42, __this);
		int32_t L_43 = V_2;
		V_6 = L_43;
		goto IL_011b;
	}

IL_010e:
	{
		XPathNavigator_t1624538935 * L_44 = V_0;
		NullCheck(L_44);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_44);
		int32_t L_45 = V_6;
		V_6 = ((int32_t)((int32_t)L_45-(int32_t)1));
	}

IL_011b:
	{
		int32_t L_46 = V_6;
		int32_t L_47 = V_4;
		if ((((int32_t)L_46) > ((int32_t)((int32_t)((int32_t)L_47+(int32_t)1)))))
		{
			goto IL_010e;
		}
	}
	{
		XPathNavigator_t1624538935 * L_48 = V_1;
		XPathNavigator_t1624538935 * L_49 = ___nav0;
		NullCheck(L_48);
		VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_48, L_49);
		int32_t L_50 = V_3;
		V_7 = L_50;
		goto IL_0143;
	}

IL_0136:
	{
		XPathNavigator_t1624538935 * L_51 = V_1;
		NullCheck(L_51);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_51);
		int32_t L_52 = V_7;
		V_7 = ((int32_t)((int32_t)L_52-(int32_t)1));
	}

IL_0143:
	{
		int32_t L_53 = V_7;
		int32_t L_54 = V_4;
		if ((((int32_t)L_53) > ((int32_t)((int32_t)((int32_t)L_54+(int32_t)1)))))
		{
			goto IL_0136;
		}
	}
	{
		XPathNavigator_t1624538935 * L_55 = V_0;
		NullCheck(L_55);
		int32_t L_56 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_55);
		if ((!(((uint32_t)L_56) == ((uint32_t)3))))
		{
			goto IL_0188;
		}
	}
	{
		XPathNavigator_t1624538935 * L_57 = V_1;
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_57);
		if ((((int32_t)L_58) == ((int32_t)3)))
		{
			goto IL_0168;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0168:
	{
		goto IL_017b;
	}

IL_016d:
	{
		XPathNavigator_t1624538935 * L_59 = V_0;
		XPathNavigator_t1624538935 * L_60 = V_1;
		NullCheck(L_59);
		bool L_61 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_59, L_60);
		if (!L_61)
		{
			goto IL_017b;
		}
	}
	{
		return (int32_t)(0);
	}

IL_017b:
	{
		XPathNavigator_t1624538935 * L_62 = V_0;
		NullCheck(L_62);
		bool L_63 = XPathNavigator_MoveToNextNamespace_m197928770(L_62, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_016d;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0188:
	{
		XPathNavigator_t1624538935 * L_64 = V_1;
		NullCheck(L_64);
		int32_t L_65 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_64);
		if ((!(((uint32_t)L_65) == ((uint32_t)3))))
		{
			goto IL_0196;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0196:
	{
		XPathNavigator_t1624538935 * L_66 = V_0;
		NullCheck(L_66);
		int32_t L_67 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_66);
		if ((!(((uint32_t)L_67) == ((uint32_t)2))))
		{
			goto IL_01d0;
		}
	}
	{
		XPathNavigator_t1624538935 * L_68 = V_1;
		NullCheck(L_68);
		int32_t L_69 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_68);
		if ((((int32_t)L_69) == ((int32_t)2)))
		{
			goto IL_01b0;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01b0:
	{
		goto IL_01c3;
	}

IL_01b5:
	{
		XPathNavigator_t1624538935 * L_70 = V_0;
		XPathNavigator_t1624538935 * L_71 = V_1;
		NullCheck(L_70);
		bool L_72 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_70, L_71);
		if (!L_72)
		{
			goto IL_01c3;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01c3:
	{
		XPathNavigator_t1624538935 * L_73 = V_0;
		NullCheck(L_73);
		bool L_74 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, L_73);
		if (L_74)
		{
			goto IL_01b5;
		}
	}
	{
		return (int32_t)(1);
	}

IL_01d0:
	{
		goto IL_01e3;
	}

IL_01d5:
	{
		XPathNavigator_t1624538935 * L_75 = V_0;
		XPathNavigator_t1624538935 * L_76 = V_1;
		NullCheck(L_75);
		bool L_77 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_75, L_76);
		if (!L_77)
		{
			goto IL_01e3;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01e3:
	{
		XPathNavigator_t1624538935 * L_78 = V_0;
		NullCheck(L_78);
		bool L_79 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_78);
		if (L_79)
		{
			goto IL_01d5;
		}
	}
	{
		return (int32_t)(1);
	}
}
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathNavigator::Compile(System.String)
extern "C"  XPathExpression_t3441638418 * XPathNavigator_Compile_m3812787918 (XPathNavigator_t1624538935 * __this, String_t* ___xpath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___xpath0;
		XPathExpression_t3441638418 * L_1 = XPathExpression_Compile_m1263926045(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator)
extern "C"  bool XPathNavigator_IsDescendant_m3913947374 (XPathNavigator_t1624538935 * __this, XPathNavigator_t1624538935 * ___nav0, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = ___nav0;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		XPathNavigator_t1624538935 * L_1 = ___nav0;
		NullCheck(L_1);
		XPathNavigator_t1624538935 * L_2 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_1);
		___nav0 = L_2;
		goto IL_0021;
	}

IL_0013:
	{
		XPathNavigator_t1624538935 * L_3 = ___nav0;
		bool L_4 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, __this, L_3);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		XPathNavigator_t1624538935 * L_5 = ___nav0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_5);
		if (L_6)
		{
			goto IL_0013;
		}
	}

IL_002c:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_MoveToAttribute_m192146504_MetadataUsageId;
extern "C"  bool XPathNavigator_MoveToAttribute_m192146504 (XPathNavigator_t1624538935 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_MoveToAttribute_m192146504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, __this);
		if (!L_0)
		{
			goto IL_0041;
		}
	}

IL_000b:
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, __this);
		String_t* L_2 = ___localName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, __this);
		String_t* L_5 = ___namespaceURI1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		return (bool)1;
	}

IL_002f:
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, __this);
		if (L_7)
		{
			goto IL_000b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_MoveToNamespace_m3481610349_MetadataUsageId;
extern "C"  bool XPathNavigator_MoveToNamespace_m3481610349 (XPathNavigator_t1624538935 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_MoveToNamespace_m3481610349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = XPathNavigator_MoveToFirstNamespace_m3738691923(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}

IL_000b:
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, __this);
		String_t* L_2 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)1;
	}

IL_001e:
	{
		bool L_4 = XPathNavigator_MoveToNextNamespace_m197928770(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_000b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirst()
extern "C"  bool XPathNavigator_MoveToFirst_m3064231242 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		bool L_0 = XPathNavigator_MoveToFirstImpl_m1974955786(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::MoveToRoot()
extern "C"  void XPathNavigator_MoveToRoot_m4278006194 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		goto IL_0005;
	}

IL_0005:
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstImpl()
extern "C"  bool XPathNavigator_MoveToFirstImpl_m1974955786 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, __this);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_001a;
		}
	}
	{
		goto IL_001c;
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, __this);
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace()
extern "C"  bool XPathNavigator_MoveToFirstNamespace_m3738691923 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, int32_t >::Invoke(26 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope) */, __this, 0);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace()
extern "C"  bool XPathNavigator_MoveToNextNamespace_m197928770 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, int32_t >::Invoke(30 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope) */, __this, 0);
		return L_0;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression)
extern "C"  XPathNodeIterator_t2394191562 * XPathNavigator_Select_m1543264903 (XPathNavigator_t1624538935 * __this, XPathExpression_t3441638418 * ___expr0, const MethodInfo* method)
{
	{
		XPathExpression_t3441638418 * L_0 = ___expr0;
		XPathNodeIterator_t2394191562 * L_1 = XPathNavigator_Select_m2072065794(__this, L_0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression,System.Xml.IXmlNamespaceResolver)
extern Il2CppClass* CompiledExpression_t938776358_il2cpp_TypeInfo_var;
extern Il2CppClass* NullIterator_t979652658_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_Select_m2072065794_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * XPathNavigator_Select_m2072065794 (XPathNavigator_t1624538935 * __this, XPathExpression_t3441638418 * ___expr0, Il2CppObject * ___ctx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_Select_m2072065794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CompiledExpression_t938776358 * V_0 = NULL;
	BaseIterator_t3696600956 * V_1 = NULL;
	{
		XPathExpression_t3441638418 * L_0 = ___expr0;
		V_0 = ((CompiledExpression_t938776358 *)CastclassClass(L_0, CompiledExpression_t938776358_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___ctx1;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		CompiledExpression_t938776358 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = CompiledExpression_get_NamespaceManager_m652200026(L_2, /*hidden argument*/NULL);
		___ctx1 = L_3;
	}

IL_0015:
	{
		Il2CppObject * L_4 = ___ctx1;
		NullIterator_t979652658 * L_5 = (NullIterator_t979652658 *)il2cpp_codegen_object_new(NullIterator_t979652658_il2cpp_TypeInfo_var);
		NullIterator__ctor_m4158990173(L_5, __this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		CompiledExpression_t938776358 * L_6 = V_0;
		BaseIterator_t3696600956 * L_7 = V_1;
		NullCheck(L_6);
		XPathNodeIterator_t2394191562 * L_8 = CompiledExpression_EvaluateNodeSet_m2426588395(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Collections.IEnumerable System.Xml.XPath.XPathNavigator::EnumerateChildren(System.Xml.XPath.XPathNavigator,System.Xml.XPath.XPathNodeType)
extern Il2CppClass* U3CEnumerateChildrenU3Ec__Iterator0_t3706982454_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_EnumerateChildren_m710965660_MetadataUsageId;
extern "C"  Il2CppObject * XPathNavigator_EnumerateChildren_m710965660 (Il2CppObject * __this /* static, unused */, XPathNavigator_t1624538935 * ___n0, int32_t ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_EnumerateChildren_m710965660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * V_0 = NULL;
	{
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_0 = (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 *)il2cpp_codegen_object_new(U3CEnumerateChildrenU3Ec__Iterator0_t3706982454_il2cpp_TypeInfo_var);
		U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2265504953(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_1 = V_0;
		XPathNavigator_t1624538935 * L_2 = ___n0;
		NullCheck(L_1);
		L_1->set_n_0(L_2);
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_3 = V_0;
		int32_t L_4 = ___type1;
		NullCheck(L_3);
		L_3->set_type_3(L_4);
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_5 = V_0;
		XPathNavigator_t1624538935 * L_6 = ___n0;
		NullCheck(L_5);
		L_5->set_U3CU24U3En_6(L_6);
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_7 = V_0;
		int32_t L_8 = ___type1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Etype_7(L_8);
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_9 = V_0;
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_10 = L_9;
		NullCheck(L_10);
		L_10->set_U24PC_4(((int32_t)-2));
		return L_10;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::SelectChildren(System.Xml.XPath.XPathNodeType)
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern Il2CppClass* EnumerableIterator_t325940342_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperIterator_t2702681854_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_SelectChildren_m2299731170_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * XPathNavigator_SelectChildren_m2299731170 (XPathNavigator_t1624538935 * __this, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_SelectChildren_m2299731170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigator_t1624538935_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = XPathNavigator_EnumerateChildren_m710965660(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		EnumerableIterator_t325940342 * L_2 = (EnumerableIterator_t325940342 *)il2cpp_codegen_object_new(EnumerableIterator_t325940342_il2cpp_TypeInfo_var);
		EnumerableIterator__ctor_m3124749935(L_2, L_1, 0, /*hidden argument*/NULL);
		WrapperIterator_t2702681854 * L_3 = (WrapperIterator_t2702681854 *)il2cpp_codegen_object_new(WrapperIterator_t2702681854_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m802118820(L_3, L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathNavigator::ToString()
extern "C"  String_t* XPathNavigator_ToString_m2589892525 (XPathNavigator_t1624538935 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		return L_0;
	}
}
// System.String System.Xml.XPath.XPathNavigator::LookupNamespace(System.String)
extern "C"  String_t* XPathNavigator_LookupNamespace_m3979214784 (XPathNavigator_t1624538935 * __this, String_t* ___prefix0, const MethodInfo* method)
{
	XPathNavigator_t1624538935 * V_0 = NULL;
	{
		XPathNavigator_t1624538935 * L_0 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t1624538935 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigator_t1624538935 * L_3 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_3);
	}

IL_001a:
	{
		XPathNavigator_t1624538935 * L_4 = V_0;
		String_t* L_5 = ___prefix0;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, String_t* >::Invoke(21 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		XPathNavigator_t1624538935 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_7);
		return L_8;
	}

IL_002d:
	{
		return (String_t*)NULL;
	}
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::.ctor()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2265504953 (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m574357945 (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2855442253 (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m2006013620 (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1876009558(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern Il2CppClass* U3CEnumerateChildrenU3Ec__Iterator0_t3706982454_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1876009558_MetadataUsageId;
extern "C"  Il2CppObject* U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1876009558 (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1876009558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_2 = (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 *)il2cpp_codegen_object_new(U3CEnumerateChildrenU3Ec__Iterator0_t3706982454_il2cpp_TypeInfo_var);
		U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2265504953(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_3 = V_0;
		XPathNavigator_t1624538935 * L_4 = __this->get_U3CU24U3En_6();
		NullCheck(L_3);
		L_3->set_n_0(L_4);
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_5 = V_0;
		int32_t L_6 = __this->get_U3CU24U3Etype_7();
		NullCheck(L_5);
		L_5->set_type_3(L_6);
		U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::MoveNext()
extern "C"  bool U3CEnumerateChildrenU3Ec__Iterator0_MoveNext_m4101567175 (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00d4;
		}
	}
	{
		goto IL_00eb;
	}

IL_0021:
	{
		XPathNavigator_t1624538935 * L_2 = __this->get_n_0();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_2);
		if (L_3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_00eb;
	}

IL_0036:
	{
		XPathNavigator_t1624538935 * L_4 = __this->get_n_0();
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_4);
		XPathNavigator_t1624538935 * L_5 = __this->get_n_0();
		NullCheck(L_5);
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_5);
		__this->set_U3CnavU3E__0_1(L_6);
		XPathNavigator_t1624538935 * L_7 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_7);
		VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_7);
		__this->set_U3Cnav2U3E__1_2((XPathNavigator_t1624538935 *)NULL);
	}

IL_0066:
	{
		int32_t L_8 = __this->get_type_3();
		if ((((int32_t)L_8) == ((int32_t)((int32_t)9))))
		{
			goto IL_0089;
		}
	}
	{
		XPathNavigator_t1624538935 * L_9 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_9);
		int32_t L_11 = __this->get_type_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_00d4;
		}
	}

IL_0089:
	{
		XPathNavigator_t1624538935 * L_12 = __this->get_U3Cnav2U3E__1_2();
		if (L_12)
		{
			goto IL_00aa;
		}
	}
	{
		XPathNavigator_t1624538935 * L_13 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_13);
		XPathNavigator_t1624538935 * L_14 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_13);
		__this->set_U3Cnav2U3E__1_2(L_14);
		goto IL_00bc;
	}

IL_00aa:
	{
		XPathNavigator_t1624538935 * L_15 = __this->get_U3Cnav2U3E__1_2();
		XPathNavigator_t1624538935 * L_16 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_15);
		VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_15, L_16);
	}

IL_00bc:
	{
		XPathNavigator_t1624538935 * L_17 = __this->get_U3Cnav2U3E__1_2();
		__this->set_U24current_5(L_17);
		__this->set_U24PC_4(1);
		goto IL_00ed;
	}

IL_00d4:
	{
		XPathNavigator_t1624538935 * L_18 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_18);
		if (L_19)
		{
			goto IL_0066;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00eb:
	{
		return (bool)0;
	}

IL_00ed:
	{
		return (bool)1;
	}
	// Dead block : IL_00ef: ldloc.1
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::Dispose()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0_Dispose_m3795091126 (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateChildrenU3Ec__Iterator0_Reset_m4206905190_MetadataUsageId;
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0_Reset_m4206905190 (U3CEnumerateChildrenU3Ec__Iterator0_t3706982454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateChildrenU3Ec__Iterator0_Reset_m4206905190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Xml.XPath.XPathNavigator/EnumerableIterator::.ctor(System.Collections.IEnumerable,System.Int32)
extern "C"  void EnumerableIterator__ctor_m3124749935 (EnumerableIterator_t325940342 * __this, Il2CppObject * ___source0, int32_t ___pos1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		XPathNodeIterator__ctor_m2186940031(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___source0;
		__this->set_source_1(L_0);
		V_0 = 0;
		goto IL_001f;
	}

IL_0014:
	{
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNavigator/EnumerableIterator::MoveNext() */, __this);
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_2 = V_0;
		int32_t L_3 = ___pos1;
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator/EnumerableIterator::Clone()
extern Il2CppClass* EnumerableIterator_t325940342_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_Clone_m1533086993_MetadataUsageId;
extern "C"  XPathNodeIterator_t2394191562 * EnumerableIterator_Clone_m1533086993 (EnumerableIterator_t325940342 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_Clone_m1533086993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_source_1();
		int32_t L_1 = __this->get_pos_3();
		EnumerableIterator_t325940342 * L_2 = (EnumerableIterator_t325940342 *)il2cpp_codegen_object_new(EnumerableIterator_t325940342_il2cpp_TypeInfo_var);
		EnumerableIterator__ctor_m3124749935(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator/EnumerableIterator::MoveNext()
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_MoveNext_m1878288367_MetadataUsageId;
extern "C"  bool EnumerableIterator_MoveNext_m1878288367 (EnumerableIterator_t325940342 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_MoveNext_m1878288367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_e_2();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_source_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_1);
		__this->set_e_2(L_2);
	}

IL_001c:
	{
		Il2CppObject * L_3 = __this->get_e_2();
		NullCheck(L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_3);
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)0;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_pos_3();
		__this->set_pos_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return (bool)1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigator/EnumerableIterator::get_CurrentPosition()
extern "C"  int32_t EnumerableIterator_get_CurrentPosition_m3954342198 (EnumerableIterator_t325940342 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_pos_3();
		return L_0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/EnumerableIterator::get_Current()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_get_Current_m2720214749_MetadataUsageId;
extern "C"  XPathNavigator_t1624538935 * EnumerableIterator_get_Current_m2720214749 (EnumerableIterator_t325940342 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_get_Current_m2720214749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1624538935 * G_B3_0 = NULL;
	{
		int32_t L_0 = __this->get_pos_3();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1624538935 *)(NULL));
		goto IL_0021;
	}

IL_0011:
	{
		Il2CppObject * L_1 = __this->get_e_2();
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = ((XPathNavigator_t1624538935 *)CastclassClass(L_2, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.XPathNavigatorComparer::.ctor()
extern "C"  void XPathNavigatorComparer__ctor_m2769507411 (XPathNavigatorComparer_t877816196 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigatorComparer::.cctor()
extern Il2CppClass* XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer__cctor_m3768254906_MetadataUsageId;
extern "C"  void XPathNavigatorComparer__cctor_m3768254906 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer__cctor_m3768254906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XPathNavigatorComparer_t877816196 * L_0 = (XPathNavigatorComparer_t877816196 *)il2cpp_codegen_object_new(XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var);
		XPathNavigatorComparer__ctor_m2769507411(L_0, /*hidden argument*/NULL);
		((XPathNavigatorComparer_t877816196_StaticFields*)XPathNavigatorComparer_t877816196_il2cpp_TypeInfo_var->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigatorComparer::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m2382237652_MetadataUsageId;
extern "C"  bool XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m2382237652 (XPathNavigatorComparer_t877816196 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m2382237652_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1624538935 * V_0 = NULL;
	XPathNavigator_t1624538935 * V_1 = NULL;
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNavigator_t1624538935 *)IsInstClass(L_0, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNavigator_t1624538935 *)IsInstClass(L_1, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
		XPathNavigator_t1624538935 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		XPathNavigator_t1624538935 * L_3 = V_1;
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		XPathNavigator_t1624538935 * L_4 = V_0;
		XPathNavigator_t1624538935 * L_5 = V_1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_4, L_5);
		G_B4_0 = ((int32_t)(L_6));
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigatorComparer::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t XPathNavigatorComparer_System_Collections_IEqualityComparer_GetHashCode_m608806058 (XPathNavigatorComparer_t877816196 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		return L_1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigatorComparer::Compare(System.Object,System.Object)
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer_Compare_m3372576234_MetadataUsageId;
extern "C"  int32_t XPathNavigatorComparer_Compare_m3372576234 (XPathNavigatorComparer_t877816196 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer_Compare_m3372576234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1624538935 * V_0 = NULL;
	XPathNavigator_t1624538935 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNavigator_t1624538935 *)IsInstClass(L_0, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNavigator_t1624538935 *)IsInstClass(L_1, XPathNavigator_t1624538935_il2cpp_TypeInfo_var));
		XPathNavigator_t1624538935 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		XPathNavigator_t1624538935 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return 1;
	}

IL_001e:
	{
		XPathNavigator_t1624538935 * L_4 = V_0;
		XPathNavigator_t1624538935 * L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_4, L_5);
		V_2 = L_6;
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_003d;
	}

IL_0039:
	{
		return 0;
	}

IL_003b:
	{
		return 1;
	}

IL_003d:
	{
		return (-1);
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator::.ctor()
extern "C"  void XPathNodeIterator__ctor_m2186940031 (XPathNodeIterator_t2394191562 * __this, const MethodInfo* method)
{
	{
		__this->set__count_0((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator::System.ICloneable.Clone()
extern "C"  Il2CppObject * XPathNodeIterator_System_ICloneable_Clone_m216028580 (XPathNodeIterator_t2394191562 * __this, const MethodInfo* method)
{
	{
		XPathNodeIterator_t2394191562 * L_0 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		return L_0;
	}
}
// System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count()
extern "C"  int32_t XPathNodeIterator_get_Count_m995835617 (XPathNodeIterator_t2394191562 * __this, const MethodInfo* method)
{
	XPathNodeIterator_t2394191562 * V_0 = NULL;
	{
		int32_t L_0 = __this->get__count_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		XPathNodeIterator_t2394191562 * L_1 = VirtFuncInvoker0< XPathNodeIterator_t2394191562 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		XPathNodeIterator_t2394191562 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_2);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		XPathNodeIterator_t2394191562 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_CurrentPosition() */, L_4);
		__this->set__count_0(L_5);
	}

IL_002f:
	{
		int32_t L_6 = __this->get__count_0();
		return L_6;
	}
}
// System.Collections.IEnumerator System.Xml.XPath.XPathNodeIterator::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator2_t490656865_il2cpp_TypeInfo_var;
extern const uint32_t XPathNodeIterator_GetEnumerator_m3547213243_MetadataUsageId;
extern "C"  Il2CppObject * XPathNodeIterator_GetEnumerator_m3547213243 (XPathNodeIterator_t2394191562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNodeIterator_GetEnumerator_m3547213243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator2_t490656865 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator2_t490656865 * L_0 = (U3CGetEnumeratorU3Ec__Iterator2_t490656865 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator2_t490656865_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator2__ctor_m3892894189(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator2_t490656865 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CGetEnumeratorU3Ec__Iterator2_t490656865 * L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2__ctor_m3892894189 (U3CGetEnumeratorU3Ec__Iterator2_t490656865 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3164273103 (U3CGetEnumeratorU3Ec__Iterator2_t490656865 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3326114659 (U3CGetEnumeratorU3Ec__Iterator2_t490656865 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator2_MoveNext_m1382971427 (U3CGetEnumeratorU3Ec__Iterator2_t490656865 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_005a;
	}

IL_0021:
	{
		goto IL_0043;
	}

IL_0026:
	{
		XPathNodeIterator_t2394191562 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		XPathNavigator_t1624538935 * L_3 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_2);
		__this->set_U24current_1(L_3);
		__this->set_U24PC_0(1);
		goto IL_005c;
	}

IL_0043:
	{
		XPathNodeIterator_t2394191562 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_4);
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_005a:
	{
		return (bool)0;
	}

IL_005c:
	{
		return (bool)1;
	}
	// Dead block : IL_005e: ldloc.1
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2_Dispose_m53083882 (U3CGetEnumeratorU3Ec__Iterator2_t490656865 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator2_Reset_m1539327130_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2_Reset_m1539327130 (U3CGetEnumeratorU3Ec__Iterator2_t490656865 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator2_Reset_m1539327130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Xml.XPath.XPathNumericFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathNumericFunction__ctor_m3502463899 (XPathNumericFunction_t3734910117 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method)
{
	{
		FunctionArguments_t3402201403 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathNumericFunction::get_ReturnType()
extern "C"  int32_t XPathNumericFunction_get_ReturnType_m4098330081 (XPathNumericFunction_t3734910117 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Object System.Xml.XPath.XPathNumericFunction::get_StaticValue()
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathNumericFunction_get_StaticValue_m649259103_MetadataUsageId;
extern "C"  Il2CppObject * XPathNumericFunction_get_StaticValue_m649259103 (XPathNumericFunction_t3734910117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNumericFunction_get_StaticValue_m649259103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, __this);
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathSortElement::.ctor()
extern "C"  void XPathSortElement__ctor_m2356815033 (XPathSortElement_t2708585374 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathSorter::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorter_Evaluate_m2674889470_MetadataUsageId;
extern "C"  Il2CppObject * XPathSorter_Evaluate_m2674889470 (XPathSorter_t3331359429 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorter_Evaluate_m2674889470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get__type_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_001e;
		}
	}
	{
		Expression_t4217024437 * L_1 = __this->get__expr_0();
		BaseIterator_t3696600956 * L_2 = ___iter0;
		NullCheck(L_1);
		double L_3 = VirtFuncInvoker1< double, BaseIterator_t3696600956 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}

IL_001e:
	{
		Expression_t4217024437 * L_6 = __this->get__expr_0();
		BaseIterator_t3696600956 * L_7 = ___iter0;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t3696600956 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		return L_8;
	}
}
// System.Int32 System.Xml.XPath.XPathSorter::Compare(System.Object,System.Object)
extern Il2CppClass* IComparer_t2207526184_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorter_Compare_m78114877_MetadataUsageId;
extern "C"  int32_t XPathSorter_Compare_m78114877 (XPathSorter_t3331359429 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorter_Compare_m78114877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__cmp_1();
		Il2CppObject * L_1 = ___o10;
		Il2CppObject * L_2 = ___o21;
		NullCheck(L_0);
		int32_t L_3 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t2207526184_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Int32 System.Xml.XPath.XPathSorters::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* XPathSortElement_t2708585374_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSorter_t3331359429_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_System_Collections_IComparer_Compare_m247046298_MetadataUsageId;
extern "C"  int32_t XPathSorters_System_Collections_IComparer_Compare_m247046298 (XPathSorters_t4176623784 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_System_Collections_IComparer_Compare_m247046298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathSortElement_t2708585374 * V_0 = NULL;
	XPathSortElement_t2708585374 * V_1 = NULL;
	int32_t V_2 = 0;
	XPathSorter_t3331359429 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathSortElement_t2708585374 *)CastclassClass(L_0, XPathSortElement_t2708585374_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathSortElement_t2708585374 *)CastclassClass(L_1, XPathSortElement_t2708585374_il2cpp_TypeInfo_var));
		V_2 = 0;
		goto IL_004d;
	}

IL_0015:
	{
		ArrayList_t2121638921 * L_2 = __this->get__rgSorters_0();
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_3 = ((XPathSorter_t3331359429 *)CastclassClass(L_4, XPathSorter_t3331359429_il2cpp_TypeInfo_var));
		XPathSorter_t3331359429 * L_5 = V_3;
		XPathSortElement_t2708585374 * L_6 = V_0;
		NullCheck(L_6);
		ObjectU5BU5D_t11523773* L_7 = L_6->get_Values_1();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		XPathSortElement_t2708585374 * L_10 = V_1;
		NullCheck(L_10);
		ObjectU5BU5D_t11523773* L_11 = L_10->get_Values_1();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		NullCheck(L_5);
		int32_t L_14 = XPathSorter_Compare_m78114877(L_5, ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))), ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))), /*hidden argument*/NULL);
		V_4 = L_14;
		int32_t L_15 = V_4;
		if (!L_15)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_16 = V_4;
		return L_16;
	}

IL_0049:
	{
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_18 = V_2;
		ArrayList_t2121638921 * L_19 = __this->get__rgSorters_0();
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_19);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_0015;
		}
	}
	{
		XPathSortElement_t2708585374 * L_21 = V_0;
		NullCheck(L_21);
		XPathNavigator_t1624538935 * L_22 = L_21->get_Navigator_0();
		XPathSortElement_t2708585374 * L_23 = V_1;
		NullCheck(L_23);
		XPathNavigator_t1624538935 * L_24 = L_23->get_Navigator_0();
		NullCheck(L_22);
		int32_t L_25 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_22, L_24);
		V_5 = L_25;
		int32_t L_26 = V_5;
		if ((((int32_t)L_26) == ((int32_t)1)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_27 = V_5;
		if ((((int32_t)L_27) == ((int32_t)2)))
		{
			goto IL_0086;
		}
	}
	{
		goto IL_008a;
	}

IL_0086:
	{
		return 0;
	}

IL_0088:
	{
		return 1;
	}

IL_008a:
	{
		return (-1);
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Xml.XPath.BaseIterator)
extern "C"  BaseIterator_t3696600956 * XPathSorters_Sort_m2612507910 (XPathSorters_t4176623784 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	ArrayList_t2121638921 * V_0 = NULL;
	{
		BaseIterator_t3696600956 * L_0 = ___iter0;
		ArrayList_t2121638921 * L_1 = XPathSorters_ToSortElementList_m2931616076(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ArrayList_t2121638921 * L_2 = V_0;
		BaseIterator_t3696600956 * L_3 = ___iter0;
		NullCheck(L_3);
		Il2CppObject * L_4 = BaseIterator_get_NamespaceManager_m3653702256(L_3, /*hidden argument*/NULL);
		BaseIterator_t3696600956 * L_5 = XPathSorters_Sort_m386310882(__this, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.ArrayList System.Xml.XPath.XPathSorters::ToSortElementList(System.Xml.XPath.BaseIterator)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSortElement_t2708585374_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSorter_t3331359429_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_ToSortElementList_m2931616076_MetadataUsageId;
extern "C"  ArrayList_t2121638921 * XPathSorters_ToSortElementList_m2931616076 (XPathSorters_t4176623784 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_ToSortElementList_m2931616076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	int32_t V_1 = 0;
	XPathSortElement_t2708585374 * V_2 = NULL;
	int32_t V_3 = 0;
	XPathSorter_t3331359429 * V_4 = NULL;
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ArrayList_t2121638921 * L_1 = __this->get__rgSorters_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_0081;
	}

IL_0017:
	{
		XPathSortElement_t2708585374 * L_3 = (XPathSortElement_t2708585374 *)il2cpp_codegen_object_new(XPathSortElement_t2708585374_il2cpp_TypeInfo_var);
		XPathSortElement__ctor_m2356815033(L_3, /*hidden argument*/NULL);
		V_2 = L_3;
		XPathSortElement_t2708585374 * L_4 = V_2;
		BaseIterator_t3696600956 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		XPathNavigator_t1624538935 * L_7 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_6);
		NullCheck(L_4);
		L_4->set_Navigator_0(L_7);
		XPathSortElement_t2708585374 * L_8 = V_2;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		L_8->set_Values_1(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_9)));
		V_3 = 0;
		goto IL_0068;
	}

IL_0041:
	{
		ArrayList_t2121638921 * L_10 = __this->get__rgSorters_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_10, L_11);
		V_4 = ((XPathSorter_t3331359429 *)CastclassClass(L_12, XPathSorter_t3331359429_il2cpp_TypeInfo_var));
		XPathSortElement_t2708585374 * L_13 = V_2;
		NullCheck(L_13);
		ObjectU5BU5D_t11523773* L_14 = L_13->get_Values_1();
		int32_t L_15 = V_3;
		XPathSorter_t3331359429 * L_16 = V_4;
		BaseIterator_t3696600956 * L_17 = ___iter0;
		NullCheck(L_16);
		Il2CppObject * L_18 = XPathSorter_Evaluate_m2674889470(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		ArrayElementTypeCheck (L_14, L_18);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppObject *)L_18);
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_20 = V_3;
		ArrayList_t2121638921 * L_21 = __this->get__rgSorters_0();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0041;
		}
	}
	{
		ArrayList_t2121638921 * L_23 = V_0;
		XPathSortElement_t2708585374 * L_24 = V_2;
		NullCheck(L_23);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_23, L_24);
	}

IL_0081:
	{
		BaseIterator_t3696600956 * L_25 = ___iter0;
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_25);
		if (L_26)
		{
			goto IL_0017;
		}
	}
	{
		ArrayList_t2121638921 * L_27 = V_0;
		return L_27;
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Collections.ArrayList,System.Xml.IXmlNamespaceResolver)
extern Il2CppClass* XPathNavigatorU5BU5D_t2382340622_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSortElement_t2708585374_il2cpp_TypeInfo_var;
extern Il2CppClass* ListIterator_t2154705993_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_Sort_m386310882_MetadataUsageId;
extern "C"  BaseIterator_t3696600956 * XPathSorters_Sort_m386310882 (XPathSorters_t4176623784 * __this, ArrayList_t2121638921 * ___rgElts0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_Sort_m386310882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigatorU5BU5D_t2382340622* V_0 = NULL;
	int32_t V_1 = 0;
	XPathSortElement_t2708585374 * V_2 = NULL;
	{
		ArrayList_t2121638921 * L_0 = ___rgElts0;
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(46 /* System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer) */, L_0, __this);
		ArrayList_t2121638921 * L_1 = ___rgElts0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_0 = ((XPathNavigatorU5BU5D_t2382340622*)SZArrayNew(XPathNavigatorU5BU5D_t2382340622_il2cpp_TypeInfo_var, (uint32_t)L_2));
		V_1 = 0;
		goto IL_0034;
	}

IL_001a:
	{
		ArrayList_t2121638921 * L_3 = ___rgElts0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_3, L_4);
		V_2 = ((XPathSortElement_t2708585374 *)CastclassClass(L_5, XPathSortElement_t2708585374_il2cpp_TypeInfo_var));
		XPathNavigatorU5BU5D_t2382340622* L_6 = V_0;
		int32_t L_7 = V_1;
		XPathSortElement_t2708585374 * L_8 = V_2;
		NullCheck(L_8);
		XPathNavigator_t1624538935 * L_9 = L_8->get_Navigator_0();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (XPathNavigator_t1624538935 *)L_9);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_11 = V_1;
		ArrayList_t2121638921 * L_12 = ___rgElts0;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigatorU5BU5D_t2382340622* L_14 = V_0;
		Il2CppObject * L_15 = ___nsm1;
		ListIterator_t2154705993 * L_16 = (ListIterator_t2154705993 *)il2cpp_codegen_object_new(ListIterator_t2154705993_il2cpp_TypeInfo_var);
		ListIterator__ctor_m320855722(L_16, (Il2CppObject *)(Il2CppObject *)L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.Xml.XmlQualifiedName)
extern "C"  Il2CppObject * XsltContext_ResolveVariable_m3172955259 (XsltContext_t4010601445 * __this, XmlQualifiedName_t176365656 * ___name0, const MethodInfo* method)
{
	{
		XmlQualifiedName_t176365656 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Namespace_m2987642414(L_0, /*hidden argument*/NULL);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(11 /* System.String System.Xml.XmlNamespaceManager::LookupPrefix(System.String) */, __this, L_1);
		XmlQualifiedName_t176365656 * L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = XmlQualifiedName_get_Name_m607016698(L_3, /*hidden argument*/NULL);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, String_t*, String_t* >::Invoke(16 /* System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.String,System.String) */, __this, L_2, L_4);
		return L_5;
	}
}
// System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.Xml.XmlQualifiedName,System.Xml.XPath.XPathResultType[])
extern "C"  Il2CppObject * XsltContext_ResolveFunction_m96561364 (XsltContext_t4010601445 * __this, XmlQualifiedName_t176365656 * ___name0, XPathResultTypeU5BU5D_t1864207532* ___argTypes1, const MethodInfo* method)
{
	{
		XmlQualifiedName_t176365656 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Name_m607016698(L_0, /*hidden argument*/NULL);
		XmlQualifiedName_t176365656 * L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = XmlQualifiedName_get_Namespace_m2987642414(L_2, /*hidden argument*/NULL);
		XPathResultTypeU5BU5D_t1864207532* L_4 = ___argTypes1;
		Il2CppObject * L_5 = VirtFuncInvoker3< Il2CppObject *, String_t*, String_t*, XPathResultTypeU5BU5D_t1864207532* >::Invoke(15 /* System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.String,System.String,System.Xml.XPath.XPathResultType[]) */, __this, L_1, L_3, L_4);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
