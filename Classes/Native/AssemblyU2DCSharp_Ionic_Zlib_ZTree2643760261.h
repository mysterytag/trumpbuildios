﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.SByte[]
struct SByteU5BU5D_t1084170289;
// System.Int16[]
struct Int16U5BU5D_t3675865332;
// Ionic.Zlib.StaticTree
struct StaticTree_t146216121;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZTree
struct  ZTree_t2643760261  : public Il2CppObject
{
public:
	// System.Int16[] Ionic.Zlib.ZTree::dyn_tree
	Int16U5BU5D_t3675865332* ___dyn_tree_10;
	// System.Int32 Ionic.Zlib.ZTree::max_code
	int32_t ___max_code_11;
	// Ionic.Zlib.StaticTree Ionic.Zlib.ZTree::staticTree
	StaticTree_t146216121 * ___staticTree_12;

public:
	inline static int32_t get_offset_of_dyn_tree_10() { return static_cast<int32_t>(offsetof(ZTree_t2643760261, ___dyn_tree_10)); }
	inline Int16U5BU5D_t3675865332* get_dyn_tree_10() const { return ___dyn_tree_10; }
	inline Int16U5BU5D_t3675865332** get_address_of_dyn_tree_10() { return &___dyn_tree_10; }
	inline void set_dyn_tree_10(Int16U5BU5D_t3675865332* value)
	{
		___dyn_tree_10 = value;
		Il2CppCodeGenWriteBarrier(&___dyn_tree_10, value);
	}

	inline static int32_t get_offset_of_max_code_11() { return static_cast<int32_t>(offsetof(ZTree_t2643760261, ___max_code_11)); }
	inline int32_t get_max_code_11() const { return ___max_code_11; }
	inline int32_t* get_address_of_max_code_11() { return &___max_code_11; }
	inline void set_max_code_11(int32_t value)
	{
		___max_code_11 = value;
	}

	inline static int32_t get_offset_of_staticTree_12() { return static_cast<int32_t>(offsetof(ZTree_t2643760261, ___staticTree_12)); }
	inline StaticTree_t146216121 * get_staticTree_12() const { return ___staticTree_12; }
	inline StaticTree_t146216121 ** get_address_of_staticTree_12() { return &___staticTree_12; }
	inline void set_staticTree_12(StaticTree_t146216121 * value)
	{
		___staticTree_12 = value;
		Il2CppCodeGenWriteBarrier(&___staticTree_12, value);
	}
};

struct ZTree_t2643760261_StaticFields
{
public:
	// System.Int32 Ionic.Zlib.ZTree::HEAP_SIZE
	int32_t ___HEAP_SIZE_1;
	// System.Int32[] Ionic.Zlib.ZTree::ExtraLengthBits
	Int32U5BU5D_t1809983122* ___ExtraLengthBits_2;
	// System.Int32[] Ionic.Zlib.ZTree::ExtraDistanceBits
	Int32U5BU5D_t1809983122* ___ExtraDistanceBits_3;
	// System.Int32[] Ionic.Zlib.ZTree::extra_blbits
	Int32U5BU5D_t1809983122* ___extra_blbits_4;
	// System.SByte[] Ionic.Zlib.ZTree::bl_order
	SByteU5BU5D_t1084170289* ___bl_order_5;
	// System.SByte[] Ionic.Zlib.ZTree::_dist_code
	SByteU5BU5D_t1084170289* ____dist_code_6;
	// System.SByte[] Ionic.Zlib.ZTree::LengthCode
	SByteU5BU5D_t1084170289* ___LengthCode_7;
	// System.Int32[] Ionic.Zlib.ZTree::LengthBase
	Int32U5BU5D_t1809983122* ___LengthBase_8;
	// System.Int32[] Ionic.Zlib.ZTree::DistanceBase
	Int32U5BU5D_t1809983122* ___DistanceBase_9;

public:
	inline static int32_t get_offset_of_HEAP_SIZE_1() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ___HEAP_SIZE_1)); }
	inline int32_t get_HEAP_SIZE_1() const { return ___HEAP_SIZE_1; }
	inline int32_t* get_address_of_HEAP_SIZE_1() { return &___HEAP_SIZE_1; }
	inline void set_HEAP_SIZE_1(int32_t value)
	{
		___HEAP_SIZE_1 = value;
	}

	inline static int32_t get_offset_of_ExtraLengthBits_2() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ___ExtraLengthBits_2)); }
	inline Int32U5BU5D_t1809983122* get_ExtraLengthBits_2() const { return ___ExtraLengthBits_2; }
	inline Int32U5BU5D_t1809983122** get_address_of_ExtraLengthBits_2() { return &___ExtraLengthBits_2; }
	inline void set_ExtraLengthBits_2(Int32U5BU5D_t1809983122* value)
	{
		___ExtraLengthBits_2 = value;
		Il2CppCodeGenWriteBarrier(&___ExtraLengthBits_2, value);
	}

	inline static int32_t get_offset_of_ExtraDistanceBits_3() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ___ExtraDistanceBits_3)); }
	inline Int32U5BU5D_t1809983122* get_ExtraDistanceBits_3() const { return ___ExtraDistanceBits_3; }
	inline Int32U5BU5D_t1809983122** get_address_of_ExtraDistanceBits_3() { return &___ExtraDistanceBits_3; }
	inline void set_ExtraDistanceBits_3(Int32U5BU5D_t1809983122* value)
	{
		___ExtraDistanceBits_3 = value;
		Il2CppCodeGenWriteBarrier(&___ExtraDistanceBits_3, value);
	}

	inline static int32_t get_offset_of_extra_blbits_4() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ___extra_blbits_4)); }
	inline Int32U5BU5D_t1809983122* get_extra_blbits_4() const { return ___extra_blbits_4; }
	inline Int32U5BU5D_t1809983122** get_address_of_extra_blbits_4() { return &___extra_blbits_4; }
	inline void set_extra_blbits_4(Int32U5BU5D_t1809983122* value)
	{
		___extra_blbits_4 = value;
		Il2CppCodeGenWriteBarrier(&___extra_blbits_4, value);
	}

	inline static int32_t get_offset_of_bl_order_5() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ___bl_order_5)); }
	inline SByteU5BU5D_t1084170289* get_bl_order_5() const { return ___bl_order_5; }
	inline SByteU5BU5D_t1084170289** get_address_of_bl_order_5() { return &___bl_order_5; }
	inline void set_bl_order_5(SByteU5BU5D_t1084170289* value)
	{
		___bl_order_5 = value;
		Il2CppCodeGenWriteBarrier(&___bl_order_5, value);
	}

	inline static int32_t get_offset_of__dist_code_6() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ____dist_code_6)); }
	inline SByteU5BU5D_t1084170289* get__dist_code_6() const { return ____dist_code_6; }
	inline SByteU5BU5D_t1084170289** get_address_of__dist_code_6() { return &____dist_code_6; }
	inline void set__dist_code_6(SByteU5BU5D_t1084170289* value)
	{
		____dist_code_6 = value;
		Il2CppCodeGenWriteBarrier(&____dist_code_6, value);
	}

	inline static int32_t get_offset_of_LengthCode_7() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ___LengthCode_7)); }
	inline SByteU5BU5D_t1084170289* get_LengthCode_7() const { return ___LengthCode_7; }
	inline SByteU5BU5D_t1084170289** get_address_of_LengthCode_7() { return &___LengthCode_7; }
	inline void set_LengthCode_7(SByteU5BU5D_t1084170289* value)
	{
		___LengthCode_7 = value;
		Il2CppCodeGenWriteBarrier(&___LengthCode_7, value);
	}

	inline static int32_t get_offset_of_LengthBase_8() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ___LengthBase_8)); }
	inline Int32U5BU5D_t1809983122* get_LengthBase_8() const { return ___LengthBase_8; }
	inline Int32U5BU5D_t1809983122** get_address_of_LengthBase_8() { return &___LengthBase_8; }
	inline void set_LengthBase_8(Int32U5BU5D_t1809983122* value)
	{
		___LengthBase_8 = value;
		Il2CppCodeGenWriteBarrier(&___LengthBase_8, value);
	}

	inline static int32_t get_offset_of_DistanceBase_9() { return static_cast<int32_t>(offsetof(ZTree_t2643760261_StaticFields, ___DistanceBase_9)); }
	inline Int32U5BU5D_t1809983122* get_DistanceBase_9() const { return ___DistanceBase_9; }
	inline Int32U5BU5D_t1809983122** get_address_of_DistanceBase_9() { return &___DistanceBase_9; }
	inline void set_DistanceBase_9(Int32U5BU5D_t1809983122* value)
	{
		___DistanceBase_9 = value;
		Il2CppCodeGenWriteBarrier(&___DistanceBase_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
