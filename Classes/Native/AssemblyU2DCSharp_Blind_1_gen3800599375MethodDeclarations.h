﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Blind`1<System.Object>
struct Blind_1_t3800599375;

#include "codegen/il2cpp-codegen.h"

// System.Void Blind`1<System.Object>::.ctor()
extern "C"  void Blind_1__ctor_m4245399685_gshared (Blind_1_t3800599375 * __this, const MethodInfo* method);
#define Blind_1__ctor_m4245399685(__this, method) ((  void (*) (Blind_1_t3800599375 *, const MethodInfo*))Blind_1__ctor_m4245399685_gshared)(__this, method)
