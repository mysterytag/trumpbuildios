﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.YieldInstruction
struct YieldInstruction_t3557331758;
struct YieldInstruction_t3557331758_marshaled_pinvoke;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<TimerFrameCore>c__Iterator16
struct  U3CTimerFrameCoreU3Ec__Iterator16_t2264980091  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator16::dueTimeFrameCount
	int32_t ___dueTimeFrameCount_0;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator16::periodFrameCount
	int32_t ___periodFrameCount_1;
	// System.Int64 UniRx.Observable/<TimerFrameCore>c__Iterator16::<sendCount>__0
	int64_t ___U3CsendCountU3E__0_2;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator16::<currentFrame>__1
	int32_t ___U3CcurrentFrameU3E__1_3;
	// UniRx.FrameCountType UniRx.Observable/<TimerFrameCore>c__Iterator16::frameCountType
	int32_t ___frameCountType_4;
	// UnityEngine.YieldInstruction UniRx.Observable/<TimerFrameCore>c__Iterator16::<yieldInstruction>__2
	YieldInstruction_t3557331758 * ___U3CyieldInstructionU3E__2_5;
	// UniRx.CancellationToken UniRx.Observable/<TimerFrameCore>c__Iterator16::cancel
	CancellationToken_t1439151560  ___cancel_6;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<TimerFrameCore>c__Iterator16::observer
	Il2CppObject* ___observer_7;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator16::$PC
	int32_t ___U24PC_8;
	// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator16::$current
	Il2CppObject * ___U24current_9;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator16::<$>dueTimeFrameCount
	int32_t ___U3CU24U3EdueTimeFrameCount_10;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator16::<$>periodFrameCount
	int32_t ___U3CU24U3EperiodFrameCount_11;
	// UniRx.FrameCountType UniRx.Observable/<TimerFrameCore>c__Iterator16::<$>frameCountType
	int32_t ___U3CU24U3EframeCountType_12;
	// UniRx.CancellationToken UniRx.Observable/<TimerFrameCore>c__Iterator16::<$>cancel
	CancellationToken_t1439151560  ___U3CU24U3Ecancel_13;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<TimerFrameCore>c__Iterator16::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_14;

public:
	inline static int32_t get_offset_of_dueTimeFrameCount_0() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___dueTimeFrameCount_0)); }
	inline int32_t get_dueTimeFrameCount_0() const { return ___dueTimeFrameCount_0; }
	inline int32_t* get_address_of_dueTimeFrameCount_0() { return &___dueTimeFrameCount_0; }
	inline void set_dueTimeFrameCount_0(int32_t value)
	{
		___dueTimeFrameCount_0 = value;
	}

	inline static int32_t get_offset_of_periodFrameCount_1() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___periodFrameCount_1)); }
	inline int32_t get_periodFrameCount_1() const { return ___periodFrameCount_1; }
	inline int32_t* get_address_of_periodFrameCount_1() { return &___periodFrameCount_1; }
	inline void set_periodFrameCount_1(int32_t value)
	{
		___periodFrameCount_1 = value;
	}

	inline static int32_t get_offset_of_U3CsendCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U3CsendCountU3E__0_2)); }
	inline int64_t get_U3CsendCountU3E__0_2() const { return ___U3CsendCountU3E__0_2; }
	inline int64_t* get_address_of_U3CsendCountU3E__0_2() { return &___U3CsendCountU3E__0_2; }
	inline void set_U3CsendCountU3E__0_2(int64_t value)
	{
		___U3CsendCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3E__1_3() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U3CcurrentFrameU3E__1_3)); }
	inline int32_t get_U3CcurrentFrameU3E__1_3() const { return ___U3CcurrentFrameU3E__1_3; }
	inline int32_t* get_address_of_U3CcurrentFrameU3E__1_3() { return &___U3CcurrentFrameU3E__1_3; }
	inline void set_U3CcurrentFrameU3E__1_3(int32_t value)
	{
		___U3CcurrentFrameU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_frameCountType_4() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___frameCountType_4)); }
	inline int32_t get_frameCountType_4() const { return ___frameCountType_4; }
	inline int32_t* get_address_of_frameCountType_4() { return &___frameCountType_4; }
	inline void set_frameCountType_4(int32_t value)
	{
		___frameCountType_4 = value;
	}

	inline static int32_t get_offset_of_U3CyieldInstructionU3E__2_5() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U3CyieldInstructionU3E__2_5)); }
	inline YieldInstruction_t3557331758 * get_U3CyieldInstructionU3E__2_5() const { return ___U3CyieldInstructionU3E__2_5; }
	inline YieldInstruction_t3557331758 ** get_address_of_U3CyieldInstructionU3E__2_5() { return &___U3CyieldInstructionU3E__2_5; }
	inline void set_U3CyieldInstructionU3E__2_5(YieldInstruction_t3557331758 * value)
	{
		___U3CyieldInstructionU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CyieldInstructionU3E__2_5, value);
	}

	inline static int32_t get_offset_of_cancel_6() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___cancel_6)); }
	inline CancellationToken_t1439151560  get_cancel_6() const { return ___cancel_6; }
	inline CancellationToken_t1439151560 * get_address_of_cancel_6() { return &___cancel_6; }
	inline void set_cancel_6(CancellationToken_t1439151560  value)
	{
		___cancel_6 = value;
	}

	inline static int32_t get_offset_of_observer_7() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___observer_7)); }
	inline Il2CppObject* get_observer_7() const { return ___observer_7; }
	inline Il2CppObject** get_address_of_observer_7() { return &___observer_7; }
	inline void set_observer_7(Il2CppObject* value)
	{
		___observer_7 = value;
		Il2CppCodeGenWriteBarrier(&___observer_7, value);
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EdueTimeFrameCount_10() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U3CU24U3EdueTimeFrameCount_10)); }
	inline int32_t get_U3CU24U3EdueTimeFrameCount_10() const { return ___U3CU24U3EdueTimeFrameCount_10; }
	inline int32_t* get_address_of_U3CU24U3EdueTimeFrameCount_10() { return &___U3CU24U3EdueTimeFrameCount_10; }
	inline void set_U3CU24U3EdueTimeFrameCount_10(int32_t value)
	{
		___U3CU24U3EdueTimeFrameCount_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EperiodFrameCount_11() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U3CU24U3EperiodFrameCount_11)); }
	inline int32_t get_U3CU24U3EperiodFrameCount_11() const { return ___U3CU24U3EperiodFrameCount_11; }
	inline int32_t* get_address_of_U3CU24U3EperiodFrameCount_11() { return &___U3CU24U3EperiodFrameCount_11; }
	inline void set_U3CU24U3EperiodFrameCount_11(int32_t value)
	{
		___U3CU24U3EperiodFrameCount_11 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EframeCountType_12() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U3CU24U3EframeCountType_12)); }
	inline int32_t get_U3CU24U3EframeCountType_12() const { return ___U3CU24U3EframeCountType_12; }
	inline int32_t* get_address_of_U3CU24U3EframeCountType_12() { return &___U3CU24U3EframeCountType_12; }
	inline void set_U3CU24U3EframeCountType_12(int32_t value)
	{
		___U3CU24U3EframeCountType_12 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ecancel_13() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U3CU24U3Ecancel_13)); }
	inline CancellationToken_t1439151560  get_U3CU24U3Ecancel_13() const { return ___U3CU24U3Ecancel_13; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3Ecancel_13() { return &___U3CU24U3Ecancel_13; }
	inline void set_U3CU24U3Ecancel_13(CancellationToken_t1439151560  value)
	{
		___U3CU24U3Ecancel_13 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_14() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator16_t2264980091, ___U3CU24U3Eobserver_14)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_14() const { return ___U3CU24U3Eobserver_14; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_14() { return &___U3CU24U3Eobserver_14; }
	inline void set_U3CU24U3Eobserver_14(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
