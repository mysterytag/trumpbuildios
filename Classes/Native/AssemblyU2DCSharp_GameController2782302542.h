﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t4006040370;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// MoneyCounter
struct MoneyCounter_t199543100;
// MoneyPack
struct MoneyPack_t290305753;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.UI.Image
struct Image_t3354615620;
// System.String
struct String_t;
// Cell`1<System.Int64>
struct Cell_1_t3029512514;
// WindowManager
struct WindowManager_t3316821373;
// BarygaOpenIABManager
struct BarygaOpenIABManager_t3621535309;
// UnityEngine.Material
struct Material_t1886596500;
// UniRx.ReactiveProperty`1<System.Double>
struct ReactiveProperty_1_t1142849652;
// UnityEngine.Camera
struct Camera_t3533968274;
// TableButtons
struct TableButtons_t1868573683;
// UIHidder
struct UIHidder_t339629410;
// UnityEngine.ParticleSystem
struct ParticleSystem_t56787138;
// CityNode
struct CityNode_t2937942573;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// GlobalStat
struct GlobalStat_t1134678199;
// TutorchikPanel
struct TutorchikPanel_t3935308711;
// AnalyticsController
struct AnalyticsController_t2265609122;
// GuiController
struct GuiController_t1495593751;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t2595906925;
// Message
struct Message_t2619578343;
// OligarchParameters
struct OligarchParameters_t821144443;
// AudioController
struct AudioController_t2316845042;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;

#include "AssemblyU2DCSharp_SceneController1039782184.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t2782302542  : public SceneController_t1039782184
{
public:
	// System.Boolean GameController::initComplited
	bool ___initComplited_2;
	// System.Boolean GameController::CheckInternetStat
	bool ___CheckInternetStat_3;
	// UnityEngine.Sprite GameController::billDefault
	Sprite_t4006040370 * ___billDefault_4;
	// UnityEngine.Sprite GameController::billGripped
	Sprite_t4006040370 * ___billGripped_5;
	// UnityEngine.GameObject GameController::billSample
	GameObject_t4012695102 * ___billSample_6;
	// UnityEngine.GameObject GameController::goldBillSample
	GameObject_t4012695102 * ___goldBillSample_7;
	// UnityEngine.SpriteRenderer GameController::holder
	SpriteRenderer_t2223784725 * ___holder_8;
	// MoneyCounter GameController::moneyCounter
	MoneyCounter_t199543100 * ___moneyCounter_9;
	// MoneyPack GameController::packSprite
	MoneyPack_t290305753 * ___packSprite_10;
	// UnityEngine.Transform GameController::content
	Transform_t284553113 * ___content_11;
	// UnityEngine.Transform GameController::moneyParent
	Transform_t284553113 * ___moneyParent_12;
	// UnityEngine.UI.Image GameController::topBack
	Image_t3354615620 * ___topBack_13;
	// System.Boolean GameController::tutorial
	bool ___tutorial_14;
	// System.Boolean GameController::analytics
	bool ___analytics_15;
	// UnityEngine.Transform GameController::mmm_countdown_5s
	Transform_t284553113 * ___mmm_countdown_5s_16;
	// UnityEngine.Transform GameController::mmm_countdown_60s
	Transform_t284553113 * ___mmm_countdown_60s_17;
	// UnityEngine.GameObject GameController::popTextSample
	GameObject_t4012695102 * ___popTextSample_18;
	// WindowManager GameController::WindowManager
	WindowManager_t3316821373 * ___WindowManager_22;
	// BarygaOpenIABManager GameController::market
	BarygaOpenIABManager_t3621535309 * ___market_23;
	// UnityEngine.Material GameController::pinchMaterial
	Material_t1886596500 * ___pinchMaterial_24;
	// UnityEngine.Material GameController::defaultMaterial
	Material_t1886596500 * ___defaultMaterial_25;
	// UnityEngine.SpriteRenderer GameController::city
	SpriteRenderer_t2223784725 * ___city_26;
	// UnityEngine.GameObject GameController::billInstance
	GameObject_t4012695102 * ___billInstance_27;
	// UnityEngine.GameObject GameController::dropSample0
	GameObject_t4012695102 * ___dropSample0_28;
	// UnityEngine.GameObject GameController::dropSample1
	GameObject_t4012695102 * ___dropSample1_29;
	// UnityEngine.GameObject GameController::mmmDrop0
	GameObject_t4012695102 * ___mmmDrop0_30;
	// UnityEngine.GameObject GameController::mmmDrop1
	GameObject_t4012695102 * ___mmmDrop1_31;
	// UnityEngine.GameObject GameController::goldDrop
	GameObject_t4012695102 * ___goldDrop_32;
	// UnityEngine.GameObject GameController::diamondDrop
	GameObject_t4012695102 * ___diamondDrop_33;
	// UnityEngine.GameObject GameController::smult_x2
	GameObject_t4012695102 * ___smult_x2_34;
	// UnityEngine.GameObject GameController::smult_x3
	GameObject_t4012695102 * ___smult_x3_35;
	// UnityEngine.GameObject GameController::smult_x5
	GameObject_t4012695102 * ___smult_x5_36;
	// UnityEngine.GameObject GameController::smult_x20
	GameObject_t4012695102 * ___smult_x20_37;
	// UnityEngine.GameObject GameController::mult_X2
	GameObject_t4012695102 * ___mult_X2_38;
	// UnityEngine.GameObject GameController::mult_X3
	GameObject_t4012695102 * ___mult_X3_39;
	// UnityEngine.GameObject GameController::mult_X5
	GameObject_t4012695102 * ___mult_X5_40;
	// UnityEngine.GameObject GameController::mult_MMM
	GameObject_t4012695102 * ___mult_MMM_41;
	// System.Int32 GameController::maxMoneyDrops
	int32_t ___maxMoneyDrops_42;
	// System.Int32 GameController::dropsCount
	int32_t ___dropsCount_43;
	// System.Single GameController::lastDropSpawned
	float ___lastDropSpawned_44;
	// System.Single GameController::lastSwipeTime
	float ___lastSwipeTime_45;
	// System.Int32 GameController::currentCombo
	int32_t ___currentCombo_46;
	// System.Int32 GameController::multipliedClickCounter
	int32_t ___multipliedClickCounter_47;
	// System.Int32 GameController::multiplier
	int32_t ___multiplier_48;
	// System.Int32 GameController::intervalSwipes
	int32_t ___intervalSwipes_49;
	// System.Single GameController::swipeRate
	float ___swipeRate_50;
	// System.Single GameController::achieveUpdateTime
	float ___achieveUpdateTime_51;
	// System.Double GameController::timedUpdateTime
	double ___timedUpdateTime_52;
	// System.Double GameController::intervalIncome
	double ___intervalIncome_53;
	// UniRx.ReactiveProperty`1<System.Double> GameController::calculatedIncome
	ReactiveProperty_1_t1142849652 * ___calculatedIncome_54;
	// System.Single GameController::passiveIncomeTime
	float ___passiveIncomeTime_55;
	// System.Single GameController::lastUpdateTime
	float ___lastUpdateTime_56;
	// System.Boolean GameController::grip
	bool ___grip_57;
	// System.Boolean GameController::gold_bill
	bool ___gold_bill_58;
	// System.Boolean GameController::goldbill_cooldown
	bool ___goldbill_cooldown_59;
	// UnityEngine.Vector2 GameController::gripWorldPosition
	Vector2_t3525329788  ___gripWorldPosition_60;
	// UnityEngine.Camera GameController::mainCamera
	Camera_t3533968274 * ___mainCamera_61;
	// TableButtons GameController::tableB
	TableButtons_t1868573683 * ___tableB_62;
	// UIHidder GameController::BoottomUIHider
	UIHidder_t339629410 * ___BoottomUIHider_63;
	// UIHidder GameController::TopUIHider
	UIHidder_t339629410 * ___TopUIHider_64;
	// UnityEngine.Transform GameController::Bucket
	Transform_t284553113 * ___Bucket_65;
	// System.Single GameController::tapTimeout
	float ___tapTimeout_66;
	// System.Single GameController::last_tap_time
	float ___last_tap_time_67;
	// UnityEngine.ParticleSystem GameController::goldCash
	ParticleSystem_t56787138 * ___goldCash_68;
	// CityNode GameController::cityNode
	CityNode_t2937942573 * ___cityNode_69;
	// TutorialPlayer GameController::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_70;
	// GlobalStat GameController::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_71;
	// TutorchikPanel GameController::TutorchikPanel
	TutorchikPanel_t3935308711 * ___TutorchikPanel_72;
	// AnalyticsController GameController::AnalyticsController
	AnalyticsController_t2265609122 * ___AnalyticsController_73;
	// GuiController GameController::GuiController
	GuiController_t1495593751 * ___GuiController_74;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> GameController::trampMoneyOffsets
	Dictionary_2_t2595906925 * ___trampMoneyOffsets_75;
	// Message GameController::Message
	Message_t2619578343 * ___Message_76;
	// OligarchParameters GameController::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_77;
	// System.Single GameController::delayShowWindow
	float ___delayShowWindow_78;
	// System.Single GameController::checkDelay
	float ___checkDelay_79;
	// AudioController GameController::AudioController
	AudioController_t2316845042 * ___AudioController_80;

public:
	inline static int32_t get_offset_of_initComplited_2() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___initComplited_2)); }
	inline bool get_initComplited_2() const { return ___initComplited_2; }
	inline bool* get_address_of_initComplited_2() { return &___initComplited_2; }
	inline void set_initComplited_2(bool value)
	{
		___initComplited_2 = value;
	}

	inline static int32_t get_offset_of_CheckInternetStat_3() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___CheckInternetStat_3)); }
	inline bool get_CheckInternetStat_3() const { return ___CheckInternetStat_3; }
	inline bool* get_address_of_CheckInternetStat_3() { return &___CheckInternetStat_3; }
	inline void set_CheckInternetStat_3(bool value)
	{
		___CheckInternetStat_3 = value;
	}

	inline static int32_t get_offset_of_billDefault_4() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___billDefault_4)); }
	inline Sprite_t4006040370 * get_billDefault_4() const { return ___billDefault_4; }
	inline Sprite_t4006040370 ** get_address_of_billDefault_4() { return &___billDefault_4; }
	inline void set_billDefault_4(Sprite_t4006040370 * value)
	{
		___billDefault_4 = value;
		Il2CppCodeGenWriteBarrier(&___billDefault_4, value);
	}

	inline static int32_t get_offset_of_billGripped_5() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___billGripped_5)); }
	inline Sprite_t4006040370 * get_billGripped_5() const { return ___billGripped_5; }
	inline Sprite_t4006040370 ** get_address_of_billGripped_5() { return &___billGripped_5; }
	inline void set_billGripped_5(Sprite_t4006040370 * value)
	{
		___billGripped_5 = value;
		Il2CppCodeGenWriteBarrier(&___billGripped_5, value);
	}

	inline static int32_t get_offset_of_billSample_6() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___billSample_6)); }
	inline GameObject_t4012695102 * get_billSample_6() const { return ___billSample_6; }
	inline GameObject_t4012695102 ** get_address_of_billSample_6() { return &___billSample_6; }
	inline void set_billSample_6(GameObject_t4012695102 * value)
	{
		___billSample_6 = value;
		Il2CppCodeGenWriteBarrier(&___billSample_6, value);
	}

	inline static int32_t get_offset_of_goldBillSample_7() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___goldBillSample_7)); }
	inline GameObject_t4012695102 * get_goldBillSample_7() const { return ___goldBillSample_7; }
	inline GameObject_t4012695102 ** get_address_of_goldBillSample_7() { return &___goldBillSample_7; }
	inline void set_goldBillSample_7(GameObject_t4012695102 * value)
	{
		___goldBillSample_7 = value;
		Il2CppCodeGenWriteBarrier(&___goldBillSample_7, value);
	}

	inline static int32_t get_offset_of_holder_8() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___holder_8)); }
	inline SpriteRenderer_t2223784725 * get_holder_8() const { return ___holder_8; }
	inline SpriteRenderer_t2223784725 ** get_address_of_holder_8() { return &___holder_8; }
	inline void set_holder_8(SpriteRenderer_t2223784725 * value)
	{
		___holder_8 = value;
		Il2CppCodeGenWriteBarrier(&___holder_8, value);
	}

	inline static int32_t get_offset_of_moneyCounter_9() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___moneyCounter_9)); }
	inline MoneyCounter_t199543100 * get_moneyCounter_9() const { return ___moneyCounter_9; }
	inline MoneyCounter_t199543100 ** get_address_of_moneyCounter_9() { return &___moneyCounter_9; }
	inline void set_moneyCounter_9(MoneyCounter_t199543100 * value)
	{
		___moneyCounter_9 = value;
		Il2CppCodeGenWriteBarrier(&___moneyCounter_9, value);
	}

	inline static int32_t get_offset_of_packSprite_10() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___packSprite_10)); }
	inline MoneyPack_t290305753 * get_packSprite_10() const { return ___packSprite_10; }
	inline MoneyPack_t290305753 ** get_address_of_packSprite_10() { return &___packSprite_10; }
	inline void set_packSprite_10(MoneyPack_t290305753 * value)
	{
		___packSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___packSprite_10, value);
	}

	inline static int32_t get_offset_of_content_11() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___content_11)); }
	inline Transform_t284553113 * get_content_11() const { return ___content_11; }
	inline Transform_t284553113 ** get_address_of_content_11() { return &___content_11; }
	inline void set_content_11(Transform_t284553113 * value)
	{
		___content_11 = value;
		Il2CppCodeGenWriteBarrier(&___content_11, value);
	}

	inline static int32_t get_offset_of_moneyParent_12() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___moneyParent_12)); }
	inline Transform_t284553113 * get_moneyParent_12() const { return ___moneyParent_12; }
	inline Transform_t284553113 ** get_address_of_moneyParent_12() { return &___moneyParent_12; }
	inline void set_moneyParent_12(Transform_t284553113 * value)
	{
		___moneyParent_12 = value;
		Il2CppCodeGenWriteBarrier(&___moneyParent_12, value);
	}

	inline static int32_t get_offset_of_topBack_13() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___topBack_13)); }
	inline Image_t3354615620 * get_topBack_13() const { return ___topBack_13; }
	inline Image_t3354615620 ** get_address_of_topBack_13() { return &___topBack_13; }
	inline void set_topBack_13(Image_t3354615620 * value)
	{
		___topBack_13 = value;
		Il2CppCodeGenWriteBarrier(&___topBack_13, value);
	}

	inline static int32_t get_offset_of_tutorial_14() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___tutorial_14)); }
	inline bool get_tutorial_14() const { return ___tutorial_14; }
	inline bool* get_address_of_tutorial_14() { return &___tutorial_14; }
	inline void set_tutorial_14(bool value)
	{
		___tutorial_14 = value;
	}

	inline static int32_t get_offset_of_analytics_15() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___analytics_15)); }
	inline bool get_analytics_15() const { return ___analytics_15; }
	inline bool* get_address_of_analytics_15() { return &___analytics_15; }
	inline void set_analytics_15(bool value)
	{
		___analytics_15 = value;
	}

	inline static int32_t get_offset_of_mmm_countdown_5s_16() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mmm_countdown_5s_16)); }
	inline Transform_t284553113 * get_mmm_countdown_5s_16() const { return ___mmm_countdown_5s_16; }
	inline Transform_t284553113 ** get_address_of_mmm_countdown_5s_16() { return &___mmm_countdown_5s_16; }
	inline void set_mmm_countdown_5s_16(Transform_t284553113 * value)
	{
		___mmm_countdown_5s_16 = value;
		Il2CppCodeGenWriteBarrier(&___mmm_countdown_5s_16, value);
	}

	inline static int32_t get_offset_of_mmm_countdown_60s_17() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mmm_countdown_60s_17)); }
	inline Transform_t284553113 * get_mmm_countdown_60s_17() const { return ___mmm_countdown_60s_17; }
	inline Transform_t284553113 ** get_address_of_mmm_countdown_60s_17() { return &___mmm_countdown_60s_17; }
	inline void set_mmm_countdown_60s_17(Transform_t284553113 * value)
	{
		___mmm_countdown_60s_17 = value;
		Il2CppCodeGenWriteBarrier(&___mmm_countdown_60s_17, value);
	}

	inline static int32_t get_offset_of_popTextSample_18() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___popTextSample_18)); }
	inline GameObject_t4012695102 * get_popTextSample_18() const { return ___popTextSample_18; }
	inline GameObject_t4012695102 ** get_address_of_popTextSample_18() { return &___popTextSample_18; }
	inline void set_popTextSample_18(GameObject_t4012695102 * value)
	{
		___popTextSample_18 = value;
		Il2CppCodeGenWriteBarrier(&___popTextSample_18, value);
	}

	inline static int32_t get_offset_of_WindowManager_22() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___WindowManager_22)); }
	inline WindowManager_t3316821373 * get_WindowManager_22() const { return ___WindowManager_22; }
	inline WindowManager_t3316821373 ** get_address_of_WindowManager_22() { return &___WindowManager_22; }
	inline void set_WindowManager_22(WindowManager_t3316821373 * value)
	{
		___WindowManager_22 = value;
		Il2CppCodeGenWriteBarrier(&___WindowManager_22, value);
	}

	inline static int32_t get_offset_of_market_23() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___market_23)); }
	inline BarygaOpenIABManager_t3621535309 * get_market_23() const { return ___market_23; }
	inline BarygaOpenIABManager_t3621535309 ** get_address_of_market_23() { return &___market_23; }
	inline void set_market_23(BarygaOpenIABManager_t3621535309 * value)
	{
		___market_23 = value;
		Il2CppCodeGenWriteBarrier(&___market_23, value);
	}

	inline static int32_t get_offset_of_pinchMaterial_24() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___pinchMaterial_24)); }
	inline Material_t1886596500 * get_pinchMaterial_24() const { return ___pinchMaterial_24; }
	inline Material_t1886596500 ** get_address_of_pinchMaterial_24() { return &___pinchMaterial_24; }
	inline void set_pinchMaterial_24(Material_t1886596500 * value)
	{
		___pinchMaterial_24 = value;
		Il2CppCodeGenWriteBarrier(&___pinchMaterial_24, value);
	}

	inline static int32_t get_offset_of_defaultMaterial_25() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___defaultMaterial_25)); }
	inline Material_t1886596500 * get_defaultMaterial_25() const { return ___defaultMaterial_25; }
	inline Material_t1886596500 ** get_address_of_defaultMaterial_25() { return &___defaultMaterial_25; }
	inline void set_defaultMaterial_25(Material_t1886596500 * value)
	{
		___defaultMaterial_25 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMaterial_25, value);
	}

	inline static int32_t get_offset_of_city_26() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___city_26)); }
	inline SpriteRenderer_t2223784725 * get_city_26() const { return ___city_26; }
	inline SpriteRenderer_t2223784725 ** get_address_of_city_26() { return &___city_26; }
	inline void set_city_26(SpriteRenderer_t2223784725 * value)
	{
		___city_26 = value;
		Il2CppCodeGenWriteBarrier(&___city_26, value);
	}

	inline static int32_t get_offset_of_billInstance_27() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___billInstance_27)); }
	inline GameObject_t4012695102 * get_billInstance_27() const { return ___billInstance_27; }
	inline GameObject_t4012695102 ** get_address_of_billInstance_27() { return &___billInstance_27; }
	inline void set_billInstance_27(GameObject_t4012695102 * value)
	{
		___billInstance_27 = value;
		Il2CppCodeGenWriteBarrier(&___billInstance_27, value);
	}

	inline static int32_t get_offset_of_dropSample0_28() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___dropSample0_28)); }
	inline GameObject_t4012695102 * get_dropSample0_28() const { return ___dropSample0_28; }
	inline GameObject_t4012695102 ** get_address_of_dropSample0_28() { return &___dropSample0_28; }
	inline void set_dropSample0_28(GameObject_t4012695102 * value)
	{
		___dropSample0_28 = value;
		Il2CppCodeGenWriteBarrier(&___dropSample0_28, value);
	}

	inline static int32_t get_offset_of_dropSample1_29() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___dropSample1_29)); }
	inline GameObject_t4012695102 * get_dropSample1_29() const { return ___dropSample1_29; }
	inline GameObject_t4012695102 ** get_address_of_dropSample1_29() { return &___dropSample1_29; }
	inline void set_dropSample1_29(GameObject_t4012695102 * value)
	{
		___dropSample1_29 = value;
		Il2CppCodeGenWriteBarrier(&___dropSample1_29, value);
	}

	inline static int32_t get_offset_of_mmmDrop0_30() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mmmDrop0_30)); }
	inline GameObject_t4012695102 * get_mmmDrop0_30() const { return ___mmmDrop0_30; }
	inline GameObject_t4012695102 ** get_address_of_mmmDrop0_30() { return &___mmmDrop0_30; }
	inline void set_mmmDrop0_30(GameObject_t4012695102 * value)
	{
		___mmmDrop0_30 = value;
		Il2CppCodeGenWriteBarrier(&___mmmDrop0_30, value);
	}

	inline static int32_t get_offset_of_mmmDrop1_31() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mmmDrop1_31)); }
	inline GameObject_t4012695102 * get_mmmDrop1_31() const { return ___mmmDrop1_31; }
	inline GameObject_t4012695102 ** get_address_of_mmmDrop1_31() { return &___mmmDrop1_31; }
	inline void set_mmmDrop1_31(GameObject_t4012695102 * value)
	{
		___mmmDrop1_31 = value;
		Il2CppCodeGenWriteBarrier(&___mmmDrop1_31, value);
	}

	inline static int32_t get_offset_of_goldDrop_32() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___goldDrop_32)); }
	inline GameObject_t4012695102 * get_goldDrop_32() const { return ___goldDrop_32; }
	inline GameObject_t4012695102 ** get_address_of_goldDrop_32() { return &___goldDrop_32; }
	inline void set_goldDrop_32(GameObject_t4012695102 * value)
	{
		___goldDrop_32 = value;
		Il2CppCodeGenWriteBarrier(&___goldDrop_32, value);
	}

	inline static int32_t get_offset_of_diamondDrop_33() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___diamondDrop_33)); }
	inline GameObject_t4012695102 * get_diamondDrop_33() const { return ___diamondDrop_33; }
	inline GameObject_t4012695102 ** get_address_of_diamondDrop_33() { return &___diamondDrop_33; }
	inline void set_diamondDrop_33(GameObject_t4012695102 * value)
	{
		___diamondDrop_33 = value;
		Il2CppCodeGenWriteBarrier(&___diamondDrop_33, value);
	}

	inline static int32_t get_offset_of_smult_x2_34() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___smult_x2_34)); }
	inline GameObject_t4012695102 * get_smult_x2_34() const { return ___smult_x2_34; }
	inline GameObject_t4012695102 ** get_address_of_smult_x2_34() { return &___smult_x2_34; }
	inline void set_smult_x2_34(GameObject_t4012695102 * value)
	{
		___smult_x2_34 = value;
		Il2CppCodeGenWriteBarrier(&___smult_x2_34, value);
	}

	inline static int32_t get_offset_of_smult_x3_35() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___smult_x3_35)); }
	inline GameObject_t4012695102 * get_smult_x3_35() const { return ___smult_x3_35; }
	inline GameObject_t4012695102 ** get_address_of_smult_x3_35() { return &___smult_x3_35; }
	inline void set_smult_x3_35(GameObject_t4012695102 * value)
	{
		___smult_x3_35 = value;
		Il2CppCodeGenWriteBarrier(&___smult_x3_35, value);
	}

	inline static int32_t get_offset_of_smult_x5_36() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___smult_x5_36)); }
	inline GameObject_t4012695102 * get_smult_x5_36() const { return ___smult_x5_36; }
	inline GameObject_t4012695102 ** get_address_of_smult_x5_36() { return &___smult_x5_36; }
	inline void set_smult_x5_36(GameObject_t4012695102 * value)
	{
		___smult_x5_36 = value;
		Il2CppCodeGenWriteBarrier(&___smult_x5_36, value);
	}

	inline static int32_t get_offset_of_smult_x20_37() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___smult_x20_37)); }
	inline GameObject_t4012695102 * get_smult_x20_37() const { return ___smult_x20_37; }
	inline GameObject_t4012695102 ** get_address_of_smult_x20_37() { return &___smult_x20_37; }
	inline void set_smult_x20_37(GameObject_t4012695102 * value)
	{
		___smult_x20_37 = value;
		Il2CppCodeGenWriteBarrier(&___smult_x20_37, value);
	}

	inline static int32_t get_offset_of_mult_X2_38() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mult_X2_38)); }
	inline GameObject_t4012695102 * get_mult_X2_38() const { return ___mult_X2_38; }
	inline GameObject_t4012695102 ** get_address_of_mult_X2_38() { return &___mult_X2_38; }
	inline void set_mult_X2_38(GameObject_t4012695102 * value)
	{
		___mult_X2_38 = value;
		Il2CppCodeGenWriteBarrier(&___mult_X2_38, value);
	}

	inline static int32_t get_offset_of_mult_X3_39() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mult_X3_39)); }
	inline GameObject_t4012695102 * get_mult_X3_39() const { return ___mult_X3_39; }
	inline GameObject_t4012695102 ** get_address_of_mult_X3_39() { return &___mult_X3_39; }
	inline void set_mult_X3_39(GameObject_t4012695102 * value)
	{
		___mult_X3_39 = value;
		Il2CppCodeGenWriteBarrier(&___mult_X3_39, value);
	}

	inline static int32_t get_offset_of_mult_X5_40() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mult_X5_40)); }
	inline GameObject_t4012695102 * get_mult_X5_40() const { return ___mult_X5_40; }
	inline GameObject_t4012695102 ** get_address_of_mult_X5_40() { return &___mult_X5_40; }
	inline void set_mult_X5_40(GameObject_t4012695102 * value)
	{
		___mult_X5_40 = value;
		Il2CppCodeGenWriteBarrier(&___mult_X5_40, value);
	}

	inline static int32_t get_offset_of_mult_MMM_41() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mult_MMM_41)); }
	inline GameObject_t4012695102 * get_mult_MMM_41() const { return ___mult_MMM_41; }
	inline GameObject_t4012695102 ** get_address_of_mult_MMM_41() { return &___mult_MMM_41; }
	inline void set_mult_MMM_41(GameObject_t4012695102 * value)
	{
		___mult_MMM_41 = value;
		Il2CppCodeGenWriteBarrier(&___mult_MMM_41, value);
	}

	inline static int32_t get_offset_of_maxMoneyDrops_42() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___maxMoneyDrops_42)); }
	inline int32_t get_maxMoneyDrops_42() const { return ___maxMoneyDrops_42; }
	inline int32_t* get_address_of_maxMoneyDrops_42() { return &___maxMoneyDrops_42; }
	inline void set_maxMoneyDrops_42(int32_t value)
	{
		___maxMoneyDrops_42 = value;
	}

	inline static int32_t get_offset_of_dropsCount_43() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___dropsCount_43)); }
	inline int32_t get_dropsCount_43() const { return ___dropsCount_43; }
	inline int32_t* get_address_of_dropsCount_43() { return &___dropsCount_43; }
	inline void set_dropsCount_43(int32_t value)
	{
		___dropsCount_43 = value;
	}

	inline static int32_t get_offset_of_lastDropSpawned_44() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___lastDropSpawned_44)); }
	inline float get_lastDropSpawned_44() const { return ___lastDropSpawned_44; }
	inline float* get_address_of_lastDropSpawned_44() { return &___lastDropSpawned_44; }
	inline void set_lastDropSpawned_44(float value)
	{
		___lastDropSpawned_44 = value;
	}

	inline static int32_t get_offset_of_lastSwipeTime_45() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___lastSwipeTime_45)); }
	inline float get_lastSwipeTime_45() const { return ___lastSwipeTime_45; }
	inline float* get_address_of_lastSwipeTime_45() { return &___lastSwipeTime_45; }
	inline void set_lastSwipeTime_45(float value)
	{
		___lastSwipeTime_45 = value;
	}

	inline static int32_t get_offset_of_currentCombo_46() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___currentCombo_46)); }
	inline int32_t get_currentCombo_46() const { return ___currentCombo_46; }
	inline int32_t* get_address_of_currentCombo_46() { return &___currentCombo_46; }
	inline void set_currentCombo_46(int32_t value)
	{
		___currentCombo_46 = value;
	}

	inline static int32_t get_offset_of_multipliedClickCounter_47() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___multipliedClickCounter_47)); }
	inline int32_t get_multipliedClickCounter_47() const { return ___multipliedClickCounter_47; }
	inline int32_t* get_address_of_multipliedClickCounter_47() { return &___multipliedClickCounter_47; }
	inline void set_multipliedClickCounter_47(int32_t value)
	{
		___multipliedClickCounter_47 = value;
	}

	inline static int32_t get_offset_of_multiplier_48() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___multiplier_48)); }
	inline int32_t get_multiplier_48() const { return ___multiplier_48; }
	inline int32_t* get_address_of_multiplier_48() { return &___multiplier_48; }
	inline void set_multiplier_48(int32_t value)
	{
		___multiplier_48 = value;
	}

	inline static int32_t get_offset_of_intervalSwipes_49() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___intervalSwipes_49)); }
	inline int32_t get_intervalSwipes_49() const { return ___intervalSwipes_49; }
	inline int32_t* get_address_of_intervalSwipes_49() { return &___intervalSwipes_49; }
	inline void set_intervalSwipes_49(int32_t value)
	{
		___intervalSwipes_49 = value;
	}

	inline static int32_t get_offset_of_swipeRate_50() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___swipeRate_50)); }
	inline float get_swipeRate_50() const { return ___swipeRate_50; }
	inline float* get_address_of_swipeRate_50() { return &___swipeRate_50; }
	inline void set_swipeRate_50(float value)
	{
		___swipeRate_50 = value;
	}

	inline static int32_t get_offset_of_achieveUpdateTime_51() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___achieveUpdateTime_51)); }
	inline float get_achieveUpdateTime_51() const { return ___achieveUpdateTime_51; }
	inline float* get_address_of_achieveUpdateTime_51() { return &___achieveUpdateTime_51; }
	inline void set_achieveUpdateTime_51(float value)
	{
		___achieveUpdateTime_51 = value;
	}

	inline static int32_t get_offset_of_timedUpdateTime_52() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___timedUpdateTime_52)); }
	inline double get_timedUpdateTime_52() const { return ___timedUpdateTime_52; }
	inline double* get_address_of_timedUpdateTime_52() { return &___timedUpdateTime_52; }
	inline void set_timedUpdateTime_52(double value)
	{
		___timedUpdateTime_52 = value;
	}

	inline static int32_t get_offset_of_intervalIncome_53() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___intervalIncome_53)); }
	inline double get_intervalIncome_53() const { return ___intervalIncome_53; }
	inline double* get_address_of_intervalIncome_53() { return &___intervalIncome_53; }
	inline void set_intervalIncome_53(double value)
	{
		___intervalIncome_53 = value;
	}

	inline static int32_t get_offset_of_calculatedIncome_54() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___calculatedIncome_54)); }
	inline ReactiveProperty_1_t1142849652 * get_calculatedIncome_54() const { return ___calculatedIncome_54; }
	inline ReactiveProperty_1_t1142849652 ** get_address_of_calculatedIncome_54() { return &___calculatedIncome_54; }
	inline void set_calculatedIncome_54(ReactiveProperty_1_t1142849652 * value)
	{
		___calculatedIncome_54 = value;
		Il2CppCodeGenWriteBarrier(&___calculatedIncome_54, value);
	}

	inline static int32_t get_offset_of_passiveIncomeTime_55() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___passiveIncomeTime_55)); }
	inline float get_passiveIncomeTime_55() const { return ___passiveIncomeTime_55; }
	inline float* get_address_of_passiveIncomeTime_55() { return &___passiveIncomeTime_55; }
	inline void set_passiveIncomeTime_55(float value)
	{
		___passiveIncomeTime_55 = value;
	}

	inline static int32_t get_offset_of_lastUpdateTime_56() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___lastUpdateTime_56)); }
	inline float get_lastUpdateTime_56() const { return ___lastUpdateTime_56; }
	inline float* get_address_of_lastUpdateTime_56() { return &___lastUpdateTime_56; }
	inline void set_lastUpdateTime_56(float value)
	{
		___lastUpdateTime_56 = value;
	}

	inline static int32_t get_offset_of_grip_57() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___grip_57)); }
	inline bool get_grip_57() const { return ___grip_57; }
	inline bool* get_address_of_grip_57() { return &___grip_57; }
	inline void set_grip_57(bool value)
	{
		___grip_57 = value;
	}

	inline static int32_t get_offset_of_gold_bill_58() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___gold_bill_58)); }
	inline bool get_gold_bill_58() const { return ___gold_bill_58; }
	inline bool* get_address_of_gold_bill_58() { return &___gold_bill_58; }
	inline void set_gold_bill_58(bool value)
	{
		___gold_bill_58 = value;
	}

	inline static int32_t get_offset_of_goldbill_cooldown_59() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___goldbill_cooldown_59)); }
	inline bool get_goldbill_cooldown_59() const { return ___goldbill_cooldown_59; }
	inline bool* get_address_of_goldbill_cooldown_59() { return &___goldbill_cooldown_59; }
	inline void set_goldbill_cooldown_59(bool value)
	{
		___goldbill_cooldown_59 = value;
	}

	inline static int32_t get_offset_of_gripWorldPosition_60() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___gripWorldPosition_60)); }
	inline Vector2_t3525329788  get_gripWorldPosition_60() const { return ___gripWorldPosition_60; }
	inline Vector2_t3525329788 * get_address_of_gripWorldPosition_60() { return &___gripWorldPosition_60; }
	inline void set_gripWorldPosition_60(Vector2_t3525329788  value)
	{
		___gripWorldPosition_60 = value;
	}

	inline static int32_t get_offset_of_mainCamera_61() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___mainCamera_61)); }
	inline Camera_t3533968274 * get_mainCamera_61() const { return ___mainCamera_61; }
	inline Camera_t3533968274 ** get_address_of_mainCamera_61() { return &___mainCamera_61; }
	inline void set_mainCamera_61(Camera_t3533968274 * value)
	{
		___mainCamera_61 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_61, value);
	}

	inline static int32_t get_offset_of_tableB_62() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___tableB_62)); }
	inline TableButtons_t1868573683 * get_tableB_62() const { return ___tableB_62; }
	inline TableButtons_t1868573683 ** get_address_of_tableB_62() { return &___tableB_62; }
	inline void set_tableB_62(TableButtons_t1868573683 * value)
	{
		___tableB_62 = value;
		Il2CppCodeGenWriteBarrier(&___tableB_62, value);
	}

	inline static int32_t get_offset_of_BoottomUIHider_63() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___BoottomUIHider_63)); }
	inline UIHidder_t339629410 * get_BoottomUIHider_63() const { return ___BoottomUIHider_63; }
	inline UIHidder_t339629410 ** get_address_of_BoottomUIHider_63() { return &___BoottomUIHider_63; }
	inline void set_BoottomUIHider_63(UIHidder_t339629410 * value)
	{
		___BoottomUIHider_63 = value;
		Il2CppCodeGenWriteBarrier(&___BoottomUIHider_63, value);
	}

	inline static int32_t get_offset_of_TopUIHider_64() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___TopUIHider_64)); }
	inline UIHidder_t339629410 * get_TopUIHider_64() const { return ___TopUIHider_64; }
	inline UIHidder_t339629410 ** get_address_of_TopUIHider_64() { return &___TopUIHider_64; }
	inline void set_TopUIHider_64(UIHidder_t339629410 * value)
	{
		___TopUIHider_64 = value;
		Il2CppCodeGenWriteBarrier(&___TopUIHider_64, value);
	}

	inline static int32_t get_offset_of_Bucket_65() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___Bucket_65)); }
	inline Transform_t284553113 * get_Bucket_65() const { return ___Bucket_65; }
	inline Transform_t284553113 ** get_address_of_Bucket_65() { return &___Bucket_65; }
	inline void set_Bucket_65(Transform_t284553113 * value)
	{
		___Bucket_65 = value;
		Il2CppCodeGenWriteBarrier(&___Bucket_65, value);
	}

	inline static int32_t get_offset_of_tapTimeout_66() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___tapTimeout_66)); }
	inline float get_tapTimeout_66() const { return ___tapTimeout_66; }
	inline float* get_address_of_tapTimeout_66() { return &___tapTimeout_66; }
	inline void set_tapTimeout_66(float value)
	{
		___tapTimeout_66 = value;
	}

	inline static int32_t get_offset_of_last_tap_time_67() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___last_tap_time_67)); }
	inline float get_last_tap_time_67() const { return ___last_tap_time_67; }
	inline float* get_address_of_last_tap_time_67() { return &___last_tap_time_67; }
	inline void set_last_tap_time_67(float value)
	{
		___last_tap_time_67 = value;
	}

	inline static int32_t get_offset_of_goldCash_68() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___goldCash_68)); }
	inline ParticleSystem_t56787138 * get_goldCash_68() const { return ___goldCash_68; }
	inline ParticleSystem_t56787138 ** get_address_of_goldCash_68() { return &___goldCash_68; }
	inline void set_goldCash_68(ParticleSystem_t56787138 * value)
	{
		___goldCash_68 = value;
		Il2CppCodeGenWriteBarrier(&___goldCash_68, value);
	}

	inline static int32_t get_offset_of_cityNode_69() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___cityNode_69)); }
	inline CityNode_t2937942573 * get_cityNode_69() const { return ___cityNode_69; }
	inline CityNode_t2937942573 ** get_address_of_cityNode_69() { return &___cityNode_69; }
	inline void set_cityNode_69(CityNode_t2937942573 * value)
	{
		___cityNode_69 = value;
		Il2CppCodeGenWriteBarrier(&___cityNode_69, value);
	}

	inline static int32_t get_offset_of_TutorialPlayer_70() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___TutorialPlayer_70)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_70() const { return ___TutorialPlayer_70; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_70() { return &___TutorialPlayer_70; }
	inline void set_TutorialPlayer_70(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_70 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_70, value);
	}

	inline static int32_t get_offset_of_GlobalStat_71() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___GlobalStat_71)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_71() const { return ___GlobalStat_71; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_71() { return &___GlobalStat_71; }
	inline void set_GlobalStat_71(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_71 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_71, value);
	}

	inline static int32_t get_offset_of_TutorchikPanel_72() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___TutorchikPanel_72)); }
	inline TutorchikPanel_t3935308711 * get_TutorchikPanel_72() const { return ___TutorchikPanel_72; }
	inline TutorchikPanel_t3935308711 ** get_address_of_TutorchikPanel_72() { return &___TutorchikPanel_72; }
	inline void set_TutorchikPanel_72(TutorchikPanel_t3935308711 * value)
	{
		___TutorchikPanel_72 = value;
		Il2CppCodeGenWriteBarrier(&___TutorchikPanel_72, value);
	}

	inline static int32_t get_offset_of_AnalyticsController_73() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___AnalyticsController_73)); }
	inline AnalyticsController_t2265609122 * get_AnalyticsController_73() const { return ___AnalyticsController_73; }
	inline AnalyticsController_t2265609122 ** get_address_of_AnalyticsController_73() { return &___AnalyticsController_73; }
	inline void set_AnalyticsController_73(AnalyticsController_t2265609122 * value)
	{
		___AnalyticsController_73 = value;
		Il2CppCodeGenWriteBarrier(&___AnalyticsController_73, value);
	}

	inline static int32_t get_offset_of_GuiController_74() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___GuiController_74)); }
	inline GuiController_t1495593751 * get_GuiController_74() const { return ___GuiController_74; }
	inline GuiController_t1495593751 ** get_address_of_GuiController_74() { return &___GuiController_74; }
	inline void set_GuiController_74(GuiController_t1495593751 * value)
	{
		___GuiController_74 = value;
		Il2CppCodeGenWriteBarrier(&___GuiController_74, value);
	}

	inline static int32_t get_offset_of_trampMoneyOffsets_75() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___trampMoneyOffsets_75)); }
	inline Dictionary_2_t2595906925 * get_trampMoneyOffsets_75() const { return ___trampMoneyOffsets_75; }
	inline Dictionary_2_t2595906925 ** get_address_of_trampMoneyOffsets_75() { return &___trampMoneyOffsets_75; }
	inline void set_trampMoneyOffsets_75(Dictionary_2_t2595906925 * value)
	{
		___trampMoneyOffsets_75 = value;
		Il2CppCodeGenWriteBarrier(&___trampMoneyOffsets_75, value);
	}

	inline static int32_t get_offset_of_Message_76() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___Message_76)); }
	inline Message_t2619578343 * get_Message_76() const { return ___Message_76; }
	inline Message_t2619578343 ** get_address_of_Message_76() { return &___Message_76; }
	inline void set_Message_76(Message_t2619578343 * value)
	{
		___Message_76 = value;
		Il2CppCodeGenWriteBarrier(&___Message_76, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_77() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___OligarchParameters_77)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_77() const { return ___OligarchParameters_77; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_77() { return &___OligarchParameters_77; }
	inline void set_OligarchParameters_77(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_77 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_77, value);
	}

	inline static int32_t get_offset_of_delayShowWindow_78() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___delayShowWindow_78)); }
	inline float get_delayShowWindow_78() const { return ___delayShowWindow_78; }
	inline float* get_address_of_delayShowWindow_78() { return &___delayShowWindow_78; }
	inline void set_delayShowWindow_78(float value)
	{
		___delayShowWindow_78 = value;
	}

	inline static int32_t get_offset_of_checkDelay_79() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___checkDelay_79)); }
	inline float get_checkDelay_79() const { return ___checkDelay_79; }
	inline float* get_address_of_checkDelay_79() { return &___checkDelay_79; }
	inline void set_checkDelay_79(float value)
	{
		___checkDelay_79 = value;
	}

	inline static int32_t get_offset_of_AudioController_80() { return static_cast<int32_t>(offsetof(GameController_t2782302542, ___AudioController_80)); }
	inline AudioController_t2316845042 * get_AudioController_80() const { return ___AudioController_80; }
	inline AudioController_t2316845042 ** get_address_of_AudioController_80() { return &___AudioController_80; }
	inline void set_AudioController_80(AudioController_t2316845042 * value)
	{
		___AudioController_80 = value;
		Il2CppCodeGenWriteBarrier(&___AudioController_80, value);
	}
};

struct GameController_t2782302542_StaticFields
{
public:
	// System.String GameController::deviceName
	String_t* ___deviceName_19;
	// System.String GameController::countryName
	String_t* ___countryName_20;
	// Cell`1<System.Int64> GameController::secondTaktGenerator
	Cell_1_t3029512514 * ___secondTaktGenerator_21;
	// System.Action`1<System.Int64> GameController::<>f__am$cache4F
	Action_1_t2995867587 * ___U3CU3Ef__amU24cache4F_81;

public:
	inline static int32_t get_offset_of_deviceName_19() { return static_cast<int32_t>(offsetof(GameController_t2782302542_StaticFields, ___deviceName_19)); }
	inline String_t* get_deviceName_19() const { return ___deviceName_19; }
	inline String_t** get_address_of_deviceName_19() { return &___deviceName_19; }
	inline void set_deviceName_19(String_t* value)
	{
		___deviceName_19 = value;
		Il2CppCodeGenWriteBarrier(&___deviceName_19, value);
	}

	inline static int32_t get_offset_of_countryName_20() { return static_cast<int32_t>(offsetof(GameController_t2782302542_StaticFields, ___countryName_20)); }
	inline String_t* get_countryName_20() const { return ___countryName_20; }
	inline String_t** get_address_of_countryName_20() { return &___countryName_20; }
	inline void set_countryName_20(String_t* value)
	{
		___countryName_20 = value;
		Il2CppCodeGenWriteBarrier(&___countryName_20, value);
	}

	inline static int32_t get_offset_of_secondTaktGenerator_21() { return static_cast<int32_t>(offsetof(GameController_t2782302542_StaticFields, ___secondTaktGenerator_21)); }
	inline Cell_1_t3029512514 * get_secondTaktGenerator_21() const { return ___secondTaktGenerator_21; }
	inline Cell_1_t3029512514 ** get_address_of_secondTaktGenerator_21() { return &___secondTaktGenerator_21; }
	inline void set_secondTaktGenerator_21(Cell_1_t3029512514 * value)
	{
		___secondTaktGenerator_21 = value;
		Il2CppCodeGenWriteBarrier(&___secondTaktGenerator_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4F_81() { return static_cast<int32_t>(offsetof(GameController_t2782302542_StaticFields, ___U3CU3Ef__amU24cache4F_81)); }
	inline Action_1_t2995867587 * get_U3CU3Ef__amU24cache4F_81() const { return ___U3CU3Ef__amU24cache4F_81; }
	inline Action_1_t2995867587 ** get_address_of_U3CU3Ef__amU24cache4F_81() { return &___U3CU3Ef__amU24cache4F_81; }
	inline void set_U3CU3Ef__amU24cache4F_81(Action_1_t2995867587 * value)
	{
		___U3CU3Ef__amU24cache4F_81 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4F_81, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
