﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>
struct Collection_1_t3860703709;
// System.Collections.Generic.IList`1<UnityEngine.Quaternion>
struct IList_1_t4058208293;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t3236402666;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Quaternion>
struct IEnumerator_1_t3374822427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void Collection_1__ctor_m1993936860_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1993936860(__this, method) ((  void (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1__ctor_m1993936860_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m21729465_gshared (Collection_1_t3860703709 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m21729465(__this, ___list0, method) ((  void (*) (Collection_1_t3860703709 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m21729465_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4005852123_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4005852123(__this, method) ((  bool (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4005852123_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2930691304_gshared (Collection_1_t3860703709 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2930691304(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3860703709 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2930691304_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1016526583_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1016526583(__this, method) ((  Il2CppObject * (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1016526583_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2831657638_gshared (Collection_1_t3860703709 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2831657638(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3860703709 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2831657638_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m748203098_gshared (Collection_1_t3860703709 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m748203098(__this, ___value0, method) ((  bool (*) (Collection_1_t3860703709 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m748203098_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m21372414_gshared (Collection_1_t3860703709 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m21372414(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3860703709 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m21372414_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m623232625_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m623232625(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3860703709 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m623232625_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2801221399_gshared (Collection_1_t3860703709 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2801221399(__this, ___value0, method) ((  void (*) (Collection_1_t3860703709 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2801221399_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3128169666_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3128169666(__this, method) ((  bool (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3128169666_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m758242996_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m758242996(__this, method) ((  Il2CppObject * (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m758242996_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2219255625_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2219255625(__this, method) ((  bool (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2219255625_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m109955728_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m109955728(__this, method) ((  bool (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m109955728_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3887657787_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3887657787(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3860703709 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3887657787_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2540050952_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2540050952(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3860703709 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2540050952_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::Add(T)
extern "C"  void Collection_1_Add_m3241051427_gshared (Collection_1_t3860703709 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3241051427(__this, ___item0, method) ((  void (*) (Collection_1_t3860703709 *, Quaternion_t1891715979 , const MethodInfo*))Collection_1_Add_m3241051427_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::Clear()
extern "C"  void Collection_1_Clear_m3695037447_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3695037447(__this, method) ((  void (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_Clear_m3695037447_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3252633947_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3252633947(__this, method) ((  void (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_ClearItems_m3252633947_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::Contains(T)
extern "C"  bool Collection_1_Contains_m3279444089_gshared (Collection_1_t3860703709 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3279444089(__this, ___item0, method) ((  bool (*) (Collection_1_t3860703709 *, Quaternion_t1891715979 , const MethodInfo*))Collection_1_Contains_m3279444089_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2179500627_gshared (Collection_1_t3860703709 * __this, QuaternionU5BU5D_t3236402666* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2179500627(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3860703709 *, QuaternionU5BU5D_t3236402666*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2179500627_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4057464400_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4057464400(__this, method) ((  Il2CppObject* (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_GetEnumerator_m4057464400_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1062286175_gshared (Collection_1_t3860703709 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1062286175(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3860703709 *, Quaternion_t1891715979 , const MethodInfo*))Collection_1_IndexOf_m1062286175_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m716904842_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, Quaternion_t1891715979  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m716904842(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3860703709 *, int32_t, Quaternion_t1891715979 , const MethodInfo*))Collection_1_Insert_m716904842_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m517370557_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, Quaternion_t1891715979  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m517370557(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3860703709 *, int32_t, Quaternion_t1891715979 , const MethodInfo*))Collection_1_InsertItem_m517370557_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::Remove(T)
extern "C"  bool Collection_1_Remove_m1638589108_gshared (Collection_1_t3860703709 * __this, Quaternion_t1891715979  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1638589108(__this, ___item0, method) ((  bool (*) (Collection_1_t3860703709 *, Quaternion_t1891715979 , const MethodInfo*))Collection_1_Remove_m1638589108_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2885725008_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2885725008(__this, ___index0, method) ((  void (*) (Collection_1_t3860703709 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2885725008_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1532484464_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1532484464(__this, ___index0, method) ((  void (*) (Collection_1_t3860703709 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1532484464_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2683407868_gshared (Collection_1_t3860703709 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2683407868(__this, method) ((  int32_t (*) (Collection_1_t3860703709 *, const MethodInfo*))Collection_1_get_Count_m2683407868_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::get_Item(System.Int32)
extern "C"  Quaternion_t1891715979  Collection_1_get_Item_m3123752694_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3123752694(__this, ___index0, method) ((  Quaternion_t1891715979  (*) (Collection_1_t3860703709 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3123752694_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3560382433_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, Quaternion_t1891715979  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3560382433(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3860703709 *, int32_t, Quaternion_t1891715979 , const MethodInfo*))Collection_1_set_Item_m3560382433_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2065803384_gshared (Collection_1_t3860703709 * __this, int32_t ___index0, Quaternion_t1891715979  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2065803384(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3860703709 *, int32_t, Quaternion_t1891715979 , const MethodInfo*))Collection_1_SetItem_m2065803384_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1392441491_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1392441491(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1392441491_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::ConvertItem(System.Object)
extern "C"  Quaternion_t1891715979  Collection_1_ConvertItem_m384692949_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m384692949(__this /* static, unused */, ___item0, method) ((  Quaternion_t1891715979  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m384692949_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m664316307_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m664316307(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m664316307_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m211027537_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m211027537(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m211027537_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Quaternion>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1874545134_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1874545134(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1874545134_gshared)(__this /* static, unused */, ___list0, method)
