﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>
struct LeftObserver_t3421766155;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>
struct CombineLatest_t3519199716;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m195482764_gshared (LeftObserver_t3421766155 * __this, CombineLatest_t3519199716 * ___parent0, const MethodInfo* method);
#define LeftObserver__ctor_m195482764(__this, ___parent0, method) ((  void (*) (LeftObserver_t3421766155 *, CombineLatest_t3519199716 *, const MethodInfo*))LeftObserver__ctor_m195482764_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m3726988583_gshared (LeftObserver_t3421766155 * __this, bool ___value0, const MethodInfo* method);
#define LeftObserver_OnNext_m3726988583(__this, ___value0, method) ((  void (*) (LeftObserver_t3421766155 *, bool, const MethodInfo*))LeftObserver_OnNext_m3726988583_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m844779005_gshared (LeftObserver_t3421766155 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define LeftObserver_OnError_m844779005(__this, ___error0, method) ((  void (*) (LeftObserver_t3421766155 *, Exception_t1967233988 *, const MethodInfo*))LeftObserver_OnError_m844779005_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Boolean>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m823678160_gshared (LeftObserver_t3421766155 * __this, const MethodInfo* method);
#define LeftObserver_OnCompleted_m823678160(__this, method) ((  void (*) (LeftObserver_t3421766155 *, const MethodInfo*))LeftObserver_OnCompleted_m823678160_gshared)(__this, method)
