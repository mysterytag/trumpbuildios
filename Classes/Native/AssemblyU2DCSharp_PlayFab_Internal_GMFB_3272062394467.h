﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t2956870243;

#include "AssemblyU2DCSharp_PlayFab_UUnit_UUnitTestCase14187365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.GMFB_327
struct  GMFB_327_t2062394467  : public UUnitTestCase_t14187365
{
public:
	// System.String[] PlayFab.Internal.GMFB_327::_examples
	StringU5BU5D_t2956870243* ____examples_4;

public:
	inline static int32_t get_offset_of__examples_4() { return static_cast<int32_t>(offsetof(GMFB_327_t2062394467, ____examples_4)); }
	inline StringU5BU5D_t2956870243* get__examples_4() const { return ____examples_4; }
	inline StringU5BU5D_t2956870243** get_address_of__examples_4() { return &____examples_4; }
	inline void set__examples_4(StringU5BU5D_t2956870243* value)
	{
		____examples_4 = value;
		Il2CppCodeGenWriteBarrier(&____examples_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
