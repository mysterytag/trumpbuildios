﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`3/Zip/RightZipObserver<System.Object,System.Object,System.Object>
struct RightZipObserver_t518765196;
// UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>
struct Zip_t2358317490;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipObservable`3/Zip/RightZipObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`3/Zip<TLeft,TRight,TResult>)
extern "C"  void RightZipObserver__ctor_m3671104837_gshared (RightZipObserver_t518765196 * __this, Zip_t2358317490 * ___parent0, const MethodInfo* method);
#define RightZipObserver__ctor_m3671104837(__this, ___parent0, method) ((  void (*) (RightZipObserver_t518765196 *, Zip_t2358317490 *, const MethodInfo*))RightZipObserver__ctor_m3671104837_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip/RightZipObserver<System.Object,System.Object,System.Object>::OnNext(TRight)
extern "C"  void RightZipObserver_OnNext_m3844837751_gshared (RightZipObserver_t518765196 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define RightZipObserver_OnNext_m3844837751(__this, ___value0, method) ((  void (*) (RightZipObserver_t518765196 *, Il2CppObject *, const MethodInfo*))RightZipObserver_OnNext_m3844837751_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip/RightZipObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void RightZipObserver_OnError_m926622510_gshared (RightZipObserver_t518765196 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method);
#define RightZipObserver_OnError_m926622510(__this, ___ex0, method) ((  void (*) (RightZipObserver_t518765196 *, Exception_t1967233988 *, const MethodInfo*))RightZipObserver_OnError_m926622510_gshared)(__this, ___ex0, method)
// System.Void UniRx.Operators.ZipObservable`3/Zip/RightZipObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void RightZipObserver_OnCompleted_m540191617_gshared (RightZipObserver_t518765196 * __this, const MethodInfo* method);
#define RightZipObserver_OnCompleted_m540191617(__this, method) ((  void (*) (RightZipObserver_t518765196 *, const MethodInfo*))RightZipObserver_OnCompleted_m540191617_gshared)(__this, method)
