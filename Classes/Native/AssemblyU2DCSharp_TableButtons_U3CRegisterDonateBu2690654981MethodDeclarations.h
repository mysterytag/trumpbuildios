﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey15E
struct U3CRegisterDonateButtonsU3Ec__AnonStorey15E_t2690654981;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey15E::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey15E__ctor_m222751209 (U3CRegisterDonateButtonsU3Ec__AnonStorey15E_t2690654981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey15E::<>m__25B(System.Boolean)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey15E_U3CU3Em__25B_m777936774 (U3CRegisterDonateButtonsU3Ec__AnonStorey15E_t2690654981 * __this, bool ___connected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey15E::<>m__25C(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey15E_U3CU3Em__25C_m3265488244 (U3CRegisterDonateButtonsU3Ec__AnonStorey15E_t2690654981 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
