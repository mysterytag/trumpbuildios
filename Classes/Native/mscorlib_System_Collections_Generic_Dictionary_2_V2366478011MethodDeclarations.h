﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1451594948MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m829140329(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2366478011 *, Dictionary_2_t444340917 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1143127465(__this, ___item0, method) ((  void (*) (ValueCollection_t2366478011 *, SharedGroupDataRecord_t3101610309 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2581849842(__this, method) ((  void (*) (ValueCollection_t2366478011 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2903643233(__this, ___item0, method) ((  bool (*) (ValueCollection_t2366478011 *, SharedGroupDataRecord_t3101610309 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m678875462(__this, ___item0, method) ((  bool (*) (ValueCollection_t2366478011 *, SharedGroupDataRecord_t3101610309 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1599133170(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2366478011 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1596195190(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2366478011 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3032086917(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2366478011 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1178819092(__this, method) ((  bool (*) (ValueCollection_t2366478011 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2651921908(__this, method) ((  bool (*) (ValueCollection_t2366478011 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1562637350(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2366478011 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3532889968(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2366478011 *, SharedGroupDataRecordU5BU5D_t275558408*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3843258521(__this, method) ((  Enumerator_t211368860  (*) (ValueCollection_t2366478011 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,PlayFab.ClientModels.SharedGroupDataRecord>::get_Count()
#define ValueCollection_get_Count_m1199657902(__this, method) ((  int32_t (*) (ValueCollection_t2366478011 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
