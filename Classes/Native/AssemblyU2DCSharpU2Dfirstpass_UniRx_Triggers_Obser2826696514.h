﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>
struct Subject_1_t848168958;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableDragTrigger
struct  ObservableDragTrigger_t2826696514  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableDragTrigger::onDrag
	Subject_1_t848168958 * ___onDrag_8;

public:
	inline static int32_t get_offset_of_onDrag_8() { return static_cast<int32_t>(offsetof(ObservableDragTrigger_t2826696514, ___onDrag_8)); }
	inline Subject_1_t848168958 * get_onDrag_8() const { return ___onDrag_8; }
	inline Subject_1_t848168958 ** get_address_of_onDrag_8() { return &___onDrag_8; }
	inline void set_onDrag_8(Subject_1_t848168958 * value)
	{
		___onDrag_8 = value;
		Il2CppCodeGenWriteBarrier(&___onDrag_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
