﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"

// System.Void System.Action`1<OnePF.Inventory>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m479994929(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2779015537 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1498188969_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<OnePF.Inventory>::Invoke(T)
#define Action_1_Invoke_m1524425160(__this, ___obj0, method) ((  void (*) (Action_1_t2779015537 *, Inventory_t2630562832 *, const MethodInfo*))Action_1_Invoke_m2789055860_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<OnePF.Inventory>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m2125458327(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2779015537 *, Inventory_t2630562832 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m917692971_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<OnePF.Inventory>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m3644164682(__this, ___result0, method) ((  void (*) (Action_1_t2779015537 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3562128182_gshared)(__this, ___result0, method)
