﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.ResultContainer`1<System.Object>
struct ResultContainer_1_t1789381198;
// System.Object
struct Il2CppObject;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;
// System.Delegate
struct Delegate_t3660574010;
// PlayFab.ErrorCallback
struct ErrorCallback_t582108558;
// System.Action`2<System.Object,PlayFab.CallRequestContainer>
struct Action_2_t3700672107;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_ErrorCallback582108558.h"

// System.Void PlayFab.Internal.ResultContainer`1<System.Object>::.ctor()
extern "C"  void ResultContainer_1__ctor_m944826126_gshared (ResultContainer_1_t1789381198 * __this, const MethodInfo* method);
#define ResultContainer_1__ctor_m944826126(__this, method) ((  void (*) (ResultContainer_1_t1789381198 *, const MethodInfo*))ResultContainer_1__ctor_m944826126_gshared)(__this, method)
// System.Void PlayFab.Internal.ResultContainer`1<System.Object>::.cctor()
extern "C"  void ResultContainer_1__cctor_m3037709919_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ResultContainer_1__cctor_m3037709919(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ResultContainer_1__cctor_m3037709919_gshared)(__this /* static, unused */, method)
// PlayFab.Internal.ResultContainer`1<TResultType> PlayFab.Internal.ResultContainer`1<System.Object>::KillWarnings()
extern "C"  ResultContainer_1_t1789381198 * ResultContainer_1_KillWarnings_m2638698408_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ResultContainer_1_KillWarnings_m2638698408(__this /* static, unused */, method) ((  ResultContainer_1_t1789381198 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ResultContainer_1_KillWarnings_m2638698408_gshared)(__this /* static, unused */, method)
// TResultType PlayFab.Internal.ResultContainer`1<System.Object>::HandleResults(PlayFab.CallRequestContainer,System.Delegate,PlayFab.ErrorCallback,System.Action`2<TResultType,PlayFab.CallRequestContainer>)
extern "C"  Il2CppObject * ResultContainer_1_HandleResults_m887745544_gshared (Il2CppObject * __this /* static, unused */, CallRequestContainer_t432318609 * ___callRequest0, Delegate_t3660574010 * ___resultCallback1, ErrorCallback_t582108558 * ___errorCallback2, Action_2_t3700672107 * ___resultAction3, const MethodInfo* method);
#define ResultContainer_1_HandleResults_m887745544(__this /* static, unused */, ___callRequest0, ___resultCallback1, ___errorCallback2, ___resultAction3, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, CallRequestContainer_t432318609 *, Delegate_t3660574010 *, ErrorCallback_t582108558 *, Action_2_t3700672107 *, const MethodInfo*))ResultContainer_1_HandleResults_m887745544_gshared)(__this /* static, unused */, ___callRequest0, ___resultCallback1, ___errorCallback2, ___resultAction3, method)
