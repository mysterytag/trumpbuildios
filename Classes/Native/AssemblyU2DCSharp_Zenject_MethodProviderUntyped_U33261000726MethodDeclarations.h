﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.MethodProviderUntyped/<GetInstance>c__AnonStorey196
struct U3CGetInstanceU3Ec__AnonStorey196_t3261000726;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.MethodProviderUntyped/<GetInstance>c__AnonStorey196::.ctor()
extern "C"  void U3CGetInstanceU3Ec__AnonStorey196__ctor_m501306052 (U3CGetInstanceU3Ec__AnonStorey196_t3261000726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Zenject.MethodProviderUntyped/<GetInstance>c__AnonStorey196::<>m__304()
extern "C"  String_t* U3CGetInstanceU3Ec__AnonStorey196_U3CU3Em__304_m487678761 (U3CGetInstanceU3Ec__AnonStorey196_t3261000726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
