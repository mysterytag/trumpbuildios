﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>
struct Empty_t222136576;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2303977857_gshared (Empty_t222136576 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m2303977857(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t222136576 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m2303977857_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>::OnNext(T)
extern "C"  void Empty_OnNext_m1440470385_gshared (Empty_t222136576 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Empty_OnNext_m1440470385(__this, ___value0, method) ((  void (*) (Empty_t222136576 *, Unit_t2558286038 , const MethodInfo*))Empty_OnNext_m1440470385_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Empty_OnError_m466677376_gshared (Empty_t222136576 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m466677376(__this, ___error0, method) ((  void (*) (Empty_t222136576 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m466677376_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.Unit>::OnCompleted()
extern "C"  void Empty_OnCompleted_m87122387_gshared (Empty_t222136576 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m87122387(__this, method) ((  void (*) (Empty_t222136576 *, const MethodInfo*))Empty_OnCompleted_m87122387_gshared)(__this, method)
