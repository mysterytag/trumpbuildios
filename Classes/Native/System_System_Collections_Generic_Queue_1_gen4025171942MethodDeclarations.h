﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2545193960MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::.ctor()
#define Queue_1__ctor_m95944814(__this, method) ((  void (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m2457144470(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t4025171942 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m2497187816(__this, method) ((  bool (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2973217862(__this, method) ((  Il2CppObject * (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2849306872(__this, method) ((  Il2CppObject* (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m4021006353(__this, method) ((  Il2CppObject * (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::Clear()
#define Queue_1_Clear_m1797045401(__this, method) ((  void (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_Clear_m448938124_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m2851770369(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t4025171942 *, IObservable_1U5BU5D_t3461854471*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::Dequeue()
#define Queue_1_Dequeue_m3046910971(__this, method) ((  Il2CppObject* (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::Peek()
#define Queue_1_Peek_m1608806930(__this, method) ((  Il2CppObject* (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::Enqueue(T)
#define Queue_1_Enqueue_m3304047882(__this, ___item0, method) ((  void (*) (Queue_1_t4025171942 *, Il2CppObject*, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m78057177(__this, ___new_size0, method) ((  void (*) (Queue_1_t4025171942 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::get_Count()
#define Queue_1_get_Count_m4162476450(__this, method) ((  int32_t (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<UniRx.IObservable`1<UniRx.Unit>>::GetEnumerator()
#define Queue_1_GetEnumerator_m4080383359(__this, method) ((  Enumerator_t1199826363  (*) (Queue_1_t4025171942 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
