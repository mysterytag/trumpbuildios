﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergRush.ImmutableList`1<System.Object>
struct ImmutableList_1_t340490602;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ZergRush.ImmutableList`1<System.Object>::.ctor()
extern "C"  void ImmutableList_1__ctor_m99192906_gshared (ImmutableList_1_t340490602 * __this, const MethodInfo* method);
#define ImmutableList_1__ctor_m99192906(__this, method) ((  void (*) (ImmutableList_1_t340490602 *, const MethodInfo*))ImmutableList_1__ctor_m99192906_gshared)(__this, method)
// System.Void ZergRush.ImmutableList`1<System.Object>::.ctor(T)
extern "C"  void ImmutableList_1__ctor_m3074981460_gshared (ImmutableList_1_t340490602 * __this, Il2CppObject * ___single0, const MethodInfo* method);
#define ImmutableList_1__ctor_m3074981460(__this, ___single0, method) ((  void (*) (ImmutableList_1_t340490602 *, Il2CppObject *, const MethodInfo*))ImmutableList_1__ctor_m3074981460_gshared)(__this, ___single0, method)
// System.Void ZergRush.ImmutableList`1<System.Object>::.ctor(T[])
extern "C"  void ImmutableList_1__ctor_m119734386_gshared (ImmutableList_1_t340490602 * __this, ObjectU5BU5D_t11523773* ___data0, const MethodInfo* method);
#define ImmutableList_1__ctor_m119734386(__this, ___data0, method) ((  void (*) (ImmutableList_1_t340490602 *, ObjectU5BU5D_t11523773*, const MethodInfo*))ImmutableList_1__ctor_m119734386_gshared)(__this, ___data0, method)
// System.Collections.IEnumerator ZergRush.ImmutableList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2591974359_gshared (ImmutableList_1_t340490602 * __this, const MethodInfo* method);
#define ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2591974359(__this, method) ((  Il2CppObject * (*) (ImmutableList_1_t340490602 *, const MethodInfo*))ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2591974359_gshared)(__this, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Object>::Add(T)
extern "C"  ImmutableList_1_t340490602 * ImmutableList_1_Add_m3924978445_gshared (ImmutableList_1_t340490602 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ImmutableList_1_Add_m3924978445(__this, ___value0, method) ((  ImmutableList_1_t340490602 * (*) (ImmutableList_1_t340490602 *, Il2CppObject *, const MethodInfo*))ImmutableList_1_Add_m3924978445_gshared)(__this, ___value0, method)
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ImmutableList_1_GetEnumerator_m364779198_gshared (ImmutableList_1_t340490602 * __this, const MethodInfo* method);
#define ImmutableList_1_GetEnumerator_m364779198(__this, method) ((  Il2CppObject* (*) (ImmutableList_1_t340490602 *, const MethodInfo*))ImmutableList_1_GetEnumerator_m364779198_gshared)(__this, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Object>::Remove(T)
extern "C"  ImmutableList_1_t340490602 * ImmutableList_1_Remove_m1092573448_gshared (ImmutableList_1_t340490602 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ImmutableList_1_Remove_m1092573448(__this, ___value0, method) ((  ImmutableList_1_t340490602 * (*) (ImmutableList_1_t340490602 *, Il2CppObject *, const MethodInfo*))ImmutableList_1_Remove_m1092573448_gshared)(__this, ___value0, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Object>::Replace(System.Int32,T)
extern "C"  ImmutableList_1_t340490602 * ImmutableList_1_Replace_m2580545005_gshared (ImmutableList_1_t340490602 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ImmutableList_1_Replace_m2580545005(__this, ___index0, ___value1, method) ((  ImmutableList_1_t340490602 * (*) (ImmutableList_1_t340490602 *, int32_t, Il2CppObject *, const MethodInfo*))ImmutableList_1_Replace_m2580545005_gshared)(__this, ___index0, ___value1, method)
// System.Int32 ZergRush.ImmutableList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ImmutableList_1_IndexOf_m1554103039_gshared (ImmutableList_1_t340490602 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ImmutableList_1_IndexOf_m1554103039(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t340490602 *, Il2CppObject *, const MethodInfo*))ImmutableList_1_IndexOf_m1554103039_gshared)(__this, ___value0, method)
// System.Int32 ZergRush.ImmutableList`1<System.Object>::get_Count()
extern "C"  int32_t ImmutableList_1_get_Count_m749861468_gshared (ImmutableList_1_t340490602 * __this, const MethodInfo* method);
#define ImmutableList_1_get_Count_m749861468(__this, method) ((  int32_t (*) (ImmutableList_1_t340490602 *, const MethodInfo*))ImmutableList_1_get_Count_m749861468_gshared)(__this, method)
