﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/LeftObserver<System.Object,System.Object,System.Object>
struct LeftObserver_t2250424592;
// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>
struct WithLatestFrom_t2869896360;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/LeftObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m2587755107_gshared (LeftObserver_t2250424592 * __this, WithLatestFrom_t2869896360 * ___parent0, const MethodInfo* method);
#define LeftObserver__ctor_m2587755107(__this, ___parent0, method) ((  void (*) (LeftObserver_t2250424592 *, WithLatestFrom_t2869896360 *, const MethodInfo*))LeftObserver__ctor_m2587755107_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/LeftObserver<System.Object,System.Object,System.Object>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m4229919434_gshared (LeftObserver_t2250424592 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define LeftObserver_OnNext_m4229919434(__this, ___value0, method) ((  void (*) (LeftObserver_t2250424592 *, Il2CppObject *, const MethodInfo*))LeftObserver_OnNext_m4229919434_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/LeftObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m3732803360_gshared (LeftObserver_t2250424592 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define LeftObserver_OnError_m3732803360(__this, ___error0, method) ((  void (*) (LeftObserver_t2250424592 *, Exception_t1967233988 *, const MethodInfo*))LeftObserver_OnError_m3732803360_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/LeftObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m1326609011_gshared (LeftObserver_t2250424592 * __this, const MethodInfo* method);
#define LeftObserver_OnCompleted_m1326609011(__this, method) ((  void (*) (LeftObserver_t2250424592 *, const MethodInfo*))LeftObserver_OnCompleted_m1326609011_gshared)(__this, method)
