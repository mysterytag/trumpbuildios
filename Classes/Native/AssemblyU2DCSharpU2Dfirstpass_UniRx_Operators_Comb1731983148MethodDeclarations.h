﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestObservable_5_t1731983148;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.CombineLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_5_t545776053;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CombineLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.Operators.CombineLatestFunc`5<T1,T2,T3,T4,TR>)
extern "C"  void CombineLatestObservable_5__ctor_m3985938352_gshared (CombineLatestObservable_5_t1731983148 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, CombineLatestFunc_5_t545776053 * ___resultSelector4, const MethodInfo* method);
#define CombineLatestObservable_5__ctor_m3985938352(__this, ___source10, ___source21, ___source32, ___source43, ___resultSelector4, method) ((  void (*) (CombineLatestObservable_5_t1731983148 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, CombineLatestFunc_5_t545776053 *, const MethodInfo*))CombineLatestObservable_5__ctor_m3985938352_gshared)(__this, ___source10, ___source21, ___source32, ___source43, ___resultSelector4, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_5_SubscribeCore_m1504255252_gshared (CombineLatestObservable_5_t1731983148 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CombineLatestObservable_5_SubscribeCore_m1504255252(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CombineLatestObservable_5_t1731983148 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatestObservable_5_SubscribeCore_m1504255252_gshared)(__this, ___observer0, ___cancel1, method)
