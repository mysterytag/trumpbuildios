﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::.ctor()
#define List_1__ctor_m98759399(__this, method) ((  void (*) (List_1_t45083516 *, const MethodInfo*))List_1__ctor_m348833073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m169004856(__this, ___collection0, method) ((  void (*) (List_1_t45083516 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::.ctor(System.Int32)
#define List_1__ctor_m1842382264(__this, ___capacity0, method) ((  void (*) (List_1_t45083516 *, int32_t, const MethodInfo*))List_1__ctor_m2892763214_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::.cctor()
#define List_1__cctor_m2579445158(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m585931897(__this, method) ((  Il2CppObject* (*) (List_1_t45083516 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m427182141(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t45083516 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3376821880(__this, method) ((  Il2CppObject * (*) (List_1_t45083516 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3230060921(__this, ___item0, method) ((  int32_t (*) (List_1_t45083516 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1356082675(__this, ___item0, method) ((  bool (*) (List_1_t45083516 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1151312721(__this, ___item0, method) ((  int32_t (*) (List_1_t45083516 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2392443388(__this, ___index0, ___item1, method) ((  void (*) (List_1_t45083516 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m4205663660(__this, ___item0, method) ((  void (*) (List_1_t45083516 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2147584564(__this, method) ((  bool (*) (List_1_t45083516 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m591560841(__this, method) ((  bool (*) (List_1_t45083516 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2826590005(__this, method) ((  Il2CppObject * (*) (List_1_t45083516 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3578138530(__this, method) ((  bool (*) (List_1_t45083516 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2924737303(__this, method) ((  bool (*) (List_1_t45083516 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2999275388(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t45083516 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1944544979(__this, ___index0, ___value1, method) ((  void (*) (List_1_t45083516 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Add(T)
#define List_1_Add_m824612024(__this, ___item0, method) ((  void (*) (List_1_t45083516 *, CartItem_t3543091843 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1236809459(__this, ___newCount0, method) ((  void (*) (List_1_t45083516 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2350383601(__this, ___collection0, method) ((  void (*) (List_1_t45083516 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1611355825(__this, ___enumerable0, method) ((  void (*) (List_1_t45083516 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m877948230(__this, ___collection0, method) ((  void (*) (List_1_t45083516 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::AsReadOnly()
#define List_1_AsReadOnly_m1152077347(__this, method) ((  ReadOnlyCollection_1_t2411269895 * (*) (List_1_t45083516 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Clear()
#define List_1_Clear_m1799859986(__this, method) ((  void (*) (List_1_t45083516 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Contains(T)
#define List_1_Contains_m3139131200(__this, ___item0, method) ((  bool (*) (List_1_t45083516 *, CartItem_t3543091843 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2479210152(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t45083516 *, CartItemU5BU5D_t2928256018*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Find(System.Predicate`1<T>)
#define List_1_Find_m2014164544(__this, ___match0, method) ((  CartItem_t3543091843 * (*) (List_1_t45083516 *, Predicate_1_t4114055741 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m440687931(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4114055741 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3797934944(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t45083516 *, int32_t, int32_t, Predicate_1_t4114055741 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m2765127919(__this, ___action0, method) ((  void (*) (List_1_t45083516 *, Action_1_t3691544548 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::GetEnumerator()
#define List_1_GetEnumerator_m637545597(__this, method) ((  Enumerator_t2425833804  (*) (List_1_t45083516 *, const MethodInfo*))List_1_GetEnumerator_m1820263692_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::IndexOf(T)
#define List_1_IndexOf_m1456396844(__this, ___item0, method) ((  int32_t (*) (List_1_t45083516 *, CartItem_t3543091843 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2187881727(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t45083516 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2225577336(__this, ___index0, method) ((  void (*) (List_1_t45083516 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Insert(System.Int32,T)
#define List_1_Insert_m2442354463(__this, ___index0, ___item1, method) ((  void (*) (List_1_t45083516 *, int32_t, CartItem_t3543091843 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m92511316(__this, ___collection0, method) ((  void (*) (List_1_t45083516 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Remove(T)
#define List_1_Remove_m364701499(__this, ___item0, method) ((  bool (*) (List_1_t45083516 *, CartItem_t3543091843 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1651538159(__this, ___match0, method) ((  int32_t (*) (List_1_t45083516 *, Predicate_1_t4114055741 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m316207333(__this, ___index0, method) ((  void (*) (List_1_t45083516 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Reverse()
#define List_1_Reverse_m456313191(__this, method) ((  void (*) (List_1_t45083516 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Sort()
#define List_1_Sort_m1350584219(__this, method) ((  void (*) (List_1_t45083516 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1529350313(__this, ___comparer0, method) ((  void (*) (List_1_t45083516 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1200545198(__this, ___comparison0, method) ((  void (*) (List_1_t45083516 *, Comparison_1_t1951799423 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::ToArray()
#define List_1_ToArray_m1955071078(__this, method) ((  CartItemU5BU5D_t2928256018* (*) (List_1_t45083516 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::TrimExcess()
#define List_1_TrimExcess_m3129214708(__this, method) ((  void (*) (List_1_t45083516 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::get_Capacity()
#define List_1_get_Capacity_m576576604(__this, method) ((  int32_t (*) (List_1_t45083516 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2741965573(__this, ___value0, method) ((  void (*) (List_1_t45083516 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::get_Count()
#define List_1_get_Count_m2015936719(__this, method) ((  int32_t (*) (List_1_t45083516 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::get_Item(System.Int32)
#define List_1_get_Item_m2894322793(__this, ___index0, method) ((  CartItem_t3543091843 * (*) (List_1_t45083516 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>::set_Item(System.Int32,T)
#define List_1_set_Item_m3860091958(__this, ___index0, ___value1, method) ((  void (*) (List_1_t45083516 *, int32_t, CartItem_t3543091843 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
