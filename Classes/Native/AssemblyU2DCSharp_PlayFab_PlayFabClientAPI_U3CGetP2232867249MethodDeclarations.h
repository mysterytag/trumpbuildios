﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromPSNAccountIDs>c__AnonStorey72
struct U3CGetPlayFabIDsFromPSNAccountIDsU3Ec__AnonStorey72_t2232867249;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromPSNAccountIDs>c__AnonStorey72::.ctor()
extern "C"  void U3CGetPlayFabIDsFromPSNAccountIDsU3Ec__AnonStorey72__ctor_m725447970 (U3CGetPlayFabIDsFromPSNAccountIDsU3Ec__AnonStorey72_t2232867249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromPSNAccountIDs>c__AnonStorey72::<>m__5F(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPlayFabIDsFromPSNAccountIDsU3Ec__AnonStorey72_U3CU3Em__5F_m3419166577 (U3CGetPlayFabIDsFromPSNAccountIDsU3Ec__AnonStorey72_t2232867249 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
