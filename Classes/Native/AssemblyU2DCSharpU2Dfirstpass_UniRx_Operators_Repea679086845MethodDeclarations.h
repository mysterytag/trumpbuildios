﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatUntilObservable`1<System.Object>
struct RepeatUntilObservable_1_t679086845;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void UniRx.Operators.RepeatUntilObservable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>,UniRx.IObservable`1<UniRx.Unit>,UnityEngine.GameObject)
extern "C"  void RepeatUntilObservable_1__ctor_m264264542_gshared (RepeatUntilObservable_1_t679086845 * __this, Il2CppObject* ___sources0, Il2CppObject* ___trigger1, GameObject_t4012695102 * ___lifeTimeChecker2, const MethodInfo* method);
#define RepeatUntilObservable_1__ctor_m264264542(__this, ___sources0, ___trigger1, ___lifeTimeChecker2, method) ((  void (*) (RepeatUntilObservable_1_t679086845 *, Il2CppObject*, Il2CppObject*, GameObject_t4012695102 *, const MethodInfo*))RepeatUntilObservable_1__ctor_m264264542_gshared)(__this, ___sources0, ___trigger1, ___lifeTimeChecker2, method)
// System.IDisposable UniRx.Operators.RepeatUntilObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * RepeatUntilObservable_1_SubscribeCore_m391756075_gshared (RepeatUntilObservable_1_t679086845 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define RepeatUntilObservable_1_SubscribeCore_m391756075(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (RepeatUntilObservable_1_t679086845 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))RepeatUntilObservable_1_SubscribeCore_m391756075_gshared)(__this, ___observer0, ___cancel1, method)
