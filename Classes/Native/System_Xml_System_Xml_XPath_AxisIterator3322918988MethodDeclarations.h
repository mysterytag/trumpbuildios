﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.AxisIterator
struct AxisIterator_t3322918988;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.NodeTest
struct NodeTest_t911751889;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1624538935;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_NodeTest911751889.h"
#include "System_Xml_System_Xml_XPath_AxisIterator3322918988.h"

// System.Void System.Xml.XPath.AxisIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.NodeTest)
extern "C"  void AxisIterator__ctor_m2307956914 (AxisIterator_t3322918988 * __this, BaseIterator_t3696600956 * ___iter0, NodeTest_t911751889 * ___test1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.AxisIterator::.ctor(System.Xml.XPath.AxisIterator)
extern "C"  void AxisIterator__ctor_m967112753 (AxisIterator_t3322918988 * __this, AxisIterator_t3322918988 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.AxisIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * AxisIterator_Clone_m2661155633 (AxisIterator_t3322918988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.AxisIterator::MoveNextCore()
extern "C"  bool AxisIterator_MoveNextCore_m314688244 (AxisIterator_t3322918988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator System.Xml.XPath.AxisIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * AxisIterator_get_Current_m1435899435 (AxisIterator_t3322918988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.AxisIterator::get_ReverseAxis()
extern "C"  bool AxisIterator_get_ReverseAxis_m1414729771 (AxisIterator_t3322918988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
