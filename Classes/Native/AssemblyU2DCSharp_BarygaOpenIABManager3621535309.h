﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t2956870243;
// System.Collections.Generic.Dictionary`2<System.String,Cell`1<System.Single>>
struct Dictionary_2_t2778004557;
// Cell`1<System.String>
struct Cell_1_t1150586534;
// BarygaOpenIABManager
struct BarygaOpenIABManager_t3621535309;
// GlobalStat
struct GlobalStat_t1134678199;
// System.Func`3<System.Single,System.String,<>__AnonType4`2<System.Single,System.String>>
struct Func_3_t4290768246;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaOpenIABManager
struct  BarygaOpenIABManager_t3621535309  : public MonoBehaviour_t3012272455
{
public:
	// System.String[] BarygaOpenIABManager::skus
	StringU5BU5D_t2956870243* ___skus_2;
	// GlobalStat BarygaOpenIABManager::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_6;

public:
	inline static int32_t get_offset_of_skus_2() { return static_cast<int32_t>(offsetof(BarygaOpenIABManager_t3621535309, ___skus_2)); }
	inline StringU5BU5D_t2956870243* get_skus_2() const { return ___skus_2; }
	inline StringU5BU5D_t2956870243** get_address_of_skus_2() { return &___skus_2; }
	inline void set_skus_2(StringU5BU5D_t2956870243* value)
	{
		___skus_2 = value;
		Il2CppCodeGenWriteBarrier(&___skus_2, value);
	}

	inline static int32_t get_offset_of_GlobalStat_6() { return static_cast<int32_t>(offsetof(BarygaOpenIABManager_t3621535309, ___GlobalStat_6)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_6() const { return ___GlobalStat_6; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_6() { return &___GlobalStat_6; }
	inline void set_GlobalStat_6(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_6 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_6, value);
	}
};

struct BarygaOpenIABManager_t3621535309_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Cell`1<System.Single>> BarygaOpenIABManager::priceDictionary
	Dictionary_2_t2778004557 * ___priceDictionary_3;
	// Cell`1<System.String> BarygaOpenIABManager::ReactiveCurrency
	Cell_1_t1150586534 * ___ReactiveCurrency_4;
	// BarygaOpenIABManager BarygaOpenIABManager::instance
	BarygaOpenIABManager_t3621535309 * ___instance_5;
	// System.Func`3<System.Single,System.String,<>__AnonType4`2<System.Single,System.String>> BarygaOpenIABManager::<>f__am$cache5
	Func_3_t4290768246 * ___U3CU3Ef__amU24cache5_7;

public:
	inline static int32_t get_offset_of_priceDictionary_3() { return static_cast<int32_t>(offsetof(BarygaOpenIABManager_t3621535309_StaticFields, ___priceDictionary_3)); }
	inline Dictionary_2_t2778004557 * get_priceDictionary_3() const { return ___priceDictionary_3; }
	inline Dictionary_2_t2778004557 ** get_address_of_priceDictionary_3() { return &___priceDictionary_3; }
	inline void set_priceDictionary_3(Dictionary_2_t2778004557 * value)
	{
		___priceDictionary_3 = value;
		Il2CppCodeGenWriteBarrier(&___priceDictionary_3, value);
	}

	inline static int32_t get_offset_of_ReactiveCurrency_4() { return static_cast<int32_t>(offsetof(BarygaOpenIABManager_t3621535309_StaticFields, ___ReactiveCurrency_4)); }
	inline Cell_1_t1150586534 * get_ReactiveCurrency_4() const { return ___ReactiveCurrency_4; }
	inline Cell_1_t1150586534 ** get_address_of_ReactiveCurrency_4() { return &___ReactiveCurrency_4; }
	inline void set_ReactiveCurrency_4(Cell_1_t1150586534 * value)
	{
		___ReactiveCurrency_4 = value;
		Il2CppCodeGenWriteBarrier(&___ReactiveCurrency_4, value);
	}

	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(BarygaOpenIABManager_t3621535309_StaticFields, ___instance_5)); }
	inline BarygaOpenIABManager_t3621535309 * get_instance_5() const { return ___instance_5; }
	inline BarygaOpenIABManager_t3621535309 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(BarygaOpenIABManager_t3621535309 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(BarygaOpenIABManager_t3621535309_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Func_3_t4290768246 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Func_3_t4290768246 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Func_3_t4290768246 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
