﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputEventDescriptor
struct InputEventDescriptor_t4263964831;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_InputEventDescriptor_Type2622298.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void InputEventDescriptor::.ctor(InputEventDescriptor/Type,UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  void InputEventDescriptor__ctor_m3444778787 (InputEventDescriptor_t4263964831 * __this, int32_t ___type_0, Vector2_t3525329788  ___inputPosition_1, Vector2_t3525329788  ___inputDelta_2, int32_t ___fingerId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputEventDescriptor::get_used()
extern "C"  bool InputEventDescriptor_get_used_m2880273794 (InputEventDescriptor_t4263964831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputEventDescriptor::Use()
extern "C"  void InputEventDescriptor_Use_m1959230465 (InputEventDescriptor_t4263964831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
