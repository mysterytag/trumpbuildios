﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType4`2<System.Object,System.Object>
struct U3CU3E__AnonType4_2_t2684394961;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType4`2<System.Object,System.Object>::.ctor(<f>__T,<valuta>__T)
extern "C"  void U3CU3E__AnonType4_2__ctor_m4203460648_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, Il2CppObject * ___f0, Il2CppObject * ___valuta1, const MethodInfo* method);
#define U3CU3E__AnonType4_2__ctor_m4203460648(__this, ___f0, ___valuta1, method) ((  void (*) (U3CU3E__AnonType4_2_t2684394961 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType4_2__ctor_m4203460648_gshared)(__this, ___f0, ___valuta1, method)
// <f>__T <>__AnonType4`2<System.Object,System.Object>::get_f()
extern "C"  Il2CppObject * U3CU3E__AnonType4_2_get_f_m1903600205_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, const MethodInfo* method);
#define U3CU3E__AnonType4_2_get_f_m1903600205(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType4_2_t2684394961 *, const MethodInfo*))U3CU3E__AnonType4_2_get_f_m1903600205_gshared)(__this, method)
// <valuta>__T <>__AnonType4`2<System.Object,System.Object>::get_valuta()
extern "C"  Il2CppObject * U3CU3E__AnonType4_2_get_valuta_m2588036671_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, const MethodInfo* method);
#define U3CU3E__AnonType4_2_get_valuta_m2588036671(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType4_2_t2684394961 *, const MethodInfo*))U3CU3E__AnonType4_2_get_valuta_m2588036671_gshared)(__this, method)
// System.Boolean <>__AnonType4`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType4_2_Equals_m2623510220_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType4_2_Equals_m2623510220(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType4_2_t2684394961 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType4_2_Equals_m2623510220_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType4`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType4_2_GetHashCode_m259438832_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, const MethodInfo* method);
#define U3CU3E__AnonType4_2_GetHashCode_m259438832(__this, method) ((  int32_t (*) (U3CU3E__AnonType4_2_t2684394961 *, const MethodInfo*))U3CU3E__AnonType4_2_GetHashCode_m259438832_gshared)(__this, method)
// System.String <>__AnonType4`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType4_2_ToString_m3668642724_gshared (U3CU3E__AnonType4_2_t2684394961 * __this, const MethodInfo* method);
#define U3CU3E__AnonType4_2_ToString_m3668642724(__this, method) ((  String_t* (*) (U3CU3E__AnonType4_2_t2684394961 *, const MethodInfo*))U3CU3E__AnonType4_2_ToString_m3668642724_gshared)(__this, method)
