﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSNotificationManager
struct IOSNotificationManager_t324309781;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void IOSNotificationManager::.ctor()
extern "C"  void IOSNotificationManager__ctor_m1076054310 (IOSNotificationManager_t324309781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationManager::setNotification(System.String,System.Int32)
extern "C"  void IOSNotificationManager_setNotification_m3129607078 (Il2CppObject * __this /* static, unused */, String_t* ___body0, int32_t ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationManager::cancelAllNotification()
extern "C"  void IOSNotificationManager_cancelAllNotification_m2249774902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationManager::SetNotification(System.String,System.Int32,System.String)
extern "C"  void IOSNotificationManager_SetNotification_m3045568834 (IOSNotificationManager_t324309781 * __this, String_t* ___body0, int32_t ___delay1, String_t* ___title2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSNotificationManager::CancelAllNotification()
extern "C"  void IOSNotificationManager_CancelAllNotification_m1072017174 (IOSNotificationManager_t324309781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSNotificationManager::CanCallMethod()
extern "C"  bool IOSNotificationManager_CanCallMethod_m309510943 (IOSNotificationManager_t324309781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
