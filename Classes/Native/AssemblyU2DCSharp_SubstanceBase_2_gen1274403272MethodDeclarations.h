﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2<System.Double,System.Double>
struct SubstanceBase_2_t1274403272;
// ICell`1<System.Double>
struct ICell_1_t2086147591;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// IStream`1<System.Double>
struct IStream_1_t1087207605;
// SubstanceBase`2/Intrusion<System.Double,System.Double>
struct Intrusion_t3779021028;
// ILiving
struct ILiving_t2639664210;
// IEmptyStream
struct IEmptyStream_t3684082468;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void SubstanceBase`2<System.Double,System.Double>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m3287773736_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method);
#define SubstanceBase_2__ctor_m3287773736(__this, method) ((  void (*) (SubstanceBase_2_t1274403272 *, const MethodInfo*))SubstanceBase_2__ctor_m3287773736_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m3136739382_gshared (SubstanceBase_2_t1274403272 * __this, double ___baseVal0, const MethodInfo* method);
#define SubstanceBase_2__ctor_m3136739382(__this, ___baseVal0, method) ((  void (*) (SubstanceBase_2_t1274403272 *, double, const MethodInfo*))SubstanceBase_2__ctor_m3136739382_gshared)(__this, ___baseVal0, method)
// T SubstanceBase`2<System.Double,System.Double>::get_baseValue()
extern "C"  double SubstanceBase_2_get_baseValue_m1197418750_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_baseValue_m1197418750(__this, method) ((  double (*) (SubstanceBase_2_t1274403272 *, const MethodInfo*))SubstanceBase_2_get_baseValue_m1197418750_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::set_baseValue(T)
extern "C"  void SubstanceBase_2_set_baseValue_m2977028949_gshared (SubstanceBase_2_t1274403272 * __this, double ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValue_m2977028949(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t1274403272 *, double, const MethodInfo*))SubstanceBase_2_set_baseValue_m2977028949_gshared)(__this, ___value0, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m1392286264_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define SubstanceBase_2_set_baseValueReactive_m1392286264(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t1274403272 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_set_baseValueReactive_m1392286264_gshared)(__this, ___value0, method)
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m3273213206_gshared (SubstanceBase_2_t1274403272 * __this, Action_1_t682969319 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_Bind_m3273213206(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, Action_1_t682969319 *, int32_t, const MethodInfo*))SubstanceBase_2_Bind_m3273213206_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m3842115533_gshared (SubstanceBase_2_t1274403272 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_OnChanged_m3842115533(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, Action_t437523947 *, int32_t, const MethodInfo*))SubstanceBase_2_OnChanged_m3842115533_gshared)(__this, ___action0, ___p1, method)
// System.Object SubstanceBase`2<System.Double,System.Double>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m2827238584_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_valueObject_m2827238584(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, const MethodInfo*))SubstanceBase_2_get_valueObject_m2827238584_gshared)(__this, method)
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m2221979564_gshared (SubstanceBase_2_t1274403272 * __this, Action_1_t682969319 * ___action0, int32_t ___p1, const MethodInfo* method);
#define SubstanceBase_2_ListenUpdates_m2221979564(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, Action_1_t682969319 *, int32_t, const MethodInfo*))SubstanceBase_2_ListenUpdates_m2221979564_gshared)(__this, ___action0, ___p1, method)
// IStream`1<T> SubstanceBase`2<System.Double,System.Double>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m3563169942_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_updates_m3563169942(__this, method) ((  Il2CppObject* (*) (SubstanceBase_2_t1274403272 *, const MethodInfo*))SubstanceBase_2_get_updates_m3563169942_gshared)(__this, method)
// T SubstanceBase`2<System.Double,System.Double>::get_value()
extern "C"  double SubstanceBase_2_get_value_m1555147663_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method);
#define SubstanceBase_2_get_value_m1555147663(__this, method) ((  double (*) (SubstanceBase_2_t1274403272 *, const MethodInfo*))SubstanceBase_2_get_value_m1555147663_gshared)(__this, method)
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Double,System.Double>::AffectInner(ILiving,InfT)
extern "C"  Intrusion_t3779021028 * SubstanceBase_2_AffectInner_m1808201801_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject * ___intruder0, double ___influence1, const MethodInfo* method);
#define SubstanceBase_2_AffectInner_m1808201801(__this, ___intruder0, ___influence1, method) ((  Intrusion_t3779021028 * (*) (SubstanceBase_2_t1274403272 *, Il2CppObject *, double, const MethodInfo*))SubstanceBase_2_AffectInner_m1808201801_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::AffectUnsafe(InfT)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m1121183351_gshared (SubstanceBase_2_t1274403272 * __this, double ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m1121183351(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, double, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m1121183351_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::AffectUnsafe(ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m4124092797_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject* ___influence0, const MethodInfo* method);
#define SubstanceBase_2_AffectUnsafe_m4124092797(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m4124092797_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::Affect(ILiving,InfT)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3920063127_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject * ___intruder0, double ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m3920063127(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, Il2CppObject *, double, const MethodInfo*))SubstanceBase_2_Affect_m3920063127_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::Affect(ILiving,ICell`1<InfT>)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3017787741_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method);
#define SubstanceBase_2_Affect_m3017787741(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_Affect_m3017787741_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::Affect(ILiving,InfT,IEmptyStream)
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m2313019691_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject * ___intruder0, double ___influence1, Il2CppObject * ___update2, const MethodInfo* method);
#define SubstanceBase_2_Affect_m2313019691(__this, ___intruder0, ___influence1, ___update2, method) ((  Il2CppObject * (*) (SubstanceBase_2_t1274403272 *, Il2CppObject *, double, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m2313019691_gshared)(__this, ___intruder0, ___influence1, ___update2, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m1444383432_gshared (SubstanceBase_2_t1274403272 * __this, Intrusion_t3779021028 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnAdd_m1444383432(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1274403272 *, Intrusion_t3779021028 *, const MethodInfo*))SubstanceBase_2_OnAdd_m1444383432_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m647466897_gshared (SubstanceBase_2_t1274403272 * __this, Intrusion_t3779021028 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnRemove_m647466897(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1274403272 *, Intrusion_t3779021028 *, const MethodInfo*))SubstanceBase_2_OnRemove_m647466897_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m4293165356_gshared (SubstanceBase_2_t1274403272 * __this, Intrusion_t3779021028 * ___intrusion0, const MethodInfo* method);
#define SubstanceBase_2_OnUpdate_m4293165356(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t1274403272 *, Intrusion_t3779021028 *, const MethodInfo*))SubstanceBase_2_OnUpdate_m4293165356_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m814067422_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method);
#define SubstanceBase_2_UpdateAll_m814067422(__this, method) ((  void (*) (SubstanceBase_2_t1274403272 *, const MethodInfo*))SubstanceBase_2_UpdateAll_m814067422_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_ClearIntrusion_m2462366554_gshared (SubstanceBase_2_t1274403272 * __this, Intrusion_t3779021028 * ___intr0, const MethodInfo* method);
#define SubstanceBase_2_ClearIntrusion_m2462366554(__this, ___intr0, method) ((  void (*) (SubstanceBase_2_t1274403272 *, Intrusion_t3779021028 *, const MethodInfo*))SubstanceBase_2_ClearIntrusion_m2462366554_gshared)(__this, ___intr0, method)
// System.Void SubstanceBase`2<System.Double,System.Double>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m954039939_gshared (SubstanceBase_2_t1274403272 * __this, double ___val0, const MethodInfo* method);
#define SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m954039939(__this, ___val0, method) ((  void (*) (SubstanceBase_2_t1274403272 *, double, const MethodInfo*))SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m954039939_gshared)(__this, ___val0, method)
