﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>
struct FromCoroutineObserver_t1441547487;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void FromCoroutineObserver__ctor_m747451158_gshared (FromCoroutineObserver_t1441547487 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromCoroutineObserver__ctor_m747451158(__this, ___observer0, ___cancel1, method) ((  void (*) (FromCoroutineObserver_t1441547487 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromCoroutineObserver__ctor_m747451158_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>::OnNext(T)
extern "C"  void FromCoroutineObserver_OnNext_m2734034620_gshared (FromCoroutineObserver_t1441547487 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define FromCoroutineObserver_OnNext_m2734034620(__this, ___value0, method) ((  void (*) (FromCoroutineObserver_t1441547487 *, Unit_t2558286038 , const MethodInfo*))FromCoroutineObserver_OnNext_m2734034620_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>::OnError(System.Exception)
extern "C"  void FromCoroutineObserver_OnError_m1763520971_gshared (FromCoroutineObserver_t1441547487 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define FromCoroutineObserver_OnError_m1763520971(__this, ___error0, method) ((  void (*) (FromCoroutineObserver_t1441547487 *, Exception_t1967233988 *, const MethodInfo*))FromCoroutineObserver_OnError_m1763520971_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>::OnCompleted()
extern "C"  void FromCoroutineObserver_OnCompleted_m1554513310_gshared (FromCoroutineObserver_t1441547487 * __this, const MethodInfo* method);
#define FromCoroutineObserver_OnCompleted_m1554513310(__this, method) ((  void (*) (FromCoroutineObserver_t1441547487 *, const MethodInfo*))FromCoroutineObserver_OnCompleted_m1554513310_gshared)(__this, method)
