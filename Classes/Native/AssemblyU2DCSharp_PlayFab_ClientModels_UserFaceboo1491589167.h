﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UserFacebookInfo
struct  UserFacebookInfo_t1491589167  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.UserFacebookInfo::<FacebookId>k__BackingField
	String_t* ___U3CFacebookIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.UserFacebookInfo::<FullName>k__BackingField
	String_t* ___U3CFullNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFacebookIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UserFacebookInfo_t1491589167, ___U3CFacebookIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CFacebookIdU3Ek__BackingField_0() const { return ___U3CFacebookIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFacebookIdU3Ek__BackingField_0() { return &___U3CFacebookIdU3Ek__BackingField_0; }
	inline void set_U3CFacebookIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CFacebookIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFacebookIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CFullNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UserFacebookInfo_t1491589167, ___U3CFullNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CFullNameU3Ek__BackingField_1() const { return ___U3CFullNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFullNameU3Ek__BackingField_1() { return &___U3CFullNameU3Ek__BackingField_1; }
	inline void set_U3CFullNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CFullNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFullNameU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
