﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Object>
struct U3COnNextU3Ec__AnonStorey6A_t2257624715;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Object>::.ctor()
extern "C"  void U3COnNextU3Ec__AnonStorey6A__ctor_m2602365450_gshared (U3COnNextU3Ec__AnonStorey6A_t2257624715 * __this, const MethodInfo* method);
#define U3COnNextU3Ec__AnonStorey6A__ctor_m2602365450(__this, method) ((  void (*) (U3COnNextU3Ec__AnonStorey6A_t2257624715 *, const MethodInfo*))U3COnNextU3Ec__AnonStorey6A__ctor_m2602365450_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleObservable`1/Throttle/<OnNext>c__AnonStorey6A<System.Object>::<>m__88()
extern "C"  void U3COnNextU3Ec__AnonStorey6A_U3CU3Em__88_m3510487379_gshared (U3COnNextU3Ec__AnonStorey6A_t2257624715 * __this, const MethodInfo* method);
#define U3COnNextU3Ec__AnonStorey6A_U3CU3Em__88_m3510487379(__this, method) ((  void (*) (U3COnNextU3Ec__AnonStorey6A_t2257624715 *, const MethodInfo*))U3COnNextU3Ec__AnonStorey6A_U3CU3Em__88_m3510487379_gshared)(__this, method)
