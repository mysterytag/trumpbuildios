﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Double,System.Double,System.Double>
struct SelectManyEnumerableObserver_t1450222884;
// UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>
struct SelectManyObservable_3_t44588665;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Double,System.Double,System.Double>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserver__ctor_m579763476_gshared (SelectManyEnumerableObserver_t1450222884 * __this, SelectManyObservable_3_t44588665 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserver__ctor_m579763476(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserver_t1450222884 *, SelectManyObservable_3_t44588665 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserver__ctor_m579763476_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Double,System.Double,System.Double>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserver_Run_m828724379_gshared (SelectManyEnumerableObserver_t1450222884 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_Run_m828724379(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserver_t1450222884 *, const MethodInfo*))SelectManyEnumerableObserver_Run_m828724379_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Double,System.Double,System.Double>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserver_OnNext_m982203162_gshared (SelectManyEnumerableObserver_t1450222884 * __this, double ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnNext_m982203162(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserver_t1450222884 *, double, const MethodInfo*))SelectManyEnumerableObserver_OnNext_m982203162_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Double,System.Double,System.Double>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserver_OnError_m3119538692_gshared (SelectManyEnumerableObserver_t1450222884 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnError_m3119538692(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserver_t1450222884 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserver_OnError_m3119538692_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Double,System.Double,System.Double>::OnCompleted()
extern "C"  void SelectManyEnumerableObserver_OnCompleted_m3026123607_gshared (SelectManyEnumerableObserver_t1450222884 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserver_OnCompleted_m3026123607(__this, method) ((  void (*) (SelectManyEnumerableObserver_t1450222884 *, const MethodInfo*))SelectManyEnumerableObserver_OnCompleted_m3026123607_gshared)(__this, method)
