﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateCharacterDataResult
struct UpdateCharacterDataResult_t1101824215;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UpdateCharacterDataResult::.ctor()
extern "C"  void UpdateCharacterDataResult__ctor_m4235875634 (UpdateCharacterDataResult_t1101824215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.UpdateCharacterDataResult::get_DataVersion()
extern "C"  uint32_t UpdateCharacterDataResult_get_DataVersion_m25832442 (UpdateCharacterDataResult_t1101824215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateCharacterDataResult::set_DataVersion(System.UInt32)
extern "C"  void UpdateCharacterDataResult_set_DataVersion_m3028528601 (UpdateCharacterDataResult_t1101824215 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
