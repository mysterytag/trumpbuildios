﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TakeWhileObservable`1<System.Object>
struct TakeWhileObservable_1_t3525448012;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeWhileObservable`1/TakeWhile<System.Object>
struct  TakeWhile_t3591352755  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.TakeWhileObservable`1<T> UniRx.Operators.TakeWhileObservable`1/TakeWhile::parent
	TakeWhileObservable_1_t3525448012 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(TakeWhile_t3591352755, ___parent_2)); }
	inline TakeWhileObservable_1_t3525448012 * get_parent_2() const { return ___parent_2; }
	inline TakeWhileObservable_1_t3525448012 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(TakeWhileObservable_1_t3525448012 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
