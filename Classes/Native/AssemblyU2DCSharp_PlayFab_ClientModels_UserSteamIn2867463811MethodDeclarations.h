﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserSteamInfo
struct UserSteamInfo_t2867463811;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen2703348181.h"
#include "mscorlib_System_Nullable_1_gen2484541604.h"

// System.Void PlayFab.ClientModels.UserSteamInfo::.ctor()
extern "C"  void UserSteamInfo__ctor_m2703729158 (UserSteamInfo_t2867463811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserSteamInfo::get_SteamId()
extern "C"  String_t* UserSteamInfo_get_SteamId_m614593347 (UserSteamInfo_t2867463811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserSteamInfo::set_SteamId(System.String)
extern "C"  void UserSteamInfo_set_SteamId_m2872239920 (UserSteamInfo_t2867463811 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserSteamInfo::get_SteamCountry()
extern "C"  String_t* UserSteamInfo_get_SteamCountry_m379284368 (UserSteamInfo_t2867463811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserSteamInfo::set_SteamCountry(System.String)
extern "C"  void UserSteamInfo_set_SteamCountry_m3626326401 (UserSteamInfo_t2867463811 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.Currency> PlayFab.ClientModels.UserSteamInfo::get_SteamCurrency()
extern "C"  Nullable_1_t2703348181  UserSteamInfo_get_SteamCurrency_m3711006361 (UserSteamInfo_t2867463811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserSteamInfo::set_SteamCurrency(System.Nullable`1<PlayFab.ClientModels.Currency>)
extern "C"  void UserSteamInfo_set_SteamCurrency_m3422548814 (UserSteamInfo_t2867463811 * __this, Nullable_1_t2703348181  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus> PlayFab.ClientModels.UserSteamInfo::get_SteamActivationStatus()
extern "C"  Nullable_1_t2484541604  UserSteamInfo_get_SteamActivationStatus_m2629906737 (UserSteamInfo_t2867463811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserSteamInfo::set_SteamActivationStatus(System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>)
extern "C"  void UserSteamInfo_set_SteamActivationStatus_m2472578754 (UserSteamInfo_t2867463811 * __this, Nullable_1_t2484541604  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
