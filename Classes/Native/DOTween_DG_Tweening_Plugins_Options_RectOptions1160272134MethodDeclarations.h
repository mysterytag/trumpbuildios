﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct RectOptions_t1160272134;
struct RectOptions_t1160272134_marshaled_pinvoke;

extern "C" void RectOptions_t1160272134_marshal_pinvoke(const RectOptions_t1160272134& unmarshaled, RectOptions_t1160272134_marshaled_pinvoke& marshaled);
extern "C" void RectOptions_t1160272134_marshal_pinvoke_back(const RectOptions_t1160272134_marshaled_pinvoke& marshaled, RectOptions_t1160272134& unmarshaled);
extern "C" void RectOptions_t1160272134_marshal_pinvoke_cleanup(RectOptions_t1160272134_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RectOptions_t1160272134;
struct RectOptions_t1160272134_marshaled_com;

extern "C" void RectOptions_t1160272134_marshal_com(const RectOptions_t1160272134& unmarshaled, RectOptions_t1160272134_marshaled_com& marshaled);
extern "C" void RectOptions_t1160272134_marshal_com_back(const RectOptions_t1160272134_marshaled_com& marshaled, RectOptions_t1160272134& unmarshaled);
extern "C" void RectOptions_t1160272134_marshal_com_cleanup(RectOptions_t1160272134_marshaled_com& marshaled);
