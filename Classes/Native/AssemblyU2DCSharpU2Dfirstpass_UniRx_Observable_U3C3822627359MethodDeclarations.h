﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey47`2<System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey47_2_t3822627359;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey47`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey47_2__ctor_m1152289233_gshared (U3CFromAsyncPatternU3Ec__AnonStorey47_2_t3822627359 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey47_2__ctor_m1152289233(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey47_2_t3822627359 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey47_2__ctor_m1152289233_gshared)(__this, method)
// UniRx.Unit UniRx.Observable/<FromAsyncPattern>c__AnonStorey47`2<System.Object,System.Object>::<>m__45(System.IAsyncResult)
extern "C"  Unit_t2558286038  U3CFromAsyncPatternU3Ec__AnonStorey47_2_U3CU3Em__45_m630157759_gshared (U3CFromAsyncPatternU3Ec__AnonStorey47_2_t3822627359 * __this, Il2CppObject * ___iar0, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey47_2_U3CU3Em__45_m630157759(__this, ___iar0, method) ((  Unit_t2558286038  (*) (U3CFromAsyncPatternU3Ec__AnonStorey47_2_t3822627359 *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey47_2_U3CU3Em__45_m630157759_gshared)(__this, ___iar0, method)
