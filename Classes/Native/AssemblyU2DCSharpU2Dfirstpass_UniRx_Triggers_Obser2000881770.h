﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.Collision>
struct Subject_1_t3057572635;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableCollisionTrigger
struct  ObservableCollisionTrigger_t2000881770  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::onCollisionEnter
	Subject_1_t3057572635 * ___onCollisionEnter_8;
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::onCollisionExit
	Subject_1_t3057572635 * ___onCollisionExit_9;
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::onCollisionStay
	Subject_1_t3057572635 * ___onCollisionStay_10;

public:
	inline static int32_t get_offset_of_onCollisionEnter_8() { return static_cast<int32_t>(offsetof(ObservableCollisionTrigger_t2000881770, ___onCollisionEnter_8)); }
	inline Subject_1_t3057572635 * get_onCollisionEnter_8() const { return ___onCollisionEnter_8; }
	inline Subject_1_t3057572635 ** get_address_of_onCollisionEnter_8() { return &___onCollisionEnter_8; }
	inline void set_onCollisionEnter_8(Subject_1_t3057572635 * value)
	{
		___onCollisionEnter_8 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionEnter_8, value);
	}

	inline static int32_t get_offset_of_onCollisionExit_9() { return static_cast<int32_t>(offsetof(ObservableCollisionTrigger_t2000881770, ___onCollisionExit_9)); }
	inline Subject_1_t3057572635 * get_onCollisionExit_9() const { return ___onCollisionExit_9; }
	inline Subject_1_t3057572635 ** get_address_of_onCollisionExit_9() { return &___onCollisionExit_9; }
	inline void set_onCollisionExit_9(Subject_1_t3057572635 * value)
	{
		___onCollisionExit_9 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionExit_9, value);
	}

	inline static int32_t get_offset_of_onCollisionStay_10() { return static_cast<int32_t>(offsetof(ObservableCollisionTrigger_t2000881770, ___onCollisionStay_10)); }
	inline Subject_1_t3057572635 * get_onCollisionStay_10() const { return ___onCollisionStay_10; }
	inline Subject_1_t3057572635 ** get_address_of_onCollisionStay_10() { return &___onCollisionStay_10; }
	inline void set_onCollisionStay_10(Subject_1_t3057572635 * value)
	{
		___onCollisionStay_10 = value;
		Il2CppCodeGenWriteBarrier(&___onCollisionStay_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
