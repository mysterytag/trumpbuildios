﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UnityEngine.Collision2D>::.ctor()
#define Subject_1__ctor_m1429983527(__this, method) ((  void (*) (Subject_1_t2390844653 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Collision2D>::get_HasObservers()
#define Subject_1_get_HasObservers_m3981933317(__this, method) ((  bool (*) (Subject_1_t2390844653 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision2D>::OnCompleted()
#define Subject_1_OnCompleted_m3008973681(__this, method) ((  void (*) (Subject_1_t2390844653 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision2D>::OnError(System.Exception)
#define Subject_1_OnError_m1027693342(__this, ___error0, method) ((  void (*) (Subject_1_t2390844653 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision2D>::OnNext(T)
#define Subject_1_OnNext_m2655337999(__this, ___value0, method) ((  void (*) (Subject_1_t2390844653 *, Collision2D_t452810033 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Collision2D>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m252103654(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2390844653 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision2D>::Dispose()
#define Subject_1_Dispose_m4017885092(__this, method) ((  void (*) (Subject_1_t2390844653 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision2D>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m3237074221(__this, method) ((  void (*) (Subject_1_t2390844653 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Collision2D>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2614038716(__this, method) ((  bool (*) (Subject_1_t2390844653 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
