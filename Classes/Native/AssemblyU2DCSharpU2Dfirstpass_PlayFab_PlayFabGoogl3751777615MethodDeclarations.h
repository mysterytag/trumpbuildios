﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady
struct GCMRegisterReady_t3751777615;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady::.ctor(System.Object,System.IntPtr)
extern "C"  void GCMRegisterReady__ctor_m2105364913 (GCMRegisterReady_t3751777615 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady::Invoke(System.Boolean)
extern "C"  void GCMRegisterReady_Invoke_m292769666 (GCMRegisterReady_t3751777615 * __this, bool ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GCMRegisterReady_t3751777615(Il2CppObject* delegate, bool ___status0);
// System.IAsyncResult PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GCMRegisterReady_BeginInvoke_m3567146159 (GCMRegisterReady_t3751777615 * __this, bool ___status0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady::EndInvoke(System.IAsyncResult)
extern "C"  void GCMRegisterReady_EndInvoke_m223822913 (GCMRegisterReady_t3751777615 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
