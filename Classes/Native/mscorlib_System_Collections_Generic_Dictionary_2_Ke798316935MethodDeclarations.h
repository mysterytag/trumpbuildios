﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>
struct KeyCollection_t798316935;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.ArrayMetadata>
struct Dictionary_2_t2770008951;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2537036892.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m699230659_gshared (KeyCollection_t798316935 * __this, Dictionary_2_t2770008951 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m699230659(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t798316935 *, Dictionary_2_t2770008951 *, const MethodInfo*))KeyCollection__ctor_m699230659_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m35103155_gshared (KeyCollection_t798316935 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m35103155(__this, ___item0, method) ((  void (*) (KeyCollection_t798316935 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m35103155_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m187733994_gshared (KeyCollection_t798316935 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m187733994(__this, method) ((  void (*) (KeyCollection_t798316935 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m187733994_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2948089655_gshared (KeyCollection_t798316935 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2948089655(__this, ___item0, method) ((  bool (*) (KeyCollection_t798316935 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2948089655_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1728780892_gshared (KeyCollection_t798316935 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1728780892(__this, ___item0, method) ((  bool (*) (KeyCollection_t798316935 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1728780892_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1858890406_gshared (KeyCollection_t798316935 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1858890406(__this, method) ((  Il2CppObject* (*) (KeyCollection_t798316935 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1858890406_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3492539740_gshared (KeyCollection_t798316935 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3492539740(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t798316935 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3492539740_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m524617175_gshared (KeyCollection_t798316935 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m524617175(__this, method) ((  Il2CppObject * (*) (KeyCollection_t798316935 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m524617175_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1316801752_gshared (KeyCollection_t798316935 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1316801752(__this, method) ((  bool (*) (KeyCollection_t798316935 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1316801752_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2467857290_gshared (KeyCollection_t798316935 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2467857290(__this, method) ((  bool (*) (KeyCollection_t798316935 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2467857290_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m359588022_gshared (KeyCollection_t798316935 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m359588022(__this, method) ((  Il2CppObject * (*) (KeyCollection_t798316935 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m359588022_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4267351160_gshared (KeyCollection_t798316935 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4267351160(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t798316935 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4267351160_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::GetEnumerator()
extern "C"  Enumerator_t2537036892  KeyCollection_GetEnumerator_m3683152731_gshared (KeyCollection_t798316935 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3683152731(__this, method) ((  Enumerator_t2537036892  (*) (KeyCollection_t798316935 *, const MethodInfo*))KeyCollection_GetEnumerator_m3683152731_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,LitJson.ArrayMetadata>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m4157658448_gshared (KeyCollection_t798316935 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m4157658448(__this, method) ((  int32_t (*) (KeyCollection_t798316935 *, const MethodInfo*))KeyCollection_get_Count_m4157658448_gshared)(__this, method)
