﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayfabConnector
struct PlayfabConnector_t194421338;
// OnePF.Purchase
struct Purchase_t160195573;
// GlobalStat/ReceiptForVerify
struct ReceiptForVerify_t3763828586;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573.h"
#include "AssemblyU2DCSharp_GlobalStat_ReceiptForVerify3763828586.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"

// System.Void PlayfabConnector::.ctor()
extern "C"  void PlayfabConnector__ctor_m3773204865 (PlayfabConnector_t194421338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayfabConnector PlayfabConnector::get_Instance()
extern "C"  PlayfabConnector_t194421338 * PlayfabConnector_get_Instance_m475474468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::Login()
extern "C"  void PlayfabConnector_Login_m664905672 (PlayfabConnector_t194421338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::VerifyReceipt(OnePF.Purchase)
extern "C"  void PlayfabConnector_VerifyReceipt_m802151915 (Il2CppObject * __this /* static, unused */, Purchase_t160195573 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::Update()
extern "C"  void PlayfabConnector_Update_m2732095916 (PlayfabConnector_t194421338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::Verify(GlobalStat/ReceiptForVerify)
extern "C"  void PlayfabConnector_Verify_m3810551188 (PlayfabConnector_t194421338 * __this, ReceiptForVerify_t3763828586 * ___receiptForVerify0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::PurchaseVerified(GlobalStat/ReceiptForVerify)
extern "C"  void PlayfabConnector_PurchaseVerified_m1418862500 (PlayfabConnector_t194421338 * __this, ReceiptForVerify_t3763828586 * ___receipt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayfabConnector::IsIphone()
extern "C"  bool PlayfabConnector_IsIphone_m62623750 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayfabConnector::IsAndroid()
extern "C"  bool PlayfabConnector_IsAndroid_m862389776 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::<Login>m__11E(PlayFab.ClientModels.LoginResult)
extern "C"  void PlayfabConnector_U3CLoginU3Em__11E_m4264004864 (PlayfabConnector_t194421338 * __this, LoginResult_t2117559510 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::<Login>m__11F(PlayFab.PlayFabError)
extern "C"  void PlayfabConnector_U3CLoginU3Em__11F_m2748809497 (PlayfabConnector_t194421338 * __this, PlayFabError_t750598646 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::<Login>m__120(PlayFab.ClientModels.LoginResult)
extern "C"  void PlayfabConnector_U3CLoginU3Em__120_m43977354 (PlayfabConnector_t194421338 * __this, LoginResult_t2117559510 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayfabConnector::<Login>m__121(PlayFab.PlayFabError)
extern "C"  void PlayfabConnector_U3CLoginU3Em__121_m2311552419 (PlayfabConnector_t194421338 * __this, PlayFabError_t750598646 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
