﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where<System.Int64>
struct Where_t689804566;
// UniRx.Operators.WhereObservable`1<System.Int64>
struct WhereObservable_1_t4045974831;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where<System.Int64>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where__ctor_m1902301417_gshared (Where_t689804566 * __this, WhereObservable_1_t4045974831 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where__ctor_m1902301417(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where_t689804566 *, WhereObservable_1_t4045974831 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where__ctor_m1902301417_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Int64>::OnNext(T)
extern "C"  void Where_OnNext_m1916666703_gshared (Where_t689804566 * __this, int64_t ___value0, const MethodInfo* method);
#define Where_OnNext_m1916666703(__this, ___value0, method) ((  void (*) (Where_t689804566 *, int64_t, const MethodInfo*))Where_OnNext_m1916666703_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Int64>::OnError(System.Exception)
extern "C"  void Where_OnError_m59220062_gshared (Where_t689804566 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where_OnError_m59220062(__this, ___error0, method) ((  void (*) (Where_t689804566 *, Exception_t1967233988 *, const MethodInfo*))Where_OnError_m59220062_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Int64>::OnCompleted()
extern "C"  void Where_OnCompleted_m2800578737_gshared (Where_t689804566 * __this, const MethodInfo* method);
#define Where_OnCompleted_m2800578737(__this, method) ((  void (*) (Where_t689804566 *, const MethodInfo*))Where_OnCompleted_m2800578737_gshared)(__this, method)
