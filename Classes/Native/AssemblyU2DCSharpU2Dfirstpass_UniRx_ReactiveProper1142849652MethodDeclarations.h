﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<System.Double>
struct ReactiveProperty_1_t1142849652;
// UniRx.IObservable`1<System.Double>
struct IObservable_1_t293314978;
// System.Collections.Generic.IEqualityComparer`1<System.Double>
struct IEqualityComparer_1_t2858783265;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ReactiveProperty`1<System.Double>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m3586573700_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3586573700(__this, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, const MethodInfo*))ReactiveProperty_1__ctor_m3586573700_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m3809603674_gshared (ReactiveProperty_1_t1142849652 * __this, double ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3809603674(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, double, const MethodInfo*))ReactiveProperty_1__ctor_m3809603674_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m4197642339_gshared (ReactiveProperty_1_t1142849652 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m4197642339(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m4197642339_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m960002363_gshared (ReactiveProperty_1_t1142849652 * __this, Il2CppObject* ___source0, double ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m960002363(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, Il2CppObject*, double, const MethodInfo*))ReactiveProperty_1__ctor_m960002363_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m3327506089_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m3327506089(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m3327506089_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Double>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m3271061201_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m3271061201(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t1142849652 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m3271061201_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<System.Double>::get_Value()
extern "C"  double ReactiveProperty_1_get_Value_m1320871017_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m1320871017(__this, method) ((  double (*) (ReactiveProperty_1_t1142849652 *, const MethodInfo*))ReactiveProperty_1_get_Value_m1320871017_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3185989544_gshared (ReactiveProperty_1_t1142849652 * __this, double ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m3185989544(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, double, const MethodInfo*))ReactiveProperty_1_set_Value_m3185989544_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2682312975_gshared (ReactiveProperty_1_t1142849652 * __this, double ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m2682312975(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, double, const MethodInfo*))ReactiveProperty_1_SetValue_m2682312975_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m2425444498_gshared (ReactiveProperty_1_t1142849652 * __this, double ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m2425444498(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, double, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m2425444498_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<System.Double>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m3002610265_gshared (ReactiveProperty_1_t1142849652 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m3002610265(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t1142849652 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m3002610265_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m2031837377_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m2031837377(__this, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, const MethodInfo*))ReactiveProperty_1_Dispose_m2031837377_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Double>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m4135923832_gshared (ReactiveProperty_1_t1142849652 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m4135923832(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t1142849652 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m4135923832_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<System.Double>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m2259213097_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m2259213097(__this, method) ((  String_t* (*) (ReactiveProperty_1_t1142849652 *, const MethodInfo*))ReactiveProperty_1_ToString_m2259213097_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<System.Double>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3187226823_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3187226823(__this, method) ((  bool (*) (ReactiveProperty_1_t1142849652 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3187226823_gshared)(__this, method)
