﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE3
struct U3CPostInjectU3Ec__AnonStoreyE3_t980490756;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE3::.ctor()
extern "C"  void U3CPostInjectU3Ec__AnonStoreyE3__ctor_m3618025979 (U3CPostInjectU3Ec__AnonStoreyE3_t980490756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE3::<>m__114(System.Int64)
extern "C"  bool U3CPostInjectU3Ec__AnonStoreyE3_U3CU3Em__114_m3743081720 (U3CPostInjectU3Ec__AnonStoreyE3_t980490756 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE3::<>m__115(System.Int64)
extern "C"  void U3CPostInjectU3Ec__AnonStoreyE3_U3CU3Em__115_m4286167429 (U3CPostInjectU3Ec__AnonStoreyE3_t980490756 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
