﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeObservable`1/Take<System.Object>
struct Take_t531252872;
// UniRx.Operators.TakeObservable`1<System.Object>
struct TakeObservable_1_t3078819489;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeObservable`1/Take<System.Object>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take__ctor_m904164971_gshared (Take_t531252872 * __this, TakeObservable_1_t3078819489 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Take__ctor_m904164971(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Take_t531252872 *, TakeObservable_1_t3078819489 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Take__ctor_m904164971_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Object>::OnNext(T)
extern "C"  void Take_OnNext_m3969312959_gshared (Take_t531252872 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Take_OnNext_m3969312959(__this, ___value0, method) ((  void (*) (Take_t531252872 *, Il2CppObject *, const MethodInfo*))Take_OnNext_m3969312959_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Object>::OnError(System.Exception)
extern "C"  void Take_OnError_m197633486_gshared (Take_t531252872 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Take_OnError_m197633486(__this, ___error0, method) ((  void (*) (Take_t531252872 *, Exception_t1967233988 *, const MethodInfo*))Take_OnError_m197633486_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Object>::OnCompleted()
extern "C"  void Take_OnCompleted_m3598065185_gshared (Take_t531252872 * __this, const MethodInfo* method);
#define Take_OnCompleted_m3598065185(__this, method) ((  void (*) (Take_t531252872 *, const MethodInfo*))Take_OnCompleted_m3598065185_gshared)(__this, method)
