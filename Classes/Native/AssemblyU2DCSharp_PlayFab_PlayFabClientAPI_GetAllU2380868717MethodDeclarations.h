﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetAllUsersCharactersRequestCallback
struct GetAllUsersCharactersRequestCallback_t2380868717;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ListUsersCharactersRequest
struct ListUsersCharactersRequest_t3884929547;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ListUsersCh3884929547.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetAllUsersCharactersRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetAllUsersCharactersRequestCallback__ctor_m2607456988 (GetAllUsersCharactersRequestCallback_t2380868717 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetAllUsersCharactersRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ListUsersCharactersRequest,System.Object)
extern "C"  void GetAllUsersCharactersRequestCallback_Invoke_m4108798956 (GetAllUsersCharactersRequestCallback_t2380868717 * __this, String_t* ___urlPath0, int32_t ___callId1, ListUsersCharactersRequest_t3884929547 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetAllUsersCharactersRequestCallback_t2380868717(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ListUsersCharactersRequest_t3884929547 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetAllUsersCharactersRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ListUsersCharactersRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetAllUsersCharactersRequestCallback_BeginInvoke_m1953406323 (GetAllUsersCharactersRequestCallback_t2380868717 * __this, String_t* ___urlPath0, int32_t ___callId1, ListUsersCharactersRequest_t3884929547 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetAllUsersCharactersRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetAllUsersCharactersRequestCallback_EndInvoke_m3227116268 (GetAllUsersCharactersRequestCallback_t2380868717 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
