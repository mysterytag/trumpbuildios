﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C<System.Object>
struct U3CCreateTimerU3Ec__AnonStorey5C_t2790700317;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C<System.Object>::.ctor()
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5C__ctor_m1565512555_gshared (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * __this, const MethodInfo* method);
#define U3CCreateTimerU3Ec__AnonStorey5C__ctor_m1565512555(__this, method) ((  void (*) (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 *, const MethodInfo*))U3CCreateTimerU3Ec__AnonStorey5C__ctor_m1565512555_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C<System.Object>::<>m__74()
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5C_U3CU3Em__74_m3527234321_gshared (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 * __this, const MethodInfo* method);
#define U3CCreateTimerU3Ec__AnonStorey5C_U3CU3Em__74_m3527234321(__this, method) ((  void (*) (U3CCreateTimerU3Ec__AnonStorey5C_t2790700317 *, const MethodInfo*))U3CCreateTimerU3Ec__AnonStorey5C_U3CU3Em__74_m3527234321_gshared)(__this, method)
