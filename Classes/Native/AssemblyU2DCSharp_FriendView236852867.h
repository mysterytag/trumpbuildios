﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.UI.Text
struct Text_t3286458198;
// System.Action
struct Action_t437523947;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell776419755.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendView
struct  FriendView_t236852867  : public TableViewCell_t776419755
{
public:
	// UnityEngine.UI.Image FriendView::icon
	Image_t3354615620 * ___icon_7;
	// UnityEngine.UI.Text FriendView::nameText
	Text_t3286458198 * ___nameText_8;
	// System.Action FriendView::press
	Action_t437523947 * ___press_9;
	// UnityEngine.Coroutine FriendView::loading
	Coroutine_t2246592261 * ___loading_10;
	// System.String FriendView::imgUrl
	String_t* ___imgUrl_11;

public:
	inline static int32_t get_offset_of_icon_7() { return static_cast<int32_t>(offsetof(FriendView_t236852867, ___icon_7)); }
	inline Image_t3354615620 * get_icon_7() const { return ___icon_7; }
	inline Image_t3354615620 ** get_address_of_icon_7() { return &___icon_7; }
	inline void set_icon_7(Image_t3354615620 * value)
	{
		___icon_7 = value;
		Il2CppCodeGenWriteBarrier(&___icon_7, value);
	}

	inline static int32_t get_offset_of_nameText_8() { return static_cast<int32_t>(offsetof(FriendView_t236852867, ___nameText_8)); }
	inline Text_t3286458198 * get_nameText_8() const { return ___nameText_8; }
	inline Text_t3286458198 ** get_address_of_nameText_8() { return &___nameText_8; }
	inline void set_nameText_8(Text_t3286458198 * value)
	{
		___nameText_8 = value;
		Il2CppCodeGenWriteBarrier(&___nameText_8, value);
	}

	inline static int32_t get_offset_of_press_9() { return static_cast<int32_t>(offsetof(FriendView_t236852867, ___press_9)); }
	inline Action_t437523947 * get_press_9() const { return ___press_9; }
	inline Action_t437523947 ** get_address_of_press_9() { return &___press_9; }
	inline void set_press_9(Action_t437523947 * value)
	{
		___press_9 = value;
		Il2CppCodeGenWriteBarrier(&___press_9, value);
	}

	inline static int32_t get_offset_of_loading_10() { return static_cast<int32_t>(offsetof(FriendView_t236852867, ___loading_10)); }
	inline Coroutine_t2246592261 * get_loading_10() const { return ___loading_10; }
	inline Coroutine_t2246592261 ** get_address_of_loading_10() { return &___loading_10; }
	inline void set_loading_10(Coroutine_t2246592261 * value)
	{
		___loading_10 = value;
		Il2CppCodeGenWriteBarrier(&___loading_10, value);
	}

	inline static int32_t get_offset_of_imgUrl_11() { return static_cast<int32_t>(offsetof(FriendView_t236852867, ___imgUrl_11)); }
	inline String_t* get_imgUrl_11() const { return ___imgUrl_11; }
	inline String_t** get_address_of_imgUrl_11() { return &___imgUrl_11; }
	inline void set_imgUrl_11(String_t* value)
	{
		___imgUrl_11 = value;
		Il2CppCodeGenWriteBarrier(&___imgUrl_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
