﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindingCondition
struct BindingCondition_t1528286123;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Zenject.BindingCondition::.ctor(System.Object,System.IntPtr)
extern "C"  void BindingCondition__ctor_m578775676 (BindingCondition_t1528286123 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingCondition::Invoke(Zenject.InjectContext)
extern "C"  bool BindingCondition_Invoke_m4026405961 (BindingCondition_t1528286123 * __this, InjectContext_t3456483891 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" bool pinvoke_delegate_wrapper_BindingCondition_t1528286123(Il2CppObject* delegate, InjectContext_t3456483891 * ___c0);
// System.IAsyncResult Zenject.BindingCondition::BeginInvoke(Zenject.InjectContext,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * BindingCondition_BeginInvoke_m1013716032 (BindingCondition_t1528286123 * __this, InjectContext_t3456483891 * ___c0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingCondition::EndInvoke(System.IAsyncResult)
extern "C"  bool BindingCondition_EndInvoke_m1550433600 (BindingCondition_t1528286123 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
