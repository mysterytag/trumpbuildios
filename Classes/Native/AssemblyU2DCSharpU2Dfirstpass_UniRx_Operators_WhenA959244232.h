﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1372125713.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhenAllObservable`1/WhenAll<System.Object>
struct  WhenAll_t959244232  : public OperatorObserverBase_2_t1372125713
{
public:
	// UniRx.IObservable`1<T>[] UniRx.Operators.WhenAllObservable`1/WhenAll::sources
	IObservable_1U5BU5D_t2205425841* ___sources_2;
	// System.Object UniRx.Operators.WhenAllObservable`1/WhenAll::gate
	Il2CppObject * ___gate_3;
	// System.Int32 UniRx.Operators.WhenAllObservable`1/WhenAll::completedCount
	int32_t ___completedCount_4;
	// System.Int32 UniRx.Operators.WhenAllObservable`1/WhenAll::length
	int32_t ___length_5;
	// T[] UniRx.Operators.WhenAllObservable`1/WhenAll::values
	ObjectU5BU5D_t11523773* ___values_6;

public:
	inline static int32_t get_offset_of_sources_2() { return static_cast<int32_t>(offsetof(WhenAll_t959244232, ___sources_2)); }
	inline IObservable_1U5BU5D_t2205425841* get_sources_2() const { return ___sources_2; }
	inline IObservable_1U5BU5D_t2205425841** get_address_of_sources_2() { return &___sources_2; }
	inline void set_sources_2(IObservable_1U5BU5D_t2205425841* value)
	{
		___sources_2 = value;
		Il2CppCodeGenWriteBarrier(&___sources_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(WhenAll_t959244232, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_completedCount_4() { return static_cast<int32_t>(offsetof(WhenAll_t959244232, ___completedCount_4)); }
	inline int32_t get_completedCount_4() const { return ___completedCount_4; }
	inline int32_t* get_address_of_completedCount_4() { return &___completedCount_4; }
	inline void set_completedCount_4(int32_t value)
	{
		___completedCount_4 = value;
	}

	inline static int32_t get_offset_of_length_5() { return static_cast<int32_t>(offsetof(WhenAll_t959244232, ___length_5)); }
	inline int32_t get_length_5() const { return ___length_5; }
	inline int32_t* get_address_of_length_5() { return &___length_5; }
	inline void set_length_5(int32_t value)
	{
		___length_5 = value;
	}

	inline static int32_t get_offset_of_values_6() { return static_cast<int32_t>(offsetof(WhenAll_t959244232, ___values_6)); }
	inline ObjectU5BU5D_t11523773* get_values_6() const { return ___values_6; }
	inline ObjectU5BU5D_t11523773** get_address_of_values_6() { return &___values_6; }
	inline void set_values_6(ObjectU5BU5D_t11523773* value)
	{
		___values_6 = value;
		Il2CppCodeGenWriteBarrier(&___values_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
