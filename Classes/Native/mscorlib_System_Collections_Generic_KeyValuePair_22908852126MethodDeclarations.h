﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21493839180MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2418137366(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2908852126 *, String_t*, KeyValuePair_2_t1782622924 , const MethodInfo*))KeyValuePair_2__ctor_m828809412_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m3069689271(__this, method) ((  String_t* (*) (KeyValuePair_2_t2908852126 *, const MethodInfo*))KeyValuePair_2_get_Key_m4107894436_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1281600339(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2908852126 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m967855845_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m662429352(__this, method) ((  KeyValuePair_2_t1782622924  (*) (KeyValuePair_2_t2908852126 *, const MethodInfo*))KeyValuePair_2_get_Value_m4235623012_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3313704403(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2908852126 *, KeyValuePair_2_t1782622924 , const MethodInfo*))KeyValuePair_2_set_Value_m768583781_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m2357919535(__this, method) ((  String_t* (*) (KeyValuePair_2_t2908852126 *, const MethodInfo*))KeyValuePair_2_ToString_m1076548957_gshared)(__this, method)
