﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t2219490770;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3827726924_gshared (DOGetter_1_t2219490770 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define DOGetter_1__ctor_m3827726924(__this, ___object0, ___method1, method) ((  void (*) (DOGetter_1_t2219490770 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m3827726924_gshared)(__this, ___object0, ___method1, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t3525329789  DOGetter_1_Invoke_m1631383013_gshared (DOGetter_1_t2219490770 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m1631383013(__this, method) ((  Vector3_t3525329789  (*) (DOGetter_1_t2219490770 *, const MethodInfo*))DOGetter_1_Invoke_m1631383013_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m881213085_gshared (DOGetter_1_t2219490770 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m881213085(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DOGetter_1_t2219490770 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))DOGetter_1_BeginInvoke_m881213085_gshared)(__this, ___callback0, ___object1, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t3525329789  DOGetter_1_EndInvoke_m370092379_gshared (DOGetter_1_t2219490770 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m370092379(__this, ___result0, method) ((  Vector3_t3525329789  (*) (DOGetter_1_t2219490770 *, Il2CppObject *, const MethodInfo*))DOGetter_1_EndInvoke_m370092379_gshared)(__this, ___result0, method)
