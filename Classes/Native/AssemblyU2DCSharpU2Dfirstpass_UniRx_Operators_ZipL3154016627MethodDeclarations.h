﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`4<System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_4_t3154016627;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_4_t1514349918;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ZipLatestObservable`4<System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.Operators.ZipLatestFunc`4<T1,T2,T3,TR>)
extern "C"  void ZipLatestObservable_4__ctor_m529988579_gshared (ZipLatestObservable_4_t3154016627 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, ZipLatestFunc_4_t1514349918 * ___resultSelector3, const MethodInfo* method);
#define ZipLatestObservable_4__ctor_m529988579(__this, ___source10, ___source21, ___source32, ___resultSelector3, method) ((  void (*) (ZipLatestObservable_4_t3154016627 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, ZipLatestFunc_4_t1514349918 *, const MethodInfo*))ZipLatestObservable_4__ctor_m529988579_gshared)(__this, ___source10, ___source21, ___source32, ___resultSelector3, method)
// System.IDisposable UniRx.Operators.ZipLatestObservable`4<System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * ZipLatestObservable_4_SubscribeCore_m2305518723_gshared (ZipLatestObservable_4_t3154016627 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ZipLatestObservable_4_SubscribeCore_m2305518723(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ZipLatestObservable_4_t3154016627 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipLatestObservable_4_SubscribeCore_m2305518723_gshared)(__this, ___observer0, ___cancel1, method)
