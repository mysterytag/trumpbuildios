﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4000251768MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m2079931233(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m304010443(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, DisposableInfo_t1284166222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2841080159(__this, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2608600562(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, int32_t, DisposableInfo_t1284166222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2803495180(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t152344274 *, DisposableInfo_t1284166222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m482453432(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m4207028126(__this, ___index0, method) ((  DisposableInfo_t1284166222 * (*) (ReadOnlyCollection_1_t152344274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m413835849(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, int32_t, DisposableInfo_t1284166222 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m562743043(__this, method) ((  bool (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3896808336(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3772512031(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2314395646(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t152344274 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3747865246(__this, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2909622658(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t152344274 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1151788886(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t152344274 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1072199369(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m753196991(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3459625945(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3828579994(__this, method) ((  bool (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2235384012(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3592997489(__this, method) ((  bool (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2648121960(__this, method) ((  bool (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1785999187(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t152344274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m205395040(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::Contains(T)
#define ReadOnlyCollection_1_Contains_m4035476049(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t152344274 *, DisposableInfo_t1284166222 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m3850901755(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t152344274 *, DisposableInfoU5BU5D_t1006847163*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m2709153704(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m3680932615(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t152344274 *, DisposableInfo_t1284166222 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2257068884(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t152344274 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Zenject.DisposableManager/DisposableInfo>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m1150238814(__this, ___index0, method) ((  DisposableInfo_t1284166222 * (*) (ReadOnlyCollection_1_t152344274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
