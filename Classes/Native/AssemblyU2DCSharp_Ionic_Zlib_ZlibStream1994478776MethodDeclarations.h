﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.ZlibStream
struct ZlibStream_t1994478776;
// System.IO.Stream
struct Stream_t219029575;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionMode1632830838.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_FlushType3984958891.h"
#include "mscorlib_System_IO_SeekOrigin3506139269.h"
#include "mscorlib_System_String968488902.h"

// System.Void Ionic.Zlib.ZlibStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode)
extern "C"  void ZlibStream__ctor_m3032282900 (ZlibStream_t1994478776 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,Ionic.Zlib.CompressionLevel)
extern "C"  void ZlibStream__ctor_m1168036299 (ZlibStream_t1994478776 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, int32_t ___level2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,System.Boolean)
extern "C"  void ZlibStream__ctor_m595317225 (ZlibStream_t1994478776 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, bool ___leaveOpen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,Ionic.Zlib.CompressionLevel,System.Boolean)
extern "C"  void ZlibStream__ctor_m179327570 (ZlibStream_t1994478776 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, int32_t ___level2, bool ___leaveOpen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.FlushType Ionic.Zlib.ZlibStream::get_FlushMode()
extern "C"  int32_t ZlibStream_get_FlushMode_m998118747 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::set_FlushMode(Ionic.Zlib.FlushType)
extern "C"  void ZlibStream_set_FlushMode_m257632170 (ZlibStream_t1994478776 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibStream::get_BufferSize()
extern "C"  int32_t ZlibStream_get_BufferSize_m3278665431 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::set_BufferSize(System.Int32)
extern "C"  void ZlibStream_set_BufferSize_m1633282794 (ZlibStream_t1994478776 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ZlibStream::get_TotalIn()
extern "C"  int64_t ZlibStream_get_TotalIn_m3280154550 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ZlibStream::get_TotalOut()
extern "C"  int64_t ZlibStream_get_TotalOut_m2906365951 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::Dispose(System.Boolean)
extern "C"  void ZlibStream_Dispose_m264669789 (ZlibStream_t1994478776 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ZlibStream::get_CanRead()
extern "C"  bool ZlibStream_get_CanRead_m2947889432 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ZlibStream::get_CanSeek()
extern "C"  bool ZlibStream_get_CanSeek_m2976644474 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ZlibStream::get_CanWrite()
extern "C"  bool ZlibStream_get_CanWrite_m1712912703 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::Flush()
extern "C"  void ZlibStream_Flush_m771455115 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ZlibStream::get_Length()
extern "C"  int64_t ZlibStream_get_Length_m2060843899 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ZlibStream::get_Position()
extern "C"  int64_t ZlibStream_get_Position_m712564734 (ZlibStream_t1994478776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::set_Position(System.Int64)
extern "C"  void ZlibStream_set_Position_m3381410547 (ZlibStream_t1994478776 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZlibStream_Read_m1931738886 (ZlibStream_t1994478776 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ZlibStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t ZlibStream_Seek_m26934153 (ZlibStream_t1994478776 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::SetLength(System.Int64)
extern "C"  void ZlibStream_SetLength_m811107777 (ZlibStream_t1994478776 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ZlibStream_Write_m2877448835 (ZlibStream_t1994478776 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.ZlibStream::CompressString(System.String)
extern "C"  ByteU5BU5D_t58506160* ZlibStream_CompressString_m3086241822 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.ZlibStream::CompressBuffer(System.Byte[])
extern "C"  ByteU5BU5D_t58506160* ZlibStream_CompressBuffer_m945797334 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ionic.Zlib.ZlibStream::UncompressString(System.Byte[])
extern "C"  String_t* ZlibStream_UncompressString_m3057668965 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___compressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.ZlibStream::UncompressBuffer(System.Byte[])
extern "C"  ByteU5BU5D_t58506160* ZlibStream_UncompressBuffer_m2234646749 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___compressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
