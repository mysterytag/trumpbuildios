﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.DeflateManager
struct DeflateManager_t3792001679;
// System.Int16[]
struct Int16U5BU5D_t3675865332;
// System.SByte[]
struct SByteU5BU5D_t1084170289;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3910383704;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_BlockState1531979153.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_FlushType3984958891.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_ZlibCodec3910383704.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionStrategy2328611910.h"

// System.Void Ionic.Zlib.DeflateManager::.ctor()
extern "C"  void DeflateManager__ctor_m2821706994 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::.cctor()
extern "C"  void DeflateManager__cctor_m1091474683 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::_InitializeLazyMatch()
extern "C"  void DeflateManager__InitializeLazyMatch_m187731348 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::_InitializeTreeData()
extern "C"  void DeflateManager__InitializeTreeData_m3710420231 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::_InitializeBlocks()
extern "C"  void DeflateManager__InitializeBlocks_m1560312357 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::pqdownheap(System.Int16[],System.Int32)
extern "C"  void DeflateManager_pqdownheap_m3306109997 (DeflateManager_t3792001679 * __this, Int16U5BU5D_t3675865332* ___tree0, int32_t ___k1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.DeflateManager::_IsSmaller(System.Int16[],System.Int32,System.Int32,System.SByte[])
extern "C"  bool DeflateManager__IsSmaller_m2464904822 (Il2CppObject * __this /* static, unused */, Int16U5BU5D_t3675865332* ___tree0, int32_t ___n1, int32_t ___m2, SByteU5BU5D_t1084170289* ___depth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::scan_tree(System.Int16[],System.Int32)
extern "C"  void DeflateManager_scan_tree_m2445496318 (DeflateManager_t3792001679 * __this, Int16U5BU5D_t3675865332* ___tree0, int32_t ___max_code1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::build_bl_tree()
extern "C"  int32_t DeflateManager_build_bl_tree_m723923168 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::send_all_trees(System.Int32,System.Int32,System.Int32)
extern "C"  void DeflateManager_send_all_trees_m2147036963 (DeflateManager_t3792001679 * __this, int32_t ___lcodes0, int32_t ___dcodes1, int32_t ___blcodes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::send_tree(System.Int16[],System.Int32)
extern "C"  void DeflateManager_send_tree_m2614187593 (DeflateManager_t3792001679 * __this, Int16U5BU5D_t3675865332* ___tree0, int32_t ___max_code1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::put_bytes(System.Byte[],System.Int32,System.Int32)
extern "C"  void DeflateManager_put_bytes_m649671262 (DeflateManager_t3792001679 * __this, ByteU5BU5D_t58506160* ___p0, int32_t ___start1, int32_t ___len2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::send_code(System.Int32,System.Int16[])
extern "C"  void DeflateManager_send_code_m3176854730 (DeflateManager_t3792001679 * __this, int32_t ___c0, Int16U5BU5D_t3675865332* ___tree1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::send_bits(System.Int32,System.Int32)
extern "C"  void DeflateManager_send_bits_m2795081977 (DeflateManager_t3792001679 * __this, int32_t ___value0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::_tr_align()
extern "C"  void DeflateManager__tr_align_m3086551379 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.DeflateManager::_tr_tally(System.Int32,System.Int32)
extern "C"  bool DeflateManager__tr_tally_m1528649016 (DeflateManager_t3792001679 * __this, int32_t ___dist0, int32_t ___lc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::send_compressed_block(System.Int16[],System.Int16[])
extern "C"  void DeflateManager_send_compressed_block_m1224105424 (DeflateManager_t3792001679 * __this, Int16U5BU5D_t3675865332* ___ltree0, Int16U5BU5D_t3675865332* ___dtree1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::set_data_type()
extern "C"  void DeflateManager_set_data_type_m3641195970 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::bi_flush()
extern "C"  void DeflateManager_bi_flush_m3344280446 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::bi_windup()
extern "C"  void DeflateManager_bi_windup_m1575139211 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::copy_block(System.Int32,System.Int32,System.Boolean)
extern "C"  void DeflateManager_copy_block_m3419941452 (DeflateManager_t3792001679 * __this, int32_t ___buf0, int32_t ___len1, bool ___header2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::flush_block_only(System.Boolean)
extern "C"  void DeflateManager_flush_block_only_m4088712354 (DeflateManager_t3792001679 * __this, bool ___eof0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.BlockState Ionic.Zlib.DeflateManager::DeflateNone(Ionic.Zlib.FlushType)
extern "C"  int32_t DeflateManager_DeflateNone_m69864356 (DeflateManager_t3792001679 * __this, int32_t ___flush0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::_tr_stored_block(System.Int32,System.Int32,System.Boolean)
extern "C"  void DeflateManager__tr_stored_block_m581866780 (DeflateManager_t3792001679 * __this, int32_t ___buf0, int32_t ___stored_len1, bool ___eof2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::_tr_flush_block(System.Int32,System.Int32,System.Boolean)
extern "C"  void DeflateManager__tr_flush_block_m2266227831 (DeflateManager_t3792001679 * __this, int32_t ___buf0, int32_t ___stored_len1, bool ___eof2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::_fillWindow()
extern "C"  void DeflateManager__fillWindow_m1121186658 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.BlockState Ionic.Zlib.DeflateManager::DeflateFast(Ionic.Zlib.FlushType)
extern "C"  int32_t DeflateManager_DeflateFast_m4289397192 (DeflateManager_t3792001679 * __this, int32_t ___flush0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.BlockState Ionic.Zlib.DeflateManager::DeflateSlow(Ionic.Zlib.FlushType)
extern "C"  int32_t DeflateManager_DeflateSlow_m3025541549 (DeflateManager_t3792001679 * __this, int32_t ___flush0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::longest_match(System.Int32)
extern "C"  int32_t DeflateManager_longest_match_m1476091615 (DeflateManager_t3792001679 * __this, int32_t ___cur_match0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.DeflateManager::get_WantRfc1950HeaderBytes()
extern "C"  bool DeflateManager_get_WantRfc1950HeaderBytes_m3960582307 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::set_WantRfc1950HeaderBytes(System.Boolean)
extern "C"  void DeflateManager_set_WantRfc1950HeaderBytes_m2863712386 (DeflateManager_t3792001679 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::Initialize(Ionic.Zlib.ZlibCodec,Ionic.Zlib.CompressionLevel)
extern "C"  int32_t DeflateManager_Initialize_m2880269751 (DeflateManager_t3792001679 * __this, ZlibCodec_t3910383704 * ___codec0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::Initialize(Ionic.Zlib.ZlibCodec,Ionic.Zlib.CompressionLevel,System.Int32)
extern "C"  int32_t DeflateManager_Initialize_m1224339584 (DeflateManager_t3792001679 * __this, ZlibCodec_t3910383704 * ___codec0, int32_t ___level1, int32_t ___bits2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::Initialize(Ionic.Zlib.ZlibCodec,Ionic.Zlib.CompressionLevel,System.Int32,Ionic.Zlib.CompressionStrategy)
extern "C"  int32_t DeflateManager_Initialize_m1576050116 (DeflateManager_t3792001679 * __this, ZlibCodec_t3910383704 * ___codec0, int32_t ___level1, int32_t ___bits2, int32_t ___compressionStrategy3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::Initialize(Ionic.Zlib.ZlibCodec,Ionic.Zlib.CompressionLevel,System.Int32,System.Int32,Ionic.Zlib.CompressionStrategy)
extern "C"  int32_t DeflateManager_Initialize_m1144706829 (DeflateManager_t3792001679 * __this, ZlibCodec_t3910383704 * ___codec0, int32_t ___level1, int32_t ___windowBits2, int32_t ___memLevel3, int32_t ___strategy4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::Reset()
extern "C"  void DeflateManager_Reset_m468139935 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::End()
extern "C"  int32_t DeflateManager_End_m3072678393 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateManager::SetDeflater()
extern "C"  void DeflateManager_SetDeflater_m3641386169 (DeflateManager_t3792001679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::SetParams(Ionic.Zlib.CompressionLevel,Ionic.Zlib.CompressionStrategy)
extern "C"  int32_t DeflateManager_SetParams_m1802497741 (DeflateManager_t3792001679 * __this, int32_t ___level0, int32_t ___strategy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::SetDictionary(System.Byte[])
extern "C"  int32_t DeflateManager_SetDictionary_m2827709267 (DeflateManager_t3792001679 * __this, ByteU5BU5D_t58506160* ___dictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateManager::Deflate(Ionic.Zlib.FlushType)
extern "C"  int32_t DeflateManager_Deflate_m1597422914 (DeflateManager_t3792001679 * __this, int32_t ___flush0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
