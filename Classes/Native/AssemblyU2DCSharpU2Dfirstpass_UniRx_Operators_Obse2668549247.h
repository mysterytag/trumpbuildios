﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>
struct ObserveOnObservable_1_t3243387575;
// UniRx.ISchedulerQueueing
struct ISchedulerQueueing_t1271699701;
// UniRx.BooleanDisposable
struct BooleanDisposable_t3065601722;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>
struct  ObserveOn__t2668549247  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.ObserveOnObservable`1<T> UniRx.Operators.ObserveOnObservable`1/ObserveOn_::parent
	ObserveOnObservable_1_t3243387575 * ___parent_2;
	// UniRx.ISchedulerQueueing UniRx.Operators.ObserveOnObservable`1/ObserveOn_::scheduler
	Il2CppObject * ___scheduler_3;
	// UniRx.BooleanDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn_::isDisposed
	BooleanDisposable_t3065601722 * ___isDisposed_4;
	// System.Action`1<T> UniRx.Operators.ObserveOnObservable`1/ObserveOn_::onNext
	Action_1_t2706738743 * ___onNext_5;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ObserveOn__t2668549247, ___parent_2)); }
	inline ObserveOnObservable_1_t3243387575 * get_parent_2() const { return ___parent_2; }
	inline ObserveOnObservable_1_t3243387575 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ObserveOnObservable_1_t3243387575 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_scheduler_3() { return static_cast<int32_t>(offsetof(ObserveOn__t2668549247, ___scheduler_3)); }
	inline Il2CppObject * get_scheduler_3() const { return ___scheduler_3; }
	inline Il2CppObject ** get_address_of_scheduler_3() { return &___scheduler_3; }
	inline void set_scheduler_3(Il2CppObject * value)
	{
		___scheduler_3 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_3, value);
	}

	inline static int32_t get_offset_of_isDisposed_4() { return static_cast<int32_t>(offsetof(ObserveOn__t2668549247, ___isDisposed_4)); }
	inline BooleanDisposable_t3065601722 * get_isDisposed_4() const { return ___isDisposed_4; }
	inline BooleanDisposable_t3065601722 ** get_address_of_isDisposed_4() { return &___isDisposed_4; }
	inline void set_isDisposed_4(BooleanDisposable_t3065601722 * value)
	{
		___isDisposed_4 = value;
		Il2CppCodeGenWriteBarrier(&___isDisposed_4, value);
	}

	inline static int32_t get_offset_of_onNext_5() { return static_cast<int32_t>(offsetof(ObserveOn__t2668549247, ___onNext_5)); }
	inline Action_1_t2706738743 * get_onNext_5() const { return ___onNext_5; }
	inline Action_1_t2706738743 ** get_address_of_onNext_5() { return &___onNext_5; }
	inline void set_onNext_5(Action_1_t2706738743 * value)
	{
		___onNext_5 = value;
		Il2CppCodeGenWriteBarrier(&___onNext_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
