﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntCell
struct IntCell_t3621690833;
// IStream`1<System.Int32>
struct IStream_1_t3400105778;

#include "codegen/il2cpp-codegen.h"

// System.Void IntCell::.ctor(System.Int32)
extern "C"  void IntCell__ctor_m3350133515 (IntCell_t3621690833 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntCell::.ctor()
extern "C"  void IntCell__ctor_m1314950330 (IntCell_t3621690833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IStream`1<System.Int32> IntCell::get_diff()
extern "C"  Il2CppObject* IntCell_get_diff_m1673881746 (IntCell_t3621690833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IntCell::get_value()
extern "C"  int32_t IntCell_get_value_m3911388878 (IntCell_t3621690833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntCell::set_value(System.Int32)
extern "C"  void IntCell_set_value_m3632710685 (IntCell_t3621690833 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
