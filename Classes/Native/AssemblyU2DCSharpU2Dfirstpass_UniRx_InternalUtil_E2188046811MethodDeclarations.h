﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<System.Byte>
struct EmptyObserver_1_t2188046811;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m59149409_gshared (EmptyObserver_1_t2188046811 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m59149409(__this, method) ((  void (*) (EmptyObserver_1_t2188046811 *, const MethodInfo*))EmptyObserver_1__ctor_m59149409_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1351535468_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m1351535468(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m1351535468_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m731198251_gshared (EmptyObserver_1_t2188046811 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m731198251(__this, method) ((  void (*) (EmptyObserver_1_t2188046811 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m731198251_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3138223064_gshared (EmptyObserver_1_t2188046811 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3138223064(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2188046811 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3138223064_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Byte>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3838710473_gshared (EmptyObserver_1_t2188046811 * __this, uint8_t ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3838710473(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2188046811 *, uint8_t, const MethodInfo*))EmptyObserver_1_OnNext_m3838710473_gshared)(__this, ___value0, method)
