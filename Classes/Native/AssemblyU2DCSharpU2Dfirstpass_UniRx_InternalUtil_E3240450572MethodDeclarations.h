﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>
struct EmptyObserver_1_t3240450572;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Pair_1_gen3831097582.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m4047917427_gshared (EmptyObserver_1_t3240450572 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m4047917427(__this, method) ((  void (*) (EmptyObserver_1_t3240450572 *, const MethodInfo*))EmptyObserver_1__ctor_m4047917427_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m449292442_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m449292442(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m449292442_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1910334141_gshared (EmptyObserver_1_t3240450572 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1910334141(__this, method) ((  void (*) (EmptyObserver_1_t3240450572 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1910334141_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3930120298_gshared (EmptyObserver_1_t3240450572 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3930120298(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t3240450572 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3930120298_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Pair`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1638980443_gshared (EmptyObserver_1_t3240450572 * __this, Pair_1_t3831097582  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m1638980443(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t3240450572 *, Pair_1_t3831097582 , const MethodInfo*))EmptyObserver_1_OnNext_m1638980443_gshared)(__this, ___value0, method)
