﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object,GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>
struct Transform_1_t2248175502;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object,GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1368709451_gshared (Transform_1_t2248175502 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1368709451(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2248175502 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1368709451_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object,GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2669645009_gshared (Transform_1_t2248175502 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2669645009(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t2248175502 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2669645009_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object,GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4175941040_gshared (Transform_1_t2248175502 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m4175941040(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2248175502 *, int32_t, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m4175941040_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object,GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2158204697_gshared (Transform_1_t2248175502 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m2158204697(__this, ___result0, method) ((  int32_t (*) (Transform_1_t2248175502 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2158204697_gshared)(__this, ___result0, method)
