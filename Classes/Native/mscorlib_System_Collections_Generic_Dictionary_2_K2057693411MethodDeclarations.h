﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct Dictionary_2_t2290665470;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2057693411.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m82768539_gshared (Enumerator_t2057693411 * __this, Dictionary_2_t2290665470 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m82768539(__this, ___host0, method) ((  void (*) (Enumerator_t2057693411 *, Dictionary_2_t2290665470 *, const MethodInfo*))Enumerator__ctor_m82768539_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m161819376_gshared (Enumerator_t2057693411 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m161819376(__this, method) ((  Il2CppObject * (*) (Enumerator_t2057693411 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m161819376_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1526321978_gshared (Enumerator_t2057693411 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1526321978(__this, method) ((  void (*) (Enumerator_t2057693411 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1526321978_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m224454141_gshared (Enumerator_t2057693411 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m224454141(__this, method) ((  void (*) (Enumerator_t2057693411 *, const MethodInfo*))Enumerator_Dispose_m224454141_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3194189418_gshared (Enumerator_t2057693411 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3194189418(__this, method) ((  bool (*) (Enumerator_t2057693411 *, const MethodInfo*))Enumerator_MoveNext_m3194189418_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4123346542_gshared (Enumerator_t2057693411 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4123346542(__this, method) ((  int32_t (*) (Enumerator_t2057693411 *, const MethodInfo*))Enumerator_get_Current_m4123346542_gshared)(__this, method)
