﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<ResolveAll>c__AnonStorey189
struct U3CResolveAllU3Ec__AnonStorey189_t628370168;
// System.Object
struct Il2CppObject;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.DiContainer/<ResolveAll>c__AnonStorey189::.ctor()
extern "C"  void U3CResolveAllU3Ec__AnonStorey189__ctor_m1964349425 (U3CResolveAllU3Ec__AnonStorey189_t628370168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer/<ResolveAll>c__AnonStorey189::<>m__2C4(Zenject.ProviderBase)
extern "C"  Il2CppObject * U3CResolveAllU3Ec__AnonStorey189_U3CU3Em__2C4_m3338917791 (U3CResolveAllU3Ec__AnonStorey189_t628370168 * __this, ProviderBase_t1627494391 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
