﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewTableCell
struct NewTableCell_t2774679728;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;

#include "codegen/il2cpp-codegen.h"

// System.Void NewTableCell::.ctor()
extern "C"  void NewTableCell__ctor_m527136747 (NewTableCell_t2774679728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform NewTableCell::get_rect()
extern "C"  RectTransform_t3317474837 * NewTableCell_get_rect_m2800814120 (NewTableCell_t2774679728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NewTableCell::GetSize()
extern "C"  float NewTableCell_GetSize_m686112628 (NewTableCell_t2774679728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
