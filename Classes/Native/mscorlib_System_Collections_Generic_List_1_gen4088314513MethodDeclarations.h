﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>
struct List_1_t4088314513;
// System.Collections.Generic.IEnumerable`1<GameAnalyticsSDK.Settings/HelpTypes>
struct IEnumerable_1_t1868542604;
// System.Collections.Generic.IEnumerator`1<GameAnalyticsSDK.Settings/HelpTypes>
struct IEnumerator_1_t479494696;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<GameAnalyticsSDK.Settings/HelpTypes>
struct ICollection_1_t3757186930;
// System.Collections.ObjectModel.ReadOnlyCollection`1<GameAnalyticsSDK.Settings/HelpTypes>
struct ReadOnlyCollection_1_t2159533596;
// GameAnalyticsSDK.Settings/HelpTypes[]
struct HelpTypesU5BU5D_t2651985161;
// System.Predicate`1<GameAnalyticsSDK.Settings/HelpTypes>
struct Predicate_1_t3862319442;
// System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>
struct Action_1_t3439808249;
// System.Collections.Generic.IComparer`1<GameAnalyticsSDK.Settings/HelpTypes>
struct IComparer_1_t1696095657;
// System.Comparison`1<GameAnalyticsSDK.Settings/HelpTypes>
struct Comparison_1_t1700063124;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2174097505.h"

// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor()
extern "C"  void List_1__ctor_m3066011987_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1__ctor_m3066011987(__this, method) ((  void (*) (List_1_t4088314513 *, const MethodInfo*))List_1__ctor_m3066011987_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2232998805_gshared (List_1_t4088314513 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2232998805(__this, ___collection0, method) ((  void (*) (List_1_t4088314513 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2232998805_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m872534267_gshared (List_1_t4088314513 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m872534267(__this, ___capacity0, method) ((  void (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1__ctor_m872534267_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::.cctor()
extern "C"  void List_1__cctor_m4262221379_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4262221379(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4262221379_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1268662908_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1268662908(__this, method) ((  Il2CppObject* (*) (List_1_t4088314513 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1268662908_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3215781338_gshared (List_1_t4088314513 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3215781338(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4088314513 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3215781338_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2891493589_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2891493589(__this, method) ((  Il2CppObject * (*) (List_1_t4088314513 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2891493589_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4171606204_gshared (List_1_t4088314513 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4171606204(__this, ___item0, method) ((  int32_t (*) (List_1_t4088314513 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4171606204_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2955297808_gshared (List_1_t4088314513 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2955297808(__this, ___item0, method) ((  bool (*) (List_1_t4088314513 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2955297808_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m388702484_gshared (List_1_t4088314513 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m388702484(__this, ___item0, method) ((  int32_t (*) (List_1_t4088314513 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m388702484_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1246713407_gshared (List_1_t4088314513 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1246713407(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4088314513 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1246713407_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m565844105_gshared (List_1_t4088314513 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m565844105(__this, ___item0, method) ((  void (*) (List_1_t4088314513 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m565844105_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3860357137_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3860357137(__this, method) ((  bool (*) (List_1_t4088314513 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3860357137_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3032340812_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3032340812(__this, method) ((  bool (*) (List_1_t4088314513 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3032340812_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2397087864_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2397087864(__this, method) ((  Il2CppObject * (*) (List_1_t4088314513 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2397087864_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3371581311_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3371581311(__this, method) ((  bool (*) (List_1_t4088314513 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3371581311_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1948242842_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1948242842(__this, method) ((  bool (*) (List_1_t4088314513 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1948242842_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2564578303_gshared (List_1_t4088314513 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2564578303(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2564578303_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m409661014_gshared (List_1_t4088314513 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m409661014(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4088314513 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m409661014_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Add(T)
extern "C"  void List_1_Add_m3566024853_gshared (List_1_t4088314513 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3566024853(__this, ___item0, method) ((  void (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_Add_m3566024853_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2189118032_gshared (List_1_t4088314513 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2189118032(__this, ___newCount0, method) ((  void (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2189118032_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3006161230_gshared (List_1_t4088314513 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3006161230(__this, ___collection0, method) ((  void (*) (List_1_t4088314513 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3006161230_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2267133454_gshared (List_1_t4088314513 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2267133454(__this, ___enumerable0, method) ((  void (*) (List_1_t4088314513 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2267133454_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2569873353_gshared (List_1_t4088314513 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2569873353(__this, ___collection0, method) ((  void (*) (List_1_t4088314513 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2569873353_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2159533596 * List_1_AsReadOnly_m4258339520_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m4258339520(__this, method) ((  ReadOnlyCollection_1_t2159533596 * (*) (List_1_t4088314513 *, const MethodInfo*))List_1_AsReadOnly_m4258339520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Clear()
extern "C"  void List_1_Clear_m884311765_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_Clear_m884311765(__this, method) ((  void (*) (List_1_t4088314513 *, const MethodInfo*))List_1_Clear_m884311765_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Contains(T)
extern "C"  bool List_1_Contains_m3594055427_gshared (List_1_t4088314513 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3594055427(__this, ___item0, method) ((  bool (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_Contains_m3594055427_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1912603973_gshared (List_1_t4088314513 * __this, HelpTypesU5BU5D_t2651985161* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1912603973(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4088314513 *, HelpTypesU5BU5D_t2651985161*, int32_t, const MethodInfo*))List_1_CopyTo_m1912603973_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m1470075331_gshared (List_1_t4088314513 * __this, Predicate_1_t3862319442 * ___match0, const MethodInfo* method);
#define List_1_Find_m1470075331(__this, ___match0, method) ((  int32_t (*) (List_1_t4088314513 *, Predicate_1_t3862319442 *, const MethodInfo*))List_1_Find_m1470075331_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2202220350_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3862319442 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2202220350(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3862319442 *, const MethodInfo*))List_1_CheckMatch_m2202220350_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1487358307_gshared (List_1_t4088314513 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3862319442 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1487358307(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4088314513 *, int32_t, int32_t, Predicate_1_t3862319442 *, const MethodInfo*))List_1_GetIndex_m1487358307_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m2221922610_gshared (List_1_t4088314513 * __this, Action_1_t3439808249 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m2221922610(__this, ___action0, method) ((  void (*) (List_1_t4088314513 *, Action_1_t3439808249 *, const MethodInfo*))List_1_ForEach_m2221922610_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::GetEnumerator()
extern "C"  Enumerator_t2174097505  List_1_GetEnumerator_m3680676544_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3680676544(__this, method) ((  Enumerator_t2174097505  (*) (List_1_t4088314513 *, const MethodInfo*))List_1_GetEnumerator_m3680676544_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2112307849_gshared (List_1_t4088314513 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2112307849(__this, ___item0, method) ((  int32_t (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_IndexOf_m2112307849_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m720684700_gshared (List_1_t4088314513 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m720684700(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4088314513 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m720684700_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1658971157_gshared (List_1_t4088314513 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1658971157(__this, ___index0, method) ((  void (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1658971157_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1945676028_gshared (List_1_t4088314513 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1945676028(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4088314513 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1945676028_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3229587569_gshared (List_1_t4088314513 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3229587569(__this, ___collection0, method) ((  void (*) (List_1_t4088314513 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3229587569_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Remove(T)
extern "C"  bool List_1_Remove_m1540592574_gshared (List_1_t4088314513 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m1540592574(__this, ___item0, method) ((  bool (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_Remove_m1540592574_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3251604876_gshared (List_1_t4088314513 * __this, Predicate_1_t3862319442 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3251604876(__this, ___match0, method) ((  int32_t (*) (List_1_t4088314513 *, Predicate_1_t3862319442 *, const MethodInfo*))List_1_RemoveAll_m3251604876_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4114496194_gshared (List_1_t4088314513 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4114496194(__this, ___index0, method) ((  void (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4114496194_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Reverse()
extern "C"  void List_1_Reverse_m1082768490_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_Reverse_m1082768490(__this, method) ((  void (*) (List_1_t4088314513 *, const MethodInfo*))List_1_Reverse_m1082768490_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Sort()
extern "C"  void List_1_Sort_m4091997048_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_Sort_m4091997048(__this, method) ((  void (*) (List_1_t4088314513 *, const MethodInfo*))List_1_Sort_m4091997048_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m364093804_gshared (List_1_t4088314513 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m364093804(__this, ___comparer0, method) ((  void (*) (List_1_t4088314513 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m364093804_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1541049803_gshared (List_1_t4088314513 * __this, Comparison_1_t1700063124 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1541049803(__this, ___comparison0, method) ((  void (*) (List_1_t4088314513 *, Comparison_1_t1700063124 *, const MethodInfo*))List_1_Sort_m1541049803_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::ToArray()
extern "C"  HelpTypesU5BU5D_t2651985161* List_1_ToArray_m3376249129_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_ToArray_m3376249129(__this, method) ((  HelpTypesU5BU5D_t2651985161* (*) (List_1_t4088314513 *, const MethodInfo*))List_1_ToArray_m3376249129_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4226126097_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m4226126097(__this, method) ((  void (*) (List_1_t4088314513 *, const MethodInfo*))List_1_TrimExcess_m4226126097_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3156266553_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3156266553(__this, method) ((  int32_t (*) (List_1_t4088314513 *, const MethodInfo*))List_1_get_Capacity_m3156266553_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3694274146_gshared (List_1_t4088314513 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3694274146(__this, ___value0, method) ((  void (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3694274146_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Count()
extern "C"  int32_t List_1_get_Count_m874341394_gshared (List_1_t4088314513 * __this, const MethodInfo* method);
#define List_1_get_Count_m874341394(__this, method) ((  int32_t (*) (List_1_t4088314513 *, const MethodInfo*))List_1_get_Count_m874341394_gshared)(__this, method)
// T System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2411830022_gshared (List_1_t4088314513 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2411830022(__this, ___index0, method) ((  int32_t (*) (List_1_t4088314513 *, int32_t, const MethodInfo*))List_1_get_Item_m2411830022_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GameAnalyticsSDK.Settings/HelpTypes>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3293485779_gshared (List_1_t4088314513 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3293485779(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4088314513 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3293485779_gshared)(__this, ___index0, ___value1, method)
