﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IProcessExecution
struct IProcessExecution_t2362207186;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroExtensions/ExecutionWait
struct  ExecutionWait_t1721103501  : public Il2CppObject
{
public:
	// IProcessExecution CoroExtensions/ExecutionWait::exec
	Il2CppObject * ___exec_0;

public:
	inline static int32_t get_offset_of_exec_0() { return static_cast<int32_t>(offsetof(ExecutionWait_t1721103501, ___exec_0)); }
	inline Il2CppObject * get_exec_0() const { return ___exec_0; }
	inline Il2CppObject ** get_address_of_exec_0() { return &___exec_0; }
	inline void set_exec_0(Il2CppObject * value)
	{
		___exec_0 = value;
		Il2CppCodeGenWriteBarrier(&___exec_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
