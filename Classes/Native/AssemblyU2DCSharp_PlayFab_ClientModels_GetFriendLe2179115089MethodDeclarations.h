﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest
struct GetFriendLeaderboardAroundCurrentUserRequest_t2179115089;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::.ctor()
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest__ctor_m205410316 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::get_StatisticName()
extern "C"  String_t* GetFriendLeaderboardAroundCurrentUserRequest_get_StatisticName_m1941564383 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::set_StatisticName(System.String)
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest_set_StatisticName_m4106543354 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::get_MaxResultsCount()
extern "C"  Nullable_1_t1438485399  GetFriendLeaderboardAroundCurrentUserRequest_get_MaxResultsCount_m1044677549 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::set_MaxResultsCount(System.Nullable`1<System.Int32>)
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest_set_MaxResultsCount_m3386655556 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::get_IncludeSteamFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardAroundCurrentUserRequest_get_IncludeSteamFriends_m1161150263 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::set_IncludeSteamFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest_set_IncludeSteamFriends_m2909565678 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::get_IncludeFacebookFriends()
extern "C"  Nullable_1_t3097043249  GetFriendLeaderboardAroundCurrentUserRequest_get_IncludeFacebookFriends_m3429366335 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest::set_IncludeFacebookFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendLeaderboardAroundCurrentUserRequest_set_IncludeFacebookFriends_m2479440268 (GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
