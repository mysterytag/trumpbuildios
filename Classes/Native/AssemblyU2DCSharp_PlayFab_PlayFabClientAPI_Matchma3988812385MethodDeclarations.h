﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/MatchmakeRequestCallback
struct MatchmakeRequestCallback_t3988812385;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.MatchmakeRequest
struct MatchmakeRequest_t4152079884;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeRe4152079884.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/MatchmakeRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void MatchmakeRequestCallback__ctor_m3094496976 (MatchmakeRequestCallback_t3988812385 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/MatchmakeRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.MatchmakeRequest,System.Object)
extern "C"  void MatchmakeRequestCallback_Invoke_m2905697215 (MatchmakeRequestCallback_t3988812385 * __this, String_t* ___urlPath0, int32_t ___callId1, MatchmakeRequest_t4152079884 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_MatchmakeRequestCallback_t3988812385(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, MatchmakeRequest_t4152079884 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/MatchmakeRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.MatchmakeRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MatchmakeRequestCallback_BeginInvoke_m1321736040 (MatchmakeRequestCallback_t3988812385 * __this, String_t* ___urlPath0, int32_t ___callId1, MatchmakeRequest_t4152079884 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/MatchmakeRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void MatchmakeRequestCallback_EndInvoke_m1612292320 (MatchmakeRequestCallback_t3988812385 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
