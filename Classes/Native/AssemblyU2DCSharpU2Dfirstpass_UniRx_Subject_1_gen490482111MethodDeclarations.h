﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<System.Int32>
struct Subject_1_t490482111;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Subject`1<System.Int32>::.ctor()
extern "C"  void Subject_1__ctor_m1321103487_gshared (Subject_1_t490482111 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1321103487(__this, method) ((  void (*) (Subject_1_t490482111 *, const MethodInfo*))Subject_1__ctor_m1321103487_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Int32>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1275029237_gshared (Subject_1_t490482111 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m1275029237(__this, method) ((  bool (*) (Subject_1_t490482111 *, const MethodInfo*))Subject_1_get_HasObservers_m1275029237_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Int32>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m827132105_gshared (Subject_1_t490482111 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m827132105(__this, method) ((  void (*) (Subject_1_t490482111 *, const MethodInfo*))Subject_1_OnCompleted_m827132105_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Int32>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m529257590_gshared (Subject_1_t490482111 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m529257590(__this, ___error0, method) ((  void (*) (Subject_1_t490482111 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m529257590_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<System.Int32>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1100834663_gshared (Subject_1_t490482111 * __this, int32_t ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1100834663(__this, ___value0, method) ((  void (*) (Subject_1_t490482111 *, int32_t, const MethodInfo*))Subject_1_OnNext_m1100834663_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m1838832148_gshared (Subject_1_t490482111 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m1838832148(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t490482111 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m1838832148_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<System.Int32>::Dispose()
extern "C"  void Subject_1_Dispose_m2463381756_gshared (Subject_1_t490482111 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2463381756(__this, method) ((  void (*) (Subject_1_t490482111 *, const MethodInfo*))Subject_1_Dispose_m2463381756_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Int32>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m2039948933_gshared (Subject_1_t490482111 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m2039948933(__this, method) ((  void (*) (Subject_1_t490482111 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m2039948933_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Int32>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1982718124_gshared (Subject_1_t490482111 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1982718124(__this, method) ((  bool (*) (Subject_1_t490482111 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1982718124_gshared)(__this, method)
