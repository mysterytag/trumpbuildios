﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::.ctor()
#define Stream_1__ctor_m1274244601(__this, method) ((  void (*) (Stream_1_t1826872014 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::Send(T)
#define Stream_1_Send_m4098429451(__this, ___value0, method) ((  void (*) (Stream_1_t1826872014 *, CollectionReplaceEvent_1_t3135732992 *, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m3063479777(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t1826872014 *, CollectionReplaceEvent_1_t3135732992 *, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m1790789539(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t1826872014 *, Action_1_t3284185697 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m1445478308(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t1826872014 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::Dispose()
#define Stream_1_Dispose_m381665270(__this, method) ((  void (*) (Stream_1_t1826872014 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m760645543(__this, ___stream0, method) ((  void (*) (Stream_1_t1826872014 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::ClearInputStream()
#define Stream_1_ClearInputStream_m4207234376(__this, method) ((  void (*) (Stream_1_t1826872014 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m3128385820(__this, method) ((  void (*) (Stream_1_t1826872014 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::Unpack(System.Int32)
#define Stream_1_Unpack_m1869857582(__this, ___p0, method) ((  void (*) (Stream_1_t1826872014 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<CollectionReplaceEvent`1<UpgradeButton>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m362332246(__this, ___val0, method) ((  void (*) (Stream_1_t1826872014 *, CollectionReplaceEvent_1_t3135732992 *, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
