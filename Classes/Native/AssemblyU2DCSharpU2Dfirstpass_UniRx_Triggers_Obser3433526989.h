﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableUpdateTrigger
struct  ObservableUpdateTrigger_t3433526989  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableUpdateTrigger::update
	Subject_1_t201353362 * ___update_8;

public:
	inline static int32_t get_offset_of_update_8() { return static_cast<int32_t>(offsetof(ObservableUpdateTrigger_t3433526989, ___update_8)); }
	inline Subject_1_t201353362 * get_update_8() const { return ___update_8; }
	inline Subject_1_t201353362 ** get_address_of_update_8() { return &___update_8; }
	inline void set_update_8(Subject_1_t201353362 * value)
	{
		___update_8 = value;
		Il2CppCodeGenWriteBarrier(&___update_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
