﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator1B
struct U3CDelayActionU3Ec__Iterator1B_t942204033;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator1B::.ctor()
extern "C"  void U3CDelayActionU3Ec__Iterator1B__ctor_m345872032 (U3CDelayActionU3Ec__Iterator1B_t942204033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator1B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayActionU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3107610738 (U3CDelayActionU3Ec__Iterator1B_t942204033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayActionU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m1850705926 (U3CDelayActionU3Ec__Iterator1B_t942204033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator1B::MoveNext()
extern "C"  bool U3CDelayActionU3Ec__Iterator1B_MoveNext_m3809248364 (U3CDelayActionU3Ec__Iterator1B_t942204033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator1B::Dispose()
extern "C"  void U3CDelayActionU3Ec__Iterator1B_Dispose_m1568824029 (U3CDelayActionU3Ec__Iterator1B_t942204033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator1B::Reset()
extern "C"  void U3CDelayActionU3Ec__Iterator1B_Reset_m2287272269 (U3CDelayActionU3Ec__Iterator1B_t942204033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
