﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ImmutableList`1<System.Object>
struct  ImmutableList_1_t340490602  : public Il2CppObject
{
public:
	// T[] ZergRush.ImmutableList`1::data
	ObjectU5BU5D_t11523773* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ImmutableList_1_t340490602, ___data_0)); }
	inline ObjectU5BU5D_t11523773* get_data_0() const { return ___data_0; }
	inline ObjectU5BU5D_t11523773** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ObjectU5BU5D_t11523773* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
