﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.MatchmakeRequest
struct  MatchmakeRequest_t4152079884  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.MatchmakeRequest::<BuildVersion>k__BackingField
	String_t* ___U3CBuildVersionU3Ek__BackingField_0;
	// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.MatchmakeRequest::<Region>k__BackingField
	Nullable_1_t212373688  ___U3CRegionU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.MatchmakeRequest::<GameMode>k__BackingField
	String_t* ___U3CGameModeU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.MatchmakeRequest::<LobbyId>k__BackingField
	String_t* ___U3CLobbyIdU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.MatchmakeRequest::<StatisticName>k__BackingField
	String_t* ___U3CStatisticNameU3Ek__BackingField_4;
	// System.String PlayFab.ClientModels.MatchmakeRequest::<CharacterId>k__BackingField
	String_t* ___U3CCharacterIdU3Ek__BackingField_5;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.MatchmakeRequest::<EnableQueue>k__BackingField
	Nullable_1_t3097043249  ___U3CEnableQueueU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CBuildVersionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MatchmakeRequest_t4152079884, ___U3CBuildVersionU3Ek__BackingField_0)); }
	inline String_t* get_U3CBuildVersionU3Ek__BackingField_0() const { return ___U3CBuildVersionU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CBuildVersionU3Ek__BackingField_0() { return &___U3CBuildVersionU3Ek__BackingField_0; }
	inline void set_U3CBuildVersionU3Ek__BackingField_0(String_t* value)
	{
		___U3CBuildVersionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBuildVersionU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MatchmakeRequest_t4152079884, ___U3CRegionU3Ek__BackingField_1)); }
	inline Nullable_1_t212373688  get_U3CRegionU3Ek__BackingField_1() const { return ___U3CRegionU3Ek__BackingField_1; }
	inline Nullable_1_t212373688 * get_address_of_U3CRegionU3Ek__BackingField_1() { return &___U3CRegionU3Ek__BackingField_1; }
	inline void set_U3CRegionU3Ek__BackingField_1(Nullable_1_t212373688  value)
	{
		___U3CRegionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CGameModeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MatchmakeRequest_t4152079884, ___U3CGameModeU3Ek__BackingField_2)); }
	inline String_t* get_U3CGameModeU3Ek__BackingField_2() const { return ___U3CGameModeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CGameModeU3Ek__BackingField_2() { return &___U3CGameModeU3Ek__BackingField_2; }
	inline void set_U3CGameModeU3Ek__BackingField_2(String_t* value)
	{
		___U3CGameModeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameModeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CLobbyIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MatchmakeRequest_t4152079884, ___U3CLobbyIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CLobbyIdU3Ek__BackingField_3() const { return ___U3CLobbyIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CLobbyIdU3Ek__BackingField_3() { return &___U3CLobbyIdU3Ek__BackingField_3; }
	inline void set_U3CLobbyIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CLobbyIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLobbyIdU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CStatisticNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MatchmakeRequest_t4152079884, ___U3CStatisticNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CStatisticNameU3Ek__BackingField_4() const { return ___U3CStatisticNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CStatisticNameU3Ek__BackingField_4() { return &___U3CStatisticNameU3Ek__BackingField_4; }
	inline void set_U3CStatisticNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CStatisticNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStatisticNameU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CCharacterIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MatchmakeRequest_t4152079884, ___U3CCharacterIdU3Ek__BackingField_5)); }
	inline String_t* get_U3CCharacterIdU3Ek__BackingField_5() const { return ___U3CCharacterIdU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCharacterIdU3Ek__BackingField_5() { return &___U3CCharacterIdU3Ek__BackingField_5; }
	inline void set_U3CCharacterIdU3Ek__BackingField_5(String_t* value)
	{
		___U3CCharacterIdU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterIdU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CEnableQueueU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MatchmakeRequest_t4152079884, ___U3CEnableQueueU3Ek__BackingField_6)); }
	inline Nullable_1_t3097043249  get_U3CEnableQueueU3Ek__BackingField_6() const { return ___U3CEnableQueueU3Ek__BackingField_6; }
	inline Nullable_1_t3097043249 * get_address_of_U3CEnableQueueU3Ek__BackingField_6() { return &___U3CEnableQueueU3Ek__BackingField_6; }
	inline void set_U3CEnableQueueU3Ek__BackingField_6(Nullable_1_t3097043249  value)
	{
		___U3CEnableQueueU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
