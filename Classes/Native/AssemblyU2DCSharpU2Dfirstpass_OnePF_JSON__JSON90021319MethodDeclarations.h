﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.JSON/_JSON
struct _JSON_t90021319;
// OnePF.JSON
struct JSON_t2649481148;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON2649481148.h"

// System.Void OnePF.JSON/_JSON::.ctor()
extern "C"  void _JSON__ctor_m4052829501 (_JSON_t90021319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON/_JSON::Deserialize(System.String)
extern "C"  JSON_t2649481148 * _JSON_Deserialize_m1156793587 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.JSON/_JSON::Serialize(OnePF.JSON)
extern "C"  String_t* _JSON_Serialize_m3685512094 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
