﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<GlobalStat/ReceiptForVerify>
struct List_1_t265820259;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Collections.Generic.List`1<Cell`1<System.Int32>>
struct List_1_t3826471388;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t3644373851;
// System.Collections.Generic.List`1<DateTimeCell>
struct List_1_t1570757814;
// System.String
struct String_t;
// DoubleCell
struct DoubleCell_t2771715315;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// UniRx.ReadOnlyReactiveProperty`1<System.Boolean>
struct ReadOnlyReactiveProperty_1_t3000407357;
// Cell`1<System.Double>
struct Cell_1_t716614246;
// UniRx.ReactiveProperty`1<System.Double>
struct ReactiveProperty_1_t1142849652;
// VKProfileInfo
struct VKProfileInfo_t3473649666;
// GlobalStat/fbProfile
struct fbProfile_t675187021;
// GameData
struct GameData_t2589969116;
// UniRx.ReactiveProperty`1<System.String>
struct ReactiveProperty_1_t1576821940;
// System.String[]
struct StringU5BU5D_t2956870243;
// UnityEngine.Color[]
struct ColorU5BU5D_t3477081137;
// System.Double[]
struct DoubleU5BU5D_t1048280995;
// GuiController
struct GuiController_t1495593751;
// GameController
struct GameController_t2782302542;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// UniRx.ReactiveProperty`1<System.Int32>
struct ReactiveProperty_1_t3455747825;
// Message
struct Message_t2619578343;
// AudioController
struct AudioController_t2316845042;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// System.Func`2<System.Int64,DateTimeCell>
struct Func_2_t2814130075;
// System.Func`2<DateTimeCell,System.Int64>
struct Func_2_t831010073;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalStat
struct  GlobalStat_t1134678199  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<GlobalStat/ReceiptForVerify> GlobalStat::receiptsForVerify
	List_1_t265820259 * ___receiptsForVerify_0;
	// System.Int32[] GlobalStat::passiveUpgradesLevels
	Int32U5BU5D_t1809983122* ___passiveUpgradesLevels_1;
	// System.Int32[] GlobalStat::safeUpgradesLevels
	Int32U5BU5D_t1809983122* ___safeUpgradesLevels_2;
	// System.Int32[] GlobalStat::billUpgradesLevels
	Int32U5BU5D_t1809983122* ___billUpgradesLevels_3;
	// System.Collections.Generic.List`1<Cell`1<System.Int32>> GlobalStat::PassiveUpgradeLevels
	List_1_t3826471388 * ___PassiveUpgradeLevels_4;
	// System.Collections.Generic.List`1<Cell`1<System.Int32>> GlobalStat::SafeUpgradesLevels
	List_1_t3826471388 * ___SafeUpgradesLevels_5;
	// System.Collections.Generic.List`1<Cell`1<System.Int32>> GlobalStat::BillUpgradesLevels
	List_1_t3826471388 * ___BillUpgradesLevels_6;
	// System.Collections.Generic.List`1<System.Int64> GlobalStat::SafeUpgradesDostavkaSerializable
	List_1_t3644373851 * ___SafeUpgradesDostavkaSerializable_7;
	// System.Collections.Generic.List`1<System.Int64> GlobalStat::PassiveUpgradesDostavkaSerializable
	List_1_t3644373851 * ___PassiveUpgradesDostavkaSerializable_8;
	// System.Collections.Generic.List`1<DateTimeCell> GlobalStat::SafeUpgradesDostavka
	List_1_t1570757814 * ___SafeUpgradesDostavka_9;
	// System.String GlobalStat::SocialId
	String_t* ___SocialId_10;
	// System.Collections.Generic.List`1<DateTimeCell> GlobalStat::PassiveUpgradeDostavka
	List_1_t1570757814 * ___PassiveUpgradeDostavka_11;
	// System.Int32 GlobalStat::UPGLvl
	int32_t ___UPGLvl_12;
	// DoubleCell GlobalStat::hardMoney
	DoubleCell_t2771715315 * ___hardMoney_13;
	// System.String GlobalStat::FBGroupStat
	String_t* ___FBGroupStat_14;
	// System.String GlobalStat::FBWallPostStat
	String_t* ___FBWallPostStat_15;
	// System.Boolean GlobalStat::VKLogStat
	bool ___VKLogStat_16;
	// System.Boolean GlobalStat::fbFirstConnect
	bool ___fbFirstConnect_17;
	// System.Double GlobalStat::hard
	double ___hard_18;
	// System.Collections.Generic.List`1<System.String> GlobalStat::vkIDNotEnabled
	List_1_t1765447871 * ___vkIDNotEnabled_19;
	// System.Boolean GlobalStat::fastDelivery
	bool ___fastDelivery_20;
	// System.String GlobalStat::EventSocial
	String_t* ___EventSocial_21;
	// System.String GlobalStat::LocalizationStr
	String_t* ___LocalizationStr_22;
	// System.Boolean GlobalStat::soundEnabled
	bool ___soundEnabled_23;
	// UniRx.ReadOnlyReactiveProperty`1<System.Boolean> GlobalStat::SoundEnabled
	ReadOnlyReactiveProperty_1_t3000407357 * ___SoundEnabled_24;
	// Cell`1<System.Double> GlobalStat::ReactiveMoney
	Cell_1_t716614246 * ___ReactiveMoney_25;
	// System.String GlobalStat::lastBoughtUpgrade
	String_t* ___lastBoughtUpgrade_26;
	// System.Double GlobalStat::money
	double ___money_27;
	// System.Double GlobalStat::countr
	double ___countr_28;
	// System.Boolean GlobalStat::doubleEarning
	bool ___doubleEarning_29;
	// System.Double GlobalStat::CounRealMoney
	double ___CounRealMoney_30;
	// System.String GlobalStat::endTime
	String_t* ___endTime_31;
	// System.Boolean GlobalStat::mmm
	bool ___mmm_32;
	// System.Boolean GlobalStat::free_mmm
	bool ___free_mmm_33;
	// System.Double GlobalStat::_passiveIncome
	double ____passiveIncome_34;
	// System.DateTime GlobalStat::lastFriendInviteTick
	DateTime_t339033936  ___lastFriendInviteTick_35;
	// System.Int32 GlobalStat::friendInviteAvailable
	int32_t ___friendInviteAvailable_36;
	// System.Int32 GlobalStat::totalFriendsInvited
	int32_t ___totalFriendsInvited_37;
	// System.Collections.Generic.List`1<System.String> GlobalStat::invitedVkIDs
	List_1_t1765447871 * ___invitedVkIDs_38;
	// System.Collections.Generic.List`1<System.String> GlobalStat::InvitedFbIDs
	List_1_t1765447871 * ___InvitedFbIDs_39;
	// System.Collections.Generic.List`1<System.String> GlobalStat::InvVkListOfId
	List_1_t1765447871 * ___InvVkListOfId_40;
	// System.Int32 GlobalStat::buttonID
	int32_t ___buttonID_41;
	// System.String GlobalStat::ButtonTitleId
	String_t* ___ButtonTitleId_42;
	// UniRx.ReactiveProperty`1<System.Double> GlobalStat::PasInc
	ReactiveProperty_1_t1142849652 * ___PasInc_43;
	// System.Double GlobalStat::_bucketIncome
	double ____bucketIncome_44;
	// System.Double GlobalStat::_bucketSize
	double ____bucketSize_45;
	// System.Double GlobalStat::currentBucket
	double ___currentBucket_46;
	// System.Int64 GlobalStat::savingStartTime
	int64_t ___savingStartTime_47;
	// System.Double GlobalStat::_achievSize
	double ____achievSize_48;
	// System.Double GlobalStat::currentAchiev
	double ___currentAchiev_49;
	// System.Double GlobalStat::_achivIncom
	double ____achivIncom_50;
	// System.Boolean GlobalStat::inappPurchased
	bool ___inappPurchased_51;
	// System.Single GlobalStat::lastAdShown
	float ___lastAdShown_52;
	// System.Int32 GlobalStat::adsShown
	int32_t ___adsShown_53;
	// System.Boolean GlobalStat::friendInvitedFB
	bool ___friendInvitedFB_54;
	// System.Boolean GlobalStat::vkWalPost
	bool ___vkWalPost_55;
	// System.Boolean GlobalStat::vkLoginRewardApplied
	bool ___vkLoginRewardApplied_56;
	// System.Boolean GlobalStat::vkJoinRewardApplied
	bool ___vkJoinRewardApplied_57;
	// System.Int32 GlobalStat::launches
	int32_t ___launches_58;
	// System.Boolean GlobalStat::showRateApp
	bool ___showRateApp_59;
	// System.DateTime GlobalStat::lastShowRateUsWindow
	DateTime_t339033936  ___lastShowRateUsWindow_60;
	// System.Boolean GlobalStat::needShowRateApp
	bool ___needShowRateApp_61;
	// System.Int32 GlobalStat::session_num
	int32_t ___session_num_62;
	// System.Int32 GlobalStat::install
	int32_t ___install_63;
	// System.String GlobalStat::customer_id
	String_t* ___customer_id_64;
	// System.DateTime GlobalStat::session_end
	DateTime_t339033936  ___session_end_65;
	// System.DateTime GlobalStat::session_start
	DateTime_t339033936  ___session_start_66;
	// System.Int32 GlobalStat::interstitial_shown_request
	int32_t ___interstitial_shown_request_67;
	// System.Int32 GlobalStat::interstitial_index
	int32_t ___interstitial_index_68;
	// System.Double GlobalStat::money_inc_total
	double ___money_inc_total_69;
	// System.Double GlobalStat::money_dec_total
	double ___money_dec_total_70;
	// System.String GlobalStat::SocialID
	String_t* ___SocialID_71;
	// System.Double GlobalStat::money_inc_click
	double ___money_inc_click_72;
	// System.Double GlobalStat::money_inc_click_row
	double ___money_inc_click_row_73;
	// System.Double GlobalStat::bill_click_count
	double ___bill_click_count_74;
	// System.Double GlobalStat::money_inc_passive
	double ___money_inc_passive_75;
	// System.Double GlobalStat::money_inc_passive_row
	double ___money_inc_passive_row_76;
	// System.Double GlobalStat::money_inc_safe
	double ___money_inc_safe_77;
	// System.Double GlobalStat::money_inc_safe_row
	double ___money_inc_safe_row_78;
	// System.Double GlobalStat::money_inc_from_double_revenue
	double ___money_inc_from_double_revenue_79;
	// System.Double GlobalStat::money_inc_press
	double ___money_inc_press_80;
	// System.Double GlobalStat::money_inc_mall
	double ___money_inc_mall_81;
	// System.Double GlobalStat::money_inc_from_ads
	double ___money_inc_from_ads_82;
	// System.Double GlobalStat::money_inc_mmm
	double ___money_inc_mmm_83;
	// System.Double GlobalStat::money_inc_deposit
	double ___money_inc_deposit_84;
	// System.Double GlobalStat::money_inc_doublemoney
	double ___money_inc_doublemoney_85;
	// System.Double GlobalStat::money_inc_lostofmoney
	double ___money_inc_lostofmoney_86;
	// System.Double GlobalStat::money_inc_cutamelon
	double ___money_inc_cutamelon_87;
	// System.Double GlobalStat::money_inc_lottery
	double ___money_inc_lottery_88;
	// System.Double GlobalStat::money_inc_invite_friends
	double ___money_inc_invite_friends_89;
	// System.Double GlobalStat::money_inc_join_group
	double ___money_inc_join_group_90;
	// System.Double GlobalStat::money_inc_login_vk
	double ___money_inc_login_vk_91;
	// System.Double GlobalStat::money_end_total
	double ___money_end_total_92;
	// System.Double GlobalStat::revenue_total
	double ___revenue_total_93;
	// System.Int32 GlobalStat::maxBu
	int32_t ___maxBu_94;
	// System.Int32 GlobalStat::maxSu
	int32_t ___maxSu_95;
	// System.Int32 GlobalStat::maxPu
	int32_t ___maxPu_96;
	// System.Single GlobalStat::purchaseTime
	float ___purchaseTime_97;
	// System.Int32 GlobalStat::earn5k
	int32_t ___earn5k_98;
	// System.Collections.Generic.List`1<System.Int64> GlobalStat::ads_revenue_end_time_list
	List_1_t3644373851 * ___ads_revenue_end_time_list_99;
	// System.Collections.Generic.List`1<System.Int64> GlobalStat::double_revenue_end_time_list
	List_1_t3644373851 * ___double_revenue_end_time_list_100;
	// System.Collections.Generic.List`1<System.Int64> GlobalStat::twenty_revenue_end_time_list
	List_1_t3644373851 * ___twenty_revenue_end_time_list_101;
	// System.Single GlobalStat::start_loading_time
	float ___start_loading_time_102;
	// System.Int32 GlobalStat::tutor_played
	int32_t ___tutor_played_103;
	// System.Int32 GlobalStat::max_combo_chain
	int32_t ___max_combo_chain_104;
	// System.Int32 GlobalStat::gold_bill_num
	int32_t ___gold_bill_num_105;
	// System.Double GlobalStat::money_sum
	double ___money_sum_106;
	// System.Int32 GlobalStat::press
	int32_t ___press_107;
	// System.Int32 GlobalStat::mall
	int32_t ___mall_108;
	// System.Int32 GlobalStat::cell
	int32_t ___cell_109;
	// System.Int32 GlobalStat::paymentCount
	int32_t ___paymentCount_110;
	// System.Double GlobalStat::totalRealRevenueGain
	double ___totalRealRevenueGain_111;
	// VKProfileInfo GlobalStat::vkProfile
	VKProfileInfo_t3473649666 * ___vkProfile_112;
	// GlobalStat/fbProfile GlobalStat::FbProfile
	fbProfile_t675187021 * ___FbProfile_113;
	// System.DateTime GlobalStat::lastInterstitalShowDateTime
	DateTime_t339033936  ___lastInterstitalShowDateTime_114;
	// GameData GlobalStat::UpgradesData
	GameData_t2589969116 * ___UpgradesData_116;
	// UniRx.ReadOnlyReactiveProperty`1<System.Boolean> GlobalStat::ReactiveDoubleEarning
	ReadOnlyReactiveProperty_1_t3000407357 * ___ReactiveDoubleEarning_117;
	// UniRx.ReactiveProperty`1<System.String> GlobalStat::ReactiveEndTime
	ReactiveProperty_1_t1576821940 * ___ReactiveEndTime_118;
	// System.Int32 GlobalStat::resetTapConter
	int32_t ___resetTapConter_122;
	// GuiController GlobalStat::GuiController
	GuiController_t1495593751 * ___GuiController_123;
	// GameController GlobalStat::GameController
	GameController_t2782302542 * ___GameController_124;
	// TutorialPlayer GlobalStat::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_125;
	// UniRx.ReactiveProperty`1<System.Int32> GlobalStat::CurrentBillUpgrade
	ReactiveProperty_1_t3455747825 * ___CurrentBillUpgrade_126;
	// Message GlobalStat::Message
	Message_t2619578343 * ___Message_127;
	// AudioController GlobalStat::AudioController
	AudioController_t2316845042 * ___AudioController_128;
	// System.Int32[] GlobalStat::hardPacks
	Int32U5BU5D_t1809983122* ___hardPacks_129;

public:
	inline static int32_t get_offset_of_receiptsForVerify_0() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___receiptsForVerify_0)); }
	inline List_1_t265820259 * get_receiptsForVerify_0() const { return ___receiptsForVerify_0; }
	inline List_1_t265820259 ** get_address_of_receiptsForVerify_0() { return &___receiptsForVerify_0; }
	inline void set_receiptsForVerify_0(List_1_t265820259 * value)
	{
		___receiptsForVerify_0 = value;
		Il2CppCodeGenWriteBarrier(&___receiptsForVerify_0, value);
	}

	inline static int32_t get_offset_of_passiveUpgradesLevels_1() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___passiveUpgradesLevels_1)); }
	inline Int32U5BU5D_t1809983122* get_passiveUpgradesLevels_1() const { return ___passiveUpgradesLevels_1; }
	inline Int32U5BU5D_t1809983122** get_address_of_passiveUpgradesLevels_1() { return &___passiveUpgradesLevels_1; }
	inline void set_passiveUpgradesLevels_1(Int32U5BU5D_t1809983122* value)
	{
		___passiveUpgradesLevels_1 = value;
		Il2CppCodeGenWriteBarrier(&___passiveUpgradesLevels_1, value);
	}

	inline static int32_t get_offset_of_safeUpgradesLevels_2() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___safeUpgradesLevels_2)); }
	inline Int32U5BU5D_t1809983122* get_safeUpgradesLevels_2() const { return ___safeUpgradesLevels_2; }
	inline Int32U5BU5D_t1809983122** get_address_of_safeUpgradesLevels_2() { return &___safeUpgradesLevels_2; }
	inline void set_safeUpgradesLevels_2(Int32U5BU5D_t1809983122* value)
	{
		___safeUpgradesLevels_2 = value;
		Il2CppCodeGenWriteBarrier(&___safeUpgradesLevels_2, value);
	}

	inline static int32_t get_offset_of_billUpgradesLevels_3() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___billUpgradesLevels_3)); }
	inline Int32U5BU5D_t1809983122* get_billUpgradesLevels_3() const { return ___billUpgradesLevels_3; }
	inline Int32U5BU5D_t1809983122** get_address_of_billUpgradesLevels_3() { return &___billUpgradesLevels_3; }
	inline void set_billUpgradesLevels_3(Int32U5BU5D_t1809983122* value)
	{
		___billUpgradesLevels_3 = value;
		Il2CppCodeGenWriteBarrier(&___billUpgradesLevels_3, value);
	}

	inline static int32_t get_offset_of_PassiveUpgradeLevels_4() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___PassiveUpgradeLevels_4)); }
	inline List_1_t3826471388 * get_PassiveUpgradeLevels_4() const { return ___PassiveUpgradeLevels_4; }
	inline List_1_t3826471388 ** get_address_of_PassiveUpgradeLevels_4() { return &___PassiveUpgradeLevels_4; }
	inline void set_PassiveUpgradeLevels_4(List_1_t3826471388 * value)
	{
		___PassiveUpgradeLevels_4 = value;
		Il2CppCodeGenWriteBarrier(&___PassiveUpgradeLevels_4, value);
	}

	inline static int32_t get_offset_of_SafeUpgradesLevels_5() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___SafeUpgradesLevels_5)); }
	inline List_1_t3826471388 * get_SafeUpgradesLevels_5() const { return ___SafeUpgradesLevels_5; }
	inline List_1_t3826471388 ** get_address_of_SafeUpgradesLevels_5() { return &___SafeUpgradesLevels_5; }
	inline void set_SafeUpgradesLevels_5(List_1_t3826471388 * value)
	{
		___SafeUpgradesLevels_5 = value;
		Il2CppCodeGenWriteBarrier(&___SafeUpgradesLevels_5, value);
	}

	inline static int32_t get_offset_of_BillUpgradesLevels_6() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___BillUpgradesLevels_6)); }
	inline List_1_t3826471388 * get_BillUpgradesLevels_6() const { return ___BillUpgradesLevels_6; }
	inline List_1_t3826471388 ** get_address_of_BillUpgradesLevels_6() { return &___BillUpgradesLevels_6; }
	inline void set_BillUpgradesLevels_6(List_1_t3826471388 * value)
	{
		___BillUpgradesLevels_6 = value;
		Il2CppCodeGenWriteBarrier(&___BillUpgradesLevels_6, value);
	}

	inline static int32_t get_offset_of_SafeUpgradesDostavkaSerializable_7() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___SafeUpgradesDostavkaSerializable_7)); }
	inline List_1_t3644373851 * get_SafeUpgradesDostavkaSerializable_7() const { return ___SafeUpgradesDostavkaSerializable_7; }
	inline List_1_t3644373851 ** get_address_of_SafeUpgradesDostavkaSerializable_7() { return &___SafeUpgradesDostavkaSerializable_7; }
	inline void set_SafeUpgradesDostavkaSerializable_7(List_1_t3644373851 * value)
	{
		___SafeUpgradesDostavkaSerializable_7 = value;
		Il2CppCodeGenWriteBarrier(&___SafeUpgradesDostavkaSerializable_7, value);
	}

	inline static int32_t get_offset_of_PassiveUpgradesDostavkaSerializable_8() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___PassiveUpgradesDostavkaSerializable_8)); }
	inline List_1_t3644373851 * get_PassiveUpgradesDostavkaSerializable_8() const { return ___PassiveUpgradesDostavkaSerializable_8; }
	inline List_1_t3644373851 ** get_address_of_PassiveUpgradesDostavkaSerializable_8() { return &___PassiveUpgradesDostavkaSerializable_8; }
	inline void set_PassiveUpgradesDostavkaSerializable_8(List_1_t3644373851 * value)
	{
		___PassiveUpgradesDostavkaSerializable_8 = value;
		Il2CppCodeGenWriteBarrier(&___PassiveUpgradesDostavkaSerializable_8, value);
	}

	inline static int32_t get_offset_of_SafeUpgradesDostavka_9() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___SafeUpgradesDostavka_9)); }
	inline List_1_t1570757814 * get_SafeUpgradesDostavka_9() const { return ___SafeUpgradesDostavka_9; }
	inline List_1_t1570757814 ** get_address_of_SafeUpgradesDostavka_9() { return &___SafeUpgradesDostavka_9; }
	inline void set_SafeUpgradesDostavka_9(List_1_t1570757814 * value)
	{
		___SafeUpgradesDostavka_9 = value;
		Il2CppCodeGenWriteBarrier(&___SafeUpgradesDostavka_9, value);
	}

	inline static int32_t get_offset_of_SocialId_10() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___SocialId_10)); }
	inline String_t* get_SocialId_10() const { return ___SocialId_10; }
	inline String_t** get_address_of_SocialId_10() { return &___SocialId_10; }
	inline void set_SocialId_10(String_t* value)
	{
		___SocialId_10 = value;
		Il2CppCodeGenWriteBarrier(&___SocialId_10, value);
	}

	inline static int32_t get_offset_of_PassiveUpgradeDostavka_11() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___PassiveUpgradeDostavka_11)); }
	inline List_1_t1570757814 * get_PassiveUpgradeDostavka_11() const { return ___PassiveUpgradeDostavka_11; }
	inline List_1_t1570757814 ** get_address_of_PassiveUpgradeDostavka_11() { return &___PassiveUpgradeDostavka_11; }
	inline void set_PassiveUpgradeDostavka_11(List_1_t1570757814 * value)
	{
		___PassiveUpgradeDostavka_11 = value;
		Il2CppCodeGenWriteBarrier(&___PassiveUpgradeDostavka_11, value);
	}

	inline static int32_t get_offset_of_UPGLvl_12() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___UPGLvl_12)); }
	inline int32_t get_UPGLvl_12() const { return ___UPGLvl_12; }
	inline int32_t* get_address_of_UPGLvl_12() { return &___UPGLvl_12; }
	inline void set_UPGLvl_12(int32_t value)
	{
		___UPGLvl_12 = value;
	}

	inline static int32_t get_offset_of_hardMoney_13() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___hardMoney_13)); }
	inline DoubleCell_t2771715315 * get_hardMoney_13() const { return ___hardMoney_13; }
	inline DoubleCell_t2771715315 ** get_address_of_hardMoney_13() { return &___hardMoney_13; }
	inline void set_hardMoney_13(DoubleCell_t2771715315 * value)
	{
		___hardMoney_13 = value;
		Il2CppCodeGenWriteBarrier(&___hardMoney_13, value);
	}

	inline static int32_t get_offset_of_FBGroupStat_14() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___FBGroupStat_14)); }
	inline String_t* get_FBGroupStat_14() const { return ___FBGroupStat_14; }
	inline String_t** get_address_of_FBGroupStat_14() { return &___FBGroupStat_14; }
	inline void set_FBGroupStat_14(String_t* value)
	{
		___FBGroupStat_14 = value;
		Il2CppCodeGenWriteBarrier(&___FBGroupStat_14, value);
	}

	inline static int32_t get_offset_of_FBWallPostStat_15() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___FBWallPostStat_15)); }
	inline String_t* get_FBWallPostStat_15() const { return ___FBWallPostStat_15; }
	inline String_t** get_address_of_FBWallPostStat_15() { return &___FBWallPostStat_15; }
	inline void set_FBWallPostStat_15(String_t* value)
	{
		___FBWallPostStat_15 = value;
		Il2CppCodeGenWriteBarrier(&___FBWallPostStat_15, value);
	}

	inline static int32_t get_offset_of_VKLogStat_16() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___VKLogStat_16)); }
	inline bool get_VKLogStat_16() const { return ___VKLogStat_16; }
	inline bool* get_address_of_VKLogStat_16() { return &___VKLogStat_16; }
	inline void set_VKLogStat_16(bool value)
	{
		___VKLogStat_16 = value;
	}

	inline static int32_t get_offset_of_fbFirstConnect_17() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___fbFirstConnect_17)); }
	inline bool get_fbFirstConnect_17() const { return ___fbFirstConnect_17; }
	inline bool* get_address_of_fbFirstConnect_17() { return &___fbFirstConnect_17; }
	inline void set_fbFirstConnect_17(bool value)
	{
		___fbFirstConnect_17 = value;
	}

	inline static int32_t get_offset_of_hard_18() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___hard_18)); }
	inline double get_hard_18() const { return ___hard_18; }
	inline double* get_address_of_hard_18() { return &___hard_18; }
	inline void set_hard_18(double value)
	{
		___hard_18 = value;
	}

	inline static int32_t get_offset_of_vkIDNotEnabled_19() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___vkIDNotEnabled_19)); }
	inline List_1_t1765447871 * get_vkIDNotEnabled_19() const { return ___vkIDNotEnabled_19; }
	inline List_1_t1765447871 ** get_address_of_vkIDNotEnabled_19() { return &___vkIDNotEnabled_19; }
	inline void set_vkIDNotEnabled_19(List_1_t1765447871 * value)
	{
		___vkIDNotEnabled_19 = value;
		Il2CppCodeGenWriteBarrier(&___vkIDNotEnabled_19, value);
	}

	inline static int32_t get_offset_of_fastDelivery_20() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___fastDelivery_20)); }
	inline bool get_fastDelivery_20() const { return ___fastDelivery_20; }
	inline bool* get_address_of_fastDelivery_20() { return &___fastDelivery_20; }
	inline void set_fastDelivery_20(bool value)
	{
		___fastDelivery_20 = value;
	}

	inline static int32_t get_offset_of_EventSocial_21() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___EventSocial_21)); }
	inline String_t* get_EventSocial_21() const { return ___EventSocial_21; }
	inline String_t** get_address_of_EventSocial_21() { return &___EventSocial_21; }
	inline void set_EventSocial_21(String_t* value)
	{
		___EventSocial_21 = value;
		Il2CppCodeGenWriteBarrier(&___EventSocial_21, value);
	}

	inline static int32_t get_offset_of_LocalizationStr_22() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___LocalizationStr_22)); }
	inline String_t* get_LocalizationStr_22() const { return ___LocalizationStr_22; }
	inline String_t** get_address_of_LocalizationStr_22() { return &___LocalizationStr_22; }
	inline void set_LocalizationStr_22(String_t* value)
	{
		___LocalizationStr_22 = value;
		Il2CppCodeGenWriteBarrier(&___LocalizationStr_22, value);
	}

	inline static int32_t get_offset_of_soundEnabled_23() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___soundEnabled_23)); }
	inline bool get_soundEnabled_23() const { return ___soundEnabled_23; }
	inline bool* get_address_of_soundEnabled_23() { return &___soundEnabled_23; }
	inline void set_soundEnabled_23(bool value)
	{
		___soundEnabled_23 = value;
	}

	inline static int32_t get_offset_of_SoundEnabled_24() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___SoundEnabled_24)); }
	inline ReadOnlyReactiveProperty_1_t3000407357 * get_SoundEnabled_24() const { return ___SoundEnabled_24; }
	inline ReadOnlyReactiveProperty_1_t3000407357 ** get_address_of_SoundEnabled_24() { return &___SoundEnabled_24; }
	inline void set_SoundEnabled_24(ReadOnlyReactiveProperty_1_t3000407357 * value)
	{
		___SoundEnabled_24 = value;
		Il2CppCodeGenWriteBarrier(&___SoundEnabled_24, value);
	}

	inline static int32_t get_offset_of_ReactiveMoney_25() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___ReactiveMoney_25)); }
	inline Cell_1_t716614246 * get_ReactiveMoney_25() const { return ___ReactiveMoney_25; }
	inline Cell_1_t716614246 ** get_address_of_ReactiveMoney_25() { return &___ReactiveMoney_25; }
	inline void set_ReactiveMoney_25(Cell_1_t716614246 * value)
	{
		___ReactiveMoney_25 = value;
		Il2CppCodeGenWriteBarrier(&___ReactiveMoney_25, value);
	}

	inline static int32_t get_offset_of_lastBoughtUpgrade_26() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___lastBoughtUpgrade_26)); }
	inline String_t* get_lastBoughtUpgrade_26() const { return ___lastBoughtUpgrade_26; }
	inline String_t** get_address_of_lastBoughtUpgrade_26() { return &___lastBoughtUpgrade_26; }
	inline void set_lastBoughtUpgrade_26(String_t* value)
	{
		___lastBoughtUpgrade_26 = value;
		Il2CppCodeGenWriteBarrier(&___lastBoughtUpgrade_26, value);
	}

	inline static int32_t get_offset_of_money_27() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_27)); }
	inline double get_money_27() const { return ___money_27; }
	inline double* get_address_of_money_27() { return &___money_27; }
	inline void set_money_27(double value)
	{
		___money_27 = value;
	}

	inline static int32_t get_offset_of_countr_28() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___countr_28)); }
	inline double get_countr_28() const { return ___countr_28; }
	inline double* get_address_of_countr_28() { return &___countr_28; }
	inline void set_countr_28(double value)
	{
		___countr_28 = value;
	}

	inline static int32_t get_offset_of_doubleEarning_29() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___doubleEarning_29)); }
	inline bool get_doubleEarning_29() const { return ___doubleEarning_29; }
	inline bool* get_address_of_doubleEarning_29() { return &___doubleEarning_29; }
	inline void set_doubleEarning_29(bool value)
	{
		___doubleEarning_29 = value;
	}

	inline static int32_t get_offset_of_CounRealMoney_30() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___CounRealMoney_30)); }
	inline double get_CounRealMoney_30() const { return ___CounRealMoney_30; }
	inline double* get_address_of_CounRealMoney_30() { return &___CounRealMoney_30; }
	inline void set_CounRealMoney_30(double value)
	{
		___CounRealMoney_30 = value;
	}

	inline static int32_t get_offset_of_endTime_31() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___endTime_31)); }
	inline String_t* get_endTime_31() const { return ___endTime_31; }
	inline String_t** get_address_of_endTime_31() { return &___endTime_31; }
	inline void set_endTime_31(String_t* value)
	{
		___endTime_31 = value;
		Il2CppCodeGenWriteBarrier(&___endTime_31, value);
	}

	inline static int32_t get_offset_of_mmm_32() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___mmm_32)); }
	inline bool get_mmm_32() const { return ___mmm_32; }
	inline bool* get_address_of_mmm_32() { return &___mmm_32; }
	inline void set_mmm_32(bool value)
	{
		___mmm_32 = value;
	}

	inline static int32_t get_offset_of_free_mmm_33() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___free_mmm_33)); }
	inline bool get_free_mmm_33() const { return ___free_mmm_33; }
	inline bool* get_address_of_free_mmm_33() { return &___free_mmm_33; }
	inline void set_free_mmm_33(bool value)
	{
		___free_mmm_33 = value;
	}

	inline static int32_t get_offset_of__passiveIncome_34() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ____passiveIncome_34)); }
	inline double get__passiveIncome_34() const { return ____passiveIncome_34; }
	inline double* get_address_of__passiveIncome_34() { return &____passiveIncome_34; }
	inline void set__passiveIncome_34(double value)
	{
		____passiveIncome_34 = value;
	}

	inline static int32_t get_offset_of_lastFriendInviteTick_35() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___lastFriendInviteTick_35)); }
	inline DateTime_t339033936  get_lastFriendInviteTick_35() const { return ___lastFriendInviteTick_35; }
	inline DateTime_t339033936 * get_address_of_lastFriendInviteTick_35() { return &___lastFriendInviteTick_35; }
	inline void set_lastFriendInviteTick_35(DateTime_t339033936  value)
	{
		___lastFriendInviteTick_35 = value;
	}

	inline static int32_t get_offset_of_friendInviteAvailable_36() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___friendInviteAvailable_36)); }
	inline int32_t get_friendInviteAvailable_36() const { return ___friendInviteAvailable_36; }
	inline int32_t* get_address_of_friendInviteAvailable_36() { return &___friendInviteAvailable_36; }
	inline void set_friendInviteAvailable_36(int32_t value)
	{
		___friendInviteAvailable_36 = value;
	}

	inline static int32_t get_offset_of_totalFriendsInvited_37() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___totalFriendsInvited_37)); }
	inline int32_t get_totalFriendsInvited_37() const { return ___totalFriendsInvited_37; }
	inline int32_t* get_address_of_totalFriendsInvited_37() { return &___totalFriendsInvited_37; }
	inline void set_totalFriendsInvited_37(int32_t value)
	{
		___totalFriendsInvited_37 = value;
	}

	inline static int32_t get_offset_of_invitedVkIDs_38() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___invitedVkIDs_38)); }
	inline List_1_t1765447871 * get_invitedVkIDs_38() const { return ___invitedVkIDs_38; }
	inline List_1_t1765447871 ** get_address_of_invitedVkIDs_38() { return &___invitedVkIDs_38; }
	inline void set_invitedVkIDs_38(List_1_t1765447871 * value)
	{
		___invitedVkIDs_38 = value;
		Il2CppCodeGenWriteBarrier(&___invitedVkIDs_38, value);
	}

	inline static int32_t get_offset_of_InvitedFbIDs_39() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___InvitedFbIDs_39)); }
	inline List_1_t1765447871 * get_InvitedFbIDs_39() const { return ___InvitedFbIDs_39; }
	inline List_1_t1765447871 ** get_address_of_InvitedFbIDs_39() { return &___InvitedFbIDs_39; }
	inline void set_InvitedFbIDs_39(List_1_t1765447871 * value)
	{
		___InvitedFbIDs_39 = value;
		Il2CppCodeGenWriteBarrier(&___InvitedFbIDs_39, value);
	}

	inline static int32_t get_offset_of_InvVkListOfId_40() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___InvVkListOfId_40)); }
	inline List_1_t1765447871 * get_InvVkListOfId_40() const { return ___InvVkListOfId_40; }
	inline List_1_t1765447871 ** get_address_of_InvVkListOfId_40() { return &___InvVkListOfId_40; }
	inline void set_InvVkListOfId_40(List_1_t1765447871 * value)
	{
		___InvVkListOfId_40 = value;
		Il2CppCodeGenWriteBarrier(&___InvVkListOfId_40, value);
	}

	inline static int32_t get_offset_of_buttonID_41() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___buttonID_41)); }
	inline int32_t get_buttonID_41() const { return ___buttonID_41; }
	inline int32_t* get_address_of_buttonID_41() { return &___buttonID_41; }
	inline void set_buttonID_41(int32_t value)
	{
		___buttonID_41 = value;
	}

	inline static int32_t get_offset_of_ButtonTitleId_42() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___ButtonTitleId_42)); }
	inline String_t* get_ButtonTitleId_42() const { return ___ButtonTitleId_42; }
	inline String_t** get_address_of_ButtonTitleId_42() { return &___ButtonTitleId_42; }
	inline void set_ButtonTitleId_42(String_t* value)
	{
		___ButtonTitleId_42 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonTitleId_42, value);
	}

	inline static int32_t get_offset_of_PasInc_43() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___PasInc_43)); }
	inline ReactiveProperty_1_t1142849652 * get_PasInc_43() const { return ___PasInc_43; }
	inline ReactiveProperty_1_t1142849652 ** get_address_of_PasInc_43() { return &___PasInc_43; }
	inline void set_PasInc_43(ReactiveProperty_1_t1142849652 * value)
	{
		___PasInc_43 = value;
		Il2CppCodeGenWriteBarrier(&___PasInc_43, value);
	}

	inline static int32_t get_offset_of__bucketIncome_44() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ____bucketIncome_44)); }
	inline double get__bucketIncome_44() const { return ____bucketIncome_44; }
	inline double* get_address_of__bucketIncome_44() { return &____bucketIncome_44; }
	inline void set__bucketIncome_44(double value)
	{
		____bucketIncome_44 = value;
	}

	inline static int32_t get_offset_of__bucketSize_45() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ____bucketSize_45)); }
	inline double get__bucketSize_45() const { return ____bucketSize_45; }
	inline double* get_address_of__bucketSize_45() { return &____bucketSize_45; }
	inline void set__bucketSize_45(double value)
	{
		____bucketSize_45 = value;
	}

	inline static int32_t get_offset_of_currentBucket_46() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___currentBucket_46)); }
	inline double get_currentBucket_46() const { return ___currentBucket_46; }
	inline double* get_address_of_currentBucket_46() { return &___currentBucket_46; }
	inline void set_currentBucket_46(double value)
	{
		___currentBucket_46 = value;
	}

	inline static int32_t get_offset_of_savingStartTime_47() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___savingStartTime_47)); }
	inline int64_t get_savingStartTime_47() const { return ___savingStartTime_47; }
	inline int64_t* get_address_of_savingStartTime_47() { return &___savingStartTime_47; }
	inline void set_savingStartTime_47(int64_t value)
	{
		___savingStartTime_47 = value;
	}

	inline static int32_t get_offset_of__achievSize_48() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ____achievSize_48)); }
	inline double get__achievSize_48() const { return ____achievSize_48; }
	inline double* get_address_of__achievSize_48() { return &____achievSize_48; }
	inline void set__achievSize_48(double value)
	{
		____achievSize_48 = value;
	}

	inline static int32_t get_offset_of_currentAchiev_49() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___currentAchiev_49)); }
	inline double get_currentAchiev_49() const { return ___currentAchiev_49; }
	inline double* get_address_of_currentAchiev_49() { return &___currentAchiev_49; }
	inline void set_currentAchiev_49(double value)
	{
		___currentAchiev_49 = value;
	}

	inline static int32_t get_offset_of__achivIncom_50() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ____achivIncom_50)); }
	inline double get__achivIncom_50() const { return ____achivIncom_50; }
	inline double* get_address_of__achivIncom_50() { return &____achivIncom_50; }
	inline void set__achivIncom_50(double value)
	{
		____achivIncom_50 = value;
	}

	inline static int32_t get_offset_of_inappPurchased_51() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___inappPurchased_51)); }
	inline bool get_inappPurchased_51() const { return ___inappPurchased_51; }
	inline bool* get_address_of_inappPurchased_51() { return &___inappPurchased_51; }
	inline void set_inappPurchased_51(bool value)
	{
		___inappPurchased_51 = value;
	}

	inline static int32_t get_offset_of_lastAdShown_52() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___lastAdShown_52)); }
	inline float get_lastAdShown_52() const { return ___lastAdShown_52; }
	inline float* get_address_of_lastAdShown_52() { return &___lastAdShown_52; }
	inline void set_lastAdShown_52(float value)
	{
		___lastAdShown_52 = value;
	}

	inline static int32_t get_offset_of_adsShown_53() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___adsShown_53)); }
	inline int32_t get_adsShown_53() const { return ___adsShown_53; }
	inline int32_t* get_address_of_adsShown_53() { return &___adsShown_53; }
	inline void set_adsShown_53(int32_t value)
	{
		___adsShown_53 = value;
	}

	inline static int32_t get_offset_of_friendInvitedFB_54() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___friendInvitedFB_54)); }
	inline bool get_friendInvitedFB_54() const { return ___friendInvitedFB_54; }
	inline bool* get_address_of_friendInvitedFB_54() { return &___friendInvitedFB_54; }
	inline void set_friendInvitedFB_54(bool value)
	{
		___friendInvitedFB_54 = value;
	}

	inline static int32_t get_offset_of_vkWalPost_55() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___vkWalPost_55)); }
	inline bool get_vkWalPost_55() const { return ___vkWalPost_55; }
	inline bool* get_address_of_vkWalPost_55() { return &___vkWalPost_55; }
	inline void set_vkWalPost_55(bool value)
	{
		___vkWalPost_55 = value;
	}

	inline static int32_t get_offset_of_vkLoginRewardApplied_56() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___vkLoginRewardApplied_56)); }
	inline bool get_vkLoginRewardApplied_56() const { return ___vkLoginRewardApplied_56; }
	inline bool* get_address_of_vkLoginRewardApplied_56() { return &___vkLoginRewardApplied_56; }
	inline void set_vkLoginRewardApplied_56(bool value)
	{
		___vkLoginRewardApplied_56 = value;
	}

	inline static int32_t get_offset_of_vkJoinRewardApplied_57() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___vkJoinRewardApplied_57)); }
	inline bool get_vkJoinRewardApplied_57() const { return ___vkJoinRewardApplied_57; }
	inline bool* get_address_of_vkJoinRewardApplied_57() { return &___vkJoinRewardApplied_57; }
	inline void set_vkJoinRewardApplied_57(bool value)
	{
		___vkJoinRewardApplied_57 = value;
	}

	inline static int32_t get_offset_of_launches_58() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___launches_58)); }
	inline int32_t get_launches_58() const { return ___launches_58; }
	inline int32_t* get_address_of_launches_58() { return &___launches_58; }
	inline void set_launches_58(int32_t value)
	{
		___launches_58 = value;
	}

	inline static int32_t get_offset_of_showRateApp_59() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___showRateApp_59)); }
	inline bool get_showRateApp_59() const { return ___showRateApp_59; }
	inline bool* get_address_of_showRateApp_59() { return &___showRateApp_59; }
	inline void set_showRateApp_59(bool value)
	{
		___showRateApp_59 = value;
	}

	inline static int32_t get_offset_of_lastShowRateUsWindow_60() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___lastShowRateUsWindow_60)); }
	inline DateTime_t339033936  get_lastShowRateUsWindow_60() const { return ___lastShowRateUsWindow_60; }
	inline DateTime_t339033936 * get_address_of_lastShowRateUsWindow_60() { return &___lastShowRateUsWindow_60; }
	inline void set_lastShowRateUsWindow_60(DateTime_t339033936  value)
	{
		___lastShowRateUsWindow_60 = value;
	}

	inline static int32_t get_offset_of_needShowRateApp_61() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___needShowRateApp_61)); }
	inline bool get_needShowRateApp_61() const { return ___needShowRateApp_61; }
	inline bool* get_address_of_needShowRateApp_61() { return &___needShowRateApp_61; }
	inline void set_needShowRateApp_61(bool value)
	{
		___needShowRateApp_61 = value;
	}

	inline static int32_t get_offset_of_session_num_62() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___session_num_62)); }
	inline int32_t get_session_num_62() const { return ___session_num_62; }
	inline int32_t* get_address_of_session_num_62() { return &___session_num_62; }
	inline void set_session_num_62(int32_t value)
	{
		___session_num_62 = value;
	}

	inline static int32_t get_offset_of_install_63() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___install_63)); }
	inline int32_t get_install_63() const { return ___install_63; }
	inline int32_t* get_address_of_install_63() { return &___install_63; }
	inline void set_install_63(int32_t value)
	{
		___install_63 = value;
	}

	inline static int32_t get_offset_of_customer_id_64() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___customer_id_64)); }
	inline String_t* get_customer_id_64() const { return ___customer_id_64; }
	inline String_t** get_address_of_customer_id_64() { return &___customer_id_64; }
	inline void set_customer_id_64(String_t* value)
	{
		___customer_id_64 = value;
		Il2CppCodeGenWriteBarrier(&___customer_id_64, value);
	}

	inline static int32_t get_offset_of_session_end_65() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___session_end_65)); }
	inline DateTime_t339033936  get_session_end_65() const { return ___session_end_65; }
	inline DateTime_t339033936 * get_address_of_session_end_65() { return &___session_end_65; }
	inline void set_session_end_65(DateTime_t339033936  value)
	{
		___session_end_65 = value;
	}

	inline static int32_t get_offset_of_session_start_66() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___session_start_66)); }
	inline DateTime_t339033936  get_session_start_66() const { return ___session_start_66; }
	inline DateTime_t339033936 * get_address_of_session_start_66() { return &___session_start_66; }
	inline void set_session_start_66(DateTime_t339033936  value)
	{
		___session_start_66 = value;
	}

	inline static int32_t get_offset_of_interstitial_shown_request_67() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___interstitial_shown_request_67)); }
	inline int32_t get_interstitial_shown_request_67() const { return ___interstitial_shown_request_67; }
	inline int32_t* get_address_of_interstitial_shown_request_67() { return &___interstitial_shown_request_67; }
	inline void set_interstitial_shown_request_67(int32_t value)
	{
		___interstitial_shown_request_67 = value;
	}

	inline static int32_t get_offset_of_interstitial_index_68() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___interstitial_index_68)); }
	inline int32_t get_interstitial_index_68() const { return ___interstitial_index_68; }
	inline int32_t* get_address_of_interstitial_index_68() { return &___interstitial_index_68; }
	inline void set_interstitial_index_68(int32_t value)
	{
		___interstitial_index_68 = value;
	}

	inline static int32_t get_offset_of_money_inc_total_69() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_total_69)); }
	inline double get_money_inc_total_69() const { return ___money_inc_total_69; }
	inline double* get_address_of_money_inc_total_69() { return &___money_inc_total_69; }
	inline void set_money_inc_total_69(double value)
	{
		___money_inc_total_69 = value;
	}

	inline static int32_t get_offset_of_money_dec_total_70() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_dec_total_70)); }
	inline double get_money_dec_total_70() const { return ___money_dec_total_70; }
	inline double* get_address_of_money_dec_total_70() { return &___money_dec_total_70; }
	inline void set_money_dec_total_70(double value)
	{
		___money_dec_total_70 = value;
	}

	inline static int32_t get_offset_of_SocialID_71() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___SocialID_71)); }
	inline String_t* get_SocialID_71() const { return ___SocialID_71; }
	inline String_t** get_address_of_SocialID_71() { return &___SocialID_71; }
	inline void set_SocialID_71(String_t* value)
	{
		___SocialID_71 = value;
		Il2CppCodeGenWriteBarrier(&___SocialID_71, value);
	}

	inline static int32_t get_offset_of_money_inc_click_72() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_click_72)); }
	inline double get_money_inc_click_72() const { return ___money_inc_click_72; }
	inline double* get_address_of_money_inc_click_72() { return &___money_inc_click_72; }
	inline void set_money_inc_click_72(double value)
	{
		___money_inc_click_72 = value;
	}

	inline static int32_t get_offset_of_money_inc_click_row_73() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_click_row_73)); }
	inline double get_money_inc_click_row_73() const { return ___money_inc_click_row_73; }
	inline double* get_address_of_money_inc_click_row_73() { return &___money_inc_click_row_73; }
	inline void set_money_inc_click_row_73(double value)
	{
		___money_inc_click_row_73 = value;
	}

	inline static int32_t get_offset_of_bill_click_count_74() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___bill_click_count_74)); }
	inline double get_bill_click_count_74() const { return ___bill_click_count_74; }
	inline double* get_address_of_bill_click_count_74() { return &___bill_click_count_74; }
	inline void set_bill_click_count_74(double value)
	{
		___bill_click_count_74 = value;
	}

	inline static int32_t get_offset_of_money_inc_passive_75() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_passive_75)); }
	inline double get_money_inc_passive_75() const { return ___money_inc_passive_75; }
	inline double* get_address_of_money_inc_passive_75() { return &___money_inc_passive_75; }
	inline void set_money_inc_passive_75(double value)
	{
		___money_inc_passive_75 = value;
	}

	inline static int32_t get_offset_of_money_inc_passive_row_76() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_passive_row_76)); }
	inline double get_money_inc_passive_row_76() const { return ___money_inc_passive_row_76; }
	inline double* get_address_of_money_inc_passive_row_76() { return &___money_inc_passive_row_76; }
	inline void set_money_inc_passive_row_76(double value)
	{
		___money_inc_passive_row_76 = value;
	}

	inline static int32_t get_offset_of_money_inc_safe_77() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_safe_77)); }
	inline double get_money_inc_safe_77() const { return ___money_inc_safe_77; }
	inline double* get_address_of_money_inc_safe_77() { return &___money_inc_safe_77; }
	inline void set_money_inc_safe_77(double value)
	{
		___money_inc_safe_77 = value;
	}

	inline static int32_t get_offset_of_money_inc_safe_row_78() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_safe_row_78)); }
	inline double get_money_inc_safe_row_78() const { return ___money_inc_safe_row_78; }
	inline double* get_address_of_money_inc_safe_row_78() { return &___money_inc_safe_row_78; }
	inline void set_money_inc_safe_row_78(double value)
	{
		___money_inc_safe_row_78 = value;
	}

	inline static int32_t get_offset_of_money_inc_from_double_revenue_79() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_from_double_revenue_79)); }
	inline double get_money_inc_from_double_revenue_79() const { return ___money_inc_from_double_revenue_79; }
	inline double* get_address_of_money_inc_from_double_revenue_79() { return &___money_inc_from_double_revenue_79; }
	inline void set_money_inc_from_double_revenue_79(double value)
	{
		___money_inc_from_double_revenue_79 = value;
	}

	inline static int32_t get_offset_of_money_inc_press_80() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_press_80)); }
	inline double get_money_inc_press_80() const { return ___money_inc_press_80; }
	inline double* get_address_of_money_inc_press_80() { return &___money_inc_press_80; }
	inline void set_money_inc_press_80(double value)
	{
		___money_inc_press_80 = value;
	}

	inline static int32_t get_offset_of_money_inc_mall_81() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_mall_81)); }
	inline double get_money_inc_mall_81() const { return ___money_inc_mall_81; }
	inline double* get_address_of_money_inc_mall_81() { return &___money_inc_mall_81; }
	inline void set_money_inc_mall_81(double value)
	{
		___money_inc_mall_81 = value;
	}

	inline static int32_t get_offset_of_money_inc_from_ads_82() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_from_ads_82)); }
	inline double get_money_inc_from_ads_82() const { return ___money_inc_from_ads_82; }
	inline double* get_address_of_money_inc_from_ads_82() { return &___money_inc_from_ads_82; }
	inline void set_money_inc_from_ads_82(double value)
	{
		___money_inc_from_ads_82 = value;
	}

	inline static int32_t get_offset_of_money_inc_mmm_83() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_mmm_83)); }
	inline double get_money_inc_mmm_83() const { return ___money_inc_mmm_83; }
	inline double* get_address_of_money_inc_mmm_83() { return &___money_inc_mmm_83; }
	inline void set_money_inc_mmm_83(double value)
	{
		___money_inc_mmm_83 = value;
	}

	inline static int32_t get_offset_of_money_inc_deposit_84() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_deposit_84)); }
	inline double get_money_inc_deposit_84() const { return ___money_inc_deposit_84; }
	inline double* get_address_of_money_inc_deposit_84() { return &___money_inc_deposit_84; }
	inline void set_money_inc_deposit_84(double value)
	{
		___money_inc_deposit_84 = value;
	}

	inline static int32_t get_offset_of_money_inc_doublemoney_85() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_doublemoney_85)); }
	inline double get_money_inc_doublemoney_85() const { return ___money_inc_doublemoney_85; }
	inline double* get_address_of_money_inc_doublemoney_85() { return &___money_inc_doublemoney_85; }
	inline void set_money_inc_doublemoney_85(double value)
	{
		___money_inc_doublemoney_85 = value;
	}

	inline static int32_t get_offset_of_money_inc_lostofmoney_86() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_lostofmoney_86)); }
	inline double get_money_inc_lostofmoney_86() const { return ___money_inc_lostofmoney_86; }
	inline double* get_address_of_money_inc_lostofmoney_86() { return &___money_inc_lostofmoney_86; }
	inline void set_money_inc_lostofmoney_86(double value)
	{
		___money_inc_lostofmoney_86 = value;
	}

	inline static int32_t get_offset_of_money_inc_cutamelon_87() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_cutamelon_87)); }
	inline double get_money_inc_cutamelon_87() const { return ___money_inc_cutamelon_87; }
	inline double* get_address_of_money_inc_cutamelon_87() { return &___money_inc_cutamelon_87; }
	inline void set_money_inc_cutamelon_87(double value)
	{
		___money_inc_cutamelon_87 = value;
	}

	inline static int32_t get_offset_of_money_inc_lottery_88() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_lottery_88)); }
	inline double get_money_inc_lottery_88() const { return ___money_inc_lottery_88; }
	inline double* get_address_of_money_inc_lottery_88() { return &___money_inc_lottery_88; }
	inline void set_money_inc_lottery_88(double value)
	{
		___money_inc_lottery_88 = value;
	}

	inline static int32_t get_offset_of_money_inc_invite_friends_89() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_invite_friends_89)); }
	inline double get_money_inc_invite_friends_89() const { return ___money_inc_invite_friends_89; }
	inline double* get_address_of_money_inc_invite_friends_89() { return &___money_inc_invite_friends_89; }
	inline void set_money_inc_invite_friends_89(double value)
	{
		___money_inc_invite_friends_89 = value;
	}

	inline static int32_t get_offset_of_money_inc_join_group_90() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_join_group_90)); }
	inline double get_money_inc_join_group_90() const { return ___money_inc_join_group_90; }
	inline double* get_address_of_money_inc_join_group_90() { return &___money_inc_join_group_90; }
	inline void set_money_inc_join_group_90(double value)
	{
		___money_inc_join_group_90 = value;
	}

	inline static int32_t get_offset_of_money_inc_login_vk_91() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_inc_login_vk_91)); }
	inline double get_money_inc_login_vk_91() const { return ___money_inc_login_vk_91; }
	inline double* get_address_of_money_inc_login_vk_91() { return &___money_inc_login_vk_91; }
	inline void set_money_inc_login_vk_91(double value)
	{
		___money_inc_login_vk_91 = value;
	}

	inline static int32_t get_offset_of_money_end_total_92() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_end_total_92)); }
	inline double get_money_end_total_92() const { return ___money_end_total_92; }
	inline double* get_address_of_money_end_total_92() { return &___money_end_total_92; }
	inline void set_money_end_total_92(double value)
	{
		___money_end_total_92 = value;
	}

	inline static int32_t get_offset_of_revenue_total_93() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___revenue_total_93)); }
	inline double get_revenue_total_93() const { return ___revenue_total_93; }
	inline double* get_address_of_revenue_total_93() { return &___revenue_total_93; }
	inline void set_revenue_total_93(double value)
	{
		___revenue_total_93 = value;
	}

	inline static int32_t get_offset_of_maxBu_94() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___maxBu_94)); }
	inline int32_t get_maxBu_94() const { return ___maxBu_94; }
	inline int32_t* get_address_of_maxBu_94() { return &___maxBu_94; }
	inline void set_maxBu_94(int32_t value)
	{
		___maxBu_94 = value;
	}

	inline static int32_t get_offset_of_maxSu_95() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___maxSu_95)); }
	inline int32_t get_maxSu_95() const { return ___maxSu_95; }
	inline int32_t* get_address_of_maxSu_95() { return &___maxSu_95; }
	inline void set_maxSu_95(int32_t value)
	{
		___maxSu_95 = value;
	}

	inline static int32_t get_offset_of_maxPu_96() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___maxPu_96)); }
	inline int32_t get_maxPu_96() const { return ___maxPu_96; }
	inline int32_t* get_address_of_maxPu_96() { return &___maxPu_96; }
	inline void set_maxPu_96(int32_t value)
	{
		___maxPu_96 = value;
	}

	inline static int32_t get_offset_of_purchaseTime_97() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___purchaseTime_97)); }
	inline float get_purchaseTime_97() const { return ___purchaseTime_97; }
	inline float* get_address_of_purchaseTime_97() { return &___purchaseTime_97; }
	inline void set_purchaseTime_97(float value)
	{
		___purchaseTime_97 = value;
	}

	inline static int32_t get_offset_of_earn5k_98() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___earn5k_98)); }
	inline int32_t get_earn5k_98() const { return ___earn5k_98; }
	inline int32_t* get_address_of_earn5k_98() { return &___earn5k_98; }
	inline void set_earn5k_98(int32_t value)
	{
		___earn5k_98 = value;
	}

	inline static int32_t get_offset_of_ads_revenue_end_time_list_99() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___ads_revenue_end_time_list_99)); }
	inline List_1_t3644373851 * get_ads_revenue_end_time_list_99() const { return ___ads_revenue_end_time_list_99; }
	inline List_1_t3644373851 ** get_address_of_ads_revenue_end_time_list_99() { return &___ads_revenue_end_time_list_99; }
	inline void set_ads_revenue_end_time_list_99(List_1_t3644373851 * value)
	{
		___ads_revenue_end_time_list_99 = value;
		Il2CppCodeGenWriteBarrier(&___ads_revenue_end_time_list_99, value);
	}

	inline static int32_t get_offset_of_double_revenue_end_time_list_100() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___double_revenue_end_time_list_100)); }
	inline List_1_t3644373851 * get_double_revenue_end_time_list_100() const { return ___double_revenue_end_time_list_100; }
	inline List_1_t3644373851 ** get_address_of_double_revenue_end_time_list_100() { return &___double_revenue_end_time_list_100; }
	inline void set_double_revenue_end_time_list_100(List_1_t3644373851 * value)
	{
		___double_revenue_end_time_list_100 = value;
		Il2CppCodeGenWriteBarrier(&___double_revenue_end_time_list_100, value);
	}

	inline static int32_t get_offset_of_twenty_revenue_end_time_list_101() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___twenty_revenue_end_time_list_101)); }
	inline List_1_t3644373851 * get_twenty_revenue_end_time_list_101() const { return ___twenty_revenue_end_time_list_101; }
	inline List_1_t3644373851 ** get_address_of_twenty_revenue_end_time_list_101() { return &___twenty_revenue_end_time_list_101; }
	inline void set_twenty_revenue_end_time_list_101(List_1_t3644373851 * value)
	{
		___twenty_revenue_end_time_list_101 = value;
		Il2CppCodeGenWriteBarrier(&___twenty_revenue_end_time_list_101, value);
	}

	inline static int32_t get_offset_of_start_loading_time_102() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___start_loading_time_102)); }
	inline float get_start_loading_time_102() const { return ___start_loading_time_102; }
	inline float* get_address_of_start_loading_time_102() { return &___start_loading_time_102; }
	inline void set_start_loading_time_102(float value)
	{
		___start_loading_time_102 = value;
	}

	inline static int32_t get_offset_of_tutor_played_103() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___tutor_played_103)); }
	inline int32_t get_tutor_played_103() const { return ___tutor_played_103; }
	inline int32_t* get_address_of_tutor_played_103() { return &___tutor_played_103; }
	inline void set_tutor_played_103(int32_t value)
	{
		___tutor_played_103 = value;
	}

	inline static int32_t get_offset_of_max_combo_chain_104() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___max_combo_chain_104)); }
	inline int32_t get_max_combo_chain_104() const { return ___max_combo_chain_104; }
	inline int32_t* get_address_of_max_combo_chain_104() { return &___max_combo_chain_104; }
	inline void set_max_combo_chain_104(int32_t value)
	{
		___max_combo_chain_104 = value;
	}

	inline static int32_t get_offset_of_gold_bill_num_105() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___gold_bill_num_105)); }
	inline int32_t get_gold_bill_num_105() const { return ___gold_bill_num_105; }
	inline int32_t* get_address_of_gold_bill_num_105() { return &___gold_bill_num_105; }
	inline void set_gold_bill_num_105(int32_t value)
	{
		___gold_bill_num_105 = value;
	}

	inline static int32_t get_offset_of_money_sum_106() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___money_sum_106)); }
	inline double get_money_sum_106() const { return ___money_sum_106; }
	inline double* get_address_of_money_sum_106() { return &___money_sum_106; }
	inline void set_money_sum_106(double value)
	{
		___money_sum_106 = value;
	}

	inline static int32_t get_offset_of_press_107() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___press_107)); }
	inline int32_t get_press_107() const { return ___press_107; }
	inline int32_t* get_address_of_press_107() { return &___press_107; }
	inline void set_press_107(int32_t value)
	{
		___press_107 = value;
	}

	inline static int32_t get_offset_of_mall_108() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___mall_108)); }
	inline int32_t get_mall_108() const { return ___mall_108; }
	inline int32_t* get_address_of_mall_108() { return &___mall_108; }
	inline void set_mall_108(int32_t value)
	{
		___mall_108 = value;
	}

	inline static int32_t get_offset_of_cell_109() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___cell_109)); }
	inline int32_t get_cell_109() const { return ___cell_109; }
	inline int32_t* get_address_of_cell_109() { return &___cell_109; }
	inline void set_cell_109(int32_t value)
	{
		___cell_109 = value;
	}

	inline static int32_t get_offset_of_paymentCount_110() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___paymentCount_110)); }
	inline int32_t get_paymentCount_110() const { return ___paymentCount_110; }
	inline int32_t* get_address_of_paymentCount_110() { return &___paymentCount_110; }
	inline void set_paymentCount_110(int32_t value)
	{
		___paymentCount_110 = value;
	}

	inline static int32_t get_offset_of_totalRealRevenueGain_111() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___totalRealRevenueGain_111)); }
	inline double get_totalRealRevenueGain_111() const { return ___totalRealRevenueGain_111; }
	inline double* get_address_of_totalRealRevenueGain_111() { return &___totalRealRevenueGain_111; }
	inline void set_totalRealRevenueGain_111(double value)
	{
		___totalRealRevenueGain_111 = value;
	}

	inline static int32_t get_offset_of_vkProfile_112() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___vkProfile_112)); }
	inline VKProfileInfo_t3473649666 * get_vkProfile_112() const { return ___vkProfile_112; }
	inline VKProfileInfo_t3473649666 ** get_address_of_vkProfile_112() { return &___vkProfile_112; }
	inline void set_vkProfile_112(VKProfileInfo_t3473649666 * value)
	{
		___vkProfile_112 = value;
		Il2CppCodeGenWriteBarrier(&___vkProfile_112, value);
	}

	inline static int32_t get_offset_of_FbProfile_113() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___FbProfile_113)); }
	inline fbProfile_t675187021 * get_FbProfile_113() const { return ___FbProfile_113; }
	inline fbProfile_t675187021 ** get_address_of_FbProfile_113() { return &___FbProfile_113; }
	inline void set_FbProfile_113(fbProfile_t675187021 * value)
	{
		___FbProfile_113 = value;
		Il2CppCodeGenWriteBarrier(&___FbProfile_113, value);
	}

	inline static int32_t get_offset_of_lastInterstitalShowDateTime_114() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___lastInterstitalShowDateTime_114)); }
	inline DateTime_t339033936  get_lastInterstitalShowDateTime_114() const { return ___lastInterstitalShowDateTime_114; }
	inline DateTime_t339033936 * get_address_of_lastInterstitalShowDateTime_114() { return &___lastInterstitalShowDateTime_114; }
	inline void set_lastInterstitalShowDateTime_114(DateTime_t339033936  value)
	{
		___lastInterstitalShowDateTime_114 = value;
	}

	inline static int32_t get_offset_of_UpgradesData_116() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___UpgradesData_116)); }
	inline GameData_t2589969116 * get_UpgradesData_116() const { return ___UpgradesData_116; }
	inline GameData_t2589969116 ** get_address_of_UpgradesData_116() { return &___UpgradesData_116; }
	inline void set_UpgradesData_116(GameData_t2589969116 * value)
	{
		___UpgradesData_116 = value;
		Il2CppCodeGenWriteBarrier(&___UpgradesData_116, value);
	}

	inline static int32_t get_offset_of_ReactiveDoubleEarning_117() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___ReactiveDoubleEarning_117)); }
	inline ReadOnlyReactiveProperty_1_t3000407357 * get_ReactiveDoubleEarning_117() const { return ___ReactiveDoubleEarning_117; }
	inline ReadOnlyReactiveProperty_1_t3000407357 ** get_address_of_ReactiveDoubleEarning_117() { return &___ReactiveDoubleEarning_117; }
	inline void set_ReactiveDoubleEarning_117(ReadOnlyReactiveProperty_1_t3000407357 * value)
	{
		___ReactiveDoubleEarning_117 = value;
		Il2CppCodeGenWriteBarrier(&___ReactiveDoubleEarning_117, value);
	}

	inline static int32_t get_offset_of_ReactiveEndTime_118() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___ReactiveEndTime_118)); }
	inline ReactiveProperty_1_t1576821940 * get_ReactiveEndTime_118() const { return ___ReactiveEndTime_118; }
	inline ReactiveProperty_1_t1576821940 ** get_address_of_ReactiveEndTime_118() { return &___ReactiveEndTime_118; }
	inline void set_ReactiveEndTime_118(ReactiveProperty_1_t1576821940 * value)
	{
		___ReactiveEndTime_118 = value;
		Il2CppCodeGenWriteBarrier(&___ReactiveEndTime_118, value);
	}

	inline static int32_t get_offset_of_resetTapConter_122() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___resetTapConter_122)); }
	inline int32_t get_resetTapConter_122() const { return ___resetTapConter_122; }
	inline int32_t* get_address_of_resetTapConter_122() { return &___resetTapConter_122; }
	inline void set_resetTapConter_122(int32_t value)
	{
		___resetTapConter_122 = value;
	}

	inline static int32_t get_offset_of_GuiController_123() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___GuiController_123)); }
	inline GuiController_t1495593751 * get_GuiController_123() const { return ___GuiController_123; }
	inline GuiController_t1495593751 ** get_address_of_GuiController_123() { return &___GuiController_123; }
	inline void set_GuiController_123(GuiController_t1495593751 * value)
	{
		___GuiController_123 = value;
		Il2CppCodeGenWriteBarrier(&___GuiController_123, value);
	}

	inline static int32_t get_offset_of_GameController_124() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___GameController_124)); }
	inline GameController_t2782302542 * get_GameController_124() const { return ___GameController_124; }
	inline GameController_t2782302542 ** get_address_of_GameController_124() { return &___GameController_124; }
	inline void set_GameController_124(GameController_t2782302542 * value)
	{
		___GameController_124 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_124, value);
	}

	inline static int32_t get_offset_of_TutorialPlayer_125() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___TutorialPlayer_125)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_125() const { return ___TutorialPlayer_125; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_125() { return &___TutorialPlayer_125; }
	inline void set_TutorialPlayer_125(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_125 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_125, value);
	}

	inline static int32_t get_offset_of_CurrentBillUpgrade_126() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___CurrentBillUpgrade_126)); }
	inline ReactiveProperty_1_t3455747825 * get_CurrentBillUpgrade_126() const { return ___CurrentBillUpgrade_126; }
	inline ReactiveProperty_1_t3455747825 ** get_address_of_CurrentBillUpgrade_126() { return &___CurrentBillUpgrade_126; }
	inline void set_CurrentBillUpgrade_126(ReactiveProperty_1_t3455747825 * value)
	{
		___CurrentBillUpgrade_126 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentBillUpgrade_126, value);
	}

	inline static int32_t get_offset_of_Message_127() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___Message_127)); }
	inline Message_t2619578343 * get_Message_127() const { return ___Message_127; }
	inline Message_t2619578343 ** get_address_of_Message_127() { return &___Message_127; }
	inline void set_Message_127(Message_t2619578343 * value)
	{
		___Message_127 = value;
		Il2CppCodeGenWriteBarrier(&___Message_127, value);
	}

	inline static int32_t get_offset_of_AudioController_128() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___AudioController_128)); }
	inline AudioController_t2316845042 * get_AudioController_128() const { return ___AudioController_128; }
	inline AudioController_t2316845042 ** get_address_of_AudioController_128() { return &___AudioController_128; }
	inline void set_AudioController_128(AudioController_t2316845042 * value)
	{
		___AudioController_128 = value;
		Il2CppCodeGenWriteBarrier(&___AudioController_128, value);
	}

	inline static int32_t get_offset_of_hardPacks_129() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199, ___hardPacks_129)); }
	inline Int32U5BU5D_t1809983122* get_hardPacks_129() const { return ___hardPacks_129; }
	inline Int32U5BU5D_t1809983122** get_address_of_hardPacks_129() { return &___hardPacks_129; }
	inline void set_hardPacks_129(Int32U5BU5D_t1809983122* value)
	{
		___hardPacks_129 = value;
		Il2CppCodeGenWriteBarrier(&___hardPacks_129, value);
	}
};

struct GlobalStat_t1134678199_StaticFields
{
public:
	// System.Single GlobalStat::priceMultiplier
	float ___priceMultiplier_115;
	// System.String[] GlobalStat::billNames
	StringU5BU5D_t2956870243* ___billNames_119;
	// UnityEngine.Color[] GlobalStat::upgradeColors
	ColorU5BU5D_t3477081137* ___upgradeColors_120;
	// System.Double[] GlobalStat::_billSizes
	DoubleU5BU5D_t1048280995* ____billSizes_121;
	// System.Func`2<System.Int64,System.Boolean> GlobalStat::<>f__am$cache82
	Func_2_t2251336571 * ___U3CU3Ef__amU24cache82_130;
	// System.Func`2<System.Int64,DateTimeCell> GlobalStat::<>f__am$cache83
	Func_2_t2814130075 * ___U3CU3Ef__amU24cache83_131;
	// System.Func`2<System.Int64,DateTimeCell> GlobalStat::<>f__am$cache84
	Func_2_t2814130075 * ___U3CU3Ef__amU24cache84_132;
	// System.Func`2<DateTimeCell,System.Int64> GlobalStat::<>f__am$cache85
	Func_2_t831010073 * ___U3CU3Ef__amU24cache85_133;
	// System.Func`2<DateTimeCell,System.Int64> GlobalStat::<>f__am$cache86
	Func_2_t831010073 * ___U3CU3Ef__amU24cache86_134;
	// System.Action`1<System.Boolean> GlobalStat::<>f__am$cache87
	Action_1_t359458046 * ___U3CU3Ef__amU24cache87_135;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> GlobalStat::<>f__switch$map5
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map5_136;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> GlobalStat::<>f__switch$map6
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map6_137;

public:
	inline static int32_t get_offset_of_priceMultiplier_115() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___priceMultiplier_115)); }
	inline float get_priceMultiplier_115() const { return ___priceMultiplier_115; }
	inline float* get_address_of_priceMultiplier_115() { return &___priceMultiplier_115; }
	inline void set_priceMultiplier_115(float value)
	{
		___priceMultiplier_115 = value;
	}

	inline static int32_t get_offset_of_billNames_119() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___billNames_119)); }
	inline StringU5BU5D_t2956870243* get_billNames_119() const { return ___billNames_119; }
	inline StringU5BU5D_t2956870243** get_address_of_billNames_119() { return &___billNames_119; }
	inline void set_billNames_119(StringU5BU5D_t2956870243* value)
	{
		___billNames_119 = value;
		Il2CppCodeGenWriteBarrier(&___billNames_119, value);
	}

	inline static int32_t get_offset_of_upgradeColors_120() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___upgradeColors_120)); }
	inline ColorU5BU5D_t3477081137* get_upgradeColors_120() const { return ___upgradeColors_120; }
	inline ColorU5BU5D_t3477081137** get_address_of_upgradeColors_120() { return &___upgradeColors_120; }
	inline void set_upgradeColors_120(ColorU5BU5D_t3477081137* value)
	{
		___upgradeColors_120 = value;
		Il2CppCodeGenWriteBarrier(&___upgradeColors_120, value);
	}

	inline static int32_t get_offset_of__billSizes_121() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ____billSizes_121)); }
	inline DoubleU5BU5D_t1048280995* get__billSizes_121() const { return ____billSizes_121; }
	inline DoubleU5BU5D_t1048280995** get_address_of__billSizes_121() { return &____billSizes_121; }
	inline void set__billSizes_121(DoubleU5BU5D_t1048280995* value)
	{
		____billSizes_121 = value;
		Il2CppCodeGenWriteBarrier(&____billSizes_121, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache82_130() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___U3CU3Ef__amU24cache82_130)); }
	inline Func_2_t2251336571 * get_U3CU3Ef__amU24cache82_130() const { return ___U3CU3Ef__amU24cache82_130; }
	inline Func_2_t2251336571 ** get_address_of_U3CU3Ef__amU24cache82_130() { return &___U3CU3Ef__amU24cache82_130; }
	inline void set_U3CU3Ef__amU24cache82_130(Func_2_t2251336571 * value)
	{
		___U3CU3Ef__amU24cache82_130 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache82_130, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache83_131() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___U3CU3Ef__amU24cache83_131)); }
	inline Func_2_t2814130075 * get_U3CU3Ef__amU24cache83_131() const { return ___U3CU3Ef__amU24cache83_131; }
	inline Func_2_t2814130075 ** get_address_of_U3CU3Ef__amU24cache83_131() { return &___U3CU3Ef__amU24cache83_131; }
	inline void set_U3CU3Ef__amU24cache83_131(Func_2_t2814130075 * value)
	{
		___U3CU3Ef__amU24cache83_131 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache83_131, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache84_132() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___U3CU3Ef__amU24cache84_132)); }
	inline Func_2_t2814130075 * get_U3CU3Ef__amU24cache84_132() const { return ___U3CU3Ef__amU24cache84_132; }
	inline Func_2_t2814130075 ** get_address_of_U3CU3Ef__amU24cache84_132() { return &___U3CU3Ef__amU24cache84_132; }
	inline void set_U3CU3Ef__amU24cache84_132(Func_2_t2814130075 * value)
	{
		___U3CU3Ef__amU24cache84_132 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache84_132, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache85_133() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___U3CU3Ef__amU24cache85_133)); }
	inline Func_2_t831010073 * get_U3CU3Ef__amU24cache85_133() const { return ___U3CU3Ef__amU24cache85_133; }
	inline Func_2_t831010073 ** get_address_of_U3CU3Ef__amU24cache85_133() { return &___U3CU3Ef__amU24cache85_133; }
	inline void set_U3CU3Ef__amU24cache85_133(Func_2_t831010073 * value)
	{
		___U3CU3Ef__amU24cache85_133 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache85_133, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache86_134() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___U3CU3Ef__amU24cache86_134)); }
	inline Func_2_t831010073 * get_U3CU3Ef__amU24cache86_134() const { return ___U3CU3Ef__amU24cache86_134; }
	inline Func_2_t831010073 ** get_address_of_U3CU3Ef__amU24cache86_134() { return &___U3CU3Ef__amU24cache86_134; }
	inline void set_U3CU3Ef__amU24cache86_134(Func_2_t831010073 * value)
	{
		___U3CU3Ef__amU24cache86_134 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache86_134, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache87_135() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___U3CU3Ef__amU24cache87_135)); }
	inline Action_1_t359458046 * get_U3CU3Ef__amU24cache87_135() const { return ___U3CU3Ef__amU24cache87_135; }
	inline Action_1_t359458046 ** get_address_of_U3CU3Ef__amU24cache87_135() { return &___U3CU3Ef__amU24cache87_135; }
	inline void set_U3CU3Ef__amU24cache87_135(Action_1_t359458046 * value)
	{
		___U3CU3Ef__amU24cache87_135 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache87_135, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_136() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___U3CU3Ef__switchU24map5_136)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map5_136() const { return ___U3CU3Ef__switchU24map5_136; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map5_136() { return &___U3CU3Ef__switchU24map5_136; }
	inline void set_U3CU3Ef__switchU24map5_136(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map5_136 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map5_136, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_137() { return static_cast<int32_t>(offsetof(GlobalStat_t1134678199_StaticFields, ___U3CU3Ef__switchU24map6_137)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map6_137() const { return ___U3CU3Ef__switchU24map6_137; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map6_137() { return &___U3CU3Ef__switchU24map6_137; }
	inline void set_U3CU3Ef__switchU24map6_137(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map6_137 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map6_137, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
