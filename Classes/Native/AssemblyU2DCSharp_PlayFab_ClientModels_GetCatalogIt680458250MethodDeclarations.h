﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCatalogItemsResult
struct GetCatalogItemsResult_t680458250;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>
struct List_1_t634580917;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetCatalogItemsResult::.ctor()
extern "C"  void GetCatalogItemsResult__ctor_m557023839 (GetCatalogItemsResult_t680458250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem> PlayFab.ClientModels.GetCatalogItemsResult::get_Catalog()
extern "C"  List_1_t634580917 * GetCatalogItemsResult_get_Catalog_m2249827724 (GetCatalogItemsResult_t680458250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCatalogItemsResult::set_Catalog(System.Collections.Generic.List`1<PlayFab.ClientModels.CatalogItem>)
extern "C"  void GetCatalogItemsResult_set_Catalog_m3126539079 (GetCatalogItemsResult_t680458250 * __this, List_1_t634580917 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
