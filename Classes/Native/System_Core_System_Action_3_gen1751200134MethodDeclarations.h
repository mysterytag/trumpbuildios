﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen2844404526MethodDeclarations.h"

// System.Void System.Action`3<UpgradeButton,UpgradeCell,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m1472350342(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t1751200134 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m2152649190_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<UpgradeButton,UpgradeCell,System.Int32>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m1411465200(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1751200134 *, UpgradeButton_t1475467342 *, UpgradeCell_t4117746046 *, int32_t, const MethodInfo*))Action_3_Invoke_m3058215458_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<UpgradeButton,UpgradeCell,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m92707681(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t1751200134 *, UpgradeButton_t1475467342 *, UpgradeCell_t4117746046 *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m525605073_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<UpgradeButton,UpgradeCell,System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m1063300742(__this, ___result0, method) ((  void (*) (Action_3_t1751200134 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m3673368310_gshared)(__this, ___result0, method)
