﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseResponseCallback
struct ValidateGooglePlayPurchaseResponseCallback_t2106573002;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest
struct ValidateGooglePlayPurchaseRequest_t4082068731;
// PlayFab.ClientModels.ValidateGooglePlayPurchaseResult
struct ValidateGooglePlayPurchaseResult_t1413753425;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateGoo4082068731.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateGoo1413753425.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ValidateGooglePlayPurchaseResponseCallback__ctor_m2809325753 (ValidateGooglePlayPurchaseResponseCallback_t2106573002 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest,PlayFab.ClientModels.ValidateGooglePlayPurchaseResult,PlayFab.PlayFabError,System.Object)
extern "C"  void ValidateGooglePlayPurchaseResponseCallback_Invoke_m1250810374 (ValidateGooglePlayPurchaseResponseCallback_t2106573002 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateGooglePlayPurchaseRequest_t4082068731 * ___request2, ValidateGooglePlayPurchaseResult_t1413753425 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ValidateGooglePlayPurchaseResponseCallback_t2106573002(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ValidateGooglePlayPurchaseRequest_t4082068731 * ___request2, ValidateGooglePlayPurchaseResult_t1413753425 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest,PlayFab.ClientModels.ValidateGooglePlayPurchaseResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ValidateGooglePlayPurchaseResponseCallback_BeginInvoke_m2874846899 (ValidateGooglePlayPurchaseResponseCallback_t2106573002 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateGooglePlayPurchaseRequest_t4082068731 * ___request2, ValidateGooglePlayPurchaseResult_t1413753425 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateGooglePlayPurchaseResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ValidateGooglePlayPurchaseResponseCallback_EndInvoke_m1501667145 (ValidateGooglePlayPurchaseResponseCallback_t2106573002 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
