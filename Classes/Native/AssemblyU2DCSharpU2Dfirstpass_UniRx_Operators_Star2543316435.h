﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.StartObservable`1<UniRx.Unit>
struct StartObservable_1_t4083709414;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.StartObservable`1/StartObserver<UniRx.Unit>
struct  StartObserver_t2543316435  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.StartObservable`1<T> UniRx.Operators.StartObservable`1/StartObserver::parent
	StartObservable_1_t4083709414 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(StartObserver_t2543316435, ___parent_2)); }
	inline StartObservable_1_t4083709414 * get_parent_2() const { return ___parent_2; }
	inline StartObservable_1_t4083709414 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(StartObservable_1_t4083709414 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
