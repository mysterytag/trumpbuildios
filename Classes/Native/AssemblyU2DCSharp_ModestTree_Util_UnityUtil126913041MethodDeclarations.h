﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Transform
struct Transform_t284553113;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t514686775;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ModestTree_Util_MouseWheelScroll2927108753.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "mscorlib_System_Type2779229935.h"

// System.Boolean ModestTree.Util.UnityUtil::get_IsAltKeyDown()
extern "C"  bool UnityUtil_get_IsAltKeyDown_m1269086535 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.Util.UnityUtil::get_IsControlKeyDown()
extern "C"  bool UnityUtil_get_IsControlKeyDown_m3588342035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.Util.UnityUtil::get_IsShiftKeyDown()
extern "C"  bool UnityUtil_get_IsShiftKeyDown_m1184300814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.Util.UnityUtil::get_WasShiftKeyJustPressed()
extern "C"  bool UnityUtil_get_WasShiftKeyJustPressed_m3648197493 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.Util.UnityUtil::get_WasAltKeyJustPressed()
extern "C"  bool UnityUtil_get_WasAltKeyJustPressed_m691645788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ModestTree.Util.MouseWheelScrollDirections ModestTree.Util.UnityUtil::CheckMouseScrollWheel()
extern "C"  int32_t UnityUtil_CheckMouseScrollWheel_m3579850857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ModestTree.Util.UnityUtil::GetDepthLevel(UnityEngine.Transform)
extern "C"  int32_t UnityUtil_GetDepthLevel_m83980134 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___transform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.GameObject> ModestTree.Util.UnityUtil::GetRootGameObjects()
extern "C"  List_1_t514686775 * UnityUtil_GetRootGameObjects_m729052607 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ModestTree.Util.UnityUtil::GetSmartDefaultValue(System.Type)
extern "C"  Il2CppObject * UnityUtil_GetSmartDefaultValue_m2015070267 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.Util.UnityUtil::<GetRootGameObjects>m__30A(UnityEngine.Transform)
extern "C"  bool UnityUtil_U3CGetRootGameObjectsU3Em__30A_m3239353298 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ModestTree.Util.UnityUtil::<GetRootGameObjects>m__30B(UnityEngine.Transform)
extern "C"  GameObject_t4012695102 * UnityUtil_U3CGetRootGameObjectsU3Em__30B_m4070462770 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
