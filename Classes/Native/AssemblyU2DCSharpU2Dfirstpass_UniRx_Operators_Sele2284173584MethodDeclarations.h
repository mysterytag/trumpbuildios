﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>
struct SelectManyObservable_2_t2284173584;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<UniRx.Unit>>
struct Func_2_t3615761334;
// System.Func`3<System.Object,System.Int32,UniRx.IObservable`1<UniRx.Unit>>
struct Func_3_t875679264;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>
struct Func_2_t2434150030;
// System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>
struct Func_3_t3989035256;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m499881472_gshared (SelectManyObservable_2_t2284173584 * __this, Il2CppObject* ___source0, Func_2_t3615761334 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m499881472(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t2284173584 *, Il2CppObject*, Func_2_t3615761334 *, const MethodInfo*))SelectManyObservable_2__ctor_m499881472_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m4030062588_gshared (SelectManyObservable_2_t2284173584 * __this, Il2CppObject* ___source0, Func_3_t875679264 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m4030062588(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t2284173584 *, Il2CppObject*, Func_3_t875679264 *, const MethodInfo*))SelectManyObservable_2__ctor_m4030062588_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m2936173840_gshared (SelectManyObservable_2_t2284173584 * __this, Il2CppObject* ___source0, Func_2_t2434150030 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m2936173840(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t2284173584 *, Il2CppObject*, Func_2_t2434150030 *, const MethodInfo*))SelectManyObservable_2__ctor_m2936173840_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m4038295700_gshared (SelectManyObservable_2_t2284173584 * __this, Il2CppObject* ___source0, Func_3_t3989035256 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m4038295700(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t2284173584 *, Il2CppObject*, Func_3_t3989035256 *, const MethodInfo*))SelectManyObservable_2__ctor_m4038295700_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * SelectManyObservable_2_SubscribeCore_m3744195857_gshared (SelectManyObservable_2_t2284173584 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectManyObservable_2_SubscribeCore_m3744195857(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectManyObservable_2_t2284173584 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObservable_2_SubscribeCore_m3744195857_gshared)(__this, ___observer0, ___cancel1, method)
