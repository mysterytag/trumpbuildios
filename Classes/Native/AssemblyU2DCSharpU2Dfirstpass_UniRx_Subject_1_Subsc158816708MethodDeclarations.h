﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Int32>>
struct Subscription_t158816708;
// UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct Subject_1_t22637510;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct IObserver_1_t296601793;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m2532258126_gshared (Subscription_t158816708 * __this, Subject_1_t22637510 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m2532258126(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t158816708 *, Subject_1_t22637510 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m2532258126_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Int32>>::Dispose()
extern "C"  void Subscription_Dispose_m167027688_gshared (Subscription_t158816708 * __this, const MethodInfo* method);
#define Subscription_Dispose_m167027688(__this, method) ((  void (*) (Subscription_t158816708 *, const MethodInfo*))Subscription_Dispose_m167027688_gshared)(__this, method)
