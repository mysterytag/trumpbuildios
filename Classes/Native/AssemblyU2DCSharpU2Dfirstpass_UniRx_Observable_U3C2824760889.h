﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.AsyncSubject`1<UniRx.Unit>
struct AsyncSubject_1_t3257925142;
// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,UniRx.Unit>
struct  U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889  : public Il2CppObject
{
public:
	// UniRx.AsyncSubject`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3::subject
	AsyncSubject_1_t3257925142 * ___subject_0;
	// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<T1,T2,TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3::<>f__ref$67
	U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 * ___U3CU3Ef__refU2467_1;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889, ___subject_0)); }
	inline AsyncSubject_1_t3257925142 * get_subject_0() const { return ___subject_0; }
	inline AsyncSubject_1_t3257925142 ** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(AsyncSubject_1_t3257925142 * value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier(&___subject_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2467_1() { return static_cast<int32_t>(offsetof(U3CFromAsyncPatternU3Ec__AnonStorey44_3_t2824760889, ___U3CU3Ef__refU2467_1)); }
	inline U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 * get_U3CU3Ef__refU2467_1() const { return ___U3CU3Ef__refU2467_1; }
	inline U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 ** get_address_of_U3CU3Ef__refU2467_1() { return &___U3CU3Ef__refU2467_1; }
	inline void set_U3CU3Ef__refU2467_1(U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 * value)
	{
		___U3CU3Ef__refU2467_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2467_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
