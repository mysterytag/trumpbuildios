﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.Quaternion>
struct Subject_1_t3829750599;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Quaternion>
struct IObserver_1_t4103714882;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void UniRx.Subject`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void Subject_1__ctor_m3837640901_gshared (Subject_1_t3829750599 * __this, const MethodInfo* method);
#define Subject_1__ctor_m3837640901(__this, method) ((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))Subject_1__ctor_m3837640901_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Quaternion>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3708830191_gshared (Subject_1_t3829750599 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3708830191(__this, method) ((  bool (*) (Subject_1_t3829750599 *, const MethodInfo*))Subject_1_get_HasObservers_m3708830191_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Quaternion>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1971621007_gshared (Subject_1_t3829750599 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m1971621007(__this, method) ((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))Subject_1_OnCompleted_m1971621007_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Quaternion>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m1324212028_gshared (Subject_1_t3829750599 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m1324212028(__this, ___error0, method) ((  void (*) (Subject_1_t3829750599 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1324212028_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Quaternion>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1426701869_gshared (Subject_1_t3829750599 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1426701869(__this, ___value0, method) ((  void (*) (Subject_1_t3829750599 *, Quaternion_t1891715979 , const MethodInfo*))Subject_1_OnNext_m1426701869_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Quaternion>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2880551578_gshared (Subject_1_t3829750599 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2880551578(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3829750599 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2880551578_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Quaternion>::Dispose()
extern "C"  void Subject_1_Dispose_m2789248962_gshared (Subject_1_t3829750599 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2789248962(__this, method) ((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))Subject_1_Dispose_m2789248962_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Quaternion>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m188438347_gshared (Subject_1_t3829750599 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m188438347(__this, method) ((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m188438347_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Quaternion>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3537875238_gshared (Subject_1_t3829750599 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3537875238(__this, method) ((  bool (*) (Subject_1_t3829750599 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3537875238_gshared)(__this, method)
