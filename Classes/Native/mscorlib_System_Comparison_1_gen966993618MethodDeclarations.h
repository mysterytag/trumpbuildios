﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UniRx.Unit>
struct Comparison_1_t966993618;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m1898637389_gshared (Comparison_1_t966993618 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m1898637389(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t966993618 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m1898637389_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<UniRx.Unit>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1691616587_gshared (Comparison_1_t966993618 * __this, Unit_t2558286038  ___x0, Unit_t2558286038  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m1691616587(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t966993618 *, Unit_t2558286038 , Unit_t2558286038 , const MethodInfo*))Comparison_1_Invoke_m1691616587_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<UniRx.Unit>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m1664236948_gshared (Comparison_1_t966993618 * __this, Unit_t2558286038  ___x0, Unit_t2558286038  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m1664236948(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t966993618 *, Unit_t2558286038 , Unit_t2558286038 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1664236948_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m409786689_gshared (Comparison_1_t966993618 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m409786689(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t966993618 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m409786689_gshared)(__this, ___result0, method)
