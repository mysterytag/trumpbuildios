﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2
struct U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_t1456778115;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2::.ctor()
extern "C"  void U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2__ctor_m2054068228 (U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_t1456778115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStoreyA2::<>m__DA(UniRx.IObserver`1<UniRx.Unit>)
extern "C"  Il2CppObject * U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_U3CU3Em__DA_m2620381903 (U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStoreyA2_t1456778115 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
