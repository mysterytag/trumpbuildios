﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/MatchmakeResponseCallback
struct MatchmakeResponseCallback_t2500205273;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.MatchmakeRequest
struct MatchmakeRequest_t4152079884;
// PlayFab.ClientModels.MatchmakeResult
struct MatchmakeResult_t30538528;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeRe4152079884.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeResu30538528.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/MatchmakeResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void MatchmakeResponseCallback__ctor_m3899869928 (MatchmakeResponseCallback_t2500205273 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/MatchmakeResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.MatchmakeRequest,PlayFab.ClientModels.MatchmakeResult,PlayFab.PlayFabError,System.Object)
extern "C"  void MatchmakeResponseCallback_Invoke_m2769317369 (MatchmakeResponseCallback_t2500205273 * __this, String_t* ___urlPath0, int32_t ___callId1, MatchmakeRequest_t4152079884 * ___request2, MatchmakeResult_t30538528 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_MatchmakeResponseCallback_t2500205273(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, MatchmakeRequest_t4152079884 * ___request2, MatchmakeResult_t30538528 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/MatchmakeResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.MatchmakeRequest,PlayFab.ClientModels.MatchmakeResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MatchmakeResponseCallback_BeginInvoke_m1472617830 (MatchmakeResponseCallback_t2500205273 * __this, String_t* ___urlPath0, int32_t ___callId1, MatchmakeRequest_t4152079884 * ___request2, MatchmakeResult_t30538528 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/MatchmakeResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void MatchmakeResponseCallback_EndInvoke_m1454403832 (MatchmakeResponseCallback_t2500205273 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
