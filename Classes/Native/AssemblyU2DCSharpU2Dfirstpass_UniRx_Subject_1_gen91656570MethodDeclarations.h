﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t91656570;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t365620853;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"

// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void Subject_1__ctor_m342849048_gshared (Subject_1_t91656570 * __this, const MethodInfo* method);
#define Subject_1__ctor_m342849048(__this, method) ((  void (*) (Subject_1_t91656570 *, const MethodInfo*))Subject_1__ctor_m342849048_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m501865844_gshared (Subject_1_t91656570 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m501865844(__this, method) ((  bool (*) (Subject_1_t91656570 *, const MethodInfo*))Subject_1_get_HasObservers_m501865844_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1258272674_gshared (Subject_1_t91656570 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m1258272674(__this, method) ((  void (*) (Subject_1_t91656570 *, const MethodInfo*))Subject_1_OnCompleted_m1258272674_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2042234319_gshared (Subject_1_t91656570 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2042234319(__this, ___error0, method) ((  void (*) (Subject_1_t91656570 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2042234319_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1596156608_gshared (Subject_1_t91656570 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1596156608(__this, ___value0, method) ((  void (*) (Subject_1_t91656570 *, CollectionMoveEvent_1_t2448589246 , const MethodInfo*))Subject_1_OnNext_m1596156608_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m497037271_gshared (Subject_1_t91656570 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m497037271(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t91656570 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m497037271_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subject_1_Dispose_m2958703701_gshared (Subject_1_t91656570 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2958703701(__this, method) ((  void (*) (Subject_1_t91656570 *, const MethodInfo*))Subject_1_Dispose_m2958703701_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m171229406_gshared (Subject_1_t91656570 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m171229406(__this, method) ((  void (*) (Subject_1_t91656570 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m171229406_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m4063057259_gshared (Subject_1_t91656570 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m4063057259(__this, method) ((  bool (*) (Subject_1_t91656570 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m4063057259_gshared)(__this, method)
