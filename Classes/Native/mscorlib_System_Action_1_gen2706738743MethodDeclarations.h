﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1024136343_gshared (Action_1_t2706738743 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m1024136343(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2706738743 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1024136343_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<UniRx.Unit>::Invoke(T)
extern "C"  void Action_1_Invoke_m1625430730_gshared (Action_1_t2706738743 * __this, Unit_t2558286038  ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m1625430730(__this, ___obj0, method) ((  void (*) (Action_1_t2706738743 *, Unit_t2558286038 , const MethodInfo*))Action_1_Invoke_m1625430730_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<UniRx.Unit>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m446945055_gshared (Action_1_t2706738743 * __this, Unit_t2558286038  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m446945055(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2706738743 *, Unit_t2558286038 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m446945055_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1504422730_gshared (Action_1_t2706738743 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m1504422730(__this, ___result0, method) ((  void (*) (Action_1_t2706738743 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m1504422730_gshared)(__this, ___result0, method)
