﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneController
struct SceneController_t1039782184;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneController::.ctor()
extern "C"  void SceneController__ctor_m1562559299 (SceneController_t1039782184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
