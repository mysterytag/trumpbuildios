﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AsyncSubject`1<System.Object>
struct AsyncSubject_1_t1536745524;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.AsyncSubject`1<System.Object>::.ctor()
extern "C"  void AsyncSubject_1__ctor_m3444076052_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method);
#define AsyncSubject_1__ctor_m3444076052(__this, method) ((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))AsyncSubject_1__ctor_m3444076052_gshared)(__this, method)
// T UniRx.AsyncSubject`1<System.Object>::get_Value()
extern "C"  Il2CppObject * AsyncSubject_1_get_Value_m3880262777_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method);
#define AsyncSubject_1_get_Value_m3880262777(__this, method) ((  Il2CppObject * (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))AsyncSubject_1_get_Value_m3880262777_gshared)(__this, method)
// System.Boolean UniRx.AsyncSubject`1<System.Object>::get_HasObservers()
extern "C"  bool AsyncSubject_1_get_HasObservers_m4197289792_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method);
#define AsyncSubject_1_get_HasObservers_m4197289792(__this, method) ((  bool (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))AsyncSubject_1_get_HasObservers_m4197289792_gshared)(__this, method)
// System.Boolean UniRx.AsyncSubject`1<System.Object>::get_IsCompleted()
extern "C"  bool AsyncSubject_1_get_IsCompleted_m4253247846_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method);
#define AsyncSubject_1_get_IsCompleted_m4253247846(__this, method) ((  bool (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))AsyncSubject_1_get_IsCompleted_m4253247846_gshared)(__this, method)
// System.Void UniRx.AsyncSubject`1<System.Object>::OnCompleted()
extern "C"  void AsyncSubject_1_OnCompleted_m933610142_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method);
#define AsyncSubject_1_OnCompleted_m933610142(__this, method) ((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))AsyncSubject_1_OnCompleted_m933610142_gshared)(__this, method)
// System.Void UniRx.AsyncSubject`1<System.Object>::OnError(System.Exception)
extern "C"  void AsyncSubject_1_OnError_m2224781003_gshared (AsyncSubject_1_t1536745524 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AsyncSubject_1_OnError_m2224781003(__this, ___error0, method) ((  void (*) (AsyncSubject_1_t1536745524 *, Exception_t1967233988 *, const MethodInfo*))AsyncSubject_1_OnError_m2224781003_gshared)(__this, ___error0, method)
// System.Void UniRx.AsyncSubject`1<System.Object>::OnNext(T)
extern "C"  void AsyncSubject_1_OnNext_m1168004028_gshared (AsyncSubject_1_t1536745524 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AsyncSubject_1_OnNext_m1168004028(__this, ___value0, method) ((  void (*) (AsyncSubject_1_t1536745524 *, Il2CppObject *, const MethodInfo*))AsyncSubject_1_OnNext_m1168004028_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.AsyncSubject`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AsyncSubject_1_Subscribe_m3526656105_gshared (AsyncSubject_1_t1536745524 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define AsyncSubject_1_Subscribe_m3526656105(__this, ___observer0, method) ((  Il2CppObject * (*) (AsyncSubject_1_t1536745524 *, Il2CppObject*, const MethodInfo*))AsyncSubject_1_Subscribe_m3526656105_gshared)(__this, ___observer0, method)
// System.Void UniRx.AsyncSubject`1<System.Object>::Dispose()
extern "C"  void AsyncSubject_1_Dispose_m2530551121_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method);
#define AsyncSubject_1_Dispose_m2530551121(__this, method) ((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))AsyncSubject_1_Dispose_m2530551121_gshared)(__this, method)
// System.Void UniRx.AsyncSubject`1<System.Object>::ThrowIfDisposed()
extern "C"  void AsyncSubject_1_ThrowIfDisposed_m3466915290_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method);
#define AsyncSubject_1_ThrowIfDisposed_m3466915290(__this, method) ((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))AsyncSubject_1_ThrowIfDisposed_m3466915290_gshared)(__this, method)
// System.Boolean UniRx.AsyncSubject`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool AsyncSubject_1_IsRequiredSubscribeOnCurrentThread_m353152055_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method);
#define AsyncSubject_1_IsRequiredSubscribeOnCurrentThread_m353152055(__this, method) ((  bool (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))AsyncSubject_1_IsRequiredSubscribeOnCurrentThread_m353152055_gshared)(__this, method)
