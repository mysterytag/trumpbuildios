﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.IOSSponsorPayPlugin/<SkipFrameCoroutineWithBlock>c__Iterator35
struct U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_t2147250006;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SponsorPay.IOSSponsorPayPlugin/<SkipFrameCoroutineWithBlock>c__Iterator35::.ctor()
extern "C"  void U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35__ctor_m3163580084 (U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_t2147250006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SponsorPay.IOSSponsorPayPlugin/<SkipFrameCoroutineWithBlock>c__Iterator35::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3066683230 (U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_t2147250006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SponsorPay.IOSSponsorPayPlugin/<SkipFrameCoroutineWithBlock>c__Iterator35::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_System_Collections_IEnumerator_get_Current_m330456818 (U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_t2147250006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SponsorPay.IOSSponsorPayPlugin/<SkipFrameCoroutineWithBlock>c__Iterator35::MoveNext()
extern "C"  bool U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_MoveNext_m3142716352 (U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_t2147250006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin/<SkipFrameCoroutineWithBlock>c__Iterator35::Dispose()
extern "C"  void U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_Dispose_m3556865521 (U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_t2147250006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin/<SkipFrameCoroutineWithBlock>c__Iterator35::Reset()
extern "C"  void U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_Reset_m810013025 (U3CSkipFrameCoroutineWithBlockU3Ec__Iterator35_t2147250006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
