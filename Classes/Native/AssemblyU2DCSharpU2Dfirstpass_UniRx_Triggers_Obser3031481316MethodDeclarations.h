﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableTriggerTrigger
struct ObservableTriggerTrigger_t3031481316;
// UnityEngine.Collider
struct Collider_t955670625;
// UniRx.IObservable`1<UnityEngine.Collider>
struct IObservable_1_t714468989;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"

// System.Void UniRx.Triggers.ObservableTriggerTrigger::.ctor()
extern "C"  void ObservableTriggerTrigger__ctor_m3219747545 (ObservableTriggerTrigger_t3031481316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTriggerTrigger::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void ObservableTriggerTrigger_OnTriggerEnter_m1897739071 (ObservableTriggerTrigger_t3031481316 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::OnTriggerEnterAsObservable()
extern "C"  Il2CppObject* ObservableTriggerTrigger_OnTriggerEnterAsObservable_m771990024 (ObservableTriggerTrigger_t3031481316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTriggerTrigger::OnTriggerExit(UnityEngine.Collider)
extern "C"  void ObservableTriggerTrigger_OnTriggerExit_m958066307 (ObservableTriggerTrigger_t3031481316 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::OnTriggerExitAsObservable()
extern "C"  Il2CppObject* ObservableTriggerTrigger_OnTriggerExitAsObservable_m3034813978 (ObservableTriggerTrigger_t3031481316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTriggerTrigger::OnTriggerStay(UnityEngine.Collider)
extern "C"  void ObservableTriggerTrigger_OnTriggerStay_m3576155518 (ObservableTriggerTrigger_t3031481316 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::OnTriggerStayAsObservable()
extern "C"  Il2CppObject* ObservableTriggerTrigger_OnTriggerStayAsObservable_m3874529301 (ObservableTriggerTrigger_t3031481316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTriggerTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableTriggerTrigger_RaiseOnCompletedOnDestroy_m2591646610 (ObservableTriggerTrigger_t3031481316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
