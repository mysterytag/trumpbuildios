﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.JSON/_JSON/Parser
struct Parser_t2383423553;
// System.String
struct String_t;
// OnePF.JSON
struct JSON_t2649481148;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON_Parse80003545.h"

// System.Void OnePF.JSON/_JSON/Parser::.ctor(System.String)
extern "C"  void Parser__ctor_m3477549511 (Parser_t2383423553 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON/_JSON/Parser::Parse(System.String)
extern "C"  JSON_t2649481148 * Parser_Parse_m839535979 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON/_JSON/Parser::Dispose()
extern "C"  void Parser_Dispose_m4146750104 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON OnePF.JSON/_JSON/Parser::ParseObject()
extern "C"  JSON_t2649481148 * Parser_ParseObject_m1044872726 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> OnePF.JSON/_JSON/Parser::ParseArray()
extern "C"  List_1_t1634065389 * Parser_ParseArray_m3171442056 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnePF.JSON/_JSON/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m3306942770 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnePF.JSON/_JSON/Parser::ParseByToken(OnePF.JSON/_JSON/Parser/TOKEN)
extern "C"  Il2CppObject * Parser_ParseByToken_m3825560905 (Parser_t2383423553 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.JSON/_JSON/Parser::ParseString()
extern "C"  String_t* Parser_ParseString_m2727177408 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnePF.JSON/_JSON/Parser::ParseNumber()
extern "C"  Il2CppObject * Parser_ParseNumber_m3254755082 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.JSON/_JSON/Parser::EatWhitespace()
extern "C"  void Parser_EatWhitespace_m1457471726 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char OnePF.JSON/_JSON/Parser::get_PeekChar()
extern "C"  uint16_t Parser_get_PeekChar_m2421335077 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char OnePF.JSON/_JSON/Parser::get_NextChar()
extern "C"  uint16_t Parser_get_NextChar_m965618621 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnePF.JSON/_JSON/Parser::get_NextWord()
extern "C"  String_t* Parser_get_NextWord_m2256749100 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnePF.JSON/_JSON/Parser/TOKEN OnePF.JSON/_JSON/Parser::get_NextToken()
extern "C"  int32_t Parser_get_NextToken_m2219232515 (Parser_t2383423553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
