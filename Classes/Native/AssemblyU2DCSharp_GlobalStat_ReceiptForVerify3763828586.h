﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalStat/ReceiptForVerify
struct  ReceiptForVerify_t3763828586  : public Il2CppObject
{
public:
	// System.String GlobalStat/ReceiptForVerify::receipt
	String_t* ___receipt_0;
	// System.String GlobalStat/ReceiptForVerify::type
	String_t* ___type_1;
	// System.String GlobalStat/ReceiptForVerify::currency
	String_t* ___currency_2;
	// System.Int32 GlobalStat/ReceiptForVerify::price
	int32_t ___price_3;
	// System.String GlobalStat/ReceiptForVerify::androidSignature
	String_t* ___androidSignature_4;
	// System.String GlobalStat/ReceiptForVerify::originalJson
	String_t* ___originalJson_5;
	// System.String GlobalStat/ReceiptForVerify::sku
	String_t* ___sku_6;

public:
	inline static int32_t get_offset_of_receipt_0() { return static_cast<int32_t>(offsetof(ReceiptForVerify_t3763828586, ___receipt_0)); }
	inline String_t* get_receipt_0() const { return ___receipt_0; }
	inline String_t** get_address_of_receipt_0() { return &___receipt_0; }
	inline void set_receipt_0(String_t* value)
	{
		___receipt_0 = value;
		Il2CppCodeGenWriteBarrier(&___receipt_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(ReceiptForVerify_t3763828586, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_currency_2() { return static_cast<int32_t>(offsetof(ReceiptForVerify_t3763828586, ___currency_2)); }
	inline String_t* get_currency_2() const { return ___currency_2; }
	inline String_t** get_address_of_currency_2() { return &___currency_2; }
	inline void set_currency_2(String_t* value)
	{
		___currency_2 = value;
		Il2CppCodeGenWriteBarrier(&___currency_2, value);
	}

	inline static int32_t get_offset_of_price_3() { return static_cast<int32_t>(offsetof(ReceiptForVerify_t3763828586, ___price_3)); }
	inline int32_t get_price_3() const { return ___price_3; }
	inline int32_t* get_address_of_price_3() { return &___price_3; }
	inline void set_price_3(int32_t value)
	{
		___price_3 = value;
	}

	inline static int32_t get_offset_of_androidSignature_4() { return static_cast<int32_t>(offsetof(ReceiptForVerify_t3763828586, ___androidSignature_4)); }
	inline String_t* get_androidSignature_4() const { return ___androidSignature_4; }
	inline String_t** get_address_of_androidSignature_4() { return &___androidSignature_4; }
	inline void set_androidSignature_4(String_t* value)
	{
		___androidSignature_4 = value;
		Il2CppCodeGenWriteBarrier(&___androidSignature_4, value);
	}

	inline static int32_t get_offset_of_originalJson_5() { return static_cast<int32_t>(offsetof(ReceiptForVerify_t3763828586, ___originalJson_5)); }
	inline String_t* get_originalJson_5() const { return ___originalJson_5; }
	inline String_t** get_address_of_originalJson_5() { return &___originalJson_5; }
	inline void set_originalJson_5(String_t* value)
	{
		___originalJson_5 = value;
		Il2CppCodeGenWriteBarrier(&___originalJson_5, value);
	}

	inline static int32_t get_offset_of_sku_6() { return static_cast<int32_t>(offsetof(ReceiptForVerify_t3763828586, ___sku_6)); }
	inline String_t* get_sku_6() const { return ___sku_6; }
	inline String_t** get_address_of_sku_6() { return &___sku_6; }
	inline void set_sku_6(String_t* value)
	{
		___sku_6 = value;
		Il2CppCodeGenWriteBarrier(&___sku_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
