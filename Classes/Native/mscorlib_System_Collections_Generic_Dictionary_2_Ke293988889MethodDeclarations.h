﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1852733134MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2730571001(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t293988889 *, Dictionary_2_t2265680905 *, const MethodInfo*))KeyCollection__ctor_m3432069128_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2693999805(__this, ___item0, method) ((  void (*) (KeyCollection_t293988889 *, SingletonId_t1838183899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3101899854_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2487704948(__this, method) ((  void (*) (KeyCollection_t293988889 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m164109637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m866154929(__this, ___item0, method) ((  bool (*) (KeyCollection_t293988889 *, SingletonId_t1838183899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2402136956_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m712090454(__this, ___item0, method) ((  bool (*) (KeyCollection_t293988889 *, SingletonId_t1838183899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1325978593_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3824991366(__this, method) ((  Il2CppObject* (*) (KeyCollection_t293988889 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3150060033_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3471094758(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t293988889 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2134327863_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3149673589(__this, method) ((  Il2CppObject * (*) (KeyCollection_t293988889 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3067601266_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2032290002(__this, method) ((  bool (*) (KeyCollection_t293988889 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m642268125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1685049348(__this, method) ((  bool (*) (KeyCollection_t293988889 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1412236495_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4150957558(__this, method) ((  Il2CppObject * (*) (KeyCollection_t293988889 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3575527099_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1698217902(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t293988889 *, SingletonIdU5BU5D_t822569050*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2803941053_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2466787643(__this, method) ((  Enumerator_t2032708847  (*) (KeyCollection_t293988889 *, const MethodInfo*))KeyCollection_GetEnumerator_m2980864032_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Zenject.SingletonId,Zenject.SingletonLazyCreator>::get_Count()
#define KeyCollection_get_Count_m2712546878(__this, method) ((  int32_t (*) (KeyCollection_t293988889 *, const MethodInfo*))KeyCollection_get_Count_m1374340501_gshared)(__this, method)
