﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_427065056.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3133764025_gshared (KeyValuePair_2_t427065056 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3133764025(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t427065056 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3133764025_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2796000783_gshared (KeyValuePair_2_t427065056 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2796000783(__this, method) ((  int32_t (*) (KeyValuePair_2_t427065056 *, const MethodInfo*))KeyValuePair_2_get_Key_m2796000783_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2230450384_gshared (KeyValuePair_2_t427065056 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2230450384(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t427065056 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2230450384_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2542701839_gshared (KeyValuePair_2_t427065056 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2542701839(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t427065056 *, const MethodInfo*))KeyValuePair_2_get_Value_m2542701839_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1258579152_gshared (KeyValuePair_2_t427065056 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1258579152(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t427065056 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1258579152_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2787054162_gshared (KeyValuePair_2_t427065056 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2787054162(__this, method) ((  String_t* (*) (KeyValuePair_2_t427065056 *, const MethodInfo*))KeyValuePair_2_ToString_m2787054162_gshared)(__this, method)
