﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687MethodDeclarations.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<System.Collections.Generic.IList`1<System.Boolean>>::.ctor(System.Boolean)
#define OperatorObservableBase_1__ctor_m625855174(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t1441642626 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m1121449362_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Collections.Generic.IList`1<System.Boolean>>::IsRequiredSubscribeOnCurrentThread()
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m326961492(__this, method) ((  bool (*) (OperatorObservableBase_1_t1441642626 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1834630736_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Collections.Generic.IList`1<System.Boolean>>::Subscribe(UniRx.IObserver`1<T>)
#define OperatorObservableBase_1_Subscribe_m694969806(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t1441642626 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m3551460656_gshared)(__this, ___observer0, method)
