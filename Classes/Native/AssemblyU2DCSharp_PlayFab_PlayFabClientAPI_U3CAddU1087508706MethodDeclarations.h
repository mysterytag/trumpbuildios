﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<AddUserVirtualCurrency>c__AnonStorey9F
struct U3CAddUserVirtualCurrencyU3Ec__AnonStorey9F_t1087508706;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<AddUserVirtualCurrency>c__AnonStorey9F::.ctor()
extern "C"  void U3CAddUserVirtualCurrencyU3Ec__AnonStorey9F__ctor_m3849909521 (U3CAddUserVirtualCurrencyU3Ec__AnonStorey9F_t1087508706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<AddUserVirtualCurrency>c__AnonStorey9F::<>m__8C(PlayFab.CallRequestContainer)
extern "C"  void U3CAddUserVirtualCurrencyU3Ec__AnonStorey9F_U3CU3Em__8C_m2269704698 (U3CAddUserVirtualCurrencyU3Ec__AnonStorey9F_t1087508706 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
