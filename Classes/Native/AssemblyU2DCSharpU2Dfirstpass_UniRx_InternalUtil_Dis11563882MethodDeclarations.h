﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<System.Object>
struct DisposedObserver_1_t11563882;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2745571030_gshared (DisposedObserver_1_t11563882 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m2745571030(__this, method) ((  void (*) (DisposedObserver_1_t11563882 *, const MethodInfo*))DisposedObserver_1__ctor_m2745571030_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3026227095_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m3026227095(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m3026227095_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m1100894688_gshared (DisposedObserver_1_t11563882 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m1100894688(__this, method) ((  void (*) (DisposedObserver_1_t11563882 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m1100894688_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m150811405_gshared (DisposedObserver_1_t11563882 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m150811405(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t11563882 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m150811405_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Object>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m4214543358_gshared (DisposedObserver_1_t11563882 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m4214543358(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t11563882 *, Il2CppObject *, const MethodInfo*))DisposedObserver_1_OnNext_m4214543358_gshared)(__this, ___value0, method)
