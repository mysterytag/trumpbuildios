﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetLeaderboardAroundPlayer>c__AnonStorey90
struct U3CGetLeaderboardAroundPlayerU3Ec__AnonStorey90_t2929766705;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetLeaderboardAroundPlayer>c__AnonStorey90::.ctor()
extern "C"  void U3CGetLeaderboardAroundPlayerU3Ec__AnonStorey90__ctor_m268772770 (U3CGetLeaderboardAroundPlayerU3Ec__AnonStorey90_t2929766705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetLeaderboardAroundPlayer>c__AnonStorey90::<>m__7D(PlayFab.CallRequestContainer)
extern "C"  void U3CGetLeaderboardAroundPlayerU3Ec__AnonStorey90_U3CU3Em__7D_m3203602221 (U3CGetLeaderboardAroundPlayerU3Ec__AnonStorey90_t2929766705 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
