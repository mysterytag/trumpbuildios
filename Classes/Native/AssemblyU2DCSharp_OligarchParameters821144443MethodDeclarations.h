﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OligarchParameters
struct OligarchParameters_t821144443;

#include "codegen/il2cpp-codegen.h"

// System.Void OligarchParameters::.ctor()
extern "C"  void OligarchParameters__ctor_m2486046592 (OligarchParameters_t821144443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OligarchParameters::IsIpad()
extern "C"  bool OligarchParameters_IsIpad_m1177273484 (OligarchParameters_t821144443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
