﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UnlockContainerItemResult
struct  UnlockContainerItemResult_t2837935741  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.UnlockContainerItemResult::<UnlockedItemInstanceId>k__BackingField
	String_t* ___U3CUnlockedItemInstanceIdU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.UnlockContainerItemResult::<UnlockedWithItemInstanceId>k__BackingField
	String_t* ___U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.UnlockContainerItemResult::<GrantedItems>k__BackingField
	List_1_t1322103281 * ___U3CGrantedItemsU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.UnlockContainerItemResult::<VirtualCurrency>k__BackingField
	Dictionary_2_t2623623230 * ___U3CVirtualCurrencyU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CUnlockedItemInstanceIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnlockContainerItemResult_t2837935741, ___U3CUnlockedItemInstanceIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CUnlockedItemInstanceIdU3Ek__BackingField_2() const { return ___U3CUnlockedItemInstanceIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUnlockedItemInstanceIdU3Ek__BackingField_2() { return &___U3CUnlockedItemInstanceIdU3Ek__BackingField_2; }
	inline void set_U3CUnlockedItemInstanceIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CUnlockedItemInstanceIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnlockedItemInstanceIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnlockContainerItemResult_t2837935741, ___U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3() const { return ___U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3() { return &___U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3; }
	inline void set_U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUnlockedWithItemInstanceIdU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CGrantedItemsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnlockContainerItemResult_t2837935741, ___U3CGrantedItemsU3Ek__BackingField_4)); }
	inline List_1_t1322103281 * get_U3CGrantedItemsU3Ek__BackingField_4() const { return ___U3CGrantedItemsU3Ek__BackingField_4; }
	inline List_1_t1322103281 ** get_address_of_U3CGrantedItemsU3Ek__BackingField_4() { return &___U3CGrantedItemsU3Ek__BackingField_4; }
	inline void set_U3CGrantedItemsU3Ek__BackingField_4(List_1_t1322103281 * value)
	{
		___U3CGrantedItemsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGrantedItemsU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnlockContainerItemResult_t2837935741, ___U3CVirtualCurrencyU3Ek__BackingField_5)); }
	inline Dictionary_2_t2623623230 * get_U3CVirtualCurrencyU3Ek__BackingField_5() const { return ___U3CVirtualCurrencyU3Ek__BackingField_5; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CVirtualCurrencyU3Ek__BackingField_5() { return &___U3CVirtualCurrencyU3Ek__BackingField_5; }
	inline void set_U3CVirtualCurrencyU3Ek__BackingField_5(Dictionary_2_t2623623230 * value)
	{
		___U3CVirtualCurrencyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
