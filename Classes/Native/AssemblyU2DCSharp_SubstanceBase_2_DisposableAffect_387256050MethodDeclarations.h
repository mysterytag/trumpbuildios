﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/DisposableAffect<System.Single,System.Single>
struct DisposableAffect_t387256050;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/DisposableAffect<System.Single,System.Single>::.ctor()
extern "C"  void DisposableAffect__ctor_m895716676_gshared (DisposableAffect_t387256050 * __this, const MethodInfo* method);
#define DisposableAffect__ctor_m895716676(__this, method) ((  void (*) (DisposableAffect_t387256050 *, const MethodInfo*))DisposableAffect__ctor_m895716676_gshared)(__this, method)
// System.Void SubstanceBase`2/DisposableAffect<System.Single,System.Single>::Dispose()
extern "C"  void DisposableAffect_Dispose_m1688549505_gshared (DisposableAffect_t387256050 * __this, const MethodInfo* method);
#define DisposableAffect_Dispose_m1688549505(__this, method) ((  void (*) (DisposableAffect_t387256050 *, const MethodInfo*))DisposableAffect_Dispose_m1688549505_gshared)(__this, method)
