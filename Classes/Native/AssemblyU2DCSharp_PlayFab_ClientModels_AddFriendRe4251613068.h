﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.AddFriendResult
struct  AddFriendResult_t4251613068  : public PlayFabResultCommon_t379512675
{
public:
	// System.Boolean PlayFab.ClientModels.AddFriendResult::<Created>k__BackingField
	bool ___U3CCreatedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCreatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AddFriendResult_t4251613068, ___U3CCreatedU3Ek__BackingField_2)); }
	inline bool get_U3CCreatedU3Ek__BackingField_2() const { return ___U3CCreatedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CCreatedU3Ek__BackingField_2() { return &___U3CCreatedU3Ek__BackingField_2; }
	inline void set_U3CCreatedU3Ek__BackingField_2(bool value)
	{
		___U3CCreatedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
