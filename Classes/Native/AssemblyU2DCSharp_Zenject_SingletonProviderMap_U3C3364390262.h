﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonProviderMap/<AddCreatorFromMethod>c__AnonStorey197`1<System.Object>
struct  U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262  : public Il2CppObject
{
public:
	// System.Func`2<Zenject.InjectContext,TConcrete> Zenject.SingletonProviderMap/<AddCreatorFromMethod>c__AnonStorey197`1::method
	Func_2_t2621245597 * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262, ___method_0)); }
	inline Func_2_t2621245597 * get_method_0() const { return ___method_0; }
	inline Func_2_t2621245597 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Func_2_t2621245597 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
