﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Func_6_gen698299427MethodDeclarations.h"

// System.Void ModestTree.Util.Func`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
#define Func_6__ctor_m4088833056(__this, ___object0, ___method1, method) ((  void (*) (Func_6_t3355333698 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_6__ctor_m1024459519_gshared)(__this, ___object0, ___method1, method)
// TResult ModestTree.Util.Func`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5)
#define Func_6_Invoke_m2807371993(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method) ((  Il2CppObject * (*) (Func_6_t3355333698 *, DiContainer_t2383114449 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Func_6_Invoke_m272686264_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method)
// System.IAsyncResult ModestTree.Util.Func`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,System.AsyncCallback,System.Object)
#define Func_6_BeginInvoke_m1108706828(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___callback5, ___object6, method) ((  Il2CppObject * (*) (Func_6_t3355333698 *, DiContainer_t2383114449 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_6_BeginInvoke_m2607504747_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___callback5, ___object6, method)
// TResult ModestTree.Util.Func`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
#define Func_6_EndInvoke_m2634289358(__this, ___result0, method) ((  Il2CppObject * (*) (Func_6_t3355333698 *, Il2CppObject *, const MethodInfo*))Func_6_EndInvoke_m857310637_gshared)(__this, ___result0, method)
