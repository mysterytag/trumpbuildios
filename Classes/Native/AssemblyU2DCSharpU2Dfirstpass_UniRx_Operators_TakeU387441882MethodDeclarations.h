﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>
struct TakeUntilOther_t387441882;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>
struct TakeUntil_t1361590388;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.Operators.TakeUntilObservable`2/TakeUntil<T,TOther>,System.IDisposable)
extern "C"  void TakeUntilOther__ctor_m1790894793_gshared (TakeUntilOther_t387441882 * __this, TakeUntil_t1361590388 * ___sourceObserver0, Il2CppObject * ___subscription1, const MethodInfo* method);
#define TakeUntilOther__ctor_m1790894793(__this, ___sourceObserver0, ___subscription1, method) ((  void (*) (TakeUntilOther_t387441882 *, TakeUntil_t1361590388 *, Il2CppObject *, const MethodInfo*))TakeUntilOther__ctor_m1790894793_gshared)(__this, ___sourceObserver0, ___subscription1, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>::OnNext(TOther)
extern "C"  void TakeUntilOther_OnNext_m257793126_gshared (TakeUntilOther_t387441882 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define TakeUntilOther_OnNext_m257793126(__this, ___value0, method) ((  void (*) (TakeUntilOther_t387441882 *, Unit_t2558286038 , const MethodInfo*))TakeUntilOther_OnNext_m257793126_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>::OnError(System.Exception)
extern "C"  void TakeUntilOther_OnError_m2727693803_gshared (TakeUntilOther_t387441882 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeUntilOther_OnError_m2727693803(__this, ___error0, method) ((  void (*) (TakeUntilOther_t387441882 *, Exception_t1967233988 *, const MethodInfo*))TakeUntilOther_OnError_m2727693803_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<UniRx.Unit,UniRx.Unit>::OnCompleted()
extern "C"  void TakeUntilOther_OnCompleted_m2505132478_gshared (TakeUntilOther_t387441882 * __this, const MethodInfo* method);
#define TakeUntilOther_OnCompleted_m2505132478(__this, method) ((  void (*) (TakeUntilOther_t387441882 *, const MethodInfo*))TakeUntilOther_OnCompleted_m2505132478_gshared)(__this, method)
