﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnceStream`1<System.Object>
struct OnceStream_1_t151376059;

#include "codegen/il2cpp-codegen.h"

// System.Void OnceStream`1<System.Object>::.ctor()
extern "C"  void OnceStream_1__ctor_m2654431905_gshared (OnceStream_1_t151376059 * __this, const MethodInfo* method);
#define OnceStream_1__ctor_m2654431905(__this, method) ((  void (*) (OnceStream_1_t151376059 *, const MethodInfo*))OnceStream_1__ctor_m2654431905_gshared)(__this, method)
