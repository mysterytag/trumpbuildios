﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.IAsyncResult>
struct Action_1_t686135974;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey45
struct  U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396  : public Il2CppObject
{
public:
	// System.Action`1<System.IAsyncResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey45::end
	Action_1_t686135974 * ___end_0;

public:
	inline static int32_t get_offset_of_end_0() { return static_cast<int32_t>(offsetof(U3CFromAsyncPatternU3Ec__AnonStorey45_t3402247396, ___end_0)); }
	inline Action_1_t686135974 * get_end_0() const { return ___end_0; }
	inline Action_1_t686135974 ** get_address_of_end_0() { return &___end_0; }
	inline void set_end_0(Action_1_t686135974 * value)
	{
		___end_0 = value;
		Il2CppCodeGenWriteBarrier(&___end_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
