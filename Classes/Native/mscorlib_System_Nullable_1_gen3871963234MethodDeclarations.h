﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<System.UInt32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2620066971_gshared (Nullable_1_t3871963234 * __this, uint32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2620066971(__this, ___value0, method) ((  void (*) (Nullable_1_t3871963234 *, uint32_t, const MethodInfo*))Nullable_1__ctor_m2620066971_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.UInt32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3075205198_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3075205198(__this, method) ((  bool (*) (Nullable_1_t3871963234 *, const MethodInfo*))Nullable_1_get_HasValue_m3075205198_gshared)(__this, method)
// T System.Nullable`1<System.UInt32>::get_Value()
extern "C"  uint32_t Nullable_1_get_Value_m22896840_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m22896840(__this, method) ((  uint32_t (*) (Nullable_1_t3871963234 *, const MethodInfo*))Nullable_1_get_Value_m22896840_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.UInt32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3727889584_gshared (Nullable_1_t3871963234 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3727889584(__this, ___other0, method) ((  bool (*) (Nullable_1_t3871963234 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3727889584_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.UInt32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2822690959_gshared (Nullable_1_t3871963234 * __this, Nullable_1_t3871963234  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2822690959(__this, ___other0, method) ((  bool (*) (Nullable_1_t3871963234 *, Nullable_1_t3871963234 , const MethodInfo*))Nullable_1_Equals_m2822690959_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.UInt32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3056501268_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3056501268(__this, method) ((  int32_t (*) (Nullable_1_t3871963234 *, const MethodInfo*))Nullable_1_GetHashCode_m3056501268_gshared)(__this, method)
// T System.Nullable`1<System.UInt32>::GetValueOrDefault()
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m4060687613_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m4060687613(__this, method) ((  uint32_t (*) (Nullable_1_t3871963234 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m4060687613_gshared)(__this, method)
// System.String System.Nullable`1<System.UInt32>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3067490610_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3067490610(__this, method) ((  String_t* (*) (Nullable_1_t3871963234 *, const MethodInfo*))Nullable_1_ToString_m3067490610_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<System.UInt32>::op_Implicit(T)
extern "C"  Nullable_1_t3871963234  Nullable_1_op_Implicit_m66995085_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m66995085(__this /* static, unused */, ___value0, method) ((  Nullable_1_t3871963234  (*) (Il2CppObject * /* static, unused */, uint32_t, const MethodInfo*))Nullable_1_op_Implicit_m66995085_gshared)(__this /* static, unused */, ___value0, method)
