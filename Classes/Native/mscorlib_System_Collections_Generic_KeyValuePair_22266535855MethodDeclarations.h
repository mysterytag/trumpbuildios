﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Cell`1<System.Single>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2389349387(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2266535855 *, String_t*, Cell_1_t1140306653 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Cell`1<System.Single>>::get_Key()
#define KeyValuePair_2_get_Key_m684680957(__this, method) ((  String_t* (*) (KeyValuePair_2_t2266535855 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Cell`1<System.Single>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4046793534(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2266535855 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Cell`1<System.Single>>::get_Value()
#define KeyValuePair_2_get_Value_m1951920737(__this, method) ((  Cell_1_t1140306653 * (*) (KeyValuePair_2_t2266535855 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Cell`1<System.Single>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2758586430(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2266535855 *, Cell_1_t1140306653 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Cell`1<System.Single>>::ToString()
#define KeyValuePair_2_ToString_m1386841482(__this, method) ((  String_t* (*) (KeyValuePair_2_t2266535855 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
