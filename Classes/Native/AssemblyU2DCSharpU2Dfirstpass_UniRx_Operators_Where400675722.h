﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.WhereObservable`1<UniRx.Unit>
struct WhereObservable_1_t3756845987;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhereObservable`1/Where<UniRx.Unit>
struct  Where_t400675722  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1/Where::parent
	WhereObservable_1_t3756845987 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Where_t400675722, ___parent_2)); }
	inline WhereObservable_1_t3756845987 * get_parent_2() const { return ___parent_2; }
	inline WhereObservable_1_t3756845987 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereObservable_1_t3756845987 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
