﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextMesh
struct TextMesh_t583678247;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t503173063;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Counter
struct  Counter_t2622483932  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.TextMesh Counter::oldCharacter
	TextMesh_t583678247 * ___oldCharacter_2;
	// UnityEngine.TextMesh Counter::newCharacter
	TextMesh_t583678247 * ___newCharacter_3;
	// UnityEngine.Transform Counter::mask
	Transform_t284553113 * ___mask_4;
	// UnityEngine.SpriteRenderer Counter::foldAnimation
	SpriteRenderer_t2223784725 * ___foldAnimation_5;
	// UnityEngine.Sprite[] Counter::frames
	SpriteU5BU5D_t503173063* ___frames_6;

public:
	inline static int32_t get_offset_of_oldCharacter_2() { return static_cast<int32_t>(offsetof(Counter_t2622483932, ___oldCharacter_2)); }
	inline TextMesh_t583678247 * get_oldCharacter_2() const { return ___oldCharacter_2; }
	inline TextMesh_t583678247 ** get_address_of_oldCharacter_2() { return &___oldCharacter_2; }
	inline void set_oldCharacter_2(TextMesh_t583678247 * value)
	{
		___oldCharacter_2 = value;
		Il2CppCodeGenWriteBarrier(&___oldCharacter_2, value);
	}

	inline static int32_t get_offset_of_newCharacter_3() { return static_cast<int32_t>(offsetof(Counter_t2622483932, ___newCharacter_3)); }
	inline TextMesh_t583678247 * get_newCharacter_3() const { return ___newCharacter_3; }
	inline TextMesh_t583678247 ** get_address_of_newCharacter_3() { return &___newCharacter_3; }
	inline void set_newCharacter_3(TextMesh_t583678247 * value)
	{
		___newCharacter_3 = value;
		Il2CppCodeGenWriteBarrier(&___newCharacter_3, value);
	}

	inline static int32_t get_offset_of_mask_4() { return static_cast<int32_t>(offsetof(Counter_t2622483932, ___mask_4)); }
	inline Transform_t284553113 * get_mask_4() const { return ___mask_4; }
	inline Transform_t284553113 ** get_address_of_mask_4() { return &___mask_4; }
	inline void set_mask_4(Transform_t284553113 * value)
	{
		___mask_4 = value;
		Il2CppCodeGenWriteBarrier(&___mask_4, value);
	}

	inline static int32_t get_offset_of_foldAnimation_5() { return static_cast<int32_t>(offsetof(Counter_t2622483932, ___foldAnimation_5)); }
	inline SpriteRenderer_t2223784725 * get_foldAnimation_5() const { return ___foldAnimation_5; }
	inline SpriteRenderer_t2223784725 ** get_address_of_foldAnimation_5() { return &___foldAnimation_5; }
	inline void set_foldAnimation_5(SpriteRenderer_t2223784725 * value)
	{
		___foldAnimation_5 = value;
		Il2CppCodeGenWriteBarrier(&___foldAnimation_5, value);
	}

	inline static int32_t get_offset_of_frames_6() { return static_cast<int32_t>(offsetof(Counter_t2622483932, ___frames_6)); }
	inline SpriteU5BU5D_t503173063* get_frames_6() const { return ___frames_6; }
	inline SpriteU5BU5D_t503173063** get_address_of_frames_6() { return &___frames_6; }
	inline void set_frames_6(SpriteU5BU5D_t503173063* value)
	{
		___frames_6 = value;
		Il2CppCodeGenWriteBarrier(&___frames_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
