﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ToObservableObservable`1/ToObservable/<Run>c__AnonStorey71<System.Object>
struct U3CRunU3Ec__AnonStorey71_t3658689275;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.ToObservableObservable`1/ToObservable/<Run>c__AnonStorey71<System.Object>::.ctor()
extern "C"  void U3CRunU3Ec__AnonStorey71__ctor_m4006368920_gshared (U3CRunU3Ec__AnonStorey71_t3658689275 * __this, const MethodInfo* method);
#define U3CRunU3Ec__AnonStorey71__ctor_m4006368920(__this, method) ((  void (*) (U3CRunU3Ec__AnonStorey71_t3658689275 *, const MethodInfo*))U3CRunU3Ec__AnonStorey71__ctor_m4006368920_gshared)(__this, method)
// System.Void UniRx.Operators.ToObservableObservable`1/ToObservable/<Run>c__AnonStorey71<System.Object>::<>m__8E(System.Action)
extern "C"  void U3CRunU3Ec__AnonStorey71_U3CU3Em__8E_m3218205967_gshared (U3CRunU3Ec__AnonStorey71_t3658689275 * __this, Action_t437523947 * ___self0, const MethodInfo* method);
#define U3CRunU3Ec__AnonStorey71_U3CU3Em__8E_m3218205967(__this, ___self0, method) ((  void (*) (U3CRunU3Ec__AnonStorey71_t3658689275 *, Action_t437523947 *, const MethodInfo*))U3CRunU3Ec__AnonStorey71_U3CU3Em__8E_m3218205967_gshared)(__this, ___self0, method)
