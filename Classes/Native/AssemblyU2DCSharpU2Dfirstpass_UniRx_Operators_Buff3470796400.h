﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t11523773;
// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3354260463.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`1/BufferTC<System.Object>
struct  BufferTC_t3470796400  : public OperatorObserverBase_2_t3354260463
{
public:
	// UniRx.Operators.BufferObservable`1<T> UniRx.Operators.BufferObservable`1/BufferTC::parent
	BufferObservable_1_t574294898 * ___parent_3;
	// System.Object UniRx.Operators.BufferObservable`1/BufferTC::gate
	Il2CppObject * ___gate_4;
	// System.Collections.Generic.List`1<T> UniRx.Operators.BufferObservable`1/BufferTC::list
	List_1_t1634065389 * ___list_5;
	// System.Int64 UniRx.Operators.BufferObservable`1/BufferTC::timerId
	int64_t ___timerId_6;
	// UniRx.SerialDisposable UniRx.Operators.BufferObservable`1/BufferTC::timerD
	SerialDisposable_t2547852742 * ___timerD_7;

public:
	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(BufferTC_t3470796400, ___parent_3)); }
	inline BufferObservable_1_t574294898 * get_parent_3() const { return ___parent_3; }
	inline BufferObservable_1_t574294898 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(BufferObservable_1_t574294898 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier(&___parent_3, value);
	}

	inline static int32_t get_offset_of_gate_4() { return static_cast<int32_t>(offsetof(BufferTC_t3470796400, ___gate_4)); }
	inline Il2CppObject * get_gate_4() const { return ___gate_4; }
	inline Il2CppObject ** get_address_of_gate_4() { return &___gate_4; }
	inline void set_gate_4(Il2CppObject * value)
	{
		___gate_4 = value;
		Il2CppCodeGenWriteBarrier(&___gate_4, value);
	}

	inline static int32_t get_offset_of_list_5() { return static_cast<int32_t>(offsetof(BufferTC_t3470796400, ___list_5)); }
	inline List_1_t1634065389 * get_list_5() const { return ___list_5; }
	inline List_1_t1634065389 ** get_address_of_list_5() { return &___list_5; }
	inline void set_list_5(List_1_t1634065389 * value)
	{
		___list_5 = value;
		Il2CppCodeGenWriteBarrier(&___list_5, value);
	}

	inline static int32_t get_offset_of_timerId_6() { return static_cast<int32_t>(offsetof(BufferTC_t3470796400, ___timerId_6)); }
	inline int64_t get_timerId_6() const { return ___timerId_6; }
	inline int64_t* get_address_of_timerId_6() { return &___timerId_6; }
	inline void set_timerId_6(int64_t value)
	{
		___timerId_6 = value;
	}

	inline static int32_t get_offset_of_timerD_7() { return static_cast<int32_t>(offsetof(BufferTC_t3470796400, ___timerD_7)); }
	inline SerialDisposable_t2547852742 * get_timerD_7() const { return ___timerD_7; }
	inline SerialDisposable_t2547852742 ** get_address_of_timerD_7() { return &___timerD_7; }
	inline void set_timerD_7(SerialDisposable_t2547852742 * value)
	{
		___timerD_7 = value;
		Il2CppCodeGenWriteBarrier(&___timerD_7, value);
	}
};

struct BufferTC_t3470796400_StaticFields
{
public:
	// T[] UniRx.Operators.BufferObservable`1/BufferTC::EmptyArray
	ObjectU5BU5D_t11523773* ___EmptyArray_2;

public:
	inline static int32_t get_offset_of_EmptyArray_2() { return static_cast<int32_t>(offsetof(BufferTC_t3470796400_StaticFields, ___EmptyArray_2)); }
	inline ObjectU5BU5D_t11523773* get_EmptyArray_2() const { return ___EmptyArray_2; }
	inline ObjectU5BU5D_t11523773** get_address_of_EmptyArray_2() { return &___EmptyArray_2; }
	inline void set_EmptyArray_2(ObjectU5BU5D_t11523773* value)
	{
		___EmptyArray_2 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyArray_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
