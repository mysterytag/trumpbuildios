﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationResult
struct AndroidDevicePushNotificationRegistrationResult_t3915401392;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationResult::.ctor()
extern "C"  void AndroidDevicePushNotificationRegistrationResult__ctor_m1703173369 (AndroidDevicePushNotificationRegistrationResult_t3915401392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
