﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>
struct Subject_1_t2490931930;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableMoveTrigger
struct  ObservableMoveTrigger_t1636085253  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData> UniRx.Triggers.ObservableMoveTrigger::onMove
	Subject_1_t2490931930 * ___onMove_8;

public:
	inline static int32_t get_offset_of_onMove_8() { return static_cast<int32_t>(offsetof(ObservableMoveTrigger_t1636085253, ___onMove_8)); }
	inline Subject_1_t2490931930 * get_onMove_8() const { return ___onMove_8; }
	inline Subject_1_t2490931930 ** get_address_of_onMove_8() { return &___onMove_8; }
	inline void set_onMove_8(Subject_1_t2490931930 * value)
	{
		___onMove_8 = value;
		Il2CppCodeGenWriteBarrier(&___onMove_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
