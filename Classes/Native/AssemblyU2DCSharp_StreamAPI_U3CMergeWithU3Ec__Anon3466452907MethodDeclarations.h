﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<MergeWith>c__AnonStorey138
struct U3CMergeWithU3Ec__AnonStorey138_t3466452907;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void StreamAPI/<MergeWith>c__AnonStorey138::.ctor()
extern "C"  void U3CMergeWithU3Ec__AnonStorey138__ctor_m446152885 (U3CMergeWithU3Ec__AnonStorey138_t3466452907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable StreamAPI/<MergeWith>c__AnonStorey138::<>m__1C7(System.Action,Priority)
extern "C"  Il2CppObject * U3CMergeWithU3Ec__AnonStorey138_U3CU3Em__1C7_m1322318171 (U3CMergeWithU3Ec__AnonStorey138_t3466452907 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
