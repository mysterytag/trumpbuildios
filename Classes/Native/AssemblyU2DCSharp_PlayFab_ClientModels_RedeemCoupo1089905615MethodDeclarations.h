﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RedeemCouponResult
struct RedeemCouponResult_t1089905615;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.RedeemCouponResult::.ctor()
extern "C"  void RedeemCouponResult__ctor_m553476814 (RedeemCouponResult_t1089905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.RedeemCouponResult::get_GrantedItems()
extern "C"  List_1_t1322103281 * RedeemCouponResult_get_GrantedItems_m1891020349 (RedeemCouponResult_t1089905615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RedeemCouponResult::set_GrantedItems(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void RedeemCouponResult_set_GrantedItems_m1860105122 (RedeemCouponResult_t1089905615 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
