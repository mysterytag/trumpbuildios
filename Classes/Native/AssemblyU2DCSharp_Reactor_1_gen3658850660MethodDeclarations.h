﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"

// System.Void Reactor`1<ICell`1<System.Int32>>::.ctor()
#define Reactor_1__ctor_m3696267043(__this, method) ((  void (*) (Reactor_1_t3658850660 *, const MethodInfo*))Reactor_1__ctor_m1161914514_gshared)(__this, method)
// System.Void Reactor`1<ICell`1<System.Int32>>::React(T)
#define Reactor_1_React_m2451823454(__this, ___t0, method) ((  void (*) (Reactor_1_t3658850660 *, Il2CppObject*, const MethodInfo*))Reactor_1_React_m1196306383_gshared)(__this, ___t0, method)
// System.Void Reactor`1<ICell`1<System.Int32>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m3484620009(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t3658850660 *, Il2CppObject*, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1003670938_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<ICell`1<System.Int32>>::Empty()
#define Reactor_1_Empty_m2278917658(__this, method) ((  bool (*) (Reactor_1_t3658850660 *, const MethodInfo*))Reactor_1_Empty_m132443017_gshared)(__this, method)
// System.IDisposable Reactor`1<ICell`1<System.Int32>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m188879498(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t3658850660 *, Action_1_t252531173 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m1495770619_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<ICell`1<System.Int32>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m3178016215(__this, method) ((  void (*) (Reactor_1_t3658850660 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m912916552_gshared)(__this, method)
// System.Void Reactor`1<ICell`1<System.Int32>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m1387513728(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t3658850660 *, Action_1_t252531173 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3381282287_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<ICell`1<System.Int32>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m2197332084(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t3658850660 *, Action_1_t252531173 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4227182179_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<ICell`1<System.Int32>>::Clear()
#define Reactor_1_Clear_m1102400334(__this, method) ((  void (*) (Reactor_1_t3658850660 *, const MethodInfo*))Reactor_1_Clear_m2863015101_gshared)(__this, method)
