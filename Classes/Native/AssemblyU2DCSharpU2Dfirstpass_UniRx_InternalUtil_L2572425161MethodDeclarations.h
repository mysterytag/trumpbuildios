﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>
struct ListObserver_1_t2572425161;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.MasterServerEvent>>
struct ImmutableList_1_t1255043596;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>
struct IObserver_1_t194839097;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m16984449_gshared (ListObserver_1_t2572425161 * __this, ImmutableList_1_t1255043596 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m16984449(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2572425161 *, ImmutableList_1_t1255043596 *, const MethodInfo*))ListObserver_1__ctor_m16984449_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1840260601_gshared (ListObserver_1_t2572425161 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m1840260601(__this, method) ((  void (*) (ListObserver_1_t2572425161 *, const MethodInfo*))ListObserver_1_OnCompleted_m1840260601_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m1874635174_gshared (ListObserver_1_t2572425161 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m1874635174(__this, ___error0, method) ((  void (*) (ListObserver_1_t2572425161 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m1874635174_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m687774871_gshared (ListObserver_1_t2572425161 * __this, int32_t ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m687774871(__this, ___value0, method) ((  void (*) (ListObserver_1_t2572425161 *, int32_t, const MethodInfo*))ListObserver_1_OnNext_m687774871_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3398811177_gshared (ListObserver_1_t2572425161 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m3398811177(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2572425161 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m3398811177_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.MasterServerEvent>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m563216514_gshared (ListObserver_1_t2572425161 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m563216514(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2572425161 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m563216514_gshared)(__this, ___observer0, method)
