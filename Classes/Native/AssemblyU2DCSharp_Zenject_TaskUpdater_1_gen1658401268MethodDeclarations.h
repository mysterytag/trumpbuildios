﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen1705995667MethodDeclarations.h"

// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::.ctor(System.Action`1<TTask>)
#define TaskUpdater_1__ctor_m1974438621(__this, ___updateFunc0, method) ((  void (*) (TaskUpdater_1_t1658401268 *, Action_1_t937964726 *, const MethodInfo*))TaskUpdater_1__ctor_m1842795290_gshared)(__this, ___updateFunc0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<Zenject.ITickable>::get_AllTasks()
#define TaskUpdater_1_get_AllTasks_m3223789827(__this, method) ((  Il2CppObject* (*) (TaskUpdater_1_t1658401268 *, const MethodInfo*))TaskUpdater_1_get_AllTasks_m2200620774_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<Zenject.ITickable>::get_ActiveTasks()
#define TaskUpdater_1_get_ActiveTasks_m361650740(__this, method) ((  Il2CppObject* (*) (TaskUpdater_1_t1658401268 *, const MethodInfo*))TaskUpdater_1_get_ActiveTasks_m515292529_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::AddTask(TTask,System.Int32)
#define TaskUpdater_1_AddTask_m2450545658(__this, ___task0, ___priority1, method) ((  void (*) (TaskUpdater_1_t1658401268 *, Il2CppObject *, int32_t, const MethodInfo*))TaskUpdater_1_AddTask_m720801655_gshared)(__this, ___task0, ___priority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::AddTaskInternal(TTask,System.Int32)
#define TaskUpdater_1_AddTaskInternal_m140487511(__this, ___task0, ___priority1, method) ((  void (*) (TaskUpdater_1_t1658401268 *, Il2CppObject *, int32_t, const MethodInfo*))TaskUpdater_1_AddTaskInternal_m1657606612_gshared)(__this, ___task0, ___priority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::RemoveTask(TTask)
#define TaskUpdater_1_RemoveTask_m2480249412(__this, ___task0, method) ((  void (*) (TaskUpdater_1_t1658401268 *, Il2CppObject *, const MethodInfo*))TaskUpdater_1_RemoveTask_m1986106881_gshared)(__this, ___task0, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::OnFrameStart()
#define TaskUpdater_1_OnFrameStart_m1028326720(__this, method) ((  void (*) (TaskUpdater_1_t1658401268 *, const MethodInfo*))TaskUpdater_1_OnFrameStart_m1489365667_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::UpdateAll()
#define TaskUpdater_1_UpdateAll_m3644818414(__this, method) ((  void (*) (TaskUpdater_1_t1658401268 *, const MethodInfo*))TaskUpdater_1_UpdateAll_m611209579_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::UpdateRange(System.Int32,System.Int32)
#define TaskUpdater_1_UpdateRange_m530098396(__this, ___minPriority0, ___maxPriority1, method) ((  void (*) (TaskUpdater_1_t1658401268 *, int32_t, int32_t, const MethodInfo*))TaskUpdater_1_UpdateRange_m1084380479_gshared)(__this, ___minPriority0, ___maxPriority1, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::ClearRemovedTasks(System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>>)
#define TaskUpdater_1_ClearRemovedTasks_m3333840195(__this, ___tasks0, method) ((  void (*) (TaskUpdater_1_t1658401268 *, LinkedList_1_t2757043551 *, const MethodInfo*))TaskUpdater_1_ClearRemovedTasks_m204003072_gshared)(__this, ___tasks0, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::AddQueuedTasks()
#define TaskUpdater_1_AddQueuedTasks_m1352249670(__this, method) ((  void (*) (TaskUpdater_1_t1658401268 *, const MethodInfo*))TaskUpdater_1_AddQueuedTasks_m2029046249_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<Zenject.ITickable>::InsertTaskSorted(Zenject.TaskUpdater`1/TaskInfo<TTask>)
#define TaskUpdater_1_InsertTaskSorted_m3808418806(__this, ___task0, method) ((  void (*) (TaskUpdater_1_t1658401268 *, TaskInfo_t1016914005 *, const MethodInfo*))TaskUpdater_1_InsertTaskSorted_m3714982387_gshared)(__this, ___task0, method)
// TTask Zenject.TaskUpdater`1<Zenject.ITickable>::<AddTaskInternal>m__2E5(Zenject.TaskUpdater`1/TaskInfo<TTask>)
#define TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m4106447477(__this /* static, unused */, ___x0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, TaskInfo_t1016914005 *, const MethodInfo*))TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m2398513880_gshared)(__this /* static, unused */, ___x0, method)
