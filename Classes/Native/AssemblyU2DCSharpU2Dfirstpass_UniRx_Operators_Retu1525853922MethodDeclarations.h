﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ReturnObservable`1<System.Object>
struct ReturnObservable_1_t1525853922;
// System.Object
struct Il2CppObject;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.ReturnObservable`1<System.Object>::.ctor(T,UniRx.IScheduler)
extern "C"  void ReturnObservable_1__ctor_m2794258036_gshared (ReturnObservable_1_t1525853922 * __this, Il2CppObject * ___value0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ReturnObservable_1__ctor_m2794258036(__this, ___value0, ___scheduler1, method) ((  void (*) (ReturnObservable_1_t1525853922 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ReturnObservable_1__ctor_m2794258036_gshared)(__this, ___value0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.ReturnObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ReturnObservable_1_SubscribeCore_m1251360082_gshared (ReturnObservable_1_t1525853922 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ReturnObservable_1_SubscribeCore_m1251360082(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ReturnObservable_1_t1525853922 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ReturnObservable_1_SubscribeCore_m1251360082_gshared)(__this, ___observer0, ___cancel1, method)
