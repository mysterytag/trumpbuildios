﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Vector2ReactiveProperty
struct Vector2ReactiveProperty_t3641574511;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2>
struct IEqualityComparer_1_t1554629143;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.Vector2ReactiveProperty::.ctor()
extern "C"  void Vector2ReactiveProperty__ctor_m1684401746 (Vector2ReactiveProperty_t3641574511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Vector2ReactiveProperty::.ctor(UnityEngine.Vector2)
extern "C"  void Vector2ReactiveProperty__ctor_m670379048 (Vector2ReactiveProperty_t3641574511 * __this, Vector2_t3525329788  ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2> UniRx.Vector2ReactiveProperty::get_EqualityComparer()
extern "C"  Il2CppObject* Vector2ReactiveProperty_get_EqualityComparer_m227569759 (Vector2ReactiveProperty_t3641574511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
