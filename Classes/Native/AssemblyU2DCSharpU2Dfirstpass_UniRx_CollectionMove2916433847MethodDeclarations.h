﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.CollectionMoveEvent`1<System.Object>::.ctor(System.Int32,System.Int32,T)
extern "C"  void CollectionMoveEvent_1__ctor_m539258343_gshared (CollectionMoveEvent_1_t2916433847 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, Il2CppObject * ___value2, const MethodInfo* method);
#define CollectionMoveEvent_1__ctor_m539258343(__this, ___oldIndex0, ___newIndex1, ___value2, method) ((  void (*) (CollectionMoveEvent_1_t2916433847 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))CollectionMoveEvent_1__ctor_m539258343_gshared)(__this, ___oldIndex0, ___newIndex1, ___value2, method)
// System.Int32 UniRx.CollectionMoveEvent`1<System.Object>::get_OldIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_OldIndex_m4078874263_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_OldIndex_m4078874263(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))CollectionMoveEvent_1_get_OldIndex_m4078874263_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<System.Object>::set_OldIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_OldIndex_m1382338918_gshared (CollectionMoveEvent_1_t2916433847 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_OldIndex_m1382338918(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t2916433847 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_OldIndex_m1382338918_gshared)(__this, ___value0, method)
// System.Int32 UniRx.CollectionMoveEvent`1<System.Object>::get_NewIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_NewIndex_m2796325118_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_NewIndex_m2796325118(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))CollectionMoveEvent_1_get_NewIndex_m2796325118_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<System.Object>::set_NewIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_NewIndex_m2763213133_gshared (CollectionMoveEvent_1_t2916433847 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_NewIndex_m2763213133(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t2916433847 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_NewIndex_m2763213133_gshared)(__this, ___value0, method)
// T UniRx.CollectionMoveEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionMoveEvent_1_get_Value_m2033004030_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_Value_m2033004030(__this, method) ((  Il2CppObject * (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))CollectionMoveEvent_1_get_Value_m2033004030_gshared)(__this, method)
// System.Void UniRx.CollectionMoveEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionMoveEvent_1_set_Value_m3017074197_gshared (CollectionMoveEvent_1_t2916433847 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_Value_m3017074197(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t2916433847 *, Il2CppObject *, const MethodInfo*))CollectionMoveEvent_1_set_Value_m3017074197_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionMoveEvent`1<System.Object>::ToString()
extern "C"  String_t* CollectionMoveEvent_1_ToString_m3558423484_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_ToString_m3558423484(__this, method) ((  String_t* (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))CollectionMoveEvent_1_ToString_m3558423484_gshared)(__this, method)
// System.Int32 UniRx.CollectionMoveEvent`1<System.Object>::GetHashCode()
extern "C"  int32_t CollectionMoveEvent_1_GetHashCode_m3735208240_gshared (CollectionMoveEvent_1_t2916433847 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_GetHashCode_m3735208240(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t2916433847 *, const MethodInfo*))CollectionMoveEvent_1_GetHashCode_m3735208240_gshared)(__this, method)
// System.Boolean UniRx.CollectionMoveEvent`1<System.Object>::Equals(UniRx.CollectionMoveEvent`1<T>)
extern "C"  bool CollectionMoveEvent_1_Equals_m320381798_gshared (CollectionMoveEvent_1_t2916433847 * __this, CollectionMoveEvent_1_t2916433847  ___other0, const MethodInfo* method);
#define CollectionMoveEvent_1_Equals_m320381798(__this, ___other0, method) ((  bool (*) (CollectionMoveEvent_1_t2916433847 *, CollectionMoveEvent_1_t2916433847 , const MethodInfo*))CollectionMoveEvent_1_Equals_m320381798_gshared)(__this, ___other0, method)
