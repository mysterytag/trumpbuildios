﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionFloor
struct XPathFunctionFloor_t1362563700;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionFloor::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionFloor__ctor_m224085036 (XPathFunctionFloor_t1362563700 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionFloor::get_HasStaticValue()
extern "C"  bool XPathFunctionFloor_get_HasStaticValue_m899265323 (XPathFunctionFloor_t1362563700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XPath.XPathFunctionFloor::get_StaticValueAsNumber()
extern "C"  double XPathFunctionFloor_get_StaticValueAsNumber_m3220284121 (XPathFunctionFloor_t1362563700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionFloor::get_Peer()
extern "C"  bool XPathFunctionFloor_get_Peer_m3353177828 (XPathFunctionFloor_t1362563700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionFloor::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionFloor_Evaluate_m1729380871 (XPathFunctionFloor_t1362563700 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionFloor::ToString()
extern "C"  String_t* XPathFunctionFloor_ToString_m918846570 (XPathFunctionFloor_t1362563700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
