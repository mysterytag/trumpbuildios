﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeLastObservable`1<System.Object>
struct TakeLastObservable_1_t3265267047;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.TakeLastObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void TakeLastObservable_1__ctor_m1732400949_gshared (TakeLastObservable_1_t3265267047 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method);
#define TakeLastObservable_1__ctor_m1732400949(__this, ___source0, ___count1, method) ((  void (*) (TakeLastObservable_1_t3265267047 *, Il2CppObject*, int32_t, const MethodInfo*))TakeLastObservable_1__ctor_m1732400949_gshared)(__this, ___source0, ___count1, method)
// System.Void UniRx.Operators.TakeLastObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern "C"  void TakeLastObservable_1__ctor_m2150835556_gshared (TakeLastObservable_1_t3265267047 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define TakeLastObservable_1__ctor_m2150835556(__this, ___source0, ___duration1, ___scheduler2, method) ((  void (*) (TakeLastObservable_1_t3265267047 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))TakeLastObservable_1__ctor_m2150835556_gshared)(__this, ___source0, ___duration1, ___scheduler2, method)
// System.IDisposable UniRx.Operators.TakeLastObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeLastObservable_1_SubscribeCore_m2269902629_gshared (TakeLastObservable_1_t3265267047 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TakeLastObservable_1_SubscribeCore_m2269902629(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TakeLastObservable_1_t3265267047 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeLastObservable_1_SubscribeCore_m2269902629_gshared)(__this, ___observer0, ___cancel1, method)
