﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.CallRequestContainer::.ctor()
extern "C"  void CallRequestContainer__ctor_m751774308 (CallRequestContainer_t432318609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.CallRequestContainer::InvokeCallback()
extern "C"  void CallRequestContainer_InvokeCallback_m3267545405 (CallRequestContainer_t432318609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
