﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Stream`1<System.Double>
struct Stream_1_t3520622932;

#include "AssemblyU2DCSharp_Cell_1_gen716614246.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoubleCell
struct  DoubleCell_t2771715315  : public Cell_1_t716614246
{
public:
	// Stream`1<System.Double> DoubleCell::diff_
	Stream_1_t3520622932 * ___diff__5;

public:
	inline static int32_t get_offset_of_diff__5() { return static_cast<int32_t>(offsetof(DoubleCell_t2771715315, ___diff__5)); }
	inline Stream_1_t3520622932 * get_diff__5() const { return ___diff__5; }
	inline Stream_1_t3520622932 ** get_address_of_diff__5() { return &___diff__5; }
	inline void set_diff__5(Stream_1_t3520622932 * value)
	{
		___diff__5 = value;
		Il2CppCodeGenWriteBarrier(&___diff__5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
