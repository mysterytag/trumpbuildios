﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>
struct U3CFilterU3Ec__AnonStorey11D_1_t441028593;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1<System.Object>
struct  U3CFilterU3Ec__AnonStorey11E_1_t4290970890  : public Il2CppObject
{
public:
	// T ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1::value
	Il2CppObject * ___value_0;
	// ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<T> ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1/<Filter>c__AnonStorey11E`1::<>f__ref$285
	U3CFilterU3Ec__AnonStorey11D_1_t441028593 * ___U3CU3Ef__refU24285_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey11E_1_t4290970890, ___value_0)); }
	inline Il2CppObject * get_value_0() const { return ___value_0; }
	inline Il2CppObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Il2CppObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier(&___value_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24285_1() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey11E_1_t4290970890, ___U3CU3Ef__refU24285_1)); }
	inline U3CFilterU3Ec__AnonStorey11D_1_t441028593 * get_U3CU3Ef__refU24285_1() const { return ___U3CU3Ef__refU24285_1; }
	inline U3CFilterU3Ec__AnonStorey11D_1_t441028593 ** get_address_of_U3CU3Ef__refU24285_1() { return &___U3CU3Ef__refU24285_1; }
	inline void set_U3CU3Ef__refU24285_1(U3CFilterU3Ec__AnonStorey11D_1_t441028593 * value)
	{
		___U3CU3Ef__refU24285_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24285_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
