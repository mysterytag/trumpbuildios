﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.StatisticUpdate
struct StatisticUpdate_t3411289641;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"

// System.Void PlayFab.ClientModels.StatisticUpdate::.ctor()
extern "C"  void StatisticUpdate__ctor_m3587796896 (StatisticUpdate_t3411289641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StatisticUpdate::get_StatisticName()
extern "C"  String_t* StatisticUpdate_get_StatisticName_m373974733 (StatisticUpdate_t3411289641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StatisticUpdate::set_StatisticName(System.String)
extern "C"  void StatisticUpdate_set_StatisticName_m37662694 (StatisticUpdate_t3411289641 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt32> PlayFab.ClientModels.StatisticUpdate::get_Version()
extern "C"  Nullable_1_t3871963234  StatisticUpdate_get_Version_m1558386529 (StatisticUpdate_t3411289641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StatisticUpdate::set_Version(System.Nullable`1<System.UInt32>)
extern "C"  void StatisticUpdate_set_Version_m2574958024 (StatisticUpdate_t3411289641 * __this, Nullable_1_t3871963234  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.StatisticUpdate::get_Value()
extern "C"  int32_t StatisticUpdate_get_Value_m3849669432 (StatisticUpdate_t3411289641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StatisticUpdate::set_Value(System.Int32)
extern "C"  void StatisticUpdate_set_Value_m692904675 (StatisticUpdate_t3411289641 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
