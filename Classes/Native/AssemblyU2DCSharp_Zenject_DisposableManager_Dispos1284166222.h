﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager/DisposableInfo
struct  DisposableInfo_t1284166222  : public Il2CppObject
{
public:
	// System.IDisposable Zenject.DisposableManager/DisposableInfo::Disposable
	Il2CppObject * ___Disposable_0;
	// System.Int32 Zenject.DisposableManager/DisposableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_Disposable_0() { return static_cast<int32_t>(offsetof(DisposableInfo_t1284166222, ___Disposable_0)); }
	inline Il2CppObject * get_Disposable_0() const { return ___Disposable_0; }
	inline Il2CppObject ** get_address_of_Disposable_0() { return &___Disposable_0; }
	inline void set_Disposable_0(Il2CppObject * value)
	{
		___Disposable_0 = value;
		Il2CppCodeGenWriteBarrier(&___Disposable_0, value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(DisposableInfo_t1284166222, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
