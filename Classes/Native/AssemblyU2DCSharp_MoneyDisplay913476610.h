﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t3286458198;
// GameController
struct GameController_t2782302542;
// GlobalStat
struct GlobalStat_t1134678199;
// OligarchParameters
struct OligarchParameters_t821144443;
// UniRx.ReactiveProperty`1<System.Double>
struct ReactiveProperty_1_t1142849652;
// System.Func`3<System.Double,System.Double,System.Double>
struct Func_3_t1933728903;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoneyDisplay
struct  MoneyDisplay_t913476610  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.Text MoneyDisplay::MoneyIncome
	Text_t3286458198 * ___MoneyIncome_2;
	// GameController MoneyDisplay::GameController
	GameController_t2782302542 * ___GameController_3;
	// GlobalStat MoneyDisplay::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_4;
	// OligarchParameters MoneyDisplay::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_5;
	// System.Double MoneyDisplay::aaaaasssss
	double ___aaaaasssss_6;
	// UniRx.ReactiveProperty`1<System.Double> MoneyDisplay::PasInc
	ReactiveProperty_1_t1142849652 * ___PasInc_7;

public:
	inline static int32_t get_offset_of_MoneyIncome_2() { return static_cast<int32_t>(offsetof(MoneyDisplay_t913476610, ___MoneyIncome_2)); }
	inline Text_t3286458198 * get_MoneyIncome_2() const { return ___MoneyIncome_2; }
	inline Text_t3286458198 ** get_address_of_MoneyIncome_2() { return &___MoneyIncome_2; }
	inline void set_MoneyIncome_2(Text_t3286458198 * value)
	{
		___MoneyIncome_2 = value;
		Il2CppCodeGenWriteBarrier(&___MoneyIncome_2, value);
	}

	inline static int32_t get_offset_of_GameController_3() { return static_cast<int32_t>(offsetof(MoneyDisplay_t913476610, ___GameController_3)); }
	inline GameController_t2782302542 * get_GameController_3() const { return ___GameController_3; }
	inline GameController_t2782302542 ** get_address_of_GameController_3() { return &___GameController_3; }
	inline void set_GameController_3(GameController_t2782302542 * value)
	{
		___GameController_3 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_3, value);
	}

	inline static int32_t get_offset_of_GlobalStat_4() { return static_cast<int32_t>(offsetof(MoneyDisplay_t913476610, ___GlobalStat_4)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_4() const { return ___GlobalStat_4; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_4() { return &___GlobalStat_4; }
	inline void set_GlobalStat_4(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_4 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_4, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_5() { return static_cast<int32_t>(offsetof(MoneyDisplay_t913476610, ___OligarchParameters_5)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_5() const { return ___OligarchParameters_5; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_5() { return &___OligarchParameters_5; }
	inline void set_OligarchParameters_5(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_5 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_5, value);
	}

	inline static int32_t get_offset_of_aaaaasssss_6() { return static_cast<int32_t>(offsetof(MoneyDisplay_t913476610, ___aaaaasssss_6)); }
	inline double get_aaaaasssss_6() const { return ___aaaaasssss_6; }
	inline double* get_address_of_aaaaasssss_6() { return &___aaaaasssss_6; }
	inline void set_aaaaasssss_6(double value)
	{
		___aaaaasssss_6 = value;
	}

	inline static int32_t get_offset_of_PasInc_7() { return static_cast<int32_t>(offsetof(MoneyDisplay_t913476610, ___PasInc_7)); }
	inline ReactiveProperty_1_t1142849652 * get_PasInc_7() const { return ___PasInc_7; }
	inline ReactiveProperty_1_t1142849652 ** get_address_of_PasInc_7() { return &___PasInc_7; }
	inline void set_PasInc_7(ReactiveProperty_1_t1142849652 * value)
	{
		___PasInc_7 = value;
		Il2CppCodeGenWriteBarrier(&___PasInc_7, value);
	}
};

struct MoneyDisplay_t913476610_StaticFields
{
public:
	// System.Func`3<System.Double,System.Double,System.Double> MoneyDisplay::<>f__am$cache6
	Func_3_t1933728903 * ___U3CU3Ef__amU24cache6_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(MoneyDisplay_t913476610_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Func_3_t1933728903 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Func_3_t1933728903 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Func_3_t1933728903 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
