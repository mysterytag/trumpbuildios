﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162
struct U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey162__ctor_m2159556445 (U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162::<>m__263(System.Boolean)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey162_U3CU3Em__263_m3905489058 (U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162::<>m__264(UniRx.Unit)
extern "C"  bool U3CRegisterDonateButtonsU3Ec__AnonStorey162_U3CU3Em__264_m3110778436 (U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162::<>m__265(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey162_U3CU3Em__265_m329909009 (U3CRegisterDonateButtonsU3Ec__AnonStorey162_t2690654993 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
