﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA8
struct U3COnValueChangedAsObservableU3Ec__AnonStoreyA8_t3117525747;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA8::.ctor()
extern "C"  void U3COnValueChangedAsObservableU3Ec__AnonStoreyA8__ctor_m2185318699 (U3COnValueChangedAsObservableU3Ec__AnonStoreyA8_t3117525747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA8::<>m__E6(UniRx.IObserver`1<System.Single>)
extern "C"  Il2CppObject * U3COnValueChangedAsObservableU3Ec__AnonStoreyA8_U3CU3Em__E6_m2944810607 (U3COnValueChangedAsObservableU3Ec__AnonStoreyA8_t3117525747 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
