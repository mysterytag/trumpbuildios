﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1369764433.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<System.Byte>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3047404298_gshared (Nullable_1_t1369764433 * __this, uint8_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3047404298(__this, ___value0, method) ((  void (*) (Nullable_1_t1369764433 *, uint8_t, const MethodInfo*))Nullable_1__ctor_m3047404298_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Byte>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m310053951_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m310053951(__this, method) ((  bool (*) (Nullable_1_t1369764433 *, const MethodInfo*))Nullable_1_get_HasValue_m310053951_gshared)(__this, method)
// T System.Nullable`1<System.Byte>::get_Value()
extern "C"  uint8_t Nullable_1_get_Value_m1771599223_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1771599223(__this, method) ((  uint8_t (*) (Nullable_1_t1369764433 *, const MethodInfo*))Nullable_1_get_Value_m1771599223_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2428685791_gshared (Nullable_1_t1369764433 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2428685791(__this, ___other0, method) ((  bool (*) (Nullable_1_t1369764433 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2428685791_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2459188352_gshared (Nullable_1_t1369764433 * __this, Nullable_1_t1369764433  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2459188352(__this, ___other0, method) ((  bool (*) (Nullable_1_t1369764433 *, Nullable_1_t1369764433 , const MethodInfo*))Nullable_1_Equals_m2459188352_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.Byte>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1455590339_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1455590339(__this, method) ((  int32_t (*) (Nullable_1_t1369764433 *, const MethodInfo*))Nullable_1_GetHashCode_m1455590339_gshared)(__this, method)
// T System.Nullable`1<System.Byte>::GetValueOrDefault()
extern "C"  uint8_t Nullable_1_GetValueOrDefault_m2207863084_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2207863084(__this, method) ((  uint8_t (*) (Nullable_1_t1369764433 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2207863084_gshared)(__this, method)
// System.String System.Nullable`1<System.Byte>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1479328675_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m1479328675(__this, method) ((  String_t* (*) (Nullable_1_t1369764433 *, const MethodInfo*))Nullable_1_ToString_m1479328675_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<System.Byte>::op_Implicit(T)
extern "C"  Nullable_1_t1369764433  Nullable_1_op_Implicit_m178797308_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m178797308(__this /* static, unused */, ___value0, method) ((  Nullable_1_t1369764433  (*) (Il2CppObject * /* static, unused */, uint8_t, const MethodInfo*))Nullable_1_op_Implicit_m178797308_gshared)(__this /* static, unused */, ___value0, method)
