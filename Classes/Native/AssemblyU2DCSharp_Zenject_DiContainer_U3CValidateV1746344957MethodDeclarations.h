﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<ValidateValidatables>c__Iterator4B
struct U3CValidateValidatablesU3Ec__Iterator4B_t1746344957;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;
// System.Type
struct Type_t;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::.ctor()
extern "C"  void U3CValidateValidatablesU3Ec__Iterator4B__ctor_m3365844736 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ZenjectResolveException Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateValidatablesU3Ec__Iterator4B_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m3896911753 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateValidatablesU3Ec__Iterator4B_System_Collections_IEnumerator_get_Current_m892842726 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateValidatablesU3Ec__Iterator4B_System_Collections_IEnumerable_GetEnumerator_m4075506785 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateValidatablesU3Ec__Iterator4B_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m244781418 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::MoveNext()
extern "C"  bool U3CValidateValidatablesU3Ec__Iterator4B_MoveNext_m3144114932 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::Dispose()
extern "C"  void U3CValidateValidatablesU3Ec__Iterator4B_Dispose_m364700477 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::Reset()
extern "C"  void U3CValidateValidatablesU3Ec__Iterator4B_Reset_m1012277677 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<>m__2CE(System.Type)
extern "C"  bool U3CValidateValidatablesU3Ec__Iterator4B_U3CU3Em__2CE_m4292112576 (U3CValidateValidatablesU3Ec__Iterator4B_t1746344957 * __this, Type_t * ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<>m__2CF(Zenject.ProviderBase)
extern "C"  bool U3CValidateValidatablesU3Ec__Iterator4B_U3CU3Em__2CF_m720747097 (Il2CppObject * __this /* static, unused */, ProviderBase_t1627494391 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<ValidateValidatables>c__Iterator4B::<>m__2D0(Zenject.ProviderBase)
extern "C"  bool U3CValidateValidatablesU3Ec__Iterator4B_U3CU3Em__2D0_m3333692834 (Il2CppObject * __this /* static, unused */, ProviderBase_t1627494391 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
