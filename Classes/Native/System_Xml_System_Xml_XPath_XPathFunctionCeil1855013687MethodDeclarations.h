﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionCeil
struct XPathFunctionCeil_t1855013687;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionCeil::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionCeil__ctor_m3298422843 (XPathFunctionCeil_t1855013687 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionCeil::get_HasStaticValue()
extern "C"  bool XPathFunctionCeil_get_HasStaticValue_m2317770444 (XPathFunctionCeil_t1855013687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XPath.XPathFunctionCeil::get_StaticValueAsNumber()
extern "C"  double XPathFunctionCeil_get_StaticValueAsNumber_m3864092994 (XPathFunctionCeil_t1855013687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionCeil::get_Peer()
extern "C"  bool XPathFunctionCeil_get_Peer_m2107441093 (XPathFunctionCeil_t1855013687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionCeil::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionCeil_Evaluate_m2197801932 (XPathFunctionCeil_t1855013687 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionCeil::ToString()
extern "C"  String_t* XPathFunctionCeil_ToString_m521177249 (XPathFunctionCeil_t1855013687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
