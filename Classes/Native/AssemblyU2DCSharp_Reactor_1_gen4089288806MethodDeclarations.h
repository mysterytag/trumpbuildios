﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<System.Double>
struct Reactor_1_t4089288806;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Double>
struct Action_1_t682969319;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<System.Double>::.ctor()
extern "C"  void Reactor_1__ctor_m2566402724_gshared (Reactor_1_t4089288806 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m2566402724(__this, method) ((  void (*) (Reactor_1_t4089288806 *, const MethodInfo*))Reactor_1__ctor_m2566402724_gshared)(__this, method)
// System.Void Reactor`1<System.Double>::React(T)
extern "C"  void Reactor_1_React_m1785767933_gshared (Reactor_1_t4089288806 * __this, double ___t0, const MethodInfo* method);
#define Reactor_1_React_m1785767933(__this, ___t0, method) ((  void (*) (Reactor_1_t4089288806 *, double, const MethodInfo*))Reactor_1_React_m1785767933_gshared)(__this, ___t0, method)
// System.Void Reactor`1<System.Double>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m2535932232_gshared (Reactor_1_t4089288806 * __this, double ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m2535932232(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t4089288806 *, double, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m2535932232_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<System.Double>::Empty()
extern "C"  bool Reactor_1_Empty_m1536931227_gshared (Reactor_1_t4089288806 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m1536931227(__this, method) ((  bool (*) (Reactor_1_t4089288806 *, const MethodInfo*))Reactor_1_Empty_m1536931227_gshared)(__this, method)
// System.IDisposable Reactor`1<System.Double>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m825091625_gshared (Reactor_1_t4089288806 * __this, Action_1_t682969319 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m825091625(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t4089288806 *, Action_1_t682969319 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m825091625_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<System.Double>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m4095892598_gshared (Reactor_1_t4089288806 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m4095892598(__this, method) ((  void (*) (Reactor_1_t4089288806 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m4095892598_gshared)(__this, method)
// System.Void Reactor`1<System.Double>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m4101117441_gshared (Reactor_1_t4089288806 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m4101117441(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t4089288806 *, Action_1_t682969319 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m4101117441_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Double>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m4217132917_gshared (Reactor_1_t4089288806 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m4217132917(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t4089288806 *, Action_1_t682969319 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4217132917_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Double>::Clear()
extern "C"  void Reactor_1_Clear_m4267503311_gshared (Reactor_1_t4089288806 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m4267503311(__this, method) ((  void (*) (Reactor_1_t4089288806 *, const MethodInfo*))Reactor_1_Clear_m4267503311_gshared)(__this, method)
