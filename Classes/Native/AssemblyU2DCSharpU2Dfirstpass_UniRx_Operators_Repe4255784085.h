﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RepeatObservable`1<System.Object>
struct  RepeatObservable_1_t4255784085  : public OperatorObservableBase_1_t4196218687
{
public:
	// T UniRx.Operators.RepeatObservable`1::value
	Il2CppObject * ___value_1;
	// System.Nullable`1<System.Int32> UniRx.Operators.RepeatObservable`1::repeatCount
	Nullable_1_t1438485399  ___repeatCount_2;
	// UniRx.IScheduler UniRx.Operators.RepeatObservable`1::scheduler
	Il2CppObject * ___scheduler_3;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(RepeatObservable_1_t4255784085, ___value_1)); }
	inline Il2CppObject * get_value_1() const { return ___value_1; }
	inline Il2CppObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Il2CppObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}

	inline static int32_t get_offset_of_repeatCount_2() { return static_cast<int32_t>(offsetof(RepeatObservable_1_t4255784085, ___repeatCount_2)); }
	inline Nullable_1_t1438485399  get_repeatCount_2() const { return ___repeatCount_2; }
	inline Nullable_1_t1438485399 * get_address_of_repeatCount_2() { return &___repeatCount_2; }
	inline void set_repeatCount_2(Nullable_1_t1438485399  value)
	{
		___repeatCount_2 = value;
	}

	inline static int32_t get_offset_of_scheduler_3() { return static_cast<int32_t>(offsetof(RepeatObservable_1_t4255784085, ___scheduler_3)); }
	inline Il2CppObject * get_scheduler_3() const { return ___scheduler_3; }
	inline Il2CppObject ** get_address_of_scheduler_3() { return &___scheduler_3; }
	inline void set_scheduler_3(Il2CppObject * value)
	{
		___scheduler_3 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
