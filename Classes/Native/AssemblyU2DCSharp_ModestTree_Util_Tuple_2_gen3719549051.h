﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct  Tuple_2_t3719549051  : public Il2CppObject
{
public:
	// T1 ModestTree.Util.Tuple`2::First
	Type_t * ___First_0;
	// T2 ModestTree.Util.Tuple`2::Second
	int32_t ___Second_1;

public:
	inline static int32_t get_offset_of_First_0() { return static_cast<int32_t>(offsetof(Tuple_2_t3719549051, ___First_0)); }
	inline Type_t * get_First_0() const { return ___First_0; }
	inline Type_t ** get_address_of_First_0() { return &___First_0; }
	inline void set_First_0(Type_t * value)
	{
		___First_0 = value;
		Il2CppCodeGenWriteBarrier(&___First_0, value);
	}

	inline static int32_t get_offset_of_Second_1() { return static_cast<int32_t>(offsetof(Tuple_2_t3719549051, ___Second_1)); }
	inline int32_t get_Second_1() const { return ___Second_1; }
	inline int32_t* get_address_of_Second_1() { return &___Second_1; }
	inline void set_Second_1(int32_t value)
	{
		___Second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
