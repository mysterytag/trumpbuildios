﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.Util.Tuple`2<System.Object,System.Int32>
struct Tuple_2_t299352770;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ModestTree.Util.Tuple`2<System.Object,System.Int32>::.ctor()
extern "C"  void Tuple_2__ctor_m3946485580_gshared (Tuple_2_t299352770 * __this, const MethodInfo* method);
#define Tuple_2__ctor_m3946485580(__this, method) ((  void (*) (Tuple_2_t299352770 *, const MethodInfo*))Tuple_2__ctor_m3946485580_gshared)(__this, method)
// System.Void ModestTree.Util.Tuple`2<System.Object,System.Int32>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m2191801465_gshared (Tuple_2_t299352770 * __this, Il2CppObject * ___first0, int32_t ___second1, const MethodInfo* method);
#define Tuple_2__ctor_m2191801465(__this, ___first0, ___second1, method) ((  void (*) (Tuple_2_t299352770 *, Il2CppObject *, int32_t, const MethodInfo*))Tuple_2__ctor_m2191801465_gshared)(__this, ___first0, ___second1, method)
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Int32>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m897686001_gshared (Tuple_2_t299352770 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_Equals_m897686001(__this, ___obj0, method) ((  bool (*) (Tuple_2_t299352770 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m897686001_gshared)(__this, ___obj0, method)
// System.Boolean ModestTree.Util.Tuple`2<System.Object,System.Int32>::Equals(ModestTree.Util.Tuple`2<T1,T2>)
extern "C"  bool Tuple_2_Equals_m194624144_gshared (Tuple_2_t299352770 * __this, Tuple_2_t299352770 * ___that0, const MethodInfo* method);
#define Tuple_2_Equals_m194624144(__this, ___that0, method) ((  bool (*) (Tuple_2_t299352770 *, Tuple_2_t299352770 *, const MethodInfo*))Tuple_2_Equals_m194624144_gshared)(__this, ___that0, method)
// System.Int32 ModestTree.Util.Tuple`2<System.Object,System.Int32>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m1510095241_gshared (Tuple_2_t299352770 * __this, const MethodInfo* method);
#define Tuple_2_GetHashCode_m1510095241(__this, method) ((  int32_t (*) (Tuple_2_t299352770 *, const MethodInfo*))Tuple_2_GetHashCode_m1510095241_gshared)(__this, method)
