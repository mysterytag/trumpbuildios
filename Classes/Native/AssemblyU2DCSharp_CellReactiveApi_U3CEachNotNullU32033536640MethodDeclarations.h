﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<EachNotNull>c__AnonStorey103`1<System.Object>
struct U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<EachNotNull>c__AnonStorey103`1<System.Object>::.ctor()
extern "C"  void U3CEachNotNullU3Ec__AnonStorey103_1__ctor_m2392627342_gshared (U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 * __this, const MethodInfo* method);
#define U3CEachNotNullU3Ec__AnonStorey103_1__ctor_m2392627342(__this, method) ((  void (*) (U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 *, const MethodInfo*))U3CEachNotNullU3Ec__AnonStorey103_1__ctor_m2392627342_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<EachNotNull>c__AnonStorey103`1<System.Object>::<>m__179(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CEachNotNullU3Ec__AnonStorey103_1_U3CU3Em__179_m811569745_gshared (U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 * __this, Action_1_t985559125 * ___act0, int32_t ___p1, const MethodInfo* method);
#define U3CEachNotNullU3Ec__AnonStorey103_1_U3CU3Em__179_m811569745(__this, ___act0, ___p1, method) ((  Il2CppObject * (*) (U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CEachNotNullU3Ec__AnonStorey103_1_U3CU3Em__179_m811569745_gshared)(__this, ___act0, ___p1, method)
