﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RefreshPSNAuthTokenRequest
struct RefreshPSNAuthTokenRequest_t4115941982;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.RefreshPSNAuthTokenRequest::.ctor()
extern "C"  void RefreshPSNAuthTokenRequest__ctor_m4253016863 (RefreshPSNAuthTokenRequest_t4115941982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RefreshPSNAuthTokenRequest::get_AuthCode()
extern "C"  String_t* RefreshPSNAuthTokenRequest_get_AuthCode_m2769373888 (RefreshPSNAuthTokenRequest_t4115941982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RefreshPSNAuthTokenRequest::set_AuthCode(System.String)
extern "C"  void RefreshPSNAuthTokenRequest_set_AuthCode_m3504508843 (RefreshPSNAuthTokenRequest_t4115941982 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RefreshPSNAuthTokenRequest::get_RedirectUri()
extern "C"  String_t* RefreshPSNAuthTokenRequest_get_RedirectUri_m703926631 (RefreshPSNAuthTokenRequest_t4115941982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RefreshPSNAuthTokenRequest::set_RedirectUri(System.String)
extern "C"  void RefreshPSNAuthTokenRequest_set_RedirectUri_m1131308274 (RefreshPSNAuthTokenRequest_t4115941982 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.RefreshPSNAuthTokenRequest::get_IssuerId()
extern "C"  Nullable_1_t1438485399  RefreshPSNAuthTokenRequest_get_IssuerId_m431805619 (RefreshPSNAuthTokenRequest_t4115941982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RefreshPSNAuthTokenRequest::set_IssuerId(System.Nullable`1<System.Int32>)
extern "C"  void RefreshPSNAuthTokenRequest_set_IssuerId_m1144519832 (RefreshPSNAuthTokenRequest_t4115941982 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
