﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TickableManager/<InitTickables>c__AnonStorey193
struct U3CInitTickablesU3Ec__AnonStorey193_t88162706;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.TickableManager/<InitTickables>c__AnonStorey193::.ctor()
extern "C"  void U3CInitTickablesU3Ec__AnonStorey193__ctor_m2583430673 (U3CInitTickablesU3Ec__AnonStorey193_t88162706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TickableManager/<InitTickables>c__AnonStorey193::<>m__2EC(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  bool U3CInitTickablesU3Ec__AnonStorey193_U3CU3Em__2EC_m234334292 (U3CInitTickablesU3Ec__AnonStorey193_t88162706 * __this, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
