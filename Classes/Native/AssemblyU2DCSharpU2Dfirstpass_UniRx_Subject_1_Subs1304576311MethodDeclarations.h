﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.Vector3>
struct Subscription_t1304576311;
// UniRx.Subject`1<UnityEngine.Vector3>
struct Subject_1_t1168397113;
// UniRx.IObserver`1<UnityEngine.Vector3>
struct IObserver_1_t1442361396;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector3>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m2987189468_gshared (Subscription_t1304576311 * __this, Subject_1_t1168397113 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m2987189468(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t1304576311 *, Subject_1_t1168397113 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m2987189468_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector3>::Dispose()
extern "C"  void Subscription_Dispose_m1942468506_gshared (Subscription_t1304576311 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1942468506(__this, method) ((  void (*) (Subscription_t1304576311 *, const MethodInfo*))Subscription_Dispose_m1942468506_gshared)(__this, method)
