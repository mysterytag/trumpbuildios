﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DeferObservable`1<System.Object>
struct DeferObservable_1_t3089955988;
// System.Func`1<UniRx.IObservable`1<System.Object>>
struct Func_1_t1738686031;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DeferObservable`1<System.Object>::.ctor(System.Func`1<UniRx.IObservable`1<T>>)
extern "C"  void DeferObservable_1__ctor_m1920396347_gshared (DeferObservable_1_t3089955988 * __this, Func_1_t1738686031 * ___observableFactory0, const MethodInfo* method);
#define DeferObservable_1__ctor_m1920396347(__this, ___observableFactory0, method) ((  void (*) (DeferObservable_1_t3089955988 *, Func_1_t1738686031 *, const MethodInfo*))DeferObservable_1__ctor_m1920396347_gshared)(__this, ___observableFactory0, method)
// System.IDisposable UniRx.Operators.DeferObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DeferObservable_1_SubscribeCore_m1896247388_gshared (DeferObservable_1_t3089955988 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DeferObservable_1_SubscribeCore_m1896247388(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DeferObservable_1_t3089955988 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DeferObservable_1_SubscribeCore_m1896247388_gshared)(__this, ___observer0, ___cancel1, method)
