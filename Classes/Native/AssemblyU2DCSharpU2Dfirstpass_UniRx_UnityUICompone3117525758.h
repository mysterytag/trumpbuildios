﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t2345609593;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAC
struct  U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_t3117525758  : public Il2CppObject
{
public:
	// UnityEngine.UI.InputField UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAC::inputField
	InputField_t2345609593 * ___inputField_0;

public:
	inline static int32_t get_offset_of_inputField_0() { return static_cast<int32_t>(offsetof(U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_t3117525758, ___inputField_0)); }
	inline InputField_t2345609593 * get_inputField_0() const { return ___inputField_0; }
	inline InputField_t2345609593 ** get_address_of_inputField_0() { return &___inputField_0; }
	inline void set_inputField_0(InputField_t2345609593 * value)
	{
		___inputField_0 = value;
		Il2CppCodeGenWriteBarrier(&___inputField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
