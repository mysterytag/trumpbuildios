﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First<UniRx.Unit>
struct First_t641189227;
// UniRx.Operators.FirstObservable`1<UniRx.Unit>
struct FirstObservable_1_t285873156;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Unit>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m2298124302_gshared (First_t641189227 * __this, FirstObservable_1_t285873156 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First__ctor_m2298124302(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First_t641189227 *, FirstObservable_1_t285873156 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First__ctor_m2298124302_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Unit>::OnNext(T)
extern "C"  void First_OnNext_m3212117675_gshared (First_t641189227 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define First_OnNext_m3212117675(__this, ___value0, method) ((  void (*) (First_t641189227 *, Unit_t2558286038 , const MethodInfo*))First_OnNext_m3212117675_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Unit>::OnError(System.Exception)
extern "C"  void First_OnError_m4251216826_gshared (First_t641189227 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First_OnError_m4251216826(__this, ___error0, method) ((  void (*) (First_t641189227 *, Exception_t1967233988 *, const MethodInfo*))First_OnError_m4251216826_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<UniRx.Unit>::OnCompleted()
extern "C"  void First_OnCompleted_m2952488461_gshared (First_t641189227 * __this, const MethodInfo* method);
#define First_OnCompleted_m2952488461(__this, method) ((  void (*) (First_t641189227 *, const MethodInfo*))First_OnCompleted_m2952488461_gshared)(__this, method)
