﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenIABEventManager
struct OpenIABEventManager_t528054099;
// System.Action
struct Action_t437523947;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Action`1<OnePF.Inventory>
struct Action_1_t2779015537;
// System.Action`1<OnePF.Purchase>
struct Action_1_t308648278;
// System.Action`2<System.Int32,System.String>
struct Action_2_t1740334453;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_String968488902.h"

// System.Void OpenIABEventManager::.ctor()
extern "C"  void OpenIABEventManager__ctor_m3426801220 (OpenIABEventManager_t528054099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_billingSupportedEvent(System.Action)
extern "C"  void OpenIABEventManager_add_billingSupportedEvent_m3299103698 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_billingSupportedEvent(System.Action)
extern "C"  void OpenIABEventManager_remove_billingSupportedEvent_m3486732849 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_billingNotSupportedEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_add_billingNotSupportedEvent_m1533046238 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_billingNotSupportedEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_remove_billingNotSupportedEvent_m1944553149 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_queryInventorySucceededEvent(System.Action`1<OnePF.Inventory>)
extern "C"  void OpenIABEventManager_add_queryInventorySucceededEvent_m3971075585 (Il2CppObject * __this /* static, unused */, Action_1_t2779015537 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_queryInventorySucceededEvent(System.Action`1<OnePF.Inventory>)
extern "C"  void OpenIABEventManager_remove_queryInventorySucceededEvent_m3994321824 (Il2CppObject * __this /* static, unused */, Action_1_t2779015537 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_queryInventoryFailedEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_add_queryInventoryFailedEvent_m568864969 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_queryInventoryFailedEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_remove_queryInventoryFailedEvent_m440677322 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_purchaseSucceededEvent(System.Action`1<OnePF.Purchase>)
extern "C"  void OpenIABEventManager_add_purchaseSucceededEvent_m3366236229 (Il2CppObject * __this /* static, unused */, Action_1_t308648278 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_purchaseSucceededEvent(System.Action`1<OnePF.Purchase>)
extern "C"  void OpenIABEventManager_remove_purchaseSucceededEvent_m3933699974 (Il2CppObject * __this /* static, unused */, Action_1_t308648278 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_purchaseFailedEvent(System.Action`2<System.Int32,System.String>)
extern "C"  void OpenIABEventManager_add_purchaseFailedEvent_m1072578300 (Il2CppObject * __this /* static, unused */, Action_2_t1740334453 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_purchaseFailedEvent(System.Action`2<System.Int32,System.String>)
extern "C"  void OpenIABEventManager_remove_purchaseFailedEvent_m1937377499 (Il2CppObject * __this /* static, unused */, Action_2_t1740334453 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_consumePurchaseSucceededEvent(System.Action`1<OnePF.Purchase>)
extern "C"  void OpenIABEventManager_add_consumePurchaseSucceededEvent_m890140291 (Il2CppObject * __this /* static, unused */, Action_1_t308648278 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_consumePurchaseSucceededEvent(System.Action`1<OnePF.Purchase>)
extern "C"  void OpenIABEventManager_remove_consumePurchaseSucceededEvent_m913386530 (Il2CppObject * __this /* static, unused */, Action_1_t308648278 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_consumePurchaseFailedEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_add_consumePurchaseFailedEvent_m1880892058 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_consumePurchaseFailedEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_remove_consumePurchaseFailedEvent_m2202042297 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_transactionRestoredEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_add_transactionRestoredEvent_m1251226944 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_transactionRestoredEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_remove_transactionRestoredEvent_m1662733855 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_restoreFailedEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_add_restoreFailedEvent_m3227380105 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_restoreFailedEvent(System.Action`1<System.String>)
extern "C"  void OpenIABEventManager_remove_restoreFailedEvent_m2934232488 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::add_restoreSucceededEvent(System.Action)
extern "C"  void OpenIABEventManager_add_restoreSucceededEvent_m4043601618 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::remove_restoreSucceededEvent(System.Action)
extern "C"  void OpenIABEventManager_remove_restoreSucceededEvent_m4231230769 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::Awake()
extern "C"  void OpenIABEventManager_Awake_m3664406439 (OpenIABEventManager_t528054099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnBillingSupported(System.String)
extern "C"  void OpenIABEventManager_OnBillingSupported_m1355956240 (OpenIABEventManager_t528054099 * __this, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnBillingNotSupported(System.String)
extern "C"  void OpenIABEventManager_OnBillingNotSupported_m173291305 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnQueryInventorySucceeded(System.String)
extern "C"  void OpenIABEventManager_OnQueryInventorySucceeded_m2647396658 (OpenIABEventManager_t528054099 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnQueryInventoryFailed(System.String)
extern "C"  void OpenIABEventManager_OnQueryInventoryFailed_m177395954 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnPurchaseSucceeded(System.String)
extern "C"  void OpenIABEventManager_OnPurchaseSucceeded_m721466431 (OpenIABEventManager_t528054099 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnPurchaseFailed(System.String)
extern "C"  void OpenIABEventManager_OnPurchaseFailed_m1827068165 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnConsumePurchaseSucceeded(System.String)
extern "C"  void OpenIABEventManager_OnConsumePurchaseSucceeded_m1419424927 (OpenIABEventManager_t528054099 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnConsumePurchaseFailed(System.String)
extern "C"  void OpenIABEventManager_OnConsumePurchaseFailed_m497267877 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnPurchaseRestored(System.String)
extern "C"  void OpenIABEventManager_OnPurchaseRestored_m1685775436 (OpenIABEventManager_t528054099 * __this, String_t* ___sku0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnRestoreFailed(System.String)
extern "C"  void OpenIABEventManager_OnRestoreFailed_m244541908 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenIABEventManager::OnRestoreFinished(System.String)
extern "C"  void OpenIABEventManager_OnRestoreFinished_m1125116159 (OpenIABEventManager_t528054099 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
