﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UniRx.Diagnostics.LogEntry>
struct Action_1_t2039608427;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.Logger
struct  Logger_t2118475020  : public Il2CppObject
{
public:
	// System.Action`1<UniRx.Diagnostics.LogEntry> UniRx.Diagnostics.Logger::logPublisher
	Action_1_t2039608427 * ___logPublisher_2;
	// System.String UniRx.Diagnostics.Logger::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_logPublisher_2() { return static_cast<int32_t>(offsetof(Logger_t2118475020, ___logPublisher_2)); }
	inline Action_1_t2039608427 * get_logPublisher_2() const { return ___logPublisher_2; }
	inline Action_1_t2039608427 ** get_address_of_logPublisher_2() { return &___logPublisher_2; }
	inline void set_logPublisher_2(Action_1_t2039608427 * value)
	{
		___logPublisher_2 = value;
		Il2CppCodeGenWriteBarrier(&___logPublisher_2, value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Logger_t2118475020, ___U3CNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CNameU3Ek__BackingField_3() const { return ___U3CNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_3() { return &___U3CNameU3Ek__BackingField_3; }
	inline void set_U3CNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_3, value);
	}
};

struct Logger_t2118475020_StaticFields
{
public:
	// System.Boolean UniRx.Diagnostics.Logger::isInitialized
	bool ___isInitialized_0;
	// System.Boolean UniRx.Diagnostics.Logger::isDebugBuild
	bool ___isDebugBuild_1;

public:
	inline static int32_t get_offset_of_isInitialized_0() { return static_cast<int32_t>(offsetof(Logger_t2118475020_StaticFields, ___isInitialized_0)); }
	inline bool get_isInitialized_0() const { return ___isInitialized_0; }
	inline bool* get_address_of_isInitialized_0() { return &___isInitialized_0; }
	inline void set_isInitialized_0(bool value)
	{
		___isInitialized_0 = value;
	}

	inline static int32_t get_offset_of_isDebugBuild_1() { return static_cast<int32_t>(offsetof(Logger_t2118475020_StaticFields, ___isDebugBuild_1)); }
	inline bool get_isDebugBuild_1() const { return ___isDebugBuild_1; }
	inline bool* get_address_of_isDebugBuild_1() { return &___isDebugBuild_1; }
	inline void set_isDebugBuild_1(bool value)
	{
		___isDebugBuild_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
