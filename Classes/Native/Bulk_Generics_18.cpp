﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;
// System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>
struct U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806;
// System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>
struct U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165;
// System.Linq.Grouping`2<System.Object,System.Object>
struct Grouping_2_t1565096229;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Linq.OrderedEnumerable`1<System.Object>
struct OrderedEnumerable_1_t1902017548;
// System.Linq.OrderedSequence`2<System.Object,System.Int32>
struct OrderedSequence_2_t1754067061;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t4146091719;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t1252154900;
// System.Linq.SortContext`1<System.Object>
struct SortContext_1_t70478024;
// System.Linq.OrderedSequence`2<System.Object,System.Object>
struct OrderedSequence_2_t4038725990;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t3536813829;
// System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>
struct U3CSortU3Ec__Iterator21_t1279425957;
// System.Linq.QuickSort`1<System.Object>
struct QuickSort_1_t2202595634;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Linq.SortSequenceContext`2<System.Object,System.Int32>
struct SortSequenceContext_2_t4213007067;
// System.Linq.SortSequenceContext`2<System.Object,System.Object>
struct SortSequenceContext_2_t2202698700;
// System.String
struct String_t;
// System.Predicate`1<GameAnalyticsSDK.Settings/HelpTypes>
struct Predicate_1_t3862319442;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Predicate`1<LitJson.PropertyMetadata>
struct Predicate_1_t218470691;
// System.Predicate`1<PlayFab.Internal.GMFB_327/testRegion>
struct Predicate_1_t2255903200;
// System.Predicate`1<System.Boolean>
struct Predicate_1_t781969239;
// System.Predicate`1<System.Byte>
struct Predicate_1_t3349657719;
// System.Predicate`1<System.Char>
struct Predicate_1_t3349670597;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t3883920346;
// System.Predicate`1<System.DateTime>
struct Predicate_1_t909997834;
// System.Predicate`1<System.Double>
struct Predicate_1_t1105480512;
// System.Predicate`1<System.Int32>
struct Predicate_1_t3418378685;
// System.Predicate`1<System.Int64>
struct Predicate_1_t3418378780;
// System.Predicate`1<System.Object>
struct Predicate_1_t1408070318;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t889699027;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t1131379460;
// System.Predicate`1<System.Single>
struct Predicate_1_t1529172919;
// System.Predicate`1<System.UInt64>
struct Predicate_1_t1556889319;
// System.Predicate`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Predicate_1_t2987485885;
// System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Predicate_1_t940225717;
// System.Predicate`1<UniRx.Unit>
struct Predicate_1_t3129249936;
// System.Predicate`1<UnityEngine.Color>
struct Predicate_1_t2159139658;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t413080809;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t1530862587;
// System.Predicate`1<UnityEngine.Quaternion>
struct Predicate_1_t2462679877;
// System.Predicate`1<UnityEngine.Rect>
struct Predicate_1_t2096392715;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t974784479;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t727885181;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t2831025503;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t4096293686;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t4096293687;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t4096293688;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t539570894;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t171826611;
// TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>
struct U3CFillU3Ec__AnonStoreyEB_t2250867686;
// TableBlock`2<System.Object,System.Object>
struct TableBlock_2_t3756399434;
// NewTableView
struct NewTableView_t2775249395;
// InstantiationPool`1<System.Object>
struct InstantiationPool_1_t1380750307;
// ZergRush.IObservableCollection`1<System.Object>
struct IObservableCollection_1_t200574308;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4105459918;
// System.Func`2<System.Object,System.Single>
struct Func_2_t2256885953;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// CollectionRemoveEvent`1<System.Object>
struct CollectionRemoveEvent_1_t898259258;
// DG.Tweening.Tweener
struct Tweener_t1766303790;
// DG.Tweening.TweenCallback
struct TweenCallback_t3786476454;
// CollectionReplaceEvent`1<System.Object>
struct CollectionReplaceEvent_1_t2497372070;
// Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>
struct U3CCalculateU3Ec__AnonStorey14A_1_t2571930805;
// System.IDisposable
struct IDisposable_t1628921374;
// ICell
struct ICell_t69513547;
// Transaction/<Calculate>c__AnonStorey149`1<System.Object>
struct U3CCalculateU3Ec__AnonStorey149_1_t1837163501;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Collections.Generic.IEnumerable`1<System.IDisposable>
struct IEnumerable_1_t206108434;
// System.Collections.Generic.IEnumerable`1<ICell>
struct IEnumerable_1_t2941667903;
// System.Func`2<ICell,System.IDisposable>
struct Func_2_t3366118735;
// UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>
struct U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259;
// UniRx.AsyncOperationExtensions/<AsAsyncOperationObservable>c__AnonStorey7C`1<System.Object>
struct U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_t469001995;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;
// UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>
struct U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625;
// UniRx.AsyncSubject`1/Subscription<System.Object>
struct Subscription_t2911320238;
// UniRx.AsyncSubject`1<System.Object>
struct AsyncSubject_1_t1536745524;
// UniRx.AsyncSubject`1/Subscription<UniRx.Unit>
struct Subscription_t337532560;
// UniRx.AsyncSubject`1<UniRx.Unit>
struct AsyncSubject_1_t3257925142;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.Exception
struct Exception_t1967233988;
// UniRx.BehaviorSubject`1/Subscription<System.Object>
struct Subscription_t2911320240;
// UniRx.BehaviorSubject`1<System.Object>
struct BehaviorSubject_1_t2662307150;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSkipIt2456608835.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSkipIt2456608835MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSkipIt4275726103.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSkipIt4275726103MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateTakeIt1105598806.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateTakeIt1105598806MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateTakeIt2924716074.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateTakeIt2924716074MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI1959607897.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI1959607897MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1844407557.h"
#include "System_Core_System_Func_2_gen1844407557MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI1494066236.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI1494066236MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3308141622.h"
#include "System_Core_System_Func_2_gen3308141622MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI3778725165.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI3778725165MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_PredicateOf_1_g2617073743.h"
#include "System_Core_System_Linq_Enumerable_PredicateOf_1_g2617073743MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Core_System_Linq_Enumerable_PredicateOf_1_ge606765376.h"
#include "System_Core_System_Linq_Enumerable_PredicateOf_1_ge606765376MethodDeclarations.h"
#include "System_Core_System_Linq_Grouping_2_gen1565096229.h"
#include "System_Core_System_Linq_Grouping_2_gen1565096229MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedEnumerable_1_gen1902017548.h"
#include "System_Core_System_Linq_OrderedEnumerable_1_gen1902017548MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen1754067061.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen1754067061MethodDeclarations.h"
#include "System_Core_System_Linq_SortDirection2805156518.h"
#include "System_Core_System_Func_2_gen4146091719.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3302075123MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3302075123.h"
#include "System_Core_System_Linq_SortContext_1_gen70478024.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen4213007067.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen4213007067MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_gen2202595634MethodDeclarations.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen4038725990.h"
#include "System_Core_System_Linq_OrderedSequence_2_gen4038725990MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1291766756MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1291766756.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen2202698700.h"
#include "System_Core_System_Linq_SortSequenceContext_2_gen2202698700MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_U3CSortU3Ec__I1279425957.h"
#include "System_Core_System_Linq_QuickSort_1_U3CSortU3Ec__I1279425957MethodDeclarations.h"
#include "System_Core_System_Linq_QuickSort_1_gen2202595634.h"
#include "mscorlib_ArrayTypes.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "System_Core_System_Linq_SortContext_1_gen70478024MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4146091719MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3910333848.h"
#include "mscorlib_System_Nullable_1_gen3910333848MethodDeclarations.h"
#include "DOTween_DG_Tweening_Ease1024295940.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_ValueType4014882752MethodDeclarations.h"
#include "mscorlib_System_ValueType4014882752.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1772024707.h"
#include "mscorlib_System_Nullable_1_gen1772024707MethodDeclarations.h"
#include "DOTween_DG_Tweening_LogBehaviour3180954095.h"
#include "mscorlib_System_Nullable_1_gen1935366568.h"
#include "mscorlib_System_Nullable_1_gen1935366568MethodDeclarations.h"
#include "DOTween_DG_Tweening_LoopType3344295956.h"
#include "mscorlib_System_Nullable_1_gen1724144949.h"
#include "mscorlib_System_Nullable_1_gen1724144949MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookUnityPlat3133074337.h"
#include "mscorlib_System_Nullable_1_gen3534247419.h"
#include "mscorlib_System_Nullable_1_gen3534247419MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_OGActionType648209511.h"
#include "mscorlib_System_Nullable_1_gen2703348181.h"
#include "mscorlib_System_Nullable_1_gen2703348181MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Currency4112277569.h"
#include "mscorlib_System_Nullable_1_gen2958521481.h"
#include "mscorlib_System_Nullable_1_gen2958521481MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeStat72483573.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "mscorlib_System_Nullable_1_gen212373688MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Region1621303076.h"
#include "mscorlib_System_Nullable_1_gen2484541604.h"
#include "mscorlib_System_Nullable_1_gen2484541604MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TitleActiva3893470992.h"
#include "mscorlib_System_Nullable_1_gen1403402234.h"
#include "mscorlib_System_Nullable_1_gen1403402234MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeStatus2812331622.h"
#include "mscorlib_System_Nullable_1_gen4258606580.h"
#include "mscorlib_System_Nullable_1_gen4258606580MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Transaction1372568672.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"
#include "mscorlib_System_Nullable_1_gen2611574664MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserDataPer4020504052.h"
#include "mscorlib_System_Nullable_1_gen417810504.h"
#include "mscorlib_System_Nullable_1_gen417810504MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserOrigina1826739892.h"
#include "mscorlib_System_Nullable_1_gen276009914.h"
#include "mscorlib_System_Nullable_1_gen276009914MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"
#include "mscorlib_System_Nullable_1_gen2936825364.h"
#include "mscorlib_System_Nullable_1_gen2936825364MethodDeclarations.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserConnection50787456.h"
#include "mscorlib_System_Nullable_1_gen4294542862.h"
#include "mscorlib_System_Nullable_1_gen4294542862MethodDeclarations.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserEducation1408504954.h"
#include "mscorlib_System_Nullable_1_gen3783893733.h"
#include "mscorlib_System_Nullable_1_gen3783893733MethodDeclarations.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserEthnicity897855825.h"
#include "mscorlib_System_Nullable_1_gen2074414583.h"
#include "mscorlib_System_Nullable_1_gen2074414583MethodDeclarations.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserGender3483343971.h"
#include "mscorlib_System_Nullable_1_gen2392475756.h"
#include "mscorlib_System_Nullable_1_gen2392475756MethodDeclarations.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserMaritalStatus3801405144.h"
#include "mscorlib_System_Nullable_1_gen3787560604.h"
#include "mscorlib_System_Nullable_1_gen3787560604MethodDeclarations.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserSexualOrientatio901522696.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"
#include "mscorlib_System_Nullable_1_gen3097043249MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1369764433.h"
#include "mscorlib_System_Nullable_1_gen1369764433MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Byte2778693821MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"
#include "mscorlib_System_Nullable_1_gen3225071844MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2303330647.h"
#include "mscorlib_System_Nullable_1_gen2303330647MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_DateTimeOffset3712260035MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3420554522.h"
#include "mscorlib_System_Nullable_1_gen3420554522MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1438485341.h"
#include "mscorlib_System_Nullable_1_gen1438485341MethodDeclarations.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_Int162847414729MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen1438485399MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1438485494.h"
#include "mscorlib_System_Nullable_1_gen1438485494MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen1446416676.h"
#include "mscorlib_System_Nullable_1_gen1446416676MethodDeclarations.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_SByte2855346064MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3844246929.h"
#include "mscorlib_System_Nullable_1_gen3844246929MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "mscorlib_System_Nullable_1_gen3649900800MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3871963176.h"
#include "mscorlib_System_Nullable_1_gen3871963176MethodDeclarations.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_UInt16985925268MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"
#include "mscorlib_System_Nullable_1_gen3871963234MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3871963329.h"
#include "mscorlib_System_Nullable_1_gen3871963329MethodDeclarations.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_UInt64985925421MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen179246372.h"
#include "mscorlib_System_Nullable_1_gen179246372MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen2116400401.h"
#include "mscorlib_System_Nullable_1_gen2116400401MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3862319442.h"
#include "mscorlib_System_Predicate_1_gen3862319442MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Predicate_1_gen218470691.h"
#include "mscorlib_System_Predicate_1_gen218470691MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089.h"
#include "mscorlib_System_Predicate_1_gen2255903200.h"
#include "mscorlib_System_Predicate_1_gen2255903200MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen781969239.h"
#include "mscorlib_System_Predicate_1_gen781969239MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3349657719.h"
#include "mscorlib_System_Predicate_1_gen3349657719MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3349670597.h"
#include "mscorlib_System_Predicate_1_gen3349670597MethodDeclarations.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Predicate_1_gen3883920346.h"
#include "mscorlib_System_Predicate_1_gen3883920346MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen909997834.h"
#include "mscorlib_System_Predicate_1_gen909997834MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1105480512.h"
#include "mscorlib_System_Predicate_1_gen1105480512MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3418378685.h"
#include "mscorlib_System_Predicate_1_gen3418378685MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3418378780.h"
#include "mscorlib_System_Predicate_1_gen3418378780MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1408070318.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen889699027.h"
#include "mscorlib_System_Predicate_1_gen889699027MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu318735129.h"
#include "mscorlib_System_Predicate_1_gen1131379460.h"
#include "mscorlib_System_Predicate_1_gen1131379460MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgu560415562.h"
#include "mscorlib_System_Predicate_1_gen1529172919.h"
#include "mscorlib_System_Predicate_1_gen1529172919MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1556889319.h"
#include "mscorlib_System_Predicate_1_gen1556889319MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2987485885.h"
#include "mscorlib_System_Predicate_1_gen2987485885MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "mscorlib_System_Predicate_1_gen940225717.h"
#include "mscorlib_System_Predicate_1_gen940225717MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Predicate_1_gen3129249936.h"
#include "mscorlib_System_Predicate_1_gen3129249936MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Predicate_1_gen2159139658.h"
#include "mscorlib_System_Predicate_1_gen2159139658MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen413080809.h"
#include "mscorlib_System_Predicate_1_gen413080809MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "mscorlib_System_Predicate_1_gen1530862587.h"
#include "mscorlib_System_Predicate_1_gen1530862587MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResu959898689.h"
#include "mscorlib_System_Predicate_1_gen2462679877.h"
#include "mscorlib_System_Predicate_1_gen2462679877MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "mscorlib_System_Predicate_1_gen2096392715.h"
#include "mscorlib_System_Predicate_1_gen2096392715MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_Predicate_1_gen974784479.h"
#include "mscorlib_System_Predicate_1_gen974784479MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo403820581.h"
#include "mscorlib_System_Predicate_1_gen727885181.h"
#include "mscorlib_System_Predicate_1_gen727885181MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo156921283.h"
#include "mscorlib_System_Predicate_1_gen2831025503.h"
#include "mscorlib_System_Predicate_1_gen2831025503MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex2260061605.h"
#include "mscorlib_System_Predicate_1_gen4096293686.h"
#include "mscorlib_System_Predicate_1_gen4096293686MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_Predicate_1_gen4096293687.h"
#include "mscorlib_System_Predicate_1_gen4096293687MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen4096293688.h"
#include "mscorlib_System_Predicate_1_gen4096293688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_ge539570894.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_ge539570894MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGette171826611.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGette171826611MethodDeclarations.h"
#include "AssemblyU2DCSharp_TableBlock_2_U3CFillU3Ec__AnonSt2250867686.h"
#include "AssemblyU2DCSharp_TableBlock_2_U3CFillU3Ec__AnonSt2250867686MethodDeclarations.h"
#include "AssemblyU2DCSharp_NewTableCell2774679728.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258MethodDeclarations.h"
#include "AssemblyU2DCSharp_InstantiatableInPool1882737686MethodDeclarations.h"
#include "AssemblyU2DCSharp_TableBlock_2_gen3756399434.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "AssemblyU2DCSharp_InstantiatableInPool1882737686.h"
#include "AssemblyU2DCSharp_TableBlock_2_gen3756399434MethodDeclarations.h"
#include "AssemblyU2DCSharp_NewTableView2775249395.h"
#include "AssemblyU2DCSharp_InstantiationPool_1_gen1380750307.h"
#include "AssemblyU2DCSharp_ConnectionCollector444796719MethodDeclarations.h"
#include "AssemblyU2DCSharp_NewTableCell2774679728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837MethodDeclarations.h"
#include "AssemblyU2DCSharp_ConnectionCollector444796719.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "DOTween_DG_Tweening_ShortcutExtensions723807504MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "AssemblyU2DCSharp_InstantiationPool_1_gen1380750307MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "System_Core_System_Action_2_gen4105459918.h"
#include "AssemblyU2DCSharp_NewTableView2775249395MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2564974692.h"
#include "mscorlib_System_Action_1_gen2564974692MethodDeclarations.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "mscorlib_System_Action_1_gen1046711963.h"
#include "mscorlib_System_Action_1_gen1046711963MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070.h"
#include "mscorlib_System_Action_1_gen2645824775.h"
#include "mscorlib_System_Action_1_gen2645824775MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2256885953.h"
#include "System_Core_System_Func_2_gen2256885953MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "System_Core_System_Action_2_gen4105459918MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987MethodDeclarations.h"
#include "DOTween_DG_Tweening_Tweener1766303790.h"
#include "DOTween_DG_Tweening_TweenCallback3786476454MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2047694392MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback3786476454.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2047694392.h"
#include "AssemblyU2DCSharp_Transaction_U3CCalculateU3Ec__An2571930805.h"
#include "AssemblyU2DCSharp_Transaction_U3CCalculateU3Ec__An2571930805MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Transaction_U3CCalculateU3Ec__An1837163501.h"
#include "System_Core_System_Func_1_gen1979887667.h"
#include "System_Core_System_Func_1_gen1979887667MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3736493403.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharp_Transaction_U3CCalculateU3Ec__An1837163501MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3366118735MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_ListDisposable2393830995MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3736493403MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen866472516.h"
#include "System_Core_System_Func_2_gen3366118735.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AotSafeExtensi3925284259.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AotSafeExtensi3925284259MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperationE469001995.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperationE469001995MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperation3431617553MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperation3431617553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperation2586538625.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperation2586538625MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3374395064MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_AsyncOperation3374395064.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_12911320238.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_12911320238MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_11536745524.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1131724091.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1131724091MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_1_337532560.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_1_337532560MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_13257925142.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2852903709.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2852903709MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1967639028.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1967639028MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_11536745524MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4109309822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4109309822MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Dis11563882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Dis11563882MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncSubject_13257925142MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1535522144.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1535522144MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1732743500.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1732743500MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BehaviorSubjec2911320238.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BehaviorSubjec2911320238MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BehaviorSubjec2662307150.h"

// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m1058801104(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m1058801104_gshared)(__this /* static, unused */, p0, method)
// System.Single System.Linq.Enumerable::Sum<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Single>)
extern "C"  float Enumerable_Sum_TisIl2CppObject_m3330304526_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2256885953 * p1, const MethodInfo* method);
#define Enumerable_Sum_TisIl2CppObject_m3330304526(__this /* static, unused */, p0, p1, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2256885953 *, const MethodInfo*))Enumerable_Sum_TisIl2CppObject_m3330304526_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  int32_t Enumerable_Count_TisIl2CppObject_m3348086026_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_Count_TisIl2CppObject_m3348086026(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_Count_TisIl2CppObject_m3348086026_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::ElementAt<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject * Enumerable_ElementAt_TisIl2CppObject_m2597679966_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_ElementAt_TisIl2CppObject_m2597679966(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_ElementAt_TisIl2CppObject_m2597679966_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3007259622_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m3007259622(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_First_TisIl2CppObject_m3007259622_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_Last_TisIl2CppObject_m2863103521_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_Last_TisIl2CppObject_m2863103521(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_Last_TisIl2CppObject_m2863103521_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnComplete_TisIl2CppObject_m4170870576_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3786476454 * p1, const MethodInfo* method);
#define TweenSettingsExtensions_OnComplete_TisIl2CppObject_m4170870576(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, TweenCallback_t3786476454 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisIl2CppObject_m4170870576_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438(__this /* static, unused */, p0, p1, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, Tweener_t1766303790 *, TweenCallback_t3786476454 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisIl2CppObject_m4170870576_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<ICell,System.IDisposable>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisICell_t69513547_TisIDisposable_t1628921374_m824486475(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3366118735 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.IEnumerator UniRx.AsyncOperationExtensions::AsObservableCore<System.Object>(!!0,UniRx.IObserver`1<!!0>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern "C"  Il2CppObject * AsyncOperationExtensions_AsObservableCore_TisIl2CppObject_m703276880_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject* p1, Il2CppObject* p2, CancellationToken_t1439151560  p3, const MethodInfo* method);
#define AsyncOperationExtensions_AsObservableCore_TisIl2CppObject_m703276880(__this /* static, unused */, p0, p1, p2, p3, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject*, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))AsyncOperationExtensions_AsObservableCore_TisIl2CppObject_m703276880_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m3112457808_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  KeyValuePair_2_t3312956448  U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2916775193_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = (KeyValuePair_2_t3312956448 )__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m3102820320_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = (KeyValuePair_2_t3312956448 )__this->get_U24current_4();
		KeyValuePair_2_t3312956448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m294036097_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1875105964_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * L_2 = (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *)L_2;
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_U3CU24U3Ecount_6();
		NullCheck(L_5);
		L_5->set_count_2(L_6);
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m762291308_MetadataUsageId;
extern "C"  bool U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m762291308_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m762291308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00bf;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CenumeratorU3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0098;
			}
		}

IL_0043:
		{
			goto IL_005d;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_005d;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00ad);
		}

IL_005d:
		{
			int32_t L_7 = (int32_t)__this->get_count_2();
			int32_t L_8 = (int32_t)L_7;
			V_2 = (int32_t)L_8;
			__this->set_count_2(((int32_t)((int32_t)L_8-(int32_t)1)));
			int32_t L_9 = V_2;
			if ((((int32_t)L_9) > ((int32_t)0)))
			{
				goto IL_0048;
			}
		}

IL_0074:
		{
			goto IL_0098;
		}

IL_0079:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject*)L_10);
			KeyValuePair_2_t3312956448  L_11 = InterfaceFuncInvoker0< KeyValuePair_2_t3312956448  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_10);
			__this->set_U24current_4(L_11);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC1, FINALLY_00ad);
		}

IL_0098:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_0079;
			}
		}

IL_00a8:
		{
			IL2CPP_LEAVE(0xB8, FINALLY_00ad);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ad;
	}

FINALLY_00ad:
	{ // begin finally (depth: 1)
		{
			bool L_14 = V_1;
			if (!L_14)
			{
				goto IL_00b1;
			}
		}

IL_00b0:
		{
			IL2CPP_END_FINALLY(173)
		}

IL_00b1:
		{
			NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *)__this);
			((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			IL2CPP_END_FINALLY(173)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(173)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_JUMP_TBL(0xC1, IL_00c1)
		IL2CPP_JUMP_TBL(0xB8, IL_00b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00bf:
	{
		return (bool)0;
	}

IL_00c1:
	{
		return (bool)1;
	}
	// Dead block : IL_00c3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m1672998541_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x2D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *)__this);
		((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x2D, IL_002d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m758890749_MetadataUsageId;
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m758890749_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m758890749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>__Finally0()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m3370359331_MetadataUsageId;
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m3370359331_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m3370359331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::.ctor()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m1475680821_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3767661958_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m1064696849_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m610539948_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m382327391_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * L_2 = (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *)L_2;
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_U3CU24U3Ecount_6();
		NullCheck(L_5);
		L_5->set_count_2(L_6);
		U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m360201407_MetadataUsageId;
extern "C"  bool U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m360201407_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m360201407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00bf;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CenumeratorU3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0098;
			}
		}

IL_0043:
		{
			goto IL_005d;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_005d;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00ad);
		}

IL_005d:
		{
			int32_t L_7 = (int32_t)__this->get_count_2();
			int32_t L_8 = (int32_t)L_7;
			V_2 = (int32_t)L_8;
			__this->set_count_2(((int32_t)((int32_t)L_8-(int32_t)1)));
			int32_t L_9 = V_2;
			if ((((int32_t)L_9) > ((int32_t)0)))
			{
				goto IL_0048;
			}
		}

IL_0074:
		{
			goto IL_0098;
		}

IL_0079:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject*)L_10);
			Il2CppObject * L_11 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_10);
			__this->set_U24current_4(L_11);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC1, FINALLY_00ad);
		}

IL_0098:
		{
			Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
			NullCheck((Il2CppObject *)L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			if (L_13)
			{
				goto IL_0079;
			}
		}

IL_00a8:
		{
			IL2CPP_LEAVE(0xB8, FINALLY_00ad);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ad;
	}

FINALLY_00ad:
	{ // begin finally (depth: 1)
		{
			bool L_14 = V_1;
			if (!L_14)
			{
				goto IL_00b1;
			}
		}

IL_00b0:
		{
			IL2CPP_END_FINALLY(173)
		}

IL_00b1:
		{
			NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *)__this);
			((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			IL2CPP_END_FINALLY(173)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(173)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_JUMP_TBL(0xC1, IL_00c1)
		IL2CPP_JUMP_TBL(0xB8, IL_00b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00bf:
	{
		return (bool)0;
	}

IL_00c1:
	{
		return (bool)1;
	}
	// Dead block : IL_00c3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::Dispose()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m688344370_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x2D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		NullCheck((U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *)__this);
		((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x2D, IL_002d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3417081058_MetadataUsageId;
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3417081058_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3417081058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Object>::<>__Finally0()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m2114886302_MetadataUsageId;
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m2114886302_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4275726103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m2114886302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U3CenumeratorU3E__0_1();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m2336549733_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  KeyValuePair_2_t3312956448  U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m690243694_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = (KeyValuePair_2_t3312956448 )__this->get_U24current_6();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m270576619_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = (KeyValuePair_2_t3312956448 )__this->get_U24current_6();
		KeyValuePair_2_t3312956448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m1517105100_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1072048833_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method)
{
	U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * L_2 = (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 *)L_2;
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * L_3 = V_0;
		int32_t L_4 = (int32_t)__this->get_U3CU24U3Ecount_7();
		NullCheck(L_3);
		L_3->set_count_0(L_4);
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Esource_8();
		NullCheck(L_5);
		L_5->set_source_2(L_6);
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1198816055_MetadataUsageId;
extern "C"  bool U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1198816055_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1198816055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_00e1;
	}

IL_0023:
	{
		int32_t L_2 = (int32_t)__this->get_count_0();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_00e1;
	}

IL_0034:
	{
		__this->set_U3CcounterU3E__0_1(0);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_2();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3);
		__this->set_U3CU24s_90U3E__1_3(L_4);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_008b;
			}
		}

IL_005b:
		{
			goto IL_00ac;
		}

IL_0060:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			NullCheck((Il2CppObject*)L_6);
			KeyValuePair_2_t3312956448  L_7 = InterfaceFuncInvoker0< KeyValuePair_2_t3312956448  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6);
			__this->set_U3CelementU3E__2_4(L_7);
			KeyValuePair_2_t3312956448  L_8 = (KeyValuePair_2_t3312956448 )__this->get_U3CelementU3E__2_4();
			__this->set_U24current_6(L_8);
			__this->set_U24PC_5(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xE3, FINALLY_00c1);
		}

IL_008b:
		{
			int32_t L_9 = (int32_t)__this->get_U3CcounterU3E__0_1();
			int32_t L_10 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
			V_2 = (int32_t)L_10;
			__this->set_U3CcounterU3E__0_1(L_10);
			int32_t L_11 = V_2;
			int32_t L_12 = (int32_t)__this->get_count_0();
			if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
			{
				goto IL_00ac;
			}
		}

IL_00a7:
		{
			IL2CPP_LEAVE(0xE1, FINALLY_00c1);
		}

IL_00ac:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			NullCheck((Il2CppObject *)L_13);
			bool L_14 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			if (L_14)
			{
				goto IL_0060;
			}
		}

IL_00bc:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c1;
	}

FINALLY_00c1:
	{ // begin finally (depth: 1)
		{
			bool L_15 = V_1;
			if (!L_15)
			{
				goto IL_00c5;
			}
		}

IL_00c4:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00c5:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			if (L_16)
			{
				goto IL_00ce;
			}
		}

IL_00cd:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00ce:
		{
			Il2CppObject* L_17 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			NullCheck((Il2CppObject *)L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
			IL2CPP_END_FINALLY(193)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(193)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_JUMP_TBL(0xE1, IL_00e1)
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00da:
	{
		__this->set_U24PC_5((-1));
	}

IL_00e1:
	{
		return (bool)0;
	}

IL_00e3:
	{
		return (bool)1;
	}
	// Dead block : IL_00e5: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m3349647970_MetadataUsageId;
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m3349647970_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m3349647970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m4277949970_MetadataUsageId;
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m4277949970_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t1105598806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m4277949970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::.ctor()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m1991994240_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m965116049_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m2872192422_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m2446998017_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m80203434_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * __this, const MethodInfo* method)
{
	U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * L_2 = (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 *)L_2;
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * L_3 = V_0;
		int32_t L_4 = (int32_t)__this->get_U3CU24U3Ecount_7();
		NullCheck(L_3);
		L_3->set_count_0(L_4);
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Esource_8();
		NullCheck(L_5);
		L_5->set_source_2(L_6);
		U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1575379860_MetadataUsageId;
extern "C"  bool U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1575379860_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1575379860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_00e1;
	}

IL_0023:
	{
		int32_t L_2 = (int32_t)__this->get_count_0();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_00e1;
	}

IL_0034:
	{
		__this->set_U3CcounterU3E__0_1(0);
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_source_2();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3);
		__this->set_U3CU24s_90U3E__1_3(L_4);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_008b;
			}
		}

IL_005b:
		{
			goto IL_00ac;
		}

IL_0060:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			NullCheck((Il2CppObject*)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_6);
			__this->set_U3CelementU3E__2_4(L_7);
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			__this->set_U24current_6(L_8);
			__this->set_U24PC_5(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xE3, FINALLY_00c1);
		}

IL_008b:
		{
			int32_t L_9 = (int32_t)__this->get_U3CcounterU3E__0_1();
			int32_t L_10 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
			V_2 = (int32_t)L_10;
			__this->set_U3CcounterU3E__0_1(L_10);
			int32_t L_11 = V_2;
			int32_t L_12 = (int32_t)__this->get_count_0();
			if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
			{
				goto IL_00ac;
			}
		}

IL_00a7:
		{
			IL2CPP_LEAVE(0xE1, FINALLY_00c1);
		}

IL_00ac:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			NullCheck((Il2CppObject *)L_13);
			bool L_14 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			if (L_14)
			{
				goto IL_0060;
			}
		}

IL_00bc:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c1;
	}

FINALLY_00c1:
	{ // begin finally (depth: 1)
		{
			bool L_15 = V_1;
			if (!L_15)
			{
				goto IL_00c5;
			}
		}

IL_00c4:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00c5:
		{
			Il2CppObject* L_16 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			if (L_16)
			{
				goto IL_00ce;
			}
		}

IL_00cd:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00ce:
		{
			Il2CppObject* L_17 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			NullCheck((Il2CppObject *)L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
			IL2CPP_END_FINALLY(193)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(193)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_JUMP_TBL(0xE1, IL_00e1)
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00da:
	{
		__this->set_U24PC_5((-1));
	}

IL_00e1:
	{
		return (bool)0;
	}

IL_00e3:
	{
		return (bool)1;
	}
	// Dead block : IL_00e5: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m2944300989_MetadataUsageId;
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m2944300989_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m2944300989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_90U3E__1_3();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m3933394477_MetadataUsageId;
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m3933394477_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2924716074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m3933394477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m2454643338_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  KeyValuePair_2_t3312956448  U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1160431387_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = (KeyValuePair_2_t3312956448 )__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3073915164_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = (KeyValuePair_2_t3312956448 )__this->get_U24current_5();
		KeyValuePair_2_t3312956448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m73174455_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3712988404_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * L_5 = V_0;
		Func_2_t1844407557 * L_6 = (Func_2_t1844407557 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m4000029002_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m4000029002_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m4000029002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_97U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			KeyValuePair_2_t3312956448  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t3312956448  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t1844407557 * L_7 = (Func_2_t1844407557 *)__this->get_predicate_3();
			KeyValuePair_2_t3312956448  L_8 = (KeyValuePair_2_t3312956448 )__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t1844407557 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t1844407557 *, KeyValuePair_2_t3312956448 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Func_2_t1844407557 *)L_7, (KeyValuePair_2_t3312956448 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			KeyValuePair_2_t3312956448  L_10 = (KeyValuePair_2_t3312956448 )__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m873485383_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m873485383_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m873485383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m101076279_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m101076279_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1959607897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m101076279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m908178940_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  int32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m367695373_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3101032618_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_5();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3319094277_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1484551462_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * L_5 = V_0;
		Func_2_t3308141622 * L_6 = (Func_2_t3308141622 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3197908888_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3197908888_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3197908888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_97U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3308141622 * L_7 = (Func_2_t3308141622 *)__this->get_predicate_3();
			int32_t L_8 = (int32_t)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3308141622 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3308141622 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Func_2_t3308141622 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			int32_t L_10 = (int32_t)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m779883321_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m779883321_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m779883321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m2849579177_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m2849579177_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1494066236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m2849579177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3836766395_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1015065924_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m1370525525_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m503616950_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2553601303_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * L_5 = V_0;
		Func_2_t1509682273 * L_6 = (Func_2_t1509682273 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m461554209_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m461554209_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m461554209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_97U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t1509682273 * L_7 = (Func_2_t1509682273 *)__this->get_predicate_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t1509682273 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Func_2_t1509682273 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1948848696_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1948848696_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1948848696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_97U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1483199336_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1483199336_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3778725165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1483199336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/PredicateOf`1<System.Int32>::.cctor()
extern "C"  void PredicateOf_1__cctor_m686174972_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Func_2_t3308141622 * L_0 = ((PredicateOf_1_t2617073743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Func_2_t3308141622 * L_2 = (Func_2_t3308141622 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Func_2_t3308141622 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((PredicateOf_1_t2617073743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
	}

IL_0018:
	{
		Func_2_t3308141622 * L_3 = ((PredicateOf_1_t2617073743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		((PredicateOf_1_t2617073743_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Always_0(L_3);
		return;
	}
}
// System.Boolean System.Linq.Enumerable/PredicateOf`1<System.Int32>::<Always>m__76(T)
extern "C"  bool PredicateOf_1_U3CAlwaysU3Em__76_m1746297546_gshared (Il2CppObject * __this /* static, unused */, int32_t ___t0, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Linq.Enumerable/PredicateOf`1<System.Object>::.cctor()
extern "C"  void PredicateOf_1__cctor_m3468613095_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Func_2_t1509682273 * L_0 = ((PredicateOf_1_t606765376_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Func_2_t1509682273 * L_2 = (Func_2_t1509682273 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Func_2_t1509682273 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((PredicateOf_1_t606765376_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
	}

IL_0018:
	{
		Func_2_t1509682273 * L_3 = ((PredicateOf_1_t606765376_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		((PredicateOf_1_t606765376_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Always_0(L_3);
		return;
	}
}
// System.Boolean System.Linq.Enumerable/PredicateOf`1<System.Object>::<Always>m__76(T)
extern "C"  bool PredicateOf_1_U3CAlwaysU3Em__76_m2075728909_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___t0, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Linq.Grouping`2<System.Object,System.Object>::.ctor(K,System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void Grouping_2__ctor_m1155702827_gshared (Grouping_2_t1565096229 * __this, Il2CppObject * ___key0, Il2CppObject* ___group1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___group1;
		__this->set_group_1(L_0);
		Il2CppObject * L_1 = ___key0;
		__this->set_key_0(L_1);
		return;
	}
}
// System.Collections.IEnumerator System.Linq.Grouping`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Grouping_2_System_Collections_IEnumerable_GetEnumerator_m3711015404_gshared (Grouping_2_t1565096229 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_group_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		return L_1;
	}
}
// K System.Linq.Grouping`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * Grouping_2_get_Key_m420217201_gshared (Grouping_2_t1565096229 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Linq.Grouping`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Grouping_2_GetEnumerator_m2775414121_gshared (Grouping_2_t1565096229 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_group_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void System.Linq.OrderedEnumerable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  void OrderedEnumerable_1__ctor_m1764331993_gshared (OrderedEnumerable_1_t1902017548 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		__this->set_source_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Linq.OrderedEnumerable`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * OrderedEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m37555435_gshared (OrderedEnumerable_1_t1902017548 * __this, const MethodInfo* method)
{
	{
		NullCheck((OrderedEnumerable_1_t1902017548 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::GetEnumerator() */, (OrderedEnumerable_1_t1902017548 *)__this);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* OrderedEnumerable_1_GetEnumerator_m1657609258_gshared (OrderedEnumerable_1_t1902017548 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_0();
		NullCheck((OrderedEnumerable_1_t1902017548 *)__this);
		Il2CppObject* L_1 = VirtFuncInvoker1< Il2CppObject*, Il2CppObject* >::Invoke(7 /* System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>) */, (OrderedEnumerable_1_t1902017548 *)__this, (Il2CppObject*)L_0);
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m1175215065_gshared (OrderedSequence_2_t1754067061 * __this, Il2CppObject* ___source0, Func_2_t4146091719 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	OrderedSequence_2_t1754067061 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	OrderedSequence_2_t1754067061 * G_B1_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedEnumerable_1_t1902017548 *)__this);
		((  void (*) (OrderedEnumerable_1_t1902017548 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OrderedEnumerable_1_t1902017548 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t4146091719 * L_1 = ___key_selector1;
		__this->set_selector_2(L_1);
		Il2CppObject* L_2 = ___comparer2;
		Il2CppObject* L_3 = (Il2CppObject*)L_2;
		G_B1_0 = L_3;
		G_B1_1 = ((OrderedSequence_2_t1754067061 *)(__this));
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = ((OrderedSequence_2_t1754067061 *)(__this));
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3302075123 * L_4 = ((  Comparer_1_t3302075123 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B2_0 = ((Il2CppObject*)(L_4));
		G_B2_1 = ((OrderedSequence_2_t1754067061 *)(G_B1_1));
	}

IL_001c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_comparer_3(G_B2_0);
		int32_t L_5 = ___direction3;
		__this->set_direction_4(L_5);
		return;
	}
}
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t70478024 * OrderedSequence_2_CreateContext_m1322519273_gshared (OrderedSequence_2_t1754067061 * __this, SortContext_1_t70478024 * ___current0, const MethodInfo* method)
{
	SortContext_1_t70478024 * V_0 = NULL;
	{
		Func_2_t4146091719 * L_0 = (Func_2_t4146091719 *)__this->get_selector_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_comparer_3();
		int32_t L_2 = (int32_t)__this->get_direction_4();
		SortContext_1_t70478024 * L_3 = ___current0;
		SortSequenceContext_2_t4213007067 * L_4 = (SortSequenceContext_2_t4213007067 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (SortSequenceContext_2_t4213007067 *, Func_2_t4146091719 *, Il2CppObject*, int32_t, SortContext_1_t70478024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Func_2_t4146091719 *)L_0, (Il2CppObject*)L_1, (int32_t)L_2, (SortContext_1_t70478024 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (SortContext_1_t70478024 *)L_4;
		OrderedEnumerable_1_t1902017548 * L_5 = (OrderedEnumerable_1_t1902017548 *)__this->get_parent_1();
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		OrderedEnumerable_1_t1902017548 * L_6 = (OrderedEnumerable_1_t1902017548 *)__this->get_parent_1();
		SortContext_1_t70478024 * L_7 = V_0;
		NullCheck((OrderedEnumerable_1_t1902017548 *)L_6);
		SortContext_1_t70478024 * L_8 = VirtFuncInvoker1< SortContext_1_t70478024 *, SortContext_1_t70478024 * >::Invoke(6 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedEnumerable_1_t1902017548 *)L_6, (SortContext_1_t70478024 *)L_7);
		return L_8;
	}

IL_0031:
	{
		SortContext_1_t70478024 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m2503037792_gshared (OrderedSequence_2_t1754067061 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedSequence_2_t1754067061 *)__this);
		SortContext_1_t70478024 * L_1 = VirtFuncInvoker1< SortContext_1_t70478024 *, SortContext_1_t70478024 * >::Invoke(6 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Int32>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedSequence_2_t1754067061 *)__this, (SortContext_1_t70478024 *)NULL);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t70478024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (SortContext_1_t70478024 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_2;
	}
}
// System.Void System.Linq.OrderedSequence`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection)
extern "C"  void OrderedSequence_2__ctor_m4118986726_gshared (OrderedSequence_2_t4038725990 * __this, Il2CppObject* ___source0, Func_2_t2135783352 * ___key_selector1, Il2CppObject* ___comparer2, int32_t ___direction3, const MethodInfo* method)
{
	Il2CppObject* G_B2_0 = NULL;
	OrderedSequence_2_t4038725990 * G_B2_1 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	OrderedSequence_2_t4038725990 * G_B1_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedEnumerable_1_t1902017548 *)__this);
		((  void (*) (OrderedEnumerable_1_t1902017548 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OrderedEnumerable_1_t1902017548 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2135783352 * L_1 = ___key_selector1;
		__this->set_selector_2(L_1);
		Il2CppObject* L_2 = ___comparer2;
		Il2CppObject* L_3 = (Il2CppObject*)L_2;
		G_B1_0 = L_3;
		G_B1_1 = ((OrderedSequence_2_t4038725990 *)(__this));
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = ((OrderedSequence_2_t4038725990 *)(__this));
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1291766756 * L_4 = ((  Comparer_1_t1291766756 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B2_0 = ((Il2CppObject*)(L_4));
		G_B2_1 = ((OrderedSequence_2_t4038725990 *)(G_B1_1));
	}

IL_001c:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_comparer_3(G_B2_0);
		int32_t L_5 = ___direction3;
		__this->set_direction_4(L_5);
		return;
	}
}
// System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::CreateContext(System.Linq.SortContext`1<TElement>)
extern "C"  SortContext_1_t70478024 * OrderedSequence_2_CreateContext_m2181301000_gshared (OrderedSequence_2_t4038725990 * __this, SortContext_1_t70478024 * ___current0, const MethodInfo* method)
{
	SortContext_1_t70478024 * V_0 = NULL;
	{
		Func_2_t2135783352 * L_0 = (Func_2_t2135783352 *)__this->get_selector_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_comparer_3();
		int32_t L_2 = (int32_t)__this->get_direction_4();
		SortContext_1_t70478024 * L_3 = ___current0;
		SortSequenceContext_2_t2202698700 * L_4 = (SortSequenceContext_2_t2202698700 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (SortSequenceContext_2_t2202698700 *, Func_2_t2135783352 *, Il2CppObject*, int32_t, SortContext_1_t70478024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Func_2_t2135783352 *)L_0, (Il2CppObject*)L_1, (int32_t)L_2, (SortContext_1_t70478024 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (SortContext_1_t70478024 *)L_4;
		OrderedEnumerable_1_t1902017548 * L_5 = (OrderedEnumerable_1_t1902017548 *)__this->get_parent_1();
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		OrderedEnumerable_1_t1902017548 * L_6 = (OrderedEnumerable_1_t1902017548 *)__this->get_parent_1();
		SortContext_1_t70478024 * L_7 = V_0;
		NullCheck((OrderedEnumerable_1_t1902017548 *)L_6);
		SortContext_1_t70478024 * L_8 = VirtFuncInvoker1< SortContext_1_t70478024 *, SortContext_1_t70478024 * >::Invoke(6 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedEnumerable`1<System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedEnumerable_1_t1902017548 *)L_6, (SortContext_1_t70478024 *)L_7);
		return L_8;
	}

IL_0031:
	{
		SortContext_1_t70478024 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>)
extern "C"  Il2CppObject* OrderedSequence_2_Sort_m2978680275_gshared (OrderedSequence_2_t4038725990 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((OrderedSequence_2_t4038725990 *)__this);
		SortContext_1_t70478024 * L_1 = VirtFuncInvoker1< SortContext_1_t70478024 *, SortContext_1_t70478024 * >::Invoke(6 /* System.Linq.SortContext`1<TElement> System.Linq.OrderedSequence`2<System.Object,System.Object>::CreateContext(System.Linq.SortContext`1<TElement>) */, (OrderedSequence_2_t4038725990 *)__this, (SortContext_1_t70478024 *)NULL);
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t70478024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, (SortContext_1_t70478024 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_2;
	}
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::.ctor()
extern "C"  void U3CSortU3Ec__Iterator21__ctor_m2478484409_gshared (U3CSortU3Ec__Iterator21_t1279425957 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TElement System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.Generic.IEnumerator<TElement>.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m345773114_gshared (U3CSortU3Ec__Iterator21_t1279425957 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m3593665815_gshared (U3CSortU3Ec__Iterator21_t1279425957 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m1080975608_gshared (U3CSortU3Ec__Iterator21_t1279425957 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CSortU3Ec__Iterator21_t1279425957 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CSortU3Ec__Iterator21_t1279425957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CSortU3Ec__Iterator21_t1279425957 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::System.Collections.Generic.IEnumerable<TElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m1867380327_gshared (U3CSortU3Ec__Iterator21_t1279425957 * __this, const MethodInfo* method)
{
	U3CSortU3Ec__Iterator21_t1279425957 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CSortU3Ec__Iterator21_t1279425957 * L_2 = (U3CSortU3Ec__Iterator21_t1279425957 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CSortU3Ec__Iterator21_t1279425957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CSortU3Ec__Iterator21_t1279425957 *)L_2;
		U3CSortU3Ec__Iterator21_t1279425957 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CSortU3Ec__Iterator21_t1279425957 * L_5 = V_0;
		SortContext_1_t70478024 * L_6 = (SortContext_1_t70478024 *)__this->get_U3CU24U3Econtext_7();
		NullCheck(L_5);
		L_5->set_context_1(L_6);
		U3CSortU3Ec__Iterator21_t1279425957 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::MoveNext()
extern "C"  bool U3CSortU3Ec__Iterator21_MoveNext_m974020195_gshared (U3CSortU3Ec__Iterator21_t1279425957 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0083;
		}
	}
	{
		goto IL_00b0;
	}

IL_0021:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		SortContext_1_t70478024 * L_3 = (SortContext_1_t70478024 *)__this->get_context_1();
		QuickSort_1_t2202595634 * L_4 = (QuickSort_1_t2202595634 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (QuickSort_1_t2202595634 *, Il2CppObject*, SortContext_1_t70478024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (Il2CppObject*)L_2, (SortContext_1_t70478024 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		__this->set_U3CsorterU3E__0_2(L_4);
		QuickSort_1_t2202595634 * L_5 = (QuickSort_1_t2202595634 *)__this->get_U3CsorterU3E__0_2();
		NullCheck((QuickSort_1_t2202595634 *)L_5);
		((  void (*) (QuickSort_1_t2202595634 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((QuickSort_1_t2202595634 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_U3CiU3E__1_3(0);
		goto IL_0091;
	}

IL_004f:
	{
		QuickSort_1_t2202595634 * L_6 = (QuickSort_1_t2202595634 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_6);
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)L_6->get_elements_0();
		QuickSort_1_t2202595634 * L_8 = (QuickSort_1_t2202595634 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_8);
		Int32U5BU5D_t1809983122* L_9 = (Int32U5BU5D_t1809983122*)L_8->get_indexes_1();
		int32_t L_10 = (int32_t)__this->get_U3CiU3E__1_3();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11)));
		__this->set_U24current_5(((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_12))));
		__this->set_U24PC_4(1);
		goto IL_00b2;
	}

IL_0083:
	{
		int32_t L_13 = (int32_t)__this->get_U3CiU3E__1_3();
		__this->set_U3CiU3E__1_3(((int32_t)((int32_t)L_13+(int32_t)1)));
	}

IL_0091:
	{
		int32_t L_14 = (int32_t)__this->get_U3CiU3E__1_3();
		QuickSort_1_t2202595634 * L_15 = (QuickSort_1_t2202595634 *)__this->get_U3CsorterU3E__0_2();
		NullCheck(L_15);
		Int32U5BU5D_t1809983122* L_16 = (Int32U5BU5D_t1809983122*)L_15->get_indexes_1();
		NullCheck(L_16);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00b0:
	{
		return (bool)0;
	}

IL_00b2:
	{
		return (bool)1;
	}
	// Dead block : IL_00b4: ldloc.1
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::Dispose()
extern "C"  void U3CSortU3Ec__Iterator21_Dispose_m2309918134_gshared (U3CSortU3Ec__Iterator21_t1279425957 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CSortU3Ec__Iterator21_Reset_m124917350_MetadataUsageId;
extern "C"  void U3CSortU3Ec__Iterator21_Reset_m124917350_gshared (U3CSortU3Ec__Iterator21_t1279425957 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSortU3Ec__Iterator21_Reset_m124917350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  void QuickSort_1__ctor_m2169278638_gshared (QuickSort_1_t2202595634 * __this, Il2CppObject* ___source0, SortContext_1_t70478024 * ___context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___source0;
		ObjectU5BU5D_t11523773* L_1 = ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_elements_0(L_1);
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get_elements_0();
		NullCheck(L_2);
		Int32U5BU5D_t1809983122* L_3 = ((  Int32U5BU5D_t1809983122* (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_indexes_1(L_3);
		SortContext_1_t70478024 * L_4 = ___context1;
		__this->set_context_2(L_4);
		return;
	}
}
// System.Int32[] System.Linq.QuickSort`1<System.Object>::CreateIndexes(System.Int32)
extern Il2CppClass* Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var;
extern const uint32_t QuickSort_1_CreateIndexes_m1344525507_MetadataUsageId;
extern "C"  Int32U5BU5D_t1809983122* QuickSort_1_CreateIndexes_m1344525507_gshared (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QuickSort_1_CreateIndexes_m1344525507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t1809983122* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___length0;
		V_0 = (Int32U5BU5D_t1809983122*)((Int32U5BU5D_t1809983122*)SZArrayNew(Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var, (uint32_t)L_0));
		V_1 = (int32_t)0;
		goto IL_0016;
	}

IL_000e:
	{
		Int32U5BU5D_t1809983122* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (int32_t)L_3);
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0016:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___length0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_7 = V_0;
		return L_7;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::PerformSort()
extern "C"  void QuickSort_1_PerformSort_m665582841_gshared (QuickSort_1_t2202595634 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_elements_0();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) > ((int32_t)1)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		SortContext_1_t70478024 * L_1 = (SortContext_1_t70478024 *)__this->get_context_2();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get_elements_0();
		NullCheck((SortContext_1_t70478024 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t11523773* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t70478024 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		NullCheck(L_3);
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
// System.Int32 System.Linq.QuickSort`1<System.Object>::CompareItems(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_CompareItems_m1696049333_gshared (QuickSort_1_t2202595634 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	{
		SortContext_1_t70478024 * L_0 = (SortContext_1_t70478024 *)__this->get_context_2();
		int32_t L_1 = ___first_index0;
		int32_t L_2 = ___second_index1;
		NullCheck((SortContext_1_t70478024 *)L_0);
		int32_t L_3 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t70478024 *)L_0, (int32_t)L_1, (int32_t)L_2);
		return L_3;
	}
}
// System.Int32 System.Linq.QuickSort`1<System.Object>::MedianOfThree(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_MedianOfThree_m4087926973_gshared (QuickSort_1_t2202595634 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___left0;
		int32_t L_1 = ___right1;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1))/(int32_t)2));
		Int32U5BU5D_t1809983122* L_2 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_6 = ___left0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		int32_t L_8 = ((  int32_t (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), (int32_t)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_9 = ___left0;
		int32_t L_10 = V_0;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_002a:
	{
		Int32U5BU5D_t1809983122* L_11 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_12 = ___right1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		Int32U5BU5D_t1809983122* L_14 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_15 = ___left0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		int32_t L_17 = ((  int32_t (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))), (int32_t)((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_18 = ___left0;
		int32_t L_19 = ___right1;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_18, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_004e:
	{
		Int32U5BU5D_t1809983122* L_20 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_21 = ___right1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		Int32U5BU5D_t1809983122* L_23 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_24 = V_0;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		int32_t L_26 = ((  int32_t (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)((L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22))), (int32_t)((L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((int32_t)L_26) >= ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_27 = V_0;
		int32_t L_28 = ___right1;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_27, (int32_t)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_0072:
	{
		int32_t L_29 = V_0;
		int32_t L_30 = ___right1;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_29, (int32_t)((int32_t)((int32_t)L_30-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Int32U5BU5D_t1809983122* L_31 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_32 = ___right1;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)L_32-(int32_t)1)));
		int32_t L_33 = ((int32_t)((int32_t)L_32-(int32_t)1));
		return ((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33)));
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::Sort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Sort_m2143186880_gshared (QuickSort_1_t2202595634 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___left0;
		int32_t L_1 = ___right1;
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)3))) > ((int32_t)L_1)))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_2 = ___left0;
		V_0 = (int32_t)L_2;
		int32_t L_3 = ___right1;
		V_1 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		int32_t L_4 = ___left0;
		int32_t L_5 = ___right1;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		int32_t L_6 = ((  int32_t (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_2 = (int32_t)L_6;
	}

IL_0018:
	{
		goto IL_001d;
	}

IL_001d:
	{
		Int32U5BU5D_t1809983122* L_7 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		V_0 = (int32_t)L_9;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_9);
		int32_t L_10 = L_9;
		int32_t L_11 = V_2;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		int32_t L_12 = ((  int32_t (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((int32_t)L_12) < ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		goto IL_003b;
	}

IL_003b:
	{
		Int32U5BU5D_t1809983122* L_13 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_14 = V_1;
		int32_t L_15 = (int32_t)((int32_t)((int32_t)L_14-(int32_t)1));
		V_1 = (int32_t)L_15;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = V_2;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		int32_t L_18 = ((  int32_t (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_16))), (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((int32_t)L_18) > ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_19 = V_0;
		int32_t L_20 = V_1;
		if ((((int32_t)L_19) >= ((int32_t)L_20)))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = V_1;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		goto IL_006d;
	}

IL_0068:
	{
		goto IL_0072;
	}

IL_006d:
	{
		goto IL_0018;
	}

IL_0072:
	{
		int32_t L_23 = V_0;
		int32_t L_24 = ___right1;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_23, (int32_t)((int32_t)((int32_t)L_24-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_25 = ___left0;
		int32_t L_26 = V_0;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_25, (int32_t)((int32_t)((int32_t)L_26-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_27 = V_0;
		int32_t L_28 = ___right1;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)((int32_t)((int32_t)L_27+(int32_t)1)), (int32_t)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		goto IL_009d;
	}

IL_0095:
	{
		int32_t L_29 = ___left0;
		int32_t L_30 = ___right1;
		NullCheck((QuickSort_1_t2202595634 *)__this);
		((  void (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_29, (int32_t)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
	}

IL_009d:
	{
		return;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::InsertionSort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_InsertionSort_m4059027839_gshared (QuickSort_1_t2202595634 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___left0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)1));
		goto IL_005a;
	}

IL_0009:
	{
		Int32U5BU5D_t1809983122* L_1 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (int32_t)((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3)));
		int32_t L_4 = V_0;
		V_1 = (int32_t)L_4;
		goto IL_002f;
	}

IL_0019:
	{
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_6 = V_1;
		Int32U5BU5D_t1809983122* L_7 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8-(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8-(int32_t)1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
	}

IL_002f:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = ___left0;
		if ((((int32_t)L_11) <= ((int32_t)L_12)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_13 = V_2;
		Int32U5BU5D_t1809983122* L_14 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)L_15-(int32_t)1)));
		int32_t L_16 = ((int32_t)((int32_t)L_15-(int32_t)1));
		NullCheck((QuickSort_1_t2202595634 *)__this);
		int32_t L_17 = ((  int32_t (*) (QuickSort_1_t2202595634 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((QuickSort_1_t2202595634 *)__this, (int32_t)L_13, (int32_t)((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((int32_t)L_17) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}

IL_004d:
	{
		Int32U5BU5D_t1809983122* L_18 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_19 = V_1;
		int32_t L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (int32_t)L_20);
		int32_t L_21 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_005a:
	{
		int32_t L_22 = V_0;
		int32_t L_23 = ___right1;
		if ((((int32_t)L_22) <= ((int32_t)L_23)))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void System.Linq.QuickSort`1<System.Object>::Swap(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Swap_m59867819_gshared (QuickSort_1_t2202595634 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_1 = ___right1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (int32_t)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_4 = ___right1;
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_6 = ___left0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (int32_t)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		Int32U5BU5D_t1809983122* L_8 = (Int32U5BU5D_t1809983122*)__this->get_indexes_1();
		int32_t L_9 = ___left0;
		int32_t L_10 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (int32_t)L_10);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.QuickSort`1<System.Object>::Sort(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  Il2CppObject* QuickSort_1_Sort_m1924284392_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, SortContext_1_t70478024 * ___context1, const MethodInfo* method)
{
	U3CSortU3Ec__Iterator21_t1279425957 * V_0 = NULL;
	{
		U3CSortU3Ec__Iterator21_t1279425957 * L_0 = (U3CSortU3Ec__Iterator21_t1279425957 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CSortU3Ec__Iterator21_t1279425957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CSortU3Ec__Iterator21_t1279425957 *)L_0;
		U3CSortU3Ec__Iterator21_t1279425957 * L_1 = V_0;
		Il2CppObject* L_2 = ___source0;
		NullCheck(L_1);
		L_1->set_source_0(L_2);
		U3CSortU3Ec__Iterator21_t1279425957 * L_3 = V_0;
		SortContext_1_t70478024 * L_4 = ___context1;
		NullCheck(L_3);
		L_3->set_context_1(L_4);
		U3CSortU3Ec__Iterator21_t1279425957 * L_5 = V_0;
		Il2CppObject* L_6 = ___source0;
		NullCheck(L_5);
		L_5->set_U3CU24U3Esource_6(L_6);
		U3CSortU3Ec__Iterator21_t1279425957 * L_7 = V_0;
		SortContext_1_t70478024 * L_8 = ___context1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Econtext_7(L_8);
		U3CSortU3Ec__Iterator21_t1279425957 * L_9 = V_0;
		U3CSortU3Ec__Iterator21_t1279425957 * L_10 = (U3CSortU3Ec__Iterator21_t1279425957 *)L_9;
		NullCheck(L_10);
		L_10->set_U24PC_4(((int32_t)-2));
		return L_10;
	}
}
// System.Void System.Linq.SortContext`1<System.Object>::.ctor(System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortContext_1__ctor_m3304039901_gshared (SortContext_1_t70478024 * __this, int32_t ___direction0, SortContext_1_t70478024 * ___child_context1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___direction0;
		__this->set_direction_0(L_0);
		SortContext_1_t70478024 * L_1 = ___child_context1;
		__this->set_child_context_1(L_1);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Int32>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m2638472483_gshared (SortSequenceContext_2_t4213007067 * __this, Func_2_t4146091719 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t70478024 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t70478024 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t70478024 *)__this);
		((  void (*) (SortContext_1_t70478024 *, int32_t, SortContext_1_t70478024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((SortContext_1_t70478024 *)__this, (int32_t)L_0, (SortContext_1_t70478024 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t4146091719 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Int32>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m3137705959_gshared (SortSequenceContext_2_t4213007067 * __this, ObjectU5BU5D_t11523773* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t70478024 * L_0 = (SortContext_1_t70478024 *)((SortContext_1_t70478024 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t70478024 * L_1 = (SortContext_1_t70478024 *)((SortContext_1_t70478024 *)__this)->get_child_context_1();
		ObjectU5BU5D_t11523773* L_2 = ___elements0;
		NullCheck((SortContext_1_t70478024 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t11523773* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t70478024 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t11523773* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		Int32U5BU5D_t1809983122* L_4 = (Int32U5BU5D_t1809983122*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t4146091719 * L_6 = (Func_2_t4146091719 *)__this->get_selector_2();
		ObjectU5BU5D_t11523773* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck((Func_2_t4146091719 *)L_6);
		int32_t L_10 = ((  int32_t (*) (Func_2_t4146091719 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t4146091719 *)L_6, (Il2CppObject *)((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_10);
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_0;
		Int32U5BU5D_t1809983122* L_13 = (Int32U5BU5D_t1809983122*)__this->get_keys_4();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.Int32>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m968446610_gshared (SortSequenceContext_2_t4213007067 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		Int32U5BU5D_t1809983122* L_1 = (Int32U5BU5D_t1809983122*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		Int32U5BU5D_t1809983122* L_4 = (Int32U5BU5D_t1809983122*)__this->get_keys_4();
		int32_t L_5 = ___second_index1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Int32>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))), (int32_t)((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))));
		V_0 = (int32_t)L_7;
		int32_t L_8 = V_0;
		if (L_8)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t70478024 * L_9 = (SortContext_1_t70478024 *)((SortContext_1_t70478024 *)__this)->get_child_context_1();
		if (!L_9)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t70478024 * L_10 = (SortContext_1_t70478024 *)((SortContext_1_t70478024 *)__this)->get_child_context_1();
		int32_t L_11 = ___first_index0;
		int32_t L_12 = ___second_index1;
		NullCheck((SortContext_1_t70478024 *)L_10);
		int32_t L_13 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t70478024 *)L_10, (int32_t)L_11, (int32_t)L_12);
		return L_13;
	}

IL_0043:
	{
		int32_t L_14 = (int32_t)((SortContext_1_t70478024 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_15 = ___second_index1;
		int32_t L_16 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_15-(int32_t)L_16));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_17 = ___first_index0;
		int32_t L_18 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_19 = (int32_t)((SortContext_1_t70478024 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_20 = V_0;
		G_B10_0 = ((-L_20));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_21 = V_0;
		G_B10_0 = L_21;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Object>::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortSequenceContext_2__ctor_m858446130_gshared (SortSequenceContext_2_t2202698700 * __this, Func_2_t2135783352 * ___selector0, Il2CppObject* ___comparer1, int32_t ___direction2, SortContext_1_t70478024 * ___child_context3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___direction2;
		SortContext_1_t70478024 * L_1 = ___child_context3;
		NullCheck((SortContext_1_t70478024 *)__this);
		((  void (*) (SortContext_1_t70478024 *, int32_t, SortContext_1_t70478024 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((SortContext_1_t70478024 *)__this, (int32_t)L_0, (SortContext_1_t70478024 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Func_2_t2135783352 * L_2 = ___selector0;
		__this->set_selector_2(L_2);
		Il2CppObject* L_3 = ___comparer1;
		__this->set_comparer_3(L_3);
		return;
	}
}
// System.Void System.Linq.SortSequenceContext`2<System.Object,System.Object>::Initialize(TElement[])
extern "C"  void SortSequenceContext_2_Initialize_m2547711222_gshared (SortSequenceContext_2_t2202698700 * __this, ObjectU5BU5D_t11523773* ___elements0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SortContext_1_t70478024 * L_0 = (SortContext_1_t70478024 *)((SortContext_1_t70478024 *)__this)->get_child_context_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		SortContext_1_t70478024 * L_1 = (SortContext_1_t70478024 *)((SortContext_1_t70478024 *)__this)->get_child_context_1();
		ObjectU5BU5D_t11523773* L_2 = ___elements0;
		NullCheck((SortContext_1_t70478024 *)L_1);
		VirtActionInvoker1< ObjectU5BU5D_t11523773* >::Invoke(4 /* System.Void System.Linq.SortContext`1<System.Object>::Initialize(TElement[]) */, (SortContext_1_t70478024 *)L_1, (ObjectU5BU5D_t11523773*)L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t11523773* L_3 = ___elements0;
		NullCheck(L_3);
		__this->set_keys_4(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_002c:
	{
		ObjectU5BU5D_t11523773* L_4 = (ObjectU5BU5D_t11523773*)__this->get_keys_4();
		int32_t L_5 = V_0;
		Func_2_t2135783352 * L_6 = (Func_2_t2135783352 *)__this->get_selector_2();
		ObjectU5BU5D_t11523773* L_7 = ___elements0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck((Func_2_t2135783352 *)L_6);
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t2135783352 *)L_6, (Il2CppObject *)((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_10);
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_0;
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)__this->get_keys_4();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Linq.SortSequenceContext`2<System.Object,System.Object>::Compare(System.Int32,System.Int32)
extern "C"  int32_t SortSequenceContext_2_Compare_m594910141_gshared (SortSequenceContext_2_t2202698700 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_comparer_3();
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_keys_4();
		int32_t L_2 = ___first_index0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		ObjectU5BU5D_t11523773* L_4 = (ObjectU5BU5D_t11523773*)__this->get_keys_4();
		int32_t L_5 = ___second_index1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))), (Il2CppObject *)((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))));
		V_0 = (int32_t)L_7;
		int32_t L_8 = V_0;
		if (L_8)
		{
			goto IL_005b;
		}
	}
	{
		SortContext_1_t70478024 * L_9 = (SortContext_1_t70478024 *)((SortContext_1_t70478024 *)__this)->get_child_context_1();
		if (!L_9)
		{
			goto IL_0043;
		}
	}
	{
		SortContext_1_t70478024 * L_10 = (SortContext_1_t70478024 *)((SortContext_1_t70478024 *)__this)->get_child_context_1();
		int32_t L_11 = ___first_index0;
		int32_t L_12 = ___second_index1;
		NullCheck((SortContext_1_t70478024 *)L_10);
		int32_t L_13 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Linq.SortContext`1<System.Object>::Compare(System.Int32,System.Int32) */, (SortContext_1_t70478024 *)L_10, (int32_t)L_11, (int32_t)L_12);
		return L_13;
	}

IL_0043:
	{
		int32_t L_14 = (int32_t)((SortContext_1_t70478024 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_15 = ___second_index1;
		int32_t L_16 = ___first_index0;
		G_B6_0 = ((int32_t)((int32_t)L_15-(int32_t)L_16));
		goto IL_005a;
	}

IL_0057:
	{
		int32_t L_17 = ___first_index0;
		int32_t L_18 = ___second_index1;
		G_B6_0 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
	}

IL_005a:
	{
		V_0 = (int32_t)G_B6_0;
	}

IL_005b:
	{
		int32_t L_19 = (int32_t)((SortContext_1_t70478024 *)__this)->get_direction_0();
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_20 = V_0;
		G_B10_0 = ((-L_20));
		goto IL_006f;
	}

IL_006e:
	{
		int32_t L_21 = V_0;
		G_B10_0 = L_21;
	}

IL_006f:
	{
		return G_B10_0;
	}
}
// System.Void System.Nullable`1<DG.Tweening.Ease>::.ctor(T)
extern "C"  void Nullable_1__ctor_m651599875_gshared (Nullable_1_t3910333848 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1535583244_gshared (Nullable_1_t3910333848 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<DG.Tweening.Ease>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1510196056_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m1510196056_gshared (Nullable_1_t3910333848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1510196056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1141050350_gshared (Nullable_1_t3910333848 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3910333848 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3910333848 *, Nullable_1_t3910333848 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3910333848 *)__this, (Nullable_1_t3910333848 )((*(Nullable_1_t3910333848 *)((Nullable_1_t3910333848 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1449385873_gshared (Nullable_1_t3910333848 * __this, Nullable_1_t3910333848  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<DG.Tweening.Ease>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1804731974_gshared (Nullable_1_t3910333848 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<DG.Tweening.Ease>::GetValueOrDefault()
extern Il2CppClass* Ease_t1024295940_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3338276923_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3338276923_gshared (Nullable_1_t3910333848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3338276923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Ease_t1024295940_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<DG.Tweening.Ease>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3552767570_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3552767570_gshared (Nullable_1_t3910333848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3552767570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<DG.Tweening.Ease>::op_Implicit(T)
extern "C"  Nullable_1_t3910333848  Nullable_1_op_Implicit_m2525359692_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t3910333848  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3910333848 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<DG.Tweening.LogBehaviour>::.ctor(T)
extern "C"  void Nullable_1__ctor_m712840334_gshared (Nullable_1_t1772024707 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1182346881_gshared (Nullable_1_t1772024707 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<DG.Tweening.LogBehaviour>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1876998915_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m1876998915_gshared (Nullable_1_t1772024707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1876998915_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3524161241_gshared (Nullable_1_t1772024707 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1772024707 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1772024707 *, Nullable_1_t1772024707 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1772024707 *)__this, (Nullable_1_t1772024707 )((*(Nullable_1_t1772024707 *)((Nullable_1_t1772024707 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2228907590_gshared (Nullable_1_t1772024707 * __this, Nullable_1_t1772024707  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<DG.Tweening.LogBehaviour>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3298904625_gshared (Nullable_1_t1772024707 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<DG.Tweening.LogBehaviour>::GetValueOrDefault()
extern Il2CppClass* LogBehaviour_t3180954095_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1211442406_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1211442406_gshared (Nullable_1_t1772024707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1211442406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (LogBehaviour_t3180954095_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<DG.Tweening.LogBehaviour>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3692740935_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3692740935_gshared (Nullable_1_t1772024707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3692740935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<DG.Tweening.LogBehaviour>::op_Implicit(T)
extern "C"  Nullable_1_t1772024707  Nullable_1_op_Implicit_m2239685057_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t1772024707  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1772024707 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<DG.Tweening.LoopType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4064011763_gshared (Nullable_1_t1935366568 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1727196284_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<DG.Tweening.LoopType>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3156116456_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3156116456_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3156116456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3402554110_gshared (Nullable_1_t1935366568 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1935366568 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1935366568 *, Nullable_1_t1935366568 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1935366568 *)__this, (Nullable_1_t1935366568 )((*(Nullable_1_t1935366568 *)((Nullable_1_t1935366568 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m915668609_gshared (Nullable_1_t1935366568 * __this, Nullable_1_t1935366568  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<DG.Tweening.LoopType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1359939670_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<DG.Tweening.LoopType>::GetValueOrDefault()
extern Il2CppClass* LoopType_t3344295956_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2011806923_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2011806923_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2011806923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (LoopType_t3344295956_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<DG.Tweening.LoopType>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3623309762_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3623309762_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3623309762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<DG.Tweening.LoopType>::op_Implicit(T)
extern "C"  Nullable_1_t1935366568  Nullable_1_op_Implicit_m3257158460_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t1935366568  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1935366568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3374458186_gshared (Nullable_1_t1724144949 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3219336471_gshared (Nullable_1_t1724144949 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3361538605_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3361538605_gshared (Nullable_1_t1724144949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3361538605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m176286997_gshared (Nullable_1_t1724144949 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1724144949 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1724144949 *, Nullable_1_t1724144949 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1724144949 *)__this, (Nullable_1_t1724144949 )((*(Nullable_1_t1724144949 *)((Nullable_1_t1724144949 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2716054666_gshared (Nullable_1_t1724144949 * __this, Nullable_1_t1724144949  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2802494829_gshared (Nullable_1_t1724144949 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::GetValueOrDefault()
extern Il2CppClass* FacebookUnityPlatform_t3133074337_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2532200098_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2532200098_gshared (Nullable_1_t1724144949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2532200098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (FacebookUnityPlatform_t3133074337_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3701328395_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3701328395_gshared (Nullable_1_t1724144949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3701328395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>::op_Implicit(T)
extern "C"  Nullable_1_t1724144949  Nullable_1_op_Implicit_m2309537029_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t1724144949  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1724144949 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<Facebook.Unity.OGActionType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3295722796_gshared (Nullable_1_t3534247419 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m145956701_gshared (Nullable_1_t3534247419 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<Facebook.Unity.OGActionType>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1374044019_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m1374044019_gshared (Nullable_1_t3534247419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1374044019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2584898305_gshared (Nullable_1_t3534247419 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3534247419 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3534247419 *, Nullable_1_t3534247419 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3534247419 *)__this, (Nullable_1_t3534247419 )((*(Nullable_1_t3534247419 *)((Nullable_1_t3534247419 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m651293982_gshared (Nullable_1_t3534247419 * __this, Nullable_1_t3534247419  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<Facebook.Unity.OGActionType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1431789285_gshared (Nullable_1_t3534247419 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<Facebook.Unity.OGActionType>::GetValueOrDefault()
extern Il2CppClass* OGActionType_t648209511_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2206545102_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2206545102_gshared (Nullable_1_t3534247419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2206545102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (OGActionType_t648209511_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<Facebook.Unity.OGActionType>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1428637112_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1428637112_gshared (Nullable_1_t3534247419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1428637112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<Facebook.Unity.OGActionType>::op_Implicit(T)
extern "C"  Nullable_1_t3534247419  Nullable_1_op_Implicit_m2848293889_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t3534247419  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3534247419 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.ClientModels.Currency>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2655699942_gshared (Nullable_1_t2703348181 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Currency>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2091484046_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.Currency>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3011379711_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3011379711_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3011379711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Currency>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m953058087_gshared (Nullable_1_t2703348181 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2703348181 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2703348181 *, Nullable_1_t2703348181 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2703348181 *)__this, (Nullable_1_t2703348181 )((*(Nullable_1_t2703348181 *)((Nullable_1_t2703348181 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Currency>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1144489528_gshared (Nullable_1_t2703348181 * __this, Nullable_1_t2703348181  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.ClientModels.Currency>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2705279883_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.Currency>::GetValueOrDefault()
extern Il2CppClass* Currency_t4112277569_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1336388442_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1336388442_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1336388442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Currency_t4112277569_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.ClientModels.Currency>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3935192347_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3935192347_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3935192347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.Currency>::op_Implicit(T)
extern "C"  Nullable_1_t2703348181  Nullable_1_op_Implicit_m4116364699_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t2703348181  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2703348181 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1412580726_gshared (Nullable_1_t2958521481 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m512914650_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2157806605_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2157806605_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2157806605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4093494363_gshared (Nullable_1_t2958521481 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2958521481 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2958521481 *, Nullable_1_t2958521481 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2958521481 *)__this, (Nullable_1_t2958521481 )((*(Nullable_1_t2958521481 *)((Nullable_1_t2958521481 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2890547844_gshared (Nullable_1_t2958521481 * __this, Nullable_1_t2958521481  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1302145715_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::GetValueOrDefault()
extern Il2CppClass* MatchmakeStatus_t72483573_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1776500584_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1776500584_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1776500584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (MatchmakeStatus_t72483573_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2740961925_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2740961925_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2740961925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::op_Implicit(T)
extern "C"  Nullable_1_t2958521481  Nullable_1_op_Implicit_m3043354751_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t2958521481  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2958521481 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.ClientModels.Region>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4044488867_gshared (Nullable_1_t212373688 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Region>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m456742667_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.Region>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3554026594_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3554026594_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3554026594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Region>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1011759370_gshared (Nullable_1_t212373688 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t212373688 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t212373688 *, Nullable_1_t212373688 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t212373688 *)__this, (Nullable_1_t212373688 )((*(Nullable_1_t212373688 *)((Nullable_1_t212373688 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Region>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1966930677_gshared (Nullable_1_t212373688 * __this, Nullable_1_t212373688  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.ClientModels.Region>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1258440174_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.Region>::GetValueOrDefault()
extern Il2CppClass* Region_t1621303076_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m87341757_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m87341757_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m87341757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Region_t1621303076_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.ClientModels.Region>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m4290986840_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m4290986840_gshared (Nullable_1_t212373688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m4290986840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.Region>::op_Implicit(T)
extern "C"  Nullable_1_t212373688  Nullable_1_op_Implicit_m1769438744_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t212373688  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t212373688 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1462692091_gshared (Nullable_1_t2484541604 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2222856735_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1896401704_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m1896401704_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1896401704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1095735158_gshared (Nullable_1_t2484541604 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2484541604 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2484541604 *, Nullable_1_t2484541604 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2484541604 *)__this, (Nullable_1_t2484541604 )((*(Nullable_1_t2484541604 *)((Nullable_1_t2484541604 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2123680521_gshared (Nullable_1_t2484541604 * __this, Nullable_1_t2484541604  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1168911694_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::GetValueOrDefault()
extern Il2CppClass* TitleActivationStatus_t3893470992_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1565473667_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1565473667_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1565473667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TitleActivationStatus_t3893470992_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m926414602_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m926414602_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m926414602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::op_Implicit(T)
extern "C"  Nullable_1_t2484541604  Nullable_1_op_Implicit_m2734339268_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t2484541604  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2484541604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.ClientModels.TradeStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3337663077_gshared (Nullable_1_t1403402234 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TradeStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3901768073_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.TradeStatus>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1520284734_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m1520284734_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1520284734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TradeStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3591382220_gshared (Nullable_1_t1403402234 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1403402234 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1403402234 *, Nullable_1_t1403402234 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1403402234 *)__this, (Nullable_1_t1403402234 )((*(Nullable_1_t1403402234 *)((Nullable_1_t1403402234 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TradeStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m4247826675_gshared (Nullable_1_t1403402234 * __this, Nullable_1_t1403402234  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.ClientModels.TradeStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4184369444_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.TradeStatus>::GetValueOrDefault()
extern Il2CppClass* TradeStatus_t2812331622_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2043457177_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2043457177_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2043457177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TradeStatus_t2812331622_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.ClientModels.TradeStatus>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2702235188_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2702235188_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2702235188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.TradeStatus>::op_Implicit(T)
extern "C"  Nullable_1_t1403402234  Nullable_1_op_Implicit_m962681646_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t1403402234  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1403402234 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4007637675_gshared (Nullable_1_t4258606580 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3137646287_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2900966136_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2900966136_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2900966136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1623809222_gshared (Nullable_1_t4258606580 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t4258606580 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t4258606580 *, Nullable_1_t4258606580 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t4258606580 *)__this, (Nullable_1_t4258606580 )((*(Nullable_1_t4258606580 *)((Nullable_1_t4258606580 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1927375801_gshared (Nullable_1_t4258606580 * __this, Nullable_1_t4258606580  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2127257502_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::GetValueOrDefault()
extern Il2CppClass* TransactionStatus_t1372568672_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3706379091_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3706379091_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3706379091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TransactionStatus_t1372568672_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m4213201722_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m4213201722_gshared (Nullable_1_t4258606580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m4213201722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.TransactionStatus>::op_Implicit(T)
extern "C"  Nullable_1_t4258606580  Nullable_1_op_Implicit_m2888462196_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t4258606580  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t4258606580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4243540691_gshared (Nullable_1_t2611574664 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3483961915_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m4106592946_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m4106592946_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4106592946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1771540442_gshared (Nullable_1_t2611574664 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2611574664 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2611574664 *, Nullable_1_t2611574664 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2611574664 *)__this, (Nullable_1_t2611574664 )((*(Nullable_1_t2611574664 *)((Nullable_1_t2611574664 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2145081893_gshared (Nullable_1_t2611574664 * __this, Nullable_1_t2611574664  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2904351678_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::GetValueOrDefault()
extern Il2CppClass* UserDataPermission_t4020504052_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3408455949_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3408455949_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3408455949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UserDataPermission_t4020504052_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3674318088_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3674318088_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3674318088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::op_Implicit(T)
extern "C"  Nullable_1_t2611574664  Nullable_1_op_Implicit_m3511051080_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t2611574664  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2611574664 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.ClientModels.UserOrigination>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1964700759_gshared (Nullable_1_t417810504 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserOrigination>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3106446002_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.UserOrigination>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2138108530_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2138108530_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2138108530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserOrigination>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3473804058_gshared (Nullable_1_t417810504 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t417810504 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t417810504 *, Nullable_1_t417810504 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t417810504 *)__this, (Nullable_1_t417810504 )((*(Nullable_1_t417810504 *)((Nullable_1_t417810504 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserOrigination>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1384101605_gshared (Nullable_1_t417810504 * __this, Nullable_1_t417810504  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.ClientModels.UserOrigination>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3907231858_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.ClientModels.UserOrigination>::GetValueOrDefault()
extern Il2CppClass* UserOrigination_t1826739892_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2844010983_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2844010983_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2844010983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UserOrigination_t1826739892_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.ClientModels.UserOrigination>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m752368934_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m752368934_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m752368934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.UserOrigination>::op_Implicit(T)
extern "C"  Nullable_1_t417810504  Nullable_1_op_Implicit_m2196646560_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t417810504  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t417810504 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3810241773_gshared (Nullable_1_t276009914 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1844179796_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m4040595498_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m4040595498_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4040595498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3973453880_gshared (Nullable_1_t276009914 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t276009914 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t276009914 *, Nullable_1_t276009914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t276009914 *)__this, (Nullable_1_t276009914 )((*(Nullable_1_t276009914 *)((Nullable_1_t276009914 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1063406599_gshared (Nullable_1_t276009914 * __this, Nullable_1_t276009914  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2481040272_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::GetValueOrDefault()
extern Il2CppClass* testRegion_t1684939302_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3631035755_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3631035755_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3631035755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (testRegion_t1684939302_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m30750408_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m30750408_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m30750408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::op_Implicit(T)
extern "C"  Nullable_1_t276009914  Nullable_1_op_Implicit_m934380354_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t276009914  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t276009914 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<SponsorPay.SPUserConnection>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2083074639_gshared (Nullable_1_t2936825364 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserConnection>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1427609527_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<SponsorPay.SPUserConnection>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m4092822326_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m4092822326_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4092822326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserConnection>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3811712734_gshared (Nullable_1_t2936825364 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2936825364 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2936825364 *, Nullable_1_t2936825364 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2936825364 *)__this, (Nullable_1_t2936825364 )((*(Nullable_1_t2936825364 *)((Nullable_1_t2936825364 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserConnection>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1284939425_gshared (Nullable_1_t2936825364 * __this, Nullable_1_t2936825364  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<SponsorPay.SPUserConnection>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3645063106_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<SponsorPay.SPUserConnection>::GetValueOrDefault()
extern Il2CppClass* SPUserConnection_t50787456_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2713716113_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2713716113_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2713716113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SPUserConnection_t50787456_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<SponsorPay.SPUserConnection>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m567589380_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m567589380_gshared (Nullable_1_t2936825364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m567589380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserConnection>::op_Implicit(T)
extern "C"  Nullable_1_t2936825364  Nullable_1_op_Implicit_m2740305604_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t2936825364  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2936825364 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<SponsorPay.SPUserEducation>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1980471517_gshared (Nullable_1_t4294542862 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserEducation>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m12063873_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<SponsorPay.SPUserEducation>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2234930694_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2234930694_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2234930694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserEducation>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3975908052_gshared (Nullable_1_t4294542862 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t4294542862 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t4294542862 *, Nullable_1_t4294542862 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t4294542862 *)__this, (Nullable_1_t4294542862 )((*(Nullable_1_t4294542862 *)((Nullable_1_t4294542862 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserEducation>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1322250731_gshared (Nullable_1_t4294542862 * __this, Nullable_1_t4294542862  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<SponsorPay.SPUserEducation>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1359729324_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<SponsorPay.SPUserEducation>::GetValueOrDefault()
extern Il2CppClass* SPUserEducation_t1408504954_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1806370913_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1806370913_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1806370913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SPUserEducation_t1408504954_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<SponsorPay.SPUserEducation>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3206598764_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3206598764_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3206598764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserEducation>::op_Implicit(T)
extern "C"  Nullable_1_t4294542862  Nullable_1_op_Implicit_m1189824294_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t4294542862  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t4294542862 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<SponsorPay.SPUserEthnicity>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3737378726_gshared (Nullable_1_t3783893733 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserEthnicity>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2087215242_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<SponsorPay.SPUserEthnicity>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3786124957_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3786124957_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3786124957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserEthnicity>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1804096171_gshared (Nullable_1_t3783893733 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3783893733 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3783893733 *, Nullable_1_t3783893733 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3783893733 *)__this, (Nullable_1_t3783893733 )((*(Nullable_1_t3783893733 *)((Nullable_1_t3783893733 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserEthnicity>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3804363316_gshared (Nullable_1_t3783893733 * __this, Nullable_1_t3783893733  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<SponsorPay.SPUserEthnicity>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1703764355_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<SponsorPay.SPUserEthnicity>::GetValueOrDefault()
extern Il2CppClass* SPUserEthnicity_t897855825_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2108021240_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2108021240_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2108021240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SPUserEthnicity_t897855825_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<SponsorPay.SPUserEthnicity>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3672279285_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3672279285_gshared (Nullable_1_t3783893733 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3672279285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserEthnicity>::op_Implicit(T)
extern "C"  Nullable_1_t3783893733  Nullable_1_op_Implicit_m3264975663_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t3783893733  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3783893733 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<SponsorPay.SPUserGender>::.ctor(T)
extern "C"  void Nullable_1__ctor_m318086284_gshared (Nullable_1_t2074414583 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserGender>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m863809844_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<SponsorPay.SPUserGender>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2727128153_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2727128153_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2727128153_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserGender>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2944519873_gshared (Nullable_1_t2074414583 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2074414583 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2074414583 *, Nullable_1_t2074414583 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2074414583 *)__this, (Nullable_1_t2074414583 )((*(Nullable_1_t2074414583 *)((Nullable_1_t2074414583 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserGender>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3820935518_gshared (Nullable_1_t2074414583 * __this, Nullable_1_t2074414583  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<SponsorPay.SPUserGender>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m250414501_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<SponsorPay.SPUserGender>::GetValueOrDefault()
extern Il2CppClass* SPUserGender_t3483343971_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3799735220_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3799735220_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3799735220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SPUserGender_t3483343971_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<SponsorPay.SPUserGender>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m496505473_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m496505473_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m496505473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserGender>::op_Implicit(T)
extern "C"  Nullable_1_t2074414583  Nullable_1_op_Implicit_m2460643905_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t2074414583  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2074414583 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<SponsorPay.SPUserMaritalStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m309386687_gshared (Nullable_1_t2392475756 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserMaritalStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1851021539_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<SponsorPay.SPUserMaritalStatus>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2109224548_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m2109224548_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2109224548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserMaritalStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3422417970_gshared (Nullable_1_t2392475756 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2392475756 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2392475756 *, Nullable_1_t2392475756 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2392475756 *)__this, (Nullable_1_t2392475756 )((*(Nullable_1_t2392475756 *)((Nullable_1_t2392475756 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserMaritalStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1772348109_gshared (Nullable_1_t2392475756 * __this, Nullable_1_t2392475756  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<SponsorPay.SPUserMaritalStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m341695754_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<SponsorPay.SPUserMaritalStatus>::GetValueOrDefault()
extern Il2CppClass* SPUserMaritalStatus_t3801405144_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2775427775_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2775427775_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2775427775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SPUserMaritalStatus_t3801405144_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<SponsorPay.SPUserMaritalStatus>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m4163853902_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m4163853902_gshared (Nullable_1_t2392475756 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m4163853902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserMaritalStatus>::op_Implicit(T)
extern "C"  Nullable_1_t2392475756  Nullable_1_op_Implicit_m3654641032_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t2392475756  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2392475756 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<SponsorPay.SPUserSexualOrientation>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2484952975_gshared (Nullable_1_t3787560604 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserSexualOrientation>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2913300403_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<SponsorPay.SPUserSexualOrientation>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3567078164_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m3567078164_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3567078164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserSexualOrientation>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2690421602_gshared (Nullable_1_t3787560604 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3787560604 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3787560604 *, Nullable_1_t3787560604 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3787560604 *)__this, (Nullable_1_t3787560604 )((*(Nullable_1_t3787560604 *)((Nullable_1_t3787560604 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<SponsorPay.SPUserSexualOrientation>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3835481501_gshared (Nullable_1_t3787560604 * __this, Nullable_1_t3787560604  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		NullCheck((ValueType_t4014882752 *)L_7);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t4014882752 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
// System.Int32 System.Nullable`1<SponsorPay.SPUserSexualOrientation>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4002844474_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}
}
// T System.Nullable`1<SponsorPay.SPUserSexualOrientation>::GetValueOrDefault()
extern Il2CppClass* SPUserSexualOrientation_t901522696_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2399866735_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2399866735_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2399866735_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SPUserSexualOrientation_t901522696_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<SponsorPay.SPUserSexualOrientation>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3130058142_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3130058142_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3130058142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		NullCheck((ValueType_t4014882752 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t4014882752 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserSexualOrientation>::op_Implicit(T)
extern "C"  Nullable_1_t3787560604  Nullable_1_op_Implicit_m3489093208_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t3787560604  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3787560604 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3590787090_gshared (Nullable_1_t3097043249 * __this, bool ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		bool L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3643594237_gshared (Nullable_1_t3097043249 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.Boolean>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m4285994375_MetadataUsageId;
extern "C"  bool Nullable_1_get_Value_m4285994375_gshared (Nullable_1_t3097043249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4285994375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1858237981_gshared (Nullable_1_t3097043249 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3097043249 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3097043249 *, Nullable_1_t3097043249 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3097043249 *)__this, (Nullable_1_t3097043249 )((*(Nullable_1_t3097043249 *)((Nullable_1_t3097043249 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3048239746_gshared (Nullable_1_t3097043249 * __this, Nullable_1_t3097043249  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		bool* L_3 = (bool*)(&___other0)->get_address_of_value_0();
		bool L_4 = (bool)__this->get_value_0();
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Boolean_Equals_m1178456600((bool*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2135813621_gshared (Nullable_1_t3097043249 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		int32_t L_2 = Boolean_GetHashCode_m841540860((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.Boolean>::GetValueOrDefault()
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1249199466_MetadataUsageId;
extern "C"  bool Nullable_1_GetValueOrDefault_m1249199466_gshared (Nullable_1_t3097043249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1249199466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool G_B3_0 = false;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.Boolean>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3292806979_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3292806979_gshared (Nullable_1_t3097043249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3292806979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		String_t* L_2 = Boolean_ToString_m2512358154((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.Boolean>::op_Implicit(T)
extern "C"  Nullable_1_t3097043249  Nullable_1_op_Implicit_m4097113597_gshared (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		Nullable_1_t3097043249  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3097043249 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.Byte>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3047404298_gshared (Nullable_1_t1369764433 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint8_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.Byte>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m310053951_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.Byte>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1771599223_MetadataUsageId;
extern "C"  uint8_t Nullable_1_get_Value_m1771599223_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1771599223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint8_t L_2 = (uint8_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2428685791_gshared (Nullable_1_t1369764433 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1369764433 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1369764433 *, Nullable_1_t1369764433 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1369764433 *)__this, (Nullable_1_t1369764433 )((*(Nullable_1_t1369764433 *)((Nullable_1_t1369764433 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2459188352_gshared (Nullable_1_t1369764433 * __this, Nullable_1_t1369764433  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint8_t* L_3 = (uint8_t*)(&___other0)->get_address_of_value_0();
		uint8_t L_4 = (uint8_t)__this->get_value_0();
		uint8_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Byte_Equals_m4077775008((uint8_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.Byte>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1455590339_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint8_t* L_1 = (uint8_t*)__this->get_address_of_value_0();
		int32_t L_2 = Byte_GetHashCode_m2610389752((uint8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.Byte>::GetValueOrDefault()
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2207863084_MetadataUsageId;
extern "C"  uint8_t Nullable_1_GetValueOrDefault_m2207863084_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2207863084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	uint8_t G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint8_t L_1 = (uint8_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Byte_t2778693821_il2cpp_TypeInfo_var, (&V_0));
		uint8_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.Byte>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1479328675_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1479328675_gshared (Nullable_1_t1369764433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1479328675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint8_t* L_1 = (uint8_t*)__this->get_address_of_value_0();
		String_t* L_2 = Byte_ToString_m961894880((uint8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.Byte>::op_Implicit(T)
extern "C"  Nullable_1_t1369764433  Nullable_1_op_Implicit_m178797308_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___value0, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value0;
		Nullable_1_t1369764433  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1369764433 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (uint8_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1725216541_gshared (Nullable_1_t3225071844 * __this, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		DateTime_t339033936  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3040846348_gshared (Nullable_1_t3225071844 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.DateTime>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3645705252_MetadataUsageId;
extern "C"  DateTime_t339033936  Nullable_1_get_Value_m3645705252_gshared (Nullable_1_t3225071844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3645705252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DateTime_t339033936  L_2 = (DateTime_t339033936 )__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m724703986_gshared (Nullable_1_t3225071844 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3225071844 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3225071844 *, Nullable_1_t3225071844 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3225071844 *)__this, (Nullable_1_t3225071844 )((*(Nullable_1_t3225071844 *)((Nullable_1_t3225071844 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2642060301_gshared (Nullable_1_t3225071844 * __this, Nullable_1_t3225071844  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		DateTime_t339033936 * L_3 = (DateTime_t339033936 *)(&___other0)->get_address_of_value_0();
		DateTime_t339033936  L_4 = (DateTime_t339033936 )__this->get_value_0();
		DateTime_t339033936  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = DateTime_Equals_m13666989((DateTime_t339033936 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4112160982_gshared (Nullable_1_t3225071844 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		DateTime_t339033936 * L_1 = (DateTime_t339033936 *)__this->get_address_of_value_0();
		int32_t L_2 = DateTime_GetHashCode_m2255586565((DateTime_t339033936 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.DateTime>::GetValueOrDefault()
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1694192421_MetadataUsageId;
extern "C"  DateTime_t339033936  Nullable_1_GetValueOrDefault_m1694192421_gshared (Nullable_1_t3225071844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1694192421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTime_t339033936  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTime_t339033936  L_1 = (DateTime_t339033936 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (DateTime_t339033936_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t339033936  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.DateTime>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3130403824_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3130403824_gshared (Nullable_1_t3225071844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3130403824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DateTime_t339033936 * L_1 = (DateTime_t339033936 *)__this->get_address_of_value_0();
		String_t* L_2 = DateTime_ToString_m3221907059((DateTime_t339033936 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.DateTime>::op_Implicit(T)
extern "C"  Nullable_1_t3225071844  Nullable_1_op_Implicit_m1479425840_gshared (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ___value0;
		Nullable_1_t3225071844  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3225071844 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (DateTime_t339033936 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.DateTimeOffset>::.ctor(T)
extern "C"  void Nullable_1__ctor_m522950596_gshared (Nullable_1_t2303330647 * __this, DateTimeOffset_t3712260035  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		DateTimeOffset_t3712260035  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2074669485_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.DateTimeOffset>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m4125167011_MetadataUsageId;
extern "C"  DateTimeOffset_t3712260035  Nullable_1_get_Value_m4125167011_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4125167011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		DateTimeOffset_t3712260035  L_2 = (DateTimeOffset_t3712260035 )__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1572111781_gshared (Nullable_1_t2303330647 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2303330647 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2303330647 *, Nullable_1_t2303330647 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2303330647 *)__this, (Nullable_1_t2303330647 )((*(Nullable_1_t2303330647 *)((Nullable_1_t2303330647 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3552532986_gshared (Nullable_1_t2303330647 * __this, Nullable_1_t2303330647  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		DateTimeOffset_t3712260035 * L_3 = (DateTimeOffset_t3712260035 *)(&___other0)->get_address_of_value_0();
		DateTimeOffset_t3712260035  L_4 = (DateTimeOffset_t3712260035 )__this->get_value_0();
		DateTimeOffset_t3712260035  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = DateTimeOffset_Equals_m1431331290((DateTimeOffset_t3712260035 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.DateTimeOffset>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2941497865_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		DateTimeOffset_t3712260035 * L_1 = (DateTimeOffset_t3712260035 *)__this->get_address_of_value_0();
		int32_t L_2 = DateTimeOffset_GetHashCode_m1972583858((DateTimeOffset_t3712260035 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.DateTimeOffset>::GetValueOrDefault()
extern Il2CppClass* DateTimeOffset_t3712260035_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m959858776_MetadataUsageId;
extern "C"  DateTimeOffset_t3712260035  Nullable_1_GetValueOrDefault_m959858776_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m959858776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTimeOffset_t3712260035  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTimeOffset_t3712260035  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		DateTimeOffset_t3712260035  L_1 = (DateTimeOffset_t3712260035 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (DateTimeOffset_t3712260035_il2cpp_TypeInfo_var, (&V_0));
		DateTimeOffset_t3712260035  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.DateTimeOffset>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2837501533_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2837501533_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2837501533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DateTimeOffset_t3712260035 * L_1 = (DateTimeOffset_t3712260035 *)__this->get_address_of_value_0();
		String_t* L_2 = DateTimeOffset_ToString_m983707174((DateTimeOffset_t3712260035 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.DateTimeOffset>::op_Implicit(T)
extern "C"  Nullable_1_t2303330647  Nullable_1_op_Implicit_m1865326813_gshared (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___value0, const MethodInfo* method)
{
	{
		DateTimeOffset_t3712260035  L_0 = ___value0;
		Nullable_1_t2303330647  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2303330647 *, DateTimeOffset_t3712260035 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (DateTimeOffset_t3712260035 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.Double>::.ctor(T)
extern "C"  void Nullable_1__ctor_m800965971_gshared (Nullable_1_t3420554522 * __this, double ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		double L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.Double>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3299336854_gshared (Nullable_1_t3420554522 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.Double>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1042825562_MetadataUsageId;
extern "C"  double Nullable_1_get_Value_m1042825562_gshared (Nullable_1_t3420554522 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1042825562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		double L_2 = (double)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.Double>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1100904808_gshared (Nullable_1_t3420554522 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3420554522 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3420554522 *, Nullable_1_t3420554522 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3420554522 *)__this, (Nullable_1_t3420554522 )((*(Nullable_1_t3420554522 *)((Nullable_1_t3420554522 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.Double>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1171469527_gshared (Nullable_1_t3420554522 * __this, Nullable_1_t3420554522  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		double* L_3 = (double*)(&___other0)->get_address_of_value_0();
		double L_4 = (double)__this->get_value_0();
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Double_Equals_m1597124279((double*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.Double>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1539710668_gshared (Nullable_1_t3420554522 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		double* L_1 = (double*)__this->get_address_of_value_0();
		int32_t L_2 = Double_GetHashCode_m2106126735((double*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.Double>::GetValueOrDefault()
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m49374171_MetadataUsageId;
extern "C"  double Nullable_1_GetValueOrDefault_m49374171_gshared (Nullable_1_t3420554522 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m49374171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double G_B3_0 = 0.0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		double L_1 = (double)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.Double>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2509934458_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2509934458_gshared (Nullable_1_t3420554522 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2509934458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		double* L_1 = (double*)__this->get_address_of_value_0();
		String_t* L_2 = Double_ToString_m3380246633((double*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.Double>::op_Implicit(T)
extern "C"  Nullable_1_t3420554522  Nullable_1_op_Implicit_m3196012154_gshared (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		Nullable_1_t3420554522  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3420554522 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (double)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.Int16>::.ctor(T)
extern "C"  void Nullable_1__ctor_m782064958_gshared (Nullable_1_t1438485341 * __this, int16_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int16_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.Int16>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3682169251_gshared (Nullable_1_t1438485341 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.Int16>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3565015099_MetadataUsageId;
extern "C"  int16_t Nullable_1_get_Value_m3565015099_gshared (Nullable_1_t1438485341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3565015099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int16_t L_2 = (int16_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.Int16>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3553249289_gshared (Nullable_1_t1438485341 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1438485341 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1438485341 *, Nullable_1_t1438485341 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1438485341 *)__this, (Nullable_1_t1438485341 )((*(Nullable_1_t1438485341 *)((Nullable_1_t1438485341 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.Int16>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2214649622_gshared (Nullable_1_t1438485341 * __this, Nullable_1_t1438485341  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int16_t* L_3 = (int16_t*)(&___other0)->get_address_of_value_0();
		int16_t L_4 = (int16_t)__this->get_value_0();
		int16_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int16_Equals_m3652534636((int16_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.Int16>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m988146273_gshared (Nullable_1_t1438485341 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int16_t* L_1 = (int16_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int16_GetHashCode_m2943154640((int16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.Int16>::GetValueOrDefault()
extern Il2CppClass* Int16_t2847414729_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m367173244_MetadataUsageId;
extern "C"  int16_t Nullable_1_GetValueOrDefault_m367173244_gshared (Nullable_1_t1438485341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m367173244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int16_t V_0 = 0;
	int16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int16_t L_1 = (int16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int16_t2847414729_il2cpp_TypeInfo_var, (&V_0));
		int16_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.Int16>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1779076247_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1779076247_gshared (Nullable_1_t1438485341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1779076247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int16_t* L_1 = (int16_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int16_ToString_m1124031606((int16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.Int16>::op_Implicit(T)
extern "C"  Nullable_1_t1438485341  Nullable_1_op_Implicit_m1905712430_gshared (Il2CppObject * __this /* static, unused */, int16_t ___value0, const MethodInfo* method)
{
	{
		int16_t L_0 = ___value0;
		Nullable_1_t1438485341  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1438485341 *, int16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int16_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.Int32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m944559736_gshared (Nullable_1_t1438485399 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1686547625_gshared (Nullable_1_t1438485399 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.Int32>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m844974555_MetadataUsageId;
extern "C"  int32_t Nullable_1_get_Value_m844974555_gshared (Nullable_1_t1438485399 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m844974555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3334191683_gshared (Nullable_1_t1438485399 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1438485399 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1438485399 *, Nullable_1_t1438485399 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1438485399 *)__this, (Nullable_1_t1438485399 )((*(Nullable_1_t1438485399 *)((Nullable_1_t1438485399 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.Int32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1592743836_gshared (Nullable_1_t1438485399 * __this, Nullable_1_t1438485399  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int32_Equals_m4061110258((int32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.Int32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2170697371_gshared (Nullable_1_t1438485399 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int32_GetHashCode_m3396943446((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.Int32>::GetValueOrDefault()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m3844036406_MetadataUsageId;
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3844036406_gshared (Nullable_1_t1438485399 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3844036406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.Int32>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2521447069_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2521447069_gshared (Nullable_1_t1438485399 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2521447069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int32_ToString_m1286526384((int32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.Int32>::op_Implicit(T)
extern "C"  Nullable_1_t1438485399  Nullable_1_op_Implicit_m1294609303_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		Nullable_1_t1438485399  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1438485399 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.Int64>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3136045143_gshared (Nullable_1_t1438485494 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int64_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.Int64>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1453954602_gshared (Nullable_1_t1438485494 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.Int64>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2328475642_MetadataUsageId;
extern "C"  int64_t Nullable_1_get_Value_m2328475642_gshared (Nullable_1_t1438485494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2328475642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int64_t L_2 = (int64_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.Int64>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3567799714_gshared (Nullable_1_t1438485494 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1438485494 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1438485494 *, Nullable_1_t1438485494 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1438485494 *)__this, (Nullable_1_t1438485494 )((*(Nullable_1_t1438485494 *)((Nullable_1_t1438485494 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.Int64>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3906407261_gshared (Nullable_1_t1438485494 * __this, Nullable_1_t1438485494  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int64_t* L_3 = (int64_t*)(&___other0)->get_address_of_value_0();
		int64_t L_4 = (int64_t)__this->get_value_0();
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Int64_Equals_m1990436019((int64_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.Int64>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1886099706_gshared (Nullable_1_t1438485494 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_0();
		int32_t L_2 = Int64_GetHashCode_m2140836887((int64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.Int64>::GetValueOrDefault()
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1837577813_MetadataUsageId;
extern "C"  int64_t Nullable_1_GetValueOrDefault_m1837577813_gshared (Nullable_1_t1438485494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1837577813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	int64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int64_t L_1 = (int64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.Int64>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1738017950_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1738017950_gshared (Nullable_1_t1438485494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1738017950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_0();
		String_t* L_2 = Int64_ToString_m3478011791((int64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.Int64>::op_Implicit(T)
extern "C"  Nullable_1_t1438485494  Nullable_1_op_Implicit_m1062016280_gshared (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		Nullable_1_t1438485494  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1438485494 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int64_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.SByte>::.ctor(T)
extern "C"  void Nullable_1__ctor_m949022853_gshared (Nullable_1_t1446416676 * __this, int8_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int8_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.SByte>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1660015292_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.SByte>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m802794946_MetadataUsageId;
extern "C"  int8_t Nullable_1_get_Value_m802794946_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m802794946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int8_t L_2 = (int8_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.SByte>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3964676304_gshared (Nullable_1_t1446416676 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1446416676 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t1446416676 *, Nullable_1_t1446416676 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t1446416676 *)__this, (Nullable_1_t1446416676 )((*(Nullable_1_t1446416676 *)((Nullable_1_t1446416676 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.SByte>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2200494191_gshared (Nullable_1_t1446416676 * __this, Nullable_1_t1446416676  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int8_t* L_3 = (int8_t*)(&___other0)->get_address_of_value_0();
		int8_t L_4 = (int8_t)__this->get_value_0();
		int8_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = SByte_Equals_m1310501829((int8_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.SByte>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m784368168_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int8_t* L_1 = (int8_t*)__this->get_address_of_value_0();
		int32_t L_2 = SByte_GetHashCode_m3213675817((int8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.SByte>::GetValueOrDefault()
extern Il2CppClass* SByte_t2855346064_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m707265795_MetadataUsageId;
extern "C"  int8_t Nullable_1_GetValueOrDefault_m707265795_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m707265795_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	int8_t G_B3_0 = 0x0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int8_t L_1 = (int8_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (SByte_t2855346064_il2cpp_TypeInfo_var, (&V_0));
		int8_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.SByte>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2659803696_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2659803696_gshared (Nullable_1_t1446416676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2659803696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int8_t* L_1 = (int8_t*)__this->get_address_of_value_0();
		String_t* L_2 = SByte_ToString_m1290989501((int8_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.SByte>::op_Implicit(T)
extern "C"  Nullable_1_t1446416676  Nullable_1_op_Implicit_m3643449141_gshared (Il2CppObject * __this /* static, unused */, int8_t ___value0, const MethodInfo* method)
{
	{
		int8_t L_0 = ___value0;
		Nullable_1_t1446416676  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t1446416676 *, int8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (int8_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.Single>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1721422666_gshared (Nullable_1_t3844246929 * __this, float ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		float L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m465802129_gshared (Nullable_1_t3844246929 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.Single>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1960755007_MetadataUsageId;
extern "C"  float Nullable_1_get_Value_m1960755007_gshared (Nullable_1_t3844246929 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1960755007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		float L_2 = (float)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3703262431_gshared (Nullable_1_t3844246929 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3844246929 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3844246929 *, Nullable_1_t3844246929 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3844246929 *)__this, (Nullable_1_t3844246929 )((*(Nullable_1_t3844246929 *)((Nullable_1_t3844246929 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m350150016_gshared (Nullable_1_t3844246929 * __this, Nullable_1_t3844246929  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		float* L_3 = (float*)(&___other0)->get_address_of_value_0();
		float L_4 = (float)__this->get_value_0();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Single_Equals_m2650902624((float*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.Single>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2699909443_gshared (Nullable_1_t3844246929 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		int32_t L_2 = Single_GetHashCode_m65342520((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.Single>::GetValueOrDefault()
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m1330293010_MetadataUsageId;
extern "C"  float Nullable_1_GetValueOrDefault_m1330293010_gshared (Nullable_1_t3844246929 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1330293010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = (float)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.Single>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m979320931_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m979320931_gshared (Nullable_1_t3844246929 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m979320931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		String_t* L_2 = Single_ToString_m5736032((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.Single>::op_Implicit(T)
extern "C"  Nullable_1_t3844246929  Nullable_1_op_Implicit_m507468515_gshared (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		Nullable_1_t3844246929  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3844246929 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4008503583_gshared (Nullable_1_t3649900800 * __this, TimeSpan_t763862892  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t763862892  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2797118855_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3338249190_MetadataUsageId;
extern "C"  TimeSpan_t763862892  Nullable_1_get_Value_m3338249190_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3338249190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t763862892  L_2 = (TimeSpan_t763862892 )__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2158814990_gshared (Nullable_1_t3649900800 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3649900800 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3649900800 *, Nullable_1_t3649900800 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3649900800 *)__this, (Nullable_1_t3649900800 )((*(Nullable_1_t3649900800 *)((Nullable_1_t3649900800 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3609411697_gshared (Nullable_1_t3649900800 * __this, Nullable_1_t3649900800  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t763862892 * L_3 = (TimeSpan_t763862892 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t763862892  L_4 = (TimeSpan_t763862892 )__this->get_value_0();
		TimeSpan_t763862892  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m2969422609((TimeSpan_t763862892 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2957066482_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t763862892 * L_1 = (TimeSpan_t763862892 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m3188156777((TimeSpan_t763862892 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2343728705_MetadataUsageId;
extern "C"  TimeSpan_t763862892  Nullable_1_GetValueOrDefault_m2343728705_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2343728705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t763862892  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		TimeSpan_t763862892  L_1 = (TimeSpan_t763862892 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TimeSpan_t763862892_il2cpp_TypeInfo_var, (&V_0));
		TimeSpan_t763862892  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3059865940_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3059865940_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3059865940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t763862892 * L_1 = (TimeSpan_t763862892 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2803989647((TimeSpan_t763862892 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.TimeSpan>::op_Implicit(T)
extern "C"  Nullable_1_t3649900800  Nullable_1_op_Implicit_m31234708_gshared (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___value0, const MethodInfo* method)
{
	{
		TimeSpan_t763862892  L_0 = ___value0;
		Nullable_1_t3649900800  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3649900800 *, TimeSpan_t763862892 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (TimeSpan_t763862892 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.UInt16>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2457572193_gshared (Nullable_1_t3871963176 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint16_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.UInt16>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m775859528_gshared (Nullable_1_t3871963176 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.UInt16>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2779205134_MetadataUsageId;
extern "C"  uint16_t Nullable_1_get_Value_m2779205134_gshared (Nullable_1_t3871963176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2779205134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint16_t L_2 = (uint16_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.UInt16>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3946947190_gshared (Nullable_1_t3871963176 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3871963176 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3871963176 *, Nullable_1_t3871963176 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3871963176 *)__this, (Nullable_1_t3871963176 )((*(Nullable_1_t3871963176 *)((Nullable_1_t3871963176 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.UInt16>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3444596745_gshared (Nullable_1_t3871963176 * __this, Nullable_1_t3871963176  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint16_t* L_3 = (uint16_t*)(&___other0)->get_address_of_value_0();
		uint16_t L_4 = (uint16_t)__this->get_value_0();
		uint16_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt16_Equals_m857648105((uint16_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.UInt16>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1873950170_gshared (Nullable_1_t3871963176 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint16_t* L_1 = (uint16_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt16_GetHashCode_m592888001((uint16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.UInt16>::GetValueOrDefault()
extern Il2CppClass* UInt16_t985925268_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m583824451_MetadataUsageId;
extern "C"  uint16_t Nullable_1_GetValueOrDefault_m583824451_gshared (Nullable_1_t3871963176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m583824451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	uint16_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint16_t L_1 = (uint16_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt16_t985925268_il2cpp_TypeInfo_var, (&V_0));
		uint16_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.UInt16>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2325119788_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2325119788_gshared (Nullable_1_t3871963176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2325119788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint16_t* L_1 = (uint16_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt16_ToString_m741885559((uint16_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.UInt16>::op_Implicit(T)
extern "C"  Nullable_1_t3871963176  Nullable_1_op_Implicit_m1801723347_gshared (Il2CppObject * __this /* static, unused */, uint16_t ___value0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value0;
		Nullable_1_t3871963176  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3871963176 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (uint16_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.UInt32>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2620066971_gshared (Nullable_1_t3871963234 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.UInt32>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3075205198_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.UInt32>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m22896840_MetadataUsageId;
extern "C"  uint32_t Nullable_1_get_Value_m22896840_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m22896840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint32_t L_2 = (uint32_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.UInt32>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3727889584_gshared (Nullable_1_t3871963234 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3871963234 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3871963234 *, Nullable_1_t3871963234 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3871963234 *)__this, (Nullable_1_t3871963234 )((*(Nullable_1_t3871963234 *)((Nullable_1_t3871963234 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.UInt32>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2822690959_gshared (Nullable_1_t3871963234 * __this, Nullable_1_t3871963234  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint32_t* L_3 = (uint32_t*)(&___other0)->get_address_of_value_0();
		uint32_t L_4 = (uint32_t)__this->get_value_0();
		uint32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt32_Equals_m1266223727((uint32_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.UInt32>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3056501268_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint32_t* L_1 = (uint32_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt32_GetHashCode_m1046676807((uint32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.UInt32>::GetValueOrDefault()
extern Il2CppClass* UInt32_t985925326_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m4060687613_MetadataUsageId;
extern "C"  uint32_t Nullable_1_GetValueOrDefault_m4060687613_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m4060687613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint32_t L_1 = (uint32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt32_t985925326_il2cpp_TypeInfo_var, (&V_0));
		uint32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.UInt32>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3067490610_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3067490610_gshared (Nullable_1_t3871963234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3067490610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint32_t* L_1 = (uint32_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt32_ToString_m904380337((uint32_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.UInt32>::op_Implicit(T)
extern "C"  Nullable_1_t3871963234  Nullable_1_op_Implicit_m66995085_gshared (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		Nullable_1_t3871963234  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3871963234 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (uint32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<System.UInt64>::.ctor(T)
extern "C"  void Nullable_1__ctor_m516585082_gshared (Nullable_1_t3871963329 * __this, uint64_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		uint64_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.UInt64>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2842612175_gshared (Nullable_1_t3871963329 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.UInt64>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1506397927_MetadataUsageId;
extern "C"  uint64_t Nullable_1_get_Value_m1506397927_gshared (Nullable_1_t3871963329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1506397927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		uint64_t L_2 = (uint64_t)__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.UInt64>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3961497615_gshared (Nullable_1_t3871963329 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3871963329 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3871963329 *, Nullable_1_t3871963329 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3871963329 *)__this, (Nullable_1_t3871963329 )((*(Nullable_1_t3871963329 *)((Nullable_1_t3871963329 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.UInt64>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m841387088_gshared (Nullable_1_t3871963329 * __this, Nullable_1_t3871963329  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		uint64_t* L_3 = (uint64_t*)(&___other0)->get_address_of_value_0();
		uint64_t L_4 = (uint64_t)__this->get_value_0();
		uint64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = UInt64_Equals_m3490516784((uint64_t*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.UInt64>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2771903603_gshared (Nullable_1_t3871963329 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		uint64_t* L_1 = (uint64_t*)__this->get_address_of_value_0();
		int32_t L_2 = UInt64_GetHashCode_m4085537544((uint64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<System.UInt64>::GetValueOrDefault()
extern Il2CppClass* UInt64_t985925421_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2054229020_MetadataUsageId;
extern "C"  uint64_t Nullable_1_GetValueOrDefault_m2054229020_gshared (Nullable_1_t3871963329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2054229020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	uint64_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint64_t L_1 = (uint64_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (UInt64_t985925421_il2cpp_TypeInfo_var, (&V_0));
		uint64_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<System.UInt64>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m2284061491_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m2284061491_gshared (Nullable_1_t3871963329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m2284061491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		uint64_t* L_1 = (uint64_t*)__this->get_address_of_value_0();
		String_t* L_2 = UInt64_ToString_m3095865744((uint64_t*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<System.UInt64>::op_Implicit(T)
extern "C"  Nullable_1_t3871963329  Nullable_1_op_Implicit_m1446545964_gshared (Il2CppObject * __this /* static, unused */, uint64_t ___value0, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value0;
		Nullable_1_t3871963329  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t3871963329 *, uint64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (uint64_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(T)
extern "C"  void Nullable_1__ctor_m283599977_gshared (Nullable_1_t179246372 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Color_t1588175760  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3235999244_gshared (Nullable_1_t179246372 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<UnityEngine.Color>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m1645550500_MetadataUsageId;
extern "C"  Color_t1588175760  Nullable_1_get_Value_m1645550500_gshared (Nullable_1_t179246372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1645550500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Color_t1588175760  L_2 = (Color_t1588175760 )__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4143955076_gshared (Nullable_1_t179246372 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t179246372 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t179246372 *, Nullable_1_t179246372 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t179246372 *)__this, (Nullable_1_t179246372 )((*(Nullable_1_t179246372 *)((Nullable_1_t179246372 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1018264123_gshared (Nullable_1_t179246372 * __this, Nullable_1_t179246372  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Color_t1588175760 * L_3 = (Color_t1588175760 *)(&___other0)->get_address_of_value_0();
		Color_t1588175760  L_4 = (Color_t1588175760 )__this->get_value_0();
		Color_t1588175760  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Color_Equals_m3016668205((Color_t1588175760 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<UnityEngine.Color>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1843133160_gshared (Nullable_1_t179246372 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Color_t1588175760 * L_1 = (Color_t1588175760 *)__this->get_address_of_value_0();
		int32_t L_2 = Color_GetHashCode_m170503301((Color_t1588175760 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<UnityEngine.Color>::GetValueOrDefault()
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m4265596023_MetadataUsageId;
extern "C"  Color_t1588175760  Nullable_1_GetValueOrDefault_m4265596023_gshared (Nullable_1_t179246372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m4265596023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t1588175760  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Color_t1588175760  L_1 = (Color_t1588175760 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_0));
		Color_t1588175760  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<UnityEngine.Color>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1692110174_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1692110174_gshared (Nullable_1_t179246372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1692110174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Color_t1588175760 * L_1 = (Color_t1588175760 *)__this->get_address_of_value_0();
		String_t* L_2 = Color_ToString_m2277845527((Color_t1588175760 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<UnityEngine.Color>::op_Implicit(T)
extern "C"  Nullable_1_t179246372  Nullable_1_op_Implicit_m3016159454_gshared (Il2CppObject * __this /* static, unused */, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ___value0;
		Nullable_1_t179246372  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t179246372 *, Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (Color_t1588175760 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2012110062_gshared (Nullable_1_t2116400401 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Vector3_t3525329789  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3543212479_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m2002199377_MetadataUsageId;
extern "C"  Vector3_t3525329789  Nullable_1_get_Value_m2002199377_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2002199377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Vector3_t3525329789  L_2 = (Vector3_t3525329789 )__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3083210417_gshared (Nullable_1_t2116400401 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2116400401 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t2116400401 *, Nullable_1_t2116400401 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t2116400401 *)__this, (Nullable_1_t2116400401 )((*(Nullable_1_t2116400401 *)((Nullable_1_t2116400401 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2485111662_gshared (Nullable_1_t2116400401 * __this, Nullable_1_t2116400401  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Vector3_t3525329789 * L_3 = (Vector3_t3525329789 *)(&___other0)->get_address_of_value_0();
		Vector3_t3525329789  L_4 = (Vector3_t3525329789 )__this->get_value_0();
		Vector3_t3525329789  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Vector3_Equals_m3337192096((Vector3_t3525329789 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<UnityEngine.Vector3>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3406313621_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Vector3_t3525329789 * L_1 = (Vector3_t3525329789 *)__this->get_address_of_value_0();
		int32_t L_2 = Vector3_GetHashCode_m3912867704((Vector3_t3525329789 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault()
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_GetValueOrDefault_m2059098796_MetadataUsageId;
extern "C"  Vector3_t3525329789  Nullable_1_GetValueOrDefault_m2059098796_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2059098796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t3525329789  L_1 = (Vector3_t3525329789 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t3525329789  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.String System.Nullable`1<UnityEngine.Vector3>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3285229841_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3285229841_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3285229841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t3525329789 * L_1 = (Vector3_t3525329789 *)__this->get_address_of_value_0();
		String_t* L_2 = Vector3_ToString_m3566373060((Vector3_t3525329789 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Nullable`1<T> System.Nullable`1<UnityEngine.Vector3>::op_Implicit(T)
extern "C"  Nullable_1_t2116400401  Nullable_1_op_Implicit_m1679940177_gshared (Il2CppObject * __this /* static, unused */, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0 = ___value0;
		Nullable_1_t2116400401  L_1;
		memset(&L_1, 0, sizeof(L_1));
		((  void (*) (Nullable_1_t2116400401 *, Vector3_t3525329789 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_1, (Vector3_t3525329789 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// System.Void System.Predicate`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2831423627_gshared (Predicate_1_t3862319442 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<GameAnalyticsSDK.Settings/HelpTypes>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2557891547_gshared (Predicate_1_t3862319442 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2557891547((Predicate_1_t3862319442 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<GameAnalyticsSDK.Settings/HelpTypes>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* HelpTypes_t3291355544_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1322242222_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1322242222_gshared (Predicate_1_t3862319442 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1322242222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(HelpTypes_t3291355544_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<GameAnalyticsSDK.Settings/HelpTypes>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m470185689_gshared (Predicate_1_t3862319442 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<LitJson.PropertyMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3577605896_gshared (Predicate_1_t218470691 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<LitJson.PropertyMetadata>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2901721850_gshared (Predicate_1_t218470691 * __this, PropertyMetadata_t3942474089  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2901721850((Predicate_1_t218470691 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, PropertyMetadata_t3942474089  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, PropertyMetadata_t3942474089  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<LitJson.PropertyMetadata>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyMetadata_t3942474089_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m199424009_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m199424009_gshared (Predicate_1_t218470691 * __this, PropertyMetadata_t3942474089  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m199424009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PropertyMetadata_t3942474089_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<LitJson.PropertyMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2825191706_gshared (Predicate_1_t218470691 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3602106871_gshared (Predicate_1_t2255903200 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<PlayFab.Internal.GMFB_327/testRegion>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1712415787_gshared (Predicate_1_t2255903200 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1712415787((Predicate_1_t2255903200 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<PlayFab.Internal.GMFB_327/testRegion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* testRegion_t1684939302_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m771909434_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m771909434_gshared (Predicate_1_t2255903200 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m771909434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(testRegion_t1684939302_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<PlayFab.Internal.GMFB_327/testRegion>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3535779977_gshared (Predicate_1_t2255903200 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3215793458_gshared (Predicate_1_t781969239 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Boolean>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m791068432_gshared (Predicate_1_t781969239 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m791068432((Predicate_1_t781969239 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1019231135_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1019231135_gshared (Predicate_1_t781969239 * __this, bool ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1019231135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1376161476_gshared (Predicate_1_t781969239 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m401407672_gshared (Predicate_1_t3349657719 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Byte>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m986317582_gshared (Predicate_1_t3349657719 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m986317582((Predicate_1_t3349657719 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Byte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m211908321_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m211908321_gshared (Predicate_1_t3349657719 * __this, uint8_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m211908321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1925862790_gshared (Predicate_1_t3349657719 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3786392874_gshared (Predicate_1_t3349670597 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Char>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2113613916_gshared (Predicate_1_t3349670597 * __this, uint16_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2113613916((Predicate_1_t3349670597 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint16_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint16_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2335096367_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2335096367_gshared (Predicate_1_t3349670597 * __this, uint16_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2335096367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Char_t2778706699_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1187390712_gshared (Predicate_1_t3349670597 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m252712190_gshared (Predicate_1_t3883920346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m684566468_gshared (Predicate_1_t3883920346 * __this, KeyValuePair_2_t3312956448  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m684566468((Predicate_1_t3883920346 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3312956448  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t3312956448  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2552303187_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2552303187_gshared (Predicate_1_t3883920346 * __this, KeyValuePair_2_t3312956448  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2552303187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3276267152_gshared (Predicate_1_t3883920346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1618560645_gshared (Predicate_1_t909997834 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.DateTime>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3409298849_gshared (Predicate_1_t909997834 * __this, DateTime_t339033936  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3409298849((Predicate_1_t909997834 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, DateTime_t339033936  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, DateTime_t339033936  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.DateTime>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1827314036_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1827314036_gshared (Predicate_1_t909997834 * __this, DateTime_t339033936  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1827314036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2037775571_gshared (Predicate_1_t909997834 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m597047503_gshared (Predicate_1_t1105480512 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Double>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m904649879_gshared (Predicate_1_t1105480512 * __this, double ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m904649879((Predicate_1_t1105480512 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, double ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, double ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2309921514_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2309921514_gshared (Predicate_1_t1105480512 * __this, double ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2309921514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3205168029_gshared (Predicate_1_t1105480512 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m659588556_gshared (Predicate_1_t3418378685 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4051395766_gshared (Predicate_1_t3418378685 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4051395766((Predicate_1_t3418378685 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1598036677_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1598036677_gshared (Predicate_1_t3418378685 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1598036677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2016546014_gshared (Predicate_1_t3418378685 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1548574148_gshared (Predicate_1_t3418378780 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int64>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1947913877_gshared (Predicate_1_t3418378780 * __this, int64_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1947913877((Predicate_1_t3418378780 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int64_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3355965860_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3355965860_gshared (Predicate_1_t3418378780 * __this, int64_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3355965860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m654038111_gshared (Predicate_1_t3418378780 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m982040097_gshared (Predicate_1_t1408070318 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4106178309_gshared (Predicate_1_t1408070318 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4106178309((Predicate_1_t1408070318 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2038073176_gshared (Predicate_1_t1408070318 * __this, Il2CppObject * ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3970497007_gshared (Predicate_1_t1408070318 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m653858156_gshared (Predicate_1_t889699027 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1065554326_gshared (Predicate_1_t889699027 * __this, CustomAttributeNamedArgument_t318735129  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1065554326((Predicate_1_t889699027 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t318735129  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t318735129  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeNamedArgument_t318735129_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2619512869_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2619512869_gshared (Predicate_1_t889699027 * __this, CustomAttributeNamedArgument_t318735129  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2619512869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t318735129_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2465278974_gshared (Predicate_1_t889699027 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1933135835_gshared (Predicate_1_t1131379460 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3613331527_gshared (Predicate_1_t1131379460 * __this, CustomAttributeTypedArgument_t560415562  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3613331527((Predicate_1_t1131379460 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t560415562  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t560415562  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeTypedArgument_t560415562_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m577130966_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m577130966_gshared (Predicate_1_t1131379460 * __this, CustomAttributeTypedArgument_t560415562  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m577130966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t560415562_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3012949485_gshared (Predicate_1_t1131379460 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1094778552_gshared (Predicate_1_t1529172919 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Single>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1825106574_gshared (Predicate_1_t1529172919 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1825106574((Predicate_1_t1529172919 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2957256161_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2957256161_gshared (Predicate_1_t1529172919 * __this, float ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2957256161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4191100422_gshared (Predicate_1_t1529172919 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2224968072_gshared (Predicate_1_t1556889319 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.UInt64>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m620268990_gshared (Predicate_1_t1556889319 * __this, uint64_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m620268990((Predicate_1_t1556889319 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint64_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t985925421_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2799491345_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2799491345_gshared (Predicate_1_t1556889319 * __this, uint64_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2799491345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t985925421_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3823524054_gshared (Predicate_1_t1556889319 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2387897219_gshared (Predicate_1_t2987485885 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UniRx.CollectionAddEvent`1<System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2477060195_gshared (Predicate_1_t2987485885 * __this, CollectionAddEvent_1_t2416521987  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2477060195((Predicate_1_t2987485885 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CollectionAddEvent_1_t2416521987  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CollectionAddEvent_1_t2416521987  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UniRx.CollectionAddEvent`1<System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CollectionAddEvent_1_t2416521987_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2311977910_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2311977910_gshared (Predicate_1_t2987485885 * __this, CollectionAddEvent_1_t2416521987  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2311977910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CollectionAddEvent_1_t2416521987_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UniRx.CollectionAddEvent`1<System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2119991121_gshared (Predicate_1_t2987485885 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3164355731_gshared (Predicate_1_t940225717 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m790483855_gshared (Predicate_1_t940225717 * __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m790483855((Predicate_1_t940225717 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Tuple_2_t369261819_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4132879390_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4132879390_gshared (Predicate_1_t940225717 * __this, Tuple_2_t369261819  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4132879390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Tuple_2_t369261819_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4049309605_gshared (Predicate_1_t940225717 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m952178207_gshared (Predicate_1_t3129249936 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UniRx.Unit>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1823779971_gshared (Predicate_1_t3129249936 * __this, Unit_t2558286038  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1823779971((Predicate_1_t3129249936 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Unit_t2558286038  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Unit_t2558286038  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UniRx.Unit>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2674985234_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2674985234_gshared (Predicate_1_t3129249936 * __this, Unit_t2558286038  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2674985234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m980522801_gshared (Predicate_1_t3129249936 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m322232627_gshared (Predicate_1_t2159139658 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2401458867_gshared (Predicate_1_t2159139658 * __this, Color_t1588175760  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2401458867((Predicate_1_t2159139658 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t1588175760  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color_t1588175760  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3019095046_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3019095046_gshared (Predicate_1_t2159139658 * __this, Color_t1588175760  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3019095046_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t1588175760_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2796463873_gshared (Predicate_1_t2159139658 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1072563380_gshared (Predicate_1_t413080809 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3562076050_gshared (Predicate_1_t413080809 * __this, Color32_t4137084207  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3562076050((Predicate_1_t413080809 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t4137084207  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color32_t4137084207  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t4137084207_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2338878821_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2338878821_gshared (Predicate_1_t413080809 * __this, Color32_t4137084207  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2338878821_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t4137084207_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4225244034_gshared (Predicate_1_t413080809 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3721624866_gshared (Predicate_1_t1530862587 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3513546528_gshared (Predicate_1_t1530862587 * __this, RaycastResult_t959898689  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3513546528((Predicate_1_t1530862587 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t959898689  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, RaycastResult_t959898689  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RaycastResult_t959898689_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2814560687_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2814560687_gshared (Predicate_1_t1530862587 * __this, RaycastResult_t959898689  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2814560687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t959898689_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3277815988_gshared (Predicate_1_t1530862587 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3166806662_gshared (Predicate_1_t2462679877 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2591548732_gshared (Predicate_1_t2462679877 * __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2591548732((Predicate_1_t2462679877 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Quaternion_t1891715979_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2803378123_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2803378123_gshared (Predicate_1_t2462679877 * __this, Quaternion_t1891715979  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2803378123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Quaternion_t1891715979_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3422895128_gshared (Predicate_1_t2462679877 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2243424448_gshared (Predicate_1_t2096392715 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Rect>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3972571202_gshared (Predicate_1_t2096392715 * __this, Rect_t1525428817  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3972571202((Predicate_1_t2096392715 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Rect_t1525428817  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Rect_t1525428817  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Rect_t1525428817_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m781519697_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m781519697_gshared (Predicate_1_t2096392715 * __this, Rect_t1525428817  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m781519697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Rect_t1525428817_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3794387154_gshared (Predicate_1_t2096392715 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2347095596_gshared (Predicate_1_t974784479 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3556004566_gshared (Predicate_1_t974784479 * __this, UICharInfo_t403820581  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3556004566((Predicate_1_t974784479 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t403820581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UICharInfo_t403820581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UICharInfo_t403820581_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2134733669_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2134733669_gshared (Predicate_1_t974784479 * __this, UICharInfo_t403820581  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2134733669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t403820581_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1961964222_gshared (Predicate_1_t974784479 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m44565326_gshared (Predicate_1_t727885181 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2836886388_gshared (Predicate_1_t727885181 * __this, UILineInfo_t156921283  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2836886388((Predicate_1_t727885181 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t156921283  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UILineInfo_t156921283  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UILineInfo_t156921283_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m94051843_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m94051843_gshared (Predicate_1_t727885181 * __this, UILineInfo_t156921283  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m94051843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t156921283_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m25903328_gshared (Predicate_1_t727885181 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3519192684_gshared (Predicate_1_t2831025503 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3781638678_gshared (Predicate_1_t2831025503 * __this, UIVertex_t2260061605  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3781638678((Predicate_1_t2831025503 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t2260061605  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UIVertex_t2260061605  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIVertex_t2260061605_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2249257509_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2249257509_gshared (Predicate_1_t2831025503 * __this, UIVertex_t2260061605  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2249257509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t2260061605_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4019409790_gshared (Predicate_1_t2831025503 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2975892103_gshared (Predicate_1_t4096293686 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2217101919_gshared (Predicate_1_t4096293686 * __this, Vector2_t3525329788  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2217101919((Predicate_1_t4096293686 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t3525329788  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t3525329788  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3964024626_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3964024626_gshared (Predicate_1_t4096293686 * __this, Vector2_t3525329788  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3964024626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t3525329788_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2181069525_gshared (Predicate_1_t4096293686 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m313728806_gshared (Predicate_1_t4096293687 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1923698912_gshared (Predicate_1_t4096293687 * __this, Vector3_t3525329789  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1923698912((Predicate_1_t4096293687 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t3525329789  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t3525329789  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2038491315_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2038491315_gshared (Predicate_1_t4096293687 * __this, Vector3_t3525329789  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2038491315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t3525329789_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3206561524_gshared (Predicate_1_t4096293687 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1946532805_gshared (Predicate_1_t4096293688 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1630295905_gshared (Predicate_1_t4096293688 * __this, Vector4_t3525329790  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1630295905((Predicate_1_t4096293688 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t3525329790  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector4_t3525329790  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m112958004_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m112958004_gshared (Predicate_1_t4096293688 * __this, Vector4_t3525329790  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m112958004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t3525329790_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4232053523_gshared (Predicate_1_t4096293688 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m4236926794_gshared (Getter_2_t539570894 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m410564889_gshared (Getter_2_t539570894 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m410564889((Getter_2_t539570894 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m3146221447_gshared (Getter_2_t539570894 * __this, Il2CppObject * ____this0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m1749574747_gshared (Getter_2_t539570894 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m3357261135_gshared (StaticGetter_1_t171826611 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * StaticGetter_1_Invoke_m3410367530_gshared (StaticGetter_1_t171826611 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StaticGetter_1_Invoke_m3410367530((StaticGetter_1_t171826611 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StaticGetter_1_BeginInvoke_m3837643130_gshared (StaticGetter_1_t171826611 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * StaticGetter_1_EndInvoke_m3212189152_gshared (StaticGetter_1_t171826611 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>::.ctor()
extern "C"  void U3CFillU3Ec__AnonStoreyEB__ctor_m143819720_gshared (U3CFillU3Ec__AnonStoreyEB_t2250867686 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>::<>m__13C(TPrefab)
extern "C"  bool U3CFillU3Ec__AnonStoreyEB_U3CU3Em__13C_m3020003420_gshared (U3CFillU3Ec__AnonStoreyEB_t2250867686 * __this, Il2CppObject * ___p0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___p0;
		NullCheck(L_0);
		bool L_1 = (bool)((NewTableCell_t2774679728 *)L_0)->get_deleted_6();
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		Il2CppObject * L_2 = ___p0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)((NewTableCell_t2774679728 *)L_2)->get_index_7();
		CollectionRemoveEvent_1_t898259258 * L_4 = (CollectionRemoveEvent_1_t898259258 *)__this->get_ev_0();
		NullCheck((CollectionRemoveEvent_1_t898259258 *)L_4);
		int32_t L_5 = ((  int32_t (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionRemoveEvent_1_t898259258 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_5))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B3_0 = 0;
	}

IL_002b:
	{
		return (bool)G_B3_0;
	}
}
// System.Void TableBlock`2/<Fill>c__AnonStoreyEB<System.Object,System.Object>::<>m__13D()
extern "C"  void U3CFillU3Ec__AnonStoreyEB_U3CU3Em__13D_m985795923_gshared (U3CFillU3Ec__AnonStoreyEB_t2250867686 * __this, const MethodInfo* method)
{
	{
		TableBlock_2_t3756399434 * L_0 = (TableBlock_2_t3756399434 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_0);
		List_1_t1634065389 * L_1 = (List_1_t1634065389 *)L_0->get_visibleList_4();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_prefab_1();
		NullCheck((List_1_t1634065389 *)L_1);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0) */, (List_1_t1634065389 *)L_1, (Il2CppObject *)L_2);
		Il2CppObject ** L_3 = (Il2CppObject **)__this->get_address_of_prefab_1();
		NullCheck((InstantiatableInPool_t1882737686 *)(*L_3));
		InstantiatableInPool_Recycle_m1209365942((InstantiatableInPool_t1882737686 *)(*L_3), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::.ctor(NewTableView,InstantiationPool`1<TPrefab>,System.String)
extern Il2CppClass* ConnectionCollector_t444796719_il2cpp_TypeInfo_var;
extern const uint32_t TableBlock_2__ctor_m218251931_MetadataUsageId;
extern "C"  void TableBlock_2__ctor_m218251931_gshared (TableBlock_2_t3756399434 * __this, NewTableView_t2775249395 * ___newTableView0, InstantiationPool_1_t1380750307 * ___pool1, String_t* ___prefabPath2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableBlock_2__ctor_m218251931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Vector2_t3525329788  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ConnectionCollector_t444796719 * L_0 = (ConnectionCollector_t444796719 *)il2cpp_codegen_object_new(ConnectionCollector_t444796719_il2cpp_TypeInfo_var);
		ConnectionCollector__ctor_m3524765980(L_0, /*hidden argument*/NULL);
		__this->set_collector_2(L_0);
		List_1_t1634065389 * L_1 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_1, (int32_t)((int32_t)100), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_visibleList_4(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		NewTableView_t2775249395 * L_2 = ___newTableView0;
		__this->set_newTableView_0(L_2);
		InstantiationPool_1_t1380750307 * L_3 = ___pool1;
		__this->set_pool_1(L_3);
		String_t* L_4 = ___prefabPath2;
		__this->set__prefabPath_7(L_4);
		NullCheck((TableBlock_2_t3756399434 *)__this);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (TableBlock_2_t3756399434 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((TableBlock_2_t3756399434 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_5;
		NullCheck((NewTableCell_t2774679728 *)(*(&V_0)));
		RectTransform_t3317474837 * L_6 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_0)), /*hidden argument*/NULL);
		NullCheck((RectTransform_t3317474837 *)L_6);
		Vector2_t3525329788  L_7 = RectTransform_get_sizeDelta_m4279424984((RectTransform_t3317474837 *)L_6, /*hidden argument*/NULL);
		V_1 = (Vector2_t3525329788 )L_7;
		float L_8 = (float)(&V_1)->get_y_2();
		__this->set_defaultCellSize_3(L_8);
		NullCheck((InstantiatableInPool_t1882737686 *)(*(&V_0)));
		InstantiatableInPool_Recycle_m1209365942((InstantiatableInPool_t1882737686 *)(*(&V_0)), /*hidden argument*/NULL);
		return;
	}
}
// TPrefab TableBlock`2<System.Object,System.Object>::CreatePrefab()
extern "C"  Il2CppObject * TableBlock_2_CreatePrefab_m2852969303_gshared (TableBlock_2_t3756399434 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		InstantiationPool_1_t1380750307 * L_0 = (InstantiationPool_1_t1380750307 *)__this->get_pool_1();
		String_t* L_1 = (String_t*)__this->get__prefabPath_7();
		NewTableView_t2775249395 * L_2 = (NewTableView_t2775249395 *)__this->get_newTableView_0();
		NullCheck(L_2);
		RectTransform_t3317474837 * L_3 = (RectTransform_t3317474837 *)L_2->get_containerRect_6();
		NullCheck((InstantiationPool_1_t1380750307 *)L_0);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (InstantiationPool_1_t1380750307 *, String_t*, Transform_t284553113 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((InstantiationPool_1_t1380750307 *)L_0, (String_t*)L_1, (Transform_t284553113 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (Il2CppObject *)L_4;
		NullCheck((NewTableCell_t2774679728 *)(*(&V_0)));
		RectTransform_t3317474837 * L_5 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_0)), /*hidden argument*/NULL);
		NewTableView_t2775249395 * L_6 = (NewTableView_t2775249395 *)__this->get_newTableView_0();
		NullCheck(L_6);
		RectTransform_t3317474837 * L_7 = (RectTransform_t3317474837 *)L_6->get_containerRect_6();
		NullCheck((Transform_t284553113 *)L_5);
		Transform_SetParent_m3449663462((Transform_t284553113 *)L_5, (Transform_t284553113 *)L_7, /*hidden argument*/NULL);
		Il2CppObject * L_8 = V_0;
		ShortcutExtensions_DOKill_m2275247369(NULL /*static, unused*/, (Component_t2126946602 *)L_8, (bool)0, /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)(*(&V_0)));
		Transform_t284553113 * L_9 = Component_get_transform_m2452535634((Component_t2126946602 *)(*(&V_0)), /*hidden argument*/NULL);
		Vector3_t3525329789  L_10 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Transform_t284553113 *)L_9);
		Transform_set_localScale_m1776830461((Transform_t284553113 *)L_9, (Vector3_t3525329789 )L_10, /*hidden argument*/NULL);
		NullCheck((NewTableCell_t2774679728 *)(*(&V_0)));
		RectTransform_t3317474837 * L_11 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_0)), /*hidden argument*/NULL);
		Vector3_t3525329789  L_12 = Vector3_get_zero_m2720511387(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Transform_t284553113 *)L_11);
		Transform_set_localPosition_m3778340736((Transform_t284553113 *)L_11, (Vector3_t3525329789 )L_12, /*hidden argument*/NULL);
		NullCheck((NewTableCell_t2774679728 *)(*(&V_0)));
		RectTransform_t3317474837 * L_13 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_0)), /*hidden argument*/NULL);
		Vector2_t3525329788  L_14 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t3525329788  L_15 = Vector2_op_Multiply_m2276706051(NULL /*static, unused*/, (Vector2_t3525329788 )L_14, (float)(2334234.0f), /*hidden argument*/NULL);
		NullCheck((RectTransform_t3317474837 *)L_13);
		RectTransform_set_anchoredPosition_m1783392546((RectTransform_t3317474837 *)L_13, (Vector2_t3525329788 )L_15, /*hidden argument*/NULL);
		Il2CppObject * L_16 = V_0;
		NullCheck(L_16);
		((NewTableCell_t2774679728 *)L_16)->set_deleted_6((bool)0);
		Il2CppObject * L_17 = V_0;
		return L_17;
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::Fill(ZergRush.IObservableCollection`1<TData>,System.Action`2<TPrefab,TData>)
extern Il2CppClass* Action_1_t2706738743_il2cpp_TypeInfo_var;
extern Il2CppClass* IStream_1_t3110977029_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1024136343_MethodInfo_var;
extern const uint32_t TableBlock_2_Fill_m920501207_MetadataUsageId;
extern "C"  void TableBlock_2_Fill_m920501207_gshared (TableBlock_2_t3756399434 * __this, Il2CppObject* ___observableData0, Action_2_t4105459918 * ___fillMethod1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableBlock_2_Fill_m920501207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t4105459918 * L_0 = ___fillMethod1;
		__this->set__fillMethod_8(L_0);
		Il2CppObject* L_1 = ___observableData0;
		__this->set__observableData_5(L_1);
		NullCheck((TableBlock_2_t3756399434 *)__this);
		((  void (*) (TableBlock_2_t3756399434 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TableBlock_2_t3756399434 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NewTableView_t2775249395 * L_2 = (NewTableView_t2775249395 *)__this->get_newTableView_0();
		NullCheck((NewTableView_t2775249395 *)L_2);
		NewTableView_NeedRebuild_m2127486155((NewTableView_t2775249395 *)L_2, /*hidden argument*/NULL);
		ConnectionCollector_t444796719 * L_3 = (ConnectionCollector_t444796719 *)__this->get_collector_2();
		Il2CppObject* L_4 = ___observableData0;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* IStream`1<UniRx.CollectionAddEvent`1<T>> ZergRush.IObservableCollection`1<System.Object>::ObserveAdd() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_4);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Action_1_t2564974692 * L_7 = (Action_1_t2564974692 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (Action_1_t2564974692 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_7, (Il2CppObject *)__this, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_8 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2564974692 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<UniRx.CollectionAddEvent`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_5, (Action_1_t2564974692 *)L_7, (int32_t)1);
		NullCheck((ConnectionCollector_t444796719 *)L_3);
		ConnectionCollector_set_add_m3757065972((ConnectionCollector_t444796719 *)L_3, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		ConnectionCollector_t444796719 * L_9 = (ConnectionCollector_t444796719 *)__this->get_collector_2();
		Il2CppObject* L_10 = ___observableData0;
		NullCheck((Il2CppObject*)L_10);
		Il2CppObject* L_11 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* IStream`1<CollectionRemoveEvent`1<T>> ZergRush.IObservableCollection`1<System.Object>::ObserveRemove() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_10);
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t1046711963 * L_13 = (Action_1_t1046711963 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		((  void (*) (Action_1_t1046711963 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(L_13, (Il2CppObject *)__this, (IntPtr_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject * L_14 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1046711963 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<CollectionRemoveEvent`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (Il2CppObject*)L_11, (Action_1_t1046711963 *)L_13, (int32_t)1);
		NullCheck((ConnectionCollector_t444796719 *)L_9);
		ConnectionCollector_set_add_m3757065972((ConnectionCollector_t444796719 *)L_9, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		ConnectionCollector_t444796719 * L_15 = (ConnectionCollector_t444796719 *)__this->get_collector_2();
		Il2CppObject* L_16 = ___observableData0;
		NullCheck((Il2CppObject*)L_16);
		Il2CppObject* L_17 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(5 /* IStream`1<UniRx.Unit> ZergRush.IObservableCollection`1<System.Object>::ObserveReset() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_16);
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t2706738743 * L_19 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(Action_1_t2706738743_il2cpp_TypeInfo_var);
		Action_1__ctor_m1024136343(L_19, (Il2CppObject *)__this, (IntPtr_t)L_18, /*hidden argument*/Action_1__ctor_m1024136343_MethodInfo_var);
		NullCheck((Il2CppObject*)L_17);
		Il2CppObject * L_20 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2706738743 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<UniRx.Unit>::Listen(System.Action`1<T>,Priority) */, IStream_1_t3110977029_il2cpp_TypeInfo_var, (Il2CppObject*)L_17, (Action_1_t2706738743 *)L_19, (int32_t)1);
		NullCheck((ConnectionCollector_t444796719 *)L_15);
		ConnectionCollector_set_add_m3757065972((ConnectionCollector_t444796719 *)L_15, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		ConnectionCollector_t444796719 * L_21 = (ConnectionCollector_t444796719 *)__this->get_collector_2();
		Il2CppObject* L_22 = ___observableData0;
		NullCheck((Il2CppObject*)L_22);
		Il2CppObject* L_23 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(3 /* IStream`1<CollectionReplaceEvent`1<T>> ZergRush.IObservableCollection`1<System.Object>::ObserveReplace() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_22);
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Action_1_t2645824775 * L_25 = (Action_1_t2645824775 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((  void (*) (Action_1_t2645824775 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(L_25, (Il2CppObject *)__this, (IntPtr_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		NullCheck((Il2CppObject*)L_23);
		Il2CppObject * L_26 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2645824775 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<CollectionReplaceEvent`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19), (Il2CppObject*)L_23, (Action_1_t2645824775 *)L_25, (int32_t)1);
		NullCheck((ConnectionCollector_t444796719 *)L_21);
		ConnectionCollector_set_add_m3757065972((ConnectionCollector_t444796719 *)L_21, (Il2CppObject *)L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Single TableBlock`2<System.Object,System.Object>::GetSize()
extern "C"  float TableBlock_2_GetSize_m1972660851_gshared (TableBlock_2_t3756399434 * __this, const MethodInfo* method)
{
	List_1_t1634065389 * G_B2_0 = NULL;
	List_1_t1634065389 * G_B1_0 = NULL;
	List_1_t1634065389 * G_B4_0 = NULL;
	int32_t G_B4_1 = 0;
	float G_B4_2 = 0.0f;
	List_1_t1634065389 * G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	float G_B3_2 = 0.0f;
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)__this->get_visibleList_4();
		Func_2_t2256885953 * L_1 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cache9_9();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		Func_2_t2256885953 * L_3 = (Func_2_t2256885953 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		((  void (*) (Func_2_t2256885953 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(L_3, (Il2CppObject *)NULL, (IntPtr_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->set_U3CU3Ef__amU24cache9_9(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t2256885953 * L_4 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cache9_9();
		float L_5 = ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2256885953 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B2_0, (Func_2_t2256885953 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		Il2CppObject* L_6 = (Il2CppObject*)__this->get__observableData_5();
		NullCheck((Il2CppObject*)L_6);
		int32_t L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25), (Il2CppObject*)L_6);
		List_1_t1634065389 * L_8 = (List_1_t1634065389 *)__this->get_visibleList_4();
		Func_2_t1509682273 * L_9 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cacheA_10();
		G_B3_0 = L_8;
		G_B3_1 = L_7;
		G_B3_2 = L_5;
		if (L_9)
		{
			G_B4_0 = L_8;
			G_B4_1 = L_7;
			G_B4_2 = L_5;
			goto IL_0051;
		}
	}
	{
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		Func_2_t1509682273 * L_11 = (Func_2_t1509682273 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		((  void (*) (Func_2_t1509682273 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->set_U3CU3Ef__amU24cacheA_10(L_11);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_0051:
	{
		Func_2_t1509682273 * L_12 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cacheA_10();
		int32_t L_13 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B4_0, (Func_2_t1509682273 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		float L_14 = (float)__this->get_defaultCellSize_3();
		return ((float)((float)G_B4_2+(float)((float)((float)(((float)((float)((int32_t)((int32_t)G_B4_1-(int32_t)L_13)))))*(float)L_14))));
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::TryShowCells(System.Single&,System.Single&,System.Single,System.Int32&)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TableBlock_2_TryShowCells_m1596422709_MetadataUsageId;
extern "C"  void TableBlock_2_TryShowCells_m1596422709_gshared (TableBlock_2_t3756399434 * __this, float* ___containerLength0, float* ___offset1, float ___startPosition2, int32_t* ___forgetOffset3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableBlock_2_TryShowCells_m1596422709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	List_1_t1634065389 * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	Enumerator_t4014815677  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Il2CppObject * V_10 = NULL;
	Enumerator_t4014815677  V_11;
	memset(&V_11, 0, sizeof(V_11));
	int32_t V_12 = 0;
	Il2CppObject * V_13 = NULL;
	Il2CppObject * V_14 = NULL;
	Enumerator_t4014815677  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t1634065389 * G_B7_0 = NULL;
	List_1_t1634065389 * G_B6_0 = NULL;
	{
		float L_0 = ___startPosition2;
		float* L_1 = ___offset1;
		NullCheck((TableBlock_2_t3756399434 *)__this);
		float L_2 = VirtFuncInvoker0< float >::Invoke(6 /* System.Single TableBlock`2<System.Object,System.Object>::GetSize() */, (TableBlock_2_t3756399434 *)__this);
		if ((!(((float)((float)((float)((float)((float)L_0+(float)(*((float*)L_1))))+(float)L_2))) < ((float)(0.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		float* L_3 = ___offset1;
		float* L_4 = ___offset1;
		NullCheck((TableBlock_2_t3756399434 *)__this);
		float L_5 = VirtFuncInvoker0< float >::Invoke(6 /* System.Single TableBlock`2<System.Object,System.Object>::GetSize() */, (TableBlock_2_t3756399434 *)__this);
		*((float*)(L_3)) = (float)((float)((float)(*((float*)L_4))+(float)L_5));
		NullCheck((TableBlock_2_t3756399434 *)__this);
		((  void (*) (TableBlock_2_t3756399434 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TableBlock_2_t3756399434 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return;
	}

IL_0028:
	{
		float L_6 = ___startPosition2;
		float* L_7 = ___offset1;
		float* L_8 = ___containerLength0;
		if ((!(((float)((float)((float)L_6+(float)(*((float*)L_7))))) > ((float)(*((float*)L_8))))))
		{
			goto IL_003b;
		}
	}
	{
		NullCheck((TableBlock_2_t3756399434 *)__this);
		((  void (*) (TableBlock_2_t3756399434 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TableBlock_2_t3756399434 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return;
	}

IL_003b:
	{
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		List_1_t1634065389 * L_9 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_9);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0096;
		}
	}
	{
		List_1_t1634065389 * L_11 = (List_1_t1634065389 *)__this->get_visibleList_4();
		Func_2_t1509682273 * L_12 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cacheB_11();
		G_B6_0 = L_11;
		if (L_12)
		{
			G_B7_0 = L_11;
			goto IL_006e;
		}
	}
	{
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		Func_2_t1509682273 * L_14 = (Func_2_t1509682273 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		((  void (*) (Func_2_t1509682273 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(L_14, (Il2CppObject *)NULL, (IntPtr_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->set_U3CU3Ef__amU24cacheB_11(L_14);
		G_B7_0 = G_B6_0;
	}

IL_006e:
	{
		Func_2_t1509682273 * L_15 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cacheB_11();
		Il2CppObject * L_16 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B7_0, (Func_2_t1509682273 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		V_2 = (Il2CppObject *)L_16;
		Il2CppObject * L_17 = V_2;
		bool L_18 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_17, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0096;
		}
	}
	{
		Il2CppObject * L_19 = V_2;
		NullCheck(L_19);
		int32_t L_20 = (int32_t)((NewTableCell_t2774679728 *)L_19)->get_index_7();
		V_1 = (int32_t)L_20;
	}

IL_0096:
	{
		float L_21 = ___startPosition2;
		float* L_22 = ___offset1;
		float L_23 = (float)__this->get_defaultCellSize_3();
		if ((!(((float)((float)((float)L_21+(float)(*((float*)L_22))))) < ((float)((-L_23))))))
		{
			goto IL_00c6;
		}
	}
	{
		float L_24 = ___startPosition2;
		float* L_25 = ___offset1;
		float L_26 = (float)__this->get_defaultCellSize_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_27 = Mathf_CeilToInt_m1037993498(NULL /*static, unused*/, (float)((float)((float)((float)((float)L_24+(float)(*((float*)L_25))))/(float)L_26)), /*hidden argument*/NULL);
		V_3 = (int32_t)((-L_27));
		int32_t L_28 = V_1;
		int32_t L_29 = V_3;
		int32_t L_30 = Mathf_Min_m2413438171(NULL /*static, unused*/, (int32_t)L_28, (int32_t)L_29, /*hidden argument*/NULL);
		int32_t L_31 = Mathf_Max_m3852759872(NULL /*static, unused*/, (int32_t)L_30, (int32_t)0, /*hidden argument*/NULL);
		V_0 = (int32_t)L_31;
	}

IL_00c6:
	{
		float* L_32 = ___offset1;
		float* L_33 = ___offset1;
		float L_34 = (float)__this->get_defaultCellSize_3();
		int32_t L_35 = V_0;
		*((float*)(L_32)) = (float)((float)((float)(*((float*)L_33))+(float)((float)((float)L_34*(float)(((float)((float)L_35)))))));
		float* L_36 = ___offset1;
		V_4 = (float)(*((float*)L_36));
		int32_t L_37 = V_0;
		V_5 = (int32_t)L_37;
		goto IL_0124;
	}

IL_00e0:
	{
		NullCheck((TableBlock_2_t3756399434 *)__this);
		Il2CppObject * L_38 = ((  Il2CppObject * (*) (TableBlock_2_t3756399434 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((TableBlock_2_t3756399434 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_6 = (Il2CppObject *)L_38;
		Action_2_t4105459918 * L_39 = (Action_2_t4105459918 *)__this->get__fillMethod_8();
		Il2CppObject * L_40 = V_6;
		Il2CppObject* L_41 = (Il2CppObject*)__this->get__observableData_5();
		int32_t L_42 = V_5;
		Il2CppObject * L_43 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)(NULL /*static, unused*/, (Il2CppObject*)L_41, (int32_t)L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		NullCheck((Action_2_t4105459918 *)L_39);
		((  void (*) (Action_2_t4105459918 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)->method)((Action_2_t4105459918 *)L_39, (Il2CppObject *)L_40, (Il2CppObject *)L_43, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35));
		Il2CppObject * L_44 = V_6;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		((NewTableCell_t2774679728 *)L_44)->set_index_7(L_45);
		List_1_t1634065389 * L_46 = (List_1_t1634065389 *)__this->get_visibleList_4();
		Il2CppObject * L_47 = V_6;
		NullCheck((List_1_t1634065389 *)L_46);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,!0) */, (List_1_t1634065389 *)L_46, (int32_t)0, (Il2CppObject *)L_47);
		int32_t L_48 = V_5;
		V_5 = (int32_t)((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_0124:
	{
		int32_t L_49 = V_5;
		int32_t L_50 = V_1;
		if ((((int32_t)L_49) < ((int32_t)L_50)))
		{
			goto IL_00e0;
		}
	}
	{
		List_1_t1634065389 * L_51 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37)->method)(L_51, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37));
		V_7 = (List_1_t1634065389 *)L_51;
		List_1_t1634065389 * L_52 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_52);
		Enumerator_t4014815677  L_53 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((List_1_t1634065389 *)L_52, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		V_9 = (Enumerator_t4014815677 )L_53;
	}

IL_0140:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01da;
		}

IL_0145:
		{
			Il2CppObject * L_54 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Enumerator_t4014815677 *)(&V_9), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
			V_8 = (Il2CppObject *)L_54;
			Il2CppObject * L_55 = V_8;
			NullCheck(L_55);
			bool L_56 = (bool)((NewTableCell_t2774679728 *)L_55)->get_deleted_6();
			if (L_56)
			{
				goto IL_0163;
			}
		}

IL_015f:
		{
			int32_t L_57 = V_0;
			V_0 = (int32_t)((int32_t)((int32_t)L_57+(int32_t)1));
		}

IL_0163:
		{
			float L_58 = ___startPosition2;
			float* L_59 = ___offset1;
			float* L_60 = ___containerLength0;
			float L_61 = (float)__this->get_defaultCellSize_3();
			if ((!(((float)((float)((float)L_58+(float)(*((float*)L_59))))) > ((float)((float)((float)(*((float*)L_60))+(float)L_61))))))
			{
				goto IL_018b;
			}
		}

IL_0175:
		{
			NullCheck((InstantiatableInPool_t1882737686 *)(*(&V_8)));
			InstantiatableInPool_Recycle_m1209365942((InstantiatableInPool_t1882737686 *)(*(&V_8)), /*hidden argument*/NULL);
			List_1_t1634065389 * L_62 = V_7;
			Il2CppObject * L_63 = V_8;
			NullCheck((List_1_t1634065389 *)L_62);
			VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_62, (Il2CppObject *)L_63);
		}

IL_018b:
		{
			float L_64 = ___startPosition2;
			float* L_65 = ___offset1;
			NullCheck((NewTableCell_t2774679728 *)(*(&V_8)));
			float L_66 = NewTableCell_GetSize_m686112628((NewTableCell_t2774679728 *)(*(&V_8)), /*hidden argument*/NULL);
			if ((!(((float)((float)((float)((float)((float)L_64+(float)(*((float*)L_65))))+(float)L_66))) < ((float)(0.0f)))))
			{
				goto IL_01c8;
			}
		}

IL_01a7:
		{
			NullCheck((InstantiatableInPool_t1882737686 *)(*(&V_8)));
			InstantiatableInPool_Recycle_m1209365942((InstantiatableInPool_t1882737686 *)(*(&V_8)), /*hidden argument*/NULL);
			List_1_t1634065389 * L_67 = V_7;
			Il2CppObject * L_68 = V_8;
			NullCheck((List_1_t1634065389 *)L_67);
			VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_67, (Il2CppObject *)L_68);
			float L_69 = V_4;
			float L_70 = (float)__this->get_defaultCellSize_3();
			V_4 = (float)((float)((float)L_69+(float)L_70));
		}

IL_01c8:
		{
			float* L_71 = ___offset1;
			float* L_72 = ___offset1;
			NullCheck((NewTableCell_t2774679728 *)(*(&V_8)));
			float L_73 = NewTableCell_GetSize_m686112628((NewTableCell_t2774679728 *)(*(&V_8)), /*hidden argument*/NULL);
			*((float*)(L_71)) = (float)((float)((float)(*((float*)L_72))+(float)L_73));
		}

IL_01da:
		{
			bool L_74 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((Enumerator_t4014815677 *)(&V_9), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
			if (L_74)
			{
				goto IL_0145;
			}
		}

IL_01e6:
		{
			IL2CPP_LEAVE(0x1F8, FINALLY_01eb);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01eb;
	}

FINALLY_01eb:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_75 = V_9;
		Enumerator_t4014815677  L_76 = L_75;
		Il2CppObject * L_77 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), &L_76);
		NullCheck((Il2CppObject *)L_77);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_77);
		IL2CPP_END_FINALLY(491)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(491)
	{
		IL2CPP_JUMP_TBL(0x1F8, IL_01f8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01f8:
	{
		List_1_t1634065389 * L_78 = V_7;
		NullCheck((List_1_t1634065389 *)L_78);
		Enumerator_t4014815677  L_79 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((List_1_t1634065389 *)L_78, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		V_11 = (Enumerator_t4014815677 )L_79;
	}

IL_0201:
	try
	{ // begin try (depth: 1)
		{
			goto IL_021d;
		}

IL_0206:
		{
			Il2CppObject * L_80 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Enumerator_t4014815677 *)(&V_11), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
			V_10 = (Il2CppObject *)L_80;
			List_1_t1634065389 * L_81 = (List_1_t1634065389 *)__this->get_visibleList_4();
			Il2CppObject * L_82 = V_10;
			NullCheck((List_1_t1634065389 *)L_81);
			VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0) */, (List_1_t1634065389 *)L_81, (Il2CppObject *)L_82);
		}

IL_021d:
		{
			bool L_83 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((Enumerator_t4014815677 *)(&V_11), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
			if (L_83)
			{
				goto IL_0206;
			}
		}

IL_0229:
		{
			IL2CPP_LEAVE(0x23B, FINALLY_022e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_022e;
	}

FINALLY_022e:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_84 = V_11;
		Enumerator_t4014815677  L_85 = L_84;
		Il2CppObject * L_86 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), &L_85);
		NullCheck((Il2CppObject *)L_86);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_86);
		IL2CPP_END_FINALLY(558)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(558)
	{
		IL2CPP_JUMP_TBL(0x23B, IL_023b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_023b:
	{
		float L_87 = ___startPosition2;
		float* L_88 = ___offset1;
		float* L_89 = ___containerLength0;
		if ((!(((float)((float)((float)L_87+(float)(*((float*)L_88))))) < ((float)(*((float*)L_89))))))
		{
			goto IL_02eb;
		}
	}
	{
		int32_t L_90 = V_0;
		float* L_91 = ___containerLength0;
		float L_92 = ___startPosition2;
		float* L_93 = ___offset1;
		float L_94 = (float)__this->get_defaultCellSize_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_95 = Mathf_CeilToInt_m1037993498(NULL /*static, unused*/, (float)((float)((float)((float)((float)((float)((float)(*((float*)L_91))-(float)L_92))-(float)(*((float*)L_93))))/(float)L_94)), /*hidden argument*/NULL);
		V_12 = (int32_t)((int32_t)((int32_t)L_90+(int32_t)L_95));
		goto IL_02d2;
	}

IL_0262:
	{
		NullCheck((TableBlock_2_t3756399434 *)__this);
		Il2CppObject * L_96 = ((  Il2CppObject * (*) (TableBlock_2_t3756399434 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((TableBlock_2_t3756399434 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_13 = (Il2CppObject *)L_96;
		Il2CppObject * L_97 = V_13;
		int32_t L_98 = V_0;
		NullCheck(L_97);
		((NewTableCell_t2774679728 *)L_97)->set_index_7(L_98);
		Action_2_t4105459918 * L_99 = (Action_2_t4105459918 *)__this->get__fillMethod_8();
		Il2CppObject * L_100 = V_13;
		Il2CppObject* L_101 = (Il2CppObject*)__this->get__observableData_5();
		int32_t L_102 = V_0;
		Il2CppObject * L_103 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)(NULL /*static, unused*/, (Il2CppObject*)L_101, (int32_t)L_102, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		NullCheck((Action_2_t4105459918 *)L_99);
		((  void (*) (Action_2_t4105459918 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)->method)((Action_2_t4105459918 *)L_99, (Il2CppObject *)L_100, (Il2CppObject *)L_103, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35));
		List_1_t1634065389 * L_104 = (List_1_t1634065389 *)__this->get_visibleList_4();
		Il2CppObject * L_105 = V_13;
		NullCheck((List_1_t1634065389 *)L_104);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_104, (Il2CppObject *)L_105);
		NullCheck((NewTableCell_t2774679728 *)(*(&V_13)));
		RectTransform_t3317474837 * L_106 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_13)), /*hidden argument*/NULL);
		Vector3_t3525329789  L_107;
		memset(&L_107, 0, sizeof(L_107));
		Vector3__ctor_m1243251509(&L_107, (float)(10000.0f), (float)(0.0f), (float)(0.0f), /*hidden argument*/NULL);
		NullCheck((Transform_t284553113 *)L_106);
		Transform_set_localPosition_m3778340736((Transform_t284553113 *)L_106, (Vector3_t3525329789 )L_107, /*hidden argument*/NULL);
		float* L_108 = ___offset1;
		float* L_109 = ___offset1;
		float L_110 = (float)__this->get_defaultCellSize_3();
		*((float*)(L_108)) = (float)((float)((float)(*((float*)L_109))+(float)L_110));
		int32_t L_111 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_111+(int32_t)1));
	}

IL_02d2:
	{
		int32_t L_112 = V_0;
		int32_t L_113 = V_12;
		if ((((int32_t)L_112) >= ((int32_t)L_113)))
		{
			goto IL_02eb;
		}
	}
	{
		int32_t L_114 = V_0;
		Il2CppObject* L_115 = (Il2CppObject*)__this->get__observableData_5();
		NullCheck((Il2CppObject*)L_115);
		int32_t L_116 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25), (Il2CppObject*)L_115);
		if ((((int32_t)L_114) < ((int32_t)L_116)))
		{
			goto IL_0262;
		}
	}

IL_02eb:
	{
		List_1_t1634065389 * L_117 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_117);
		Enumerator_t4014815677  L_118 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((List_1_t1634065389 *)L_117, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		V_15 = (Enumerator_t4014815677 )L_118;
	}

IL_02f8:
	try
	{ // begin try (depth: 1)
		{
			goto IL_034e;
		}

IL_02fd:
		{
			Il2CppObject * L_119 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Enumerator_t4014815677 *)(&V_15), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
			V_14 = (Il2CppObject *)L_119;
			NullCheck((NewTableCell_t2774679728 *)(*(&V_14)));
			RectTransform_t3317474837 * L_120 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_14)), /*hidden argument*/NULL);
			Vector3_t3525329789  L_121 = Vector3_get_zero_m2720511387(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck((Transform_t284553113 *)L_120);
			Transform_set_localPosition_m3778340736((Transform_t284553113 *)L_120, (Vector3_t3525329789 )L_121, /*hidden argument*/NULL);
			NullCheck((NewTableCell_t2774679728 *)(*(&V_14)));
			RectTransform_t3317474837 * L_122 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_14)), /*hidden argument*/NULL);
			float L_123 = V_4;
			Vector2_t3525329788  L_124;
			memset(&L_124, 0, sizeof(L_124));
			Vector2__ctor_m3620597967(&L_124, (float)(0.0f), (float)((-L_123)), /*hidden argument*/NULL);
			NullCheck((RectTransform_t3317474837 *)L_122);
			RectTransform_set_anchoredPosition_m1783392546((RectTransform_t3317474837 *)L_122, (Vector2_t3525329788 )L_124, /*hidden argument*/NULL);
			float L_125 = V_4;
			NullCheck((NewTableCell_t2774679728 *)(*(&V_14)));
			float L_126 = NewTableCell_GetSize_m686112628((NewTableCell_t2774679728 *)(*(&V_14)), /*hidden argument*/NULL);
			V_4 = (float)((float)((float)L_125+(float)L_126));
		}

IL_034e:
		{
			bool L_127 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((Enumerator_t4014815677 *)(&V_15), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
			if (L_127)
			{
				goto IL_02fd;
			}
		}

IL_035a:
		{
			IL2CPP_LEAVE(0x36C, FINALLY_035f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_035f;
	}

FINALLY_035f:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_128 = V_15;
		Enumerator_t4014815677  L_129 = L_128;
		Il2CppObject * L_130 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), &L_129);
		NullCheck((Il2CppObject *)L_130);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_130);
		IL2CPP_END_FINALLY(863)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(863)
	{
		IL2CPP_JUMP_TBL(0x36C, IL_036c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_036c:
	{
		float* L_131 = ___offset1;
		float L_132 = V_4;
		*((float*)(L_131)) = (float)L_132;
		return;
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::Dispose()
extern "C"  void TableBlock_2_Dispose_m766308175_gshared (TableBlock_2_t3756399434 * __this, const MethodInfo* method)
{
	{
		NullCheck((TableBlock_2_t3756399434 *)__this);
		((  void (*) (TableBlock_2_t3756399434 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TableBlock_2_t3756399434 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NewTableView_t2775249395 * L_0 = (NewTableView_t2775249395 *)__this->get_newTableView_0();
		NullCheck((NewTableView_t2775249395 *)L_0);
		NewTableView_RemoveBlock_m191244155((NewTableView_t2775249395 *)L_0, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::Clear(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TableBlock_2_Clear_m825508468_MetadataUsageId;
extern "C"  void TableBlock_2_Clear_m825508468_gshared (TableBlock_2_t3756399434 * __this, bool ___needRebuildTable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableBlock_2_Clear_m825508468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Enumerator_t4014815677  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_0);
		Enumerator_t4014815677  L_1 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((List_1_t1634065389 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		V_1 = (Enumerator_t4014815677 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			Il2CppObject * L_2 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Enumerator_t4014815677 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
			V_0 = (Il2CppObject *)L_2;
			NullCheck((InstantiatableInPool_t1882737686 *)(*(&V_0)));
			InstantiatableInPool_Recycle_m1209365942((InstantiatableInPool_t1882737686 *)(*(&V_0)), /*hidden argument*/NULL);
		}

IL_0026:
		{
			bool L_3 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((Enumerator_t4014815677 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
			if (L_3)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_4 = V_1;
		Enumerator_t4014815677  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), &L_5);
		NullCheck((Il2CppObject *)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		List_1_t1634065389 * L_7 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_7);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Object>::Clear() */, (List_1_t1634065389 *)L_7);
		bool L_8 = ___needRebuildTable0;
		if (!L_8)
		{
			goto IL_005f;
		}
	}
	{
		NewTableView_t2775249395 * L_9 = (NewTableView_t2775249395 *)__this->get_newTableView_0();
		NullCheck((NewTableView_t2775249395 *)L_9);
		NewTableView_NeedRebuild_m2127486155((NewTableView_t2775249395 *)L_9, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::<Fill>m__133(UniRx.CollectionAddEvent`1<TData>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t TableBlock_2_U3CFillU3Em__133_m2005993633_MetadataUsageId;
extern "C"  void TableBlock_2_U3CFillU3Em__133_m2005993633_gshared (TableBlock_2_t3756399434 * __this, CollectionAddEvent_1_t2416521987  ___ev0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableBlock_2_U3CFillU3Em__133_m2005993633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Enumerator_t4014815677  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	Enumerator_t4014815677  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Il2CppObject * V_8 = NULL;
	Enumerator_t4014815677  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t1634065389 * G_B11_0 = NULL;
	List_1_t1634065389 * G_B10_0 = NULL;
	List_1_t1634065389 * G_B13_0 = NULL;
	List_1_t1634065389 * G_B12_0 = NULL;
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_0);
		Enumerator_t4014815677  L_1 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((List_1_t1634065389 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		V_1 = (Enumerator_t4014815677 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0053;
		}

IL_0011:
		{
			Il2CppObject * L_2 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Enumerator_t4014815677 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
			V_0 = (Il2CppObject *)L_2;
			Il2CppObject * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = (bool)((NewTableCell_t2774679728 *)L_3)->get_deleted_6();
			if (L_4)
			{
				goto IL_0053;
			}
		}

IL_0029:
		{
			Il2CppObject * L_5 = V_0;
			NullCheck(L_5);
			int32_t L_6 = (int32_t)((NewTableCell_t2774679728 *)L_5)->get_index_7();
			int32_t L_7 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)((CollectionAddEvent_1_t2416521987 *)(&___ev0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
			if ((((int32_t)L_6) < ((int32_t)L_7)))
			{
				goto IL_0053;
			}
		}

IL_0040:
		{
			Il2CppObject * L_8 = V_0;
			Il2CppObject * L_9 = (Il2CppObject *)L_8;
			NullCheck(L_9);
			int32_t L_10 = (int32_t)((NewTableCell_t2774679728 *)L_9)->get_index_7();
			NullCheck(L_9);
			((NewTableCell_t2774679728 *)L_9)->set_index_7(((int32_t)((int32_t)L_10+(int32_t)1)));
		}

IL_0053:
		{
			bool L_11 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((Enumerator_t4014815677 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
			if (L_11)
			{
				goto IL_0011;
			}
		}

IL_005f:
		{
			IL2CPP_LEAVE(0x70, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_12 = V_1;
		Enumerator_t4014815677  L_13 = L_12;
		Il2CppObject * L_14 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), &L_13);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0070:
	{
		List_1_t1634065389 * L_15 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t1634065389 *)L_15);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_02b9;
		}
	}
	{
		List_1_t1634065389 * L_17 = (List_1_t1634065389 *)__this->get_visibleList_4();
		Func_2_t1509682273 * L_18 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cacheC_12();
		G_B10_0 = L_17;
		if (L_18)
		{
			G_B11_0 = L_17;
			goto IL_009f;
		}
	}
	{
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46));
		Func_2_t1509682273 * L_20 = (Func_2_t1509682273 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		((  void (*) (Func_2_t1509682273 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(L_20, (Il2CppObject *)NULL, (IntPtr_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->set_U3CU3Ef__amU24cacheC_12(L_20);
		G_B11_0 = G_B10_0;
	}

IL_009f:
	{
		Func_2_t1509682273 * L_21 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cacheC_12();
		Il2CppObject * L_22 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B11_0, (Func_2_t1509682273 *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		V_2 = (Il2CppObject *)L_22;
		List_1_t1634065389 * L_23 = (List_1_t1634065389 *)__this->get_visibleList_4();
		Func_2_t1509682273 * L_24 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cacheD_13();
		G_B12_0 = L_23;
		if (L_24)
		{
			G_B13_0 = L_23;
			goto IL_00c8;
		}
	}
	{
		IntPtr_t L_25;
		L_25.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48));
		Func_2_t1509682273 * L_26 = (Func_2_t1509682273 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		((  void (*) (Func_2_t1509682273 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(L_26, (Il2CppObject *)NULL, (IntPtr_t)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->set_U3CU3Ef__amU24cacheD_13(L_26);
		G_B13_0 = G_B12_0;
	}

IL_00c8:
	{
		Func_2_t1509682273 * L_27 = ((TableBlock_2_t3756399434_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->static_fields)->get_U3CU3Ef__amU24cacheD_13();
		Il2CppObject * L_28 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49)->method)(NULL /*static, unused*/, (Il2CppObject*)G_B13_0, (Func_2_t1509682273 *)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49));
		V_3 = (Il2CppObject *)L_28;
		int32_t L_29 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)((CollectionAddEvent_1_t2416521987 *)(&___ev0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
		Il2CppObject * L_30 = V_2;
		NullCheck(L_30);
		int32_t L_31 = (int32_t)((NewTableCell_t2774679728 *)L_30)->get_index_7();
		if ((((int32_t)L_29) < ((int32_t)((int32_t)((int32_t)L_31-(int32_t)1)))))
		{
			goto IL_0204;
		}
	}
	{
		int32_t L_32 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)((CollectionAddEvent_1_t2416521987 *)(&___ev0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
		Il2CppObject * L_33 = V_3;
		NullCheck(L_33);
		int32_t L_34 = (int32_t)((NewTableCell_t2774679728 *)L_33)->get_index_7();
		if ((((int32_t)L_32) > ((int32_t)((int32_t)((int32_t)L_34+(int32_t)1)))))
		{
			goto IL_0204;
		}
	}
	{
		NullCheck((TableBlock_2_t3756399434 *)__this);
		Il2CppObject * L_35 = ((  Il2CppObject * (*) (TableBlock_2_t3756399434 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((TableBlock_2_t3756399434 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_4 = (Il2CppObject *)L_35;
		Il2CppObject * L_36 = V_4;
		int32_t L_37 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)((CollectionAddEvent_1_t2416521987 *)(&___ev0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
		NullCheck(L_36);
		((NewTableCell_t2774679728 *)L_36)->set_index_7(L_37);
		Il2CppObject * L_38 = V_4;
		NullCheck(L_38);
		((NewTableCell_t2774679728 *)L_38)->set_deleted_6((bool)0);
		Action_2_t4105459918 * L_39 = (Action_2_t4105459918 *)__this->get__fillMethod_8();
		Il2CppObject * L_40 = V_4;
		Il2CppObject * L_41 = ((  Il2CppObject * (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50)->method)((CollectionAddEvent_1_t2416521987 *)(&___ev0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50));
		NullCheck((Action_2_t4105459918 *)L_39);
		((  void (*) (Action_2_t4105459918 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)->method)((Action_2_t4105459918 *)L_39, (Il2CppObject *)L_40, (Il2CppObject *)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35));
		NullCheck((NewTableCell_t2774679728 *)(*(&V_4)));
		RectTransform_t3317474837 * L_42 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_4)), /*hidden argument*/NULL);
		Vector2_t3525329788  L_43 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t3525329788  L_44 = Vector2_op_Multiply_m2276706051(NULL /*static, unused*/, (Vector2_t3525329788 )L_43, (float)(234234.0f), /*hidden argument*/NULL);
		NullCheck((RectTransform_t3317474837 *)L_42);
		RectTransform_set_anchoredPosition_m1783392546((RectTransform_t3317474837 *)L_42, (Vector2_t3525329788 )L_44, /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)(*(&V_4)));
		Transform_t284553113 * L_45 = Component_get_transform_m2452535634((Component_t2126946602 *)(*(&V_4)), /*hidden argument*/NULL);
		Vector3_t3525329789  L_46 = Vector3_get_zero_m2720511387(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Transform_t284553113 *)L_45);
		Transform_set_localScale_m1776830461((Transform_t284553113 *)L_45, (Vector3_t3525329789 )L_46, /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)(*(&V_4)));
		Transform_t284553113 * L_47 = Component_get_transform_m2452535634((Component_t2126946602 *)(*(&V_4)), /*hidden argument*/NULL);
		Vector3_t3525329789  L_48 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		ShortcutExtensions_DOScale_m4284974744(NULL /*static, unused*/, (Transform_t284553113 *)L_47, (Vector3_t3525329789 )L_48, (float)(0.2f), /*hidden argument*/NULL);
		V_5 = (int32_t)0;
		List_1_t1634065389 * L_49 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_49);
		Enumerator_t4014815677  L_50 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((List_1_t1634065389 *)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		V_7 = (Enumerator_t4014815677 )L_50;
	}

IL_01a6:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01d7;
		}

IL_01ab:
		{
			Il2CppObject * L_51 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Enumerator_t4014815677 *)(&V_7), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
			V_6 = (Il2CppObject *)L_51;
			Il2CppObject * L_52 = V_6;
			NullCheck(L_52);
			int32_t L_53 = (int32_t)((NewTableCell_t2774679728 *)L_52)->get_index_7();
			int32_t L_54 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)((CollectionAddEvent_1_t2416521987 *)(&___ev0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
			if ((((int32_t)L_53) <= ((int32_t)L_54)))
			{
				goto IL_01d1;
			}
		}

IL_01cc:
		{
			goto IL_01e3;
		}

IL_01d1:
		{
			int32_t L_55 = V_5;
			V_5 = (int32_t)((int32_t)((int32_t)L_55+(int32_t)1));
		}

IL_01d7:
		{
			bool L_56 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((Enumerator_t4014815677 *)(&V_7), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
			if (L_56)
			{
				goto IL_01ab;
			}
		}

IL_01e3:
		{
			IL2CPP_LEAVE(0x1F5, FINALLY_01e8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01e8;
	}

FINALLY_01e8:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_57 = V_7;
		Enumerator_t4014815677  L_58 = L_57;
		Il2CppObject * L_59 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), &L_58);
		NullCheck((Il2CppObject *)L_59);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_59);
		IL2CPP_END_FINALLY(488)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(488)
	{
		IL2CPP_JUMP_TBL(0x1F5, IL_01f5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01f5:
	{
		List_1_t1634065389 * L_60 = (List_1_t1634065389 *)__this->get_visibleList_4();
		int32_t L_61 = V_5;
		Il2CppObject * L_62 = V_4;
		NullCheck((List_1_t1634065389 *)L_60);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,!0) */, (List_1_t1634065389 *)L_60, (int32_t)L_61, (Il2CppObject *)L_62);
	}

IL_0204:
	{
		int32_t L_63 = ((  int32_t (*) (CollectionAddEvent_1_t2416521987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)((CollectionAddEvent_1_t2416521987 *)(&___ev0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
		Il2CppObject * L_64 = V_2;
		NullCheck(L_64);
		int32_t L_65 = (int32_t)((NewTableCell_t2774679728 *)L_64)->get_index_7();
		if ((((int32_t)L_63) >= ((int32_t)((int32_t)((int32_t)L_65-(int32_t)1)))))
		{
			goto IL_02b9;
		}
	}
	{
		NewTableView_t2775249395 * L_66 = (NewTableView_t2775249395 *)__this->get_newTableView_0();
		NullCheck(L_66);
		RectTransform_t3317474837 * L_67 = (RectTransform_t3317474837 *)L_66->get_containerRect_6();
		RectTransform_t3317474837 * L_68 = (RectTransform_t3317474837 *)L_67;
		NullCheck((RectTransform_t3317474837 *)L_68);
		Vector2_t3525329788  L_69 = RectTransform_get_anchoredPosition_m2546422825((RectTransform_t3317474837 *)L_68, /*hidden argument*/NULL);
		Vector2_t3525329788  L_70 = Vector2_get_up_m1197831267(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_71 = (float)__this->get_defaultCellSize_3();
		Vector2_t3525329788  L_72 = Vector2_op_Multiply_m2276706051(NULL /*static, unused*/, (Vector2_t3525329788 )L_70, (float)L_71, /*hidden argument*/NULL);
		Vector2_t3525329788  L_73 = Vector2_op_Addition_m2925842746(NULL /*static, unused*/, (Vector2_t3525329788 )L_69, (Vector2_t3525329788 )L_72, /*hidden argument*/NULL);
		NullCheck((RectTransform_t3317474837 *)L_68);
		RectTransform_set_anchoredPosition_m1783392546((RectTransform_t3317474837 *)L_68, (Vector2_t3525329788 )L_73, /*hidden argument*/NULL);
		List_1_t1634065389 * L_74 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_74);
		Enumerator_t4014815677  L_75 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((List_1_t1634065389 *)L_74, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		V_9 = (Enumerator_t4014815677 )L_75;
	}

IL_0255:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0290;
		}

IL_025a:
		{
			Il2CppObject * L_76 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Enumerator_t4014815677 *)(&V_9), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
			V_8 = (Il2CppObject *)L_76;
			NullCheck((NewTableCell_t2774679728 *)(*(&V_8)));
			RectTransform_t3317474837 * L_77 = NewTableCell_get_rect_m2800814120((NewTableCell_t2774679728 *)(*(&V_8)), /*hidden argument*/NULL);
			RectTransform_t3317474837 * L_78 = (RectTransform_t3317474837 *)L_77;
			NullCheck((RectTransform_t3317474837 *)L_78);
			Vector2_t3525329788  L_79 = RectTransform_get_anchoredPosition_m2546422825((RectTransform_t3317474837 *)L_78, /*hidden argument*/NULL);
			Vector2_t3525329788  L_80 = Vector2_get_up_m1197831267(NULL /*static, unused*/, /*hidden argument*/NULL);
			float L_81 = (float)__this->get_defaultCellSize_3();
			Vector2_t3525329788  L_82 = Vector2_op_Multiply_m2276706051(NULL /*static, unused*/, (Vector2_t3525329788 )L_80, (float)L_81, /*hidden argument*/NULL);
			Vector2_t3525329788  L_83 = Vector2_op_Subtraction_m1346777296(NULL /*static, unused*/, (Vector2_t3525329788 )L_79, (Vector2_t3525329788 )L_82, /*hidden argument*/NULL);
			NullCheck((RectTransform_t3317474837 *)L_78);
			RectTransform_set_anchoredPosition_m1783392546((RectTransform_t3317474837 *)L_78, (Vector2_t3525329788 )L_83, /*hidden argument*/NULL);
		}

IL_0290:
		{
			bool L_84 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((Enumerator_t4014815677 *)(&V_9), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
			if (L_84)
			{
				goto IL_025a;
			}
		}

IL_029c:
		{
			IL2CPP_LEAVE(0x2AE, FINALLY_02a1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_02a1;
	}

FINALLY_02a1:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_85 = V_9;
		Enumerator_t4014815677  L_86 = L_85;
		Il2CppObject * L_87 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), &L_86);
		NullCheck((Il2CppObject *)L_87);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_87);
		IL2CPP_END_FINALLY(673)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(673)
	{
		IL2CPP_JUMP_TBL(0x2AE, IL_02ae)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_02ae:
	{
		NewTableView_t2775249395 * L_88 = (NewTableView_t2775249395 *)__this->get_newTableView_0();
		NullCheck((NewTableView_t2775249395 *)L_88);
		NewTableView_Rebuild_m4014602049((NewTableView_t2775249395 *)L_88, /*hidden argument*/NULL);
	}

IL_02b9:
	{
		return;
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::<Fill>m__134(CollectionRemoveEvent`1<TData>)
extern Il2CppClass* TweenCallback_t3786476454_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438_MethodInfo_var;
extern const uint32_t TableBlock_2_U3CFillU3Em__134_m2215213507_MetadataUsageId;
extern "C"  void TableBlock_2_U3CFillU3Em__134_m2215213507_gshared (TableBlock_2_t3756399434 * __this, CollectionRemoveEvent_1_t898259258 * ___ev0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableBlock_2_U3CFillU3Em__134_m2215213507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Enumerator_t4014815677  V_1;
	memset(&V_1, 0, sizeof(V_1));
	U3CFillU3Ec__AnonStoreyEB_t2250867686 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_0 = (U3CFillU3Ec__AnonStoreyEB_t2250867686 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 51));
		((  void (*) (U3CFillU3Ec__AnonStoreyEB_t2250867686 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52));
		V_2 = (U3CFillU3Ec__AnonStoreyEB_t2250867686 *)L_0;
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_1 = V_2;
		CollectionRemoveEvent_1_t898259258 * L_2 = ___ev0;
		NullCheck(L_1);
		L_1->set_ev_0(L_2);
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_3 = V_2;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_4 = V_2;
		List_1_t1634065389 * L_5 = (List_1_t1634065389 *)__this->get_visibleList_4();
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_6 = V_2;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 53));
		Func_2_t1509682273 * L_8 = (Func_2_t1509682273 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		((  void (*) (Func_2_t1509682273 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(NULL /*static, unused*/, (Il2CppObject*)L_5, (Func_2_t1509682273 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		NullCheck(L_4);
		L_4->set_prefab_1(L_9);
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_10 = V_2;
		NullCheck(L_10);
		Il2CppObject * L_11 = (Il2CppObject *)L_10->get_prefab_1();
		bool L_12 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, (Object_t3878351788 *)L_11, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_008a;
		}
	}
	{
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_13 = V_2;
		NullCheck(L_13);
		Il2CppObject * L_14 = (Il2CppObject *)L_13->get_prefab_1();
		NullCheck(L_14);
		((NewTableCell_t2774679728 *)L_14)->set_deleted_6((bool)1);
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_15 = V_2;
		NullCheck(L_15);
		Il2CppObject ** L_16 = (Il2CppObject **)L_15->get_address_of_prefab_1();
		NullCheck((Component_t2126946602 *)(*L_16));
		Transform_t284553113 * L_17 = Component_get_transform_m2452535634((Component_t2126946602 *)(*L_16), /*hidden argument*/NULL);
		Tweener_t1766303790 * L_18 = ShortcutExtensions_DOScaleY_m2850359017(NULL /*static, unused*/, (Transform_t284553113 *)L_17, (float)(0.0f), (float)(0.2f), /*hidden argument*/NULL);
		U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_19 = V_2;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 54));
		TweenCallback_t3786476454 * L_21 = (TweenCallback_t3786476454 *)il2cpp_codegen_object_new(TweenCallback_t3786476454_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3440504173(L_21, (Il2CppObject *)L_19, (IntPtr_t)L_20, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438(NULL /*static, unused*/, (Tweener_t1766303790 *)L_18, (TweenCallback_t3786476454 *)L_21, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t1766303790_m3265777438_MethodInfo_var);
	}

IL_008a:
	{
		List_1_t1634065389 * L_22 = (List_1_t1634065389 *)__this->get_visibleList_4();
		NullCheck((List_1_t1634065389 *)L_22);
		Enumerator_t4014815677  L_23 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((List_1_t1634065389 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		V_1 = (Enumerator_t4014815677 )L_23;
	}

IL_0096:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e3;
		}

IL_009b:
		{
			Il2CppObject * L_24 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Enumerator_t4014815677 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
			V_0 = (Il2CppObject *)L_24;
			Il2CppObject * L_25 = V_0;
			NullCheck(L_25);
			int32_t L_26 = (int32_t)((NewTableCell_t2774679728 *)L_25)->get_index_7();
			U3CFillU3Ec__AnonStoreyEB_t2250867686 * L_27 = V_2;
			NullCheck(L_27);
			CollectionRemoveEvent_1_t898259258 * L_28 = (CollectionRemoveEvent_1_t898259258 *)L_27->get_ev_0();
			NullCheck((CollectionRemoveEvent_1_t898259258 *)L_28);
			int32_t L_29 = ((  int32_t (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 55)->method)((CollectionRemoveEvent_1_t898259258 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 55));
			if ((((int32_t)L_26) <= ((int32_t)((int32_t)((int32_t)L_29-(int32_t)1)))))
			{
				goto IL_00e3;
			}
		}

IL_00c0:
		{
			Il2CppObject * L_30 = V_0;
			NullCheck(L_30);
			bool L_31 = (bool)((NewTableCell_t2774679728 *)L_30)->get_deleted_6();
			if (L_31)
			{
				goto IL_00e3;
			}
		}

IL_00d0:
		{
			Il2CppObject * L_32 = V_0;
			Il2CppObject * L_33 = (Il2CppObject *)L_32;
			NullCheck(L_33);
			int32_t L_34 = (int32_t)((NewTableCell_t2774679728 *)L_33)->get_index_7();
			NullCheck(L_33);
			((NewTableCell_t2774679728 *)L_33)->set_index_7(((int32_t)((int32_t)L_34-(int32_t)1)));
		}

IL_00e3:
		{
			bool L_35 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((Enumerator_t4014815677 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
			if (L_35)
			{
				goto IL_009b;
			}
		}

IL_00ef:
		{
			IL2CPP_LEAVE(0x100, FINALLY_00f4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00f4;
	}

FINALLY_00f4:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_36 = V_1;
		Enumerator_t4014815677  L_37 = L_36;
		Il2CppObject * L_38 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42), &L_37);
		NullCheck((Il2CppObject *)L_38);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_38);
		IL2CPP_END_FINALLY(244)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(244)
	{
		IL2CPP_JUMP_TBL(0x100, IL_0100)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0100:
	{
		return;
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::<Fill>m__135(UniRx.Unit)
extern "C"  void TableBlock_2_U3CFillU3Em__135_m3174864091_gshared (TableBlock_2_t3756399434 * __this, Unit_t2558286038  ___ev0, const MethodInfo* method)
{
	{
		NullCheck((TableBlock_2_t3756399434 *)__this);
		((  void (*) (TableBlock_2_t3756399434 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TableBlock_2_t3756399434 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return;
	}
}
// System.Void TableBlock`2<System.Object,System.Object>::<Fill>m__136(CollectionReplaceEvent`1<TData>)
extern "C"  void TableBlock_2_U3CFillU3Em__136_m3982713043_gshared (TableBlock_2_t3756399434 * __this, CollectionReplaceEvent_1_t2497372070 * ___ev0, const MethodInfo* method)
{
	{
		NullCheck((TableBlock_2_t3756399434 *)__this);
		((  void (*) (TableBlock_2_t3756399434 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((TableBlock_2_t3756399434 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return;
	}
}
// System.Single TableBlock`2<System.Object,System.Object>::<GetSize>m__137(TPrefab)
extern "C"  float TableBlock_2_U3CGetSizeU3Em__137_m822119827_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method)
{
	{
		NullCheck((NewTableCell_t2774679728 *)(*(&___prefab0)));
		float L_0 = NewTableCell_GetSize_m686112628((NewTableCell_t2774679728 *)(*(&___prefab0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean TableBlock`2<System.Object,System.Object>::<GetSize>m__138(TPrefab)
extern "C"  bool TableBlock_2_U3CGetSizeU3Em__138_m2967880650_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___prefab0;
		NullCheck(L_0);
		bool L_1 = (bool)((NewTableCell_t2774679728 *)L_0)->get_deleted_6();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean TableBlock`2<System.Object,System.Object>::<TryShowCells>m__139(TPrefab)
extern "C"  bool TableBlock_2_U3CTryShowCellsU3Em__139_m1794268649_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___prefab0;
		NullCheck(L_0);
		bool L_1 = (bool)((NewTableCell_t2774679728 *)L_0)->get_deleted_6();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean TableBlock`2<System.Object,System.Object>::<Fill>m__13A(TPrefab)
extern "C"  bool TableBlock_2_U3CFillU3Em__13A_m3090221195_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___prefab0;
		NullCheck(L_0);
		bool L_1 = (bool)((NewTableCell_t2774679728 *)L_0)->get_deleted_6();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean TableBlock`2<System.Object,System.Object>::<Fill>m__13B(TPrefab)
extern "C"  bool TableBlock_2_U3CFillU3Em__13B_m2893707690_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___prefab0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___prefab0;
		NullCheck(L_0);
		bool L_1 = (bool)((NewTableCell_t2774679728 *)L_0)->get_deleted_6();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>::.ctor()
extern "C"  void U3CCalculateU3Ec__AnonStorey14A_1__ctor_m1769840975_gshared (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>::<>m__1F9(ICell)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* ICell_t69513547_il2cpp_TypeInfo_var;
extern const uint32_t U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1F9_m3542568358_MetadataUsageId;
extern "C"  Il2CppObject * U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1F9_m3542568358_gshared (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1F9_m3542568358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___cell0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_t437523947 * L_2 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_2, (Il2CppObject *)__this, (IntPtr_t)L_1, /*hidden argument*/NULL);
		int32_t L_3 = (int32_t)__this->get_p_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable ICell::OnChanged(System.Action,Priority) */, ICell_t69513547_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (Action_t437523947 *)L_2, (int32_t)L_3);
		return L_4;
	}
}
// System.Void Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>::<>m__1FA()
extern "C"  void U3CCalculateU3Ec__AnonStorey14A_1_U3CU3Em__1FA_m3144402486_gshared (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * L_0 = (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 *)__this->get_U3CU3Ef__refU24329_3();
		NullCheck(L_0);
		Func_1_t1979887667 * L_1 = (Func_1_t1979887667 *)L_0->get_formula_1();
		NullCheck((Func_1_t1979887667 *)L_1);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Func_1_t1979887667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (Il2CppObject *)L_2;
		CellJoinDisposable_1_t3736493403 * L_3 = (CellJoinDisposable_1_t3736493403 *)__this->get_group_1();
		NullCheck(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)L_3->get_lastValue_2();
		Il2CppObject * L_5 = V_0;
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)__this->get_reaction_2();
		Il2CppObject * L_8 = V_0;
		NullCheck((Action_1_t985559125 *)L_7);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Action_1_t985559125 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		CellJoinDisposable_1_t3736493403 * L_9 = (CellJoinDisposable_1_t3736493403 *)__this->get_group_1();
		Il2CppObject * L_10 = V_0;
		NullCheck(L_9);
		L_9->set_lastValue_2(L_10);
	}

IL_0049:
	{
		return;
	}
}
// System.Void Transaction/<Calculate>c__AnonStorey149`1<System.Object>::.ctor()
extern "C"  void U3CCalculateU3Ec__AnonStorey149_1__ctor_m290250690_gshared (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable Transaction/<Calculate>c__AnonStorey149`1<System.Object>::<>m__1EA(System.Action`1<T>,Priority)
extern Il2CppClass* Func_2_t3366118735_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_2__ctor_m51924019_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisICell_t69513547_TisIDisposable_t1628921374_m824486475_MethodInfo_var;
extern const uint32_t U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EA_m2078672075_MetadataUsageId;
extern "C"  Il2CppObject * U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EA_m2078672075_gshared (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EA_m2078672075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * V_0 = NULL;
	{
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_0 = (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 *)L_0;
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24329_3(__this);
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_2 = V_0;
		int32_t L_3 = ___p1;
		NullCheck(L_2);
		L_2->set_p_0(L_3);
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_4 = V_0;
		Action_1_t985559125 * L_5 = ___reaction0;
		NullCheck(L_4);
		L_4->set_reaction_2(L_5);
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_6 = V_0;
		CellJoinDisposable_1_t3736493403 * L_7 = (CellJoinDisposable_1_t3736493403 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (CellJoinDisposable_1_t3736493403 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck(L_6);
		L_6->set_group_1(L_7);
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_8 = V_0;
		NullCheck(L_8);
		CellJoinDisposable_1_t3736493403 * L_9 = (CellJoinDisposable_1_t3736493403 *)L_8->get_group_1();
		List_1_t866472516 * L_10 = (List_1_t866472516 *)__this->get_copyTouched_0();
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t3366118735 * L_13 = (Func_2_t3366118735 *)il2cpp_codegen_object_new(Func_2_t3366118735_il2cpp_TypeInfo_var);
		Func_2__ctor_m51924019(L_13, (Il2CppObject *)L_11, (IntPtr_t)L_12, /*hidden argument*/Func_2__ctor_m51924019_MethodInfo_var);
		Il2CppObject* L_14 = Enumerable_Select_TisICell_t69513547_TisIDisposable_t1628921374_m824486475(NULL /*static, unused*/, (Il2CppObject*)L_10, (Func_2_t3366118735 *)L_13, /*hidden argument*/Enumerable_Select_TisICell_t69513547_TisIDisposable_t1628921374_m824486475_MethodInfo_var);
		NullCheck((ListDisposable_t2393830995 *)L_9);
		ListDisposable_SetArray_m1080170738((ListDisposable_t2393830995 *)L_9, (Il2CppObject*)L_14, /*hidden argument*/NULL);
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_15 = V_0;
		NullCheck(L_15);
		CellJoinDisposable_1_t3736493403 * L_16 = (CellJoinDisposable_1_t3736493403 *)L_15->get_group_1();
		Func_1_t1979887667 * L_17 = (Func_1_t1979887667 *)__this->get_formula_1();
		NullCheck((Func_1_t1979887667 *)L_17);
		Il2CppObject * L_18 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_1_t1979887667 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck(L_16);
		L_16->set_lastValue_2(L_18);
		U3CCalculateU3Ec__AnonStorey14A_1_t2571930805 * L_19 = V_0;
		NullCheck(L_19);
		CellJoinDisposable_1_t3736493403 * L_20 = (CellJoinDisposable_1_t3736493403 *)L_19->get_group_1();
		return L_20;
	}
}
// T Transaction/<Calculate>c__AnonStorey149`1<System.Object>::<>m__1EB()
extern "C"  Il2CppObject * U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EB_m1173453380_gshared (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * __this, const MethodInfo* method)
{
	{
		Func_1_t1979887667 * L_0 = (Func_1_t1979887667 *)__this->get_formula_1();
		NullCheck((Func_1_t1979887667 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Func_1_t1979887667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Func_1_t1979887667 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::.ctor()
extern "C"  void U3CAsSafeEnumerableU3Ec__Iterator1D_1__ctor_m2146879469_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m900220374_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3808416163_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m54988620_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CAsSafeEnumerableU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m934906879_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method)
{
	U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * L_2 = (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 *)L_2;
		U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsSafeEnumerableU3Ec__Iterator1D_1_MoveNext_m1829913015_MetadataUsageId;
extern "C"  bool U3CAsSafeEnumerableU3Ec__Iterator1D_1_MoveNext_m1829913015_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsSafeEnumerableU3Ec__Iterator1D_1_MoveNext_m1829913015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_00b4;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CeU3E__0_1(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CeU3E__0_1();
		__this->set_U3CU24s_338U3E__1_2(((Il2CppObject *)IsInst(L_4, IDisposable_t1628921374_il2cpp_TypeInfo_var)));
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0048:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_007d;
			}
		}

IL_0054:
		{
			goto IL_007d;
		}

IL_0059:
		{
			Il2CppObject * L_6 = (Il2CppObject *)__this->get_U3CeU3E__0_1();
			NullCheck((Il2CppObject *)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			__this->set_U24current_4(((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))));
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB6, FINALLY_0092);
		}

IL_007d:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CeU3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_0059;
			}
		}

IL_008d:
		{
			IL2CPP_LEAVE(0xAD, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0096;
			}
		}

IL_0095:
		{
			IL2CPP_END_FINALLY(146)
		}

IL_0096:
		{
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CU24s_338U3E__1_2();
			if (!L_11)
			{
				goto IL_00ac;
			}
		}

IL_00a1:
		{
			Il2CppObject * L_12 = (Il2CppObject *)__this->get_U3CU24s_338U3E__1_2();
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		}

IL_00ac:
		{
			IL2CPP_END_FINALLY(146)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0xB6, IL_00b6)
		IL2CPP_JUMP_TBL(0xAD, IL_00ad)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00ad:
	{
		__this->set_U24PC_3((-1));
	}

IL_00b4:
	{
		return (bool)0;
	}

IL_00b6:
	{
		return (bool)1;
	}
	// Dead block : IL_00b8: ldloc.2
}
// System.Void UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsSafeEnumerableU3Ec__Iterator1D_1_Dispose_m1465150698_MetadataUsageId;
extern "C"  void U3CAsSafeEnumerableU3Ec__Iterator1D_1_Dispose_m1465150698_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsSafeEnumerableU3Ec__Iterator1D_1_Dispose_m1465150698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_338U3E__1_2();
			if (!L_2)
			{
				goto IL_003c;
			}
		}

IL_0031:
		{
			Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CU24s_338U3E__1_2();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsSafeEnumerableU3Ec__Iterator1D_1_Reset_m4088279706_MetadataUsageId;
extern "C"  void U3CAsSafeEnumerableU3Ec__Iterator1D_1_Reset_m4088279706_gshared (U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsSafeEnumerableU3Ec__Iterator1D_1_Reset_m4088279706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.AsyncOperationExtensions/<AsAsyncOperationObservable>c__AnonStorey7C`1<System.Object>::.ctor()
extern "C"  void U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1__ctor_m1425434309_gshared (U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_t469001995 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.AsyncOperationExtensions/<AsAsyncOperationObservable>c__AnonStorey7C`1<System.Object>::<>m__A4(UniRx.IObserver`1<T>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_U3CU3Em__A4_m3523013009_gshared (U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_t469001995 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_asyncOperation_0();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_progress_1();
		CancellationToken_t1439151560  L_3 = ___cancellation1;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject*, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject*)L_1, (Il2CppObject*)L_2, (CancellationToken_t1439151560 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_4;
	}
}
// System.Void UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::.ctor()
extern "C"  void U3CAsObservableCoreU3Ec__Iterator1E_1__ctor_m3313671467_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAsObservableCoreU3Ec__Iterator1E_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1809234951_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsObservableCoreU3Ec__Iterator1E_1_System_Collections_IEnumerator_get_Current_m4042763675_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::MoveNext()
extern Il2CppClass* IProgress_1_t4173802291_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsObservableCoreU3Ec__Iterator1E_1_MoveNext_m4170056961_MetadataUsageId;
extern "C"  bool U3CAsObservableCoreU3Ec__Iterator1E_1_MoveNext_m4170056961_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsObservableCoreU3Ec__Iterator1E_1_MoveNext_m4170056961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	bool V_3 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_008c;
		}
		if (L_1 == 2)
		{
			goto IL_00ea;
		}
	}
	{
		goto IL_0171;
	}

IL_0025:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_reportProgress_0();
		if (!L_2)
		{
			goto IL_00b7;
		}
	}
	{
		goto IL_008c;
	}

IL_0035:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_3 = (Il2CppObject*)__this->get_reportProgress_0();
		Il2CppObject ** L_4 = (Il2CppObject **)__this->get_address_of_asyncOperation_1();
		NullCheck((AsyncOperation_t3374395064 *)(*L_4));
		float L_5 = AsyncOperation_get_progress_m2178550628((AsyncOperation_t3374395064 *)(*L_4), /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, (Il2CppObject*)L_3, (float)L_5);
		goto IL_0079;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0056;
		throw e;
	}

CATCH_0056:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_6 = V_1;
			__this->set_U3CexU3E__0_4(L_6);
			Il2CppObject* L_7 = (Il2CppObject*)__this->get_observer_3();
			Exception_t1967233988 * L_8 = (Exception_t1967233988 *)__this->get_U3CexU3E__0_4();
			NullCheck((Il2CppObject*)L_7);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_7, (Exception_t1967233988 *)L_8);
			goto IL_0171;
		}

IL_0074:
		{
			; // IL_0074: leave IL_0079
		}
	} // end catch (depth: 1)

IL_0079:
	{
		__this->set_U24current_7(NULL);
		__this->set_U24PC_6(1);
		goto IL_0173;
	}

IL_008c:
	{
		Il2CppObject ** L_9 = (Il2CppObject **)__this->get_address_of_asyncOperation_1();
		NullCheck((AsyncOperation_t3374395064 *)(*L_9));
		bool L_10 = AsyncOperation_get_isDone_m2747591837((AsyncOperation_t3374395064 *)(*L_9), /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00b2;
		}
	}
	{
		CancellationToken_t1439151560 * L_11 = (CancellationToken_t1439151560 *)__this->get_address_of_cancel_2();
		bool L_12 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0035;
		}
	}

IL_00b2:
	{
		goto IL_00ea;
	}

IL_00b7:
	{
		Il2CppObject ** L_13 = (Il2CppObject **)__this->get_address_of_asyncOperation_1();
		NullCheck((AsyncOperation_t3374395064 *)(*L_13));
		bool L_14 = AsyncOperation_get_isDone_m2747591837((AsyncOperation_t3374395064 *)(*L_13), /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00ea;
		}
	}
	{
		Il2CppObject * L_15 = (Il2CppObject *)__this->get_asyncOperation_1();
		__this->set_U24current_7(L_15);
		__this->set_U24PC_6(2);
		goto IL_0173;
	}

IL_00ea:
	{
		CancellationToken_t1439151560 * L_16 = (CancellationToken_t1439151560 *)__this->get_address_of_cancel_2();
		bool L_17 = CancellationToken_get_IsCancellationRequested_m1021497867((CancellationToken_t1439151560 *)L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00ff;
		}
	}
	{
		goto IL_0171;
	}

IL_00ff:
	{
		Il2CppObject* L_18 = (Il2CppObject*)__this->get_reportProgress_0();
		if (!L_18)
		{
			goto IL_014e;
		}
	}

IL_010a:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_19 = (Il2CppObject*)__this->get_reportProgress_0();
		Il2CppObject ** L_20 = (Il2CppObject **)__this->get_address_of_asyncOperation_1();
		NullCheck((AsyncOperation_t3374395064 *)(*L_20));
		float L_21 = AsyncOperation_get_progress_m2178550628((AsyncOperation_t3374395064 *)(*L_20), /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_19);
		InterfaceActionInvoker1< float >::Invoke(0 /* System.Void UniRx.IProgress`1<System.Single>::Report(T) */, IProgress_1_t4173802291_il2cpp_TypeInfo_var, (Il2CppObject*)L_19, (float)L_21);
		goto IL_014e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_012b;
		throw e;
	}

CATCH_012b:
	{ // begin catch(System.Exception)
		{
			V_2 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_22 = V_2;
			__this->set_U3CexU3E__1_5(L_22);
			Il2CppObject* L_23 = (Il2CppObject*)__this->get_observer_3();
			Exception_t1967233988 * L_24 = (Exception_t1967233988 *)__this->get_U3CexU3E__1_5();
			NullCheck((Il2CppObject*)L_23);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_23, (Exception_t1967233988 *)L_24);
			goto IL_0171;
		}

IL_0149:
		{
			; // IL_0149: leave IL_014e
		}
	} // end catch (depth: 1)

IL_014e:
	{
		Il2CppObject* L_25 = (Il2CppObject*)__this->get_observer_3();
		Il2CppObject * L_26 = (Il2CppObject *)__this->get_asyncOperation_1();
		NullCheck((Il2CppObject*)L_25);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_25, (Il2CppObject *)L_26);
		Il2CppObject* L_27 = (Il2CppObject*)__this->get_observer_3();
		NullCheck((Il2CppObject*)L_27);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_27);
		__this->set_U24PC_6((-1));
	}

IL_0171:
	{
		return (bool)0;
	}

IL_0173:
	{
		return (bool)1;
	}
	// Dead block : IL_0175: ldloc.3
}
// System.Void UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::Dispose()
extern "C"  void U3CAsObservableCoreU3Ec__Iterator1E_1_Dispose_m1765796520_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsObservableCoreU3Ec__Iterator1E_1_Reset_m960104408_MetadataUsageId;
extern "C"  void U3CAsObservableCoreU3Ec__Iterator1E_1_Reset_m960104408_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsObservableCoreU3Ec__Iterator1E_1_Reset_m960104408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.AsyncSubject`1/Subscription<System.Object>::.ctor(UniRx.AsyncSubject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m522143833_MetadataUsageId;
extern "C"  void Subscription__ctor_m522143833_gshared (Subscription_t2911320238 * __this, AsyncSubject_1_t1536745524 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m522143833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		AsyncSubject_1_t1536745524 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.AsyncSubject`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m3345175043_gshared (Subscription_t2911320238 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1131724091 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			AsyncSubject_1_t1536745524 * L_2 = (AsyncSubject_1_t1536745524 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			AsyncSubject_1_t1536745524 * L_3 = (AsyncSubject_1_t1536745524 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				AsyncSubject_1_t1536745524 * L_6 = (AsyncSubject_1_t1536745524 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_6();
				V_2 = (ListObserver_1_t1131724091 *)((ListObserver_1_t1131724091 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1131724091 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				AsyncSubject_1_t1536745524 * L_9 = (AsyncSubject_1_t1536745524 *)__this->get_parent_1();
				ListObserver_1_t1131724091 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1131724091 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1131724091 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_6(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				AsyncSubject_1_t1536745524 * L_13 = (AsyncSubject_1_t1536745524 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t246459410 * L_14 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_6(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((AsyncSubject_1_t1536745524 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.AsyncSubject`1/Subscription<UniRx.Unit>::.ctor(UniRx.AsyncSubject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1842391409_MetadataUsageId;
extern "C"  void Subscription__ctor_m1842391409_gshared (Subscription_t337532560 * __this, AsyncSubject_1_t3257925142 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1842391409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		AsyncSubject_1_t3257925142 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.AsyncSubject`1/Subscription<UniRx.Unit>::Dispose()
extern "C"  void Subscription_Dispose_m244388123_gshared (Subscription_t337532560 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2852903709 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			AsyncSubject_1_t3257925142 * L_2 = (AsyncSubject_1_t3257925142 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			AsyncSubject_1_t3257925142 * L_3 = (AsyncSubject_1_t3257925142 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				AsyncSubject_1_t3257925142 * L_6 = (AsyncSubject_1_t3257925142 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_6();
				V_2 = (ListObserver_1_t2852903709 *)((ListObserver_1_t2852903709 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2852903709 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				AsyncSubject_1_t3257925142 * L_9 = (AsyncSubject_1_t3257925142 *)__this->get_parent_1();
				ListObserver_1_t2852903709 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2852903709 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2852903709 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2852903709 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_6(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				AsyncSubject_1_t3257925142 * L_13 = (AsyncSubject_1_t3257925142 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1967639028 * L_14 = ((EmptyObserver_1_t1967639028_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_6(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((AsyncSubject_1_t3257925142 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.AsyncSubject`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t AsyncSubject_1__ctor_m3444076052_MetadataUsageId;
extern "C"  void AsyncSubject_1__ctor_m3444076052_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1__ctor_m3444076052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t246459410 * L_1 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_6(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T UniRx.AsyncSubject`1<System.Object>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral281548416;
extern const uint32_t AsyncSubject_1_get_Value_m3880262777_MetadataUsageId;
extern "C"  Il2CppObject * AsyncSubject_1_get_Value_m3880262777_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_get_Value_m3880262777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((AsyncSubject_1_t1536745524 *)__this);
		((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = (bool)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral281548416, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001c:
	{
		Exception_t1967233988 * L_2 = (Exception_t1967233988 *)__this->get_lastError_5();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Exception_t1967233988 * L_3 = (Exception_t1967233988 *)__this->get_lastError_5();
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002e:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_lastValue_1();
		return L_4;
	}
}
// System.Boolean UniRx.AsyncSubject`1<System.Object>::get_HasObservers()
extern "C"  bool AsyncSubject_1_get_HasObservers_m4197289792_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_6();
		if (((EmptyObserver_1_t246459410 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_3();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_4();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean UniRx.AsyncSubject`1<System.Object>::get_IsCompleted()
extern "C"  bool AsyncSubject_1_get_IsCompleted_m4253247846_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isStopped_3();
		return L_0;
	}
}
// System.Void UniRx.AsyncSubject`1<System.Object>::OnCompleted()
extern "C"  void AsyncSubject_1_OnCompleted_m933610142_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_3 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_3;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((AsyncSubject_1_t1536745524 *)__this);
			((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_2 = (bool)__this->get_isStopped_3();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x74, FINALLY_004f);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_6();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t246459410 * L_4 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_6(L_4);
			__this->set_isStopped_3((bool)1);
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_lastValue_1();
			V_1 = (Il2CppObject *)L_5;
			bool L_6 = (bool)__this->get_hasValue_2();
			V_2 = (bool)L_6;
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_3;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0056:
	{
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_006e;
		}
	}
	{
		Il2CppObject* L_9 = V_0;
		Il2CppObject * L_10 = V_1;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Il2CppObject *)L_10);
		Il2CppObject* L_11 = V_0;
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
		goto IL_0074;
	}

IL_006e:
	{
		Il2CppObject* L_12 = V_0;
		NullCheck((Il2CppObject*)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_12);
	}

IL_0074:
	{
		return;
	}
}
// System.Void UniRx.AsyncSubject`1<System.Object>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t AsyncSubject_1_OnError_m2224781003_MetadataUsageId;
extern "C"  void AsyncSubject_1_OnError_m2224781003_gshared (AsyncSubject_1_t1536745524 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_OnError_m2224781003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((AsyncSubject_1_t1536745524 *)__this);
			((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_4 = (bool)__this->get_isStopped_3();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_6();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t246459410 * L_6 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_6(L_6);
			__this->set_isStopped_3((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_5(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.AsyncSubject`1<System.Object>::OnNext(T)
extern "C"  void AsyncSubject_1_OnNext_m1168004028_gshared (AsyncSubject_1_t1536745524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((AsyncSubject_1_t1536745524 *)__this);
			((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_2 = (bool)__this->get_isStopped_3();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		}

IL_0023:
		{
			__this->set_hasValue_2((bool)1);
			Il2CppObject * L_3 = ___value0;
			__this->set_lastValue_1(L_3);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		return;
	}
}
// System.IDisposable UniRx.AsyncSubject`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t AsyncSubject_1_Subscribe_m3526656105_MetadataUsageId;
extern "C"  Il2CppObject * AsyncSubject_1_Subscribe_m3526656105_gshared (AsyncSubject_1_t1536745524 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_Subscribe_m3526656105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	ListObserver_1_t1131724091 * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_6));
		Il2CppObject * L_2 = V_6;
		V_1 = (Il2CppObject *)L_2;
		V_2 = (bool)0;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_observerLock_0();
		V_3 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_3;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((AsyncSubject_1_t1536745524 *)__this);
			((  void (*) (AsyncSubject_1_t1536745524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t1536745524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_5 = (bool)__this->get_isStopped_3();
			if (L_5)
			{
				goto IL_00b2;
			}
		}

IL_003e:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_outObserver_6();
			V_4 = (ListObserver_1_t1131724091 *)((ListObserver_1_t1131724091 *)IsInst(L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t1131724091 * L_7 = V_4;
			if (!L_7)
			{
				goto IL_0065;
			}
		}

IL_0052:
		{
			ListObserver_1_t1131724091 * L_8 = V_4;
			Il2CppObject* L_9 = ___observer0;
			NullCheck((ListObserver_1_t1131724091 *)L_8);
			Il2CppObject* L_10 = ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t1131724091 *)L_8, (Il2CppObject*)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_6(L_10);
			goto IL_00a4;
		}

IL_0065:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_outObserver_6();
			V_5 = (Il2CppObject*)L_11;
			Il2CppObject* L_12 = V_5;
			if (!((EmptyObserver_1_t246459410 *)IsInst(L_12, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
			{
				goto IL_0085;
			}
		}

IL_0079:
		{
			Il2CppObject* L_13 = ___observer0;
			__this->set_outObserver_6(L_13);
			goto IL_00a4;
		}

IL_0085:
		{
			IObserver_1U5BU5D_t3998655818* L_14 = (IObserver_1U5BU5D_t3998655818*)((IObserver_1U5BU5D_t3998655818*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_15 = V_5;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
			ArrayElementTypeCheck (L_14, L_15);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_15);
			IObserver_1U5BU5D_t3998655818* L_16 = (IObserver_1U5BU5D_t3998655818*)L_14;
			Il2CppObject* L_17 = ___observer0;
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
			ArrayElementTypeCheck (L_16, L_17);
			(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_17);
			ImmutableList_1_t4109309822 * L_18 = (ImmutableList_1_t4109309822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t4109309822 *, IObserver_1U5BU5D_t3998655818*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_18, (IObserver_1U5BU5D_t3998655818*)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t1131724091 * L_19 = (ListObserver_1_t1131724091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t1131724091 *, ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_19, (ImmutableList_1_t4109309822 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_6(L_19);
		}

IL_00a4:
		{
			Il2CppObject* L_20 = ___observer0;
			Subscription_t2911320238 * L_21 = (Subscription_t2911320238 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t2911320238 *, AsyncSubject_1_t1536745524 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_21, (AsyncSubject_1_t1536745524 *)__this, (Il2CppObject*)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_7 = (Il2CppObject *)L_21;
			IL2CPP_LEAVE(0x109, FINALLY_00cc);
		}

IL_00b2:
		{
			Exception_t1967233988 * L_22 = (Exception_t1967233988 *)__this->get_lastError_5();
			V_0 = (Exception_t1967233988 *)L_22;
			Il2CppObject * L_23 = (Il2CppObject *)__this->get_lastValue_1();
			V_1 = (Il2CppObject *)L_23;
			bool L_24 = (bool)__this->get_hasValue_2();
			V_2 = (bool)L_24;
			IL2CPP_LEAVE(0xD3, FINALLY_00cc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00cc;
	}

FINALLY_00cc:
	{ // begin finally (depth: 1)
		Il2CppObject * L_25 = V_3;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_25, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(204)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(204)
	{
		IL2CPP_JUMP_TBL(0x109, IL_0109)
		IL2CPP_JUMP_TBL(0xD3, IL_00d3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d3:
	{
		Exception_t1967233988 * L_26 = V_0;
		if (!L_26)
		{
			goto IL_00e5;
		}
	}
	{
		Il2CppObject* L_27 = ___observer0;
		Exception_t1967233988 * L_28 = V_0;
		NullCheck((Il2CppObject*)L_27);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_27, (Exception_t1967233988 *)L_28);
		goto IL_0103;
	}

IL_00e5:
	{
		bool L_29 = V_2;
		if (!L_29)
		{
			goto IL_00fd;
		}
	}
	{
		Il2CppObject* L_30 = ___observer0;
		Il2CppObject * L_31 = V_1;
		NullCheck((Il2CppObject*)L_30);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_30, (Il2CppObject *)L_31);
		Il2CppObject* L_32 = ___observer0;
		NullCheck((Il2CppObject*)L_32);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_32);
		goto IL_0103;
	}

IL_00fd:
	{
		Il2CppObject* L_33 = ___observer0;
		NullCheck((Il2CppObject*)L_33);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_33);
	}

IL_0103:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_34 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_34;
	}

IL_0109:
	{
		Il2CppObject * L_35 = V_7;
		return L_35;
	}
}
// System.Void UniRx.AsyncSubject`1<System.Object>::Dispose()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t AsyncSubject_1_Dispose_m2530551121_MetadataUsageId;
extern "C"  void AsyncSubject_1_Dispose_m2530551121_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_Dispose_m2530551121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t11563882 * L_2 = ((DisposedObserver_1_t11563882_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_6(L_2);
		__this->set_lastError_5((Exception_t1967233988 *)NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_3 = V_1;
		__this->set_lastValue_1(L_3);
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.AsyncSubject`1<System.Object>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t AsyncSubject_1_ThrowIfDisposed_m3466915290_MetadataUsageId;
extern "C"  void AsyncSubject_1_ThrowIfDisposed_m3466915290_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_ThrowIfDisposed_m3466915290_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.AsyncSubject`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool AsyncSubject_1_IsRequiredSubscribeOnCurrentThread_m353152055_gshared (AsyncSubject_1_t1536745524 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t AsyncSubject_1__ctor_m781162576_MetadataUsageId;
extern "C"  void AsyncSubject_1__ctor_m781162576_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1__ctor_m781162576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t1967639028 * L_1 = ((EmptyObserver_1_t1967639028_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_6(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T UniRx.AsyncSubject`1<UniRx.Unit>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral281548416;
extern const uint32_t AsyncSubject_1_get_Value_m4281593879_MetadataUsageId;
extern "C"  Unit_t2558286038  AsyncSubject_1_get_Value_m4281593879_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_get_Value_m4281593879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((AsyncSubject_1_t3257925142 *)__this);
		((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t3257925142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = (bool)__this->get_isStopped_3();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral281548416, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001c:
	{
		Exception_t1967233988 * L_2 = (Exception_t1967233988 *)__this->get_lastError_5();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Exception_t1967233988 * L_3 = (Exception_t1967233988 *)__this->get_lastError_5();
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002e:
	{
		Unit_t2558286038  L_4 = (Unit_t2558286038 )__this->get_lastValue_1();
		return L_4;
	}
}
// System.Boolean UniRx.AsyncSubject`1<UniRx.Unit>::get_HasObservers()
extern "C"  bool AsyncSubject_1_get_HasObservers_m2378705980_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_6();
		if (((EmptyObserver_1_t1967639028 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_3();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_4();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean UniRx.AsyncSubject`1<UniRx.Unit>::get_IsCompleted()
extern "C"  bool AsyncSubject_1_get_IsCompleted_m1977826538_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isStopped_3();
		return L_0;
	}
}
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::OnCompleted()
extern "C"  void AsyncSubject_1_OnCompleted_m1935364058_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Unit_t2558286038  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_3 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_3;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((AsyncSubject_1_t3257925142 *)__this);
			((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t3257925142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_2 = (bool)__this->get_isStopped_3();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x74, FINALLY_004f);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_6();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t1967639028 * L_4 = ((EmptyObserver_1_t1967639028_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_6(L_4);
			__this->set_isStopped_3((bool)1);
			Unit_t2558286038  L_5 = (Unit_t2558286038 )__this->get_lastValue_1();
			V_1 = (Unit_t2558286038 )L_5;
			bool L_6 = (bool)__this->get_hasValue_2();
			V_2 = (bool)L_6;
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_3;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0056:
	{
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_006e;
		}
	}
	{
		Il2CppObject* L_9 = V_0;
		Unit_t2558286038  L_10 = V_1;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Unit_t2558286038 )L_10);
		Il2CppObject* L_11 = V_0;
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_11);
		goto IL_0074;
	}

IL_006e:
	{
		Il2CppObject* L_12 = V_0;
		NullCheck((Il2CppObject*)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_12);
	}

IL_0074:
	{
		return;
	}
}
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t AsyncSubject_1_OnError_m3927640583_MetadataUsageId;
extern "C"  void AsyncSubject_1_OnError_m3927640583_gshared (AsyncSubject_1_t3257925142 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_OnError_m3927640583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((AsyncSubject_1_t3257925142 *)__this);
			((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t3257925142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_4 = (bool)__this->get_isStopped_3();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_6();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t1967639028 * L_6 = ((EmptyObserver_1_t1967639028_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_6(L_6);
			__this->set_isStopped_3((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_5(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::OnNext(T)
extern "C"  void AsyncSubject_1_OnNext_m1908662008_gshared (AsyncSubject_1_t3257925142 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((AsyncSubject_1_t3257925142 *)__this);
			((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t3257925142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_2 = (bool)__this->get_isStopped_3();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		}

IL_0023:
		{
			__this->set_hasValue_2((bool)1);
			Unit_t2558286038  L_3 = ___value0;
			__this->set_lastValue_1(L_3);
			IL2CPP_LEAVE(0x3D, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		return;
	}
}
// System.IDisposable UniRx.AsyncSubject`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t AsyncSubject_1_Subscribe_m931781903_MetadataUsageId;
extern "C"  Il2CppObject * AsyncSubject_1_Subscribe_m931781903_gshared (AsyncSubject_1_t3257925142 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_Subscribe_m931781903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Unit_t2558286038  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	ListObserver_1_t2852903709 * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Unit_t2558286038  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Il2CppObject * V_7 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Initobj (Unit_t2558286038_il2cpp_TypeInfo_var, (&V_6));
		Unit_t2558286038  L_2 = V_6;
		V_1 = (Unit_t2558286038 )L_2;
		V_2 = (bool)0;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_observerLock_0();
		V_3 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_3;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((AsyncSubject_1_t3257925142 *)__this);
			((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((AsyncSubject_1_t3257925142 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			bool L_5 = (bool)__this->get_isStopped_3();
			if (L_5)
			{
				goto IL_00b2;
			}
		}

IL_003e:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_outObserver_6();
			V_4 = (ListObserver_1_t2852903709 *)((ListObserver_1_t2852903709 *)IsInst(L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t2852903709 * L_7 = V_4;
			if (!L_7)
			{
				goto IL_0065;
			}
		}

IL_0052:
		{
			ListObserver_1_t2852903709 * L_8 = V_4;
			Il2CppObject* L_9 = ___observer0;
			NullCheck((ListObserver_1_t2852903709 *)L_8);
			Il2CppObject* L_10 = ((  Il2CppObject* (*) (ListObserver_1_t2852903709 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t2852903709 *)L_8, (Il2CppObject*)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_6(L_10);
			goto IL_00a4;
		}

IL_0065:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_outObserver_6();
			V_5 = (Il2CppObject*)L_11;
			Il2CppObject* L_12 = V_5;
			if (!((EmptyObserver_1_t1967639028 *)IsInst(L_12, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
			{
				goto IL_0085;
			}
		}

IL_0079:
		{
			Il2CppObject* L_13 = ___observer0;
			__this->set_outObserver_6(L_13);
			goto IL_00a4;
		}

IL_0085:
		{
			IObserver_1U5BU5D_t960117152* L_14 = (IObserver_1U5BU5D_t960117152*)((IObserver_1U5BU5D_t960117152*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_15 = V_5;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
			ArrayElementTypeCheck (L_14, L_15);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_15);
			IObserver_1U5BU5D_t960117152* L_16 = (IObserver_1U5BU5D_t960117152*)L_14;
			Il2CppObject* L_17 = ___observer0;
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
			ArrayElementTypeCheck (L_16, L_17);
			(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_17);
			ImmutableList_1_t1535522144 * L_18 = (ImmutableList_1_t1535522144 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t1535522144 *, IObserver_1U5BU5D_t960117152*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_18, (IObserver_1U5BU5D_t960117152*)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t2852903709 * L_19 = (ListObserver_1_t2852903709 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t2852903709 *, ImmutableList_1_t1535522144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_19, (ImmutableList_1_t1535522144 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_6(L_19);
		}

IL_00a4:
		{
			Il2CppObject* L_20 = ___observer0;
			Subscription_t337532560 * L_21 = (Subscription_t337532560 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t337532560 *, AsyncSubject_1_t3257925142 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_21, (AsyncSubject_1_t3257925142 *)__this, (Il2CppObject*)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_7 = (Il2CppObject *)L_21;
			IL2CPP_LEAVE(0x109, FINALLY_00cc);
		}

IL_00b2:
		{
			Exception_t1967233988 * L_22 = (Exception_t1967233988 *)__this->get_lastError_5();
			V_0 = (Exception_t1967233988 *)L_22;
			Unit_t2558286038  L_23 = (Unit_t2558286038 )__this->get_lastValue_1();
			V_1 = (Unit_t2558286038 )L_23;
			bool L_24 = (bool)__this->get_hasValue_2();
			V_2 = (bool)L_24;
			IL2CPP_LEAVE(0xD3, FINALLY_00cc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00cc;
	}

FINALLY_00cc:
	{ // begin finally (depth: 1)
		Il2CppObject * L_25 = V_3;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_25, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(204)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(204)
	{
		IL2CPP_JUMP_TBL(0x109, IL_0109)
		IL2CPP_JUMP_TBL(0xD3, IL_00d3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d3:
	{
		Exception_t1967233988 * L_26 = V_0;
		if (!L_26)
		{
			goto IL_00e5;
		}
	}
	{
		Il2CppObject* L_27 = ___observer0;
		Exception_t1967233988 * L_28 = V_0;
		NullCheck((Il2CppObject*)L_27);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_27, (Exception_t1967233988 *)L_28);
		goto IL_0103;
	}

IL_00e5:
	{
		bool L_29 = V_2;
		if (!L_29)
		{
			goto IL_00fd;
		}
	}
	{
		Il2CppObject* L_30 = ___observer0;
		Unit_t2558286038  L_31 = V_1;
		NullCheck((Il2CppObject*)L_30);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_30, (Unit_t2558286038 )L_31);
		Il2CppObject* L_32 = ___observer0;
		NullCheck((Il2CppObject*)L_32);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_32);
		goto IL_0103;
	}

IL_00fd:
	{
		Il2CppObject* L_33 = ___observer0;
		NullCheck((Il2CppObject*)L_33);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_33);
	}

IL_0103:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_34 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_34;
	}

IL_0109:
	{
		Il2CppObject * L_35 = V_7;
		return L_35;
	}
}
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::Dispose()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t AsyncSubject_1_Dispose_m3271209101_MetadataUsageId;
extern "C"  void AsyncSubject_1_Dispose_m3271209101_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_Dispose_m3271209101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Unit_t2558286038  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t1732743500 * L_2 = ((DisposedObserver_1_t1732743500_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_6(L_2);
		__this->set_lastError_5((Exception_t1967233988 *)NULL);
		Initobj (Unit_t2558286038_il2cpp_TypeInfo_var, (&V_1));
		Unit_t2558286038  L_3 = V_1;
		__this->set_lastValue_1(L_3);
		IL2CPP_LEAVE(0x41, FINALLY_003a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t AsyncSubject_1_ThrowIfDisposed_m3994647830_MetadataUsageId;
extern "C"  void AsyncSubject_1_ThrowIfDisposed_m3994647830_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncSubject_1_ThrowIfDisposed_m3994647830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.AsyncSubject`1<UniRx.Unit>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool AsyncSubject_1_IsRequiredSubscribeOnCurrentThread_m4252009011_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.BehaviorSubject`1/Subscription<System.Object>::.ctor(UniRx.BehaviorSubject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3932765271_MetadataUsageId;
extern "C"  void Subscription__ctor_m3932765271_gshared (Subscription_t2911320240 * __this, BehaviorSubject_1_t2662307150 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3932765271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		BehaviorSubject_1_t2662307150 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.BehaviorSubject`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m587048817_gshared (Subscription_t2911320240 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1131724091 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			BehaviorSubject_1_t2662307150 * L_2 = (BehaviorSubject_1_t2662307150 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			BehaviorSubject_1_t2662307150 * L_3 = (BehaviorSubject_1_t2662307150 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				BehaviorSubject_1_t2662307150 * L_6 = (BehaviorSubject_1_t2662307150 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_5();
				V_2 = (ListObserver_1_t1131724091 *)((ListObserver_1_t1131724091 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1131724091 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				BehaviorSubject_1_t2662307150 * L_9 = (BehaviorSubject_1_t2662307150 *)__this->get_parent_1();
				ListObserver_1_t1131724091 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1131724091 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1131724091 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_5(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				BehaviorSubject_1_t2662307150 * L_13 = (BehaviorSubject_1_t2662307150 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t246459410 * L_14 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_5(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((BehaviorSubject_1_t2662307150 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
