﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPurchaseRequest
struct GetPurchaseRequest_t593210440;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetPurchaseRequest::.ctor()
extern "C"  void GetPurchaseRequest__ctor_m3799503477 (GetPurchaseRequest_t593210440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetPurchaseRequest::get_OrderId()
extern "C"  String_t* GetPurchaseRequest_get_OrderId_m3474981142 (GetPurchaseRequest_t593210440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPurchaseRequest::set_OrderId(System.String)
extern "C"  void GetPurchaseRequest_set_OrderId_m2178478115 (GetPurchaseRequest_t593210440 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
