﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen520705716MethodDeclarations.h"

// System.Void Zenject.BinderGeneric`1<BarygaVkController>::.ctor(Zenject.DiContainer,System.String,Zenject.SingletonProviderMap)
#define BinderGeneric_1__ctor_m4293323089(__this, ___container0, ___identifier1, ___singletonMap2, method) ((  void (*) (BinderGeneric_1_t3300763217 *, DiContainer_t2383114449 *, String_t*, SingletonProviderMap_t1557411893 *, const MethodInfo*))BinderGeneric_1__ctor_m467851124_gshared)(__this, ___container0, ___identifier1, ___singletonMap2, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<BarygaVkController>::ToMethod(System.Func`2<Zenject.InjectContext,TContract>)
#define BinderGeneric_1_ToMethod_m703829872(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t3300763217 *, Func_2_t1106335802 *, const MethodInfo*))BinderGeneric_1_ToMethod_m4174284639_gshared)(__this, ___method0, method)
