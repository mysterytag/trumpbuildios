﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey30
struct U3CGetWWWU3Ec__AnonStorey30_t1804159459;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey30::.ctor()
extern "C"  void U3CGetWWWU3Ec__AnonStorey30__ctor_m2985900381 (U3CGetWWWU3Ec__AnonStorey30_t1804159459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey30::<>m__1B(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CGetWWWU3Ec__AnonStorey30_U3CU3Em__1B_m3149040067 (U3CGetWWWU3Ec__AnonStorey30_t1804159459 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
