﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe_`1<System.Boolean>
struct Subscribe__1_t2955937592;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe_`1<System.Boolean>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m4185702714_gshared (Subscribe__1_t2955937592 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method);
#define Subscribe__1__ctor_m4185702714(__this, ___onError0, ___onCompleted1, method) ((  void (*) (Subscribe__1_t2955937592 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe__1__ctor_m4185702714_gshared)(__this, ___onError0, ___onCompleted1, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Boolean>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m660831263_gshared (Subscribe__1_t2955937592 * __this, bool ___value0, const MethodInfo* method);
#define Subscribe__1_OnNext_m660831263(__this, ___value0, method) ((  void (*) (Subscribe__1_t2955937592 *, bool, const MethodInfo*))Subscribe__1_OnNext_m660831263_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Boolean>::OnError(System.Exception)
extern "C"  void Subscribe__1_OnError_m4173429038_gshared (Subscribe__1_t2955937592 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe__1_OnError_m4173429038(__this, ___error0, method) ((  void (*) (Subscribe__1_t2955937592 *, Exception_t1967233988 *, const MethodInfo*))Subscribe__1_OnError_m4173429038_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Boolean>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m3892969857_gshared (Subscribe__1_t2955937592 * __this, const MethodInfo* method);
#define Subscribe__1_OnCompleted_m3892969857(__this, method) ((  void (*) (Subscribe__1_t2955937592 *, const MethodInfo*))Subscribe__1_OnCompleted_m3892969857_gshared)(__this, method)
