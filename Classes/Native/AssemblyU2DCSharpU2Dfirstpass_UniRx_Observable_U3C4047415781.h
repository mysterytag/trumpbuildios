﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.Observable/<SelectMany>c__AnonStorey4F`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1<System.Object>
struct  U3CSelectManyU3Ec__AnonStorey50_1_t4047415781  : public Il2CppObject
{
public:
	// T UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1::x
	Il2CppObject * ___x_0;
	// UniRx.Observable/<SelectMany>c__AnonStorey4F`1<T> UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1::<>f__ref$79
	U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 * ___U3CU3Ef__refU2479_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey50_1_t4047415781, ___x_0)); }
	inline Il2CppObject * get_x_0() const { return ___x_0; }
	inline Il2CppObject ** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Il2CppObject * value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier(&___x_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2479_1() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey50_1_t4047415781, ___U3CU3Ef__refU2479_1)); }
	inline U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 * get_U3CU3Ef__refU2479_1() const { return ___U3CU3Ef__refU2479_1; }
	inline U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 ** get_address_of_U3CU3Ef__refU2479_1() { return &___U3CU3Ef__refU2479_1; }
	inline void set_U3CU3Ef__refU2479_1(U3CSelectManyU3Ec__AnonStorey4F_1_t3757673476 * value)
	{
		___U3CU3Ef__refU2479_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2479_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
