﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t2937500249;
// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t67115091;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils2/<OnPointerDown>c__AnonStoreyF0
struct  U3COnPointerDownU3Ec__AnonStoreyF0_t1374650039  : public Il2CppObject
{
public:
	// System.Action Utils2/<OnPointerDown>c__AnonStoreyF0::act
	Action_t437523947 * ___act_0;
	// UnityEngine.EventSystems.EventTrigger Utils2/<OnPointerDown>c__AnonStoreyF0::self
	EventTrigger_t2937500249 * ___self_1;
	// UnityEngine.EventSystems.EventTrigger/Entry Utils2/<OnPointerDown>c__AnonStoreyF0::entry
	Entry_t67115091 * ___entry_2;

public:
	inline static int32_t get_offset_of_act_0() { return static_cast<int32_t>(offsetof(U3COnPointerDownU3Ec__AnonStoreyF0_t1374650039, ___act_0)); }
	inline Action_t437523947 * get_act_0() const { return ___act_0; }
	inline Action_t437523947 ** get_address_of_act_0() { return &___act_0; }
	inline void set_act_0(Action_t437523947 * value)
	{
		___act_0 = value;
		Il2CppCodeGenWriteBarrier(&___act_0, value);
	}

	inline static int32_t get_offset_of_self_1() { return static_cast<int32_t>(offsetof(U3COnPointerDownU3Ec__AnonStoreyF0_t1374650039, ___self_1)); }
	inline EventTrigger_t2937500249 * get_self_1() const { return ___self_1; }
	inline EventTrigger_t2937500249 ** get_address_of_self_1() { return &___self_1; }
	inline void set_self_1(EventTrigger_t2937500249 * value)
	{
		___self_1 = value;
		Il2CppCodeGenWriteBarrier(&___self_1, value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3COnPointerDownU3Ec__AnonStoreyF0_t1374650039, ___entry_2)); }
	inline Entry_t67115091 * get_entry_2() const { return ___entry_2; }
	inline Entry_t67115091 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_t67115091 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier(&___entry_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
