﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate
struct TlsClientCertificate_t2807829361;
// Mono.Security.Protocol.Tls.Context
struct Context_t4123145639;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t273828613;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3336811651;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Context4123145639.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate273828612.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3336811650.h"

// System.Void Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern "C"  void TlsClientCertificate__ctor_m3823740723 (TlsClientCertificate_t2807829361 * __this, Context_t4123145639 * ___context0, ByteU5BU5D_t58506160* ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate::Update()
extern "C"  void TlsClientCertificate_Update_m116448180 (TlsClientCertificate_t2807829361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate::ProcessAsSsl3()
extern "C"  void TlsClientCertificate_ProcessAsSsl3_m1872304607 (TlsClientCertificate_t2807829361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate::ProcessAsTls1()
extern "C"  void TlsClientCertificate_ProcessAsTls1_m1894675726 (TlsClientCertificate_t2807829361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate::checkCertificateUsage(Mono.Security.X509.X509Certificate)
extern "C"  bool TlsClientCertificate_checkCertificateUsage_m1115050287 (TlsClientCertificate_t2807829361 * __this, X509Certificate_t273828613 * ___cert0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate::validateCertificates(Mono.Security.X509.X509CertificateCollection)
extern "C"  void TlsClientCertificate_validateCertificates_m1563954323 (TlsClientCertificate_t2807829361 * __this, X509CertificateCollection_t3336811651 * ___certificates0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
