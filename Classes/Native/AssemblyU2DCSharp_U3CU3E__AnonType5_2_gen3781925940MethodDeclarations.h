﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType5`2<System.Object,System.Object>
struct U3CU3E__AnonType5_2_t3781925940;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType5`2<System.Object,System.Object>::.ctor(<time>__T,<_>__T)
extern "C"  void U3CU3E__AnonType5_2__ctor_m2680221738_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, Il2CppObject * ___time0, Il2CppObject * ____1, const MethodInfo* method);
#define U3CU3E__AnonType5_2__ctor_m2680221738(__this, ___time0, ____1, method) ((  void (*) (U3CU3E__AnonType5_2_t3781925940 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType5_2__ctor_m2680221738_gshared)(__this, ___time0, ____1, method)
// <time>__T <>__AnonType5`2<System.Object,System.Object>::get_time()
extern "C"  Il2CppObject * U3CU3E__AnonType5_2_get_time_m1506448158_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, const MethodInfo* method);
#define U3CU3E__AnonType5_2_get_time_m1506448158(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType5_2_t3781925940 *, const MethodInfo*))U3CU3E__AnonType5_2_get_time_m1506448158_gshared)(__this, method)
// <_>__T <>__AnonType5`2<System.Object,System.Object>::get__()
extern "C"  Il2CppObject * U3CU3E__AnonType5_2_get___m2300451264_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, const MethodInfo* method);
#define U3CU3E__AnonType5_2_get___m2300451264(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType5_2_t3781925940 *, const MethodInfo*))U3CU3E__AnonType5_2_get___m2300451264_gshared)(__this, method)
// System.Boolean <>__AnonType5`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType5_2_Equals_m3316902925_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType5_2_Equals_m3316902925(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType5_2_t3781925940 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType5_2_Equals_m3316902925_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType5`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType5_2_GetHashCode_m2230378289_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, const MethodInfo* method);
#define U3CU3E__AnonType5_2_GetHashCode_m2230378289(__this, method) ((  int32_t (*) (U3CU3E__AnonType5_2_t3781925940 *, const MethodInfo*))U3CU3E__AnonType5_2_GetHashCode_m2230378289_gshared)(__this, method)
// System.String <>__AnonType5`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType5_2_ToString_m2643948803_gshared (U3CU3E__AnonType5_2_t3781925940 * __this, const MethodInfo* method);
#define U3CU3E__AnonType5_2_ToString_m2643948803(__this, method) ((  String_t* (*) (U3CU3E__AnonType5_2_t3781925940 *, const MethodInfo*))U3CU3E__AnonType5_2_ToString_m2643948803_gshared)(__this, method)
