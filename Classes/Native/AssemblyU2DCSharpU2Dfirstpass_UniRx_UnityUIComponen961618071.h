﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Selectable
struct Selectable_t3621744255;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityUIComponentExtensions/<SubscribeToInteractable>c__AnonStoreyA6
struct  U3CSubscribeToInteractableU3Ec__AnonStoreyA6_t961618071  : public Il2CppObject
{
public:
	// UnityEngine.UI.Selectable UniRx.UnityUIComponentExtensions/<SubscribeToInteractable>c__AnonStoreyA6::selectable
	Selectable_t3621744255 * ___selectable_0;

public:
	inline static int32_t get_offset_of_selectable_0() { return static_cast<int32_t>(offsetof(U3CSubscribeToInteractableU3Ec__AnonStoreyA6_t961618071, ___selectable_0)); }
	inline Selectable_t3621744255 * get_selectable_0() const { return ___selectable_0; }
	inline Selectable_t3621744255 ** get_address_of_selectable_0() { return &___selectable_0; }
	inline void set_selectable_0(Selectable_t3621744255 * value)
	{
		___selectable_0 = value;
		Il2CppCodeGenWriteBarrier(&___selectable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
