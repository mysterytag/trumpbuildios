﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateSharedGroupDataResponseCallback
struct UpdateSharedGroupDataResponseCallback_t3984445825;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateSharedGroupDataRequest
struct UpdateSharedGroupDataRequest_t1900307044;
// PlayFab.ClientModels.UpdateSharedGroupDataResult
struct UpdateSharedGroupDataResult_t1620468680;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateShare1900307044.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateShare1620468680.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateSharedGroupDataResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateSharedGroupDataResponseCallback__ctor_m3581613456 (UpdateSharedGroupDataResponseCallback_t3984445825 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateSharedGroupDataResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateSharedGroupDataRequest,PlayFab.ClientModels.UpdateSharedGroupDataResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UpdateSharedGroupDataResponseCallback_Invoke_m2960202481 (UpdateSharedGroupDataResponseCallback_t3984445825 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateSharedGroupDataRequest_t1900307044 * ___request2, UpdateSharedGroupDataResult_t1620468680 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateSharedGroupDataResponseCallback_t3984445825(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateSharedGroupDataRequest_t1900307044 * ___request2, UpdateSharedGroupDataResult_t1620468680 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateSharedGroupDataResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateSharedGroupDataRequest,PlayFab.ClientModels.UpdateSharedGroupDataResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateSharedGroupDataResponseCallback_BeginInvoke_m1527440062 (UpdateSharedGroupDataResponseCallback_t3984445825 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateSharedGroupDataRequest_t1900307044 * ___request2, UpdateSharedGroupDataResult_t1620468680 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateSharedGroupDataResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateSharedGroupDataResponseCallback_EndInvoke_m3109539744 (UpdateSharedGroupDataResponseCallback_t3984445825 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
