﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`3<System.Boolean,System.Boolean,System.Boolean>
struct Func_3_t3063550794;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`3<System.Boolean,System.Boolean,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3670658982_gshared (Func_3_t3063550794 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_3__ctor_m3670658982(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t3063550794 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m3670658982_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Boolean,System.Boolean,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m3734720759_gshared (Func_3_t3063550794 * __this, bool ___arg10, bool ___arg21, const MethodInfo* method);
#define Func_3_Invoke_m3734720759(__this, ___arg10, ___arg21, method) ((  bool (*) (Func_3_t3063550794 *, bool, bool, const MethodInfo*))Func_3_Invoke_m3734720759_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Boolean,System.Boolean,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m2554513532_gshared (Func_3_t3063550794 * __this, bool ___arg10, bool ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Func_3_BeginInvoke_m2554513532(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t3063550794 *, bool, bool, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m2554513532_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Boolean,System.Boolean,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m1120760516_gshared (Func_3_t3063550794 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_3_EndInvoke_m1120760516(__this, ___result0, method) ((  bool (*) (Func_3_t3063550794 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m1120760516_gshared)(__this, ___result0, method)
