﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Counter
struct Counter_t2622483932;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Counter::.ctor()
extern "C"  void Counter__ctor_m1804671247 (Counter_t2622483932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Counter::Start()
extern "C"  void Counter_Start_m751809039 (Counter_t2622483932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Counter::SetText(System.String)
extern "C"  void Counter_SetText_m2360067174 (Counter_t2622483932 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Counter::TextChangeCoroutine()
extern "C"  Il2CppObject * Counter_TextChangeCoroutine_m2750578384 (Counter_t2622483932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
