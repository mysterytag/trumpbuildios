﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>
struct SwitchObserver_t321759539;
// UniRx.Operators.SwitchObservable`1<System.Object>
struct SwitchObservable_1_t1854652486;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::.ctor(UniRx.Operators.SwitchObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void SwitchObserver__ctor_m3135145794_gshared (SwitchObserver_t321759539 * __this, SwitchObservable_1_t1854652486 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SwitchObserver__ctor_m3135145794(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SwitchObserver_t321759539 *, SwitchObservable_1_t1854652486 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SwitchObserver__ctor_m3135145794_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::Run()
extern "C"  Il2CppObject * SwitchObserver_Run_m3168455675_gshared (SwitchObserver_t321759539 * __this, const MethodInfo* method);
#define SwitchObserver_Run_m3168455675(__this, method) ((  Il2CppObject * (*) (SwitchObserver_t321759539 *, const MethodInfo*))SwitchObserver_Run_m3168455675_gshared)(__this, method)
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::OnNext(UniRx.IObservable`1<T>)
extern "C"  void SwitchObserver_OnNext_m2371630088_gshared (SwitchObserver_t321759539 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define SwitchObserver_OnNext_m2371630088(__this, ___value0, method) ((  void (*) (SwitchObserver_t321759539 *, Il2CppObject*, const MethodInfo*))SwitchObserver_OnNext_m2371630088_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::OnError(System.Exception)
extern "C"  void SwitchObserver_OnError_m4055392420_gshared (SwitchObserver_t321759539 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SwitchObserver_OnError_m4055392420(__this, ___error0, method) ((  void (*) (SwitchObserver_t321759539 *, Exception_t1967233988 *, const MethodInfo*))SwitchObserver_OnError_m4055392420_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>::OnCompleted()
extern "C"  void SwitchObserver_OnCompleted_m238545399_gshared (SwitchObserver_t321759539 * __this, const MethodInfo* method);
#define SwitchObserver_OnCompleted_m238545399(__this, method) ((  void (*) (SwitchObserver_t321759539 *, const MethodInfo*))SwitchObserver_OnCompleted_m238545399_gshared)(__this, method)
