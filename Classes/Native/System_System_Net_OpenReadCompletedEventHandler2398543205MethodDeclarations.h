﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.OpenReadCompletedEventHandler
struct OpenReadCompletedEventHandler_t2398543205;
// System.Object
struct Il2CppObject;
// System.Net.OpenReadCompletedEventArgs
struct OpenReadCompletedEventArgs_t223760182;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_OpenReadCompletedEventArgs223760182.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.OpenReadCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OpenReadCompletedEventHandler__ctor_m1559053553 (OpenReadCompletedEventHandler_t2398543205 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.OpenReadCompletedEventHandler::Invoke(System.Object,System.Net.OpenReadCompletedEventArgs)
extern "C"  void OpenReadCompletedEventHandler_Invoke_m1793628985 (OpenReadCompletedEventHandler_t2398543205 * __this, Il2CppObject * ___sender0, OpenReadCompletedEventArgs_t223760182 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OpenReadCompletedEventHandler_t2398543205(Il2CppObject* delegate, Il2CppObject * ___sender0, OpenReadCompletedEventArgs_t223760182 * ___e1);
// System.IAsyncResult System.Net.OpenReadCompletedEventHandler::BeginInvoke(System.Object,System.Net.OpenReadCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OpenReadCompletedEventHandler_BeginInvoke_m3698310650 (OpenReadCompletedEventHandler_t2398543205 * __this, Il2CppObject * ___sender0, OpenReadCompletedEventArgs_t223760182 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.OpenReadCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OpenReadCompletedEventHandler_EndInvoke_m3208828801 (OpenReadCompletedEventHandler_t2398543205 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
