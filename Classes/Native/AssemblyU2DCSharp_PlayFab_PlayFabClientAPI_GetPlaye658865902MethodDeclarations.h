﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayerTradesRequestCallback
struct GetPlayerTradesRequestCallback_t658865902;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayerTradesRequest
struct GetPlayerTradesRequest_t3393757081;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayerTr3393757081.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayerTradesRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayerTradesRequestCallback__ctor_m3823021277 (GetPlayerTradesRequestCallback_t658865902 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayerTradesRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayerTradesRequest,System.Object)
extern "C"  void GetPlayerTradesRequestCallback_Invoke_m2641055583 (GetPlayerTradesRequestCallback_t658865902 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayerTradesRequest_t3393757081 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayerTradesRequestCallback_t658865902(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayerTradesRequest_t3393757081 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayerTradesRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayerTradesRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayerTradesRequestCallback_BeginInvoke_m58568898 (GetPlayerTradesRequestCallback_t658865902 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayerTradesRequest_t3393757081 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayerTradesRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayerTradesRequestCallback_EndInvoke_m446502253 (GetPlayerTradesRequestCallback_t658865902 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
