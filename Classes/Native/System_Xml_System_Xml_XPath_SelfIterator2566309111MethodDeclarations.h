﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.SelfIterator
struct SelfIterator_t2566309111;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1624538935;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3696234203;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1624538935.h"
#include "System_Xml_System_Xml_XPath_SelfIterator2566309111.h"

// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void SelfIterator__ctor_m2835358572 (SelfIterator_t2566309111 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void SelfIterator__ctor_m4150585400 (SelfIterator_t2566309111 * __this, XPathNavigator_t1624538935 * ___nav0, Il2CppObject * ___nsm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.SelfIterator,System.Boolean)
extern "C"  void SelfIterator__ctor_m3078894092 (SelfIterator_t2566309111 * __this, SelfIterator_t2566309111 * ___other0, bool ___clone1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SelfIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * SelfIterator_Clone_m3186820838 (SelfIterator_t2566309111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.SelfIterator::MoveNextCore()
extern "C"  bool SelfIterator_MoveNextCore_m260577695 (SelfIterator_t2566309111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SelfIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * SelfIterator_get_Current_m4205100576 (SelfIterator_t2566309111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
