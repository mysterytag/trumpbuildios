﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>
struct ToYieldInstruction_t3682831619;
// UniRx.ObservableYieldInstruction`1<System.Int64>
struct ObservableYieldInstruction_1_t674360470;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>::.ctor(UniRx.ObservableYieldInstruction`1<T>)
extern "C"  void ToYieldInstruction__ctor_m2398821652_gshared (ToYieldInstruction_t3682831619 * __this, ObservableYieldInstruction_1_t674360470 * ___parent0, const MethodInfo* method);
#define ToYieldInstruction__ctor_m2398821652(__this, ___parent0, method) ((  void (*) (ToYieldInstruction_t3682831619 *, ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ToYieldInstruction__ctor_m2398821652_gshared)(__this, ___parent0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>::OnNext(T)
extern "C"  void ToYieldInstruction_OnNext_m2002294157_gshared (ToYieldInstruction_t3682831619 * __this, int64_t ___value0, const MethodInfo* method);
#define ToYieldInstruction_OnNext_m2002294157(__this, ___value0, method) ((  void (*) (ToYieldInstruction_t3682831619 *, int64_t, const MethodInfo*))ToYieldInstruction_OnNext_m2002294157_gshared)(__this, ___value0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>::OnError(System.Exception)
extern "C"  void ToYieldInstruction_OnError_m4267832988_gshared (ToYieldInstruction_t3682831619 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ToYieldInstruction_OnError_m4267832988(__this, ___error0, method) ((  void (*) (ToYieldInstruction_t3682831619 *, Exception_t1967233988 *, const MethodInfo*))ToYieldInstruction_OnError_m4267832988_gshared)(__this, ___error0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int64>::OnCompleted()
extern "C"  void ToYieldInstruction_OnCompleted_m2614670319_gshared (ToYieldInstruction_t3682831619 * __this, const MethodInfo* method);
#define ToYieldInstruction_OnCompleted_m2614670319(__this, method) ((  void (*) (ToYieldInstruction_t3682831619 *, const MethodInfo*))ToYieldInstruction_OnCompleted_m2614670319_gshared)(__this, method)
