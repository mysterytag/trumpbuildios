﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_5_t2495851293;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_5__ctor_m1759325575_gshared (Factory_5_t2495851293 * __this, const MethodInfo* method);
#define Factory_5__ctor_m1759325575(__this, method) ((  void (*) (Factory_5_t2495851293 *, const MethodInfo*))Factory_5__ctor_m1759325575_gshared)(__this, method)
// System.Type Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_5_get_ConstructedType_m1911314354_gshared (Factory_5_t2495851293 * __this, const MethodInfo* method);
#define Factory_5_get_ConstructedType_m1911314354(__this, method) ((  Type_t * (*) (Factory_5_t2495851293 *, const MethodInfo*))Factory_5_get_ConstructedType_m1911314354_gshared)(__this, method)
// System.Type[] Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_5_get_ProvidedTypes_m1262492698_gshared (Factory_5_t2495851293 * __this, const MethodInfo* method);
#define Factory_5_get_ProvidedTypes_m1262492698(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_5_t2495851293 *, const MethodInfo*))Factory_5_get_ProvidedTypes_m1262492698_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_5_get_Container_m1453275623_gshared (Factory_5_t2495851293 * __this, const MethodInfo* method);
#define Factory_5_get_Container_m1453275623(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_5_t2495851293 *, const MethodInfo*))Factory_5_get_Container_m1453275623_gshared)(__this, method)
// TValue Zenject.Factory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4)
extern "C"  Il2CppObject * Factory_5_Create_m3103857335_gshared (Factory_5_t2495851293 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, const MethodInfo* method);
#define Factory_5_Create_m3103857335(__this, ___param10, ___param21, ___param32, ___param43, method) ((  Il2CppObject * (*) (Factory_5_t2495851293 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_5_Create_m3103857335_gshared)(__this, ___param10, ___param21, ___param32, ___param43, method)
