﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConnectionCollector
struct ConnectionCollector_t444796719;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void ConnectionCollector::.ctor()
extern "C"  void ConnectionCollector__ctor_m3524765980 (ConnectionCollector_t444796719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionCollector::set_add(System.IDisposable)
extern "C"  void ConnectionCollector_set_add_m3757065972 (ConnectionCollector_t444796719 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionCollector::DisconnectAll()
extern "C"  void ConnectionCollector_DisconnectAll_m662901599 (ConnectionCollector_t444796719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionCollector::RemoveAndDispose(System.IDisposable)
extern "C"  void ConnectionCollector_RemoveAndDispose_m4283979306 (ConnectionCollector_t444796719 * __this, Il2CppObject * ___disp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionCollector::Dispose()
extern "C"  void ConnectionCollector_Dispose_m2764160601 (ConnectionCollector_t444796719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionCollector::Collect(System.IDisposable)
extern "C"  void ConnectionCollector_Collect_m1234976186 (ConnectionCollector_t444796719 * __this, Il2CppObject * ___connection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
