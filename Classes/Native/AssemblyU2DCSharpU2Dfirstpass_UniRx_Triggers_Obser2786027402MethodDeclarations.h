﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// UnityEngine.Component
struct Component_t2126946602;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IObservable`1<UnityEngine.Collision2D>
struct IObservable_1_t211608397;
// UniRx.IObservable`1<UnityEngine.Collision>
struct IObservable_1_t878336379;
// UniRx.IObservable`1<UnityEngine.Collider2D>
struct IObservable_1_t1648836559;
// UniRx.IObservable`1<UnityEngine.Collider>
struct IObservable_1_t714468989;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData>
struct IObservable_1_t3305902090;
// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_t644839684;
// UniRx.IObservable`1<UnityEngine.EventSystems.AxisEventData>
struct IObservable_1_t311695674;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour644839684.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// UniRx.IObservable`1<System.Int32> UniRx.Triggers.ObservableTriggerExtensions::OnAnimatorIKAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnAnimatorIKAsObservable_m3976249393 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnAnimatorMoveAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnAnimatorMoveAsObservable_m3800577877 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionEnter2DAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionEnter2DAsObservable_m633075360 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionExit2DAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionExit2DAsObservable_m3948509874 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionStay2DAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionStay2DAsObservable_m3728303703 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionEnterAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionEnterAsObservable_m740171268 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionExitAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionExitAsObservable_m3446909618 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionStayAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionStayAsObservable_m3996400535 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnDestroyAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnDestroyAsObservable_m942404325 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnEnableAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnEnableAsObservable_m3684152970 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnDisableAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnDisableAsObservable_m445159735 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::FixedUpdateAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_FixedUpdateAsObservable_m4047654115 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::LateUpdateAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_LateUpdateAsObservable_m3218501405 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerEnter2DAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerEnter2DAsObservable_m1683777818 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerExit2DAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerExit2DAsObservable_m2181288184 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerStay2DAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerStay2DAsObservable_m1961082013 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerEnterAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerEnterAsObservable_m3712376958 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerExitAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerExitAsObservable_m2572955896 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerStayAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerStayAsObservable_m3122446813 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::UpdateAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_UpdateAsObservable_m1182734883 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnBecameInvisibleAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnBecameInvisibleAsObservable_m3145962795 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnBecameVisibleAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnBecameVisibleAsObservable_m1445639878 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnBeforeTransformParentChangedAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnBeforeTransformParentChangedAsObservable_m1274628496 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnTransformParentChangedAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTransformParentChangedAsObservable_m2539629967 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnTransformChildrenChangedAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTransformChildrenChangedAsObservable_m1175886820 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnCanvasGroupChangedAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCanvasGroupChangedAsObservable_m1312548000 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnRectTransformDimensionsChangeAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnRectTransformDimensionsChangeAsObservable_m1024425562 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnRectTransformRemovedAsObservable(UnityEngine.Component)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnRectTransformRemovedAsObservable_m316790357 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableTriggerExtensions::OnDeselectAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnDeselectAsObservable_m2644310346 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.AxisEventData> UniRx.Triggers.ObservableTriggerExtensions::OnMoveAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnMoveAsObservable_m557396494 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnPointerDownAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnPointerDownAsObservable_m964311540 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnPointerEnterAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnPointerEnterAsObservable_m3641618426 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnPointerExitAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnPointerExitAsObservable_m1578121072 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnPointerUpAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnPointerUpAsObservable_m2192746925 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableTriggerExtensions::OnSelectAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnSelectAsObservable_m2870672521 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnPointerClickAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnPointerClickAsObservable_m204104362 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableTriggerExtensions::OnSubmitAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnSubmitAsObservable_m3090581669 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnDragAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnDragAsObservable_m3996509011 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnBeginDragAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnBeginDragAsObservable_m923057714 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnEndDragAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnEndDragAsObservable_m1350563748 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnDropAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnDropAsObservable_m3488768078 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableTriggerExtensions::OnUpdateSelectedAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnUpdateSelectedAsObservable_m3913140497 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnInitializePotentialDragAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnInitializePotentialDragAsObservable_m3774931703 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableTriggerExtensions::OnCancelAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCancelAsObservable_m1266571911 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableTriggerExtensions::OnScrollAsObservable(UnityEngine.EventSystems.UIBehaviour)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnScrollAsObservable_m1722134380 (Il2CppObject * __this /* static, unused */, UIBehaviour_t644839684 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int32> UniRx.Triggers.ObservableTriggerExtensions::OnAnimatorIKAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnAnimatorIKAsObservable_m2197392307 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnAnimatorMoveAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnAnimatorMoveAsObservable_m1046542607 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionEnter2DAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionEnter2DAsObservable_m1638212388 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionExit2DAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionExit2DAsObservable_m1337467218 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionStay2DAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionStay2DAsObservable_m3101010509 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionEnterAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionEnterAsObservable_m663218240 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionExitAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionExitAsObservable_m2967728466 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.Triggers.ObservableTriggerExtensions::OnCollisionStayAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCollisionStayAsObservable_m2822077709 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnDestroyAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnDestroyAsObservable_m2637475711 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnEnableAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnEnableAsObservable_m1732337786 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnDisableAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnDisableAsObservable_m107795309 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::FixedUpdateAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_FixedUpdateAsObservable_m115971393 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::LateUpdateAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_LateUpdateAsObservable_m182041159 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerEnter2DAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerEnter2DAsObservable_m4145217514 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerExit2DAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerExit2DAsObservable_m2388169676 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerStay2DAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerStay2DAsObservable_m4151712967 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerEnterAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerEnterAsObservable_m2607281414 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerExitAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerExitAsObservable_m1644966860 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerExtensions::OnTriggerStayAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTriggerStayAsObservable_m1499316103 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::UpdateAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_UpdateAsObservable_m1497788417 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnBecameInvisibleAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnBecameInvisibleAsObservable_m2228311545 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnBecameVisibleAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnBecameVisibleAsObservable_m1057908670 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnBeforeTransformParentChangedAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnBeforeTransformParentChangedAsObservable_m51523124 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnTransformParentChangedAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTransformParentChangedAsObservable_m611863061 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnTransformChildrenChangedAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnTransformChildrenChangedAsObservable_m1285498464 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnCanvasGroupChangedAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnCanvasGroupChangedAsObservable_m1227027748 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnRectTransformDimensionsChangeAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnRectTransformDimensionsChangeAsObservable_m885166762 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerExtensions::OnRectTransformRemovedAsObservable(UnityEngine.GameObject)
extern "C"  Il2CppObject* ObservableTriggerExtensions_OnRectTransformRemovedAsObservable_m423311887 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
