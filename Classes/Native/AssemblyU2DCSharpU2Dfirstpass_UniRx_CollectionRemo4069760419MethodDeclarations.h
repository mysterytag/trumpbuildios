﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemo4069760419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Int32,T)
extern "C"  void CollectionRemoveEvent_1__ctor_m570772917_gshared (CollectionRemoveEvent_1_t4069760419 * __this, int32_t ___index0, Tuple_2_t369261819  ___value1, const MethodInfo* method);
#define CollectionRemoveEvent_1__ctor_m570772917(__this, ___index0, ___value1, method) ((  void (*) (CollectionRemoveEvent_1_t4069760419 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))CollectionRemoveEvent_1__ctor_m570772917_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Index()
extern "C"  int32_t CollectionRemoveEvent_1_get_Index_m3875147401_gshared (CollectionRemoveEvent_1_t4069760419 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_get_Index_m3875147401(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))CollectionRemoveEvent_1_get_Index_m3875147401_gshared)(__this, method)
// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Index(System.Int32)
extern "C"  void CollectionRemoveEvent_1_set_Index_m1562070256_gshared (CollectionRemoveEvent_1_t4069760419 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionRemoveEvent_1_set_Index_m1562070256(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t4069760419 *, int32_t, const MethodInfo*))CollectionRemoveEvent_1_set_Index_m1562070256_gshared)(__this, ___value0, method)
// T UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::get_Value()
extern "C"  Tuple_2_t369261819  CollectionRemoveEvent_1_get_Value_m3444388657_gshared (CollectionRemoveEvent_1_t4069760419 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_get_Value_m3444388657(__this, method) ((  Tuple_2_t369261819  (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))CollectionRemoveEvent_1_get_Value_m3444388657_gshared)(__this, method)
// System.Void UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::set_Value(T)
extern "C"  void CollectionRemoveEvent_1_set_Value_m2422397472_gshared (CollectionRemoveEvent_1_t4069760419 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define CollectionRemoveEvent_1_set_Value_m2422397472(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t4069760419 *, Tuple_2_t369261819 , const MethodInfo*))CollectionRemoveEvent_1_set_Value_m2422397472_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::ToString()
extern "C"  String_t* CollectionRemoveEvent_1_ToString_m1076386913_gshared (CollectionRemoveEvent_1_t4069760419 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_ToString_m1076386913(__this, method) ((  String_t* (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))CollectionRemoveEvent_1_ToString_m1076386913_gshared)(__this, method)
// System.Int32 UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode()
extern "C"  int32_t CollectionRemoveEvent_1_GetHashCode_m3568852913_gshared (CollectionRemoveEvent_1_t4069760419 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_GetHashCode_m3568852913(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t4069760419 *, const MethodInfo*))CollectionRemoveEvent_1_GetHashCode_m3568852913_gshared)(__this, method)
// System.Boolean UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(UniRx.CollectionRemoveEvent`1<T>)
extern "C"  bool CollectionRemoveEvent_1_Equals_m2262826342_gshared (CollectionRemoveEvent_1_t4069760419 * __this, CollectionRemoveEvent_1_t4069760419  ___other0, const MethodInfo* method);
#define CollectionRemoveEvent_1_Equals_m2262826342(__this, ___other0, method) ((  bool (*) (CollectionRemoveEvent_1_t4069760419 *, CollectionRemoveEvent_1_t4069760419 , const MethodInfo*))CollectionRemoveEvent_1_Equals_m2262826342_gshared)(__this, ___other0, method)
