﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Func`1<System.Object>
struct Func_1_t1979887667;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<ToAsync>c__AnonStorey38`1<System.Object>
struct  U3CToAsyncU3Ec__AnonStorey38_1_t1221489661  : public Il2CppObject
{
public:
	// UniRx.IScheduler UniRx.Observable/<ToAsync>c__AnonStorey38`1::scheduler
	Il2CppObject * ___scheduler_0;
	// System.Func`1<T> UniRx.Observable/<ToAsync>c__AnonStorey38`1::function
	Func_1_t1979887667 * ___function_1;

public:
	inline static int32_t get_offset_of_scheduler_0() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStorey38_1_t1221489661, ___scheduler_0)); }
	inline Il2CppObject * get_scheduler_0() const { return ___scheduler_0; }
	inline Il2CppObject ** get_address_of_scheduler_0() { return &___scheduler_0; }
	inline void set_scheduler_0(Il2CppObject * value)
	{
		___scheduler_0 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_0, value);
	}

	inline static int32_t get_offset_of_function_1() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStorey38_1_t1221489661, ___function_1)); }
	inline Func_1_t1979887667 * get_function_1() const { return ___function_1; }
	inline Func_1_t1979887667 ** get_address_of_function_1() { return &___function_1; }
	inline void set_function_1(Func_1_t1979887667 * value)
	{
		___function_1 = value;
		Il2CppCodeGenWriteBarrier(&___function_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
