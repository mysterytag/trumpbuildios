﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ConcatObservable`1<System.Object>
struct ConcatObservable_1_t1571046694;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;
// System.Action
struct Action_t437523947;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ConcatObservable`1/Concat<System.Object>
struct  Concat_t120413325  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.ConcatObservable`1<T> UniRx.Operators.ConcatObservable`1/Concat::parent
	ConcatObservable_1_t1571046694 * ___parent_2;
	// System.Object UniRx.Operators.ConcatObservable`1/Concat::gate
	Il2CppObject * ___gate_3;
	// System.Boolean UniRx.Operators.ConcatObservable`1/Concat::isDisposed
	bool ___isDisposed_4;
	// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Operators.ConcatObservable`1/Concat::e
	Il2CppObject* ___e_5;
	// UniRx.SerialDisposable UniRx.Operators.ConcatObservable`1/Concat::subscription
	SerialDisposable_t2547852742 * ___subscription_6;
	// System.Action UniRx.Operators.ConcatObservable`1/Concat::nextSelf
	Action_t437523947 * ___nextSelf_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Concat_t120413325, ___parent_2)); }
	inline ConcatObservable_1_t1571046694 * get_parent_2() const { return ___parent_2; }
	inline ConcatObservable_1_t1571046694 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ConcatObservable_1_t1571046694 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Concat_t120413325, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_isDisposed_4() { return static_cast<int32_t>(offsetof(Concat_t120413325, ___isDisposed_4)); }
	inline bool get_isDisposed_4() const { return ___isDisposed_4; }
	inline bool* get_address_of_isDisposed_4() { return &___isDisposed_4; }
	inline void set_isDisposed_4(bool value)
	{
		___isDisposed_4 = value;
	}

	inline static int32_t get_offset_of_e_5() { return static_cast<int32_t>(offsetof(Concat_t120413325, ___e_5)); }
	inline Il2CppObject* get_e_5() const { return ___e_5; }
	inline Il2CppObject** get_address_of_e_5() { return &___e_5; }
	inline void set_e_5(Il2CppObject* value)
	{
		___e_5 = value;
		Il2CppCodeGenWriteBarrier(&___e_5, value);
	}

	inline static int32_t get_offset_of_subscription_6() { return static_cast<int32_t>(offsetof(Concat_t120413325, ___subscription_6)); }
	inline SerialDisposable_t2547852742 * get_subscription_6() const { return ___subscription_6; }
	inline SerialDisposable_t2547852742 ** get_address_of_subscription_6() { return &___subscription_6; }
	inline void set_subscription_6(SerialDisposable_t2547852742 * value)
	{
		___subscription_6 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_6, value);
	}

	inline static int32_t get_offset_of_nextSelf_7() { return static_cast<int32_t>(offsetof(Concat_t120413325, ___nextSelf_7)); }
	inline Action_t437523947 * get_nextSelf_7() const { return ___nextSelf_7; }
	inline Action_t437523947 ** get_address_of_nextSelf_7() { return &___nextSelf_7; }
	inline void set_nextSelf_7(Action_t437523947 * value)
	{
		___nextSelf_7 = value;
		Il2CppCodeGenWriteBarrier(&___nextSelf_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
