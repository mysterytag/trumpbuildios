﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,PlayFab.UUnit.Region>
struct ShimEnumerator_t3212835608;
// System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>
struct Dictionary_2_t1782110920;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1426499528_gshared (ShimEnumerator_t3212835608 * __this, Dictionary_2_t1782110920 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1426499528(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3212835608 *, Dictionary_2_t1782110920 *, const MethodInfo*))ShimEnumerator__ctor_m1426499528_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,PlayFab.UUnit.Region>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2463609885_gshared (ShimEnumerator_t3212835608 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2463609885(__this, method) ((  bool (*) (ShimEnumerator_t3212835608 *, const MethodInfo*))ShimEnumerator_MoveNext_m2463609885_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,PlayFab.UUnit.Region>::get_Entry()
extern "C"  DictionaryEntry_t130027246  ShimEnumerator_get_Entry_m130390445_gshared (ShimEnumerator_t3212835608 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m130390445(__this, method) ((  DictionaryEntry_t130027246  (*) (ShimEnumerator_t3212835608 *, const MethodInfo*))ShimEnumerator_get_Entry_m130390445_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,PlayFab.UUnit.Region>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1740357932_gshared (ShimEnumerator_t3212835608 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1740357932(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3212835608 *, const MethodInfo*))ShimEnumerator_get_Key_m1740357932_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,PlayFab.UUnit.Region>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2790130878_gshared (ShimEnumerator_t3212835608 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2790130878(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3212835608 *, const MethodInfo*))ShimEnumerator_get_Value_m2790130878_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,PlayFab.UUnit.Region>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2680871878_gshared (ShimEnumerator_t3212835608 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2680871878(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3212835608 *, const MethodInfo*))ShimEnumerator_get_Current_m2680871878_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,PlayFab.UUnit.Region>::Reset()
extern "C"  void ShimEnumerator_Reset_m3707246106_gshared (ShimEnumerator_t3212835608 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3707246106(__this, method) ((  void (*) (ShimEnumerator_t3212835608 *, const MethodInfo*))ShimEnumerator_Reset_m3707246106_gshared)(__this, method)
