﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Mono_Security_Mono_Security_X509_X520_AttributeTyp3850245072.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X520/CountryName
struct  CountryName_t191116065  : public AttributeTypeAndValue_t3850245072
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
