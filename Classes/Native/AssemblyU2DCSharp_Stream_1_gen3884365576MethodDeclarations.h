﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::.ctor()
#define Stream_1__ctor_m3284131035(__this, method) ((  void (*) (Stream_1_t3884365576 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::Send(T)
#define Stream_1_Send_m1813348589(__this, ___value0, method) ((  void (*) (Stream_1_t3884365576 *, CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m908637763(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t3884365576 *, CollectionRemoveEvent_1_t898259258 *, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<CollectionRemoveEvent`1<System.Object>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m2494911743(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t3884365576 *, Action_1_t1046711963 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<CollectionRemoveEvent`1<System.Object>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m3787213000(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t3884365576 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::Dispose()
#define Stream_1_Dispose_m3442212440(__this, method) ((  void (*) (Stream_1_t3884365576 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m88709637(__this, ___stream0, method) ((  void (*) (Stream_1_t3884365576 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::ClearInputStream()
#define Stream_1_ClearInputStream_m190586150(__this, method) ((  void (*) (Stream_1_t3884365576 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m1643074554(__this, method) ((  void (*) (Stream_1_t3884365576 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::Unpack(System.Int32)
#define Stream_1_Unpack_m3046511500(__this, ___p0, method) ((  void (*) (Stream_1_t3884365576 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<System.Object>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m2026896056(__this, ___val0, method) ((  void (*) (Stream_1_t3884365576 *, CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
