﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DelayObservable`1<System.Object>
struct DelayObservable_1_t2635131389;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>
struct Queue_1_t4227271813;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DelayObservable`1/Delay<System.Object>
struct  Delay_t3380693988  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DelayObservable`1<T> UniRx.Operators.DelayObservable`1/Delay::parent
	DelayObservable_1_t2635131389 * ___parent_2;
	// System.Object UniRx.Operators.DelayObservable`1/Delay::gate
	Il2CppObject * ___gate_3;
	// System.Boolean UniRx.Operators.DelayObservable`1/Delay::hasFailed
	bool ___hasFailed_4;
	// System.Boolean UniRx.Operators.DelayObservable`1/Delay::running
	bool ___running_5;
	// System.Boolean UniRx.Operators.DelayObservable`1/Delay::active
	bool ___active_6;
	// System.Exception UniRx.Operators.DelayObservable`1/Delay::exception
	Exception_t1967233988 * ___exception_7;
	// System.Collections.Generic.Queue`1<UniRx.Timestamped`1<T>> UniRx.Operators.DelayObservable`1/Delay::queue
	Queue_1_t4227271813 * ___queue_8;
	// System.Boolean UniRx.Operators.DelayObservable`1/Delay::onCompleted
	bool ___onCompleted_9;
	// System.DateTimeOffset UniRx.Operators.DelayObservable`1/Delay::completeAt
	DateTimeOffset_t3712260035  ___completeAt_10;
	// System.IDisposable UniRx.Operators.DelayObservable`1/Delay::sourceSubscription
	Il2CppObject * ___sourceSubscription_11;
	// System.TimeSpan UniRx.Operators.DelayObservable`1/Delay::delay
	TimeSpan_t763862892  ___delay_12;
	// System.Boolean UniRx.Operators.DelayObservable`1/Delay::ready
	bool ___ready_13;
	// UniRx.SerialDisposable UniRx.Operators.DelayObservable`1/Delay::cancelable
	SerialDisposable_t2547852742 * ___cancelable_14;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___parent_2)); }
	inline DelayObservable_1_t2635131389 * get_parent_2() const { return ___parent_2; }
	inline DelayObservable_1_t2635131389 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DelayObservable_1_t2635131389 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_hasFailed_4() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___hasFailed_4)); }
	inline bool get_hasFailed_4() const { return ___hasFailed_4; }
	inline bool* get_address_of_hasFailed_4() { return &___hasFailed_4; }
	inline void set_hasFailed_4(bool value)
	{
		___hasFailed_4 = value;
	}

	inline static int32_t get_offset_of_running_5() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___running_5)); }
	inline bool get_running_5() const { return ___running_5; }
	inline bool* get_address_of_running_5() { return &___running_5; }
	inline void set_running_5(bool value)
	{
		___running_5 = value;
	}

	inline static int32_t get_offset_of_active_6() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___active_6)); }
	inline bool get_active_6() const { return ___active_6; }
	inline bool* get_address_of_active_6() { return &___active_6; }
	inline void set_active_6(bool value)
	{
		___active_6 = value;
	}

	inline static int32_t get_offset_of_exception_7() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___exception_7)); }
	inline Exception_t1967233988 * get_exception_7() const { return ___exception_7; }
	inline Exception_t1967233988 ** get_address_of_exception_7() { return &___exception_7; }
	inline void set_exception_7(Exception_t1967233988 * value)
	{
		___exception_7 = value;
		Il2CppCodeGenWriteBarrier(&___exception_7, value);
	}

	inline static int32_t get_offset_of_queue_8() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___queue_8)); }
	inline Queue_1_t4227271813 * get_queue_8() const { return ___queue_8; }
	inline Queue_1_t4227271813 ** get_address_of_queue_8() { return &___queue_8; }
	inline void set_queue_8(Queue_1_t4227271813 * value)
	{
		___queue_8 = value;
		Il2CppCodeGenWriteBarrier(&___queue_8, value);
	}

	inline static int32_t get_offset_of_onCompleted_9() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___onCompleted_9)); }
	inline bool get_onCompleted_9() const { return ___onCompleted_9; }
	inline bool* get_address_of_onCompleted_9() { return &___onCompleted_9; }
	inline void set_onCompleted_9(bool value)
	{
		___onCompleted_9 = value;
	}

	inline static int32_t get_offset_of_completeAt_10() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___completeAt_10)); }
	inline DateTimeOffset_t3712260035  get_completeAt_10() const { return ___completeAt_10; }
	inline DateTimeOffset_t3712260035 * get_address_of_completeAt_10() { return &___completeAt_10; }
	inline void set_completeAt_10(DateTimeOffset_t3712260035  value)
	{
		___completeAt_10 = value;
	}

	inline static int32_t get_offset_of_sourceSubscription_11() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___sourceSubscription_11)); }
	inline Il2CppObject * get_sourceSubscription_11() const { return ___sourceSubscription_11; }
	inline Il2CppObject ** get_address_of_sourceSubscription_11() { return &___sourceSubscription_11; }
	inline void set_sourceSubscription_11(Il2CppObject * value)
	{
		___sourceSubscription_11 = value;
		Il2CppCodeGenWriteBarrier(&___sourceSubscription_11, value);
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___delay_12)); }
	inline TimeSpan_t763862892  get_delay_12() const { return ___delay_12; }
	inline TimeSpan_t763862892 * get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(TimeSpan_t763862892  value)
	{
		___delay_12 = value;
	}

	inline static int32_t get_offset_of_ready_13() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___ready_13)); }
	inline bool get_ready_13() const { return ___ready_13; }
	inline bool* get_address_of_ready_13() { return &___ready_13; }
	inline void set_ready_13(bool value)
	{
		___ready_13 = value;
	}

	inline static int32_t get_offset_of_cancelable_14() { return static_cast<int32_t>(offsetof(Delay_t3380693988, ___cancelable_14)); }
	inline SerialDisposable_t2547852742 * get_cancelable_14() const { return ___cancelable_14; }
	inline SerialDisposable_t2547852742 ** get_address_of_cancelable_14() { return &___cancelable_14; }
	inline void set_cancelable_14(SerialDisposable_t2547852742 * value)
	{
		___cancelable_14 = value;
		Il2CppCodeGenWriteBarrier(&___cancelable_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
