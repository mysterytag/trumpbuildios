﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableTransformChangedTrigger
struct ObservableTransformChangedTrigger_t2733332686;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableTransformChangedTrigger::.ctor()
extern "C"  void ObservableTransformChangedTrigger__ctor_m2235549981 (ObservableTransformChangedTrigger_t2733332686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTransformChangedTrigger::OnBeforeTransformParentChanged()
extern "C"  void ObservableTransformChangedTrigger_OnBeforeTransformParentChanged_m401929347 (ObservableTransformChangedTrigger_t2733332686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::OnBeforeTransformParentChangedAsObservable()
extern "C"  Il2CppObject* ObservableTransformChangedTrigger_OnBeforeTransformParentChangedAsObservable_m2296209728 (ObservableTransformChangedTrigger_t2733332686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTransformChangedTrigger::OnTransformParentChanged()
extern "C"  void ObservableTransformChangedTrigger_OnTransformParentChanged_m3001308580 (ObservableTransformChangedTrigger_t2733332686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::OnTransformParentChangedAsObservable()
extern "C"  Il2CppObject* ObservableTransformChangedTrigger_OnTransformParentChangedAsObservable_m2779451105 (ObservableTransformChangedTrigger_t2733332686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTransformChangedTrigger::OnTransformChildrenChanged()
extern "C"  void ObservableTransformChangedTrigger_OnTransformChildrenChanged_m3294885295 (ObservableTransformChangedTrigger_t2733332686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::OnTransformChildrenChangedAsObservable()
extern "C"  Il2CppObject* ObservableTransformChangedTrigger_OnTransformChildrenChangedAsObservable_m2626561132 (ObservableTransformChangedTrigger_t2733332686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableTransformChangedTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableTransformChangedTrigger_RaiseOnCompletedOnDestroy_m334098390 (ObservableTransformChangedTrigger_t2733332686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
