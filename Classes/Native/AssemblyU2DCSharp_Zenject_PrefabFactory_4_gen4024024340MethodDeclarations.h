﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>
struct PrefabFactory_4_t4024024340;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"

// System.Void Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void PrefabFactory_4__ctor_m2509232688_gshared (PrefabFactory_4_t4024024340 * __this, const MethodInfo* method);
#define PrefabFactory_4__ctor_m2509232688(__this, method) ((  void (*) (PrefabFactory_4_t4024024340 *, const MethodInfo*))PrefabFactory_4__ctor_m2509232688_gshared)(__this, method)
// T Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(UnityEngine.GameObject,P1,P2,P3)
extern "C"  Il2CppObject * PrefabFactory_4_Create_m3562910205_gshared (PrefabFactory_4_t4024024340 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject * ___param1, Il2CppObject * ___param22, Il2CppObject * ___param33, const MethodInfo* method);
#define PrefabFactory_4_Create_m3562910205(__this, ___prefab0, ___param1, ___param22, ___param33, method) ((  Il2CppObject * (*) (PrefabFactory_4_t4024024340 *, GameObject_t4012695102 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))PrefabFactory_4_Create_m3562910205_gshared)(__this, ___prefab0, ___param1, ___param22, ___param33, method)
// T Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::Create(System.String,P1,P2,P3)
extern "C"  Il2CppObject * PrefabFactory_4_Create_m1225018481_gshared (PrefabFactory_4_t4024024340 * __this, String_t* ___prefabResourceName0, Il2CppObject * ___param1, Il2CppObject * ___param22, Il2CppObject * ___param33, const MethodInfo* method);
#define PrefabFactory_4_Create_m1225018481(__this, ___prefabResourceName0, ___param1, ___param22, ___param33, method) ((  Il2CppObject * (*) (PrefabFactory_4_t4024024340 *, String_t*, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))PrefabFactory_4_Create_m1225018481_gshared)(__this, ___prefabResourceName0, ___param1, ___param22, ___param33, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.PrefabFactory`4<System.Object,System.Object,System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* PrefabFactory_4_Validate_m3057531913_gshared (PrefabFactory_4_t4024024340 * __this, const MethodInfo* method);
#define PrefabFactory_4_Validate_m3057531913(__this, method) ((  Il2CppObject* (*) (PrefabFactory_4_t4024024340 *, const MethodInfo*))PrefabFactory_4_Validate_m3057531913_gshared)(__this, method)
