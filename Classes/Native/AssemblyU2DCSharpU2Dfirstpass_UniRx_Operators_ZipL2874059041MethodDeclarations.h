﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>
struct ZipLatest_t2874059041;
// UniRx.Operators.ZipLatestObservable`1<System.Object>
struct ZipLatestObservable_1_t2717078458;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3003598734;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>::.ctor(UniRx.Operators.ZipLatestObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void ZipLatest__ctor_m3920799819_gshared (ZipLatest_t2874059041 * __this, ZipLatestObservable_1_t2717078458 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ZipLatest__ctor_m3920799819(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ZipLatest_t2874059041 *, ZipLatestObservable_1_t2717078458 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipLatest__ctor_m3920799819_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>::Run()
extern "C"  Il2CppObject * ZipLatest_Run_m3414372351_gshared (ZipLatest_t2874059041 * __this, const MethodInfo* method);
#define ZipLatest_Run_m3414372351(__this, method) ((  Il2CppObject * (*) (ZipLatest_t2874059041 *, const MethodInfo*))ZipLatest_Run_m3414372351_gshared)(__this, method)
// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>::Publish(System.Int32)
extern "C"  void ZipLatest_Publish_m2208776495_gshared (ZipLatest_t2874059041 * __this, int32_t ___index0, const MethodInfo* method);
#define ZipLatest_Publish_m2208776495(__this, ___index0, method) ((  void (*) (ZipLatest_t2874059041 *, int32_t, const MethodInfo*))ZipLatest_Publish_m2208776495_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>::OnNext(System.Collections.Generic.IList`1<T>)
extern "C"  void ZipLatest_OnNext_m3420429456_gshared (ZipLatest_t2874059041 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define ZipLatest_OnNext_m3420429456(__this, ___value0, method) ((  void (*) (ZipLatest_t2874059041 *, Il2CppObject*, const MethodInfo*))ZipLatest_OnNext_m3420429456_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>::OnError(System.Exception)
extern "C"  void ZipLatest_OnError_m4163364200_gshared (ZipLatest_t2874059041 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ZipLatest_OnError_m4163364200(__this, ___error0, method) ((  void (*) (ZipLatest_t2874059041 *, Exception_t1967233988 *, const MethodInfo*))ZipLatest_OnError_m4163364200_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>::OnCompleted()
extern "C"  void ZipLatest_OnCompleted_m441808059_gshared (ZipLatest_t2874059041 * __this, const MethodInfo* method);
#define ZipLatest_OnCompleted_m441808059(__this, method) ((  void (*) (ZipLatest_t2874059041 *, const MethodInfo*))ZipLatest_OnCompleted_m441808059_gshared)(__this, method)
