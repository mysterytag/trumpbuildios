﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m260774232_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m260774232(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m260774232_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  int32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3840031457_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3840031457(__this, method) ((  int32_t (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3840031457_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3128249368_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3128249368(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3128249368_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2859677817_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2859677817(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2859677817_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2806456816_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2806456816(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2806456816_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>::MoveNext()
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3284508004_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3284508004(__this, method) ((  bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3284508004_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>::Dispose()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1394216853_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1394216853(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1394216853_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Int32>::Reset()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2202174469_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2202174469(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2978233240 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m2202174469_gshared)(__this, method)
