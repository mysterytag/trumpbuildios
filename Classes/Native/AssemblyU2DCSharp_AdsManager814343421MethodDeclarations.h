﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdsManager
struct AdsManager_t814343421;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void AdsManager::.ctor()
extern "C"  void AdsManager__ctor_m3400990782 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdsManager::get_videoPlaying()
extern "C"  bool AdsManager_get_videoPlaying_m1050990550 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::set_videoPlaying(System.Boolean)
extern "C"  void AdsManager_set_videoPlaying_m3823594637 (AdsManager_t814343421 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::Start()
extern "C"  void AdsManager_Start_m2348128574 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onBannerLoaded()
extern "C"  void AdsManager_onBannerLoaded_m3359865014 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onBannerFailedToLoad(System.String)
extern "C"  void AdsManager_onBannerFailedToLoad_m4044754003 (AdsManager_t814343421 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onBannerShown()
extern "C"  void AdsManager_onBannerShown_m3917249250 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onBannerClicked()
extern "C"  void AdsManager_onBannerClicked_m2012419192 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onInterstitialLoaded()
extern "C"  void AdsManager_onInterstitialLoaded_m3914372534 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onInterstitialFailedToLoad(System.String,System.String)
extern "C"  void AdsManager_onInterstitialFailedToLoad_m1542990415 (AdsManager_t814343421 * __this, String_t* ___source0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onInterstitialShown(System.String,System.String)
extern "C"  void AdsManager_onInterstitialShown_m3432120476 (AdsManager_t814343421 * __this, String_t* ___source0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onInterstitialClicked(System.String,System.String)
extern "C"  void AdsManager_onInterstitialClicked_m1257953606 (AdsManager_t814343421 * __this, String_t* ___source0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onInterstitialClosed(System.String,System.String)
extern "C"  void AdsManager_onInterstitialClosed_m3456557121 (AdsManager_t814343421 * __this, String_t* ___source0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::ShowVideo()
extern "C"  void AdsManager_ShowVideo_m3892082650 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::ShowBanner()
extern "C"  void AdsManager_ShowBanner_m1683291087 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::HideBanner()
extern "C"  void AdsManager_HideBanner_m3097487636 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onVideoLoaded()
extern "C"  void AdsManager_onVideoLoaded_m2107316029 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onVideoFailedToLoad()
extern "C"  void AdsManager_onVideoFailedToLoad_m1333895254 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onVideoShown()
extern "C"  void AdsManager_onVideoShown_m2768465787 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onVideoFinished(System.String)
extern "C"  void AdsManager_onVideoFinished_m3979871384 (AdsManager_t814343421 * __this, String_t* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onVideoClosed()
extern "C"  void AdsManager_onVideoClosed_m1354042084 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::onVideoClicked()
extern "C"  void AdsManager_onVideoClicked_m1838106321 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::ClearInterstitialShowTime()
extern "C"  void AdsManager_ClearInterstitialShowTime_m3449981375 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::ShowInterstitial()
extern "C"  void AdsManager_ShowInterstitial_m1807540815 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdsManager::ShowImageInterstitial()
extern "C"  bool AdsManager_ShowImageInterstitial_m1741067346 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdsManager::ShowVideoInterstitial()
extern "C"  bool AdsManager_ShowVideoInterstitial_m2022836082 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AdsManager::InterstitialAdsCoroutine()
extern "C"  Il2CppObject * AdsManager_InterstitialAdsCoroutine_m103212386 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdsManager::Waiting()
extern "C"  bool AdsManager_Waiting_m4041669493 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::Update()
extern "C"  void AdsManager_Update_m4078361231 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__CC(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__CC_m3082825283 (AdsManager_t814343421 * __this, String_t* ___s0, String_t* ___s11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__CD(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__CD_m3094141410 (AdsManager_t814343421 * __this, String_t* ___s0, String_t* ___s11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__CE(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__CE_m3105457537 (AdsManager_t814343421 * __this, String_t* ___s0, String_t* ___s11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__CF(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__CF_m3116773664 (AdsManager_t814343421 * __this, String_t* ___s0, String_t* ___s11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D0(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__D0_m3218618807 (AdsManager_t814343421 * __this, String_t* ___s0, String_t* ___s11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D1(System.String,System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__D1_m264175378 (AdsManager_t814343421 * __this, String_t* ___s0, String_t* ___s11, String_t* ___arg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D2(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__D2_m3241251061 (Il2CppObject * __this /* static, unused */, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D3(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__D3_m3252567188 (Il2CppObject * __this /* static, unused */, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D4(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__D4_m3263883315 (Il2CppObject * __this /* static, unused */, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D5(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__D5_m3275199442 (Il2CppObject * __this /* static, unused */, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D6(System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__D6_m3286515569 (Il2CppObject * __this /* static, unused */, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D7(System.String,System.String,System.String)
extern "C"  void AdsManager_U3CStartU3Em__D7_m2705946444 (Il2CppObject * __this /* static, unused */, String_t* ___adProvider0, String_t* ___currencyName1, String_t* ___currencyValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D8()
extern "C"  void AdsManager_U3CStartU3Em__D8_m917008303 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__D9(System.String)
extern "C"  void AdsManager_U3CStartU3Em__D9_m592724370 (AdsManager_t814343421 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<Start>m__DA()
extern "C"  void AdsManager_U3CStartU3Em__DA_m917016952 (AdsManager_t814343421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager::<onInterstitialFailedToLoad>m__DB(System.Int64)
extern "C"  void AdsManager_U3ConInterstitialFailedToLoadU3Em__DB_m1514020736 (AdsManager_t814343421 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
