﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkKongregateResponseCallback
struct LinkKongregateResponseCallback_t2160524123;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkKongregateAccountRequest
struct LinkKongregateAccountRequest_t2234535079;
// PlayFab.ClientModels.LinkKongregateAccountResult
struct LinkKongregateAccountResult_t1077060901;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkKongreg2234535079.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkKongreg1077060901.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkKongregateResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkKongregateResponseCallback__ctor_m4190582474 (LinkKongregateResponseCallback_t2160524123 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkKongregateResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkKongregateAccountRequest,PlayFab.ClientModels.LinkKongregateAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkKongregateResponseCallback_Invoke_m2345313573 (LinkKongregateResponseCallback_t2160524123 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkKongregateAccountRequest_t2234535079 * ___request2, LinkKongregateAccountResult_t1077060901 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkKongregateResponseCallback_t2160524123(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkKongregateAccountRequest_t2234535079 * ___request2, LinkKongregateAccountResult_t1077060901 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkKongregateResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkKongregateAccountRequest,PlayFab.ClientModels.LinkKongregateAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkKongregateResponseCallback_BeginInvoke_m1692626230 (LinkKongregateResponseCallback_t2160524123 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkKongregateAccountRequest_t2234535079 * ___request2, LinkKongregateAccountResult_t1077060901 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkKongregateResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkKongregateResponseCallback_EndInvoke_m1141480922 (LinkKongregateResponseCallback_t2160524123 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
