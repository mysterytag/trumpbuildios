﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromGameCenterIDs>c__AnonStorey6F
struct U3CGetPlayFabIDsFromGameCenterIDsU3Ec__AnonStorey6F_t3501509505;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromGameCenterIDs>c__AnonStorey6F::.ctor()
extern "C"  void U3CGetPlayFabIDsFromGameCenterIDsU3Ec__AnonStorey6F__ctor_m1063947602 (U3CGetPlayFabIDsFromGameCenterIDsU3Ec__AnonStorey6F_t3501509505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromGameCenterIDs>c__AnonStorey6F::<>m__5C(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPlayFabIDsFromGameCenterIDsU3Ec__AnonStorey6F_U3CU3Em__5C_m2464850654 (U3CGetPlayFabIDsFromGameCenterIDsU3Ec__AnonStorey6F_t3501509505 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
