﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
struct IEnumerable_1_t67735429;
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
struct IEnumerator_1_t2973654817;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// System.Func`2<System.Reflection.PropertyInfo,System.Boolean>
struct Func_2_t2846737840;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47
struct  U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871  : public Il2CppObject
{
public:
	// System.Type Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::type
	Type_t * ___type_0;
	// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::<propInfos>__0
	Il2CppObject* ___U3CpropInfosU3E__0_1;
	// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo> Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::<$s_269>__1
	Il2CppObject* ___U3CU24s_269U3E__1_2;
	// System.Reflection.PropertyInfo Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::<propInfo>__2
	PropertyInfo_t * ___U3CpropInfoU3E__2_3;
	// System.Int32 Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::$PC
	int32_t ___U24PC_4;
	// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::$current
	InjectableInfo_t1147709774 * ___U24current_5;
	// System.Type Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::<$>type
	Type_t * ___U3CU24U3Etype_6;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_U3CpropInfosU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871, ___U3CpropInfosU3E__0_1)); }
	inline Il2CppObject* get_U3CpropInfosU3E__0_1() const { return ___U3CpropInfosU3E__0_1; }
	inline Il2CppObject** get_address_of_U3CpropInfosU3E__0_1() { return &___U3CpropInfosU3E__0_1; }
	inline void set_U3CpropInfosU3E__0_1(Il2CppObject* value)
	{
		___U3CpropInfosU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpropInfosU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_269U3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871, ___U3CU24s_269U3E__1_2)); }
	inline Il2CppObject* get_U3CU24s_269U3E__1_2() const { return ___U3CU24s_269U3E__1_2; }
	inline Il2CppObject** get_address_of_U3CU24s_269U3E__1_2() { return &___U3CU24s_269U3E__1_2; }
	inline void set_U3CU24s_269U3E__1_2(Il2CppObject* value)
	{
		___U3CU24s_269U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_269U3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CpropInfoU3E__2_3() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871, ___U3CpropInfoU3E__2_3)); }
	inline PropertyInfo_t * get_U3CpropInfoU3E__2_3() const { return ___U3CpropInfoU3E__2_3; }
	inline PropertyInfo_t ** get_address_of_U3CpropInfoU3E__2_3() { return &___U3CpropInfoU3E__2_3; }
	inline void set_U3CpropInfoU3E__2_3(PropertyInfo_t * value)
	{
		___U3CpropInfoU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpropInfoU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871, ___U24current_5)); }
	inline InjectableInfo_t1147709774 * get_U24current_5() const { return ___U24current_5; }
	inline InjectableInfo_t1147709774 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(InjectableInfo_t1147709774 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_6() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871, ___U3CU24U3Etype_6)); }
	inline Type_t * get_U3CU24U3Etype_6() const { return ___U3CU24U3Etype_6; }
	inline Type_t ** get_address_of_U3CU24U3Etype_6() { return &___U3CU24U3Etype_6; }
	inline void set_U3CU24U3Etype_6(Type_t * value)
	{
		___U3CU24U3Etype_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etype_6, value);
	}
};

struct U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_StaticFields
{
public:
	// System.Func`2<System.Reflection.PropertyInfo,System.Boolean> Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::<>f__am$cache7
	Func_2_t2846737840 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t2846737840 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t2846737840 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t2846737840 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
