﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First<System.Object>
struct First_t3214976905;
// UniRx.Operators.FirstObservable`1<System.Object>
struct FirstObservable_1_t2859660834;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First<System.Object>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m714786572_gshared (First_t3214976905 * __this, FirstObservable_1_t2859660834 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First__ctor_m714786572(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First_t3214976905 *, FirstObservable_1_t2859660834 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First__ctor_m714786572_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Object>::OnNext(T)
extern "C"  void First_OnNext_m1616456489_gshared (First_t3214976905 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define First_OnNext_m1616456489(__this, ___value0, method) ((  void (*) (First_t3214976905 *, Il2CppObject *, const MethodInfo*))First_OnNext_m1616456489_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Object>::OnError(System.Exception)
extern "C"  void First_OnError_m3978023992_gshared (First_t3214976905 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First_OnError_m3978023992(__this, ___error0, method) ((  void (*) (First_t3214976905 *, Exception_t1967233988 *, const MethodInfo*))First_OnError_m3978023992_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Object>::OnCompleted()
extern "C"  void First_OnCompleted_m1092426635_gshared (First_t3214976905 * __this, const MethodInfo* method);
#define First_OnCompleted_m1092426635(__this, method) ((  void (*) (First_t3214976905 *, const MethodInfo*))First_OnCompleted_m1092426635_gshared)(__this, method)
