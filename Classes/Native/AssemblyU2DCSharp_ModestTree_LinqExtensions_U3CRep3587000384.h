﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Object
struct Il2CppObject;
// System.Predicate`1<System.Object>
struct Predicate_1_t1408070318;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1<System.Object>
struct  U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384  : public Il2CppObject
{
public:
	// System.Boolean ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::<replaced>__0
	bool ___U3CreplacedU3E__0_0;
	// System.Collections.Generic.IEnumerable`1<T> ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::enumerable
	Il2CppObject* ___enumerable_1;
	// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::<$s_238>__1
	Il2CppObject* ___U3CU24s_238U3E__1_2;
	// T ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::<t>__2
	Il2CppObject * ___U3CtU3E__2_3;
	// System.Predicate`1<T> ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::match
	Predicate_1_t1408070318 * ___match_4;
	// T ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::replacement
	Il2CppObject * ___replacement_5;
	// System.Int32 ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::$PC
	int32_t ___U24PC_6;
	// T ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::$current
	Il2CppObject * ___U24current_7;
	// System.Collections.Generic.IEnumerable`1<T> ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::<$>enumerable
	Il2CppObject* ___U3CU24U3Eenumerable_8;
	// System.Predicate`1<T> ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::<$>match
	Predicate_1_t1408070318 * ___U3CU24U3Ematch_9;
	// T ModestTree.LinqExtensions/<ReplaceOrAppend>c__Iterator39`1::<$>replacement
	Il2CppObject * ___U3CU24U3Ereplacement_10;

public:
	inline static int32_t get_offset_of_U3CreplacedU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___U3CreplacedU3E__0_0)); }
	inline bool get_U3CreplacedU3E__0_0() const { return ___U3CreplacedU3E__0_0; }
	inline bool* get_address_of_U3CreplacedU3E__0_0() { return &___U3CreplacedU3E__0_0; }
	inline void set_U3CreplacedU3E__0_0(bool value)
	{
		___U3CreplacedU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_enumerable_1() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___enumerable_1)); }
	inline Il2CppObject* get_enumerable_1() const { return ___enumerable_1; }
	inline Il2CppObject** get_address_of_enumerable_1() { return &___enumerable_1; }
	inline void set_enumerable_1(Il2CppObject* value)
	{
		___enumerable_1 = value;
		Il2CppCodeGenWriteBarrier(&___enumerable_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_238U3E__1_2() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___U3CU24s_238U3E__1_2)); }
	inline Il2CppObject* get_U3CU24s_238U3E__1_2() const { return ___U3CU24s_238U3E__1_2; }
	inline Il2CppObject** get_address_of_U3CU24s_238U3E__1_2() { return &___U3CU24s_238U3E__1_2; }
	inline void set_U3CU24s_238U3E__1_2(Il2CppObject* value)
	{
		___U3CU24s_238U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_238U3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CtU3E__2_3() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___U3CtU3E__2_3)); }
	inline Il2CppObject * get_U3CtU3E__2_3() const { return ___U3CtU3E__2_3; }
	inline Il2CppObject ** get_address_of_U3CtU3E__2_3() { return &___U3CtU3E__2_3; }
	inline void set_U3CtU3E__2_3(Il2CppObject * value)
	{
		___U3CtU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtU3E__2_3, value);
	}

	inline static int32_t get_offset_of_match_4() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___match_4)); }
	inline Predicate_1_t1408070318 * get_match_4() const { return ___match_4; }
	inline Predicate_1_t1408070318 ** get_address_of_match_4() { return &___match_4; }
	inline void set_match_4(Predicate_1_t1408070318 * value)
	{
		___match_4 = value;
		Il2CppCodeGenWriteBarrier(&___match_4, value);
	}

	inline static int32_t get_offset_of_replacement_5() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___replacement_5)); }
	inline Il2CppObject * get_replacement_5() const { return ___replacement_5; }
	inline Il2CppObject ** get_address_of_replacement_5() { return &___replacement_5; }
	inline void set_replacement_5(Il2CppObject * value)
	{
		___replacement_5 = value;
		Il2CppCodeGenWriteBarrier(&___replacement_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eenumerable_8() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___U3CU24U3Eenumerable_8)); }
	inline Il2CppObject* get_U3CU24U3Eenumerable_8() const { return ___U3CU24U3Eenumerable_8; }
	inline Il2CppObject** get_address_of_U3CU24U3Eenumerable_8() { return &___U3CU24U3Eenumerable_8; }
	inline void set_U3CU24U3Eenumerable_8(Il2CppObject* value)
	{
		___U3CU24U3Eenumerable_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eenumerable_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ematch_9() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___U3CU24U3Ematch_9)); }
	inline Predicate_1_t1408070318 * get_U3CU24U3Ematch_9() const { return ___U3CU24U3Ematch_9; }
	inline Predicate_1_t1408070318 ** get_address_of_U3CU24U3Ematch_9() { return &___U3CU24U3Ematch_9; }
	inline void set_U3CU24U3Ematch_9(Predicate_1_t1408070318 * value)
	{
		___U3CU24U3Ematch_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ematch_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ereplacement_10() { return static_cast<int32_t>(offsetof(U3CReplaceOrAppendU3Ec__Iterator39_1_t3587000384, ___U3CU24U3Ereplacement_10)); }
	inline Il2CppObject * get_U3CU24U3Ereplacement_10() const { return ___U3CU24U3Ereplacement_10; }
	inline Il2CppObject ** get_address_of_U3CU24U3Ereplacement_10() { return &___U3CU24U3Ereplacement_10; }
	inline void set_U3CU24U3Ereplacement_10(Il2CppObject * value)
	{
		___U3CU24U3Ereplacement_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ereplacement_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
