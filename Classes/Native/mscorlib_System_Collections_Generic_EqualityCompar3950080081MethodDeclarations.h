﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<SponsorPay.SPLogLevel>
struct EqualityComparer_1_t3950080081;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.EqualityComparer`1<SponsorPay.SPLogLevel>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2190692764_gshared (EqualityComparer_1_t3950080081 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2190692764(__this, method) ((  void (*) (EqualityComparer_1_t3950080081 *, const MethodInfo*))EqualityComparer_1__ctor_m2190692764_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<SponsorPay.SPLogLevel>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3004870033_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3004870033(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3004870033_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<SponsorPay.SPLogLevel>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2900712053_gshared (EqualityComparer_1_t3950080081 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2900712053(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3950080081 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2900712053_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<SponsorPay.SPLogLevel>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3375910965_gshared (EqualityComparer_1_t3950080081 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3375910965(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3950080081 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3375910965_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<SponsorPay.SPLogLevel>::get_Default()
extern "C"  EqualityComparer_1_t3950080081 * EqualityComparer_1_get_Default_m175513926_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m175513926(__this /* static, unused */, method) ((  EqualityComparer_1_t3950080081 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m175513926_gshared)(__this /* static, unused */, method)
