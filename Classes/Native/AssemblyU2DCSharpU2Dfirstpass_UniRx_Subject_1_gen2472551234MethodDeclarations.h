﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<System.Double>
struct Subject_1_t2472551234;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Subject`1<System.Double>::.ctor()
extern "C"  void Subject_1__ctor_m862046378_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method);
#define Subject_1__ctor_m862046378(__this, method) ((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))Subject_1__ctor_m862046378_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Double>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3034142370_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3034142370(__this, method) ((  bool (*) (Subject_1_t2472551234 *, const MethodInfo*))Subject_1_get_HasObservers_m3034142370_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Double>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3602535604_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3602535604(__this, method) ((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))Subject_1_OnCompleted_m3602535604_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Double>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m1588041697_gshared (Subject_1_t2472551234 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m1588041697(__this, ___error0, method) ((  void (*) (Subject_1_t2472551234 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1588041697_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<System.Double>::OnNext(T)
extern "C"  void Subject_1_OnNext_m2328584402_gshared (Subject_1_t2472551234 * __this, double ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m2328584402(__this, ___value0, method) ((  void (*) (Subject_1_t2472551234 *, double, const MethodInfo*))Subject_1_OnNext_m2328584402_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<System.Double>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m178381993_gshared (Subject_1_t2472551234 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m178381993(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2472551234 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m178381993_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<System.Double>::Dispose()
extern "C"  void Subject_1_Dispose_m3691131495_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m3691131495(__this, method) ((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))Subject_1_Dispose_m3691131495_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Double>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m3461776624_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m3461776624(__this, method) ((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m3461776624_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Double>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3358465817_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3358465817(__this, method) ((  bool (*) (Subject_1_t2472551234 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3358465817_gshared)(__this, method)
