﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>
struct U3CCreateTimerU3Ec__AnonStorey5D_t162307190;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>::.ctor()
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5D__ctor_m1070085756_gshared (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * __this, const MethodInfo* method);
#define U3CCreateTimerU3Ec__AnonStorey5D__ctor_m1070085756(__this, method) ((  void (*) (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 *, const MethodInfo*))U3CCreateTimerU3Ec__AnonStorey5D__ctor_m1070085756_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>::<>m__75()
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5D_U3CU3Em__75_m4163451299_gshared (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * __this, const MethodInfo* method);
#define U3CCreateTimerU3Ec__AnonStorey5D_U3CU3Em__75_m4163451299(__this, method) ((  void (*) (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 *, const MethodInfo*))U3CCreateTimerU3Ec__AnonStorey5D_U3CU3Em__75_m4163451299_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC/<CreateTimer>c__AnonStorey5D<System.Object>::<>m__76(System.Action`1<System.TimeSpan>)
extern "C"  void U3CCreateTimerU3Ec__AnonStorey5D_U3CU3Em__76_m4291568422_gshared (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 * __this, Action_1_t912315597 * ___self0, const MethodInfo* method);
#define U3CCreateTimerU3Ec__AnonStorey5D_U3CU3Em__76_m4291568422(__this, ___self0, method) ((  void (*) (U3CCreateTimerU3Ec__AnonStorey5D_t162307190 *, Action_1_t912315597 *, const MethodInfo*))U3CCreateTimerU3Ec__AnonStorey5D_U3CU3Em__76_m4291568422_gshared)(__this, ___self0, method)
