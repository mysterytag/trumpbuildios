﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1592429805.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"

// System.Void System.Array/InternalEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2549207061_gshared (InternalEnumerator_1_t1592429805 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2549207061(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1592429805 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2549207061_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2201025387_gshared (InternalEnumerator_1_t1592429805 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2201025387(__this, method) ((  void (*) (InternalEnumerator_1_t1592429805 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2201025387_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3092076503_gshared (InternalEnumerator_1_t1592429805 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3092076503(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1592429805 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3092076503_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2119925804_gshared (InternalEnumerator_1_t1592429805 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2119925804(__this, method) ((  void (*) (InternalEnumerator_1_t1592429805 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2119925804_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4116159703_gshared (InternalEnumerator_1_t1592429805 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4116159703(__this, method) ((  bool (*) (InternalEnumerator_1_t1592429805 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4116159703_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3169245916_gshared (InternalEnumerator_1_t1592429805 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3169245916(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1592429805 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3169245916_gshared)(__this, method)
