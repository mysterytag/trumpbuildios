﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InstanceProvider
struct InstanceProvider_t2383610171;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.InstanceProvider::.ctor(System.Type,System.Object)
extern "C"  void InstanceProvider__ctor_m608186609 (InstanceProvider_t2383610171 * __this, Type_t * ___instanceType0, Il2CppObject * ___instance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.InstanceProvider::GetInstanceType()
extern "C"  Type_t * InstanceProvider_GetInstanceType_m3028565759 (InstanceProvider_t2383610171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.InstanceProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * InstanceProvider_GetInstance_m2017904041 (InstanceProvider_t2383610171 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.InstanceProvider::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* InstanceProvider_ValidateBinding_m270655055 (InstanceProvider_t2383610171 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
