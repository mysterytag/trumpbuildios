﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Tweener
struct Tweener_t1766303790;
// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"

// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions46_DOFade_m3953805469 (Il2CppObject * __this /* static, unused */, Image_t3354615620 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions46_DOAnchorPosX_m1321998970 (Il2CppObject * __this /* static, unused */, RectTransform_t3317474837 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions46_DOAnchorPosY_m838323643 (Il2CppObject * __this /* static, unused */, RectTransform_t3317474837 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
