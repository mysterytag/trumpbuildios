﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3<System.Object,System.Object,System.Object>
struct CombineLatestObservable_3_t2740954106;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CombineLatestObservable`3<System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<TLeft>,UniRx.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
extern "C"  void CombineLatestObservable_3__ctor_m3043640545_gshared (CombineLatestObservable_3_t2740954106 * __this, Il2CppObject* ___left0, Il2CppObject* ___right1, Func_3_t1892209229 * ___selector2, const MethodInfo* method);
#define CombineLatestObservable_3__ctor_m3043640545(__this, ___left0, ___right1, ___selector2, method) ((  void (*) (CombineLatestObservable_3_t2740954106 *, Il2CppObject*, Il2CppObject*, Func_3_t1892209229 *, const MethodInfo*))CombineLatestObservable_3__ctor_m3043640545_gshared)(__this, ___left0, ___right1, ___selector2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_3_SubscribeCore_m2531434021_gshared (CombineLatestObservable_3_t2740954106 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CombineLatestObservable_3_SubscribeCore_m2531434021(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CombineLatestObservable_3_t2740954106 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatestObservable_3_SubscribeCore_m2531434021_gshared)(__this, ___observer0, ___cancel1, method)
