﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonId
struct  PrefabSingletonId_t3643845047  : public Il2CppObject
{
public:
	// System.String Zenject.PrefabSingletonId::Identifier
	String_t* ___Identifier_0;
	// UnityEngine.GameObject Zenject.PrefabSingletonId::Prefab
	GameObject_t4012695102 * ___Prefab_1;
	// System.String Zenject.PrefabSingletonId::ResourcePath
	String_t* ___ResourcePath_2;

public:
	inline static int32_t get_offset_of_Identifier_0() { return static_cast<int32_t>(offsetof(PrefabSingletonId_t3643845047, ___Identifier_0)); }
	inline String_t* get_Identifier_0() const { return ___Identifier_0; }
	inline String_t** get_address_of_Identifier_0() { return &___Identifier_0; }
	inline void set_Identifier_0(String_t* value)
	{
		___Identifier_0 = value;
		Il2CppCodeGenWriteBarrier(&___Identifier_0, value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(PrefabSingletonId_t3643845047, ___Prefab_1)); }
	inline GameObject_t4012695102 * get_Prefab_1() const { return ___Prefab_1; }
	inline GameObject_t4012695102 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(GameObject_t4012695102 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier(&___Prefab_1, value);
	}

	inline static int32_t get_offset_of_ResourcePath_2() { return static_cast<int32_t>(offsetof(PrefabSingletonId_t3643845047, ___ResourcePath_2)); }
	inline String_t* get_ResourcePath_2() const { return ___ResourcePath_2; }
	inline String_t** get_address_of_ResourcePath_2() { return &___ResourcePath_2; }
	inline void set_ResourcePath_2(String_t* value)
	{
		___ResourcePath_2 = value;
		Il2CppCodeGenWriteBarrier(&___ResourcePath_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
