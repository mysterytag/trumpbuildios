﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where_<UnityEngine.Vector2>
struct Where__t3586611641;
// UniRx.Operators.WhereObservable`1<UnityEngine.Vector2>
struct WhereObservable_1_t428922441;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where_<UnityEngine.Vector2>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where___ctor_m695703770_gshared (Where__t3586611641 * __this, WhereObservable_1_t428922441 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where___ctor_m695703770(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where__t3586611641 *, WhereObservable_1_t428922441 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where___ctor_m695703770_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<UnityEngine.Vector2>::OnNext(T)
extern "C"  void Where__OnNext_m3909082240_gshared (Where__t3586611641 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define Where__OnNext_m3909082240(__this, ___value0, method) ((  void (*) (Where__t3586611641 *, Vector2_t3525329788 , const MethodInfo*))Where__OnNext_m3909082240_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void Where__OnError_m1174075279_gshared (Where__t3586611641 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where__OnError_m1174075279(__this, ___error0, method) ((  void (*) (Where__t3586611641 *, Exception_t1967233988 *, const MethodInfo*))Where__OnError_m1174075279_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<UnityEngine.Vector2>::OnCompleted()
extern "C"  void Where__OnCompleted_m3385674082_gshared (Where__t3586611641 * __this, const MethodInfo* method);
#define Where__OnCompleted_m3385674082(__this, method) ((  void (*) (Where__t3586611641 *, const MethodInfo*))Where__OnCompleted_m3385674082_gshared)(__this, method)
