﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.PlayFabClientAPI
struct  PlayFabClientAPI_t4176460733  : public Il2CppObject
{
public:

public:
};

struct PlayFabClientAPI_t4176460733_StaticFields
{
public:
	// System.String PlayFab.PlayFabClientAPI::_authKey
	String_t* ____authKey_0;

public:
	inline static int32_t get_offset_of__authKey_0() { return static_cast<int32_t>(offsetof(PlayFabClientAPI_t4176460733_StaticFields, ____authKey_0)); }
	inline String_t* get__authKey_0() const { return ____authKey_0; }
	inline String_t** get_address_of__authKey_0() { return &____authKey_0; }
	inline void set__authKey_0(String_t* value)
	{
		____authKey_0 = value;
		Il2CppCodeGenWriteBarrier(&____authKey_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
