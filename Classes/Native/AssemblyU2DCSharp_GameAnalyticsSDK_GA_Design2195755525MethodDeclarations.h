﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3844246929.h"

// System.Void GameAnalyticsSDK.GA_Design::NewEvent(System.String,System.Single)
extern "C"  void GA_Design_NewEvent_m2732472201 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, float ___eventValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Design::NewEvent(System.String)
extern "C"  void GA_Design_NewEvent_m2037308324 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Design::CreateNewEvent(System.String,System.Nullable`1<System.Single>)
extern "C"  void GA_Design_CreateNewEvent_m1830518076 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Nullable_1_t3844246929  ___eventValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
