﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2<System.Object,System.Object>
struct U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAccumulateU3Ec__AnonStoreyFC_2__ctor_m987777597_gshared (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * __this, const MethodInfo* method);
#define U3CAccumulateU3Ec__AnonStoreyFC_2__ctor_m987777597(__this, method) ((  void (*) (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 *, const MethodInfo*))U3CAccumulateU3Ec__AnonStoreyFC_2__ctor_m987777597_gshared)(__this, method)
// System.Void CellReactiveApi/<Accumulate>c__AnonStoreyFB`2/<Accumulate>c__AnonStoreyFC`2<System.Object,System.Object>::<>m__191(T2)
extern "C"  void U3CAccumulateU3Ec__AnonStoreyFC_2_U3CU3Em__191_m2024471559_gshared (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CAccumulateU3Ec__AnonStoreyFC_2_U3CU3Em__191_m2024471559(__this, ___val0, method) ((  void (*) (U3CAccumulateU3Ec__AnonStoreyFC_2_t3105718393 *, Il2CppObject *, const MethodInfo*))U3CAccumulateU3Ec__AnonStoreyFC_2_U3CU3Em__191_m2024471559_gshared)(__this, ___val0, method)
