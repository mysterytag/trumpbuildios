﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<UnityEngine.Color,UnityEngine.Color>
struct Func_2_t2057442760;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`2<UnityEngine.Color,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m424454832_gshared (Func_2_t2057442760 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m424454832(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2057442760 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m424454832_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UnityEngine.Color,UnityEngine.Color>::Invoke(T)
extern "C"  Color_t1588175760  Func_2_Invoke_m985981234_gshared (Func_2_t2057442760 * __this, Color_t1588175760  ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m985981234(__this, ___arg10, method) ((  Color_t1588175760  (*) (Func_2_t2057442760 *, Color_t1588175760 , const MethodInfo*))Func_2_Invoke_m985981234_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UnityEngine.Color,UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m311746913_gshared (Func_2_t2057442760 * __this, Color_t1588175760  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m311746913(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2057442760 *, Color_t1588175760 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m311746913_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UnityEngine.Color,UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t1588175760  Func_2_EndInvoke_m4122244578_gshared (Func_2_t2057442760 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m4122244578(__this, ___result0, method) ((  Color_t1588175760  (*) (Func_2_t2057442760 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4122244578_gshared)(__this, ___result0, method)
