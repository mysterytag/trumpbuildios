﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186MethodDeclarations.h"

// System.Void UniRx.Tuple`2<System.Single[],System.Int32>::.ctor(T1,T2)
#define Tuple_2__ctor_m4200422816(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t3951111710 *, SingleU5BU5D_t1219431280*, int32_t, const MethodInfo*))Tuple_2__ctor_m1925708277_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<System.Single[],System.Int32>::System.IComparable.CompareTo(System.Object)
#define Tuple_2_System_IComparable_CompareTo_m3018813141(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t3951111710 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m3860401504_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.Single[],System.Int32>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m2348654567(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t3951111710 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m2475097650_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<System.Single[],System.Int32>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m3931259614(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t3951111710 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m2785203497_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<System.Single[],System.Int32>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m3004560156(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t3951111710 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m4116156913_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<System.Single[],System.Int32>::UniRx.ITuple.ToString()
#define Tuple_2_UniRx_ITuple_ToString_m3174243273(__this, method) ((  String_t* (*) (Tuple_2_t3951111710 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m1025111316_gshared)(__this, method)
// T1 UniRx.Tuple`2<System.Single[],System.Int32>::get_Item1()
#define Tuple_2_get_Item1_m3917678752(__this, method) ((  SingleU5BU5D_t1219431280* (*) (Tuple_2_t3951111710 *, const MethodInfo*))Tuple_2_get_Item1_m3848247211_gshared)(__this, method)
// T2 UniRx.Tuple`2<System.Single[],System.Int32>::get_Item2()
#define Tuple_2_get_Item2_m1987060608(__this, method) ((  int32_t (*) (Tuple_2_t3951111710 *, const MethodInfo*))Tuple_2_get_Item2_m3868585547_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.Single[],System.Int32>::Equals(System.Object)
#define Tuple_2_Equals_m521616786(__this, ___obj0, method) ((  bool (*) (Tuple_2_t3951111710 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m201304029_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.Single[],System.Int32>::GetHashCode()
#define Tuple_2_GetHashCode_m1500587306(__this, method) ((  int32_t (*) (Tuple_2_t3951111710 *, const MethodInfo*))Tuple_2_GetHashCode_m849819893_gshared)(__this, method)
// System.String UniRx.Tuple`2<System.Single[],System.Int32>::ToString()
#define Tuple_2_ToString_m2478001352(__this, method) ((  String_t* (*) (Tuple_2_t3951111710 *, const MethodInfo*))Tuple_2_ToString_m172897693_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.Single[],System.Int32>::Equals(UniRx.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m3004044823(__this, ___other0, method) ((  bool (*) (Tuple_2_t3951111710 *, Tuple_2_t3951111710 , const MethodInfo*))Tuple_2_Equals_m2628948332_gshared)(__this, ___other0, method)
