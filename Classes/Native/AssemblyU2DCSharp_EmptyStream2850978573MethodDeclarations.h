﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmptyStream
struct EmptyStream_t2850978573;
// IEmptyStream
struct IEmptyStream_t3684082468;

#include "codegen/il2cpp-codegen.h"

// System.Void EmptyStream::.ctor()
extern "C"  void EmptyStream__ctor_m2786205438 (EmptyStream_t2850978573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmptyStream::Send()
extern "C"  void EmptyStream_Send_m2813379598 (EmptyStream_t2850978573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmptyStream::SetInputStream(IEmptyStream)
extern "C"  void EmptyStream_SetInputStream_m3583425098 (EmptyStream_t2850978573 * __this, Il2CppObject * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmptyStream::<SetInputStream>m__1BF()
extern "C"  void EmptyStream_U3CSetInputStreamU3Em__1BF_m4047172200 (EmptyStream_t2850978573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
