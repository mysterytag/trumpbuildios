﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ConfirmPurchaseRequest
struct ConfirmPurchaseRequest_t1360937886;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ConfirmPurchaseRequest::.ctor()
extern "C"  void ConfirmPurchaseRequest__ctor_m2594010847 (ConfirmPurchaseRequest_t1360937886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ConfirmPurchaseRequest::get_OrderId()
extern "C"  String_t* ConfirmPurchaseRequest_get_OrderId_m3715466368 (ConfirmPurchaseRequest_t1360937886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConfirmPurchaseRequest::set_OrderId(System.String)
extern "C"  void ConfirmPurchaseRequest_set_OrderId_m1542164345 (ConfirmPurchaseRequest_t1360937886 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
