﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuspiciousVKWrapper
struct  SuspiciousVKWrapper_t2556219713  : public MonoBehaviour_t3012272455
{
public:
	// System.String SuspiciousVKWrapper::link
	String_t* ___link_2;

public:
	inline static int32_t get_offset_of_link_2() { return static_cast<int32_t>(offsetof(SuspiciousVKWrapper_t2556219713, ___link_2)); }
	inline String_t* get_link_2() const { return ___link_2; }
	inline String_t** get_address_of_link_2() { return &___link_2; }
	inline void set_link_2(String_t* value)
	{
		___link_2 = value;
		Il2CppCodeGenWriteBarrier(&___link_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
