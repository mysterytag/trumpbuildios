﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.Operators.NeverObservable`1<System.Object>
struct NeverObservable_1_t958614430;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>
struct NthCombineLatestObserverBase_1_t2199482330;
// System.Exception
struct Exception_t1967233988;
// UniRx.Operators.NthZipLatestObserverBase`1<System.Object>
struct NthZipLatestObserverBase_1_t3084363628;
// UniRx.Operators.NthZipObserverBase`1<System.Object>
struct NthZipObserverBase_1_t3759589597;
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t1434690852;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<System.Object>
struct U3COnCompletedU3Ec__AnonStorey63_t4115224287;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<UniRx.Unit>
struct U3COnCompletedU3Ec__AnonStorey63_t1541436609;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<System.Object>
struct U3COnErrorU3Ec__AnonStorey62_t681819067;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<UniRx.Unit>
struct U3COnErrorU3Ec__AnonStorey62_t2402998685;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<System.Object>
struct U3COnNextU3Ec__AnonStorey61_t1362241787;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<UniRx.Unit>
struct U3COnNextU3Ec__AnonStorey61_t3083421405;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>
struct ObserveOn__t947369629;
// UniRx.Operators.ObserveOnObservable`1<System.Object>
struct ObserveOnObservable_1_t1522207957;
// UniRx.ISchedulerQueueing
struct ISchedulerQueueing_t1271699701;
// System.Object
struct Il2CppObject;
// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>
struct ObserveOn__t2668549247;
// UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>
struct ObserveOnObservable_1_t3243387575;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>
struct ObserveOn_t3253138108;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>
struct ObserveOn_t679350430;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>
struct OfType_t384837642;
// UniRx.Operators.OfTypeObservable`2<System.Object,System.Object>
struct OfTypeObservable_2_t4125111696;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Boolean>
struct U3CSubscribeU3Ec__AnonStorey5B_t4103979366;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Double>
struct U3CSubscribeU3Ec__AnonStorey5B_t132523343;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int32>
struct U3CSubscribeU3Ec__AnonStorey5B_t2445421516;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int64>
struct U3CSubscribeU3Ec__AnonStorey5B_t2445421611;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Object>
struct U3CSubscribeU3Ec__AnonStorey5B_t435113149;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Single>
struct U3CSubscribeU3Ec__AnonStorey5B_t556215750;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t2014528716;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct U3CSubscribeU3Ec__AnonStorey5B_t1546684115;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionMoveEvent`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t2514440576;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct U3CSubscribeU3Ec__AnonStorey5B_t2046595975;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionRemoveEvent`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t4135611749;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct U3CSubscribeU3Ec__AnonStorey5B_t3667767148;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionReplaceEvent`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t1439757265;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct U3CSubscribeU3Ec__AnonStorey5B_t971912664;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t4143955033;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t2920295020;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t3376933792;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Pair`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t3429104311;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.TimeInterval`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t306072271;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Timestamped`1<System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t2117191002;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`2<System.Object,System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t4262235844;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t1248905831;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t625237010;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Unit>
struct U3CSubscribeU3Ec__AnonStorey5B_t2156292767;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Color>
struct U3CSubscribeU3Ec__AnonStorey5B_t1186182489;
// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Vector2>
struct U3CSubscribeU3Ec__AnonStorey5B_t3123336517;
// UniRx.Operators.OperatorObservableBase`1<System.Boolean>
struct OperatorObservableBase_1_t3570117608;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// UniRx.Operators.OperatorObservableBase`1<System.Double>
struct OperatorObservableBase_1_t3893628881;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// UniRx.Operators.OperatorObservableBase`1<System.Int32>
struct OperatorObservableBase_1_t1911559758;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// UniRx.Operators.OperatorObservableBase`1<System.Int64>
struct OperatorObservableBase_1_t1911559853;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// UniRx.Operators.OperatorObservableBase`1<System.Object>
struct OperatorObservableBase_1_t4196218687;
// UniRx.Operators.OperatorObservableBase`1<System.Single>
struct OperatorObservableBase_1_t22353992;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<System.Object>>
struct OperatorObservableBase_1_t1480666958;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObservableBase_1_t1012822357;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct OperatorObservableBase_1_t1980578818;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObservableBase_1_t1512734217;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t365620853;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct OperatorObservableBase_1_t3601749991;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObservableBase_1_t3133905390;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t1986792026;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct OperatorObservableBase_1_t905895507;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;
// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObservableBase_1_t438050906;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t3585904838;
// UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct OperatorObservableBase_1_t3610093275;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;
// UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct OperatorObservableBase_1_t2386433262;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;
// UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct OperatorObservableBase_1_t2843072034;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;
// UniRx.Operators.OperatorObservableBase`1<UniRx.Pair`1<System.Object>>
struct OperatorObservableBase_1_t2895242553;
// UniRx.IObserver`1<UniRx.Pair`1<System.Object>>
struct IObserver_1_t1748129189;
// UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>
struct OperatorObservableBase_1_t4067177809;
// UniRx.IObserver`1<UniRx.TimeInterval`1<System.Object>>
struct IObserver_1_t2920064445;
// UniRx.Operators.OperatorObservableBase`1<UniRx.Timestamped`1<System.Object>>
struct OperatorObservableBase_1_t1583329244;
// UniRx.IObserver`1<UniRx.Timestamped`1<System.Object>>
struct IObserver_1_t436215880;
// UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`2<System.Object,System.Object>>
struct OperatorObservableBase_1_t3728374086;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;
// UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct OperatorObservableBase_1_t715044073;
// UniRx.IObserver`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct IObserver_1_t3862898005;
// UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct OperatorObservableBase_1_t91375252;
// UniRx.IObserver`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct IObserver_1_t3239229184;
// UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>
struct OperatorObservableBase_1_t1622431009;
// UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>
struct OperatorObservableBase_1_t652320731;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>
struct OperatorObservableBase_1_t2589474759;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>
struct OperatorObserverBase_2_t60103313;
// UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>
struct OperatorObserverBase_2_t1921935565;
// UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>
struct OperatorObserverBase_2_t701568569;
// UniRx.Operators.OperatorObserverBase`2<System.Int32,UnityEngine.Color>
struct OperatorObserverBase_2_t3737296838;
// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Boolean>
struct OperatorObserverBase_2_t1303321368;
// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int32>
struct OperatorObserverBase_2_t3939730814;
// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>
struct OperatorObserverBase_2_t3939730909;
// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>
struct OperatorObserverBase_2_t1929422447;
// UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>
struct OperatorObserverBase_2_t322678519;
// UniRx.Operators.OperatorObserverBase`2<System.Object,System.Boolean>
struct OperatorObserverBase_2_t561667070;
// UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>
struct OperatorObserverBase_2_t1187768149;
// UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Pair`1<System.Object>>
struct OperatorObserverBase_2_t4181759311;
// UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.TimeInterval`1<System.Object>>
struct OperatorObserverBase_2_t1058727271;
// UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Timestamped`1<System.Object>>
struct OperatorObserverBase_2_t2869846002;
// UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>
struct OperatorObserverBase_2_t2908947767;
// UniRx.Operators.OperatorObserverBase`2<System.Single,System.Single>
struct OperatorObserverBase_2_t2516778257;
// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<System.Object>,UniRx.CollectionAddEvent`1<System.Object>>
struct OperatorObserverBase_2_t3545535033;
// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObserverBase_2_t2669133373;
// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<System.Object>,UniRx.CollectionMoveEvent`1<System.Object>>
struct OperatorObserverBase_2_t2793497833;
// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObserverBase_2_t1917096173;
// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<System.Object>,UniRx.CollectionRemoveEvent`1<System.Object>>
struct OperatorObserverBase_2_t3997058997;
// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObserverBase_2_t3120657337;
// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>
struct OperatorObserverBase_2_t3791266949;
// UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct OperatorObserverBase_2_t2914865289;
// UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryAddEvent`2<System.Object,System.Object>,UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct OperatorObserverBase_2_t3051653477;
// UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>,UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct OperatorObserverBase_2_t3745721;
// UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>,UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct OperatorObserverBase_2_t154015849;
// UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>
struct OperatorObserverBase_2_t311366489;
// UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,System.Int64>
struct OperatorObserverBase_2_t159537945;
// UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,System.Object>
struct OperatorObserverBase_2_t2444196779;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Never958614430.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Never958614430MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthC2199482330.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthC2199482330MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthZ3084363628.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthZ3084363628MethodDeclarations.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthZ3759589597.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_NthZ3759589597MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse4115224287.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse4115224287MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedListNode_11330911994MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen3369050920MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen3369050920.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_SingleAssignme2336378823MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse3253138108.h"
#include "System_System_Collections_Generic_LinkedListNode_11330911994.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse1541436609.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse1541436609MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obser679350430.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obser681819067.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obser681819067MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse2402998685.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse2402998685MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse1362241787.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse1362241787MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse3083421405.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse3083421405MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obser947369629.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obser947369629MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse1522207957.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2115686693MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2115686693.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse2668549247.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse2668549247MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse3243387575.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse3253138108MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "System_System_Collections_Generic_LinkedList_1_Enum511663335MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_Enum511663335.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obser679350430MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse1522207957MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_OptimizedObser2379048080.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Obse3243387575MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_OfTyp384837642.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_OfTyp384837642MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_OfTy4125111696.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_OfTy4125111696MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4103979366.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4103979366MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3570117608.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3570117608MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera132523343.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera132523343MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3893628881.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3893628881MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2445421516.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2445421516MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559758.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559758MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2445421611.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2445421611MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559853MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera435113149.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera435113149MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera556215750.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera556215750MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat22353992.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat22353992MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2014528716.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2014528716MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1480666958.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1480666958MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1546684115.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1546684115MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1012822357.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1012822357MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2514440576.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2514440576MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1980578818.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1980578818MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2046595975.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2046595975MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1512734217.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1512734217MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4135611749.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4135611749MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3601749991.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3601749991MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3667767148.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3667767148MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3133905390.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3133905390MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1439757265.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1439757265MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera905895507.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera905895507MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera971912664.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera971912664MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera438050906.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera438050906MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4143955033.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4143955033MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3610093275.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3610093275MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2920295020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2920295020MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2386433262.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2386433262MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3376933792.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3376933792MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2843072034.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2843072034MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3429104311.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3429104311MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2895242553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2895242553MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera306072271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera306072271MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4067177809.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4067177809MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2117191002.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2117191002MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1583329244.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1583329244MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4262235844.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4262235844MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3728374086.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3728374086MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1248905831.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1248905831MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera715044073.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera715044073MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera625237010.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera625237010MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat91375252.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat91375252MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2156292767.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2156292767MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1186182489.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1186182489MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera652320731.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera652320731MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3123336517.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3123336517MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2589474759.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2589474759MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3915325627.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3915325627MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1921935565.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1921935565MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4238836900.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4238836900MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767777.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767777MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3737296838.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3737296838MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em997528750.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em997528750MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1303321368.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1303321368MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730814.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730814MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767872.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767872MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1929422447.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1929422447MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera322678519.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera322678519MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682778.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682778MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera561667070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera561667070MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4181759311.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4181759311MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3240450572.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3240450572MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1058727271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1058727271MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em117418532.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em117418532MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2869846002.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2869846002MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1928537263.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1928537263MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1967639028.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1967639028MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2516778257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2516778257MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em367562011.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em367562011MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3545535033.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3545535033MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1825874977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1825874977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2669133373.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2669133373MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1358030376.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1358030376MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2793497833.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2793497833MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2325786837.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2325786837MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1917096173.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1917096173MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1857942236.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1857942236MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3997058997.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3997058997MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3946958010.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3946958010MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3120657337.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3120657337MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3479113409.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3479113409MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3791266949.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3791266949MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1251103526.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1251103526MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2914865289.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2914865289MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em783258925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em783258925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3051653477.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3051653477MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3955301294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3955301294MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operato3745721.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operato3745721MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2731641281.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2731641281MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera154015849.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera154015849MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3188280053.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3188280053MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera311366489.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera311366489MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4073582105.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4073582105MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera159537945.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera159537945MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2444196779.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2444196779MethodDeclarations.h"

// System.Void UniRx.ISchedulerQueueing::ScheduleQueueing<System.Object>(UniRx.ICancelable,!!0,System.Action`1<!!0>)
extern "C"  void ISchedulerQueueing_ScheduleQueueing_TisIl2CppObject_m254789063_gshared (Il2CppObject * __this, Il2CppObject * p0, Il2CppObject * p1, Action_1_t985559125 * p2, const MethodInfo* method);
#define ISchedulerQueueing_ScheduleQueueing_TisIl2CppObject_m254789063(__this, p0, p1, p2, method) ((  void (*) (Il2CppObject *, Il2CppObject *, Il2CppObject *, Action_1_t985559125 *, const MethodInfo*))ISchedulerQueueing_ScheduleQueueing_TisIl2CppObject_m254789063_gshared)(__this, p0, p1, p2, method)
// System.Void UniRx.ISchedulerQueueing::ScheduleQueueing<System.Exception>(UniRx.ICancelable,!!0,System.Action`1<!!0>)
#define ISchedulerQueueing_ScheduleQueueing_TisException_t1967233988_m4205968163(__this, p0, p1, p2, method) ((  void (*) (Il2CppObject *, Il2CppObject *, Exception_t1967233988 *, Action_1_t2115686693 *, const MethodInfo*))ISchedulerQueueing_ScheduleQueueing_TisIl2CppObject_m254789063_gshared)(__this, p0, p1, p2, method)
// System.Void UniRx.ISchedulerQueueing::ScheduleQueueing<UniRx.Unit>(UniRx.ICancelable,!!0,System.Action`1<!!0>)
extern "C"  void ISchedulerQueueing_ScheduleQueueing_TisUnit_t2558286038_m118559967_gshared (Il2CppObject * __this, Il2CppObject * p0, Unit_t2558286038  p1, Action_1_t2706738743 * p2, const MethodInfo* method);
#define ISchedulerQueueing_ScheduleQueueing_TisUnit_t2558286038_m118559967(__this, p0, p1, p2, method) ((  void (*) (Il2CppObject *, Il2CppObject *, Unit_t2558286038 , Action_1_t2706738743 *, const MethodInfo*))ISchedulerQueueing_ScheduleQueueing_TisUnit_t2558286038_m118559967_gshared)(__this, p0, p1, p2, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisIl2CppObject_m1909184259_gshared)(__this /* static, unused */, p0, method)
// System.Boolean UniRx.OptimizedObservableExtensions::IsRequiredSubscribeOnCurrentThread<UniRx.Unit>(UniRx.IObservable`1<!!0>)
extern "C"  bool OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))OptimizedObservableExtensions_IsRequiredSubscribeOnCurrentThread_TisUnit_t2558286038_m849542303_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Threading.Interlocked::Exchange<System.Object>(!!0&,!!0)
extern "C"  Il2CppObject * Interlocked_Exchange_TisIl2CppObject_m893924159_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, const MethodInfo* method);
#define Interlocked_Exchange_TisIl2CppObject_m893924159(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, const MethodInfo*))Interlocked_Exchange_TisIl2CppObject_m893924159_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Threading.Interlocked::Exchange<System.IDisposable>(!!0&,!!0)
#define Interlocked_Exchange_TisIDisposable_t1628921374_m3169264138(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, const MethodInfo*))Interlocked_Exchange_TisIl2CppObject_m893924159_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.NeverObservable`1<System.Object>::.ctor()
extern "C"  void NeverObservable_1__ctor_m206993564_gshared (NeverObservable_1_t958614430 * __this, const MethodInfo* method)
{
	{
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.IDisposable UniRx.Operators.NeverObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t NeverObservable_1_SubscribeCore_m2729017090_MetadataUsageId;
extern "C"  Il2CppObject * NeverObservable_1_SubscribeCore_m2729017090_gshared (NeverObservable_1_t958614430 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NeverObservable_1_SubscribeCore_m2729017090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_0;
	}
}
// System.Void UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::.ctor(System.Int32,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern const uint32_t NthCombineLatestObserverBase_1__ctor_m1135319011_MetadataUsageId;
extern "C"  void NthCombineLatestObserverBase_1__ctor_m1135319011_gshared (NthCombineLatestObserverBase_1_t2199482330 * __this, int32_t ___length0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NthCombineLatestObserverBase_1__ctor_m1135319011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_2 = ___length0;
		__this->set_length_2(L_2);
		__this->set_isAllValueStarted_3((bool)0);
		int32_t L_3 = ___length0;
		__this->set_isStarted_4(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_3)));
		int32_t L_4 = ___length0;
		__this->set_isCompleted_5(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		return;
	}
}
// System.Void UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::Publish(System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t NthCombineLatestObserverBase_1_Publish_m3159882668_MetadataUsageId;
extern "C"  void NthCombineLatestObserverBase_1_Publish_m3159882668_gshared (NthCombineLatestObserverBase_1_t2199482330 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NthCombineLatestObserverBase_1_Publish_m3159882668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * V_5 = NULL;
	bool V_6 = false;
	int32_t V_7 = 0;
	Il2CppObject * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BooleanU5BU5D_t3804927312* L_0 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_4();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		bool L_2 = (bool)__this->get_isAllValueStarted_3();
		if (!L_2)
		{
			goto IL_0058;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_8));
		Il2CppObject * L_3 = V_8;
		V_0 = (Il2CppObject *)L_3;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		NullCheck((NthCombineLatestObserverBase_1_t2199482330 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(14 /* T UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::GetResult() */, (NthCombineLatestObserverBase_1_t2199482330 *)__this);
		V_0 = (Il2CppObject *)L_4;
		goto IL_0050;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002b;
		throw e;
	}

CATCH_002b:
	{ // begin catch(System.Exception)
		{
			V_1 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_002c:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_5 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_6 = V_1;
			NullCheck((Il2CppObject*)L_5);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_5, (Exception_t1967233988 *)L_6);
			IL2CPP_LEAVE(0x46, FINALLY_003f);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_003f;
		}

FINALLY_003f:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(63)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(63)
		{
			IL2CPP_JUMP_TBL(0x46, IL_0046)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0046:
		{
			goto IL_0143;
		}

IL_004b:
		{
			; // IL_004b: leave IL_0050
		}
	} // end catch (depth: 1)

IL_0050:
	{
		Il2CppObject * L_7 = V_0;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::OnNext(TSource) */, (OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject *)L_7);
		return;
	}

IL_0058:
	{
		V_2 = (bool)1;
		V_3 = (int32_t)0;
		goto IL_0079;
	}

IL_0061:
	{
		BooleanU5BU5D_t3804927312* L_8 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_4();
		int32_t L_9 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		if (((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))))
		{
			goto IL_0075;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0085;
	}

IL_0075:
	{
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_12 = V_3;
		int32_t L_13 = (int32_t)__this->get_length_2();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0061;
		}
	}

IL_0085:
	{
		bool L_14 = V_2;
		__this->set_isAllValueStarted_3(L_14);
		bool L_15 = (bool)__this->get_isAllValueStarted_3();
		if (!L_15)
		{
			goto IL_00e0;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_9));
		Il2CppObject * L_16 = V_9;
		V_4 = (Il2CppObject *)L_16;
	}

IL_00a3:
	try
	{ // begin try (depth: 1)
		NullCheck((NthCombineLatestObserverBase_1_t2199482330 *)__this);
		Il2CppObject * L_17 = VirtFuncInvoker0< Il2CppObject * >::Invoke(14 /* T UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::GetResult() */, (NthCombineLatestObserverBase_1_t2199482330 *)__this);
		V_4 = (Il2CppObject *)L_17;
		goto IL_00d7;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00b0;
		throw e;
	}

CATCH_00b0:
	{ // begin catch(System.Exception)
		{
			V_5 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_00b2:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_18 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_19 = V_5;
			NullCheck((Il2CppObject*)L_18);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_18, (Exception_t1967233988 *)L_19);
			IL2CPP_LEAVE(0xCD, FINALLY_00c6);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00c6;
		}

FINALLY_00c6:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(198)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(198)
		{
			IL2CPP_JUMP_TBL(0xCD, IL_00cd)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00cd:
		{
			goto IL_0143;
		}

IL_00d2:
		{
			; // IL_00d2: leave IL_00d7
		}
	} // end catch (depth: 1)

IL_00d7:
	{
		Il2CppObject * L_20 = V_4;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::OnNext(TSource) */, (OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject *)L_20);
		return;
	}

IL_00e0:
	{
		V_6 = (bool)1;
		V_7 = (int32_t)0;
		goto IL_0114;
	}

IL_00eb:
	{
		int32_t L_21 = V_7;
		int32_t L_22 = ___index0;
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_00f8;
		}
	}
	{
		goto IL_010e;
	}

IL_00f8:
	{
		BooleanU5BU5D_t3804927312* L_23 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_5();
		int32_t L_24 = V_7;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		if (((L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25))))
		{
			goto IL_010e;
		}
	}
	{
		V_6 = (bool)0;
		goto IL_0121;
	}

IL_010e:
	{
		int32_t L_26 = V_7;
		V_7 = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_0114:
	{
		int32_t L_27 = V_7;
		int32_t L_28 = (int32_t)__this->get_length_2();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_00eb;
		}
	}

IL_0121:
	{
		bool L_29 = V_6;
		if (!L_29)
		{
			goto IL_0142;
		}
	}

IL_0128:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_30 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_30);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_30);
		IL2CPP_LEAVE(0x141, FINALLY_013a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_013a;
	}

FINALLY_013a:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(314)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(314)
	{
		IL2CPP_JUMP_TBL(0x141, IL_0141)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0141:
	{
		return;
	}

IL_0142:
	{
		return;
	}

IL_0143:
	{
		return;
	}
}
// System.Void UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::Done(System.Int32)
extern "C"  void NthCombineLatestObserverBase_1_Done_m1063512233_gshared (NthCombineLatestObserverBase_1_t2199482330 * __this, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BooleanU5BU5D_t3804927312* L_0 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_5();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		V_0 = (bool)1;
		V_1 = (int32_t)0;
		goto IL_002a;
	}

IL_0012:
	{
		BooleanU5BU5D_t3804927312* L_2 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_5();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		if (((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))))
		{
			goto IL_0026;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0036;
	}

IL_0026:
	{
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = (int32_t)__this->get_length_2();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0012;
		}
	}

IL_0036:
	{
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0055;
		}
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.NthCombineLatestObserverBase`1<System.Object>::Fail(System.Exception)
extern "C"  void NthCombineLatestObserverBase_1_Fail_m4048805028_gshared (NthCombineLatestObserverBase_1_t2199482330 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::.ctor(System.Int32,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern const uint32_t NthZipLatestObserverBase_1__ctor_m1799730757_MetadataUsageId;
extern "C"  void NthZipLatestObserverBase_1__ctor_m1799730757_gshared (NthZipLatestObserverBase_1_t3084363628 * __this, int32_t ___length0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NthZipLatestObserverBase_1__ctor_m1799730757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer1;
		Il2CppObject * L_1 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_2 = ___length0;
		__this->set_length_2(L_2);
		int32_t L_3 = ___length0;
		__this->set_isStarted_3(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_3)));
		int32_t L_4 = ___length0;
		__this->set_isCompleted_4(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		return;
	}
}
// System.Void UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::Publish(System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t NthZipLatestObserverBase_1_Publish_m2577470350_MetadataUsageId;
extern "C"  void NthZipLatestObserverBase_1_Publish_m2577470350_gshared (NthZipLatestObserverBase_1_t3084363628 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NthZipLatestObserverBase_1_Publish_m2577470350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * V_4 = NULL;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BooleanU5BU5D_t3804927312* L_0 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_3();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		V_0 = (bool)0;
		V_1 = (bool)1;
		V_2 = (int32_t)0;
		goto IL_0047;
	}

IL_0014:
	{
		BooleanU5BU5D_t3804927312* L_2 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_3();
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		if (((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))))
		{
			goto IL_0028;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0053;
	}

IL_0028:
	{
		int32_t L_5 = V_2;
		int32_t L_6 = ___index0;
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0043;
	}

IL_0034:
	{
		BooleanU5BU5D_t3804927312* L_7 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_4();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		if (!((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))))
		{
			goto IL_0043;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0043:
	{
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_11 = V_2;
		int32_t L_12 = (int32_t)__this->get_length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0014;
		}
	}

IL_0053:
	{
		bool L_13 = V_1;
		if (!L_13)
		{
			goto IL_00d1;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_6));
		Il2CppObject * L_14 = V_6;
		V_3 = (Il2CppObject *)L_14;
	}

IL_0064:
	try
	{ // begin try (depth: 1)
		NullCheck((NthZipLatestObserverBase_1_t3084363628 *)__this);
		Il2CppObject * L_15 = VirtFuncInvoker0< Il2CppObject * >::Invoke(14 /* T UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::GetResult() */, (NthZipLatestObserverBase_1_t3084363628 *)__this);
		V_3 = (Il2CppObject *)L_15;
		goto IL_0097;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0070;
		throw e;
	}

CATCH_0070:
	{ // begin catch(System.Exception)
		{
			V_4 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_0072:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_16 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_17 = V_4;
			NullCheck((Il2CppObject*)L_16);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_16, (Exception_t1967233988 *)L_17);
			IL2CPP_LEAVE(0x8D, FINALLY_0086);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0086;
		}

FINALLY_0086:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(134)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(134)
		{
			IL2CPP_JUMP_TBL(0x8D, IL_008d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_008d:
		{
			goto IL_012f;
		}

IL_0092:
		{
			; // IL_0092: leave IL_0097
		}
	} // end catch (depth: 1)

IL_0097:
	{
		Il2CppObject * L_18 = V_3;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::OnNext(TSource) */, (OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject *)L_18);
		bool L_19 = V_0;
		if (!L_19)
		{
			goto IL_00be;
		}
	}

IL_00a4:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_20 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_20);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_20);
		IL2CPP_LEAVE(0xBD, FINALLY_00b6);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00b6;
	}

FINALLY_00b6:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(182)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(182)
	{
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00bd:
	{
		return;
	}

IL_00be:
	{
		BooleanU5BU5D_t3804927312* L_21 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_3();
		int32_t L_22 = (int32_t)__this->get_length_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_21, (int32_t)0, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}

IL_00d1:
	{
		V_5 = (int32_t)0;
		goto IL_0122;
	}

IL_00d9:
	{
		int32_t L_23 = V_5;
		int32_t L_24 = ___index0;
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_00e6;
		}
	}
	{
		goto IL_011c;
	}

IL_00e6:
	{
		BooleanU5BU5D_t3804927312* L_25 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_4();
		int32_t L_26 = V_5;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		if (!((L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27))))
		{
			goto IL_011c;
		}
	}
	{
		BooleanU5BU5D_t3804927312* L_28 = (BooleanU5BU5D_t3804927312*)__this->get_isStarted_3();
		int32_t L_29 = V_5;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		if (((L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30))))
		{
			goto IL_011c;
		}
	}

IL_0102:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_31 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_31);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_31);
		IL2CPP_LEAVE(0x11B, FINALLY_0114);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0114;
	}

FINALLY_0114:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(276)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(276)
	{
		IL2CPP_JUMP_TBL(0x11B, IL_011b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_011b:
	{
		return;
	}

IL_011c:
	{
		int32_t L_32 = V_5;
		V_5 = (int32_t)((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0122:
	{
		int32_t L_33 = V_5;
		int32_t L_34 = (int32_t)__this->get_length_2();
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_00d9;
		}
	}

IL_012f:
	{
		return;
	}
}
// System.Void UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::Done(System.Int32)
extern "C"  void NthZipLatestObserverBase_1_Done_m2636819463_gshared (NthZipLatestObserverBase_1_t3084363628 * __this, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BooleanU5BU5D_t3804927312* L_0 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_4();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		V_0 = (bool)1;
		V_1 = (int32_t)0;
		goto IL_002a;
	}

IL_0012:
	{
		BooleanU5BU5D_t3804927312* L_2 = (BooleanU5BU5D_t3804927312*)__this->get_isCompleted_4();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		if (((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))))
		{
			goto IL_0026;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0036;
	}

IL_0026:
	{
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = (int32_t)__this->get_length_2();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0012;
		}
	}

IL_0036:
	{
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0055;
		}
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.NthZipLatestObserverBase`1<System.Object>::Fail(System.Exception)
extern "C"  void NthZipLatestObserverBase_1_Fail_m3173892354_gshared (NthZipLatestObserverBase_1_t3084363628 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void NthZipObserverBase_1__ctor_m216454881_gshared (NthZipObserverBase_1_t3759589597 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::SetQueue(System.Collections.ICollection[])
extern Il2CppClass* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern const uint32_t NthZipObserverBase_1_SetQueue_m1622220249_MetadataUsageId;
extern "C"  void NthZipObserverBase_1_SetQueue_m1622220249_gshared (NthZipObserverBase_1_t3759589597 * __this, ICollectionU5BU5D_t1434690852* ___queues0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NthZipObserverBase_1_SetQueue_m1622220249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ICollectionU5BU5D_t1434690852* L_0 = ___queues0;
		__this->set_queues_2(L_0);
		ICollectionU5BU5D_t1434690852* L_1 = ___queues0;
		NullCheck(L_1);
		__this->set_length_4((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))));
		int32_t L_2 = (int32_t)__this->get_length_4();
		__this->set_isDone_3(((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)L_2)));
		return;
	}
}
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::Dequeue(System.Int32)
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const uint32_t NthZipObserverBase_1_Dequeue_m89794376_MetadataUsageId;
extern "C"  void NthZipObserverBase_1_Dequeue_m89794376_gshared (NthZipObserverBase_1_t3759589597 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NthZipObserverBase_1_Dequeue_m89794376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		V_1 = (int32_t)0;
		goto IL_0026;
	}

IL_0009:
	{
		ICollectionU5BU5D_t1434690852* L_0 = (ICollectionU5BU5D_t1434690852*)__this->get_queues_2();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((Il2CppObject *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2))));
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2))));
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0032;
	}

IL_0022:
	{
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0009;
		}
	}

IL_0032:
	{
		bool L_7 = V_0;
		if (L_7)
		{
			goto IL_0092;
		}
	}
	{
		V_2 = (bool)1;
		V_3 = (int32_t)0;
		goto IL_0065;
	}

IL_0041:
	{
		int32_t L_8 = V_3;
		int32_t L_9 = ___index0;
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_004d;
		}
	}
	{
		goto IL_0061;
	}

IL_004d:
	{
		BooleanU5BU5D_t3804927312* L_10 = (BooleanU5BU5D_t3804927312*)__this->get_isDone_3();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		if (((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12))))
		{
			goto IL_0061;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0071;
	}

IL_0061:
	{
		int32_t L_13 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0065:
	{
		int32_t L_14 = V_3;
		int32_t L_15 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0041;
		}
	}

IL_0071:
	{
		bool L_16 = V_2;
		if (!L_16)
		{
			goto IL_0091;
		}
	}

IL_0077:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_17 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_17);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_17);
		IL2CPP_LEAVE(0x90, FINALLY_0089);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0089;
	}

FINALLY_0089:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(137)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(137)
	{
		IL2CPP_JUMP_TBL(0x90, IL_0090)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0090:
	{
		return;
	}

IL_0091:
	{
		return;
	}

IL_0092:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_6));
		Il2CppObject * L_18 = V_6;
		V_4 = (Il2CppObject *)L_18;
	}

IL_009e:
	try
	{ // begin try (depth: 1)
		NullCheck((NthZipObserverBase_1_t3759589597 *)__this);
		Il2CppObject * L_19 = VirtFuncInvoker0< Il2CppObject * >::Invoke(14 /* T UniRx.Operators.NthZipObserverBase`1<System.Object>::GetResult() */, (NthZipObserverBase_1_t3759589597 *)__this);
		V_4 = (Il2CppObject *)L_19;
		goto IL_00d2;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ab;
		throw e;
	}

CATCH_00ab:
	{ // begin catch(System.Exception)
		{
			V_5 = (Exception_t1967233988 *)((Exception_t1967233988 *)__exception_local);
		}

IL_00ad:
		try
		{ // begin try (depth: 2)
			Il2CppObject* L_20 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t1967233988 * L_21 = V_5;
			NullCheck((Il2CppObject*)L_20);
			InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_20, (Exception_t1967233988 *)L_21);
			IL2CPP_LEAVE(0xC8, FINALLY_00c1);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00c1;
		}

FINALLY_00c1:
		{ // begin finally (depth: 2)
			NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
			VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
			IL2CPP_END_FINALLY(193)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(193)
		{
			IL2CPP_JUMP_TBL(0xC8, IL_00c8)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00c8:
		{
			goto IL_00da;
		}

IL_00cd:
		{
			; // IL_00cd: leave IL_00d2
		}
	} // end catch (depth: 1)

IL_00d2:
	{
		Il2CppObject * L_22 = V_4;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::OnNext(TSource) */, (OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject *)L_22);
	}

IL_00da:
	{
		return;
	}
}
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::Done(System.Int32)
extern "C"  void NthZipObserverBase_1_Done_m2951121230_gshared (NthZipObserverBase_1_t3759589597 * __this, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BooleanU5BU5D_t3804927312* L_0 = (BooleanU5BU5D_t3804927312*)__this->get_isDone_3();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		V_0 = (bool)1;
		V_1 = (int32_t)0;
		goto IL_002a;
	}

IL_0012:
	{
		BooleanU5BU5D_t3804927312* L_2 = (BooleanU5BU5D_t3804927312*)__this->get_isDone_3();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		if (((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))))
		{
			goto IL_0026;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0036;
	}

IL_0026:
	{
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0012;
		}
	}

IL_0036:
	{
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0055;
		}
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_9 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_9);
		IL2CPP_LEAVE(0x55, FINALLY_004e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::Fail(System.Exception)
extern "C"  void NthZipObserverBase_1_Fail_m681288393_gshared (NthZipObserverBase_1_t3759589597 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<System.Object>::.ctor()
extern "C"  void U3COnCompletedU3Ec__AnonStorey63__ctor_m170323964_gshared (U3COnCompletedU3Ec__AnonStorey63_t4115224287 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<System.Object>::<>m__80()
extern const MethodInfo* LinkedListNode_1_get_List_m4040538594_MethodInfo_var;
extern const MethodInfo* LinkedList_1_Remove_m2973981888_MethodInfo_var;
extern const uint32_t U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m2780820669_MetadataUsageId;
extern "C"  void U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m2780820669_gshared (U3COnCompletedU3Ec__AnonStorey63_t4115224287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m2780820669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_self_0();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_0);
		ObserveOn_t3253138108 * L_1 = (ObserveOn_t3253138108 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_1);
		LinkedList_1_t3369050920 * L_2 = (LinkedList_1_t3369050920 *)L_1->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_2;
		LinkedList_1_t3369050920 * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			LinkedListNode_1_t1330911994 * L_4 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_4);
			LinkedList_1_t3369050920 * L_5 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_4, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			if (!L_5)
			{
				goto IL_0043;
			}
		}

IL_002d:
		{
			LinkedListNode_1_t1330911994 * L_6 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_6);
			LinkedList_1_t3369050920 * L_7 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_6, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			LinkedListNode_1_t1330911994 * L_8 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedList_1_t3369050920 *)L_7);
			LinkedList_1_Remove_m2973981888((LinkedList_1_t3369050920 *)L_7, (LinkedListNode_1_t1330911994 *)L_8, /*hidden argument*/LinkedList_1_Remove_m2973981888_MethodInfo_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		ObserveOn_t3253138108 * L_10 = (ObserveOn_t3253138108 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11);
		IL2CPP_LEAVE(0x72, FINALLY_0066);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		ObserveOn_t3253138108 * L_12 = (ObserveOn_t3253138108 *)__this->get_U3CU3Ef__this_2();
		NullCheck((OperatorObserverBase_2_t1187768149 *)L_12);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_12);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x72, IL_0072)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0072:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<UniRx.Unit>::.ctor()
extern "C"  void U3COnCompletedU3Ec__AnonStorey63__ctor_m3742015336_gshared (U3COnCompletedU3Ec__AnonStorey63_t1541436609 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<UniRx.Unit>::<>m__80()
extern const MethodInfo* LinkedListNode_1_get_List_m4040538594_MethodInfo_var;
extern const MethodInfo* LinkedList_1_Remove_m2973981888_MethodInfo_var;
extern const uint32_t U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m3497359657_MetadataUsageId;
extern "C"  void U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m3497359657_gshared (U3COnCompletedU3Ec__AnonStorey63_t1541436609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m3497359657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_self_0();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_0);
		ObserveOn_t679350430 * L_1 = (ObserveOn_t679350430 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_1);
		LinkedList_1_t3369050920 * L_2 = (LinkedList_1_t3369050920 *)L_1->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_2;
		LinkedList_1_t3369050920 * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			LinkedListNode_1_t1330911994 * L_4 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_4);
			LinkedList_1_t3369050920 * L_5 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_4, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			if (!L_5)
			{
				goto IL_0043;
			}
		}

IL_002d:
		{
			LinkedListNode_1_t1330911994 * L_6 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_6);
			LinkedList_1_t3369050920 * L_7 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_6, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			LinkedListNode_1_t1330911994 * L_8 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedList_1_t3369050920 *)L_7);
			LinkedList_1_Remove_m2973981888((LinkedList_1_t3369050920 *)L_7, (LinkedListNode_1_t1330911994 *)L_8, /*hidden argument*/LinkedList_1_Remove_m2973981888_MethodInfo_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		ObserveOn_t679350430 * L_10 = (ObserveOn_t679350430 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11);
		IL2CPP_LEAVE(0x72, FINALLY_0066);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		ObserveOn_t679350430 * L_12 = (ObserveOn_t679350430 *)__this->get_U3CU3Ef__this_2();
		NullCheck((OperatorObserverBase_2_t4165376397 *)L_12);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)L_12);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x72, IL_0072)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0072:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<System.Object>::.ctor()
extern "C"  void U3COnErrorU3Ec__AnonStorey62__ctor_m2294652856_gshared (U3COnErrorU3Ec__AnonStorey62_t681819067 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<System.Object>::<>m__7F()
extern const MethodInfo* LinkedListNode_1_get_List_m4040538594_MethodInfo_var;
extern const MethodInfo* LinkedList_1_Remove_m2973981888_MethodInfo_var;
extern const uint32_t U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m4151411632_MetadataUsageId;
extern "C"  void U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m4151411632_gshared (U3COnErrorU3Ec__AnonStorey62_t681819067 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m4151411632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_self_0();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_0);
		ObserveOn_t3253138108 * L_1 = (ObserveOn_t3253138108 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_1);
		LinkedList_1_t3369050920 * L_2 = (LinkedList_1_t3369050920 *)L_1->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_2;
		LinkedList_1_t3369050920 * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			LinkedListNode_1_t1330911994 * L_4 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_4);
			LinkedList_1_t3369050920 * L_5 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_4, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			if (!L_5)
			{
				goto IL_0043;
			}
		}

IL_002d:
		{
			LinkedListNode_1_t1330911994 * L_6 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_6);
			LinkedList_1_t3369050920 * L_7 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_6, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			LinkedListNode_1_t1330911994 * L_8 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedList_1_t3369050920 *)L_7);
			LinkedList_1_Remove_m2973981888((LinkedList_1_t3369050920 *)L_7, (LinkedListNode_1_t1330911994 *)L_8, /*hidden argument*/LinkedList_1_Remove_m2973981888_MethodInfo_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		ObserveOn_t3253138108 * L_10 = (ObserveOn_t3253138108 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_12 = (Exception_t1967233988 *)__this->get_error_2();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11, (Exception_t1967233988 *)L_12);
		IL2CPP_LEAVE(0x78, FINALLY_006c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		ObserveOn_t3253138108 * L_13 = (ObserveOn_t3253138108 *)__this->get_U3CU3Ef__this_3();
		NullCheck((OperatorObserverBase_2_t1187768149 *)L_13);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)L_13);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x78, IL_0078)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0078:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<UniRx.Unit>::.ctor()
extern "C"  void U3COnErrorU3Ec__AnonStorey62__ctor_m4253313324_gshared (U3COnErrorU3Ec__AnonStorey62_t2402998685 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<UniRx.Unit>::<>m__7F()
extern const MethodInfo* LinkedListNode_1_get_List_m4040538594_MethodInfo_var;
extern const MethodInfo* LinkedList_1_Remove_m2973981888_MethodInfo_var;
extern const uint32_t U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m933478436_MetadataUsageId;
extern "C"  void U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m933478436_gshared (U3COnErrorU3Ec__AnonStorey62_t2402998685 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m933478436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_self_0();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_0);
		ObserveOn_t679350430 * L_1 = (ObserveOn_t679350430 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_1);
		LinkedList_1_t3369050920 * L_2 = (LinkedList_1_t3369050920 *)L_1->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_2;
		LinkedList_1_t3369050920 * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			LinkedListNode_1_t1330911994 * L_4 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_4);
			LinkedList_1_t3369050920 * L_5 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_4, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			if (!L_5)
			{
				goto IL_0043;
			}
		}

IL_002d:
		{
			LinkedListNode_1_t1330911994 * L_6 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_6);
			LinkedList_1_t3369050920 * L_7 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_6, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			LinkedListNode_1_t1330911994 * L_8 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedList_1_t3369050920 *)L_7);
			LinkedList_1_Remove_m2973981888((LinkedList_1_t3369050920 *)L_7, (LinkedListNode_1_t1330911994 *)L_8, /*hidden argument*/LinkedList_1_Remove_m2973981888_MethodInfo_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		ObserveOn_t679350430 * L_10 = (ObserveOn_t679350430 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_12 = (Exception_t1967233988 *)__this->get_error_2();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11, (Exception_t1967233988 *)L_12);
		IL2CPP_LEAVE(0x78, FINALLY_006c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		ObserveOn_t679350430 * L_13 = (ObserveOn_t679350430 *)__this->get_U3CU3Ef__this_3();
		NullCheck((OperatorObserverBase_2_t4165376397 *)L_13);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)L_13);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x78, IL_0078)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0078:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<System.Object>::.ctor()
extern "C"  void U3COnNextU3Ec__AnonStorey61__ctor_m509757722_gshared (U3COnNextU3Ec__AnonStorey61_t1362241787 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<System.Object>::<>m__7E()
extern const MethodInfo* LinkedListNode_1_get_List_m4040538594_MethodInfo_var;
extern const MethodInfo* LinkedList_1_Remove_m2973981888_MethodInfo_var;
extern const uint32_t U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m2559138001_MetadataUsageId;
extern "C"  void U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m2559138001_gshared (U3COnNextU3Ec__AnonStorey61_t1362241787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m2559138001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_self_0();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_0);
		ObserveOn_t3253138108 * L_1 = (ObserveOn_t3253138108 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_1);
		LinkedList_1_t3369050920 * L_2 = (LinkedList_1_t3369050920 *)L_1->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_2;
		LinkedList_1_t3369050920 * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			LinkedListNode_1_t1330911994 * L_4 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_4);
			LinkedList_1_t3369050920 * L_5 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_4, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			if (!L_5)
			{
				goto IL_0043;
			}
		}

IL_002d:
		{
			LinkedListNode_1_t1330911994 * L_6 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_6);
			LinkedList_1_t3369050920 * L_7 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_6, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			LinkedListNode_1_t1330911994 * L_8 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedList_1_t3369050920 *)L_7);
			LinkedList_1_Remove_m2973981888((LinkedList_1_t3369050920 *)L_7, (LinkedListNode_1_t1330911994 *)L_8, /*hidden argument*/LinkedList_1_Remove_m2973981888_MethodInfo_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	{
		ObserveOn_t3253138108 * L_10 = (ObserveOn_t3253138108 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_value_2();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<UniRx.Unit>::.ctor()
extern "C"  void U3COnNextU3Ec__AnonStorey61__ctor_m1853688586_gshared (U3COnNextU3Ec__AnonStorey61_t3083421405 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<UniRx.Unit>::<>m__7E()
extern const MethodInfo* LinkedListNode_1_get_List_m4040538594_MethodInfo_var;
extern const MethodInfo* LinkedList_1_Remove_m2973981888_MethodInfo_var;
extern const uint32_t U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m1291542209_MetadataUsageId;
extern "C"  void U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m1291542209_gshared (U3COnNextU3Ec__AnonStorey61_t3083421405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m1291542209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_self_0();
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t2336378823 *)L_0);
		ObserveOn_t679350430 * L_1 = (ObserveOn_t679350430 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_1);
		LinkedList_1_t3369050920 * L_2 = (LinkedList_1_t3369050920 *)L_1->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_2;
		LinkedList_1_t3369050920 * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			LinkedListNode_1_t1330911994 * L_4 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_4);
			LinkedList_1_t3369050920 * L_5 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_4, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			if (!L_5)
			{
				goto IL_0043;
			}
		}

IL_002d:
		{
			LinkedListNode_1_t1330911994 * L_6 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedListNode_1_t1330911994 *)L_6);
			LinkedList_1_t3369050920 * L_7 = LinkedListNode_1_get_List_m4040538594((LinkedListNode_1_t1330911994 *)L_6, /*hidden argument*/LinkedListNode_1_get_List_m4040538594_MethodInfo_var);
			LinkedListNode_1_t1330911994 * L_8 = (LinkedListNode_1_t1330911994 *)__this->get_node_1();
			NullCheck((LinkedList_1_t3369050920 *)L_7);
			LinkedList_1_Remove_m2973981888((LinkedList_1_t3369050920 *)L_7, (LinkedListNode_1_t1330911994 *)L_8, /*hidden argument*/LinkedList_1_Remove_m2973981888_MethodInfo_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_9 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	{
		ObserveOn_t679350430 * L_10 = (ObserveOn_t679350430 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		Il2CppObject* L_11 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)L_10)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_12 = (Unit_t2558286038 )__this->get_value_2();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11, (Unit_t2558286038 )L_12);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::.ctor(UniRx.Operators.ObserveOnObservable`1<T>,UniRx.ISchedulerQueueing,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern const uint32_t ObserveOn___ctor_m1548936999_MetadataUsageId;
extern "C"  void ObserveOn___ctor_m1548936999_gshared (ObserveOn__t947369629 * __this, ObserveOnObservable_1_t1522207957 * ___parent0, Il2CppObject * ___scheduler1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn___ctor_m1548936999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer2;
		Il2CppObject * L_1 = ___cancel3;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObserveOnObservable_1_t1522207957 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		Il2CppObject * L_3 = ___scheduler1;
		__this->set_scheduler_3(L_3);
		BooleanDisposable_t3065601722 * L_4 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_4, /*hidden argument*/NULL);
		__this->set_isDisposed_4(L_4);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_6, (Il2CppObject *)__this, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_onNext_5(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::Run()
extern "C"  Il2CppObject * ObserveOn__Run_m3768363898_gshared (ObserveOn__t947369629 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		ObserveOnObservable_1_t1522207957 * L_0 = (ObserveOnObservable_1_t1522207957 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		BooleanDisposable_t3065601722 * L_4 = (BooleanDisposable_t3065601722 *)__this->get_isDisposed_4();
		Il2CppObject * L_5 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnNext_(T)
extern "C"  void ObserveOn__OnNext__m2423936093_gshared (ObserveOn__t947369629 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnError_(System.Exception)
extern "C"  void ObserveOn__OnError__m1521757812_gshared (ObserveOn__t947369629 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnCompleted_(UniRx.Unit)
extern "C"  void ObserveOn__OnCompleted__m1706344645_gshared (ObserveOn__t947369629 * __this, Unit_t2558286038  ____0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnNext(T)
extern "C"  void ObserveOn__OnNext_m2710539262_gshared (ObserveOn__t947369629 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_3();
		BooleanDisposable_t3065601722 * L_1 = (BooleanDisposable_t3065601722 *)__this->get_isDisposed_4();
		Il2CppObject * L_2 = ___value0;
		Action_1_t985559125 * L_3 = (Action_1_t985559125 *)__this->get_onNext_5();
		NullCheck((Il2CppObject *)L_0);
		GenericInterfaceActionInvoker3< Il2CppObject *, Il2CppObject *, Action_1_t985559125 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Action_1_t985559125 *)L_3);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnError(System.Exception)
extern Il2CppClass* Action_1_t2115686693_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m15890083_MethodInfo_var;
extern const MethodInfo* ISchedulerQueueing_ScheduleQueueing_TisException_t1967233988_m4205968163_MethodInfo_var;
extern const uint32_t ObserveOn__OnError_m3538414349_MetadataUsageId;
extern "C"  void ObserveOn__OnError_m3538414349_gshared (ObserveOn__t947369629 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn__OnError_m3538414349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_3();
		BooleanDisposable_t3065601722 * L_1 = (BooleanDisposable_t3065601722 *)__this->get_isDisposed_4();
		Exception_t1967233988 * L_2 = ___error0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Action_1_t2115686693 * L_4 = (Action_1_t2115686693 *)il2cpp_codegen_object_new(Action_1_t2115686693_il2cpp_TypeInfo_var);
		Action_1__ctor_m15890083(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/Action_1__ctor_m15890083_MethodInfo_var);
		NullCheck((Il2CppObject *)L_0);
		GenericInterfaceActionInvoker3< Il2CppObject *, Exception_t1967233988 *, Action_1_t2115686693 * >::Invoke(ISchedulerQueueing_ScheduleQueueing_TisException_t1967233988_m4205968163_MethodInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (Exception_t1967233988 *)L_2, (Action_1_t2115686693 *)L_4);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<System.Object>::OnCompleted()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2706738743_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1024136343_MethodInfo_var;
extern const MethodInfo* ISchedulerQueueing_ScheduleQueueing_TisUnit_t2558286038_m118559967_MethodInfo_var;
extern const uint32_t ObserveOn__OnCompleted_m1272777184_MetadataUsageId;
extern "C"  void ObserveOn__OnCompleted_m1272777184_gshared (ObserveOn__t947369629 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn__OnCompleted_m1272777184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_3();
		BooleanDisposable_t3065601722 * L_1 = (BooleanDisposable_t3065601722 *)__this->get_isDisposed_4();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_1_t2706738743 * L_4 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(Action_1_t2706738743_il2cpp_TypeInfo_var);
		Action_1__ctor_m1024136343(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/Action_1__ctor_m1024136343_MethodInfo_var);
		NullCheck((Il2CppObject *)L_0);
		GenericInterfaceActionInvoker3< Il2CppObject *, Unit_t2558286038 , Action_1_t2706738743 * >::Invoke(ISchedulerQueueing_ScheduleQueueing_TisUnit_t2558286038_m118559967_MethodInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (Unit_t2558286038 )L_2, (Action_1_t2706738743 *)L_4);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::.ctor(UniRx.Operators.ObserveOnObservable`1<T>,UniRx.ISchedulerQueueing,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* BooleanDisposable_t3065601722_il2cpp_TypeInfo_var;
extern const uint32_t ObserveOn___ctor_m172035631_MetadataUsageId;
extern "C"  void ObserveOn___ctor_m172035631_gshared (ObserveOn__t2668549247 * __this, ObserveOnObservable_1_t3243387575 * ___parent0, Il2CppObject * ___scheduler1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn___ctor_m172035631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer2;
		Il2CppObject * L_1 = ___cancel3;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObserveOnObservable_1_t3243387575 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		Il2CppObject * L_3 = ___scheduler1;
		__this->set_scheduler_3(L_3);
		BooleanDisposable_t3065601722 * L_4 = (BooleanDisposable_t3065601722 *)il2cpp_codegen_object_new(BooleanDisposable_t3065601722_il2cpp_TypeInfo_var);
		BooleanDisposable__ctor_m2534881639(L_4, /*hidden argument*/NULL);
		__this->set_isDisposed_4(L_4);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t2706738743 * L_6 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t2706738743 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_6, (Il2CppObject *)__this, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_onNext_5(L_6);
		return;
	}
}
// System.IDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::Run()
extern "C"  Il2CppObject * ObserveOn__Run_m3473698140_gshared (ObserveOn__t2668549247 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		ObserveOnObservable_1_t3243387575 * L_0 = (ObserveOnObservable_1_t3243387575 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		BooleanDisposable_t3065601722 * L_4 = (BooleanDisposable_t3065601722 *)__this->get_isDisposed_4();
		Il2CppObject * L_5 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnNext_(T)
extern "C"  void ObserveOn__OnNext__m181846373_gshared (ObserveOn__t2668549247 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Unit_t2558286038  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Unit_t2558286038  >::Invoke(2 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0, (Unit_t2558286038 )L_1);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnError_(System.Exception)
extern "C"  void ObserveOn__OnError__m1977977724_gshared (ObserveOn__t2668549247 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnCompleted_(UniRx.Unit)
extern "C"  void ObserveOn__OnCompleted__m3391733709_gshared (ObserveOn__t2668549247 * __this, Unit_t2558286038  ____0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t4165376397 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UniRx.Unit>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>::Dispose() */, (OperatorObserverBase_2_t4165376397 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnNext(T)
extern "C"  void ObserveOn__OnNext_m1391287798_gshared (ObserveOn__t2668549247 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_3();
		BooleanDisposable_t3065601722 * L_1 = (BooleanDisposable_t3065601722 *)__this->get_isDisposed_4();
		Unit_t2558286038  L_2 = ___value0;
		Action_1_t2706738743 * L_3 = (Action_1_t2706738743 *)__this->get_onNext_5();
		NullCheck((Il2CppObject *)L_0);
		GenericInterfaceActionInvoker3< Il2CppObject *, Unit_t2558286038 , Action_1_t2706738743 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Il2CppObject *)L_0, (Il2CppObject *)L_1, (Unit_t2558286038 )L_2, (Action_1_t2706738743 *)L_3);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnError(System.Exception)
extern Il2CppClass* Action_1_t2115686693_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m15890083_MethodInfo_var;
extern const MethodInfo* ISchedulerQueueing_ScheduleQueueing_TisException_t1967233988_m4205968163_MethodInfo_var;
extern const uint32_t ObserveOn__OnError_m4245867781_MetadataUsageId;
extern "C"  void ObserveOn__OnError_m4245867781_gshared (ObserveOn__t2668549247 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn__OnError_m4245867781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_3();
		BooleanDisposable_t3065601722 * L_1 = (BooleanDisposable_t3065601722 *)__this->get_isDisposed_4();
		Exception_t1967233988 * L_2 = ___error0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Action_1_t2115686693 * L_4 = (Action_1_t2115686693 *)il2cpp_codegen_object_new(Action_1_t2115686693_il2cpp_TypeInfo_var);
		Action_1__ctor_m15890083(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/Action_1__ctor_m15890083_MethodInfo_var);
		NullCheck((Il2CppObject *)L_0);
		GenericInterfaceActionInvoker3< Il2CppObject *, Exception_t1967233988 *, Action_1_t2115686693 * >::Invoke(ISchedulerQueueing_ScheduleQueueing_TisException_t1967233988_m4205968163_MethodInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (Exception_t1967233988 *)L_2, (Action_1_t2115686693 *)L_4);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn_<UniRx.Unit>::OnCompleted()
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2706738743_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1024136343_MethodInfo_var;
extern const MethodInfo* ISchedulerQueueing_ScheduleQueueing_TisUnit_t2558286038_m118559967_MethodInfo_var;
extern const uint32_t ObserveOn__OnCompleted_m2509316056_MetadataUsageId;
extern "C"  void ObserveOn__OnCompleted_m2509316056_gshared (ObserveOn__t2668549247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn__OnCompleted_m2509316056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_3();
		BooleanDisposable_t3065601722 * L_1 = (BooleanDisposable_t3065601722 *)__this->get_isDisposed_4();
		IL2CPP_RUNTIME_CLASS_INIT(Unit_t2558286038_il2cpp_TypeInfo_var);
		Unit_t2558286038  L_2 = Unit_get_Default_m3654553472(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_1_t2706738743 * L_4 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(Action_1_t2706738743_il2cpp_TypeInfo_var);
		Action_1__ctor_m1024136343(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/Action_1__ctor_m1024136343_MethodInfo_var);
		NullCheck((Il2CppObject *)L_0);
		GenericInterfaceActionInvoker3< Il2CppObject *, Unit_t2558286038 , Action_1_t2706738743 * >::Invoke(ISchedulerQueueing_ScheduleQueueing_TisUnit_t2558286038_m118559967_MethodInfo_var, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (Unit_t2558286038 )L_2, (Action_1_t2706738743 *)L_4);
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::.ctor(UniRx.Operators.ObserveOnObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* LinkedList_1_t3369050920_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m3255264475_MethodInfo_var;
extern const uint32_t ObserveOn__ctor_m3391667725_MetadataUsageId;
extern "C"  void ObserveOn__ctor_m3391667725_gshared (ObserveOn_t3253138108 * __this, ObserveOnObservable_1_t1522207957 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn__ctor_m3391667725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t3369050920 * L_0 = (LinkedList_1_t3369050920 *)il2cpp_codegen_object_new(LinkedList_1_t3369050920_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m3255264475(L_0, /*hidden argument*/LinkedList_1__ctor_m3255264475_MethodInfo_var);
		__this->set_scheduleDisposables_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObserveOnObservable_1_t1522207957 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::Run()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ObserveOn_Run_m1320885093_MetadataUsageId;
extern "C"  Il2CppObject * ObserveOn_Run_m1320885093_gshared (ObserveOn_t3253138108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_Run_m1320885093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		__this->set_isDisposed_4((bool)0);
		ObserveOnObservable_1_t1522207957 * L_0 = (ObserveOnObservable_1_t1522207957 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_t437523947 * L_5 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, (Il2CppObject *)__this, (IntPtr_t)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = Disposable_Create_m2910846009(NULL /*static, unused*/, (Action_t437523947 *)L_5, /*hidden argument*/NULL);
		Il2CppObject * L_7 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::OnNext(T)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m2922662396_MethodInfo_var;
extern const uint32_t ObserveOn_OnNext_m1031103679_MetadataUsageId;
extern "C"  void ObserveOn_OnNext_m1031103679_gshared (ObserveOn_t3253138108 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_OnNext_m1031103679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	U3COnNextU3Ec__AnonStorey61_t1362241787 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3COnNextU3Ec__AnonStorey61_t1362241787 * L_0 = (U3COnNextU3Ec__AnonStorey61_t1362241787 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3COnNextU3Ec__AnonStorey61_t1362241787 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3COnNextU3Ec__AnonStorey61_t1362241787 *)L_0;
		U3COnNextU3Ec__AnonStorey61_t1362241787 * L_1 = V_1;
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_2(L_2);
		U3COnNextU3Ec__AnonStorey61_t1362241787 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_3(__this);
		U3COnNextU3Ec__AnonStorey61_t1362241787 * L_4 = V_1;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_self_0(L_5);
		LinkedList_1_t3369050920 * L_6 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_6;
		LinkedList_1_t3369050920 * L_7 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		{
			bool L_8 = (bool)__this->get_isDisposed_4();
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0058);
		}

IL_003c:
		{
			U3COnNextU3Ec__AnonStorey61_t1362241787 * L_9 = V_1;
			LinkedList_1_t3369050920 * L_10 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			U3COnNextU3Ec__AnonStorey61_t1362241787 * L_11 = V_1;
			NullCheck(L_11);
			SingleAssignmentDisposable_t2336378823 * L_12 = (SingleAssignmentDisposable_t2336378823 *)L_11->get_self_0();
			NullCheck((LinkedList_1_t3369050920 *)L_10);
			LinkedListNode_1_t1330911994 * L_13 = LinkedList_1_AddLast_m2922662396((LinkedList_1_t3369050920 *)L_10, (Il2CppObject *)L_12, /*hidden argument*/LinkedList_1_AddLast_m2922662396_MethodInfo_var);
			NullCheck(L_9);
			L_9->set_node_1(L_13);
			IL2CPP_LEAVE(0x5F, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005f:
	{
		U3COnNextU3Ec__AnonStorey61_t1362241787 * L_15 = V_1;
		NullCheck(L_15);
		SingleAssignmentDisposable_t2336378823 * L_16 = (SingleAssignmentDisposable_t2336378823 *)L_15->get_self_0();
		ObserveOnObservable_1_t1522207957 * L_17 = (ObserveOnObservable_1_t1522207957 *)__this->get_parent_2();
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_scheduler_2();
		U3COnNextU3Ec__AnonStorey61_t1362241787 * L_19 = V_1;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Action_t437523947 * L_21 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_21, (Il2CppObject *)L_19, (IntPtr_t)L_20, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_18);
		Il2CppObject * L_22 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_18, (Action_t437523947 *)L_21);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_16);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_16, (Il2CppObject *)L_22, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::OnError(System.Exception)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m2922662396_MethodInfo_var;
extern const uint32_t ObserveOn_OnError_m674111950_MetadataUsageId;
extern "C"  void ObserveOn_OnError_m674111950_gshared (ObserveOn_t3253138108 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_OnError_m674111950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	U3COnErrorU3Ec__AnonStorey62_t681819067 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3COnErrorU3Ec__AnonStorey62_t681819067 * L_0 = (U3COnErrorU3Ec__AnonStorey62_t681819067 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (U3COnErrorU3Ec__AnonStorey62_t681819067 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_1 = (U3COnErrorU3Ec__AnonStorey62_t681819067 *)L_0;
		U3COnErrorU3Ec__AnonStorey62_t681819067 * L_1 = V_1;
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck(L_1);
		L_1->set_error_2(L_2);
		U3COnErrorU3Ec__AnonStorey62_t681819067 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_3(__this);
		U3COnErrorU3Ec__AnonStorey62_t681819067 * L_4 = V_1;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_self_0(L_5);
		LinkedList_1_t3369050920 * L_6 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_6;
		LinkedList_1_t3369050920 * L_7 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		{
			bool L_8 = (bool)__this->get_isDisposed_4();
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0058);
		}

IL_003c:
		{
			U3COnErrorU3Ec__AnonStorey62_t681819067 * L_9 = V_1;
			LinkedList_1_t3369050920 * L_10 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			U3COnErrorU3Ec__AnonStorey62_t681819067 * L_11 = V_1;
			NullCheck(L_11);
			SingleAssignmentDisposable_t2336378823 * L_12 = (SingleAssignmentDisposable_t2336378823 *)L_11->get_self_0();
			NullCheck((LinkedList_1_t3369050920 *)L_10);
			LinkedListNode_1_t1330911994 * L_13 = LinkedList_1_AddLast_m2922662396((LinkedList_1_t3369050920 *)L_10, (Il2CppObject *)L_12, /*hidden argument*/LinkedList_1_AddLast_m2922662396_MethodInfo_var);
			NullCheck(L_9);
			L_9->set_node_1(L_13);
			IL2CPP_LEAVE(0x5F, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005f:
	{
		U3COnErrorU3Ec__AnonStorey62_t681819067 * L_15 = V_1;
		NullCheck(L_15);
		SingleAssignmentDisposable_t2336378823 * L_16 = (SingleAssignmentDisposable_t2336378823 *)L_15->get_self_0();
		ObserveOnObservable_1_t1522207957 * L_17 = (ObserveOnObservable_1_t1522207957 *)__this->get_parent_2();
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_scheduler_2();
		U3COnErrorU3Ec__AnonStorey62_t681819067 * L_19 = V_1;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Action_t437523947 * L_21 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_21, (Il2CppObject *)L_19, (IntPtr_t)L_20, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_18);
		Il2CppObject * L_22 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_18, (Action_t437523947 *)L_21);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_16);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_16, (Il2CppObject *)L_22, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::OnCompleted()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m2922662396_MethodInfo_var;
extern const uint32_t ObserveOn_OnCompleted_m1538693665_MetadataUsageId;
extern "C"  void ObserveOn_OnCompleted_m1538693665_gshared (ObserveOn_t3253138108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_OnCompleted_m1538693665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	U3COnCompletedU3Ec__AnonStorey63_t4115224287 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3COnCompletedU3Ec__AnonStorey63_t4115224287 * L_0 = (U3COnCompletedU3Ec__AnonStorey63_t4115224287 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3COnCompletedU3Ec__AnonStorey63_t4115224287 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_1 = (U3COnCompletedU3Ec__AnonStorey63_t4115224287 *)L_0;
		U3COnCompletedU3Ec__AnonStorey63_t4115224287 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3COnCompletedU3Ec__AnonStorey63_t4115224287 * L_2 = V_1;
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_self_0(L_3);
		LinkedList_1_t3369050920 * L_4 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_4;
		LinkedList_1_t3369050920 * L_5 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		{
			bool L_6 = (bool)__this->get_isDisposed_4();
			if (!L_6)
			{
				goto IL_0035;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0051);
		}

IL_0035:
		{
			U3COnCompletedU3Ec__AnonStorey63_t4115224287 * L_7 = V_1;
			LinkedList_1_t3369050920 * L_8 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			U3COnCompletedU3Ec__AnonStorey63_t4115224287 * L_9 = V_1;
			NullCheck(L_9);
			SingleAssignmentDisposable_t2336378823 * L_10 = (SingleAssignmentDisposable_t2336378823 *)L_9->get_self_0();
			NullCheck((LinkedList_1_t3369050920 *)L_8);
			LinkedListNode_1_t1330911994 * L_11 = LinkedList_1_AddLast_m2922662396((LinkedList_1_t3369050920 *)L_8, (Il2CppObject *)L_10, /*hidden argument*/LinkedList_1_AddLast_m2922662396_MethodInfo_var);
			NullCheck(L_7);
			L_7->set_node_1(L_11);
			IL2CPP_LEAVE(0x58, FINALLY_0051);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0051;
	}

FINALLY_0051:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_12 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(81)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(81)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0058:
	{
		U3COnCompletedU3Ec__AnonStorey63_t4115224287 * L_13 = V_1;
		NullCheck(L_13);
		SingleAssignmentDisposable_t2336378823 * L_14 = (SingleAssignmentDisposable_t2336378823 *)L_13->get_self_0();
		ObserveOnObservable_1_t1522207957 * L_15 = (ObserveOnObservable_1_t1522207957 *)__this->get_parent_2();
		NullCheck(L_15);
		Il2CppObject * L_16 = (Il2CppObject *)L_15->get_scheduler_2();
		U3COnCompletedU3Ec__AnonStorey63_t4115224287 * L_17 = V_1;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_t437523947 * L_19 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_16);
		Il2CppObject * L_20 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_16, (Action_t437523947 *)L_19);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_14);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_14, (Il2CppObject *)L_20, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::<Run>m__7D()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t511663336_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_GetEnumerator_m4107050716_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1256013079_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2208938038_MethodInfo_var;
extern const uint32_t ObserveOn_U3CRunU3Em__7D_m4123616088_MetadataUsageId;
extern "C"  void ObserveOn_U3CRunU3Em__7D_m4123616088_gshared (ObserveOn_t3253138108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_U3CRunU3Em__7D_m4123616088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t511663336  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LinkedList_1_t3369050920 * L_0 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_0;
		LinkedList_1_t3369050920 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			__this->set_isDisposed_4((bool)1);
			LinkedList_1_t3369050920 * L_2 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			NullCheck((LinkedList_1_t3369050920 *)L_2);
			Enumerator_t511663336  L_3 = LinkedList_1_GetEnumerator_m4107050716((LinkedList_1_t3369050920 *)L_2, /*hidden argument*/LinkedList_1_GetEnumerator_m4107050716_MethodInfo_var);
			V_2 = (Enumerator_t511663336 )L_3;
		}

IL_0020:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0033;
			}

IL_0025:
			{
				Il2CppObject * L_4 = Enumerator_get_Current_m1256013079((Enumerator_t511663336 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m1256013079_MethodInfo_var);
				V_1 = (Il2CppObject *)L_4;
				Il2CppObject * L_5 = V_1;
				NullCheck((Il2CppObject *)L_5);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			}

IL_0033:
			{
				bool L_6 = Enumerator_MoveNext_m2208938038((Enumerator_t511663336 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m2208938038_MethodInfo_var);
				if (L_6)
				{
					goto IL_0025;
				}
			}

IL_003f:
			{
				IL2CPP_LEAVE(0x50, FINALLY_0044);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0044;
		}

FINALLY_0044:
		{ // begin finally (depth: 2)
			Enumerator_t511663336  L_7 = V_2;
			Enumerator_t511663336  L_8 = L_7;
			Il2CppObject * L_9 = Box(Enumerator_t511663336_il2cpp_TypeInfo_var, &L_8);
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(68)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(68)
		{
			IL2CPP_JUMP_TBL(0x50, IL_0050)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0050:
		{
			LinkedList_1_t3369050920 * L_10 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			NullCheck((LinkedList_1_t3369050920 *)L_10);
			VirtActionInvoker0::Invoke(14 /* System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::Clear() */, (LinkedList_1_t3369050920 *)L_10);
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_11 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::.ctor(UniRx.Operators.ObserveOnObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* LinkedList_1_t3369050920_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m3255264475_MethodInfo_var;
extern const uint32_t ObserveOn__ctor_m368439203_MetadataUsageId;
extern "C"  void ObserveOn__ctor_m368439203_gshared (ObserveOn_t679350430 * __this, ObserveOnObservable_1_t3243387575 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn__ctor_m368439203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t3369050920 * L_0 = (LinkedList_1_t3369050920 *)il2cpp_codegen_object_new(LinkedList_1_t3369050920_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m3255264475(L_0, /*hidden argument*/LinkedList_1__ctor_m3255264475_MethodInfo_var);
		__this->set_scheduleDisposables_3(L_0);
		Il2CppObject* L_1 = ___observer1;
		Il2CppObject * L_2 = ___cancel2;
		NullCheck((OperatorObserverBase_2_t4165376397 *)__this);
		((  void (*) (OperatorObserverBase_2_t4165376397 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t4165376397 *)__this, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObserveOnObservable_1_t3243387575 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::Run()
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ObserveOn_Run_m2434727249_MetadataUsageId;
extern "C"  Il2CppObject * ObserveOn_Run_m2434727249_gshared (ObserveOn_t679350430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_Run_m2434727249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		__this->set_isDisposed_4((bool)0);
		ObserveOnObservable_1_t3243387575 * L_0 = (ObserveOnObservable_1_t3243387575 *)__this->get_parent_2();
		NullCheck(L_0);
		Il2CppObject* L_1 = (Il2CppObject*)L_0->get_source_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject*)__this);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Action_t437523947 * L_5 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, (Il2CppObject *)__this, (IntPtr_t)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = Disposable_Create_m2910846009(NULL /*static, unused*/, (Action_t437523947 *)L_5, /*hidden argument*/NULL);
		Il2CppObject * L_7 = StableCompositeDisposable_Create_m3764788535(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::OnNext(T)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m2922662396_MethodInfo_var;
extern const uint32_t ObserveOn_OnNext_m2580489429_MetadataUsageId;
extern "C"  void ObserveOn_OnNext_m2580489429_gshared (ObserveOn_t679350430 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_OnNext_m2580489429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	U3COnNextU3Ec__AnonStorey61_t3083421405 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3COnNextU3Ec__AnonStorey61_t3083421405 * L_0 = (U3COnNextU3Ec__AnonStorey61_t3083421405 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3COnNextU3Ec__AnonStorey61_t3083421405 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3COnNextU3Ec__AnonStorey61_t3083421405 *)L_0;
		U3COnNextU3Ec__AnonStorey61_t3083421405 * L_1 = V_1;
		Unit_t2558286038  L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_2(L_2);
		U3COnNextU3Ec__AnonStorey61_t3083421405 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_3(__this);
		U3COnNextU3Ec__AnonStorey61_t3083421405 * L_4 = V_1;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_self_0(L_5);
		LinkedList_1_t3369050920 * L_6 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_6;
		LinkedList_1_t3369050920 * L_7 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		{
			bool L_8 = (bool)__this->get_isDisposed_4();
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0058);
		}

IL_003c:
		{
			U3COnNextU3Ec__AnonStorey61_t3083421405 * L_9 = V_1;
			LinkedList_1_t3369050920 * L_10 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			U3COnNextU3Ec__AnonStorey61_t3083421405 * L_11 = V_1;
			NullCheck(L_11);
			SingleAssignmentDisposable_t2336378823 * L_12 = (SingleAssignmentDisposable_t2336378823 *)L_11->get_self_0();
			NullCheck((LinkedList_1_t3369050920 *)L_10);
			LinkedListNode_1_t1330911994 * L_13 = LinkedList_1_AddLast_m2922662396((LinkedList_1_t3369050920 *)L_10, (Il2CppObject *)L_12, /*hidden argument*/LinkedList_1_AddLast_m2922662396_MethodInfo_var);
			NullCheck(L_9);
			L_9->set_node_1(L_13);
			IL2CPP_LEAVE(0x5F, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005f:
	{
		U3COnNextU3Ec__AnonStorey61_t3083421405 * L_15 = V_1;
		NullCheck(L_15);
		SingleAssignmentDisposable_t2336378823 * L_16 = (SingleAssignmentDisposable_t2336378823 *)L_15->get_self_0();
		ObserveOnObservable_1_t3243387575 * L_17 = (ObserveOnObservable_1_t3243387575 *)__this->get_parent_2();
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_scheduler_2();
		U3COnNextU3Ec__AnonStorey61_t3083421405 * L_19 = V_1;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Action_t437523947 * L_21 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_21, (Il2CppObject *)L_19, (IntPtr_t)L_20, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_18);
		Il2CppObject * L_22 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_18, (Action_t437523947 *)L_21);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_16);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_16, (Il2CppObject *)L_22, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::OnError(System.Exception)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m2922662396_MethodInfo_var;
extern const uint32_t ObserveOn_OnError_m127989220_MetadataUsageId;
extern "C"  void ObserveOn_OnError_m127989220_gshared (ObserveOn_t679350430 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_OnError_m127989220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	U3COnErrorU3Ec__AnonStorey62_t2402998685 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3COnErrorU3Ec__AnonStorey62_t2402998685 * L_0 = (U3COnErrorU3Ec__AnonStorey62_t2402998685 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (U3COnErrorU3Ec__AnonStorey62_t2402998685 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_1 = (U3COnErrorU3Ec__AnonStorey62_t2402998685 *)L_0;
		U3COnErrorU3Ec__AnonStorey62_t2402998685 * L_1 = V_1;
		Exception_t1967233988 * L_2 = ___error0;
		NullCheck(L_1);
		L_1->set_error_2(L_2);
		U3COnErrorU3Ec__AnonStorey62_t2402998685 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_3(__this);
		U3COnErrorU3Ec__AnonStorey62_t2402998685 * L_4 = V_1;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_self_0(L_5);
		LinkedList_1_t3369050920 * L_6 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_6;
		LinkedList_1_t3369050920 * L_7 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		{
			bool L_8 = (bool)__this->get_isDisposed_4();
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0058);
		}

IL_003c:
		{
			U3COnErrorU3Ec__AnonStorey62_t2402998685 * L_9 = V_1;
			LinkedList_1_t3369050920 * L_10 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			U3COnErrorU3Ec__AnonStorey62_t2402998685 * L_11 = V_1;
			NullCheck(L_11);
			SingleAssignmentDisposable_t2336378823 * L_12 = (SingleAssignmentDisposable_t2336378823 *)L_11->get_self_0();
			NullCheck((LinkedList_1_t3369050920 *)L_10);
			LinkedListNode_1_t1330911994 * L_13 = LinkedList_1_AddLast_m2922662396((LinkedList_1_t3369050920 *)L_10, (Il2CppObject *)L_12, /*hidden argument*/LinkedList_1_AddLast_m2922662396_MethodInfo_var);
			NullCheck(L_9);
			L_9->set_node_1(L_13);
			IL2CPP_LEAVE(0x5F, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_14 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005f:
	{
		U3COnErrorU3Ec__AnonStorey62_t2402998685 * L_15 = V_1;
		NullCheck(L_15);
		SingleAssignmentDisposable_t2336378823 * L_16 = (SingleAssignmentDisposable_t2336378823 *)L_15->get_self_0();
		ObserveOnObservable_1_t3243387575 * L_17 = (ObserveOnObservable_1_t3243387575 *)__this->get_parent_2();
		NullCheck(L_17);
		Il2CppObject * L_18 = (Il2CppObject *)L_17->get_scheduler_2();
		U3COnErrorU3Ec__AnonStorey62_t2402998685 * L_19 = V_1;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Action_t437523947 * L_21 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_21, (Il2CppObject *)L_19, (IntPtr_t)L_20, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_18);
		Il2CppObject * L_22 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_18, (Action_t437523947 *)L_21);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_16);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_16, (Il2CppObject *)L_22, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::OnCompleted()
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m2922662396_MethodInfo_var;
extern const uint32_t ObserveOn_OnCompleted_m1986420535_MetadataUsageId;
extern "C"  void ObserveOn_OnCompleted_m1986420535_gshared (ObserveOn_t679350430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_OnCompleted_m1986420535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	U3COnCompletedU3Ec__AnonStorey63_t1541436609 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3COnCompletedU3Ec__AnonStorey63_t1541436609 * L_0 = (U3COnCompletedU3Ec__AnonStorey63_t1541436609 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3COnCompletedU3Ec__AnonStorey63_t1541436609 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_1 = (U3COnCompletedU3Ec__AnonStorey63_t1541436609 *)L_0;
		U3COnCompletedU3Ec__AnonStorey63_t1541436609 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3COnCompletedU3Ec__AnonStorey63_t1541436609 * L_2 = V_1;
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_self_0(L_3);
		LinkedList_1_t3369050920 * L_4 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_4;
		LinkedList_1_t3369050920 * L_5 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		{
			bool L_6 = (bool)__this->get_isDisposed_4();
			if (!L_6)
			{
				goto IL_0035;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_0051);
		}

IL_0035:
		{
			U3COnCompletedU3Ec__AnonStorey63_t1541436609 * L_7 = V_1;
			LinkedList_1_t3369050920 * L_8 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			U3COnCompletedU3Ec__AnonStorey63_t1541436609 * L_9 = V_1;
			NullCheck(L_9);
			SingleAssignmentDisposable_t2336378823 * L_10 = (SingleAssignmentDisposable_t2336378823 *)L_9->get_self_0();
			NullCheck((LinkedList_1_t3369050920 *)L_8);
			LinkedListNode_1_t1330911994 * L_11 = LinkedList_1_AddLast_m2922662396((LinkedList_1_t3369050920 *)L_8, (Il2CppObject *)L_10, /*hidden argument*/LinkedList_1_AddLast_m2922662396_MethodInfo_var);
			NullCheck(L_7);
			L_7->set_node_1(L_11);
			IL2CPP_LEAVE(0x58, FINALLY_0051);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0051;
	}

FINALLY_0051:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_12 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(81)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(81)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0058:
	{
		U3COnCompletedU3Ec__AnonStorey63_t1541436609 * L_13 = V_1;
		NullCheck(L_13);
		SingleAssignmentDisposable_t2336378823 * L_14 = (SingleAssignmentDisposable_t2336378823 *)L_13->get_self_0();
		ObserveOnObservable_1_t3243387575 * L_15 = (ObserveOnObservable_1_t3243387575 *)__this->get_parent_2();
		NullCheck(L_15);
		Il2CppObject * L_16 = (Il2CppObject *)L_15->get_scheduler_2();
		U3COnCompletedU3Ec__AnonStorey63_t1541436609 * L_17 = V_1;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_t437523947 * L_19 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_19, (Il2CppObject *)L_17, (IntPtr_t)L_18, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_16);
		Il2CppObject * L_20 = InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_16, (Action_t437523947 *)L_19);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_14);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_14, (Il2CppObject *)L_20, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<UniRx.Unit>::<Run>m__7D()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t511663336_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_GetEnumerator_m4107050716_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1256013079_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2208938038_MethodInfo_var;
extern const uint32_t ObserveOn_U3CRunU3Em__7D_m3860964226_MetadataUsageId;
extern "C"  void ObserveOn_U3CRunU3Em__7D_m3860964226_gshared (ObserveOn_t679350430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOn_U3CRunU3Em__7D_m3860964226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedList_1_t3369050920 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t511663336  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		LinkedList_1_t3369050920 * L_0 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
		V_0 = (LinkedList_1_t3369050920 *)L_0;
		LinkedList_1_t3369050920 * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			__this->set_isDisposed_4((bool)1);
			LinkedList_1_t3369050920 * L_2 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			NullCheck((LinkedList_1_t3369050920 *)L_2);
			Enumerator_t511663336  L_3 = LinkedList_1_GetEnumerator_m4107050716((LinkedList_1_t3369050920 *)L_2, /*hidden argument*/LinkedList_1_GetEnumerator_m4107050716_MethodInfo_var);
			V_2 = (Enumerator_t511663336 )L_3;
		}

IL_0020:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0033;
			}

IL_0025:
			{
				Il2CppObject * L_4 = Enumerator_get_Current_m1256013079((Enumerator_t511663336 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m1256013079_MethodInfo_var);
				V_1 = (Il2CppObject *)L_4;
				Il2CppObject * L_5 = V_1;
				NullCheck((Il2CppObject *)L_5);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			}

IL_0033:
			{
				bool L_6 = Enumerator_MoveNext_m2208938038((Enumerator_t511663336 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m2208938038_MethodInfo_var);
				if (L_6)
				{
					goto IL_0025;
				}
			}

IL_003f:
			{
				IL2CPP_LEAVE(0x50, FINALLY_0044);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0044;
		}

FINALLY_0044:
		{ // begin finally (depth: 2)
			Enumerator_t511663336  L_7 = V_2;
			Enumerator_t511663336  L_8 = L_7;
			Il2CppObject * L_9 = Box(Enumerator_t511663336_il2cpp_TypeInfo_var, &L_8);
			NullCheck((Il2CppObject *)L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			IL2CPP_END_FINALLY(68)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(68)
		{
			IL2CPP_JUMP_TBL(0x50, IL_0050)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0050:
		{
			LinkedList_1_t3369050920 * L_10 = (LinkedList_1_t3369050920 *)__this->get_scheduleDisposables_3();
			NullCheck((LinkedList_1_t3369050920 *)L_10);
			VirtActionInvoker0::Invoke(14 /* System.Void System.Collections.Generic.LinkedList`1<System.IDisposable>::Clear() */, (LinkedList_1_t3369050920 *)L_10);
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		LinkedList_1_t3369050920 * L_11 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern "C"  void ObserveOnObservable_1__ctor_m3150553362_gshared (ObserveOnObservable_1_t1522207957 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject * L_3 = ___scheduler1;
		__this->set_scheduler_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ObserveOnObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* ISchedulerQueueing_t1271699701_il2cpp_TypeInfo_var;
extern const uint32_t ObserveOnObservable_1_SubscribeCore_m2846530067_MetadataUsageId;
extern "C"  Il2CppObject * ObserveOnObservable_1_SubscribeCore_m2846530067_gshared (ObserveOnObservable_1_t1522207957 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOnObservable_1_SubscribeCore_m2846530067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_2();
		V_0 = (Il2CppObject *)((Il2CppObject *)IsInst(L_0, ISchedulerQueueing_t1271699701_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = V_0;
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		ObserveOn_t3253138108 * L_4 = (ObserveOn_t3253138108 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ObserveOn_t3253138108 *, ObserveOnObservable_1_t1522207957 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (ObserveOnObservable_1_t1522207957 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((ObserveOn_t3253138108 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (ObserveOn_t3253138108 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ObserveOn_t3253138108 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_5;
	}

IL_0020:
	{
		Il2CppObject * L_6 = V_0;
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		ObserveOn__t947369629 * L_9 = (ObserveOn__t947369629 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObserveOn__t947369629 *, ObserveOnObservable_1_t1522207957 *, Il2CppObject *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (ObserveOnObservable_1_t1522207957 *)__this, (Il2CppObject *)L_6, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((ObserveOn__t947369629 *)L_9);
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (ObserveOn__t947369629 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((ObserveOn__t947369629 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_10;
	}
}
// System.Void UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern "C"  void ObserveOnObservable_1__ctor_m410358756_gshared (ObserveOnObservable_1_t3243387575 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		((  void (*) (OperatorObservableBase_1_t1622431009 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t1622431009 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		Il2CppObject * L_3 = ___scheduler1;
		__this->set_scheduler_2(L_3);
		return;
	}
}
// System.IDisposable UniRx.Operators.ObserveOnObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern Il2CppClass* ISchedulerQueueing_t1271699701_il2cpp_TypeInfo_var;
extern const uint32_t ObserveOnObservable_1_SubscribeCore_m2620755791_MetadataUsageId;
extern "C"  Il2CppObject * ObserveOnObservable_1_SubscribeCore_m2620755791_gshared (ObserveOnObservable_1_t3243387575 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObserveOnObservable_1_SubscribeCore_m2620755791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_2();
		V_0 = (Il2CppObject *)((Il2CppObject *)IsInst(L_0, ISchedulerQueueing_t1271699701_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = V_0;
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_2 = ___observer0;
		Il2CppObject * L_3 = ___cancel1;
		ObserveOn_t679350430 * L_4 = (ObserveOn_t679350430 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (ObserveOn_t679350430 *, ObserveOnObservable_1_t3243387575 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_4, (ObserveOnObservable_1_t3243387575 *)__this, (Il2CppObject*)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((ObserveOn_t679350430 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (ObserveOn_t679350430 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ObserveOn_t679350430 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_5;
	}

IL_0020:
	{
		Il2CppObject * L_6 = V_0;
		Il2CppObject* L_7 = ___observer0;
		Il2CppObject * L_8 = ___cancel1;
		ObserveOn__t2668549247 * L_9 = (ObserveOn__t2668549247 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObserveOn__t2668549247 *, ObserveOnObservable_1_t3243387575 *, Il2CppObject *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(L_9, (ObserveOnObservable_1_t3243387575 *)__this, (Il2CppObject *)L_6, (Il2CppObject*)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((ObserveOn__t2668549247 *)L_9);
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (ObserveOn__t2668549247 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((ObserveOn__t2668549247 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_10;
	}
}
// System.Void UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OfType__ctor_m3081147877_gshared (OfType_t384837642 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___observer0;
		Il2CppObject * L_1 = ___cancel1;
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		((  void (*) (OperatorObserverBase_2_t1187768149 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((OperatorObserverBase_2_t1187768149 *)__this, (Il2CppObject*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>::OnNext(TSource)
extern "C"  void OfType_OnNext_m2739570037_gshared (OfType_t384837642 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_002a;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		Il2CppObject* L_2 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Il2CppObject *)L_3);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void OfType_OnError_m2296821407_gshared (OfType_t384837642 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t1967233988 * L_1 = ___error0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Exception_t1967233988 *)L_1);
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_001a:
	{
		return;
	}
}
// System.Void UniRx.Operators.OfTypeObservable`2/OfType<System.Object,System.Object>::OnCompleted()
extern "C"  void OfType_OnCompleted_m2576556146_gshared (OfType_t384837642 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Il2CppObject* L_0 = (Il2CppObject*)((OperatorObserverBase_2_t1187768149 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		IL2CPP_LEAVE(0x19, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		NullCheck((OperatorObserverBase_2_t1187768149 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose() */, (OperatorObserverBase_2_t1187768149 *)__this);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0019:
	{
		return;
	}
}
// System.Void UniRx.Operators.OfTypeObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>)
extern "C"  void OfTypeObservable_2__ctor_m2848500676_gshared (OfTypeObservable_2_t4125111696 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		((  void (*) (OperatorObservableBase_1_t4196218687 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((OperatorObservableBase_1_t4196218687 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		return;
	}
}
// System.IDisposable UniRx.Operators.OfTypeObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * OfTypeObservable_2_SubscribeCore_m3325553601_gshared (OfTypeObservable_2_t4125111696 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_source_1();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject * L_2 = ___cancel1;
		OfType_t384837642 * L_3 = (OfType_t384837642 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (OfType_t384837642 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_3, (Il2CppObject*)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Boolean>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1315711019_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Boolean>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3986125840_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t3570117608 * L_1 = (OperatorObservableBase_1_t3570117608 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3570117608 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3570117608 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Double>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m598786264_gshared (U3CSubscribeU3Ec__AnonStorey5B_t132523343 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Double>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2216203645_gshared (U3CSubscribeU3Ec__AnonStorey5B_t132523343 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t3893628881 * L_1 = (OperatorObservableBase_1_t3893628881 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3893628881 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Double>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3893628881 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int32>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1035516561_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int32>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1007224054_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t1911559758 * L_1 = (OperatorObservableBase_1_t1911559758 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1911559758 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1911559758 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int64>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2123829296_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int64>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3198709461_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t1911559853 * L_1 = (OperatorObservableBase_1_t1911559853 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1911559853 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1911559853 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Object>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m3489265350_gshared (U3CSubscribeU3Ec__AnonStorey5B_t435113149 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Object>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1122764779_gshared (U3CSubscribeU3Ec__AnonStorey5B_t435113149 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t4196218687 * L_1 = (OperatorObservableBase_1_t4196218687 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t4196218687 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t4196218687 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Single>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m3147227279_gshared (U3CSubscribeU3Ec__AnonStorey5B_t556215750 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Single>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3136660340_gshared (U3CSubscribeU3Ec__AnonStorey5B_t556215750 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t22353992 * L_1 = (OperatorObservableBase_1_t22353992 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t22353992 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Single>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t22353992 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m822342948_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2305812169_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t1480666958 * L_1 = (OperatorObservableBase_1_t1480666958 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1480666958 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1480666958 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2871798460_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m442570337_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t1012822357 * L_1 = (OperatorObservableBase_1_t1012822357 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1012822357 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1012822357 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m52628186_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionMoveEvent`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1344300799_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t1980578818 * L_1 = (OperatorObservableBase_1_t1980578818 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1980578818 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1980578818 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2252552390_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2347551211_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t1512734217 * L_1 = (OperatorObservableBase_1_t1512734217 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1512734217 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1512734217 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m178979533_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionRemoveEvent`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2508860978_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t3601749991 * L_1 = (OperatorObservableBase_1_t3601749991 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3601749991 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3601749991 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2644085171_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m653431704_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t3133905390 * L_1 = (OperatorObservableBase_1_t3133905390 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3133905390 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3133905390 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m671978679_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionReplaceEvent`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3834637724_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t905895507 * L_1 = (OperatorObservableBase_1_t905895507 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t905895507 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t905895507 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m3196155913_gshared (U3CSubscribeU3Ec__AnonStorey5B_t971912664 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2912437358_gshared (U3CSubscribeU3Ec__AnonStorey5B_t971912664 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t438050906 * L_1 = (OperatorObservableBase_1_t438050906 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t438050906 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t438050906 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m39373529_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1491477310_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t3610093275 * L_1 = (OperatorObservableBase_1_t3610093275 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3610093275 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3610093275 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2744395186_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2562075607_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t2386433262 * L_1 = (OperatorObservableBase_1_t2386433262 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t2386433262 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t2386433262 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m933259564_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1522497745_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t2843072034 * L_1 = (OperatorObservableBase_1_t2843072034 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t2843072034 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t2843072034 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Pair`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m227286753_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Pair`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1687459142_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t2895242553 * L_1 = (OperatorObservableBase_1_t2895242553 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t2895242553 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Pair`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t2895242553 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.TimeInterval`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1520230665_gshared (U3CSubscribeU3Ec__AnonStorey5B_t306072271 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.TimeInterval`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2961010030_gshared (U3CSubscribeU3Ec__AnonStorey5B_t306072271 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t4067177809 * L_1 = (OperatorObservableBase_1_t4067177809 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t4067177809 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t4067177809 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Timestamped`1<System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1042729392_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Timestamped`1<System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3643787349_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t1583329244 * L_1 = (OperatorObservableBase_1_t1583329244 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1583329244 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Timestamped`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1583329244 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2595250922_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`2<System.Object,System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m968358671_gshared (U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t3728374086 * L_1 = (OperatorObservableBase_1_t3728374086 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3728374086 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3728374086 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m4125943511_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3085121468_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t715044073 * L_1 = (OperatorObservableBase_1_t715044073 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t715044073 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t715044073 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m4059825604_gshared (U3CSubscribeU3Ec__AnonStorey5B_t625237010 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3970322281_gshared (U3CSubscribeU3Ec__AnonStorey5B_t625237010 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t91375252 * L_1 = (OperatorObservableBase_1_t91375252 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t91375252 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t91375252 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Unit>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2116033758_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Unit>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2164739_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t1622431009 * L_1 = (OperatorObservableBase_1_t1622431009 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1622431009 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1622431009 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Color>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1328540788_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Color>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3430631961_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t652320731 * L_1 = (OperatorObservableBase_1_t652320731 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t652320731 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t652320731 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1676577376_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Vector2>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2886343941_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * __this, const MethodInfo* method)
{
	{
		SingleAssignmentDisposable_t2336378823 * L_0 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		OperatorObservableBase_1_t2589474759 * L_1 = (OperatorObservableBase_1_t2589474759 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_observer_0();
		SingleAssignmentDisposable_t2336378823 * L_3 = (SingleAssignmentDisposable_t2336378823 *)__this->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t2589474759 *)L_1);
		Il2CppObject * L_4 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t2589474759 *)L_1, (Il2CppObject*)L_2, (Il2CppObject *)L_3);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_0);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<System.Boolean>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m286364973_gshared (OperatorObservableBase_1_t3570117608 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Boolean>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m219767309_gshared (OperatorObservableBase_1_t3570117608 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m19433973_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m19433973_gshared (OperatorObservableBase_1_t3570117608 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m19433973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t4103979366 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3570117608 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3570117608 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4103979366 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<System.Double>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m1397930020_gshared (OperatorObservableBase_1_t3893628881 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Double>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1223935358_gshared (OperatorObservableBase_1_t3893628881 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Double>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1301098818_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1301098818_gshared (OperatorObservableBase_1_t3893628881 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1301098818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t132523343 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t132523343 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t132523343 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t132523343 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3893628881 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Double>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3893628881 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t132523343 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<System.Int32>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m89273299_gshared (OperatorObservableBase_1_t1911559758 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Int32>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2190956967_gshared (OperatorObservableBase_1_t1911559758 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1043764827_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1043764827_gshared (OperatorObservableBase_1_t1911559758 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1043764827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1911559758 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1911559758 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<System.Int64>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m322881330_gshared (OperatorObservableBase_1_t1911559853 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Int64>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3602103400_gshared (OperatorObservableBase_1_t1911559853 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1755692794_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1755692794_gshared (OperatorObservableBase_1_t1911559853 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1755692794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1911559853 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1911559853 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<System.Object>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m1121449362_gshared (OperatorObservableBase_1_t4196218687 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1834630736_gshared (OperatorObservableBase_1_t4196218687 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m3551460656_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3551460656_gshared (OperatorObservableBase_1_t4196218687 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m3551460656_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t435113149 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t435113149 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t435113149 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t435113149 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t4196218687 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t4196218687 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t435113149 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<System.Single>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m4000287643_gshared (OperatorObservableBase_1_t22353992 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<System.Single>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2802103591_gshared (OperatorObservableBase_1_t22353992 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Single>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1800231929_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1800231929_gshared (OperatorObservableBase_1_t22353992 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1800231929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t556215750 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t556215750 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t556215750 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t556215750 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t22353992 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<System.Single>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t22353992 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t556215750 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3379631600_gshared (OperatorObservableBase_1_t1480666958 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3170605106_gshared (OperatorObservableBase_1_t1480666958 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m2850056590_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m2850056590_gshared (OperatorObservableBase_1_t1480666958 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m2850056590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2014528716 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1480666958 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1480666958 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2014528716 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m4145965310_gshared (OperatorObservableBase_1_t1012822357 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3237727772_gshared (OperatorObservableBase_1_t1012822357 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m2674514310_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m2674514310_gshared (OperatorObservableBase_1_t1012822357 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m2674514310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1012822357 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1012822357 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3809725212_gshared (OperatorObservableBase_1_t1980578818 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m382070334_gshared (OperatorObservableBase_1_t1980578818 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1803413412_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1803413412_gshared (OperatorObservableBase_1_t1980578818 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1803413412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2514440576 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2514440576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2514440576 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1980578818 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1980578818 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2514440576 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m1270960978_gshared (OperatorObservableBase_1_t1512734217 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2100380560_gshared (OperatorObservableBase_1_t1512734217 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m3392920368_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3392920368_gshared (OperatorObservableBase_1_t1512734217 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m3392920368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2046595975 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2046595975 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2046595975 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1512734217 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1512734217 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2046595975 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3936515855_gshared (OperatorObservableBase_1_t3601749991 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m617826027_gshared (OperatorObservableBase_1_t3601749991 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m33434263_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m33434263_gshared (OperatorObservableBase_1_t3601749991 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m33434263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t4135611749 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t4135611749 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t4135611749 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3601749991 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3601749991 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4135611749 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3514658879_gshared (OperatorObservableBase_1_t3133905390 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3052594691_gshared (OperatorObservableBase_1_t3133905390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m3957440797_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3957440797_gshared (OperatorObservableBase_1_t3133905390 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m3957440797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t3667767148 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3667767148 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t3667767148 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3133905390 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3133905390 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3667767148 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m347555267_gshared (OperatorObservableBase_1_t905895507 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2263078399_gshared (OperatorObservableBase_1_t905895507 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m3540047905_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3540047905_gshared (OperatorObservableBase_1_t905895507 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m3540047905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t1439757265 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1439757265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t1439757265 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t905895507 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t905895507 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1439757265 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3144420875_gshared (OperatorObservableBase_1_t438050906 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3476954991_gshared (OperatorObservableBase_1_t438050906 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m523155411_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m523155411_gshared (OperatorObservableBase_1_t438050906 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m523155411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t971912664 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t971912664 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t971912664 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t971912664 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t438050906 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t438050906 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t971912664 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m574362149_gshared (OperatorObservableBase_1_t3610093275 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2194074717_gshared (OperatorObservableBase_1_t3610093275 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1554170947_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1554170947_gshared (OperatorObservableBase_1_t3610093275 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1554170947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t4143955033 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t4143955033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t4143955033 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3610093275 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3610093275 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4143955033 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3444905460_gshared (OperatorObservableBase_1_t2386433262 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3625574502_gshared (OperatorObservableBase_1_t2386433262 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m2541197948_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m2541197948_gshared (OperatorObservableBase_1_t2386433262 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m2541197948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2920295020 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2920295020 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2920295020 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t2386433262 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t2386433262 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2920295020 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m918933688_gshared (OperatorObservableBase_1_t2843072034 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1093387114_gshared (OperatorObservableBase_1_t2843072034 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m3161911702_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3161911702_gshared (OperatorObservableBase_1_t2843072034 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m3161911702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t2843072034 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t2843072034 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.Pair`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m1288468909_gshared (OperatorObservableBase_1_t2895242553 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.Pair`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2404778453_gshared (OperatorObservableBase_1_t2895242553 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Pair`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m2478659659_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m2478659659_gshared (OperatorObservableBase_1_t2895242553 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m2478659659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t3429104311 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t2895242553 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Pair`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t2895242553 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3429104311 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m1003778261_gshared (OperatorObservableBase_1_t4067177809 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2877731245_gshared (OperatorObservableBase_1_t4067177809 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m3230356083_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3230356083_gshared (OperatorObservableBase_1_t4067177809 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m3230356083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t306072271 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t306072271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t306072271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t306072271 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t4067177809 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t4067177809 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t306072271 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.Timestamped`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m63723122_gshared (OperatorObservableBase_1_t1583329244 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.Timestamped`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1517348904_gshared (OperatorObservableBase_1_t1583329244 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Timestamped`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1817523322_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1817523322_gshared (OperatorObservableBase_1_t1583329244 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1817523322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2117191002 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2117191002 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2117191002 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1583329244 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Timestamped`1<System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1583329244 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2117191002 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3390949164_gshared (OperatorObservableBase_1_t3728374086 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3424800814_gshared (OperatorObservableBase_1_t3728374086 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1233653684_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1233653684_gshared (OperatorObservableBase_1_t3728374086 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1233653684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t4262235844 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t4262235844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t4262235844 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t3728374086 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t3728374086 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t4262235844 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m1317439769_gshared (OperatorObservableBase_1_t715044073 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1009456545_gshared (OperatorObservableBase_1_t715044073 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m3094300833_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3094300833_gshared (OperatorObservableBase_1_t715044073 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m3094300833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t715044073 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t715044073 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m711874054_gshared (OperatorObservableBase_1_t91375252 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3166908436_gshared (OperatorObservableBase_1_t91375252 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m2627411086_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m2627411086_gshared (OperatorObservableBase_1_t91375252 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m2627411086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t625237010 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t625237010 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t625237010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t625237010 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t91375252 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t91375252 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t625237010 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m284357152_gshared (OperatorObservableBase_1_t1622431009 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m1449971386_gshared (OperatorObservableBase_1_t1622431009 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m469429672_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m469429672_gshared (OperatorObservableBase_1_t1622431009 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m469429672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2156292767 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2156292767 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t2156292767 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t1622431009 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t1622431009 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t2156292767 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3365105728_gshared (OperatorObservableBase_1_t652320731 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3379150818_gshared (OperatorObservableBase_1_t652320731 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m1120264414_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1120264414_gshared (OperatorObservableBase_1_t652320731 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m1120264414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t652320731 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t652320731 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m2658942956_gshared (OperatorObservableBase_1_t2589474759 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___isRequiredSubscribeOnCurrentThread0;
		__this->set_isRequiredSubscribeOnCurrentThread_0(L_0);
		return;
	}
}
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3648010678_gshared (OperatorObservableBase_1_t2589474759 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		return L_0;
	}
}
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObservableBase_1_Subscribe_m943572682_MetadataUsageId;
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m943572682_gshared (OperatorObservableBase_1_t2589474759 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObservableBase_1_Subscribe_m943572682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * V_0 = NULL;
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_0 = (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 *)L_0;
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_1 = V_0;
		Il2CppObject* L_2 = ___observer0;
		NullCheck(L_1);
		L_1->set_observer_0(L_2);
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_4 = V_0;
		SingleAssignmentDisposable_t2336378823 * L_5 = (SingleAssignmentDisposable_t2336378823 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t2336378823_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m917449122(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_subscription_1(L_5);
		bool L_6 = (bool)__this->get_isRequiredSubscribeOnCurrentThread_0();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		bool L_7 = Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = ((Scheduler_t103934925_StaticFields*)Scheduler_t103934925_il2cpp_TypeInfo_var->static_fields)->get_CurrentThread_0();
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11);
		goto IL_006d;
	}

IL_0050:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_12 = V_0;
		NullCheck(L_12);
		SingleAssignmentDisposable_t2336378823 * L_13 = (SingleAssignmentDisposable_t2336378823 *)L_12->get_subscription_1();
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject* L_15 = (Il2CppObject*)L_14->get_observer_0();
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_16 = V_0;
		NullCheck(L_16);
		SingleAssignmentDisposable_t2336378823 * L_17 = (SingleAssignmentDisposable_t2336378823 *)L_16->get_subscription_1();
		NullCheck((OperatorObservableBase_1_t2589474759 *)__this);
		Il2CppObject * L_18 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject*, Il2CppObject * >::Invoke(6 /* System.IDisposable UniRx.Operators.OperatorObservableBase`1<UnityEngine.Vector2>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable) */, (OperatorObservableBase_1_t2589474759 *)__this, (Il2CppObject*)L_15, (Il2CppObject *)L_17);
		NullCheck((SingleAssignmentDisposable_t2336378823 *)L_13);
		SingleAssignmentDisposable_set_Disposable_m1599028885((SingleAssignmentDisposable_t2336378823 *)L_13, (Il2CppObject *)L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * L_19 = V_0;
		NullCheck(L_19);
		SingleAssignmentDisposable_t2336378823 * L_20 = (SingleAssignmentDisposable_t2336378823 *)L_19->get_subscription_1();
		return L_20;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1574733264_gshared (OperatorObserverBase_2_t60103313 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2651616794_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2651616794_gshared (OperatorObserverBase_2_t60103313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2651616794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3915325627 * L_0 = ((EmptyObserver_1_t3915325627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3632559528_gshared (OperatorObserverBase_2_t1921935565 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Double,System.Double>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m3668090178_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m3668090178_gshared (OperatorObserverBase_2_t1921935565 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m3668090178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t4238836900 * L_0 = ((EmptyObserver_1_t4238836900_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3978315088_gshared (OperatorObserverBase_2_t701568569 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m3213321882_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m3213321882_gshared (OperatorObserverBase_2_t701568569 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m3213321882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2256767777 * L_0 = ((EmptyObserver_1_t2256767777_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,UnityEngine.Color>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3519008741_gshared (OperatorObserverBase_2_t3737296838 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,UnityEngine.Color>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m1461818149_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m1461818149_gshared (OperatorObserverBase_2_t3737296838 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m1461818149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t997528750 * L_0 = ((EmptyObserver_1_t997528750_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Boolean>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1368023573_gshared (OperatorObserverBase_2_t1303321368 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Boolean>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2455748341_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2455748341_gshared (OperatorObserverBase_2_t1303321368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2455748341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3915325627 * L_0 = ((EmptyObserver_1_t3915325627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int32>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m2053408303_gshared (OperatorObserverBase_2_t3939730814 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int32>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m1865236251_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m1865236251_gshared (OperatorObserverBase_2_t3939730814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m1865236251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2256767777 * L_0 = ((EmptyObserver_1_t2256767777_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3985458928_gshared (OperatorObserverBase_2_t3939730909 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m4056721658_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m4056721658_gshared (OperatorObserverBase_2_t3939730909 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m4056721658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2256767872 * L_0 = ((EmptyObserver_1_t2256767872_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1945314260_gshared (OperatorObserverBase_2_t1929422447 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m1235051158_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m1235051158_gshared (OperatorObserverBase_2_t1929422447 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m1235051158_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t246459410 * L_0 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3493258426_gshared (OperatorObserverBase_2_t322678519 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2199592304_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2199592304_gshared (OperatorObserverBase_2_t322678519 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2199592304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2934682778 * L_0 = ((EmptyObserver_1_t2934682778_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Boolean>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3112582429_gshared (OperatorObserverBase_2_t561667070 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Boolean>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2145671405_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2145671405_gshared (OperatorObserverBase_2_t561667070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2145671405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3915325627 * L_0 = ((EmptyObserver_1_t3915325627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1863043020_gshared (OperatorObserverBase_2_t1187768149 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m3164711326_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m3164711326_gshared (OperatorObserverBase_2_t1187768149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m3164711326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t246459410 * L_0 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Pair`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3106988753_gshared (OperatorObserverBase_2_t4181759311 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Pair`1<System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m986082233_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m986082233_gshared (OperatorObserverBase_2_t4181759311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m986082233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3240450572 * L_0 = ((EmptyObserver_1_t3240450572_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.TimeInterval`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m4102253737_gshared (OperatorObserverBase_2_t1058727271 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.TimeInterval`1<System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m3076929761_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m3076929761_gshared (OperatorObserverBase_2_t1058727271 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m3076929761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t117418532 * L_0 = ((EmptyObserver_1_t117418532_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Timestamped`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1384576056_gshared (OperatorObserverBase_2_t2869846002 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Timestamped`1<System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m3532085426_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m3532085426_gshared (OperatorObserverBase_2_t2869846002 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m3532085426_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t1928537263 * L_0 = ((EmptyObserver_1_t1928537263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m476501194_gshared (OperatorObserverBase_2_t2908947767 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m73684832_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m73684832_gshared (OperatorObserverBase_2_t2908947767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m73684832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t1967639028 * L_0 = ((EmptyObserver_1_t1967639028_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Single,System.Single>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1873532218_gshared (OperatorObserverBase_2_t2516778257 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Single,System.Single>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m1488289008_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m1488289008_gshared (OperatorObserverBase_2_t2516778257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m1488289008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t367562011 * L_0 = ((EmptyObserver_1_t367562011_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<System.Object>,UniRx.CollectionAddEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m483541008_gshared (OperatorObserverBase_2_t3545535033 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<System.Object>,UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2240521178_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2240521178_gshared (OperatorObserverBase_2_t3545535033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2240521178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t1825874977 * L_0 = ((EmptyObserver_1_t1825874977_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m2668489712_gshared (OperatorObserverBase_2_t2669133373 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m1659708410_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m1659708410_gshared (OperatorObserverBase_2_t2669133373 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m1659708410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t1358030376 * L_0 = ((EmptyObserver_1_t1358030376_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<System.Object>,UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1146492848_gshared (OperatorObserverBase_2_t2793497833 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<System.Object>,UniRx.CollectionMoveEvent`1<System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2429276218_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2429276218_gshared (OperatorObserverBase_2_t2793497833 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2429276218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2325786837 * L_0 = ((EmptyObserver_1_t2325786837_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3648213196_gshared (OperatorObserverBase_2_t1917096173 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2243018910_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2243018910_gshared (OperatorObserverBase_2_t1917096173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2243018910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t1857942236 * L_0 = ((EmptyObserver_1_t1857942236_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<System.Object>,UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m176297936_gshared (OperatorObserverBase_2_t3997058997 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<System.Object>,UniRx.CollectionRemoveEvent`1<System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m593896474_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m593896474_gshared (OperatorObserverBase_2_t3997058997 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m593896474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3946958010 * L_0 = ((EmptyObserver_1_t3946958010_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1359642226_gshared (OperatorObserverBase_2_t3120657337 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m448628408_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m448628408_gshared (OperatorObserverBase_2_t3120657337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m448628408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3479113409 * L_0 = ((EmptyObserver_1_t3479113409_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1289283818_gshared (OperatorObserverBase_2_t3791266949 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<System.Object>,UniRx.CollectionReplaceEvent`1<System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2423068480_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2423068480_gshared (OperatorObserverBase_2_t3791266949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2423068480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t1251103526 * L_0 = ((EmptyObserver_1_t1251103526_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m2139166480_gshared (OperatorObserverBase_2_t2914865289 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>,UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m1008807130_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m1008807130_gshared (OperatorObserverBase_2_t2914865289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m1008807130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t783258925 * L_0 = ((EmptyObserver_1_t783258925_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryAddEvent`2<System.Object,System.Object>,UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m4168802150_gshared (OperatorObserverBase_2_t3051653477 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryAddEvent`2<System.Object,System.Object>,UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m545351236_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m545351236_gshared (OperatorObserverBase_2_t3051653477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m545351236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3955301294 * L_0 = ((EmptyObserver_1_t3955301294_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>,UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m4215535280_gshared (OperatorObserverBase_2_t3745721 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>,UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m974606650_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m974606650_gshared (OperatorObserverBase_2_t3745721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m974606650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2731641281 * L_0 = ((EmptyObserver_1_t2731641281_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>,UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1489859200_gshared (OperatorObserverBase_2_t154015849 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>,UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m751425386_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m751425386_gshared (OperatorObserverBase_2_t154015849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m751425386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3188280053 * L_0 = ((EmptyObserver_1_t3188280053_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m315769264_gshared (OperatorObserverBase_2_t311366489 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m4095600186_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m4095600186_gshared (OperatorObserverBase_2_t311366489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m4095600186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t4073582105 * L_0 = ((EmptyObserver_1_t4073582105_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,System.Int64>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m2842832862_gshared (OperatorObserverBase_2_t159537945 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,System.Int64>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m3548631244_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m3548631244_gshared (OperatorObserverBase_2_t159537945 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m3548631244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2256767872 * L_0 = ((EmptyObserver_1_t2256767872_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,System.Object>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m883644582_gshared (OperatorObserverBase_2_t2444196779 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___observer0;
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject * L_1 = ___cancel1;
		__this->set_cancel_1(L_1);
		return;
	}
}
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t OperatorObserverBase_2_Dispose_m2664117508_MetadataUsageId;
extern "C"  void OperatorObserverBase_2_Dispose_m2664117508_gshared (OperatorObserverBase_2_t2444196779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OperatorObserverBase_2_Dispose_m2664117508_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t246459410 * L_0 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		il2cpp_codegen_memory_barrier();
		__this->set_observer_0(L_0);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_cancel_1();
		Il2CppObject * L_2 = InterlockedExchangeImpl<Il2CppObject *>((Il2CppObject **)L_1, (Il2CppObject *)NULL);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
	}

IL_0026:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
