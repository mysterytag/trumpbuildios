﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VkConnectorBase
struct VkConnectorBase_t2999232489;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// UniRx.ReactiveProperty`1<VKProfileInfo>
struct ReactiveProperty_1_t4081982704;
// System.Action`1<VKProfileInfo>
struct Action_1_t3622102371;
// AchievementDescription
struct AchievementDescription_t2323334509;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action`1<System.Collections.Generic.List`1<FriendInfo>>
struct Action_1_t1181882086;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// VKProfileInfo
struct VKProfileInfo_t3473649666;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AchievementDescription2323334509.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen3445990771.h"

// System.Void VkConnectorBase::.ctor()
extern "C"  void VkConnectorBase__ctor_m998700450 (VkConnectorBase_t2999232489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ReactiveProperty`1<System.Boolean> VkConnectorBase::get_Connected()
extern "C"  ReactiveProperty_1_t819338379 * VkConnectorBase_get_Connected_m1101394317 (VkConnectorBase_t2999232489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::set_Connected(UniRx.ReactiveProperty`1<System.Boolean>)
extern "C"  void VkConnectorBase_set_Connected_m2549287132 (VkConnectorBase_t2999232489 * __this, ReactiveProperty_1_t819338379 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ReactiveProperty`1<VKProfileInfo> VkConnectorBase::get_VkProfile()
extern "C"  ReactiveProperty_1_t4081982704 * VkConnectorBase_get_VkProfile_m4199548157 (VkConnectorBase_t2999232489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::set_VkProfile(UniRx.ReactiveProperty`1<VKProfileInfo>)
extern "C"  void VkConnectorBase_set_VkProfile_m948153434 (VkConnectorBase_t2999232489 * __this, ReactiveProperty_1_t4081982704 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::GetUserProfile(System.Action`1<VKProfileInfo>)
extern "C"  void VkConnectorBase_GetUserProfile_m1510635320 (VkConnectorBase_t2999232489 * __this, Action_1_t3622102371 * ___doWithProfile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::PostAchievement(AchievementDescription)
extern "C"  void VkConnectorBase_PostAchievement_m1134735970 (VkConnectorBase_t2999232489 * __this, AchievementDescription_t2323334509 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::WallPost(System.String,System.String)
extern "C"  void VkConnectorBase_WallPost_m826259026 (VkConnectorBase_t2999232489 * __this, String_t* ___text0, String_t* ___s1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::CheckLoginStatus()
extern "C"  void VkConnectorBase_CheckLoginStatus_m902504373 (VkConnectorBase_t2999232489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::Login()
extern "C"  void VkConnectorBase_Login_m2185368553 (VkConnectorBase_t2999232489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::JoinGroup(System.Action`1<System.Boolean>)
extern "C"  void VkConnectorBase_JoinGroup_m2374171038 (VkConnectorBase_t2999232489 * __this, Action_1_t359458046 * ___joinResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::GetFriends(System.Action`1<System.Collections.Generic.List`1<FriendInfo>>,System.Collections.Generic.List`1<System.String>)
extern "C"  void VkConnectorBase_GetFriends_m2595702841 (VkConnectorBase_t2999232489 * __this, Action_1_t1181882086 * ___withFriends0, List_1_t1765447871 * ___exceptIds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::InviteFriend(System.String,System.Action`1<System.Boolean>)
extern "C"  void VkConnectorBase_InviteFriend_m3924499046 (VkConnectorBase_t2999232489 * __this, String_t* ___friendId0, Action_1_t359458046 * ___succesfull1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::Logout()
extern "C"  void VkConnectorBase_Logout_m3327738412 (VkConnectorBase_t2999232489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VkConnectorBase::GetSafeKey(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  String_t* VkConnectorBase_GetSafeKey_m4245084118 (VkConnectorBase_t2999232489 * __this, Dictionary_2_t2474804324 * ___dict0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VKProfileInfo VkConnectorBase::ParseProfile(System.String)
extern "C"  VKProfileInfo_t3473649666 * VkConnectorBase_ParseProfile_m1658735931 (VkConnectorBase_t2999232489 * __this, String_t* ___profileResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::<VkConnectorBase>m__DC(System.Int64)
extern "C"  void VkConnectorBase_U3CVkConnectorBaseU3Em__DC_m3965162191 (VkConnectorBase_t2999232489 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VkConnectorBase::<GetUserProfile>m__DD(UniRx.Tuple`2<System.String,System.String>)
extern "C"  bool VkConnectorBase_U3CGetUserProfileU3Em__DD_m3818133603 (Il2CppObject * __this /* static, unused */, Tuple_2_t3445990771  ___tuple0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VkConnectorBase::<Login>m__DF(UniRx.Tuple`2<System.String,System.String>)
extern "C"  bool VkConnectorBase_U3CLoginU3Em__DF_m1731426600 (Il2CppObject * __this /* static, unused */, Tuple_2_t3445990771  ___tuple0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VkConnectorBase::<Login>m__E0(UniRx.Tuple`2<System.String,System.String>)
extern "C"  void VkConnectorBase_U3CLoginU3Em__E0_m610358245 (VkConnectorBase_t2999232489 * __this, Tuple_2_t3445990771  ___tuple0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VkConnectorBase::<JoinGroup>m__E1(UniRx.Tuple`2<System.String,System.String>)
extern "C"  bool VkConnectorBase_U3CJoinGroupU3Em__E1_m1469315774 (Il2CppObject * __this /* static, unused */, Tuple_2_t3445990771  ___tuple0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VkConnectorBase::<GetFriends>m__E3(UniRx.Tuple`2<System.String,System.String>)
extern "C"  bool VkConnectorBase_U3CGetFriendsU3Em__E3_m4277281384 (Il2CppObject * __this /* static, unused */, Tuple_2_t3445990771  ___tuple0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VkConnectorBase::<GetFriends>m__E4(UniRx.Tuple`2<System.String,System.String>)
extern "C"  bool VkConnectorBase_U3CGetFriendsU3Em__E4_m2576540905 (Il2CppObject * __this /* static, unused */, Tuple_2_t3445990771  ___tuple0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VkConnectorBase::<InviteFriend>m__E6(UniRx.Tuple`2<System.String,System.String>)
extern "C"  bool VkConnectorBase_U3CInviteFriendU3Em__E6_m870528467 (Il2CppObject * __this /* static, unused */, Tuple_2_t3445990771  ___tuple0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
