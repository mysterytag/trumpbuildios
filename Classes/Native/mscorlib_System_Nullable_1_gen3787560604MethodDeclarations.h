﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3787560604.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserSexualOrientatio901522696.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<SponsorPay.SPUserSexualOrientation>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2484952975_gshared (Nullable_1_t3787560604 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2484952975(__this, ___value0, method) ((  void (*) (Nullable_1_t3787560604 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2484952975_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserSexualOrientation>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2913300403_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2913300403(__this, method) ((  bool (*) (Nullable_1_t3787560604 *, const MethodInfo*))Nullable_1_get_HasValue_m2913300403_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserSexualOrientation>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3567078164_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3567078164(__this, method) ((  int32_t (*) (Nullable_1_t3787560604 *, const MethodInfo*))Nullable_1_get_Value_m3567078164_gshared)(__this, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserSexualOrientation>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2690421602_gshared (Nullable_1_t3787560604 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2690421602(__this, ___other0, method) ((  bool (*) (Nullable_1_t3787560604 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2690421602_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserSexualOrientation>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3835481501_gshared (Nullable_1_t3787560604 * __this, Nullable_1_t3787560604  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3835481501(__this, ___other0, method) ((  bool (*) (Nullable_1_t3787560604 *, Nullable_1_t3787560604 , const MethodInfo*))Nullable_1_Equals_m3835481501_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<SponsorPay.SPUserSexualOrientation>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4002844474_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m4002844474(__this, method) ((  int32_t (*) (Nullable_1_t3787560604 *, const MethodInfo*))Nullable_1_GetHashCode_m4002844474_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserSexualOrientation>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2399866735_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2399866735(__this, method) ((  int32_t (*) (Nullable_1_t3787560604 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2399866735_gshared)(__this, method)
// System.String System.Nullable`1<SponsorPay.SPUserSexualOrientation>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3130058142_gshared (Nullable_1_t3787560604 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3130058142(__this, method) ((  String_t* (*) (Nullable_1_t3787560604 *, const MethodInfo*))Nullable_1_ToString_m3130058142_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserSexualOrientation>::op_Implicit(T)
extern "C"  Nullable_1_t3787560604  Nullable_1_op_Implicit_m3489093208_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m3489093208(__this /* static, unused */, ___value0, method) ((  Nullable_1_t3787560604  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m3489093208_gshared)(__this /* static, unused */, ___value0, method)
