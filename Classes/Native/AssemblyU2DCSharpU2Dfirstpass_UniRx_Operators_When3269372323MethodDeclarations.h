﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>
struct WhenAllCollectionObserver_t3269372323;
// UniRx.Operators.WhenAllObservable`1/WhenAll<UniRx.Tuple`2<System.Object,System.Object>>
struct WhenAll_t491399631;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Operators.WhenAllObservable`1/WhenAll<T>,System.Int32)
extern "C"  void WhenAllCollectionObserver__ctor_m370205132_gshared (WhenAllCollectionObserver_t3269372323 * __this, WhenAll_t491399631 * ___parent0, int32_t ___index1, const MethodInfo* method);
#define WhenAllCollectionObserver__ctor_m370205132(__this, ___parent0, ___index1, method) ((  void (*) (WhenAllCollectionObserver_t3269372323 *, WhenAll_t491399631 *, int32_t, const MethodInfo*))WhenAllCollectionObserver__ctor_m370205132_gshared)(__this, ___parent0, ___index1, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void WhenAllCollectionObserver_OnNext_m2957292157_gshared (WhenAllCollectionObserver_t3269372323 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define WhenAllCollectionObserver_OnNext_m2957292157(__this, ___value0, method) ((  void (*) (WhenAllCollectionObserver_t3269372323 *, Tuple_2_t369261819 , const MethodInfo*))WhenAllCollectionObserver_OnNext_m2957292157_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void WhenAllCollectionObserver_OnError_m316027788_gshared (WhenAllCollectionObserver_t3269372323 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WhenAllCollectionObserver_OnError_m316027788(__this, ___error0, method) ((  void (*) (WhenAllCollectionObserver_t3269372323 *, Exception_t1967233988 *, const MethodInfo*))WhenAllCollectionObserver_OnError_m316027788_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void WhenAllCollectionObserver_OnCompleted_m378329311_gshared (WhenAllCollectionObserver_t3269372323 * __this, const MethodInfo* method);
#define WhenAllCollectionObserver_OnCompleted_m378329311(__this, method) ((  void (*) (WhenAllCollectionObserver_t3269372323 *, const MethodInfo*))WhenAllCollectionObserver_OnCompleted_m378329311_gshared)(__this, method)
