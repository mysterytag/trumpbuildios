﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.UUnit.UUnitTestSuite
struct UUnitTestSuite_t2558391287;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.UnityIncrementalTestRunner
struct  UnityIncrementalTestRunner_t1527238513  : public MonoBehaviour_t3012272455
{
public:
	// PlayFab.UUnit.UUnitTestSuite PlayFab.UUnit.UnityIncrementalTestRunner::suite
	UUnitTestSuite_t2558391287 * ___suite_2;

public:
	inline static int32_t get_offset_of_suite_2() { return static_cast<int32_t>(offsetof(UnityIncrementalTestRunner_t1527238513, ___suite_2)); }
	inline UUnitTestSuite_t2558391287 * get_suite_2() const { return ___suite_2; }
	inline UUnitTestSuite_t2558391287 ** get_address_of_suite_2() { return &___suite_2; }
	inline void set_suite_2(UUnitTestSuite_t2558391287 * value)
	{
		___suite_2 = value;
		Il2CppCodeGenWriteBarrier(&___suite_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
