﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tests
struct Tests_t80698881;

#include "codegen/il2cpp-codegen.h"

// System.Void Tests::.ctor()
extern "C"  void Tests__ctor_m131701898 (Tests_t80698881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tests::TestReactive()
extern "C"  void Tests_TestReactive_m1658118501 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tests::<TestReactive>m__1E5(System.Int32,System.Int32)
extern "C"  int32_t Tests_U3CTestReactiveU3Em__1E5_m1156697999 (Il2CppObject * __this /* static, unused */, int32_t ___v10, int32_t ___v21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tests::<TestReactive>m__1E6(System.Int32)
extern "C"  void Tests_U3CTestReactiveU3Em__1E6_m1100261079 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
