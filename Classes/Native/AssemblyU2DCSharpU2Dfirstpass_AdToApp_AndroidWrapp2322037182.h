﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29
struct  U3CInitializeSDKU3Ec__AnonStorey29_t2322037182  : public Il2CppObject
{
public:
	// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29::adContentType
	String_t* ___adContentType_0;
	// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29::sdkKey
	String_t* ___sdkKey_1;

public:
	inline static int32_t get_offset_of_adContentType_0() { return static_cast<int32_t>(offsetof(U3CInitializeSDKU3Ec__AnonStorey29_t2322037182, ___adContentType_0)); }
	inline String_t* get_adContentType_0() const { return ___adContentType_0; }
	inline String_t** get_address_of_adContentType_0() { return &___adContentType_0; }
	inline void set_adContentType_0(String_t* value)
	{
		___adContentType_0 = value;
		Il2CppCodeGenWriteBarrier(&___adContentType_0, value);
	}

	inline static int32_t get_offset_of_sdkKey_1() { return static_cast<int32_t>(offsetof(U3CInitializeSDKU3Ec__AnonStorey29_t2322037182, ___sdkKey_1)); }
	inline String_t* get_sdkKey_1() const { return ___sdkKey_1; }
	inline String_t** get_address_of_sdkKey_1() { return &___sdkKey_1; }
	inline void set_sdkKey_1(String_t* value)
	{
		___sdkKey_1 = value;
		Il2CppCodeGenWriteBarrier(&___sdkKey_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
