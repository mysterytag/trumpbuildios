﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ValueAdditive
struct ValueAdditive_t3122520125;

#include "codegen/il2cpp-codegen.h"

// System.Void ValueAdditive::.ctor()
extern "C"  void ValueAdditive__ctor_m3048764878 (ValueAdditive_t3122520125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValueAdditive::.ctor(System.Double)
extern "C"  void ValueAdditive__ctor_m661185588 (ValueAdditive_t3122520125 * __this, double ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ValueAdditive::Result()
extern "C"  double ValueAdditive_Result_m2255850384 (ValueAdditive_t3122520125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
