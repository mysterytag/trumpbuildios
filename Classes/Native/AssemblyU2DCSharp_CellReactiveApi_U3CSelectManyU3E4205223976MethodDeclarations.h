﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Int32,System.Int32,System.Int32>
struct U3CSelectManyU3Ec__AnonStorey119_3_t4205223976;
// ICell`1<System.Int32>
struct ICell_1_t104078468;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Int32,System.Int32,System.Int32>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey119_3__ctor_m3417166508_gshared (U3CSelectManyU3Ec__AnonStorey119_3_t4205223976 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey119_3__ctor_m3417166508(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey119_3_t4205223976 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey119_3__ctor_m3417166508_gshared)(__this, method)
// ICell`1<TR> CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Int32,System.Int32,System.Int32>::<>m__18E(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey119_3_U3CU3Em__18E_m3232065760_gshared (U3CSelectManyU3Ec__AnonStorey119_3_t4205223976 * __this, int32_t ___x0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey119_3_U3CU3Em__18E_m3232065760(__this, ___x0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey119_3_t4205223976 *, int32_t, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey119_3_U3CU3Em__18E_m3232065760_gshared)(__this, ___x0, method)
