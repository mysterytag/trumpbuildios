﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VkConnectorBase/<GetFriends>c__AnonStoreyDB/<GetFriends>c__AnonStoreyDA
struct U3CGetFriendsU3Ec__AnonStoreyDA_t472681729;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void VkConnectorBase/<GetFriends>c__AnonStoreyDB/<GetFriends>c__AnonStoreyDA::.ctor()
extern "C"  void U3CGetFriendsU3Ec__AnonStoreyDA__ctor_m2103982557 (U3CGetFriendsU3Ec__AnonStoreyDA_t472681729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VkConnectorBase/<GetFriends>c__AnonStoreyDB/<GetFriends>c__AnonStoreyDA::<>m__E9(System.String)
extern "C"  bool U3CGetFriendsU3Ec__AnonStoreyDA_U3CU3Em__E9_m268798740 (U3CGetFriendsU3Ec__AnonStoreyDA_t472681729 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VkConnectorBase/<GetFriends>c__AnonStoreyDB/<GetFriends>c__AnonStoreyDA::<>m__EA(System.String)
extern "C"  bool U3CGetFriendsU3Ec__AnonStoreyDA_U3CU3Em__EA_m479492620 (U3CGetFriendsU3Ec__AnonStoreyDA_t472681729 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
