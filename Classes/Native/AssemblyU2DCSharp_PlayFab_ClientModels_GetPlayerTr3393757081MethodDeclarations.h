﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayerTradesRequest
struct GetPlayerTradesRequest_t3393757081;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1403402234.h"

// System.Void PlayFab.ClientModels.GetPlayerTradesRequest::.ctor()
extern "C"  void GetPlayerTradesRequest__ctor_m1877697860 (GetPlayerTradesRequest_t3393757081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.TradeStatus> PlayFab.ClientModels.GetPlayerTradesRequest::get_StatusFilter()
extern "C"  Nullable_1_t1403402234  GetPlayerTradesRequest_get_StatusFilter_m1847635291 (GetPlayerTradesRequest_t3393757081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayerTradesRequest::set_StatusFilter(System.Nullable`1<PlayFab.ClientModels.TradeStatus>)
extern "C"  void GetPlayerTradesRequest_set_StatusFilter_m4170321200 (GetPlayerTradesRequest_t3393757081 * __this, Nullable_1_t1403402234  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
