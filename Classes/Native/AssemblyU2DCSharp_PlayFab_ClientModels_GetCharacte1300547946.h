﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord>
struct Dictionary_2_t1801537638;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetCharacterDataResult
struct  GetCharacterDataResult_t1300547946  : public PlayFabResultCommon_t379512675
{
public:
	// System.String PlayFab.ClientModels.GetCharacterDataResult::<CharacterId>k__BackingField
	String_t* ___U3CCharacterIdU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.UserDataRecord> PlayFab.ClientModels.GetCharacterDataResult::<Data>k__BackingField
	Dictionary_2_t1801537638 * ___U3CDataU3Ek__BackingField_3;
	// System.UInt32 PlayFab.ClientModels.GetCharacterDataResult::<DataVersion>k__BackingField
	uint32_t ___U3CDataVersionU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CCharacterIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetCharacterDataResult_t1300547946, ___U3CCharacterIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CCharacterIdU3Ek__BackingField_2() const { return ___U3CCharacterIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCharacterIdU3Ek__BackingField_2() { return &___U3CCharacterIdU3Ek__BackingField_2; }
	inline void set_U3CCharacterIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CCharacterIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetCharacterDataResult_t1300547946, ___U3CDataU3Ek__BackingField_3)); }
	inline Dictionary_2_t1801537638 * get_U3CDataU3Ek__BackingField_3() const { return ___U3CDataU3Ek__BackingField_3; }
	inline Dictionary_2_t1801537638 ** get_address_of_U3CDataU3Ek__BackingField_3() { return &___U3CDataU3Ek__BackingField_3; }
	inline void set_U3CDataU3Ek__BackingField_3(Dictionary_2_t1801537638 * value)
	{
		___U3CDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CDataVersionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetCharacterDataResult_t1300547946, ___U3CDataVersionU3Ek__BackingField_4)); }
	inline uint32_t get_U3CDataVersionU3Ek__BackingField_4() const { return ___U3CDataVersionU3Ek__BackingField_4; }
	inline uint32_t* get_address_of_U3CDataVersionU3Ek__BackingField_4() { return &___U3CDataVersionU3Ek__BackingField_4; }
	inline void set_U3CDataVersionU3Ek__BackingField_4(uint32_t value)
	{
		___U3CDataVersionU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
