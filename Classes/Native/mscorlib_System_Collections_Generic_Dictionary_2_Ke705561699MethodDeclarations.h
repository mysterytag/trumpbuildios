﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct Dictionary_2_t938533758;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke705561699.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3037708627_gshared (Enumerator_t705561699 * __this, Dictionary_2_t938533758 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3037708627(__this, ___host0, method) ((  void (*) (Enumerator_t705561699 *, Dictionary_2_t938533758 *, const MethodInfo*))Enumerator__ctor_m3037708627_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m540834552_gshared (Enumerator_t705561699 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m540834552(__this, method) ((  Il2CppObject * (*) (Enumerator_t705561699 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m540834552_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1780892034_gshared (Enumerator_t705561699 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1780892034(__this, method) ((  void (*) (Enumerator_t705561699 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1780892034_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m846680757_gshared (Enumerator_t705561699 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m846680757(__this, method) ((  void (*) (Enumerator_t705561699 *, const MethodInfo*))Enumerator_Dispose_m846680757_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2102329906_gshared (Enumerator_t705561699 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2102329906(__this, method) ((  bool (*) (Enumerator_t705561699 *, const MethodInfo*))Enumerator_MoveNext_m2102329906_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2376623910_gshared (Enumerator_t705561699 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2376623910(__this, method) ((  int32_t (*) (Enumerator_t705561699 *, const MethodInfo*))Enumerator_get_Current_m2376623910_gshared)(__this, method)
