﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// BioCollection`1<System.Object>
struct BioCollection_1_t694114648;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BioCollection`1/<InsertItem>c__AnonStoreyF7<System.Object>
struct  U3CInsertItemU3Ec__AnonStoreyF7_t871750507  : public Il2CppObject
{
public:
	// T BioCollection`1/<InsertItem>c__AnonStoreyF7::item
	Il2CppObject * ___item_0;
	// BioCollection`1<T> BioCollection`1/<InsertItem>c__AnonStoreyF7::<>f__this
	BioCollection_1_t694114648 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CInsertItemU3Ec__AnonStoreyF7_t871750507, ___item_0)); }
	inline Il2CppObject * get_item_0() const { return ___item_0; }
	inline Il2CppObject ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Il2CppObject * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier(&___item_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CInsertItemU3Ec__AnonStoreyF7_t871750507, ___U3CU3Ef__this_1)); }
	inline BioCollection_1_t694114648 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline BioCollection_1_t694114648 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(BioCollection_1_t694114648 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
