﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_7_gen289622050.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5,T6,T7)
extern "C"  void Tuple_7__ctor_m4244055942_gshared (Tuple_7_t289622050 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, Il2CppObject * ___item43, Il2CppObject * ___item54, Il2CppObject * ___item65, Il2CppObject * ___item76, const MethodInfo* method);
#define Tuple_7__ctor_m4244055942(__this, ___item10, ___item21, ___item32, ___item43, ___item54, ___item65, ___item76, method) ((  void (*) (Tuple_7_t289622050 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_7__ctor_m4244055942_gshared)(__this, ___item10, ___item21, ___item32, ___item43, ___item54, ___item65, ___item76, method)
// System.Int32 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_7_System_IComparable_CompareTo_m1721963210_gshared (Tuple_7_t289622050 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_7_System_IComparable_CompareTo_m1721963210(__this, ___obj0, method) ((  int32_t (*) (Tuple_7_t289622050 *, Il2CppObject *, const MethodInfo*))Tuple_7_System_IComparable_CompareTo_m1721963210_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_7_UniRx_IStructuralComparable_CompareTo_m2613814300_gshared (Tuple_7_t289622050 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_7_UniRx_IStructuralComparable_CompareTo_m2613814300(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_7_t289622050 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_7_UniRx_IStructuralComparable_CompareTo_m2613814300_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_7_UniRx_IStructuralEquatable_Equals_m2016919559_gshared (Tuple_7_t289622050 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_7_UniRx_IStructuralEquatable_Equals_m2016919559(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_7_t289622050 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_7_UniRx_IStructuralEquatable_Equals_m2016919559_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_7_UniRx_IStructuralEquatable_GetHashCode_m470376903_gshared (Tuple_7_t289622050 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_7_UniRx_IStructuralEquatable_GetHashCode_m470376903(__this, ___comparer0, method) ((  int32_t (*) (Tuple_7_t289622050 *, Il2CppObject *, const MethodInfo*))Tuple_7_UniRx_IStructuralEquatable_GetHashCode_m470376903_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_7_UniRx_ITuple_ToString_m2669204228_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_UniRx_ITuple_ToString_m2669204228(__this, method) ((  String_t* (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_UniRx_ITuple_ToString_m2669204228_gshared)(__this, method)
// T1 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_7_get_Item1_m3647201905_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_get_Item1_m3647201905(__this, method) ((  Il2CppObject * (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_get_Item1_m3647201905_gshared)(__this, method)
// T2 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_7_get_Item2_m1282466419_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_get_Item2_m1282466419(__this, method) ((  Il2CppObject * (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_get_Item2_m1282466419_gshared)(__this, method)
// T3 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_7_get_Item3_m3212698229_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_get_Item3_m3212698229(__this, method) ((  Il2CppObject * (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_get_Item3_m3212698229_gshared)(__this, method)
// T4 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item4()
extern "C"  Il2CppObject * Tuple_7_get_Item4_m847962743_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_get_Item4_m847962743(__this, method) ((  Il2CppObject * (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_get_Item4_m847962743_gshared)(__this, method)
// T5 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item5()
extern "C"  Il2CppObject * Tuple_7_get_Item5_m2778194553_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_get_Item5_m2778194553(__this, method) ((  Il2CppObject * (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_get_Item5_m2778194553_gshared)(__this, method)
// T6 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item6()
extern "C"  Il2CppObject * Tuple_7_get_Item6_m413459067_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_get_Item6_m413459067(__this, method) ((  Il2CppObject * (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_get_Item6_m413459067_gshared)(__this, method)
// T7 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Item7()
extern "C"  Il2CppObject * Tuple_7_get_Item7_m2343690877_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_get_Item7_m2343690877(__this, method) ((  Il2CppObject * (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_get_Item7_m2343690877_gshared)(__this, method)
// System.Boolean UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_7_Equals_m905609915_gshared (Tuple_7_t289622050 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_7_Equals_m905609915(__this, ___obj0, method) ((  bool (*) (Tuple_7_t289622050 *, Il2CppObject *, const MethodInfo*))Tuple_7_Equals_m905609915_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_7_GetHashCode_m838579167_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_GetHashCode_m838579167(__this, method) ((  int32_t (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_GetHashCode_m838579167_gshared)(__this, method)
// System.String UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* Tuple_7_ToString_m3889000877_gshared (Tuple_7_t289622050 * __this, const MethodInfo* method);
#define Tuple_7_ToString_m3889000877(__this, method) ((  String_t* (*) (Tuple_7_t289622050 *, const MethodInfo*))Tuple_7_ToString_m3889000877_gshared)(__this, method)
// System.Boolean UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Equals(UniRx.Tuple`7<T1,T2,T3,T4,T5,T6,T7>)
extern "C"  bool Tuple_7_Equals_m1296753488_gshared (Tuple_7_t289622050 * __this, Tuple_7_t289622050  ___other0, const MethodInfo* method);
#define Tuple_7_Equals_m1296753488(__this, ___other0, method) ((  bool (*) (Tuple_7_t289622050 *, Tuple_7_t289622050 , const MethodInfo*))Tuple_7_Equals_m1296753488_gshared)(__this, ___other0, method)
