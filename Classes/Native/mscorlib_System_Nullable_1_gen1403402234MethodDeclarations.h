﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1403402234.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeStatus2812331622.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.ClientModels.TradeStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3337663077_gshared (Nullable_1_t1403402234 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3337663077(__this, ___value0, method) ((  void (*) (Nullable_1_t1403402234 *, int32_t, const MethodInfo*))Nullable_1__ctor_m3337663077_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TradeStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3901768073_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3901768073(__this, method) ((  bool (*) (Nullable_1_t1403402234 *, const MethodInfo*))Nullable_1_get_HasValue_m3901768073_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.TradeStatus>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1520284734_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1520284734(__this, method) ((  int32_t (*) (Nullable_1_t1403402234 *, const MethodInfo*))Nullable_1_get_Value_m1520284734_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TradeStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3591382220_gshared (Nullable_1_t1403402234 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3591382220(__this, ___other0, method) ((  bool (*) (Nullable_1_t1403402234 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3591382220_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TradeStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m4247826675_gshared (Nullable_1_t1403402234 * __this, Nullable_1_t1403402234  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m4247826675(__this, ___other0, method) ((  bool (*) (Nullable_1_t1403402234 *, Nullable_1_t1403402234 , const MethodInfo*))Nullable_1_Equals_m4247826675_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.ClientModels.TradeStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4184369444_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m4184369444(__this, method) ((  int32_t (*) (Nullable_1_t1403402234 *, const MethodInfo*))Nullable_1_GetHashCode_m4184369444_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.TradeStatus>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2043457177_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2043457177(__this, method) ((  int32_t (*) (Nullable_1_t1403402234 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2043457177_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.ClientModels.TradeStatus>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2702235188_gshared (Nullable_1_t1403402234 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2702235188(__this, method) ((  String_t* (*) (Nullable_1_t1403402234 *, const MethodInfo*))Nullable_1_ToString_m2702235188_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.TradeStatus>::op_Implicit(T)
extern "C"  Nullable_1_t1403402234  Nullable_1_op_Implicit_m962681646_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m962681646(__this /* static, unused */, ___value0, method) ((  Nullable_1_t1403402234  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m962681646_gshared)(__this /* static, unused */, ___value0, method)
