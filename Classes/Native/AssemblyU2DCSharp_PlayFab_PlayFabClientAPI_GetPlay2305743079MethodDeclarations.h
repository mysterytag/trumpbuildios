﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromSteamIDsRequestCallback
struct GetPlayFabIDsFromSteamIDsRequestCallback_t2305743079;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest
struct GetPlayFabIDsFromSteamIDsRequest_t241435282;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID241435282.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromSteamIDsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromSteamIDsRequestCallback__ctor_m2441740886 (GetPlayFabIDsFromSteamIDsRequestCallback_t2305743079 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromSteamIDsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest,System.Object)
extern "C"  void GetPlayFabIDsFromSteamIDsRequestCallback_Invoke_m3481834239 (GetPlayFabIDsFromSteamIDsRequestCallback_t2305743079 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromSteamIDsRequest_t241435282 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromSteamIDsRequestCallback_t2305743079(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromSteamIDsRequest_t241435282 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromSteamIDsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromSteamIDsRequestCallback_BeginInvoke_m3815675636 (GetPlayFabIDsFromSteamIDsRequestCallback_t2305743079 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromSteamIDsRequest_t241435282 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromSteamIDsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromSteamIDsRequestCallback_EndInvoke_m2729971558 (GetPlayFabIDsFromSteamIDsRequestCallback_t2305743079 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
