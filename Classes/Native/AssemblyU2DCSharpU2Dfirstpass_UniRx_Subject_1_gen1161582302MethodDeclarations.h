﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.Bounds>
struct Subject_1_t1161582302;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Bounds>
struct IObserver_1_t1435546585;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"

// System.Void UniRx.Subject`1<UnityEngine.Bounds>::.ctor()
extern "C"  void Subject_1__ctor_m766065820_gshared (Subject_1_t1161582302 * __this, const MethodInfo* method);
#define Subject_1__ctor_m766065820(__this, method) ((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))Subject_1__ctor_m766065820_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Bounds>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2805980088_gshared (Subject_1_t1161582302 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m2805980088(__this, method) ((  bool (*) (Subject_1_t1161582302 *, const MethodInfo*))Subject_1_get_HasObservers_m2805980088_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Bounds>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m771984166_gshared (Subject_1_t1161582302 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m771984166(__this, method) ((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))Subject_1_OnCompleted_m771984166_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Bounds>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m3928535891_gshared (Subject_1_t1161582302 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m3928535891(__this, ___error0, method) ((  void (*) (Subject_1_t1161582302 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m3928535891_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Bounds>::OnNext(T)
extern "C"  void Subject_1_OnNext_m285581380_gshared (Subject_1_t1161582302 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m285581380(__this, ___value0, method) ((  void (*) (Subject_1_t1161582302 *, Bounds_t3518514978 , const MethodInfo*))Subject_1_OnNext_m285581380_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Bounds>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m708065009_gshared (Subject_1_t1161582302 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m708065009(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1161582302 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m708065009_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Bounds>::Dispose()
extern "C"  void Subject_1_Dispose_m1648128473_gshared (Subject_1_t1161582302 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1648128473(__this, method) ((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))Subject_1_Dispose_m1648128473_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Bounds>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1482371682_gshared (Subject_1_t1161582302 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1482371682(__this, method) ((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1482371682_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Bounds>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1585564335_gshared (Subject_1_t1161582302 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1585564335(__this, method) ((  bool (*) (Subject_1_t1161582302 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1585564335_gshared)(__this, method)
