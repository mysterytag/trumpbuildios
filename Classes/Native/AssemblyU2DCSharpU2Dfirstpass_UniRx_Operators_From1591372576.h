﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Action>
struct Action_1_t585976652;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable
struct  FromEventObservable_t1591372576  : public OperatorObservableBase_1_t1622431009
{
public:
	// System.Action`1<System.Action> UniRx.Operators.FromEventObservable::addHandler
	Action_1_t585976652 * ___addHandler_1;
	// System.Action`1<System.Action> UniRx.Operators.FromEventObservable::removeHandler
	Action_1_t585976652 * ___removeHandler_2;

public:
	inline static int32_t get_offset_of_addHandler_1() { return static_cast<int32_t>(offsetof(FromEventObservable_t1591372576, ___addHandler_1)); }
	inline Action_1_t585976652 * get_addHandler_1() const { return ___addHandler_1; }
	inline Action_1_t585976652 ** get_address_of_addHandler_1() { return &___addHandler_1; }
	inline void set_addHandler_1(Action_1_t585976652 * value)
	{
		___addHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___addHandler_1, value);
	}

	inline static int32_t get_offset_of_removeHandler_2() { return static_cast<int32_t>(offsetof(FromEventObservable_t1591372576, ___removeHandler_2)); }
	inline Action_1_t585976652 * get_removeHandler_2() const { return ___removeHandler_2; }
	inline Action_1_t585976652 ** get_address_of_removeHandler_2() { return &___removeHandler_2; }
	inline void set_removeHandler_2(Action_1_t585976652 * value)
	{
		___removeHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&___removeHandler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
