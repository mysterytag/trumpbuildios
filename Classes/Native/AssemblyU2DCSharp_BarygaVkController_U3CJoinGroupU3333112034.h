﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BarygaVkController
struct BarygaVkController_t3617163921;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaVkController/<JoinGroup>c__AnonStoreyDD
struct  U3CJoinGroupU3Ec__AnonStoreyDD_t3333112034  : public Il2CppObject
{
public:
	// System.Double BarygaVkController/<JoinGroup>c__AnonStoreyDD::reward
	double ___reward_0;
	// BarygaVkController BarygaVkController/<JoinGroup>c__AnonStoreyDD::<>f__this
	BarygaVkController_t3617163921 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_reward_0() { return static_cast<int32_t>(offsetof(U3CJoinGroupU3Ec__AnonStoreyDD_t3333112034, ___reward_0)); }
	inline double get_reward_0() const { return ___reward_0; }
	inline double* get_address_of_reward_0() { return &___reward_0; }
	inline void set_reward_0(double value)
	{
		___reward_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CJoinGroupU3Ec__AnonStoreyDD_t3333112034, ___U3CU3Ef__this_1)); }
	inline BarygaVkController_t3617163921 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline BarygaVkController_t3617163921 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(BarygaVkController_t3617163921 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
