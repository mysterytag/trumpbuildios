﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Object>
struct Select__t3173410592;
// UniRx.Operators.SelectObservable`2<System.Object,System.Object>
struct SelectObservable_2_t575849553;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Object>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select___ctor_m2951814999_gshared (Select__t3173410592 * __this, SelectObservable_2_t575849553 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select___ctor_m2951814999(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select__t3173410592 *, SelectObservable_2_t575849553 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select___ctor_m2951814999_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Object>::OnNext(T)
extern "C"  void Select__OnNext_m1557241361_gshared (Select__t3173410592 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Select__OnNext_m1557241361(__this, ___value0, method) ((  void (*) (Select__t3173410592 *, Il2CppObject *, const MethodInfo*))Select__OnNext_m1557241361_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Select__OnError_m3093395232_gshared (Select__t3173410592 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select__OnError_m3093395232(__this, ___error0, method) ((  void (*) (Select__t3173410592 *, Exception_t1967233988 *, const MethodInfo*))Select__OnError_m3093395232_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Object,System.Object>::OnCompleted()
extern "C"  void Select__OnCompleted_m2496780915_gshared (Select__t3173410592 * __this, const MethodInfo* method);
#define Select__OnCompleted_m2496780915(__this, method) ((  void (*) (Select__t3173410592 *, const MethodInfo*))Select__OnCompleted_m2496780915_gshared)(__this, method)
