﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>
struct SelectManyObservable_2_t2284173584;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<System.Object,UniRx.Unit>
struct  SelectManyEnumerableObserver_t3538509988  : public OperatorObserverBase_2_t2908947767
{
public:
	// UniRx.Operators.SelectManyObservable`2<TSource,TResult> UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver::parent
	SelectManyObservable_2_t2284173584 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectManyEnumerableObserver_t3538509988, ___parent_2)); }
	inline SelectManyObservable_2_t2284173584 * get_parent_2() const { return ___parent_2; }
	inline SelectManyObservable_2_t2284173584 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyObservable_2_t2284173584 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
