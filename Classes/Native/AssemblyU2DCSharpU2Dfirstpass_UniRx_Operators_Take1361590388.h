﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TakeUntilObservable`2<UniRx.Unit,UniRx.Unit>
struct TakeUntilObservable_2_t2928839418;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>
struct  TakeUntil_t1361590388  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.TakeUntilObservable`2<T,TOther> UniRx.Operators.TakeUntilObservable`2/TakeUntil::parent
	TakeUntilObservable_2_t2928839418 * ___parent_2;
	// System.Object UniRx.Operators.TakeUntilObservable`2/TakeUntil::gate
	Il2CppObject * ___gate_3;
	// System.Boolean UniRx.Operators.TakeUntilObservable`2/TakeUntil::open
	bool ___open_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(TakeUntil_t1361590388, ___parent_2)); }
	inline TakeUntilObservable_2_t2928839418 * get_parent_2() const { return ___parent_2; }
	inline TakeUntilObservable_2_t2928839418 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(TakeUntilObservable_2_t2928839418 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(TakeUntil_t1361590388, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_open_4() { return static_cast<int32_t>(offsetof(TakeUntil_t1361590388, ___open_4)); }
	inline bool get_open_4() const { return ___open_4; }
	inline bool* get_address_of_open_4() { return &___open_4; }
	inline void set_open_4(bool value)
	{
		___open_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
