﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<InternetCor>c__IteratorD
struct U3CInternetCorU3Ec__IteratorD_t2146188936;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<InternetCor>c__IteratorD::.ctor()
extern "C"  void U3CInternetCorU3Ec__IteratorD__ctor_m784738676 (U3CInternetCorU3Ec__IteratorD_t2146188936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<InternetCor>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInternetCorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1270229992 (U3CInternetCorU3Ec__IteratorD_t2146188936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<InternetCor>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInternetCorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1984757628 (U3CInternetCorU3Ec__IteratorD_t2146188936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<InternetCor>c__IteratorD::MoveNext()
extern "C"  bool U3CInternetCorU3Ec__IteratorD_MoveNext_m3602801640 (U3CInternetCorU3Ec__IteratorD_t2146188936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<InternetCor>c__IteratorD::Dispose()
extern "C"  void U3CInternetCorU3Ec__IteratorD_Dispose_m2412873905 (U3CInternetCorU3Ec__IteratorD_t2146188936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<InternetCor>c__IteratorD::Reset()
extern "C"  void U3CInternetCorU3Ec__IteratorD_Reset_m2726138913 (U3CInternetCorU3Ec__IteratorD_t2146188936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
