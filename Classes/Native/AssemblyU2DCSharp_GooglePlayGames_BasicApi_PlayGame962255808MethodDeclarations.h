﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder
struct Builder_t1895597948;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t716623425;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t4292352560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGame962255808.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGam1895597947.h"

// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration::.ctor(GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder)
extern "C"  void PlayGamesClientConfiguration__ctor_m67953019 (PlayGamesClientConfiguration_t962255808 * __this, Builder_t1895597948 * ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration::.cctor()
extern "C"  void PlayGamesClientConfiguration__cctor_m3831285928 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_EnableSavedGames()
extern "C"  bool PlayGamesClientConfiguration_get_EnableSavedGames_m2725869937 (PlayGamesClientConfiguration_t962255808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_EnableDeprecatedCloudSave()
extern "C"  bool PlayGamesClientConfiguration_get_EnableDeprecatedCloudSave_m97990762 (PlayGamesClientConfiguration_t962255808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_InvitationDelegate()
extern "C"  InvitationReceivedDelegate_t716623425 * PlayGamesClientConfiguration_get_InvitationDelegate_m2304162202 (PlayGamesClientConfiguration_t962255808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_MatchDelegate()
extern "C"  MatchDelegate_t4292352560 * PlayGamesClientConfiguration_get_MatchDelegate_m1536379219 (PlayGamesClientConfiguration_t962255808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
