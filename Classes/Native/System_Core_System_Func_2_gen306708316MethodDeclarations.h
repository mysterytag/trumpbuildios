﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"

// System.Void System.Func`2<SettingsButton,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3674514748(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t306708316 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m563515303_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<SettingsButton,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m4154134438(__this, ___arg10, method) ((  bool (*) (Func_2_t306708316 *, SettingsButton_t915256661 *, const MethodInfo*))Func_2_Invoke_m1882130143_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<SettingsButton,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m177990229(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t306708316 *, SettingsButton_t915256661 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1852288274_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<SettingsButton,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3066009582(__this, ___result0, method) ((  bool (*) (Func_2_t306708316 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1659014741_gshared)(__this, ___result0, method)
