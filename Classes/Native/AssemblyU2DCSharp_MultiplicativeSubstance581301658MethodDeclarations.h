﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MultiplicativeSubstance
struct MultiplicativeSubstance_t581301658;
// SubstanceBase`2/Intrusion<System.Single,System.Single>
struct Intrusion_t78896424;

#include "codegen/il2cpp-codegen.h"

// System.Void MultiplicativeSubstance::.ctor()
extern "C"  void MultiplicativeSubstance__ctor_m4202305297 (MultiplicativeSubstance_t581301658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiplicativeSubstance::.ctor(System.Single)
extern "C"  void MultiplicativeSubstance__ctor_m201371194 (MultiplicativeSubstance_t581301658 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MultiplicativeSubstance::Result()
extern "C"  float MultiplicativeSubstance_Result_m3752482084 (MultiplicativeSubstance_t581301658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MultiplicativeSubstance::<Result>m__1DB(System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>)
extern "C"  float MultiplicativeSubstance_U3CResultU3Em__1DB_m2994426172 (Il2CppObject * __this /* static, unused */, float ___current0, Intrusion_t78896424 * ___intr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
