﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SuspiciousVKWrapper
struct SuspiciousVKWrapper_t2556219713;

#include "codegen/il2cpp-codegen.h"

// System.Void SuspiciousVKWrapper::.ctor()
extern "C"  void SuspiciousVKWrapper__ctor_m2906790730 (SuspiciousVKWrapper_t2556219713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
