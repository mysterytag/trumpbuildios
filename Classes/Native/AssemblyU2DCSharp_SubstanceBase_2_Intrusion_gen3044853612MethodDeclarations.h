﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/Intrusion<System.Object,System.Object>
struct Intrusion_t3044853612;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/Intrusion<System.Object,System.Object>::.ctor()
extern "C"  void Intrusion__ctor_m2864964220_gshared (Intrusion_t3044853612 * __this, const MethodInfo* method);
#define Intrusion__ctor_m2864964220(__this, method) ((  void (*) (Intrusion_t3044853612 *, const MethodInfo*))Intrusion__ctor_m2864964220_gshared)(__this, method)
