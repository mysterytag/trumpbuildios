﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>
struct ReactivePropertyObserver_t3083397356;
// UniRx.ReactiveProperty`1<System.Int32>
struct ReactiveProperty_1_t3455747825;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m523330523_gshared (ReactivePropertyObserver_t3083397356 * __this, ReactiveProperty_1_t3455747825 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m523330523(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t3083397356 *, ReactiveProperty_1_t3455747825 *, const MethodInfo*))ReactivePropertyObserver__ctor_m523330523_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m4087905648_gshared (ReactivePropertyObserver_t3083397356 * __this, int32_t ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m4087905648(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t3083397356 *, int32_t, const MethodInfo*))ReactivePropertyObserver_OnNext_m4087905648_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3399727231_gshared (ReactivePropertyObserver_t3083397356 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m3399727231(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t3083397356 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m3399727231_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m475787858_gshared (ReactivePropertyObserver_t3083397356 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m475787858(__this, method) ((  void (*) (ReactivePropertyObserver_t3083397356 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m475787858_gshared)(__this, method)
