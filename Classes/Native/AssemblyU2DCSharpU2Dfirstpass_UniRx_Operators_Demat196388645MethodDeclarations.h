﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>
struct Dematerialize_t196388645;
// UniRx.Operators.DematerializeObservable`1<System.Object>
struct DematerializeObservable_1_t1297640382;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::.ctor(UniRx.Operators.DematerializeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Dematerialize__ctor_m3301817088_gshared (Dematerialize_t196388645 * __this, DematerializeObservable_1_t1297640382 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Dematerialize__ctor_m3301817088(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Dematerialize_t196388645 *, DematerializeObservable_1_t1297640382 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Dematerialize__ctor_m3301817088_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::Run()
extern "C"  Il2CppObject * Dematerialize_Run_m940466119_gshared (Dematerialize_t196388645 * __this, const MethodInfo* method);
#define Dematerialize_Run_m940466119(__this, method) ((  Il2CppObject * (*) (Dematerialize_t196388645 *, const MethodInfo*))Dematerialize_Run_m940466119_gshared)(__this, method)
// System.Void UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::OnNext(UniRx.Notification`1<T>)
extern "C"  void Dematerialize_OnNext_m646096099_gshared (Dematerialize_t196388645 * __this, Notification_1_t38356375 * ___value0, const MethodInfo* method);
#define Dematerialize_OnNext_m646096099(__this, ___value0, method) ((  void (*) (Dematerialize_t196388645 *, Notification_1_t38356375 *, const MethodInfo*))Dematerialize_OnNext_m646096099_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::OnError(System.Exception)
extern "C"  void Dematerialize_OnError_m3809367600_gshared (Dematerialize_t196388645 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Dematerialize_OnError_m3809367600(__this, ___error0, method) ((  void (*) (Dematerialize_t196388645 *, Exception_t1967233988 *, const MethodInfo*))Dematerialize_OnError_m3809367600_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DematerializeObservable`1/Dematerialize<System.Object>::OnCompleted()
extern "C"  void Dematerialize_OnCompleted_m218681731_gshared (Dematerialize_t196388645 * __this, const MethodInfo* method);
#define Dematerialize_OnCompleted_m218681731(__this, method) ((  void (*) (Dematerialize_t196388645 *, const MethodInfo*))Dematerialize_OnCompleted_m218681731_gshared)(__this, method)
