﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1848703245;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AchievementController/AchievementsInfo
struct  AchievementsInfo_t1267089938  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> AchievementController/AchievementsInfo::achieved
	Dictionary_2_t1848703245 * ___achieved_0;

public:
	inline static int32_t get_offset_of_achieved_0() { return static_cast<int32_t>(offsetof(AchievementsInfo_t1267089938, ___achieved_0)); }
	inline Dictionary_2_t1848703245 * get_achieved_0() const { return ___achieved_0; }
	inline Dictionary_2_t1848703245 ** get_address_of_achieved_0() { return &___achieved_0; }
	inline void set_achieved_0(Dictionary_2_t1848703245 * value)
	{
		___achieved_0 = value;
		Il2CppCodeGenWriteBarrier(&___achieved_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
