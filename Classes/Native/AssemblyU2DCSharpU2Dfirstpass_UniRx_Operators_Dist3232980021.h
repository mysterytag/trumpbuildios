﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3535795091;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DistinctObservable`1/Distinct<System.Object>
struct  Distinct_t3232980021  : public OperatorObserverBase_2_t1187768149
{
public:
	// System.Collections.Generic.HashSet`1<T> UniRx.Operators.DistinctObservable`1/Distinct::hashSet
	HashSet_1_t3535795091 * ___hashSet_2;

public:
	inline static int32_t get_offset_of_hashSet_2() { return static_cast<int32_t>(offsetof(Distinct_t3232980021, ___hashSet_2)); }
	inline HashSet_1_t3535795091 * get_hashSet_2() const { return ___hashSet_2; }
	inline HashSet_1_t3535795091 ** get_address_of_hashSet_2() { return &___hashSet_2; }
	inline void set_hashSet_2(HashSet_1_t3535795091 * value)
	{
		___hashSet_2 = value;
		Il2CppCodeGenWriteBarrier(&___hashSet_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
