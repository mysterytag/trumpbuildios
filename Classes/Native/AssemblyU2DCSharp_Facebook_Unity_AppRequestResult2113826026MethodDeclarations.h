﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AppRequestResult
struct AppRequestResult_t2113826026;
// Facebook.Unity.ResultContainer
struct ResultContainer_t79372963;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3840643258;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ResultContainer79372963.h"
#include "mscorlib_System_String968488902.h"

// System.Void Facebook.Unity.AppRequestResult::.ctor(Facebook.Unity.ResultContainer)
extern "C"  void AppRequestResult__ctor_m3054503708 (AppRequestResult_t2113826026 * __this, ResultContainer_t79372963 * ___resultContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppRequestResult::get_RequestID()
extern "C"  String_t* AppRequestResult_get_RequestID_m427131809 (AppRequestResult_t2113826026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppRequestResult::set_RequestID(System.String)
extern "C"  void AppRequestResult_set_RequestID_m1737709752 (AppRequestResult_t2113826026 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AppRequestResult::get_To()
extern "C"  Il2CppObject* AppRequestResult_get_To_m4039872525 (AppRequestResult_t2113826026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppRequestResult::set_To(System.Collections.Generic.IEnumerable`1<System.String>)
extern "C"  void AppRequestResult_set_To_m1640121278 (AppRequestResult_t2113826026 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppRequestResult::ToString()
extern "C"  String_t* AppRequestResult_ToString_m3080033390 (AppRequestResult_t2113826026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
