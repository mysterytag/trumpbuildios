﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t2938797301;
// UnityEngine.Events.UnityAction
struct UnityAction_t909267611;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergEngineTools/UnityActionDisposable
struct  UnityActionDisposable_t208063915  : public Il2CppObject
{
public:
	// UnityEngine.Events.UnityEvent ZergEngineTools/UnityActionDisposable::e
	UnityEvent_t2938797301 * ___e_0;
	// UnityEngine.Events.UnityAction ZergEngineTools/UnityActionDisposable::action
	UnityAction_t909267611 * ___action_1;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(UnityActionDisposable_t208063915, ___e_0)); }
	inline UnityEvent_t2938797301 * get_e_0() const { return ___e_0; }
	inline UnityEvent_t2938797301 ** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(UnityEvent_t2938797301 * value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier(&___e_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(UnityActionDisposable_t208063915, ___action_1)); }
	inline UnityAction_t909267611 * get_action_1() const { return ___action_1; }
	inline UnityAction_t909267611 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(UnityAction_t909267611 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
