﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2877437650MethodDeclarations.h"

// System.Void System.Func`2<System.Int64,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2646909148(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3008820132 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m165973854_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Int64,System.String>::Invoke(T)
#define Func_2_Invoke_m4185182618(__this, ___arg10, method) ((  String_t* (*) (Func_2_t3008820132 *, int64_t, const MethodInfo*))Func_2_Invoke_m794738632_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Int64,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1934365005(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3008820132 *, int64_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4176984443_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Int64,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2410567290(__this, ___result0, method) ((  String_t* (*) (Func_2_t3008820132 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3572281612_gshared)(__this, ___result0, method)
