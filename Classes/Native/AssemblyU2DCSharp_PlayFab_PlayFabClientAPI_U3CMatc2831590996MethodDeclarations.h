﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<Matchmake>c__AnonStoreyB6
struct U3CMatchmakeU3Ec__AnonStoreyB6_t2831590996;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<Matchmake>c__AnonStoreyB6::.ctor()
extern "C"  void U3CMatchmakeU3Ec__AnonStoreyB6__ctor_m3969659775 (U3CMatchmakeU3Ec__AnonStoreyB6_t2831590996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<Matchmake>c__AnonStoreyB6::<>m__A3(PlayFab.CallRequestContainer)
extern "C"  void U3CMatchmakeU3Ec__AnonStoreyB6_U3CU3Em__A3_m2262267567 (U3CMatchmakeU3Ec__AnonStoreyB6_t2831590996 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
