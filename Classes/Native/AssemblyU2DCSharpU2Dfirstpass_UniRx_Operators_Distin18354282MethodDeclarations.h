﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>
struct DistinctUntilChanged_t18354282;
// UniRx.Operators.DistinctUntilChangedObservable`1<System.Int32>
struct DistinctUntilChangedObservable_1_t1541420035;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>::.ctor(UniRx.Operators.DistinctUntilChangedObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DistinctUntilChanged__ctor_m647915949_gshared (DistinctUntilChanged_t18354282 * __this, DistinctUntilChangedObservable_1_t1541420035 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DistinctUntilChanged__ctor_m647915949(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DistinctUntilChanged_t18354282 *, DistinctUntilChangedObservable_1_t1541420035 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DistinctUntilChanged__ctor_m647915949_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>::OnNext(T)
extern "C"  void DistinctUntilChanged_OnNext_m1535557800_gshared (DistinctUntilChanged_t18354282 * __this, int32_t ___value0, const MethodInfo* method);
#define DistinctUntilChanged_OnNext_m1535557800(__this, ___value0, method) ((  void (*) (DistinctUntilChanged_t18354282 *, int32_t, const MethodInfo*))DistinctUntilChanged_OnNext_m1535557800_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>::OnError(System.Exception)
extern "C"  void DistinctUntilChanged_OnError_m3502541751_gshared (DistinctUntilChanged_t18354282 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DistinctUntilChanged_OnError_m3502541751(__this, ___error0, method) ((  void (*) (DistinctUntilChanged_t18354282 *, Exception_t1967233988 *, const MethodInfo*))DistinctUntilChanged_OnError_m3502541751_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>::OnCompleted()
extern "C"  void DistinctUntilChanged_OnCompleted_m410376586_gshared (DistinctUntilChanged_t18354282 * __this, const MethodInfo* method);
#define DistinctUntilChanged_OnCompleted_m410376586(__this, method) ((  void (*) (DistinctUntilChanged_t18354282 *, const MethodInfo*))DistinctUntilChanged_OnCompleted_m410376586_gshared)(__this, method)
