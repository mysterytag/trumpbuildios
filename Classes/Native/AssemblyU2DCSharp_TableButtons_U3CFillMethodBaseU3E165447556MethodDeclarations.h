﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<FillMethodBase>c__AnonStorey15B
struct U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556;
// <>__AnonType5`2<System.DateTime,System.Int64>
struct U3CU3E__AnonType5_2_t3897323094;
// <>__AnonType6`2<System.Double,System.Int32>
struct U3CU3E__AnonType6_2_t3631555212;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<FillMethodBase>c__AnonStorey15B::.ctor()
extern "C"  void U3CFillMethodBaseU3Ec__AnonStorey15B__ctor_m682736459 (U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<FillMethodBase>c__AnonStorey15B::<>m__234(<>__AnonType5`2<System.DateTime,System.Int64>)
extern "C"  void U3CFillMethodBaseU3Ec__AnonStorey15B_U3CU3Em__234_m3503884688 (U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556 * __this, U3CU3E__AnonType5_2_t3897323094 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<FillMethodBase>c__AnonStorey15B::<>m__235(System.Int32)
extern "C"  void U3CFillMethodBaseU3Ec__AnonStorey15B_U3CU3Em__235_m3376313459 (U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<FillMethodBase>c__AnonStorey15B::<>m__237(<>__AnonType6`2<System.Double,System.Int32>)
extern "C"  void U3CFillMethodBaseU3Ec__AnonStorey15B_U3CU3Em__237_m2420181047 (U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556 * __this, U3CU3E__AnonType6_2_t3631555212 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TableButtons/<FillMethodBase>c__AnonStorey15B::<>m__238(UniRx.Unit)
extern "C"  bool U3CFillMethodBaseU3Ec__AnonStorey15B_U3CU3Em__238_m3645066581 (U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<FillMethodBase>c__AnonStorey15B::<>m__239(UniRx.Unit)
extern "C"  void U3CFillMethodBaseU3Ec__AnonStorey15B_U3CU3Em__239_m1998688010 (U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
