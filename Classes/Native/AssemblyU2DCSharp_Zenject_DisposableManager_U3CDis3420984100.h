﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DisposableManager/<DisposableManager>c__AnonStorey18E
struct  U3CDisposableManagerU3Ec__AnonStorey18E_t3420984100  : public Il2CppObject
{
public:
	// System.IDisposable Zenject.DisposableManager/<DisposableManager>c__AnonStorey18E::disposable
	Il2CppObject * ___disposable_0;

public:
	inline static int32_t get_offset_of_disposable_0() { return static_cast<int32_t>(offsetof(U3CDisposableManagerU3Ec__AnonStorey18E_t3420984100, ___disposable_0)); }
	inline Il2CppObject * get_disposable_0() const { return ___disposable_0; }
	inline Il2CppObject ** get_address_of_disposable_0() { return &___disposable_0; }
	inline void set_disposable_0(Il2CppObject * value)
	{
		___disposable_0 = value;
		Il2CppCodeGenWriteBarrier(&___disposable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
