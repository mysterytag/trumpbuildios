﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.PresenterBase
struct PresenterBase_t3597493419;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.PresenterBase::.ctor()
extern "C"  void PresenterBase__ctor_m4005750934 (PresenterBase_t3597493419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.PresenterBase::BeforeInitialize(UniRx.Unit)
extern "C"  void PresenterBase_BeforeInitialize_m1220651073 (PresenterBase_t3597493419 * __this, Unit_t2558286038  ___argument0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.PresenterBase::Initialize(UniRx.Unit)
extern "C"  void PresenterBase_Initialize_m462658466 (PresenterBase_t3597493419 * __this, Unit_t2558286038  ___argument0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.PresenterBase::ForceInitialize()
extern "C"  void PresenterBase_ForceInitialize_m97601551 (PresenterBase_t3597493419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
