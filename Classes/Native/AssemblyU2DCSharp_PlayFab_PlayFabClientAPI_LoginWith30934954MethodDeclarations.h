﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithKongregateRequestCallback
struct LoginWithKongregateRequestCallback_t30934954;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithKongregateRequest
struct LoginWithKongregateRequest_t366847061;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithKon366847061.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithKongregateRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithKongregateRequestCallback__ctor_m769617817 (LoginWithKongregateRequestCallback_t30934954 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithKongregateRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithKongregateRequest,System.Object)
extern "C"  void LoginWithKongregateRequestCallback_Invoke_m3152104543 (LoginWithKongregateRequestCallback_t30934954 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithKongregateRequest_t366847061 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithKongregateRequestCallback_t30934954(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithKongregateRequest_t366847061 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithKongregateRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithKongregateRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithKongregateRequestCallback_BeginInvoke_m1179895610 (LoginWithKongregateRequestCallback_t30934954 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithKongregateRequest_t366847061 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithKongregateRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithKongregateRequestCallback_EndInvoke_m1510250025 (LoginWithKongregateRequestCallback_t30934954 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
