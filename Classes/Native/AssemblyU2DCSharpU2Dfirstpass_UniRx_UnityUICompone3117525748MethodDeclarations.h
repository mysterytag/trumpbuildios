﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA9
struct U3COnValueChangedAsObservableU3Ec__AnonStoreyA9_t3117525748;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA9::.ctor()
extern "C"  void U3COnValueChangedAsObservableU3Ec__AnonStoreyA9__ctor_m1988805194 (U3COnValueChangedAsObservableU3Ec__AnonStoreyA9_t3117525748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA9::<>m__E7(UniRx.IObserver`1<UnityEngine.Vector2>)
extern "C"  Il2CppObject * U3COnValueChangedAsObservableU3Ec__AnonStoreyA9_U3CU3Em__E7_m2195785440 (U3COnValueChangedAsObservableU3Ec__AnonStoreyA9_t3117525748 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
