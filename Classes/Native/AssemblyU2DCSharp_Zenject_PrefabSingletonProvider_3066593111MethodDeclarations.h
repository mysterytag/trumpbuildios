﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E
struct U3CValidateBindingU3Ec__Iterator4E_t3066593111;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::.ctor()
extern "C"  void U3CValidateBindingU3Ec__Iterator4E__ctor_m1648110574 (U3CValidateBindingU3Ec__Iterator4E_t3066593111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ZenjectResolveException Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::System.Collections.Generic.IEnumerator<Zenject.ZenjectResolveException>.get_Current()
extern "C"  ZenjectResolveException_t1201052999 * U3CValidateBindingU3Ec__Iterator4E_System_Collections_Generic_IEnumeratorU3CZenject_ZenjectResolveExceptionU3E_get_Current_m482321591 (U3CValidateBindingU3Ec__Iterator4E_t3066593111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateBindingU3Ec__Iterator4E_System_Collections_IEnumerator_get_Current_m1766329218 (U3CValidateBindingU3Ec__Iterator4E_t3066593111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CValidateBindingU3Ec__Iterator4E_System_Collections_IEnumerable_GetEnumerator_m3165783875 (U3CValidateBindingU3Ec__Iterator4E_t3066593111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::System.Collections.Generic.IEnumerable<Zenject.ZenjectResolveException>.GetEnumerator()
extern "C"  Il2CppObject* U3CValidateBindingU3Ec__Iterator4E_System_Collections_Generic_IEnumerableU3CZenject_ZenjectResolveExceptionU3E_GetEnumerator_m56003394 (U3CValidateBindingU3Ec__Iterator4E_t3066593111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::MoveNext()
extern "C"  bool U3CValidateBindingU3Ec__Iterator4E_MoveNext_m1641240622 (U3CValidateBindingU3Ec__Iterator4E_t3066593111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::Dispose()
extern "C"  void U3CValidateBindingU3Ec__Iterator4E_Dispose_m3184579755 (U3CValidateBindingU3Ec__Iterator4E_t3066593111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::Reset()
extern "C"  void U3CValidateBindingU3Ec__Iterator4E_Reset_m3589510811 (U3CValidateBindingU3Ec__Iterator4E_t3066593111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
