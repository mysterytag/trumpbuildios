﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.LazyTask`1<System.Int32>
struct LazyTask_1_t4247718180;
// System.Object
struct Il2CppObject;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8
struct  U3CLazyTaskTestU3Ec__Iterator8_t1612341822  : public Il2CppObject
{
public:
	// UniRx.LazyTask`1<System.Int32> UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::<task>__0
	LazyTask_1_t4247718180 * ___U3CtaskU3E__0_0;
	// System.Int32 UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::$PC
	int32_t ___U24PC_1;
	// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::$current
	Il2CppObject * ___U24current_2;

public:
	inline static int32_t get_offset_of_U3CtaskU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLazyTaskTestU3Ec__Iterator8_t1612341822, ___U3CtaskU3E__0_0)); }
	inline LazyTask_1_t4247718180 * get_U3CtaskU3E__0_0() const { return ___U3CtaskU3E__0_0; }
	inline LazyTask_1_t4247718180 ** get_address_of_U3CtaskU3E__0_0() { return &___U3CtaskU3E__0_0; }
	inline void set_U3CtaskU3E__0_0(LazyTask_1_t4247718180 * value)
	{
		___U3CtaskU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtaskU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CLazyTaskTestU3Ec__Iterator8_t1612341822, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLazyTaskTestU3Ec__Iterator8_t1612341822, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}
};

struct U3CLazyTaskTestU3Ec__Iterator8_t1612341822_StaticFields
{
public:
	// System.Func`1<System.Int32> UniRx.Examples.Sample06_ConvertToCoroutine/<LazyTaskTest>c__Iterator8::<>f__am$cache3
	Func_1_t3990196034 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(U3CLazyTaskTestU3Ec__Iterator8_t1612341822_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_1_t3990196034 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_1_t3990196034 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_1_t3990196034 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
