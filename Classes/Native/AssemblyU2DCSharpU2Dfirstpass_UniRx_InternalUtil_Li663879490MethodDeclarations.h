﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct ListObserver_1_t663879490;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ImmutableList_1_t3641465221;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3190591379_gshared (ListObserver_1_t663879490 * __this, ImmutableList_1_t3641465221 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m3190591379(__this, ___observers0, method) ((  void (*) (ListObserver_1_t663879490 *, ImmutableList_1_t3641465221 *, const MethodInfo*))ListObserver_1__ctor_m3190591379_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2618954891_gshared (ListObserver_1_t663879490 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m2618954891(__this, method) ((  void (*) (ListObserver_1_t663879490 *, const MethodInfo*))ListObserver_1_OnCompleted_m2618954891_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m527125816_gshared (ListObserver_1_t663879490 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m527125816(__this, ___error0, method) ((  void (*) (ListObserver_1_t663879490 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m527125816_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m716874793_gshared (ListObserver_1_t663879490 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m716874793(__this, ___value0, method) ((  void (*) (ListObserver_1_t663879490 *, Tuple_2_t369261819 , const MethodInfo*))ListObserver_1_OnNext_m716874793_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2701762761_gshared (ListObserver_1_t663879490 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2701762761(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t663879490 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2701762761_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m960731618_gshared (ListObserver_1_t663879490 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m960731618(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t663879490 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m960731618_gshared)(__this, ___observer0, method)
