﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetUserCombinedInfoRequest
struct  GetUserCombinedInfoRequest_t3431761899  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::<PlayFabId>k__BackingField
	String_t* ___U3CPlayFabIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::<Username>k__BackingField
	String_t* ___U3CUsernameU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::<Email>k__BackingField
	String_t* ___U3CEmailU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::<TitleDisplayName>k__BackingField
	String_t* ___U3CTitleDisplayNameU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::<GetAccountInfo>k__BackingField
	Nullable_1_t3097043249  ___U3CGetAccountInfoU3Ek__BackingField_4;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::<GetInventory>k__BackingField
	Nullable_1_t3097043249  ___U3CGetInventoryU3Ek__BackingField_5;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::<GetVirtualCurrency>k__BackingField
	Nullable_1_t3097043249  ___U3CGetVirtualCurrencyU3Ek__BackingField_6;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::<GetUserData>k__BackingField
	Nullable_1_t3097043249  ___U3CGetUserDataU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetUserCombinedInfoRequest::<UserDataKeys>k__BackingField
	List_1_t1765447871 * ___U3CUserDataKeysU3Ek__BackingField_8;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::<GetReadOnlyData>k__BackingField
	Nullable_1_t3097043249  ___U3CGetReadOnlyDataU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetUserCombinedInfoRequest::<ReadOnlyDataKeys>k__BackingField
	List_1_t1765447871 * ___U3CReadOnlyDataKeysU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CPlayFabIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CPlayFabIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CPlayFabIdU3Ek__BackingField_0() const { return ___U3CPlayFabIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPlayFabIdU3Ek__BackingField_0() { return &___U3CPlayFabIdU3Ek__BackingField_0; }
	inline void set_U3CPlayFabIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CPlayFabIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayFabIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CUsernameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CUsernameU3Ek__BackingField_1)); }
	inline String_t* get_U3CUsernameU3Ek__BackingField_1() const { return ___U3CUsernameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUsernameU3Ek__BackingField_1() { return &___U3CUsernameU3Ek__BackingField_1; }
	inline void set_U3CUsernameU3Ek__BackingField_1(String_t* value)
	{
		___U3CUsernameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUsernameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CEmailU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CEmailU3Ek__BackingField_2)); }
	inline String_t* get_U3CEmailU3Ek__BackingField_2() const { return ___U3CEmailU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CEmailU3Ek__BackingField_2() { return &___U3CEmailU3Ek__BackingField_2; }
	inline void set_U3CEmailU3Ek__BackingField_2(String_t* value)
	{
		___U3CEmailU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEmailU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTitleDisplayNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CTitleDisplayNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CTitleDisplayNameU3Ek__BackingField_3() const { return ___U3CTitleDisplayNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTitleDisplayNameU3Ek__BackingField_3() { return &___U3CTitleDisplayNameU3Ek__BackingField_3; }
	inline void set_U3CTitleDisplayNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CTitleDisplayNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTitleDisplayNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CGetAccountInfoU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CGetAccountInfoU3Ek__BackingField_4)); }
	inline Nullable_1_t3097043249  get_U3CGetAccountInfoU3Ek__BackingField_4() const { return ___U3CGetAccountInfoU3Ek__BackingField_4; }
	inline Nullable_1_t3097043249 * get_address_of_U3CGetAccountInfoU3Ek__BackingField_4() { return &___U3CGetAccountInfoU3Ek__BackingField_4; }
	inline void set_U3CGetAccountInfoU3Ek__BackingField_4(Nullable_1_t3097043249  value)
	{
		___U3CGetAccountInfoU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CGetInventoryU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CGetInventoryU3Ek__BackingField_5)); }
	inline Nullable_1_t3097043249  get_U3CGetInventoryU3Ek__BackingField_5() const { return ___U3CGetInventoryU3Ek__BackingField_5; }
	inline Nullable_1_t3097043249 * get_address_of_U3CGetInventoryU3Ek__BackingField_5() { return &___U3CGetInventoryU3Ek__BackingField_5; }
	inline void set_U3CGetInventoryU3Ek__BackingField_5(Nullable_1_t3097043249  value)
	{
		___U3CGetInventoryU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CGetVirtualCurrencyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CGetVirtualCurrencyU3Ek__BackingField_6)); }
	inline Nullable_1_t3097043249  get_U3CGetVirtualCurrencyU3Ek__BackingField_6() const { return ___U3CGetVirtualCurrencyU3Ek__BackingField_6; }
	inline Nullable_1_t3097043249 * get_address_of_U3CGetVirtualCurrencyU3Ek__BackingField_6() { return &___U3CGetVirtualCurrencyU3Ek__BackingField_6; }
	inline void set_U3CGetVirtualCurrencyU3Ek__BackingField_6(Nullable_1_t3097043249  value)
	{
		___U3CGetVirtualCurrencyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CGetUserDataU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CGetUserDataU3Ek__BackingField_7)); }
	inline Nullable_1_t3097043249  get_U3CGetUserDataU3Ek__BackingField_7() const { return ___U3CGetUserDataU3Ek__BackingField_7; }
	inline Nullable_1_t3097043249 * get_address_of_U3CGetUserDataU3Ek__BackingField_7() { return &___U3CGetUserDataU3Ek__BackingField_7; }
	inline void set_U3CGetUserDataU3Ek__BackingField_7(Nullable_1_t3097043249  value)
	{
		___U3CGetUserDataU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUserDataKeysU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CUserDataKeysU3Ek__BackingField_8)); }
	inline List_1_t1765447871 * get_U3CUserDataKeysU3Ek__BackingField_8() const { return ___U3CUserDataKeysU3Ek__BackingField_8; }
	inline List_1_t1765447871 ** get_address_of_U3CUserDataKeysU3Ek__BackingField_8() { return &___U3CUserDataKeysU3Ek__BackingField_8; }
	inline void set_U3CUserDataKeysU3Ek__BackingField_8(List_1_t1765447871 * value)
	{
		___U3CUserDataKeysU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserDataKeysU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CGetReadOnlyDataU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CGetReadOnlyDataU3Ek__BackingField_9)); }
	inline Nullable_1_t3097043249  get_U3CGetReadOnlyDataU3Ek__BackingField_9() const { return ___U3CGetReadOnlyDataU3Ek__BackingField_9; }
	inline Nullable_1_t3097043249 * get_address_of_U3CGetReadOnlyDataU3Ek__BackingField_9() { return &___U3CGetReadOnlyDataU3Ek__BackingField_9; }
	inline void set_U3CGetReadOnlyDataU3Ek__BackingField_9(Nullable_1_t3097043249  value)
	{
		___U3CGetReadOnlyDataU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CReadOnlyDataKeysU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GetUserCombinedInfoRequest_t3431761899, ___U3CReadOnlyDataKeysU3Ek__BackingField_10)); }
	inline List_1_t1765447871 * get_U3CReadOnlyDataKeysU3Ek__BackingField_10() const { return ___U3CReadOnlyDataKeysU3Ek__BackingField_10; }
	inline List_1_t1765447871 ** get_address_of_U3CReadOnlyDataKeysU3Ek__BackingField_10() { return &___U3CReadOnlyDataKeysU3Ek__BackingField_10; }
	inline void set_U3CReadOnlyDataKeysU3Ek__BackingField_10(List_1_t1765447871 * value)
	{
		___U3CReadOnlyDataKeysU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReadOnlyDataKeysU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
