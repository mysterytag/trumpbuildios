﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CellUtils.CellJoinDisposable`1<System.Object>
struct CellJoinDisposable_1_t3736493403;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// CellReactiveApi/<Merge>c__AnonStorey112`3<System.DateTime,System.Int64,System.Object>
struct U3CMergeU3Ec__AnonStorey112_3_t3245937911;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.DateTime,System.Int64,System.Object>
struct  U3CMergeU3Ec__AnonStorey113_3_t2913556904  : public Il2CppObject
{
public:
	// CellUtils.CellJoinDisposable`1<T3> CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3::disp
	CellJoinDisposable_1_t3736493403 * ___disp_0;
	// System.Action`1<T3> CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3::reaction
	Action_1_t985559125 * ___reaction_1;
	// CellReactiveApi/<Merge>c__AnonStorey112`3<T,T2,T3> CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3::<>f__ref$274
	U3CMergeU3Ec__AnonStorey112_3_t3245937911 * ___U3CU3Ef__refU24274_2;

public:
	inline static int32_t get_offset_of_disp_0() { return static_cast<int32_t>(offsetof(U3CMergeU3Ec__AnonStorey113_3_t2913556904, ___disp_0)); }
	inline CellJoinDisposable_1_t3736493403 * get_disp_0() const { return ___disp_0; }
	inline CellJoinDisposable_1_t3736493403 ** get_address_of_disp_0() { return &___disp_0; }
	inline void set_disp_0(CellJoinDisposable_1_t3736493403 * value)
	{
		___disp_0 = value;
		Il2CppCodeGenWriteBarrier(&___disp_0, value);
	}

	inline static int32_t get_offset_of_reaction_1() { return static_cast<int32_t>(offsetof(U3CMergeU3Ec__AnonStorey113_3_t2913556904, ___reaction_1)); }
	inline Action_1_t985559125 * get_reaction_1() const { return ___reaction_1; }
	inline Action_1_t985559125 ** get_address_of_reaction_1() { return &___reaction_1; }
	inline void set_reaction_1(Action_1_t985559125 * value)
	{
		___reaction_1 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24274_2() { return static_cast<int32_t>(offsetof(U3CMergeU3Ec__AnonStorey113_3_t2913556904, ___U3CU3Ef__refU24274_2)); }
	inline U3CMergeU3Ec__AnonStorey112_3_t3245937911 * get_U3CU3Ef__refU24274_2() const { return ___U3CU3Ef__refU24274_2; }
	inline U3CMergeU3Ec__AnonStorey112_3_t3245937911 ** get_address_of_U3CU3Ef__refU24274_2() { return &___U3CU3Ef__refU24274_2; }
	inline void set_U3CU3Ef__refU24274_2(U3CMergeU3Ec__AnonStorey112_3_t3245937911 * value)
	{
		___U3CU3Ef__refU24274_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24274_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
