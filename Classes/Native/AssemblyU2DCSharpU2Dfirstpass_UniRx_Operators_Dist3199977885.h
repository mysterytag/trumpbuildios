﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Collections.Generic.IEqualityComparer`1<System.Boolean>
struct IEqualityComparer_1_t2535271992;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3570117608.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>
struct  DistinctUntilChangedObservable_1_t3199977885  : public OperatorObservableBase_1_t3570117608
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.DistinctUntilChangedObservable`1::source
	Il2CppObject* ___source_1;
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.Operators.DistinctUntilChangedObservable`1::comparer
	Il2CppObject* ___comparer_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(DistinctUntilChangedObservable_1_t3199977885, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_comparer_2() { return static_cast<int32_t>(offsetof(DistinctUntilChangedObservable_1_t3199977885, ___comparer_2)); }
	inline Il2CppObject* get_comparer_2() const { return ___comparer_2; }
	inline Il2CppObject** get_address_of_comparer_2() { return &___comparer_2; }
	inline void set_comparer_2(Il2CppObject* value)
	{
		___comparer_2 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
