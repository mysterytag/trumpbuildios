﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Int32>
struct Select_t2143399540;
// UniRx.Operators.SelectObservable`2<System.Int64,System.Int32>
struct SelectObservable_2_t3327812218;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Int32>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m2902142203_gshared (Select_t2143399540 * __this, SelectObservable_2_t3327812218 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select__ctor_m2902142203(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select_t2143399540 *, SelectObservable_2_t3327812218 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select__ctor_m2902142203_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Int32>::OnNext(T)
extern "C"  void Select_OnNext_m1290755309_gshared (Select_t2143399540 * __this, int64_t ___value0, const MethodInfo* method);
#define Select_OnNext_m1290755309(__this, ___value0, method) ((  void (*) (Select_t2143399540 *, int64_t, const MethodInfo*))Select_OnNext_m1290755309_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Int32>::OnError(System.Exception)
extern "C"  void Select_OnError_m859700732_gshared (Select_t2143399540 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select_OnError_m859700732(__this, ___error0, method) ((  void (*) (Select_t2143399540 *, Exception_t1967233988 *, const MethodInfo*))Select_OnError_m859700732_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Int32>::OnCompleted()
extern "C"  void Select_OnCompleted_m2952579919_gshared (Select_t2143399540 * __this, const MethodInfo* method);
#define Select_OnCompleted_m2952579919(__this, method) ((  void (*) (Select_t2143399540 *, const MethodInfo*))Select_OnCompleted_m2952579919_gshared)(__this, method)
