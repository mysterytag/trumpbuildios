﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LogEventResponseCallback
struct LogEventResponseCallback_t2006404508;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LogEventRequest
struct LogEventRequest_t3812150249;
// PlayFab.ClientModels.LogEventResult
struct LogEventResult_t3344709027;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LogEventReq3812150249.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LogEventRes3344709027.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LogEventResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogEventResponseCallback__ctor_m2833604747 (LogEventResponseCallback_t2006404508 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LogEventResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LogEventRequest,PlayFab.ClientModels.LogEventResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LogEventResponseCallback_Invoke_m963854808 (LogEventResponseCallback_t2006404508 * __this, String_t* ___urlPath0, int32_t ___callId1, LogEventRequest_t3812150249 * ___request2, LogEventResult_t3344709027 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LogEventResponseCallback_t2006404508(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LogEventRequest_t3812150249 * ___request2, LogEventResult_t3344709027 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LogEventResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LogEventRequest,PlayFab.ClientModels.LogEventResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LogEventResponseCallback_BeginInvoke_m2535339269 (LogEventResponseCallback_t2006404508 * __this, String_t* ___urlPath0, int32_t ___callId1, LogEventRequest_t3812150249 * ___request2, LogEventResult_t3344709027 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LogEventResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogEventResponseCallback_EndInvoke_m2692534299 (LogEventResponseCallback_t2006404508 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
