﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/Timer
struct  Timer_t80811813  : public OperatorObserverBase_2_t3939730909
{
public:
	// System.Int64 UniRx.Operators.TimerObservable/Timer::index
	int64_t ___index_2;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Timer_t80811813, ___index_2)); }
	inline int64_t get_index_2() const { return ___index_2; }
	inline int64_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int64_t value)
	{
		___index_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
