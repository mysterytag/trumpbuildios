﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionName
struct XPathFunctionName_t1855337661;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionName::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionName__ctor_m17270517 (XPathFunctionName_t1855337661 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionName::get_ReturnType()
extern "C"  int32_t XPathFunctionName_get_ReturnType_m2142310159 (XPathFunctionName_t1855337661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionName::get_Peer()
extern "C"  bool XPathFunctionName_get_Peer_m3377846347 (XPathFunctionName_t1855337661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionName::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionName_Evaluate_m2105002246 (XPathFunctionName_t1855337661 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionName::ToString()
extern "C"  String_t* XPathFunctionName_ToString_m1791582503 (XPathFunctionName_t1855337661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
