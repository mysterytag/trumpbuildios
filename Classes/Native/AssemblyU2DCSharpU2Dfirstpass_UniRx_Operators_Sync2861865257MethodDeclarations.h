﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>
struct Synchronize_t2861865257;
// UniRx.Operators.SynchronizeObservable`1<System.Object>
struct SynchronizeObservable_1_t736720834;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>::.ctor(UniRx.Operators.SynchronizeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Synchronize__ctor_m3932415724_gshared (Synchronize_t2861865257 * __this, SynchronizeObservable_1_t736720834 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Synchronize__ctor_m3932415724(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Synchronize_t2861865257 *, SynchronizeObservable_1_t736720834 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Synchronize__ctor_m3932415724_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>::OnNext(T)
extern "C"  void Synchronize_OnNext_m212815145_gshared (Synchronize_t2861865257 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Synchronize_OnNext_m212815145(__this, ___value0, method) ((  void (*) (Synchronize_t2861865257 *, Il2CppObject *, const MethodInfo*))Synchronize_OnNext_m212815145_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>::OnError(System.Exception)
extern "C"  void Synchronize_OnError_m3169711672_gshared (Synchronize_t2861865257 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Synchronize_OnError_m3169711672(__this, ___error0, method) ((  void (*) (Synchronize_t2861865257 *, Exception_t1967233988 *, const MethodInfo*))Synchronize_OnError_m3169711672_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>::OnCompleted()
extern "C"  void Synchronize_OnCompleted_m2979151243_gshared (Synchronize_t2861865257 * __this, const MethodInfo* method);
#define Synchronize_OnCompleted_m2979151243(__this, method) ((  void (*) (Synchronize_t2861865257 *, const MethodInfo*))Synchronize_OnCompleted_m2979151243_gshared)(__this, method)
