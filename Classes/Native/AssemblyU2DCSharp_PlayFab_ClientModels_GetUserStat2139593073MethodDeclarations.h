﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetUserStatisticsResult
struct GetUserStatisticsResult_t2139593073;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetUserStatisticsResult::.ctor()
extern "C"  void GetUserStatisticsResult__ctor_m3940267864 (GetUserStatisticsResult_t2139593073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserStatisticsResult::get_UserStatistics()
extern "C"  Dictionary_2_t190145395 * GetUserStatisticsResult_get_UserStatistics_m4167672328 (GetUserStatisticsResult_t2139593073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserStatisticsResult::set_UserStatistics(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetUserStatisticsResult_set_UserStatistics_m749750093 (GetUserStatisticsResult_t2139593073 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
