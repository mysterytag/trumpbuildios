﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Progression_2058507475.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void GameAnalyticsSDK.GA_Progression::NewEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String)
extern "C"  void GA_Progression_NewEvent_m1361716027 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Progression::NewEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String)
extern "C"  void GA_Progression_NewEvent_m3309013047 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Progression::NewEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.String)
extern "C"  void GA_Progression_NewEvent_m1896514611 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Progression::NewEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.Int32)
extern "C"  void GA_Progression_NewEvent_m3001504124 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, int32_t ___score2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Progression::NewEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.Int32)
extern "C"  void GA_Progression_NewEvent_m462087680 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, int32_t ___score3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Progression::NewEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.String,System.Int32)
extern "C"  void GA_Progression_NewEvent_m1501447044 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, int32_t ___score4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Progression::CreateEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.String,System.Nullable`1<System.Int32>)
extern "C"  void GA_Progression_CreateEvent_m1108958979 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, Nullable_1_t1438485399  ___score4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
