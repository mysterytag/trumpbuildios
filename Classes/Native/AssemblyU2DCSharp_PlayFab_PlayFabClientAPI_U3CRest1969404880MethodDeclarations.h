﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<RestoreIOSPurchases>c__AnonStoreyB2
struct U3CRestoreIOSPurchasesU3Ec__AnonStoreyB2_t1969404880;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<RestoreIOSPurchases>c__AnonStoreyB2::.ctor()
extern "C"  void U3CRestoreIOSPurchasesU3Ec__AnonStoreyB2__ctor_m1438991747 (U3CRestoreIOSPurchasesU3Ec__AnonStoreyB2_t1969404880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<RestoreIOSPurchases>c__AnonStoreyB2::<>m__9F(PlayFab.CallRequestContainer)
extern "C"  void U3CRestoreIOSPurchasesU3Ec__AnonStoreyB2_U3CU3Em__9F_m1069352846 (U3CRestoreIOSPurchasesU3Ec__AnonStoreyB2_t1969404880 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
