﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<UnregisterProvider>c__AnonStorey186
struct U3CUnregisterProviderU3Ec__AnonStorey186_t1919525821;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

// System.Void Zenject.DiContainer/<UnregisterProvider>c__AnonStorey186::.ctor()
extern "C"  void U3CUnregisterProviderU3Ec__AnonStorey186__ctor_m1434216140 (U3CUnregisterProviderU3Ec__AnonStorey186_t1919525821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<UnregisterProvider>c__AnonStorey186::<>m__2BF(Zenject.ProviderBase)
extern "C"  bool U3CUnregisterProviderU3Ec__AnonStorey186_U3CU3Em__2BF_m1915716118 (U3CUnregisterProviderU3Ec__AnonStorey186_t1919525821 * __this, ProviderBase_t1627494391 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
