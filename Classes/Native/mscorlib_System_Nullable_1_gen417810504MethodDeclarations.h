﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen417810504.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserOrigina1826739892.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.ClientModels.UserOrigination>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1964700759_gshared (Nullable_1_t417810504 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1964700759(__this, ___value0, method) ((  void (*) (Nullable_1_t417810504 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1964700759_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserOrigination>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3106446002_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3106446002(__this, method) ((  bool (*) (Nullable_1_t417810504 *, const MethodInfo*))Nullable_1_get_HasValue_m3106446002_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.UserOrigination>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2138108530_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2138108530(__this, method) ((  int32_t (*) (Nullable_1_t417810504 *, const MethodInfo*))Nullable_1_get_Value_m2138108530_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserOrigination>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3473804058_gshared (Nullable_1_t417810504 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3473804058(__this, ___other0, method) ((  bool (*) (Nullable_1_t417810504 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3473804058_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserOrigination>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1384101605_gshared (Nullable_1_t417810504 * __this, Nullable_1_t417810504  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1384101605(__this, ___other0, method) ((  bool (*) (Nullable_1_t417810504 *, Nullable_1_t417810504 , const MethodInfo*))Nullable_1_Equals_m1384101605_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.ClientModels.UserOrigination>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3907231858_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3907231858(__this, method) ((  int32_t (*) (Nullable_1_t417810504 *, const MethodInfo*))Nullable_1_GetHashCode_m3907231858_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.UserOrigination>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2844010983_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2844010983(__this, method) ((  int32_t (*) (Nullable_1_t417810504 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2844010983_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.ClientModels.UserOrigination>::ToString()
extern "C"  String_t* Nullable_1_ToString_m752368934_gshared (Nullable_1_t417810504 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m752368934(__this, method) ((  String_t* (*) (Nullable_1_t417810504 *, const MethodInfo*))Nullable_1_ToString_m752368934_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.UserOrigination>::op_Implicit(T)
extern "C"  Nullable_1_t417810504  Nullable_1_op_Implicit_m2196646560_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m2196646560(__this /* static, unused */, ___value0, method) ((  Nullable_1_t417810504  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m2196646560_gshared)(__this /* static, unused */, ___value0, method)
