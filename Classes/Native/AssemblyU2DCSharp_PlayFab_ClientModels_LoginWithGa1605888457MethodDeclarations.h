﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithGameCenterRequest
struct LoginWithGameCenterRequest_t1605888457;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithGameCenterRequest::.ctor()
extern "C"  void LoginWithGameCenterRequest__ctor_m1248947220 (LoginWithGameCenterRequest_t1605888457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithGameCenterRequest::get_TitleId()
extern "C"  String_t* LoginWithGameCenterRequest_get_TitleId_m1711872639 (LoginWithGameCenterRequest_t1605888457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithGameCenterRequest::set_TitleId(System.String)
extern "C"  void LoginWithGameCenterRequest_set_TitleId_m561804890 (LoginWithGameCenterRequest_t1605888457 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithGameCenterRequest::get_PlayerId()
extern "C"  String_t* LoginWithGameCenterRequest_get_PlayerId_m1123207826 (LoginWithGameCenterRequest_t1605888457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithGameCenterRequest::set_PlayerId(System.String)
extern "C"  void LoginWithGameCenterRequest_set_PlayerId_m3616986393 (LoginWithGameCenterRequest_t1605888457 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithGameCenterRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithGameCenterRequest_get_CreateAccount_m2982747107 (LoginWithGameCenterRequest_t1605888457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithGameCenterRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithGameCenterRequest_set_CreateAccount_m505130434 (LoginWithGameCenterRequest_t1605888457 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
