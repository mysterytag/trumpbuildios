﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer
struct TutorialPlayer_t4281139199;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.List`1<TutorialTask>
struct List_1_t1570237212;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// BottomButton[]
struct BottomButtonU5BU5D_t4253761680;
// TutorialTask
struct TutorialTask_t773278243;
// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// System.Func`1<System.Boolean>
struct Func_1_t1353786588;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// BottomButton
struct BottomButton_t1932555613;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TutorialTask773278243.h"
#include "AssemblyU2DCSharp_BottomButton1932555613.h"

// System.Void TutorialPlayer::.ctor()
extern "C"  void TutorialPlayer__ctor_m893344636 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::TutorScenario()
extern "C"  void TutorialPlayer_TutorScenario_m3852291840 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::StartTutor()
extern "C"  void TutorialPlayer_StartTutor_m1636615164 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::End()
extern "C"  void TutorialPlayer_End_m902926581 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TutorialPlayer::StartPlay(System.Collections.Generic.List`1<TutorialTask>)
extern "C"  Il2CppObject * TutorialPlayer_StartPlay_m3128389389 (TutorialPlayer_t4281139199 * __this, List_1_t1570237212 * ___tasks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine TutorialPlayer::WaitForPressBottomButton(BottomButton[])
extern "C"  Coroutine_t2246592261 * TutorialPlayer_WaitForPressBottomButton_m1081546183 (TutorialPlayer_t4281139199 * __this, BottomButtonU5BU5D_t4253761680* ___buttons0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TutorialPlayer::WaitForButton(TutorialTask)
extern "C"  Il2CppObject * TutorialPlayer_WaitForButton_m3623907829 (TutorialPlayer_t4281139199 * __this, TutorialTask_t773278243 * ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine TutorialPlayer::WaitForSubject(UniRx.IObservable`1<System.String>)
extern "C"  Coroutine_t2246592261 * TutorialPlayer_WaitForSubject_m2530445357 (TutorialPlayer_t4281139199 * __this, Il2CppObject* ___observable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TutorialPlayer::WaitForMMM()
extern "C"  Il2CppObject * TutorialPlayer_WaitForMMM_m363352105 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine TutorialPlayer::WaitFor(System.Func`1<System.Boolean>)
extern "C"  Coroutine_t2246592261 * TutorialPlayer_WaitFor_m3106475049 (TutorialPlayer_t4281139199 * __this, Func_1_t1353786588 * ___predicate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine TutorialPlayer::WaitForTap()
extern "C"  Coroutine_t2246592261 * TutorialPlayer_WaitForTap_m3251002537 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TutorialPlayer::WaitForCorroutine(System.Func`1<System.Boolean>)
extern "C"  Il2CppObject * TutorialPlayer_WaitForCorroutine_m2162333025 (TutorialPlayer_t4281139199 * __this, Func_1_t1353786588 * ___predicate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TutorialPlayer::WaitForBill(System.Int32)
extern "C"  Il2CppObject * TutorialPlayer_WaitForBill_m3452737758 (TutorialPlayer_t4281139199 * __this, int32_t ___money0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TutorialPlayer::ShowCharAndText(TutorialTask)
extern "C"  Il2CppObject * TutorialPlayer_ShowCharAndText_m102833984 (TutorialPlayer_t4281139199 * __this, TutorialTask_t773278243 * ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::<TutorScenario>m__13E()
extern "C"  void TutorialPlayer_U3CTutorScenarioU3Em__13E_m2694121188 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::<TutorScenario>m__13F()
extern "C"  void TutorialPlayer_U3CTutorScenarioU3Em__13F_m2694122149 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::<TutorScenario>m__140()
extern "C"  void TutorialPlayer_U3CTutorScenarioU3Em__140_m2694130798 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::<TutorScenario>m__141()
extern "C"  void TutorialPlayer_U3CTutorScenarioU3Em__141_m2694131759 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::<TutorScenario>m__142()
extern "C"  void TutorialPlayer_U3CTutorScenarioU3Em__142_m2694132720 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::<TutorScenario>m__143()
extern "C"  void TutorialPlayer_U3CTutorScenarioU3Em__143_m2694133681 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::<TutorScenario>m__144()
extern "C"  void TutorialPlayer_U3CTutorScenarioU3Em__144_m2694134642 (TutorialPlayer_t4281139199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer::<TutorScenario>m__145()
extern "C"  void TutorialPlayer_U3CTutorScenarioU3Em__145_m2694135603 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> TutorialPlayer::<WaitForPressBottomButton>m__146(BottomButton)
extern "C"  Il2CppObject* TutorialPlayer_U3CWaitForPressBottomButtonU3Em__146_m2276310639 (Il2CppObject * __this /* static, unused */, BottomButton_t1932555613 * ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer::<WaitForTap>m__14C()
extern "C"  bool TutorialPlayer_U3CWaitForTapU3Em__14C_m4057083266 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
