﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo
struct OnStateMachineInfo_t865869923;
// UnityEngine.Animator
struct Animator_t792326996;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"

// System.Void UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::.ctor(UnityEngine.Animator,System.Int32)
extern "C"  void OnStateMachineInfo__ctor_m995375500 (OnStateMachineInfo_t865869923 * __this, Animator_t792326996 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Animator UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::get_Animator()
extern "C"  Animator_t792326996 * OnStateMachineInfo_get_Animator_m2209204900 (OnStateMachineInfo_t865869923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::set_Animator(UnityEngine.Animator)
extern "C"  void OnStateMachineInfo_set_Animator_m1641920609 (OnStateMachineInfo_t865869923 * __this, Animator_t792326996 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::get_StateMachinePathHash()
extern "C"  int32_t OnStateMachineInfo_get_StateMachinePathHash_m3184418071 (OnStateMachineInfo_t865869923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::set_StateMachinePathHash(System.Int32)
extern "C"  void OnStateMachineInfo_set_StateMachinePathHash_m2290533906 (OnStateMachineInfo_t865869923 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
