﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B
struct U3CAsObservableU3Ec__AnonStorey7B_t3743595045;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UnityEngine.AsyncOperation>
struct IObserver_1_t1291426671;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey7B__ctor_m713408322 (U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B::<>m__A3(UniRx.IObserver`1<UnityEngine.AsyncOperation>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CAsObservableU3Ec__AnonStorey7B_U3CU3Em__A3_m2420906715 (U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
