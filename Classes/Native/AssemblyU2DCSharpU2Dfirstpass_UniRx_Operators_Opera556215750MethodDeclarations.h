﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Single>
struct U3CSubscribeU3Ec__AnonStorey5B_t556215750;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Single>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m3147227279_gshared (U3CSubscribeU3Ec__AnonStorey5B_t556215750 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m3147227279(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t556215750 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m3147227279_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Single>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3136660340_gshared (U3CSubscribeU3Ec__AnonStorey5B_t556215750 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3136660340(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t556215750 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3136660340_gshared)(__this, method)
