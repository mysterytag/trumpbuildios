﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t3286458198;
// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UniRx.ReactiveProperty`1<System.String>
struct ReactiveProperty_1_t1576821940;
// TutorialArrow
struct TutorialArrow_t2479747883;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorchikPanel
struct  TutorchikPanel_t3935308711  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.Text TutorchikPanel::Text
	Text_t3286458198 * ___Text_2;
	// UnityEngine.UI.Image TutorchikPanel::Fon
	Image_t3354615620 * ___Fon_3;
	// UnityEngine.RectTransform TutorchikPanel::TextBack
	RectTransform_t3317474837 * ___TextBack_4;
	// UnityEngine.RectTransform TutorchikPanel::Character
	RectTransform_t3317474837 * ___Character_5;
	// UnityEngine.GameObject TutorchikPanel::Hand
	GameObject_t4012695102 * ___Hand_6;
	// UniRx.ReactiveProperty`1<System.String> TutorchikPanel::TutorialText
	ReactiveProperty_1_t1576821940 * ___TutorialText_7;
	// TutorialArrow TutorchikPanel::TutorialArrow
	TutorialArrow_t2479747883 * ___TutorialArrow_8;

public:
	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(TutorchikPanel_t3935308711, ___Text_2)); }
	inline Text_t3286458198 * get_Text_2() const { return ___Text_2; }
	inline Text_t3286458198 ** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(Text_t3286458198 * value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier(&___Text_2, value);
	}

	inline static int32_t get_offset_of_Fon_3() { return static_cast<int32_t>(offsetof(TutorchikPanel_t3935308711, ___Fon_3)); }
	inline Image_t3354615620 * get_Fon_3() const { return ___Fon_3; }
	inline Image_t3354615620 ** get_address_of_Fon_3() { return &___Fon_3; }
	inline void set_Fon_3(Image_t3354615620 * value)
	{
		___Fon_3 = value;
		Il2CppCodeGenWriteBarrier(&___Fon_3, value);
	}

	inline static int32_t get_offset_of_TextBack_4() { return static_cast<int32_t>(offsetof(TutorchikPanel_t3935308711, ___TextBack_4)); }
	inline RectTransform_t3317474837 * get_TextBack_4() const { return ___TextBack_4; }
	inline RectTransform_t3317474837 ** get_address_of_TextBack_4() { return &___TextBack_4; }
	inline void set_TextBack_4(RectTransform_t3317474837 * value)
	{
		___TextBack_4 = value;
		Il2CppCodeGenWriteBarrier(&___TextBack_4, value);
	}

	inline static int32_t get_offset_of_Character_5() { return static_cast<int32_t>(offsetof(TutorchikPanel_t3935308711, ___Character_5)); }
	inline RectTransform_t3317474837 * get_Character_5() const { return ___Character_5; }
	inline RectTransform_t3317474837 ** get_address_of_Character_5() { return &___Character_5; }
	inline void set_Character_5(RectTransform_t3317474837 * value)
	{
		___Character_5 = value;
		Il2CppCodeGenWriteBarrier(&___Character_5, value);
	}

	inline static int32_t get_offset_of_Hand_6() { return static_cast<int32_t>(offsetof(TutorchikPanel_t3935308711, ___Hand_6)); }
	inline GameObject_t4012695102 * get_Hand_6() const { return ___Hand_6; }
	inline GameObject_t4012695102 ** get_address_of_Hand_6() { return &___Hand_6; }
	inline void set_Hand_6(GameObject_t4012695102 * value)
	{
		___Hand_6 = value;
		Il2CppCodeGenWriteBarrier(&___Hand_6, value);
	}

	inline static int32_t get_offset_of_TutorialText_7() { return static_cast<int32_t>(offsetof(TutorchikPanel_t3935308711, ___TutorialText_7)); }
	inline ReactiveProperty_1_t1576821940 * get_TutorialText_7() const { return ___TutorialText_7; }
	inline ReactiveProperty_1_t1576821940 ** get_address_of_TutorialText_7() { return &___TutorialText_7; }
	inline void set_TutorialText_7(ReactiveProperty_1_t1576821940 * value)
	{
		___TutorialText_7 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialText_7, value);
	}

	inline static int32_t get_offset_of_TutorialArrow_8() { return static_cast<int32_t>(offsetof(TutorchikPanel_t3935308711, ___TutorialArrow_8)); }
	inline TutorialArrow_t2479747883 * get_TutorialArrow_8() const { return ___TutorialArrow_8; }
	inline TutorialArrow_t2479747883 ** get_address_of_TutorialArrow_8() { return &___TutorialArrow_8; }
	inline void set_TutorialArrow_8(TutorialArrow_t2479747883 * value)
	{
		___TutorialArrow_8 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialArrow_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
