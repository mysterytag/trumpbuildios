﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetSharedGroupDataRequest
struct GetSharedGroupDataRequest_t3614043377;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.GetSharedGroupDataRequest::.ctor()
extern "C"  void GetSharedGroupDataRequest__ctor_m1452009048 (GetSharedGroupDataRequest_t3614043377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetSharedGroupDataRequest::get_SharedGroupId()
extern "C"  String_t* GetSharedGroupDataRequest_get_SharedGroupId_m3186429599 (GetSharedGroupDataRequest_t3614043377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetSharedGroupDataRequest::set_SharedGroupId(System.String)
extern "C"  void GetSharedGroupDataRequest_set_SharedGroupId_m347475220 (GetSharedGroupDataRequest_t3614043377 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetSharedGroupDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetSharedGroupDataRequest_get_Keys_m1853890330 (GetSharedGroupDataRequest_t3614043377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetSharedGroupDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetSharedGroupDataRequest_set_Keys_m88879121 (GetSharedGroupDataRequest_t3614043377 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetSharedGroupDataRequest::get_GetMembers()
extern "C"  Nullable_1_t3097043249  GetSharedGroupDataRequest_get_GetMembers_m3879334625 (GetSharedGroupDataRequest_t3614043377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetSharedGroupDataRequest::set_GetMembers(System.Nullable`1<System.Boolean>)
extern "C"  void GetSharedGroupDataRequest_set_GetMembers_m4191086236 (GetSharedGroupDataRequest_t3614043377 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
