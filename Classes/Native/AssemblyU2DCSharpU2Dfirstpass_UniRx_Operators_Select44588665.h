﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Double>
struct IObservable_1_t293314978;
// System.Func`2<System.Double,UniRx.IObservable`1<System.Double>>
struct Func_2_t2628749132;
// System.Func`3<System.Double,System.Int32,UniRx.IObservable`1<System.Double>>
struct Func_3_t2454229394;
// System.Func`2<System.Double,System.Collections.Generic.IEnumerable`1<System.Double>>
struct Func_2_t1447137828;
// System.Func`3<System.Double,System.Int32,System.Collections.Generic.IEnumerable`1<System.Double>>
struct Func_3_t1272618090;
// System.Func`3<System.Double,System.Double,System.Double>
struct Func_3_t1933728903;
// System.Func`5<System.Double,System.Int32,System.Double,System.Int32,System.Double>
struct Func_5_t2889627055;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3893628881.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>
struct  SelectManyObservable_3_t44588665  : public OperatorObservableBase_1_t3893628881
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.SelectManyObservable`3::source
	Il2CppObject* ___source_1;
	// System.Func`2<TSource,UniRx.IObservable`1<TCollection>> UniRx.Operators.SelectManyObservable`3::collectionSelector
	Func_2_t2628749132 * ___collectionSelector_2;
	// System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TCollection>> UniRx.Operators.SelectManyObservable`3::collectionSelectorWithIndex
	Func_3_t2454229394 * ___collectionSelectorWithIndex_3;
	// System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>> UniRx.Operators.SelectManyObservable`3::collectionSelectorEnumerable
	Func_2_t1447137828 * ___collectionSelectorEnumerable_4;
	// System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TCollection>> UniRx.Operators.SelectManyObservable`3::collectionSelectorEnumerableWithIndex
	Func_3_t1272618090 * ___collectionSelectorEnumerableWithIndex_5;
	// System.Func`3<TSource,TCollection,TResult> UniRx.Operators.SelectManyObservable`3::resultSelector
	Func_3_t1933728903 * ___resultSelector_6;
	// System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult> UniRx.Operators.SelectManyObservable`3::resultSelectorWithIndex
	Func_5_t2889627055 * ___resultSelectorWithIndex_7;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t44588665, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_collectionSelector_2() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t44588665, ___collectionSelector_2)); }
	inline Func_2_t2628749132 * get_collectionSelector_2() const { return ___collectionSelector_2; }
	inline Func_2_t2628749132 ** get_address_of_collectionSelector_2() { return &___collectionSelector_2; }
	inline void set_collectionSelector_2(Func_2_t2628749132 * value)
	{
		___collectionSelector_2 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelector_2, value);
	}

	inline static int32_t get_offset_of_collectionSelectorWithIndex_3() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t44588665, ___collectionSelectorWithIndex_3)); }
	inline Func_3_t2454229394 * get_collectionSelectorWithIndex_3() const { return ___collectionSelectorWithIndex_3; }
	inline Func_3_t2454229394 ** get_address_of_collectionSelectorWithIndex_3() { return &___collectionSelectorWithIndex_3; }
	inline void set_collectionSelectorWithIndex_3(Func_3_t2454229394 * value)
	{
		___collectionSelectorWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelectorWithIndex_3, value);
	}

	inline static int32_t get_offset_of_collectionSelectorEnumerable_4() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t44588665, ___collectionSelectorEnumerable_4)); }
	inline Func_2_t1447137828 * get_collectionSelectorEnumerable_4() const { return ___collectionSelectorEnumerable_4; }
	inline Func_2_t1447137828 ** get_address_of_collectionSelectorEnumerable_4() { return &___collectionSelectorEnumerable_4; }
	inline void set_collectionSelectorEnumerable_4(Func_2_t1447137828 * value)
	{
		___collectionSelectorEnumerable_4 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelectorEnumerable_4, value);
	}

	inline static int32_t get_offset_of_collectionSelectorEnumerableWithIndex_5() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t44588665, ___collectionSelectorEnumerableWithIndex_5)); }
	inline Func_3_t1272618090 * get_collectionSelectorEnumerableWithIndex_5() const { return ___collectionSelectorEnumerableWithIndex_5; }
	inline Func_3_t1272618090 ** get_address_of_collectionSelectorEnumerableWithIndex_5() { return &___collectionSelectorEnumerableWithIndex_5; }
	inline void set_collectionSelectorEnumerableWithIndex_5(Func_3_t1272618090 * value)
	{
		___collectionSelectorEnumerableWithIndex_5 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelectorEnumerableWithIndex_5, value);
	}

	inline static int32_t get_offset_of_resultSelector_6() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t44588665, ___resultSelector_6)); }
	inline Func_3_t1933728903 * get_resultSelector_6() const { return ___resultSelector_6; }
	inline Func_3_t1933728903 ** get_address_of_resultSelector_6() { return &___resultSelector_6; }
	inline void set_resultSelector_6(Func_3_t1933728903 * value)
	{
		___resultSelector_6 = value;
		Il2CppCodeGenWriteBarrier(&___resultSelector_6, value);
	}

	inline static int32_t get_offset_of_resultSelectorWithIndex_7() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t44588665, ___resultSelectorWithIndex_7)); }
	inline Func_5_t2889627055 * get_resultSelectorWithIndex_7() const { return ___resultSelectorWithIndex_7; }
	inline Func_5_t2889627055 ** get_address_of_resultSelectorWithIndex_7() { return &___resultSelectorWithIndex_7; }
	inline void set_resultSelectorWithIndex_7(Func_5_t2889627055 * value)
	{
		___resultSelectorWithIndex_7 = value;
		Il2CppCodeGenWriteBarrier(&___resultSelectorWithIndex_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
