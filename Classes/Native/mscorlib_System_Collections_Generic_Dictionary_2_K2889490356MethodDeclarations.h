﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>
struct KeyCollection_t2889490356;
// System.Collections.Generic.Dictionary`2<System.Double,System.Object>
struct Dictionary_2_t566215076;
// System.Collections.Generic.IEnumerator`1<System.Double>
struct IEnumerator_1_t2017623062;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Double[]
struct DoubleU5BU5D_t1048280995;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke333243017.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2047358362_gshared (KeyCollection_t2889490356 * __this, Dictionary_2_t566215076 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2047358362(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2889490356 *, Dictionary_2_t566215076 *, const MethodInfo*))KeyCollection__ctor_m2047358362_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1365543804_gshared (KeyCollection_t2889490356 * __this, double ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1365543804(__this, ___item0, method) ((  void (*) (KeyCollection_t2889490356 *, double, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1365543804_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3608109043_gshared (KeyCollection_t2889490356 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3608109043(__this, method) ((  void (*) (KeyCollection_t2889490356 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3608109043_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1017426190_gshared (KeyCollection_t2889490356 * __this, double ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1017426190(__this, ___item0, method) ((  bool (*) (KeyCollection_t2889490356 *, double, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1017426190_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2129006067_gshared (KeyCollection_t2889490356 * __this, double ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2129006067(__this, ___item0, method) ((  bool (*) (KeyCollection_t2889490356 *, double, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2129006067_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3173699247_gshared (KeyCollection_t2889490356 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3173699247(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2889490356 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3173699247_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2126114789_gshared (KeyCollection_t2889490356 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2126114789(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2889490356 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2126114789_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3501866656_gshared (KeyCollection_t2889490356 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3501866656(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2889490356 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3501866656_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1375083759_gshared (KeyCollection_t2889490356 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1375083759(__this, method) ((  bool (*) (KeyCollection_t2889490356 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1375083759_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m414954593_gshared (KeyCollection_t2889490356 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m414954593(__this, method) ((  bool (*) (KeyCollection_t2889490356 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m414954593_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m402947021_gshared (KeyCollection_t2889490356 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m402947021(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2889490356 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m402947021_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3487728719_gshared (KeyCollection_t2889490356 * __this, DoubleU5BU5D_t1048280995* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3487728719(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2889490356 *, DoubleU5BU5D_t1048280995*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3487728719_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::GetEnumerator()
extern "C"  Enumerator_t333243017  KeyCollection_GetEnumerator_m323776690_gshared (KeyCollection_t2889490356 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m323776690(__this, method) ((  Enumerator_t333243017  (*) (KeyCollection_t2889490356 *, const MethodInfo*))KeyCollection_GetEnumerator_m323776690_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1262350119_gshared (KeyCollection_t2889490356 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1262350119(__this, method) ((  int32_t (*) (KeyCollection_t2889490356 *, const MethodInfo*))KeyCollection_get_Count_m1262350119_gshared)(__this, method)
