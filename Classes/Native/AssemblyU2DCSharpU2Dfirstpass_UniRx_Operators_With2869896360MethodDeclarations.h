﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>
struct WithLatestFrom_t2869896360;
// UniRx.Operators.WithLatestFromObservable`3<System.Object,System.Object,System.Object>
struct WithLatestFromObservable_3_t1427945019;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.WithLatestFromObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void WithLatestFrom__ctor_m136069944_gshared (WithLatestFrom_t2869896360 * __this, WithLatestFromObservable_3_t1427945019 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define WithLatestFrom__ctor_m136069944(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (WithLatestFrom_t2869896360 *, WithLatestFromObservable_3_t1427945019 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))WithLatestFrom__ctor_m136069944_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * WithLatestFrom_Run_m2183478343_gshared (WithLatestFrom_t2869896360 * __this, const MethodInfo* method);
#define WithLatestFrom_Run_m2183478343(__this, method) ((  Il2CppObject * (*) (WithLatestFrom_t2869896360 *, const MethodInfo*))WithLatestFrom_Run_m2183478343_gshared)(__this, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>::OnNext(TResult)
extern "C"  void WithLatestFrom_OnNext_m2054459844_gshared (WithLatestFrom_t2869896360 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define WithLatestFrom_OnNext_m2054459844(__this, ___value0, method) ((  void (*) (WithLatestFrom_t2869896360 *, Il2CppObject *, const MethodInfo*))WithLatestFrom_OnNext_m2054459844_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void WithLatestFrom_OnError_m4143645040_gshared (WithLatestFrom_t2869896360 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WithLatestFrom_OnError_m4143645040(__this, ___error0, method) ((  void (*) (WithLatestFrom_t2869896360 *, Exception_t1967233988 *, const MethodInfo*))WithLatestFrom_OnError_m4143645040_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void WithLatestFrom_OnCompleted_m4163656899_gshared (WithLatestFrom_t2869896360 * __this, const MethodInfo* method);
#define WithLatestFrom_OnCompleted_m4163656899(__this, method) ((  void (*) (WithLatestFrom_t2869896360 *, const MethodInfo*))WithLatestFrom_OnCompleted_m4163656899_gshared)(__this, method)
