﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>
struct Merge_t3474609588;
// UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>
struct MergeOuterObserver_t3334261548;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>::.ctor(UniRx.Operators.MergeObservable`1/MergeOuterObserver<T>,System.IDisposable)
extern "C"  void Merge__ctor_m3303531498_gshared (Merge_t3474609588 * __this, MergeOuterObserver_t3334261548 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Merge__ctor_m3303531498(__this, ___parent0, ___cancel1, method) ((  void (*) (Merge_t3474609588 *, MergeOuterObserver_t3334261548 *, Il2CppObject *, const MethodInfo*))Merge__ctor_m3303531498_gshared)(__this, ___parent0, ___cancel1, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>::OnNext(T)
extern "C"  void Merge_OnNext_m784216725_gshared (Merge_t3474609588 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Merge_OnNext_m784216725(__this, ___value0, method) ((  void (*) (Merge_t3474609588 *, Unit_t2558286038 , const MethodInfo*))Merge_OnNext_m784216725_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Merge_OnError_m4265378724_gshared (Merge_t3474609588 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Merge_OnError_m4265378724(__this, ___error0, method) ((  void (*) (Merge_t3474609588 *, Exception_t1967233988 *, const MethodInfo*))Merge_OnError_m4265378724_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>::OnCompleted()
extern "C"  void Merge_OnCompleted_m3180891383_gshared (Merge_t3474609588 * __this, const MethodInfo* method);
#define Merge_OnCompleted_m3180891383(__this, method) ((  void (*) (Merge_t3474609588 *, const MethodInfo*))Merge_OnCompleted_m3180891383_gshared)(__this, method)
