﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetTitleNewsRequest
struct GetTitleNewsRequest_t3432781354;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetTitleNewsRequest::.ctor()
extern "C"  void GetTitleNewsRequest__ctor_m823948735 (GetTitleNewsRequest_t3432781354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetTitleNewsRequest::get_Count()
extern "C"  Nullable_1_t1438485399  GetTitleNewsRequest_get_Count_m430093492 (GetTitleNewsRequest_t3432781354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetTitleNewsRequest::set_Count(System.Nullable`1<System.Int32>)
extern "C"  void GetTitleNewsRequest_set_Count_m2625469727 (GetTitleNewsRequest_t3432781354 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
