﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen340490602MethodDeclarations.h"

// System.Void ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::.ctor()
#define ImmutableList_1__ctor_m1704606511(__this, method) ((  void (*) (ImmutableList_1_t1593135615 *, const MethodInfo*))ImmutableList_1__ctor_m99192906_gshared)(__this, method)
// System.Void ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::.ctor(T)
#define ImmutableList_1__ctor_m1303195663(__this, ___single0, method) ((  void (*) (ImmutableList_1_t1593135615 *, Action_1_t2089751433 *, const MethodInfo*))ImmutableList_1__ctor_m3074981460_gshared)(__this, ___single0, method)
// System.Void ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::.ctor(T[])
#define ImmutableList_1__ctor_m2535599981(__this, ___data0, method) ((  void (*) (ImmutableList_1_t1593135615 *, Action_1U5BU5D_t680290356*, const MethodInfo*))ImmutableList_1__ctor_m119734386_gshared)(__this, ___data0, method)
// System.Collections.IEnumerator ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::System.Collections.IEnumerable.GetEnumerator()
#define ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2722020578(__this, method) ((  Il2CppObject * (*) (ImmutableList_1_t1593135615 *, const MethodInfo*))ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2591974359_gshared)(__this, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::Add(T)
#define ImmutableList_1_Add_m989038680(__this, ___value0, method) ((  ImmutableList_1_t1593135615 * (*) (ImmutableList_1_t1593135615 *, Action_1_t2089751433 *, const MethodInfo*))ImmutableList_1_Add_m3924978445_gshared)(__this, ___value0, method)
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::GetEnumerator()
#define ImmutableList_1_GetEnumerator_m2269842667(__this, method) ((  Il2CppObject* (*) (ImmutableList_1_t1593135615 *, const MethodInfo*))ImmutableList_1_GetEnumerator_m364779198_gshared)(__this, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::Remove(T)
#define ImmutableList_1_Remove_m3520017373(__this, ___value0, method) ((  ImmutableList_1_t1593135615 * (*) (ImmutableList_1_t1593135615 *, Action_1_t2089751433 *, const MethodInfo*))ImmutableList_1_Remove_m1092573448_gshared)(__this, ___value0, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::Replace(System.Int32,T)
#define ImmutableList_1_Replace_m1749663746(__this, ___index0, ___value1, method) ((  ImmutableList_1_t1593135615 * (*) (ImmutableList_1_t1593135615 *, int32_t, Action_1_t2089751433 *, const MethodInfo*))ImmutableList_1_Replace_m2580545005_gshared)(__this, ___index0, ___value1, method)
// System.Int32 ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m2241668182(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t1593135615 *, Action_1_t2089751433 *, const MethodInfo*))ImmutableList_1_IndexOf_m1554103039_gshared)(__this, ___value0, method)
// System.Int32 ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>::get_Count()
#define ImmutableList_1_get_Count_m589544421(__this, method) ((  int32_t (*) (ImmutableList_1_t1593135615 *, const MethodInfo*))ImmutableList_1_get_Count_m749861468_gshared)(__this, method)
