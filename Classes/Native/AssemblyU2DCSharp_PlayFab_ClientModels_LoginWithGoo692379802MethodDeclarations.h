﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithGoogleAccountRequest
struct LoginWithGoogleAccountRequest_t692379802;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::.ctor()
extern "C"  void LoginWithGoogleAccountRequest__ctor_m3359589839 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::get_TitleId()
extern "C"  String_t* LoginWithGoogleAccountRequest_get_TitleId_m2464370516 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::set_TitleId(System.String)
extern "C"  void LoginWithGoogleAccountRequest_set_TitleId_m2424149183 (LoginWithGoogleAccountRequest_t692379802 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::get_AccessToken()
extern "C"  String_t* LoginWithGoogleAccountRequest_get_AccessToken_m2842926134 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::set_AccessToken(System.String)
extern "C"  void LoginWithGoogleAccountRequest_set_AccessToken_m852629149 (LoginWithGoogleAccountRequest_t692379802 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithGoogleAccountRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithGoogleAccountRequest_get_CreateAccount_m3278260460 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithGoogleAccountRequest_set_CreateAccount_m4207863783 (LoginWithGoogleAccountRequest_t692379802 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::get_PublisherId()
extern "C"  String_t* LoginWithGoogleAccountRequest_get_PublisherId_m2370936056 (LoginWithGoogleAccountRequest_t692379802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithGoogleAccountRequest::set_PublisherId(System.String)
extern "C"  void LoginWithGoogleAccountRequest_set_PublisherId_m835117339 (LoginWithGoogleAccountRequest_t692379802 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
