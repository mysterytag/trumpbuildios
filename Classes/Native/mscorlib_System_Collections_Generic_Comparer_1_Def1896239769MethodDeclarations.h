﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.Unit>
struct DefaultComparer_t1896239770;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.Unit>::.ctor()
extern "C"  void DefaultComparer__ctor_m636591586_gshared (DefaultComparer_t1896239770 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m636591586(__this, method) ((  void (*) (DefaultComparer_t1896239770 *, const MethodInfo*))DefaultComparer__ctor_m636591586_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UniRx.Unit>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3431630549_gshared (DefaultComparer_t1896239770 * __this, Unit_t2558286038  ___x0, Unit_t2558286038  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3431630549(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1896239770 *, Unit_t2558286038 , Unit_t2558286038 , const MethodInfo*))DefaultComparer_Compare_m3431630549_gshared)(__this, ___x0, ___y1, method)
