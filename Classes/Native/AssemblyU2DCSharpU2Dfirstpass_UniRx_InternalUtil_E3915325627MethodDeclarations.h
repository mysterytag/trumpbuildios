﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<System.Boolean>
struct EmptyObserver_1_t3915325627;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m469315673_gshared (EmptyObserver_1_t3915325627 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m469315673(__this, method) ((  void (*) (EmptyObserver_1_t3915325627 *, const MethodInfo*))EmptyObserver_1__ctor_m469315673_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1181787764_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m1181787764(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m1181787764_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2102066467_gshared (EmptyObserver_1_t3915325627 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2102066467(__this, method) ((  void (*) (EmptyObserver_1_t3915325627 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2102066467_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m340529616_gshared (EmptyObserver_1_t3915325627 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m340529616(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t3915325627 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m340529616_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Boolean>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2871498945_gshared (EmptyObserver_1_t3915325627 * __this, bool ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2871498945(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t3915325627 *, bool, const MethodInfo*))EmptyObserver_1_OnNext_m2871498945_gshared)(__this, ___value0, method)
