﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<EveryUpdateCore>c__Iterator11
struct  U3CEveryUpdateCoreU3Ec__Iterator11_t819860698  : public Il2CppObject
{
public:
	// UniRx.CancellationToken UniRx.Observable/<EveryUpdateCore>c__Iterator11::cancellationToken
	CancellationToken_t1439151560  ___cancellationToken_0;
	// System.Int64 UniRx.Observable/<EveryUpdateCore>c__Iterator11::<count>__0
	int64_t ___U3CcountU3E__0_1;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<EveryUpdateCore>c__Iterator11::observer
	Il2CppObject* ___observer_2;
	// System.Int32 UniRx.Observable/<EveryUpdateCore>c__Iterator11::$PC
	int32_t ___U24PC_3;
	// System.Object UniRx.Observable/<EveryUpdateCore>c__Iterator11::$current
	Il2CppObject * ___U24current_4;
	// UniRx.CancellationToken UniRx.Observable/<EveryUpdateCore>c__Iterator11::<$>cancellationToken
	CancellationToken_t1439151560  ___U3CU24U3EcancellationToken_5;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<EveryUpdateCore>c__Iterator11::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_6;

public:
	inline static int32_t get_offset_of_cancellationToken_0() { return static_cast<int32_t>(offsetof(U3CEveryUpdateCoreU3Ec__Iterator11_t819860698, ___cancellationToken_0)); }
	inline CancellationToken_t1439151560  get_cancellationToken_0() const { return ___cancellationToken_0; }
	inline CancellationToken_t1439151560 * get_address_of_cancellationToken_0() { return &___cancellationToken_0; }
	inline void set_cancellationToken_0(CancellationToken_t1439151560  value)
	{
		___cancellationToken_0 = value;
	}

	inline static int32_t get_offset_of_U3CcountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CEveryUpdateCoreU3Ec__Iterator11_t819860698, ___U3CcountU3E__0_1)); }
	inline int64_t get_U3CcountU3E__0_1() const { return ___U3CcountU3E__0_1; }
	inline int64_t* get_address_of_U3CcountU3E__0_1() { return &___U3CcountU3E__0_1; }
	inline void set_U3CcountU3E__0_1(int64_t value)
	{
		___U3CcountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_observer_2() { return static_cast<int32_t>(offsetof(U3CEveryUpdateCoreU3Ec__Iterator11_t819860698, ___observer_2)); }
	inline Il2CppObject* get_observer_2() const { return ___observer_2; }
	inline Il2CppObject** get_address_of_observer_2() { return &___observer_2; }
	inline void set_observer_2(Il2CppObject* value)
	{
		___observer_2 = value;
		Il2CppCodeGenWriteBarrier(&___observer_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CEveryUpdateCoreU3Ec__Iterator11_t819860698, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CEveryUpdateCoreU3Ec__Iterator11_t819860698, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcancellationToken_5() { return static_cast<int32_t>(offsetof(U3CEveryUpdateCoreU3Ec__Iterator11_t819860698, ___U3CU24U3EcancellationToken_5)); }
	inline CancellationToken_t1439151560  get_U3CU24U3EcancellationToken_5() const { return ___U3CU24U3EcancellationToken_5; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3EcancellationToken_5() { return &___U3CU24U3EcancellationToken_5; }
	inline void set_U3CU24U3EcancellationToken_5(CancellationToken_t1439151560  value)
	{
		___U3CU24U3EcancellationToken_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_6() { return static_cast<int32_t>(offsetof(U3CEveryUpdateCoreU3Ec__Iterator11_t819860698, ___U3CU24U3Eobserver_6)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_6() const { return ___U3CU24U3Eobserver_6; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_6() { return &___U3CU24U3Eobserver_6; }
	inline void set_U3CU24U3Eobserver_6(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
