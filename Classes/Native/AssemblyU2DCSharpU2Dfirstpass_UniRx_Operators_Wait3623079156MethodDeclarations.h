﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.Wait`1<System.Object>
struct Wait_1_t3623079156;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.Wait`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan)
extern "C"  void Wait_1__ctor_m451294477_gshared (Wait_1_t3623079156 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___timeout1, const MethodInfo* method);
#define Wait_1__ctor_m451294477(__this, ___source0, ___timeout1, method) ((  void (*) (Wait_1_t3623079156 *, Il2CppObject*, TimeSpan_t763862892 , const MethodInfo*))Wait_1__ctor_m451294477_gshared)(__this, ___source0, ___timeout1, method)
// System.Void UniRx.Operators.Wait`1<System.Object>::.cctor()
extern "C"  void Wait_1__cctor_m2558645205_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Wait_1__cctor_m2558645205(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Wait_1__cctor_m2558645205_gshared)(__this /* static, unused */, method)
// T UniRx.Operators.Wait`1<System.Object>::Run()
extern "C"  Il2CppObject * Wait_1_Run_m1297460480_gshared (Wait_1_t3623079156 * __this, const MethodInfo* method);
#define Wait_1_Run_m1297460480(__this, method) ((  Il2CppObject * (*) (Wait_1_t3623079156 *, const MethodInfo*))Wait_1_Run_m1297460480_gshared)(__this, method)
// System.Void UniRx.Operators.Wait`1<System.Object>::OnNext(T)
extern "C"  void Wait_1_OnNext_m2604406656_gshared (Wait_1_t3623079156 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Wait_1_OnNext_m2604406656(__this, ___value0, method) ((  void (*) (Wait_1_t3623079156 *, Il2CppObject *, const MethodInfo*))Wait_1_OnNext_m2604406656_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.Wait`1<System.Object>::OnError(System.Exception)
extern "C"  void Wait_1_OnError_m643904143_gshared (Wait_1_t3623079156 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Wait_1_OnError_m643904143(__this, ___error0, method) ((  void (*) (Wait_1_t3623079156 *, Exception_t1967233988 *, const MethodInfo*))Wait_1_OnError_m643904143_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.Wait`1<System.Object>::OnCompleted()
extern "C"  void Wait_1_OnCompleted_m1031013474_gshared (Wait_1_t3623079156 * __this, const MethodInfo* method);
#define Wait_1_OnCompleted_m1031013474(__this, method) ((  void (*) (Wait_1_t3623079156 *, const MethodInfo*))Wait_1_OnCompleted_m1031013474_gshared)(__this, method)
