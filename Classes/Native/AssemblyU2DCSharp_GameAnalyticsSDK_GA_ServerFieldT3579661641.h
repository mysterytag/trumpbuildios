﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>
struct Dictionary_2_t1069916240;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.GA_ServerFieldTypes
struct  GA_ServerFieldTypes_t3579661641  : public Il2CppObject
{
public:

public:
};

struct GA_ServerFieldTypes_t3579661641_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String> GameAnalyticsSDK.GA_ServerFieldTypes::Fields
	Dictionary_2_t1069916240 * ___Fields_0;

public:
	inline static int32_t get_offset_of_Fields_0() { return static_cast<int32_t>(offsetof(GA_ServerFieldTypes_t3579661641_StaticFields, ___Fields_0)); }
	inline Dictionary_2_t1069916240 * get_Fields_0() const { return ___Fields_0; }
	inline Dictionary_2_t1069916240 ** get_address_of_Fields_0() { return &___Fields_0; }
	inline void set_Fields_0(Dictionary_2_t1069916240 * value)
	{
		___Fields_0 = value;
		Il2CppCodeGenWriteBarrier(&___Fields_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
