﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.IFactory`1<System.Object>
struct IFactory_1_t2124284520;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryNested`2<System.Object,System.Object>
struct  FactoryNested_2_t2234585871  : public Il2CppObject
{
public:
	// Zenject.IFactory`1<TConcrete> Zenject.FactoryNested`2::_concreteFactory
	Il2CppObject* ____concreteFactory_0;

public:
	inline static int32_t get_offset_of__concreteFactory_0() { return static_cast<int32_t>(offsetof(FactoryNested_2_t2234585871, ____concreteFactory_0)); }
	inline Il2CppObject* get__concreteFactory_0() const { return ____concreteFactory_0; }
	inline Il2CppObject** get_address_of__concreteFactory_0() { return &____concreteFactory_0; }
	inline void set__concreteFactory_0(Il2CppObject* value)
	{
		____concreteFactory_0 = value;
		Il2CppCodeGenWriteBarrier(&____concreteFactory_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
