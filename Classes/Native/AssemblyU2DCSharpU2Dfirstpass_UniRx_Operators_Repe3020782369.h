﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.RepeatSafeObservable`1<System.Object>
struct RepeatSafeObservable_1_t4177995194;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;
// System.Action
struct Action_t437523947;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RepeatSafeObservable`1/RepeatSafe<System.Object>
struct  RepeatSafe_t3020782369  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.RepeatSafeObservable`1<T> UniRx.Operators.RepeatSafeObservable`1/RepeatSafe::parent
	RepeatSafeObservable_1_t4177995194 * ___parent_2;
	// System.Object UniRx.Operators.RepeatSafeObservable`1/RepeatSafe::gate
	Il2CppObject * ___gate_3;
	// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Operators.RepeatSafeObservable`1/RepeatSafe::e
	Il2CppObject* ___e_4;
	// UniRx.SerialDisposable UniRx.Operators.RepeatSafeObservable`1/RepeatSafe::subscription
	SerialDisposable_t2547852742 * ___subscription_5;
	// System.Action UniRx.Operators.RepeatSafeObservable`1/RepeatSafe::nextSelf
	Action_t437523947 * ___nextSelf_6;
	// System.Boolean UniRx.Operators.RepeatSafeObservable`1/RepeatSafe::isDisposed
	bool ___isDisposed_7;
	// System.Boolean UniRx.Operators.RepeatSafeObservable`1/RepeatSafe::isRunNext
	bool ___isRunNext_8;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(RepeatSafe_t3020782369, ___parent_2)); }
	inline RepeatSafeObservable_1_t4177995194 * get_parent_2() const { return ___parent_2; }
	inline RepeatSafeObservable_1_t4177995194 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(RepeatSafeObservable_1_t4177995194 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(RepeatSafe_t3020782369, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(RepeatSafe_t3020782369, ___e_4)); }
	inline Il2CppObject* get_e_4() const { return ___e_4; }
	inline Il2CppObject** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(Il2CppObject* value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier(&___e_4, value);
	}

	inline static int32_t get_offset_of_subscription_5() { return static_cast<int32_t>(offsetof(RepeatSafe_t3020782369, ___subscription_5)); }
	inline SerialDisposable_t2547852742 * get_subscription_5() const { return ___subscription_5; }
	inline SerialDisposable_t2547852742 ** get_address_of_subscription_5() { return &___subscription_5; }
	inline void set_subscription_5(SerialDisposable_t2547852742 * value)
	{
		___subscription_5 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_5, value);
	}

	inline static int32_t get_offset_of_nextSelf_6() { return static_cast<int32_t>(offsetof(RepeatSafe_t3020782369, ___nextSelf_6)); }
	inline Action_t437523947 * get_nextSelf_6() const { return ___nextSelf_6; }
	inline Action_t437523947 ** get_address_of_nextSelf_6() { return &___nextSelf_6; }
	inline void set_nextSelf_6(Action_t437523947 * value)
	{
		___nextSelf_6 = value;
		Il2CppCodeGenWriteBarrier(&___nextSelf_6, value);
	}

	inline static int32_t get_offset_of_isDisposed_7() { return static_cast<int32_t>(offsetof(RepeatSafe_t3020782369, ___isDisposed_7)); }
	inline bool get_isDisposed_7() const { return ___isDisposed_7; }
	inline bool* get_address_of_isDisposed_7() { return &___isDisposed_7; }
	inline void set_isDisposed_7(bool value)
	{
		___isDisposed_7 = value;
	}

	inline static int32_t get_offset_of_isRunNext_8() { return static_cast<int32_t>(offsetof(RepeatSafe_t3020782369, ___isRunNext_8)); }
	inline bool get_isRunNext_8() const { return ___isRunNext_8; }
	inline bool* get_address_of_isRunNext_8() { return &___isRunNext_8; }
	inline void set_isRunNext_8(bool value)
	{
		___isRunNext_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
