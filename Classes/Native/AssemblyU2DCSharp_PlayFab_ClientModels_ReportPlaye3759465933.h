﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.ReportPlayerClientResult
struct  ReportPlayerClientResult_t3759465933  : public PlayFabResultCommon_t379512675
{
public:
	// System.Boolean PlayFab.ClientModels.ReportPlayerClientResult::<Updated>k__BackingField
	bool ___U3CUpdatedU3Ek__BackingField_2;
	// System.Int32 PlayFab.ClientModels.ReportPlayerClientResult::<SubmissionsRemaining>k__BackingField
	int32_t ___U3CSubmissionsRemainingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CUpdatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReportPlayerClientResult_t3759465933, ___U3CUpdatedU3Ek__BackingField_2)); }
	inline bool get_U3CUpdatedU3Ek__BackingField_2() const { return ___U3CUpdatedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CUpdatedU3Ek__BackingField_2() { return &___U3CUpdatedU3Ek__BackingField_2; }
	inline void set_U3CUpdatedU3Ek__BackingField_2(bool value)
	{
		___U3CUpdatedU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CSubmissionsRemainingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ReportPlayerClientResult_t3759465933, ___U3CSubmissionsRemainingU3Ek__BackingField_3)); }
	inline int32_t get_U3CSubmissionsRemainingU3Ek__BackingField_3() const { return ___U3CSubmissionsRemainingU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CSubmissionsRemainingU3Ek__BackingField_3() { return &___U3CSubmissionsRemainingU3Ek__BackingField_3; }
	inline void set_U3CSubmissionsRemainingU3Ek__BackingField_3(int32_t value)
	{
		___U3CSubmissionsRemainingU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
