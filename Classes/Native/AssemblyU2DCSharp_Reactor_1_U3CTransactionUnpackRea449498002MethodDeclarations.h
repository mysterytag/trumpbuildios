﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.CollectionAddEvent`1<System.Object>>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1609400899_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1609400899(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1609400899_gshared)(__this, method)
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.CollectionAddEvent`1<System.Object>>::<>m__1D6()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m3772628665_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m3772628665(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t449498002 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m3772628665_gshared)(__this, method)
