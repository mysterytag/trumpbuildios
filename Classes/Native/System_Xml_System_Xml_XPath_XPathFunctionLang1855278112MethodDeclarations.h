﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionLang
struct XPathFunctionLang_t1855278112;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionLang::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionLang__ctor_m915197874 (XPathFunctionLang_t1855278112 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLang::get_ReturnType()
extern "C"  int32_t XPathFunctionLang_get_ReturnType_m4120654770 (XPathFunctionLang_t1855278112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionLang::get_Peer()
extern "C"  bool XPathFunctionLang_get_Peer_m3306550062 (XPathFunctionLang_t1855278112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionLang::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionLang_Evaluate_m1010965763 (XPathFunctionLang_t1855278112 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionLang::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool XPathFunctionLang_EvaluateBoolean_m1037770014 (XPathFunctionLang_t1855278112 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionLang::ToString()
extern "C"  String_t* XPathFunctionLang_ToString_m1720286218 (XPathFunctionLang_t1855278112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
