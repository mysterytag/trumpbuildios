﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t792326996;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo4162640357.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo
struct  OnStateInfo_t3845837216  : public Il2CppObject
{
public:
	// UnityEngine.Animator UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::<Animator>k__BackingField
	Animator_t792326996 * ___U3CAnimatorU3Ek__BackingField_0;
	// UnityEngine.AnimatorStateInfo UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::<StateInfo>k__BackingField
	AnimatorStateInfo_t4162640357  ___U3CStateInfoU3Ek__BackingField_1;
	// System.Int32 UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::<LayerIndex>k__BackingField
	int32_t ___U3CLayerIndexU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAnimatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OnStateInfo_t3845837216, ___U3CAnimatorU3Ek__BackingField_0)); }
	inline Animator_t792326996 * get_U3CAnimatorU3Ek__BackingField_0() const { return ___U3CAnimatorU3Ek__BackingField_0; }
	inline Animator_t792326996 ** get_address_of_U3CAnimatorU3Ek__BackingField_0() { return &___U3CAnimatorU3Ek__BackingField_0; }
	inline void set_U3CAnimatorU3Ek__BackingField_0(Animator_t792326996 * value)
	{
		___U3CAnimatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAnimatorU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CStateInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OnStateInfo_t3845837216, ___U3CStateInfoU3Ek__BackingField_1)); }
	inline AnimatorStateInfo_t4162640357  get_U3CStateInfoU3Ek__BackingField_1() const { return ___U3CStateInfoU3Ek__BackingField_1; }
	inline AnimatorStateInfo_t4162640357 * get_address_of_U3CStateInfoU3Ek__BackingField_1() { return &___U3CStateInfoU3Ek__BackingField_1; }
	inline void set_U3CStateInfoU3Ek__BackingField_1(AnimatorStateInfo_t4162640357  value)
	{
		___U3CStateInfoU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLayerIndexU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OnStateInfo_t3845837216, ___U3CLayerIndexU3Ek__BackingField_2)); }
	inline int32_t get_U3CLayerIndexU3Ek__BackingField_2() const { return ___U3CLayerIndexU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLayerIndexU3Ek__BackingField_2() { return &___U3CLayerIndexU3Ek__BackingField_2; }
	inline void set_U3CLayerIndexU3Ek__BackingField_2(int32_t value)
	{
		___U3CLayerIndexU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
