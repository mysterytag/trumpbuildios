﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>
struct SkipUntilOuterObserver_t2866202317;
// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>
struct SkipUntil_t2756386132;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther<System.Object,System.Object>
struct  SkipUntilOther_t2413447818  : public Il2CppObject
{
public:
	// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<T,TOther> UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther::parent
	SkipUntilOuterObserver_t2866202317 * ___parent_0;
	// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<T,TOther> UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther::sourceObserver
	SkipUntil_t2756386132 * ___sourceObserver_1;
	// System.IDisposable UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntilOther::subscription
	Il2CppObject * ___subscription_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(SkipUntilOther_t2413447818, ___parent_0)); }
	inline SkipUntilOuterObserver_t2866202317 * get_parent_0() const { return ___parent_0; }
	inline SkipUntilOuterObserver_t2866202317 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(SkipUntilOuterObserver_t2866202317 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_sourceObserver_1() { return static_cast<int32_t>(offsetof(SkipUntilOther_t2413447818, ___sourceObserver_1)); }
	inline SkipUntil_t2756386132 * get_sourceObserver_1() const { return ___sourceObserver_1; }
	inline SkipUntil_t2756386132 ** get_address_of_sourceObserver_1() { return &___sourceObserver_1; }
	inline void set_sourceObserver_1(SkipUntil_t2756386132 * value)
	{
		___sourceObserver_1 = value;
		Il2CppCodeGenWriteBarrier(&___sourceObserver_1, value);
	}

	inline static int32_t get_offset_of_subscription_2() { return static_cast<int32_t>(offsetof(SkipUntilOther_t2413447818, ___subscription_2)); }
	inline Il2CppObject * get_subscription_2() const { return ___subscription_2; }
	inline Il2CppObject ** get_address_of_subscription_2() { return &___subscription_2; }
	inline void set_subscription_2(Il2CppObject * value)
	{
		___subscription_2 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
