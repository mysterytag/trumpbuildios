﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellUtils.CellJoinDisposable`1<System.Int32>
struct CellJoinDisposable_1_t1451834474;

#include "codegen/il2cpp-codegen.h"

// System.Void CellUtils.CellJoinDisposable`1<System.Int32>::.ctor()
extern "C"  void CellJoinDisposable_1__ctor_m1277547980_gshared (CellJoinDisposable_1_t1451834474 * __this, const MethodInfo* method);
#define CellJoinDisposable_1__ctor_m1277547980(__this, method) ((  void (*) (CellJoinDisposable_1_t1451834474 *, const MethodInfo*))CellJoinDisposable_1__ctor_m1277547980_gshared)(__this, method)
