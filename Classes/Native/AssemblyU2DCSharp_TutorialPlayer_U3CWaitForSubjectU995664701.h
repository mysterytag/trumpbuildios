﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPlayer/<WaitForSubject>c__AnonStoreyEE
struct  U3CWaitForSubjectU3Ec__AnonStoreyEE_t995664701  : public Il2CppObject
{
public:
	// System.Boolean TutorialPlayer/<WaitForSubject>c__AnonStoreyEE::b
	bool ___b_0;

public:
	inline static int32_t get_offset_of_b_0() { return static_cast<int32_t>(offsetof(U3CWaitForSubjectU3Ec__AnonStoreyEE_t995664701, ___b_0)); }
	inline bool get_b_0() const { return ___b_0; }
	inline bool* get_address_of_b_0() { return &___b_0; }
	inline void set_b_0(bool value)
	{
		___b_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
