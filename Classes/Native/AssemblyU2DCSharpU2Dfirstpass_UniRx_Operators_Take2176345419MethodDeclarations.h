﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>
struct TakeLast__t2176345419;
// UniRx.Operators.TakeLastObservable`1<System.Object>
struct TakeLastObservable_1_t3265267047;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::.ctor(UniRx.Operators.TakeLastObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeLast___ctor_m2106102018_gshared (TakeLast__t2176345419 * __this, TakeLastObservable_1_t3265267047 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define TakeLast___ctor_m2106102018(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (TakeLast__t2176345419 *, TakeLastObservable_1_t3265267047 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeLast___ctor_m2106102018_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::Run()
extern "C"  Il2CppObject * TakeLast__Run_m144593786_gshared (TakeLast__t2176345419 * __this, const MethodInfo* method);
#define TakeLast__Run_m144593786(__this, method) ((  Il2CppObject * (*) (TakeLast__t2176345419 *, const MethodInfo*))TakeLast__Run_m144593786_gshared)(__this, method)
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::OnNext(T)
extern "C"  void TakeLast__OnNext_m3517845822_gshared (TakeLast__t2176345419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define TakeLast__OnNext_m3517845822(__this, ___value0, method) ((  void (*) (TakeLast__t2176345419 *, Il2CppObject *, const MethodInfo*))TakeLast__OnNext_m3517845822_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::OnError(System.Exception)
extern "C"  void TakeLast__OnError_m1909715021_gshared (TakeLast__t2176345419 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeLast__OnError_m1909715021(__this, ___error0, method) ((  void (*) (TakeLast__t2176345419 *, Exception_t1967233988 *, const MethodInfo*))TakeLast__OnError_m1909715021_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::OnCompleted()
extern "C"  void TakeLast__OnCompleted_m2461462304_gshared (TakeLast__t2176345419 * __this, const MethodInfo* method);
#define TakeLast__OnCompleted_m2461462304(__this, method) ((  void (*) (TakeLast__t2176345419 *, const MethodInfo*))TakeLast__OnCompleted_m2461462304_gshared)(__this, method)
// System.Void UniRx.Operators.TakeLastObservable`1/TakeLast_<System.Object>::Trim(System.TimeSpan)
extern "C"  void TakeLast__Trim_m323671436_gshared (TakeLast__t2176345419 * __this, TimeSpan_t763862892  ___now0, const MethodInfo* method);
#define TakeLast__Trim_m323671436(__this, ___now0, method) ((  void (*) (TakeLast__t2176345419 *, TimeSpan_t763862892 , const MethodInfo*))TakeLast__Trim_m323671436_gshared)(__this, ___now0, method)
