﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SingletonInstanceHelper
struct SingletonInstanceHelper_t833281251;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.SingletonInstanceHelper::.ctor()
extern "C"  void SingletonInstanceHelper__ctor_m581640156 (SingletonInstanceHelper_t833281251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
