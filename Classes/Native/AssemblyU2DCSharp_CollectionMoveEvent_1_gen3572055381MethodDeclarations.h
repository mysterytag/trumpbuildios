﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CollectionMoveEvent`1<System.Object>
struct CollectionMoveEvent_1_t3572055381;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CollectionMoveEvent`1<System.Object>::.ctor(System.Int32,System.Int32,T)
extern "C"  void CollectionMoveEvent_1__ctor_m2094255715_gshared (CollectionMoveEvent_1_t3572055381 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, Il2CppObject * ___value2, const MethodInfo* method);
#define CollectionMoveEvent_1__ctor_m2094255715(__this, ___oldIndex0, ___newIndex1, ___value2, method) ((  void (*) (CollectionMoveEvent_1_t3572055381 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))CollectionMoveEvent_1__ctor_m2094255715_gshared)(__this, ___oldIndex0, ___newIndex1, ___value2, method)
// System.Int32 CollectionMoveEvent`1<System.Object>::get_OldIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_OldIndex_m1896787371_gshared (CollectionMoveEvent_1_t3572055381 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_OldIndex_m1896787371(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))CollectionMoveEvent_1_get_OldIndex_m1896787371_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<System.Object>::set_OldIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_OldIndex_m3746196962_gshared (CollectionMoveEvent_1_t3572055381 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_OldIndex_m3746196962(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3572055381 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_OldIndex_m3746196962_gshared)(__this, ___value0, method)
// System.Int32 CollectionMoveEvent`1<System.Object>::get_NewIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_NewIndex_m614238226_gshared (CollectionMoveEvent_1_t3572055381 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_NewIndex_m614238226(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))CollectionMoveEvent_1_get_NewIndex_m614238226_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<System.Object>::set_NewIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_NewIndex_m832103881_gshared (CollectionMoveEvent_1_t3572055381 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_NewIndex_m832103881(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3572055381 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_NewIndex_m832103881_gshared)(__this, ___value0, method)
// T CollectionMoveEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionMoveEvent_1_get_Value_m3472123074_gshared (CollectionMoveEvent_1_t3572055381 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_get_Value_m3472123074(__this, method) ((  Il2CppObject * (*) (CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))CollectionMoveEvent_1_get_Value_m3472123074_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionMoveEvent_1_set_Value_m4226272145_gshared (CollectionMoveEvent_1_t3572055381 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionMoveEvent_1_set_Value_m4226272145(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3572055381 *, Il2CppObject *, const MethodInfo*))CollectionMoveEvent_1_set_Value_m4226272145_gshared)(__this, ___value0, method)
// System.String CollectionMoveEvent`1<System.Object>::ToString()
extern "C"  String_t* CollectionMoveEvent_1_ToString_m512063864_gshared (CollectionMoveEvent_1_t3572055381 * __this, const MethodInfo* method);
#define CollectionMoveEvent_1_ToString_m512063864(__this, method) ((  String_t* (*) (CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))CollectionMoveEvent_1_ToString_m512063864_gshared)(__this, method)
