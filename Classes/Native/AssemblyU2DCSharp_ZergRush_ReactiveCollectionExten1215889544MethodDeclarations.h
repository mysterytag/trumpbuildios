﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>
struct FilterCollection_1_t1215889544;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t2564974692;
// System.Action`1<CollectionRemoveEvent`1<System.Object>>
struct Action_1_t1046711963;
// System.Action`1<CollectionMoveEvent`1<System.Object>>
struct Action_1_t3720508086;
// System.Action`1<CollectionReplaceEvent`1<System.Object>>
struct Action_1_t2645824775;
// System.Action
struct Action_t437523947;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::.ctor()
extern "C"  void FilterCollection_1__ctor_m3312569187_gshared (FilterCollection_1_t1215889544 * __this, const MethodInfo* method);
#define FilterCollection_1__ctor_m3312569187(__this, method) ((  void (*) (FilterCollection_1_t1215889544 *, const MethodInfo*))FilterCollection_1__ctor_m3312569187_gshared)(__this, method)
// System.Collections.IEnumerator ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FilterCollection_1_System_Collections_IEnumerable_GetEnumerator_m3622164014_gshared (FilterCollection_1_t1215889544 * __this, const MethodInfo* method);
#define FilterCollection_1_System_Collections_IEnumerable_GetEnumerator_m3622164014(__this, method) ((  Il2CppObject * (*) (FilterCollection_1_t1215889544 *, const MethodInfo*))FilterCollection_1_System_Collections_IEnumerable_GetEnumerator_m3622164014_gshared)(__this, method)
// System.IDisposable ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::Listen(System.Action`1<UniRx.CollectionAddEvent`1<T>>,System.Action`1<CollectionRemoveEvent`1<T>>,System.Action`1<CollectionMoveEvent`1<T>>,System.Action`1<CollectionReplaceEvent`1<T>>,System.Action)
extern "C"  Il2CppObject * FilterCollection_1_Listen_m3372260382_gshared (FilterCollection_1_t1215889544 * __this, Action_1_t2564974692 * ___onAdd0, Action_1_t1046711963 * ___onRemove1, Action_1_t3720508086 * ___onMove2, Action_1_t2645824775 * ___onReplace3, Action_t437523947 * ___onReset4, const MethodInfo* method);
#define FilterCollection_1_Listen_m3372260382(__this, ___onAdd0, ___onRemove1, ___onMove2, ___onReplace3, ___onReset4, method) ((  Il2CppObject * (*) (FilterCollection_1_t1215889544 *, Action_1_t2564974692 *, Action_1_t1046711963 *, Action_1_t3720508086 *, Action_1_t2645824775 *, Action_t437523947 *, const MethodInfo*))FilterCollection_1_Listen_m3372260382_gshared)(__this, ___onAdd0, ___onRemove1, ___onMove2, ___onReplace3, ___onReset4, method)
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* FilterCollection_1_GetEnumerator_m2686019359_gshared (FilterCollection_1_t1215889544 * __this, const MethodInfo* method);
#define FilterCollection_1_GetEnumerator_m2686019359(__this, method) ((  Il2CppObject* (*) (FilterCollection_1_t1215889544 *, const MethodInfo*))FilterCollection_1_GetEnumerator_m2686019359_gshared)(__this, method)
// System.Int32 ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::get_Count()
extern "C"  int32_t FilterCollection_1_get_Count_m3310743833_gshared (FilterCollection_1_t1215889544 * __this, const MethodInfo* method);
#define FilterCollection_1_get_Count_m3310743833(__this, method) ((  int32_t (*) (FilterCollection_1_t1215889544 *, const MethodInfo*))FilterCollection_1_get_Count_m3310743833_gshared)(__this, method)
// T ZergRush.ReactiveCollectionExtension/FilterCollection`1<System.Object>::ElementAt(System.Int32)
extern "C"  Il2CppObject * FilterCollection_1_ElementAt_m760621826_gshared (FilterCollection_1_t1215889544 * __this, int32_t ___index0, const MethodInfo* method);
#define FilterCollection_1_ElementAt_m760621826(__this, ___index0, method) ((  Il2CppObject * (*) (FilterCollection_1_t1215889544 *, int32_t, const MethodInfo*))FilterCollection_1_ElementAt_m760621826_gshared)(__this, ___index0, method)
