﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1772335823MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m670953128(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3187348769 *, Dictionary_2_t3420320828 *, const MethodInfo*))Enumerator__ctor_m3964067898_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3671724931(__this, method) ((  Il2CppObject * (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1343675569_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2252261645(__this, method) ((  void (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1506656315_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1136932036(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1590144242_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3307168287(__this, method) ((  Il2CppObject * (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2738884813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m961361137(__this, method) ((  Il2CppObject * (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m301789215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::MoveNext()
#define Enumerator_MoveNext_m2182375613(__this, method) ((  bool (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_MoveNext_m2642109931_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::get_Current()
#define Enumerator_get_Current_m1721380959(__this, method) ((  KeyValuePair_2_t2908852126  (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_get_Current_m3682831217_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2595657990(__this, method) ((  String_t* (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_get_CurrentKey_m4039755444_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m473662790(__this, method) ((  KeyValuePair_2_t1782622924  (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_get_CurrentValue_m2295259764_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::Reset()
#define Enumerator_Reset_m3338865914(__this, method) ((  void (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_Reset_m950125452_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::VerifyState()
#define Enumerator_VerifyState_m3732093315(__this, method) ((  void (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_VerifyState_m4211030677_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2988855339(__this, method) ((  void (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_VerifyCurrent_m3686159549_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>::Dispose()
#define Enumerator_Dispose_m2833002314(__this, method) ((  void (*) (Enumerator_t3187348769 *, const MethodInfo*))Enumerator_Dispose_m765954396_gshared)(__this, method)
