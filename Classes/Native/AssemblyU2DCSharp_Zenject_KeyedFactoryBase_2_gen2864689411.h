﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Object,System.Type>>
struct List_1_t1028126887;
// System.Collections.Generic.Dictionary`2<System.Object,System.Type>
struct Dictionary_2_t1471581369;
// System.Type
struct Type_t;
// System.Func`2<ModestTree.Util.Tuple`2<System.Object,System.Type>,System.Object>
struct Func_2_t361610966;
// System.Func`2<System.Object,System.String>
struct Func_2_t2267165834;
// System.Func`2<ModestTree.Util.Tuple`2<System.Object,System.Type>,System.Type>
struct Func_2_t2303734481;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.KeyedFactoryBase`2<System.Object,System.Object>
struct  KeyedFactoryBase_2_t2864689411  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.KeyedFactoryBase`2::_container
	DiContainer_t2383114449 * ____container_0;
	// System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<TKey,System.Type>> Zenject.KeyedFactoryBase`2::_typePairs
	List_1_t1028126887 * ____typePairs_1;
	// System.Collections.Generic.Dictionary`2<TKey,System.Type> Zenject.KeyedFactoryBase`2::_typeMap
	Dictionary_2_t1471581369 * ____typeMap_2;
	// System.Type Zenject.KeyedFactoryBase`2::_fallbackType
	Type_t * ____fallbackType_3;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(KeyedFactoryBase_2_t2864689411, ____container_0)); }
	inline DiContainer_t2383114449 * get__container_0() const { return ____container_0; }
	inline DiContainer_t2383114449 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t2383114449 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier(&____container_0, value);
	}

	inline static int32_t get_offset_of__typePairs_1() { return static_cast<int32_t>(offsetof(KeyedFactoryBase_2_t2864689411, ____typePairs_1)); }
	inline List_1_t1028126887 * get__typePairs_1() const { return ____typePairs_1; }
	inline List_1_t1028126887 ** get_address_of__typePairs_1() { return &____typePairs_1; }
	inline void set__typePairs_1(List_1_t1028126887 * value)
	{
		____typePairs_1 = value;
		Il2CppCodeGenWriteBarrier(&____typePairs_1, value);
	}

	inline static int32_t get_offset_of__typeMap_2() { return static_cast<int32_t>(offsetof(KeyedFactoryBase_2_t2864689411, ____typeMap_2)); }
	inline Dictionary_2_t1471581369 * get__typeMap_2() const { return ____typeMap_2; }
	inline Dictionary_2_t1471581369 ** get_address_of__typeMap_2() { return &____typeMap_2; }
	inline void set__typeMap_2(Dictionary_2_t1471581369 * value)
	{
		____typeMap_2 = value;
		Il2CppCodeGenWriteBarrier(&____typeMap_2, value);
	}

	inline static int32_t get_offset_of__fallbackType_3() { return static_cast<int32_t>(offsetof(KeyedFactoryBase_2_t2864689411, ____fallbackType_3)); }
	inline Type_t * get__fallbackType_3() const { return ____fallbackType_3; }
	inline Type_t ** get_address_of__fallbackType_3() { return &____fallbackType_3; }
	inline void set__fallbackType_3(Type_t * value)
	{
		____fallbackType_3 = value;
		Il2CppCodeGenWriteBarrier(&____fallbackType_3, value);
	}
};

struct KeyedFactoryBase_2_t2864689411_StaticFields
{
public:
	// System.Func`2<ModestTree.Util.Tuple`2<TKey,System.Type>,TKey> Zenject.KeyedFactoryBase`2::<>f__am$cache4
	Func_2_t361610966 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<TKey,System.String> Zenject.KeyedFactoryBase`2::<>f__am$cache5
	Func_2_t2267165834 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<ModestTree.Util.Tuple`2<TKey,System.Type>,TKey> Zenject.KeyedFactoryBase`2::<>f__am$cache6
	Func_2_t361610966 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<ModestTree.Util.Tuple`2<TKey,System.Type>,System.Type> Zenject.KeyedFactoryBase`2::<>f__am$cache7
	Func_2_t2303734481 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(KeyedFactoryBase_2_t2864689411_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t361610966 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t361610966 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t361610966 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(KeyedFactoryBase_2_t2864689411_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t2267165834 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t2267165834 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t2267165834 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(KeyedFactoryBase_2_t2864689411_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t361610966 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t361610966 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t361610966 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(KeyedFactoryBase_2_t2864689411_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t2303734481 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t2303734481 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t2303734481 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
