﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey41_2__ctor_m4176316363_gshared (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey41_2__ctor_m4176316363(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey41_2__ctor_m4176316363_gshared)(__this, method)
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2<System.Object,System.Object>::<>m__41(T1)
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m3548228373_gshared (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m3548228373(__this, ___x0, method) ((  Il2CppObject* (*) (U3CFromAsyncPatternU3Ec__AnonStorey41_2_t1532408781 *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey41_2_U3CU3Em__41_m3548228373_gshared)(__this, ___x0, method)
