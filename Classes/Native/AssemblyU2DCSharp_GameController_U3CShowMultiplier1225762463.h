﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object
struct Il2CppObject;
// GameController
struct GameController_t2782302542;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/<ShowMultiplierAnimation>c__Iterator13
struct  U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 GameController/<ShowMultiplierAnimation>c__Iterator13::<pos>__0
	Vector3_t3525329789  ___U3CposU3E__0_0;
	// UnityEngine.GameObject GameController/<ShowMultiplierAnimation>c__Iterator13::mult
	GameObject_t4012695102 * ___mult_1;
	// UnityEngine.GameObject GameController/<ShowMultiplierAnimation>c__Iterator13::<m>__1
	GameObject_t4012695102 * ___U3CmU3E__1_2;
	// System.Int32 GameController/<ShowMultiplierAnimation>c__Iterator13::$PC
	int32_t ___U24PC_3;
	// System.Object GameController/<ShowMultiplierAnimation>c__Iterator13::$current
	Il2CppObject * ___U24current_4;
	// UnityEngine.GameObject GameController/<ShowMultiplierAnimation>c__Iterator13::<$>mult
	GameObject_t4012695102 * ___U3CU24U3Emult_5;
	// GameController GameController/<ShowMultiplierAnimation>c__Iterator13::<>f__this
	GameController_t2782302542 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CposU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463, ___U3CposU3E__0_0)); }
	inline Vector3_t3525329789  get_U3CposU3E__0_0() const { return ___U3CposU3E__0_0; }
	inline Vector3_t3525329789 * get_address_of_U3CposU3E__0_0() { return &___U3CposU3E__0_0; }
	inline void set_U3CposU3E__0_0(Vector3_t3525329789  value)
	{
		___U3CposU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_mult_1() { return static_cast<int32_t>(offsetof(U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463, ___mult_1)); }
	inline GameObject_t4012695102 * get_mult_1() const { return ___mult_1; }
	inline GameObject_t4012695102 ** get_address_of_mult_1() { return &___mult_1; }
	inline void set_mult_1(GameObject_t4012695102 * value)
	{
		___mult_1 = value;
		Il2CppCodeGenWriteBarrier(&___mult_1, value);
	}

	inline static int32_t get_offset_of_U3CmU3E__1_2() { return static_cast<int32_t>(offsetof(U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463, ___U3CmU3E__1_2)); }
	inline GameObject_t4012695102 * get_U3CmU3E__1_2() const { return ___U3CmU3E__1_2; }
	inline GameObject_t4012695102 ** get_address_of_U3CmU3E__1_2() { return &___U3CmU3E__1_2; }
	inline void set_U3CmU3E__1_2(GameObject_t4012695102 * value)
	{
		___U3CmU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Emult_5() { return static_cast<int32_t>(offsetof(U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463, ___U3CU24U3Emult_5)); }
	inline GameObject_t4012695102 * get_U3CU24U3Emult_5() const { return ___U3CU24U3Emult_5; }
	inline GameObject_t4012695102 ** get_address_of_U3CU24U3Emult_5() { return &___U3CU24U3Emult_5; }
	inline void set_U3CU24U3Emult_5(GameObject_t4012695102 * value)
	{
		___U3CU24U3Emult_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Emult_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463, ___U3CU3Ef__this_6)); }
	inline GameController_t2782302542 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline GameController_t2782302542 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(GameController_t2782302542 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
