﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AbstractLayoutCell
struct AbstractLayoutCell_t3337132430;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void AbstractLayoutCell::.ctor(UnityEngine.Vector2,System.Single)
extern "C"  void AbstractLayoutCell__ctor_m3988337650 (AbstractLayoutCell_t3337132430 * __this, Vector2_t3525329788  ___inPos0, float ___inHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
