﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithSteamRequest
struct LoginWithSteamRequest_t700401790;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithSteamRequest::.ctor()
extern "C"  void LoginWithSteamRequest__ctor_m446985323 (LoginWithSteamRequest_t700401790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithSteamRequest::get_TitleId()
extern "C"  String_t* LoginWithSteamRequest_get_TitleId_m3578439152 (LoginWithSteamRequest_t700401790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithSteamRequest::set_TitleId(System.String)
extern "C"  void LoginWithSteamRequest_set_TitleId_m3441182883 (LoginWithSteamRequest_t700401790 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithSteamRequest::get_SteamTicket()
extern "C"  String_t* LoginWithSteamRequest_get_SteamTicket_m4194703513 (LoginWithSteamRequest_t700401790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithSteamRequest::set_SteamTicket(System.String)
extern "C"  void LoginWithSteamRequest_set_SteamTicket_m2268266522 (LoginWithSteamRequest_t700401790 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithSteamRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithSteamRequest_get_CreateAccount_m3919639688 (LoginWithSteamRequest_t700401790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithSteamRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithSteamRequest_set_CreateAccount_m1222035147 (LoginWithSteamRequest_t700401790 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
