﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeIntervalObservable`1<System.Object>
struct TimeIntervalObservable_1_t4159975444;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.TimeInterval`1<System.Object>>
struct IObserver_1_t2920064445;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TimeIntervalObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern "C"  void TimeIntervalObservable_1__ctor_m898044517_gshared (TimeIntervalObservable_1_t4159975444 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define TimeIntervalObservable_1__ctor_m898044517(__this, ___source0, ___scheduler1, method) ((  void (*) (TimeIntervalObservable_1_t4159975444 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TimeIntervalObservable_1__ctor_m898044517_gshared)(__this, ___source0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.TimeIntervalObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.TimeInterval`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * TimeIntervalObservable_1_SubscribeCore_m3860261785_gshared (TimeIntervalObservable_1_t4159975444 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TimeIntervalObservable_1_SubscribeCore_m3860261785(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TimeIntervalObservable_1_t4159975444 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TimeIntervalObservable_1_SubscribeCore_m3860261785_gshared)(__this, ___observer0, ___cancel1, method)
