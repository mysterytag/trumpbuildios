﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromFacebookIDs>c__AnonStorey6E
struct U3CGetPlayFabIDsFromFacebookIDsU3Ec__AnonStorey6E_t4116743937;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromFacebookIDs>c__AnonStorey6E::.ctor()
extern "C"  void U3CGetPlayFabIDsFromFacebookIDsU3Ec__AnonStorey6E__ctor_m1605094866 (U3CGetPlayFabIDsFromFacebookIDsU3Ec__AnonStorey6E_t4116743937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPlayFabIDsFromFacebookIDs>c__AnonStorey6E::<>m__5B(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPlayFabIDsFromFacebookIDsU3Ec__AnonStorey6E_U3CU3Em__5B_m485964573 (U3CGetPlayFabIDsFromFacebookIDsU3Ec__AnonStorey6E_t4116743937 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
