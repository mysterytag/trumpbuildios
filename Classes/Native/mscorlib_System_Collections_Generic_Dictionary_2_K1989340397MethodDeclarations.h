﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1852733134MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3665098353(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1989340397 *, Dictionary_2_t3961032413 *, const MethodInfo*))KeyCollection__ctor_m3432069128_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m813326405(__this, ___item0, method) ((  void (*) (KeyCollection_t1989340397 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3101899854_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3589657340(__this, method) ((  void (*) (KeyCollection_t1989340397 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m164109637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1565240873(__this, ___item0, method) ((  bool (*) (KeyCollection_t1989340397 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2402136956_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m869242318(__this, ___item0, method) ((  bool (*) (KeyCollection_t1989340397 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1325978593_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3378571918(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1989340397 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3150060033_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2468814190(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1989340397 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2134327863_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m224435965(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1989340397 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3067601266_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3838984010(__this, method) ((  bool (*) (KeyCollection_t1989340397 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m642268125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2113076348(__this, method) ((  bool (*) (KeyCollection_t1989340397 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1412236495_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3661197550(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1989340397 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3575527099_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m244343078(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1989340397 *, StringU5BU5D_t2956870243*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2803941053_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::GetEnumerator()
#define KeyCollection_GetEnumerator_m4262495795(__this, method) ((  Enumerator_t3728060355  (*) (KeyCollection_t1989340397 *, const MethodInfo*))KeyCollection_GetEnumerator_m2980864032_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,AchievementDescription>::get_Count()
#define KeyCollection_get_Count_m2524645814(__this, method) ((  int32_t (*) (KeyCollection_t1989340397 *, const MethodInfo*))KeyCollection_get_Count_m1374340501_gshared)(__this, method)
