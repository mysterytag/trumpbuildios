﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ConsumeItemRequest
struct ConsumeItemRequest_t3677119120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ConsumeItemRequest::.ctor()
extern "C"  void ConsumeItemRequest__ctor_m3753618733 (ConsumeItemRequest_t3677119120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ConsumeItemRequest::get_ItemInstanceId()
extern "C"  String_t* ConsumeItemRequest_get_ItemInstanceId_m1500091840 (ConsumeItemRequest_t3677119120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConsumeItemRequest::set_ItemInstanceId(System.String)
extern "C"  void ConsumeItemRequest_set_ItemInstanceId_m317791979 (ConsumeItemRequest_t3677119120 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.ConsumeItemRequest::get_ConsumeCount()
extern "C"  int32_t ConsumeItemRequest_get_ConsumeCount_m2531568741 (ConsumeItemRequest_t3677119120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConsumeItemRequest::set_ConsumeCount(System.Int32)
extern "C"  void ConsumeItemRequest_set_ConsumeCount_m3605101432 (ConsumeItemRequest_t3677119120 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ConsumeItemRequest::get_CharacterId()
extern "C"  String_t* ConsumeItemRequest_get_CharacterId_m896998953 (ConsumeItemRequest_t3677119120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConsumeItemRequest::set_CharacterId(System.String)
extern "C"  void ConsumeItemRequest_set_CharacterId_m1750914672 (ConsumeItemRequest_t3677119120 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
