﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t2688674948;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat774457940.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Quaternion>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m792515304_gshared (Enumerator_t774457940 * __this, List_1_t2688674948 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m792515304(__this, ___l0, method) ((  void (*) (Enumerator_t774457940 *, List_1_t2688674948 *, const MethodInfo*))Enumerator__ctor_m792515304_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Quaternion>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m962859114_gshared (Enumerator_t774457940 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m962859114(__this, method) ((  void (*) (Enumerator_t774457940 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m962859114_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Quaternion>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3257587744_gshared (Enumerator_t774457940 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3257587744(__this, method) ((  Il2CppObject * (*) (Enumerator_t774457940 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3257587744_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Quaternion>::Dispose()
extern "C"  void Enumerator_Dispose_m3205405389_gshared (Enumerator_t774457940 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3205405389(__this, method) ((  void (*) (Enumerator_t774457940 *, const MethodInfo*))Enumerator_Dispose_m3205405389_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Quaternion>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1991125894_gshared (Enumerator_t774457940 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1991125894(__this, method) ((  void (*) (Enumerator_t774457940 *, const MethodInfo*))Enumerator_VerifyState_m1991125894_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Quaternion>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1168696218_gshared (Enumerator_t774457940 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1168696218(__this, method) ((  bool (*) (Enumerator_t774457940 *, const MethodInfo*))Enumerator_MoveNext_m1168696218_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Quaternion>::get_Current()
extern "C"  Quaternion_t1891715979  Enumerator_get_Current_m2412307039_gshared (Enumerator_t774457940 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2412307039(__this, method) ((  Quaternion_t1891715979  (*) (Enumerator_t774457940 *, const MethodInfo*))Enumerator_get_Current_m2412307039_gshared)(__this, method)
