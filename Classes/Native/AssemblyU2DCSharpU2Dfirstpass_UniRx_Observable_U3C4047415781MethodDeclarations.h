﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1<System.Object>
struct U3CSelectManyU3Ec__AnonStorey50_1_t4047415781;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1<System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey50_1__ctor_m2643319079_gshared (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey50_1__ctor_m2643319079(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey50_1__ctor_m2643319079_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey4F`1/<SelectMany>c__AnonStorey50`1<System.Object>::<>m__68()
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey50_1_U3CU3Em__68_m722859426_gshared (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey50_1_U3CU3Em__68_m722859426(__this, method) ((  Il2CppObject * (*) (U3CSelectManyU3Ec__AnonStorey50_1_t4047415781 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey50_1_U3CU3Em__68_m722859426_gshared)(__this, method)
