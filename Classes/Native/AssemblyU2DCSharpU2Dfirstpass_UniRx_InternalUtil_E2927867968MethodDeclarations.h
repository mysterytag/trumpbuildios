﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>
struct EmptyObserver_1_t2927867968;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1891988060_gshared (EmptyObserver_1_t2927867968 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m1891988060(__this, method) ((  void (*) (EmptyObserver_1_t2927867968 *, const MethodInfo*))EmptyObserver_1__ctor_m1891988060_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2334958801_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m2334958801(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m2334958801_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m51409126_gshared (EmptyObserver_1_t2927867968 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m51409126(__this, method) ((  void (*) (EmptyObserver_1_t2927867968 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m51409126_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2756376851_gshared (EmptyObserver_1_t2927867968 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m2756376851(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2927867968 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m2756376851_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Bounds>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4260062724_gshared (EmptyObserver_1_t2927867968 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m4260062724(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2927867968 *, Bounds_t3518514978 , const MethodInfo*))EmptyObserver_1_OnNext_m4260062724_gshared)(__this, ___value0, method)
