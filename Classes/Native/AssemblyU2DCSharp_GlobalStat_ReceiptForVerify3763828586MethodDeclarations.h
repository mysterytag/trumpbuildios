﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/ReceiptForVerify
struct ReceiptForVerify_t3763828586;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/ReceiptForVerify::.ctor()
extern "C"  void ReceiptForVerify__ctor_m2522281961 (ReceiptForVerify_t3763828586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
