﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SettingsButton
struct SettingsButton_t915256661;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectionReplaceEvent`1<SettingsButton>
struct  CollectionReplaceEvent_1_t2575522311  : public Il2CppObject
{
public:
	// System.Int32 CollectionReplaceEvent`1::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_0;
	// T CollectionReplaceEvent`1::<OldValue>k__BackingField
	SettingsButton_t915256661 * ___U3COldValueU3Ek__BackingField_1;
	// T CollectionReplaceEvent`1::<NewValue>k__BackingField
	SettingsButton_t915256661 * ___U3CNewValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionReplaceEvent_1_t2575522311, ___U3CIndexU3Ek__BackingField_0)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_0() const { return ___U3CIndexU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_0() { return &___U3CIndexU3Ek__BackingField_0; }
	inline void set_U3CIndexU3Ek__BackingField_0(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3COldValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionReplaceEvent_1_t2575522311, ___U3COldValueU3Ek__BackingField_1)); }
	inline SettingsButton_t915256661 * get_U3COldValueU3Ek__BackingField_1() const { return ___U3COldValueU3Ek__BackingField_1; }
	inline SettingsButton_t915256661 ** get_address_of_U3COldValueU3Ek__BackingField_1() { return &___U3COldValueU3Ek__BackingField_1; }
	inline void set_U3COldValueU3Ek__BackingField_1(SettingsButton_t915256661 * value)
	{
		___U3COldValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3COldValueU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CNewValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CollectionReplaceEvent_1_t2575522311, ___U3CNewValueU3Ek__BackingField_2)); }
	inline SettingsButton_t915256661 * get_U3CNewValueU3Ek__BackingField_2() const { return ___U3CNewValueU3Ek__BackingField_2; }
	inline SettingsButton_t915256661 ** get_address_of_U3CNewValueU3Ek__BackingField_2() { return &___U3CNewValueU3Ek__BackingField_2; }
	inline void set_U3CNewValueU3Ek__BackingField_2(SettingsButton_t915256661 * value)
	{
		___U3CNewValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNewValueU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
