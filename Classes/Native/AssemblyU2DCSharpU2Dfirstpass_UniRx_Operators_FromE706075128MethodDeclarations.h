﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>
struct FromEvent_t706075128;
// UniRx.Operators.FromEventObservable_`1<System.Int32>
struct FromEventObservable__1_t350522882;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>::.ctor(UniRx.Operators.FromEventObservable_`1<T>,UniRx.IObserver`1<T>)
extern "C"  void FromEvent__ctor_m3298522655_gshared (FromEvent_t706075128 * __this, FromEventObservable__1_t350522882 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m3298522655(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t706075128 *, FromEventObservable__1_t350522882 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m3298522655_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>::Register()
extern "C"  bool FromEvent_Register_m2524226652_gshared (FromEvent_t706075128 * __this, const MethodInfo* method);
#define FromEvent_Register_m2524226652(__this, method) ((  bool (*) (FromEvent_t706075128 *, const MethodInfo*))FromEvent_Register_m2524226652_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>::OnNext(T)
extern "C"  void FromEvent_OnNext_m412581743_gshared (FromEvent_t706075128 * __this, int32_t ___value0, const MethodInfo* method);
#define FromEvent_OnNext_m412581743(__this, ___value0, method) ((  void (*) (FromEvent_t706075128 *, int32_t, const MethodInfo*))FromEvent_OnNext_m412581743_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FromEventObservable_`1/FromEvent<System.Int32>::Dispose()
extern "C"  void FromEvent_Dispose_m1775128836_gshared (FromEvent_t706075128 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m1775128836(__this, method) ((  void (*) (FromEvent_t706075128 *, const MethodInfo*))FromEvent_Dispose_m1775128836_gshared)(__this, method)
