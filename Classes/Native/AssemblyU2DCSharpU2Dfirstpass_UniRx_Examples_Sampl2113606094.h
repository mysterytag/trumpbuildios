﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.CompositeDisposable
struct CompositeDisposable_t1894629977;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t490482111;
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct EventHandler_1_t3942771990;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>
struct Func_2_t1776554224;
// System.Func`2<System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>
struct Func_2_t4235389922;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample09_EventHandling
struct  Sample09_EventHandling_t2113606094  : public MonoBehaviour_t3012272455
{
public:
	// UniRx.CompositeDisposable UniRx.Examples.Sample09_EventHandling::disposables
	CompositeDisposable_t1894629977 * ___disposables_2;
	// UniRx.Subject`1<System.Int32> UniRx.Examples.Sample09_EventHandling::onBarBar
	Subject_1_t490482111 * ___onBarBar_3;
	// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling::FooBar
	EventHandler_1_t3942771990 * ___FooBar_4;
	// System.Action`1<System.Int32> UniRx.Examples.Sample09_EventHandling::FooFoo
	Action_1_t2995867492 * ___FooFoo_5;

public:
	inline static int32_t get_offset_of_disposables_2() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t2113606094, ___disposables_2)); }
	inline CompositeDisposable_t1894629977 * get_disposables_2() const { return ___disposables_2; }
	inline CompositeDisposable_t1894629977 ** get_address_of_disposables_2() { return &___disposables_2; }
	inline void set_disposables_2(CompositeDisposable_t1894629977 * value)
	{
		___disposables_2 = value;
		Il2CppCodeGenWriteBarrier(&___disposables_2, value);
	}

	inline static int32_t get_offset_of_onBarBar_3() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t2113606094, ___onBarBar_3)); }
	inline Subject_1_t490482111 * get_onBarBar_3() const { return ___onBarBar_3; }
	inline Subject_1_t490482111 ** get_address_of_onBarBar_3() { return &___onBarBar_3; }
	inline void set_onBarBar_3(Subject_1_t490482111 * value)
	{
		___onBarBar_3 = value;
		Il2CppCodeGenWriteBarrier(&___onBarBar_3, value);
	}

	inline static int32_t get_offset_of_FooBar_4() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t2113606094, ___FooBar_4)); }
	inline EventHandler_1_t3942771990 * get_FooBar_4() const { return ___FooBar_4; }
	inline EventHandler_1_t3942771990 ** get_address_of_FooBar_4() { return &___FooBar_4; }
	inline void set_FooBar_4(EventHandler_1_t3942771990 * value)
	{
		___FooBar_4 = value;
		Il2CppCodeGenWriteBarrier(&___FooBar_4, value);
	}

	inline static int32_t get_offset_of_FooFoo_5() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t2113606094, ___FooFoo_5)); }
	inline Action_1_t2995867492 * get_FooFoo_5() const { return ___FooFoo_5; }
	inline Action_1_t2995867492 ** get_address_of_FooFoo_5() { return &___FooFoo_5; }
	inline void set_FooFoo_5(Action_1_t2995867492 * value)
	{
		___FooFoo_5 = value;
		Il2CppCodeGenWriteBarrier(&___FooFoo_5, value);
	}
};

struct Sample09_EventHandling_t2113606094_StaticFields
{
public:
	// System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>> UniRx.Examples.Sample09_EventHandling::<>f__am$cache4
	Func_2_t1776554224 * ___U3CU3Ef__amU24cache4_6;
	// System.Func`2<System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>> UniRx.Examples.Sample09_EventHandling::<>f__am$cache5
	Func_2_t4235389922 * ___U3CU3Ef__amU24cache5_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t2113606094_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Func_2_t1776554224 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Func_2_t1776554224 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Func_2_t1776554224 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t2113606094_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Func_2_t4235389922 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Func_2_t4235389922 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Func_2_t4235389922 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
