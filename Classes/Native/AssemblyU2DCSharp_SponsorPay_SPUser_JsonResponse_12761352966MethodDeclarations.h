﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>
struct JsonResponse_1_t2761352966;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m3195743510_gshared (JsonResponse_1_t2761352966 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m3195743510(__this, method) ((  void (*) (JsonResponse_1_t2761352966 *, const MethodInfo*))JsonResponse_1__ctor_m3195743510_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m3662287789_gshared (JsonResponse_1_t2761352966 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m3662287789(__this, method) ((  String_t* (*) (JsonResponse_1_t2761352966 *, const MethodInfo*))JsonResponse_1_get_key_m3662287789_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m109506732_gshared (JsonResponse_1_t2761352966 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m109506732(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2761352966 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m109506732_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::get_value()
extern "C"  Nullable_1_t1438485399  JsonResponse_1_get_value_m776780635_gshared (JsonResponse_1_t2761352966 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m776780635(__this, method) ((  Nullable_1_t1438485399  (*) (JsonResponse_1_t2761352966 *, const MethodInfo*))JsonResponse_1_get_value_m776780635_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m3726866230_gshared (JsonResponse_1_t2761352966 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m3726866230(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2761352966 *, Nullable_1_t1438485399 , const MethodInfo*))JsonResponse_1_set_value_m3726866230_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m1218195926_gshared (JsonResponse_1_t2761352966 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m1218195926(__this, method) ((  String_t* (*) (JsonResponse_1_t2761352966 *, const MethodInfo*))JsonResponse_1_get_error_m1218195926_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<System.Int32>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m517647203_gshared (JsonResponse_1_t2761352966 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m517647203(__this, ___value0, method) ((  void (*) (JsonResponse_1_t2761352966 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m517647203_gshared)(__this, ___value0, method)
