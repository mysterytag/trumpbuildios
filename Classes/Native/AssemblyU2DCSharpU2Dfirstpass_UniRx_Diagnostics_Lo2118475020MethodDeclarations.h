﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Diagnostics.Logger
struct Logger_t2118475020;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Exception
struct Exception_t1967233988;
// UniRx.Diagnostics.LogEntry
struct LogEntry_t1891155722;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1891155722.h"

// System.Void UniRx.Diagnostics.Logger::.ctor(System.String)
extern "C"  void Logger__ctor_m3165661341 (Logger_t2118475020 * __this, String_t* ___loggerName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::.cctor()
extern "C"  void Logger__cctor_m2729944520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.Diagnostics.Logger::get_Name()
extern "C"  String_t* Logger_get_Name_m2162682262 (Logger_t2118475020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::set_Name(System.String)
extern "C"  void Logger_set_Name_m4046211323 (Logger_t2118475020 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::Debug(System.Object,UnityEngine.Object)
extern "C"  void Logger_Debug_m3604783602 (Logger_t2118475020 * __this, Il2CppObject * ___message0, Object_t3878351788 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::DebugFormat(System.String,System.Object[])
extern "C"  void Logger_DebugFormat_m11770049 (Logger_t2118475020 * __this, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::Log(System.Object,UnityEngine.Object)
extern "C"  void Logger_Log_m1113486115 (Logger_t2118475020 * __this, Il2CppObject * ___message0, Object_t3878351788 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::LogFormat(System.String,System.Object[])
extern "C"  void Logger_LogFormat_m3098177008 (Logger_t2118475020 * __this, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::Warning(System.Object,UnityEngine.Object)
extern "C"  void Logger_Warning_m2950760571 (Logger_t2118475020 * __this, Il2CppObject * ___message0, Object_t3878351788 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::WarningFormat(System.String,System.Object[])
extern "C"  void Logger_WarningFormat_m2278275480 (Logger_t2118475020 * __this, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::Error(System.Object,UnityEngine.Object)
extern "C"  void Logger_Error_m2103554151 (Logger_t2118475020 * __this, Il2CppObject * ___message0, Object_t3878351788 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::ErrorFormat(System.String,System.Object[])
extern "C"  void Logger_ErrorFormat_m379648556 (Logger_t2118475020 * __this, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::Exception(System.Exception,UnityEngine.Object)
extern "C"  void Logger_Exception_m784696238 (Logger_t2118475020 * __this, Exception_t1967233988 * ___exception0, Object_t3878351788 * ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.Logger::Raw(UniRx.Diagnostics.LogEntry)
extern "C"  void Logger_Raw_m3355044675 (Logger_t2118475020 * __this, LogEntry_t1891155722 * ___logEntry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
