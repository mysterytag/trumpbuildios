﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381MethodDeclarations.h"

// System.Void CollectionMoveEvent`1<SettingsButton>::.ctor(System.Int32,System.Int32,T)
#define CollectionMoveEvent_1__ctor_m3033655676(__this, ___oldIndex0, ___newIndex1, ___value2, method) ((  void (*) (CollectionMoveEvent_1_t3650205622 *, int32_t, int32_t, SettingsButton_t915256661 *, const MethodInfo*))CollectionMoveEvent_1__ctor_m2094255715_gshared)(__this, ___oldIndex0, ___newIndex1, ___value2, method)
// System.Int32 CollectionMoveEvent`1<SettingsButton>::get_OldIndex()
#define CollectionMoveEvent_1_get_OldIndex_m2436874984(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t3650205622 *, const MethodInfo*))CollectionMoveEvent_1_get_OldIndex_m1896787371_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<SettingsButton>::set_OldIndex(System.Int32)
#define CollectionMoveEvent_1_set_OldIndex_m4062032379(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3650205622 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_OldIndex_m3746196962_gshared)(__this, ___value0, method)
// System.Int32 CollectionMoveEvent`1<SettingsButton>::get_NewIndex()
#define CollectionMoveEvent_1_get_NewIndex_m1154325839(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t3650205622 *, const MethodInfo*))CollectionMoveEvent_1_get_NewIndex_m614238226_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<SettingsButton>::set_NewIndex(System.Int32)
#define CollectionMoveEvent_1_set_NewIndex_m1147939298(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3650205622 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_NewIndex_m832103881_gshared)(__this, ___value0, method)
// T CollectionMoveEvent`1<SettingsButton>::get_Value()
#define CollectionMoveEvent_1_get_Value_m1005789479(__this, method) ((  SettingsButton_t915256661 * (*) (CollectionMoveEvent_1_t3650205622 *, const MethodInfo*))CollectionMoveEvent_1_get_Value_m3472123074_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<SettingsButton>::set_Value(T)
#define CollectionMoveEvent_1_set_Value_m239288170(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3650205622 *, SettingsButton_t915256661 *, const MethodInfo*))CollectionMoveEvent_1_set_Value_m4226272145_gshared)(__this, ___value0, method)
// System.String CollectionMoveEvent`1<SettingsButton>::ToString()
#define CollectionMoveEvent_1_ToString_m1322484651(__this, method) ((  String_t* (*) (CollectionMoveEvent_1_t3650205622 *, const MethodInfo*))CollectionMoveEvent_1_ToString_m512063864_gshared)(__this, method)
