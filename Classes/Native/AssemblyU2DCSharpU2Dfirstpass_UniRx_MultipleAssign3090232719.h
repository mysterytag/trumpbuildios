﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.BooleanDisposable
struct BooleanDisposable_t3065601722;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MultipleAssignmentDisposable
struct  MultipleAssignmentDisposable_t3090232719  : public Il2CppObject
{
public:
	// System.Object UniRx.MultipleAssignmentDisposable::gate
	Il2CppObject * ___gate_1;
	// System.IDisposable UniRx.MultipleAssignmentDisposable::current
	Il2CppObject * ___current_2;

public:
	inline static int32_t get_offset_of_gate_1() { return static_cast<int32_t>(offsetof(MultipleAssignmentDisposable_t3090232719, ___gate_1)); }
	inline Il2CppObject * get_gate_1() const { return ___gate_1; }
	inline Il2CppObject ** get_address_of_gate_1() { return &___gate_1; }
	inline void set_gate_1(Il2CppObject * value)
	{
		___gate_1 = value;
		Il2CppCodeGenWriteBarrier(&___gate_1, value);
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(MultipleAssignmentDisposable_t3090232719, ___current_2)); }
	inline Il2CppObject * get_current_2() const { return ___current_2; }
	inline Il2CppObject ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(Il2CppObject * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier(&___current_2, value);
	}
};

struct MultipleAssignmentDisposable_t3090232719_StaticFields
{
public:
	// UniRx.BooleanDisposable UniRx.MultipleAssignmentDisposable::True
	BooleanDisposable_t3065601722 * ___True_0;

public:
	inline static int32_t get_offset_of_True_0() { return static_cast<int32_t>(offsetof(MultipleAssignmentDisposable_t3090232719_StaticFields, ___True_0)); }
	inline BooleanDisposable_t3065601722 * get_True_0() const { return ___True_0; }
	inline BooleanDisposable_t3065601722 ** get_address_of_True_0() { return &___True_0; }
	inline void set_True_0(BooleanDisposable_t3065601722 * value)
	{
		___True_0 = value;
		Il2CppCodeGenWriteBarrier(&___True_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
