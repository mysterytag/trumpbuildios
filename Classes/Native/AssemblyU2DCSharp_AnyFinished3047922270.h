﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<IProcess,System.Boolean>
struct Func_2_t3494101351;

#include "AssemblyU2DCSharp_MetaProcessBase2426506875.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnyFinished
struct  AnyFinished_t3047922270  : public MetaProcessBase_t2426506875
{
public:

public:
};

struct AnyFinished_t3047922270_StaticFields
{
public:
	// System.Func`2<IProcess,System.Boolean> AnyFinished::<>f__am$cache0
	Func_2_t3494101351 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(AnyFinished_t3047922270_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t3494101351 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t3494101351 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t3494101351 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
