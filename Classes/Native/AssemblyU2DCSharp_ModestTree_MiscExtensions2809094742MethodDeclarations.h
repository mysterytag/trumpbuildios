﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3840643258;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.String ModestTree.MiscExtensions::Fmt(System.String,System.Object[])
extern "C"  String_t* MiscExtensions_Fmt_m1348433569 (Il2CppObject * __this /* static, unused */, String_t* ___s0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ModestTree.MiscExtensions::Join(System.Collections.Generic.IEnumerable`1<System.String>,System.String)
extern "C"  String_t* MiscExtensions_Join_m3896482411 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___values0, String_t* ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
