﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>
struct U3CPrependU3Ec__Iterator38_1_t1439900156;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::.ctor()
extern "C"  void U3CPrependU3Ec__Iterator38_1__ctor_m2448574809_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator38_1__ctor_m2448574809(__this, method) ((  void (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))U3CPrependU3Ec__Iterator38_1__ctor_m2448574809_gshared)(__this, method)
// T ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator38_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2329936032_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator38_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2329936032(__this, method) ((  Il2CppObject * (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))U3CPrependU3Ec__Iterator38_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2329936032_gshared)(__this, method)
// System.Object ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator38_1_System_Collections_IEnumerator_get_Current_m3051129453_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator38_1_System_Collections_IEnumerator_get_Current_m3051129453(__this, method) ((  Il2CppObject * (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))U3CPrependU3Ec__Iterator38_1_System_Collections_IEnumerator_get_Current_m3051129453_gshared)(__this, method)
// System.Collections.IEnumerator ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrependU3Ec__Iterator38_1_System_Collections_IEnumerable_GetEnumerator_m4155552232_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator38_1_System_Collections_IEnumerable_GetEnumerator_m4155552232(__this, method) ((  Il2CppObject * (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))U3CPrependU3Ec__Iterator38_1_System_Collections_IEnumerable_GetEnumerator_m4155552232_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CPrependU3Ec__Iterator38_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3592397859_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator38_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3592397859(__this, method) ((  Il2CppObject* (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))U3CPrependU3Ec__Iterator38_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3592397859_gshared)(__this, method)
// System.Boolean ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::MoveNext()
extern "C"  bool U3CPrependU3Ec__Iterator38_1_MoveNext_m2446620411_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator38_1_MoveNext_m2446620411(__this, method) ((  bool (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))U3CPrependU3Ec__Iterator38_1_MoveNext_m2446620411_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::Dispose()
extern "C"  void U3CPrependU3Ec__Iterator38_1_Dispose_m3631563606_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator38_1_Dispose_m3631563606(__this, method) ((  void (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))U3CPrependU3Ec__Iterator38_1_Dispose_m3631563606_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<Prepend>c__Iterator38`1<System.Object>::Reset()
extern "C"  void U3CPrependU3Ec__Iterator38_1_Reset_m95007750_gshared (U3CPrependU3Ec__Iterator38_1_t1439900156 * __this, const MethodInfo* method);
#define U3CPrependU3Ec__Iterator38_1_Reset_m95007750(__this, method) ((  void (*) (U3CPrependU3Ec__Iterator38_1_t1439900156 *, const MethodInfo*))U3CPrependU3Ec__Iterator38_1_Reset_m95007750_gshared)(__this, method)
