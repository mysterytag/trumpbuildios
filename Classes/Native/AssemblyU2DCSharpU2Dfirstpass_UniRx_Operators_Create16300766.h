﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CreateObservable`1/Create<System.Boolean>
struct  Create_t16300766  : public OperatorObserverBase_2_t60103313
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
