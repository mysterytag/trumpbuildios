﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.NetworkPlayer>
struct Subscription_t3355351190;
// UniRx.Subject`1<UnityEngine.NetworkPlayer>
struct Subject_1_t3219171992;
// UniRx.IObserver`1<UnityEngine.NetworkPlayer>
struct IObserver_1_t3493136275;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkPlayer>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m3055518045_gshared (Subscription_t3355351190 * __this, Subject_1_t3219171992 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m3055518045(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t3355351190 *, Subject_1_t3219171992 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m3055518045_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkPlayer>::Dispose()
extern "C"  void Subscription_Dispose_m3054422649_gshared (Subscription_t3355351190 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3054422649(__this, method) ((  void (*) (Subscription_t3355351190 *, const MethodInfo*))Subscription_Dispose_m3054422649_gshared)(__this, method)
