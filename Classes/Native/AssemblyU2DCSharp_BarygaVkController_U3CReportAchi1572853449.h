﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AchievementDescription
struct AchievementDescription_t2323334509;
// BarygaVkController
struct BarygaVkController_t3617163921;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaVkController/<ReportAchievement>c__AnonStoreyDE
struct  U3CReportAchievementU3Ec__AnonStoreyDE_t1572853449  : public Il2CppObject
{
public:
	// AchievementDescription BarygaVkController/<ReportAchievement>c__AnonStoreyDE::a
	AchievementDescription_t2323334509 * ___a_0;
	// BarygaVkController BarygaVkController/<ReportAchievement>c__AnonStoreyDE::<>f__this
	BarygaVkController_t3617163921 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CReportAchievementU3Ec__AnonStoreyDE_t1572853449, ___a_0)); }
	inline AchievementDescription_t2323334509 * get_a_0() const { return ___a_0; }
	inline AchievementDescription_t2323334509 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(AchievementDescription_t2323334509 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier(&___a_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CReportAchievementU3Ec__AnonStoreyDE_t1572853449, ___U3CU3Ef__this_1)); }
	inline BarygaVkController_t3617163921 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline BarygaVkController_t3617163921 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(BarygaVkController_t3617163921 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
