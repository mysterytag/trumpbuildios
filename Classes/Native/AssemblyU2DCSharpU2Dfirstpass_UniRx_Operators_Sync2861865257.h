﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SynchronizeObservable`1<System.Object>
struct SynchronizeObservable_1_t736720834;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SynchronizeObservable`1/Synchronize<System.Object>
struct  Synchronize_t2861865257  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.SynchronizeObservable`1<T> UniRx.Operators.SynchronizeObservable`1/Synchronize::parent
	SynchronizeObservable_1_t736720834 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Synchronize_t2861865257, ___parent_2)); }
	inline SynchronizeObservable_1_t736720834 * get_parent_2() const { return ___parent_2; }
	inline SynchronizeObservable_1_t736720834 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SynchronizeObservable_1_t736720834 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
