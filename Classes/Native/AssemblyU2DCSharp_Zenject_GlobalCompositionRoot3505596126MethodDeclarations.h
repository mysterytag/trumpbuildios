﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GlobalCompositionRoot
struct GlobalCompositionRoot_t3505596126;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.IEnumerable`1<Zenject.IInstaller>
struct IEnumerable_1_t1032410536;
// System.Collections.Generic.IEnumerable`1<Zenject.MonoInstaller>
struct IEnumerable_1_t2384762926;
// Zenject.GlobalInstallerConfig
struct GlobalInstallerConfig_t625471004;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_GlobalCompositionRoot3505596126.h"
#include "AssemblyU2DCSharp_Zenject_GlobalInstallerConfig625471004.h"

// System.Void Zenject.GlobalCompositionRoot::.ctor()
extern "C"  void GlobalCompositionRoot__ctor_m1975940609 (GlobalCompositionRoot_t3505596126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.DiContainer Zenject.GlobalCompositionRoot::get_Container()
extern "C"  DiContainer_t2383114449 * GlobalCompositionRoot_get_Container_m2598759885 (GlobalCompositionRoot_t3505596126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.GlobalCompositionRoot Zenject.GlobalCompositionRoot::get_Instance()
extern "C"  GlobalCompositionRoot_t3505596126 * GlobalCompositionRoot_get_Instance_m1562876472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.GlobalCompositionRoot::Awake()
extern "C"  void GlobalCompositionRoot_Awake_m2213545828 (GlobalCompositionRoot_t3505596126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.GlobalCompositionRoot::InitializeIfNecessary()
extern "C"  void GlobalCompositionRoot_InitializeIfNecessary_m696140705 (GlobalCompositionRoot_t3505596126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.GlobalCompositionRoot::OnDestroy()
extern "C"  void GlobalCompositionRoot_OnDestroy_m830169722 (GlobalCompositionRoot_t3505596126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.DiContainer Zenject.GlobalCompositionRoot::CreateContainer(System.Boolean,Zenject.GlobalCompositionRoot)
extern "C"  DiContainer_t2383114449 * GlobalCompositionRoot_CreateContainer_m2409088339 (Il2CppObject * __this /* static, unused */, bool ___allowNullBindings0, GlobalCompositionRoot_t3505596126 * ___root1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.IInstaller> Zenject.GlobalCompositionRoot::GetGlobalInstallers()
extern "C"  Il2CppObject* GlobalCompositionRoot_GetGlobalInstallers_m478894089 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.MonoInstaller> Zenject.GlobalCompositionRoot::<GetGlobalInstallers>m__2D1(Zenject.GlobalInstallerConfig)
extern "C"  Il2CppObject* GlobalCompositionRoot_U3CGetGlobalInstallersU3Em__2D1_m3217520177 (Il2CppObject * __this /* static, unused */, GlobalInstallerConfig_t625471004 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
