﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5D
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5D_t2116090499;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void PlayFab.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5D::.ctor()
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey5D__ctor_m3362835596 (U3CGetSetMethodByReflectionU3Ec__AnonStorey5D_t2116090499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5D::<>m__49(System.Object,System.Object)
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey5D_U3CU3Em__49_m3697658760 (U3CGetSetMethodByReflectionU3Ec__AnonStorey5D_t2116090499 * __this, Il2CppObject * ___source0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
