﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>
struct U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::.ctor()
extern "C"  void U3CAsObservableCoreU3Ec__Iterator1E_1__ctor_m3313671467_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method);
#define U3CAsObservableCoreU3Ec__Iterator1E_1__ctor_m3313671467(__this, method) ((  void (*) (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 *, const MethodInfo*))U3CAsObservableCoreU3Ec__Iterator1E_1__ctor_m3313671467_gshared)(__this, method)
// System.Object UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAsObservableCoreU3Ec__Iterator1E_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1809234951_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method);
#define U3CAsObservableCoreU3Ec__Iterator1E_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1809234951(__this, method) ((  Il2CppObject * (*) (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 *, const MethodInfo*))U3CAsObservableCoreU3Ec__Iterator1E_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1809234951_gshared)(__this, method)
// System.Object UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsObservableCoreU3Ec__Iterator1E_1_System_Collections_IEnumerator_get_Current_m4042763675_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method);
#define U3CAsObservableCoreU3Ec__Iterator1E_1_System_Collections_IEnumerator_get_Current_m4042763675(__this, method) ((  Il2CppObject * (*) (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 *, const MethodInfo*))U3CAsObservableCoreU3Ec__Iterator1E_1_System_Collections_IEnumerator_get_Current_m4042763675_gshared)(__this, method)
// System.Boolean UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::MoveNext()
extern "C"  bool U3CAsObservableCoreU3Ec__Iterator1E_1_MoveNext_m4170056961_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method);
#define U3CAsObservableCoreU3Ec__Iterator1E_1_MoveNext_m4170056961(__this, method) ((  bool (*) (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 *, const MethodInfo*))U3CAsObservableCoreU3Ec__Iterator1E_1_MoveNext_m4170056961_gshared)(__this, method)
// System.Void UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::Dispose()
extern "C"  void U3CAsObservableCoreU3Ec__Iterator1E_1_Dispose_m1765796520_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method);
#define U3CAsObservableCoreU3Ec__Iterator1E_1_Dispose_m1765796520(__this, method) ((  void (*) (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 *, const MethodInfo*))U3CAsObservableCoreU3Ec__Iterator1E_1_Dispose_m1765796520_gshared)(__this, method)
// System.Void UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>::Reset()
extern "C"  void U3CAsObservableCoreU3Ec__Iterator1E_1_Reset_m960104408_gshared (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 * __this, const MethodInfo* method);
#define U3CAsObservableCoreU3Ec__Iterator1E_1_Reset_m960104408(__this, method) ((  void (*) (U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625 *, const MethodInfo*))U3CAsObservableCoreU3Ec__Iterator1E_1_Reset_m960104408_gshared)(__this, method)
