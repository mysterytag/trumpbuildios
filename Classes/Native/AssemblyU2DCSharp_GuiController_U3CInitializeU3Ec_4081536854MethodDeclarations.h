﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GuiController/<Initialize>c__AnonStorey56
struct U3CInitializeU3Ec__AnonStorey56_t4081536854;
// <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>
struct U3CU3E__AnonType0_4_t733143067;
// <>__AnonType1`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType1_2_t2559104484;
// <>__AnonType2`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType2_2_t3656635463;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void GuiController/<Initialize>c__AnonStorey56::.ctor()
extern "C"  void U3CInitializeU3Ec__AnonStorey56__ctor_m1558043341 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GuiController/<Initialize>c__AnonStorey56::<>m__1F(UniRx.Unit)
extern "C"  bool U3CInitializeU3Ec__AnonStorey56_U3CU3Em__1F_m2570252963 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__20(UniRx.Unit)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__20_m2449457528 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__22(<>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__22_m805642788 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, U3CU3E__AnonType0_4_t733143067 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__25(<>__AnonType1`2<System.Boolean,System.Boolean>)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__25_m2423615046 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, U3CU3E__AnonType1_2_t2559104484 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GuiController/<Initialize>c__AnonStorey56::<>m__26(System.Int64)
extern "C"  int32_t U3CInitializeU3Ec__AnonStorey56_U3CU3Em__26_m2656544634 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__28(UnityEngine.Color)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__28_m2023674506 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Color_t1588175760  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__29(System.Boolean)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__29_m654820756 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__2B(<>__AnonType2`2<System.Boolean,System.Boolean>)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__2B_m4048224276 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, U3CU3E__AnonType2_2_t3656635463 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GuiController/<Initialize>c__AnonStorey56::<>m__2C(UniRx.Unit)
extern "C"  bool U3CInitializeU3Ec__AnonStorey56_U3CU3Em__2C_m2944903359 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__2D(UniRx.Unit)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__2D_m876364684 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GuiController/<Initialize>c__AnonStorey56::<>m__2E(UniRx.Unit)
extern "C"  bool U3CInitializeU3Ec__AnonStorey56_U3CU3Em__2E_m2358097345 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__2F(UniRx.Unit)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__2F_m289558670 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GuiController/<Initialize>c__AnonStorey56::<>m__30(UniRx.Unit)
extern "C"  bool U3CInitializeU3Ec__AnonStorey56_U3CU3Em__30_m3719034571 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__31(UniRx.Unit)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__31_m1650495896 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GuiController/<Initialize>c__AnonStorey56::<>m__32(UniRx.Unit)
extern "C"  bool U3CInitializeU3Ec__AnonStorey56_U3CU3Em__32_m3132228557 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__33(UniRx.Unit)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__33_m1063689882 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GuiController/<Initialize>c__AnonStorey56::<>m__34(UniRx.Unit)
extern "C"  bool U3CInitializeU3Ec__AnonStorey56_U3CU3Em__34_m2545422543 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__35(UniRx.Unit)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__35_m476883868 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GuiController/<Initialize>c__AnonStorey56::<>m__36(UniRx.Unit)
extern "C"  bool U3CInitializeU3Ec__AnonStorey56_U3CU3Em__36_m1958616529 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__37(UniRx.Unit)
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__37_m4185045150 (U3CInitializeU3Ec__AnonStorey56_t4081536854 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56::<>m__38()
extern "C"  void U3CInitializeU3Ec__AnonStorey56_U3CU3Em__38_m644171643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
