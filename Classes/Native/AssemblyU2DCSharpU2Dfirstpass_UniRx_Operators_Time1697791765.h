﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559853.h"
#include "mscorlib_System_Nullable_1_gen2303330647.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable
struct  TimerObservable_t1697791765  : public OperatorObservableBase_1_t1911559853
{
public:
	// System.Nullable`1<System.DateTimeOffset> UniRx.Operators.TimerObservable::dueTimeA
	Nullable_1_t2303330647  ___dueTimeA_1;
	// System.Nullable`1<System.TimeSpan> UniRx.Operators.TimerObservable::dueTimeB
	Nullable_1_t3649900800  ___dueTimeB_2;
	// System.Nullable`1<System.TimeSpan> UniRx.Operators.TimerObservable::period
	Nullable_1_t3649900800  ___period_3;
	// UniRx.IScheduler UniRx.Operators.TimerObservable::scheduler
	Il2CppObject * ___scheduler_4;

public:
	inline static int32_t get_offset_of_dueTimeA_1() { return static_cast<int32_t>(offsetof(TimerObservable_t1697791765, ___dueTimeA_1)); }
	inline Nullable_1_t2303330647  get_dueTimeA_1() const { return ___dueTimeA_1; }
	inline Nullable_1_t2303330647 * get_address_of_dueTimeA_1() { return &___dueTimeA_1; }
	inline void set_dueTimeA_1(Nullable_1_t2303330647  value)
	{
		___dueTimeA_1 = value;
	}

	inline static int32_t get_offset_of_dueTimeB_2() { return static_cast<int32_t>(offsetof(TimerObservable_t1697791765, ___dueTimeB_2)); }
	inline Nullable_1_t3649900800  get_dueTimeB_2() const { return ___dueTimeB_2; }
	inline Nullable_1_t3649900800 * get_address_of_dueTimeB_2() { return &___dueTimeB_2; }
	inline void set_dueTimeB_2(Nullable_1_t3649900800  value)
	{
		___dueTimeB_2 = value;
	}

	inline static int32_t get_offset_of_period_3() { return static_cast<int32_t>(offsetof(TimerObservable_t1697791765, ___period_3)); }
	inline Nullable_1_t3649900800  get_period_3() const { return ___period_3; }
	inline Nullable_1_t3649900800 * get_address_of_period_3() { return &___period_3; }
	inline void set_period_3(Nullable_1_t3649900800  value)
	{
		___period_3 = value;
	}

	inline static int32_t get_offset_of_scheduler_4() { return static_cast<int32_t>(offsetof(TimerObservable_t1697791765, ___scheduler_4)); }
	inline Il2CppObject * get_scheduler_4() const { return ___scheduler_4; }
	inline Il2CppObject ** get_address_of_scheduler_4() { return &___scheduler_4; }
	inline void set_scheduler_4(Il2CppObject * value)
	{
		___scheduler_4 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
