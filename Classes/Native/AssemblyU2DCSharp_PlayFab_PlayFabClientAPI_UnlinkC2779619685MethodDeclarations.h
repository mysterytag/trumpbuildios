﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkCustomIDResponseCallback
struct UnlinkCustomIDResponseCallback_t2779619685;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkCustomIDRequest
struct UnlinkCustomIDRequest_t1348755968;
// PlayFab.ClientModels.UnlinkCustomIDResult
struct UnlinkCustomIDResult_t3957981356;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkCusto1348755968.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkCusto3957981356.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkCustomIDResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkCustomIDResponseCallback__ctor_m3143704532 (UnlinkCustomIDResponseCallback_t2779619685 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkCustomIDResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkCustomIDRequest,PlayFab.ClientModels.UnlinkCustomIDResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkCustomIDResponseCallback_Invoke_m1671371713 (UnlinkCustomIDResponseCallback_t2779619685 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkCustomIDRequest_t1348755968 * ___request2, UnlinkCustomIDResult_t3957981356 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkCustomIDResponseCallback_t2779619685(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkCustomIDRequest_t1348755968 * ___request2, UnlinkCustomIDResult_t3957981356 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkCustomIDResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkCustomIDRequest,PlayFab.ClientModels.UnlinkCustomIDResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkCustomIDResponseCallback_BeginInvoke_m4088771886 (UnlinkCustomIDResponseCallback_t2779619685 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkCustomIDRequest_t1348755968 * ___request2, UnlinkCustomIDResult_t3957981356 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkCustomIDResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkCustomIDResponseCallback_EndInvoke_m524655588 (UnlinkCustomIDResponseCallback_t2779619685 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
