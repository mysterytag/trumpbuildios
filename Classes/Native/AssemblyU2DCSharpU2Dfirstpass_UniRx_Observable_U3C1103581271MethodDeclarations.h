﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey44_3__ctor_m1812564839_gshared (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey44_3__ctor_m1812564839(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey44_3__ctor_m1812564839_gshared)(__this, method)
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3/<FromAsyncPattern>c__AnonStorey44`3<System.Object,System.Object,System.Object>::<>m__67(System.IAsyncResult)
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m2007566162_gshared (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 * __this, Il2CppObject * ___iar0, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m2007566162(__this, ___iar0, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey44_3_t1103581271 *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey44_3_U3CU3Em__67_m2007566162_gshared)(__this, ___iar0, method)
