﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils2/<Lift>c__Iterator2A`1<System.Object>
struct U3CLiftU3Ec__Iterator2A_1_t2546142915;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils2/<Lift>c__Iterator2A`1<System.Object>::.ctor()
extern "C"  void U3CLiftU3Ec__Iterator2A_1__ctor_m878985243_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method);
#define U3CLiftU3Ec__Iterator2A_1__ctor_m878985243(__this, method) ((  void (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))U3CLiftU3Ec__Iterator2A_1__ctor_m878985243_gshared)(__this, method)
// T Utils2/<Lift>c__Iterator2A`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CLiftU3Ec__Iterator2A_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m572408994_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method);
#define U3CLiftU3Ec__Iterator2A_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m572408994(__this, method) ((  Il2CppObject * (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))U3CLiftU3Ec__Iterator2A_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m572408994_gshared)(__this, method)
// System.Object Utils2/<Lift>c__Iterator2A`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLiftU3Ec__Iterator2A_1_System_Collections_IEnumerator_get_Current_m4138424107_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method);
#define U3CLiftU3Ec__Iterator2A_1_System_Collections_IEnumerator_get_Current_m4138424107(__this, method) ((  Il2CppObject * (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))U3CLiftU3Ec__Iterator2A_1_System_Collections_IEnumerator_get_Current_m4138424107_gshared)(__this, method)
// System.Collections.IEnumerator Utils2/<Lift>c__Iterator2A`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CLiftU3Ec__Iterator2A_1_System_Collections_IEnumerable_GetEnumerator_m4066860774_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method);
#define U3CLiftU3Ec__Iterator2A_1_System_Collections_IEnumerable_GetEnumerator_m4066860774(__this, method) ((  Il2CppObject * (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))U3CLiftU3Ec__Iterator2A_1_System_Collections_IEnumerable_GetEnumerator_m4066860774_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Utils2/<Lift>c__Iterator2A`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CLiftU3Ec__Iterator2A_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3934413669_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method);
#define U3CLiftU3Ec__Iterator2A_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3934413669(__this, method) ((  Il2CppObject* (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))U3CLiftU3Ec__Iterator2A_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3934413669_gshared)(__this, method)
// System.Boolean Utils2/<Lift>c__Iterator2A`1<System.Object>::MoveNext()
extern "C"  bool U3CLiftU3Ec__Iterator2A_1_MoveNext_m2812105209_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method);
#define U3CLiftU3Ec__Iterator2A_1_MoveNext_m2812105209(__this, method) ((  bool (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))U3CLiftU3Ec__Iterator2A_1_MoveNext_m2812105209_gshared)(__this, method)
// System.Void Utils2/<Lift>c__Iterator2A`1<System.Object>::Dispose()
extern "C"  void U3CLiftU3Ec__Iterator2A_1_Dispose_m2789511576_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method);
#define U3CLiftU3Ec__Iterator2A_1_Dispose_m2789511576(__this, method) ((  void (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))U3CLiftU3Ec__Iterator2A_1_Dispose_m2789511576_gshared)(__this, method)
// System.Void Utils2/<Lift>c__Iterator2A`1<System.Object>::Reset()
extern "C"  void U3CLiftU3Ec__Iterator2A_1_Reset_m2820385480_gshared (U3CLiftU3Ec__Iterator2A_1_t2546142915 * __this, const MethodInfo* method);
#define U3CLiftU3Ec__Iterator2A_1_Reset_m2820385480(__this, method) ((  void (*) (U3CLiftU3Ec__Iterator2A_1_t2546142915 *, const MethodInfo*))U3CLiftU3Ec__Iterator2A_1_Reset_m2820385480_gshared)(__this, method)
