﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnePF.OpenIAB_Android
struct  OpenIAB_Android_t58679140  : public Il2CppObject
{
public:

public:
};

struct OpenIAB_Android_t58679140_StaticFields
{
public:
	// System.String OnePF.OpenIAB_Android::STORE_GOOGLE
	String_t* ___STORE_GOOGLE_0;
	// System.String OnePF.OpenIAB_Android::STORE_AMAZON
	String_t* ___STORE_AMAZON_1;
	// System.String OnePF.OpenIAB_Android::STORE_SAMSUNG
	String_t* ___STORE_SAMSUNG_2;
	// System.String OnePF.OpenIAB_Android::STORE_NOKIA
	String_t* ___STORE_NOKIA_3;
	// System.String OnePF.OpenIAB_Android::STORE_SKUBIT
	String_t* ___STORE_SKUBIT_4;
	// System.String OnePF.OpenIAB_Android::STORE_SKUBIT_TEST
	String_t* ___STORE_SKUBIT_TEST_5;
	// System.String OnePF.OpenIAB_Android::STORE_YANDEX
	String_t* ___STORE_YANDEX_6;
	// System.String OnePF.OpenIAB_Android::STORE_APPLAND
	String_t* ___STORE_APPLAND_7;
	// System.String OnePF.OpenIAB_Android::STORE_SLIDEME
	String_t* ___STORE_SLIDEME_8;
	// System.String OnePF.OpenIAB_Android::STORE_APTOIDE
	String_t* ___STORE_APTOIDE_9;

public:
	inline static int32_t get_offset_of_STORE_GOOGLE_0() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_GOOGLE_0)); }
	inline String_t* get_STORE_GOOGLE_0() const { return ___STORE_GOOGLE_0; }
	inline String_t** get_address_of_STORE_GOOGLE_0() { return &___STORE_GOOGLE_0; }
	inline void set_STORE_GOOGLE_0(String_t* value)
	{
		___STORE_GOOGLE_0 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_GOOGLE_0, value);
	}

	inline static int32_t get_offset_of_STORE_AMAZON_1() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_AMAZON_1)); }
	inline String_t* get_STORE_AMAZON_1() const { return ___STORE_AMAZON_1; }
	inline String_t** get_address_of_STORE_AMAZON_1() { return &___STORE_AMAZON_1; }
	inline void set_STORE_AMAZON_1(String_t* value)
	{
		___STORE_AMAZON_1 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_AMAZON_1, value);
	}

	inline static int32_t get_offset_of_STORE_SAMSUNG_2() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_SAMSUNG_2)); }
	inline String_t* get_STORE_SAMSUNG_2() const { return ___STORE_SAMSUNG_2; }
	inline String_t** get_address_of_STORE_SAMSUNG_2() { return &___STORE_SAMSUNG_2; }
	inline void set_STORE_SAMSUNG_2(String_t* value)
	{
		___STORE_SAMSUNG_2 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_SAMSUNG_2, value);
	}

	inline static int32_t get_offset_of_STORE_NOKIA_3() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_NOKIA_3)); }
	inline String_t* get_STORE_NOKIA_3() const { return ___STORE_NOKIA_3; }
	inline String_t** get_address_of_STORE_NOKIA_3() { return &___STORE_NOKIA_3; }
	inline void set_STORE_NOKIA_3(String_t* value)
	{
		___STORE_NOKIA_3 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_NOKIA_3, value);
	}

	inline static int32_t get_offset_of_STORE_SKUBIT_4() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_SKUBIT_4)); }
	inline String_t* get_STORE_SKUBIT_4() const { return ___STORE_SKUBIT_4; }
	inline String_t** get_address_of_STORE_SKUBIT_4() { return &___STORE_SKUBIT_4; }
	inline void set_STORE_SKUBIT_4(String_t* value)
	{
		___STORE_SKUBIT_4 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_SKUBIT_4, value);
	}

	inline static int32_t get_offset_of_STORE_SKUBIT_TEST_5() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_SKUBIT_TEST_5)); }
	inline String_t* get_STORE_SKUBIT_TEST_5() const { return ___STORE_SKUBIT_TEST_5; }
	inline String_t** get_address_of_STORE_SKUBIT_TEST_5() { return &___STORE_SKUBIT_TEST_5; }
	inline void set_STORE_SKUBIT_TEST_5(String_t* value)
	{
		___STORE_SKUBIT_TEST_5 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_SKUBIT_TEST_5, value);
	}

	inline static int32_t get_offset_of_STORE_YANDEX_6() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_YANDEX_6)); }
	inline String_t* get_STORE_YANDEX_6() const { return ___STORE_YANDEX_6; }
	inline String_t** get_address_of_STORE_YANDEX_6() { return &___STORE_YANDEX_6; }
	inline void set_STORE_YANDEX_6(String_t* value)
	{
		___STORE_YANDEX_6 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_YANDEX_6, value);
	}

	inline static int32_t get_offset_of_STORE_APPLAND_7() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_APPLAND_7)); }
	inline String_t* get_STORE_APPLAND_7() const { return ___STORE_APPLAND_7; }
	inline String_t** get_address_of_STORE_APPLAND_7() { return &___STORE_APPLAND_7; }
	inline void set_STORE_APPLAND_7(String_t* value)
	{
		___STORE_APPLAND_7 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_APPLAND_7, value);
	}

	inline static int32_t get_offset_of_STORE_SLIDEME_8() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_SLIDEME_8)); }
	inline String_t* get_STORE_SLIDEME_8() const { return ___STORE_SLIDEME_8; }
	inline String_t** get_address_of_STORE_SLIDEME_8() { return &___STORE_SLIDEME_8; }
	inline void set_STORE_SLIDEME_8(String_t* value)
	{
		___STORE_SLIDEME_8 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_SLIDEME_8, value);
	}

	inline static int32_t get_offset_of_STORE_APTOIDE_9() { return static_cast<int32_t>(offsetof(OpenIAB_Android_t58679140_StaticFields, ___STORE_APTOIDE_9)); }
	inline String_t* get_STORE_APTOIDE_9() const { return ___STORE_APTOIDE_9; }
	inline String_t** get_address_of_STORE_APTOIDE_9() { return &___STORE_APTOIDE_9; }
	inline void set_STORE_APTOIDE_9(String_t* value)
	{
		___STORE_APTOIDE_9 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_APTOIDE_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
