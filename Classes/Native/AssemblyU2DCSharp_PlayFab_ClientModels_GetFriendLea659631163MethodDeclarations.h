﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult
struct GetFriendLeaderboardAroundCurrentUserResult_t659631163;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>
struct List_1_t3038850335;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult::.ctor()
extern "C"  void GetFriendLeaderboardAroundCurrentUserResult__ctor_m4080615630 (GetFriendLeaderboardAroundCurrentUserResult_t659631163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetFriendLeaderboardAroundCurrentUserResult_get_Leaderboard_m1620700299 (GetFriendLeaderboardAroundCurrentUserResult_t659631163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetFriendLeaderboardAroundCurrentUserResult_set_Leaderboard_m2879914404 (GetFriendLeaderboardAroundCurrentUserResult_t659631163 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
