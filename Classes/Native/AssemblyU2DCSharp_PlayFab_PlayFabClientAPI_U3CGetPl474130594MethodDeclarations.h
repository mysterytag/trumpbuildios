﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPlayerTrades>c__AnonStoreyD3
struct U3CGetPlayerTradesU3Ec__AnonStoreyD3_t474130594;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPlayerTrades>c__AnonStoreyD3::.ctor()
extern "C"  void U3CGetPlayerTradesU3Ec__AnonStoreyD3__ctor_m3293940977 (U3CGetPlayerTradesU3Ec__AnonStoreyD3_t474130594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPlayerTrades>c__AnonStoreyD3::<>m__C0(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPlayerTradesU3Ec__AnonStoreyD3_U3CU3Em__C0_m3820905372 (U3CGetPlayerTradesU3Ec__AnonStoreyD3_t474130594 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
