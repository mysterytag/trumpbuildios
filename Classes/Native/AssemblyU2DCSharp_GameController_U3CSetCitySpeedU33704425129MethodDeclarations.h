﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<SetCitySpeed>c__Iterator11
struct U3CSetCitySpeedU3Ec__Iterator11_t3704425129;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<SetCitySpeed>c__Iterator11::.ctor()
extern "C"  void U3CSetCitySpeedU3Ec__Iterator11__ctor_m4061837363 (U3CSetCitySpeedU3Ec__Iterator11_t3704425129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<SetCitySpeed>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetCitySpeedU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4078202185 (U3CSetCitySpeedU3Ec__Iterator11_t3704425129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<SetCitySpeed>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetCitySpeedU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m2180260573 (U3CSetCitySpeedU3Ec__Iterator11_t3704425129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<SetCitySpeed>c__Iterator11::MoveNext()
extern "C"  bool U3CSetCitySpeedU3Ec__Iterator11_MoveNext_m3016854473 (U3CSetCitySpeedU3Ec__Iterator11_t3704425129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<SetCitySpeed>c__Iterator11::Dispose()
extern "C"  void U3CSetCitySpeedU3Ec__Iterator11_Dispose_m3493684144 (U3CSetCitySpeedU3Ec__Iterator11_t3704425129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<SetCitySpeed>c__Iterator11::Reset()
extern "C"  void U3CSetCitySpeedU3Ec__Iterator11_Reset_m1708270304 (U3CSetCitySpeedU3Ec__Iterator11_t3704425129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
