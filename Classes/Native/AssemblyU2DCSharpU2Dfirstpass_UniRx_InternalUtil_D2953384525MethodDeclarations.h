﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct DisposedObserver_1_t2953384525;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRepl3778927063.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m448195900_gshared (DisposedObserver_1_t2953384525 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m448195900(__this, method) ((  void (*) (DisposedObserver_1_t2953384525 *, const MethodInfo*))DisposedObserver_1__ctor_m448195900_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m527074801_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m527074801(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m527074801_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3052553158_gshared (DisposedObserver_1_t2953384525 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3052553158(__this, method) ((  void (*) (DisposedObserver_1_t2953384525 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3052553158_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1000831987_gshared (DisposedObserver_1_t2953384525 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1000831987(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2953384525 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1000831987_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m4050233572_gshared (DisposedObserver_1_t2953384525 * __this, DictionaryReplaceEvent_2_t3778927063  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m4050233572(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2953384525 *, DictionaryReplaceEvent_2_t3778927063 , const MethodInfo*))DisposedObserver_1_OnNext_m4050233572_gshared)(__this, ___value0, method)
