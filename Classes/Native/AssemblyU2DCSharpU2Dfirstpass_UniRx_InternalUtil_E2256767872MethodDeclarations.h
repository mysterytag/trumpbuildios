﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<System.Int64>
struct EmptyObserver_1_t2256767872;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2458143710_gshared (EmptyObserver_1_t2256767872 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2458143710(__this, method) ((  void (*) (EmptyObserver_1_t2256767872 *, const MethodInfo*))EmptyObserver_1__ctor_m2458143710_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2705914767_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m2705914767(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m2705914767_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1668456680_gshared (EmptyObserver_1_t2256767872 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1668456680(__this, method) ((  void (*) (EmptyObserver_1_t2256767872 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1668456680_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4025169429_gshared (EmptyObserver_1_t2256767872 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m4025169429(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2256767872 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m4025169429_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int64>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2874795782_gshared (EmptyObserver_1_t2256767872 * __this, int64_t ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2874795782(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2256767872 *, int64_t, const MethodInfo*))EmptyObserver_1_OnNext_m2874795782_gshared)(__this, ___value0, method)
