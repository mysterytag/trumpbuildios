﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Double,System.Object>
struct Dictionary_2_t566215076;
// System.Collections.Generic.IEqualityComparer`1<System.Double>
struct IEqualityComparer_1_t2858783265;
// System.Collections.Generic.IDictionary`2<System.Double,System.Object>
struct IDictionary_2_t1741880863;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Collections.Generic.ICollection`1<System.Double>
struct ICollection_1_t1000348000;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.ICollection
struct ICollection_t3761522009;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Double,System.Object>[]
struct KeyValuePair_2U5BU5D_t3948568611;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Double,System.Object>>
struct IEnumerator_1_t1537852822;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Double,System.Object>
struct KeyCollection_t2889490356;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Double,System.Object>
struct ValueCollection_t2488352170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g54746374.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En333243017.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m4071119057_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4071119057(__this, method) ((  void (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2__ctor_m4071119057_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3184154824_gshared (Dictionary_2_t566215076 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3184154824(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3184154824_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m4235244903_gshared (Dictionary_2_t566215076 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m4235244903(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4235244903_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m275405986_gshared (Dictionary_2_t566215076 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m275405986(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t566215076 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m275405986_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2635226358_gshared (Dictionary_2_t566215076 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2635226358(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2635226358_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2259240091_gshared (Dictionary_2_t566215076 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2259240091(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t566215076 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2259240091_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m4116881042_gshared (Dictionary_2_t566215076 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m4116881042(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t566215076 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2__ctor_m4116881042_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2271903581_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2271903581(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2271903581_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1367842653_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1367842653(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1367842653_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3305636951_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3305636951(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3305636951_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m808865669_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m808865669(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m808865669_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1541941936_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1541941936(__this, method) ((  bool (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1541941936_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1889222217_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1889222217(__this, method) ((  bool (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1889222217_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m4109861271_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m4109861271(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t566215076 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m4109861271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1014997500_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1014997500(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1014997500_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m642797269_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m642797269(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m642797269_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m47007361_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m47007361(__this, ___key0, method) ((  bool (*) (Dictionary_2_t566215076 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m47007361_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1364038714_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1364038714(__this, ___key0, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1364038714_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m828956787_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m828956787(__this, method) ((  bool (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m828956787_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3197866015_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3197866015(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3197866015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2065683191_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2065683191(__this, method) ((  bool (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2065683191_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1863211984_gshared (Dictionary_2_t566215076 * __this, KeyValuePair_2_t54746374  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1863211984(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t566215076 *, KeyValuePair_2_t54746374 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1863211984_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1389521266_gshared (Dictionary_2_t566215076 * __this, KeyValuePair_2_t54746374  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1389521266(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t566215076 *, KeyValuePair_2_t54746374 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1389521266_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2144991156_gshared (Dictionary_2_t566215076 * __this, KeyValuePair_2U5BU5D_t3948568611* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2144991156(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t566215076 *, KeyValuePair_2U5BU5D_t3948568611*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2144991156_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1987069207_gshared (Dictionary_2_t566215076 * __this, KeyValuePair_2_t54746374  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1987069207(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t566215076 *, KeyValuePair_2_t54746374 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1987069207_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3542590867_gshared (Dictionary_2_t566215076 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3542590867(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3542590867_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m255009358_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m255009358(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m255009358_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m72665803_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m72665803(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m72665803_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Double,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1829996006_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1829996006(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1829996006_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Double,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1120260025_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1120260025(__this, method) ((  int32_t (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_get_Count_m1120260025_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Double,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3169476114_gshared (Dictionary_2_t566215076 * __this, double ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3169476114(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t566215076 *, double, const MethodInfo*))Dictionary_2_get_Item_m3169476114_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3578023121_gshared (Dictionary_2_t566215076 * __this, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3578023121(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t566215076 *, double, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m3578023121_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1634609225_gshared (Dictionary_2_t566215076 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1634609225(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t566215076 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1634609225_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3651559054_gshared (Dictionary_2_t566215076 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3651559054(__this, ___size0, method) ((  void (*) (Dictionary_2_t566215076 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3651559054_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m862038538_gshared (Dictionary_2_t566215076 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m862038538(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m862038538_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Double,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t54746374  Dictionary_2_make_pair_m1472712022_gshared (Il2CppObject * __this /* static, unused */, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1472712022(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t54746374  (*) (Il2CppObject * /* static, unused */, double, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1472712022_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Double,System.Object>::pick_key(TKey,TValue)
extern "C"  double Dictionary_2_pick_key_m564257760_gshared (Il2CppObject * __this /* static, unused */, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m564257760(__this /* static, unused */, ___key0, ___value1, method) ((  double (*) (Il2CppObject * /* static, unused */, double, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m564257760_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Double,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m1725906592_gshared (Il2CppObject * __this /* static, unused */, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1725906592(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, double, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m1725906592_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m4101526149_gshared (Dictionary_2_t566215076 * __this, KeyValuePair_2U5BU5D_t3948568611* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m4101526149(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t566215076 *, KeyValuePair_2U5BU5D_t3948568611*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m4101526149_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1708435847_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1708435847(__this, method) ((  void (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_Resize_m1708435847_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2771859204_gshared (Dictionary_2_t566215076 * __this, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2771859204(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t566215076 *, double, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2771859204_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1477252348_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1477252348(__this, method) ((  void (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_Clear_m1477252348_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m756629218_gshared (Dictionary_2_t566215076 * __this, double ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m756629218(__this, ___key0, method) ((  bool (*) (Dictionary_2_t566215076 *, double, const MethodInfo*))Dictionary_2_ContainsKey_m756629218_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1662407138_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1662407138(__this, ___value0, method) ((  bool (*) (Dictionary_2_t566215076 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m1662407138_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3207410671_gshared (Dictionary_2_t566215076 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3207410671(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t566215076 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2_GetObjectData_m3207410671_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Double,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2908448213_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2908448213(__this, ___sender0, method) ((  void (*) (Dictionary_2_t566215076 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2908448213_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2978985166_gshared (Dictionary_2_t566215076 * __this, double ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2978985166(__this, ___key0, method) ((  bool (*) (Dictionary_2_t566215076 *, double, const MethodInfo*))Dictionary_2_Remove_m2978985166_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2331088187_gshared (Dictionary_2_t566215076 * __this, double ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2331088187(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t566215076 *, double, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2331088187_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Double,System.Object>::get_Keys()
extern "C"  KeyCollection_t2889490356 * Dictionary_2_get_Keys_m1512618660_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1512618660(__this, method) ((  KeyCollection_t2889490356 * (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_get_Keys_m1512618660_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Double,System.Object>::get_Values()
extern "C"  ValueCollection_t2488352170 * Dictionary_2_get_Values_m2893867556_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2893867556(__this, method) ((  ValueCollection_t2488352170 * (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_get_Values_m2893867556_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Double,System.Object>::ToTKey(System.Object)
extern "C"  double Dictionary_2_ToTKey_m14116667_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m14116667(__this, ___key0, method) ((  double (*) (Dictionary_2_t566215076 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m14116667_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Double,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m38220731_gshared (Dictionary_2_t566215076 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m38220731(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t566215076 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m38220731_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Double,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m740520625_gshared (Dictionary_2_t566215076 * __this, KeyValuePair_2_t54746374  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m740520625(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t566215076 *, KeyValuePair_2_t54746374 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m740520625_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Double,System.Object>::GetEnumerator()
extern "C"  Enumerator_t333243018  Dictionary_2_GetEnumerator_m1703555030_gshared (Dictionary_2_t566215076 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1703555030(__this, method) ((  Enumerator_t333243018  (*) (Dictionary_2_t566215076 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1703555030_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Double,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t130027246  Dictionary_2_U3CCopyToU3Em__0_m4074189989_gshared (Il2CppObject * __this /* static, unused */, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m4074189989(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t130027246  (*) (Il2CppObject * /* static, unused */, double, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m4074189989_gshared)(__this /* static, unused */, ___key0, ___value1, method)
