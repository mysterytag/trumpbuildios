﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult
struct GetFriendLeaderboardAroundPlayerResult_t589881892;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>
struct List_1_t3038850335;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult::.ctor()
extern "C"  void GetFriendLeaderboardAroundPlayerResult__ctor_m740834073 (GetFriendLeaderboardAroundPlayerResult_t589881892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry> PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult::get_Leaderboard()
extern "C"  List_1_t3038850335 * GetFriendLeaderboardAroundPlayerResult_get_Leaderboard_m262406048 (GetFriendLeaderboardAroundPlayerResult_t589881892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendLeaderboardAroundPlayerResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern "C"  void GetFriendLeaderboardAroundPlayerResult_set_Leaderboard_m3862934255 (GetFriendLeaderboardAroundPlayerResult_t589881892 * __this, List_1_t3038850335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
