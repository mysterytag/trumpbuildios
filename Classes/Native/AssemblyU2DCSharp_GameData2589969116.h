﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UpgradeDescriptor[]
struct UpgradeDescriptorU5BU5D_t3305946858;
// SerializableDictionary`2<System.String,AchievementDescription>
struct SerializableDictionary_2_t2406034027;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameData
struct  GameData_t2589969116  : public Il2CppObject
{
public:
	// UpgradeDescriptor[] GameData::payUpgrades
	UpgradeDescriptorU5BU5D_t3305946858* ___payUpgrades_0;
	// UpgradeDescriptor[] GameData::billUpgrades
	UpgradeDescriptorU5BU5D_t3305946858* ___billUpgrades_1;
	// UpgradeDescriptor[] GameData::safeUpgrades
	UpgradeDescriptorU5BU5D_t3305946858* ___safeUpgrades_2;
	// UpgradeDescriptor[] GameData::passiveUpgrades
	UpgradeDescriptorU5BU5D_t3305946858* ___passiveUpgrades_3;
	// SerializableDictionary`2<System.String,AchievementDescription> GameData::achievements
	SerializableDictionary_2_t2406034027 * ___achievements_4;

public:
	inline static int32_t get_offset_of_payUpgrades_0() { return static_cast<int32_t>(offsetof(GameData_t2589969116, ___payUpgrades_0)); }
	inline UpgradeDescriptorU5BU5D_t3305946858* get_payUpgrades_0() const { return ___payUpgrades_0; }
	inline UpgradeDescriptorU5BU5D_t3305946858** get_address_of_payUpgrades_0() { return &___payUpgrades_0; }
	inline void set_payUpgrades_0(UpgradeDescriptorU5BU5D_t3305946858* value)
	{
		___payUpgrades_0 = value;
		Il2CppCodeGenWriteBarrier(&___payUpgrades_0, value);
	}

	inline static int32_t get_offset_of_billUpgrades_1() { return static_cast<int32_t>(offsetof(GameData_t2589969116, ___billUpgrades_1)); }
	inline UpgradeDescriptorU5BU5D_t3305946858* get_billUpgrades_1() const { return ___billUpgrades_1; }
	inline UpgradeDescriptorU5BU5D_t3305946858** get_address_of_billUpgrades_1() { return &___billUpgrades_1; }
	inline void set_billUpgrades_1(UpgradeDescriptorU5BU5D_t3305946858* value)
	{
		___billUpgrades_1 = value;
		Il2CppCodeGenWriteBarrier(&___billUpgrades_1, value);
	}

	inline static int32_t get_offset_of_safeUpgrades_2() { return static_cast<int32_t>(offsetof(GameData_t2589969116, ___safeUpgrades_2)); }
	inline UpgradeDescriptorU5BU5D_t3305946858* get_safeUpgrades_2() const { return ___safeUpgrades_2; }
	inline UpgradeDescriptorU5BU5D_t3305946858** get_address_of_safeUpgrades_2() { return &___safeUpgrades_2; }
	inline void set_safeUpgrades_2(UpgradeDescriptorU5BU5D_t3305946858* value)
	{
		___safeUpgrades_2 = value;
		Il2CppCodeGenWriteBarrier(&___safeUpgrades_2, value);
	}

	inline static int32_t get_offset_of_passiveUpgrades_3() { return static_cast<int32_t>(offsetof(GameData_t2589969116, ___passiveUpgrades_3)); }
	inline UpgradeDescriptorU5BU5D_t3305946858* get_passiveUpgrades_3() const { return ___passiveUpgrades_3; }
	inline UpgradeDescriptorU5BU5D_t3305946858** get_address_of_passiveUpgrades_3() { return &___passiveUpgrades_3; }
	inline void set_passiveUpgrades_3(UpgradeDescriptorU5BU5D_t3305946858* value)
	{
		___passiveUpgrades_3 = value;
		Il2CppCodeGenWriteBarrier(&___passiveUpgrades_3, value);
	}

	inline static int32_t get_offset_of_achievements_4() { return static_cast<int32_t>(offsetof(GameData_t2589969116, ___achievements_4)); }
	inline SerializableDictionary_2_t2406034027 * get_achievements_4() const { return ___achievements_4; }
	inline SerializableDictionary_2_t2406034027 ** get_address_of_achievements_4() { return &___achievements_4; }
	inline void set_achievements_4(SerializableDictionary_2_t2406034027 * value)
	{
		___achievements_4 = value;
		Il2CppCodeGenWriteBarrier(&___achievements_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
