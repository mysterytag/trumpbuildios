﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>
struct DoOnTerminate_t2350010872;
// UniRx.Operators.DoOnTerminateObservable`1<System.Object>
struct DoOnTerminateObservable_1_t3263525393;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::.ctor(UniRx.Operators.DoOnTerminateObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnTerminate__ctor_m2580601697_gshared (DoOnTerminate_t2350010872 * __this, DoOnTerminateObservable_1_t3263525393 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DoOnTerminate__ctor_m2580601697(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DoOnTerminate_t2350010872 *, DoOnTerminateObservable_1_t3263525393 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnTerminate__ctor_m2580601697_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::Run()
extern "C"  Il2CppObject * DoOnTerminate_Run_m3579094941_gshared (DoOnTerminate_t2350010872 * __this, const MethodInfo* method);
#define DoOnTerminate_Run_m3579094941(__this, method) ((  Il2CppObject * (*) (DoOnTerminate_t2350010872 *, const MethodInfo*))DoOnTerminate_Run_m3579094941_gshared)(__this, method)
// System.Void UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::OnNext(T)
extern "C"  void DoOnTerminate_OnNext_m525404663_gshared (DoOnTerminate_t2350010872 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DoOnTerminate_OnNext_m525404663(__this, ___value0, method) ((  void (*) (DoOnTerminate_t2350010872 *, Il2CppObject *, const MethodInfo*))DoOnTerminate_OnNext_m525404663_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::OnError(System.Exception)
extern "C"  void DoOnTerminate_OnError_m357062406_gshared (DoOnTerminate_t2350010872 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DoOnTerminate_OnError_m357062406(__this, ___error0, method) ((  void (*) (DoOnTerminate_t2350010872 *, Exception_t1967233988 *, const MethodInfo*))DoOnTerminate_OnError_m357062406_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DoOnTerminateObservable`1/DoOnTerminate<System.Object>::OnCompleted()
extern "C"  void DoOnTerminate_OnCompleted_m4031570777_gshared (DoOnTerminate_t2350010872 * __this, const MethodInfo* method);
#define DoOnTerminate_OnCompleted_m4031570777(__this, method) ((  void (*) (DoOnTerminate_t2350010872 *, const MethodInfo*))DoOnTerminate_OnCompleted_m4031570777_gshared)(__this, method)
