﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// AdsManager
struct AdsManager_t814343421;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdsManager/<InterstitialAdsCoroutine>c__IteratorB
struct  U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503  : public Il2CppObject
{
public:
	// System.Int32 AdsManager/<InterstitialAdsCoroutine>c__IteratorB::$PC
	int32_t ___U24PC_0;
	// System.Object AdsManager/<InterstitialAdsCoroutine>c__IteratorB::$current
	Il2CppObject * ___U24current_1;
	// AdsManager AdsManager/<InterstitialAdsCoroutine>c__IteratorB::<>f__this
	AdsManager_t814343421 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503, ___U24current_1)); }
	inline Il2CppObject * get_U24current_1() const { return ___U24current_1; }
	inline Il2CppObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(Il2CppObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503, ___U3CU3Ef__this_2)); }
	inline AdsManager_t814343421 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline AdsManager_t814343421 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(AdsManager_t814343421 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
