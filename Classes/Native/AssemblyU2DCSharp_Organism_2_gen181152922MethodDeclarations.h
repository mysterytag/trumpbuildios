﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Organism_2_gen948289219MethodDeclarations.h"

// System.Void Organism`2<System.Object,IRoot>::.ctor()
#define Organism_2__ctor_m472793266(__this, method) ((  void (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2__ctor_m4082911749_gshared)(__this, method)
// TCarrier Organism`2<System.Object,IRoot>::get_carrier()
#define Organism_2_get_carrier_m1883469688(__this, method) ((  Il2CppObject * (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_get_carrier_m4029902219_gshared)(__this, method)
// System.Void Organism`2<System.Object,IRoot>::set_carrier(TCarrier)
#define Organism_2_set_carrier_m684610183(__this, ___value0, method) ((  void (*) (Organism_2_t181152922 *, Il2CppObject *, const MethodInfo*))Organism_2_set_carrier_m2958823066_gshared)(__this, ___value0, method)
// TRoot Organism`2<System.Object,IRoot>::get_root()
#define Organism_2_get_root_m2819486656(__this, method) ((  Il2CppObject * (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_get_root_m2383393421_gshared)(__this, method)
// System.Void Organism`2<System.Object,IRoot>::set_root(TRoot)
#define Organism_2_set_root_m4151140395(__this, ___value0, method) ((  void (*) (Organism_2_t181152922 *, Il2CppObject *, const MethodInfo*))Organism_2_set_root_m984770174_gshared)(__this, ___value0, method)
// IOnceEmptyStream Organism`2<System.Object,IRoot>::get_fellAsleep()
#define Organism_2_get_fellAsleep_m2977553910(__this, method) ((  Il2CppObject * (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_get_fellAsleep_m2050669571_gshared)(__this, method)
// System.Void Organism`2<System.Object,IRoot>::DisposeWhenAsleep(System.IDisposable)
#define Organism_2_DisposeWhenAsleep_m2146898005(__this, ___disposable0, method) ((  void (*) (Organism_2_t181152922 *, Il2CppObject *, const MethodInfo*))Organism_2_DisposeWhenAsleep_m153634408_gshared)(__this, ___disposable0, method)
// System.Boolean Organism`2<System.Object,IRoot>::get_isAlive()
#define Organism_2_get_isAlive_m3529533886(__this, method) ((  bool (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_get_isAlive_m434369745_gshared)(__this, method)
// System.Boolean Organism`2<System.Object,IRoot>::get_primal()
#define Organism_2_get_primal_m241151768(__this, method) ((  bool (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_get_primal_m1803875749_gshared)(__this, method)
// System.Void Organism`2<System.Object,IRoot>::Setup(IOrganism,IRoot)
#define Organism_2_Setup_m1643172331(__this, ___c0, ___r1, method) ((  void (*) (Organism_2_t181152922 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Organism_2_Setup_m3424294968_gshared)(__this, ___c0, ___r1, method)
// System.Void Organism`2<System.Object,IRoot>::WakeUp()
#define Organism_2_WakeUp_m2464736689(__this, method) ((  void (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_WakeUp_m2709259966_gshared)(__this, method)
// System.Void Organism`2<System.Object,IRoot>::Spread()
#define Organism_2_Spread_m416865989(__this, method) ((  void (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_Spread_m661389266_gshared)(__this, method)
// System.Void Organism`2<System.Object,IRoot>::Collect(System.IDisposable)
#define Organism_2_Collect_m1193356880(__this, ___tail0, method) ((  void (*) (Organism_2_t181152922 *, Il2CppObject *, const MethodInfo*))Organism_2_Collect_m831016995_gshared)(__this, ___tail0, method)
// System.Void Organism`2<System.Object,IRoot>::set_collect(System.IDisposable)
#define Organism_2_set_collect_m71362707(__this, ___value0, method) ((  void (*) (Organism_2_t181152922 *, Il2CppObject *, const MethodInfo*))Organism_2_set_collect_m1070393574_gshared)(__this, ___value0, method)
// System.Void Organism`2<System.Object,IRoot>::ToSleep()
#define Organism_2_ToSleep_m3539000364(__this, method) ((  void (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_ToSleep_m2529287359_gshared)(__this, method)
// System.Void Organism`2<System.Object,IRoot>::Put(IOrganism)
#define Organism_2_Put_m2103422808(__this, ___organism0, method) ((  void (*) (Organism_2_t181152922 *, Il2CppObject *, const MethodInfo*))Organism_2_Put_m1862734501_gshared)(__this, ___organism0, method)
// System.Void Organism`2<System.Object,IRoot>::Take(IOrganism)
#define Organism_2_Take_m3506424926(__this, ___organism0, method) ((  void (*) (Organism_2_t181152922 *, Il2CppObject *, const MethodInfo*))Organism_2_Take_m340054705_gshared)(__this, ___organism0, method)
// IOrganism Organism`2<System.Object,IRoot>::ChangeRoot(TRoot)
#define Organism_2_ChangeRoot_m508179024(__this, ___r0, method) ((  Il2CppObject * (*) (Organism_2_t181152922 *, Il2CppObject *, const MethodInfo*))Organism_2_ChangeRoot_m2085091299_gshared)(__this, ___r0, method)
// System.Void Organism`2<System.Object,IRoot>::RegisterChildren()
#define Organism_2_RegisterChildren_m689197588(__this, method) ((  void (*) (Organism_2_t181152922 *, const MethodInfo*))Organism_2_RegisterChildren_m1720665825_gshared)(__this, method)
// System.Void Organism`2<System.Object,IRoot>::<WakeUp>m__1AD(IOrganism)
#define Organism_2_U3CWakeUpU3Em__1AD_m3991463899(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CWakeUpU3Em__1AD_m1095000686_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<System.Object,IRoot>::<Spread>m__1AE(IOrganism)
#define Organism_2_U3CSpreadU3Em__1AE_m1136516686(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CSpreadU3Em__1AE_m2535020769_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<System.Object,IRoot>::<ToSleep>m__1AF(IOrganism)
#define Organism_2_U3CToSleepU3Em__1AF_m2811091296(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CToSleepU3Em__1AF_m3215044909_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<System.Object,IRoot>::<ToSleep>m__1B0(System.IDisposable)
#define Organism_2_U3CToSleepU3Em__1B0_m760601526(__this /* static, unused */, ___d0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CToSleepU3Em__1B0_m3346233993_gshared)(__this /* static, unused */, ___d0, method)
