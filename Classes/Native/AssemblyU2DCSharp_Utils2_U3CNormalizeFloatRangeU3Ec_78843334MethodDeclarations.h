﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils2/<NormalizeFloatRange>c__AnonStoreyF1
struct U3CNormalizeFloatRangeU3Ec__AnonStoreyF1_t78843334;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils2/<NormalizeFloatRange>c__AnonStoreyF1::.ctor()
extern "C"  void U3CNormalizeFloatRangeU3Ec__AnonStoreyF1__ctor_m1141568663 (U3CNormalizeFloatRangeU3Ec__AnonStoreyF1_t78843334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils2/<NormalizeFloatRange>c__AnonStoreyF1::<>m__159(System.Single)
extern "C"  float U3CNormalizeFloatRangeU3Ec__AnonStoreyF1_U3CU3Em__159_m3020951616 (U3CNormalizeFloatRangeU3Ec__AnonStoreyF1_t78843334 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
