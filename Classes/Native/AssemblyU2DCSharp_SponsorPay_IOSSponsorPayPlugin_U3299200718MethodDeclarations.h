﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.IOSSponsorPayPlugin/<LaunchOfferWall>c__AnonStorey152
struct U3CLaunchOfferWallU3Ec__AnonStorey152_t299200718;

#include "codegen/il2cpp-codegen.h"

// System.Void SponsorPay.IOSSponsorPayPlugin/<LaunchOfferWall>c__AnonStorey152::.ctor()
extern "C"  void U3CLaunchOfferWallU3Ec__AnonStorey152__ctor_m2300241614 (U3CLaunchOfferWallU3Ec__AnonStorey152_t299200718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin/<LaunchOfferWall>c__AnonStorey152::<>m__205(System.Int32)
extern "C"  void U3CLaunchOfferWallU3Ec__AnonStorey152_U3CU3Em__205_m3001604339 (U3CLaunchOfferWallU3Ec__AnonStorey152_t299200718 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
