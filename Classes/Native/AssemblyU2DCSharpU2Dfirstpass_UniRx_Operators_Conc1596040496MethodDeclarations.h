﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>
struct U3CCombineSourcesU3Ec__Iterator18_t1596040496;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::.ctor()
extern "C"  void U3CCombineSourcesU3Ec__Iterator18__ctor_m845918298_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__Iterator18__ctor_m845918298(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))U3CCombineSourcesU3Ec__Iterator18__ctor_m845918298_gshared)(__this, method)
// UniRx.IObservable`1<T> UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::System.Collections.Generic.IEnumerator<UniRx.IObservable<T>>.get_Current()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m84683126_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m84683126(__this, method) ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))U3CCombineSourcesU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m84683126_gshared)(__this, method)
// System.Object UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m2440641484_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m2440641484(__this, method) ((  Il2CppObject * (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))U3CCombineSourcesU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m2440641484_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCombineSourcesU3Ec__Iterator18_System_Collections_IEnumerable_GetEnumerator_m933254623_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__Iterator18_System_Collections_IEnumerable_GetEnumerator_m933254623(__this, method) ((  Il2CppObject * (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))U3CCombineSourcesU3Ec__Iterator18_System_Collections_IEnumerable_GetEnumerator_m933254623_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::System.Collections.Generic.IEnumerable<UniRx.IObservable<T>>.GetEnumerator()
extern "C"  Il2CppObject* U3CCombineSourcesU3Ec__Iterator18_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m1598665587_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__Iterator18_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m1598665587(__this, method) ((  Il2CppObject* (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))U3CCombineSourcesU3Ec__Iterator18_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m1598665587_gshared)(__this, method)
// System.Boolean UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::MoveNext()
extern "C"  bool U3CCombineSourcesU3Ec__Iterator18_MoveNext_m4290604914_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__Iterator18_MoveNext_m4290604914(__this, method) ((  bool (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))U3CCombineSourcesU3Ec__Iterator18_MoveNext_m4290604914_gshared)(__this, method)
// System.Void UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::Dispose()
extern "C"  void U3CCombineSourcesU3Ec__Iterator18_Dispose_m1076948503_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__Iterator18_Dispose_m1076948503(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))U3CCombineSourcesU3Ec__Iterator18_Dispose_m1076948503_gshared)(__this, method)
// System.Void UniRx.Operators.ConcatObservable`1/<CombineSources>c__Iterator18<System.Object>::Reset()
extern "C"  void U3CCombineSourcesU3Ec__Iterator18_Reset_m2787318535_gshared (U3CCombineSourcesU3Ec__Iterator18_t1596040496 * __this, const MethodInfo* method);
#define U3CCombineSourcesU3Ec__Iterator18_Reset_m2787318535(__this, method) ((  void (*) (U3CCombineSourcesU3Ec__Iterator18_t1596040496 *, const MethodInfo*))U3CCombineSourcesU3Ec__Iterator18_Reset_m2787318535_gshared)(__this, method)
