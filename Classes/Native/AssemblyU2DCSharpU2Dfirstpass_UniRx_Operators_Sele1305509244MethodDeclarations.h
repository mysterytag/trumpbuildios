﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Double,System.Double,System.Double>
struct SelectManyObserver_t1305509244;
// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Double,System.Double,System.Double>
struct SelectManyObserverWithIndex_t885021286;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Double,System.Double,System.Double>::.ctor(UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<TSource,TCollection,TResult>,TSource,System.Int32,System.IDisposable)
extern "C"  void SelectManyObserver__ctor_m677168303_gshared (SelectManyObserver_t1305509244 * __this, SelectManyObserverWithIndex_t885021286 * ___parent0, double ___value1, int32_t ___index2, Il2CppObject * ___cancel3, const MethodInfo* method);
#define SelectManyObserver__ctor_m677168303(__this, ___parent0, ___value1, ___index2, ___cancel3, method) ((  void (*) (SelectManyObserver_t1305509244 *, SelectManyObserverWithIndex_t885021286 *, double, int32_t, Il2CppObject *, const MethodInfo*))SelectManyObserver__ctor_m677168303_gshared)(__this, ___parent0, ___value1, ___index2, ___cancel3, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Double,System.Double,System.Double>::OnNext(TCollection)
extern "C"  void SelectManyObserver_OnNext_m338014083_gshared (SelectManyObserver_t1305509244 * __this, double ___value0, const MethodInfo* method);
#define SelectManyObserver_OnNext_m338014083(__this, ___value0, method) ((  void (*) (SelectManyObserver_t1305509244 *, double, const MethodInfo*))SelectManyObserver_OnNext_m338014083_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Double,System.Double,System.Double>::OnError(System.Exception)
extern "C"  void SelectManyObserver_OnError_m2951178640_gshared (SelectManyObserver_t1305509244 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserver_OnError_m2951178640(__this, ___error0, method) ((  void (*) (SelectManyObserver_t1305509244 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserver_OnError_m2951178640_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Double,System.Double,System.Double>::OnCompleted()
extern "C"  void SelectManyObserver_OnCompleted_m1666970851_gshared (SelectManyObserver_t1305509244 * __this, const MethodInfo* method);
#define SelectManyObserver_OnCompleted_m1666970851(__this, method) ((  void (*) (SelectManyObserver_t1305509244 *, const MethodInfo*))SelectManyObserver_OnCompleted_m1666970851_gshared)(__this, method)
