﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DisposableManager/DisposableInfo
struct DisposableInfo_t1284166222;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.DisposableManager/DisposableInfo::.ctor(System.IDisposable,System.Int32)
extern "C"  void DisposableInfo__ctor_m1783949365 (DisposableInfo_t1284166222 * __this, Il2CppObject * ___disposable0, int32_t ___priority1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
