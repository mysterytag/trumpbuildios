﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenLog
struct ScreenLog_t2129044248;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void ScreenLog::.ctor()
extern "C"  void ScreenLog__ctor_m182349139 (ScreenLog_t2129044248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenLog::.cctor()
extern "C"  void ScreenLog__cctor_m875759802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenLog::OnGUI()
extern "C"  void ScreenLog_OnGUI_m3972715085 (ScreenLog_t2129044248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenLog::AddMessage(System.String)
extern "C"  void ScreenLog_AddMessage_m2067164203 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenLog::Clear()
extern "C"  void ScreenLog_Clear_m1883449726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
