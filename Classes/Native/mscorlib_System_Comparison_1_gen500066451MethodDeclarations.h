﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen1721664028MethodDeclarations.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m467148048(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t500066451 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m449166892_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::Invoke(T,T)
#define Comparison_1_Invoke_m691203304(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t500066451 *, KeyValuePair_2_t2091358871 , KeyValuePair_2_t2091358871 , const MethodInfo*))Comparison_1_Invoke_m2456628300_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1884996145(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t500066451 *, KeyValuePair_2_t2091358871 , KeyValuePair_2_t2091358871 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m898417557_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m2717846148(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t500066451 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m2038311072_gshared)(__this, ___result0, method)
