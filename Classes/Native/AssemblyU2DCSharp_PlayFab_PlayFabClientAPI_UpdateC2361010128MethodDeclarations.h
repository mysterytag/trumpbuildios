﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateCharacterDataResponseCallback
struct UpdateCharacterDataResponseCallback_t2361010128;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateCharacterDataRequest
struct UpdateCharacterDataRequest_t3002197813;
// PlayFab.ClientModels.UpdateCharacterDataResult
struct UpdateCharacterDataResult_t1101824215;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara3002197813.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara1101824215.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterDataResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateCharacterDataResponseCallback__ctor_m3476431967 (UpdateCharacterDataResponseCallback_t2361010128 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterDataResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateCharacterDataRequest,PlayFab.ClientModels.UpdateCharacterDataResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UpdateCharacterDataResponseCallback_Invoke_m2803723166 (UpdateCharacterDataResponseCallback_t2361010128 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterDataRequest_t3002197813 * ___request2, UpdateCharacterDataResult_t1101824215 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateCharacterDataResponseCallback_t2361010128(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterDataRequest_t3002197813 * ___request2, UpdateCharacterDataResult_t1101824215 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateCharacterDataResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateCharacterDataRequest,PlayFab.ClientModels.UpdateCharacterDataResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateCharacterDataResponseCallback_BeginInvoke_m3944675311 (UpdateCharacterDataResponseCallback_t2361010128 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterDataRequest_t3002197813 * ___request2, UpdateCharacterDataResult_t1101824215 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterDataResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateCharacterDataResponseCallback_EndInvoke_m1406180847 (UpdateCharacterDataResponseCallback_t2361010128 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
