﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.ISubject`1<System.Object>
struct ISubject_1_t353709935;
// System.Object
struct Il2CppObject;
// UniRx.Observable/ConnectableObservable`1/Connection<System.Object>
struct Connection_t217434055;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/ConnectableObservable`1<System.Object>
struct  ConnectableObservable_1_t608944977  : public Il2CppObject
{
public:
	// UniRx.IObservable`1<T> UniRx.Observable/ConnectableObservable`1::source
	Il2CppObject* ___source_0;
	// UniRx.ISubject`1<T> UniRx.Observable/ConnectableObservable`1::subject
	Il2CppObject* ___subject_1;
	// System.Object UniRx.Observable/ConnectableObservable`1::gate
	Il2CppObject * ___gate_2;
	// UniRx.Observable/ConnectableObservable`1/Connection<T> UniRx.Observable/ConnectableObservable`1::connection
	Connection_t217434055 * ___connection_3;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(ConnectableObservable_1_t608944977, ___source_0)); }
	inline Il2CppObject* get_source_0() const { return ___source_0; }
	inline Il2CppObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Il2CppObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}

	inline static int32_t get_offset_of_subject_1() { return static_cast<int32_t>(offsetof(ConnectableObservable_1_t608944977, ___subject_1)); }
	inline Il2CppObject* get_subject_1() const { return ___subject_1; }
	inline Il2CppObject** get_address_of_subject_1() { return &___subject_1; }
	inline void set_subject_1(Il2CppObject* value)
	{
		___subject_1 = value;
		Il2CppCodeGenWriteBarrier(&___subject_1, value);
	}

	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(ConnectableObservable_1_t608944977, ___gate_2)); }
	inline Il2CppObject * get_gate_2() const { return ___gate_2; }
	inline Il2CppObject ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(Il2CppObject * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier(&___gate_2, value);
	}

	inline static int32_t get_offset_of_connection_3() { return static_cast<int32_t>(offsetof(ConnectableObservable_1_t608944977, ___connection_3)); }
	inline Connection_t217434055 * get_connection_3() const { return ___connection_3; }
	inline Connection_t217434055 ** get_address_of_connection_3() { return &___connection_3; }
	inline void set_connection_3(Connection_t217434055 * value)
	{
		___connection_3 = value;
		Il2CppCodeGenWriteBarrier(&___connection_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
