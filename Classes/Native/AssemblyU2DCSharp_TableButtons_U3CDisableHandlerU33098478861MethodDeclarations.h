﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<DisableHandler>c__AnonStorey158
struct U3CDisableHandlerU3Ec__AnonStorey158_t3098478861;

#include "codegen/il2cpp-codegen.h"

// System.Void TableButtons/<DisableHandler>c__AnonStorey158::.ctor()
extern "C"  void U3CDisableHandlerU3Ec__AnonStorey158__ctor_m3278758882 (U3CDisableHandlerU3Ec__AnonStorey158_t3098478861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<DisableHandler>c__AnonStorey158::<>m__22B(System.Double)
extern "C"  void U3CDisableHandlerU3Ec__AnonStorey158_U3CU3Em__22B_m1028038985 (U3CDisableHandlerU3Ec__AnonStorey158_t3098478861 * __this, double ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
