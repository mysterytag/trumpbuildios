﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PlayFab.ClientModels.UserTitleInfo
struct UserTitleInfo_t4099117963;
// PlayFab.ClientModels.UserPrivateAccountInfo
struct UserPrivateAccountInfo_t2894244435;
// PlayFab.ClientModels.UserFacebookInfo
struct UserFacebookInfo_t1491589167;
// PlayFab.ClientModels.UserSteamInfo
struct UserSteamInfo_t2867463811;
// PlayFab.ClientModels.UserGameCenterInfo
struct UserGameCenterInfo_t386684176;
// PlayFab.ClientModels.UserIosDeviceInfo
struct UserIosDeviceInfo_t3585390070;
// PlayFab.ClientModels.UserAndroidDeviceInfo
struct UserAndroidDeviceInfo_t810110808;
// PlayFab.ClientModels.UserKongregateInfo
struct UserKongregateInfo_t155682308;
// PlayFab.ClientModels.UserPsnInfo
struct UserPsnInfo_t260686782;
// PlayFab.ClientModels.UserGoogleInfo
struct UserGoogleInfo_t1689260258;
// PlayFab.ClientModels.UserXboxInfo
struct UserXboxInfo_t1624454844;
// PlayFab.ClientModels.UserCustomIdInfo
struct UserCustomIdInfo_t1941075669;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UserAccountInfo
struct  UserAccountInfo_t960776096  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.UserAccountInfo::<PlayFabId>k__BackingField
	String_t* ___U3CPlayFabIdU3Ek__BackingField_0;
	// System.DateTime PlayFab.ClientModels.UserAccountInfo::<Created>k__BackingField
	DateTime_t339033936  ___U3CCreatedU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.UserAccountInfo::<Username>k__BackingField
	String_t* ___U3CUsernameU3Ek__BackingField_2;
	// PlayFab.ClientModels.UserTitleInfo PlayFab.ClientModels.UserAccountInfo::<TitleInfo>k__BackingField
	UserTitleInfo_t4099117963 * ___U3CTitleInfoU3Ek__BackingField_3;
	// PlayFab.ClientModels.UserPrivateAccountInfo PlayFab.ClientModels.UserAccountInfo::<PrivateInfo>k__BackingField
	UserPrivateAccountInfo_t2894244435 * ___U3CPrivateInfoU3Ek__BackingField_4;
	// PlayFab.ClientModels.UserFacebookInfo PlayFab.ClientModels.UserAccountInfo::<FacebookInfo>k__BackingField
	UserFacebookInfo_t1491589167 * ___U3CFacebookInfoU3Ek__BackingField_5;
	// PlayFab.ClientModels.UserSteamInfo PlayFab.ClientModels.UserAccountInfo::<SteamInfo>k__BackingField
	UserSteamInfo_t2867463811 * ___U3CSteamInfoU3Ek__BackingField_6;
	// PlayFab.ClientModels.UserGameCenterInfo PlayFab.ClientModels.UserAccountInfo::<GameCenterInfo>k__BackingField
	UserGameCenterInfo_t386684176 * ___U3CGameCenterInfoU3Ek__BackingField_7;
	// PlayFab.ClientModels.UserIosDeviceInfo PlayFab.ClientModels.UserAccountInfo::<IosDeviceInfo>k__BackingField
	UserIosDeviceInfo_t3585390070 * ___U3CIosDeviceInfoU3Ek__BackingField_8;
	// PlayFab.ClientModels.UserAndroidDeviceInfo PlayFab.ClientModels.UserAccountInfo::<AndroidDeviceInfo>k__BackingField
	UserAndroidDeviceInfo_t810110808 * ___U3CAndroidDeviceInfoU3Ek__BackingField_9;
	// PlayFab.ClientModels.UserKongregateInfo PlayFab.ClientModels.UserAccountInfo::<KongregateInfo>k__BackingField
	UserKongregateInfo_t155682308 * ___U3CKongregateInfoU3Ek__BackingField_10;
	// PlayFab.ClientModels.UserPsnInfo PlayFab.ClientModels.UserAccountInfo::<PsnInfo>k__BackingField
	UserPsnInfo_t260686782 * ___U3CPsnInfoU3Ek__BackingField_11;
	// PlayFab.ClientModels.UserGoogleInfo PlayFab.ClientModels.UserAccountInfo::<GoogleInfo>k__BackingField
	UserGoogleInfo_t1689260258 * ___U3CGoogleInfoU3Ek__BackingField_12;
	// PlayFab.ClientModels.UserXboxInfo PlayFab.ClientModels.UserAccountInfo::<XboxInfo>k__BackingField
	UserXboxInfo_t1624454844 * ___U3CXboxInfoU3Ek__BackingField_13;
	// PlayFab.ClientModels.UserCustomIdInfo PlayFab.ClientModels.UserAccountInfo::<CustomIdInfo>k__BackingField
	UserCustomIdInfo_t1941075669 * ___U3CCustomIdInfoU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CPlayFabIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CPlayFabIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CPlayFabIdU3Ek__BackingField_0() const { return ___U3CPlayFabIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPlayFabIdU3Ek__BackingField_0() { return &___U3CPlayFabIdU3Ek__BackingField_0; }
	inline void set_U3CPlayFabIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CPlayFabIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayFabIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CCreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CCreatedU3Ek__BackingField_1)); }
	inline DateTime_t339033936  get_U3CCreatedU3Ek__BackingField_1() const { return ___U3CCreatedU3Ek__BackingField_1; }
	inline DateTime_t339033936 * get_address_of_U3CCreatedU3Ek__BackingField_1() { return &___U3CCreatedU3Ek__BackingField_1; }
	inline void set_U3CCreatedU3Ek__BackingField_1(DateTime_t339033936  value)
	{
		___U3CCreatedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CUsernameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CUsernameU3Ek__BackingField_2)); }
	inline String_t* get_U3CUsernameU3Ek__BackingField_2() const { return ___U3CUsernameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUsernameU3Ek__BackingField_2() { return &___U3CUsernameU3Ek__BackingField_2; }
	inline void set_U3CUsernameU3Ek__BackingField_2(String_t* value)
	{
		___U3CUsernameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUsernameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTitleInfoU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CTitleInfoU3Ek__BackingField_3)); }
	inline UserTitleInfo_t4099117963 * get_U3CTitleInfoU3Ek__BackingField_3() const { return ___U3CTitleInfoU3Ek__BackingField_3; }
	inline UserTitleInfo_t4099117963 ** get_address_of_U3CTitleInfoU3Ek__BackingField_3() { return &___U3CTitleInfoU3Ek__BackingField_3; }
	inline void set_U3CTitleInfoU3Ek__BackingField_3(UserTitleInfo_t4099117963 * value)
	{
		___U3CTitleInfoU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTitleInfoU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPrivateInfoU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CPrivateInfoU3Ek__BackingField_4)); }
	inline UserPrivateAccountInfo_t2894244435 * get_U3CPrivateInfoU3Ek__BackingField_4() const { return ___U3CPrivateInfoU3Ek__BackingField_4; }
	inline UserPrivateAccountInfo_t2894244435 ** get_address_of_U3CPrivateInfoU3Ek__BackingField_4() { return &___U3CPrivateInfoU3Ek__BackingField_4; }
	inline void set_U3CPrivateInfoU3Ek__BackingField_4(UserPrivateAccountInfo_t2894244435 * value)
	{
		___U3CPrivateInfoU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPrivateInfoU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CFacebookInfoU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CFacebookInfoU3Ek__BackingField_5)); }
	inline UserFacebookInfo_t1491589167 * get_U3CFacebookInfoU3Ek__BackingField_5() const { return ___U3CFacebookInfoU3Ek__BackingField_5; }
	inline UserFacebookInfo_t1491589167 ** get_address_of_U3CFacebookInfoU3Ek__BackingField_5() { return &___U3CFacebookInfoU3Ek__BackingField_5; }
	inline void set_U3CFacebookInfoU3Ek__BackingField_5(UserFacebookInfo_t1491589167 * value)
	{
		___U3CFacebookInfoU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFacebookInfoU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CSteamInfoU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CSteamInfoU3Ek__BackingField_6)); }
	inline UserSteamInfo_t2867463811 * get_U3CSteamInfoU3Ek__BackingField_6() const { return ___U3CSteamInfoU3Ek__BackingField_6; }
	inline UserSteamInfo_t2867463811 ** get_address_of_U3CSteamInfoU3Ek__BackingField_6() { return &___U3CSteamInfoU3Ek__BackingField_6; }
	inline void set_U3CSteamInfoU3Ek__BackingField_6(UserSteamInfo_t2867463811 * value)
	{
		___U3CSteamInfoU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSteamInfoU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CGameCenterInfoU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CGameCenterInfoU3Ek__BackingField_7)); }
	inline UserGameCenterInfo_t386684176 * get_U3CGameCenterInfoU3Ek__BackingField_7() const { return ___U3CGameCenterInfoU3Ek__BackingField_7; }
	inline UserGameCenterInfo_t386684176 ** get_address_of_U3CGameCenterInfoU3Ek__BackingField_7() { return &___U3CGameCenterInfoU3Ek__BackingField_7; }
	inline void set_U3CGameCenterInfoU3Ek__BackingField_7(UserGameCenterInfo_t386684176 * value)
	{
		___U3CGameCenterInfoU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameCenterInfoU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CIosDeviceInfoU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CIosDeviceInfoU3Ek__BackingField_8)); }
	inline UserIosDeviceInfo_t3585390070 * get_U3CIosDeviceInfoU3Ek__BackingField_8() const { return ___U3CIosDeviceInfoU3Ek__BackingField_8; }
	inline UserIosDeviceInfo_t3585390070 ** get_address_of_U3CIosDeviceInfoU3Ek__BackingField_8() { return &___U3CIosDeviceInfoU3Ek__BackingField_8; }
	inline void set_U3CIosDeviceInfoU3Ek__BackingField_8(UserIosDeviceInfo_t3585390070 * value)
	{
		___U3CIosDeviceInfoU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIosDeviceInfoU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CAndroidDeviceInfoU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CAndroidDeviceInfoU3Ek__BackingField_9)); }
	inline UserAndroidDeviceInfo_t810110808 * get_U3CAndroidDeviceInfoU3Ek__BackingField_9() const { return ___U3CAndroidDeviceInfoU3Ek__BackingField_9; }
	inline UserAndroidDeviceInfo_t810110808 ** get_address_of_U3CAndroidDeviceInfoU3Ek__BackingField_9() { return &___U3CAndroidDeviceInfoU3Ek__BackingField_9; }
	inline void set_U3CAndroidDeviceInfoU3Ek__BackingField_9(UserAndroidDeviceInfo_t810110808 * value)
	{
		___U3CAndroidDeviceInfoU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAndroidDeviceInfoU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CKongregateInfoU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CKongregateInfoU3Ek__BackingField_10)); }
	inline UserKongregateInfo_t155682308 * get_U3CKongregateInfoU3Ek__BackingField_10() const { return ___U3CKongregateInfoU3Ek__BackingField_10; }
	inline UserKongregateInfo_t155682308 ** get_address_of_U3CKongregateInfoU3Ek__BackingField_10() { return &___U3CKongregateInfoU3Ek__BackingField_10; }
	inline void set_U3CKongregateInfoU3Ek__BackingField_10(UserKongregateInfo_t155682308 * value)
	{
		___U3CKongregateInfoU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKongregateInfoU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CPsnInfoU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CPsnInfoU3Ek__BackingField_11)); }
	inline UserPsnInfo_t260686782 * get_U3CPsnInfoU3Ek__BackingField_11() const { return ___U3CPsnInfoU3Ek__BackingField_11; }
	inline UserPsnInfo_t260686782 ** get_address_of_U3CPsnInfoU3Ek__BackingField_11() { return &___U3CPsnInfoU3Ek__BackingField_11; }
	inline void set_U3CPsnInfoU3Ek__BackingField_11(UserPsnInfo_t260686782 * value)
	{
		___U3CPsnInfoU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPsnInfoU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CGoogleInfoU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CGoogleInfoU3Ek__BackingField_12)); }
	inline UserGoogleInfo_t1689260258 * get_U3CGoogleInfoU3Ek__BackingField_12() const { return ___U3CGoogleInfoU3Ek__BackingField_12; }
	inline UserGoogleInfo_t1689260258 ** get_address_of_U3CGoogleInfoU3Ek__BackingField_12() { return &___U3CGoogleInfoU3Ek__BackingField_12; }
	inline void set_U3CGoogleInfoU3Ek__BackingField_12(UserGoogleInfo_t1689260258 * value)
	{
		___U3CGoogleInfoU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGoogleInfoU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CXboxInfoU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CXboxInfoU3Ek__BackingField_13)); }
	inline UserXboxInfo_t1624454844 * get_U3CXboxInfoU3Ek__BackingField_13() const { return ___U3CXboxInfoU3Ek__BackingField_13; }
	inline UserXboxInfo_t1624454844 ** get_address_of_U3CXboxInfoU3Ek__BackingField_13() { return &___U3CXboxInfoU3Ek__BackingField_13; }
	inline void set_U3CXboxInfoU3Ek__BackingField_13(UserXboxInfo_t1624454844 * value)
	{
		___U3CXboxInfoU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CXboxInfoU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CCustomIdInfoU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(UserAccountInfo_t960776096, ___U3CCustomIdInfoU3Ek__BackingField_14)); }
	inline UserCustomIdInfo_t1941075669 * get_U3CCustomIdInfoU3Ek__BackingField_14() const { return ___U3CCustomIdInfoU3Ek__BackingField_14; }
	inline UserCustomIdInfo_t1941075669 ** get_address_of_U3CCustomIdInfoU3Ek__BackingField_14() { return &___U3CCustomIdInfoU3Ek__BackingField_14; }
	inline void set_U3CCustomIdInfoU3Ek__BackingField_14(UserCustomIdInfo_t1941075669 * value)
	{
		___U3CCustomIdInfoU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCustomIdInfoU3Ek__BackingField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
