﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UpdateUserDataRequest
struct  UpdateUserDataRequest_t3516896929  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.UpdateUserDataRequest::<Data>k__BackingField
	Dictionary_2_t2606186806 * ___U3CDataU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.UpdateUserDataRequest::<KeysToRemove>k__BackingField
	List_1_t1765447871 * ___U3CKeysToRemoveU3Ek__BackingField_1;
	// System.Nullable`1<PlayFab.ClientModels.UserDataPermission> PlayFab.ClientModels.UpdateUserDataRequest::<Permission>k__BackingField
	Nullable_1_t2611574664  ___U3CPermissionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UpdateUserDataRequest_t3516896929, ___U3CDataU3Ek__BackingField_0)); }
	inline Dictionary_2_t2606186806 * get_U3CDataU3Ek__BackingField_0() const { return ___U3CDataU3Ek__BackingField_0; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CDataU3Ek__BackingField_0() { return &___U3CDataU3Ek__BackingField_0; }
	inline void set_U3CDataU3Ek__BackingField_0(Dictionary_2_t2606186806 * value)
	{
		___U3CDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CKeysToRemoveU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UpdateUserDataRequest_t3516896929, ___U3CKeysToRemoveU3Ek__BackingField_1)); }
	inline List_1_t1765447871 * get_U3CKeysToRemoveU3Ek__BackingField_1() const { return ___U3CKeysToRemoveU3Ek__BackingField_1; }
	inline List_1_t1765447871 ** get_address_of_U3CKeysToRemoveU3Ek__BackingField_1() { return &___U3CKeysToRemoveU3Ek__BackingField_1; }
	inline void set_U3CKeysToRemoveU3Ek__BackingField_1(List_1_t1765447871 * value)
	{
		___U3CKeysToRemoveU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeysToRemoveU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CPermissionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UpdateUserDataRequest_t3516896929, ___U3CPermissionU3Ek__BackingField_2)); }
	inline Nullable_1_t2611574664  get_U3CPermissionU3Ek__BackingField_2() const { return ___U3CPermissionU3Ek__BackingField_2; }
	inline Nullable_1_t2611574664 * get_address_of_U3CPermissionU3Ek__BackingField_2() { return &___U3CPermissionU3Ek__BackingField_2; }
	inline void set_U3CPermissionU3Ek__BackingField_2(Nullable_1_t2611574664  value)
	{
		___U3CPermissionU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
