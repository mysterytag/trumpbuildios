﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.MonoBehaviourSingletonProvider
struct MonoBehaviourSingletonProvider_t3595575479;
// System.Type
struct Type_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.MonoBehaviourSingletonProvider::.ctor(System.Type,Zenject.DiContainer,UnityEngine.GameObject)
extern "C"  void MonoBehaviourSingletonProvider__ctor_m266568240 (MonoBehaviourSingletonProvider_t3595575479 * __this, Type_t * ___componentType0, DiContainer_t2383114449 * ___container1, GameObject_t4012695102 * ___gameObject2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.MonoBehaviourSingletonProvider::GetInstanceType()
extern "C"  Type_t * MonoBehaviourSingletonProvider_GetInstanceType_m1433484227 (MonoBehaviourSingletonProvider_t3595575479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.MonoBehaviourSingletonProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * MonoBehaviourSingletonProvider_GetInstance_m2305383973 (MonoBehaviourSingletonProvider_t3595575479 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.MonoBehaviourSingletonProvider::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* MonoBehaviourSingletonProvider_ValidateBinding_m2475255627 (MonoBehaviourSingletonProvider_t3595575479 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
