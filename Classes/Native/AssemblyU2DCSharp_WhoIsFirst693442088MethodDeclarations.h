﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WhoIsFirst
struct WhoIsFirst_t693442088;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Collections.Generic.IEnumerable`1<IProcess>
struct IEnumerable_1_t2603424474;
// IProcess
struct IProcess_t4026237414;

#include "codegen/il2cpp-codegen.h"

// System.Void WhoIsFirst::.ctor(System.Action`1<System.Int32>,System.Collections.Generic.IEnumerable`1<IProcess>)
extern "C"  void WhoIsFirst__ctor_m3961649105 (WhoIsFirst_t693442088 * __this, Action_1_t2995867492 * ___result0, Il2CppObject* ___inProcesses1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WhoIsFirst::get_finished()
extern "C"  bool WhoIsFirst_get_finished_m3608731008 (WhoIsFirst_t693442088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WhoIsFirst::SetFinished(IProcess)
extern "C"  void WhoIsFirst_SetFinished_m856193375 (WhoIsFirst_t693442088 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
