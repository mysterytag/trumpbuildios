﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetFriendsListRequestCallback
struct GetFriendsListRequestCallback_t2916847479;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetFriendsListRequest
struct GetFriendsListRequest_t3925362978;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendsL3925362978.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetFriendsListRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFriendsListRequestCallback__ctor_m556753030 (GetFriendsListRequestCallback_t2916847479 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendsListRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendsListRequest,System.Object)
extern "C"  void GetFriendsListRequestCallback_Invoke_m1351624555 (GetFriendsListRequestCallback_t2916847479 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendsListRequest_t3925362978 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetFriendsListRequestCallback_t2916847479(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetFriendsListRequest_t3925362978 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetFriendsListRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendsListRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFriendsListRequestCallback_BeginInvoke_m3840555390 (GetFriendsListRequestCallback_t2916847479 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendsListRequest_t3925362978 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendsListRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetFriendsListRequestCallback_EndInvoke_m2460222870 (GetFriendsListRequestCallback_t2916847479 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
