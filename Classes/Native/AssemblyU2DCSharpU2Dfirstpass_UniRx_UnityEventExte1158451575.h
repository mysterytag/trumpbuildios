﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t1065917714;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9C`4<System.Object,System.Object,System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575  : public Il2CppObject
{
public:
	// UnityEngine.Events.UnityEvent`4<T0,T1,T2,T3> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9C`4::unityEvent
	UnityEvent_4_t1065917714 * ___unityEvent_0;

public:
	inline static int32_t get_offset_of_unityEvent_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575, ___unityEvent_0)); }
	inline UnityEvent_4_t1065917714 * get_unityEvent_0() const { return ___unityEvent_0; }
	inline UnityEvent_4_t1065917714 ** get_address_of_unityEvent_0() { return &___unityEvent_0; }
	inline void set_unityEvent_0(UnityEvent_4_t1065917714 * value)
	{
		___unityEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___unityEvent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
