﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Single>
struct U3CReportU3Ec__AnonStorey59_t941352311;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Single>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey59__ctor_m4232380630_gshared (U3CReportU3Ec__AnonStorey59_t941352311 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey59__ctor_m4232380630(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey59_t941352311 *, const MethodInfo*))U3CReportU3Ec__AnonStorey59__ctor_m4232380630_gshared)(__this, method)
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Single>::<>m__6D()
extern "C"  void U3CReportU3Ec__AnonStorey59_U3CU3Em__6D_m2291964269_gshared (U3CReportU3Ec__AnonStorey59_t941352311 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey59_U3CU3Em__6D_m2291964269(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey59_t941352311 *, const MethodInfo*))U3CReportU3Ec__AnonStorey59_U3CU3Em__6D_m2291964269_gshared)(__this, method)
