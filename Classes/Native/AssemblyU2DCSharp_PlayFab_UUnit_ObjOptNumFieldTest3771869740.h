﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.UUnit.ObjOptNumFieldTest
struct ObjOptNumFieldTest_t3771869740;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1446416676.h"
#include "mscorlib_System_Nullable_1_gen1369764433.h"
#include "mscorlib_System_Nullable_1_gen1438485341.h"
#include "mscorlib_System_Nullable_1_gen3871963176.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"
#include "mscorlib_System_Nullable_1_gen1438485494.h"
#include "mscorlib_System_Nullable_1_gen3871963329.h"
#include "mscorlib_System_Nullable_1_gen3844246929.h"
#include "mscorlib_System_Nullable_1_gen3420554522.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.ObjOptNumFieldTest
struct  ObjOptNumFieldTest_t3771869740  : public Il2CppObject
{
public:
	// System.Nullable`1<System.SByte> PlayFab.UUnit.ObjOptNumFieldTest::<SbyteValue>k__BackingField
	Nullable_1_t1446416676  ___U3CSbyteValueU3Ek__BackingField_4;
	// System.Nullable`1<System.Byte> PlayFab.UUnit.ObjOptNumFieldTest::<ByteValue>k__BackingField
	Nullable_1_t1369764433  ___U3CByteValueU3Ek__BackingField_5;
	// System.Nullable`1<System.Int16> PlayFab.UUnit.ObjOptNumFieldTest::<ShortValue>k__BackingField
	Nullable_1_t1438485341  ___U3CShortValueU3Ek__BackingField_6;
	// System.Nullable`1<System.UInt16> PlayFab.UUnit.ObjOptNumFieldTest::<UshortValue>k__BackingField
	Nullable_1_t3871963176  ___U3CUshortValueU3Ek__BackingField_7;
	// System.Nullable`1<System.Int32> PlayFab.UUnit.ObjOptNumFieldTest::<IntValue>k__BackingField
	Nullable_1_t1438485399  ___U3CIntValueU3Ek__BackingField_8;
	// System.Nullable`1<System.UInt32> PlayFab.UUnit.ObjOptNumFieldTest::<UintValue>k__BackingField
	Nullable_1_t3871963234  ___U3CUintValueU3Ek__BackingField_9;
	// System.Nullable`1<System.Int64> PlayFab.UUnit.ObjOptNumFieldTest::<LongValue>k__BackingField
	Nullable_1_t1438485494  ___U3CLongValueU3Ek__BackingField_10;
	// System.Nullable`1<System.UInt64> PlayFab.UUnit.ObjOptNumFieldTest::<UlongValue>k__BackingField
	Nullable_1_t3871963329  ___U3CUlongValueU3Ek__BackingField_11;
	// System.Nullable`1<System.Single> PlayFab.UUnit.ObjOptNumFieldTest::<FloatValue>k__BackingField
	Nullable_1_t3844246929  ___U3CFloatValueU3Ek__BackingField_12;
	// System.Nullable`1<System.Double> PlayFab.UUnit.ObjOptNumFieldTest::<DoubleValue>k__BackingField
	Nullable_1_t3420554522  ___U3CDoubleValueU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CSbyteValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CSbyteValueU3Ek__BackingField_4)); }
	inline Nullable_1_t1446416676  get_U3CSbyteValueU3Ek__BackingField_4() const { return ___U3CSbyteValueU3Ek__BackingField_4; }
	inline Nullable_1_t1446416676 * get_address_of_U3CSbyteValueU3Ek__BackingField_4() { return &___U3CSbyteValueU3Ek__BackingField_4; }
	inline void set_U3CSbyteValueU3Ek__BackingField_4(Nullable_1_t1446416676  value)
	{
		___U3CSbyteValueU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CByteValueU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CByteValueU3Ek__BackingField_5)); }
	inline Nullable_1_t1369764433  get_U3CByteValueU3Ek__BackingField_5() const { return ___U3CByteValueU3Ek__BackingField_5; }
	inline Nullable_1_t1369764433 * get_address_of_U3CByteValueU3Ek__BackingField_5() { return &___U3CByteValueU3Ek__BackingField_5; }
	inline void set_U3CByteValueU3Ek__BackingField_5(Nullable_1_t1369764433  value)
	{
		___U3CByteValueU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CShortValueU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CShortValueU3Ek__BackingField_6)); }
	inline Nullable_1_t1438485341  get_U3CShortValueU3Ek__BackingField_6() const { return ___U3CShortValueU3Ek__BackingField_6; }
	inline Nullable_1_t1438485341 * get_address_of_U3CShortValueU3Ek__BackingField_6() { return &___U3CShortValueU3Ek__BackingField_6; }
	inline void set_U3CShortValueU3Ek__BackingField_6(Nullable_1_t1438485341  value)
	{
		___U3CShortValueU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CUshortValueU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CUshortValueU3Ek__BackingField_7)); }
	inline Nullable_1_t3871963176  get_U3CUshortValueU3Ek__BackingField_7() const { return ___U3CUshortValueU3Ek__BackingField_7; }
	inline Nullable_1_t3871963176 * get_address_of_U3CUshortValueU3Ek__BackingField_7() { return &___U3CUshortValueU3Ek__BackingField_7; }
	inline void set_U3CUshortValueU3Ek__BackingField_7(Nullable_1_t3871963176  value)
	{
		___U3CUshortValueU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIntValueU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CIntValueU3Ek__BackingField_8)); }
	inline Nullable_1_t1438485399  get_U3CIntValueU3Ek__BackingField_8() const { return ___U3CIntValueU3Ek__BackingField_8; }
	inline Nullable_1_t1438485399 * get_address_of_U3CIntValueU3Ek__BackingField_8() { return &___U3CIntValueU3Ek__BackingField_8; }
	inline void set_U3CIntValueU3Ek__BackingField_8(Nullable_1_t1438485399  value)
	{
		___U3CIntValueU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CUintValueU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CUintValueU3Ek__BackingField_9)); }
	inline Nullable_1_t3871963234  get_U3CUintValueU3Ek__BackingField_9() const { return ___U3CUintValueU3Ek__BackingField_9; }
	inline Nullable_1_t3871963234 * get_address_of_U3CUintValueU3Ek__BackingField_9() { return &___U3CUintValueU3Ek__BackingField_9; }
	inline void set_U3CUintValueU3Ek__BackingField_9(Nullable_1_t3871963234  value)
	{
		___U3CUintValueU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CLongValueU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CLongValueU3Ek__BackingField_10)); }
	inline Nullable_1_t1438485494  get_U3CLongValueU3Ek__BackingField_10() const { return ___U3CLongValueU3Ek__BackingField_10; }
	inline Nullable_1_t1438485494 * get_address_of_U3CLongValueU3Ek__BackingField_10() { return &___U3CLongValueU3Ek__BackingField_10; }
	inline void set_U3CLongValueU3Ek__BackingField_10(Nullable_1_t1438485494  value)
	{
		___U3CLongValueU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CUlongValueU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CUlongValueU3Ek__BackingField_11)); }
	inline Nullable_1_t3871963329  get_U3CUlongValueU3Ek__BackingField_11() const { return ___U3CUlongValueU3Ek__BackingField_11; }
	inline Nullable_1_t3871963329 * get_address_of_U3CUlongValueU3Ek__BackingField_11() { return &___U3CUlongValueU3Ek__BackingField_11; }
	inline void set_U3CUlongValueU3Ek__BackingField_11(Nullable_1_t3871963329  value)
	{
		___U3CUlongValueU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CFloatValueU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CFloatValueU3Ek__BackingField_12)); }
	inline Nullable_1_t3844246929  get_U3CFloatValueU3Ek__BackingField_12() const { return ___U3CFloatValueU3Ek__BackingField_12; }
	inline Nullable_1_t3844246929 * get_address_of_U3CFloatValueU3Ek__BackingField_12() { return &___U3CFloatValueU3Ek__BackingField_12; }
	inline void set_U3CFloatValueU3Ek__BackingField_12(Nullable_1_t3844246929  value)
	{
		___U3CFloatValueU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CDoubleValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740, ___U3CDoubleValueU3Ek__BackingField_13)); }
	inline Nullable_1_t3420554522  get_U3CDoubleValueU3Ek__BackingField_13() const { return ___U3CDoubleValueU3Ek__BackingField_13; }
	inline Nullable_1_t3420554522 * get_address_of_U3CDoubleValueU3Ek__BackingField_13() { return &___U3CDoubleValueU3Ek__BackingField_13; }
	inline void set_U3CDoubleValueU3Ek__BackingField_13(Nullable_1_t3420554522  value)
	{
		___U3CDoubleValueU3Ek__BackingField_13 = value;
	}
};

struct ObjOptNumFieldTest_t3771869740_StaticFields
{
public:
	// PlayFab.UUnit.ObjOptNumFieldTest PlayFab.UUnit.ObjOptNumFieldTest::Max
	ObjOptNumFieldTest_t3771869740 * ___Max_0;
	// PlayFab.UUnit.ObjOptNumFieldTest PlayFab.UUnit.ObjOptNumFieldTest::Min
	ObjOptNumFieldTest_t3771869740 * ___Min_1;
	// PlayFab.UUnit.ObjOptNumFieldTest PlayFab.UUnit.ObjOptNumFieldTest::Zero
	ObjOptNumFieldTest_t3771869740 * ___Zero_2;
	// PlayFab.UUnit.ObjOptNumFieldTest PlayFab.UUnit.ObjOptNumFieldTest::Null
	ObjOptNumFieldTest_t3771869740 * ___Null_3;

public:
	inline static int32_t get_offset_of_Max_0() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740_StaticFields, ___Max_0)); }
	inline ObjOptNumFieldTest_t3771869740 * get_Max_0() const { return ___Max_0; }
	inline ObjOptNumFieldTest_t3771869740 ** get_address_of_Max_0() { return &___Max_0; }
	inline void set_Max_0(ObjOptNumFieldTest_t3771869740 * value)
	{
		___Max_0 = value;
		Il2CppCodeGenWriteBarrier(&___Max_0, value);
	}

	inline static int32_t get_offset_of_Min_1() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740_StaticFields, ___Min_1)); }
	inline ObjOptNumFieldTest_t3771869740 * get_Min_1() const { return ___Min_1; }
	inline ObjOptNumFieldTest_t3771869740 ** get_address_of_Min_1() { return &___Min_1; }
	inline void set_Min_1(ObjOptNumFieldTest_t3771869740 * value)
	{
		___Min_1 = value;
		Il2CppCodeGenWriteBarrier(&___Min_1, value);
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740_StaticFields, ___Zero_2)); }
	inline ObjOptNumFieldTest_t3771869740 * get_Zero_2() const { return ___Zero_2; }
	inline ObjOptNumFieldTest_t3771869740 ** get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(ObjOptNumFieldTest_t3771869740 * value)
	{
		___Zero_2 = value;
		Il2CppCodeGenWriteBarrier(&___Zero_2, value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(ObjOptNumFieldTest_t3771869740_StaticFields, ___Null_3)); }
	inline ObjOptNumFieldTest_t3771869740 * get_Null_3() const { return ___Null_3; }
	inline ObjOptNumFieldTest_t3771869740 ** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(ObjOptNumFieldTest_t3771869740 * value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier(&___Null_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
