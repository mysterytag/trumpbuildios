﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromCoroutine>c__AnonStorey4A
struct U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Observable/<FromCoroutine>c__AnonStorey4A::.ctor()
extern "C"  void U3CFromCoroutineU3Ec__AnonStorey4A__ctor_m2466286203 (U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable/<FromCoroutine>c__AnonStorey4A::<>m__48(UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CFromCoroutineU3Ec__AnonStorey4A_U3CU3Em__48_m3829821962 (U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
