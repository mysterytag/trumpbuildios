﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1<System.Object>
struct WhereObservable_1_t2035666369;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Func`3<System.Object,System.Int32,System.Boolean>
struct Func_3_t3064567499;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhereObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m3146672172_gshared (WhereObservable_1_t2035666369 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___predicate1, const MethodInfo* method);
#define WhereObservable_1__ctor_m3146672172(__this, ___source0, ___predicate1, method) ((  void (*) (WhereObservable_1_t2035666369 *, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))WhereObservable_1__ctor_m3146672172_gshared)(__this, ___source0, ___predicate1, method)
// System.Void UniRx.Operators.WhereObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m1939124858_gshared (WhereObservable_1_t2035666369 * __this, Il2CppObject* ___source0, Func_3_t3064567499 * ___predicateWithIndex1, const MethodInfo* method);
#define WhereObservable_1__ctor_m1939124858(__this, ___source0, ___predicateWithIndex1, method) ((  void (*) (WhereObservable_1_t2035666369 *, Il2CppObject*, Func_3_t3064567499 *, const MethodInfo*))WhereObservable_1__ctor_m1939124858_gshared)(__this, ___source0, ___predicateWithIndex1, method)
// UniRx.IObservable`1<T> UniRx.Operators.WhereObservable`1<System.Object>::CombinePredicate(System.Func`2<T,System.Boolean>)
extern "C"  Il2CppObject* WhereObservable_1_CombinePredicate_m2037959187_gshared (WhereObservable_1_t2035666369 * __this, Func_2_t1509682273 * ___combinePredicate0, const MethodInfo* method);
#define WhereObservable_1_CombinePredicate_m2037959187(__this, ___combinePredicate0, method) ((  Il2CppObject* (*) (WhereObservable_1_t2035666369 *, Func_2_t1509682273 *, const MethodInfo*))WhereObservable_1_CombinePredicate_m2037959187_gshared)(__this, ___combinePredicate0, method)
// System.IDisposable UniRx.Operators.WhereObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * WhereObservable_1_SubscribeCore_m1925634247_gshared (WhereObservable_1_t2035666369 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define WhereObservable_1_SubscribeCore_m1925634247(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (WhereObservable_1_t2035666369 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhereObservable_1_SubscribeCore_m1925634247_gshared)(__this, ___observer0, ___cancel1, method)
