﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct FromEvent_t2732089756;
// UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct FromEventObservable_2_t2442263458;
// UniRx.IObserver`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct IObserver_1_t3239229184;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_4_gen1027230281.h"

// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m1555144599_gshared (FromEvent_t2732089756 * __this, FromEventObservable_2_t2442263458 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m1555144599(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t2732089756 *, FromEventObservable_2_t2442263458 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m1555144599_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::Register()
extern "C"  bool FromEvent_Register_m122661685_gshared (FromEvent_t2732089756 * __this, const MethodInfo* method);
#define FromEvent_Register_m122661685(__this, method) ((  bool (*) (FromEvent_t2732089756 *, const MethodInfo*))FromEvent_Register_m122661685_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m582067869_gshared (FromEvent_t2732089756 * __this, Tuple_4_t1027230281  ___args0, const MethodInfo* method);
#define FromEvent_OnNext_m582067869(__this, ___args0, method) ((  void (*) (FromEvent_t2732089756 *, Tuple_4_t1027230281 , const MethodInfo*))FromEvent_OnNext_m582067869_gshared)(__this, ___args0, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::Dispose()
extern "C"  void FromEvent_Dispose_m1930688339_gshared (FromEvent_t2732089756 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m1930688339(__this, method) ((  void (*) (FromEvent_t2732089756 *, const MethodInfo*))FromEvent_Dispose_m1930688339_gshared)(__this, method)
