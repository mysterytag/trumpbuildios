﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.Util.Debugging.ProfileBlock
struct ProfileBlock_t2731282750;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ModestTree.Util.Debugging.ProfileBlock::.ctor()
extern "C"  void ProfileBlock__ctor_m2697136375 (ProfileBlock_t2731282750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ModestTree.Util.Debugging.ProfileBlock ModestTree.Util.Debugging.ProfileBlock::Start()
extern "C"  ProfileBlock_t2731282750 * ProfileBlock_Start_m854692478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ModestTree.Util.Debugging.ProfileBlock ModestTree.Util.Debugging.ProfileBlock::Start(System.String)
extern "C"  ProfileBlock_t2731282750 * ProfileBlock_Start_m1530300740 (Il2CppObject * __this /* static, unused */, String_t* ___sampleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ModestTree.Util.Debugging.ProfileBlock ModestTree.Util.Debugging.ProfileBlock::Start(System.String,System.Object)
extern "C"  ProfileBlock_t2731282750 * ProfileBlock_Start_m3968945426 (Il2CppObject * __this /* static, unused */, String_t* ___sampleNameFormat0, Il2CppObject * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ModestTree.Util.Debugging.ProfileBlock ModestTree.Util.Debugging.ProfileBlock::Start(System.String,System.Object,System.Object)
extern "C"  ProfileBlock_t2731282750 * ProfileBlock_Start_m2297057376 (Il2CppObject * __this /* static, unused */, String_t* ___sampleNameFormat0, Il2CppObject * ___obj11, Il2CppObject * ___obj22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Util.Debugging.ProfileBlock::Dispose()
extern "C"  void ProfileBlock_Dispose_m1981059956 (ProfileBlock_t2731282750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
