﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<System.Action`1<System.Single>,UnityEngine.Events.UnityAction`1<System.Single>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2338920712(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2410444992 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Action`1<System.Single>,UnityEngine.Events.UnityAction`1<System.Single>>::Invoke(T)
#define Func_2_Invoke_m2094534110(__this, ___arg10, method) ((  UnityAction_1_t938670926 * (*) (Func_2_t2410444992 *, Action_1_t1106661726 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Action`1<System.Single>,UnityEngine.Events.UnityAction`1<System.Single>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1567255697(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2410444992 *, Action_1_t1106661726 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Action`1<System.Single>,UnityEngine.Events.UnityAction`1<System.Single>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m728640694(__this, ___result0, method) ((  UnityAction_1_t938670926 * (*) (Func_2_t2410444992 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
