﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Organism`2/<Setup>c__AnonStorey11F<System.Object,System.Object>
struct U3CSetupU3Ec__AnonStorey11F_t4199016171;
// IOrganism
struct IOrganism_t2580843579;

#include "codegen/il2cpp-codegen.h"

// System.Void Organism`2/<Setup>c__AnonStorey11F<System.Object,System.Object>::.ctor()
extern "C"  void U3CSetupU3Ec__AnonStorey11F__ctor_m1680593202_gshared (U3CSetupU3Ec__AnonStorey11F_t4199016171 * __this, const MethodInfo* method);
#define U3CSetupU3Ec__AnonStorey11F__ctor_m1680593202(__this, method) ((  void (*) (U3CSetupU3Ec__AnonStorey11F_t4199016171 *, const MethodInfo*))U3CSetupU3Ec__AnonStorey11F__ctor_m1680593202_gshared)(__this, method)
// System.Void Organism`2/<Setup>c__AnonStorey11F<System.Object,System.Object>::<>m__1AC(IOrganism)
extern "C"  void U3CSetupU3Ec__AnonStorey11F_U3CU3Em__1AC_m466440573_gshared (U3CSetupU3Ec__AnonStorey11F_t4199016171 * __this, Il2CppObject * ___child0, const MethodInfo* method);
#define U3CSetupU3Ec__AnonStorey11F_U3CU3Em__1AC_m466440573(__this, ___child0, method) ((  void (*) (U3CSetupU3Ec__AnonStorey11F_t4199016171 *, Il2CppObject *, const MethodInfo*))U3CSetupU3Ec__AnonStorey11F_U3CU3Em__1AC_m466440573_gshared)(__this, ___child0, method)
