﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>
struct U3CDoThisU3Ec__AnonStorey12B_t2640087060;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23615068783.h"

// System.Void StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>::.ctor()
extern "C"  void U3CDoThisU3Ec__AnonStorey12B__ctor_m1274937601_gshared (U3CDoThisU3Ec__AnonStorey12B_t2640087060 * __this, const MethodInfo* method);
#define U3CDoThisU3Ec__AnonStorey12B__ctor_m1274937601(__this, method) ((  void (*) (U3CDoThisU3Ec__AnonStorey12B_t2640087060 *, const MethodInfo*))U3CDoThisU3Ec__AnonStorey12B__ctor_m1274937601_gshared)(__this, method)
// System.Void StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>::<>m__1BD()
extern "C"  void U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BD_m4100598539_gshared (U3CDoThisU3Ec__AnonStorey12B_t2640087060 * __this, const MethodInfo* method);
#define U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BD_m4100598539(__this, method) ((  void (*) (U3CDoThisU3Ec__AnonStorey12B_t2640087060 *, const MethodInfo*))U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BD_m4100598539_gshared)(__this, method)
// System.Boolean StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>::<>m__1BE(System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<T>>)
extern "C"  bool U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BE_m3459815349_gshared (U3CDoThisU3Ec__AnonStorey12B_t2640087060 * __this, KeyValuePair_2_t3615068783  ___tuple10, const MethodInfo* method);
#define U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BE_m3459815349(__this, ___tuple10, method) ((  bool (*) (U3CDoThisU3Ec__AnonStorey12B_t2640087060 *, KeyValuePair_2_t3615068783 , const MethodInfo*))U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BE_m3459815349_gshared)(__this, ___tuple10, method)
