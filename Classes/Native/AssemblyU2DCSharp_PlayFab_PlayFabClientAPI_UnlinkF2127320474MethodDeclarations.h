﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkFacebookAccountResponseCallback
struct UnlinkFacebookAccountResponseCallback_t2127320474;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkFacebookAccountRequest
struct UnlinkFacebookAccountRequest_t3175165483;
// PlayFab.ClientModels.UnlinkFacebookAccountResult
struct UnlinkFacebookAccountResult_t830309153;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkFaceb3175165483.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkFacebo830309153.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkFacebookAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkFacebookAccountResponseCallback__ctor_m3426435369 (UnlinkFacebookAccountResponseCallback_t2127320474 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkFacebookAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkFacebookAccountRequest,PlayFab.ClientModels.UnlinkFacebookAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkFacebookAccountResponseCallback_Invoke_m3211191612 (UnlinkFacebookAccountResponseCallback_t2127320474 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkFacebookAccountRequest_t3175165483 * ___request2, UnlinkFacebookAccountResult_t830309153 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkFacebookAccountResponseCallback_t2127320474(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkFacebookAccountRequest_t3175165483 * ___request2, UnlinkFacebookAccountResult_t830309153 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkFacebookAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkFacebookAccountRequest,PlayFab.ClientModels.UnlinkFacebookAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkFacebookAccountResponseCallback_BeginInvoke_m1484001253 (UnlinkFacebookAccountResponseCallback_t2127320474 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkFacebookAccountRequest_t3175165483 * ___request2, UnlinkFacebookAccountResult_t830309153 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkFacebookAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkFacebookAccountResponseCallback_EndInvoke_m430419897 (UnlinkFacebookAccountResponseCallback_t2127320474 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
