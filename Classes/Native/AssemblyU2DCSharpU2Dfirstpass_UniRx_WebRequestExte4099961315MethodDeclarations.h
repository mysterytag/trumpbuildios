﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1<System.Object>
struct U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1<System.Object>::.ctor()
extern "C"  void U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1__ctor_m3003253013_gshared (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315 * __this, const MethodInfo* method);
#define U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1__ctor_m3003253013(__this, method) ((  void (*) (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315 *, const MethodInfo*))U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1__ctor_m3003253013_gshared)(__this, method)
// System.IDisposable UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1<System.Object>::<>m__36(UniRx.IObserver`1<TResult>)
extern "C"  Il2CppObject * U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_U3CU3Em__36_m3504808277_gshared (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_U3CU3Em__36_m3504808277(__this, ___observer0, method) ((  Il2CppObject * (*) (U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315 *, Il2CppObject*, const MethodInfo*))U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_U3CU3Em__36_m3504808277_gshared)(__this, ___observer0, method)
