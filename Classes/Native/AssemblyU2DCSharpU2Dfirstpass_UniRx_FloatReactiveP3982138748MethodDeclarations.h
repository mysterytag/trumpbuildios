﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.FloatReactiveProperty
struct FloatReactiveProperty_t3982138748;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.FloatReactiveProperty::.ctor()
extern "C"  void FloatReactiveProperty__ctor_m4216839909 (FloatReactiveProperty_t3982138748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.FloatReactiveProperty::.ctor(System.Single)
extern "C"  void FloatReactiveProperty__ctor_m2376679142 (FloatReactiveProperty_t3982138748 * __this, float ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
