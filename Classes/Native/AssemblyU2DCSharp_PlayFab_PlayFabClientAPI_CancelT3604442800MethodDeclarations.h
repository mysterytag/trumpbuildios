﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/CancelTradeResponseCallback
struct CancelTradeResponseCallback_t3604442800;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.CancelTradeRequest
struct CancelTradeRequest_t685728853;
// PlayFab.ClientModels.CancelTradeResponse
struct CancelTradeResponse_t3042931035;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CancelTradeR685728853.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CancelTrade3042931035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/CancelTradeResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CancelTradeResponseCallback__ctor_m2577484607 (CancelTradeResponseCallback_t3604442800 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/CancelTradeResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.CancelTradeRequest,PlayFab.ClientModels.CancelTradeResponse,PlayFab.PlayFabError,System.Object)
extern "C"  void CancelTradeResponseCallback_Invoke_m388040674 (CancelTradeResponseCallback_t3604442800 * __this, String_t* ___urlPath0, int32_t ___callId1, CancelTradeRequest_t685728853 * ___request2, CancelTradeResponse_t3042931035 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CancelTradeResponseCallback_t3604442800(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, CancelTradeRequest_t685728853 * ___request2, CancelTradeResponse_t3042931035 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/CancelTradeResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.CancelTradeRequest,PlayFab.ClientModels.CancelTradeResponse,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CancelTradeResponseCallback_BeginInvoke_m2966007339 (CancelTradeResponseCallback_t3604442800 * __this, String_t* ___urlPath0, int32_t ___callId1, CancelTradeRequest_t685728853 * ___request2, CancelTradeResponse_t3042931035 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/CancelTradeResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CancelTradeResponseCallback_EndInvoke_m2694984911 (CancelTradeResponseCallback_t3604442800 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
