﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<UniRx.Unit,UniRx.Unit>
struct SelectManyObserverWithIndex_t795413344;
// UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>
struct SelectManyObservable_2_t3540602214;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyObserverWithIndex__ctor_m2643077502_gshared (SelectManyObserverWithIndex_t795413344 * __this, SelectManyObservable_2_t3540602214 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyObserverWithIndex__ctor_m2643077502(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyObserverWithIndex_t795413344 *, SelectManyObservable_2_t3540602214 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex__ctor_m2643077502_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<UniRx.Unit,UniRx.Unit>::Run()
extern "C"  Il2CppObject * SelectManyObserverWithIndex_Run_m1441505024_gshared (SelectManyObserverWithIndex_t795413344 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_Run_m1441505024(__this, method) ((  Il2CppObject * (*) (SelectManyObserverWithIndex_t795413344 *, const MethodInfo*))SelectManyObserverWithIndex_Run_m1441505024_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<UniRx.Unit,UniRx.Unit>::OnNext(TSource)
extern "C"  void SelectManyObserverWithIndex_OnNext_m743568169_gshared (SelectManyObserverWithIndex_t795413344 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnNext_m743568169(__this, ___value0, method) ((  void (*) (SelectManyObserverWithIndex_t795413344 *, Unit_t2558286038 , const MethodInfo*))SelectManyObserverWithIndex_OnNext_m743568169_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<UniRx.Unit,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectManyObserverWithIndex_OnError_m3323433811_gshared (SelectManyObserverWithIndex_t795413344 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnError_m3323433811(__this, ___error0, method) ((  void (*) (SelectManyObserverWithIndex_t795413344 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserverWithIndex_OnError_m3323433811_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<UniRx.Unit,UniRx.Unit>::OnCompleted()
extern "C"  void SelectManyObserverWithIndex_OnCompleted_m1363307302_gshared (SelectManyObserverWithIndex_t795413344 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnCompleted_m1363307302(__this, method) ((  void (*) (SelectManyObserverWithIndex_t795413344 *, const MethodInfo*))SelectManyObserverWithIndex_OnCompleted_m1363307302_gshared)(__this, method)
