﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RemoveFriendRequest
struct RemoveFriendRequest_t4041798621;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.RemoveFriendRequest::.ctor()
extern "C"  void RemoveFriendRequest__ctor_m1481789804 (RemoveFriendRequest_t4041798621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RemoveFriendRequest::get_FriendPlayFabId()
extern "C"  String_t* RemoveFriendRequest_get_FriendPlayFabId_m497708622 (RemoveFriendRequest_t4041798621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RemoveFriendRequest::set_FriendPlayFabId(System.String)
extern "C"  void RemoveFriendRequest_set_FriendPlayFabId_m2721090373 (RemoveFriendRequest_t4041798621 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
