﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithCustomIDRequestCallback
struct LoginWithCustomIDRequestCallback_t1450663257;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithCustomIDRequest
struct LoginWithCustomIDRequest_t3362330884;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithCu3362330884.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithCustomIDRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithCustomIDRequestCallback__ctor_m4221494728 (LoginWithCustomIDRequestCallback_t1450663257 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithCustomIDRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithCustomIDRequest,System.Object)
extern "C"  void LoginWithCustomIDRequestCallback_Invoke_m2009436351 (LoginWithCustomIDRequestCallback_t1450663257 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithCustomIDRequest_t3362330884 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithCustomIDRequestCallback_t1450663257(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithCustomIDRequest_t3362330884 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithCustomIDRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithCustomIDRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithCustomIDRequestCallback_BeginInvoke_m4237357656 (LoginWithCustomIDRequestCallback_t1450663257 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithCustomIDRequest_t3362330884 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithCustomIDRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithCustomIDRequestCallback_EndInvoke_m2795273176 (LoginWithCustomIDRequestCallback_t1450663257 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
