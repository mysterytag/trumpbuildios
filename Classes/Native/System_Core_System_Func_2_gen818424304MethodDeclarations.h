﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<UniRx.Unit,UniRx.Unit>
struct Func_2_t818424304;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`2<UniRx.Unit,UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3136843952_gshared (Func_2_t818424304 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m3136843952(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t818424304 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3136843952_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UniRx.Unit,UniRx.Unit>::Invoke(T)
extern "C"  Unit_t2558286038  Func_2_Invoke_m1982162866_gshared (Func_2_t818424304 * __this, Unit_t2558286038  ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m1982162866(__this, ___arg10, method) ((  Unit_t2558286038  (*) (Func_2_t818424304 *, Unit_t2558286038 , const MethodInfo*))Func_2_Invoke_m1982162866_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UniRx.Unit,UniRx.Unit>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2437874785_gshared (Func_2_t818424304 * __this, Unit_t2558286038  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m2437874785(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t818424304 *, Unit_t2558286038 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2437874785_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UniRx.Unit,UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  Unit_t2558286038  Func_2_EndInvoke_m3638624098_gshared (Func_2_t818424304 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m3638624098(__this, ___result0, method) ((  Unit_t2558286038  (*) (Func_2_t818424304 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3638624098_gshared)(__this, ___result0, method)
