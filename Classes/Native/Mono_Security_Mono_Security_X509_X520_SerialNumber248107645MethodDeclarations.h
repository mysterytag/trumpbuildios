﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/SerialNumber
struct SerialNumber_t248107645;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/SerialNumber::.ctor()
extern "C"  void SerialNumber__ctor_m2532758252 (SerialNumber_t248107645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
