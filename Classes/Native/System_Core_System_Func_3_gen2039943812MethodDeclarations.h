﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen698853017MethodDeclarations.h"

// System.Void System.Func`3<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.ProviderBase,System.Type>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m2816279978(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t2039943812 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m3649639326_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.ProviderBase,System.Type>::Invoke(T1,T2)
#define Func_3_Invoke_m3984054351(__this, ___arg10, ___arg21, method) ((  Type_t * (*) (Func_3_t2039943812 *, KeyValuePair_2_t2091358871 , ProviderBase_t1627494391 *, const MethodInfo*))Func_3_Invoke_m1601313647_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.ProviderBase,System.Type>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m3923813200(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t2039943812 *, KeyValuePair_2_t2091358871 , ProviderBase_t1627494391 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3113524596_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.ProviderBase,System.Type>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m3352659564(__this, ___result0, method) ((  Type_t * (*) (Func_3_t2039943812 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m159770444_gshared)(__this, ___result0, method)
