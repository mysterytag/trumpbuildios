﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>
struct ToYieldInstruction_t3682831524;
// UniRx.ObservableYieldInstruction`1<System.Int32>
struct ObservableYieldInstruction_1_t674360375;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>::.ctor(UniRx.ObservableYieldInstruction`1<T>)
extern "C"  void ToYieldInstruction__ctor_m482937683_gshared (ToYieldInstruction_t3682831524 * __this, ObservableYieldInstruction_1_t674360375 * ___parent0, const MethodInfo* method);
#define ToYieldInstruction__ctor_m482937683(__this, ___parent0, method) ((  void (*) (ToYieldInstruction_t3682831524 *, ObservableYieldInstruction_1_t674360375 *, const MethodInfo*))ToYieldInstruction__ctor_m482937683_gshared)(__this, ___parent0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>::OnNext(T)
extern "C"  void ToYieldInstruction_OnNext_m4105776046_gshared (ToYieldInstruction_t3682831524 * __this, int32_t ___value0, const MethodInfo* method);
#define ToYieldInstruction_OnNext_m4105776046(__this, ___value0, method) ((  void (*) (ToYieldInstruction_t3682831524 *, int32_t, const MethodInfo*))ToYieldInstruction_OnNext_m4105776046_gshared)(__this, ___value0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>::OnError(System.Exception)
extern "C"  void ToYieldInstruction_OnError_m2847681213_gshared (ToYieldInstruction_t3682831524 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ToYieldInstruction_OnError_m2847681213(__this, ___error0, method) ((  void (*) (ToYieldInstruction_t3682831524 *, Exception_t1967233988 *, const MethodInfo*))ToYieldInstruction_OnError_m2847681213_gshared)(__this, ___error0, method)
// System.Void UniRx.ObservableYieldInstruction`1/ToYieldInstruction<System.Int32>::OnCompleted()
extern "C"  void ToYieldInstruction_OnCompleted_m2899267984_gshared (ToYieldInstruction_t3682831524 * __this, const MethodInfo* method);
#define ToYieldInstruction_OnCompleted_m2899267984(__this, method) ((  void (*) (ToYieldInstruction_t3682831524 *, const MethodInfo*))ToYieldInstruction_OnCompleted_m2899267984_gshared)(__this, method)
