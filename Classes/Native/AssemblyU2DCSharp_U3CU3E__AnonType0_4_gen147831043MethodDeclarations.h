﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>
struct U3CU3E__AnonType0_4_t147831043;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::.ctor(<show>__T,<tutor>__T,<bottom>__T,<hidden>__T)
extern "C"  void U3CU3E__AnonType0_4__ctor_m3696565507_gshared (U3CU3E__AnonType0_4_t147831043 * __this, Il2CppObject * ___show0, Il2CppObject * ___tutor1, Il2CppObject * ___bottom2, Il2CppObject * ___hidden3, const MethodInfo* method);
#define U3CU3E__AnonType0_4__ctor_m3696565507(__this, ___show0, ___tutor1, ___bottom2, ___hidden3, method) ((  void (*) (U3CU3E__AnonType0_4_t147831043 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_4__ctor_m3696565507_gshared)(__this, ___show0, ___tutor1, ___bottom2, ___hidden3, method)
// <show>__T <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::get_show()
extern "C"  Il2CppObject * U3CU3E__AnonType0_4_get_show_m884195869_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_get_show_m884195869(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_4_t147831043 *, const MethodInfo*))U3CU3E__AnonType0_4_get_show_m884195869_gshared)(__this, method)
// <tutor>__T <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::get_tutor()
extern "C"  Il2CppObject * U3CU3E__AnonType0_4_get_tutor_m2532450895_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_get_tutor_m2532450895(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_4_t147831043 *, const MethodInfo*))U3CU3E__AnonType0_4_get_tutor_m2532450895_gshared)(__this, method)
// <bottom>__T <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::get_bottom()
extern "C"  Il2CppObject * U3CU3E__AnonType0_4_get_bottom_m2040523357_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_get_bottom_m2040523357(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_4_t147831043 *, const MethodInfo*))U3CU3E__AnonType0_4_get_bottom_m2040523357_gshared)(__this, method)
// <hidden>__T <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::get_hidden()
extern "C"  Il2CppObject * U3CU3E__AnonType0_4_get_hidden_m4036773693_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_get_hidden_m4036773693(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType0_4_t147831043 *, const MethodInfo*))U3CU3E__AnonType0_4_get_hidden_m4036773693_gshared)(__this, method)
// System.Boolean <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_4_Equals_m4034822574_gshared (U3CU3E__AnonType0_4_t147831043 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType0_4_Equals_m4034822574(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType0_4_t147831043 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_4_Equals_m4034822574_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_4_GetHashCode_m2266149074_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_GetHashCode_m2266149074(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_4_t147831043 *, const MethodInfo*))U3CU3E__AnonType0_4_GetHashCode_m2266149074_gshared)(__this, method)
// System.String <>__AnonType0`4<System.Object,System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType0_4_ToString_m133639554_gshared (U3CU3E__AnonType0_4_t147831043 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_ToString_m133639554(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_4_t147831043 *, const MethodInfo*))U3CU3E__AnonType0_4_ToString_m133639554_gshared)(__this, method)
