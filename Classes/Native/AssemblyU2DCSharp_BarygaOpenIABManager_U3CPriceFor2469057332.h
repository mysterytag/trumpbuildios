﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaOpenIABManager/<PriceFormatted>c__AnonStoreyEA
struct  U3CPriceFormattedU3Ec__AnonStoreyEA_t2469057332  : public Il2CppObject
{
public:
	// System.Boolean BarygaOpenIABManager/<PriceFormatted>c__AnonStoreyEA::withCurrency
	bool ___withCurrency_0;

public:
	inline static int32_t get_offset_of_withCurrency_0() { return static_cast<int32_t>(offsetof(U3CPriceFormattedU3Ec__AnonStoreyEA_t2469057332, ___withCurrency_0)); }
	inline bool get_withCurrency_0() const { return ___withCurrency_0; }
	inline bool* get_address_of_withCurrency_0() { return &___withCurrency_0; }
	inline void set_withCurrency_0(bool value)
	{
		___withCurrency_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
