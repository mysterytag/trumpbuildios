﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,UniRx.Unit>
struct U3CSelectManyU3Ec__AnonStorey49_2_t4141348958;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey49_2__ctor_m2308581076_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t4141348958 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey49_2__ctor_m2308581076(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey49_2_t4141348958 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey49_2__ctor_m2308581076_gshared)(__this, method)
// UniRx.IObservable`1<TR> UniRx.Observable/<SelectMany>c__AnonStorey49`2<System.Object,UniRx.Unit>::<>m__47(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m3377694824_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t4141348958 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m3377694824(__this, ____0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey49_2_t4141348958 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m3377694824_gshared)(__this, ____0, method)
