﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct VectorOptions_t3308462279;
struct VectorOptions_t3308462279_marshaled_pinvoke;

extern "C" void VectorOptions_t3308462279_marshal_pinvoke(const VectorOptions_t3308462279& unmarshaled, VectorOptions_t3308462279_marshaled_pinvoke& marshaled);
extern "C" void VectorOptions_t3308462279_marshal_pinvoke_back(const VectorOptions_t3308462279_marshaled_pinvoke& marshaled, VectorOptions_t3308462279& unmarshaled);
extern "C" void VectorOptions_t3308462279_marshal_pinvoke_cleanup(VectorOptions_t3308462279_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VectorOptions_t3308462279;
struct VectorOptions_t3308462279_marshaled_com;

extern "C" void VectorOptions_t3308462279_marshal_com(const VectorOptions_t3308462279& unmarshaled, VectorOptions_t3308462279_marshaled_com& marshaled);
extern "C" void VectorOptions_t3308462279_marshal_com_back(const VectorOptions_t3308462279_marshaled_com& marshaled, VectorOptions_t3308462279& unmarshaled);
extern "C" void VectorOptions_t3308462279_marshal_com_cleanup(VectorOptions_t3308462279_marshaled_com& marshaled);
