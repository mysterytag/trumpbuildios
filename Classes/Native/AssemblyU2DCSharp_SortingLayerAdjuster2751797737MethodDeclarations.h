﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SortingLayerAdjuster
struct SortingLayerAdjuster_t2751797737;
// UnityEngine.Transform
struct Transform_t284553113;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"

// System.Void SortingLayerAdjuster::.ctor()
extern "C"  void SortingLayerAdjuster__ctor_m2879870674 (SortingLayerAdjuster_t2751797737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SortingLayerAdjuster::Awake()
extern "C"  void SortingLayerAdjuster_Awake_m3117475893 (SortingLayerAdjuster_t2751797737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SortingLayerAdjuster::SetLayer()
extern "C"  void SortingLayerAdjuster_SetLayer_m2846654977 (SortingLayerAdjuster_t2751797737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SortingLayerAdjuster::SetLayerWithChildren(UnityEngine.Transform)
extern "C"  void SortingLayerAdjuster_SetLayerWithChildren_m2689552695 (SortingLayerAdjuster_t2751797737 * __this, Transform_t284553113 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
