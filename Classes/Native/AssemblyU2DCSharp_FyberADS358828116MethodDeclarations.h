﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FyberADS
struct FyberADS_t358828116;
// System.String
struct String_t;
// SponsorPay.SuccessfulCurrencyResponse
struct SuccessfulCurrencyResponse_t823827974;
// SponsorPay.RequestError
struct RequestError_t1597064339;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_SponsorPay_SuccessfulCurrencyResp823827974.h"
#include "AssemblyU2DCSharp_SponsorPay_RequestError1597064339.h"

// System.Void FyberADS::.ctor()
extern "C"  void FyberADS__ctor_m577213639 (FyberADS_t358828116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FyberADS::Start()
extern "C"  void FyberADS_Start_m3819318727 (FyberADS_t358828116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FyberADS::OnOFWResultReceived(System.String)
extern "C"  void FyberADS_OnOFWResultReceived_m2680153022 (FyberADS_t358828116 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FyberADS::OnSuccessfulCurrencyRequestReceived(SponsorPay.SuccessfulCurrencyResponse)
extern "C"  void FyberADS_OnSuccessfulCurrencyRequestReceived_m3385218459 (FyberADS_t358828116 * __this, SuccessfulCurrencyResponse_t823827974 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FyberADS::OnSPDeltaOfCoinsRequestFailed(SponsorPay.RequestError)
extern "C"  void FyberADS_OnSPDeltaOfCoinsRequestFailed_m770926967 (FyberADS_t358828116 * __this, RequestError_t1597064339 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FyberADS::OnNativeExceptionReceivedFromSDK(System.String)
extern "C"  void FyberADS_OnNativeExceptionReceivedFromSDK_m3561982479 (FyberADS_t358828116 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FyberADS::Update()
extern "C"  void FyberADS_Update_m2440615718 (FyberADS_t358828116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FyberADS::StartADS()
extern "C"  void FyberADS_StartADS_m3369531147 (FyberADS_t358828116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
