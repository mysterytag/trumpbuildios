﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<CancelTrade>c__AnonStoreyD2
struct U3CCancelTradeU3Ec__AnonStoreyD2_t3970316645;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<CancelTrade>c__AnonStoreyD2::.ctor()
extern "C"  void U3CCancelTradeU3Ec__AnonStoreyD2__ctor_m2569080718 (U3CCancelTradeU3Ec__AnonStoreyD2_t3970316645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<CancelTrade>c__AnonStoreyD2::<>m__BF(PlayFab.CallRequestContainer)
extern "C"  void U3CCancelTradeU3Ec__AnonStoreyD2_U3CU3Em__BF_m3482200880 (U3CCancelTradeU3Ec__AnonStoreyD2_t3970316645 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
