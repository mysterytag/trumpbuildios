﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2C
struct  U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048  : public Il2CppObject
{
public:
	// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2C::bannerPositon
	String_t* ___bannerPositon_0;

public:
	inline static int32_t get_offset_of_bannerPositon_0() { return static_cast<int32_t>(offsetof(U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048, ___bannerPositon_0)); }
	inline String_t* get_bannerPositon_0() const { return ___bannerPositon_0; }
	inline String_t** get_address_of_bannerPositon_0() { return &___bannerPositon_0; }
	inline void set_bannerPositon_0(String_t* value)
	{
		___bannerPositon_0 = value;
		Il2CppCodeGenWriteBarrier(&___bannerPositon_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
