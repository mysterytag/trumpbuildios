﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FinallyObservable`1<System.Object>
struct FinallyObservable_1_t2652127741;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action
struct Action_t437523947;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.FinallyObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void FinallyObservable_1__ctor_m2944988073_gshared (FinallyObservable_1_t2652127741 * __this, Il2CppObject* ___source0, Action_t437523947 * ___finallyAction1, const MethodInfo* method);
#define FinallyObservable_1__ctor_m2944988073(__this, ___source0, ___finallyAction1, method) ((  void (*) (FinallyObservable_1_t2652127741 *, Il2CppObject*, Action_t437523947 *, const MethodInfo*))FinallyObservable_1__ctor_m2944988073_gshared)(__this, ___source0, ___finallyAction1, method)
// System.IDisposable UniRx.Operators.FinallyObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FinallyObservable_1_SubscribeCore_m2576989483_gshared (FinallyObservable_1_t2652127741 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FinallyObservable_1_SubscribeCore_m2576989483(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FinallyObservable_1_t2652127741 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FinallyObservable_1_SubscribeCore_m2576989483_gshared)(__this, ___observer0, ___cancel1, method)
