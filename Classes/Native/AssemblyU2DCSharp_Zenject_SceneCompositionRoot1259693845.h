﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<Zenject.DiContainer>
struct Action_1_t2531567154;
// Zenject.MonoInstaller[]
struct MonoInstallerU5BU5D_t2181499551;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.IDependencyRoot
struct IDependencyRoot_t2368104171;
// System.Collections.Generic.List`1<Zenject.IInstaller>
struct List_1_t3252182445;
// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject>
struct Func_2_t391253545;
// System.Func`2<Zenject.IInstaller,System.Boolean>
struct Func_2_t126643105;

#include "AssemblyU2DCSharp_Zenject_CompositionRoot1939928321.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneCompositionRoot
struct  SceneCompositionRoot_t1259693845  : public CompositionRoot_t1939928321
{
public:
	// System.Boolean Zenject.SceneCompositionRoot::OnlyInjectWhenActive
	bool ___OnlyInjectWhenActive_4;
	// System.Boolean Zenject.SceneCompositionRoot::InjectFullScene
	bool ___InjectFullScene_5;
	// Zenject.MonoInstaller[] Zenject.SceneCompositionRoot::Installers
	MonoInstallerU5BU5D_t2181499551* ___Installers_6;
	// Zenject.DiContainer Zenject.SceneCompositionRoot::_container
	DiContainer_t2383114449 * ____container_7;
	// Zenject.IDependencyRoot Zenject.SceneCompositionRoot::_dependencyRoot
	Il2CppObject * ____dependencyRoot_8;

public:
	inline static int32_t get_offset_of_OnlyInjectWhenActive_4() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845, ___OnlyInjectWhenActive_4)); }
	inline bool get_OnlyInjectWhenActive_4() const { return ___OnlyInjectWhenActive_4; }
	inline bool* get_address_of_OnlyInjectWhenActive_4() { return &___OnlyInjectWhenActive_4; }
	inline void set_OnlyInjectWhenActive_4(bool value)
	{
		___OnlyInjectWhenActive_4 = value;
	}

	inline static int32_t get_offset_of_InjectFullScene_5() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845, ___InjectFullScene_5)); }
	inline bool get_InjectFullScene_5() const { return ___InjectFullScene_5; }
	inline bool* get_address_of_InjectFullScene_5() { return &___InjectFullScene_5; }
	inline void set_InjectFullScene_5(bool value)
	{
		___InjectFullScene_5 = value;
	}

	inline static int32_t get_offset_of_Installers_6() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845, ___Installers_6)); }
	inline MonoInstallerU5BU5D_t2181499551* get_Installers_6() const { return ___Installers_6; }
	inline MonoInstallerU5BU5D_t2181499551** get_address_of_Installers_6() { return &___Installers_6; }
	inline void set_Installers_6(MonoInstallerU5BU5D_t2181499551* value)
	{
		___Installers_6 = value;
		Il2CppCodeGenWriteBarrier(&___Installers_6, value);
	}

	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845, ____container_7)); }
	inline DiContainer_t2383114449 * get__container_7() const { return ____container_7; }
	inline DiContainer_t2383114449 ** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(DiContainer_t2383114449 * value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier(&____container_7, value);
	}

	inline static int32_t get_offset_of__dependencyRoot_8() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845, ____dependencyRoot_8)); }
	inline Il2CppObject * get__dependencyRoot_8() const { return ____dependencyRoot_8; }
	inline Il2CppObject ** get_address_of__dependencyRoot_8() { return &____dependencyRoot_8; }
	inline void set__dependencyRoot_8(Il2CppObject * value)
	{
		____dependencyRoot_8 = value;
		Il2CppCodeGenWriteBarrier(&____dependencyRoot_8, value);
	}
};

struct SceneCompositionRoot_t1259693845_StaticFields
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.SceneCompositionRoot::BeforeInstallHooks
	Action_1_t2531567154 * ___BeforeInstallHooks_2;
	// System.Action`1<Zenject.DiContainer> Zenject.SceneCompositionRoot::AfterInstallHooks
	Action_1_t2531567154 * ___AfterInstallHooks_3;
	// System.Collections.Generic.List`1<Zenject.IInstaller> Zenject.SceneCompositionRoot::_staticInstallers
	List_1_t3252182445 * ____staticInstallers_9;
	// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject> Zenject.SceneCompositionRoot::<>f__am$cache8
	Func_2_t391253545 * ___U3CU3Ef__amU24cache8_10;
	// System.Func`2<Zenject.IInstaller,System.Boolean> Zenject.SceneCompositionRoot::<>f__am$cache9
	Func_2_t126643105 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_BeforeInstallHooks_2() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845_StaticFields, ___BeforeInstallHooks_2)); }
	inline Action_1_t2531567154 * get_BeforeInstallHooks_2() const { return ___BeforeInstallHooks_2; }
	inline Action_1_t2531567154 ** get_address_of_BeforeInstallHooks_2() { return &___BeforeInstallHooks_2; }
	inline void set_BeforeInstallHooks_2(Action_1_t2531567154 * value)
	{
		___BeforeInstallHooks_2 = value;
		Il2CppCodeGenWriteBarrier(&___BeforeInstallHooks_2, value);
	}

	inline static int32_t get_offset_of_AfterInstallHooks_3() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845_StaticFields, ___AfterInstallHooks_3)); }
	inline Action_1_t2531567154 * get_AfterInstallHooks_3() const { return ___AfterInstallHooks_3; }
	inline Action_1_t2531567154 ** get_address_of_AfterInstallHooks_3() { return &___AfterInstallHooks_3; }
	inline void set_AfterInstallHooks_3(Action_1_t2531567154 * value)
	{
		___AfterInstallHooks_3 = value;
		Il2CppCodeGenWriteBarrier(&___AfterInstallHooks_3, value);
	}

	inline static int32_t get_offset_of__staticInstallers_9() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845_StaticFields, ____staticInstallers_9)); }
	inline List_1_t3252182445 * get__staticInstallers_9() const { return ____staticInstallers_9; }
	inline List_1_t3252182445 ** get_address_of__staticInstallers_9() { return &____staticInstallers_9; }
	inline void set__staticInstallers_9(List_1_t3252182445 * value)
	{
		____staticInstallers_9 = value;
		Il2CppCodeGenWriteBarrier(&____staticInstallers_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Func_2_t391253545 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Func_2_t391253545 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Func_2_t391253545 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(SceneCompositionRoot_t1259693845_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Func_2_t126643105 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Func_2_t126643105 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Func_2_t126643105 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
