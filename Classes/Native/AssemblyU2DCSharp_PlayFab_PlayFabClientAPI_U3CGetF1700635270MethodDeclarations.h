﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundCurrentUser>c__AnonStorey8C
struct U3CGetFriendLeaderboardAroundCurrentUserU3Ec__AnonStorey8C_t1700635270;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundCurrentUser>c__AnonStorey8C::.ctor()
extern "C"  void U3CGetFriendLeaderboardAroundCurrentUserU3Ec__AnonStorey8C__ctor_m2435020685 (U3CGetFriendLeaderboardAroundCurrentUserU3Ec__AnonStorey8C_t1700635270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundCurrentUser>c__AnonStorey8C::<>m__79(PlayFab.CallRequestContainer)
extern "C"  void U3CGetFriendLeaderboardAroundCurrentUserU3Ec__AnonStorey8C_U3CU3Em__79_m3936149773 (U3CGetFriendLeaderboardAroundCurrentUserU3Ec__AnonStorey8C_t1700635270 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
