﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics/LocalyticsSessionWillClose
struct LocalyticsSessionWillClose_t2690468515;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillClose::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsSessionWillClose__ctor_m1198798954 (LocalyticsSessionWillClose_t2690468515 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillClose::Invoke()
extern "C"  void LocalyticsSessionWillClose_Invoke_m2626000196 (LocalyticsSessionWillClose_t2690468515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionWillClose_t2690468515(Il2CppObject* delegate);
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsSessionWillClose::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsSessionWillClose_BeginInvoke_m776122055 (LocalyticsSessionWillClose_t2690468515 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillClose::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsSessionWillClose_EndInvoke_m246968186 (LocalyticsSessionWillClose_t2690468515 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
