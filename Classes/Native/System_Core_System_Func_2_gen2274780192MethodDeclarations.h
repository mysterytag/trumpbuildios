﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2470508636MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m127044965(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2274780192 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1887411215_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase>>::Invoke(T)
#define Func_2_Invoke_m329135565(__this, ___arg10, method) ((  Il2CppObject* (*) (Func_2_t2274780192 *, KeyValuePair_2_t2091358871 , const MethodInfo*))Func_2_Invoke_m3228321655_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m240771836(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2274780192 *, KeyValuePair_2_t2091358871 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2365319082_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2729418791(__this, ___result0, method) ((  Il2CppObject* (*) (Func_2_t2274780192 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3637779645_gshared)(__this, ___result0, method)
