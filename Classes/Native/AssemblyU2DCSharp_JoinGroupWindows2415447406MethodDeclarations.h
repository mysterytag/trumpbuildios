﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JoinGroupWindows
struct JoinGroupWindows_t2415447406;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void JoinGroupWindows::.ctor()
extern "C"  void JoinGroupWindows__ctor_m2525074669 (JoinGroupWindows_t2415447406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JoinGroupWindows::PostInject()
extern "C"  void JoinGroupWindows_PostInject_m2914094152 (JoinGroupWindows_t2415447406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JoinGroupWindows::AraJoinGroup()
extern "C"  void JoinGroupWindows_AraJoinGroup_m2190972540 (JoinGroupWindows_t2415447406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JoinGroupWindows::Closes()
extern "C"  void JoinGroupWindows_Closes_m2465013746 (JoinGroupWindows_t2415447406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JoinGroupWindows::<PostInject>m__3B(UniRx.Unit)
extern "C"  void JoinGroupWindows_U3CPostInjectU3Em__3B_m995080954 (JoinGroupWindows_t2415447406 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JoinGroupWindows::<PostInject>m__3C(UniRx.Unit)
extern "C"  void JoinGroupWindows_U3CPostInjectU3Em__3C_m701677947 (JoinGroupWindows_t2415447406 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
