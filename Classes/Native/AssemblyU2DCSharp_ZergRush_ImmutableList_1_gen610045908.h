﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Single>[]
struct Action_1U5BU5D_t184620907;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ImmutableList`1<System.Action`1<System.Single>>
struct  ImmutableList_1_t610045908  : public Il2CppObject
{
public:
	// T[] ZergRush.ImmutableList`1::data
	Action_1U5BU5D_t184620907* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ImmutableList_1_t610045908, ___data_0)); }
	inline Action_1U5BU5D_t184620907* get_data_0() const { return ___data_0; }
	inline Action_1U5BU5D_t184620907** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(Action_1U5BU5D_t184620907* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
