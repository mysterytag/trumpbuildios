﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.Quaternion>
struct Subscription_t3965929797;
// UniRx.Subject`1<UnityEngine.Quaternion>
struct Subject_1_t3829750599;
// UniRx.IObserver`1<UnityEngine.Quaternion>
struct IObserver_1_t4103714882;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.Quaternion>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1897287774_gshared (Subscription_t3965929797 * __this, Subject_1_t3829750599 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1897287774(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t3965929797 *, Subject_1_t3829750599 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1897287774_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Quaternion>::Dispose()
extern "C"  void Subscription_Dispose_m2945587928_gshared (Subscription_t3965929797 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2945587928(__this, method) ((  void (*) (Subscription_t3965929797 *, const MethodInfo*))Subscription_Dispose_m2945587928_gshared)(__this, method)
