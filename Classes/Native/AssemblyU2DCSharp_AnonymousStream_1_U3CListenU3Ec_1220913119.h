﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnonymousStream`1/<Listen>c__AnonStorey12C<System.Object>
struct  U3CListenU3Ec__AnonStorey12C_t1220913119  : public Il2CppObject
{
public:
	// System.Action AnonymousStream`1/<Listen>c__AnonStorey12C::observer
	Action_t437523947 * ___observer_0;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CListenU3Ec__AnonStorey12C_t1220913119, ___observer_0)); }
	inline Action_t437523947 * get_observer_0() const { return ___observer_0; }
	inline Action_t437523947 ** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Action_t437523947 * value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
