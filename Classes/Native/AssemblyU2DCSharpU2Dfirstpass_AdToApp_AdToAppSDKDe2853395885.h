﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.String,System.String>
struct Action_2_t2887221574;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t2150340505;
// System.Action
struct Action_t437523947;
// System.Action`1<System.String>
struct Action_1_t1116941607;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdToApp.AdToAppSDKDelegate
struct  AdToAppSDKDelegate_t2853395885  : public MonoBehaviour_t3012272455
{
public:
	// System.Action`2<System.String,System.String> AdToApp.AdToAppSDKDelegate::OnInterstitialStarted
	Action_2_t2887221574 * ___OnInterstitialStarted_2;
	// System.Action`2<System.String,System.String> AdToApp.AdToAppSDKDelegate::OnInterstitialClosed
	Action_2_t2887221574 * ___OnInterstitialClosed_3;
	// System.Action`2<System.String,System.String> AdToApp.AdToAppSDKDelegate::OnInterstitialFailedToAppear
	Action_2_t2887221574 * ___OnInterstitialFailedToAppear_4;
	// System.Action`2<System.String,System.String> AdToApp.AdToAppSDKDelegate::OnInterstitialClicked
	Action_2_t2887221574 * ___OnInterstitialClicked_5;
	// System.Action`2<System.String,System.String> AdToApp.AdToAppSDKDelegate::OnFirstInterstitialLoad
	Action_2_t2887221574 * ___OnFirstInterstitialLoad_6;
	// System.Action`3<System.String,System.String,System.String> AdToApp.AdToAppSDKDelegate::OnRewardedCompleted
	Action_3_t2150340505 * ___OnRewardedCompleted_7;
	// System.Action AdToApp.AdToAppSDKDelegate::OnBannerLoad
	Action_t437523947 * ___OnBannerLoad_8;
	// System.Action`1<System.String> AdToApp.AdToAppSDKDelegate::OnBannerFailedToLoad
	Action_1_t1116941607 * ___OnBannerFailedToLoad_9;
	// System.Action AdToApp.AdToAppSDKDelegate::OnBannerClicked
	Action_t437523947 * ___OnBannerClicked_10;

public:
	inline static int32_t get_offset_of_OnInterstitialStarted_2() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnInterstitialStarted_2)); }
	inline Action_2_t2887221574 * get_OnInterstitialStarted_2() const { return ___OnInterstitialStarted_2; }
	inline Action_2_t2887221574 ** get_address_of_OnInterstitialStarted_2() { return &___OnInterstitialStarted_2; }
	inline void set_OnInterstitialStarted_2(Action_2_t2887221574 * value)
	{
		___OnInterstitialStarted_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialStarted_2, value);
	}

	inline static int32_t get_offset_of_OnInterstitialClosed_3() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnInterstitialClosed_3)); }
	inline Action_2_t2887221574 * get_OnInterstitialClosed_3() const { return ___OnInterstitialClosed_3; }
	inline Action_2_t2887221574 ** get_address_of_OnInterstitialClosed_3() { return &___OnInterstitialClosed_3; }
	inline void set_OnInterstitialClosed_3(Action_2_t2887221574 * value)
	{
		___OnInterstitialClosed_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialClosed_3, value);
	}

	inline static int32_t get_offset_of_OnInterstitialFailedToAppear_4() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnInterstitialFailedToAppear_4)); }
	inline Action_2_t2887221574 * get_OnInterstitialFailedToAppear_4() const { return ___OnInterstitialFailedToAppear_4; }
	inline Action_2_t2887221574 ** get_address_of_OnInterstitialFailedToAppear_4() { return &___OnInterstitialFailedToAppear_4; }
	inline void set_OnInterstitialFailedToAppear_4(Action_2_t2887221574 * value)
	{
		___OnInterstitialFailedToAppear_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialFailedToAppear_4, value);
	}

	inline static int32_t get_offset_of_OnInterstitialClicked_5() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnInterstitialClicked_5)); }
	inline Action_2_t2887221574 * get_OnInterstitialClicked_5() const { return ___OnInterstitialClicked_5; }
	inline Action_2_t2887221574 ** get_address_of_OnInterstitialClicked_5() { return &___OnInterstitialClicked_5; }
	inline void set_OnInterstitialClicked_5(Action_2_t2887221574 * value)
	{
		___OnInterstitialClicked_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialClicked_5, value);
	}

	inline static int32_t get_offset_of_OnFirstInterstitialLoad_6() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnFirstInterstitialLoad_6)); }
	inline Action_2_t2887221574 * get_OnFirstInterstitialLoad_6() const { return ___OnFirstInterstitialLoad_6; }
	inline Action_2_t2887221574 ** get_address_of_OnFirstInterstitialLoad_6() { return &___OnFirstInterstitialLoad_6; }
	inline void set_OnFirstInterstitialLoad_6(Action_2_t2887221574 * value)
	{
		___OnFirstInterstitialLoad_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnFirstInterstitialLoad_6, value);
	}

	inline static int32_t get_offset_of_OnRewardedCompleted_7() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnRewardedCompleted_7)); }
	inline Action_3_t2150340505 * get_OnRewardedCompleted_7() const { return ___OnRewardedCompleted_7; }
	inline Action_3_t2150340505 ** get_address_of_OnRewardedCompleted_7() { return &___OnRewardedCompleted_7; }
	inline void set_OnRewardedCompleted_7(Action_3_t2150340505 * value)
	{
		___OnRewardedCompleted_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnRewardedCompleted_7, value);
	}

	inline static int32_t get_offset_of_OnBannerLoad_8() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnBannerLoad_8)); }
	inline Action_t437523947 * get_OnBannerLoad_8() const { return ___OnBannerLoad_8; }
	inline Action_t437523947 ** get_address_of_OnBannerLoad_8() { return &___OnBannerLoad_8; }
	inline void set_OnBannerLoad_8(Action_t437523947 * value)
	{
		___OnBannerLoad_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnBannerLoad_8, value);
	}

	inline static int32_t get_offset_of_OnBannerFailedToLoad_9() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnBannerFailedToLoad_9)); }
	inline Action_1_t1116941607 * get_OnBannerFailedToLoad_9() const { return ___OnBannerFailedToLoad_9; }
	inline Action_1_t1116941607 ** get_address_of_OnBannerFailedToLoad_9() { return &___OnBannerFailedToLoad_9; }
	inline void set_OnBannerFailedToLoad_9(Action_1_t1116941607 * value)
	{
		___OnBannerFailedToLoad_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnBannerFailedToLoad_9, value);
	}

	inline static int32_t get_offset_of_OnBannerClicked_10() { return static_cast<int32_t>(offsetof(AdToAppSDKDelegate_t2853395885, ___OnBannerClicked_10)); }
	inline Action_t437523947 * get_OnBannerClicked_10() const { return ___OnBannerClicked_10; }
	inline Action_t437523947 ** get_address_of_OnBannerClicked_10() { return &___OnBannerClicked_10; }
	inline void set_OnBannerClicked_10(Action_t437523947 * value)
	{
		___OnBannerClicked_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnBannerClicked_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
