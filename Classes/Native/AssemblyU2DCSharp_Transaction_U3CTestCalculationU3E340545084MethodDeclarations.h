﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Transaction/<TestCalculation>c__AnonStorey14C
struct U3CTestCalculationU3Ec__AnonStorey14C_t340545084;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Transaction/<TestCalculation>c__AnonStorey14C::.ctor()
extern "C"  void U3CTestCalculationU3Ec__AnonStorey14C__ctor_m1730512544 (U3CTestCalculationU3Ec__AnonStorey14C_t340545084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Transaction/<TestCalculation>c__AnonStorey14C::<>m__1F7()
extern "C"  String_t* U3CTestCalculationU3Ec__AnonStorey14C_U3CU3Em__1F7_m2745441528 (U3CTestCalculationU3Ec__AnonStorey14C_t340545084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
