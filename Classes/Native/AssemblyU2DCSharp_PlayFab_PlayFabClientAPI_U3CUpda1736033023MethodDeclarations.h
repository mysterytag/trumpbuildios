﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UpdateUserPublisherData>c__AnonStorey99
struct U3CUpdateUserPublisherDataU3Ec__AnonStorey99_t1736033023;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UpdateUserPublisherData>c__AnonStorey99::.ctor()
extern "C"  void U3CUpdateUserPublisherDataU3Ec__AnonStorey99__ctor_m2370698036 (U3CUpdateUserPublisherDataU3Ec__AnonStorey99_t1736033023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UpdateUserPublisherData>c__AnonStorey99::<>m__86(PlayFab.CallRequestContainer)
extern "C"  void U3CUpdateUserPublisherDataU3Ec__AnonStorey99_U3CU3Em__86_m3440422288 (U3CUpdateUserPublisherDataU3Ec__AnonStorey99_t1736033023 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
