﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.LastObservable`1/Last<System.Object>
struct Last_t3745612479;
// UniRx.Operators.LastObservable`1<System.Object>
struct LastObservable_1_t3225858136;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.LastObservable`1/Last<System.Object>::.ctor(UniRx.Operators.LastObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Last__ctor_m3920580506_gshared (Last_t3745612479 * __this, LastObservable_1_t3225858136 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Last__ctor_m3920580506(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Last_t3745612479 *, LastObservable_1_t3225858136 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Last__ctor_m3920580506_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.LastObservable`1/Last<System.Object>::OnNext(T)
extern "C"  void Last_OnNext_m1586024607_gshared (Last_t3745612479 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Last_OnNext_m1586024607(__this, ___value0, method) ((  void (*) (Last_t3745612479 *, Il2CppObject *, const MethodInfo*))Last_OnNext_m1586024607_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.LastObservable`1/Last<System.Object>::OnError(System.Exception)
extern "C"  void Last_OnError_m1471008174_gshared (Last_t3745612479 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Last_OnError_m1471008174(__this, ___error0, method) ((  void (*) (Last_t3745612479 *, Exception_t1967233988 *, const MethodInfo*))Last_OnError_m1471008174_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.LastObservable`1/Last<System.Object>::OnCompleted()
extern "C"  void Last_OnCompleted_m2876315137_gshared (Last_t3745612479 * __this, const MethodInfo* method);
#define Last_OnCompleted_m2876315137(__this, method) ((  void (*) (Last_t3745612479 *, const MethodInfo*))Last_OnCompleted_m2876315137_gshared)(__this, method)
