﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DoOnErrorObservable`1<System.Object>
struct DoOnErrorObservable_1_t434875552;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DoOnErrorObservable`1/DoOnError<System.Object>
struct  DoOnError_t3716721415  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DoOnErrorObservable`1<T> UniRx.Operators.DoOnErrorObservable`1/DoOnError::parent
	DoOnErrorObservable_1_t434875552 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(DoOnError_t3716721415, ___parent_2)); }
	inline DoOnErrorObservable_1_t434875552 * get_parent_2() const { return ___parent_2; }
	inline DoOnErrorObservable_1_t434875552 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DoOnErrorObservable_1_t434875552 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
