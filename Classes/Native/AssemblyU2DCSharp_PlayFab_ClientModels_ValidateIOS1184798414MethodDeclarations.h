﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ValidateIOSReceiptResult
struct ValidateIOSReceiptResult_t1184798414;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.ValidateIOSReceiptResult::.ctor()
extern "C"  void ValidateIOSReceiptResult__ctor_m545707823 (ValidateIOSReceiptResult_t1184798414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
