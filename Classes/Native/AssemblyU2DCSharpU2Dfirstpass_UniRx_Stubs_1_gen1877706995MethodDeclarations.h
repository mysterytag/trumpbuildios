﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Stubs`1<System.Double>::.cctor()
extern "C"  void Stubs_1__cctor_m474768202_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Stubs_1__cctor_m474768202(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m474768202_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<System.Double>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m1774145094_gshared (Il2CppObject * __this /* static, unused */, double ___t0, const MethodInfo* method);
#define Stubs_1_U3CIgnoreU3Em__71_m1774145094(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, double, const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m1774145094_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<System.Double>::<Identity>m__72(T)
extern "C"  double Stubs_1_U3CIdentityU3Em__72_m91553976_gshared (Il2CppObject * __this /* static, unused */, double ___t0, const MethodInfo* method);
#define Stubs_1_U3CIdentityU3Em__72_m91553976(__this /* static, unused */, ___t0, method) ((  double (*) (Il2CppObject * /* static, unused */, double, const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m91553976_gshared)(__this /* static, unused */, ___t0, method)
