﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<System.Single>
struct EmptyObserver_1_t367562011;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m626072225_gshared (EmptyObserver_1_t367562011 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m626072225(__this, method) ((  void (*) (EmptyObserver_1_t367562011 *, const MethodInfo*))EmptyObserver_1__ctor_m626072225_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1746273580_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m1746273580(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m1746273580_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1011618155_gshared (EmptyObserver_1_t367562011 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1011618155(__this, method) ((  void (*) (EmptyObserver_1_t367562011 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1011618155_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2871589400_gshared (EmptyObserver_1_t367562011 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m2871589400(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t367562011 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m2871589400_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Single>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3190690057_gshared (EmptyObserver_1_t367562011 * __this, float ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3190690057(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t367562011 *, float, const MethodInfo*))EmptyObserver_1_OnNext_m3190690057_gshared)(__this, ___value0, method)
