﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateCharacterDataRequestCallback
struct UpdateCharacterDataRequestCallback_t2875943562;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateCharacterDataRequest
struct UpdateCharacterDataRequest_t3002197813;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara3002197813.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateCharacterDataRequestCallback__ctor_m1002627705 (UpdateCharacterDataRequestCallback_t2875943562 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateCharacterDataRequest,System.Object)
extern "C"  void UpdateCharacterDataRequestCallback_Invoke_m1748932703 (UpdateCharacterDataRequestCallback_t2875943562 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterDataRequest_t3002197813 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateCharacterDataRequestCallback_t2875943562(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterDataRequest_t3002197813 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateCharacterDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateCharacterDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateCharacterDataRequestCallback_BeginInvoke_m2023546106 (UpdateCharacterDataRequestCallback_t2875943562 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterDataRequest_t3002197813 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateCharacterDataRequestCallback_EndInvoke_m2857662729 (UpdateCharacterDataRequestCallback_t2875943562 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
