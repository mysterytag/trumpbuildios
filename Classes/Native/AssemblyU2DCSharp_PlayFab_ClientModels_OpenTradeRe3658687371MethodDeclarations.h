﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.OpenTradeResponse
struct OpenTradeResponse_t3658687371;
// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770.h"

// System.Void PlayFab.ClientModels.OpenTradeResponse::.ctor()
extern "C"  void OpenTradeResponse__ctor_m2343269118 (OpenTradeResponse_t3658687371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.OpenTradeResponse::get_Trade()
extern "C"  TradeInfo_t1933812770 * OpenTradeResponse_get_Trade_m2344114126 (OpenTradeResponse_t3658687371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.OpenTradeResponse::set_Trade(PlayFab.ClientModels.TradeInfo)
extern "C"  void OpenTradeResponse_set_Trade_m1175227471 (OpenTradeResponse_t3658687371 * __this, TradeInfo_t1933812770 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
