﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.MonoHttpDate
struct MonoHttpDate_t4294596579;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.Net.MonoHttpDate::.ctor()
extern "C"  void MonoHttpDate__ctor_m27503969 (MonoHttpDate_t4294596579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.MonoHttpDate::.cctor()
extern "C"  void MonoHttpDate__cctor_m370526828 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Net.MonoHttpDate::Parse(System.String)
extern "C"  DateTime_t339033936  MonoHttpDate_Parse_m3732925207 (Il2CppObject * __this /* static, unused */, String_t* ___dateStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
