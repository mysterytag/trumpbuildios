﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>
struct DelayFrame_t1619463347;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26<System.Object>
struct  U3COnNextDelayU3Ec__Iterator26_t3660093125  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26::<frameCount>__0
	int32_t ___U3CframeCountU3E__0_0;
	// T UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26::value
	Il2CppObject * ___value_1;
	// System.Int32 UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26::$PC
	int32_t ___U24PC_2;
	// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26::$current
	Il2CppObject * ___U24current_3;
	// T UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26::<$>value
	Il2CppObject * ___U3CU24U3Evalue_4;
	// UniRx.Operators.DelayFrameObservable`1/DelayFrame<T> UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnNextDelay>c__Iterator26::<>f__this
	DelayFrame_t1619463347 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CframeCountU3E__0_0() { return static_cast<int32_t>(offsetof(U3COnNextDelayU3Ec__Iterator26_t3660093125, ___U3CframeCountU3E__0_0)); }
	inline int32_t get_U3CframeCountU3E__0_0() const { return ___U3CframeCountU3E__0_0; }
	inline int32_t* get_address_of_U3CframeCountU3E__0_0() { return &___U3CframeCountU3E__0_0; }
	inline void set_U3CframeCountU3E__0_0(int32_t value)
	{
		___U3CframeCountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(U3COnNextDelayU3Ec__Iterator26_t3660093125, ___value_1)); }
	inline Il2CppObject * get_value_1() const { return ___value_1; }
	inline Il2CppObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Il2CppObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3COnNextDelayU3Ec__Iterator26_t3660093125, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3COnNextDelayU3Ec__Iterator26_t3660093125, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Evalue_4() { return static_cast<int32_t>(offsetof(U3COnNextDelayU3Ec__Iterator26_t3660093125, ___U3CU24U3Evalue_4)); }
	inline Il2CppObject * get_U3CU24U3Evalue_4() const { return ___U3CU24U3Evalue_4; }
	inline Il2CppObject ** get_address_of_U3CU24U3Evalue_4() { return &___U3CU24U3Evalue_4; }
	inline void set_U3CU24U3Evalue_4(Il2CppObject * value)
	{
		___U3CU24U3Evalue_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Evalue_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3COnNextDelayU3Ec__Iterator26_t3660093125, ___U3CU3Ef__this_5)); }
	inline DelayFrame_t1619463347 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline DelayFrame_t1619463347 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(DelayFrame_t1619463347 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
