﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;

#include "AssemblyU2DCSharp_Zenject_Installer3915807453.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializablePrioritiesInstaller
struct  InitializablePrioritiesInstaller_t2944667948  : public Installer_t3915807453
{
public:
	// System.Collections.Generic.List`1<System.Type> Zenject.InitializablePrioritiesInstaller::_initializables
	List_1_t3576188904 * ____initializables_1;

public:
	inline static int32_t get_offset_of__initializables_1() { return static_cast<int32_t>(offsetof(InitializablePrioritiesInstaller_t2944667948, ____initializables_1)); }
	inline List_1_t3576188904 * get__initializables_1() const { return ____initializables_1; }
	inline List_1_t3576188904 ** get_address_of__initializables_1() { return &____initializables_1; }
	inline void set__initializables_1(List_1_t3576188904 * value)
	{
		____initializables_1 = value;
		Il2CppCodeGenWriteBarrier(&____initializables_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
