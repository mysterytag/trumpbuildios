﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SubscribeOnObservable`1<System.Object>
struct SubscribeOnObservable_1_t2329329235;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SubscribeOnObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern "C"  void SubscribeOnObservable_1__ctor_m2955604196_gshared (SubscribeOnObservable_1_t2329329235 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define SubscribeOnObservable_1__ctor_m2955604196(__this, ___source0, ___scheduler1, method) ((  void (*) (SubscribeOnObservable_1_t2329329235 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SubscribeOnObservable_1__ctor_m2955604196_gshared)(__this, ___source0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.SubscribeOnObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SubscribeOnObservable_1_SubscribeCore_m271267109_gshared (SubscribeOnObservable_1_t2329329235 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SubscribeOnObservable_1_SubscribeCore_m271267109(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SubscribeOnObservable_1_t2329329235 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SubscribeOnObservable_1_SubscribeCore_m271267109_gshared)(__this, ___observer0, ___cancel1, method)
