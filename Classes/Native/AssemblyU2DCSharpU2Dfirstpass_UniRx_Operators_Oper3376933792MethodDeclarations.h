﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t3376933792;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m933259564_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m933259564(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m933259564_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1522497745_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1522497745(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3376933792 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1522497745_gshared)(__this, method)
