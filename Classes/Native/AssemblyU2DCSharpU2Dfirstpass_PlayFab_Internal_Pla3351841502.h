﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.Internal.PlayFabPluginEventHandler
struct PlayFabPluginEventHandler_t3351841502;
// System.Collections.Generic.Dictionary`2<System.Int32,PlayFab.CallRequestContainer>
struct Dictionary_2_t923129392;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.PlayFabPluginEventHandler
struct  PlayFabPluginEventHandler_t3351841502  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct PlayFabPluginEventHandler_t3351841502_StaticFields
{
public:
	// PlayFab.Internal.PlayFabPluginEventHandler PlayFab.Internal.PlayFabPluginEventHandler::_playFabEvtHandler
	PlayFabPluginEventHandler_t3351841502 * ____playFabEvtHandler_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,PlayFab.CallRequestContainer> PlayFab.Internal.PlayFabPluginEventHandler::HttpHandlers
	Dictionary_2_t923129392 * ___HttpHandlers_3;

public:
	inline static int32_t get_offset_of__playFabEvtHandler_2() { return static_cast<int32_t>(offsetof(PlayFabPluginEventHandler_t3351841502_StaticFields, ____playFabEvtHandler_2)); }
	inline PlayFabPluginEventHandler_t3351841502 * get__playFabEvtHandler_2() const { return ____playFabEvtHandler_2; }
	inline PlayFabPluginEventHandler_t3351841502 ** get_address_of__playFabEvtHandler_2() { return &____playFabEvtHandler_2; }
	inline void set__playFabEvtHandler_2(PlayFabPluginEventHandler_t3351841502 * value)
	{
		____playFabEvtHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&____playFabEvtHandler_2, value);
	}

	inline static int32_t get_offset_of_HttpHandlers_3() { return static_cast<int32_t>(offsetof(PlayFabPluginEventHandler_t3351841502_StaticFields, ___HttpHandlers_3)); }
	inline Dictionary_2_t923129392 * get_HttpHandlers_3() const { return ___HttpHandlers_3; }
	inline Dictionary_2_t923129392 ** get_address_of_HttpHandlers_3() { return &___HttpHandlers_3; }
	inline void set_HttpHandlers_3(Dictionary_2_t923129392 * value)
	{
		___HttpHandlers_3 = value;
		Il2CppCodeGenWriteBarrier(&___HttpHandlers_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
