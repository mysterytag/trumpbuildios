﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987MethodDeclarations.h"

// System.Void UniRx.CollectionAddEvent`1<UpgradeButton>::.ctor(System.Int32,T)
#define CollectionAddEvent_1__ctor_m1081144916(__this, ___index0, ___value1, method) ((  void (*) (CollectionAddEvent_1_t3054882909 *, int32_t, UpgradeButton_t1475467342 *, const MethodInfo*))CollectionAddEvent_1__ctor_m4121910756_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionAddEvent`1<UpgradeButton>::get_Index()
#define CollectionAddEvent_1_get_Index_m3409345000(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t3054882909 *, const MethodInfo*))CollectionAddEvent_1_get_Index_m1649808760_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<UpgradeButton>::set_Index(System.Int32)
#define CollectionAddEvent_1_set_Index_m2403289551(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t3054882909 *, int32_t, const MethodInfo*))CollectionAddEvent_1_set_Index_m4001500511_gshared)(__this, ___value0, method)
// T UniRx.CollectionAddEvent`1<UpgradeButton>::get_Value()
#define CollectionAddEvent_1_get_Value_m3641901712(__this, method) ((  UpgradeButton_t1475467342 * (*) (CollectionAddEvent_1_t3054882909 *, const MethodInfo*))CollectionAddEvent_1_get_Value_m1882365472_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<UpgradeButton>::set_Value(T)
#define CollectionAddEvent_1_set_Value_m100414113(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t3054882909 *, UpgradeButton_t1475467342 *, const MethodInfo*))CollectionAddEvent_1_set_Value_m1389365521_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionAddEvent`1<UpgradeButton>::ToString()
#define CollectionAddEvent_1_ToString_m1428821282(__this, method) ((  String_t* (*) (CollectionAddEvent_1_t3054882909 *, const MethodInfo*))CollectionAddEvent_1_ToString_m3034630034_gshared)(__this, method)
// System.Int32 UniRx.CollectionAddEvent`1<UpgradeButton>::GetHashCode()
#define CollectionAddEvent_1_GetHashCode_m2609344336(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t3054882909 *, const MethodInfo*))CollectionAddEvent_1_GetHashCode_m3912132320_gshared)(__this, method)
// System.Boolean UniRx.CollectionAddEvent`1<UpgradeButton>::Equals(UniRx.CollectionAddEvent`1<T>)
#define CollectionAddEvent_1_Equals_m1271752592(__this, ___other0, method) ((  bool (*) (CollectionAddEvent_1_t3054882909 *, CollectionAddEvent_1_t3054882909 , const MethodInfo*))CollectionAddEvent_1_Equals_m2095755040_gshared)(__this, ___other0, method)
