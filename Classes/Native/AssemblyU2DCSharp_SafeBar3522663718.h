﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t514686775;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SafeBar
struct  SafeBar_t3522663718  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject SafeBar::fillerSample
	GameObject_t4012695102 * ___fillerSample_2;
	// System.Single SafeBar::currentRatio
	float ___currentRatio_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> SafeBar::fillers
	List_1_t514686775 * ___fillers_4;

public:
	inline static int32_t get_offset_of_fillerSample_2() { return static_cast<int32_t>(offsetof(SafeBar_t3522663718, ___fillerSample_2)); }
	inline GameObject_t4012695102 * get_fillerSample_2() const { return ___fillerSample_2; }
	inline GameObject_t4012695102 ** get_address_of_fillerSample_2() { return &___fillerSample_2; }
	inline void set_fillerSample_2(GameObject_t4012695102 * value)
	{
		___fillerSample_2 = value;
		Il2CppCodeGenWriteBarrier(&___fillerSample_2, value);
	}

	inline static int32_t get_offset_of_currentRatio_3() { return static_cast<int32_t>(offsetof(SafeBar_t3522663718, ___currentRatio_3)); }
	inline float get_currentRatio_3() const { return ___currentRatio_3; }
	inline float* get_address_of_currentRatio_3() { return &___currentRatio_3; }
	inline void set_currentRatio_3(float value)
	{
		___currentRatio_3 = value;
	}

	inline static int32_t get_offset_of_fillers_4() { return static_cast<int32_t>(offsetof(SafeBar_t3522663718, ___fillers_4)); }
	inline List_1_t514686775 * get_fillers_4() const { return ___fillers_4; }
	inline List_1_t514686775 ** get_address_of_fillers_4() { return &___fillers_4; }
	inline void set_fillers_4(List_1_t514686775 * value)
	{
		___fillers_4 = value;
		Il2CppCodeGenWriteBarrier(&___fillers_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
