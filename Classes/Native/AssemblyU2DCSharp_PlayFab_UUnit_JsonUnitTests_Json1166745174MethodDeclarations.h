﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.JsonUnitTests/JsonLongTest
struct JsonLongTest_t1166745174;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.UUnit.JsonUnitTests/JsonLongTest::.ctor()
extern "C"  void JsonLongTest__ctor_m4223868987 (JsonLongTest_t1166745174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.JsonUnitTests/JsonLongTest::TestObjNumField()
extern "C"  void JsonLongTest_TestObjNumField_m2746594386 (JsonLongTest_t1166745174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.JsonUnitTests/JsonLongTest::TestObjNumProp()
extern "C"  void JsonLongTest_TestObjNumProp_m1907523565 (JsonLongTest_t1166745174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.JsonUnitTests/JsonLongTest::TestStructNumField()
extern "C"  void JsonLongTest_TestStructNumField_m3772821028 (JsonLongTest_t1166745174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.JsonUnitTests/JsonLongTest::TestObjOptNumField()
extern "C"  void JsonLongTest_TestObjOptNumField_m4140818379 (JsonLongTest_t1166745174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.JsonUnitTests/JsonLongTest::OtherSpecificDatatypes()
extern "C"  void JsonLongTest_OtherSpecificDatatypes_m69080374 (JsonLongTest_t1166745174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
