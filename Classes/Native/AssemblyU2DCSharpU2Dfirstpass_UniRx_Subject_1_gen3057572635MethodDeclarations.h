﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UnityEngine.Collision>::.ctor()
#define Subject_1__ctor_m4130905429(__this, method) ((  void (*) (Subject_1_t3057572635 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Collision>::get_HasObservers()
#define Subject_1_get_HasObservers_m3205831959(__this, method) ((  bool (*) (Subject_1_t3057572635 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision>::OnCompleted()
#define Subject_1_OnCompleted_m2683026719(__this, method) ((  void (*) (Subject_1_t3057572635 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision>::OnError(System.Exception)
#define Subject_1_OnError_m3519068108(__this, ___error0, method) ((  void (*) (Subject_1_t3057572635 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision>::OnNext(T)
#define Subject_1_OnNext_m4081039037(__this, ___value0, method) ((  void (*) (Subject_1_t3057572635 *, Collision_t1119538015 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.Collision>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m393147220(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3057572635 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision>::Dispose()
#define Subject_1_Dispose_m1148618834(__this, method) ((  void (*) (Subject_1_t3057572635 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.Collision>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m1450688475(__this, method) ((  void (*) (Subject_1_t3057572635 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.Collision>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2686653006(__this, method) ((  bool (*) (Subject_1_t3057572635 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
