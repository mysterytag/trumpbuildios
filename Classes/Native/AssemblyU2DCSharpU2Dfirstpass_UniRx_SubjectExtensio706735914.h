﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.SubjectExtensions/AnonymousSubject`2<System.Object,System.Object>
struct  AnonymousSubject_2_t706735914  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.SubjectExtensions/AnonymousSubject`2::observer
	Il2CppObject* ___observer_0;
	// UniRx.IObservable`1<U> UniRx.SubjectExtensions/AnonymousSubject`2::observable
	Il2CppObject* ___observable_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(AnonymousSubject_2_t706735914, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_observable_1() { return static_cast<int32_t>(offsetof(AnonymousSubject_2_t706735914, ___observable_1)); }
	inline Il2CppObject* get_observable_1() const { return ___observable_1; }
	inline Il2CppObject** get_address_of_observable_1() { return &___observable_1; }
	inline void set_observable_1(Il2CppObject* value)
	{
		___observable_1 = value;
		Il2CppCodeGenWriteBarrier(&___observable_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
