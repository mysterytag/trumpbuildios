﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableYieldInstruction`1<System.Int64>
struct ObservableYieldInstruction_1_t674360470;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObservableYieldInstruction`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void ObservableYieldInstruction_1__ctor_m2213384432_gshared (ObservableYieldInstruction_1_t674360470 * __this, Il2CppObject* ___source0, bool ___reThrowOnError1, const MethodInfo* method);
#define ObservableYieldInstruction_1__ctor_m2213384432(__this, ___source0, ___reThrowOnError1, method) ((  void (*) (ObservableYieldInstruction_1_t674360470 *, Il2CppObject*, bool, const MethodInfo*))ObservableYieldInstruction_1__ctor_m2213384432_gshared)(__this, ___source0, ___reThrowOnError1, method)
// T UniRx.ObservableYieldInstruction`1<System.Int64>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int64_t ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2784390775_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2784390775(__this, method) ((  int64_t (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2784390775_gshared)(__this, method)
// System.Object UniRx.ObservableYieldInstruction`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m448923042_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m448923042(__this, method) ((  Il2CppObject * (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_get_Current_m448923042_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int64>::System.Collections.IEnumerator.MoveNext()
extern "C"  bool ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m2633288877_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m2633288877(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_MoveNext_m2633288877_gshared)(__this, method)
// System.Void UniRx.ObservableYieldInstruction`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m4227088300_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m4227088300(__this, method) ((  void (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_System_Collections_IEnumerator_Reset_m4227088300_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int64>::get_HasError()
extern "C"  bool ObservableYieldInstruction_1_get_HasError_m2926121257_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_HasError_m2926121257(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_get_HasError_m2926121257_gshared)(__this, method)
// System.Boolean UniRx.ObservableYieldInstruction`1<System.Int64>::get_HasResult()
extern "C"  bool ObservableYieldInstruction_1_get_HasResult_m3078562238_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_HasResult_m3078562238(__this, method) ((  bool (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_get_HasResult_m3078562238_gshared)(__this, method)
// T UniRx.ObservableYieldInstruction`1<System.Int64>::get_Result()
extern "C"  int64_t ObservableYieldInstruction_1_get_Result_m3536245275_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_Result_m3536245275(__this, method) ((  int64_t (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_get_Result_m3536245275_gshared)(__this, method)
// System.Exception UniRx.ObservableYieldInstruction`1<System.Int64>::get_Error()
extern "C"  Exception_t1967233988 * ObservableYieldInstruction_1_get_Error_m174596886_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_get_Error_m174596886(__this, method) ((  Exception_t1967233988 * (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_get_Error_m174596886_gshared)(__this, method)
// System.Void UniRx.ObservableYieldInstruction`1<System.Int64>::Dispose()
extern "C"  void ObservableYieldInstruction_1_Dispose_m2108654667_gshared (ObservableYieldInstruction_1_t674360470 * __this, const MethodInfo* method);
#define ObservableYieldInstruction_1_Dispose_m2108654667(__this, method) ((  void (*) (ObservableYieldInstruction_1_t674360470 *, const MethodInfo*))ObservableYieldInstruction_1_Dispose_m2108654667_gshared)(__this, method)
