﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen1694707683MethodDeclarations.h"

// System.Void System.Func`3<System.Boolean,System.Int32,<>__AnonType3`2<System.Boolean,System.Int32>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m2490981866(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t3953209855 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m3971538071_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Boolean,System.Int32,<>__AnonType3`2<System.Boolean,System.Int32>>::Invoke(T1,T2)
#define Func_3_Invoke_m2324398387(__this, ___arg10, ___arg21, method) ((  U3CU3E__AnonType3_2_t3095608592 * (*) (Func_3_t3953209855 *, bool, int32_t, const MethodInfo*))Func_3_Invoke_m457544530_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Boolean,System.Int32,<>__AnonType3`2<System.Boolean,System.Int32>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m2828338616(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t3953209855 *, bool, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3026494547_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Boolean,System.Int32,<>__AnonType3`2<System.Boolean,System.Int32>>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m1635009032(__this, ___result0, method) ((  U3CU3E__AnonType3_2_t3095608592 * (*) (Func_3_t3953209855 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m730517961_gshared)(__this, ___result0, method)
