﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IFactoryBinder`3<System.Object,System.Object,System.Object>
struct  IFactoryBinder_3_t153990564  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.IFactoryBinder`3::_container
	DiContainer_t2383114449 * ____container_0;
	// System.String Zenject.IFactoryBinder`3::_identifier
	String_t* ____identifier_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(IFactoryBinder_3_t153990564, ____container_0)); }
	inline DiContainer_t2383114449 * get__container_0() const { return ____container_0; }
	inline DiContainer_t2383114449 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t2383114449 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier(&____container_0, value);
	}

	inline static int32_t get_offset_of__identifier_1() { return static_cast<int32_t>(offsetof(IFactoryBinder_3_t153990564, ____identifier_1)); }
	inline String_t* get__identifier_1() const { return ____identifier_1; }
	inline String_t** get_address_of__identifier_1() { return &____identifier_1; }
	inline void set__identifier_1(String_t* value)
	{
		____identifier_1 = value;
		Il2CppCodeGenWriteBarrier(&____identifier_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
