﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RestoreIOSPurchasesRequestCallback
struct RestoreIOSPurchasesRequestCallback_t2601568225;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RestoreIOSPurchasesRequest
struct RestoreIOSPurchasesRequest_t2671348620;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RestoreIOSP2671348620.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RestoreIOSPurchasesRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RestoreIOSPurchasesRequestCallback__ctor_m2730926160 (RestoreIOSPurchasesRequestCallback_t2601568225 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RestoreIOSPurchasesRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RestoreIOSPurchasesRequest,System.Object)
extern "C"  void RestoreIOSPurchasesRequestCallback_Invoke_m3585862335 (RestoreIOSPurchasesRequestCallback_t2601568225 * __this, String_t* ___urlPath0, int32_t ___callId1, RestoreIOSPurchasesRequest_t2671348620 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RestoreIOSPurchasesRequestCallback_t2601568225(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RestoreIOSPurchasesRequest_t2671348620 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RestoreIOSPurchasesRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RestoreIOSPurchasesRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RestoreIOSPurchasesRequestCallback_BeginInvoke_m1751648808 (RestoreIOSPurchasesRequestCallback_t2601568225 * __this, String_t* ___urlPath0, int32_t ___callId1, RestoreIOSPurchasesRequest_t2671348620 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RestoreIOSPurchasesRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RestoreIOSPurchasesRequestCallback_EndInvoke_m3918969440 (RestoreIOSPurchasesRequestCallback_t2601568225 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
