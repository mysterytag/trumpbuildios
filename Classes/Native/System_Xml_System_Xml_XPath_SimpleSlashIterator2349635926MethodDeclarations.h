﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.SimpleSlashIterator
struct SimpleSlashIterator_t2349635926;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.NodeSet
struct NodeSet_t3503134685;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1624538935;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_NodeSet3503134685.h"
#include "System_Xml_System_Xml_XPath_SimpleSlashIterator2349635926.h"

// System.Void System.Xml.XPath.SimpleSlashIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.NodeSet)
extern "C"  void SimpleSlashIterator__ctor_m3208158132 (SimpleSlashIterator_t2349635926 * __this, BaseIterator_t3696600956 * ___left0, NodeSet_t3503134685 * ___expr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.SimpleSlashIterator::.ctor(System.Xml.XPath.SimpleSlashIterator)
extern "C"  void SimpleSlashIterator__ctor_m1778448769 (SimpleSlashIterator_t2349635926 * __this, SimpleSlashIterator_t2349635926 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SimpleSlashIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * SimpleSlashIterator_Clone_m2687801187 (SimpleSlashIterator_t2349635926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.SimpleSlashIterator::MoveNextCore()
extern "C"  bool SimpleSlashIterator_MoveNextCore_m3882540316 (SimpleSlashIterator_t2349635926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleSlashIterator::get_Current()
extern "C"  XPathNavigator_t1624538935 * SimpleSlashIterator_get_Current_m3159180143 (SimpleSlashIterator_t2349635926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
