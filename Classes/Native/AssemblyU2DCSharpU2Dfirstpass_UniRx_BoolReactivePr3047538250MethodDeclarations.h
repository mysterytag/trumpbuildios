﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.BoolReactiveProperty
struct BoolReactiveProperty_t3047538250;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.BoolReactiveProperty::.ctor()
extern "C"  void BoolReactiveProperty__ctor_m466976127 (BoolReactiveProperty_t3047538250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.BoolReactiveProperty::.ctor(System.Boolean)
extern "C"  void BoolReactiveProperty__ctor_m4231473334 (BoolReactiveProperty_t3047538250 * __this, bool ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
