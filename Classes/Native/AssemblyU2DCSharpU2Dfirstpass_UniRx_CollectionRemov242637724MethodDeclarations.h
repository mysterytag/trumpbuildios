﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.CollectionRemoveEvent`1<System.Object>::.ctor(System.Int32,T)
extern "C"  void CollectionRemoveEvent_1__ctor_m1246874125_gshared (CollectionRemoveEvent_1_t242637724 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define CollectionRemoveEvent_1__ctor_m1246874125(__this, ___index0, ___value1, method) ((  void (*) (CollectionRemoveEvent_1_t242637724 *, int32_t, Il2CppObject *, const MethodInfo*))CollectionRemoveEvent_1__ctor_m1246874125_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionRemoveEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionRemoveEvent_1_get_Index_m3125150261_gshared (CollectionRemoveEvent_1_t242637724 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_get_Index_m3125150261(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))CollectionRemoveEvent_1_get_Index_m3125150261_gshared)(__this, method)
// System.Void UniRx.CollectionRemoveEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionRemoveEvent_1_set_Index_m2755269448_gshared (CollectionRemoveEvent_1_t242637724 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionRemoveEvent_1_set_Index_m2755269448(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t242637724 *, int32_t, const MethodInfo*))CollectionRemoveEvent_1_set_Index_m2755269448_gshared)(__this, ___value0, method)
// T UniRx.CollectionRemoveEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionRemoveEvent_1_get_Value_m432042667_gshared (CollectionRemoveEvent_1_t242637724 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_get_Value_m432042667(__this, method) ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))CollectionRemoveEvent_1_get_Value_m432042667_gshared)(__this, method)
// System.Void UniRx.CollectionRemoveEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionRemoveEvent_1_set_Value_m2243992264_gshared (CollectionRemoveEvent_1_t242637724 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionRemoveEvent_1_set_Value_m2243992264(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t242637724 *, Il2CppObject *, const MethodInfo*))CollectionRemoveEvent_1_set_Value_m2243992264_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionRemoveEvent`1<System.Object>::ToString()
extern "C"  String_t* CollectionRemoveEvent_1_ToString_m676615279_gshared (CollectionRemoveEvent_1_t242637724 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_ToString_m676615279(__this, method) ((  String_t* (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))CollectionRemoveEvent_1_ToString_m676615279_gshared)(__this, method)
// System.Int32 UniRx.CollectionRemoveEvent`1<System.Object>::GetHashCode()
extern "C"  int32_t CollectionRemoveEvent_1_GetHashCode_m81139805_gshared (CollectionRemoveEvent_1_t242637724 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_GetHashCode_m81139805(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t242637724 *, const MethodInfo*))CollectionRemoveEvent_1_GetHashCode_m81139805_gshared)(__this, method)
// System.Boolean UniRx.CollectionRemoveEvent`1<System.Object>::Equals(UniRx.CollectionRemoveEvent`1<T>)
extern "C"  bool CollectionRemoveEvent_1_Equals_m376446406_gshared (CollectionRemoveEvent_1_t242637724 * __this, CollectionRemoveEvent_1_t242637724  ___other0, const MethodInfo* method);
#define CollectionRemoveEvent_1_Equals_m376446406(__this, ___other0, method) ((  bool (*) (CollectionRemoveEvent_1_t242637724 *, CollectionRemoveEvent_1_t242637724 , const MethodInfo*))CollectionRemoveEvent_1_Equals_m376446406_gshared)(__this, ___other0, method)
