﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<WaitForPressBottomButton>c__AnonStoreyEC
struct U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_t3991585071;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TutorialPlayer/<WaitForPressBottomButton>c__AnonStoreyEC::.ctor()
extern "C"  void U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC__ctor_m2372021852 (U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_t3991585071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForPressBottomButton>c__AnonStoreyEC::<>m__147(UniRx.Unit)
extern "C"  void U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_U3CU3Em__147_m2092610037 (U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_t3991585071 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForPressBottomButton>c__AnonStoreyEC::<>m__148(UniRx.Unit)
extern "C"  void U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_U3CU3Em__148_m1799207030 (U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_t3991585071 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<WaitForPressBottomButton>c__AnonStoreyEC::<>m__149()
extern "C"  bool U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_U3CU3Em__149_m1628539399 (U3CWaitForPressBottomButtonU3Ec__AnonStoreyEC_t3991585071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
