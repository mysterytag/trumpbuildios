﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>
struct Buffer_t492975322;
// UniRx.Operators.BufferObservable`1/BufferT<System.Object>
struct BufferT_t129222477;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>::.ctor(UniRx.Operators.BufferObservable`1/BufferT<T>)
extern "C"  void Buffer__ctor_m2214677866_gshared (Buffer_t492975322 * __this, BufferT_t129222477 * ___parent0, const MethodInfo* method);
#define Buffer__ctor_m2214677866(__this, ___parent0, method) ((  void (*) (Buffer_t492975322 *, BufferT_t129222477 *, const MethodInfo*))Buffer__ctor_m2214677866_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>::OnNext(System.Int64)
extern "C"  void Buffer_OnNext_m3663740246_gshared (Buffer_t492975322 * __this, int64_t ___value0, const MethodInfo* method);
#define Buffer_OnNext_m3663740246(__this, ___value0, method) ((  void (*) (Buffer_t492975322 *, int64_t, const MethodInfo*))Buffer_OnNext_m3663740246_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>::OnError(System.Exception)
extern "C"  void Buffer_OnError_m613702249_gshared (Buffer_t492975322 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Buffer_OnError_m613702249(__this, ___error0, method) ((  void (*) (Buffer_t492975322 *, Exception_t1967233988 *, const MethodInfo*))Buffer_OnError_m613702249_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>::OnCompleted()
extern "C"  void Buffer_OnCompleted_m1276158780_gshared (Buffer_t492975322 * __this, const MethodInfo* method);
#define Buffer_OnCompleted_m1276158780(__this, method) ((  void (*) (Buffer_t492975322 *, const MethodInfo*))Buffer_OnCompleted_m1276158780_gshared)(__this, method)
