﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1<System.Object>
struct  U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerable`1<T> UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1::source
	Il2CppObject* ___source_0;
	// System.Collections.IEnumerator UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1::<e>__0
	Il2CppObject * ___U3CeU3E__0_1;
	// System.IDisposable UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1::<$s_338>__1
	Il2CppObject * ___U3CU24s_338U3E__1_2;
	// System.Int32 UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1::$PC
	int32_t ___U24PC_3;
	// T UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1::$current
	Il2CppObject * ___U24current_4;
	// System.Collections.Generic.IEnumerable`1<T> UniRx.AotSafeExtensions/<AsSafeEnumerable>c__Iterator1D`1::<$>source
	Il2CppObject* ___U3CU24U3Esource_5;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259, ___source_0)); }
	inline Il2CppObject* get_source_0() const { return ___source_0; }
	inline Il2CppObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Il2CppObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}

	inline static int32_t get_offset_of_U3CeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259, ___U3CeU3E__0_1)); }
	inline Il2CppObject * get_U3CeU3E__0_1() const { return ___U3CeU3E__0_1; }
	inline Il2CppObject ** get_address_of_U3CeU3E__0_1() { return &___U3CeU3E__0_1; }
	inline void set_U3CeU3E__0_1(Il2CppObject * value)
	{
		___U3CeU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_338U3E__1_2() { return static_cast<int32_t>(offsetof(U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259, ___U3CU24s_338U3E__1_2)); }
	inline Il2CppObject * get_U3CU24s_338U3E__1_2() const { return ___U3CU24s_338U3E__1_2; }
	inline Il2CppObject ** get_address_of_U3CU24s_338U3E__1_2() { return &___U3CU24s_338U3E__1_2; }
	inline void set_U3CU24s_338U3E__1_2(Il2CppObject * value)
	{
		___U3CU24s_338U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_338U3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esource_5() { return static_cast<int32_t>(offsetof(U3CAsSafeEnumerableU3Ec__Iterator1D_1_t3925284259, ___U3CU24U3Esource_5)); }
	inline Il2CppObject* get_U3CU24U3Esource_5() const { return ___U3CU24U3Esource_5; }
	inline Il2CppObject** get_address_of_U3CU24U3Esource_5() { return &___U3CU24U3Esource_5; }
	inline void set_U3CU24U3Esource_5(Il2CppObject* value)
	{
		___U3CU24U3Esource_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esource_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
