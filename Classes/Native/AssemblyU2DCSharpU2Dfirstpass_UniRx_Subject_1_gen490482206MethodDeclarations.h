﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<System.Int64>
struct Subject_1_t490482206;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Subject`1<System.Int64>::.ctor()
extern "C"  void Subject_1__ctor_m2409416222_gshared (Subject_1_t490482206 * __this, const MethodInfo* method);
#define Subject_1__ctor_m2409416222(__this, method) ((  void (*) (Subject_1_t490482206 *, const MethodInfo*))Subject_1__ctor_m2409416222_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Int64>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m933210102_gshared (Subject_1_t490482206 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m933210102(__this, method) ((  bool (*) (Subject_1_t490482206 *, const MethodInfo*))Subject_1_get_HasObservers_m933210102_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Int64>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m542534440_gshared (Subject_1_t490482206 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m542534440(__this, method) ((  void (*) (Subject_1_t490482206 *, const MethodInfo*))Subject_1_OnCompleted_m542534440_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Int64>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m1949409365_gshared (Subject_1_t490482206 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m1949409365(__this, ___error0, method) ((  void (*) (Subject_1_t490482206 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1949409365_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<System.Int64>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3292320070_gshared (Subject_1_t490482206 * __this, int64_t ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m3292320070(__this, ___value0, method) ((  void (*) (Subject_1_t490482206 *, int64_t, const MethodInfo*))Subject_1_OnNext_m3292320070_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2550760115_gshared (Subject_1_t490482206 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2550760115(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t490482206 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2550760115_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<System.Int64>::Dispose()
extern "C"  void Subject_1_Dispose_m359899867_gshared (Subject_1_t490482206 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m359899867(__this, method) ((  void (*) (Subject_1_t490482206 *, const MethodInfo*))Subject_1_Dispose_m359899867_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Int64>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m643449188_gshared (Subject_1_t490482206 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m643449188(__this, method) ((  void (*) (Subject_1_t490482206 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m643449188_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Int64>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3393864557_gshared (Subject_1_t490482206 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3393864557(__this, method) ((  bool (*) (Subject_1_t490482206 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3393864557_gshared)(__this, method)
