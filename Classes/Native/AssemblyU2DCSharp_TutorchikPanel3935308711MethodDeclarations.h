﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorchikPanel
struct TutorchikPanel_t3935308711;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void TutorchikPanel::.ctor()
extern "C"  void TutorchikPanel__ctor_m3779783892 (TutorchikPanel_t3935308711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorchikPanel::PostInject()
extern "C"  void TutorchikPanel_PostInject_m3092647105 (TutorchikPanel_t3935308711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorchikPanel::KillTweans()
extern "C"  void TutorchikPanel_KillTweans_m241530770 (TutorchikPanel_t3935308711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorchikPanel::<PostInject>m__27A(System.String)
extern "C"  void TutorchikPanel_U3CPostInjectU3Em__27A_m524819858 (TutorchikPanel_t3935308711 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorchikPanel::<PostInject>m__27B()
extern "C"  void TutorchikPanel_U3CPostInjectU3Em__27B_m262085425 (TutorchikPanel_t3935308711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorchikPanel::<PostInject>m__27C()
extern "C"  void TutorchikPanel_U3CPostInjectU3Em__27C_m262086386 (TutorchikPanel_t3935308711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorchikPanel::<PostInject>m__27D()
extern "C"  void TutorchikPanel_U3CPostInjectU3Em__27D_m262087347 (TutorchikPanel_t3935308711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
