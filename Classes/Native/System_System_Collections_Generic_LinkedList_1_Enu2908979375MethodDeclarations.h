﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<IProcess>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m348691861(__this, ___parent0, method) ((  void (*) (Enumerator_t2908979376 *, LinkedList_1_t1471399664 *, const MethodInfo*))Enumerator__ctor_m857368315_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<IProcess>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2771360492(__this, method) ((  Il2CppObject * (*) (Enumerator_t2908979376 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<IProcess>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2582697142(__this, method) ((  void (*) (Enumerator_t2908979376 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<IProcess>::get_Current()
#define Enumerator_get_Current_m861793811(__this, method) ((  Il2CppObject * (*) (Enumerator_t2908979376 *, const MethodInfo*))Enumerator_get_Current_m1124073047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<IProcess>::MoveNext()
#define Enumerator_MoveNext_m4191291770(__this, method) ((  bool (*) (Enumerator_t2908979376 *, const MethodInfo*))Enumerator_MoveNext_m2358966120_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<IProcess>::Dispose()
#define Enumerator_Dispose_m3335238401(__this, method) ((  void (*) (Enumerator_t2908979376 *, const MethodInfo*))Enumerator_Dispose_m272587367_gshared)(__this, method)
