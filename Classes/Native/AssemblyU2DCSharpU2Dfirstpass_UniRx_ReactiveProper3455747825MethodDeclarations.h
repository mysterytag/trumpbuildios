﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<System.Int32>
struct ReactiveProperty_1_t3455747825;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t876714142;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ReactiveProperty`1<System.Int32>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m1270444133_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1270444133(__this, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, const MethodInfo*))ReactiveProperty_1__ctor_m1270444133_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m729063833_gshared (ReactiveProperty_1_t3455747825 * __this, int32_t ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m729063833(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, int32_t, const MethodInfo*))ReactiveProperty_1__ctor_m729063833_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m3020016772_gshared (ReactiveProperty_1_t3455747825 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3020016772(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m3020016772_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m3133198620_gshared (ReactiveProperty_1_t3455747825 * __this, Il2CppObject* ___source0, int32_t ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3133198620(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, Il2CppObject*, int32_t, const MethodInfo*))ReactiveProperty_1__ctor_m3133198620_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m246966248_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m246966248(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m246966248_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Int32>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m3877458094_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m3877458094(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t3455747825 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m3877458094_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<System.Int32>::get_Value()
extern "C"  int32_t ReactiveProperty_1_get_Value_m1685227756_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m1685227756(__this, method) ((  int32_t (*) (ReactiveProperty_1_t3455747825 *, const MethodInfo*))ReactiveProperty_1_get_Value_m1685227756_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3338686823_gshared (ReactiveProperty_1_t3455747825 * __this, int32_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m3338686823(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, int32_t, const MethodInfo*))ReactiveProperty_1_set_Value_m3338686823_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m886123376_gshared (ReactiveProperty_1_t3455747825 * __this, int32_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m886123376(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, int32_t, const MethodInfo*))ReactiveProperty_1_SetValue_m886123376_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m363687219_gshared (ReactiveProperty_1_t3455747825 * __this, int32_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m363687219(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, int32_t, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m363687219_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m4285240932_gshared (ReactiveProperty_1_t3455747825 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m4285240932(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t3455747825 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m4285240932_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m1024382818_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m1024382818(__this, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, const MethodInfo*))ReactiveProperty_1_Dispose_m1024382818_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Int32>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m4214225241_gshared (ReactiveProperty_1_t3455747825 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m4214225241(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t3455747825 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m4214225241_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<System.Int32>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m1266433294_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m1266433294(__this, method) ((  String_t* (*) (ReactiveProperty_1_t3455747825 *, const MethodInfo*))ReactiveProperty_1_ToString_m1266433294_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<System.Int32>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2531383614_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2531383614(__this, method) ((  bool (*) (ReactiveProperty_1_t3455747825 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2531383614_gshared)(__this, method)
