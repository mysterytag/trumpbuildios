﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MethodProvider`1<System.Object>
struct  MethodProvider_1_t512585361  : public ProviderBase_t1627494391
{
public:
	// System.Func`2<Zenject.InjectContext,T> Zenject.MethodProvider`1::_method
	Func_2_t2621245597 * ____method_2;

public:
	inline static int32_t get_offset_of__method_2() { return static_cast<int32_t>(offsetof(MethodProvider_1_t512585361, ____method_2)); }
	inline Func_2_t2621245597 * get__method_2() const { return ____method_2; }
	inline Func_2_t2621245597 ** get_address_of__method_2() { return &____method_2; }
	inline void set__method_2(Func_2_t2621245597 * value)
	{
		____method_2 = value;
		Il2CppCodeGenWriteBarrier(&____method_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
