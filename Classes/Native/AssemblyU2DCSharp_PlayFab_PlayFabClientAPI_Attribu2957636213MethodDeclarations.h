﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AttributeInstallRequestCallback
struct AttributeInstallRequestCallback_t2957636213;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AttributeInstallRequest
struct AttributeInstallRequest_t4240476704;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AttributeIn4240476704.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AttributeInstallRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AttributeInstallRequestCallback__ctor_m3452577924 (AttributeInstallRequestCallback_t2957636213 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AttributeInstallRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AttributeInstallRequest,System.Object)
extern "C"  void AttributeInstallRequestCallback_Invoke_m459399791 (AttributeInstallRequestCallback_t2957636213 * __this, String_t* ___urlPath0, int32_t ___callId1, AttributeInstallRequest_t4240476704 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AttributeInstallRequestCallback_t2957636213(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AttributeInstallRequest_t4240476704 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AttributeInstallRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AttributeInstallRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AttributeInstallRequestCallback_BeginInvoke_m606402302 (AttributeInstallRequestCallback_t2957636213 * __this, String_t* ___urlPath0, int32_t ___callId1, AttributeInstallRequest_t4240476704 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AttributeInstallRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AttributeInstallRequestCallback_EndInvoke_m661552276 (AttributeInstallRequestCallback_t2957636213 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
