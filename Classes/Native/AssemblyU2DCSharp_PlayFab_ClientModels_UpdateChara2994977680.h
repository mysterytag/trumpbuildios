﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UpdateCharacterStatisticsResult
struct  UpdateCharacterStatisticsResult_t2994977680  : public PlayFabResultCommon_t379512675
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
