﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RunCloudScriptResponseCallback
struct RunCloudScriptResponseCallback_t1743857083;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RunCloudScriptRequest
struct RunCloudScriptRequest_t180913386;
// PlayFab.ClientModels.RunCloudScriptResult
struct RunCloudScriptResult_t3227572354;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RunCloudScri180913386.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RunCloudScr3227572354.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RunCloudScriptResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RunCloudScriptResponseCallback__ctor_m4220760362 (RunCloudScriptResponseCallback_t1743857083 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RunCloudScriptResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RunCloudScriptRequest,PlayFab.ClientModels.RunCloudScriptResult,PlayFab.PlayFabError,System.Object)
extern "C"  void RunCloudScriptResponseCallback_Invoke_m66165079 (RunCloudScriptResponseCallback_t1743857083 * __this, String_t* ___urlPath0, int32_t ___callId1, RunCloudScriptRequest_t180913386 * ___request2, RunCloudScriptResult_t3227572354 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RunCloudScriptResponseCallback_t1743857083(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RunCloudScriptRequest_t180913386 * ___request2, RunCloudScriptResult_t3227572354 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RunCloudScriptResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RunCloudScriptRequest,PlayFab.ClientModels.RunCloudScriptResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RunCloudScriptResponseCallback_BeginInvoke_m2943799364 (RunCloudScriptResponseCallback_t1743857083 * __this, String_t* ___urlPath0, int32_t ___callId1, RunCloudScriptRequest_t180913386 * ___request2, RunCloudScriptResult_t3227572354 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RunCloudScriptResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RunCloudScriptResponseCallback_EndInvoke_m2566015034 (RunCloudScriptResponseCallback_t1743857083 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
