﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharp_CellUtils_ListDisposable2393830995.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellUtils.CellJoinDisposable`1<System.Int32>
struct  CellJoinDisposable_1_t1451834474  : public ListDisposable_t2393830995
{
public:
	// System.Object CellUtils.CellJoinDisposable`1::lastValue
	Il2CppObject * ___lastValue_2;

public:
	inline static int32_t get_offset_of_lastValue_2() { return static_cast<int32_t>(offsetof(CellJoinDisposable_1_t1451834474, ___lastValue_2)); }
	inline Il2CppObject * get_lastValue_2() const { return ___lastValue_2; }
	inline Il2CppObject ** get_address_of_lastValue_2() { return &___lastValue_2; }
	inline void set_lastValue_2(Il2CppObject * value)
	{
		___lastValue_2 = value;
		Il2CppCodeGenWriteBarrier(&___lastValue_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
