﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserFacebookInfo
struct UserFacebookInfo_t1491589167;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserFacebookInfo::.ctor()
extern "C"  void UserFacebookInfo__ctor_m666229998 (UserFacebookInfo_t1491589167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserFacebookInfo::get_FacebookId()
extern "C"  String_t* UserFacebookInfo_get_FacebookId_m1528173405 (UserFacebookInfo_t1491589167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserFacebookInfo::set_FacebookId(System.String)
extern "C"  void UserFacebookInfo_set_FacebookId_m3398699054 (UserFacebookInfo_t1491589167 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserFacebookInfo::get_FullName()
extern "C"  String_t* UserFacebookInfo_get_FullName_m2657458966 (UserFacebookInfo_t1491589167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserFacebookInfo::set_FullName(System.String)
extern "C"  void UserFacebookInfo_set_FullName_m1578687317 (UserFacebookInfo_t1491589167 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
