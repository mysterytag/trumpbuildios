﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ScheduledNotifier`1<System.Single>
struct ScheduledNotifier_1_t2478225832;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Single>
struct  U3CReportU3Ec__AnonStorey59_t941352311  : public Il2CppObject
{
public:
	// T UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59::value
	float ___value_0;
	// UniRx.ScheduledNotifier`1<T> UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59::<>f__this
	ScheduledNotifier_1_t2478225832 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__AnonStorey59_t941352311, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CReportU3Ec__AnonStorey59_t941352311, ___U3CU3Ef__this_1)); }
	inline ScheduledNotifier_1_t2478225832 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline ScheduledNotifier_1_t2478225832 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(ScheduledNotifier_1_t2478225832 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
