﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.InterstitialRequestResponseReceivedHandler
struct InterstitialRequestResponseReceivedHandler_t2506718783;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.InterstitialRequestResponseReceivedHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void InterstitialRequestResponseReceivedHandler__ctor_m856782460 (InterstitialRequestResponseReceivedHandler_t2506718783 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialRequestResponseReceivedHandler::Invoke(System.Boolean)
extern "C"  void InterstitialRequestResponseReceivedHandler_Invoke_m551029965 (InterstitialRequestResponseReceivedHandler_t2506718783 * __this, bool ___offersAvailable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_InterstitialRequestResponseReceivedHandler_t2506718783(Il2CppObject* delegate, bool ___offersAvailable0);
// System.IAsyncResult SponsorPay.InterstitialRequestResponseReceivedHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InterstitialRequestResponseReceivedHandler_BeginInvoke_m2166848626 (InterstitialRequestResponseReceivedHandler_t2506718783 * __this, bool ___offersAvailable0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.InterstitialRequestResponseReceivedHandler::EndInvoke(System.IAsyncResult)
extern "C"  void InterstitialRequestResponseReceivedHandler_EndInvoke_m1857783436 (InterstitialRequestResponseReceivedHandler_t2506718783 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
