﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>
struct Aggregate_t245075056;
// UniRx.Operators.AggregateObservable`3<System.Object,System.Object,System.Object>
struct AggregateObservable_3_t3567497731;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.AggregateObservable`3<TSource,TAccumulate,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void Aggregate__ctor_m876770702_gshared (Aggregate_t245075056 * __this, AggregateObservable_3_t3567497731 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Aggregate__ctor_m876770702(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Aggregate_t245075056 *, AggregateObservable_3_t3567497731 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Aggregate__ctor_m876770702_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::OnNext(TSource)
extern "C"  void Aggregate_OnNext_m2816633106_gshared (Aggregate_t245075056 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Aggregate_OnNext_m2816633106(__this, ___value0, method) ((  void (*) (Aggregate_t245075056 *, Il2CppObject *, const MethodInfo*))Aggregate_OnNext_m2816633106_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Aggregate_OnError_m394295804_gshared (Aggregate_t245075056 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Aggregate_OnError_m394295804(__this, ___error0, method) ((  void (*) (Aggregate_t245075056 *, Exception_t1967233988 *, const MethodInfo*))Aggregate_OnError_m394295804_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AggregateObservable`3/Aggregate<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void Aggregate_OnCompleted_m3877193551_gshared (Aggregate_t245075056 * __this, const MethodInfo* method);
#define Aggregate_OnCompleted_m3877193551(__this, method) ((  void (*) (Aggregate_t245075056 *, const MethodInfo*))Aggregate_OnCompleted_m3877193551_gshared)(__this, method)
