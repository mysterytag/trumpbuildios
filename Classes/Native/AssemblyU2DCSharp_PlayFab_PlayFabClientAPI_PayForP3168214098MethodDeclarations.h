﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/PayForPurchaseRequestCallback
struct PayForPurchaseRequestCallback_t3168214098;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.PayForPurchaseRequest
struct PayForPurchaseRequest_t2884624637;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PayForPurch2884624637.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/PayForPurchaseRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PayForPurchaseRequestCallback__ctor_m652663777 (PayForPurchaseRequestCallback_t3168214098 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/PayForPurchaseRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.PayForPurchaseRequest,System.Object)
extern "C"  void PayForPurchaseRequestCallback_Invoke_m2817301301 (PayForPurchaseRequestCallback_t3168214098 * __this, String_t* ___urlPath0, int32_t ___callId1, PayForPurchaseRequest_t2884624637 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_PayForPurchaseRequestCallback_t3168214098(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, PayForPurchaseRequest_t2884624637 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/PayForPurchaseRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.PayForPurchaseRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PayForPurchaseRequestCallback_BeginInvoke_m2117892254 (PayForPurchaseRequestCallback_t3168214098 * __this, String_t* ___urlPath0, int32_t ___callId1, PayForPurchaseRequest_t2884624637 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/PayForPurchaseRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PayForPurchaseRequestCallback_EndInvoke_m2549966449 (PayForPurchaseRequestCallback_t3168214098 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
