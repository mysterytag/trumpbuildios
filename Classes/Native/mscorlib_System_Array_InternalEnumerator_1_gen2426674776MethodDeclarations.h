﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2426674776.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273.h"

// System.Void System.Array/InternalEnumerator`1<UniRx.Timestamped`1<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m857097407_gshared (InternalEnumerator_1_t2426674776 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m857097407(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2426674776 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m857097407_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.Timestamped`1<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1411932801_gshared (InternalEnumerator_1_t2426674776 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1411932801(__this, method) ((  void (*) (InternalEnumerator_1_t2426674776 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1411932801_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UniRx.Timestamped`1<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4277978413_gshared (InternalEnumerator_1_t2426674776 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4277978413(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2426674776 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4277978413_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UniRx.Timestamped`1<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1363140950_gshared (InternalEnumerator_1_t2426674776 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1363140950(__this, method) ((  void (*) (InternalEnumerator_1_t2426674776 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1363140950_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UniRx.Timestamped`1<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2243934317_gshared (InternalEnumerator_1_t2426674776 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2243934317(__this, method) ((  bool (*) (InternalEnumerator_1_t2426674776 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2243934317_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UniRx.Timestamped`1<System.Object>>::get_Current()
extern "C"  Timestamped_1_t2519184273  InternalEnumerator_1_get_Current_m2985224902_gshared (InternalEnumerator_1_t2426674776 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2985224902(__this, method) ((  Timestamped_1_t2519184273  (*) (InternalEnumerator_1_t2426674776 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2985224902_gshared)(__this, method)
