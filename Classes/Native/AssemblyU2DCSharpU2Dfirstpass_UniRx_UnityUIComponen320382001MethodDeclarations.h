﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA3
struct U3CSubscribeToTextU3Ec__AnonStoreyA3_t320382001;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA3::.ctor()
extern "C"  void U3CSubscribeToTextU3Ec__AnonStoreyA3__ctor_m3025358871 (U3CSubscribeToTextU3Ec__AnonStoreyA3_t320382001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA3::<>m__E1(System.String)
extern "C"  void U3CSubscribeToTextU3Ec__AnonStoreyA3_U3CU3Em__E1_m2483564502 (U3CSubscribeToTextU3Ec__AnonStoreyA3_t320382001 * __this, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
