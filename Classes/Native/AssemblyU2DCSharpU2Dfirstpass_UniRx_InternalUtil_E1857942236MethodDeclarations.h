﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObserver_1_t1857942236;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3500759128_gshared (EmptyObserver_1_t1857942236 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m3500759128(__this, method) ((  void (*) (EmptyObserver_1_t1857942236 *, const MethodInfo*))EmptyObserver_1__ctor_m3500759128_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m667254357_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m667254357(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m667254357_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2992040418_gshared (EmptyObserver_1_t1857942236 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2992040418(__this, method) ((  void (*) (EmptyObserver_1_t1857942236 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2992040418_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3466057743_gshared (EmptyObserver_1_t1857942236 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3466057743(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1857942236 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3466057743_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4100832512_gshared (EmptyObserver_1_t1857942236 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m4100832512(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1857942236 *, CollectionMoveEvent_1_t2448589246 , const MethodInfo*))EmptyObserver_1_OnNext_m4100832512_gshared)(__this, ___value0, method)
