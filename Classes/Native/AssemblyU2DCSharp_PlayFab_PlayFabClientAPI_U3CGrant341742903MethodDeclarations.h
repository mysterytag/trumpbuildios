﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GrantCharacterToUser>c__AnonStoreyCB
struct U3CGrantCharacterToUserU3Ec__AnonStoreyCB_t341742903;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GrantCharacterToUser>c__AnonStoreyCB::.ctor()
extern "C"  void U3CGrantCharacterToUserU3Ec__AnonStoreyCB__ctor_m1562479708 (U3CGrantCharacterToUserU3Ec__AnonStoreyCB_t341742903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GrantCharacterToUser>c__AnonStoreyCB::<>m__B8(PlayFab.CallRequestContainer)
extern "C"  void U3CGrantCharacterToUserU3Ec__AnonStoreyCB_U3CU3Em__B8_m52680688 (U3CGrantCharacterToUserU3Ec__AnonStoreyCB_t341742903 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
