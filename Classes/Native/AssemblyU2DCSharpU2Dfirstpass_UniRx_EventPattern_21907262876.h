﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.Examples.Sample09_EventHandling/MyEventArgs
struct MyEventArgs_t3100194347;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct  EventPattern_2_t1907262876  : public Il2CppObject
{
public:
	// TSender UniRx.EventPattern`2::<Sender>k__BackingField
	Il2CppObject * ___U3CSenderU3Ek__BackingField_0;
	// TEventArgs UniRx.EventPattern`2::<EventArgs>k__BackingField
	MyEventArgs_t3100194347 * ___U3CEventArgsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CSenderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EventPattern_2_t1907262876, ___U3CSenderU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CSenderU3Ek__BackingField_0() const { return ___U3CSenderU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CSenderU3Ek__BackingField_0() { return &___U3CSenderU3Ek__BackingField_0; }
	inline void set_U3CSenderU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CSenderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSenderU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CEventArgsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EventPattern_2_t1907262876, ___U3CEventArgsU3Ek__BackingField_1)); }
	inline MyEventArgs_t3100194347 * get_U3CEventArgsU3Ek__BackingField_1() const { return ___U3CEventArgsU3Ek__BackingField_1; }
	inline MyEventArgs_t3100194347 ** get_address_of_U3CEventArgsU3Ek__BackingField_1() { return &___U3CEventArgsU3Ek__BackingField_1; }
	inline void set_U3CEventArgsU3Ek__BackingField_1(MyEventArgs_t3100194347 * value)
	{
		___U3CEventArgsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEventArgsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
