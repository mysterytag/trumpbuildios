﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.StableCompositeDisposable/NAry
struct NAry_t2389818;
// System.IDisposable[]
struct IDisposableU5BU5D_t165183403;
// System.Collections.Generic.IEnumerable`1<System.IDisposable>
struct IEnumerable_1_t206108434;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.StableCompositeDisposable/NAry::.ctor(System.IDisposable[])
extern "C"  void NAry__ctor_m762476142 (NAry_t2389818 * __this, IDisposableU5BU5D_t165183403* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.StableCompositeDisposable/NAry::.ctor(System.Collections.Generic.IEnumerable`1<System.IDisposable>)
extern "C"  void NAry__ctor_m1676968911 (NAry_t2389818 * __this, Il2CppObject* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.StableCompositeDisposable/NAry::get_IsDisposed()
extern "C"  bool NAry_get_IsDisposed_m3734151582 (NAry_t2389818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.StableCompositeDisposable/NAry::Dispose()
extern "C"  void NAry_Dispose_m1942366071 (NAry_t2389818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
