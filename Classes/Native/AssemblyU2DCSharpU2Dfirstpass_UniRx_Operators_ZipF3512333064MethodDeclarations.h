﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_5_t3512333064;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipFunc_5__ctor_m1668006884_gshared (ZipFunc_5_t3512333064 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ZipFunc_5__ctor_m1668006884(__this, ___object0, ___method1, method) ((  void (*) (ZipFunc_5_t3512333064 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ZipFunc_5__ctor_m1668006884_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * ZipFunc_5_Invoke_m526341887_gshared (ZipFunc_5_t3512333064 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
#define ZipFunc_5_Invoke_m526341887(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  Il2CppObject * (*) (ZipFunc_5_t3512333064 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ZipFunc_5_Invoke_m526341887_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipFunc_5_BeginInvoke_m2551056839_gshared (ZipFunc_5_t3512333064 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method);
#define ZipFunc_5_BeginInvoke_m2551056839(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (ZipFunc_5_t3512333064 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ZipFunc_5_BeginInvoke_m2551056839_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TR UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ZipFunc_5_EndInvoke_m2102480539_gshared (ZipFunc_5_t3512333064 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ZipFunc_5_EndInvoke_m2102480539(__this, ___result0, method) ((  Il2CppObject * (*) (ZipFunc_5_t3512333064 *, Il2CppObject *, const MethodInfo*))ZipFunc_5_EndInvoke_m2102480539_gshared)(__this, ___result0, method)
