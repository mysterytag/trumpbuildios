﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension
struct SubjectKeyIdentifierExtension_t106392422;
// Mono.Security.X509.X509Extension
struct X509Extension_t1510964269;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_X509_X509Extension1510964268.h"

// System.Void Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::.ctor(Mono.Security.X509.X509Extension)
extern "C"  void SubjectKeyIdentifierExtension__ctor_m1260506604 (SubjectKeyIdentifierExtension_t106392422 * __this, X509Extension_t1510964269 * ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::Decode()
extern "C"  void SubjectKeyIdentifierExtension_Decode_m1734897586 (SubjectKeyIdentifierExtension_t106392422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::get_Name()
extern "C"  String_t* SubjectKeyIdentifierExtension_get_Name_m602902165 (SubjectKeyIdentifierExtension_t106392422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::get_Identifier()
extern "C"  ByteU5BU5D_t58506160* SubjectKeyIdentifierExtension_get_Identifier_m1016563980 (SubjectKeyIdentifierExtension_t106392422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::ToString()
extern "C"  String_t* SubjectKeyIdentifierExtension_ToString_m4259594541 (SubjectKeyIdentifierExtension_t106392422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
