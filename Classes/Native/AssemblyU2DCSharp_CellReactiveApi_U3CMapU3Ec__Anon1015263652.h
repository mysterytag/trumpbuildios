﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// ICell`1<System.Object>
struct ICell_1_t2388737397;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Object,System.Object>
struct  U3CMapU3Ec__AnonStorey10E_2_t1015263652  : public Il2CppObject
{
public:
	// System.Func`2<T,T2> CellReactiveApi/<Map>c__AnonStorey10E`2::map
	Func_2_t2135783352 * ___map_0;
	// ICell`1<T> CellReactiveApi/<Map>c__AnonStorey10E`2::cell
	Il2CppObject* ___cell_1;

public:
	inline static int32_t get_offset_of_map_0() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey10E_2_t1015263652, ___map_0)); }
	inline Func_2_t2135783352 * get_map_0() const { return ___map_0; }
	inline Func_2_t2135783352 ** get_address_of_map_0() { return &___map_0; }
	inline void set_map_0(Func_2_t2135783352 * value)
	{
		___map_0 = value;
		Il2CppCodeGenWriteBarrier(&___map_0, value);
	}

	inline static int32_t get_offset_of_cell_1() { return static_cast<int32_t>(offsetof(U3CMapU3Ec__AnonStorey10E_2_t1015263652, ___cell_1)); }
	inline Il2CppObject* get_cell_1() const { return ___cell_1; }
	inline Il2CppObject** get_address_of_cell_1() { return &___cell_1; }
	inline void set_cell_1(Il2CppObject* value)
	{
		___cell_1 = value;
		Il2CppCodeGenWriteBarrier(&___cell_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
