﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserKongregateInfo
struct UserKongregateInfo_t155682308;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserKongregateInfo::.ctor()
extern "C"  void UserKongregateInfo__ctor_m465513529 (UserKongregateInfo_t155682308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserKongregateInfo::get_KongregateId()
extern "C"  String_t* UserKongregateInfo_get_KongregateId_m166118855 (UserKongregateInfo_t155682308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserKongregateInfo::set_KongregateId(System.String)
extern "C"  void UserKongregateInfo_set_KongregateId_m2338834564 (UserKongregateInfo_t155682308 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserKongregateInfo::get_KongregateName()
extern "C"  String_t* UserKongregateInfo_get_KongregateName_m868919415 (UserKongregateInfo_t155682308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserKongregateInfo::set_KongregateName(System.String)
extern "C"  void UserKongregateInfo_set_KongregateName_m3957878164 (UserKongregateInfo_t155682308 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
