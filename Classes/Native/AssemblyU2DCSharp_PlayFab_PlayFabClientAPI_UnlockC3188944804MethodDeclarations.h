﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlockContainerItemRequestCallback
struct UnlockContainerItemRequestCallback_t3188944804;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlockContainerItemRequest
struct UnlockContainerItemRequest_t987080271;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockContai987080271.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlockContainerItemRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlockContainerItemRequestCallback__ctor_m1620566675 (UnlockContainerItemRequestCallback_t3188944804 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlockContainerItemRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlockContainerItemRequest,System.Object)
extern "C"  void UnlockContainerItemRequestCallback_Invoke_m1711141023 (UnlockContainerItemRequestCallback_t3188944804 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerItemRequest_t987080271 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlockContainerItemRequestCallback_t3188944804(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerItemRequest_t987080271 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlockContainerItemRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlockContainerItemRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlockContainerItemRequestCallback_BeginInvoke_m3599773230 (UnlockContainerItemRequestCallback_t3188944804 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerItemRequest_t987080271 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlockContainerItemRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlockContainerItemRequestCallback_EndInvoke_m462525987 (UnlockContainerItemRequestCallback_t3188944804 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
