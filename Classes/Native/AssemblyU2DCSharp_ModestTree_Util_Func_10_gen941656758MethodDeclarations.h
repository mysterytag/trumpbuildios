﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_10_t941656758;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_10__ctor_m1413835974_gshared (Func_10_t941656758 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_10__ctor_m1413835974(__this, ___object0, ___method1, method) ((  void (*) (Func_10_t941656758 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_10__ctor_m1413835974_gshared)(__this, ___object0, ___method1, method)
// TResult ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7,T8,T9)
extern "C"  Il2CppObject * Func_10_Invoke_m2701011777_gshared (Func_10_t941656758 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, Il2CppObject * ___arg98, const MethodInfo* method);
#define Func_10_Invoke_m2701011777(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___arg98, method) ((  Il2CppObject * (*) (Func_10_t941656758 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Func_10_Invoke_m2701011777_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___arg98, method)
// System.IAsyncResult ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,T8,T9,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_10_BeginInvoke_m2118615612_gshared (Func_10_t941656758 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, Il2CppObject * ___arg98, AsyncCallback_t1363551830 * ___callback9, Il2CppObject * ___object10, const MethodInfo* method);
#define Func_10_BeginInvoke_m2118615612(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___arg98, ___callback9, ___object10, method) ((  Il2CppObject * (*) (Func_10_t941656758 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_10_BeginInvoke_m2118615612_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___arg98, ___callback9, ___object10, method)
// TResult ModestTree.Util.Func`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_10_EndInvoke_m3790753400_gshared (Func_10_t941656758 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_10_EndInvoke_m3790753400(__this, ___result0, method) ((  Il2CppObject * (*) (Func_10_t941656758 *, Il2CppObject *, const MethodInfo*))Func_10_EndInvoke_m3790753400_gshared)(__this, ___result0, method)
