﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Image
struct Image_t3354615620;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620.h"

// System.Void ImageExtension::SetBlackAndWhite(UnityEngine.UI.Image,System.Single)
extern "C"  void ImageExtension_SetBlackAndWhite_m3688469801 (Il2CppObject * __this /* static, unused */, Image_t3354615620 * ___img0, float ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
