﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellUtils.EmptyDisposable
struct EmptyDisposable_t1512564738;

#include "codegen/il2cpp-codegen.h"

// System.Void CellUtils.EmptyDisposable::.ctor()
extern "C"  void EmptyDisposable__ctor_m791146077 (EmptyDisposable_t1512564738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.EmptyDisposable::Dispose()
extern "C"  void EmptyDisposable_Dispose_m4275418970 (EmptyDisposable_t1512564738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
