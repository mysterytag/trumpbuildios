﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics/LocalyticsSessionWillOpen
struct LocalyticsSessionWillOpen_t2581002303;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillOpen::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsSessionWillOpen__ctor_m632890358 (LocalyticsSessionWillOpen_t2581002303 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillOpen::Invoke(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void LocalyticsSessionWillOpen_Invoke_m2750767463 (LocalyticsSessionWillOpen_t2581002303 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionWillOpen_t2581002303(Il2CppObject* delegate, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2);
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsSessionWillOpen::BeginInvoke(System.Boolean,System.Boolean,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsSessionWillOpen_BeginInvoke_m3906664340 (LocalyticsSessionWillOpen_t2581002303 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionWillOpen::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsSessionWillOpen_EndInvoke_m2314797318 (LocalyticsSessionWillOpen_t2581002303 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
