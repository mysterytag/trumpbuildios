﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.LifecycleHelper
struct LifecycleHelper_t3719100935;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.LifecycleHelper::.ctor()
extern "C"  void LifecycleHelper__ctor_m819661470 (LifecycleHelper_t3719100935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.LifecycleHelper::add_onApplicationPausedEvent(System.Action`1<System.Boolean>)
extern "C"  void LifecycleHelper_add_onApplicationPausedEvent_m1624423188 (LifecycleHelper_t3719100935 * __this, Action_1_t359458046 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.LifecycleHelper::remove_onApplicationPausedEvent(System.Action`1<System.Boolean>)
extern "C"  void LifecycleHelper_remove_onApplicationPausedEvent_m3150970401 (LifecycleHelper_t3719100935 * __this, Action_1_t359458046 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.LifecycleHelper::OnApplicationPause(System.Boolean)
extern "C"  void LifecycleHelper_OnApplicationPause_m3832293794 (LifecycleHelper_t3719100935 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
