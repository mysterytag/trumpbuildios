﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_6_t3119944383;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipFunc_6__ctor_m2969936593_gshared (ZipFunc_6_t3119944383 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ZipFunc_6__ctor_m2969936593(__this, ___object0, ___method1, method) ((  void (*) (ZipFunc_6_t3119944383 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ZipFunc_6__ctor_m2969936593_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5)
extern "C"  Il2CppObject * ZipFunc_6_Invoke_m3286375891_gshared (ZipFunc_6_t3119944383 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, const MethodInfo* method);
#define ZipFunc_6_Invoke_m3286375891(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method) ((  Il2CppObject * (*) (ZipFunc_6_t3119944383 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ZipFunc_6_Invoke_m3286375891_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, method)
// System.IAsyncResult UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipFunc_6_BeginInvoke_m3047228613_gshared (ZipFunc_6_t3119944383 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, AsyncCallback_t1363551830 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method);
#define ZipFunc_6_BeginInvoke_m3047228613(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___callback5, ___object6, method) ((  Il2CppObject * (*) (ZipFunc_6_t3119944383 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ZipFunc_6_BeginInvoke_m3047228613_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___callback5, ___object6, method)
// TR UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ZipFunc_6_EndInvoke_m3105979336_gshared (ZipFunc_6_t3119944383 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ZipFunc_6_EndInvoke_m3105979336(__this, ___result0, method) ((  Il2CppObject * (*) (ZipFunc_6_t3119944383 *, Il2CppObject *, const MethodInfo*))ZipFunc_6_EndInvoke_m3105979336_gshared)(__this, ___result0, method)
