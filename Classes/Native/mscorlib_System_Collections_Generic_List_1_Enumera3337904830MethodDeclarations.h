﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.Purchase>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1359702246(__this, ___l0, method) ((  void (*) (Enumerator_t3337904830 *, List_1_t957154542 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.Purchase>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m187904940(__this, method) ((  void (*) (Enumerator_t3337904830 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<OnePF.Purchase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3075999458(__this, method) ((  Il2CppObject * (*) (Enumerator_t3337904830 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.Purchase>::Dispose()
#define Enumerator_Dispose_m567021643(__this, method) ((  void (*) (Enumerator_t3337904830 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.Purchase>::VerifyState()
#define Enumerator_VerifyState_m2862133764(__this, method) ((  void (*) (Enumerator_t3337904830 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<OnePF.Purchase>::MoveNext()
#define Enumerator_MoveNext_m2755449605(__this, method) ((  bool (*) (Enumerator_t3337904830 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<OnePF.Purchase>::get_Current()
#define Enumerator_get_Current_m3326613047(__this, method) ((  Purchase_t160195573 * (*) (Enumerator_t3337904830 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
