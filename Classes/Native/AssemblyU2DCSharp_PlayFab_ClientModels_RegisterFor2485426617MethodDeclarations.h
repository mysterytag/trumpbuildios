﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RegisterForIOSPushNotificationResult
struct RegisterForIOSPushNotificationResult_t2485426617;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationResult::.ctor()
extern "C"  void RegisterForIOSPushNotificationResult__ctor_m431209892 (RegisterForIOSPushNotificationResult_t2485426617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
