﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsResult
struct GetPlayFabIDsFromGameCenterIDsResult_t627925241;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GameCenterPlayFabIdPair>
struct List_1_t3860087018;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromGameCenterIDsResult__ctor_m2729909860 (GetPlayFabIDsFromGameCenterIDsResult_t627925241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.GameCenterPlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsResult::get_Data()
extern "C"  List_1_t3860087018 * GetPlayFabIDsFromGameCenterIDsResult_get_Data_m1742347037 (GetPlayFabIDsFromGameCenterIDsResult_t627925241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromGameCenterIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.GameCenterPlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromGameCenterIDsResult_set_Data_m3641332654 (GetPlayFabIDsFromGameCenterIDsResult_t627925241 * __this, List_1_t3860087018 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
