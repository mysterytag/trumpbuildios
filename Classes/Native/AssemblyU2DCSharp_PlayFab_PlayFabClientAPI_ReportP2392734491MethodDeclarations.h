﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ReportPlayerResponseCallback
struct ReportPlayerResponseCallback_t2392734491;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ReportPlayerClientRequest
struct ReportPlayerClientRequest_t3784712447;
// PlayFab.ClientModels.ReportPlayerClientResult
struct ReportPlayerClientResult_t3759465933;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ReportPlaye3784712447.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ReportPlaye3759465933.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ReportPlayerResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ReportPlayerResponseCallback__ctor_m3206324874 (ReportPlayerResponseCallback_t2392734491 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ReportPlayerResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ReportPlayerClientRequest,PlayFab.ClientModels.ReportPlayerClientResult,PlayFab.PlayFabError,System.Object)
extern "C"  void ReportPlayerResponseCallback_Invoke_m1630966615 (ReportPlayerResponseCallback_t2392734491 * __this, String_t* ___urlPath0, int32_t ___callId1, ReportPlayerClientRequest_t3784712447 * ___request2, ReportPlayerClientResult_t3759465933 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReportPlayerResponseCallback_t2392734491(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ReportPlayerClientRequest_t3784712447 * ___request2, ReportPlayerClientResult_t3759465933 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ReportPlayerResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ReportPlayerClientRequest,PlayFab.ClientModels.ReportPlayerClientResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReportPlayerResponseCallback_BeginInvoke_m511380228 (ReportPlayerResponseCallback_t2392734491 * __this, String_t* ___urlPath0, int32_t ___callId1, ReportPlayerClientRequest_t3784712447 * ___request2, ReportPlayerClientResult_t3759465933 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ReportPlayerResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ReportPlayerResponseCallback_EndInvoke_m4134201754 (ReportPlayerResponseCallback_t2392734491 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
