﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Stack_1_t1588395187;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2195698409.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C"  void Enumerator__ctor_m3970752666_gshared (Enumerator_t2195698410 * __this, Stack_1_t1588395187 * ___t0, const MethodInfo* method);
#define Enumerator__ctor_m3970752666(__this, ___t0, method) ((  void (*) (Enumerator_t2195698410 *, Stack_1_t1588395187 *, const MethodInfo*))Enumerator__ctor_m3970752666_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1906801758_gshared (Enumerator_t2195698410 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1906801758(__this, method) ((  void (*) (Enumerator_t2195698410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1906801758_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3933016266_gshared (Enumerator_t2195698410 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3933016266(__this, method) ((  Il2CppObject * (*) (Enumerator_t2195698410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3933016266_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m2852088409_gshared (Enumerator_t2195698410 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2852088409(__this, method) ((  void (*) (Enumerator_t2195698410 *, const MethodInfo*))Enumerator_Dispose_m2852088409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3622732982_gshared (Enumerator_t2195698410 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3622732982(__this, method) ((  bool (*) (Enumerator_t2195698410 *, const MethodInfo*))Enumerator_MoveNext_m3622732982_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3312956448  Enumerator_get_Current_m2377937865_gshared (Enumerator_t2195698410 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2377937865(__this, method) ((  KeyValuePair_2_t3312956448  (*) (Enumerator_t2195698410 *, const MethodInfo*))Enumerator_get_Current_m2377937865_gshared)(__this, method)
