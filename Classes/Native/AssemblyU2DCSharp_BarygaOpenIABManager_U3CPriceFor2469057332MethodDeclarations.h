﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarygaOpenIABManager/<PriceFormatted>c__AnonStoreyEA
struct U3CPriceFormattedU3Ec__AnonStoreyEA_t2469057332;
// System.String
struct String_t;
// <>__AnonType4`2<System.Single,System.String>
struct U3CU3E__AnonType4_2_t4023684950;

#include "codegen/il2cpp-codegen.h"

// System.Void BarygaOpenIABManager/<PriceFormatted>c__AnonStoreyEA::.ctor()
extern "C"  void U3CPriceFormattedU3Ec__AnonStoreyEA__ctor_m1748099945 (U3CPriceFormattedU3Ec__AnonStoreyEA_t2469057332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BarygaOpenIABManager/<PriceFormatted>c__AnonStoreyEA::<>m__12E(<>__AnonType4`2<System.Single,System.String>)
extern "C"  String_t* U3CPriceFormattedU3Ec__AnonStoreyEA_U3CU3Em__12E_m2241491366 (U3CPriceFormattedU3Ec__AnonStoreyEA_t2469057332 * __this, U3CU3E__AnonType4_2_t4023684950 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
