﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage
struct LocalyticsWillDismissInAppMessage_t2580912496;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsWillDismissInAppMessage__ctor_m1599719591 (LocalyticsWillDismissInAppMessage_t2580912496 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage::Invoke()
extern "C"  void LocalyticsWillDismissInAppMessage_Invoke_m1116501953 (LocalyticsWillDismissInAppMessage_t2580912496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LocalyticsWillDismissInAppMessage_t2580912496(Il2CppObject* delegate);
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsWillDismissInAppMessage_BeginInvoke_m4217893922 (LocalyticsWillDismissInAppMessage_t2580912496 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsWillDismissInAppMessage_EndInvoke_m3485574711 (LocalyticsWillDismissInAppMessage_t2580912496 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
