﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2C
struct U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2C::.ctor()
extern "C"  void U3CShowBannerAtPositonU3Ec__AnonStorey2C__ctor_m1007824560 (U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2C::<>m__3(System.Object)
extern "C"  void U3CShowBannerAtPositonU3Ec__AnonStorey2C_U3CU3Em__3_m2695302680 (U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
