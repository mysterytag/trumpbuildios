﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateUserPublisherDataRequestCallback
struct UpdateUserPublisherDataRequestCallback_t3262855618;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateUserDataRequest
struct UpdateUserDataRequest_t3516896929;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserD3516896929.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateUserPublisherDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateUserPublisherDataRequestCallback__ctor_m2100059569 (UpdateUserPublisherDataRequestCallback_t3262855618 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserPublisherDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserDataRequest,System.Object)
extern "C"  void UpdateUserPublisherDataRequestCallback_Invoke_m3495088449 (UpdateUserPublisherDataRequestCallback_t3262855618 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserDataRequest_t3516896929 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateUserPublisherDataRequestCallback_t3262855618(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateUserDataRequest_t3516896929 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateUserPublisherDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateUserPublisherDataRequestCallback_BeginInvoke_m1076663546 (UpdateUserPublisherDataRequestCallback_t3262855618 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserDataRequest_t3516896929 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserPublisherDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateUserPublisherDataRequestCallback_EndInvoke_m4134293569 (UpdateUserPublisherDataRequestCallback_t3262855618 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
