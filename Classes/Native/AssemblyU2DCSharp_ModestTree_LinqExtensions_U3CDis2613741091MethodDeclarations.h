﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>
struct U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CDistinctByImplU3Ec__Iterator3C_2__ctor_m3111199782_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method);
#define U3CDistinctByImplU3Ec__Iterator3C_2__ctor_m3111199782(__this, method) ((  void (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))U3CDistinctByImplU3Ec__Iterator3C_2__ctor_m3111199782_gshared)(__this, method)
// TSource ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1603733615_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method);
#define U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1603733615(__this, method) ((  Il2CppObject * (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1603733615_gshared)(__this, method)
// System.Object ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_IEnumerator_get_Current_m428164938_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method);
#define U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_IEnumerator_get_Current_m428164938(__this, method) ((  Il2CppObject * (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_IEnumerator_get_Current_m428164938_gshared)(__this, method)
// System.Collections.IEnumerator ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_IEnumerable_GetEnumerator_m3673712651_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method);
#define U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_IEnumerable_GetEnumerator_m3673712651(__this, method) ((  Il2CppObject * (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_IEnumerable_GetEnumerator_m3673712651_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2685995522_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method);
#define U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2685995522(__this, method) ((  Il2CppObject* (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))U3CDistinctByImplU3Ec__Iterator3C_2_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2685995522_gshared)(__this, method)
// System.Boolean ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CDistinctByImplU3Ec__Iterator3C_2_MoveNext_m881064182_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method);
#define U3CDistinctByImplU3Ec__Iterator3C_2_MoveNext_m881064182(__this, method) ((  bool (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))U3CDistinctByImplU3Ec__Iterator3C_2_MoveNext_m881064182_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CDistinctByImplU3Ec__Iterator3C_2_Dispose_m464035555_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method);
#define U3CDistinctByImplU3Ec__Iterator3C_2_Dispose_m464035555(__this, method) ((  void (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))U3CDistinctByImplU3Ec__Iterator3C_2_Dispose_m464035555_gshared)(__this, method)
// System.Void ModestTree.LinqExtensions/<DistinctByImpl>c__Iterator3C`2<System.Object,System.Object>::Reset()
extern "C"  void U3CDistinctByImplU3Ec__Iterator3C_2_Reset_m757632723_gshared (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 * __this, const MethodInfo* method);
#define U3CDistinctByImplU3Ec__Iterator3C_2_Reset_m757632723(__this, method) ((  void (*) (U3CDistinctByImplU3Ec__Iterator3C_2_t2613741091 *, const MethodInfo*))U3CDistinctByImplU3Ec__Iterator3C_2_Reset_m757632723_gshared)(__this, method)
