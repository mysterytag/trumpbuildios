﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Tacticsoft.TableViewCell>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m418228873(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t755761836 *, int32_t, TableViewCell_t776419755 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Tacticsoft.TableViewCell>::get_Key()
#define KeyValuePair_2_get_Key_m924351167(__this, method) ((  int32_t (*) (KeyValuePair_2_t755761836 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Tacticsoft.TableViewCell>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2728492544(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t755761836 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Tacticsoft.TableViewCell>::get_Value()
#define KeyValuePair_2_get_Value_m1684889891(__this, method) ((  TableViewCell_t776419755 * (*) (KeyValuePair_2_t755761836 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Tacticsoft.TableViewCell>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1309528576(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t755761836 *, TableViewCell_t776419755 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Tacticsoft.TableViewCell>::ToString()
#define KeyValuePair_2_ToString_m923467080(__this, method) ((  String_t* (*) (KeyValuePair_2_t755761836 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
