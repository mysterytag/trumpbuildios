﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1997629214(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1564921418 *, Dictionary_2_t1797893477 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m185240909(__this, method) ((  Il2CppObject * (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3663475415(__this, method) ((  void (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2683723662(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2700860521(__this, method) ((  Il2CppObject * (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2415150267(__this, method) ((  Il2CppObject * (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::MoveNext()
#define Enumerator_MoveNext_m2289425636(__this, method) ((  bool (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::get_Current()
#define Enumerator_get_Current_m4289358153(__this, method) ((  KeyValuePair_2_t1286424775  (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m13751888(__this, method) ((  String_t* (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m438033680(__this, method) ((  Purchase_t160195573 * (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::Reset()
#define Enumerator_Reset_m3279828080(__this, method) ((  void (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::VerifyState()
#define Enumerator_VerifyState_m3352588409(__this, method) ((  void (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3356860833(__this, method) ((  void (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnePF.Purchase>::Dispose()
#define Enumerator_Dispose_m1932218688(__this, method) ((  void (*) (Enumerator_t1564921418 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
