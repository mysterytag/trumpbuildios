﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryMethodUntyped`1<System.Object>
struct FactoryMethodUntyped_1_t1598907365;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Func`3<Zenject.DiContainer,System.Object[],System.Object>
struct Func_3_t2319412757;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// System.Type[]
struct TypeU5BU5D_t3431720054;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"

// System.Void Zenject.FactoryMethodUntyped`1<System.Object>::.ctor(Zenject.DiContainer,System.Func`3<Zenject.DiContainer,System.Object[],TContract>)
extern "C"  void FactoryMethodUntyped_1__ctor_m4245491325_gshared (FactoryMethodUntyped_1_t1598907365 * __this, DiContainer_t2383114449 * ___container0, Func_3_t2319412757 * ___method1, const MethodInfo* method);
#define FactoryMethodUntyped_1__ctor_m4245491325(__this, ___container0, ___method1, method) ((  void (*) (FactoryMethodUntyped_1_t1598907365 *, DiContainer_t2383114449 *, Func_3_t2319412757 *, const MethodInfo*))FactoryMethodUntyped_1__ctor_m4245491325_gshared)(__this, ___container0, ___method1, method)
// TContract Zenject.FactoryMethodUntyped`1<System.Object>::Create(System.Object[])
extern "C"  Il2CppObject * FactoryMethodUntyped_1_Create_m2411369028_gshared (FactoryMethodUntyped_1_t1598907365 * __this, ObjectU5BU5D_t11523773* ___constructorArgs0, const MethodInfo* method);
#define FactoryMethodUntyped_1_Create_m2411369028(__this, ___constructorArgs0, method) ((  Il2CppObject * (*) (FactoryMethodUntyped_1_t1598907365 *, ObjectU5BU5D_t11523773*, const MethodInfo*))FactoryMethodUntyped_1_Create_m2411369028_gshared)(__this, ___constructorArgs0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.FactoryMethodUntyped`1<System.Object>::Validate(System.Type[])
extern "C"  Il2CppObject* FactoryMethodUntyped_1_Validate_m65064609_gshared (FactoryMethodUntyped_1_t1598907365 * __this, TypeU5BU5D_t3431720054* ___extras0, const MethodInfo* method);
#define FactoryMethodUntyped_1_Validate_m65064609(__this, ___extras0, method) ((  Il2CppObject* (*) (FactoryMethodUntyped_1_t1598907365 *, TypeU5BU5D_t3431720054*, const MethodInfo*))FactoryMethodUntyped_1_Validate_m65064609_gshared)(__this, ___extras0, method)
