﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactoryBinder_5_t1497530694;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// ModestTree.Util.Func`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_6_t3355333698;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,System.String)
extern "C"  void IFactoryBinder_5__ctor_m3485687515_gshared (IFactoryBinder_5_t1497530694 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, const MethodInfo* method);
#define IFactoryBinder_5__ctor_m3485687515(__this, ___container0, ___identifier1, method) ((  void (*) (IFactoryBinder_5_t1497530694 *, DiContainer_t2383114449 *, String_t*, const MethodInfo*))IFactoryBinder_5__ctor_m3485687515_gshared)(__this, ___container0, ___identifier1, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToMethod(ModestTree.Util.Func`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TContract>)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToMethod_m448282366_gshared (IFactoryBinder_5_t1497530694 * __this, Func_6_t3355333698 * ___method0, const MethodInfo* method);
#define IFactoryBinder_5_ToMethod_m448282366(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, Func_6_t3355333698 *, const MethodInfo*))IFactoryBinder_5_ToMethod_m448282366_gshared)(__this, ___method0, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToFactory()
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToFactory_m4119533772_gshared (IFactoryBinder_5_t1497530694 * __this, const MethodInfo* method);
#define IFactoryBinder_5_ToFactory_m4119533772(__this, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, const MethodInfo*))IFactoryBinder_5_ToFactory_m4119533772_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.IFactoryBinder`5<System.Object,System.Object,System.Object,System.Object,System.Object>::ToPrefab(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * IFactoryBinder_5_ToPrefab_m185350300_gshared (IFactoryBinder_5_t1497530694 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method);
#define IFactoryBinder_5_ToPrefab_m185350300(__this, ___prefab0, method) ((  BindingConditionSetter_t259147722 * (*) (IFactoryBinder_5_t1497530694 *, GameObject_t4012695102 *, const MethodInfo*))IFactoryBinder_5_ToPrefab_m185350300_gshared)(__this, ___prefab0, method)
