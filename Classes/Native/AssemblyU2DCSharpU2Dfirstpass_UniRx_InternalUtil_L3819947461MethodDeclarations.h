﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>
struct ListObserver_1_t3819947461;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>
struct ImmutableList_1_t2502565896;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.Vector4>
struct IObserver_1_t1442361397;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1015813821_gshared (ListObserver_1_t3819947461 * __this, ImmutableList_1_t2502565896 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m1015813821(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3819947461 *, ImmutableList_1_t2502565896 *, const MethodInfo*))ListObserver_1__ctor_m1015813821_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3837991477_gshared (ListObserver_1_t3819947461 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m3837991477(__this, method) ((  void (*) (ListObserver_1_t3819947461 *, const MethodInfo*))ListObserver_1_OnCompleted_m3837991477_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2763465698_gshared (ListObserver_1_t3819947461 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2763465698(__this, ___error0, method) ((  void (*) (ListObserver_1_t3819947461 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2763465698_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m760024787_gshared (ListObserver_1_t3819947461 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m760024787(__this, ___value0, method) ((  void (*) (ListObserver_1_t3819947461 *, Vector4_t3525329790 , const MethodInfo*))ListObserver_1_OnNext_m760024787_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3484725157_gshared (ListObserver_1_t3819947461 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m3484725157(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3819947461 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m3484725157_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector4>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m226086278_gshared (ListObserver_1_t3819947461 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m226086278(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3819947461 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m226086278_gshared)(__this, ___observer0, method)
