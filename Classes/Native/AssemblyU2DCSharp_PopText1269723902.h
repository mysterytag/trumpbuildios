﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextMesh
struct TextMesh_t583678247;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopText
struct  PopText_t1269723902  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.TextMesh PopText::text
	TextMesh_t583678247 * ___text_2;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(PopText_t1269723902, ___text_2)); }
	inline TextMesh_t583678247 * get_text_2() const { return ___text_2; }
	inline TextMesh_t583678247 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(TextMesh_t583678247 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
