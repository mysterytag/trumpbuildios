﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen3765090877MethodDeclarations.h"

// System.Void System.Func`3<System.Action`1<System.Int32>,Priority,System.IDisposable>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m2494461244(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t722536535 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m112902395_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Action`1<System.Int32>,Priority,System.IDisposable>::Invoke(T1,T2)
#define Func_3_Invoke_m3229106573(__this, ___arg10, ___arg21, method) ((  Il2CppObject * (*) (Func_3_t722536535 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))Func_3_Invoke_m1059675506_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Action`1<System.Int32>,Priority,System.IDisposable>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m3645882254(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t722536535 *, Action_1_t2995867492 *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3594911735_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Action`1<System.Int32>,Priority,System.IDisposable>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m3320381678(__this, ___result0, method) ((  Il2CppObject * (*) (Func_3_t722536535 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m2561706921_gshared)(__this, ___result0, method)
