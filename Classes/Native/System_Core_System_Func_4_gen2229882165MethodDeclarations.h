﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_4_gen3628925182MethodDeclarations.h"

// System.Void System.Func`4<Zenject.DiContainer,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
#define Func_4__ctor_m1171481031(__this, ___object0, ___method1, method) ((  void (*) (Func_4_t2229882165 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_4__ctor_m283050854_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`4<Zenject.DiContainer,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
#define Func_4_Invoke_m3890806373(__this, ___arg10, ___arg21, ___arg32, method) ((  Il2CppObject * (*) (Func_4_t2229882165 *, DiContainer_t2383114449 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Func_4_Invoke_m4260158276_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Func`4<Zenject.DiContainer,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Func_4_BeginInvoke_m741624954(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Func_4_t2229882165 *, DiContainer_t2383114449 *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_4_BeginInvoke_m1091833817_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// TResult System.Func`4<Zenject.DiContainer,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
#define Func_4_EndInvoke_m937249273(__this, ___result0, method) ((  Il2CppObject * (*) (Func_4_t2229882165 *, Il2CppObject *, const MethodInfo*))Func_4_EndInvoke_m3792811160_gshared)(__this, ___result0, method)
