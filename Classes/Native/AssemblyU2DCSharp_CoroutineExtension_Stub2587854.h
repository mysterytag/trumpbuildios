﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroutineExtension/Stub
struct  Stub_t2587854  : public Il2CppObject
{
public:
	// System.Boolean CoroutineExtension/Stub::called
	bool ___called_0;

public:
	inline static int32_t get_offset_of_called_0() { return static_cast<int32_t>(offsetof(Stub_t2587854, ___called_0)); }
	inline bool get_called_0() const { return ___called_0; }
	inline bool* get_address_of_called_0() { return &___called_0; }
	inline void set_called_0(bool value)
	{
		___called_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
