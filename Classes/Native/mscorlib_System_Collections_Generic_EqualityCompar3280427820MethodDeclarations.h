﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<LitJson.PropertyMetadata>
struct DefaultComparer_t3280427820;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<LitJson.PropertyMetadata>::.ctor()
extern "C"  void DefaultComparer__ctor_m552479615_gshared (DefaultComparer_t3280427820 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m552479615(__this, method) ((  void (*) (DefaultComparer_t3280427820 *, const MethodInfo*))DefaultComparer__ctor_m552479615_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<LitJson.PropertyMetadata>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2887158420_gshared (DefaultComparer_t3280427820 * __this, PropertyMetadata_t3942474089  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2887158420(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3280427820 *, PropertyMetadata_t3942474089 , const MethodInfo*))DefaultComparer_GetHashCode_m2887158420_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<LitJson.PropertyMetadata>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1062264340_gshared (DefaultComparer_t3280427820 * __this, PropertyMetadata_t3942474089  ___x0, PropertyMetadata_t3942474089  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1062264340(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3280427820 *, PropertyMetadata_t3942474089 , PropertyMetadata_t3942474089 , const MethodInfo*))DefaultComparer_Equals_m1062264340_gshared)(__this, ___x0, ___y1, method)
