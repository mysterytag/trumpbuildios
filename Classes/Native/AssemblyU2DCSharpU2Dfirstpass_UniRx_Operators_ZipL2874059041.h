﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ZipLatestObservable`1<System.Object>
struct ZipLatestObservable_1_t2717078458;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Boolean[]
struct BooleanU5BU5D_t3804927312;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4067176365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>
struct  ZipLatest_t2874059041  : public OperatorObserverBase_2_t4067176365
{
public:
	// UniRx.Operators.ZipLatestObservable`1<T> UniRx.Operators.ZipLatestObservable`1/ZipLatest::parent
	ZipLatestObservable_1_t2717078458 * ___parent_2;
	// System.Object UniRx.Operators.ZipLatestObservable`1/ZipLatest::gate
	Il2CppObject * ___gate_3;
	// System.Int32 UniRx.Operators.ZipLatestObservable`1/ZipLatest::length
	int32_t ___length_4;
	// T[] UniRx.Operators.ZipLatestObservable`1/ZipLatest::values
	ObjectU5BU5D_t11523773* ___values_5;
	// System.Boolean[] UniRx.Operators.ZipLatestObservable`1/ZipLatest::isStarted
	BooleanU5BU5D_t3804927312* ___isStarted_6;
	// System.Boolean[] UniRx.Operators.ZipLatestObservable`1/ZipLatest::isCompleted
	BooleanU5BU5D_t3804927312* ___isCompleted_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ZipLatest_t2874059041, ___parent_2)); }
	inline ZipLatestObservable_1_t2717078458 * get_parent_2() const { return ___parent_2; }
	inline ZipLatestObservable_1_t2717078458 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ZipLatestObservable_1_t2717078458 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(ZipLatest_t2874059041, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(ZipLatest_t2874059041, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}

	inline static int32_t get_offset_of_values_5() { return static_cast<int32_t>(offsetof(ZipLatest_t2874059041, ___values_5)); }
	inline ObjectU5BU5D_t11523773* get_values_5() const { return ___values_5; }
	inline ObjectU5BU5D_t11523773** get_address_of_values_5() { return &___values_5; }
	inline void set_values_5(ObjectU5BU5D_t11523773* value)
	{
		___values_5 = value;
		Il2CppCodeGenWriteBarrier(&___values_5, value);
	}

	inline static int32_t get_offset_of_isStarted_6() { return static_cast<int32_t>(offsetof(ZipLatest_t2874059041, ___isStarted_6)); }
	inline BooleanU5BU5D_t3804927312* get_isStarted_6() const { return ___isStarted_6; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isStarted_6() { return &___isStarted_6; }
	inline void set_isStarted_6(BooleanU5BU5D_t3804927312* value)
	{
		___isStarted_6 = value;
		Il2CppCodeGenWriteBarrier(&___isStarted_6, value);
	}

	inline static int32_t get_offset_of_isCompleted_7() { return static_cast<int32_t>(offsetof(ZipLatest_t2874059041, ___isCompleted_7)); }
	inline BooleanU5BU5D_t3804927312* get_isCompleted_7() const { return ___isCompleted_7; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isCompleted_7() { return &___isCompleted_7; }
	inline void set_isCompleted_7(BooleanU5BU5D_t3804927312* value)
	{
		___isCompleted_7 = value;
		Il2CppCodeGenWriteBarrier(&___isCompleted_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
