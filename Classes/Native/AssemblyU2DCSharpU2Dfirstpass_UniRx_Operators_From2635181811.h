﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Action`1<System.Object>>
struct Action_1_t1134011830;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable_`1<System.Object>
struct  FromEventObservable__1_t2635181811  : public OperatorObservableBase_1_t4196218687
{
public:
	// System.Action`1<System.Action`1<T>> UniRx.Operators.FromEventObservable_`1::addHandler
	Action_1_t1134011830 * ___addHandler_1;
	// System.Action`1<System.Action`1<T>> UniRx.Operators.FromEventObservable_`1::removeHandler
	Action_1_t1134011830 * ___removeHandler_2;

public:
	inline static int32_t get_offset_of_addHandler_1() { return static_cast<int32_t>(offsetof(FromEventObservable__1_t2635181811, ___addHandler_1)); }
	inline Action_1_t1134011830 * get_addHandler_1() const { return ___addHandler_1; }
	inline Action_1_t1134011830 ** get_address_of_addHandler_1() { return &___addHandler_1; }
	inline void set_addHandler_1(Action_1_t1134011830 * value)
	{
		___addHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___addHandler_1, value);
	}

	inline static int32_t get_offset_of_removeHandler_2() { return static_cast<int32_t>(offsetof(FromEventObservable__1_t2635181811, ___removeHandler_2)); }
	inline Action_1_t1134011830 * get_removeHandler_2() const { return ___removeHandler_2; }
	inline Action_1_t1134011830 ** get_address_of_removeHandler_2() { return &___removeHandler_2; }
	inline void set_removeHandler_2(Action_1_t1134011830 * value)
	{
		___removeHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&___removeHandler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
