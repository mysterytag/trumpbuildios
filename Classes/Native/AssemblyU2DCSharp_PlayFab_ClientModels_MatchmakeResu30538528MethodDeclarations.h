﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.MatchmakeResult
struct MatchmakeResult_t30538528;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen2958521481.h"

// System.Void PlayFab.ClientModels.MatchmakeResult::.ctor()
extern "C"  void MatchmakeResult__ctor_m545669257 (MatchmakeResult_t30538528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeResult::get_LobbyID()
extern "C"  String_t* MatchmakeResult_get_LobbyID_m1869227948 (MatchmakeResult_t30538528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeResult::set_LobbyID(System.String)
extern "C"  void MatchmakeResult_set_LobbyID_m468652583 (MatchmakeResult_t30538528 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeResult::get_ServerHostname()
extern "C"  String_t* MatchmakeResult_get_ServerHostname_m3062055773 (MatchmakeResult_t30538528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeResult::set_ServerHostname(System.String)
extern "C"  void MatchmakeResult_set_ServerHostname_m3355871828 (MatchmakeResult_t30538528 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.MatchmakeResult::get_ServerPort()
extern "C"  Nullable_1_t1438485399  MatchmakeResult_get_ServerPort_m676658807 (MatchmakeResult_t30538528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeResult::set_ServerPort(System.Nullable`1<System.Int32>)
extern "C"  void MatchmakeResult_set_ServerPort_m3987972498 (MatchmakeResult_t30538528 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeResult::get_Ticket()
extern "C"  String_t* MatchmakeResult_get_Ticket_m2895155795 (MatchmakeResult_t30538528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeResult::set_Ticket(System.String)
extern "C"  void MatchmakeResult_set_Ticket_m1381743134 (MatchmakeResult_t30538528 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.MatchmakeResult::get_Expires()
extern "C"  String_t* MatchmakeResult_get_Expires_m4171866799 (MatchmakeResult_t30538528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeResult::set_Expires(System.String)
extern "C"  void MatchmakeResult_set_Expires_m109630468 (MatchmakeResult_t30538528 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.MatchmakeResult::get_PollWaitTimeMS()
extern "C"  Nullable_1_t1438485399  MatchmakeResult_get_PollWaitTimeMS_m2588024442 (MatchmakeResult_t30538528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeResult::set_PollWaitTimeMS(System.Nullable`1<System.Int32>)
extern "C"  void MatchmakeResult_set_PollWaitTimeMS_m3188423599 (MatchmakeResult_t30538528 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus> PlayFab.ClientModels.MatchmakeResult::get_Status()
extern "C"  Nullable_1_t2958521481  MatchmakeResult_get_Status_m2210551709 (MatchmakeResult_t30538528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.MatchmakeResult::set_Status(System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>)
extern "C"  void MatchmakeResult_set_Status_m1088729692 (MatchmakeResult_t30538528 * __this, Nullable_1_t2958521481  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
