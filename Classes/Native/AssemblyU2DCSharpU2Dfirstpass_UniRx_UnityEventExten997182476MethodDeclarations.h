﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9B`4<System.Object,System.Object,System.Object,System.Object>
struct U3CAsObservableU3Ec__AnonStorey9B_4_t997182476;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9B`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey9B_4__ctor_m908265610_gshared (U3CAsObservableU3Ec__AnonStorey9B_4_t997182476 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey9B_4__ctor_m908265610(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey9B_4_t997182476 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey9B_4__ctor_m908265610_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9B`4<System.Object,System.Object,System.Object,System.Object>::<>m__D7(T0,T1,T2,T3)
extern "C"  void U3CAsObservableU3Ec__AnonStorey9B_4_U3CU3Em__D7_m2786442174_gshared (U3CAsObservableU3Ec__AnonStorey9B_4_t997182476 * __this, Il2CppObject * ___t00, Il2CppObject * ___t11, Il2CppObject * ___t22, Il2CppObject * ___t33, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey9B_4_U3CU3Em__D7_m2786442174(__this, ___t00, ___t11, ___t22, ___t33, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey9B_4_t997182476 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey9B_4_U3CU3Em__D7_m2786442174_gshared)(__this, ___t00, ___t11, ___t22, ___t33, method)
