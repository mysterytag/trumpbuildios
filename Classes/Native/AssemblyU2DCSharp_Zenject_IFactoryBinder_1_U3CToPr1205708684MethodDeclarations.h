﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`1/<ToPrefab>c__AnonStorey175<System.Object>
struct U3CToPrefabU3Ec__AnonStorey175_t1205708684;
// Zenject.IFactory`1<System.Object>
struct IFactory_1_t2124284520;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`1/<ToPrefab>c__AnonStorey175<System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey175__ctor_m1665939678_gshared (U3CToPrefabU3Ec__AnonStorey175_t1205708684 * __this, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey175__ctor_m1665939678(__this, method) ((  void (*) (U3CToPrefabU3Ec__AnonStorey175_t1205708684 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey175__ctor_m1665939678_gshared)(__this, method)
// Zenject.IFactory`1<TContract> Zenject.IFactoryBinder`1/<ToPrefab>c__AnonStorey175<System.Object>::<>m__297(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey175_U3CU3Em__297_m1482902648_gshared (U3CToPrefabU3Ec__AnonStorey175_t1205708684 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey175_U3CU3Em__297_m1482902648(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToPrefabU3Ec__AnonStorey175_t1205708684 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey175_U3CU3Em__297_m1482902648_gshared)(__this, ___ctx0, method)
