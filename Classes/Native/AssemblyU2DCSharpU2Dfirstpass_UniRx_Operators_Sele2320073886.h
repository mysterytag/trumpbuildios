﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>
struct SelectManyObservable_3_t744723289;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Object,System.Int64,System.Object>
struct  SelectManyEnumerableObserverWithIndex_t2320073886  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult> UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex::parent
	SelectManyObservable_3_t744723289 * ___parent_2;
	// System.Int32 UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectManyEnumerableObserverWithIndex_t2320073886, ___parent_2)); }
	inline SelectManyObservable_3_t744723289 * get_parent_2() const { return ___parent_2; }
	inline SelectManyObservable_3_t744723289 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyObservable_3_t744723289 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(SelectManyEnumerableObserverWithIndex_t2320073886, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
