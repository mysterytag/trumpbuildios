﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PersistentData
struct PersistentData_t2127272705;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PersistentData::.ctor()
extern "C"  void PersistentData__ctor_m3346137786 (PersistentData_t2127272705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PersistentData::GetFilePath(System.String,System.Int32)
extern "C"  String_t* PersistentData_GetFilePath_m3905711329 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, int32_t ___saveId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
