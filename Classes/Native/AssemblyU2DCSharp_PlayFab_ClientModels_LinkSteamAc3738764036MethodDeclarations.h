﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkSteamAccountResult
struct LinkSteamAccountResult_t3738764036;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkSteamAccountResult::.ctor()
extern "C"  void LinkSteamAccountResult__ctor_m4274825785 (LinkSteamAccountResult_t3738764036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
