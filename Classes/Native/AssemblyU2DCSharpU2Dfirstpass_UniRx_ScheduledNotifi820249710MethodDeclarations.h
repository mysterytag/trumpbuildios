﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Object>
struct U3CReportU3Ec__AnonStorey59_t820249710;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Object>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey59__ctor_m279451405_gshared (U3CReportU3Ec__AnonStorey59_t820249710 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey59__ctor_m279451405(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey59_t820249710 *, const MethodInfo*))U3CReportU3Ec__AnonStorey59__ctor_m279451405_gshared)(__this, method)
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Object>::<>m__6D()
extern "C"  void U3CReportU3Ec__AnonStorey59_U3CU3Em__6D_m278068708_gshared (U3CReportU3Ec__AnonStorey59_t820249710 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey59_U3CU3Em__6D_m278068708(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey59_t820249710 *, const MethodInfo*))U3CReportU3Ec__AnonStorey59_U3CU3Em__6D_m278068708_gshared)(__this, method)
