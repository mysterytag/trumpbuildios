﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeUntilObservable`2<System.Object,System.Object>
struct TakeUntilObservable_2_t4246198466;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TakeUntilObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<TOther>)
extern "C"  void TakeUntilObservable_2__ctor_m2718047514_gshared (TakeUntilObservable_2_t4246198466 * __this, Il2CppObject* ___source0, Il2CppObject* ___other1, const MethodInfo* method);
#define TakeUntilObservable_2__ctor_m2718047514(__this, ___source0, ___other1, method) ((  void (*) (TakeUntilObservable_2_t4246198466 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))TakeUntilObservable_2__ctor_m2718047514_gshared)(__this, ___source0, ___other1, method)
// System.IDisposable UniRx.Operators.TakeUntilObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeUntilObservable_2_SubscribeCore_m3785682308_gshared (TakeUntilObservable_2_t4246198466 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TakeUntilObservable_2_SubscribeCore_m3785682308(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TakeUntilObservable_2_t4246198466 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeUntilObservable_2_SubscribeCore_m3785682308_gshared)(__this, ___observer0, ___cancel1, method)
