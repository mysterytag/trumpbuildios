﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GameServerRegionsRequest
struct GameServerRegionsRequest_t2476765941;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GameServerRegionsRequest::.ctor()
extern "C"  void GameServerRegionsRequest__ctor_m957134568 (GameServerRegionsRequest_t2476765941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameServerRegionsRequest::get_BuildVersion()
extern "C"  String_t* GameServerRegionsRequest_get_BuildVersion_m3813179180 (GameServerRegionsRequest_t2476765941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameServerRegionsRequest::set_BuildVersion(System.String)
extern "C"  void GameServerRegionsRequest_set_BuildVersion_m4229456255 (GameServerRegionsRequest_t2476765941 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameServerRegionsRequest::get_TitleId()
extern "C"  String_t* GameServerRegionsRequest_get_TitleId_m862606099 (GameServerRegionsRequest_t2476765941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameServerRegionsRequest::set_TitleId(System.String)
extern "C"  void GameServerRegionsRequest_set_TitleId_m3640554246 (GameServerRegionsRequest_t2476765941 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
